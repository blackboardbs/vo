#!/bin/bash

# Database details
SOURCE_DB="tenante85ecbee-f9f9-4591-8b7a-12040c7a667b"
TARGET_DB="tenant7d51380f-64a0-4b1c-a1e9-5ad6b9259afb"
DB_USER="gandalf"
DB_PASS="a<V({2KvY8?>jd5*"

# MySQL command prefix
MYSQL_CMD="mysql -u$DB_USER -p$DB_PASS"

# Function to get view dependencies
get_dependencies() {
    echo "$1" | grep -i "from" | grep -oP '`\K[^`]+' | sort -u
}

# Create the target database if it doesn't exist
$MYSQL_CMD -e "CREATE DATABASE IF NOT EXISTS $(TARGET_DB);"

# Copy tables (structure and data)
echo "Copying tables..."
$MYSQL_CMD -e "
    SET SESSION group_concat_max_len = 1000000;
    SELECT GROUP_CONCAT(table_name SEPARATOR ' ')
    FROM information_schema.tables
    WHERE table_schema = $(SOURCE_DB) AND table_type = 'BASE TABLE'
" | while read -r tables; do
    $MYSQL_CMD -e "USE $(SOURCE_DB); SET sql_mode = ''; SELECT * FROM $tables" |
    $MYSQL_CMD "$(TARGET_DB)"
done

# Get and sort views
echo "Sorting views..."
views=$($MYSQL_CMD -N -e "
    SELECT table_name
    FROM information_schema.views
    WHERE table_schema = $(SOURCE_DB)
")

sorted_views=""
while [ "$views" != "$sorted_views" ]; do
    for view in $views; do
        view_def=$($MYSQL_CMD -N -e "SHOW CREATE VIEW $(SOURCE_DB).$(view)" | sed -n 's/.*CREATE.*VIEW.*AS //p')
        deps=$(get_dependencies "$view_def")
        if [ -z "$deps" ] || [[ $sorted_views =~ $deps ]]; then
            if [[ ! $sorted_views =~ $view ]]; then
                sorted_views="$sorted_views $view"
            fi
        fi
    done
done

# Create views in the correct order
echo "Creating views..."
for view in $sorted_views; do
    $MYSQL_CMD -e "
        USE $(TARGET_DB);
        SET sql_mode = '';
        DROP VIEW IF EXISTS $view;
        $(MYSQL_CMD -N -e "SHOW CREATE VIEW $(SOURCE_DB).$(view)" | sed 's/CREATE/CREATE OR REPLACE/')
    "
done

echo "Database copy completed successfully!"