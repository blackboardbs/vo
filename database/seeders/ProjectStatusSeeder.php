<?php

namespace Database\Seeders;

use App\Models\ProjectStatus;
use Illuminate\Database\Seeder;

class ProjectStatusSeeder extends Seeder
{
    public function run(): void
    {
        ProjectStatus::truncate();

        $projectStatus = tenancy()->central(fn() => \App\Models\Central\ProjectStatus::all());

        ProjectStatus::insert($projectStatus->toArray());
    }
}
