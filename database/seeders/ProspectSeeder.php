<?php

namespace Database\Seeders;

use App\Models\Prospect;
use Illuminate\Database\Seeder;

class ProspectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Prospect::insert([
            ['id' => '1', 'prospect_name' => 'Central City Bank', 'solution' => 'New MS SQL Data Warehouse', 'scope' => 'MS SQL, SSRS, Power BI implementation, design, support.', 'hours' => '2000', 'value' => '200000', 'resources' => '2 x fte', 'decision_date' => '2018-11-01', 'est_start_date' => '2019-07-31', 'chance' => '85', 'partners' => 'ABC Technology', 'note' => '', 'contact' => 'Joe Blog', 'status_id' => '1', 'company_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
        ]);
    }
}
