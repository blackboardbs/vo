<?php

namespace Database\Seeders;

use App\Models\RejectionReason;
use Illuminate\Database\Seeder;

class RejectionReasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RejectionReason::truncate();

        $rejectionReason = tenancy()->central(fn() => \App\Models\Central\RejectionReason::all());

        RejectionReason::insert($rejectionReason->toArray());
    }
}
