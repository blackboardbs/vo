<?php

namespace Database\Seeders;

use App\Models\BusinessFunction;
use Illuminate\Database\Seeder;

class BusinessFunctionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        BusinessFunction::truncate();

        $businessFunction = tenancy()->central(fn() => \App\Models\Central\BusinessFunction::all());

        BusinessFunction::insert($businessFunction->toArray());
    }
}
