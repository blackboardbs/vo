<?php

namespace Database\Seeders;

use App\Models\BillingCycle;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BillingCycleSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BillingCycle::truncate();

        $billingCycle = tenancy()->central(fn() => \App\Models\Central\BillingCycle::all());

        BillingCycle::insert($billingCycle->toArray());
    }
}
