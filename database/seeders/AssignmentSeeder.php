<?php

namespace Database\Seeders;

use App\Models\Assignment;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/assignment.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $assignment = new Assignment;
            $assignment->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $assignment->employee_id = isset($record['employee_id']) && $record['employee_id'] != '' && $record['employee_id'] != 'NULL' ? $record['employee_id'] : null;
            $assignment->project_id = isset($record['project_id']) && $record['project_id'] != '' && $record['project_id'] != 'NULL' ? $record['project_id'] : null;
            $assignment->start_date = isset($record['start_date']) && $record['start_date'] != '' && $record['start_date'] != 'NULL' ? $record['start_date'] : null;
            $assignment->end_date = isset($record['end_date']) && $record['end_date'] != '' && $record['end_date'] != 'NULL' ? $record['end_date'] : null;
            $assignment->rate = isset($record['rate']) && $record['rate'] != '' && $record['rate'] != 'NULL' ? $record['rate'] : null;
            $assignment->bonus_rate = isset($record['bonus_rate']) && $record['bonus_rate'] != '' && $record['bonus_rate'] != 'NULL' ? $record['bonus_rate'] : null;
            $assignment->hours = isset($record['hours']) && $record['hours'] != '' && $record['hours'] != 'NULL' ? $record['hours'] : null;
            $assignment->number_hours = isset($record['number_hours']) && $record['number_hours'] != '' && $record['number_hours'] != 'NULL' ? $record['number_hours'] : null;
            $assignment->vendor_id = isset($record['vendor_id']) && $record['vendor_id'] != '' && $record['vendor_id'] != 'NULL' ? $record['vendor_id'] : null;
            $assignment->report_to = isset($record['report_to']) && $record['report_to'] != '' && $record['report_to'] != 'NULL' ? $record['report_to'] : null;
            $assignment->report_to_email = isset($record['report_to_email']) && $record['report_to_email'] != '' && $record['report_to_email'] != 'NULL' ? $record['report_to_email'] : null;
            $assignment->report_phone = isset($record['report_phone']) && $record['report_phone'] != '' && $record['report_phone'] != 'NULL' ? $record['report_phone'] : null;
            $assignment->billable = isset($record['billable']) && $record['billable'] != '' && $record['billable'] != 'NULL' ? $record['billable'] : null;
            $assignment->note_1 = isset($record['note_1']) && $record['note_1'] != '' && $record['note_1'] != 'NULL' ? $record['note_1'] : null;
            $assignment->note_2 = isset($record['note_2']) && $record['note_2'] != '' && $record['note_2'] != 'NULL' ? $record['note_2'] : null;
            $assignment->exp_id = isset($record['exp_id']) && $record['exp_id'] != '' && $record['exp_id'] != 'NULL' ? $record['exp_id'] : null;
            $assignment->function = isset($record['function']) && $record['function'] != '' && $record['function'] != 'NULL' ? $record['function'] : null;
            $assignment->system = isset($record['system']) && $record['system'] != '' && $record['system'] != 'NULL' ? $record['system'] : null;
            $assignment->country_id = isset($record['country_id']) && $record['country_id'] != '' && $record['country_id'] != 'NULL' ? $record['country_id'] : null;
            $assignment->vat_rate_id = isset($record['vat_rate_id']) && $record['vat_rate_id'] != '' && $record['vat_rate_id'] != 'NULL' ? $record['vat_rate_id'] : null;
            $assignment->currency = isset($record['currency']) && $record['currency'] != '' && $record['currency'] != 'NULL' ? $record['currency'] : null;
            $assignment->internal_cost_rate = isset($record['internal_cost_rate']) && $record['internal_cost_rate'] != '' && $record['internal_cost_rate'] != 'NULL' ? $record['internal_cost_rate'] : null;
            $assignment->external_cost_rate = isset($record['external_cost_rate']) && $record['external_cost_rate'] != '' && $record['external_cost_rate'] != 'NULL' ? $record['external_cost_rate'] : null;
            $assignment->invoice_rate = isset($record['invoice_rate']) && $record['invoice_rate'] != '' && $record['invoice_rate'] != 'NULL' ? $record['invoice_rate'] : null;
            $assignment->extension_id = isset($record['extension_id']) && $record['extension_id'] != '' && $record['extension_id'] != 'NULL' ? $record['extension_id'] : null;
            $assignment->assignment_status = isset($record['assignment_status']) && $record['assignment_status'] != '' && $record['assignment_status'] != 'NULL' ? $record['assignment_status'] : null;
            $assignment->claim_approver_id = isset($record['claim_approver_id']) && $record['claim_approver_id'] != '' && $record['claim_approver_id'] != 'NULL' ? $record['claim_approver_id'] : null;
            $assignment->medical_certificate_type = isset($record['medical_certificate_type']) && $record['medical_certificate_type'] != '' && $record['medical_certificate_type'] != 'NULL' ? $record['medical_certificate_type'] : null;
            $assignment->medical_certificate_text = isset($record['medical_certificate_text']) && $record['medical_certificate_text'] != '' && $record['medical_certificate_text'] != 'NULL' ? $record['medical_certificate_text'] : null;
            $assignment->assignment_approver = isset($record['assignment_approver']) && $record['assignment_approver'] != '' && $record['assignment_approver'] != 'NULL' ? $record['assignment_approver'] : null;
            $assignment->project_type = isset($record['project_type']) && $record['project_type'] != '' && $record['project_type'] != 'NULL' ? $record['project_type'] : null;
            $assignment->status = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : null;
            $assignment->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : null;
            $assignment->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            $assignment->location = isset($record['location']) && $record['location'] != '' && $record['location'] != 'NULL' ? $record['location'] : null;
            $assignment->hours_of_work = isset($record['hours_of_work']) && $record['hours_of_work'] != '' && $record['hours_of_work'] != 'NULL' ? $record['hours_of_work'] : null;
            $assignment->vendor_template_id = isset($record['vendor_template_id']) && $record['vendor_template_id'] != '' && $record['vendor_template_id'] != 'NULL' ? $record['vendor_template_id'] : null;
            $assignment->resource_template_id = isset($record['resource_template_id']) && $record['resource_template_id'] != '' && $record['resource_template_id'] != 'NULL' ? $record['resource_template_id'] : null;
            $assignment->role = isset($record['role']) && $record['role'] != '' && $record['role'] != 'NULL' ? $record['role'] : null;
            $assignment->issue_date = isset($record['issue_date']) && $record['issue_date'] != '' && $record['issue_date'] != 'NULL' ? $record['issue_date'] : null;
            $assignment->approved_date = isset($record['approved_date']) && $record['approved_date'] != '' && $record['approved_date'] != 'NULL' ? $record['approved_date'] : null;
            $assignment->site_id = isset($record['site_id']) && $record['site_id'] != '' && $record['site_id'] != 'NULL' ? $record['site_id'] : null;
            //$resource->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$resource->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $assignment->save();
        }

        /*Assignment::insert([
            ['id' => '1', 'employee_id' => '12', 'project_id' => '1', 'start_date' => '2017-08-01', 'end_date' => '2017-12-31', 'rate' => '0.00', 'bonus_rate' => '150.00', 'hours' => '160', 'number_hours' => null, 'vendor_id' => 1, 'report_to' => 'Dr. Walsh', 'report_to_email' => 'walsh@blackboardbi.com', 'report_phone' => '+2755-555-5555', 'billable' => '0', 'note_1' => 'Provide BI support as required. Dashboard Design and Development', 'note_2' => '', 'exp_id' => null, 'function' => '1', 'system' => '5', 'country_id' => '1', 'vat_rate_id' => null, 'currency' => 'R', 'internal_cost_rate' => '100.00', 'external_cost_rate' => '0.00', 'invoice_rate' => '150', 'extension_id' => null, 'assignment_status' => '2', 'claim_approver_id' => '1', 'medical_certificate_type' => null, 'medical_certificate_text' => '', 'assignment_approver' => null, 'project_type' => null, 'location' => '', 'hours_of_work' => null, 'role' => null, 'issue_date' => null, 'approved_date' => null, 'site_id' => null, 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2017-10-10 21:01:34', 'updated_at' => '2018-08-15 07:13:55'],
            ['id' => '2', 'employee_id' => '1', 'project_id' => '1', 'start_date' => '2017-08-01', 'end_date' => '2017-12-31', 'rate' => '0.00', 'bonus_rate' => '150.00', 'hours' => '120', 'number_hours' => null, 'vendor_id' => 2, 'report_to' => 'Dr. Walsh', 'report_to_email' => 'walsh@blackboardbi.com', 'report_phone' => '+2755-555-5555', 'billable' => '0', 'note_1' => 'Provide BI support as required.', 'note_2' => '', 'exp_id' => null, 'function' => '3', 'system' => '5', 'country_id' => '1', 'vat_rate_id' => null, 'currency' => 'R', 'internal_cost_rate' => '100.00', 'external_cost_rate' => '0.00', 'invoice_rate' => '150', 'extension_id' => null, 'assignment_status' => '2', 'claim_approver_id' => '1', 'medical_certificate_type' => null, 'medical_certificate_text' => '', 'assignment_approver' => null, 'project_type' => null, 'location' => '', 'hours_of_work' => null, 'role' => null, 'issue_date' => null, 'approved_date' => null, 'site_id' => null, 'status' => '6', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-03-19 07:41:08', 'updated_at' => '2018-08-15 07:13:55'],
            ['id' => '3', 'employee_id' => '6', 'project_id' => '2', 'start_date' => '2017-08-01', 'end_date' => '2017-09-30', 'rate' => '0.00', 'bonus_rate' => '150.00', 'hours' => '218', 'number_hours' => null, 'vendor_id' => 3, 'report_to' => 'Dr. Walsh', 'report_to_email' => 'walsh@blackboardbi.com', 'report_phone' => '+2755-555-5555', 'billable' => '1', 'note_1' => 'Develop BOS Solution with HANA and BO', 'note_2' => '', 'exp_id' => null, 'function' => '1', 'system' => '5', 'country_id' => '1', 'vat_rate_id' => null, 'currency' => 'R', 'internal_cost_rate' => '0.00', 'external_cost_rate' => '50.00', 'invoice_rate' => '150', 'extension_id' => null, 'assignment_status' => '2', 'claim_approver_id' => '1', 'medical_certificate_type' => null, 'medical_certificate_text' => '', 'assignment_approver' => null, 'project_type' => null, 'location' => '', 'hours_of_work' => null, 'role' => null, 'issue_date' => null, 'approved_date' => null, 'site_id' => null, 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2017-11-18 06:36:42', 'updated_at' => '2018-08-15 07:13:55'],
            ['id' => '4', 'employee_id' => '1', 'project_id' => '3', 'start_date' => '2017-08-01', 'end_date' => '2017-11-30', 'rate' => '0.00', 'bonus_rate' => '150.00', 'hours' => '230', 'number_hours' => null, 'vendor_id' => 4, 'report_to' => 'Dr. Walsh', 'report_to_email' => 'walsh@blackboardbi.com', 'report_phone' => '+2755-555-5555', 'billable' => '1', 'note_1' => 'Develop SAP HANA solution for Supply Chain Dashboard', 'note_2' => '', 'exp_id' => null, 'function' => '1', 'system' => '3', 'country_id' => '1', 'vat_rate_id' => null, 'currency' => 'R', 'internal_cost_rate' => '100.00', 'external_cost_rate' => '0.00', 'invoice_rate' => '150', 'extension_id' => null, 'assignment_status' => '2', 'claim_approver_id' => '1', 'medical_certificate_type' => null, 'medical_certificate_text' => '', 'assignment_approver' => null, 'project_type' => null, 'location' => '', 'hours_of_work' => null, 'role' => null, 'issue_date' => null, 'approved_date' => null, 'site_id' => null, 'status' => '6', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-03-19 07:40:38', 'updated_at' => '2018-08-15 07:13:55'],
            ['id' => '5', 'employee_id' => '13', 'project_id' => '1', 'start_date' => '2017-08-01', 'end_date' => '2017-12-31', 'rate' => '0.00', 'bonus_rate' => '150.00', 'hours' => '80', 'number_hours' => null, 'vendor_id' => 5, 'report_to' => 'Dr. Walsh', 'report_to_email' => 'walsh@blackboardbi.com', 'report_phone' => '+2755-555-5555', 'billable' => '0', 'note_1' => 'Dashboard design - Lumira and Power BI', 'note_2' => '', 'exp_id' => null, 'function' => '1', 'system' => '9', 'country_id' => '1', 'vat_rate_id' => null, 'currency' => 'R', 'internal_cost_rate' => '100.00', 'external_cost_rate' => '0.00', 'invoice_rate' => '150', 'extension_id' => null, 'assignment_status' => '2', 'claim_approver_id' => '1', 'medical_certificate_type' => null, 'medical_certificate_text' => '', 'assignment_approver' => null, 'project_type' => null, 'location' => '', 'hours_of_work' => null, 'role' => null, 'issue_date' => null, 'approved_date' => null, 'site_id' => null, 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2017-10-10 21:03:28', 'updated_at' => '2018-08-15 07:13:55'],
        ]);*/
    }
}
