<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/users.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $user = new User;
            $user->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $user->first_name = isset($record['first_name']) && $record['first_name'] != '' && $record['first_name'] != 'NULL' ? $record['first_name'] : null;
            $user->last_name = isset($record['last_name']) && $record['last_name'] != '' && $record['last_name'] != 'NULL' ? $record['last_name'] : null;
            $user->email = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? $record['email'] : null;
            $user->phone = isset($record['phone']) && $record['phone'] != '' && $record['phone'] != 'NULL' ? $record['phone'] : null;
            $user->password = isset($record['password']) && $record['password'] != '' && $record['password'] != 'NULL' ? $record['password'] : null;
            $user->avatar = isset($record['avatar']) && $record['avatar'] != '' && $record['avatar'] != 'NULL' ? $record['avatar'] : null;
            $user->resource_id = isset($record['resource_id']) && $record['resource_id'] != '' && $record['resource_id'] != 'NULL' ? $record['resource_id'] : null;
            $user->vendor_id = isset($record['vendor_id']) && $record['vendor_id'] != '' && $record['vendor_id'] != 'NULL' ? $record['vendor_id'] : null;
            $user->customer_id = isset($record['customer_id']) && $record['customer_id'] != '' && $record['customer_id'] != 'NULL' ? $record['customer_id'] : null;
            $user->active_office_id = isset($record['active_office_id']) && $record['active_office_id'] != '' && $record['active_office_id'] != 'NULL' ? $record['active_office_id'] : null;
            $user->remember_token = isset($record['remember_token']) && $record['remember_token'] != '' && $record['remember_token'] != 'NULL' ? $record['remember_token'] : null;
            $user->expiry_date = isset($record['expiry_date']) && $record['expiry_date'] != '' && $record['expiry_date'] != 'NULL' ? $record['expiry_date'] : null;
            $user->status_id = isset($record['status_id']) && $record['status_id'] != '' && $record['status_id'] != 'NULL' ? $record['status_id'] : null;
            $user->save();
        }
    }
}
