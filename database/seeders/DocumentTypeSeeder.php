<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DocumentType::truncate();

        $documentType = tenancy()->central(fn() => \App\Models\Central\DocumentType::all());

        DocumentType::insert($documentType->toArray());
    }
}
