<?php

namespace Database\Seeders;

use App\Models\WarningMaintenance;
use Illuminate\Database\Seeder;

class WarningMaintenanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WarningMaintenance::truncate();

        $warningMaintanance = tenancy()->central(fn() => \App\Models\Central\WarningMaintenance::all());

        WarningMaintenance::insert($warningMaintanance->toArray());
    }
}
