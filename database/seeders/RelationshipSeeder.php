<?php

namespace Database\Seeders;

use App\Models\Relationship;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Relationship::truncate();

        $relationship = tenancy()->central(fn() => \App\Models\Central\Relationship::all());

        Relationship::insert($relationship->toArray());
    }
}
