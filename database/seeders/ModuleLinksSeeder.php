<?php

namespace Database\Seeders;

use App\Models\ModuleLinks;
use Illuminate\Database\Seeder;

class ModuleLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ModuleLinks::insert([
            ['module_id' => 1, 'view' => 'users', 'add' => 'users/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 2, 'view' => 'recents', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 3, 'view' => 'dashboard', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 4, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()], //Company left menu*/
            ['module_id' => 5, 'view' => 'company', 'add' => 'company/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 6, 'view' => 'plan_exp', 'add' => 'plan_exp/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 7, 'view' => 'utilization', 'add' => 'utilization/create/week', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 8, 'view' => 'exptracking', 'add' => 'exptracking/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 9, 'view' => 'calendar', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            //['module_id' => 10, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 11, 'view' => 'resource', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 12, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 13, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 14, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 15, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 16, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 17, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 18, 'view' => 'anniversary', 'add' => 'anniversary/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 19, 'view' => 'leave', 'add' => 'leave/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 20, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 21, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 22, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 23, 'view' => 'assessment', 'add' => 'assessment/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 24, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 25, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 26, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 27, 'view' => 'customer', 'add' => 'customer/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 28, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 29, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 30, 'view' => 'project', 'add' => 'project/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 31, 'view' => 'assignment', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 32, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 33, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 34, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 35, 'view' => 'timesheet', 'add' => 'timesheet/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 36, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 37, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 38, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 39, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 40, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 41, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 42, 'view' => 'customer_invoice', 'add' => 'customer_invoice/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 43, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 44, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 45, 'view' => 'vendor', 'add' => 'vendor/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            //['module_id' => 46, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 47, 'view' => 'vendor/invoice/view', 'add' => 'vendor/invoice/function/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 48, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 49, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 50, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 51, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 52, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 53, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 54, 'view' => 'dashboard', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 55, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 56, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 57, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 58, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 59, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 60, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 61, 'view' => 'assetreg', 'add' => 'assetreg/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 62, 'view' => 'cv', 'add' => 'cv/user', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 63, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 64, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 65, 'view' => 'leave_balance', 'add' => 'leave_balance/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            //['module_id' => 66, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 67, 'view' => 'configs', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 68, 'view' => 'module', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 69, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 70, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
            ['module_id' => 71, 'view' => 'prospects', 'add' => 'prospects/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 72, 'view' => 'scouting', 'add' => 'scouting/create', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            /*['module_id' => 73, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 74, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['module_id' => 75, 'view' => '', 'add' => '', 'creator_id' => 1, 'status_id' => 1, 'created_at' => now(), 'updated_at' => now()],*/
        ]);
    }
}
