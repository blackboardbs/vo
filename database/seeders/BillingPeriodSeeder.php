<?php

namespace Database\Seeders;

use App\Models\BillingPeriod;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BillingPeriodSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BillingPeriod::truncate();

        $periods = tenancy()->central(fn() => \App\Models\Central\BillingPeriod::all());

        BillingPeriod::insert($periods->toArray());
    }
}
