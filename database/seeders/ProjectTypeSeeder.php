<?php

namespace Database\Seeders;

use App\Models\ProjectType;
use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProjectType::truncate();

        $projectType = tenancy()->central(fn() => \App\Models\Central\ProjectType::all());

        ProjectType::insert($projectType->toArray());
    }
}
