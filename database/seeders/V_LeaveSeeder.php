<?php

namespace Database\Seeders;

use App\Models\V_Leave;
use Illuminate\Database\Seeder;

class V_LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_Leave::insert([
            ['id' => '1', 'employee_id' => '1', 'yearmonth' => '201709', 'ldays' => '6', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'employee_id' => '12', 'yearmonth' => '201712', 'ldays' => '7', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'employee_id' => '13', 'yearmonth' => '201711', 'ldays' => '2', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'employee_id' => '13', 'yearmonth' => '201712', 'ldays' => '7', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'employee_id' => '13', 'yearmonth' => '201803', 'ldays' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'employee_id' => '13', 'yearmonth' => '201804', 'ldays' => '3', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'employee_id' => '13', 'yearmonth' => '201807', 'ldays' => '3', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'employee_id' => '14', 'yearmonth' => '201712', 'ldays' => '5', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'employee_id' => '15', 'yearmonth' => '201803', 'ldays' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'employee_id' => '15', 'yearmonth' => '201805', 'ldays' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '11', 'employee_id' => '15', 'yearmonth' => '201808', 'ldays' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '12', 'employee_id' => '15', 'yearmonth' => '201809', 'ldays' => '5', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
