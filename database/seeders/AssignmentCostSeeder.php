<?php

namespace Database\Seeders;

use App\Models\AssignmentCost;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class AssignmentCostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/assignment_cost.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $assignment_cost = new AssignmentCost;
            $assignment_cost->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $assignment_cost->ass_id = isset($record['ass_id']) && $record['ass_id'] != '' && $record['ass_id'] != 'NULL' ? $record['ass_id'] : null;
            $assignment_cost->account_id = isset($record['account_id']) && $record['account_id'] != '' && $record['account_id'] != 'NULL' ? $record['account_id'] : null;
            $assignment_cost->account_element_id = isset($record['account_element_id']) && $record['account_element_id'] != '' && $record['account_element_id'] != 'NULL' ? $record['account_element_id'] : null;
            $assignment_cost->description = isset($record['description']) && $record['description'] != '' && $record['description'] != 'NULL' ? $record['description'] : null;
            $assignment_cost->note = isset($record['note']) && $record['note'] != '' && $record['note'] != 'NULL' ? $record['note'] : null;
            $assignment_cost->cost = isset($record['cost']) && $record['cost'] != '' && $record['cost'] != 'NULL' ? $record['cost'] : null;
            $assignment_cost->payment_frequency = isset($record['payment_frequency']) && $record['payment_frequency'] != '' && $record['payment_frequency'] != 'NULL' ? $record['payment_frequency'] : null;
            $assignment_cost->payment_method = isset($record['payment_method']) && $record['payment_method'] != '' && $record['payment_method'] != 'NULL' ? $record['payment_method'] : null;
            $assignment_cost->status = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : null;
            $assignment_cost->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : null;
            $assignment_cost->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            $assignment_cost->approver = isset($record['approver']) && $record['approver'] != '' && $record['approver'] != 'NULL' ? $record['approver'] : null;
            //$resource->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$resource->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $assignment_cost->save();
        }
    }
}
