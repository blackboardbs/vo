<?php

namespace Database\Seeders;

use App\Models\AssignmentStandardCost;
use Illuminate\Database\Seeder;

class AssignmentStandardCostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AssignmentStandardCost::truncate();

        $assignmentStandardCost = tenancy()->central(fn() => \App\Models\Central\AssignmentStandardCost::all());

        AssignmentStandardCost::insert($assignmentStandardCost->toArray());
    }
}
