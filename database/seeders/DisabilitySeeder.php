<?php

namespace Database\Seeders;

use App\Models\Disability;
use Illuminate\Database\Seeder;

class DisabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Disability::truncate();

        $diasability = tenancy()->central(fn() => \App\Models\Central\Disability::all());

        Disability::insert([
            ['id' => '1', 'description' => 'No', 'status' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '2', 'description' => 'Yes', 'status' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
        ]);
    }
}
