<?php

namespace Database\Seeders;

use App\Models\BillingPeriodStyle;
use App\Models\Central\TemplateStyle;
use Illuminate\Database\Seeder;

class TemplateStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BillingPeriodStyle::truncate();

        $templateStyle = tenancy()->central(fn() => TemplateStyle::all());

        BillingPeriodStyle::insert($templateStyle->toArray());
    }
}
