<?php

namespace Database\Seeders;

use App\Models\AssetRegister;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class AssetRegisterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/asset_register.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $asset_register = new AssetRegister;
            $asset_register->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $asset_register->asset_class = isset($record['asset_class']) && $record['asset_class'] != '' && $record['asset_class'] != 'NULL' ? $record['asset_class'] : null;
            $asset_register->asset_nr = isset($record['asset_nr']) && $record['asset_nr'] != '' && $record['asset_nr'] != 'NULL' ? $record['asset_nr'] : null;
            $asset_register->make = isset($record['make']) && $record['make'] != '' && $record['make'] != 'NULL' ? $record['make'] : null;
            $asset_register->model = isset($record['model']) && $record['model'] != '' && $record['model'] != 'NULL' ? $record['model'] : null;
            $asset_register->serial_nr = isset($record['serial_nr']) && $record['serial_nr'] != '' && $record['serial_nr'] != 'NULL' ? $record['serial_nr'] : null;
            $asset_register->picture = isset($record['picture']) && $record['picture'] != '' && $record['picture'] != 'NULL' ? $record['picture'] : null;
            $asset_register->aquire_date = isset($record['aquire_date']) && $record['aquire_date'] != '' && $record['aquire_date'] != 'NULL' ? $record['aquire_date'] : null;
            $asset_register->retire_date = isset($record['retire_date']) && $record['retire_date'] != '' && $record['retire_date'] != 'NULL' ? $record['retire_date'] : null;
            $asset_register->original_value = isset($record['original_value']) && $record['original_value'] != '' && $record['original_value'] != 'NULL' ? $record['original_value'] : null;
            $asset_register->issued_to = isset($record['issued_to']) && $record['issued_to'] != '' && $record['issued_to'] != 'NULL' ? $record['issued_to'] : null;
            $asset_register->note = isset($record['note']) && $record['note'] != '' && $record['note'] != 'NULL' ? $record['note'] : null;
            $asset_register->est_life = isset($record['est_life']) && $record['est_life'] != '' && $record['est_life'] != 'NULL' ? $record['est_life'] : null;
            $asset_register->processor = isset($record['processor']) && $record['processor'] != '' && $record['processor'] != 'NULL' ? $record['processor'] : null;
            $asset_register->ram = isset($record['ram']) && $record['ram'] != '' && $record['ram'] != 'NULL' ? $record['ram'] : null;
            $asset_register->hdd = isset($record['hdd']) && $record['hdd'] != '' && $record['hdd'] != 'NULL' ? $record['hdd'] : null;
            $asset_register->screen_size = isset($record['screen_size']) && $record['screen_size'] != '' && $record['screen_size'] != 'NULL' ? $record['screen_size'] : null;
            $asset_register->supplier = isset($record['supplier']) && $record['supplier'] != '' && $record['supplier'] != 'NULL' ? $record['supplier'] : null;
            $asset_register->acc_form = isset($record['acc_form']) && $record['acc_form'] != '' && $record['acc_form'] != 'NULL' ? $record['acc_form'] : null;
            $asset_register->status_id = isset($record['status_id']) && $record['status_id'] != '' && $record['status_id'] != 'NULL' ? $record['status_id'] : null;
            $asset_register->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            $asset_register->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : null;
            //$asset_register->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$asset_register->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $asset_register->save();
        }
        /*AssetRegister::insert([
            ['id' => '1', 'asset_class' => '1', 'asset_nr' => 'B000001', 'make' => 'Lenovo', 'model' => 'ABC123', 'serial_nr' => '3TE32M17310301G5JA56971', 'picture' => '', 'aquire_date' => '2018-01-07', 'retire_date' => '2022-01-07', 'original_value' => '10000.00', 'issued_to' => '1', 'note' => '', 'est_life' => '48', 'processor' => '0', 'ram' => '16 GB', 'hdd' => '1TB', 'screen_size' => '17"', 'supplier' => 'Micro SA', 'acc_form' => '', 'status_id' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-01-08 00:00:00', 'updated_at' => '2018-08-30 17:30:11'],
            ['id' => '2', 'asset_class' => '1', 'asset_nr' => 'B000002', 'make' => 'Dell', 'model' => 'A65M5010UW', 'serial_nr' => '3TE65F17350101G5074', 'picture' => '.', 'aquire_date' => '2018-01-05', 'retire_date' => '2022-01-05', 'original_value' => '12000.00', 'issued_to' => '1', 'note' => '', 'est_life' => '48', 'processor' => 'i5 3.4Ghz', 'ram' => '12GB', 'hdd' => '1TB', 'screen_size' => '15"', 'supplier' => '', 'acc_form' => 'No', 'status_id' => '1', 'company_id' => '1', 'creator_id' => '7', 'created_at' => '2018-01-08 00:00:00', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '3', 'asset_class' => '1', 'asset_nr' => 'B000003', 'make' => 'Asus', 'model' => 'ABC3000UW', 'serial_nr' => 'AF17110801GC6SJ1083', 'picture' => '.', 'aquire_date' => '2017-11-23', 'retire_date' => '2022-01-23', 'original_value' => '9500.00', 'issued_to' => '1', 'note' => '', 'est_life' => '48', 'processor' => 'i5', 'ram' => '12GB', 'hdd' => '2TB', 'screen_size' => '15"', 'supplier' => 'Makro', 'acc_form' => 'No', 'status_id' => '1', 'company_id' => '1', 'creator_id' => '7', 'created_at' => '2018-01-08 00:00:00', 'updated_at' => '2018-08-15 07:13:54'],
        ]);*/
    }
}
