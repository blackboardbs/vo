<?php

namespace Database\Seeders;

use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Role::truncate();

        $roles = tenancy()->central(fn() => Role::all());

        $roles = $roles->toArray()??$this->readCsv('roles.csv');

        Role::insert($roles);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
