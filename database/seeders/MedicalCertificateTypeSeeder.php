<?php

namespace Database\Seeders;

use App\Models\MedicalCertificateType;
use Illuminate\Database\Seeder;

class MedicalCertificateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MedicalCertificateType::truncate();

        $medCertType = tenancy()->central(fn() => \App\Models\Central\MedicalCertificateType::all());

        MedicalCertificateType::insert($medCertType->toArray());
    }
}
