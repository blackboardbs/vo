<?php

namespace Database\Seeders;

use App\Models\LeaveType;
use Illuminate\Database\Seeder;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        LeaveType::truncate();

        $leaveType = tenancy()->central(fn() => \App\Models\Central\LeaveType::all());

        LeaveType::insert($leaveType->toArray());
    }
}
