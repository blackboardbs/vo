<?php

namespace Database\Seeders;

use App\Models\Favourite;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class FavouritesSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Favourite::truncate();

        $favourites = tenancy()->central(fn() => \App\Models\Central\Favourite::all());

        Favourite::insert($favourites->toArray());
    }
}
