<?php

namespace Database\Seeders;

use App\Models\ModuleSettings;
use Illuminate\Database\Seeder;

class ModuleSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $industries = [1, 2, 3, 4, 5];
        //Seed common compulsory modules for all industries
        foreach ($industries as $industry) {
            ModuleSettings::insert([
                ['industry_id' => $industry, 'module_id' => '1', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '2', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '4', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '5', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '9', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '10', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '11', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '26', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '27', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '52', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '54', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '67', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '68', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '74', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '76', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '77', 'selected' => '1', 'selectable' => '0', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],

                //Selectable
                ['industry_id' => $industry, 'module_id' => '3', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '6', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '7', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '8', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '12', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '13', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '14', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '15', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '16', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '17', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '18', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '19', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '20', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '21', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '22', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '23', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '24', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '25', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '28', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                /*['industry_id' => $industry, 'module_id' => '29', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now() ],*/
                ['industry_id' => $industry, 'module_id' => '30', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '31', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '32', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '33', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '34', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '35', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '36', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '37', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '38', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '39', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '40', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '41', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '42', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '43', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '44', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '45', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '46', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '47', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '48', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '49', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '50', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '51', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '53', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '55', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '56', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '57', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '58', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '59', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '60', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '61', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '62', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '63', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '64', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '65', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '66', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '69', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '70', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '71', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '72', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '73', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
                ['industry_id' => $industry, 'module_id' => '75', 'selected' => '0', 'selectable' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
            ]);
        }

        //Industry 1
        ModuleSettings::whereIn('module_id', [3, 6, 7, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28/*, 29*/ , 30, 31, 32, 33, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 69, 70, 71, 72, 73, 75])
            ->where('industry_id', 1)
            ->update(['selected' => 1]);

        //Industry 2
        ModuleSettings::whereIn('module_id', [30, 31, 35, 50, 51])
            ->where('industry_id', 1)
            ->update(['selected' => 1]);

        //Industry 3
        ModuleSettings::whereIn('module_id', [12, 13, 14, 15, 16, 17, 30, 31, 35, 44, 45, 47, 57, 58, 59, 62, 72])
            ->where('industry_id', 3)
            ->update(['selected' => 1]);

        //Industry 4
        ModuleSettings::whereIn('module_id', [12, 13, 14, 15, 16, 17, 57, 58, 59, 62, 72])
            ->where('industry_id', 4)
            ->update(['selected' => 1]);

        //Industry 5
        ModuleSettings::whereIn('module_id', [30, 31, 35, 42])
            ->where('industry_id', 5)
            ->update(['selected' => 1]);
    }
}
