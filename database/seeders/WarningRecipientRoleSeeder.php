<?php

namespace Database\Seeders;

use App\Models\WarningRecipientRole;
use Illuminate\Database\Seeder;

class WarningRecipientRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WarningRecipientRole::truncate();

        $warningRecipeRole = tenancy()->central(fn() => \App\Models\Central\WarningRecipientRole::all());

        WarningRecipientRole::insert($warningRecipeRole->toArray());

    }
}
