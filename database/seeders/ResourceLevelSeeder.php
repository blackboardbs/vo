<?php

namespace Database\Seeders;

use App\Models\ResourceLevel;
use Illuminate\Database\Seeder;

class ResourceLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ResourceLevel::truncate();

        $resourceLevel = tenancy()->central(fn() => \App\Models\Central\ResourceLevel::all());

        ResourceLevel::insert($resourceLevel->toArray());
    }
}
