<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Country::truncate();

        $country = tenancy()->central(fn($tenant) => \App\Models\Central\Country::all());

        Country::insert($country->toArray());
    }
}
