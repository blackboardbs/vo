<?php

namespace Database\Seeders;

use App\Models\ActionStatus;
use Illuminate\Database\Seeder;

class ActionStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ActionStatus::truncate();

        $actionStatus = tenancy()->central(fn() => \App\Models\Central\ActionStatus::all());

        ActionStatus::insert($actionStatus->toArray());
    }
}
