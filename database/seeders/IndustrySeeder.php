<?php

namespace Database\Seeders;

use App\Models\Industry;
use Illuminate\Database\Seeder;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Industry::truncate();

        $industry = tenancy()->central(fn() => \App\Models\Central\Industry::all());

        Industry::insert($industry->toArray());
    }
}
