<?php

namespace Database\Seeders;

use App\Enum\Status;
use App\Models\PlanUtilization;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class PlanUtilizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PlanUtilization::truncate();

        $planUtilization = tenancy()->central(fn() => \App\Models\Central\PlanUtilization::all());

        PlanUtilization::insert($planUtilization->toArray());
    }
}
