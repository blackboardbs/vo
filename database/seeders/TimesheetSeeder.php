<?php

namespace Database\Seeders;

use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class TimesheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/timesheet_sw-2.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $timesheet = new Timesheet;
            $timesheet->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $timesheet->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            $timesheet->employee_id = isset($record['employee_id']) && $record['employee_id'] != '' && $record['employee_id'] != 'NULL' ? $record['employee_id'] : null;
            $timesheet->customer_id = isset($record['customer_id']) && $record['customer_id'] != '' && $record['customer_id'] != 'NULL' ? $record['customer_id'] : null;
            $timesheet->project_id = isset($record['project_id']) && $record['project_id'] != '' && $record['project_id'] != 'NULL' ? $record['project_id'] : null;

            //Start Calculations
            $timesheet->year_week = isset($record['year_week']) && $record['year_week'] != '' && $record['year_week'] != 'NULL' ? $record['year_week'] : null;

            $year = substr($timesheet->year_week, 0, 4);
            $week = substr($timesheet->year_week, -2);

            $year = (int) $year;
            $week = (int) $week;

            $date = Carbon::now();
            $date2 = Carbon::now();
            $date3 = Carbon::now();
            $date4 = Carbon::now();
            $date5 = Carbon::now();
            $date6 = Carbon::now();
            $date7 = Carbon::now();
            $date8 = Carbon::now();

            $date->setISODate($year, $week);
            $date2->setISODate($year, $week);
            $date3->setISODate($year, $week);
            $date4->setISODate($year, $week);
            $date5->setISODate($year, $week);
            $date6->setISODate($year, $week);
            $date7->setISODate($year, $week);
            $date8->setISODate($year, $week);

            $timesheet->year = $year;
            $timesheet->week = $week;
            $timesheet->month = $date->month;
            $timesheet->first_day_of_week = $date->startOfWeek();
            $timesheet->last_day_of_week = $date2->endOfWeek(); //Todo: Investigate why date changes first_day_of_week if date is used, it seems date is passed by reference
            $timesheet->first_day_of_month = $date3->startOfMonth(); //Todo: Investigate why date changes first_day_of_month if date is used, it seems date is passed by reference
            $timesheet->last_day_of_month = $date4->endOfMonth(); //Todo: Investigate why date changes first_day_of_month if date is used, it seems date is passed by reference

            $temp_first_date_of_week = $date5->startOfWeek();
            $temp_last_date_of_week = $date6->endOfWeek();
            $timesheet->first_date_of_week = $temp_first_date_of_week->format('d');
            $timesheet->last_date_of_week = $temp_last_date_of_week->format('d');

            $temp_first_date_of_month = $date7->startOfMonth();
            $temp_last_date_of_month = $date8->endOfMonth();
            $timesheet->first_date_of_month = $temp_first_date_of_month->format('d');
            $timesheet->last_date_of_month = $temp_last_date_of_month->format('d');
            //End Calculations

            $timesheet->emp_id = isset($record['emp_id']) && $record['emp_id'] != '' && $record['emp_id'] != 'NULL' ? $record['emp_id'] : null;
            $timesheet->yearwk = isset($record['yearwk']) && $record['yearwk'] != '' && $record['yearwk'] != 'NULL' ? $record['yearwk'] : null;
            $timesheet->version = isset($record['version']) && $record['version'] != '' && $record['version'] != 'NULL' ? $record['version'] : null;
            $timesheet->date = isset($record['date']) && $record['date'] != '' && $record['date'] != 'NULL' ? $record['date'] : null;
            //$timesheet->year = isset($record['year']) && $record['year'] != '' && $record['year'] != 'NULL' ? $record['year'] : NULL;
            //$timesheet->month = isset($record['month']) && $record['month'] != '' && $record['month'] != 'NULL' ? $record['month'] : NULL;
            //$timesheet->week = isset($record['week']) && $record['week'] != '' && $record['week'] != 'NULL' ? $record['week'] : NULL;
            //$timesheet->first_day_of_week = isset($record['first_day_of_week']) && $record['first_day_of_week'] != '' && $record['first_day_of_week'] != 'NULL' ? $record['first_day_of_week'] : NULL;
            //$timesheet->last_day_of_week = isset($record['last_day_of_week']) && $record['last_day_of_week'] != '' && $record['last_day_of_week'] != 'NULL' ? $record['last_day_of_week'] : NULL;
            //$timesheet->first_day_of_month = isset($record['first_day_of_month']) && $record['first_day_of_month'] != '' && $record['first_day_of_month'] != 'NULL' ? $record['first_day_of_month'] : NULL;
            //$timesheet->last_day_of_month = isset($record['last_day_of_month']) && $record['last_day_of_month'] != '' && $record['last_day_of_month'] != 'NULL' ? $record['last_day_of_month'] : NULL;
            //$timesheet->first_date_of_week = isset($record['first_date_of_week']) && $record['first_date_of_week'] != '' && $record['first_date_of_week'] != 'NULL' ? $record['first_date_of_week'] : NULL;
            //$timesheet->last_date_of_week = isset($record['last_date_of_week']) && $record['last_date_of_week'] != '' && $record['last_date_of_week'] != 'NULL' ? $record['last_date_of_week'] : NULL;
            //$timesheet->first_date_of_month = isset($record['first_date_of_month']) && $record['first_date_of_month'] != '' && $record['first_date_of_month'] != 'NULL' ? $record['first_date_of_month'] : NULL;
            //$timesheet->last_date_of_month = isset($record['last_date_of_month']) && $record['last_date_of_month'] != '' && $record['last_date_of_month'] != 'NULL' ? $record['last_date_of_month'] : NULL;
            $timesheet->day = isset($record['day']) && $record['day'] != '' && $record['day'] != 'NULL' ? $record['day'] : null;
            $timesheet->day_name = isset($record['day_name']) && $record['day_name'] != '' && $record['day_name'] != 'NULL' ? $record['day_name'] : null;
            $timesheet->hours = isset($record['hours']) && $record['hours'] != '' && $record['hours'] != 'NULL' ? $record['hours'] : null;
            $timesheet->wbs_id = isset($record['wbs_id']) && $record['wbs_id'] != '' && $record['wbs_id'] != 'NULL' ? $record['wbs_id'] : null;
            $timesheet->mileage = isset($record['mileage']) && $record['mileage'] != '' && $record['mileage'] != 'NULL' ? $record['mileage'] : null;
            $timesheet->invoice_number = isset($record['invoice_number']) && $record['invoice_number'] != '' && $record['invoice_number'] != 'NULL' ? $record['invoice_number'] : null;
            $timesheet->invoice_date = isset($record['invoice_date']) && $record['invoice_date'] != '' && $record['invoice_date'] != 'NULL' ? $record['invoice_date'] : null;
            $timesheet->invoice_due_date = isset($record['invoice_due_date']) && $record['invoice_due_date'] != '' && $record['invoice_due_date'] != 'NULL' ? $record['invoice_due_date'] : null;
            $timesheet->invoice_paid_date = isset($record['invoice_paid_date']) && $record['invoice_paid_date'] != '' && $record['invoice_paid_date'] != 'NULL' ? $record['invoice_paid_date'] : null;
            $timesheet->inv_ref = isset($record['inv_ref']) && $record['inv_ref'] != '' && $record['inv_ref'] != 'NULL' ? $record['inv_ref'] : null;
            $timesheet->timeline_id = isset($record['timeline_id']) && $record['timeline_id'] != '' && $record['timeline_id'] != 'NULL' ? $record['timeline_id'] : null;
            $timesheet->billable = isset($record['billable']) && $record['billable'] != '' && $record['billable'] != 'NULL' ? $record['billable'] : null;
            $timesheet->is_billable = isset($record['is_billable']) && $record['is_billable'] != '' && $record['is_billable'] != 'NULL' ? $record['is_billable'] : null;
            $timesheet->cust_inv_ref = isset($record['cust_inv_ref']) && $record['cust_inv_ref'] != '' && $record['cust_inv_ref'] != 'NULL' ? $record['cust_inv_ref'] : null;
            $timesheet->vendor_invoice_number = isset($record['vendor_invoice_number']) && $record['vendor_invoice_number'] != '' && $record['vendor_invoice_number'] != 'NULL' ? $record['vendor_invoice_number'] : null;
            $timesheet->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : null;
            $timesheet->system_id = isset($record['system_id']) && $record['system_id'] != '' && $record['system_id'] != 'NULL' ? $record['system_id'] : null;
            $timesheet->status_id = isset($record['status_id']) && $record['status_id'] != '' && $record['status_id'] != 'NULL' ? $record['status_id'] : null;
            $timesheet->timesheet_lock = isset($record['timesheet_lock']) && $record['timesheet_lock'] != '' && $record['timesheet_lock'] != 'NULL' ? $record['timesheet_lock'] : null;
            $timesheet->vendor_invoice_status = isset($record['vendor_invoice_status']) && $record['vendor_invoice_status'] != '' && $record['vendor_invoice_status'] != 'NULL' ? $record['vendor_invoice_status'] : null;
            $timesheet->vendor_invoice_date = isset($record['vendor_invoice_date']) && $record['vendor_invoice_date'] != '' && $record['vendor_invoice_date'] != 'NULL' ? $record['vendor_invoice_date'] : null;
            $timesheet->vendor_due_date = isset($record['vendor_due_date']) && $record['vendor_due_date'] != '' && $record['vendor_due_date'] != 'NULL' ? $record['vendor_due_date'] : null;
            $timesheet->vendor_paid_date = isset($record['vendor_paid_date']) && $record['vendor_paid_date'] != '' && $record['vendor_paid_date'] != 'NULL' ? $record['vendor_paid_date'] : null;
            $timesheet->vendor_unallocate_date = isset($record['vendor_unallocate_date']) && $record['vendor_unallocate_date'] != '' && $record['vendor_unallocate_date'] != 'NULL' ? $record['vendor_unallocate_date'] : null;
            $timesheet->bill_status = isset($record['bill_status']) && $record['bill_status'] != '' && $record['bill_status'] != 'NULL' ? $record['bill_status'] : null;
            $timesheet->exp_paid = isset($record['exp_paid']) && $record['exp_paid'] != '' && $record['exp_paid'] != 'NULL' ? $record['exp_paid'] : null;
            //$timesheet->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$timesheet->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $timesheet->save();
        }
        /*Timesheet::insert([
            ['id' => '1', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201836', 'version' => 'TM RW_SASOL_DAP_2018', 'date' => '2018-09-03', 'year' => '2018', 'month' => '9', 'week' => '36', 'day' => '3', 'day_name' => 'Monday', 'customer_id' => '3015', 'hours' => '0', 'wbs_id' => '32', 'mileage' => '0', 'timeline_id' => '989', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'company_id' => '1', 'emp_id' => '15', 'yearwk' => '201836', 'version' => 'SL BBBI0002 1836', 'date' => '2018-09-03', 'year' => '2018', 'month' => '9', 'week' => '36', 'day' => '3', 'day_name' => 'Monday', 'customer_id' => '3015', 'hours' => '0', 'wbs_id' => '15', 'mileage' => '0', 'timeline_id' => '992', 'billable' => '0', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201732', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-01', 'year' => '2017', 'month' => '8', 'week' => '32', 'day' => '1', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '11', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201732', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-01', 'year' => '2017', 'month' => '8', 'week' => '32', 'day' => '1', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '0', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '34', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201733', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-08', 'year' => '2017', 'month' => '8', 'week' => '33', 'day' => '8', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '7', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '10', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201733', 'version' => 'TM RW_SASOL_BOS_2017', 'date' => '2017-08-08', 'year' => '2017', 'month' => '8', 'week' => '33', 'day' => '8', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '2', 'mileage' => '0', 'timeline_id' => '12', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201733', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-08', 'year' => '2017', 'month' => '8', 'week' => '33', 'day' => '8', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '1', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '35', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201734', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-15', 'year' => '2017', 'month' => '8', 'week' => '34', 'day' => '15', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '9', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201734', 'version' => 'TM RW_SASOL_BOS_2017', 'date' => '2017-08-15', 'year' => '2017', 'month' => '8', 'week' => '34', 'day' => '15', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '2', 'wbs_id' => '2', 'mileage' => '0', 'timeline_id' => '13', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201735', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-22', 'year' => '2017', 'month' => '8', 'week' => '35', 'day' => '22', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '2', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '1', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '11', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201735', 'version' => 'TM RW_SASOL_BOS_2017', 'date' => '2017-08-22', 'year' => '2017', 'month' => '8', 'week' => '35', 'day' => '22', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '2', 'mileage' => '0', 'timeline_id' => '14', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '12', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201736', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-29', 'year' => '2017', 'month' => '8', 'week' => '36', 'day' => '29', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '2', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '2', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '13', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201736', 'version' => 'TM RW_SASOL_BOS_2017', 'date' => '2017-08-29', 'year' => '2017', 'month' => '8', 'week' => '36', 'day' => '29', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '2', 'mileage' => '0', 'timeline_id' => '15', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '14', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201736', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-08-29', 'year' => '2017', 'month' => '8', 'week' => '36', 'day' => '29', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '6', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '36', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '15', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201737', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-09-05', 'year' => '2017', 'month' => '9', 'week' => '37', 'day' => '5', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '3', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '16', 'company_id' => '1', 'emp_id' => '6', 'yearwk' => '201737', 'version' => 'TM RW_SASOL_BOS_2017', 'date' => '2017-09-05', 'year' => '2017', 'month' => '9', 'week' => '37', 'day' => '5', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '2', 'mileage' => '0', 'timeline_id' => '16', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '17', 'company_id' => '1', 'emp_id' => '1', 'yearwk' => '201738', 'version' => 'SW RW_SASOL_SCD_2017', 'date' => '2017-09-12', 'year' => '2017', 'month' => '9', 'week' => '38', 'day' => '12', 'day_name' => 'Tuesday', 'customer_id' => '3015', 'hours' => '8', 'wbs_id' => '3', 'mileage' => '0', 'timeline_id' => '4', 'billable' => '1', 'created_at' => now(), 'updated_at' => now()],
        ]);*/
    }
}
