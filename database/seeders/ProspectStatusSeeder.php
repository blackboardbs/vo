<?php

namespace Database\Seeders;

use App\Models\ProspectStatus;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProspectStatusSeeder extends Seeder
{

    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProspectStatus::truncate();

        $prospectStatus = tenancy()->central(fn() => \App\Models\Central\ProspectStatus::all());

        ProspectStatus::insert($prospectStatus->toArray());
    }
}
