<?php

namespace Database\Seeders;

use App\Models\IndustryExperience;
use Illuminate\Database\Seeder;

class IndustryExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        IndustryExperience::insert([
            ['id' => '1', 'ind_id' => '16', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '2', 'ind_id' => '2', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '3', 'ind_id' => '15', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '4', 'ind_id' => '17', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '5', 'ind_id' => '14', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '6', 'ind_id' => '13', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '7', 'ind_id' => '12', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '8', 'ind_id' => '5', 'res_id' => '1', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '9', 'ind_id' => '2', 'res_id' => '3', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '10', 'ind_id' => '5', 'res_id' => '3', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '11', 'ind_id' => '10', 'res_id' => '3', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '12', 'ind_id' => '15', 'res_id' => '3', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '13', 'ind_id' => '14', 'res_id' => '3', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '14', 'ind_id' => '12', 'res_id' => '4', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '15', 'ind_id' => '6', 'res_id' => '4', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '16', 'ind_id' => '17', 'res_id' => '4', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '17', 'ind_id' => '5', 'res_id' => '4', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '18', 'ind_id' => '17', 'res_id' => '2', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '19', 'ind_id' => '12', 'res_id' => '2', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '20', 'ind_id' => '2', 'res_id' => '9', 'status' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '21', 'ind_id' => '7', 'res_id' => '9', 'status' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '22', 'ind_id' => '5', 'res_id' => '11', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '23', 'ind_id' => '13', 'res_id' => '11', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '24', 'ind_id' => '17', 'res_id' => '11', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '25', 'ind_id' => '16', 'res_id' => '11', 'status' => '1', 'creator_id' => '7', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '26', 'ind_id' => '2', 'res_id' => '12', 'status' => '1', 'creator_id' => '10', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
        ]);
    }
}
