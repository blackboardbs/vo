<?php

namespace Database\Seeders;

use App\Models\Central\ProjectTerm;
use App\Models\ProjectTerms;
use Illuminate\Database\Seeder;

class ProjectTermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProjectTerms::truncate();

        $projectTerms = tenancy()->central(fn() => ProjectTerm::all());

        ProjectTerms::insert($projectTerms->toArray());
    }
}
