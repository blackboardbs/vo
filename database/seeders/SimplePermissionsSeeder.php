<?php

namespace Database\Seeders;

use App\Models\Central\SimplePermission;
use App\Models\SimplePermissions;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class SimplePermissionsSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SimplePermissions::truncate();

        $simplePermission = tenancy()->central(fn() => SimplePermission::where('role_id', 1)->get());

        SimplePermissions::insert($simplePermission->toArray());
    }
}
