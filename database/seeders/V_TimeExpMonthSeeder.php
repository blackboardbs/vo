<?php

namespace Database\Seeders;

use App\Models\V_TimeExpMonth;
use Illuminate\Database\Seeder;

class V_TimeExpMonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_TimeExpMonth::insert([
            ['id' => '1', 'company_id' => '2', 'employee_id' => '16', 'year' => '2018', 'month' => '4', 'expenses' => '13339.00', 'expenses_claim' => '13339.00', 'expenses_bill' => '13339.00', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
