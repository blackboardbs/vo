<?php

namespace Database\Seeders;

use App\Models\ScheduleRunFrequency;
use Illuminate\Database\Seeder;

class ScheduleRunFrequencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ScheduleRunFrequency::truncate();

        $scheduleRunFrequency = tenancy()->central(fn() => \App\Models\Central\ScheduleRunFrequency::all());

        ScheduleRunFrequency::insert($scheduleRunFrequency->toArray());
    }
}
