<?php

namespace Database\Seeders;

use App\Models\BBBEELevel;
use Illuminate\Database\Seeder;

class BBBEELevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BBBEELevel::truncate();

        $bbbeeLevel = tenancy()->central(fn() => \App\Models\Central\BBBEELevel::all());

        BBBEELevel::insert($bbbeeLevel->toArray());
    }
}
