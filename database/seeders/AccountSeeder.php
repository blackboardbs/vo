<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Account::truncate();

        $account = tenancy()->central(fn() => \App\Models\Central\Account::all());

        Account::insert($account->toArray());
    }
}
