<?php

namespace Database\Seeders;

use App\Models\Security;
use Illuminate\Database\Seeder;

class SecurityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Security::truncate();

        $security = tenancy()->central(fn() => \App\Models\Central\Security::all());

        Security::insert($security->toArray());
    }
}
