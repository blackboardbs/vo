<?php

namespace Database\Seeders;

use App\Models\AccountType;
use Illuminate\Database\Seeder;

class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AccountType::truncate();

        $accountType = tenancy()->central(fn() => \App\Models\Central\AccountType::all());

        AccountType::insert($accountType->toArray());
    }
}
