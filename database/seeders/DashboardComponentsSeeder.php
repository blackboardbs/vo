<?php

namespace Database\Seeders;

use App\Models\Central\DashboardComponent;
use App\Models\DynamicDashboard;
use Illuminate\Database\Seeder;

class DashboardComponentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DynamicDashboard::truncate();

        $dynamicDashboard = tenancy()->central(fn() => DashboardComponent::all());

        DynamicDashboard::insert($dynamicDashboard->toArray());
    }
}
