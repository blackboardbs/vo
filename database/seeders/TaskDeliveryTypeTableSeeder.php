<?php

namespace Database\Seeders;

use App\Models\Central\DeliveryType;
use App\Models\TaskDeliveryType;
use Illuminate\Database\Seeder;

class TaskDeliveryTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TaskDeliveryType::truncate();

        $deliveryType = tenancy()->central(fn() => DeliveryType::all());

        TaskDeliveryType::insert($deliveryType->toArray());
    }
}
