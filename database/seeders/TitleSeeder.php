<?php

namespace Database\Seeders;

use App\Models\Title;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TitleSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Title::truncate();

        $title = tenancy()->central(fn() => \App\Models\Central\Title::all());

        Title::insert($title->toArray());
    }
}
