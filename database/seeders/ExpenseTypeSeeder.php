<?php

namespace Database\Seeders;

use App\Models\ExpenseType;
use Illuminate\Database\Seeder;

class ExpenseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ExpenseType::truncate();

        $expenseType = tenancy()->central(fn() => \App\Models\Central\ExpenseType::all());

        ExpenseType::insert($expenseType->toArray());
    }
}
