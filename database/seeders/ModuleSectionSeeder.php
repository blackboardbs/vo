<?php

namespace Database\Seeders;

use App\Models\ModuleRole;
use App\Models\ModuleSection;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ModuleSectionSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ModuleSection::truncate();

        $moduleSection = tenancy()->central(fn() => \App\Models\Central\ModuleSection::all());

        ModuleSection::insert($moduleSection->toArray());
    }
}
