<?php

namespace Database\Seeders;

use App\Models\Central\DefaultDashboard;
use App\Models\LandingPageDashboard;
use Illuminate\Database\Seeder;

class LandingPageDashboardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        LandingPageDashboard::truncate();

        $lpDashboard = tenancy()->central(fn() => \App\Models\Central\LandingPageDashboard::all());

        LandingPageDashboard::insert($lpDashboard->toArray());
    }
}
