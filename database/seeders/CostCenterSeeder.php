<?php

namespace Database\Seeders;

use App\Models\CostCenter;
use Illuminate\Database\Seeder;

class CostCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CostCenter::truncate();

        $costCenter = tenancy()->central(fn() => \App\Models\Central\CostCenter::all());

        CostCenter::insert($costCenter->toArray());
    }
}
