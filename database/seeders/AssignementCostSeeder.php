<?php

namespace Database\Seeders;

use App\Models\AssignmentCost;
use Illuminate\Database\Seeder;

class AssignementCostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AssignmentCost::insert([
            ['id' => '35', 'ass_id' => '6', 'account_id' => '9', 'account_element_id' => '36', 'description' => 'Mileage RTT R200 per day x 21 days', 'note' => '', 'cost' => '4200.00', 'payment_frequency' => 'Monthly', 'payment_method' => '3', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2017-10-18 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '36', 'ass_id' => '14', 'account_id' => '9', 'account_element_id' => '36', 'description' => 'Milleage', 'note' => 'R200 per day to RTT', 'cost' => '4200.00', 'payment_frequency' => 'Monthly', 'payment_method' => '3', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2017-11-16 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '37', 'ass_id' => '22', 'account_id' => '9', 'account_element_id' => '36', 'description' => 'Travel to RTT @ R200 per day', 'note' => 'Travel to RTT @ R200 per day', 'cost' => '26400.00', 'payment_frequency' => 'Monthly', 'payment_method' => '3', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-01-18 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '38', 'ass_id' => '30', 'account_id' => '1', 'account_element_id' => '15', 'description' => 'Data', 'note' => 'R100 per month for data to skype and research', 'cost' => '400.00', 'payment_frequency' => 'Monthly', 'payment_method' => '3', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-03-31 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '39', 'ass_id' => '30', 'account_id' => '3', 'account_element_id' => '3', 'description' => 'Recruitment Commission', 'note' => 'R3500 per month for 3 months. Submit as expense claim.', 'cost' => '10500.00', 'payment_frequency' => 'Monthly', 'payment_method' => '2', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-03-31 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '40', 'ass_id' => '54', 'account_id' => '9', 'account_element_id' => '0', 'description' => 'Traveling', 'note' => 'feegerghr', 'cost' => '1000.00', 'payment_frequency' => 'Monthly in arears', 'payment_method' => '4', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-06 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '41', 'ass_id' => '54', 'account_id' => '9', 'account_element_id' => '0', 'description' => 'Hotel', 'note' => 'eserdvefdv', 'cost' => '400.00', 'payment_frequency' => 'Monthly in arears', 'payment_method' => '4', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-06 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '42', 'ass_id' => '54', 'account_id' => '9', 'account_element_id' => '64', 'description' => '', 'note' => 'fervrev wewefw few wef wec', 'cost' => '1000.00', 'payment_frequency' => 'Monthly in arears', 'payment_method' => '2', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-14 22:00:00', 'updated_at' => '2018-08-18 22:58:10'],
            ['id' => '43', 'ass_id' => '55', 'account_id' => '9', 'account_element_id' => '37', 'description' => 'Travel to Customer', 'note' => 'Travel to Albania', 'cost' => '1000.00', 'payment_frequency' => 'Monthly', 'payment_method' => '3', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-26 17:58:57', 'updated_at' => '2018-08-26 17:58:57'],
            ['id' => '44', 'ass_id' => '55', 'account_id' => '1', 'account_element_id' => '15', 'description' => 'Test DInternet', 'note' => 'Test workflow', 'cost' => '400.00', 'payment_frequency' => 'Monthly', 'payment_method' => '4', 'status' => '1', 'company_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-26 18:02:18', 'updated_at' => '2018-08-26 18:02:18'],
        ]);
    }
}
