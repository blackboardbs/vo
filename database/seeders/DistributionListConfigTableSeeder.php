<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DistributionListConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $candidate_email_body = '<p>Please see attached below our <MONTHNAME> list of TOP CANDIDATES prepared specifically for'.
                                    'your company!!  The list contains some of our candidates that are available immediately up to 1'.
                                    'Calendar Month.  Please take note that the candidates are selected based on the skills normally'.
                                    'requested or used by your company.  Please advise when requesting a CV or sending through a'.
                                    'job spec when you would like the person to start (if accepted for the role) – It will help us'.
                                    'evaluate what position to focus on for when.</p>'.
                                    '<p>The list contains BEE and non-BEE candidates in Gauteng (mostly), KZN and Cape Town areas'.
                                    '(some willing to relocate).  Please have a quick look through our list and advise what <b>CV reference'.
                                    'number</b> you would like me to send through to you!  To make it easier, please also provide'.
                                    'tentative time slots so we can find out the availability from the candidate when we send the CV.'.
                                    'But of course, these are not all candidates we have (much, MUCH more candidates and skills for'.
                                    'you to have a look at) so feel free to suggest any alternative CV’s and I will send through asap so'.
                                    'we can get the ball rolling!</p>'.
                                    '<p>Please also take note that we do not only assist with IT vacancies / candidates – we also assist our'.
                                    'client on a regular basis with Administration, Financial, Legal, Sales, Marketing, etc. vacancies so'.
                                    'we have suitable candidates on our database.  Please request any assistance and we will advise'.
                                    'accordingly.  We also have an Engineering Department if you would require assistance in that field'.
                                    'or would like to view their marketing list.</p>'.
                                    '<CVLIST>'.
                                    '<p>Please advise if you would wish not to receive these lists anymore and I will take your e-mail'.
                                    'address off the list. <a href="">Click here to opt out.</a></p>'.
                                    '<p>Kind regards / Vriendelike groete,</p>'.
                                    '<p><b><CONSULTANTNAME> | Blackboard Recruitment Team Leader<br/>Blackboard BI</b></p>'.
                                    '<p>Mobile: <CELLNUMBER><br/>'.
                                    'Opposite 27 The Spiral Walk | Menlo Park | Pretoria</p>';

        $job_email_body = '<p>Please see attached below our <MONTHNAME> list of TOP Jobs that matches your skills and experience</p>'.
                            '<p><some text on how to apply for these positions></p>'.
                            '<JOBLIST>'.
                            '<p>Please advise if you would wish not to receive these lists anymore and I will take your e-mail'.
                            'address off the list. <a href="">Click here to opt out.</a></p>'.
                            '<p>Kind regards / Vriendelike groete,</p>'.
                            '<p><b><CONSULTANTNAME> | Blackboard Recruitment Team Leader<br/>Blackboard BI</b></p>'.
                            '<p>Mobile: <CELLNUMBER><br/>'.
                            'Opposite 27 The Spiral Walk | Menlo Park | Pretoria</p>';

        App\Models\DistributionListConfig::insert([
            ['id' => '1', 'name' => 'Candidates', 'from' => 'info@blackboardbs.com', 'subject' => 'Candidates List', 'email_body' => $candidate_email_body, 'status_id' => 1, 'creator_id' => 1, 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'name' => 'Jobs', 'from' => 'info@blackboardbs.com', 'subject' => 'Jobs List', 'email_body' => $job_email_body, 'status_id' => 1, 'creator_id' => 1, 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
