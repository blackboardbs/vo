<?php

namespace Database\Seeders;

use App\Models\InvoiceStatus;
use Illuminate\Database\Seeder;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        InvoiceStatus::truncate();

        $invoiceStatus = tenancy()->central(fn() => \App\Models\Central\InvoiceStatus::all());

        InvoiceStatus::insert($invoiceStatus->toArray());
    }
}
