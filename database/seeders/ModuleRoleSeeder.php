<?php

namespace Database\Seeders;

use App\Models\Central\Module;
use App\Models\ModuleRole;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ModuleRoleSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ModuleRole::truncate();

        $moduleRole = tenancy()->central(fn() => \App\Models\Central\ModuleRole::all());

        ModuleRole::insert($moduleRole->toArray());
    }
}
