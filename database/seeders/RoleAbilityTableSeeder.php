<?php

namespace Database\Seeders;

use App\Models\Central\RoleAbility;
use App\Models\RoleAbilities;
use Illuminate\Database\Seeder;

class RoleAbilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RoleAbilities::truncate();

        $roleAbility = tenancy()->central(fn() => RoleAbility::all());

        RoleAbilities::insert($roleAbility->toArray());
    }
}
