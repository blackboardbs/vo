<?php

namespace Database\Seeders;

use App\Models\WorkType;
use Illuminate\Database\Seeder;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WorkType::truncate();

        $workType = tenancy()->central(fn() => \App\Models\Central\WorkType::all());

        WorkType::insert($workType->toArray());
    }
}
