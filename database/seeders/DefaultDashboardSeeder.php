<?php

namespace Database\Seeders;

use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use App\Models\DefaultDashboard;

class DefaultDashboardSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DefaultDashboard::truncate();

        DefaultDashboard::insert($this->readCsv('default_dashboards.csv'));
    }
}
