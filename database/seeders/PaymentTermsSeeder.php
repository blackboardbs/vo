<?php

namespace Database\Seeders;

use App\Models\PaymentTerm;
use Illuminate\Database\Seeder;

class PaymentTermsSeeder extends Seeder
{
    public function run(): void
    {
        PaymentTerm::truncate();

        $paymentTerms = tenancy()->central(fn() => \App\Models\Central\PaymentTerm::all());

        PaymentTerm::insert($paymentTerms->toArray());
    }
}
