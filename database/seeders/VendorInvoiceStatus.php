<?php

namespace Database\Seeders;

use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use App\Models\VendorInvoiceStatus as Status;


class VendorInvoiceStatus extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Status::truncate();

        $vendorInvoiceStatus = tenancy()->central(fn() => \App\Models\Central\VendorInvoiceStatus::all());

        Status::insert($vendorInvoiceStatus->toArray());
    }
}
