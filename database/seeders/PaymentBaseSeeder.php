<?php

namespace Database\Seeders;

use App\Models\PaymentBase;
use Illuminate\Database\Seeder;

class PaymentBaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PaymentBase::truncate();

        $paymentBase = tenancy()->central(fn() => \App\Models\Central\PaymentBase::all());

        PaymentBase::insert($paymentBase->toArray());
    }
}
