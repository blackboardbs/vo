<?php

namespace Database\Seeders;

use App\Models\Calendar;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class CalendarSeeder extends Seeder
{
    use HasCsv;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Calendar::truncate();

        $calendar = tenancy()->central(fn() => \App\Models\Central\Calendar::all());

        foreach ($calendar->chunk(300) as $calendar){
            Calendar::insert($calendar->toArray());
        }
    }
}
