<?php

namespace Database\Seeders;

use App\Models\V_TimeExp;
use Illuminate\Database\Seeder;

class V_TimeExpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_TimeExp::insert([
            ['id' => '1', 'company_id' => '1', 'time_id' => '22', 'yearwk' => '201741', 'expenses' => '200.00', 'expenses_claim' => '200.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'company_id' => '1', 'time_id' => '23', 'yearwk' => '201742', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'company_id' => '1', 'time_id' => '34', 'yearwk' => '201745', 'expenses' => '400.00', 'expenses_claim' => '400.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'company_id' => '1', 'time_id' => '36', 'yearwk' => '201745', 'expenses' => '400.00', 'expenses_claim' => '400.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'company_id' => '1', 'time_id' => '37', 'yearwk' => '201746', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'company_id' => '1', 'time_id' => '40', 'yearwk' => '201747', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'company_id' => '1', 'time_id' => '43', 'yearwk' => '201748', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'company_id' => '1', 'time_id' => '48', 'yearwk' => '201749', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'company_id' => '1', 'time_id' => '59', 'yearwk' => '201750', 'expenses' => '1000.00', 'expenses_claim' => '1000.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'company_id' => '1', 'time_id' => '63', 'yearwk' => '201750', 'expenses' => '456.00', 'expenses_claim' => '456.00', 'expenses_bill' => '0.00', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
