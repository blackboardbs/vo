<?php

namespace Database\Seeders;

use App\Models\ResourcePosition;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ResourcePositionSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ResourcePosition::truncate();

        $resourcePosition = tenancy()->central(fn() => \App\Models\Central\ResourcePosition::all());

        ResourcePosition::insert($resourcePosition->toArray());
    }
}
