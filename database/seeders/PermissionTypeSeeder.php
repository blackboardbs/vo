<?php

namespace Database\Seeders;

use App\Models\PermissionType;
use Illuminate\Database\Seeder;

class PermissionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PermissionType::truncate();

        $permissionType = tenancy()->central(fn() => \App\Models\Central\PermissionType::all());

        PermissionType::insert([
            ['id' => '1', 'name' => 'View', 'status_id' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'name' => 'Create', 'status_id' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'name' => 'Update', 'status_id' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'name' => 'Full', 'status_id' => '1', 'creator_id' => '1', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
