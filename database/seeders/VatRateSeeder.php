<?php

namespace Database\Seeders;

use App\Models\VatRate;
use Illuminate\Database\Seeder;

class VatRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VatRate::truncate();

        $vat = tenancy()->central(fn() => \App\Models\Central\VatRate::all());

        VatRate::insert($vat->toArray());
    }
}
