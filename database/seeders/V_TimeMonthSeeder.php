<?php

namespace Database\Seeders;

use App\Models\V_TimeMonth;
use Illuminate\Database\Seeder;

class V_TimeMonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_TimeMonth::insert([
            ['id' => '1', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '7', 'hours_bill' => '0', 'hours_no_bill' => '0', 'amount' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '8', 'hours_bill' => '166', 'hours_no_bill' => '0', 'amount' => '141100.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '9', 'hours_bill' => '145', 'hours_no_bill' => '0', 'amount' => '123250.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '10', 'hours_bill' => '129', 'hours_no_bill' => '0', 'amount' => '109650.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '11', 'hours_bill' => '114', 'hours_no_bill' => '45', 'amount' => '96900.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'company_id' => '1', 'employee_id' => '1', 'year' => '2017', 'month' => '12', 'hours_bill' => '75', 'hours_no_bill' => '40', 'amount' => '63750.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'company_id' => '1', 'employee_id' => '1', 'year' => '2018', 'month' => '1', 'hours_bill' => '206', 'hours_no_bill' => '0', 'amount' => '175100.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'company_id' => '1', 'employee_id' => '1', 'year' => '2018', 'month' => '2', 'hours_bill' => '167', 'hours_no_bill' => '2', 'amount' => '141950.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'company_id' => '1', 'employee_id' => '1', 'year' => '2018', 'month' => '3', 'hours_bill' => '194', 'hours_no_bill' => '55', 'amount' => '164900.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'company_id' => '1', 'employee_id' => '1', 'year' => '2018', 'month' => '4', 'hours_bill' => '146', 'hours_no_bill' => '23', 'amount' => '124100.00', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
