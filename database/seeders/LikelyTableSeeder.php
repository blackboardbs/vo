<?php

namespace Database\Seeders;

use App\Models\Likely;
use Illuminate\Database\Seeder;

class LikelyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Likely::truncate();

        $likely = tenancy()->central(fn() => \App\Models\Central\Likely::all());

        Likely::insert($likely->toArray());
    }
}
