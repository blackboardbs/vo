<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PaymentMethod::truncate();

        $paymentMethod = tenancy()->central(fn() => \App\Models\Central\PaymentMethod::all());

        PaymentMethod::insert($paymentMethod->toArray());
    }
}
