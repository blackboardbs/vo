<?php

namespace Database\Seeders;

use App\Models\AccountElement;
use Illuminate\Database\Seeder;

class AccountElementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AccountElement::truncate();

        $element = tenancy()->central(fn() => \App\Models\Central\AccountElement::all());

        AccountElement::insert($element->toArray());
    }
}
