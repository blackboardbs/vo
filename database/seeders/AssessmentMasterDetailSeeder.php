<?php

namespace Database\Seeders;

use App\Models\AssessmentMasterDetails;
use App\Models\Central\AssessmentMasterDetail;
use Illuminate\Database\Seeder;

class AssessmentMasterDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AssessmentMasterDetails::truncate();

        $assessmentMasterDetails = tenancy()->central(fn() => AssessmentMasterDetail::all());

        AssessmentMasterDetails::insert($assessmentMasterDetails->toArray());
    }
}
