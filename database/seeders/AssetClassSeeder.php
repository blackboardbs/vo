<?php

namespace Database\Seeders;

use App\Models\AssetClass;
use Illuminate\Database\Seeder;

class AssetClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        AssetClass::truncate();

        $equipmentClass = tenancy()->central(fn() => \App\Models\Central\AssetClass::all());

        AssetClass::insert($equipmentClass->toArray());
    }
}
