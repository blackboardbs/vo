<?php

namespace Database\Seeders;

use App\Models\OnboardingStatus;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OnboardingStatusSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        OnboardingStatus::truncate();

        $onboardingStatus = tenancy()->central(fn() => \App\Models\Central\OnboardingStatus::all());

        OnboardingStatus::insert($onboardingStatus->toArray());
    }
}
