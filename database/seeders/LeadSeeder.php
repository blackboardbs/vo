<?php

namespace Database\Seeders;

use App\Models\Lead;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Lead::insert([
            ['id' => '1', 'name' => 'Network', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'name' => 'LinkedIn', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'name' => 'Facebook', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'name' => 'Eisting Customer', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'name' => 'Word Of Mouth', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'name' => 'Cold Call', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'name' => 'Referral', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
