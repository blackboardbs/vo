<?php

namespace Database\Seeders;

use App\Models\CvCompany;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class CvCompanySeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CvCompany::truncate();

        CvCompany::insert($this->readCsv('cv_company.csv'));
    }
}
