<?php

namespace Database\Seeders;

use App\Models\ProcessStatus;
use Illuminate\Database\Seeder;

class ProcessStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProcessStatus::truncate();

        $processStatus = tenancy()->central(fn() => \App\Models\Central\ProcessStatus::all());

        ProcessStatus::insert($processStatus->toArray());
    }
}
