<?php

namespace Database\Seeders;

use App\Models\AssessmentMaster;
use Illuminate\Database\Seeder;

class AssessmentMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AssessmentMaster::truncate();

        $assessmentMaster = tenancy()->central(fn() => \App\Models\Central\AssessmentMaster::all());

        AssessmentMaster::insert($assessmentMaster->toArray());
    }
}
