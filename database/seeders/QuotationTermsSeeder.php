<?php

namespace Database\Seeders;

use App\Models\Central\QuotationTerm;
use App\Models\QuotationTerms;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class QuotationTermsSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        QuotationTerms::truncate();

        $quotationTerms = tenancy()->central(fn() => QuotationTerm::all());

        QuotationTerms::insert($quotationTerms->toArray());
    }
}
