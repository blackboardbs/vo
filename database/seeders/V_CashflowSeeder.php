<?php

namespace Database\Seeders;

use App\Models\V_Cashflow;
use Illuminate\Database\Seeder;

class V_CashflowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_Cashflow::insert([
            ['id' => '1', 'company_id' => '1', 't_date' => '2018-08-06', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'Vodacom', 'source' => 'PE', 'amount_out' => '2500.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'company_id' => '1', 't_date' => '2018-09-10', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'Vodacom', 'source' => 'PE', 'amount_out' => '2500.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'company_id' => '1', 't_date' => '2018-10-08', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'Vodacom', 'source' => 'PE', 'amount_out' => '2500.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'company_id' => '1', 't_date' => '2018-11-12', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'Vodacom', 'source' => 'PE', 'amount_out' => '2500.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'company_id' => '1', 't_date' => '2018-12-10', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'Vodacom', 'source' => 'PE', 'amount_out' => '2500.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'company_id' => '1', 't_date' => '2018-08-15', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'VOX Telecom', 'source' => 'PE', 'amount_out' => '1250.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'company_id' => '1', 't_date' => '2018-09-15', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'VOX Telecom', 'source' => 'PE', 'amount_out' => '1250.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'company_id' => '1', 't_date' => '2018-10-15', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'VOX Telecom', 'source' => 'PE', 'amount_out' => '1250.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'company_id' => '1', 't_date' => '2018-11-15', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'VOX Telecom', 'source' => 'PE', 'amount_out' => '1250.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'company_id' => '1', 't_date' => '2018-12-14', 'account_id' => '1', 'account_element_id' => '14', 'description' => 'VOX Telecom', 'source' => 'PE', 'amount_out' => '1250.00', 'amount_in' => '0.00', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
