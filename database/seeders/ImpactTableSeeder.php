<?php

namespace Database\Seeders;

use App\Models\Impact;
use Illuminate\Database\Seeder;

class ImpactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Impact::truncate();

        $impact = tenancy()->central(fn() => \App\Models\Central\Impact::all());

        Impact::insert($impact->toArray());
    }
}
