<?php

namespace Database\Seeders;

use App\Models\RiskArea;
use Illuminate\Database\Seeder;

class RiskAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RiskArea::truncate();

        $riskArea = tenancy()->central(fn() => \App\Models\Central\RiskArea::all());

        RiskArea::insert($riskArea->toArray());
    }
}
