<?php

namespace Database\Seeders;

use App\Models\Commission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Commission::truncate();

        $commission = tenancy()->central(fn() => \App\Models\Central\Commission::all());

        Commission::insert($commission->toArray());

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
