<?php

namespace Database\Seeders;

use App\Models\Availability;
use Illuminate\Database\Seeder;

class AvailabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Availability::insert([
            ['id' => '1', 'primary_ind' => '1', 'res_id' => '1', 'avail_status' => '2', 'status_date' => '2018-05-01', 'avail_note' => 'uguguh ubtu guy', 'avail_date' => '2018-07-01', 'rate_hour' => '200.00', 'rate_month' => '10000.00', 'status_id' => '1', 'creator_id' => '1', 'created_at' => '2018-05-01 00:00:00', 'updated_at' => '2018-08-15 07:13:54'],
        ]);
    }
}
