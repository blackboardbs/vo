<?php

namespace Database\Seeders;

use App\Models\UserType;
use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        UserType::truncate();

        $userType = tenancy()->central(fn() => \App\Models\Central\UserType::all());

        UserType::insert($userType->toArray());
    }
}
