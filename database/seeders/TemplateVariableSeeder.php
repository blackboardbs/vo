<?php

namespace Database\Seeders;

use App\Models\TemplateVariable;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TemplateVariableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TemplateVariable::truncate();

        $templateVariables = tenancy()->central(fn() => \App\Models\Central\TemplateVariable::all());

        TemplateVariable::insert($templateVariables->toArray());
    }
}
