<?php

namespace Database\Seeders;

use App\Models\Central\Term;
use App\Models\Terms;
use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Terms::truncate();

        $terms = tenancy()->central(fn() => Term::all());

        Terms::insert($terms->toArray());
    }
}
