<?php

namespace Database\Seeders;

use App\Models\SystemHealth;
use App\Traits\HasCsv;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SysytemHealthMasterSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SystemHealth::truncate();

        $systemHealth = tenancy()->central(fn() => \App\Models\Central\SystemHealth::all());

        SystemHealth::insert($systemHealth->toArray());
    }
}
