<?php

namespace Database\Seeders;

use App\Models\ScoutingRole;
use Illuminate\Database\Seeder;

class ScoutingRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ScoutingRole::truncate();

        $scoutingRoles = tenancy()->central(fn() => \App\Models\Central\ScoutingRole::all());

        ScoutingRole::insert($scoutingRoles->toArray());
    }
}
