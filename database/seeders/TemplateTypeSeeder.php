<?php

namespace Database\Seeders;

use App\Models\TemplateType;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class TemplateTypeSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TemplateType::truncate();

        $templateType = tenancy()->central(fn() => \App\Models\Central\TemplateType::all());

        TemplateType::insert($templateType->toArray());
    }
}
