<?php

namespace Database\Seeders;

use App\Models\Central\ShCategory;
use App\Models\SystemHealthCategory;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ShCategorySeeder extends Seeder
{
        use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SystemHealthCategory::truncate();

        $shCategory = tenancy()->central(fn() => ShCategory::all());

        SystemHealthCategory::insert($shCategory->toArray());
    }
}
