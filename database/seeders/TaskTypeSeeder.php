<?php

namespace Database\Seeders;

use App\Models\TaskType;
use Illuminate\Database\Seeder;

class TaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TaskType::truncate();

        $taskType = tenancy()->central(fn() => \App\Models\Central\TaskType::all());

        TaskType::insert($taskType->toArray());
    }
}
