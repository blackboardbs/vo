<?php

namespace Database\Seeders;

use App\Models\Leave;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/leave.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $leave = new Leave;
            $leave->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $leave->emp_id = isset($record['emp_id']) && $record['emp_id'] != '' && $record['emp_id'] != 'NULL' ? $record['emp_id'] : null;
            $leave->date_from = isset($record['date_from']) && $record['date_from'] != '' && $record['date_from'] != 'NULL' ? $record['date_from'] : null;
            $leave->date_to = isset($record['date_to']) && $record['date_to'] != '' && $record['date_to'] != 'NULL' ? $record['date_to'] : null;
            $leave->no_of_days = isset($record['no_of_days']) && $record['no_of_days'] != '' && $record['no_of_days'] != 'NULL' ? $record['no_of_days'] : null;
            $leave->leave_type_id = isset($record['leave_type_id']) && $record['leave_type_id'] != '' && $record['leave_type_id'] != 'NULL' ? $record['leave_type_id'] : null;
            $leave->contact_details = isset($record['contact_details']) && $record['contact_details'] != '' && $record['contact_details'] != 'NULL' ? $record['contact_details'] : null;
            $leave->approve_emp_id = isset($record['approve_emp_id']) && $record['approve_emp_id'] != '' && $record['approve_emp_id'] != 'NULL' ? $record['approve_emp_id'] : null;
            $leave->approve_date = isset($record['approve_date']) && $record['approve_date'] != '' && $record['approve_date'] != 'NULL' ? $record['approve_date'] : null;
            $leave->status = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : null;
            $leave->leave_status = isset($record['leave_status']) && $record['leave_status'] != '' && $record['leave_status'] != 'NULL' ? $record['leave_status'] : null;
            $leave->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : null;
            //$leave->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$leave->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $leave->save();
        }
        //
        /*Leave::insert([
            ['id' => '1', 'emp_id' => '1', 'date_from' => '2017-09-29', 'date_to' => '2017-09-29', 'no_of_days' => '6', 'leave_type_id' => '1', 'contact_details' => 'Phone', 'approve_emp_id' => '1', 'approve_date' => '2017-10-13', 'leave_status' => '1', 'status' => '1', 'creator_id' => '1', 'created_at' => now(),'updated_at' => now()],
            ['id' => '2', 'emp_id' => '12', 'date_from' => '2017-12-18', 'date_to' => '2017-12-18', 'no_of_days' => '7', 'leave_type_id' => '1', 'contact_details' => 'Not available', 'approve_emp_id' => '1', 'approve_date' => '2017-10-24', 'leave_status' => '1', 'status' => '1', 'creator_id' => '12', 'created_at' => now(),'updated_at' => now()],
            ['id' => '3', 'emp_id' => '13', 'date_from' => '2017-11-06', 'date_to' => '2017-11-06', 'no_of_days' => '2', 'leave_type_id' => '6', 'contact_details' => '0814472196', 'approve_emp_id' => '1', 'approve_date' => '2017-10-24', 'leave_status' => '1', 'status' => '1', 'creator_id' => '13', 'created_at' => now(),'updated_at' => now()],
            ['id' => '4', 'emp_id' => '13', 'date_from' => '2017-11-20', 'date_to' => '2017-11-20', 'no_of_days' => '2', 'leave_type_id' => '6', 'contact_details' => '0814472196', 'approve_emp_id' => '1', 'approve_date' => '2017-10-24', 'leave_status' => '1', 'status' => '1', 'creator_id' => '13', 'created_at' => now(),'updated_at' => now()],
            ['id' => '6', 'emp_id' => '14', 'date_from' => '2017-12-25', 'date_to' => '2017-12-25', 'no_of_days' => '5', 'leave_type_id' => '4', 'contact_details' => 'Mandatory Leave', 'approve_emp_id' => '1', 'approve_date' => '2017-12-15', 'leave_status' => '1', 'status' => '1', 'creator_id' => '1', 'created_at' => now(),'updated_at' => now()],
            ['id' => '7', 'emp_id' => '13', 'date_from' => '2017-12-21', 'date_to' => '2017-12-21', 'no_of_days' => '7', 'leave_type_id' => '1', 'contact_details' => '0814472196', 'approve_emp_id' => '7', 'approve_date' => '2018-01-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '13', 'created_at' => now(),'updated_at' => now()],
            ['id' => '8', 'emp_id' => '15', 'date_from' => '2018-03-02', 'date_to' => '2018-03-02', 'no_of_days' => '1', 'leave_type_id' => '1', 'contact_details' => '0785136499', 'approve_emp_id' => '1', 'approve_date' => '2018-02-24', 'leave_status' => '1', 'status' => '1', 'creator_id' => '1', 'created_at' => now(),'updated_at' => now()],
            ['id' => '9', 'emp_id' => '15', 'date_from' => '2018-03-21', 'date_to' => '2018-03-21', 'no_of_days' => '2', 'leave_type_id' => '1', 'contact_details' => '0785136499', 'approve_emp_id' => '7', 'approve_date' => '2018-03-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '15', 'created_at' => now(),'updated_at' => now()],
            ['id' => '10', 'emp_id' => '15', 'date_from' => '2018-03-22', 'date_to' => '2018-03-22', 'no_of_days' => '2', 'leave_type_id' => '1', 'contact_details' => '0785136499', 'approve_emp_id' => '7', 'approve_date' => '2018-03-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '15', 'created_at' => now(),'updated_at' => now()],
            ['id' => '11', 'emp_id' => '15', 'date_from' => '2018-03-28', 'date_to' => '2018-03-28', 'no_of_days' => '2', 'leave_type_id' => '1', 'contact_details' => '0785136499', 'approve_emp_id' => '7', 'approve_date' => '2018-03-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '15', 'created_at' => now(),'updated_at' => now()],
            ['id' => '12', 'emp_id' => '13', 'date_from' => '2018-03-28', 'date_to' => '2018-03-28', 'no_of_days' => '1', 'leave_type_id' => '1', 'contact_details' => '0814472196', 'approve_emp_id' => '7', 'approve_date' => '2018-03-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '13', 'created_at' => now(),'updated_at' => now()],
            ['id' => '13', 'emp_id' => '13', 'date_from' => '2018-04-18', 'date_to' => '2018-04-18', 'no_of_days' => '3', 'leave_type_id' => '1', 'contact_details' => '0814472196', 'approve_emp_id' => '7', 'approve_date' => '2018-03-29', 'leave_status' => '1', 'status' => '1', 'creator_id' => '13', 'created_at' => now(),'updated_at' => now()],
            ['id' => '14', 'emp_id' => '15', 'date_from' => '2018-05-28', 'date_to' => '2018-05-28', 'no_of_days' => '1', 'leave_type_id' => '1', 'contact_details' => '0785136499', 'approve_emp_id' => '1', 'approve_date' => '2018-05-18', 'leave_status' => '1', 'status' => '1', 'creator_id' => '1', 'created_at' => now(),'updated_at' => now()]
        ]);*/
    }
}
