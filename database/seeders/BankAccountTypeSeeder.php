<?php

namespace Database\Seeders;

use App\Models\BankAccountType;
use Illuminate\Database\Seeder;

class BankAccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BankAccountType::truncate();

        $bankAccountType = tenancy()->central(fn() => \App\Models\Central\BankAccountType::all());

        BankAccountType::insert($bankAccountType->toArray());
    }
}
