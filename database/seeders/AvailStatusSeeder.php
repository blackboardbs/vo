<?php

namespace Database\Seeders;

use App\Models\AvailStatus;
use Illuminate\Database\Seeder;

class AvailStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AvailStatus::truncate();

        $availStatus = tenancy()->central(fn() => \App\Models\Central\AvailStatus::all());

        AvailStatus::insert($availStatus->toArray());
    }
}
