<?php

namespace Database\Seeders;

use App\Models\Financial;
use Illuminate\Database\Seeder;

class FinancialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Financial::truncate();

        $financial = tenancy()->central(fn() => \App\Models\Central\Financial::all());

        Financial::insert($financial->toArray());
    }
}
