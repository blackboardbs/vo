<?php

namespace Database\Seeders;

use App\Models\SprintStatus;
use Illuminate\Database\Seeder;

class SprintStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SprintStatus::truncate();

        $sprintStatus = tenancy()->central(fn() => \App\Models\Central\SprintStatus::all());

        SprintStatus::insert($sprintStatus->toArray());
    }
}
