<?php

namespace Database\Seeders;

use App\Models\Naturalization;
use Illuminate\Database\Seeder;

class NaturalizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Naturalization::truncate();

        $naturalization = tenancy()->central(fn() => \App\Models\Central\Naturalization::all());

        Naturalization::insert($naturalization->toArray());
    }
}
