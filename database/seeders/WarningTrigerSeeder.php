<?php

namespace Database\Seeders;

use App\Models\WarningTriger;
use Illuminate\Database\Seeder;

class WarningTrigerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WarningTriger::truncate();

        $triggers = tenancy()->central(fn() => \App\Models\Central\WarningTriger::all());

        WarningTriger::insert($triggers->toArray());
    }
}
