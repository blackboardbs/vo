<?php

namespace Database\Seeders;

use App\Models\TechnicalRating;
use Illuminate\Database\Seeder;

class TechnicalRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TechnicalRating::truncate();

        $technicalRating = tenancy()->central(fn() => \App\Models\Central\TechnicalRating::all());

        TechnicalRating::insert($technicalRating->toArray());
    }
}
