<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class EventSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Event::truncate();

        $events = tenancy()->central(fn() => \App\Models\Central\Event::all());

        Event::insert($events->toArray());
    }
}
