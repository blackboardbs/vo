<?php

namespace Database\Seeders;

use App\Models\BusinessRating;
use Illuminate\Database\Seeder;

class BusinessRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BusinessRating::truncate();

        $businessRate = tenancy()->central(fn() => \App\Models\Central\BusinessRating::all());

        BusinessRating::insert($businessRate->toArray());
    }
}
