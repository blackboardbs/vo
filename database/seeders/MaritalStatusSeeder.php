<?php

namespace Database\Seeders;

use App\Models\MaritalStatus;
use Illuminate\Database\Seeder;

class MaritalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MaritalStatus::truncate();

        $maritalStatus = tenancy()->central(fn() => \App\Models\Central\MaritalStatus::all());

        MaritalStatus::insert($maritalStatus->toArray());
    }
}
