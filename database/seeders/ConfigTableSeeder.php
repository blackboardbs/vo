<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*Config::insert([
            [
                'id' => '1',
                'days_to_approve_leave' => '2',
                'calendar_content' => 'Calendar Content',
                'home_page_message' => 'Welcome to web Office',
                'auditing_records' => '1',
                'site_title' => 'Marvel Studios',
                'site_name' => 'www.blackboardbi.com',
                'admin_email' => 'info@blackboardbi.com',
                'assessment_approver_user_id' => '1',
                'utilization_method' => '2',
                'utilization_hours_per_week' => '40',
                'utilization_cost_hours_per_week' => '40',
                'cost_min_percent' => '10',
                'cost_min_colour' => 'FFFFFF',
                'cost_mid_colour' => 'FFFFFF',
                'cost_max_colour' => 'FFFFFF',
                'cost_max_percent' => '20',
                'absolute_path' => '/var/www/html/web_office',
                'project_terms_id' => '2',
                'claim_approver_id' => '1',
                'vendor_template_id' => '0',
                'resource_template_id' => '0',
                'site_id' => '1',
                'company_id' => '1',
                'cost_center_id' => '1',
                'timesheet_weeks' => '12',
                'timesheet_weeks_future' => 0,
                'created_at' => now(),
                'updated_at' => '2018-09-02 09:09:01'],
        ]);*/

        $csv = Reader::createFromPath(database_path('/data/config.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $user = new Config;
            $user->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $user->days_to_approve_leave = isset($record['days_to_approve_leave']) && $record['days_to_approve_leave'] != '' && $record['days_to_approve_leave'] != 'NULL' ? $record['days_to_approve_leave'] : null;
            $user->calendar_content = isset($record['calendar_content']) && $record['calendar_content'] != '' && $record['calendar_content'] != 'NULL' ? $record['calendar_content'] : null;
            $user->home_page_message = isset($record['home_page_message']) && $record['home_page_message'] != '' && $record['home_page_message'] != 'NULL' ? $record['home_page_message'] : null;
            $user->auditing_records = isset($record['auditing_records']) && $record['auditing_records'] != '' && $record['auditing_records'] != 'NULL' ? $record['auditing_records'] : null;
            $user->site_title = isset($record['site_title']) && $record['site_title'] != '' && $record['site_title'] != 'NULL' ? $record['site_title'] : null;
            $user->site_name = isset($record['site_name']) && $record['site_name'] != '' && $record['site_name'] != 'NULL' ? $record['site_name'] : null;
            $user->admin_email = isset($record['admin_email']) && $record['admin_email'] != '' && $record['admin_email'] != 'NULL' ? $record['admin_email'] : null;
            $user->assessment_approver_user_id = isset($record['assessment_approver_user_id']) && $record['assessment_approver_user_id'] != '' && $record['assessment_approver_user_id'] != 'NULL' ? $record['assessment_approver_user_id'] : null;
            $user->utilization_method = isset($record['utilization_method']) && $record['utilization_method'] != '' && $record['utilization_method'] != 'NULL' ? $record['utilization_method'] : null;
            $user->utilization_hours_per_week = isset($record['utilization_hours_per_week']) && $record['utilization_hours_per_week'] != '' && $record['utilization_hours_per_week'] != 'NULL' ? $record['utilization_hours_per_week'] : null;
            $user->utilization_cost_hours_per_week = isset($record['utilization_cost_hours_per_week']) && $record['utilization_cost_hours_per_week'] != '' && $record['utilization_cost_hours_per_week'] != 'NULL' ? $record['utilization_cost_hours_per_week'] : null;
            $user->cost_min_percent = isset($record['cost_min_percent']) && $record['cost_min_percent'] != '' && $record['cost_min_percent'] != 'NULL' ? $record['cost_min_percent'] : null;
            $user->cost_min_colour = isset($record['cost_min_colour']) && $record['cost_min_colour'] != '' && $record['cost_min_colour'] != 'NULL' ? $record['cost_min_colour'] : null;
            $user->cost_mid_colour = isset($record['cost_mid_colour']) && $record['cost_mid_colour'] != '' && $record['cost_mid_colour'] != 'NULL' ? $record['cost_mid_colour'] : null;
            $user->cost_max_colour = isset($record['cost_max_colour']) && $record['cost_max_colour'] != '' && $record['cost_max_colour'] != 'NULL' ? $record['cost_max_colour'] : null;
            $user->cost_max_percent = isset($record['cost_max_percent']) && $record['cost_max_percent'] != '' && $record['cost_max_percent'] != 'NULL' ? $record['cost_max_percent'] : null;
            $user->absolute_path = isset($record['absolute_path']) && $record['absolute_path'] != '' && $record['absolute_path'] != 'NULL' ? $record['absolute_path'] : null;
            $user->project_terms_id = isset($record['project_terms_id']) && $record['project_terms_id'] != '' && $record['project_terms_id'] != 'NULL' ? $record['project_terms_id'] : null;
            $user->claim_approver_id = isset($record['claim_approver_id']) && $record['claim_approver_id'] != '' && $record['claim_approver_id'] != 'NULL' ? $record['claim_approver_id'] : null;
            $user->vendor_template_id = isset($record['vendor_template_id']) && $record['vendor_template_id'] != '' && $record['vendor_template_id'] != 'NULL' ? $record['vendor_template_id'] : null;
            $user->resource_template_id = isset($record['resource_template_id']) && $record['resource_template_id'] != '' && $record['resource_template_id'] != 'NULL' ? $record['resource_template_id'] : null;
            $user->site_id = isset($record['site_id']) && $record['site_id'] != '' && $record['site_id'] != 'NULL' ? $record['site_id'] : null;
            $user->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            $user->cost_center_id = isset($record['cost_center_id']) && $record['cost_center_id'] != '' && $record['cost_center_id'] != 'NULL' ? $record['cost_center_id'] : null;
            $user->timesheet_weeks = isset($record['timesheet_weeks']) && $record['timesheet_weeks'] != '' && $record['timesheet_weeks'] != 'NULL' ? $record['timesheet_weeks'] : null;
            $user->timesheet_weeks_future = isset($record['timesheet_weeks_future']) && $record['timesheet_weeks_future'] != '' && $record['timesheet_weeks_future'] != 'NULL' ? $record['timesheet_weeks_future'] : null;
            $user->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : null;
            $user->save();
        }
    }
}
