<?php

namespace Database\Seeders;

use App\Models\ContractType;
use Illuminate\Database\Seeder;

class ContractTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ContractType::truncate();

        $contractType = tenancy()->central(fn() => \App\Models\Central\ContractType::all());

        ContractType::insert($contractType->toArray());
    }
}
