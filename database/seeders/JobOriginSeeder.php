<?php

namespace Database\Seeders;

use App\Models\JobOrigin;
use Illuminate\Database\Seeder;

class JobOriginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        JobOrigin::truncate();

        $jobOrigin = tenancy()->central(fn() => \App\Models\Central\JobOrigin::all());

        JobOrigin::insert($jobOrigin->toArray());
    }
}
