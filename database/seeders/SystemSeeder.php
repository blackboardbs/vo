<?php

namespace Database\Seeders;

use App\Models\System;
use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        System::truncate();

        $system = tenancy()->central(fn() => \App\Models\Central\System::all());

        System::insert($system->toArray());
    }
}
