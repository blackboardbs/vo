<?php

namespace Database\Seeders;

use App\Models\Expense;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/expense.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $expense = new Expense;
            $expense->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $expense->travel = isset($record['travel']) && $record['travel'] != '' && $record['travel'] != 'NULL' ? $record['travel'] : null;
            $expense->parking = isset($record['parking']) && $record['parking'] != '' && $record['parking'] != 'NULL' ? $record['parking'] : null;
            $expense->car_rental = isset($record['car_rental']) && $record['car_rental'] != '' && $record['car_rental'] != 'NULL' ? $record['car_rental'] : null;
            $expense->flights = isset($record['flights']) && $record['flights'] != '' && $record['flights'] != 'NULL' ? $record['flights'] : null;
            $expense->other = isset($record['other']) && $record['other'] != '' && $record['other'] != 'NULL' ? $record['other'] : null;
            $expense->accommodation = isset($record['accommodation']) && $record['accommodation'] != '' && $record['accommodation'] != 'NULL' ? $record['accommodation'] : null;
            $expense->out_of_town = isset($record['out_of_town']) && $record['out_of_town'] != '' && $record['out_of_town'] != 'NULL' ? $record['out_of_town'] : null;
            $expense->per_diem = isset($record['per_diem']) && $record['per_diem'] != '' && $record['per_diem'] != 'NULL' ? $record['per_diem'] : null;
            $expense->toll = isset($record['toll']) && $record['toll'] != '' && $record['toll'] != 'NULL' ? $record['toll'] : null;
            $expense->data = isset($record['data']) && $record['data'] != '' && $record['data'] != 'NULL' ? $record['data'] : null;
            //$expense->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$expense->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $expense->save();
        }
        /*Expense::insert([
            ['id' => '1', 'travel' => '3019', 'parking' => 'RTT Non-Billable Support', 'car_rental' => 'RTT_NB_201701', 'flights' => 'N/A', 'other' => '12', 'accommodation' => '1', 'out_of_town' => '13', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '2', 'travel' => '3015', 'parking' => 'Sasol Bums on Seat', 'car_rental' => 'RW_SASOL_BOS_201701', 'flights' => '4505202786', 'other' => '6', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '3', 'travel' => '3015', 'parking' => 'Sasol SCD 001', 'car_rental' => 'RW_SASOL_SCD_201701', 'flights' => '4504976639', 'other' => '1', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '4', 'travel' => '3019', 'parking' => 'RTT BI Support 01', 'car_rental' => 'RTT_BI_201710', 'flights' => '41030003332', 'other' => '12', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '5', 'travel' => '3015', 'parking' => 'Sasol SCD 002', 'car_rental' => 'RW_SASOL_SCD_201702', 'flights' => 'PO TBA', 'other' => '1', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '6', 'travel' => '3023', 'parking' => 'Blackboard Training Site', 'car_rental' => 'BBBI0001', 'flights' => 'BBBI0001', 'other' => '13', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '7', 'travel' => '3023', 'parking' => 'Blackboard Analysis', 'car_rental' => 'BBA0001', 'flights' => 'BBA0001', 'other' => '13', 'accommodation' => '1', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '8', 'travel' => '3023', 'parking' => 'Blackboard General', 'car_rental' => 'BBI_GEN', 'flights' => 'BBI_GEN', 'other' => '1', 'accommodation' => '12', 'out_of_town' => '13', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '9', 'travel' => '3019', 'parking' => 'RTT BI Support 02', 'car_rental' => 'RTT_BI_201711', 'flights' => '41030003411', 'other' => '12', 'accommodation' => '0', 'out_of_town' => '0', 'per_diem' => '0', 'toll' => '0', 'data' => '0', 'created_at' => '2017-12-31 00:00:00', 'updated_at' => '2017-12-31 00:00:00'],
            ['id' => '10', 'travel' => 'Test 1', 'parking' => 'Test 2', 'car_rental' => '', 'flights' => '', 'other' => 'Test 7', 'accommodation' => 'Test 3', 'out_of_town' => 'Test 5', 'per_diem' => 'Test 4', 'toll' => '', 'data' => 'Test 6', 'created_at' => '2018-08-17 15:45:55', 'updated_at' => '2018-08-17 15:45:55'],
            ['id' => '11', 'travel' => 'Test 1', 'parking' => 'Test 2', 'car_rental' => '', 'flights' => '', 'other' => 'Test 7', 'accommodation' => 'Test 3', 'out_of_town' => 'Test 5', 'per_diem' => 'Test 4', 'toll' => '', 'data' => 'Test 6', 'created_at' => '2018-08-17 15:54:06', 'updated_at' => '2018-08-17 15:54:06'],
            ['id' => '12', 'travel' => 'test', 'parking' => 'Billable Expenses Travel	Parking', 'car_rental' => '', 'flights' => '', 'other' => 'test', 'accommodation' => 'test', 'out_of_town' => 'test', 'per_diem' => 'test', 'toll' => '', 'data' => 'test', 'created_at' => '2018-08-17 16:12:26', 'updated_at' => '2018-08-17 16:12:26'],
            ['id' => '13', 'travel' => 'N/A', 'parking' => 'N/A', 'car_rental' => '', 'flights' => '', 'other' => 'This is other text', 'accommodation' => 'As arranged per customer.', 'out_of_town' => 'N/A', 'per_diem' => 'R330 per night locally. $50 per night in US.', 'toll' => '', 'data' => 'No internet or phone to be claimed.', 'created_at' => '2018-08-26 16:53:50', 'updated_at' => '2018-08-26 16:54:29'],
            ['id' => '14', 'travel' => 'dffdd', 'parking' => 'dfbd', 'car_rental' => '', 'flights' => '', 'other' => 'dfbx', 'accommodation' => 'fdbdf', 'out_of_town' => 'df', 'per_diem' => 'df', 'toll' => '', 'data' => 'dfb', 'created_at' => '2018-08-26 17:32:09', 'updated_at' => '2018-08-26 17:32:09'],
            ['id' => '15', 'travel' => 'Included', 'parking' => 'N/A', 'car_rental' => '', 'flights' => '', 'other' => 'This is other text', 'accommodation' => 'As arranged per customer.', 'out_of_town' => 'Claim R200 per day. Non billable.', 'per_diem' => 'R330 per night locally. $50 per night in US.', 'toll' => '', 'data' => 'No internet or phone to be claimed.', 'created_at' => '2018-08-26 17:33:48', 'updated_at' => '2018-08-26 17:34:59'],
            ['id' => '16', 'travel' => 'N/A', 'parking' => 'N/A', 'car_rental' => '', 'flights' => '', 'other' => 'N/A', 'accommodation' => 'N/A', 'out_of_town' => 'N/A', 'per_diem' => 'N/A', 'toll' => '', 'data' => 'N/A', 'created_at' => '2018-08-26 17:37:30', 'updated_at' => '2018-08-26 17:37:30'],
            ['id' => '17', 'travel' => 'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination', 'parking' => 'Reimbursable as incurred, including Airport Parking', 'car_rental' => '', 'flights' => '', 'other' => 'Reimbursable as incurred.', 'accommodation' => 'To be arranged by the client.', 'out_of_town' => 'R300 per day in South Africa and US $80 per day internationally (including Zimbabwe, Botswana)', 'per_diem' => 'Per Diem - To ask Stefan', 'toll' => '', 'data' => 'Data - To ask Stefan', 'created_at' => '2018-09-02 08:56:36', 'updated_at' => '2018-09-02 08:56:36'],
            ['id' => '18', 'travel' => 'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination', 'parking' => 'Reimbursable as incurred, including Airport Parking', 'car_rental' => '', 'flights' => '', 'other' => 'Reimbursable as incurred.', 'accommodation' => 'To be arranged by the client.', 'out_of_town' => 'R300 per day in South Africa and US $80 per day internationally (including Zimbabwe, Botswana)', 'per_diem' => 'Per Diem - To ask Stefan', 'toll' => '', 'data' => 'Data - To ask Stefan', 'created_at' => '2018-09-02 09:04:34', 'updated_at' => '2018-09-02 09:04:34'],
            ['id' => '19', 'travel' => 'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination. test 1', 'parking' => 'Reimbursable as incurred, including Airport Parking. test 1', 'car_rental' => '', 'flights' => '', 'other' => 'Reimbursable as incurred. test 1', 'accommodation' => 'To be arranged by the client. test 1', 'out_of_town' => 'R300 per day in South Africa and US $80 per day internationally (including Zimbabwe, Botswana). test 1', 'per_diem' => 'Per Diem - To ask Stefan. test 1', 'toll' => '', 'data' => 'Data - To ask Stefan. test 1', 'created_at' => '2018-09-02 09:13:24', 'updated_at' => '2018-09-02 09:13:24'],
            ['id' => '20', 'travel' => 'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination. test 1', 'parking' => 'Reimbursable as incurred, including Airport Parking. test 1', 'car_rental' => '', 'flights' => '', 'other' => 'Reimbursable as incurred. test 1', 'accommodation' => 'To be arranged by the client. test 1', 'out_of_town' => 'R300 per day in South Africa and US $80 per day internationally (including Zimbabwe, Botswana). test 1', 'per_diem' => 'Per Diem - To ask Stefan. test 1', 'toll' => '', 'data' => 'Data - To ask Stefan. test 1', 'created_at' => '2018-09-02 09:19:15', 'updated_at' => '2018-09-02 09:19:15'],
            ['id' => '21', 'travel' => 'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination. test 12', 'parking' => 'Reimbursable as incurred, including Airport Parking. test 12', 'car_rental' => '', 'flights' => '', 'other' => 'Reimbursable as incurred. test 2', 'accommodation' => 'fdsfsdf', 'out_of_town' => 'R300 per day in South Africa and US $80 per day internationally (including Zimbabwe, Botswana). test 13', 'per_diem' => 'Per Diem - To ask Stefan. test 14', 'toll' => '', 'data' => 'Data - To ask Stefan. test 14', 'created_at' => '2018-09-02 09:22:06', 'updated_at' => '2018-09-02 09:24:25'],
        ]);*/
    }
}
