<?php

namespace Database\Seeders;

use App\Models\Utilization;
use Illuminate\Database\Seeder;

class UtilizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Utilization::insert([
            ['id' => '1', 'yearwk' => '201730',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'yearwk' => '201731',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'yearwk' => '201732',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'yearwk' => '201733',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'yearwk' => '201734',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'yearwk' => '201735',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'yearwk' => '201736',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'yearwk' => '201737',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'yearwk' => '201738',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'yearwk' => '201739',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '11', 'yearwk' => '201740',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '12', 'yearwk' => '201741',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '13', 'yearwk' => '201742',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '14', 'yearwk' => '201743',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '15', 'yearwk' => '201744',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '16', 'yearwk' => '201745',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '17', 'yearwk' => '201746',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '18', 'yearwk' => '201747',	'res_no' => '3',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '19', 'yearwk' => '201748',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '20', 'yearwk' => '201749',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '21', 'yearwk' => '201750',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '22', 'yearwk' => '201751',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '23', 'yearwk' => '201752',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '24', 'yearwk' => '20181',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '25', 'yearwk' => '201802',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '26', 'yearwk' => '201803',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '27', 'yearwk' => '201804',	'res_no' => '4',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '28', 'yearwk' => '201805',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '29', 'yearwk' => '201806',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '30', 'yearwk' => '201807',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '31', 'yearwk' => '201808',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '32', 'yearwk' => '201809',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '33', 'yearwk' => '201810',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '34', 'yearwk' => '201811',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '35', 'yearwk' => '201812',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '36', 'yearwk' => '201813',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '37', 'yearwk' => '201814',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '38', 'yearwk' => '201815',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '39', 'yearwk' => '201816',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '40', 'yearwk' => '201817',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
            ['id' => '41', 'yearwk' => '201818',	'res_no' => '5',	'wk_hours' => '40',	'status_id' => '1',	'company_id' => '1',	'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
