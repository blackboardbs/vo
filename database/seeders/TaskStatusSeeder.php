<?php

namespace Database\Seeders;

use App\Models\TaskStatus;
use Illuminate\Database\Seeder;

class TaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TaskStatus::truncate();

        $taskStatus = tenancy()->central(fn() => \App\Models\Central\TaskStatus::all());

        TaskStatus::insert($taskStatus->toArray());
    }
}
