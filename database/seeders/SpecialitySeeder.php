<?php

namespace Database\Seeders;

use App\Models\Speciality;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class SpecialitySeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Speciality::truncate();

        $speciality = tenancy()->central(fn() => \App\Models\Central\Speciality::all());

        Speciality::insert($speciality->toArray());
    }
}
