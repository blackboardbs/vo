<?php

namespace Database\Seeders;

use App\Models\UserFavourite;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class UserFavouritesSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        UserFavourite::truncate();

        $userFavourite = tenancy()->central(fn() => \App\Models\Central\UserFavourite::all());

        UserFavourite::insert($userFavourite->toArray());
    }
}
