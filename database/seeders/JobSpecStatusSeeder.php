<?php

namespace Database\Seeders;

use App\Models\JobSpecStatus;
use Illuminate\Database\Seeder;

class JobSpecStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        JobSpecStatus::truncate();

        $jobSpecStatus = tenancy()->central(fn() => \App\Models\Central\JobSpecStatus::all());

        JobSpecStatus::insert($jobSpecStatus->toArray());
    }
}
