<?php

namespace Database\Seeders;

use App\Models\Profession;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ProfessionSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Profession::truncate();

        $profession = tenancy()->central(fn() => \App\Models\Central\Profession::all());

        Profession::insert($profession->toArray());
    }
}
