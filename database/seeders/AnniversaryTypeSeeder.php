<?php

namespace Database\Seeders;

use App\Models\AnniversaryType;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class AnniversaryTypeSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AnniversaryType::truncate();

        $anniversay = tenancy()->central(fn() => \App\Models\Central\AnniversaryType::all());

        AnniversaryType::insert($anniversay->toArray());
    }
}
