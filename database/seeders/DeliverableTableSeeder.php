<?php

namespace Database\Seeders;

use App\Models\Deliverable;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class DeliverableTableSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Deliverable::truncate();

        $deliverable = tenancy()->central(fn() => \App\Models\Central\Deliverable::all());

        Deliverable::insert($deliverable->toArray());
    }
}
