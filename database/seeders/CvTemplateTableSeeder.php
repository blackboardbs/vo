<?php

namespace Database\Seeders;

use App\Models\CvTemplate;
use Illuminate\Database\Seeder;

class CvTemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CvTemplate::truncate();

        $cvTemplate = tenancy()->central(fn() => \App\Models\Central\CvTemplate::all());

        CvTemplate::insert($cvTemplate->toArray());
    }
}
