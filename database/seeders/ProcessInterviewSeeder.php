<?php

namespace Database\Seeders;

use App\Models\ProcessInterview;
use Illuminate\Database\Seeder;

class ProcessInterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProcessInterview::truncate();

        $processInterview = tenancy()->central(fn() => \App\Models\Central\ProcessInterview::all());

        ProcessInterview::insert($processInterview->toArray());
    }
}
