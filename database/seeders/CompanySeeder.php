<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;


class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/company.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record):

            $company = new Company;
            $company->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : NULL;
            $company->parent_company_id = isset($record['parent_company_id']) && $record['parent_company_id'] != '' && $record['parent_company_id'] != 'NULL' ? $record['parent_company_id'] : NULL;
            $company->company_name = isset($record['company_name']) && $record['company_name'] != '' && $record['company_name'] != 'NULL' ? $record['company_name'] : NULL;
            $company->business_reg_no = isset($record['business_reg_no']) && $record['business_reg_no'] != '' && $record['business_reg_no'] != 'NULL' ? $record['business_reg_no'] : NULL;
            $company->vat_no = isset($record['vat_no']) && $record['vat_no'] != '' && $record['vat_no'] != 'NULL' ? $record['vat_no'] : NULL;
            $company->contact_firstname = isset($record['contact_firstname']) && $record['contact_firstname'] != '' && $record['contact_firstname'] != 'NULL' ? $record['contact_firstname'] : NULL;
            $company->contact_lastname = isset($record['contact_lastname']) && $record['contact_lastname'] != '' && $record['contact_lastname'] != 'NULL' ? $record['contact_lastname'] : NULL;
            $company->phone = isset($record['phone']) && $record['phone'] != '' && $record['phone'] != 'NULL' ? $record['phone'] : NULL;
            $company->cell = isset($record['cell']) && $record['cell'] != '' && $record['cell'] != 'NULL' ? $record['cell'] : NULL;
            $company->fax = isset($record['fax']) && $record['fax'] != '' && $record['fax'] != 'NULL' ? $record['fax'] : NULL;
            $company->email = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? $record['email'] : NULL;
            $company->web = isset($record['web']) && $record['web'] != '' && $record['web'] != 'NULL' ? $record['web'] : NULL;
            $company->postal_address_line1 = isset($record['postal_address_line1']) && $record['postal_address_line1'] != '' && $record['postal_address_line1'] != 'NULL' ? $record['postal_address_line1'] : NULL;
            $company->postal_address_line2 = isset($record['postal_address_line2']) && $record['postal_address_line2'] != '' && $record['postal_address_line2'] != 'NULL' ? $record['postal_address_line2'] : NULL;
            $company->postal_address_line3 = isset($record['postal_address_line3']) && $record['postal_address_line3'] != '' && $record['postal_address_line3'] != 'NULL' ? $record['postal_address_line3'] : NULL;
            $company->city_suburb = isset($record['city_suburb']) && $record['city_suburb'] != '' && $record['city_suburb'] != 'NULL' ? $record['city_suburb'] : NULL;
            $company->state_province = isset($record['state_province']) && $record['state_province'] != '' && $record['state_province'] != 'NULL' ? $record['state_province'] : NULL;
            $company->postal_zipcode = isset($record['postal_zipcode']) && $record['postal_zipcode'] != '' && $record['postal_zipcode'] != 'NULL' ? $record['postal_zipcode'] : NULL;
            $company->country_id = isset($record['country_id']) && $record['country_id'] != '' && $record['country_id'] != 'NULL' ? $record['country_id'] : NULL;
            $company->company_logo = isset($record['company_logo']) && $record['company_logo'] != '' && $record['company_logo'] != 'NULL' ? $record['company_logo'] : NULL;
            $company->bbbee_level_id = isset($record['bbbee_level_id']) && $record['bbbee_level_id'] != '' && $record['bbbee_level_id'] != 'NULL' ? $record['bbbee_level_id'] : NULL;
            $company->bbbee_certificate_expiry = isset($record['bbbee_certificate_expiry']) && $record['bbbee_certificate_expiry'] != '' && $record['bbbee_certificate_expiry'] != 'NULL' ? $record['bbbee_certificate_expiry'] : NULL;
            $company->black_ownership = isset($record['black_ownership']) && $record['black_ownership'] != '' && $record['black_ownership'] != 'NULL' ? $record['black_ownership'] : NULL;
            $company->black_female_ownership = isset($record['black_female_ownership']) && $record['black_female_ownership'] != '' && $record['black_female_ownership'] != 'NULL' ? $record['black_female_ownership'] : NULL;
            $company->black_voting_rights = isset($record['black_voting_rights']) && $record['black_voting_rights'] != '' && $record['black_voting_rights'] != 'NULL' ? $record['black_voting_rights'] : NULL;
            $company->black_female_voting_rights = isset($record['black_female_voting_rights']) && $record['black_female_voting_rights'] != '' && $record['black_female_voting_rights'] != 'NULL' ? $record['black_female_voting_rights'] : NULL;
            $company->bbbee_recognition_level = isset($record['bbbee_recognition_level']) && $record['bbbee_recognition_level'] != '' && $record['bbbee_recognition_level'] != 'NULL' ? $record['bbbee_recognition_level'] : NULL;
            $company->bank_name = isset($record['bank_name']) && $record['bank_name'] != '' && $record['bank_name'] != 'NULL' ? $record['bank_name'] : NULL;
            $company->branch_code = isset($record['branch_code']) && $record['branch_code'] != '' && $record['branch_code'] != 'NULL' ? $record['branch_code'] : NULL;
            $company->bank_acc_no = isset($record['bank_acc_no']) && $record['bank_acc_no'] != '' && $record['bank_acc_no'] != 'NULL' ? $record['bank_acc_no'] : NULL;
            $company->status_id = isset($record['status_id']) && $record['status_id'] != '' && $record['status_id'] != 'NULL' ? $record['status_id'] : NULL;
            $company->creator_id = isset($record['creator_id']) && $record['creator_id'] != '' && $record['creator_id'] != 'NULL' ? $record['creator_id'] : NULL;
            //$company->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$company->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $company->save();
        endforeach;
        /*Company::insert([
            ['id' => '1', 'parent_company_id' => '0', 'company_name' => 'Queen Consolidated (Pty) Ltd', 'business_reg_no' => '2000/111999/09', 'vat_no' => '45012345678', 'contact_firstname' => 'Oliver', 'contact_lastname' => 'Queen', 'phone' => '+2755 555 5555', 'cell' => '+2755 555 5555', 'fax' => '+2755 555 5555', 'email' => 'wo@blackboardbi.com', 'web' => 'https://www.blackboardbi.com', 'postal_address_line1' => 'PO Box 55', 'postal_address_line2' => 'City Centre', 'postal_address_line3' => '', 'city_suburb' => 'Star City', 'state_province' => 'Starling', 'postal_zipcode' => '00001', 'country_id' => '1', 'company_logo' => 'default.jpg', 'bbbee_level_id' => '1', 'bbbee_certificate_expiry' => '1999-01-01', 'black_ownership' => '0.00', 'black_female_ownership' => '0.00', 'black_voting_rights' => '0.00', 'black_female_voting_rights' => '0.00', 'bbbee_recognition_level' => '0.00', 'bank_name' => 'Star City Bank (Cheque Account)', 'branch_code' => '999 999', 'bank_acc_no' => '987654321', 'status_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '2', 'parent_company_id' => '0', 'company_name' => 'Star Labs (Pty) Ltd', 'business_reg_no' => '2001/0012345/01', 'vat_no' => 'N/A', 'contact_firstname' => 'Harrison', 'contact_lastname' => 'Walsh', 'phone' => '+2755 555 555', 'cell' => '+2755 555 555', 'fax' => '+2755 555 555', 'email' => 'wo@blackboardbi.com', 'web' => 'https://www.blackboardbi.com', 'postal_address_line1' => 'PO Box 99', 'postal_address_line2' => 'City', 'postal_address_line3' => '', 'city_suburb' => 'Central City', 'state_province' => 'Central', 'postal_zipcode' => '0002', 'country_id' => '1', 'company_logo' => 'default.jpg', 'bbbee_level_id' => '1', 'bbbee_certificate_expiry' => '1999-01-01', 'black_ownership' => '0.00', 'black_female_ownership' => '0.00', 'black_voting_rights' => '0.00', 'black_female_voting_rights' => '0.00', 'bbbee_recognition_level' => '0.00', 'bank_name' => 'Central National Bank (Cheque Account)', 'branch_code' => '999 555', 'bank_acc_no' => '4321987654', 'status_id' => '1', 'creator_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
        ]);*/
    }
}
