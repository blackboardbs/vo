<?php

namespace Database\Seeders;

use App\Models\BBBEERace;
use Illuminate\Database\Seeder;

class BBBEERaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BBBEERace::truncate();

        $bbbeeRace = tenancy()->central(fn() => \App\Models\Central\BBBEERace::all());

        BBBEERace::insert($bbbeeRace->toArray());
    }
}
