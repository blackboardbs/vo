<?php

namespace Database\Seeders;

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PaymentType::truncate();

        $paymentType = tenancy()->central(fn() => \App\Models\Central\PaymentType::all());

        PaymentType::insert($paymentType->toArray());
    }
}
