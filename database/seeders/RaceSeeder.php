<?php

namespace Database\Seeders;

use App\Models\Race;
use Illuminate\Database\Seeder;

class RaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Race::truncate();

        $race = tenancy()->central(fn() => \App\Models\Central\Race::all());

        Race::insert($race->toArray());
    }
}
