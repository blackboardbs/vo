<?php

namespace Database\Seeders;

use App\Models\PlannedExpense;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class PlanExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/plan_exp.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $plan_exp = new PlannedExpense;
            $plan_exp->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $plan_exp->description = isset($record['description']) && $record['description'] != '' && $record['description'] != 'NULL' ? $record['description'] : null;
            $plan_exp->plan_exp_value = isset($record['plan_exp_value']) && $record['plan_exp_value'] != '' && $record['plan_exp_value'] != 'NULL' ? $record['plan_exp_value'] : null;
            $plan_exp->plan_exp_date = isset($record['plan_exp_date']) && $record['plan_exp_date'] != '' && $record['plan_exp_date'] != 'NULL' ? $record['plan_exp_date'] : null;
            $plan_exp->status = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : null;
            $plan_exp->account_id = isset($record['account_id']) && $record['account_id'] != '' && $record['account_id'] != 'NULL' ? $record['account_id'] : null;
            $plan_exp->account_element_id = isset($record['account_element_id']) && $record['account_element_id'] != '' && $record['account_element_id'] != 'NULL' ? $record['account_element_id'] : null;
            $plan_exp->company_id = isset($record['company_id']) && $record['company_id'] != '' && $record['company_id'] != 'NULL' ? $record['company_id'] : null;
            //$leave->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$leave->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $plan_exp->save();
        }
    }
}
