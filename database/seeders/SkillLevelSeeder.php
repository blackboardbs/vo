<?php

namespace Database\Seeders;

use App\Models\SkillLevel;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class SkillLevelSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        SkillLevel::truncate();

        $skillLevel = tenancy()->central(fn() => \App\Models\Central\SkillLevel::all());

        SkillLevel::insert($this->readCsv('skill_level.csv'));
    }
}
