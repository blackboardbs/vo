<?php

namespace Database\Seeders;

use App\Models\Template;
use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Template::truncate();

        $template = tenancy()->central(fn() => \App\Models\Central\Template::all());

        Template::insert($template->toArray());
    }
}
