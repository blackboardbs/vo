<?php

namespace Database\Seeders;

use App\Models\Suburb;
use Illuminate\Database\Seeder;

class SuburbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Suburb::truncate();

        $suburb = tenancy()->central(fn() => \App\Models\Central\Suburb::all());

        Suburb::insert($suburb->toArray());
    }
}
