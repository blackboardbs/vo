<?php

namespace Database\Seeders;

use App\Models\Status;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Status::truncate();

        $status = tenancy()->central(fn() => \App\Models\Central\Status::all());

        Status::insert($status->toArray());

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
