<?php

namespace Database\Seeders;

use App\Models\Anniversary;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class AnniversarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $csv = Reader::createFromPath(database_path('/data/anniversary.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $anniversary = new Anniversary;
            $anniversary->id = isset($record['id']) && $record['id'] != '' && $record['id'] != 'NULL' ? $record['id'] : null;
            $anniversary->anniversary = isset($record['anniversary']) && $record['anniversary'] != '' && $record['anniversary'] != 'NULL' ? $record['anniversary'] : null;
            $anniversary->emp_id = isset($record['emp_id']) && $record['emp_id'] != '' && $record['emp_id'] != 'NULL' ? $record['emp_id'] : null;
            $anniversary->name = isset($record['name']) && $record['name'] != '' && $record['name'] != 'NULL' ? $record['name'] : null;
            $anniversary->email = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? $record['email'] : null;
            $anniversary->phone = isset($record['phone']) && $record['phone'] != '' && $record['phone'] != 'NULL' ? $record['phone'] : null;
            $anniversary->contact_email = isset($record['contact_email']) && $record['contact_email'] != '' && $record['contact_email'] != 'NULL' ? $record['contact_email'] : null;
            $anniversary->contact_phone = isset($record['contact_phone']) && $record['contact_phone'] != '' && $record['contact_phone'] != 'NULL' ? $record['contact_phone'] : null;
            $anniversary->relationship = isset($record['relationship']) && $record['relationship'] != '' && $record['relationship'] != 'NULL' ? $record['relationship'] : null;
            $anniversary->anniversary_date = isset($record['anniversary_date']) && $record['anniversary_date'] != '' && $record['anniversary_date'] != 'NULL' ? $record['anniversary_date'] : null;
            $anniversary->status_id = isset($record['status_id']) && $record['status_id'] != '' && $record['status_id'] != 'NULL' ? $record['status_id'] : null;
            //$anniversary->created_at = isset($record['created_at']) && $record['created_at'] != '' && $record['created_at'] != 'NULL' ? $record['created_at'] : NULL;
            //$anniversary->updated_at = isset($record['updated_at']) && $record['updated_at'] != '' && $record['updated_at'] != 'NULL' ? $record['updated_at'] : NULL;
            $anniversary->save();
        }
        /*Anniversary::insert([
            ['id' => '1', 'anniversary' => 'Birthday', 'emp_id' => '3', 'name' => 'Paul', 'email' => 'paul@blackboardbi.com', 'phone' => '5559995555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '1971-09-17', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '5', 'anniversary' => 'Birthday', 'emp_id' => '1', 'name' => 'Oliver Queen', 'email' => 'arrow@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '2', 'relationship' => 'Self', 'anniversary_date' => '1972-06-13', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '8', 'anniversary' => 'Birthday', 'emp_id' => '1', 'name' => 'Thea Queen', 'email' => 'redarrow@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '2', 'relationship' => 'Self', 'anniversary_date' => '1975-10-30', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '17', 'anniversary' => 'Birthday', 'emp_id' => '6', 'name' => 'Hulk', 'email' => 'hulk@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '1981-09-04', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '18', 'anniversary' => 'Birthday', 'emp_id' => '8', 'name' => 'Spiderman', 'email' => 'spiderman@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '2', 'relationship' => 'Self', 'anniversary_date' => '1989-09-17', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '22', 'anniversary' => 'Birthday', 'emp_id' => '9', 'name' => 'Harrison Walsh', 'email' => 'walsh@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '1980-09-21', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '23', 'anniversary' => 'Birthday', 'emp_id' => '1', 'name' => 'Kevin Clark', 'email' => 'superman@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '1995-12-27', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '24', 'anniversary' => 'Birthday', 'emp_id' => '13', 'name' => 'Black Widdow', 'email' => 'widdow@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '2017-11-26', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '25', 'anniversary' => 'Birthday', 'emp_id' => '1', 'name' => 'Green Lantern', 'email' => 'green@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '1973-05-11', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
            ['id' => '26', 'anniversary' => 'Birthday', 'emp_id' => '15', 'name' => 'Spider Woman', 'email' => 'spiderwoman@blackboardbi.com', 'phone' => '+2755 555 5555', 'contact_email' => '1', 'contact_phone' => '1', 'relationship' => 'Self', 'anniversary_date' => '2018-02-26', 'status_id' => '1', 'created_at' => '2018-08-15 07:13:54', 'updated_at' => '2018-08-15 07:13:54'],
        ]);*/
    }
}
