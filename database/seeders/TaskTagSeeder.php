<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TaskTag;

class TaskTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tag = new TaskTag();
        $tag->id = 1;
        $tag->name = 'Frown';
        $tag->description = 'Frown';
        $tag->icon = '<i class="fas fa-frown fa-lg" style="color: red;"></i>';
        $tag->color = 'red';
        $tag->status = 1;
        $tag->save();

        $tag = new TaskTag();
        $tag->id = 2;
        $tag->name = 'Exclamation';
        $tag->description = 'Exclamation';
        $tag->icon = '<i class="fas fa-exclamation fa-lg" style="color: orange;"></i>';
        $tag->color = 'orange';
        $tag->status = 1;
        $tag->save();

        $tag = new TaskTag();
        $tag->id = 3;
        $tag->name = 'Smile';
        $tag->description = 'Smile';
        $tag->icon = '<i class="fas fa-smile fa-lg" style="color: green;"></i>';
        $tag->color = 'green';
        $tag->status = 1;
        $tag->save();

        $tag = new TaskTag();
        $tag->id = 4;
        $tag->name = 'Test';
        $tag->description = 'Test';
        $tag->icon = '<i class="fas fa-user-check fa-lg" style="color: purple;"></i>';
        $tag->color = 'purple';
        $tag->status = 1;
        $tag->save();

        $tag = new TaskTag();
        $tag->id = 5;
        $tag->name = 'Deploy';
        $tag->description = 'Deploy';
        $tag->icon = '<i class="fas fa-upload fa-lg" style="color: blue;"></i>';
        $tag->color = 'blue';
        $tag->status = 1;
        $tag->save();

        $tag = new TaskTag();
        $tag->id = 6;
        $tag->name = 'Bug';
        $tag->description = 'Bug';
        $tag->icon = '<i class="fas fa-bug fa-lg" style="color: red;"></i>';
        $tag->color = 'red';
        $tag->status = 1;
        $tag->save();
    }
}
