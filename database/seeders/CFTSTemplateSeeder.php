<?php

namespace Database\Seeders;

use App\Models\CFTSTemplate;
use Illuminate\Database\Seeder;

class CFTSTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CFTSTemplate::truncate();

        $cftsTemplate = tenancy()->central(fn() => \App\Models\Central\CFTSTemplate::all());

        CFTSTemplate::insert($cftsTemplate->toArray());
    }
}
