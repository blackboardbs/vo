<?php

namespace Database\Seeders;

use App\Models\ResourceType;
use Illuminate\Database\Seeder;

class ResourceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ResourceType::truncate();

        $resource = tenancy()->central(fn() => \App\Models\Central\ResourceType::all());

        ResourceType::insert($resource->toArray());
    }
}
