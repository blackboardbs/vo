<?php

namespace Database\Seeders;

use App\Models\Project;
//use Silber\Bouncer\Bouncer;
use Illuminate\Database\Seeder;

class BouncerRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Bouncer::tables([
            'roles' => 'bouncer_roles',
            'permissions' => 'bouncer_permissions',
        ]);

        //Admin has access to every module of the system
        Bouncer::allow('admin')->everything();

        //Project Roles and abilities
        Bouncer::allow('admin_manager')->toManage(Project::class);
        Bouncer::allow('manager')->to('view_team', Project::class);
        Bouncer::allow('manager')->to('show_team', Project::class);

        //Assignment Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\Assignment::class);
        Bouncer::allow('manager')->to('view_team', \App\Models\Assignment::class);
        Bouncer::allow('manager')->to('show_team', \App\Models\Assignment::class);
        Bouncer::allow('consultant')->to('view_team', \App\Models\Assignment::class);
        Bouncer::allow('consultant')->to('show_team', \App\Models\Assignment::class);
        Bouncer::allow('contractor')->to('view_team', \App\Models\Assignment::class);
        Bouncer::allow('contractor')->to('show_team', \App\Models\Assignment::class);
        Bouncer::allow('cv_resource')->to('view_team', \App\Models\Assignment::class);
        Bouncer::allow('cv_resource')->to('show_team', \App\Models\Assignment::class);
        Bouncer::allow('customer_client')->to('view_team', \App\Models\Assignment::class);
        Bouncer::allow('customer_client')->to('show_team', \App\Models\Assignment::class);

        //Resource Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\Resource::class);
        Bouncer::allow('manager')->to('view', \App\Models\Resource::class);
        Bouncer::allow('manager')->to('show', \App\Models\Resource::class);
        Bouncer::allow('consultant')->to('view_team', \App\Models\Resource::class);
        Bouncer::allow('contractor')->to('view_team', \App\Models\Resource::class);
        Bouncer::allow('consultant')->to('show_team', \App\Models\Resource::class);
        Bouncer::allow('contractor')->to('show_team', \App\Models\Resource::class);

        //Timesheet Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\Timesheet::class);
        Bouncer::allow('manager')->to('view_team', \App\Models\Timesheet::class);
        Bouncer::allow('manager')->to('show_team', \App\Models\Timesheet::class);
        Bouncer::allow('manager')->to('update_team', \App\Models\Timesheet::class);
        Bouncer::allow('consultant')->to('view_team', \App\Models\Timesheet::class);
        Bouncer::allow('consultant')->to('show_team', \App\Models\Timesheet::class);
        Bouncer::allow('consultant')->to('update_team', \App\Models\Timesheet::class);
        Bouncer::allow('contractor')->to('view_team', \App\Models\Timesheet::class);
        Bouncer::allow('contractor')->to('show_team', \App\Models\Timesheet::class);
        Bouncer::allow('contractor')->to('update_team', \App\Models\Timesheet::class);
        Bouncer::allow('vendor')->to('view_team', \App\Models\Timesheet::class);
        Bouncer::allow('vendor')->to('show_team', \App\Models\Timesheet::class);
        Bouncer::allow('customer_client')->to('view_team', \App\Models\Timesheet::class);
        Bouncer::allow('customer_client')->to('show_team', \App\Models\Timesheet::class);

        //Leave Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\Leave::class);
        Bouncer::allow('manager')->to('view_team', \App\Models\Leave::class);
        Bouncer::allow('manager')->to('show_team', \App\Models\Leave::class);
        Bouncer::allow('manager')->to('update_team', \App\Models\Leave::class);
        Bouncer::allow('consultant')->to('view_team', \App\Models\Leave::class);
        Bouncer::allow('consultant')->to('show_team', \App\Models\Leave::class);
        Bouncer::allow('consultant')->to('update_team', \App\Models\Leave::class);
        Bouncer::allow('contractor')->to('view_team', \App\Models\Leave::class);
        Bouncer::allow('contractor')->to('show_team', \App\Models\Leave::class);
        Bouncer::allow('contractor')->to('update_team', \App\Models\Leave::class);

        //Leave Statement Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\LeaveStatement::class);
        Bouncer::allow('manager')->to('view_team', \App\Models\LeaveStatement::class);
        Bouncer::allow('manager')->to('show_team', \App\Models\LeaveStatement::class);
        Bouncer::allow('consultant')->to('view_team', \App\Models\LeaveStatement::class);
        Bouncer::allow('consultant')->to('show_team', \App\Models\LeaveStatement::class);
        Bouncer::allow('contractor')->to('view_team', \App\Models\LeaveStatement::class);
        Bouncer::allow('contractor')->to('show_team', \App\Models\LeaveStatement::class);

        //Leave Balance Roles and abilities
        Bouncer::allow('admin_manager')->toManage(\App\Models\LeaveBalance::class);
        Bouncer::allow('manager')->to('view_team', \App\Models\LeaveBalance::class);
        Bouncer::allow('manager')->to('show_team', \App\Models\LeaveBalance::class);
    }
}
