<?php

namespace Database\Seeders;

use App\Models\Module;
use App\Traits\HasCsv;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    use HasCsv;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Module::truncate();

        $module = tenancy()->central(fn() => \App\Models\Central\Module::all());

        Module::insert($module->toArray());
    }
}
