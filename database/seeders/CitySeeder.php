<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        City::truncate();

        $city = tenancy()->central(fn() => \App\Models\Central\City::all());

        City::insert($city->toArray());
    }
}
