<?php

namespace Database\Seeders;

use App\Models\V_TimeLine;
use Illuminate\Database\Seeder;

class V_TimeLineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        V_TimeLine::insert([
            ['id' => '1', 'company_id' => '1', 'time_id' => '2', 'yearwk' => '201735', 'hours' => '30', 'hours_bill' => '30', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '2', 'company_id' => '1', 'time_id' => '3', 'yearwk' => '201736', 'hours' => '12', 'hours_bill' => '12', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '3', 'company_id' => '1', 'time_id' => '4', 'yearwk' => '201737', 'hours' => '34', 'hours_bill' => '34', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '4', 'company_id' => '1', 'time_id' => '5', 'yearwk' => '201738', 'hours' => '39', 'hours_bill' => '39', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '5', 'company_id' => '1', 'time_id' => '6', 'yearwk' => '201739', 'hours' => '38', 'hours_bill' => '38', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '6', 'company_id' => '1', 'time_id' => '7', 'yearwk' => '201740', 'hours' => '24', 'hours_bill' => '24', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '7', 'company_id' => '1', 'time_id' => '8', 'yearwk' => '201741', 'hours' => '0', 'hours_bill' => '0', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '8', 'company_id' => '1', 'time_id' => '9', 'yearwk' => '201742', 'hours' => '13', 'hours_bill' => '13', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '9', 'company_id' => '1', 'time_id' => '10', 'yearwk' => '201734', 'hours' => '40', 'hours_bill' => '40', 'created_at' => now(), 'updated_at' => now()],
            ['id' => '10', 'company_id' => '1', 'time_id' => '11', 'yearwk' => '201733', 'hours' => '29', 'hours_bill' => '29', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
