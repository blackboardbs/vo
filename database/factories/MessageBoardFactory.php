<?php

namespace Database\Factories;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MessageBoard>
 */
class MessageBoardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'message' => $this->faker->paragraph(),
            'message_date' => $this->faker->dateTimeBetween()->format("Y-m-d"),
            'status_id' => Status::factory()->create()->id
        ];
    }
}
