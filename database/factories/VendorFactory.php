<?php

namespace Database\Factories;

use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vendor>
 */
class VendorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::factory()->create();

        return [
            'vendor_name' => $this->faker->company(),
            'business_reg_no' => str(Str::random(12))->upper()->toString(),
            'vat_no' => str(Str::random(12))->upper()->toString(),
            'contact_firstname' => $this->faker->firstName(),
            'contact_lastname' => $this->faker->lastName(),
            'contact_birthday' => $this->faker->dateTimeBetween('-40 years', '-18 years')->format("Y-m-d"),
            'phone' => $this->faker->phoneNumber(),
            'cell' => $this->faker->phoneNumber(),
            'email' => $this->faker->safeEmail(),
            'account_manager' => $user->id,
            'assignment_approver' => $user->id,
            'status_id' => Status::factory()->create()->id,
            'creator_id' => $user->id,
            'invoice_contact_id' => $user->id,
            'payment_terms_days' => 30
        ];
    }
}
