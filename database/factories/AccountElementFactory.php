<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AccountElement>
 */
class AccountElementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'account_id' => Account::factory()->create()->id,
            'description' => $this->faker->words(rand(1,3), true),
            'account_element' => $this->faker->words(rand(1,3), true),
            'status' => Status::factory()->create()->id,
            'creator_id' => User::factory()->create()->id
        ];
    }
}
