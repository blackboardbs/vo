<?php

namespace Database\Factories;

use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Assignment>
 */
class AssignmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'start_date' => now()->format('Y-m-d'),
            'end_date' => now()->addMonths(5)->format('Y-m-d'),
            'assignment_approver' => User::factory()->create()->id,
            'hours' => $this->faker->numberBetween(),
            'note_1' => $this->faker->text(),
            'billable' => (int) $this->faker->boolean(),
            'status' => Status::factory()->create()->id,
        ];
    }
}
