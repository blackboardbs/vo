<?php

namespace Database\Factories;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BillingCycle>
 */
class BillingCycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $description = $this->faker->words(4, true);
        return [
            'name' => $description,
            'description' => $description,
            'max_weeks' => 5,
            'status_id' => Status::factory()->create()->id
        ];
    }
}
