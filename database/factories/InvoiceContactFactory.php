<?php

namespace Database\Factories;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\InvoiceContact>
 */
class InvoiceContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->safeEmail(),
            'contact_number' => $this->faker->phoneNumber(),
            'birthday' => $this->faker->dateTimeBetween('-50 years', '-18 years')->format('Y-m-d'),
            'status_id' => Status::factory()->create()->id,
        ];
    }
}
