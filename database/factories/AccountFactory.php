<?php

namespace Database\Factories;

use App\Models\AccountType;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Account>
 */
class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'account_type_id' => AccountType::factory()->create()->id,
            'description' =>  $this->faker->words(rand(1,4), true),
            'status' => Status::factory()->create()->id,
            'creator_id' => User::factory()->create()->id,
            'account_group' => $this->faker->word()
        ];
    }
}
