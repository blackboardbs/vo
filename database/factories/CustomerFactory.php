<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::factory()->create();

        return [
            'customer_name' => $this->faker->company(),
            'account_manager' => $user->id,
            'contact_firstname' => $this->faker->firstName(),
            'contact_lastname' => $this->faker->lastName(),
            'country_id' => Country::factory()->create(),
            'creator_id' => $user->id,
            'status' => Status::factory()->create()->id,
        ];
    }
}
