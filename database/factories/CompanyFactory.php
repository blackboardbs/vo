<?php

namespace Database\Factories;

use App\Models\BBBEELevel;
use App\Models\Country;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'company_name' => $this->faker->company(),
            'status_id' => Status::factory()->create()->id,
            'email' => $this->faker->safeEmail(),
            'creator_id' => User::factory()->create()->id,
            'country_id' => Country::factory()->create()->id,
            'bbbee_level_id' => BBBEELevel::factory()->create()->id
        ];
    }
}
