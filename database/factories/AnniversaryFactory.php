<?php

namespace Database\Factories;

use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Anniversary>
 */
class AnniversaryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::factory()->create();
        return [
            'anniversary' => $this->faker->word(3, true),
            'emp_id' => $user->id,
            'name' => $user->name(),
            'email' => $user->email,
            'phone' => $user->phone,
            'contact_email' => $this->faker->safeEmail(),
            'contact_phone' => $this->faker->phoneNumber(),
            'relationship' => $this->faker->word(),
            'anniversary_date' => now()->toDateString(),
            'status_id' => Status::factory()->create()->id
        ];
    }
}
