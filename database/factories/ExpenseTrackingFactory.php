<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExpenseTrackingFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'employee_id' => User::factory()->create()->id,
            'transaction_date' => now()->subDays($this->faker->numberBetween(1, 90))->toDateString(),
            'account_id' => Account::factory()->create()->id,
            'account_element_id' => AccountElement::factory()->create()->id,
            'description' => $this->faker->text(80),
            'company_id' => Company::factory()->create()->id,
            'assignment_id' => Assignment::factory()->create()->id,
            'yearwk' => now()->year.now()->subWeeks($this->faker->numberBetween(1, 40))->weekOfYear,
            'claimable' => $this->faker->numberBetween(0, 1),
            'billable' => $this->faker->numberBetween(0, 1),
            'payment_reference' => strtoupper(str_replace(' ', '_', $this->faker->text(10))),
            'document' => $this->faker->imageUrl(640, 480),
            'amount' => $this->faker->numberBetween(100, 20000),
            'status_id' => Status::factory()->create()->id,
            'cost_center' => $this->faker->numberBetween(1, \App\Models\CostCenter::all()->count()),
            'approved_by' => null,
            'approved_on' => null,
            'approval_status' => 1,
            'payment_status' => 1,
        ];
    }
}
