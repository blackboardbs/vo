<?php

namespace Database\Factories;

use App\Enum\Status;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber(),
            'avatar' => \Illuminate\Http\UploadedFile::fake()->image('avatar.jpg', 250,250),
            'resource_id' => $this->faker->numberBetween(1,10),
            'vendor_id' => $this->faker->numberBetween(1,10),
            'customer_id' => $this->faker->numberBetween(1,10),
            'company_id' => $this->faker->numberBetween(1,10),
            'expiry_date' => $this->faker->dateTime(now()->addYears(4)),
            'status_id' => fake()->randomElement(Status::values()),
            'calendar_view' => Str::random(30),
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'active_office_id' => $this->faker->numberBetween(1,10),
            'login_user' => $this->faker->numberBetween(1,10),
            'usertype_id' => $this->faker->numberBetween(1,10),
            'appointment_manager_id' => $this->faker->numberBetween(1,10),
            'remember_token' => Str::random(10),
        ];
    }
}
