<?php

namespace Database\Factories;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Utilization>
 */
class UtilizationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'yearwk' => now()->format("YW"),
            'res_no' => 3,
            'wk_hours' => 40,
            'cost_res_no' => 0,
            'cost_wk_hours' => 0,
            'status_id' => Status::factory()->create()->id,
            'company_id' => 1
        ];
    }
}
