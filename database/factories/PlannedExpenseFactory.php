<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\Company;
use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlannedExpense>
 */
class PlannedExpenseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'description' => $this->faker->words(rand(1,5), true),
            'plan_exp_value' => rand(100, 100000),
            'plan_exp_date' => now()->addMonths(rand(1,6))->toDateString(),
            'status' => Status::factory()->create()->id,
            'account_id' => Account::factory()->create()->id,
            'account_element_id' => AccountElement::factory()->create()->id,
            'company_id' => Company::factory()->create()->id
        ];
    }
}
