<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CvSubmitFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'sent_to_email' => $this->faker->safeEmail(),
            'sent_to_email_cc' => $this->faker->safeEmail().','.$this->faker->safeEmail(),
            'sent_to_email_bcc' => $this->faker->safeEmail().','.$this->faker->safeEmail(),
            'commission' => $this->faker->numberBetween(1, 50),
            'subject' => $this->faker->text(20),
            'cv_id' => $this->faker->numberBetween(1, \App\Models\Cv::all()->count()),
            'job_spec_id' => $this->faker->numberBetween(1, \App\Models\JobSpec::all()->count()),
            'rate' => $this->faker->numberBetween(200, 2000),
            'ctc' => $this->faker->numberBetween(10000, 500000),
            'template' => $this->faker->numberBetween(1, 5),
            'format' => $this->faker->numberBetween(10, 20),
            'text_message' => $this->faker->paragraph(),
            'status_id' => $this->faker->numberBetween(1, 5),
            'creator_id' => $this->faker->numberBetween(1, User::all()->count()),
            'created_at' => now()->subDays($this->faker->numberBetween(0, 60)),
        ];
    }
}
