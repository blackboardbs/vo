<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class JobSpecFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'reference_number' => 'JS'.$this->faker->unique()->randomDigit(),
            'role_id' => $this->faker->numberBetween(1, \App\Models\ScoutingRole::all()->count()),
            'position_description' => $this->faker->paragraph(),
            'profession_id' => $this->faker->numberBetween(1, \App\Models\Profession::all()->count()),
            'speciality_id' => $this->faker->numberBetween(1, \App\Models\Speciality::all()->count()),
            'client' => $this->faker->company(),
            'placement_by' => $this->faker->firstName().' '.$this->faker->lastName(),
        ];
    }
}
