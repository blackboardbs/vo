#!/bin/bash

UserName="gandalf"
Password="a<V({2KvY8?>jd5*"
SourceDatabaseName="tenante85ecbee-f9f9-4591-8b7a-12040c7a667b"
DestinationDatabaseName="tenant7d51380f-64a0-4b1c-a1e9-5ad6b9259afb"
ViewNames=$(mysql -N -e "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA='$SourceDatabaseName'")

rm database/$SourceDatabaseName.sql

echo "Removed old database file."

MYSQLDUMP_CMD="mysqldump --skip-triggers --routines --events --single-transaction"

for VIEW in $ViewNames; do
    MYSQLDUMP_CMD+=" --ignore-table=$SourceDatabaseName.$VIEW"
done

MYSQLDUMP_CMD+=" $SourceDatabaseName > database/${SourceDatabaseName}.sql"

eval $MYSQLDUMP_CMD

#mysqldump -u$UserName -p$Password --skip-triggers $SourceDatabaseName > database/$SourceDatabaseName.sql

chown www-data:www-data database/$SourceDatabaseName.sql

chmod 777 database/$SourceDatabaseName.sql

echo "Dumped new database from Demo App."

mysql -u$UserName -p$Password -e "DROP DATABASE \`$DestinationDatabaseName\`;"

echo "Dropped Tryme database"

mysql -u$UserName -p$Password -e "CREATE DATABASE \`$DestinationDatabaseName\`;"

echo "Tryme Database created"

mysql -u$UserName -p$Password $DestinationDatabaseName < database/$SourceDatabaseName.sql

echo "Database imported"

mysql -u$UserName -p$Password $DestinationDatabaseName < database/views.sql

echo "Views imported successfully"
