<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quotation_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id');
            $table->integer('terms_id');
            $table->string('term');
            $table->integer('row_order');
            $table->integer('freeze')->default('0');
            $table->integer('status');
            $table->integer('creator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quotation_terms');
    }
};
