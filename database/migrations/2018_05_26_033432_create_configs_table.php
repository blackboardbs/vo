<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('days_to_approve_leave')->nullable();
            $table->string('calendar_content')->nullable();
            $table->string('home_page_message')->nullable();
            $table->integer('auditing_records')->nullable();
            $table->string('site_title')->nullable();
            $table->string('site_name')->nullable();
            $table->string('admin_email')->nullable();
            $table->integer('assessment_approver_user_id')->nullable();
            $table->integer('company_id')->default(1)->nullable();
            $table->integer('cost_center_id')->nullable();
            $table->integer('utilization_method')->default('1')->nullable();
            $table->string('utilization_hours_per_week')->default('40')->nullable();
            $table->string('utilization_cost_hours_per_week')->default('40')->nullable();
            $table->integer('project_terms_id')->default('1')->nullable();
            $table->integer('claim_approver_id')->default('0')->nullable();
            $table->integer('vendor_template_id')->default('0')->nullable();
            $table->integer('resource_template_id')->default('0')->nullable();
            $table->integer('site_id')->default('0')->nullable();
            $table->integer('vat_rate_id')->default(1)->nullable();
            $table->integer('timesheet_weeks')->default(12)->nullable();
            $table->integer('timesheet_weeks_future')->default(0)->nullable();
            $table->string('calendar_action_colour')->default('ffffff')->nullable();
            $table->string('calendar_task_colour')->default('ffffff')->nullable();
            $table->string('calendar_leave_colour')->default('ffffff')->nullable();
            $table->string('calendar_assignment_colour')->default('ffffff')->nullable();
            $table->string('calendar_assessment_colour')->default('ffffff')->nullable();
            $table->string('calendar_anniversary_colour')->default('ffffff')->nullable();
            $table->string('calendar_cinvoice_colour')->default('ffffff')->nullable();
            $table->string('calendar_vinvoice_colour')->default('ffffff')->nullable();
            $table->string('calendar_user_colour')->default('ffffff')->nullable();
            $table->string('calendar_medcert_colour')->default('ffffff')->nullable();
            $table->string('calendar_events_colour')->default('ffffff')->nullable();
            $table->integer('annual_leave_days')->default(17);
            $table->string('background_color', 50)->nullable();
            $table->string('font_color', 50)->nullable();
            $table->string('site_logo', 50)->nullable();
            $table->string('active_link', 50)->nullable();
            $table->string('logo_placement', 50)->nullable();
            $table->string('invoice_body_msg')->nullable();
            $table->integer('first_warning_days')->default(30)->nullable();
            $table->integer('second_warning_days')->default(7)->nullable();
            $table->integer('last_warning_days')->default(1)->nullable();
            $table->integer('first_warning_percentage')->default(70)->nullable();
            $table->integer('second_warning_percentage')->default(80)->nullable();
            $table->integer('last_warning_percentage')->default(90)->nullable();
            $table->string('recruitment_default_email')->nullable();
            $table->string('recruitment_default_contact_number')->nullable();
            $table->string('recruitment_default_cv_id')->nullable();
            $table->string('recruitment_default_company_id')->nullable();
            $table->string('default_invoice_template')->nullable();
            $table->string('default_pf_invoice_template')->nullable();
            $table->integer('timesheet_template_id')->nullable();
            $table->string('customer_invoice_prefix')->nullable();
            $table->string('customer_invoice_format')->nullable();
            $table->string('customer_invoice_start_number')->nullable();
            $table->string('customer_pro_forma_invoice_prefix')->nullable();
            $table->string('customer_pro_forma_invoice_format')->nullable();
            $table->string('customer_pro_forma_invoice_start_number')->nullable();
            $table->string('vendor_invoice_prefix')->nullable();
            $table->string('vendor_invoice_format')->nullable();
            $table->string('vendor_invoice_start_number')->nullable();
            $table->unsignedInteger('billing_cycle_id')->nullable();
            $table->integer('cost_min_percent')->nullable();
            $table->string('cost_min_colour')->nullable();
            $table->string('cost_mid_colour')->nullable();
            $table->integer('cost_max_percent')->nullable();
            $table->string('cost_max_colour')->nullable();
            $table->string('absolute_path')->nullable();
            $table->integer('assessment_frequency')->nullable();
            $table->string('assessment_master')->nullable();
            $table->integer('ojs_minimum_qty')->nullable();
            $table->string('ojs_minimum_color')->nullable();
            $table->integer('ojs_target_qty')->nullable();
            $table->string('ojs_target_color')->nullable();
            $table->string('ojs_between_color')->nullable();
            $table->integer('cvsm_minimum_qty')->nullable();
            $table->string('cvsm_minimum_color')->nullable();
            $table->string('cvsm_between_color')->nullable();
            $table->integer('cvsm_target_qty')->nullable();
            $table->string('cvsm_target_color')->nullable();
            $table->integer('cvsw_minimum_qty')->nullable();
            $table->string('cvsw_minimum_color')->nullable();
            $table->string('cvsw_between_color')->nullable();
            $table->integer('cvsw_target_qty')->nullable();
            $table->string('cvsw_target_color')->nullable();
            $table->integer('nsam_minimum_qty')->nullable();
            $table->string('nsam_minimum_color')->nullable();
            $table->string('nsam_between_color')->nullable();
            $table->integer('nsam_target_qty')->nullable();
            $table->string('nsam_target_color')->nullable();
            $table->integer('nsaw_minimum_qty')->nullable();
            $table->string('nsaw_minimum_color')->nullable();
            $table->string('nsaw_between_color')->nullable();
            $table->integer('nsaw_target_qty')->nullable();
            $table->string('nsaw_target_color')->nullable();
            $table->integer('default_cv_template')->nullable();
            $table->text('cv_submission_text')->nullable();
            $table->integer('default_custom_template')->nullable();
            $table->string('currency', 5)->nullable();
            $table->string('invoice_prefix')->nullable();
            $table->integer('invoice_number_length')->nullable();
            $table->unsignedInteger('hr_user_id')->nullable();
            $table->unsignedInteger('hr_approval_manager_id')->nullable();
            $table->string('supply_chain_email')->nullable();
            $table->unsignedInteger('supply_chain_approval_manager_id')->nullable();
            $table->string('customer_email')->nullable();
            $table->unsignedInteger('customer_approval_manager_id')->nullable();
            $table->integer('kanban_nr_of_tasks')->nullable();
            $table->integer('kanban_show_impediment')->nullable();
            $table->integer('kanban_show_hours')->nullable();
            $table->integer('kanban_show_actuals')->nullable();
            $table->integer('kanban_show_timeframe')->nullable();
            $table->integer('kanban_show_completed')->nullable();
            $table->integer('sh_good_min')->nullable();
            $table->integer('sh_good_max')->nullable();
            $table->integer('sh_fair_min')->nullable();
            $table->integer('sh_fair_max')->nullable();
            $table->integer('sh_bad_min')->nullable();
            $table->integer('sh_bad_max')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('configs');
    }
};
