<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('project_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('terms_version')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->text('exp_travel');
            $table->text('exp_parking');
            $table->text('exp_car_rental');
            $table->text('exp_flights');
            $table->text('exp_other');
            $table->text('exp_per_diem');
            $table->text('exp_accommodation');
            $table->text('exp_out_of_town');
            $table->text('exp_toll');
            $table->text('exp_data');
            $table->text('exp_hours_of_work');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('project_terms');
    }
};
