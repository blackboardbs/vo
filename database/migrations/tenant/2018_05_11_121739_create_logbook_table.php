<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logbook', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('time_id')->nullable();
            $table->integer('emp_id')->nullable();
            $table->date('log_date')->nullable();
            $table->string('description')->nullable();
            $table->integer('open_reading')->nullable();
            $table->integer('km')->nullable();
            $table->decimal('fuel_cost',9,2)->nullable();
            $table->decimal('maint_cost',9,2)->nullable();
            $table->integer('status_id')->unsigned();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('logbook');
    }
};
