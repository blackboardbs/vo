<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_stories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('feature_id')->nullable();
            $table->text('details')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->double('hours_planned')->nullable();
            $table->integer('billable')->nullable();
            $table->integer('dependency_id')->nullable();
            $table->integer('sprint_id')->nullable();
            $table->double('confidence_percentage')->nullable();
            $table->double('estimate_cost')->nullable();
            $table->double('estimate_income')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_stories');
    }
};
