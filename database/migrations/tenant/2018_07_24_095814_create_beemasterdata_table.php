<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('beemasterdata', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_type');
            $table->integer('naturalization_id');
            $table->integer('race_id');
            $table->integer('bbbee_level');
            $table->integer('bbbee_race');
            $table->integer('gender');
            $table->integer('disability');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('beemasterdata');
    }
};
