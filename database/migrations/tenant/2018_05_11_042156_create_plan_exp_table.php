<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plan_exp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->decimal('plan_exp_value',13,2)->default('0.00');
            $table->date('plan_exp_date');
            $table->integer('status')->default('1')->unsigned();
            $table->integer('account_id')->unsigned();
            $table->integer('account_element_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
            $table->foreign('status')->references('id')->on('status');
            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plan_exp');
    }
};
