<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->id();
            $table->text('travel')->nullable();
            $table->text('parking')->nullable();
            $table->text('car_rental')->nullable();
            $table->text('flights')->nullable();
            $table->text('other')->nullable();
            $table->text('accommodation')->nullable();
            $table->text('out_of_town')->nullable();
            $table->text('per_diem')->nullable();
            $table->text('toll')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('expense');
    }
};
