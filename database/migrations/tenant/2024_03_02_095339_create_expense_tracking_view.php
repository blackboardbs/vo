<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `expense_tracking_view`  AS SELECT `U`.`resource_id` AS `resource_id`, concat(`U`.`first_name`,' ',`U`.`last_name`) AS `resource`, `C`.`id` AS `company_id`, `C`.`company_name` AS `company_name`, `A`.`id` AS `account_id`, `A`.`description` AS `account_name`, `AE`.`id` AS `account_element_id`, `AE`.`description` AS `account_element_name`, `ET`.`amount` AS `amount`, `ET`.`claimable` AS `claimable`, `ET`.`billable` AS `billable`, `ET`.`approved_by` AS `approved_by`, `ET`.`payment_reference` AS `payment_reference`, `ET`.`payment_status` AS `payment_status`, `ET`.`approval_status` AS `approval_status`, `ET`.`assignment_id` AS `assignment_id`, yearweek(`ET`.`transaction_date`,0) AS `yearwk`, `ET`.`approved_on` AS `approved_on`, `ET`.`payment_date` AS `payment_date` FROM ((((`exp_tracking` `ET` left join `users` `U` on((`U`.`resource_id` = `ET`.`employee_id`))) left join `account` `A` on((`ET`.`account_id` = `A`.`id`))) left join `account_element` `AE` on((`ET`.`account_element_id` = `AE`.`id`))) left join `company` `C` on((`C`.`id` = `ET`.`company_id`))) ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("DROP VIEW IF EXISTS `expense_tracking_view`");
    }
};
