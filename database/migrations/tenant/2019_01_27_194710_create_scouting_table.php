<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scouting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resource_first_name')->nullable();
            $table->string('resource_last_name')->nullable();
            $table->integer('role_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('process_status_id')->nullable();
            $table->integer('interview_status_id')->nullable();
            $table->string('document_link')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('technical_rating_id')->nullable();
            $table->integer('business_rating_id')->nullable();
            $table->integer('resource_level_id')->nullable();
            $table->integer('assessed_by')->nullable();
            $table->double('estimated_hourly_rate')->nullable();
            $table->string('availability')->nullable();
            $table->integer('referral')->nullable();
            $table->boolean('is_permanent')->nullable();
            $table->boolean('is_contract')->nullable();
            $table->boolean('is_temporary')->nullable();
            $table->integer('profession_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->double('hourly_rate_minimum')->nullable();
            $table->double('hourly_rate_maximum')->nullable();
            $table->double('monthly_salary_minimum')->nullable();
            $table->double('monthly_salary_maximum')->nullable();
            $table->string('skills')->nullable();
            $table->string('reference_code', 60)->nullable();
            $table->text('note')->nullable();
            $table->datetime('convertion_date')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scouting');
    }
};
