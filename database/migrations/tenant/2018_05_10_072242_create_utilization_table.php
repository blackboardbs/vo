<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('utilization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('yearwk');
            $table->integer('res_no');
            $table->integer('wk_hours');
            $table->integer('cost_res_no')->nullable();
            $table->integer('cost_wk_hours')->nullable();
            $table->integer('status');
            $table->integer('company_id')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('utilization');
    }
};
