<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prospect', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prospect_name')->nullable();
            $table->string('solution')->nullable();
            $table->string('scope')->nullable();
            $table->integer('hours')->nullable();
            $table->integer('value')->nullable();
            $table->string('resources')->nullable();
            $table->date('decision_date')->nullable();
            $table->date('converted_date')->nullable();
            $table->date('est_start_date')->nullable();
            $table->integer('chance')->nullable();
            $table->string('partners')->nullable();
            $table->text('note')->nullable();
            $table->string('contact')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('account_manager_id')->nullable();
            $table->integer('duration')->nullable();
            $table->string('margin')->nullable();
            $table->integer('industry_id')->nullable();
            $table->string('file')->nullable();
            $table->integer('lead_from')->nullable();
            $table->integer('emote_id')->nullable();
            $table->date('cancelled_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prospect');
    }
};
