<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assessment_measure_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment_id');
            $table->integer('assessment_master_id');
            $table->integer('assessment_master_detail_id');
            $table->string('assessment_group')->nullable();
            $table->integer('score');
            $table->mediumText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assessment_measure_scores');
    }
};
