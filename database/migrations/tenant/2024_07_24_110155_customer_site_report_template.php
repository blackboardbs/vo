<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_site_report_templates', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('review_week_from')->nullable();
            $table->integer('review_week_to')->nullable();
            $table->integer('plan_week_from')->nullable();
            $table->integer('plan_week_to')->nullable();
            $table->date('review_date_from')->nullable();
            $table->date('review_date_to')->nullable();
            $table->date('plan_date_from')->nullable();
            $table->date('plan_date_to')->nullable();
            $table->integer('include_consultant')->nullable();
            $table->integer('include_task_hours')->nullable();
            $table->integer('include_summary_hours')->nullable();
            $table->integer('include_consultant_name')->nullable();
            $table->integer('include_report_to')->nullable();
            $table->integer('include_risks')->nullable();
            $table->integer('include_customer_logo')->nullable();
            $table->integer('include_company_logo')->nullable();
            $table->string('assigned_to_name')->nullable();
            $table->string('site_report_title')->nullable();
            $table->text('site_report_footer')->nullable();
            $table->string('report_emailed_at')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
