<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendor_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_invoice_number')->nullable();
            $table->unsignedInteger('vendor_id')->nullable();
            $table->unsignedInteger('company_id')->nullable();
            $table->string('vendor_invoice_ref')->nullable();
            $table->date('vendor_invoice_date')->nullable();
            $table->date('vendor_due_date')->nullable();
            $table->date('vendor_paid_date')->nullable();
            $table->date('vendor_unallocate_date')->nullable();
            $table->decimal('vendor_invoice_value', 10)->nullable();
            $table->integer('vendor_invoice_status')->nullable();
            $table->mediumText('vendor_invoice_note')->nullable();
            $table->string('vendor_reference')->nullable();
            $table->unsignedInteger('project_id')->nullable();
            $table->unsignedInteger('resource_id')->nullable();
            $table->string('currency', 5)->nullable();
            $table->unsignedInteger('conversion_rate')->nullable();
            $table->unsignedInteger('vat_rate_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_invoice');
    }
};
