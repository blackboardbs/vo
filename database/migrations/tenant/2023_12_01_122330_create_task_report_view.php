<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `task_report_view`  AS SELECT `TRV`.`task_id` AS `task_id`, `TSK`.`description` AS `task_description`, `TSK`.`start_date` AS `task_start_date`, `TSK`.`end_date` AS `task_end_date`, `TSK`.`hours_planned` AS `hours_planned`, `TSK`.`billable` AS `task_billable`, (SELECT `TS`.`description` FROM `task_status` `TS` WHERE (`TS`.`id` = `TSK`.`status`)) AS `task_status`, (SELECT `TDT`.`name` FROM `task_delivery_types` `TDT` WHERE (`TDT`.`id` = `TSK`.`task_delivery_type_id`)) AS `task_delivery_type`, `TRV`.`consultant_first_name` AS `consultant_first_name`, `TRV`.`consultant_last_name` AS `consultant_last_name`, `TRV`.`employee_id` AS `employee_id`, `TRV`.`company_id` AS `company_id`, `TRV`.`company_name` AS `company_name`, `TRV`.`project_id` AS `project_id`, `TRV`.`project_name` AS `project_name`, `TRV`.`customer_id` AS `customer_id`, `TRV`.`customer_name` AS `customer_name`, `TRV`.`total_hours` AS `total_hours`, `TRV`.`team_id` AS `team_id`, `TRV`.`team_name` AS `team_name`, `TRV`.`team_manager_id` AS `team_manager_id`, `TRV`.`is_billable` AS `is_billable`, `TRV`.`invoice_status_id` AS `invoice_status_id`, `TRV`.`invoice_status` AS `invoice_status`, `TRV`.`year_month` AS `year_month`, `TRV`.`cost_center` AS `cost_center`, `TRV`.`cost_center_id` AS `cost_center_id`, `TRV`.`year_week` AS `year_week`, `F`.`id` AS `feature_id`, `F`.`name` AS `feature_name`, `E`.`id` AS `epic_id`, `E`.`name` AS `epic_name`, `US`.`id` AS `user_story_id`, `US`.`name` AS `user_story_name` FROM ((((`timesheet_report_view` `TRV` LEFT JOIN `epics` `E` ON((`E`.`project_id` = `TRV`.`project_id`))) LEFT JOIN `features` `F` ON((`F`.`epic_id` = `E`.`id`))) LEFT JOIN `user_stories` `US` ON((`US`.`feature_id` = `F`.`id`))) LEFT JOIN `tasks` `TSK` ON((`TSK`.`id` = `TRV`.`task_id`)))");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('DROP VIEW IF EXISTS task_report_view');
    }
};
