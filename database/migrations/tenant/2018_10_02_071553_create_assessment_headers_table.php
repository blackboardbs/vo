<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assessment_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_id');
            $table->date('assessment_date');
            $table->date('next_assessment');
            $table->date('assessment_period_start');
            $table->date('assessment_period_end');
            $table->integer('customer_id');
            $table->integer('assessed_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assessment_headers');
    }
};
