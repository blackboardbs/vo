<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('timesheet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('year_week')->nullable();
            $table->unsignedInteger('billing_period_id')->nullable();
            $table->date('date')->nullable();
            $table->integer('year')->nullable();
            $table->integer('month')->nullable();
            $table->integer('week')->nullable();
            $table->date('first_day_of_week')->nullable();
            $table->date('last_day_of_week')->nullable();
            $table->date('first_day_of_month')->nullable();
            $table->date('last_day_of_month')->nullable();
            $table->integer('first_date_of_week')->nullable();
            $table->integer('last_date_of_week')->nullable();
            $table->integer('first_date_of_month')->nullable();
            $table->integer('last_date_of_month')->nullable();
            $table->integer('day')->nullable();
            $table->string('day_name')->nullable();
            $table->integer('hours')->nullable();
            $table->integer('mileage')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('invoice_note')->nullable();
            $table->integer('cf_timesheet_id')->nullable();
            $table->date('invoice_date')->nullable();
            $table->date('invoice_due_date')->nullable();
            $table->date('invoice_paid_date')->nullable();
            $table->string('inv_ref')->nullable();
            $table->integer('timeline_id')->nullable();
            $table->integer('is_billable')->nullable();
            $table->string('cust_inv_ref')->nullable();
            $table->string('vendor_invoice_number')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('system_id')->nullable();
            $table->integer('status_id')->default(1)->nullable();
            $table->integer('timesheet_lock')->nullable()->nullable();
            $table->integer('vendor_invoice_status')->nullable();
            $table->date('vendor_invoice_date')->nullable();
            $table->date('vendor_due_date')->nullable();
            $table->date('vendor_paid_date')->nullable();
            $table->date('vendor_unallocate_date')->nullable();
            $table->string('vendor_invoice_ref')->nullable();
            $table->string('vendor_reference')->nullable();
            $table->integer('bill_status')->nullable()->default(1);
            $table->integer('exp_paid')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('timesheet');
    }
};
