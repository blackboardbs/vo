<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assessment_measures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment_detail_id');
            $table->integer('assessment_master_id');
            $table->integer('assessment_measure_masters_id');
            $table->string('assessment_group');
            $table->integer('score');
            $table->text('notes');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('assessment_measures');
    }
};
