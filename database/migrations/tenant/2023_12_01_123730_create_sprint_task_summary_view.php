<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sprint_task_summary_view`  AS SELECT `st`.`SprintId` AS `SprintId`, `st`.`Sprint` AS `Sprint`, `st`.`Goal` AS `Goal`, `st`.`Resource_ID` AS `Resource_ID`, CONCAT(`u`.`first_name`,' ',`u`.`last_name`) AS `ResourceName`, `st`.`No_of_Task` AS `No_of_Task`, `st`.`Done` AS `Done`, `st`.`Backlog` AS `Backlog`, `st`.`Planned` AS `Planned`, `st`.`InProgress` AS `InProgress`, `st`.`Completed` AS `Completed`, `st`.`TasksOutstanding` AS `TasksOutstanding`, `st`.`OverdueTasks` AS `OverdueTasks`, `st`.`Impediments` AS `Impediments`, `st`.`HoursPlanned` AS `HoursPlanned`, `st`.`ActualHours` AS `ActualHours` FROM ((SELECT `a`.`SprintId` AS `SprintId`,`a`.`Sprint` AS `Sprint`,`a`.`Goal` AS `Goal`,`a`.`Resource_ID` AS `Resource_ID`,SUM(`a`.`No_of_Task`) AS `No_of_Task`,SUM(`a`.`Done`) AS `Done`,SUM(`a`.`Backlog`) AS `Backlog`,SUM(`a`.`Planned`) AS `Planned`,SUM(`a`.`InProgress`) AS `InProgress`,SUM(`a`.`Completed`) AS `Completed`,SUM(`a`.`TasksOutstanding`) AS `TasksOutstanding`,SUM(`a`.`OverdueTasks`) AS `OverdueTasks`,SUM(`a`.`Impediments`) AS `Impediments`,SUM(`a`.`HoursPlanned`) AS `HoursPlanned`,SUM(`a`.`ActualHours`) AS `ActualHours` FROM (SELECT `s`.`id` AS `SprintId`,`s`.`name` AS `Sprint`,' ' AS `Goal`,`sr`.`employee_id` AS `Resource_ID`,0 AS `No_of_Task`,0 AS `Done`,0 AS `Backlog`,0 AS `Planned`,0 AS `InProgress`,0 AS `Completed`,0 AS `TasksOutstanding`,0 AS `OverdueTasks`,0 AS `Impediments`,0 AS `HoursPlanned`,SUM((CASE WHEN ((`p`.`project_type_id` = 1) AND (`ts`.`billable` = 'Yes')) THEN `ts`.`time_decimal` WHEN (`p`.`project_type_id` = 2) THEN `ts`.`time_decimal` ELSE 0 END)) AS `ActualHours` FROM ((((`sprints` `s` LEFT JOIN `sprint_resources` `sr` ON((`s`.`id` = `sr`.`sprint_id`))) LEFT JOIN `tasks` `t` ON(((`sr`.`employee_id` = `t`.`employee_id`) AND (`sr`.`sprint_id` = `t`.`sprint_id`)))) LEFT JOIN `timesheet_view` `ts` ON((`ts`.`task_id` = `t`.`id`))) LEFT JOIN `projects` `p` ON((`ts`.`project_name` = `p`.`name`))) GROUP BY `s`.`id`,`s`.`name`,`sr`.`employee_id` UNION ALL SELECT `s`.`id` AS `Sprint`,`s`.`name` AS `Sprint`,' ' AS `Goal`,`sr`.`employee_id` AS `Resource_ID`,COUNT(DISTINCT `t`.`id`) AS `No_of_Task`,SUM((CASE WHEN (`t`.`status` = 4) THEN 1 ELSE 0 END)) AS `Done`,SUM((CASE WHEN (`t`.`status` = 1) THEN 1 ELSE 0 END)) AS `Backlog`,SUM((CASE WHEN (`t`.`status` = 2) THEN 1 ELSE 0 END)) AS `Planned`,SUM((CASE WHEN (`t`.`status` = 3) THEN 1 ELSE 0 END)) AS `InProgress`,SUM((CASE WHEN (`t`.`status` = 5) THEN 1 ELSE 0 END)) AS `Completed`,SUM((CASE WHEN (`t`.`status` IN (1,2,3)) THEN 1 ELSE 0 END)) AS `TasksOutstanding`,SUM((CASE WHEN ((`t`.`end_date` < CAST(NOW() AS DATE)) AND (`t`.`status` IN (1,2,3))) THEN 1 ELSE 0 END)) AS `OverdueTasks`,SUM((CASE WHEN (`t`.`status` = 6) THEN 1 ELSE 0 END)) AS `Impediments`,SUM(`t`.`hours_planned`) AS `HoursPlanned`,0 AS `ActualHours` FROM ((`sprints` `s` LEFT JOIN `sprint_resources` `sr` ON((`s`.`id` = `sr`.`sprint_id`))) LEFT JOIN `tasks` `t` ON(((`sr`.`employee_id` = `t`.`employee_id`) AND (`sr`.`sprint_id` = `t`.`sprint_id`)))) GROUP BY `s`.`id`,`s`.`name`,`sr`.`employee_id`) `a` GROUP BY `a`.`SprintId`,`a`.`Sprint`,`a`.`Goal`,`a`.`Resource_ID`) `st` LEFT JOIN `users` `u` ON((`u`.`id` = `st`.`Resource_ID`)))");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sprint_task_summary_view');
    }
};
