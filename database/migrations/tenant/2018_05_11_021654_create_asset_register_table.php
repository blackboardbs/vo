<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asset_register', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_class')->nullable();
            $table->string('asset_nr')->nullable();
            $table->string('make')->nullable();
            $table->text('additional_info')->nullable();
            $table->string('model')->nullable();
            $table->string('serial_nr')->nullable();
            $table->string('picture')->nullable();
            $table->date('aquire_date')->nullable();
            $table->date('retire_date')->nullable();
            $table->decimal('original_value',13,2)->nullable();
            $table->dateTime('written_off_at')->nullable();
            $table->dateTime('sold_at')->nullable();
            $table->integer('written_off_value')->nullable();
            $table->integer('sold_value')->nullable();
            $table->integer('issued_to')->nullable();
            $table->string('note')->nullable();
            $table->integer('est_life')->nullable();
            $table->string('processor')->nullable();
            $table->string('ram')->nullable();
            $table->string('hdd')->nullable();
            $table->string('screen_size')->nullable();
            $table->string('supplier')->nullable();
            $table->string('acc_form')->nullable();
            $table->integer('warranty_months')->nullable();
            $table->date('warranty_expiry_date')->nullable();
            $table->text('warranty_note')->nullable();
            $table->integer('support_months')->nullable();
            $table->date('support_expiry_date')->nullable();
            $table->string('support_provided_by')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('reference_number')->nullable();
            $table->text('support_note')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asset_register');
    }
};
