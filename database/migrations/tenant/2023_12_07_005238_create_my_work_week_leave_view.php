<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `my_work_week_leave_view`  AS WITH     `cte_L` AS (SELECT `B`.`resource_id` AS `resource_id`,`B`.`opening_balance` AS `opening_balance`,`B`.`leave_per_cycle` AS `leave_per_cycle`,`B`.`balance_date` AS `balance_date`,SUM((CASE WHEN ((`L`.`date_from` >= `B`.`balance_date`) AND (`L`.`date_from` <= CAST(NOW() AS DATE))) THEN `L`.`no_of_days` ELSE 0 END)) AS `Days_Taken`,((`B`.`leave_per_cycle` / 12) * TIMESTAMPDIFF(MONTH,`B`.`balance_date`,NOW())) AS `MonthlyDays`,SUM((CASE WHEN (COALESCE(`L`.`approve_date`,1) = 1) THEN `L`.`no_of_days` ELSE 0 END)) AS `Leave_Days_Not_Approved` FROM (`leave_balance` `B` JOIN `leave` `L` ON(((`B`.`resource_id` = `L`.`emp_id`) AND (`L`.`date_from` >= `B`.`balance_date`)))) GROUP BY `B`.`resource_id`,`B`.`opening_balance`,`B`.`leave_per_cycle`,`B`.`balance_date`) SELECT `CO`.`company_name` AS `company_name`,`U`.`company_id` AS `company_id`,`T`.`team_name` AS `team_name`,`R`.`team_id` AS `team_id`,`T`.`team_manager` AS `team_manager_id`,`U`.`first_name` AS `consultant_first_name`,`U`.`last_name` AS `consultant_last_name`,`cte_L`.`resource_id` AS `employee_id`,((`cte_L`.`opening_balance` - `cte_L`.`Days_Taken`) + `cte_L`.`MonthlyDays`) AS `Current_Leave_Balance`,`cte_L`.`Leave_Days_Not_Approved` AS `Leave_Days_Not_Approved`,`cte_L`.`Days_Taken` AS `Days_Taken` FROM ((((`cte_L` LEFT JOIN `users` `U` ON((`cte_L`.`resource_id` = `U`.`id`))) LEFT JOIN `resource` `R` ON((`U`.`resource_id` = `R`.`id`))) LEFT JOIN `team` `T` ON((`R`.`team_id` = `T`.`id`))) LEFT JOIN `company` `CO` ON((`U`.`company_id` = `CO`.`id`)))  ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS my_work_week_leave_view');
    }
};
