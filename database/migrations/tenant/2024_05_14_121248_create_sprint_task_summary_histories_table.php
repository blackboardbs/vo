<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_task_summary_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sprint_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('no_of_tasks')->default(0);
            $table->double('planned_hours')->default(0);
            $table->double('booked_hours')->default(0);
            $table->unsignedInteger('overdue_tasks')->default(0);
            $table->unsignedInteger('backlog')->default(0);
            $table->unsignedInteger('planned')->default(0);
            $table->unsignedInteger('in_progress')->default(0);
            $table->unsignedInteger('done')->default(0);
            $table->unsignedInteger('completed')->default(0);
            $table->unsignedInteger('impediments')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_sprint_task_summary_histories');
    }
};
