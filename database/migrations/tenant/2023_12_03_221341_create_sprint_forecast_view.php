<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sprint_forecast_view`  AS SELECT `bdh`.`Sprintid` AS `Sprintid`, `bdh`.`CalendarDate` AS `CalendarDate`, `bdh`.`TargetHours` AS `TargetHours`, `bdh`.`Hours` AS `Hours`, SUM(`bdh`.`Hours`) OVER (PARTITION BY `bdh`.`Sprintid` ORDER BY `bdh`.`Sprintid`,`bdh`.`CalendarDate` ) AS `CummHours`, SUM(`bdh`.`Capacity`) OVER (PARTITION BY `bdh`.`Sprintid` ORDER BY `bdh`.`Sprintid`,`bdh`.`CalendarDate` ) AS `CummCapacity` FROM (SELECT `fc`.`Sprintid` AS `Sprintid`,`fc`.`CalendarDate` AS `CalendarDate`,`fc`.`TargetHours` AS `TargetHours`,SUM(`fc`.`Hours`) AS `Hours`,SUM(`fc`.`Capacity`) AS `Capacity` FROM (SELECT `s`.`id` AS `Sprintid`,`c`.`date` AS `CalendarDate`,`g`.`TargetHours` AS `TargetHours`,SUM((CASE WHEN ((`p`.`project_type_id` = 1) AND (`ts`.`billable` = 'Yes')) THEN `ts`.`time_decimal` WHEN (`p`.`project_type_id` = 2) THEN `ts`.`time_decimal` ELSE 0 END)) AS `Hours`,0 AS `Capacity` FROM ((((((`calendar` `c` LEFT JOIN `sprints` `s` ON((`c`.`date` BETWEEN `s`.`start_date` AND `s`.`end_date`))) LEFT JOIN (SELECT `s1`.`id` AS `id`,SUM(`t1`.`hours_planned`) AS `TargetHours` FROM (`sprints` `s1` LEFT JOIN `tasks` `t1` ON((`s1`.`id` = `t1`.`sprint_id`))) GROUP BY `s1`.`id`) `g` ON((`g`.`id` = `s`.`id`))) LEFT JOIN `tasks` `t` ON((`s`.`id` = `t`.`sprint_id`))) LEFT JOIN `timesheet_view` `ts` ON(((`t`.`id` = `ts`.`task_id`) AND (`c`.`date` = `ts`.`timesheet_date`)))) LEFT JOIN `projects` `p` ON((`s`.`project_id` = `p`.`id`))) LEFT JOIN (SELECT `sr`.`sprint_id` AS `sprint_id`,SUM(`sr`.`capacity`) AS `Capacity` FROM `sprint_resources` `sr` GROUP BY `sr`.`sprint_id`) `sc` ON((`sc`.`sprint_id` = `s`.`id`))) WHERE (`s`.`id` >= 1) GROUP BY `s`.`id`,`c`.`date`,`g`.`TargetHours` UNION ALL SELECT `s`.`id` AS `Sprintid`,`c`.`date` AS `CalendarDate`,`g`.`TargetHours` AS `TargetHours`,0 AS `Hours`,SUM((CASE WHEN ((`c`.`weekday` = 'N') OR (`c`.`public_holiday` = 'Y') OR (`l`.`leave_ind` = 1)) THEN 0 ELSE `sc`.`Capacity` END)) AS `Capacity` FROM ((((`calendar` `c` LEFT JOIN `sprints` `s` ON((`c`.`date` BETWEEN `s`.`start_date` AND `s`.`end_date`))) LEFT JOIN (SELECT `s1`.`id` AS `id`,SUM(`t1`.`hours_planned`) AS `TargetHours` FROM (`sprints` `s1` LEFT JOIN `tasks` `t1` ON((`s1`.`id` = `t1`.`sprint_id`))) GROUP BY `s1`.`id`) `g` ON((`g`.`id` = `s`.`id`))) LEFT JOIN (SELECT `sr`.`employee_id` AS `employee_id`,`sr`.`sprint_id` AS `sprint_id`,SUM(`sr`.`capacity`) AS `Capacity` FROM `sprint_resources` `sr` GROUP BY `sr`.`employee_id`,`sr`.`sprint_id`) `sc` ON((`sc`.`sprint_id` = `s`.`id`))) LEFT JOIN (SELECT `leave`.`emp_id` AS `emp_id`,`leave`.`date_from` AS `date_from`,`leave`.`date_to` AS `date_to`,1 AS `leave_ind` FROM `leave` WHERE ((COALESCE(`leave`.`leave_status`,1) = 1) AND (`leave`.`status` = 1))) `l` ON(((`sc`.`employee_id` = `l`.`emp_id`) AND (`c`.`date` BETWEEN `l`.`date_from` AND `l`.`date_to`)))) GROUP BY `s`.`id`,`c`.`date`,`g`.`TargetHours`) `fc` GROUP BY `fc`.`Sprintid`,`fc`.`CalendarDate`,`fc`.`TargetHours`) AS `bdh` GROUP BY `bdh`.`Sprintid`, `bdh`.`CalendarDate`, `bdh`.`TargetHours`, `bdh`.`Hours` ORDER BY `bdh`.`Sprintid` ASC, `bdh`.`CalendarDate` ASC ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sprint_forecast_view');
    }
};
