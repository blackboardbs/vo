<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('recurrings', function (Blueprint $table) {
            $table->id();
            $table->integer('recurrable_id');
            $table->string('recurrable_type');
            $table->string('full_name')->nullable();
            $table->dateTime('first_invoice_at')->nullable();
            $table->dateTime('last_invoiced_at')->nullable();
            $table->integer('due_days_from_invoice_date')->nullable();
            $table->integer('frequency')->nullable();
            $table->integer('interval')->nullable();
            $table->integer('occurrences')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->dateTime('scheduled_at')->nullable();
            $table->string('email_subject')->nullable();
            $table->string('emails')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('recurring');
    }
};
