<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('timeline', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description_of_work')->nullable();
            $table->integer('timesheet_id')->nullable();
            $table->integer('yearwk')->nullable();
            $table->integer('mon')->default(0)->nullable();
            $table->integer('tue')->default(0)->nullable();
            $table->integer('wed')->default(0)->nullable();
            $table->integer('thu')->default(0)->nullable();
            $table->integer('fri')->default(0)->nullable();
            $table->integer('sat')->default(0)->nullable();
            $table->integer('sun')->default(0)->nullable();
            $table->integer('total')->default(0)->nullable();
            $table->integer('mon_m')->default(0)->nullable();
            $table->integer('tue_m')->default(0)->nullable();
            $table->integer('wed_m')->default(0)->nullable();
            $table->integer('thu_m')->default(0)->nullable();
            $table->integer('fri_m')->default(0)->nullable();
            $table->integer('sat_m')->default(0)->nullable();
            $table->integer('sun_m')->default(0)->nullable();
            $table->integer('total_m')->default(0)->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('mileage')->default(0)->nullable();
            $table->integer('is_billable')->default(1)->nullable();
            $table->boolean('vend_is_billable')->nullable();
            $table->integer('task_id')->default(0)->nullable();
            $table->integer('status')->default(1)->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('task_delivery_type_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('timeline');
    }
};
