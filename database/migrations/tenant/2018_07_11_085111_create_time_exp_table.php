<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('time_exp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('timesheet_id')->nullable();
            $table->integer('yearwk')->nullable();
            $table->date('date')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('is_billable')->nullable();
            $table->integer('is_approved')->nullable();
            $table->integer('claim')->nullable()->default(1);
            $table->string('description')->nullable();
            $table->decimal('amount', 10)->nullable()->default(0.00);
            $table->integer('account_id')->nullable();
            $table->integer('account_element_id')->nullable();
            $table->date('paid_date')->nullable();
            $table->string('file_name')->nullable();
            $table->date('claim_auth_date')->nullable();
            $table->string('claim_auth_ip')->nullable();
            $table->string('claim_auth_user')->nullable()->default(0);
            $table->integer('status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('time_exp');
    }
};
