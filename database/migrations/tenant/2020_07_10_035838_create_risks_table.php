<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('risks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('project_id')->nullable();
            $table->date('risk_date')->nullable();
            $table->integer('risk_area_id')->nullable();
            $table->string('risk_item')->nullable();
            $table->integer('resposibility_user_id')->nullable();
            $table->integer('accountablity_user_id')->nullable();
            $table->integer('likelihood_id')->nullable();
            $table->integer('impact_id')->nullable();
            $table->integer('security_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('risk_mitigation_strategy')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('risks');
    }
};
