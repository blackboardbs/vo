<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('assignment_warning_checks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_id');
            $table->string('warning_type');
            $table->integer('warning_no');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('assignment_warning_checks');
    }
};
