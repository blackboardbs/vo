<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assignment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('project_id');
            $table->string('customer_po')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->decimal('rate',12,2)->nullable()->default('0.00');
            $table->decimal('bonus_rate',12,2)->nullable();
            $table->integer('hours')->nullable();
            $table->integer('number_hours')->nullable();
            $table->string('vendor_id')->nullable();
            $table->string('report_to')->nullable();
            $table->string('report_to_email')->nullable();
            $table->string('report_phone')->nullable();
            $table->integer('billable')->nullable();
            $table->text('note_1')->nullable();
            $table->text('note_2')->nullable();
            $table->integer('exp_id')->nullable();
            $table->integer('function')->nullable();
            $table->integer('system')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('vat_rate_id')->nullable();
            $table->string('currency')->nullable()->default('ZAR');
            $table->decimal('internal_cost_rate',12,2)->nullable();
            $table->decimal('external_cost_rate',12,2)->nullable();
            $table->string('invoice_rate')->nullable();
            $table->integer('extension_id')->nullable();
            $table->integer('assignment_status')->nullable();
            $table->integer('claim_approver_id')->nullable();
            $table->integer('medical_certificate_type')->nullable();
            $table->string('medical_certificate_text')->nullable();
            $table->integer('assignment_approver')->nullable();
            $table->integer('project_type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('location')->nullable();
            $table->string('hours_of_work')->nullable();
            $table->integer('max_time_per_day_hours')->nullable();
            $table->integer('max_time_per_day_min')->nullable();
            $table->integer('capacity_allocation_hours')->nullable();
            $table->integer('capacity_allocation_min')->nullable();
            $table->decimal('capacity_allocation_percentage',12,2)->nullable();
            $table->date('planned_start_date')->nullable();
            $table->date('planned_end_date')->nullable();
            $table->integer('vendor_template_id')->nullable();
            $table->integer('resource_template_id')->nullable();
            $table->integer('product_owner_id')->nullable();
            $table->integer('project_manager_ts_approver')->nullable();
            $table->integer('product_owner_ts_approver')->nullable();
            $table->integer('report_to_ts_approver')->nullable();
            $table->string('role')->nullable();
            $table->date('issue_date')->nullable();
            $table->date('approved_date')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('project_manager_new_id')->nullable();
            $table->integer('project_manager_new_ts_approver')->nullable();
            $table->integer('line_manager_id')->nullable();
            $table->integer('line_manager_ts_approver')->nullable();
            $table->integer('resource_manager_id')->nullable();
            $table->unsignedInteger('claim_approver_ts_approver')->nullable();
            $table->unsignedInteger('resource_manager_ts_approver')->nullable();
            $table->string('internal_owner_known_as')->nullable();
            $table->string('product_owner_known_as')->nullable();
            $table->string('project_manager_new_known_as')->nullable();
            $table->string('line_manager_known_as')->nullable();
            $table->string('claim_approver_known_as')->nullable();
            $table->string('resource_manager_known_as')->nullable();
            $table->unsignedInteger('copy_from_user')->nullable();
            $table->double('fixed_labour_cost')->nullable();
            $table->double('fixed_price')->nullable();
            $table->decimal('rate_per_billable_hour')->nullable();
            $table->decimal('rate_per_non_billable_hour')->nullable();
            $table->unsignedInteger('invoice_rate_percent')->nullable();
            $table->decimal('assignment_fixed_cost')->nullable();
            $table->unsignedInteger('due_assignment_status_id')->nullable();
            $table->unsignedInteger('referred_vendor_id')->nullable();
            $table->unsignedInteger('referred_user_id')->nullable();
            $table->string('currency_sec')->nullable();
            $table->decimal('internal_cost_rate_sec')->nullable();
            $table->decimal('external_cost_rate_sec')->nullable();
            $table->decimal('invoice_rate_sec')->nullable();
            $table->decimal('rate_sec')->nullable();
            $table->decimal('fixed_labour_cost_sec')->nullable();
            $table->decimal('fixed_price_sec')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assignment');
    }
};
