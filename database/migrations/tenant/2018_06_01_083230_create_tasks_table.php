<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('ass_id')->nullable();
            $table->integer('grouptask_id')->nullable();
            $table->string('description')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->double('hours_planned')->nullable();
            $table->integer('dependency')->nullable();
            $table->integer('parent_activity_id')->default(0)->nullable();
            $table->integer('billable')->nullable();
            $table->longText('note_1')->nullable();
            $table->integer('milestone')->nullable();
            $table->date('invoice_date')->nullable();
            $table->string('invoice_ref')->nullable();
            $table->integer('user_story_id')->nullable();
            $table->integer('invoice_status')->default(0)->nullable();
            $table->integer('status')->default(1)->nullable();
            $table->integer('task_type_id')->default(1)->nullable();
            $table->integer('task_delivery_type_id')->default(1)->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('sprint_id')->nullable();
            $table->double('weight')->nullable();
            $table->double('remaining_time')->nullable();
            $table->integer('emote_id')->nullable();
            $table->integer('task_permission_id')->nullable();
            $table->integer('goal_id')->nullable();
            $table->boolean('on_kanban')->nullable();
            $table->text('definition_of_ready')->nullable();
            $table->text('definition_of_done')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
