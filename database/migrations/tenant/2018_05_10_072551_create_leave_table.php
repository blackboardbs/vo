<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->date('half_day')->nullable();
            $table->string('no_of_days')->nullable();
            $table->string('leave_type_id')->nullable();
            $table->string('contact_details')->nullable();
            $table->integer('approve_emp_id')->nullable();
            $table->date('approve_date')->nullable();
            $table->integer('status')->nullable();
            $table->integer('leave_status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave');
    }
};
