<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cv_qualities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cv_id')->nullable();
            $table->boolean('introduction_letter')->nullable();
            $table->date('introduction_letter_date')->nullable();
            $table->boolean('terms_and_conditions')->nullable();
            $table->boolean('criminal_record')->nullable();
            $table->boolean('credit_check')->nullable();
            $table->text('result_text')->nullable();
            $table->double('quality_percentage')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cv_qualities');
    }
};
