<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_site_report_planned', function (Blueprint $table) {
            $table->id();
            $table->integer('site_report_id')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('task_status')->nullable();
            $table->string('task')->nullable();
            $table->decimal('actual_hours',10,2)->nullable();
            $table->decimal('hours_available',10,2)->nullable();
            $table->decimal('hours_planned',10,2)->nullable();
            $table->text('notes')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_site_report_planned');
    }
};
