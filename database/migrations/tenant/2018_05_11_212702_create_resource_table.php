<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resource', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('cv_id')->nullable();
            $table->string('resource_no')->nullable();
            $table->string('resource_pref_name')->nullable();
            $table->integer('resource_position')->unsigned()->nullable();
            $table->integer('resource_level')->unsigned()->nullable();
            $table->integer('commission_id')->unsigned()->nullable();
            $table->integer('cost_center_id')->nullable();
            $table->integer('technical_rating_id')->nullable();
            $table->integer('business_skill_id')->nullable();
            $table->date('join_date')->nullable();
            $table->date('termination_date')->nullable();
            $table->date('medical_certificate_expiry')->nullable();
            $table->integer('medical_certificate_type')->nullable();
            $table->integer('manager_id')->unsigned()->nullable();
            $table->string('home_phone')->nullable()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('resource_type')->default('1')->unsigned()->nullable();
            $table->integer('project_type')->default('1')->unsigned()->nullable();
            $table->integer('util')->default('1')->unsigned()->nullable();
            $table->date('util_date_from')->nullable();
            $table->date('util_date_to')->nullable();
            $table->integer('standard_cost_bracket')->nullable();
            $table->integer('team_id')->default('0')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resource');
    }
};
