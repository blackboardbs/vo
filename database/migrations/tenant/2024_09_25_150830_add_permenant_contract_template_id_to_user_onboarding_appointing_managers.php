<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('user_onboarding_appointing_managers', function (Blueprint $table) {
            $table->foreignId('permenant_contract_template_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('user_onboarding_appointing_managers', function (Blueprint $table) {
            $table->dropColumn('permenant_contract_template_id');
        });
    }
};
