<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assessment_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment_id');
            $table->text('assessment_task_description');
            $table->integer('competency_level');
            $table->integer('comfort_level');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assessment_activities');
    }
};
