<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('job_specs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_number')->nullable();
            $table->integer('role_id')->nullable();
            $table->text('position_description')->nullable();
            $table->integer('profession_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->string('client')->nullable();
            $table->string('placement_by')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('location')->nullable();
            $table->integer('employment_type_id')->nullable();
            $table->integer('work_type_id')->nullable();
            $table->string('salary')->nullable();
            $table->double('hourly_rate_minimum')->nullable();
            $table->double('hourly_rate_maximum')->nullable();
            $table->double('monthly_salary_minimum')->nullable();
            $table->double('monthly_salary_maximum')->nullable();
            $table->date('start_date')->nullable();
            $table->date('applicaiton_closing_date')->nullable();
            $table->text('duties_and_reponsibilities')->nullable();
            $table->text('desired_experience_and_qualification')->nullable();
            $table->text('package_and_renumeration')->nullable();
            $table->string('respond_to')->nullable();
            $table->text('special_note')->nullable();
            $table->integer('cv_wishlist_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('origin_id')->nullable();
            $table->string('skills')->nullable();
            $table->integer('priority')->nullable();
            $table->integer('b_e_e')->nullable();
            $table->integer('gender')->nullable();
            $table->unsignedInteger('recruiter_id')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('job_specs');
    }
};
