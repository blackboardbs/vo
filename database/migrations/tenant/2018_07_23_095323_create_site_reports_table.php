<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('site_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->nullable();
            $table->integer('consultant_emp_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->string('report_to_firstname', 30)->nullable();
            $table->string('report_to_lastname', 30)->nullable();
            $table->date('assignment_start_date')->nullable();
            $table->date('assignment_end_date')->nullable();
            $table->date('reporting_start_date')->nullable();
            $table->date('reporting_end_date')->nullable();
            $table->integer('internal_review_emp_id')->nullable();
            $table->date('internal_review_date')->nullable();
            $table->integer('status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('site_reports');
    }
};
