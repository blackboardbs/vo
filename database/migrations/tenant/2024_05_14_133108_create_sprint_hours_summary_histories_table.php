<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_hours_summary_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sprint_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->double('planned_hours')->default(0);
            $table->double('booked_hours')->default(0);
            $table->double('outstanding_hours')->default(0);
            $table->double('available_hours')->default(0);
            $table->string('potential_outcome')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_hours_summary_histories');
    }
};
