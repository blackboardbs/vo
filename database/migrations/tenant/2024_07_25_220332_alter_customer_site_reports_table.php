<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('customer_site_reports', function (Blueprint $table) {
            $table->renameColumn('report_title', 'site_report_title');
        });
    }

    public function down()
    {
        Schema::table('customer_site_reports', function (Blueprint $table) {
            $table->renameColumn('site_report_title', 'report_title');
        });
    }
};
