<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_id')->nullable();
            $table->integer('leave_type_id')->nullable();
            $table->date('balance_date')->nullable();
            $table->string('leave_per_cycle')->nullable();
            $table->date('cycle_start')->nullable();
            $table->date('cycle_end')->nullable();
            $table->string('opening_balance')->nullable();
            $table->string('total_accrued')->nullable();
            $table->string('leave_taken')->nullable();
            $table->string('closing_balance')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave_balance');
    }
};
