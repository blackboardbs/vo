<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */

    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->integer('resource_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->date('expiry_date')->nullable();
            $table->integer('status_id')->default('1');
            $table->string('calendar_view')->nullable();
            $table->integer('active_office_id')->nullable()->unsigned();
            $table->integer('login_user')->nullable();
            $table->unsignedInteger('usertype_id')->nullable();
            $table->integer('appointment_manager_id')->nullable();
            $table->boolean('new_setup')->default(0)->nullable();
            $table->boolean('started_setup')->default(0)->nullable();
            $table->boolean('subscription_cancelled')->default(0)->nullable();
            $table->boolean('onboarding')->default(0)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
