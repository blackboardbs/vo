<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assignment_cost', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ass_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('account_element_id')->nullable();
            $table->string('description')->nullable();
            $table->text('note')->nullable();
            $table->decimal('cost')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->integer('payment_method')->default(1);
            $table->integer('status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->default(1);
            $table->integer('approver')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assignment_cost');
    }
};
