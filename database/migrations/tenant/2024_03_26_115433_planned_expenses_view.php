<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `planned_expenses_view`
        AS
        SELECT 
            p.company_id,
            a.account_type_id,
            p.account_id,
            a.description,
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(date_add(now(), INTERVAL -1 Month)) * 100) + month(date_add(now(), INTERVAL -1 Month)) THEN  p.plan_exp_value ELSE 0 END) AS period_01,
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(now()) * 100) + month(now()) THEN  p.plan_exp_value ELSE 0 END) AS period_02,
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(date_add(now(), INTERVAL 1 Month)) * 100) + month(date_add(now(), INTERVAL 1 Month)) THEN  p.plan_exp_value ELSE 0 END) AS period_03,    
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(date_add(now(), INTERVAL 2 Month)) * 100) + month(date_add(now(), INTERVAL 2 Month)) THEN  p.plan_exp_value ELSE 0 END) AS period_04,
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(date_add(now(), INTERVAL 3 Month)) * 100) + month(date_add(now(), INTERVAL 3 Month)) THEN  p.plan_exp_value ELSE 0 END) AS period_05,    
            sum(CASE WHEN (year(p.plan_exp_date) * 100) + month(p.plan_exp_date) = (year(date_add(now(), INTERVAL 4 Month)) * 100) + month(date_add(now(), INTERVAL 4 Month)) THEN  p.plan_exp_value ELSE 0 END) AS period_06
        FROM `plan_exp` p left outer join account a on p.account_id = a.id
        GROUP BY	
            p.company_id,	
            a.account_type_id,
            p.account_id,
            a.description
        ORDER BY 
            p.company_id,	
            a.account_type_id,
            p.account_id;");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS planned_expenses_view');
    }
};
