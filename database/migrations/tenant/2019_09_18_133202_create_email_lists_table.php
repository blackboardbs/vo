<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('email_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('file')->nullable();
            $table->string('from_rules')->nullable();
            $table->string('to_rules')->nullable();
            $table->string('schedule')->nullable();
            $table->date('last_run_date')->nullable();
            $table->date('next_run_date')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('email_lists');
    }
};
