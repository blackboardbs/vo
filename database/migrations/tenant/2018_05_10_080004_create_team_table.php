<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('team', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_company_id')->unsigned();
            $table->string('team_name');
            $table->integer('team_manager');
            $table->integer('status_id')->unsigned();
            $table->integer('creator_id');
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('parent_company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('team');
    }
};
