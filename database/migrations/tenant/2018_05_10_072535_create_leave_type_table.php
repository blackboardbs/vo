<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->integer('accrue')->default('0');
            $table->integer('cycle_length')->default('12');
            $table->integer('min_length')->default('3');
            $table->integer('max_days')->nullable();
            $table->integer('status');
            $table->integer('creator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave_type');
    }
};
