<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extension', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_1')->nullable();
            $table->integer('hours_1')->nullable();
            $table->string('notes_1')->nullable();
            $table->date('start_date_1')->nullable();
            $table->date('end_date_1')->nullable();
            $table->date('date_2')->nullable();
            $table->integer('hours_2')->nullable();
            $table->string('notes_2')->nullable();
            $table->date('start_date_2')->nullable();
            $table->date('end_date_2')->nullable();
            $table->date('date_3')->nullable();
            $table->integer('hours_3')->nullable();
            $table->string('notes_3')->nullable();
            $table->date('start_date_3')->nullable();
            $table->date('end_date_3')->nullable();
            $table->string('ref_1')->nullable();
            $table->string('ref_2')->nullable();
            $table->string('ref_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('extension');
    }
};
