<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_resources', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sprint_id')->nullable();
            $table->unsignedInteger('goal_id')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('function_id')->nullable();
            $table->decimal('capacity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_resources');
    }
};
