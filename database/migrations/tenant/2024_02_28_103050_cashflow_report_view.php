<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `cashflow_report_view`
        AS
        SELECT `X`.`table_id` AS `table_id`
            ,`X`.`tracking` AS `tracking`
            ,`X`.`id` AS `id`
            ,`X`.`name` AS `name`
            ,`X`.`reference` AS `reference`
            ,`X`.`type` AS `type`
            ,((((((((`X`.`overdue` + `X`.`5`) + `X`.`10`) + `X`.`20`) + `X`.`30`) + `X`.`60`) + `X`.`90`) + `X`.`120`) + `X`.`150`) AS `total`
            ,`X`.`overdue` AS `overdue`
            ,`X`.`5` AS `5`
            ,`X`.`10` AS `10`
            ,`X`.`20` AS `20`
            ,`X`.`30` AS `30`
            ,`X`.`60` AS `60`
            ,`X`.`90` AS `90`
            ,`X`.`120` AS `120`
            ,`X`.`150` AS `150`
        FROM (
            SELECT '1' AS `table_id`
                ,'Income' AS `tracking`
                ,`C`.`id` AS `id`
                ,`C`.`customer_name` AS `name`
                ,`CI`.`inv_ref` AS `reference`
                ,'customer' AS `type`
                ,SUM(`CI`.`invoice_value`) AS `total`
                ,SUM((
                        CASE 
                            WHEN (`CI`.`due_date` < CURDATE())
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `overdue`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` >= CURDATE())
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 5 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `5`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 5 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 10 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `10`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 10 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 20 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `20`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 20 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 30 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `30`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 30 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 60 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `60`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 60 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 90 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `90`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 90 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 120 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `120`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`CI`.`due_date` > (CURDATE() + INTERVAL 120 DAY))
                                    AND (`CI`.`due_date` <= (CURDATE() + INTERVAL 150 DAY))
                                    )
                                THEN `CI`.`invoice_value`
                            ELSE 0
                            END
                        )) AS `150`
            FROM (
                `customer` `C` LEFT JOIN `customer_invoices` `CI` ON ((`CI`.`customer_id` = `C`.`id`))
                )
            WHERE (
                    (`CI`.`bill_status` <= 2)
                    AND (`CI`.`paid_date` IS NULL)
                    )
            GROUP BY `CI`.`id`
                ,`C`.`customer_name`
                ,`CI`.`inv_ref`
            
            UNION ALL
            
            SELECT '2' AS `table_id`
                ,'Income' AS `tracking`
                ,`C`.`id` AS `id`
                ,`C`.`customer_name` AS `name`
                ,'-' AS `reference`
                ,'customer' AS `type`
                ,SUM(((`TL`.`total` + (`TL`.`total_m` / 60)) * `A`.`invoice_rate`)) AS `total`
                ,0 AS `overdue`
                ,0 AS `5`
                ,0 AS `10`
                ,0 AS `20`
                ,SUM(((`TL`.`total` + (`TL`.`total_m` / 60)) * `A`.`invoice_rate`)) AS `30`
                ,0 AS `60`
                ,0 AS `90`
                ,0 AS `120`
                ,0 AS `150`
            FROM (
                (
                    (
                        `timesheet` `T` LEFT JOIN `timeline` `TL` ON ((`T`.`id` = `TL`.`timesheet_id`))
                        ) LEFT JOIN `assignment` `A` ON (
                            (
                                (`T`.`employee_id` = `A`.`employee_id`)
                                AND (`T`.`project_id` = `A`.`project_id`)
                                )
                            )
                    ) LEFT JOIN `customer` `C` ON ((`T`.`customer_id` = `C`.`id`))
                )
            WHERE (
                    (`T`.`bill_status` = 1)
                    AND (`TL`.`is_billable` = 1)
                    )
            GROUP BY `C`.`id`
                ,`C`.`customer_name`
            
            UNION ALL
            
            SELECT '3' AS `table_id`
                ,'Income' AS `tracking`
                ,`A`.`id` AS `id`
                ,`A`.`description` AS `name`
                ,'' AS `reference`
                ,'account' AS `type`
                ,SUM(`PE`.`plan_exp_value`) AS `Total`
                ,SUM(0) AS `overdue`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` >= CURDATE())
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 5 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `5`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 5 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 10 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `10`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 10 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 20 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `20`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 20 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 30 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `30`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 30 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 60 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `60`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 60 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 90 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `90`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 90 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 120 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `120`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 120 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 150 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `150`
            FROM (
                `account` `A` LEFT JOIN `plan_exp` `PE` ON ((`PE`.`account_id` = `A`.`id`))
                )
            WHERE (
                    (`PE`.`plan_exp_date` >= CURDATE())
                    AND (`A`.`account_type_id` = 1)
                    )
            GROUP BY `A`.`id`
            
            UNION ALL
            
            SELECT `assignment`.`table_id` AS `table_id`
                ,`assignment`.`tracking` AS `tracking`
                ,`assignment`.`id` AS `id`
                ,`assignment`.`name` AS `name`
                ,`assignment`.`reference` AS `reference`
                ,`assignment`.`type` AS `type`
                ,(((`assignment`.`60` + `assignment`.`90`) + `assignment`.`120`) + `assignment`.`150`) AS `total`
                ,`assignment`.`overdue` AS `overdue`
                ,`assignment`.`5` AS `5`
                ,`assignment`.`10` AS `10`
                ,`assignment`.`20` AS `20`
                ,`assignment`.`30` AS `30`
                ,`assignment`.`60` AS `60`
                ,`assignment`.`90` AS `90`
                ,`assignment`.`120` AS `120`
                ,`assignment`.`150` AS `150`
            FROM (
                SELECT '4' AS `table_id`
                    ,'Income' AS `tracking`
                    ,`C`.`id` AS `id`
                    ,`C`.`customer_name` AS `name`
                    ,`P`.`name` AS `reference`
                    ,'customer' AS `type`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`A`.`end_date` >= (CURDATE() + INTERVAL 30 DAY))
                                        AND (`A`.`start_date` <= CURDATE())
                                        )
                                    THEN ROUND(((`A`.`invoice_rate` * (`A`.`hours` / (TO_DAYS(`A`.`end_date`) - TO_DAYS(`A`.`start_date`)))) * 30), 0)
                                ELSE 0
                                END
                            )) AS `total`
                    ,0 AS `overdue`
                    ,0 AS `5`
                    ,0 AS `10`
                    ,0 AS `20`
                    ,0 AS `30`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`A`.`end_date` >= (CURDATE() + INTERVAL 30 DAY))
                                        AND (`A`.`start_date` <= CURDATE())
                                        )
                                    THEN ROUND(((`A`.`invoice_rate` * (`A`.`hours` / (TO_DAYS(`A`.`end_date`) - TO_DAYS(`A`.`start_date`)))) * 30), 0)
                                ELSE 0
                                END
                            )) AS `60`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`A`.`end_date` >= (CURDATE() + INTERVAL 60 DAY))
                                        AND (`A`.`start_date` <= (CURDATE() + INTERVAL 30 DAY))
                                        )
                                    THEN ROUND(((`A`.`invoice_rate` * (`A`.`hours` / (TO_DAYS(`A`.`end_date`) - TO_DAYS(`A`.`start_date`)))) * 30), 0)
                                ELSE 0
                                END
                            )) AS `90`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`A`.`end_date` >= (CURDATE() + INTERVAL 90 DAY))
                                        AND (`A`.`start_date` <= (CURDATE() + INTERVAL 60 DAY))
                                        )
                                    THEN ROUND(((`A`.`invoice_rate` * (`A`.`hours` / (TO_DAYS(`A`.`end_date`) - TO_DAYS(`A`.`start_date`)))) * 30), 0)
                                ELSE 0
                                END
                            )) AS `120`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`A`.`end_date` >= (CURDATE() + INTERVAL 120 DAY))
                                        AND (`A`.`start_date` <= (CURDATE() + INTERVAL 90 DAY))
                                        )
                                    THEN ROUND(((`A`.`invoice_rate` * (`A`.`hours` / (TO_DAYS(`A`.`end_date`) - TO_DAYS(`A`.`start_date`)))) * 30), 0)
                                ELSE 0
                                END
                            )) AS `150`
                FROM (
                    (
                        `assignment` `A` LEFT JOIN `projects` `P` ON ((`A`.`project_id` = `P`.`id`))
                        ) LEFT JOIN `customer` `C` ON ((`C`.`id` = `P`.`customer_id`))
                    )
                WHERE (
                        (`A`.`end_date` > CURDATE())
                        AND (`A`.`invoice_rate` IS NOT NULL)
                        AND (`A`.`hours` > 0)
                        )
                GROUP BY `C`.`id`
                    ,`C`.`customer_name`
                    ,`P`.`name`
                ) `assignment`
            GROUP BY `assignment`.`table_id`
                ,`assignment`.`tracking`
                ,`assignment`.`id`
                ,`assignment`.`name`
                ,`assignment`.`reference`
                ,`assignment`.`type`
                ,`assignment`.`overdue`
                ,`assignment`.`5`
                ,`assignment`.`10`
                ,`assignment`.`20`
                ,`assignment`.`30`
                ,`assignment`.`60`
                ,`assignment`.`90`
                ,`assignment`.`120`
                ,`assignment`.`150`
            
            UNION ALL
            
            SELECT '5' AS `table_id`
                ,'Income' AS `tracking`
                ,1 AS `id`
                ,'Prospect' AS `name`
                ,`P`.`prospect_name` AS `reference`
                ,'customer' AS `type`
                ,(`P`.`value` * `P`.`chance`) AS `total`
                ,0 AS `overdue`
                ,0 AS `5`
                ,0 AS `10`
                ,0 AS `20`
                ,0 AS `30`
                ,(
                    CASE 
                        WHEN (`P`.`est_start_date` <= (CURDATE() + INTERVAL 60 DAY))
                            THEN (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)
                        ELSE (`P`.`value` / 6)
                        END
                    ) AS `60`
                ,(
                    CASE 
                        WHEN (`P`.`est_start_date` <= (CURDATE() + INTERVAL 60 DAY))
                            THEN (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)
                        ELSE (`P`.`value` / 6)
                        END
                    ) AS `90`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 120 DAY) BETWEEN `P`.`est_start_date`
                                    AND (`P`.`est_start_date` + INTERVAL `P`.`duration` MONTH)
                                )
                            THEN (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)
                        ELSE 0
                        END
                    ) AS `120`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 150 DAY) BETWEEN `P`.`est_start_date`
                                    AND (`P`.`est_start_date` + INTERVAL `P`.`duration` MONTH)
                                )
                            THEN (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)
                        ELSE 0
                        END
                    ) AS `150`
            FROM (
                `prospect` `P` LEFT JOIN `customer` `C` ON ((`P`.`customer_id` = `C`.`id`))
                )
            WHERE (`P`.`status_id` <= 5)
            
            UNION ALL
            
            SELECT '6' AS `table_id`
                ,'Expense' AS `tracking`
                ,`V`.`id` AS `id`
                ,`V`.`vendor_name` AS `name`
                ,'' AS `reference`
                ,'vendor' AS `type`
                ,SUM(`VI`.`vendor_invoice_value`) AS `total`
                ,(
                    CASE 
                        WHEN (`VI`.`vendor_due_date` > CURDATE())
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `overdue`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN CURDATE()
                                    AND (CURDATE() + INTERVAL 5 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `5`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 5 DAY)
                                    AND (CURDATE() + INTERVAL 10 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `10`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 10 DAY)
                                    AND (CURDATE() + INTERVAL 20 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `20`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 20 DAY)
                                    AND (CURDATE() + INTERVAL 30 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `30`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 30 DAY)
                                    AND (CURDATE() + INTERVAL 60 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `60`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 60 DAY)
                                    AND (CURDATE() + INTERVAL 90 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `90`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 90 DAY)
                                    AND (CURDATE() + INTERVAL 120 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `120`
                ,(
                    CASE 
                        WHEN (
                                `VI`.`vendor_due_date` BETWEEN (CURDATE() + INTERVAL 120 DAY)
                                    AND (CURDATE() + INTERVAL 150 DAY)
                                )
                            THEN SUM(`VI`.`vendor_invoice_value`)
                        ELSE 0
                        END
                    ) AS `150`
            FROM (
                `vendor` `V` LEFT JOIN `vendor_invoice` `VI` ON ((`V`.`id` = `VI`.`vendor_id`))
                )
            WHERE (
                    (`VI`.`vendor_invoice_status` <= 2)
                    AND (`VI`.`vendor_paid_date` IS NULL)
                    )
            GROUP BY `V`.`vendor_name`
                ,`V`.`id`
                ,`VI`.`vendor_due_date`
            
            UNION ALL
            
            SELECT '7' AS `table_id`
                ,'Expense' AS `tracking`
                ,`V`.`id` AS `id`
                ,`V`.`vendor_name` AS `name`
                ,' ' AS `reference`
                ,'vendor' AS `type`
                ,SUM(((`TL`.`total` + (`TL`.`total_m` / 60)) * `A`.`external_cost_rate`)) AS `total`
                ,0 AS `overdue`
                ,0 AS `5`
                ,0 AS `10`
                ,0 AS `20`
                ,SUM(((`TL`.`total` + (`TL`.`total_m` / 60)) * `A`.`external_cost_rate`)) AS `30`
                ,0 AS `60`
                ,0 AS `90`
                ,0 AS `120`
                ,0 AS `150`
            FROM (
                (
                    (
                        (
                            `timesheet` `T` LEFT JOIN `timeline` `TL` ON ((`T`.`id` = `TL`.`timesheet_id`))
                            ) LEFT JOIN `assignment` `A` ON (
                                (
                                    (`T`.`employee_id` = `A`.`employee_id`)
                                    AND (`T`.`project_id` = `A`.`project_id`)
                                    )
                                )
                        ) JOIN `users` `U` ON (
                            (
                                (`U`.`id` = `T`.`employee_id`)
                                AND (`U`.`vendor_id` > 0)
                                )
                            )
                    ) LEFT JOIN `vendor` `V` ON ((`V`.`id` = `U`.`vendor_id`))
                )
            WHERE (
                    (`TL`.`vend_is_billable` = 1)
                    AND (`T`.`vendor_invoice_status` IS NULL)
                    )
            GROUP BY `V`.`id`
                ,`V`.`vendor_name`
            
            UNION ALL
            
            SELECT '8' AS `table_id`
                ,'Expense' AS `tracking`
                ,`A`.`id` AS `id`
                ,`A`.`description` AS `name`
                ,'' AS `reference`
                ,'account' AS `type`
                ,SUM(`PE`.`plan_exp_value`) AS `total`
                ,0 AS `overdue`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` >= CURDATE())
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 5 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `5`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 5 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 10 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `10`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 10 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 20 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `20`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 20 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 30 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `30`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 30 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 60 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `60`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 60 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 90 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `90`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 90 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 120 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `120`
                ,SUM((
                        CASE 
                            WHEN (
                                    (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 120 DAY))
                                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 150 DAY))
                                    )
                                THEN `PE`.`plan_exp_value`
                            ELSE 0
                            END
                        )) AS `150`
            FROM (
                `plan_exp` `PE` LEFT JOIN `account` `A` ON ((`A`.`id` = `PE`.`account_id`))
                )
            WHERE (
                    (`A`.`account_type_id` = 2)
                    AND (`PE`.`status` = 1)
                    AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 150 DAY))
                    )
            GROUP BY `A`.`id`
            
            UNION ALL
            
            SELECT '9' AS `table_id`
                ,'Expense' AS `tracking`
                ,`C`.`id` AS `id`
                ,`C`.`customer_name` AS `name`
                ,`P`.`name` AS `reference`
                ,'customer' AS `type`
                ,SUM(`AC`.`cost`) AS `total`
                ,0 AS `overdue`
                ,0 AS `5`
                ,0 AS `10`
                ,0 AS `20`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 30 DAY) BETWEEN `A`.`start_date`
                                    AND `A`.`end_date`
                                )
                            THEN (`AC`.`cost` / TIMESTAMPDIFF(MONTH, `A`.`start_date`, `A`.`end_date`))
                        ELSE 0
                        END
                    ) AS `30`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 60 DAY) BETWEEN `A`.`start_date`
                                    AND `A`.`end_date`
                                )
                            THEN (`AC`.`cost` / TIMESTAMPDIFF(MONTH, `A`.`start_date`, `A`.`end_date`))
                        ELSE 0
                        END
                    ) AS `60`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 90 DAY) BETWEEN `A`.`start_date`
                                    AND `A`.`end_date`
                                )
                            THEN (`AC`.`cost` / TIMESTAMPDIFF(MONTH, `A`.`start_date`, `A`.`end_date`))
                        ELSE 0
                        END
                    ) AS `90`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 120 DAY) BETWEEN `A`.`start_date`
                                    AND `A`.`end_date`
                                )
                            THEN (`AC`.`cost` / TIMESTAMPDIFF(MONTH, `A`.`start_date`, `A`.`end_date`))
                        ELSE 0
                        END
                    ) AS `120`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 150 DAY) BETWEEN `A`.`start_date`
                                    AND `A`.`end_date`
                                )
                            THEN (`AC`.`cost` / TIMESTAMPDIFF(MONTH, `A`.`start_date`, `A`.`end_date`))
                        ELSE 0
                        END
                    ) AS `150`
            FROM (
                (
                    (
                        `assignment_cost` `AC` LEFT JOIN `assignment` `A` ON ((`A`.`id` = `AC`.`ass_id`))
                        ) LEFT JOIN `projects` `P` ON ((`A`.`project_id` = `P`.`id`))
                    ) LEFT JOIN `customer` `C` ON ((`P`.`customer_id` = `C`.`id`))
                )
            GROUP BY `C`.`id`
                ,`P`.`name`
                ,`A`.`start_date`
                ,`A`.`end_date`
                ,`AC`.`cost`
            
            UNION ALL
            
            SELECT `exp`.`table_id` AS `table_id`
                ,`exp`.`tracking` AS `tracking`
                ,`exp`.`id` AS `id`
                ,`exp`.`name` AS `name`
                ,`exp`.`reference` AS `reference`
                ,`exp`.`type` AS `type`
                ,SUM(`exp`.`total`) AS `total`
                ,SUM(`exp`.`overdue`) AS `overdue`
                ,SUM(`exp`.`5`) AS `5`
                ,SUM(`exp`.`10`) AS `10`
                ,SUM(`exp`.`20`) AS `20`
                ,SUM(`exp`.`30`) AS `30`
                ,SUM(`exp`.`60`) AS `60`
                ,SUM(`exp`.`90`) AS `90`
                ,SUM(`exp`.`120`) AS `120`
                ,SUM(`exp`.`150`) AS `150`
            FROM (
                SELECT '10' AS `table_id`
                    ,'Expense' AS `tracking`
                    ,`A`.`id` AS `id`
                    ,`A`.`description` AS `name`
                    ,' ' AS `reference`
                    ,'account' AS `type`
                    ,`ET`.`amount` AS `total`
                    ,`ET`.`amount` AS `overdue`
                    ,0 AS `5`
                    ,0 AS `10`
                    ,0 AS `20`
                    ,0 AS `30`
                    ,0 AS `60`
                    ,0 AS `90`
                    ,0 AS `120`
                    ,0 AS `150`
                FROM (
                    `exp_tracking` `ET` LEFT JOIN `account` `A` ON ((`A`.`id` = `ET`.`account_id`))
                    )
                WHERE (
                        (`ET`.`claimable` = 1)
                        AND (`ET`.`payment_status` = 1)
                        )
                
                UNION ALL
                
                SELECT '10' AS `table_id`
                    ,'Expense' AS `tracking`
                    ,`A`.`id` AS `id`
                    ,`A`.`description` AS `name`
                    ,'' AS `reference`
                    ,'account' AS `type`
                    ,SUM(`PE`.`plan_exp_value`) AS `Total`
                    ,SUM(0) AS `overdue`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` >= CURDATE())
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 5 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `5`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 5 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 10 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `10`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 10 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 20 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `20`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 20 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 30 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `30`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 30 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 60 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `60`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 60 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 90 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `90`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 90 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 120 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `120`
                    ,SUM((
                            CASE 
                                WHEN (
                                        (`PE`.`plan_exp_date` > (CURDATE() + INTERVAL 120 DAY))
                                        AND (`PE`.`plan_exp_date` <= (CURDATE() + INTERVAL 150 DAY))
                                        )
                                    THEN `PE`.`plan_exp_value`
                                ELSE 0
                                END
                            )) AS `150`
                FROM (
                    `account` `A` LEFT JOIN `plan_exp` `PE` ON ((`PE`.`account_id` = `A`.`id`))
                    )
                WHERE (
                        (`PE`.`plan_exp_date` >= CURDATE())
                        AND (`A`.`account_type_id` = 2)
                        )
                GROUP BY `A`.`id`
                    ,`A`.`description`
                ) `exp`
            GROUP BY `exp`.`table_id`
                ,`exp`.`tracking`
                ,`exp`.`id`
                ,`exp`.`name`
                ,`exp`.`reference`
                ,`exp`.`type`
            
            UNION ALL
            
            SELECT '11' AS `table_id`
                ,'Expense' AS `tracking`
                ,`C`.`id` AS `id`
                ,`C`.`customer_name` AS `name`
                ,`PR`.`name` AS `reference`
                ,'customer' AS `type`
                ,0 AS `total`
                ,0 AS `overdue`
                ,0 AS `5`
                ,0 AS `10`
                ,0 AS `20`
                ,0 AS `30`
                ,(
                    CASE 
                        WHEN (`P`.`est_start_date` <= (CURDATE() + INTERVAL 60 DAY))
                            THEN ((((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`) - ((80 * (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)) / 100))
                        ELSE 0
                        END
                    ) AS `60`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 90 DAY) BETWEEN `P`.`est_start_date`
                                    AND (`P`.`est_start_date` + INTERVAL `P`.`duration` MONTH)
                                )
                            THEN ((((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`) - ((80 * (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)) / 100))
                        ELSE 0
                        END
                    ) AS `90`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 120 DAY) BETWEEN `P`.`est_start_date`
                                    AND (`P`.`est_start_date` + INTERVAL `P`.`duration` MONTH)
                                )
                            THEN ((((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`) - ((80 * (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)) / 100))
                        ELSE 0
                        END
                    ) AS `120`
                ,(
                    CASE 
                        WHEN (
                                (CURDATE() + INTERVAL 150 DAY) BETWEEN `P`.`est_start_date`
                                    AND (`P`.`est_start_date` + INTERVAL `P`.`duration` MONTH)
                                )
                            THEN ((((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`) - ((80 * (((`P`.`chance` * `P`.`value`) / 100) / `P`.`duration`)) / 100))
                        ELSE 0
                        END
                    ) AS `150`
            FROM (
                (
                    `prospect` `P` LEFT JOIN `customer` `C` ON ((`P`.`customer_id` = `C`.`id`))
                    ) LEFT JOIN `projects` `PR` ON ((`PR`.`customer_id` = `C`.`id`))
                )
            WHERE (`P`.`status_id` <= 5)
            ) `X`");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS cashflow_report_view');
    }
};
