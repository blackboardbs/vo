<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cfts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->nullable();
            $table->string('additional_text')->nullable();
            $table->date('start_date')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('digisign')->nullable();
            $table->integer('destination')->nullable();
            $table->integer('sign_date')->nullable();
            $table->integer('sign_user')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('customer_invoice_status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cfts');
    }
};
