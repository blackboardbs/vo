<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cv', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('res_id')->nullable();
            $table->string('company')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('period')->nullable();
            $table->integer('period_start')->nullable();
            $table->integer('period_end')->nullable();
            $table->integer('current_project')->nullable();
            $table->string('role')->nullable();
            $table->text('project')->nullable();
            $table->text('responsibility')->nullable();
            $table->text('tools')->nullable();
            $table->text('reason_for_leaving')->nullable();
            $table->integer('row_order')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cv');
    }
};
