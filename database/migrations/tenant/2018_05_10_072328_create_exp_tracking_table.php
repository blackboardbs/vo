<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('exp_tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->date('transaction_date');
            $table->integer('account_id');
            $table->integer('account_element_id');
            $table->string('description');
            $table->integer('company_id')->nullable();
            $table->integer('assignment_id')->nullable();
            $table->string('yearwk')->nullable();
            $table->boolean('claimable')->default('0');
            $table->boolean('billable')->default('0');
            $table->string('payment_reference')->nullable();
            $table->string('document')->nullable();
            $table->string('amount');
            $table->integer('status_id');
            $table->integer('cost_center')->nullable();
            $table->integer('approved_by')->nullable();
            $table->date('approved_on')->nullable();
            $table->integer('approval_status')->default(1);
            $table->date('payment_date')->nullable();
            $table->integer('payment_status')->nullable();
            $table->mediumText('rejection_message')->nullable();
            $table->unsignedInteger('expense_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('exp_tracking');
    }
};
