<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('risk_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('risk_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->longtext('feedback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('risk_feedbacks');
    }
};
