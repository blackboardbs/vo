<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendor_invoice_line_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_invoice_line_expense')->nullable();
            $table->integer('vendor_invoice_number')->nullable();
            $table->integer('time_id')->nullable();
            $table->integer('time_exp_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('claim')->nullable();
            $table->integer('emp_id')->nullable();
            $table->integer('ass_id')->nullable();
            $table->string('vendor_po')->nullable();
            $table->text('line_text')->nullable();
            $table->decimal('amount', 10)->nullable();
            $table->integer('creator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_invoice_line_expenses');
    }
};
