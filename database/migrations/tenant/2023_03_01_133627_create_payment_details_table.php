<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_onboarding_id')->nullable();
            $table->unsignedInteger('bank_account_type_id')->nullable();
            $table->string('account_name')->nullable();
            $table->unsignedInteger('relationship_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('branch_number')->nullable();
            $table->string('branch_name')->nullable();
            $table->bigInteger('account_number')->nullable();
            $table->string('proof_of_bank_account')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_details');
    }
};
