<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `my_work_week_view`  AS WITH 
        `cte_plan` AS(
        SELECT
            `TVD`.`task_id` AS `task_id`,
            `TVD`.`no_of_days` AS `no_of_days`,
            `TVD`.`remaining_hours` AS `remaining_hours`,
            (
                CASE WHEN(`TVD`.`no_of_days` - COALESCE(`TVD`.`no_of_leave_days`,0) > 0) THEN(
                    `TVD`.`remaining_hours` / (`TVD`.`no_of_days` - COALESCE(`TVD`.`no_of_leave_days`,0))
                ) ELSE 0
            END) AS `avg_planned_hours`
        FROM
        (
        SELECT
            `TB`.`task_id` AS `task_id`,
            (
            SELECT
                COUNT(`CC`.`date`)
            FROM
                `calendar` `CC`
            WHERE
                (
                    (`CC`.`date` >= CAST(NOW() AS DATE)) AND(`CC`.`date` <= `TB`.`task_end_date`) AND(`CC`.`weekday` = 'Y') 		AND(`CC`.`public_holiday` = 'N'))
                ) AS `no_of_days`,
                (
                    CASE WHEN(
                        (
                            `TB`.`hours_planned` - `TB`.`actual_hours`
                        ) < 0
                    ) THEN 0 ELSE(
                        `TB`.`hours_planned` - COALESCE(`TB`.`actual_hours`, 0)
                    )
                END
        ) AS `remaining_hours` ,
        (   SELECT
            SUM(leave_days)
            FROM
                `leavedays_view` `LD`
            WHERE
               (`LD`.`date` >= CAST(NOW() AS DATE)) AND (`LD`.`date` <= `TB`.`task_end_date`) AND
                 `LD`.`emp_id` = `TB`.`employee_id`
         ) AS `no_of_leave_days`    
        FROM
        `task_basic_view` `TB`
        WHERE
            `TB`.`task_start_date` <=(CAST(NOW() AS DATE) + INTERVAL 12 DAY) AND
            `TB`.`task_end_date` >=(CAST(NOW() AS DATE) + INTERVAL -(12) DAY)
    ) `TVD`
    ), 
    `cte_factor` AS(
    SELECT
        `FT`.`employee_id`,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) THEN FT.factor ELSE 0 END) AS F0,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 1 DAY THEN FT.factor ELSE 0 END) AS F1,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 2 DAY THEN FT.factor ELSE 0 END) AS F2,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 3 DAY THEN FT.factor ELSE 0 END) AS F3,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 4 DAY THEN FT.factor ELSE 0 END) AS F4,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 5 DAY THEN FT.factor ELSE 0 END) AS F5,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 6 DAY THEN FT.factor ELSE 0 END) AS F6,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 7 DAY THEN FT.factor ELSE 0 END) AS F7,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 8 DAY THEN FT.factor ELSE 0 END) AS F8,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 9 DAY THEN FT.factor ELSE 0 END) AS F9,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 10 DAY THEN FT.factor ELSE 0 END) AS F10,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 11 DAY THEN FT.factor ELSE 0 END) AS F11,
        SUM(CASE WHEN FT.plan_date = CAST(NOW() AS DATE) + INTERVAL 12 DAY THEN FT.factor ELSE 0 END) AS F12
    FROM
    (
    SELECT 
        H.`date` AS plan_date,
        `TB`.`employee_id`,
        CASE 
            WHEN `weekday` = 'N' OR `public_holiday` = 'Y' THEN 0 
            WHEN COALESCE((SELECT L.leave_days FROM `leavedays_view` L WHERE L.`date` = H.`date` AND L.emp_id = `TB`.`employee_id`),0) = 1 THEN 0
            WHEN COALESCE((SELECT L.leave_days FROM `leavedays_view` L WHERE L.`date` = H.`date` AND L.emp_id = `TB`.`employee_id`),0) = 0.5 THEN 0.5  
            ELSE 1
            END AS factor
    FROM 
        (SELECT DISTINCT(`employee_id`) AS `employee_id` FROM `task_basic_view` WHERE `task_start_date` <=(CAST(NOW() AS DATE) + INTERVAL 12 DAY) AND
            `task_end_date` >=(CAST(NOW() AS DATE) + INTERVAL -(12) DAY)) `TB`, `calendar` H
    WHERE 
        H.`date` >= CAST(NOW() AS DATE) AND H.`date` <= CAST(NOW() AS DATE) + INTERVAL 12 DAY
    ) FT
    GROUP BY
        `FT`.`employee_id`
    )    
        
    SELECT
        `MW`.`company_name` AS `company_name`,
        `MW`.`company_id` AS `company_id`,
        `MW`.`customer_name` AS `customer_name`,
        `MW`.`customer_id` AS `customer_id`,
        `MW`.`project_name` AS `project_name`,
        `MW`.`project_id` AS `project_id`,
        `MW`.`team_name` AS `team_name`,
        `MW`.`team_id` AS `team_id`,
        `MW`.`team_manager_id` AS `team_manager_id`,
        `MW`.`consultant_first_name` AS `consultant_first_name`,
        `MW`.`consultant_last_name` AS `consultant_last_name`,
        `MW`.`employee_id` AS `employee_id`,
        `MW`.`task` AS `task`,
        `MW`.`task_id` AS `task_id`,
        `MW`.`hours_planned` AS `hours_planned`,
        `MW`.`actual_hours` AS `actual_hours`,
        `MW`.`remaining_hours` AS `remaining_hours`,
        `MW`.`A12` AS `A12`,
        `MW`.`A11` AS `A11`,
        `MW`.`A10` AS `A10`,
        `MW`.`A9` AS `A9`,
        `MW`.`A8` AS `A8`,
        `MW`.`A7` AS `A7`,
        `MW`.`A6` AS `A6`,
        `MW`.`A5` AS `A5`,
        `MW`.`A4` AS `A4`,
        `MW`.`A3` AS `A3`,
        `MW`.`A2` AS `A2`,
        `MW`.`A1` AS `A1`,
        `MW`.`A0` AS `A0`,
        `MW`.`P0` AS `P0`,
        `MW`.`P1` AS `P1`,
        `MW`.`P2` AS `P2`,
        `MW`.`P3` AS `P3`,
        `MW`.`P4` AS `P4`,
        `MW`.`P5` AS `P5`,
        `MW`.`P6` AS `P6`,
        `MW`.`P7` AS `P7`,
        `MW`.`P8` AS `P8`,
        `MW`.`P9` AS `P9`,
        `MW`.`P10` AS `P10`,
        `MW`.`P11` AS `P11`,
        `MW`.`P12` AS `P12`,
        (
            CASE WHEN(`MW`.`task_id` > 0) THEN ' ' ELSE 'Unknown'
        END
    ) AS `description_of_work`
    FROM
        (
        SELECT
            `MWW`.`company_name` AS `company_name`,
            `MWW`.`company_id` AS `company_id`,
            `MWW`.`customer_name` AS `customer_name`,
            `MWW`.`customer_id` AS `customer_id`,
            `MWW`.`project_name` AS `project_name`,
            `MWW`.`project_id` AS `project_id`,
            `MWW`.`team_name` AS `team_name`,
            `MWW`.`team_id` AS `team_id`,
            `MWW`.`team_manager_id` AS `team_manager_id`,
            `MWW`.`consultant_first_name` AS `consultant_first_name`,
            `MWW`.`consultant_last_name` AS `consultant_last_name`,
            `MWW`.`employee_id` AS `employee_id`,
            `MWW`.`task` AS `task`,
            `MWW`.`task_id` AS `task_id`,
            SUM(`MWW`.`hours_planned`) AS `hours_planned`,
            SUM(`MWW`.`actual_hours`) AS `actual_hours`,
            SUM(`MWW`.`remaining_hours`) AS `remaining_hours`,
            SUM(`MWW`.`A12`) AS `A12`,
            SUM(`MWW`.`A11`) AS `A11`,
            SUM(`MWW`.`A10`) AS `A10`,
            SUM(`MWW`.`A9`) AS `A9`,
            SUM(`MWW`.`A8`) AS `A8`,
            SUM(`MWW`.`A7`) AS `A7`,
            SUM(`MWW`.`A6`) AS `A6`,
            SUM(`MWW`.`A5`) AS `A5`,
            SUM(`MWW`.`A4`) AS `A4`,
            SUM(`MWW`.`A3`) AS `A3`,
            SUM(`MWW`.`A2`) AS `A2`,
            SUM(`MWW`.`A1`) AS `A1`,
            SUM(`MWW`.`A0`) AS `A0`,
            SUM(`MWW`.`P0` * `MWW`.`F0`) AS `P0`,
            SUM(`MWW`.`P1` * `MWW`.`F1`) AS `P1`,
            SUM(`MWW`.`P2` * `MWW`.`F2`) AS `P2`,
            SUM(`MWW`.`P3` * `MWW`.`F3`) AS `P3`,
            SUM(`MWW`.`P4` * `MWW`.`F4`) AS `P4`,
            SUM(`MWW`.`P5` * `MWW`.`F5`) AS `P5`,
            SUM(`MWW`.`P6` * `MWW`.`F6`) AS `P6`,
            SUM(`MWW`.`P7` * `MWW`.`F7`) AS `P7`,
            SUM(`MWW`.`P8` * `MWW`.`F8`) AS `P8`,
            SUM(`MWW`.`P9` * `MWW`.`F9`) AS `P9`,
            SUM(`MWW`.`P10` * `MWW`.`F10`) AS `P10`,
            SUM(`MWW`.`P11` * `MWW`.`F11`) AS `P11`,
            SUM(`MWW`.`P12` * `MWW`.`F12`) AS `P12`
        FROM
            (
            SELECT
                `timesheet_view`.`company_name` AS `company_name`,
                `timesheet_view`.`company_id` AS `company_id`,
                `timesheet_view`.`customer_name` AS `customer_name`,
                `timesheet_view`.`customer_id` AS `customer_id`,
                `timesheet_view`.`project_name` AS `project_name`,
                `timesheet_view`.`project_id` AS `project_id`,
                `timesheet_view`.`team_name` AS `team_name`,
                `timesheet_view`.`team_id` AS `team_id`,
                `timesheet_view`.`team_manager_id` AS `team_manager_id`,
                `timesheet_view`.`consultant_first_name` AS `consultant_first_name`,
                `timesheet_view`.`consultant_last_name` AS `consultant_last_name`,
                `timesheet_view`.`employee_id` AS `employee_id`,
                `timesheet_view`.`task` AS `task`,
                `timesheet_view`.`task_id` AS `task_id`,
                0 AS `hours_planned`,
                0 AS `actual_hours`,
                0 AS `remaining_hours`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(12) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A12`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(11) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A11`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(10) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A10`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(9) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A9`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(8) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A8`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(7) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A7`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(6) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A6`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(5) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A5`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(4) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A4`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(3) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A3`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(2) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A2`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` =(
                                CAST(NOW() AS DATE) + INTERVAL -(1) DAY)
                            ) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A1`,
                SUM(
                    (
                        CASE WHEN(
                            `timesheet_view`.`timesheet_date` = CAST(NOW() AS DATE)) THEN `timesheet_view`.`time_decimal` ELSE 0
                        END
                    )
                ) AS `A0`,
                0 AS `P0`,
                0 AS `P1`,
                0 AS `P2`,
                0 AS `P3`,
                0 AS `P4`,
                0 AS `P5`,
                0 AS `P6`,
                0 AS `P7`,
                0 AS `P8`,
                0 AS `P9`,
                0 AS `P10`,
                0 AS `P11`,
                0 AS `P12`,
                0 AS `F0`,
                0 AS `F1`,
                0 AS `F2`,
                0 AS `F3`,
                0 AS `F4`,
                0 AS `F5`,
                0 AS `F6`,
                0 AS `F7`,
                0 AS `F8`,
                0 AS `F9`,
                0 AS `F10`,
                0 AS `F11`,
                0 AS `F12`
            FROM
                `timesheet_view`
            WHERE
                (
                    `timesheet_view`.`timesheet_date` BETWEEN(
                        CAST(NOW() AS DATE) + INTERVAL -(12) DAY) AND CAST(NOW() AS DATE))
                    GROUP BY
                        `timesheet_view`.`company_name`,
                        `timesheet_view`.`company_id`,
                        `timesheet_view`.`customer_name`,
                        `timesheet_view`.`customer_id`,
                        `timesheet_view`.`project_name`,
                        `timesheet_view`.`project_id`,
                        `timesheet_view`.`team_name`,
                        `timesheet_view`.`team_id`,
                        `timesheet_view`.`team_manager_id`,
                        `timesheet_view`.`consultant_first_name`,
                        `timesheet_view`.`consultant_last_name`,
                        `timesheet_view`.`employee_id`,
                        `timesheet_view`.`task`,
                        `timesheet_view`.`task_id`
                    UNION ALL
                SELECT
                    `TV`.`company_name` AS `company_name`,
                    `TV`.`company_id` AS `company_id`,
                    `TV`.`customer` AS `customer`,
                    `TV`.`customer_id` AS `customer_id`,
                    `TV`.`project` AS `project`,
                    `TV`.`project_id` AS `project_id`,
                    `TV`.`team_name` AS `team_name`,
                    `TV`.`team_id` AS `team_id`,
                    `TV`.`team_manager_id` AS `team_manager_id`,
                    `TV`.`first_name` AS `consultant_first_name`,
                    `TV`.`last_name` AS `consultant_last_name`,
                    `TV`.`employee_id` AS `employee_id`,
                    `TV`.`task_description` AS `task`,
                    `TV`.`task_id` AS `task_id`,
                    `TV`.`hours_planned` AS `hours_planned`,
                    `TV`.`actual_hours` AS `actual_hours`,
                    `cte_plan`.`remaining_hours` AS `remaining_hours`,
                    0 AS `A12`,
                    0 AS `A11`,
                    0 AS `A10`,
                    0 AS `A9`,
                    0 AS `A8`,
                    0 AS `A7`,
                    0 AS `A6`,
                    0 AS `A5`,
                    0 AS `A4`,
                    0 AS `A3`,
                    0 AS `A2`,
                    0 AS `A1`,
                    0 AS `A0`,
                    (
                        CASE WHEN(
                            (
                                `TV`.`task_start_date` <= CAST(NOW() AS DATE)) AND(
                                    `TV`.`task_end_date` >= CAST(NOW() AS DATE))
                                ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                            END
                        ) AS `P0`,
                        (
                            CASE WHEN(
                                (
                                    `TV`.`task_start_date` <=(
                                        CAST(NOW() AS DATE) + INTERVAL 1 DAY)
                                    ) AND(
                                        `TV`.`task_end_date` >=(
                                            CAST(NOW() AS DATE) + INTERVAL 1 DAY)
                                        )
                                    ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                END
                            ) AS `P1`,
                            (
                                CASE WHEN(
                                    (
                                        `TV`.`task_start_date` <=(
                                            CAST(NOW() AS DATE) + INTERVAL 2 DAY)
                                        ) AND(
                                            `TV`.`task_end_date` >=(
                                                CAST(NOW() AS DATE) + INTERVAL 2 DAY)
                                            )
                                        ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                    END
                                ) AS `P2`,
                                (
                                    CASE WHEN(
                                        (
                                            `TV`.`task_start_date` <=(
                                                CAST(NOW() AS DATE) + INTERVAL 3 DAY)
                                            ) AND(
                                                `TV`.`task_end_date` >=(
                                                    CAST(NOW() AS DATE) + INTERVAL 3 DAY)
                                                )
                                            ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                        END
                                    ) AS `P3`,
                                    (
                                        CASE WHEN(
                                            (
                                                `TV`.`task_start_date` <=(
                                                    CAST(NOW() AS DATE) + INTERVAL 4 DAY)
                                                ) AND(
                                                    `TV`.`task_end_date` >=(
                                                        CAST(NOW() AS DATE) + INTERVAL 4 DAY)
                                                    )
                                                ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                            END
                                        ) AS `P4`,
                                        (
                                            CASE WHEN(
                                                (
                                                    `TV`.`task_start_date` <=(
                                                        CAST(NOW() AS DATE) + INTERVAL 5 DAY)
                                                    ) AND(
                                                        `TV`.`task_end_date` >=(
                                                            CAST(NOW() AS DATE) + INTERVAL 5 DAY)
                                                        )
                                                    ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                END
                                            ) AS `P5`,
                                            (
                                                CASE WHEN(
                                                    (
                                                        `TV`.`task_start_date` <=(
                                                            CAST(NOW() AS DATE) + INTERVAL 6 DAY)
                                                        ) AND(
                                                            `TV`.`task_end_date` >=(
                                                                CAST(NOW() AS DATE) + INTERVAL 6 DAY)
                                                            )
                                                        ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                    END
                                                ) AS `P6`,
                                                (
                                                    CASE WHEN(
                                                        (
                                                            `TV`.`task_start_date` <=(
                                                                CAST(NOW() AS DATE) + INTERVAL 7 DAY)
                                                            ) AND(
                                                                `TV`.`task_end_date` >=(
                                                                    CAST(NOW() AS DATE) + INTERVAL 7 DAY)
                                                                )
                                                            ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                        END
                                                    ) AS `P7`,
                                                    (
                                                        CASE WHEN(
                                                            (
                                                                `TV`.`task_start_date` <=(
                                                                    CAST(NOW() AS DATE) + INTERVAL 8 DAY)
                                                                ) AND(
                                                                    `TV`.`task_end_date` >=(
                                                                        CAST(NOW() AS DATE) + INTERVAL 8 DAY)
                                                                    )
                                                                ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                            END
                                                        ) AS `P8`,
                                                        (
                                                            CASE WHEN(
                                                                (
                                                                    `TV`.`task_start_date` <=(
                                                                        CAST(NOW() AS DATE) + INTERVAL 9 DAY)
                                                                    ) AND(
                                                                        `TV`.`task_end_date` >=(
                                                                            CAST(NOW() AS DATE) + INTERVAL 9 DAY)
                                                                        )
                                                                    ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                                END
                                                            ) AS `P9`,
                                                            (
                                                                CASE WHEN(
                                                                    (
                                                                        `TV`.`task_start_date` <=(
                                                                            CAST(NOW() AS DATE) + INTERVAL 10 DAY)
                                                                        ) AND(
                                                                            `TV`.`task_end_date` >=(
                                                                                CAST(NOW() AS DATE) + INTERVAL 10 DAY)
                                                                            )
                                                                        ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                                    END
                                                                ) AS `P10`,
                                                                (
                                                                    CASE WHEN(
                                                                        (
                                                                            `TV`.`task_start_date` <=(
                                                                                CAST(NOW() AS DATE) + INTERVAL 11 DAY)
                                                                            ) AND(
                                                                                `TV`.`task_end_date` >=(
                                                                                    CAST(NOW() AS DATE) + INTERVAL 11 DAY)
                                                                                )
                                                                            ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                                        END
                                                                    ) AS `P11`,
                                                                    (
                                                                        CASE WHEN(
                                                                            (
                                                                                `TV`.`task_start_date` <=(
                                                                                    CAST(NOW() AS DATE) + INTERVAL 12 DAY)
                                                                                ) AND(
                                                                                    `TV`.`task_end_date` >=(
                                                                                        CAST(NOW() AS DATE) + INTERVAL 12 DAY)
                                                                                    )
                                                                                ) THEN `cte_plan`.`avg_planned_hours` ELSE 0
                                                                            END
                                                                        ) AS `P12`,
                -- Factor in public holiday or Weekend or leave (This is on an Employee, task record level. Based on today + planned dates)
                F.`F0` AS `F0`,
                F.`F1` AS `F1`,
                F.`F2` AS `F2`,
                F.`F3` AS `F3`,
                F.`F4` AS `F4`,
                F.`F5` AS `F5`,
                F.`F6` AS `F6`,
                F.`F7` AS `F7`,
                F.`F8` AS `F8`,
                F.`F9` AS `F9`,
                F.`F10` AS `F10`,
                F.`F11` AS `F11`,
                F.`F12` AS `F12`
                                                                    FROM
                                                                        `task_basic_view` `TV`
                                                                        LEFT JOIN `cte_plan` ON (`TV`.`task_id` = `cte_plan`.`task_id`)
                                                                        LEFT OUTER JOIN `cte_factor` F ON (`TV`.`employee_id` = `F`.`employee_id`)
                                                                    WHERE
                                                                        (
                                                                            (
                                                                                `TV`.`task_start_date` <=(
                                                                                    CAST(NOW() AS DATE) + INTERVAL 12 DAY)
                                                                                ) AND(
                                                                                    `TV`.`task_end_date` >=(
                                                                                        CAST(NOW() AS DATE) + INTERVAL -(12) DAY)
                                                                                    ) AND(`TV`.`task_status_id` <= 3)
                                                                                )
                                                                            ) `MWW`
                                                                        GROUP BY
                                                                            `MWW`.`company_name`,
                                                                            `MWW`.`company_id`,
                                                                            `MWW`.`customer_name`,
                                                                            `MWW`.`customer_id`,
                                                                            `MWW`.`project_name`,
                                                                            `MWW`.`project_id`,
                                                                            `MWW`.`team_name`,
                                                                            `MWW`.`team_id`,
                                                                            `MWW`.`team_manager_id`,
                                                                            `MWW`.`consultant_first_name`,
                                                                            `MWW`.`consultant_last_name`,
                                                                            `MWW`.`employee_id`,
                                                                            `MWW`.`task`,
                                                                            `MWW`.`task_id`
                                                                        ) `MW`;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS my_work_week_view');
    }
};
