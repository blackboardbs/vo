<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quotation', function (Blueprint $table) {
            $table->increments('id');
            /*$table->integer('quotation_id');*/
            $table->integer('customer_id')->nullable();
            $table->string('contact_firstname')->nullable();
            $table->string('contact_lastname')->nullable();
            $table->integer('prepared_by_emp_id')->nullable();
            $table->string('customer_ref')->nullable();
            $table->string('phone')->nullable();
            $table->string('cell')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('postal_address_line1')->nullable();
            $table->string('postal_address_line2')->nullable();
            $table->string('postal_address_line3')->nullable();
            $table->integer('city_suburb_id')->nullable();
            $table->integer('state_province_id')->nullable();
            $table->string('postal_zipcode')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('consultant_emp_id')->nullable();
            $table->decimal('hour_rate')->nullable();
            $table->decimal('duration')->nullable();
            $table->decimal('amount',17,2)->default(0.00);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('scope_of_work')->nullable();
            $table->integer('exp_id')->nullable();
            $table->integer('terms_id')->default(0);
            $table->integer('update_id')->default(0);
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quotation');
    }
};
