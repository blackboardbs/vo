<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contact_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_onboarding_id')->nullable();
            $table->string('physical_address')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('email')->nullable();
            $table->string('home_number')->nullable();
            $table->string('work_number')->nullable();
            $table->string('cell_number')->nullable();
            $table->string('next_of_kin_name')->nullable();
            $table->string('next_of_kin_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contact_details');
    }
};
