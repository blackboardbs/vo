<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_invoice_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_invoice_line')->nullable();
            $table->integer('customer_invoice_number')->nullable();
            $table->integer('timesheet_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('assignment_id')->nullable();
            $table->string('line_text')->nullable();
            $table->decimal('quantity',10)->nullable();
            $table->decimal('price',10)->nullable();
            $table->decimal('invoice_line_value',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_invoice_lines');
    }
};
