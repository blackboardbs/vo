<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('
            CREATE PROCEDURE `site_report_hours_summary_sproc`(IN woy INT,IN proj_id INT,IN emp_id INT)
            BEGIN
                WITH cte_act AS
                        (
                            SELECT
                            project_id,
                            0 AS po_hours_budgeted,
                            SUM(CASE WHEN invoice_status_id IN (2,3) THEN hours_billable ELSE 0 END) AS po_billed_hours,
                            SUM(CASE WHEN invoice_status_id IN (1) THEN hours_billable ELSE 0 END) AS po_hours_unbilled,
                            SUM(CASE WHEN invoice_status_id IN (1) AND year_week < woy THEN hours_billable ELSE 0 END) AS po_hours_unbilled_previous,
                            SUM(CASE WHEN invoice_status_id IN (1) AND year_week = woy THEN hours_billable ELSE 0 END) AS po_hours_unbilled_last
                        FROM `timesheet_report_view` 
                        WHERE 
                            project_id = proj_id AND
                        --	employee_id = emp_id and
                            year_week <= woy
                        GROUP BY
                            customer_id,
                            project_id
                        ),
                        cte_po AS
                        (
                        SELECT 
                            p.`id` AS project_id,
                            p.`total_project_hours`,
                            SUM(a.`hours`) AS assignment_hours
                        FROM 
                            `projects` p, `assignment` a
                        WHERE
                            p.`id` = proj_id 
                            AND p.`id` = a.`project_id`
                    --   AND (`employee_id` = emp_id or emp_id  = -1)
                        GROUP BY	
                            p.id,
                            p.total_project_hours
                        )

                        SELECT	
                            
                            NOW() AS as_at,
                            x.project_id,
                            COALESCE(x.`total_project_hours`,assignment_hours) AS po_hours_budgeted,
                            y.po_billed_hours,
                            y.po_hours_unbilled,
                            y.po_hours_unbilled_previous,
                            y.po_hours_unbilled_last,
                            (COALESCE(x.`total_project_hours`,assignment_hours) - y.po_billed_hours - y.po_hours_unbilled) AS po_hours_remaining    
                        FROM	
                            cte_po `x` LEFT OUTER JOIN
                            cte_act `y` ON (x.project_id = y.project_id);
            END
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS site_report_hours_summary_sproc');
    }
};
