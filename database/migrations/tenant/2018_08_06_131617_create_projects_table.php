<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('ref')->nullable();
            $table->string('type')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('customer_po')->nullable();
            $table->integer('project_type_id')->nullable();
            $table->integer('quotation_id')->nullable();
            $table->string('customer_ref')->nullable();
            $table->string('consultants')->nullable();
            $table->integer('manager_id')->nullable();
            $table->integer('assignment_approver_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->decimal('est_labour_cost')->nullable();
            $table->decimal('est_other_cost')->nullable();
            $table->integer('exp_id')->nullable();
            $table->integer('billable')->nullable();
            $table->string('billing_note')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->longText('vendor_billing_note')->nullable();
            $table->string('location')->nullable();
            $table->string('hours_of_work')->nullable();
            $table->string('opex_project')->nullable();
            $table->integer('site_id')->nullable();
            $table->tinyInteger('is_fixed_price')->nullable();
            $table->double('fixed_price_labour')->nullable();
            $table->double('fixed_price_expenses')->nullable();
            $table->double('total_fixed_price')->nullable();
            $table->double('project_fixed_cost_labour')->nullable();
            $table->double('project_fixed_cost_expense')->nullable();
            $table->double('project_total_fixed_cost')->nullable();
            $table->string('total_project_hours')->nullable();
            $table->integer('timesheet_template_id')->nullable();
            $table->boolean('is_vendor')->nullable();
            $table->double('total_expenses')->nullable();
            $table->double('expense_claimable_invoiced')->nullable();
            $table->unsignedInteger('billing_cycle_id')->nullable();
            $table->unsignedInteger('vendor_invoice_contact_id')->nullable();
            $table->unsignedInteger('customer_invoice_contact_id')->nullable();
            $table->text('scope')->nullable();
            $table->unsignedInteger('billing_timesheet_templete_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('projects');
    }
};
