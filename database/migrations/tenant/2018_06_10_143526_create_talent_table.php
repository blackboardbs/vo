<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('talent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resource_pref_name')->nullable();
            $table->string('resource_middle_name')->nullable();
            $table->string('id_no')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('nationality')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('contract_house')->nullable();
            $table->integer('naturalization_id')->nullable();
            $table->integer('race')->nullable();
            $table->integer('bbbee_race')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('disability')->nullable();
            $table->integer('marital_status_id')->nullable();
            $table->string('home_language')->nullable();
            $table->string('other_language')->nullable();
            $table->string('health')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->integer('criminal_offence_ind')->nullable()->default('1');
            $table->string('criminal_offence')->nullable();
            $table->integer('drivers_license_ind')->nullable()->default('1');
            $table->string('drivers_license')->nullable();
            $table->string('main_qualification')->nullable();
            $table->integer('role_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->string('main_skill')->nullable();
            $table->string('cell')->nullable();
            $table->string('cell2')->nullable();
            $table->string('fax')->nullable();
            $table->string('personal_email')->nullable();
            $table->boolean('is_permanent')->nullable();
            $table->boolean('is_contract')->nullable();
            $table->boolean('is_temporary')->nullable();
            $table->integer('profession_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->double('hourly_rate_minimum')->nullable();
            $table->double('hourly_rate_maximum')->nullable();
            $table->double('monthly_salary_minimum')->nullable();
            $table->double('monthly_salary_maximum')->nullable();
            $table->double('charge_out_rate')->nullable();
            $table->integer('status_id')->default('1');
            $table->integer('creator_id')->nullable();
            $table->text('note')->nullable();
            $table->integer('cv_template_id')->nullable();
            $table->string('current_location')->nullable();
            $table->integer('dependants')->nullable();
            $table->tinyInteger('own_transport')->nullable();
            $table->double('current_salary_nett')->nullable();
            $table->double('expected_salary_nett')->nullable();
            $table->string('school_matriculated')->nullable();
            $table->string('highest_grade')->nullable();
            $table->integer('school_completion_year')->nullable();
            $table->text('reason_for_leaving')->nullable();
            $table->string('skills')->nullable();
            $table->string("reference_code")->nullable();
            $table->unsignedInteger("scouting_id")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('talent');
    }
};
