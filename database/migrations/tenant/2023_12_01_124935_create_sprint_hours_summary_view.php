<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sprint_hours_summary_view` AS 
SELECT 
  `A`.`SprintId` AS `SprintId`, 
  `A`.`Sprint` AS `Sprint`, 
  `A`.`Resource_ID` AS `Resource_ID`, 
  `A`.`ResourceName` AS `ResourceName`, 
  `A`.`HoursPlanned` AS `HoursPlanned`, 
  `A`.`ActualHours` AS `ActualHours`, 
  `A`.`HoursOutstanding` AS `HoursOutstanding`, 
  `A`.`Capacity` AS `HoursAvailable`, 
  (
    `A`.`Capacity` - `A`.`HoursOutstanding`
  ) AS `PotentialOutcome`, 
  (
    CASE WHEN (
      `A`.`Capacity` < `A`.`HoursOutstanding`
    ) THEN 'Inufficient hours' ELSE '' END
  ) AS `PotentialOutcomeText` 
FROM 
  (
    SELECT 
      `HoursSummary`.`SprintId` AS `SprintId`, 
      `HoursSummary`.`Sprint` AS `Sprint`, 
      `HoursSummary`.`Resource_ID` AS `Resource_ID`, 
      `HoursSummary`.`ResourceName` AS `ResourceName`, 
      SUM(`HoursSummary`.`HoursPlanned`) AS `HoursPlanned`, 
      SUM(`HoursSummary`.`ActualHours`) AS `ActualHours`, 
      (
        SUM(`HoursSummary`.`HoursPlanned`) - SUM(`HoursSummary`.`ActualHours`)
      ) AS `HoursOutstanding`, 
      SUM(
        (
          (
            `HoursSummary`.`DaysOutstanding` - `HoursSummary`.`DaysLeave`
          ) * `HoursSummary`.`Capacity`
        )
      ) AS `Capacity`, 
      SUM(
        `HoursSummary`.`DaysOutstanding`
      ) AS `DaysOutstanding` 
    FROM 
      (
        SELECT 
          `S`.`id` AS `SprintId`, 
          `S`.`name` AS `Sprint`, 
          `SR`.`employee_id` AS `Resource_ID`, 
          CONCAT(
            `U`.`first_name`, ' ', `U`.`last_name`
          ) AS `ResourceName`, 
          0 AS `HoursPlanned`, 
          SUM(
            (
              CASE WHEN (
                (`P`.`project_type_id` = 1) 
                AND (`TS`.`billable` = 'Yes')
              ) THEN `TS`.`time_decimal` WHEN (`P`.`project_type_id` = 2) THEN `TS`.`time_decimal` ELSE 0 END
            )
          ) AS `ActualHours`, 
          0 AS `Capacity`, 
          0 AS `DaysOutstanding`, 
          0 AS `DaysLeave` 
        FROM 
          (
            (
              (
                (
                  (
                    `sprints` `S` 
                    LEFT JOIN `sprint_resources` `SR` ON(
                      (`S`.`id` = `SR`.`sprint_id`)
                    )
                  ) 
                  LEFT JOIN `users` `U` ON(
                    (`U`.`id` = `SR`.`employee_id`)
                  )
                ) 
                LEFT JOIN `tasks` `T` ON(
                  (
                    (
                      `SR`.`employee_id` = `T`.`employee_id`
                    ) 
                    AND (
                      `SR`.`sprint_id` = `T`.`sprint_id`
                    )
                  )
                )
              ) 
              LEFT JOIN `timesheet_view` `TS` ON(
                (`TS`.`task_id` = `T`.`id`)
              )
            ) 
            LEFT JOIN `projects` `P` ON(
              (`TS`.`project_name` = `P`.`name`)
            )
          ) 
        WHERE 
          (`S`.`deleted_at` IS NULL) 
        GROUP BY 
          `S`.`id`, 
          `S`.`name`, 
          `SR`.`employee_id`,
          `U`.`first_name`,
          `U`.`last_name`
        UNION ALL 
        SELECT 
          `S`.`id` AS `SprintId`, 
          `S`.`name` AS `Sprint`, 
          `SR`.`employee_id` AS `Resource_ID`, 
          CONCAT(
            `U`.`first_name`, ' ', `U`.`last_name`
          ) AS `ResourceName`, 
          SUM(`T`.`hours_planned`) AS `HoursPlanned`, 
          0 AS `ActualHours`, 
          0 AS `Capacity`, 
          0 AS `DaysOutstanding`, 
          0 AS `DaysLeave` 
        FROM 
          (
            (
              (
                `sprints` `S` 
                LEFT JOIN `sprint_resources` `SR` ON(
                  (`S`.`id` = `SR`.`sprint_id`)
                )
              ) 
              LEFT JOIN `users` `U` ON(
                (`U`.`id` = `SR`.`employee_id`)
              )
            ) 
            LEFT JOIN `tasks` `T` ON(
              (
                (
                  `SR`.`employee_id` = `T`.`employee_id`
                ) 
                AND (
                  `SR`.`sprint_id` = `T`.`sprint_id`
                )
              )
            )
          ) 
        WHERE 
          (`S`.`deleted_at` IS NULL) 
        GROUP BY 
          `S`.`id`, 
          `S`.`name`, 
          `SR`.`employee_id`,
          `U`.`first_name`,
          `U`.`last_name`
        UNION ALL 
        SELECT 
          `S`.`id` AS `SprintId`, 
          `S`.`name` AS `Sprint`, 
          `SR`.`employee_id` AS `Resource_ID`, 
          CONCAT(
            `U`.`first_name`, ' ', `U`.`last_name`
          ) AS `ResourceName`, 
          0 AS `HoursPlanned`, 
          0 AS `ActualHours`, 
          COALESCE(`SR`.`capacity`, 0) AS `Capacity`, 
          COUNT(DISTINCT `C`.`date`) AS `DaysOutstanding`, 
          SUM(
            COALESCE(`L`.`leave_days`, 0)
          ) AS `DaysLeave` 
        FROM 
          (
            (
              (
                (
                  `sprints` `S` 
                  LEFT JOIN `sprint_resources` `SR` ON(
                    (`S`.`id` = `SR`.`sprint_id`)
                  )
                ) 
                LEFT JOIN `users` `U` ON(
                  (`U`.`id` = `SR`.`employee_id`)
                )
              ) 
              LEFT JOIN `calendar` `C` ON(
                (
                  (
                    `C`.`date` BETWEEN CAST(NOW() AS DATE) 
                    AND `S`.`end_date`
                  ) 
                  AND (`C`.`weekday` = 'Y') 
                  AND (`C`.`public_holiday` = 'N') 
                  AND (`C`.`date` >= `S`.`start_date`)
                )
              )
            ) 
            LEFT JOIN `leavedays_view` `L` ON(
              (
                (
                  `L`.`emp_id` = `SR`.`employee_id`
                ) 
                AND (`L`.`date` = `C`.`date`)
              )
            )
          ) 
        WHERE 
          (`S`.`deleted_at` IS NULL) 
        GROUP BY 
          `S`.`id`, 
          `S`.`name`, 
          `SR`.`employee_id`, 
          `SR`.`capacity`,
          `U`.`first_name`,
          `U`.`last_name`
      ) `HoursSummary` 
    GROUP BY 
      `HoursSummary`.`SprintId`, 
      `HoursSummary`.`Sprint`, 
      `HoursSummary`.`Resource_ID`, 
      `HoursSummary`.`ResourceName`
  ) AS `A` 
GROUP BY 
  `A`.`SprintId`, 
  `A`.`Sprint`, 
  `A`.`Resource_ID`, 
  `A`.`ResourceName`,
  `A`.`HoursPlanned`, 
  `A`.`ActualHours`, 
  `A`.`HoursOutstanding`, 
  `A`.`Capacity`
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sprint_hours_summary_view');
    }
};
