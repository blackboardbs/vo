<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `my_work_week_expense_view`  AS WITH     `cte_EE` AS (SELECT `E`.`company_id` AS `company_id`,`T`.`employee_id` AS `employee_id`,SUM((CASE WHEN (`E`.`is_approved` <> 1) THEN 1 ELSE 0 END)) AS `Expenses_Not_Approved`,SUM((CASE WHEN (COALESCE(`E`.`paid_date`,1) = 1) THEN 1 ELSE 0 END)) AS `Expenses_Not_Paid` FROM (`time_exp` `E` JOIN `timesheet` `T` ON((`E`.`timesheet_id` = `T`.`id`))) WHERE ((`E`.`is_approved` <> 1) OR (COALESCE(`E`.`paid_date`,1) = 1)) GROUP BY `E`.`company_id`,`T`.`employee_id` UNION ALL SELECT `E`.`company_id` AS `company_id`,`E`.`employee_id` AS `employee_id`,SUM((CASE WHEN (COALESCE(`E`.`approved_on`,1) = 1) THEN 1 ELSE 0 END)) AS `Expenses_Not_Approved`,SUM((CASE WHEN (COALESCE(`E`.`payment_date`,1) = 1) THEN 1 ELSE 0 END)) AS `Expenses_Not_Paid` FROM `exp_tracking` `E` WHERE ((COALESCE(`E`.`approved_on`,1) = 1) OR (COALESCE(`E`.`payment_date`,1) = 1)) GROUP BY `E`.`company_id`,`E`.`employee_id`) SELECT `CO`.`company_name` AS `company_name`,`cte_EE`.`company_id` AS `company_id`,`T`.`team_name` AS `team_name`,`R`.`team_id` AS `team_id`,`T`.`team_manager` AS `team_manager_id`,`U`.`first_name` AS `consultant_first_name`,`U`.`last_name` AS `consultant_last_name`,`cte_EE`.`employee_id` AS `employee_id`,SUM(`cte_EE`.`Expenses_Not_Approved`) AS `Expenses_Not_Approved`,SUM(`cte_EE`.`Expenses_Not_Paid`) AS `Expenses_Not_Paid` FROM ((((`cte_EE` LEFT JOIN `users` `U` ON((`cte_EE`.`employee_id` = `U`.`id`))) LEFT JOIN `resource` `R` ON((`U`.`resource_id` = `R`.`id`))) LEFT JOIN `team` `T` ON((`R`.`team_id` = `T`.`id`))) LEFT JOIN `company` `CO` ON((`U`.`company_id` = `CO`.`id`))) GROUP BY `CO`.`company_name`,`cte_EE`.`company_id`,`T`.`team_name`,`R`.`team_id`,`T`.`team_manager`,`U`.`first_name`,`U`.`last_name`,`cte_EE`.`employee_id`  ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS my_work_week_expense_view');
    }
};
