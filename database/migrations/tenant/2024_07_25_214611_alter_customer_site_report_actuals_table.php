<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('customer_site_report_actuals', function (Blueprint $table) {
            $table->text('notes')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('customer_site_report_actuals', function (Blueprint $table) {
            $table->text('notes')->nullable(false)->change();
        });
    }
};
