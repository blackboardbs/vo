<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('vendor_invoice_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_invoice_line')->nullable();
            $table->integer('vendor_invoice_number')->nullable();
            $table->integer('time_id')->nullable();
            $table->integer('emp_id')->nullable();
            $table->integer('ass_id')->nullable();
            $table->string('vendor_po')->nullable();
            $table->mediumText('line_text')->nullable();
            $table->decimal('quantity',10)->nullable();
            $table->decimal('price',10)->nullable();
            $table->decimal('invoice_line_value', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_invoice_lines');
    }
};
