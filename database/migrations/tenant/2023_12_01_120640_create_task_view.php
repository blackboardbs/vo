<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `task_view`  AS SELECT `tsk`.`id` AS `task_id`, `tsk`.`description` AS `task_description`, `tsk`.`start_date` AS `task_start_date`, `tsk`.`end_date` AS `task_end_date`, `tsk`.`hours_planned` AS `hours_planned`, `tsk`.`billable` AS `task_billable`, `tsk`.`status` AS `task_status_id`, (SELECT `ts`.`description` FROM `task_status` `ts` WHERE (`ts`.`id` = `tsk`.`status`)) AS `task_status`, (SELECT `tdt`.`name` FROM `task_delivery_types` `tdt` WHERE (`tdt`.`id` = `tsk`.`task_delivery_type_id`)) AS `task_delivery_type`, `u`.`first_name` AS `first_name`, `u`.`last_name` AS `last_name`, `tsk`.`employee_id` AS `employee_id`, `p`.`company_id` AS `company_id`, `co`.`company_name` AS `company_name`, `tsk`.`project_id` AS `project_id`, (SELECT `p`.`name` FROM `projects` `p` WHERE (`p`.`id` = `tsk`.`project_id`)) AS `project`, `tsk`.`customer_id` AS `customer_id`, (SELECT `c`.`customer_name` FROM `customer` `c` WHERE (`c`.`id` = `tsk`.`customer_id`)) AS `customer`, `tss`.`actual_hours` AS `actual_hours`, `tm`.`id` AS `team_id`, `tm`.`team_name` AS `team_name`, `tm`.`team_manager` AS `team_manager_id`, `tss`.`is_billable` AS `is_billable`, `tss`.`project_type_id` AS `project_type_id`, `tss`.`billing_period_id` AS `billing_period_id`, `tss`.`bill_status_id` AS `bill_status_id`, `tss`.`bill_status` AS `bill_status`, `tss`.`year_month` AS `year_month`, `tss`.`cost_center` AS `cost_center`, `tss`.`cost_center_id` AS `cost_center_id`, `tss`.`year_week` AS `year_week` FROM (((((((`tasks` `tsk` LEFT JOIN `users` `u` ON((`tsk`.`employee_id` = `u`.`id`))) LEFT JOIN `resource` `r` ON((`r`.`id` = `u`.`resource_id`))) LEFT JOIN `team` `tm` ON((`tm`.`id` = `r`.`team_id`))) LEFT JOIN `assignment` `a` ON(((`tsk`.`project_id` = `a`.`project_id`) AND (`tsk`.`employee_id` = `a`.`employee_id`)))) LEFT JOIN `projects` `p` ON((`p`.`id` = `a`.`project_id`))) LEFT JOIN `company` `co` ON((`co`.`id` = `p`.`company_id`))) LEFT JOIN (SELECT `timesheet_view`.`task_id` AS `task_id`,`timesheet_view`.`is_billable` AS `is_billable`,`timesheet_view`.`project_type_id` AS `project_type_id`,`timesheet_view`.`billing_period_id` AS `billing_period_id`,`timesheet_view`.`bill_status_id` AS `bill_status_id`,`timesheet_view`.`bill_status` AS `bill_status`,`timesheet_view`.`year_month` AS `year_month`,`timesheet_view`.`cost_center` AS `cost_center`,`timesheet_view`.`cost_center_id` AS `cost_center_id`,`timesheet_view`.`year_week` AS `year_week`,SUM(`timesheet_view`.`time_decimal`) AS `actual_hours` FROM `timesheet_view` GROUP BY `timesheet_view`.`task_id`,`timesheet_view`.`is_billable`,`timesheet_view`.`project_type_id`,`timesheet_view`.`billing_period_id`,`timesheet_view`.`bill_status_id`,`timesheet_view`.`bill_status`,`timesheet_view`.`year_month`,`timesheet_view`.`cost_center`,`timesheet_view`.`cost_center_id`,`timesheet_view`.`year_week`) `tss` ON((`tsk`.`id` = `tss`.`task_id`))) WHERE (`a`.`assignment_status` IN (1,2,3))");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS task_view');
    }
};
