<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared("
            CREATE PROCEDURE `site_report_detail_actual_2_sproc`(IN woy INT,IN woy2 INT,IN proj_id INT,IN emp_id INT)
            BEGIN
                IF emp_id IS NULL THEN
                    SET emp_id = -1; 
                END IF;
                
                SELECT
                    project_id,
                    CONCAT(
                        IFNULL(`task`, 'No Task'),
                        IF(`description_of_work` IS NOT NULL AND `description_of_work` != '', 
                        CONCAT(' - ', `description_of_work`), 
                        '')
                    ) AS 'task',
                    `task_status`,
                    concat(consultant_first_name,' ',consultant_last_name) AS 'employee_name',
                    sum(total_hours) AS total_hours,
                    sum(hours_billable) AS hours_billable,
                    sum(hours_non_billable) AS hours_non_billable	
                FROM `timesheet_report_view` 
                where 
                    project_id = proj_id and
                    (employee_id = emp_id OR emp_id = -1) and
                    year_week >= woy and 
                    year_week <= woy2
                GROUP BY
                    project_id,
                    CONCAT(
                        IFNULL(`task`, 'No Task'),
                        IF(`description_of_work` IS NOT NULL AND `description_of_work` != '', 
                        CONCAT(' - ', `description_of_work`), 
                        '')
                    ),
                    `task_status`,
                    concat(consultant_first_name,' ',consultant_last_name);
            END
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
DB::unprepared('DROP PROCEDURE IF EXISTS site_report_detail_actual_2_sproc');
    }
};
