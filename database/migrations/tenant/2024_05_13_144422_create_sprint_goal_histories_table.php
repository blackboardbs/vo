<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_goal_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sprint_id');
            $table->unsignedInteger('goal_id')->nullable();
            $table->integer('no_of_tasks')->default(0);
            $table->integer('task_completed')->default(0);
            $table->integer('task_outstanding')->default(0);
            $table->integer('overdue_tasks')->default(0);
            $table->integer('impediments')->default(0);
            $table->integer('planned_hours')->default(0);
            $table->integer('booked_hours')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_goal_histories');
    }
};
