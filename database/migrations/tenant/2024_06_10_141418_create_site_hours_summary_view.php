<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `site_hours_summary_view` AS WITH cte_act AS
            (
                SELECT
                project_id,
                0 AS po_hours_budgeted,
                sum(CASE WHEN invoice_status_id in (2,3) THEN hours_billable ELSE 0 END) AS po_billed_hours,
                sum(CASE WHEN invoice_status_id in (1) THEN hours_billable ELSE 0 END) AS po_hours_unbilled,
                sum(CASE WHEN invoice_status_id in (1) AND year_week < 202416 THEN hours_billable ELSE 0 END) AS po_hours_unbilled_previous,
                sum(CASE WHEN invoice_status_id in (1) AND year_week = 202416 THEN hours_billable ELSE 0 END) AS po_hours_unbilled_last
            FROM `timesheet_report_view` 
            where 
                project_id = 59 and
            --	employee_id = 17 and
                year_week <= 202416
            GROUP BY
                customer_id,
                project_id
            ),
            cte_po AS
            (
            SELECT 
                p.`id` AS project_id,
                p.`total_project_hours`,
                sum(a.`hours`) AS assignment_hours
            FROM 
                `projects` p, `assignment` a
            WHERE
                p.`id` = 59 
                AND p.`id` = a.`project_id`
            -- 	AND `employee_id` = 17
            GROUP BY	
                p.id,
                p.total_project_hours
            )

            SELECT	
                
                now() AS as_at,
                x.project_id,
                COALESCE(x.`total_project_hours`,assignment_hours) AS po_hours_budgeted,
                y.po_billed_hours,
                y.po_hours_unbilled,
                y.po_hours_unbilled_previous,
                y.po_hours_unbilled_last,
                (COALESCE(x.`total_project_hours`,assignment_hours) - y.po_billed_hours - y.po_hours_unbilled) AS po_hours_remaining    
            FROM	
                cte_po x left outer join
                cte_act y on (x.project_id = y.project_id)");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS site_hours_summary_view');
    }
};
