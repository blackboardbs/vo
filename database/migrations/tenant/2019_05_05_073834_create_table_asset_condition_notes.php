<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asset_condition_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id')->nullable();
            $table->text('note')->nullable();
            $table->integer('include_in_acceptance_letter')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asset_condition_notes');
    }
};
