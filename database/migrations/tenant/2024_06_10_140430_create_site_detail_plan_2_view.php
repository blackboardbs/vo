<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `site_detail_plan_2_view` AS WITH cte_date AS
            (
            SELECT 
                min(`date`) AS date_from,
                max(`date`) AS date_to
            FROM 
                `calendar` 
            WHERE 
                (`year`*100)+`week` >= 202416 AND 
                (`year`*100)+`week` <= 202416
            )

            SELECT
                t.project_id,
                t.task_description AS task,
                t.task_status,	
                sum(t.hours_planned - COALESCE(t.actual_hours,0)) AS hours_available,
                sum(t.hours_planned) AS hours_planned,
                sum(COALESCE(t.actual_hours,0)) AS actual_hours	
            FROM `task_view` t, `cte_date` c
            where 
                t.project_id = 59 and
                t.employee_id = 17 and
                t.task_start_date <= c.date_to AND
                t.task_end_date >= c.date_from
            GROUP BY
                t.project_id,
                t.task_description,
                t.task_status");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS site_detail_plan_2_view');
    }
};
