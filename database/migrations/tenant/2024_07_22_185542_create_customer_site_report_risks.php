<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_site_report_risks', function (Blueprint $table) {
            $table->id();
            $table->integer('site_report_id')->nullable();
            $table->string('name')->nullable();
            $table->string('likelihood')->nullable();
            $table->string('impact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_site_report_risks');
    }
};
