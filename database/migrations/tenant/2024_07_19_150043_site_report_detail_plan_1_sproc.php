<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('
            CREATE PROCEDURE `site_report_detail_plan_1_sproc`(IN woy DATE,IN woy2 DATE,IN proj_id INT,IN emp_id INT)
            BEGIN
                IF emp_id IS NULL THEN
                    SET emp_id = -1; 
                END IF;
                
                 SELECT
                                                                                    `t`.`project_id`       AS `project_id`,
                                                                                    CONCAT(`t`.`first_name`," ",`t`.`last_name`) AS `employee_name`,
                                                                                    `t`.`task_description` AS `task`,
                                                                                    `t`.`task_status`      AS `task_status`,
                                                                                    SUM((`t`.`hours_planned` - COALESCE(`t`.`actual_hours`,0))) AS `hours_available`,
                                                                                    SUM(`t`.`hours_planned`) AS `hours_planned`,
                                                                                    SUM(COALESCE(`t`.`actual_hours`,0)) AS `actual_hours`
                                                                                    FROM `task_view` `t`
                                                                                    WHERE ((`t`.`project_id` = proj_id)
                                                                                            AND (`t`.`employee_id` = emp_id OR emp_id = -1)
                                                                                            AND (`t`.`task_start_date` <= woy)
                                                                                            AND (`t`.`task_end_date` >= woy2))
                GROUP BY `t`.`project_id`,CONCAT(`t`.`first_name`," ",`t`.`last_name`),`t`.`task_description`,`t`.`task_status`;
            END
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS site_report_detail_plan_1_sproc');
    }
};
