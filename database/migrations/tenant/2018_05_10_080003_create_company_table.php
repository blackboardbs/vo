<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_company_id')->unsigned()->default('0');
            $table->string('company_name')->nullable();
            $table->string('business_reg_no')->nullable();
            $table->string('vat_no')->nullable()->default('N/A');
            $table->string('contact_firstname')->nullable();
            $table->string('contact_lastname')->nullable();
            $table->string('phone')->nullable();
            $table->string('cell')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();
            $table->string('postal_address_line1')->nullable();
            $table->string('postal_address_line2')->nullable();
            $table->string('postal_address_line3')->nullable();
            $table->string('city_suburb')->nullable();
            $table->string('state_province')->nullable();
            $table->string('postal_zipcode')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('company_logo')->nullable()->default('default.png');
            $table->integer('bbbee_level_id')->unsigned()->nullable();
            $table->date('bbbee_certificate_expiry')->nullable();
            $table->decimal('black_ownership', 20,2)->nullable()->default('0.00');
            $table->decimal('black_female_ownership', 20,2)->nullable()->default('0.00');
            $table->decimal('black_voting_rights', 20,2)->nullable()->default('0.00');
            $table->decimal('black_female_voting_rights', 20,2)->nullable()->default('0.00');
            $table->decimal('bbbee_recognition_level', 20,2)->nullable()->default('0.00');
            $table->string('bank_name')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('bank_acc_no')->nullable();
            $table->string('account_name')->nullable();
            $table->string('branch_name')->nullable();
            $table->integer('bank_account_type_id')->nullable();
            $table->string('swift_code')->nullable();
            $table->string('invoice_text')->nullable();
            $table->integer('status_id')->unsigned();
            $table->unsignedInteger('billing_cycle_id')->nullable();
            $table->integer('creator_id')->unsigned();
            $table->timestamps();

            /*$table->foreign('status_id')->references('id')->on('status');
            $table->foreign('country_id')->references('id')->on('country');
            $table->foreign('bbbee_level_id')->references('id')->on('bbbee_level');*/

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company');
    }
};
