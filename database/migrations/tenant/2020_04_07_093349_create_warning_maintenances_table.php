<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('warning_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('function_area')->nullable();
            $table->string('type')->nullable();
            $table->text('event')->nullable();
            $table->text('trigger')->nullable();
            $table->integer('time')->nullable();
            $table->string('recipient_role')->nullable();
            $table->string('notifications')->nullable(); // E - Email, N - Notify
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('warning_maintenances');
    }
};
