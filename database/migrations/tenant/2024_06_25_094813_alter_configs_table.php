<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->string('site_report_title')->default('Site Report');
            $table->integer('include_consultants')->default(1);
            $table->integer('include_task_hours')->default(0);
            $table->integer('include_hours_summary')->default(1);
            $table->integer('include_consultant_name')->default(1);
            $table->integer('include_report_to')->default(1);
            $table->integer('include_risks')->default(1);
            $table->integer('include_customer_logo')->default(1);
            $table->integer('include_company_logo')->default(1);
            $table->text('site_report_footer');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->dropColumn('site_report_title');
            $table->dropColumn('include_consultants');
            $table->dropColumn('include_task_hours');
            $table->dropColumn('include_hours_summary');
            $table->dropColumn('include_consultant_name');
            $table->dropColumn('include_report_to');
            $table->dropColumn('include_risks');
            $table->dropColumn('include_customer_logo');
            $table->dropColumn('include_company_logo');
            $table->dropColumn('site_report_footer');
        });
    }
};
