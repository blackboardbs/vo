<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `leavedays_view`  AS SELECT `L`.`emp_id` AS `emp_id`, `C`.`date` AS `date`, (CASE WHEN (`L`.`half_day` = `C`.`date`) THEN 0.5 ELSE 1 END) AS `leave_days` FROM (`leave` `L` LEFT JOIN `calendar` `C` ON(((`C`.`date` BETWEEN `L`.`date_from` AND `L`.`date_to`) AND (`C`.`weekday` = 'Y') AND (`C`.`public_holiday` = 'N')))) WHERE ((`L`.`status` = 1) AND (COALESCE(`L`.`leave_status`,0) <> 2))");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS leavedays_view');
    }
};
