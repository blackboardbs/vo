<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `my_work_week_assignment_view`  AS SELECT `ASS`.`company_name` AS `company_name`, `ASS`.`company_id` AS `company_id`, `ASS`.`customer_name` AS `customer_name`, `ASS`.`customer_id` AS `customer_id`, `ASS`.`project_name` AS `project_name`, `ASS`.`project_id` AS `project_id`, `ASS`.`team_name` AS `team_name`, `ASS`.`team_id` AS `team_id`, `ASS`.`team_manager_id` AS `team_manager_id`, `ASS`.`consultant_first_name` AS `consultant_first_name`, `ASS`.`consultant_last_name` AS `consultant_last_name`, `ASS`.`employee_id` AS `employee_id`, SUM(`ASS`.`no_of_income`) AS `no_of_income`, SUM(`ASS`.`plan_hours_on_income`) AS `plan_hours_on_income`, SUM(`ASS`.`actual_hours_on_income`) AS `actual_hours_on_income`, (SUM(`ASS`.`plan_hours_on_income`) - SUM(`ASS`.`actual_hours_on_income`)) AS `available_hours_on_income`, SUM(`ASS`.`no_of_cost`) AS `no_of_cost`, SUM(`ASS`.`plan_hours_on_income`) AS `plan_hours_on_cost`, SUM(`ASS`.`actual_hours_on_cost`) AS `actual_hours_on_cost`, (SUM(`ASS`.`plan_hours_on_cost`) - SUM(`ASS`.`actual_hours_on_cost`)) AS `available_hours_on_cost` FROM (SELECT `CO`.`company_name` AS `company_name`,`P`.`company_id` AS `company_id`,`C`.`customer_name` AS `customer_name`,`P`.`customer_id` AS `customer_id`,`P`.`name` AS `project_name`,`A`.`project_id` AS `project_id`,`T`.`team_name` AS `team_name`,`R`.`team_id` AS `team_id`,`T`.`team_manager` AS `team_manager_id`,`U`.`first_name` AS `consultant_first_name`,`U`.`last_name` AS `consultant_last_name`,`A`.`employee_id` AS `employee_id`,SUM((CASE WHEN ((`A`.`assignment_status` IN (1,2,3)) AND (`P`.`project_type_id` = 1)) THEN 1 ELSE 0 END)) AS `no_of_income`,SUM((CASE WHEN ((`A`.`assignment_status` IN (1,2,3)) AND (`P`.`project_type_id` = 1)) THEN `A`.`hours` ELSE 0 END)) AS `plan_hours_on_income`,0 AS `actual_hours_on_income`,SUM((CASE WHEN ((`A`.`assignment_status` IN (1,2,3)) AND (`P`.`project_type_id` = 2)) THEN 1 ELSE 0 END)) AS `no_of_cost`,SUM((CASE WHEN ((`A`.`assignment_status` IN (1,2,3)) AND (`P`.`project_type_id` = 2)) THEN `A`.`hours` ELSE 0 END)) AS `plan_hours_on_cost`,0 AS `actual_hours_on_cost` FROM ((((((`assignment` `A` LEFT JOIN `projects` `P` ON((`A`.`project_id` = `P`.`id`))) LEFT JOIN `customer` `C` ON((`P`.`customer_id` = `C`.`id`))) LEFT JOIN `resource` `R` ON((`A`.`employee_id` = `R`.`id`))) LEFT JOIN `team` `T` ON((`R`.`team_id` = `T`.`id`))) LEFT JOIN `company` `CO` ON((`P`.`company_id` = `CO`.`id`))) LEFT JOIN `users` `U` ON((`A`.`employee_id` = `U`.`id`))) WHERE (`A`.`assignment_status` IN (1,2,3)) GROUP BY `CO`.`company_name`,`P`.`company_id`,`C`.`customer_name`,`P`.`customer_id`,`P`.`name`,`A`.`project_id`,`T`.`team_name`,`R`.`team_id`,`T`.`team_manager`,`U`.`first_name`,`U`.`last_name`,`A`.`employee_id` UNION ALL SELECT `T`.`company_name` AS `company_name`,`T`.`company_id` AS `company_id`,`T`.`customer_name` AS `customer_name`,`T`.`customer_id` AS `customer_id`,`T`.`project_name` AS `project_name`,`T`.`project_id` AS `project_id`,`T`.`team_name` AS `team_name`,`T`.`team_id` AS `team_id`,`T`.`team_manager_id` AS `team_manager_id`,`T`.`consultant_first_name` AS `consultant_first_name`,`T`.`consultant_last_name` AS `consultant_last_name`,`T`.`employee_id` AS `employee_id`,0 AS `no_of_income`,0 AS `plan_hours_on_income`,SUM((CASE WHEN ((`T`.`project_type_id` = 1) AND (`T`.`is_billable` = 1)) THEN `T`.`time_decimal` ELSE 0 END)) AS `actual_hours_on_income`,0 AS `no_of_cost`,0 AS `plan_hours_on_cost`,SUM((CASE WHEN (`T`.`project_type_id` = 2) THEN `T`.`time_decimal` ELSE 0 END)) AS `actual_hours_on_cost` FROM (`timesheet_view` `T` LEFT JOIN `assignment` `A` ON(((`A`.`project_id` = `T`.`project_id`) AND (`A`.`employee_id` = `T`.`employee_id`)))) WHERE (`A`.`assignment_status` IN (1,2,3)) GROUP BY `T`.`company_name`,`T`.`company_id`,`T`.`customer_name`,`T`.`customer_id`,`T`.`project_name`,`T`.`project_id`,`T`.`team_name`,`T`.`team_id`,`T`.`team_manager_id`,`T`.`consultant_first_name`,`T`.`consultant_last_name`,`T`.`employee_id`) AS `ASS` GROUP BY `ASS`.`company_name`, `ASS`.`company_id`, `ASS`.`customer_name`, `ASS`.`customer_id`, `ASS`.`project_name`, `ASS`.`project_id`, `ASS`.`team_name`, `ASS`.`team_id`, `ASS`.`team_manager_id`, `ASS`.`consultant_first_name`, `ASS`.`consultant_last_name`, `ASS`.`employee_id` ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS my_work_week_assignment_view');
    }
};
