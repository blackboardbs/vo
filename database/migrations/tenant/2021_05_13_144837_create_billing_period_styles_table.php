<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('billing_period_styles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('template_id');
            $table->integer('status_id');
            $table->string('left_logo')->nullable();
            $table->string('center_logo')->nullable();
            $table->string('right_logo')->nullable();
            $table->string('logos_vertical_alignment')->nullable();
            $table->boolean('is_billing_period_included')->nullable();
            $table->string('resource_label')->default('Resource Name')->nullable();
            $table->string('project_label')->default('Project Name')->nullable();
            $table->string('start_date_label')->default('Date From')->nullable();
            $table->string('end_date_label')->default('end_date_label')->nullable();
            $table->string('po_number_label')->default('PO Number')->nullable();
            $table->text('comment')->nullable();
            $table->string('header_color')->nullable();
            $table->string('header_background')->nullable();
            $table->string('detail_color')->nullable();
            $table->string('detail_background')->nullable();
            $table->string('footer_color')->nullable();
            $table->string('footer_background')->nullable();
            $table->boolean('is_signed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('billing_period_styles');
    }
};
