<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('vendor_name')->nullable();
            $table->string('business_reg_no')->nullable();
            $table->string('vat_no')->nullable();
            $table->string('contact_firstname')->nullable();
            $table->string('contact_lastname')->nullable();
            $table->date('contact_birthday')->nullable();
            $table->string('phone')->nullable();
            $table->string('cell')->nullable();
            $table->string('cell2')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('postal_address_line1')->nullable();
            $table->string('postal_address_line2')->nullable();
            $table->string('postal_address_line3')->nullable();
            $table->string('city_suburb')->nullable();
            $table->string('state_province')->nullable();
            $table->string('postal_zipcode')->nullable();
            $table->string('street_address_line1')->nullable();
            $table->string('street_address_line2')->nullable();
            $table->string('street_address_line3')->nullable();
            $table->string('street_city_suburb')->nullable();
            $table->string('street_zipcode')->nullable();
            $table->string('web')->nullable();
            $table->string('bbbee_level')->nullable();
            $table->date('bbbee_certificate_expiry')->nullable();
            $table->string('payment_terms')->nullable();
            $table->integer('country_id')->default('1')->nullable();
            $table->integer('account_manager')->nullable();
            $table->integer('assignment_approver')->nullable();
            $table->integer('payment_terms_days')->default('30')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('bank_acc_no')->nullable();
            $table->string('vendor_logo')->nullable();
            $table->string('billing_period')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('account_name')->nullable();
            $table->string('branch_name')->nullable();
            $table->integer('bank_account_type_id')->nullable();
            $table->string('swift_code')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('invoice_contact_id')->nullable();
            $table->boolean('onboarding')->nullable();
            $table->unsignedInteger('supply_chain_appointment_manager_id')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('vendor');
    }
};
