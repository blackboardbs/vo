<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('customer_site_reports', function (Blueprint $table) {
            $table->string('report_to')->nullable();
            $table->string('project_name')->nullable();
            $table->string('consultant_name')->nullable();
            $table->text('site_report_footer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('customer_site_reports', function (Blueprint $table) {
            $table->dropColumn('report_to');
            $table->dropColumn('project_name');
            $table->dropColumn('consultant_name');
            $table->dropColumn('site_report_footer');
        });
    }
};
