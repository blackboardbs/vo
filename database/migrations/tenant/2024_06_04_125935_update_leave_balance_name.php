<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('leave_balance', function (Blueprint $table) {
            $table->integer('pay_out')->nullable();
            $table->date('pay_out_date')->nullable();
            $table->string('pay_out_balance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('leave_balance', function (Blueprint $table) {
            $table->dropColumn('pay_out');
            $table->dropColumn('pay_out_date');
            $table->dropColumn('pay_out_balance');
        });
    }
};
