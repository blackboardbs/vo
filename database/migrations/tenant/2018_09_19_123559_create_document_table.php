<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->nullable();
            $table->string('name')->nullable();
            $table->string('file')->nullable();
            $table->string('document_type_id')->nullable();
            $table->string('reference')->nullable();
            $table->integer('reference_id')->nullable();
            $table->integer('creator_id')->unsigned();
            $table->integer('owner_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable();
            $table->integer('digisign_status_id')->default(1)->nullable();
            $table->integer('digisign_approver_user_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document');
    }
};
