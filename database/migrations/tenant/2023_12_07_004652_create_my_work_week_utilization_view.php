<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `my_work_week_utilization_view`  AS SELECT `AU`.`company_name` AS `company_name`, `AU`.`company_id` AS `company_id`, `AU`.`customer_name` AS `customer_name`, `AU`.`customer_id` AS `customer_id`, `AU`.`project_name` AS `project_name`, `AU`.`project_id` AS `project_id`, `AU`.`team_name` AS `team_name`, `AU`.`team_id` AS `team_id`, `AU`.`team_manager_id` AS `team_manager_id`, `AU`.`consultant_first_name` AS `consultant_first_name`, `AU`.`consultant_last_name` AS `consultant_last_name`, `AU`.`employee_id` AS `employee_id`, `AU`.`year_week` AS `year_week`, (`PU`.`res_no` * `PU`.`wk_hours`) AS `current_week_base`, (`PU`.`cost_res_no` * `PU`.`cost_wk_hours`) AS `last_week_base`, SUM(`AU`.`current_week_total`) AS `current_week_total`, SUM(`AU`.`current_week_billable`) AS `current_week_billable`, SUM(`AU`.`last_week_total`) AS `last_week_total`, SUM(`AU`.`last_week_billable`) AS `last_week_billable` FROM ((SELECT `timesheet_view`.`company_name` AS `company_name`,`timesheet_view`.`company_id` AS `company_id`,`timesheet_view`.`customer_name` AS `customer_name`,`timesheet_view`.`customer_id` AS `customer_id`,`timesheet_view`.`project_name` AS `project_name`,`timesheet_view`.`project_id` AS `project_id`,`timesheet_view`.`team_name` AS `team_name`,`timesheet_view`.`team_id` AS `team_id`,`timesheet_view`.`team_manager_id` AS `team_manager_id`,`timesheet_view`.`consultant_first_name` AS `consultant_first_name`,`timesheet_view`.`consultant_last_name` AS `consultant_last_name`,`timesheet_view`.`employee_id` AS `employee_id`,`timesheet_view`.`year_week` AS `year_week`,(CASE WHEN (`timesheet_view`.`year_week` = ((YEAR(NOW()) * 100) + WEEK(NOW(),0))) THEN `timesheet_view`.`time_decimal` ELSE 0 END) AS `current_week_total`,(CASE WHEN ((`timesheet_view`.`year_week` = ((YEAR(NOW()) * 100) + WEEK(NOW(),0))) AND (`timesheet_view`.`project_type_id` = 1) AND (`timesheet_view`.`is_billable` = 1)) THEN `timesheet_view`.`time_decimal` ELSE 0 END) AS `current_week_billable`,(CASE WHEN (`timesheet_view`.`year_week` = ((YEAR((NOW() + INTERVAL -(7) DAY)) * 100) + WEEK((NOW() + INTERVAL -(7) DAY),0))) THEN `timesheet_view`.`time_decimal` ELSE 0 END) AS `last_week_total`,(CASE WHEN ((`timesheet_view`.`year_week` = ((YEAR((NOW() + INTERVAL -(7) DAY)) * 100) + WEEK((NOW() + INTERVAL -(7) DAY),0))) AND (`timesheet_view`.`project_type_id` = 1) AND (`timesheet_view`.`is_billable` = 1)) THEN `timesheet_view`.`time_decimal` ELSE 0 END) AS `last_week_billable` FROM `timesheet_view` WHERE (`timesheet_view`.`year_week` IN (((YEAR((NOW() + INTERVAL -(7) DAY)) * 100) + WEEK((NOW() + INTERVAL -(7) DAY),0)),((YEAR(NOW()) * 100) + WEEK(NOW(),0))))) `AU` LEFT JOIN `plan_utilization` `PU` ON((`AU`.`year_week` = `PU`.`yearwk`))) GROUP BY `AU`.`company_name`, `AU`.`company_id`, `AU`.`customer_name`, `AU`.`customer_id`, `AU`.`project_name`, `AU`.`project_id`, `AU`.`team_name`, `AU`.`team_id`, `AU`.`team_manager_id`, `AU`.`consultant_first_name`, `AU`.`consultant_last_name`, `AU`.`employee_id`, `AU`.`year_week`, (`PU`.`res_no` * `PU`.`wk_hours`), (`PU`.`cost_res_no` * `PU`.`cost_wk_hours`) ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS my_work_week_utilization_view');
    }
};
