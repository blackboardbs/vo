<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sh_dashboard_view`  AS SELECT `M`.`id` AS `KPI_id`, `M`.`kpi` AS `kpi`, `M`.`category` AS `category`, `C`.`name` AS `category_name`, `M`.`unit_of_measure` AS `unit_of_measure`, `M`.`definition` AS `definition`, `M`.`problem` AS `problem`, `M`.`resolution` AS `resolution`, `M`.`measure_calculation` AS `measure_calculation`, `M`.`list_url` AS `list_url`, `M`.`list_filters` AS `list_filters`, `M`.`fix` AS `fix`, COALESCE(`K`.`measure`,0) AS `measure`, (CASE WHEN ((COALESCE(`K`.`measure`,0) >= `F`.`sh_good_min`) AND (COALESCE(`K`.`measure`,0) <= `F`.`sh_good_max`)) THEN `M`.`cf_coulour_01` WHEN ((COALESCE(`K`.`measure`,0) >= `F`.`sh_fair_min`) AND (COALESCE(`K`.`measure`,0) <= `F`.`sh_fair_max`)) THEN `M`.`cf_coulour_02` WHEN ((COALESCE(`K`.`measure`,0) >= `F`.`sh_bad_min`) AND (COALESCE(`K`.`measure`,0) <= `F`.`sh_bad_max`)) THEN `M`.`cf_coulour_03` ELSE 'Black' END) AS `cf_colour`, (CASE WHEN ((`K`.`measure` >= `F`.`sh_good_min`) AND (`K`.`measure` <= `F`.`sh_good_max`)) THEN `M`.`health_score_01` WHEN ((`K`.`measure` >= `F`.`sh_fair_min`) AND (`K`.`measure` <= `F`.`sh_fair_max`)) THEN `M`.`health_score_02` WHEN ((`K`.`measure` >= `F`.`sh_bad_min`) AND (`K`.`measure` <= `F`.`sh_bad_max`)) THEN `M`.`health_score_03` ELSE 0 END) AS `health_score` FROM (`configs` `F` JOIN ((`sh_master_data` `M` LEFT JOIN `sh_kpi_view` `K` ON((`M`.`id` = `K`.`kpi_id`))) LEFT JOIN `sh_categories` `C` ON((`M`.`category` = `C`.`id`)))) ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sh_dashboard_view');
    }
};
