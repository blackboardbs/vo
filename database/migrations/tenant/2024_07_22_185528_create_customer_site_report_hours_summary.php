<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_site_report_hours_summary', function (Blueprint $table) {
            $table->id();
            $table->integer('site_report_id')->nullable();
            $table->dateTime('as_at')->nullable();
            $table->string('po_hours_budgeted')->nullable();
            $table->string('po_billed_hours')->nullable();
            $table->string('po_hours_unbilled_previous')->nullable();
            $table->string('po_hours_unbilled_last')->nullable();
            $table->string('po_hours_unbilled')->nullable();
            $table->string('po_hours_remaining')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_site_report_hours_summary');
    }
};
