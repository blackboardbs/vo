<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('availability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('primary_ind')->default('1');
            $table->integer('res_id')->nullable();
            $table->integer('avail_status')->default('1');
            $table->date('status_date')->nullable();
            $table->string('avail_note')->nullable();
            $table->date('avail_date')->nullable();
            $table->decimal('rate_hour',13,2)->nullable();
            $table->decimal('rate_month',13,2)->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('availability');
    }
};
