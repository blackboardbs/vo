<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->nullable(); //1 = Customer, 2 = Vendor
            $table->integer('invoice_type_id')->nullable(); //1 = Customer, 2 = Vendor
            $table->string('description')->nullable();
            $table->decimal('quantity', 10)->nullable();
            $table->decimal('price_excl', 10)->nullable();
            $table->string('vat_code')->nullable();
            $table->double('vat_percentage')->nullable();
            $table->decimal('discount_amount', 10)->nullable();
            $table->decimal('total', 10)->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invoice_items');
    }
};
