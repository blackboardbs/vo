<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sprint_analysis_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sprint_id');
            $table->date('date');
            $table->double('actual_task_hours')->default(0.00);
            $table->double('actual_task_hours_cumulative')->default(0.00);
            $table->double('goal_hours')->default(0.00);
            $table->double('actual_hours')->default(0.00);
            $table->double('actual_hours_cumulative')->default(0.00);
            $table->double('capacity_hours')->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sprint_analysis_histories');
    }
};
