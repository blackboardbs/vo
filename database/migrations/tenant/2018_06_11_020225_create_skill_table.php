<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('skill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('res_id')->nullable();
            $table->string('res_skill')->nullable();
            $table->integer('tool_id')->default('0');
            $table->string('tool')->nullable();
            $table->string('years_experience')->nullable();
            $table->integer('skill_level')->default('1');
            $table->integer('row_order')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('skill');
    }
};
