<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `timesheet_report_view`  AS SELECT `timesheet_view`.`company_name` AS `company_name`, `timesheet_view`.`company_id` AS `company_id`, `timesheet_view`.`customer_name` AS `customer_name`, `timesheet_view`.`customer_id` AS `customer_id`, `timesheet_view`.`project_name` AS `project_name`, `timesheet_view`.`project_id` AS `project_id`, `timesheet_view`.`team_name` AS `team_name`, `timesheet_view`.`team_id` AS `team_id`, `timesheet_view`.`team_manager_id` AS `team_manager_id`, `timesheet_view`.`consultant_first_name` AS `consultant_first_name`, `timesheet_view`.`consultant_last_name` AS `consultant_last_name`, `timesheet_view`.`employee_id` AS `employee_id`, `timesheet_view`.`task` AS `task`, `timesheet_view`.`task_id` AS `task_id`, `timesheet_view`.`description_of_work` AS `description_of_work`, `timesheet_view`.`year_week` AS `year_week`, `timesheet_view`.`year_month` AS `year_month`, `timesheet_view`.`is_billable` AS `is_billable`, `timesheet_view`.`bill_status` AS `invoice_status`, `timesheet_view`.`bill_status_id` AS `invoice_status_id`, `timesheet_view`.`cost_center` AS `cost_center`, `timesheet_view`.`cost_center_id` AS `cost_center_id`, `timesheet_view`.`timesheet_date` AS `timesheet_date`, `timesheet_view`.`inv_ref` AS `invoice_reference`, SUM(`timesheet_view`.`time_decimal`) AS `total_hours`, SUM((CASE WHEN (`timesheet_view`.`is_billable` = 1) THEN `timesheet_view`.`time_decimal` ELSE 0 END)) AS `Hours_Billable`, SUM((CASE WHEN (`timesheet_view`.`is_billable` = 0) THEN `timesheet_view`.`time_decimal` ELSE 0 END)) AS `Hours_Non_Billable` FROM `timesheet_view` GROUP BY `timesheet_view`.`company_name`, `timesheet_view`.`company_id`, `timesheet_view`.`customer_name`, `timesheet_view`.`customer_id`, `timesheet_view`.`project_name`, `timesheet_view`.`project_id`, `timesheet_view`.`team_name`, `timesheet_view`.`team_id`, `timesheet_view`.`team_manager_id`, `timesheet_view`.`consultant_first_name`, `timesheet_view`.`consultant_last_name`, `timesheet_view`.`employee_id`, `timesheet_view`.`task`, `timesheet_view`.`task_id`, `timesheet_view`.`description_of_work`, `timesheet_view`.`year_week`, `timesheet_view`.`year_month`, `timesheet_view`.`is_billable`, `timesheet_view`.`bill_status`, `timesheet_view`.`bill_status_id`, `timesheet_view`.`cost_center`, `timesheet_view`.`cost_center_id`, `timesheet_view`.`timesheet_date`, `timesheet_view`.`inv_ref` ;");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS timesheet_report_view');
    }
};
