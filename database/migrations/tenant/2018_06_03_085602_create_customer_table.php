<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name')->nullable();
            $table->string('business_reg_no')->nullable();
            $table->string('vat_no')->nullable();
            $table->string('contact_firstname')->nullable();
            $table->string('contact_lastname')->nullable();
            $table->date('contact_birthday')->nullable();
            $table->integer('medical_certificate_type')->nullable();
            $table->string('medical_certificate_text')->nullable();
            $table->string('phone')->nullable();
            $table->string('cell')->nullable();
            $table->string('cell2')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('postal_address_line1')->nullable();
            $table->string('postal_address_line2')->nullable();
            $table->string('postal_address_line3')->nullable();
            $table->string('city_suburb')->nullable();
            $table->string('state_province')->nullable();
            $table->string('postal_zipcode')->nullable();
            $table->string('street_address_line1')->nullable();
            $table->string('street_address_line2')->nullable();
            $table->string('street_address_line3')->nullable();
            $table->string('street_city_suburb')->nullable();
            $table->integer('payment_terms')->nullable()->default(30);
            $table->string('street_zipcode')->nullable();
            $table->string('billing_note')->nullable();
            $table->string('web')->nullable();
            $table->integer('country_id')->nullable()->default('1');
            $table->integer('account_manager')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('bbbee_level')->nullable();
            $table->date('bbbee_certificate_expiry')->nullable();
            $table->integer('bbbee_level_preference')->nullable();
            $table->decimal('commission')->nullable()->default(0);
            $table->integer('status')->nullable();
            $table->integer('creator_id')->nullable();
            $table->unsignedInteger('billing_cycle_id')->nullable();
            $table->string('logo')->nullable();
            $table->unsignedInteger('invoice_contact_id')->nullable();
            $table->boolean('onboarding')->nullable();
            $table->unsignedInteger('appointment_manager_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer');
    }
};
