<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_invoice_number')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('inv_ref')->nullable();
            $table->date('invoice_date')->nullable();
            $table->date('due_date')->nullable();
            $table->date('paid_date')->nullable();
            $table->integer('customer_id')->nullable();
            $table->date('customer_unallocate_date')->nullable();
            $table->integer('bill_status')->nullable();
            $table->text('invoice_notes')->nullable();
            $table->decimal('invoice_value', 10)->nullable();
            $table->string('cust_inv_ref')->nullable();
            $table->unsignedInteger('project_id')->nullable();
            $table->unsignedInteger('resource_id')->nullable();
            $table->string('currency')->nullable();
            $table->unsignedInteger('conversion_rate')->nullable();
            $table->unsignedInteger('vat_rate_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_invoices');
    }
};
