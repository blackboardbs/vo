<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `project_progress_report_view` AS 
        SELECT
            `TSV`.`company_name` AS `company_name`,
            `TSV`.`company_id` AS `company_id`,
            `TSV`.`customer_name` AS `customer_name`,
            `TSV`.`customer_id` AS `customer_id`,
            `TSV`.`project_name` AS `project_name`,
            `TSV`.`project_id` AS `project_id`,
            `TSV`.`team_name` AS `team_name`,
            `TSV`.`team_id` AS `team_id`,
            `TSV`.`team_manager_id` AS `team_manager_id`,
            `TSV`.`consultant_first_name` AS `consultant_first_name`,
            `TSV`.`consultant_last_name` AS `consultant_last_name`,
            `TSV`.`employee_id` AS `employee_id`,
            `TSV`.`task` AS `task`,
            `TSV`.`task_id` AS `task_id`,
            `TSV`.`year_week` AS `year_week`,
            `TSV`.`year_month` AS `year_month`,
            `TSV`.`is_billable` AS `is_billable`,
            `TSV`.`bill_status` AS `invoice_status`,
            `TSV`.`bill_status_id` AS `invoice_status_id`,
            `F`.`id` AS `feature_id`,
                        `F`.`name` AS `feature_name`,
                        `E`.`id` AS `epic_id`,
                        `E`.`name` AS `epic_name`,
                        `US`.`id` AS `user_story_id`,
                        `US`.`name` AS `user_story_name`,
            `TSV`.`cost_center` AS `cost_center`,
            `TSV`.`cost_center_id` AS `cost_center_id`,
            `TSV`.`timesheet_date` AS `timesheet_date`,
            `TSV`.`inv_ref` AS `invoice_reference`,
            `TSKS`.`description` AS `task_status`,
            `TSKS`.`id` AS `task_status_id`,
                        MAX(`TSK`.`hours_planned`) AS `hours_planned`,
            SUM(`TSV`.`time_decimal`) AS `total_hours`,
            SUM(
                (
                    CASE WHEN(`TSV`.`is_billable` = 1) THEN `TSV`.`time_decimal` ELSE 0
                END
            )
        ) AS `Hours_Billable`,
        SUM(
            (
                CASE WHEN(`TSV`.`is_billable` = 0) THEN `TSV`.`time_decimal` ELSE 0
            END
        )
        ) AS `Hours_Non_Billable`
        FROM
            `timesheet_view` TSV
                        LEFT OUTER JOIN `tasks` `TSK` ON `TSK`.`id` = `TSV`.`task_id`
                        
                        LEFT OUTER JOIN `task_status` `TSKS` ON `TSKS`.`id` = `TSK`.`status`
            LEFT OUTER JOIN `user_stories` `US` ON `US`.`id` = `TSK`.`user_story_id`
            LEFT OUTER JOIN `features` `F` ON `F`.`id` = `US`.`feature_id`
            LEFT OUTER JOIN `epics` `E` ON `E`.`id` = `F`.epic_id
            
          WHERE `TSV`.`time_decimal` <> 0 -- and `TSV`.`task_id` = 2843  
            
            
            
        GROUP BY
            `TSV`.`task`,
            `TSV`.`task_id`,
            `TSV`.`company_name`,
            `TSV`.`company_id`,
            `TSV`.`customer_name`,
            `TSV`.`customer_id`,
            `TSV`.`project_name`,
            `TSV`.`project_id`,
            `TSV`.`team_name`,
            `TSV`.`team_id`,
            `TSV`.`team_manager_id`,
            `TSV`.`consultant_first_name`,
            `TSV`.`consultant_last_name`,
            `TSV`.`employee_id`,
            `TSV`.`description_of_work`,
            `TSV`.`year_week`,
            `TSV`.`year_month`,
            `TSV`.`is_billable`,
            `TSV`.`bill_status`,
            `TSV`.`bill_status_id`,
            `TSV`.`cost_center`,
            `TSV`.`cost_center_id`,
            `TSV`.`timesheet_date`,
            `TSV`.`inv_ref`,
                        `F`.`id`,
                        `F`.`name`,
                        `E`.`id`,
                        `E`.`name`,
                        `US`.`id`,
                        `US`.`name`,
                        `TSK`.`hours_planned`,
                        `TSKS`.`description`,
                        `TSKS`.`id`");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS progress_report_view');
    }
};
