<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resource_task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_number');
            $table->string('subject');
            $table->string('description');
            $table->date('due_date');
            $table->integer('task_user');
            $table->string('rejection_reason')->nullable();
            $table->date('rejection_date')->nullable();
            $table->boolean('email_task')->default('0');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->date('completed_date')->nullable();
            $table->integer('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resource_task');
    }
};
