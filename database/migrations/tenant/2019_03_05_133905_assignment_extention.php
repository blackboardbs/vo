<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assignment_extension', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_id')->nullable();
            $table->date('extension_date')->nullable();
            $table->integer('extension_hours')->nullable();
            $table->text('extension_notes')->nullable();
            $table->date('extension_start_date')->nullable();
            $table->date('extension_end_date')->nullable();
            $table->string('extension_ref')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assignment_extension');
    }
};
