<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_invoice_line_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_invoice_line_expense')->nullable();
            $table->integer('customer_invoice_number')->nullable();
            $table->integer('timesheet_id')->nullable();
            $table->integer('time_exp_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('bill')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('assignment_id')->nullable();
            $table->string('customer_po')->nullable();
            $table->text('line_text')->nullable();
            $table->decimal('amount', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_invoice_line_expenses');
    }
};
