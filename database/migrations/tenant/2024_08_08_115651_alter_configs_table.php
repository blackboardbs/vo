<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->integer('kanban_show_backlog')->default(1);
            $table->integer('kanban_show_planned')->default(1);
            $table->integer('kanban_show_in_progress')->default(1);
            $table->integer('kanban_show_done')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->dropColumn('kanban_show_backlog');
            $table->dropColumn('kanban_show_planned');
            $table->dropColumn('kanban_show_in_progress');
            $table->dropColumn('kanban_show_done');
        });
    }
};
