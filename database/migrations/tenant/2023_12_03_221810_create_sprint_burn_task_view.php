<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sprint_burn_task_view`  AS SELECT `bdh`.`Sprintid` AS `Sprintid`, `bdh`.`CalendarDate` AS `CalendarDate`, `bdh`.`TargetHours` AS `TargetHours`, `bdh`.`Hours` AS `Hours`, SUM(`bdh`.`Hours`) OVER (PARTITION BY `bdh`.`Sprintid` ORDER BY `bdh`.`Sprintid`,`bdh`.`CalendarDate` ) AS `CummHours` FROM (SELECT `s`.`id` AS `Sprintid`,`c`.`date` AS `CalendarDate`,`g`.`TargetHours` AS `TargetHours`,SUM(`t`.`hours_planned`) AS `Hours` FROM (((`calendar` `c` LEFT JOIN `sprints` `s` ON((`c`.`date` BETWEEN `s`.`start_date` AND `s`.`end_date`))) LEFT JOIN (SELECT `s1`.`id` AS `id`,SUM(`t1`.`hours_planned`) AS `TargetHours` FROM (`sprints` `s1` LEFT JOIN `tasks` `t1` ON((`s1`.`id` = `t1`.`sprint_id`))) GROUP BY `s1`.`id`) `g` ON((`g`.`id` = `s`.`id`))) LEFT JOIN `tasks` `t` ON(((`s`.`id` = `t`.`sprint_id`) AND (`c`.`date` = CAST(`t`.`updated_at` AS DATE)) AND (`t`.`status` = 5)))) WHERE (`s`.`id` >= 1) GROUP BY `s`.`id`,`c`.`date`,`g`.`TargetHours`) AS `bdh` ORDER BY `bdh`.`Sprintid` ASC, `bdh`.`CalendarDate` ASC ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sprint_burn_task_view');
    }
};
