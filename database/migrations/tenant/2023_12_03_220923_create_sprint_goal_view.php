<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `sprint_goal_view`  AS SELECT `goal`.`Sprint` AS `Sprint`, `goal`.`Goal` AS `Goal`, SUM(`goal`.`No_of_Task`) AS `No_of_Task`, SUM(`goal`.`Done`) AS `Done`, SUM(`goal`.`Completed`) AS `Completed`, SUM(`goal`.`TasksOutstanding`) AS `TasksOutstanding`, SUM(`goal`.`OverdueTasks`) AS `OverdueTasks`, SUM(`goal`.`HoursPlanned`) AS `HoursPlanned`, SUM(`goal`.`Impediments`) AS `Impediments`, SUM(`goal`.`ActualHours`) AS `ActualHours` FROM (SELECT `s`.`name` AS `Sprint`,`g`.`name` AS `Goal`,COUNT(`t`.`id`) AS `No_of_Task`,SUM((CASE WHEN (`t`.`status` = 4) THEN 1 ELSE 0 END)) AS `Done`,SUM((CASE WHEN (`t`.`status` = 5) THEN 1 ELSE 0 END)) AS `Completed`,SUM((CASE WHEN (`t`.`status` IN (1,2,3)) THEN 1 ELSE 0 END)) AS `TasksOutstanding`,SUM((CASE WHEN ((`t`.`end_date` < NOW()) AND (`t`.`status` IN (1,2,3))) THEN 1 ELSE 0 END)) AS `OverdueTasks`,SUM((CASE WHEN (`t`.`status` = 6) THEN 1 ELSE 0 END)) AS `Impediments`,SUM(`t`.`hours_planned`) AS `HoursPlanned`,0 AS `ActualHours` FROM ((`sprints` `s` LEFT JOIN `goals` `g` ON((`s`.`id` = `g`.`sprint_id`))) LEFT JOIN `tasks` `t` ON((`g`.`id` = `t`.`goal_id`))) GROUP BY `s`.`name`,`g`.`name` UNION ALL SELECT `s`.`name` AS `Sprint`,`g`.`name` AS `Goal`,0 AS `No_of_Task`,0 AS `Done`,0 AS `Completed`,0 AS `TasksOutstanding`,0 AS `OverdueTasks`,0 AS `HoursPlanned`,0 AS `Impediments`,SUM((CASE WHEN ((`p`.`project_type_id` = 1) AND (`ts`.`billable` = 'Yes')) THEN `ts`.`time_decimal` WHEN (`p`.`project_type_id` = 2) THEN `ts`.`time_decimal` ELSE 0 END)) AS `ActualHours` FROM ((((`sprints` `s` LEFT JOIN `goals` `g` ON((`s`.`id` = `g`.`sprint_id`))) LEFT JOIN `tasks` `t` ON((`g`.`id` = `t`.`goal_id`))) LEFT JOIN `projects` `p` ON((`s`.`project_id` = `p`.`id`))) LEFT JOIN `timesheet_view` `ts` ON((`ts`.`task_id` = `t`.`id`))) GROUP BY `s`.`name`,`g`.`name`) AS `goal` GROUP BY `goal`.`Sprint`, `goal`.`Goal` ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS sprint_goal_view');
    }
};
