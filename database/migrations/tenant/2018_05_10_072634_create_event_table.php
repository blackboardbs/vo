<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event', function (Blueprint $table) {
            $table->increments('id');
            $table->text('event');
            $table->integer('row_order')->nullable();
            $table->date('event_date')->nullable();
            $table->string('role_ids')->nullable();
            $table->string('team_ids')->nullable();
            $table->string('user_ids')->nullable();
            $table->string('project_ids')->nullable();
            $table->text('details')->nullable();
            $table->string('document_name')->nullable();
            $table->string('document_url')->nullable();
            $table->string('link')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event');
    }
};
