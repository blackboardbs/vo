<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('digisign_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_sign_date')->nullable();
            $table->integer('document_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('user_number')->nullable();
            $table->integer('signed')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('is_claim_approver')->nullable();
            $table->integer('owner_id')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('digisign_users');
    }
};
