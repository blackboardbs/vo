<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->text('description')->nullable();
            $table->text('detail')->nullable();
            $table->date('due_date')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->integer('assigned_user_id')->nullable();
            $table->string('related_users')->nullable();
            $table->date('acceptance_date')->nullable();
            $table->date('completed_date')->nullable();
            $table->integer('rejection_reason_id')->nullable();
            $table->date('reject_timestamp')->nullable();
            $table->integer('original_user_id')->nullable();
            $table->date('re_assign_date')->nullable();
            $table->tinyInteger('email_user')->nullable();
            $table->integer('related_project_id')->nullable();
            $table->integer('related_task_id')->nullable();
            $table->integer('related_customer_id')->nullable();
            $table->integer('related_job_spec_id')->nullable();
            $table->integer('related_customer_invoice_id')->nullable();
            $table->integer('related_vendor_invoice_id')->nullable();
            $table->integer('priority_id')->nullable();
            $table->string('document_name')->nullable();
            $table->string('document_url')->nullable();
            $table->string('link')->nullable();
            $table->tinyInteger('send_notification_assigned')->nullable();
            $table->tinyInteger('send_notification_created')->nullable();
            $table->tinyInteger('send_notification_related')->nullable();
            $table->integer('team_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('prospect_id')->nullable();
            $table->integer('estimated_effort')->nullable();
            $table->integer('actual_hours')->nullable();
            $table->integer('dependency_id')->nullable();
            $table->integer('board_id')->nullable();
            $table->integer('sprint_id')->nullable();
            $table->integer('emote_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('actions');
    }
};
