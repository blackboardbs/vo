<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('default_dashboards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->text('top_component');
            $table->text('charts_tables');
            $table->string('dashboard_name');
            $table->integer('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('default_dashboards');
    }
};
