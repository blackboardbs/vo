<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_onboarding_appointing_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_onboarding_id')->nullable();
            $table->unsignedInteger('contract_type_id')->nullable();
            $table->unsignedInteger('payment_base_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('position')->nullable();
            $table->unsignedInteger('annual_salary')->nullable();
            $table->integer('leave_days')->nullable();
            $table->smallInteger('notice_period')->nullable();
            $table->unsignedInteger('reporting_manager')->nullable();
            $table->string('other_income')->nullable();
            $table->text('other_conditions')->nullable();
            $table->boolean('require_laptop')->nullable();
            $table->boolean('require_phone')->nullable();
            $table->boolean('require_mobile_internet')->nullable();
            $table->text('notes')->nullable();
            $table->text('responsibilities')->nullable();
            $table->unsignedInteger('payment_type_id')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->dateTime('issued_at')->nullable();
            $table->dateTime('recieved_at')->nullable();
            $table->dateTime('signed_at')->nullable();
            $table->dateTime('filed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_onboarding_appointing_managers');
    }
};
