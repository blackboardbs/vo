<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('distribution_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('from_query')->nullable();
            $table->text('to_query')->nullable();
            $table->text('from_query_type_id')->nullable();
            $table->text('to_query_type_id')->nullable();
            $table->text('to_query_filters')->nullable();
            $table->text('from_query_filters')->nullable();
            $table->integer('run_frequency_id')->nullable();
            $table->timestamp('last_run_date')->nullable();
            $table->timestamp('next_run_date')->nullable();
            $table->string('from')->nullable();
            $table->string('subject')->nullable();
            $table->text('email_body')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('distribution_lists');
    }
};
