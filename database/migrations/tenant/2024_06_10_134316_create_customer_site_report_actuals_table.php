<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('customer_site_report_actuals', function (Blueprint $table) {
            $table->id();
            $table->integer('site_report_id')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('task_status')->nullable();
            $table->string('task')->nullable();
            $table->decimal('total_hours',10,2);
            $table->decimal('hours_billable',10,2);
            $table->decimal('hours_non_billable',10,2);
            $table->text('notes');
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customer_site_report_actuals');
    }
};
