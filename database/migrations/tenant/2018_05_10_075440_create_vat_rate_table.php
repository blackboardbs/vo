<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vat_rate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->decimal('vat_rate', 5,2)->default('0.00');
            $table->integer('vat_code')->default(1);
            $table->date('start_date')->default(Carbon\Carbon::now()->toDateString());
            $table->date('end_date')->default(Carbon\Carbon::now()->toDateString());
            $table->integer('status');
            $table->integer('creator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vat_rate');
    }
};
