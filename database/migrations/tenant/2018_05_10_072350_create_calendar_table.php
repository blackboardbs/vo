<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('calendar', function (Blueprint $table) {
            $table->increments('calendar_id');
            $table->date('date');
            $table->integer('year');
            $table->integer('month');
            $table->integer('week');
            $table->integer('day');
            $table->string('day_name');
            $table->date('start_of_week_date');
            $table->date('end_of_week_date');
            $table->char('weekday');
            $table->char('weekend');
            $table->char('public_holiday');
            $table->integer('mon');
            $table->integer('tue');
            $table->integer('wed');
            $table->integer('thu');
            $table->integer('fri');
            $table->integer('sat');
            $table->integer('sun');
            $table->integer('status');
            $table->integer('creator_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('calendar');
    }
};
