<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sh_master_data', function (Blueprint $table) {
            $table->id();
            $table->integer('category')->nullable();
            $table->string('kpi')->nullable();
            $table->string('unit_of_measure')->nullable();
            $table->text('definition')->nullable();
            $table->text('problem')->nullable();
            $table->text('resolution')->nullable();
            $table->string('cf_coulour_01')->nullable();
            $table->string('cf_coulour_02')->nullable();
            $table->string('cf_coulour_03')->nullable();
            $table->integer('health_score_01')->nullable();
            $table->integer('health_score_02')->nullable();
            $table->integer('health_score_03')->nullable();
            $table->text('measure_calculation')->nullable();
            $table->string('list_url')->nullable();
            $table->string('list_filters')->nullable();
            $table->text('fix')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sh_master_data');
    }
};
