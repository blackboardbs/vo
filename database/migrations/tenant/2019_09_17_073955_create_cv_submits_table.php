<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cv_submits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sent_to_email')->nullable();
            $table->text('sent_to_email_cc')->nullable();
            $table->text('sent_to_email_bcc')->nullable();
            $table->float('commission')->nullable();
            $table->string('subject')->nullable();
            $table->integer('cv_id')->nullable();
            $table->integer('job_spec_id')->nullable();
            $table->float('rate')->nullable();
            $table->float('ctc')->nullable();
            $table->integer('template')->nullable();
            $table->integer('format')->nullable();
            $table->text('text_message')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('creator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cv_submits');
    }
};
