<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_onboardings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('title_id')->nullable();
            $table->string('Known_as')->nullable();
            $table->string('id_number')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('country_id')->nullable();
            $table->text('directive_number')->nullable();
            $table->bigInteger('income_tax_number')->nullable();
            $table->string('id_passport')->nullable();
            $table->string('proof_of_address')->nullable();
            $table->string('popia')->nullable();
            $table->boolean('popia_accept')->nullable();
            $table->dateTime('commited_at')->nullable();
            $table->dateTime('appointment_confirmed_at')->nullable();
            $table->unsignedInteger('status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_onboardings');
    }
};
