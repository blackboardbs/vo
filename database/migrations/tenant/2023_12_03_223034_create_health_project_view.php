<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW `health_project_view`  AS WITH     `cte_A` AS (SELECT `assignment`.`project_id` AS `project_id`,SUM(`assignment`.`hours`) AS `hours` FROM `assignment` GROUP BY `assignment`.`project_id`), `cte_T` AS (SELECT `T`.`project_id` AS `project_id`,SUM((CASE WHEN ((`T`.`project_type_id` = 1) AND (`T`.`is_billable` = 1)) THEN `T`.`time_decimal` ELSE 0 END)) AS `bill_hours`,SUM((CASE WHEN (`T`.`project_type_id` = 2) THEN `T`.`time_decimal` ELSE 0 END)) AS `nonbill_hours` FROM `timesheet_view` `T` GROUP BY `T`.`project_id`) SELECT `P`.`id` AS `project_id`,`P`.`status_id` AS `project_status`,`P`.`project_type_id` AS `project_type_id`,`A`.`hours` AS `assigned_hours`,`T`.`bill_hours` AS `bill_hours`,`T`.`nonbill_hours` AS `nonbill_hours` FROM ((`projects` `P` LEFT JOIN `cte_A` `A` ON((`P`.`id` = `A`.`project_id`))) LEFT JOIN `cte_T` `T` ON((`P`.`id` = `T`.`project_id`))) WHERE (`P`.`status_id` < 4)  ;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS health_project_view');
    }
};
