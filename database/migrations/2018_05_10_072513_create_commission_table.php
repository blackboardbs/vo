<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commission', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->integer('hour01')->nullable();
            $table->integer('comm01')->default('0')->nullable();
            $table->integer('hour02')->nullable();
            $table->integer('comm02')->default('0')->nullable();
            $table->integer('hour03')->nullable();
            $table->integer('comm03')->default('0')->nullable();
            $table->integer('hour04')->nullable();
            $table->integer('comm04')->default('0')->nullable();
            $table->integer('hour05')->nullable();
            $table->integer('comm05')->default('0')->nullable();
            $table->integer('hour06')->nullable();
            $table->integer('comm06')->default('0')->nullable();
            $table->integer('hour07')->nullable();
            $table->integer('comm07')->default('0')->nullable();
            $table->integer('hour08')->nullable();
            $table->integer('comm08')->default('0')->nullable();
            $table->integer('hour09')->nullable();
            $table->integer('comm09')->default('0')->nullable();
            $table->integer('hour10')->nullable();
            $table->integer('comm10')->default('0')->nullable();
            $table->integer('hour11')->nullable();
            $table->integer('comm11')->default('0')->nullable();
            $table->integer('hour12')->nullable();
            $table->integer('comm12')->default('0')->nullable();
            $table->integer('min_rate')->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('creator_id')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commission');
    }
};
