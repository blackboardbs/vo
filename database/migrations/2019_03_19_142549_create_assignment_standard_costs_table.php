<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assignment_standard_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->decimal('standard_cost_rate',6,2);
            $table->decimal('min_rate', 12,2);
            $table->decimal('max_rate', 12,2);
            $table->integer('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assignment_standard_costs');
    }
};
