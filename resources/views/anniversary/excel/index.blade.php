<table>
    <thead>
    <tr>
        <th>Anniversary Date</th>
        <th>Anniversary Type</th>
        <th>Name</th>
        <th>Relationship</th>
        <th>Resource</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($anniversary as $referrer)
        <tr>
            <td>{{$referrer->anniversary_date}}</td>
            <td>{{$referrer->anniversary_type?->description}}</td>
            <td>{{$referrer->name}}</td>
            <td>{{$referrer->relationshipd?->description}}</td>
            <td>{{$referrer->resource?->name()}}</td>
            <td>{{$referrer->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>