@extends('adminlte.default')

@section('title') Add Anniversary @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            {{Form::open(['url' => route('anniversary.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Anniversary Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('anniversary_date',old('anniversary_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('anniversary_date') ? ' is-invalid' : ''),'placeholder' => 'Anniversary Date'])}}
                        @foreach($errors->get('anniversary_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Relationship:</th>
                    <td>{{Form::select('relationship',$relationship,old('relationship'),['class'=>'form-control form-control-sm '. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'Relationship'])}}
                        @foreach($errors->get('amount') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td colspan="3">{{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Linked To: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('emp_id',$users_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_id') ? ' is-invalid' : ''),'id'=>'resource'])}}
                        @foreach($errors->get('emp_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Anniversary: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('anniversary',$anniversary_type,old('anniversary'),['class'=>'form-control form-control-sm '. ($errors->has('anniversary') ? ' is-invalid' : ''),'id'=>'anniversary'])}}
                        @foreach($errors->get('anniversary') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{Form::text('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',old('phone'),['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Email:</th>
                    <td>{{Form::select('contact_email',array('1' => 'Yes','2' => 'No'),null,['class'=>'form-control form-control-sm'. ($errors->has('contact_email') ? ' is-invalid' : ''),'id'=>'contact_email'])}}
                        @foreach($errors->get('contact_email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Phone:</th>
                    <td>{{Form::select('contact_phone',array('1' => 'Yes','2' => 'No'),null,['class'=>'form-control form-control-sm'. ($errors->has('contact_phone') ? ' is-invalid' : ''),'id'=>'contact_phone'])}}
                        @foreach($errors->get('contact_phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection