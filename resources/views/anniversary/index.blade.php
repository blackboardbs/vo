@extends('adminlte.default')

@section('title') Anniversary @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('anniversary.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Anniversary</a>
            <x-export route="anniversary.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('anniversary.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('anniversary_date', 'Anniversary Date')</th>
                    <th>@sortablelink('anniversary', 'Anniversary Type')</th>
                    <th>@sortablelink('name', 'Name')</th>
                    <th>@sortablelink('relationshipd.description', 'Relationship')</th>
                    <th>@sortablelink('resource.first_name', 'Resource')</th>
                    <th>@sortablelink('status.description','Status')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($anniversary as $referrer)
                    <tr>
                        <td><a href="{{route('anniversary.show',$referrer)}}">{{$referrer->anniversary_date}}</a></td>
                        <td>{{$referrer->anniversary_type?->description}}</td>
                        <td>{{$referrer->name}}</td>
                        <td>{{$referrer->relationshipd?->description}}</td>
                        <td>{{$referrer->resource?->name()}}</td>
                        <td>{{$referrer->status?->description}}</td>
                        @if($can_update)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('anniversary.edit',$referrer)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    @if(Auth::user()->hasRole('admin'))
                                        {{ Form::open(['method' => 'DELETE','route' => ['anniversary.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                        {{ Form::close() }}
                                    @endif
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No anniversaries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $anniversary->firstItem() }} - {{ $anniversary->lastItem() }} of {{ $anniversary->total() }}
                        </td>
                        <td>
                            {{ $anniversary->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
