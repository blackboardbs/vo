@extends('adminlte.default')

@section('title') View Anniversary @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('anniversary.edit', $anniversary)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Anniversary</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Anniversary Date:</th>
                    <td>{{$anniversary->anniversary_date}}</td>
                    <th>Relationship:</th>
                    <td>{{$anniversary->relationshipd?->description}}</td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td colspan="3">{{$anniversary->name}}</td>
                </tr>
                <tr>
                    <th>Linked To:</th>
                    <td>{{$anniversary->resource?->name()}}</td>
                    <th>Anniversary:</th>
                    <td>{{$anniversary->anniversary_type?->description}}</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{$anniversary->email}}</td>
                    <th>Phone:</th>
                    <td>{{$anniversary->phone}}</td>
                </tr>
                <tr>
                    <th>Contact Email:</th>
                    <td>{{($anniversary->contact_email == '1' ? 'Yes' : 'No' )}}</td>
                    <th>Contact Phone:</th>
                    <td>{{($anniversary->contact_phone == '1' ? 'Yes' : 'No')}}</td>
                </tr>

                <tr>
                    <th>Status:</th>
                    <td>{{$anniversary->status?->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
