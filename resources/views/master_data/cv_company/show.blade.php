@extends('adminlte.default')

@section('title') View Master CV Company @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('cv_company.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('cv_company.edit', $cv_company[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($cv_company as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th rowspan="2">CV Company Logo</th>
                        <td rowspan="2"><img src="{{route('cv_company_logo',['q'=>$result->logo])}}" id="company-preview-large" class="company-avatar company-avatar-profile"/></td>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$result->statusd['description']}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection