@extends('adminlte.default')

@section('title') View Master Payment Terms @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('payment_terms.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('payment_terms.edit', $payment_term)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$payment_term->description}}</td>
                    <th>Status</th>
                    <td>{{$payment_term->status->description}}</td>
                </tr>
                </table>

        </div>
    </div>
@endsection