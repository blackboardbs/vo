@extends('adminlte.default')

@section('title') View Master Account Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('master_bankaccounttype.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('master_bankaccounttype.edit', $account_type)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th class="bg-gray-light">Description</th>
                        <td>{{$account_type->description}}</td>
                        <th class="bg-gray-light">Status</th>
                        <td>{{$account_type->status->description}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection