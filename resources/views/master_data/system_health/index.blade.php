@extends('adminlte.default')

@section('title') Master Data - System Health @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('system_health_master.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> System Health</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form id="searchform" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('system_health_master.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th class="last">Category</th>
                    <th>KPI</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($systems as $system)
                    <tr>
                        <td class="pr-3" nowrap><a href="{{route('system_health_master.show',$system)}}">{{$system->category_name->name}}</a></td>
                        <td>{{$system->kpi}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('system_health_master.edit',$system)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['system_health_master.destroy', $system],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data template style entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $systems->firstItem() }} - {{ $systems->lastItem() }} of {{ $systems->total() }}
                        </td>
                        <td>
                            {{ $systems->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
