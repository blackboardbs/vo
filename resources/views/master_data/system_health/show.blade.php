@extends('adminlte.default')

@section('title') View Master System Health @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('system_health_master.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('system_health_master.edit', $system)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <hr>
    <div class="table-responsive">
        {{Form::open(['url' => route('system_health_master.update',$system), 'method' => 'put','class'=>'mt-3','id'=>'saveForm'])}}
        {{-- @foreach($systemsh as $systemh) --}}
        <table class="table table-bordered table-sm">
            <tr>
                <th>Category</th>
                <td colspan="2">{{ $system->category_name->name }}</td>
                    <th>Unit of Measure</th>
                    <td colspan="2">{{ $system->unit_of_measure }}</td>
            </tr>
            <tr>
                <th>KPI</th>
                <td colspan="5">{{ $system->kpi }}</td>
            </tr>
            <tr>
                <th>Definition</th>
                <td colspan="5">{{ $system->definition }}</td>
            </tr>
            <tr>
                <th>Problem</th>
                <td colspan="5">{{ $system->problem }}</td>
            </tr>
            <tr>
                <th>Resolution</th>
                <td colspan="5">{{ $system->resolution }}</td>
            </tr>
            <tr>
                <th>Formatting Rule 1</th>
                <td>{{ $system->cf_coulour_01 }}</td>
                <th>Formatting Rule 2</th>
                <td>{{ $system->cf_coulour_02 }}</td>
                <th>Formatting Rule 3</th>
                <td>{{ $system->cf_coulour_03 }}</td>
            </tr>
            <tr>
                <th>Health Score 1</th>
                <td>{{ $system->health_score_01 }}</td>
                    <th>Health Score 2</th>
                    <td>{{ $system->health_score_02 }}</td>
                    <th>Health Score 3</th>
                    <td>{{ $system->health_score_03 }}</td>
            </tr>
            <tr>
                <th>Measure Calculation</th>
                <td colspan="5">{{ $system->measure_calculation }}</td>
            </tr>
            <tr>
                <th>URL</th>
                <td colspan="2">{{ $system->list_url }}</td>
                <th>Filters</th>
                <td colspan="2">{{ $system->list_filters }}</td>
            </tr>
            <tr>
                <th>Fix</th>
                <td colspan="5">{{ $system->fix }}</td>
            </tr>
        </table>
        {{-- @endforeach --}}
        {{Form::close()}}
    </div>
</div>
@endsection