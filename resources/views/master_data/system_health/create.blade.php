@extends('adminlte.default')

@section('title') Create Master System Health @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('system_health_master.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary float-right ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')<div class="container-fluid">
    <hr>
    <div class="table-responsive">
        {{Form::open(['url' => route('system_health_master.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
        {{-- @foreach($systemsh as $systemh) --}}
        <table class="table table-bordered table-sm">
            <tr>
                <th>Category <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="2">{{Form::select('category',$categories_dropdown,(isset($_GET['category']) && $_GET['category'] != '' ? $_GET['category'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('category') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('category') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                    <th>Unit of Measure <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="2">{{ Form::text('unit_of_measure', (isset($_GET['unit_of_measure']) ? $_GET['unit_of_measure'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('unit_of_measure') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                        @foreach($errors->get('unit_of_measure') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
            </tr>
            <tr>
                <th>KPI <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::text('kpi', (isset($_GET['kpi']) ? $_GET['kpi'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('kpi') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('kpi') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Definition <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::textarea('definition', (isset($_GET['definition']) ? $_GET['definition'] : ''), ['rows'=>'3','class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('definition') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Problem <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::textarea('problem', (isset($_GET['problem']) ? $_GET['problem'] : ''), ['rows'=>'3','class'=>'form-control form-control-sm'. ($errors->has('problem') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('problem') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Resolution <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::textarea('resolution', (isset($_GET['resolution']) ? $_GET['resolution'] : ''), ['rows'=>'3','class'=>'form-control form-control-sm'. ($errors->has('resolution') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('resolution') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Formatting Rule 1 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('cf1',$cf_dropdown,(isset($_GET['cf1']) ? $_GET['cf1'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('cf1') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('cf1') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                <th>Formatting Rule 2 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('cf2',$cf_dropdown,(isset($_GET['cf2']) ? $_GET['cf2'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('cf2') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('cf2') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                <th>Formatting Rule 3 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('cf3',$cf_dropdown,(isset($_GET['cf3']) ? $_GET['cf3'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('cf3') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('cf3') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Health Score 1 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::text('hs1',(isset($_GET['hs1']) ? $_GET['hs1'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('hs1') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('hs1') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                    <th>Health Score 2 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('hs2', (isset($_GET['hs2']) ? $_GET['hs2'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('hs2') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                        @foreach($errors->get('hs2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Health Score 3 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('hs3', (isset($_GET['hs3']) ? $_GET['hs3'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('hs3') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                        @foreach($errors->get('hs3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
            </tr>
            <tr>
                <th>Measure Calculation <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::textarea('measure_calculation', (isset($_GET['measure_calculation']) ? $_GET['measure_calculation'] : ''), ['rows'=>'3','class'=>'form-control form-control-sm'. ($errors->has('measure_calculation') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('measure_calculation') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>URL <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="2">{{Form::text('url',(isset($_GET['url']) ? $_GET['url'] : ''),['class'=>'form-control form-control-sm'. ($errors->has('url') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('url') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                <th>Filters <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="2">{{ Form::text('filters', (isset($_GET['filters']) ? $_GET['filters'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('filters') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('filters') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Fix</th>
                <td colspan="5">{{ Form::textarea('fix', (isset($_GET['fix']) ? $_GET['fix'] : ''), ['rows'=>'3','class'=>'form-control form-control-sm'. ($errors->has('fix') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('fix') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
        </table>
        {{-- @endforeach --}}
        {{Form::close()}}
    </div>
</div>
@endsection