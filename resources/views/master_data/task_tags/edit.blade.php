@extends('adminlte.default')

@section('title') Edit Master Task Tag @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('tasktags.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary float-right ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('tasktags.update',$tasktag->id), 'method' => 'put','class'=>'mt-3','id'=>'saveForm'])}}
        <table class="table table-bordered table-sm">
            <tr>
                <th>Icon <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    <div class="input-group">
                        <div style="width: 50%; margin-top:5px;">
                            <input hidden type="text" id="icon" name="icon"  value="{{ old('icon', $tasktag->icon) }}">
                            <button type="button" style="border: 1px solid rgba(95, 95, 95, 0.264);" class="btn btn-default btn-sm" role="iconpicker" data-iconset="fontawesome5" data-search="true" data-search-text="Search icons..." data-placement="bottom"></button>
                            <span style="float: right;">Old: {!!$tasktag->icon!!}</span>
                        </div>
                    </div>
                    @foreach($errors->get('icon') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Colour <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    <div class="input-group">
                        <div style="width: 50%; margin-top:5px;">
                            <input type="color" class="form-control" id="color" name="color" value="{{ old('color', $tasktag->color) }}">
                        </div>
                    </div>
                    @foreach($errors->get('color') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="2">{{ Form::text('name', ($tasktag->name), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('name') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('status',$status_dropdown,$tasktag->status,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                    @foreach($errors->get('status') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::text('description', ($tasktag->description), ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('description') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
        </table>
        {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/js/bootstrap-iconpicker.bundle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            
            $('[role="iconpicker"]').iconpicker();

            $('[role="iconpicker"]').on('change', function(event) {
                const selectedIcon = $('[role="iconpicker"]').find('input').val();
                const color = $('#color').val();
                const newIconHtml = `<i class="${selectedIcon} fa-lg" style="color: ${color};"></i>`;
                $('#icon').val(newIconHtml);
            });

            $('#color').on('change', function() {
                const selectedIcon = $('[role="iconpicker"]').find('input').val();
                const oldIcon = $('#icon').val();
                const color = $(this).val();
                if (selectedIcon == 'empty') {
                    const newIconHtml = oldIcon.replace(/color:\s*#[0-9a-fA-F]{6};/, `color: ${color};`);
                    $('#icon').val(newIconHtml);
                } else {
                    const newIconHtml = `<i class="${selectedIcon} fa-lg" style="color: ${color};"></i>`;
                    $('#icon').val(newIconHtml);
                }
                // const newIconHtml = `<i class="${selectedIcon} fa-lg" style="color: ${color};"></i>`;
                // updateIconHtml(selectedIcon, color);
                // $('#icon').val(newIconHtml);
            });

            function updateIconHtml(iconClass, color) {
                const iconHtml = `<i class="${iconClass} fa-lg" style="color: ${color};"></i>`;
                $('#icon').val(iconHtml);
            }

        });

    </script>
@endsection