@extends('adminlte.default')

@section('title') View Master Task Tag @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('tasktags.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('tasktags.edit', $tasktag->id)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-sm">
            <tr>
                <th>Icon <small class="text-muted d-none d-sm-inline"></small></th>
                <td>
                    {!! $tasktag->icon !!}
                </td>
                <th>Colour <small class="text-muted d-none d-sm-inline"></small></th>
                <td style="background-color: {{ $tasktag->color }};"></td>
                <th>Name <small class="text-muted d-none d-sm-inline"></small></th>
                <td colspan="2">
                    {{$tasktag->name}}
                </td>
            </tr>
            <tr>
                <th>Status</th>
                <td>{{$tasktag->statusd->description}}</td>
                <th>Description <small class="text-muted d-none d-sm-inline"></small></th>
                <td colspan="5">
                    {{$tasktag->description}}
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection