@extends('adminlte.default')

@section('title') Create Master Task Tags @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('tasktags.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary float-right ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')<div class="container-fluid">
    <hr>
    <div class="table-responsive">
        {{Form::open(['url' => route('tasktags.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
        <table class="table table-bordered table-sm">
            <tr>
                <th>Icon <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    <div class="input-group">
                        <div style="width: 50%; margin-top:5px;">
                            {{-- <label for="icon" class="form-label">Icon</label> --}}
                            <input type="text" id="icon" name="icon" hidden>
                            <button type="button" style="border: 1px solid rgba(95, 95, 95, 0.264);" class="btn btn-default btn-sm" role="iconpicker" data-iconset="fontawesome5" data-search="true" data-search-text="Search icons..." data-placement="bottom"></button>
                        </div>
                    </div>
                    @foreach($errors->get('icon') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Colour <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    <div class="input-group">
                        <div style="width: 50%; margin-top:5px;">
                            {{-- <label for="color" class="form-label">Color</label> --}}
                            <input type="color" class="form-control" id="color" name="color" value="#000000">
                        </div>
                    </div>
                    @foreach($errors->get('color') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="2">{{ Form::text('name', (isset($_GET['name']) ? $_GET['name'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('name') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                    @foreach($errors->get('status') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td colspan="5">{{ Form::text('description', (isset($_GET['description']) ? $_GET['description'] : ''), ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                    @foreach($errors->get('description') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
        </table>
        {{Form::close()}}
    </div>
</div>
@endsection

@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/js/bootstrap-iconpicker.bundle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            // Initialize icon picker
            $('[role="iconpicker"]').iconpicker();

            // Update icon input and preview when icon is selected
            $('[role="iconpicker"]').on('iconpickerSelected', function(event) {
                const selectedIcon = event.iconpickerValue;
                const color = $('#color').val();
                updateIconHtml(selectedIcon, color);
            });

            // Update icon preview when color changes
            $('#color').on('change', function() {
                const selectedIcon = $('[role="iconpicker"]').find('input').val();
                const color = $(this).val();
                updateIconHtml(selectedIcon, color);
            });

            function updateIconHtml(iconClass, color) {
                const iconHtml = `<i class="${iconClass} fa-lg" style="color: ${color};"></i>`;
                $('#icon').val(iconHtml);
            }

        });

    </script>
@endsection