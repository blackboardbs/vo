@extends('adminlte.default')

@section('title') View Master Expense Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('expense_type.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('expense_type.edit', $expense_type)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$expense_type->description}}</td>
                    <th>Status</th>
                    <td>{{$expense_type->status->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection