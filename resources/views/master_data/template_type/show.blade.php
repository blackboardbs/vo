@extends('adminlte.default')

@section('title') View Master Template Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('master_template.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('master_template.edit', $template_type)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Template Type</a>
            </div>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$template_type->name}}</td>
                        <th>Status</th>
                        <td>{{$template_type->status->description}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection