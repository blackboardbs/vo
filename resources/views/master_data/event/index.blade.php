@extends('adminlte.default')

@section('title') Events @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))
        <a href="{{route('event.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Event</a>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                Matching<br>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}}
            </div>
            {{--Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>--}}
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('event_date','Event Date')</th>
                    <th>@sortablelink('event','Event')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))
                    <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($event as $result)
                    <tr>
                        <td>{{$result->event_date}}</td>
                        <td><a href="{{route('event.show',$result)}}">{{$result->event}}</a></td>
                        <td>{{isset($result->statusd->description)?$result->statusd->description:''}}</td>
                        @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))
                        <td>
                            <div class="d-flex">
                            <a href="{{route('event.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['event.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data event entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $event->links() }}
        </div>
    </div>
@endsection
