@extends('adminlte.default')

@section('title') Edit Event @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('event.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($event as $result)
                {{Form::open(['url' => route('event.update',['costid' => $result]), 'method' => 'post','class'=>'mt-3'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Event <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">{{ Form::text('description', $result->event, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Event Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::text('event_date', $result->event_date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('event_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>

                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown,$result->status,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                <table class="table table-borderless">
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </table>
                {{Form::close()}}
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection