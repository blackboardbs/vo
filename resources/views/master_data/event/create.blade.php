@extends('adminlte.default')

@section('title') Add Event @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('event.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('event.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Event <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('description', old('description'), ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Event Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('event_date',old('event_date'),['class'=>'form-control datepicker form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('event_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td colspan="3">
                        {{Form::select('roles',$roles_drop_down,old('roles'),['class'=>' form-control form-control-sm'. ($errors->has('roles') ? ' is-invalid' : ''),'id'=>'roles', 'multiple' => 'multiple'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                            <strong id="roles-error"></strong>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>Team</td>
                    <td colspan="3">
                        {{Form::select('teams',$teams_drop_down,old('teams'),['class'=>' form-control form-control-sm'. ($errors->has('teams') ? ' is-invalid' : ''),'id'=>'teams', 'multiple' => 'multiple'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                            <strong id="teams-error"></strong>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>User</td>
                    <td colspan="3">
                        {{Form::select('users',$users_drop_down,old('users'),['class'=>' form-control form-control-sm'. ($errors->has('users') ? ' is-invalid' : ''),'id'=>'users', 'multiple' => 'multiple'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                            <strong id="users-error"></strong>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>Project</td>
                    <td colspan="3">
                        {{Form::select('projects',$projects_drop_down,old('projects'),['class'=>' form-control form-control-sm'. ($errors->has('projects') ? ' is-invalid' : ''),'id'=>'projects', 'multiple' => 'multiple'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                            <strong id="projects-error"></strong>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>Detail</td>
                    <td colspan="3">
                        {{Form::textarea('detail',old('detail'),['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('detail') ? ' is-invalid' : ''),'id'=>'detail'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                                                                <strong id="detail-error"></strong>
                                                            </span>
                    </td>
                </tr>
                <tr>
                    <td>Link</td>
                    <td colspan="3">
                        {{Form::text('link',old('link'),['class'=>'form-control form-control-sm'. ($errors->has('link') ? ' is-invalid' : ''),'id'=>'link'])}}
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                                                                <strong id="link-error"></strong>
                                                            </span>
                    </td>
                </tr>
                <tr>
                    <td>Document</td>
                    <td colspan="3">
                        <input type="file" id="file" name="file" />
                    </td>
                </tr>
                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <table class="table table-borderless">
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection