@extends('adminlte.default')

@section('title') View Events @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{--<a href="{{route('event.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Event</a>--}}
        <a style="margin-right: 10px;" href="{{($path == 1 ? url()->previous() : route('event.index'))}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Event</th>
                    <td>{{$event->event}}</td>
                    <th>Event Date</th>
                    <td>{{$event->event_date}}</td>
                </tr>
                <tr>
                    <th>Roles</th>
                    <td colspan="3">
                        @foreach($roles as $key => $role)
                            {{((count($roles) - 1) != $key)?$role->name.' ,':$role->name}}
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Users</th>
                    <td colspan="3">
                        @foreach($users as $key=>$user)
                            {{((count($users) - 1) != $key)?$user->first_name.' '.$user->last_name.' ,':$user->first_name.' '.$user->last_name}}
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Teams</th>
                    <td colspan="3">
                        @foreach($teams as $key=>$team)
                            {{((count($teams) - 1) != $key)?$team->team_name.' ,':$team->team_name}}
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Projects</th>
                    <td colspan="3">
                        @foreach($projects as $key=>$project)
                            {{((count($projects) - 1) != $key)?$project->name.' ,':$project->name}}
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Documents</th>
                    <td colspan="3">
                        @foreach($documents as $document)
                            <a href="{{route('events.files', $document)}}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$event->statusd->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection