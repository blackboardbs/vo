@extends('adminlte.default')

@section('title') View Master Title @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('sprintstatus.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('sprintstatus.edit', $sprintstatus)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Sprint Status</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$sprintstatus->description}}</td>
                    <th>Status</th>
                    <td>{{$sprintstatus->status?->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection