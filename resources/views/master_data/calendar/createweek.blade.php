@extends('adminlte.default')

@section('title') Add Master Calendar @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('master_calendar.storerange'), 'method' => 'post','class'=>'mt-3', 'autocomplete'=>'off','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Start Date</th>
                    <td>{{Form::text('start_date',old('start_date'),['class'=>'datepicker start_date form-control form-control-sm'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder' => 'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>End Date</th>
                    <td>{{Form::text('end_date',old('end_date'),['class'=>'datepicker end_date form-control form-control-sm'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder' => 'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    /**
     *  Add a getWeek() method in Javascript inbuilt Date object.
     * This function is the colsest I could find which is ISO-8601 compatible. This is what php's `Date->format('w')` uses.
     * ISO-8601 means.
     *    Week starts from Monday.
     *    Week 1 is the week with first thurday of the year or the week which has 4th jan in it.
     * @param  {[Date]}   Prototype binding with Date Object.
     * @return {[Int]}    Integer from 1 - 53 which denotes the week of the year.
     */
    Date.prototype.getWeek = function () {
        var target  = new Date(this.valueOf());
        var dayNr   = (this.getDay() + 6) % 7;
        target.setDate(target.getDate() - dayNr + 3);
        var firstThursday = target.valueOf();
        target.setMonth(0, 1);
        if (target.getDay() != 4) {
            target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
        }
        return 1 + Math.ceil((firstThursday - target) / 604800000);
    }

    function getDateRangeOfWeek(weekNo){
        var d1 = new Date('2018-09-08');
        numOfdaysPastSinceLastMonday = eval(d1.getDay()- 1);
        d1.setDate(d1.getDate() - numOfdaysPastSinceLastMonday);
        var weekNoToday = d1.getWeek();
        var weeksInTheFuture = eval( weekNo - weekNoToday );
        d1.setDate(d1.getDate() + eval( 7 * weeksInTheFuture ));
        var rangeIsFrom = eval(d1.getMonth()+1) +"/" + d1.getDate() + "/" + d1.getFullYear();
        d1.setDate(d1.getDate() + 6);
        var rangeIsTo = eval(d1.getMonth()+1) +"/" + d1.getDate() + "/" + d1.getFullYear() ;
        return rangeIsFrom + " to "+rangeIsTo;
    };

    console.log(getDateRangeOfWeek($('week_num').val()));
/*while(start <= end){

var mm = ((start.getMonth()+1)>=10)?(start.getMonth()+1):'0'+(start.getMonth()+1);
var dd = ((start.getDate())>=10)? (start.getDate()) : '0' + (start.getDate());
var yyyy = start.getFullYear();
var date = dd+"/"+mm+"/"+yyyy; //yyyy-mm-dd

//alert(date);
    alert(getDateOfWeeksAway(ISO8601_week_no(start), 27, 10, 2016));

start = new Date(start.setDate(start.getDate() + 1)); //date increase by 1


    function getDateOfWeeksAway(w, d, m, y) {
        var numOfDaysAway = d + w * 7;
        return new Date(y, m-1, numOfDaysAway);
    }


}*/
</script>
    @endsection
