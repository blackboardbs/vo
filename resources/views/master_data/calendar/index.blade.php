@extends('adminlte.default')

@section('title') Master Data - Calendar @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('master_calendar.create',['type'=>'day'])}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Day</a>
        <a href="{{route('master_calendar.create',['type'=>'week'])}}" class="btn btn-dark btn-sm float-right" style="margin-right:5px;"><i class="fa fa-plus"></i> Date Range</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::date('date_from',old('date_from'),['class'=>'form-control form-control-sm  w-100'])}}
                        <span>From</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::date('date_to',old('date_to'),['class'=>'form-control form-control-sm  w-100'])}}
                        <span>To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <select class="form-control" name="status_filter" id="status_filter" style="width: 120px;">
                            <option value="0">All</option>
                            <option selected value="1">Active</option>
                            <option value="2">Suspended</option>
                            <option value="6">Closed</option>
                        </select>
                        <span>Status</span>
                    </label>
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <a href="{{ route('master_calendar.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('date','Date')</th>
                    <th>@sortablelink('year')</th>
                    <th>@sortablelink('month')</th>
                    <th>@sortablelink('week')</th>
                    <th>@sortablelink('day')</th>
                    <th>@sortablelink('day_name')</th>
                    <th>@sortablelink('public_holiday', 'Public Holiday')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($calendar as $result)
                    <tr>
                        <td><a href="{{route('master_calendar.show',$result)}}">{{$result->date}}</a></td>
                        <td>{{$result->year}}</td>
                        <td>{{$result->month}}</td>
                        <td>{{$result->week}}</td>
                        <td>{{$result->day}}</td>
                        <td>{{$result->day_name}}</td>
                        <td>{{$result->public_holiday}}</td>
                        <td>{{$result->statusd?->description}}</td>
                        <td>
                            <div class="d-flex">
                                <a href="{{route('master_calendar.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['master_calendar.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No calendar entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $calendar->firstItem() }} - {{ $calendar->lastItem() }} of {{ $calendar->total() }}
                        </td>
                        <td>
                            {{ $calendar->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function() {
            $('#status_filter').on('change', function() {
                this.form.submit();
            });
        });
    </script>

@endsection