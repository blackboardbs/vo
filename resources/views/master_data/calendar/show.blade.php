@extends('adminlte.default')

@section('title') View Master Calendar @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('master_calendar.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('master_calendar.edit', $mcalendar[0])}}" class="btn btn-success float-right ml-1" style="margin-left: 5px;">Edit</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($mcalendar as $result)
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Date</th>
                    <td>{{$result->date}}</td>
                    <th>Year</th>
                    <td>{{$result->year}}</td>
                </tr>
                <tr>
                    <th>Month</th>
                    <td>{{$result->month}}</td>
                    <th>Day</th>
                    <td>{{$result->day}}</td>
                </tr>
                <tr>
                    <th>Day Name</th>
                    <td>{{$result->day_name}}</td>
                    <th>Week</th>
                    <td>{{$result->week}}</td>
                </tr>
                <tr>
                    <th>Weekday</th>
                    <td>{{$result->weekday}}</td>
                    <th>Weekend</th>
                    <td>{{$result->weekend}}</td>
                <tr>
                    <th>Public Holiday</th>
                    <td>{{$result->public_holiday}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Mon</th>
                    <td>{{$result->mon}}</td>
                    <th>Tue</th>
                    <td>{{$result->tue}}</td>
                </tr>
                <tr>
                    <th>Wed</th>
                    <td>{{$result->wed}}</td>
                    <th>Thu</th>
                    <td>{{$result->thu}}</td>
                </tr>
                <tr>
                    <th>Fri</th>
                    <td>{{$result->fri}}</td>
                    <th>Sat</th>
                    <td>{{$result->sat}}</td>
                </tr>
                <tr>
                    <th>Sun</th>
                    <td>{{$result->sun}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Week Start Date</th>
                    <td>{{$result->start_of_week_date}}</td>
                    <th>Week End Date</th>
                    <td>{{$result->end_of_week_date}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$result->statusd->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            @endforeach
        </div>
    </div>
@endsection