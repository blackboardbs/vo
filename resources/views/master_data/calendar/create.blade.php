@extends('adminlte.default')

@section('title') Add Master Calendar @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('master_calendar.store'), 'method' => 'post','class'=>'mt-3', 'autocomplete'=>'off','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Date</th>
                    <td>{{Form::text('calendar_date',old('calendar_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('calendar_date') ? ' is-invalid' : ''),'placeholder' => 'Date'])}}
                        @foreach($errors->get('calendar_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Year</th>
                    <td><select name="year" class="year form-control form-control-sm  {{($errors->has('year') ? ' is-invalid' : '')}}">
                            <option value="{{(old('year') > 0 ? old('year') : 0)}}">{{(old('year') > 0 ? old('year') : 0)}}</option>
                            @for($i = date("Y"); $i > 1970; --$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                            @foreach($errors->get('year') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Month</th>
                    <td><select name="month" class="month form-control form-control-sm  {{($errors->has('month') ? ' is-invalid' : '')}}">
                            <option value="{{(old('month') > 0 ? old('month') : 0)}}">{{(old('month') > 0 ? old('month') : 0)}}</option>
                            @for($i = 1; $i < 13; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                            @foreach($errors->get('month') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                    <th>Day</th>
                    <td><select name="day" class="day form-control form-control-sm  {{($errors->has('day') ? ' is-invalid' : '')}}">
                            <option value="{{(old('day') > 0 ? old('day') : 0)}}">{{(old('day') > 0 ? old('day') : 0)}}</option>
                            @for($i = 1; $i < 32; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                            @foreach($errors->get('day') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Day Name</th>
                    <td><select name="dayname" class="dayname form-control form-control-sm  {{($errors->has('dayname') ? ' is-invalid' : '')}}">
                            <option value="{{(old('dayname') > 0 ? old('dayname') : 0)}}">{{(old('dayname') > 0 ? old('dayname') : 0)}}</option>

                            <option value="Sunday">Sunday</option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                            <option value="Friday">Friday</option>
                            <option value="Saturday">Saturday</option>

                            @foreach($errors->get('dayname') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                    <th>Week</th>
                    <td><select name="week" class="week form-control form-control-sm  {{($errors->has('week') ? ' is-invalid' : '')}}">
                            <option value="{{(old('week') > 0 ? old('week') : 0)}}">{{(old('week') > 0 ? old('week') : 0)}}</option>
                            @for($i = 1; $i < 53; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                            @foreach($errors->get('week') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Weekday</th>
                    <td><select name="weekday" class="weekday form-control form-control-sm  {{($errors->has('weekday') ? ' is-invalid' : '')}}">
                            <option value="{{(old('weekday') > 0 ? old('weekday') : 0)}}">{{(old('weekday') > 0 ? old('weekday') : 0)}}</option>

                            <option value="N">N</option>
                            <option value="Y">Y</option>

                            @foreach($errors->get('weekday') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                    <th>Weekend</th>
                    <td><select name="weekend" class="weekend form-control form-control-sm  {{($errors->has('weekend') ? ' is-invalid' : '')}}">
                            <option value="{{(old('weekend') > 0 ? old('weekend') : 0)}}">{{(old('weekend') > 0 ? old('weekend') : 0)}}</option>

                            <option value="N">N</option>
                            <option value="Y">Y</option>

                            @foreach($errors->get('weekend') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                <tr>
                    <th>Public Holiday</th>
                    <td><select name="publich" class="publich form-control form-control-sm  {{($errors->has('publich') ? ' is-invalid' : '')}}">
                            {{--<option value="{{(old('publich') > 'N' ? old('publich') : 'N')}}">{{(old('publich') > 'N' ? old('publich') : 'N')}}</option>--}}

                            <option value="N">N</option>
                            <option value="Y">Y</option>

                            @foreach($errors->get('publich') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Mon</th>
                    <td>{{Form::text('mon',old('mon'),['class'=>'mon form-control form-control-sm'. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('mon') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Tue</th>
                    <td>{{Form::text('tue',old('tue'),['class'=>'tue form-control form-control-sm'. ($errors->has('tue') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('tue') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Wed</th>
                    <td>{{Form::text('wed',old('wed'),['class'=>'wed form-control form-control-sm'. ($errors->has('wed') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('wed') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Thu</th>
                    <td>{{Form::text('thu',old('thu'),['class'=>'thu form-control form-control-sm'. ($errors->has('thu') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('thu') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fri</th>
                    <td>{{Form::text('fri',old('fri'),['class'=>'fri form-control form-control-sm'. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('fri') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Sat</th>
                    <td>{{Form::text('sat',old('sat'),['class'=>'sat form-control form-control-sm'. ($errors->has('tue') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('sat') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Sun</th>
                    <td>{{Form::text('sun',old('sun'),['class'=>'sun form-control form-control-sm'. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder' => ''])}}
                        @foreach($errors->get('sun') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Week Start Date</th>
                    <td>{{Form::text('start_of_week_date',old('start_of_week_date'),['class'=>'datepicker start_of_week_date form-control form-control-sm'. ($errors->has('start_of_week_date') ? ' is-invalid' : ''),'placeholder' => 'Start Date'])}}
                        @foreach($errors->get('start_of_week_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Week End Date</th>
                    <td>{{Form::text('end_of_week_date',old('end_of_week_date'),['class'=>'datepicker end_of_week_date form-control form-control-sm'. ($errors->has('end_of_week_date') ? ' is-invalid' : ''),'placeholder' => 'End Date'])}}
                        @foreach($errors->get('end_of_week_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });

        function previousMonth(d){
            var date = new Date($('#calendar_date').val());
            var lastDay = new Date(date.getFullYear(), (date.getMonth()-1) + 1, 0);
            var s = lastDay.getDate()+(d);
            return s;

        }

        function greaterDays(i){
            var s = i;
            console.log(s);
            var date = new Date($('#calendar_date').val());
            var d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var diff = (s - d.getDate());
            var f = diff;

            return f;
        }

        function negativeNumDays(i){
            var s = i;
            var date = new Date($('#calendar_date').val());
            var d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            if(s > d.getDate()) {

                var f = greaterDays(s);
            } else {
                var f = s;
            }
            if(f <= 0){

                f = previousMonth(s);
            }


            return f;
        }

        function negativeNumMonth(i) {
            var s = i;
            var date = new Date($('#calendar_date').val());
            var d = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            if (s < 0) {
                date.setDate(date.getDate() + s);
                var f = moment(new Date(date.toLocaleString())).format("YYYY-MM-DD");
            }
            if (s > 0) {
                var a = date.getDate();
                var c = a+s;
                date.setDate(c);
                console.log(s);
                var f = moment(new Date(date.toLocaleString())).format("YYYY-MM-DD");
            } else {

            }


            return f;
        }

        function getMondayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-6:1)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getTuesdayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-5:2)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getWednesdayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-4:3)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getThursdayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-3:4)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getFridayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-2:5)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getSaturdayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?-1:6)-day );
            var f = negativeNumDays(s);
            return f;
        }
        function getSundayOfCurrentWeek(d)
        {
            var day = d.getDay();
            var s = (d.getDate() + (day == 0?0:7)-day );
            var f = negativeNumDays(s);
            return f;
        }

        $('#calendar_date').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            showWeek:true,
            inline: true,
            onSelect: function(dateText, inst) {
                var date = new Date(dateText);
                // change date.GetDay() to date.GetDate()
                var currentWeekDay = date.getDay();
                if(currentWeekDay == 0) {
                    var lessDays = 6;
                } else {
                    var lessDays = currentWeekDay - 1;
                }
                var wkStart = new Date(new Date(date).setDate(date.getDate() - lessDays));
                var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));
                //var first = negativeNumMonth((date.getDate() - date.getDay()));
                //var last = first+7;
                //console.log(last);
                //console.log(date.getDay() + ' ' + date.getDate() + ' ' + date.getMonth() + ' ' + date.getFullYear() + ' ' + ($.datepicker.iso8601Week(date)-1));

                $('.year').val(date.getFullYear());
                $('.month').val(date.getMonth()+1);
                $('.day').val(date.getDate());
                var weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                $('.dayname').val(weekday[currentWeekDay]);
                $('.week').val($.datepicker.iso8601Week(date));
                if(currentWeekDay == '0' || currentWeekDay == '6'){
                    $('.weekday').val('N');
                    $('.weekend').val('Y');
                } else {
                    $('.weekday').val('Y');
                    $('.weekend').val('N');
                }
                $('.mon').val(getMondayOfCurrentWeek(date));
                $('.tue').val(getTuesdayOfCurrentWeek(date));
                $('.wed').val(getWednesdayOfCurrentWeek(date));
                $('.thu').val(getThursdayOfCurrentWeek(date));
                $('.fri').val(getFridayOfCurrentWeek(date));
                $('.sat').val(getSaturdayOfCurrentWeek(date));
                $('.sun').val(getSundayOfCurrentWeek(date));

                $('.start_of_week_date').val(wkStart.getFullYear()+'-'+(wkStart.getMonth() < 10 ? '0'+wkStart.getMonth() : wkStart.getMonth() )+'-'+(wkStart.getDate() < 10 ? '0'+wkStart.getDate() : wkStart.getDate() ));
                $('.end_of_week_date').val(wkEnd.getFullYear()+'-'+(wkEnd.getMonth() < 10 ? '0'+wkEnd.getMonth() : wkEnd.getMonth() )+'-'+(wkEnd.getDate() < 10 ? '0'+wkEnd.getDate() : wkEnd.getDate() ));

                $('.year').chosen().trigger("chosen:updated");
                $('.month').chosen().trigger("chosen:updated");
                $('.day').chosen().trigger("chosen:updated");
                $('.dayname').chosen().trigger("chosen:updated");
                $('.week').chosen().trigger("chosen:updated");
                $('.weekday').chosen().trigger("chosen:updated");
                $('.weekend').chosen().trigger("chosen:updated");
            }
        });


    </script>
@endsection
