@extends('adminlte.default')

@section('title') View Master Data Process Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('process_status.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('process_status.edit', $process_status)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Name</th>
                    <td>{{$process_status->name}}</td>
                    <th>Status</th>
                    <td>{{$process_status->status->description}}</td>
                </tr>
            </table>

        </div>
    </div>
@endsection