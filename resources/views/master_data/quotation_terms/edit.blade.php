@extends('adminlte.default')

@section('title') Edit Master Quotation Term @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($quotation_terms as $quotation_term)
                {{Form::open(['url' => route('quotation_terms.update',$quotation_term->id), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Quotation <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td> {{Form::select('quotation',$quotation_dropdown,$quotation_term->quotation_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('quotation') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Terms <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td width="40%"> {{Form::select('terms_id',$terms_dropdown,$quotation_term->terms_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('terms_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>

                    </tr>
                    <tr>
                        <th>Term <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('term', $quotation_term->term, ['class'=>'form-control form-control-sm'. ($errors->has('term') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('term') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Freeze</th>
                        <td>{{ Form::text('freeze', $quotation_term->freeze, ['class'=>'form-control form-control-sm'. ($errors->has('freeze') ? ' is-invalid' : '')]) }}
                                    @foreach($errors->get('freeze') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{Form::select('status',$status_dropdown,$quotation_term->status,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                                    @foreach($errors->get('status') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                {{Form::close()}}
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection