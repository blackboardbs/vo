@extends('adminlte.default')

@section('title') View Master Quotation Term @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('quotation_terms.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('quotation_terms.edit', $quotation_terms[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($quotation_terms as $quotation_term)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Quotation</th>
                        <td> {{$quotation_term->quotation->scope_of_work }}</td>
                        <th>Terms</th>
                        <td style="width: 35%"> {{$quotation_term->terms->term }}</td>

                    </tr>
                    <tr>
                        <th>Term</th>
                        <td>{{ $quotation_term->term }}</td>
                        <th>Freeze</th>
                        <td>{{ $quotation_term->freeze }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{ $quotation_term->statusd->description }}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection