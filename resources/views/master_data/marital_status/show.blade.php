@extends('adminlte.default')

@section('title') View Master Data Marital Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('marital_status.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('marital_status.edit', $marital_status[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($marital_status as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection