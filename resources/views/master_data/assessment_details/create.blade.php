@extends('adminlte.default')

@section('title') Add Master Assessment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('assessment_master_details.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Assessment Master <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('assessment_master_id',$assessment_master,1,['class'=>'form-control form-control-sm ','id'=>'assessment_master_id'])}}
                        @foreach($errors->get('assessment_master_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Assessment Group <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{ Form::text('assessment_group', old('assessment_group'), ['class'=>'form-control form-control-sm'. ($errors->has('assessment_group') ? ' is-invalid' : ''), 'id' => 'assessment_group', 'autocomplete' => 'off']) }}
                        @foreach($errors->get('assessment_group') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Assessment Measure <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('assessment_measure', old('assessment_measure'), ['class'=>'form-control form-control-sm'. ($errors->has('assessment_measure') ? ' is-invalid' : ''), 'id' => 'assessment_measure', 'autocomplete' => 'off']) }}
                        @foreach($errors->get('assessment_measure') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->assessment_group.'",' !!} @endforeach];

        $( "#assessment_group" ).autocomplete({
            source: autocomplete_elements
        });

        let autocomplete_elements2 = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->assessment_measure.'",' !!} @endforeach];

        $( "#assessment_measure" ).autocomplete({
            source: autocomplete_elements2
        });
    </script>
@endsection