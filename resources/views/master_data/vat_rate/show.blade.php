@extends('adminlte.default')

@section('title') View Master Vat Rate @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('vat_rate.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('vat_rate.edit', $vat_rate[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($vat_rate as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Vat Rate</th>
                        <td>{{$result->vat_rate}}%</td>
                    </tr>
                    <tr>
                        <th>Vat Code</th>
                        <td>{{$result->vat_code}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Rate Start Date</th>
                        <td>{{$result->start_date}}</td>
                        <th>Rate End Date</th>
                        <td>{{$result->end_date}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection