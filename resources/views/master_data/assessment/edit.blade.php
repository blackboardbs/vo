@extends('adminlte.default')

@section('title') Edit Master Data - Assessment KPI @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('assessment_master.update',['assessment_master' => $assessment]), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td colspan="3">{{ Form::text('description', $assessment->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Status</th>
                        <td>{{Form::select('status',$status_dropdown,$assessment->status,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : ''),'id'=>'status'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->description.'",' !!} @endforeach];

        $( "#description" ).autocomplete({
            source: autocomplete_elements
        });
    </script>
@endsection