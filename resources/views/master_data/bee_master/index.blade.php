@extends('adminlte.default')

@section('title') Master Data - BEE @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('bee_master.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> BEE Master</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form id="searchform" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('bee_master.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('description','Project Type')</th>
                    <th>@sortablelink('Naturalization')</th>
                    <th>@sortablelink('Race')</th>
                    <th>@sortablelink('BBBEE Level')</th>
                    <th>@sortablelink('BBBEE Race')</th>
                    <th>@sortablelink('Gender')</th>
                    <th>@sortablelink('Disability')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($bee_master as $result)
                    <tr>
                        <td><a href="{{route('bee_master.show',$result)}}">{{$result->project_t->description}}</a></td>
                        <td>{{$result->naturalization->description}}</td>
                        <td>{{$result->race->description}}</td>
                        <td>{{$result->bbbee_l->description}}</td>
                        <td>{{$result->bbbee_r->description}}</td>
                        <td>{{$result->genderd->description }}</td>
                        <td>{{$result->disabilityd->description}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('bee_master.edit',$result->id)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['bee_master.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data medical certificate type entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $bee_master->firstItem() }} - {{ $bee_master->lastItem() }} of {{ $bee_master->total() }}
                        </td>
                        <td>
                            {{ $bee_master->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
