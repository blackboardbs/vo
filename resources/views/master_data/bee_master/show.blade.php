@extends('adminlte.default')

@section('title') View Master Medical Certificate Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <a href="{{route('bee_master.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
        @foreach($bee_master as $result)
        <a href="{{route('bee_master.edit',$result->id)}}" class="btn btn-success float-right ml-1">Edit</a>
        @endforeach
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($bee_master as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Project Type:</th>
                        <td>{{$result->project_t->description}}</td>
                        <th>Naturalization:</th>
                        <td>{{$result->naturalization->description}}</td>
                    </tr>
                    <tr>
                        <th>Race:</th>
                        <td>{{$result->race->description}}</td>
                        <th>BBBEE Level:</th>
                        <td>{{$result->bbbee_l->description}}</td>
                    </tr>
                    <tr>
                        <th>BBBEE Race:</th>
                        <td>{{$result->bbbee_r->description}}</td>
                        <th>Gender</th>
                        <td>{{$result->genderd->description}}</td>
                    </tr>
                    <tr>
                        <th>Disability:</th>
                        <td>{{$result->disabilityd->description}}</td>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection