@extends('adminlte.default')

@section('title') Add Master Data BEE @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('bee_master.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Project Type: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('project_type',$project_t_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('project_type') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('project_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Naturalization: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('naturalization_id',$naturalization_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('naturalization_id') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('naturalization_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Race: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('race_id',$race_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('race_id') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('race_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BEE Level: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('bbbee_level',$bbbee_l_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('bbbee_level') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('bbbee_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BEE Race: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('bbbee_race',$bbbee_r_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('bbbee_race') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('bbbee_race') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Gender: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('gender',$gender_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('gender') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('gender') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Disability: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('disability',$disability_dropdown,1,['class'=>'form-control form-control-sm '.($errors->has('disability') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('disability') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>&nbsp;</th>
                    <td>&nbsp;</td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection