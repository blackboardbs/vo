@extends('adminlte.default')

@section('title') View Master Assignment Standard Cost Rate @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('assignment_standard_cost.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('assignment_standard_cost.edit', $standard_cost)}}" class="btn btn-success float-right ml-1">Edit</a>
            </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">

                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$standard_cost->description}}</td>
                        <th>Standard Cost Rate</th>
                        <td>{{$standard_cost->standard_cost_rate}}</td>
                    </tr>
                    <tr>
                        <th>Min Monthly Cost to Company</th>
                        <td>{{$standard_cost->min_rate}}</td>
                        <th>Max Monthly Cost to Company</th>
                        <td>{{$standard_cost->max_rate}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$standard_cost->status->description}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
        </div>
    </div>
@endsection