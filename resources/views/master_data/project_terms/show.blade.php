@extends('adminlte.default')

@section('title') View Master Data Project Terms @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('project_terms.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('project_terms.edit', $project_terms)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Terms Version</th>
                    <td>{{$project_terms->terms_version}}</td>
                    <th>Start Date</th>
                    <td>{{ $project_terms->start_date }}</td>
                    <th>End Date</th>
                    <td>{{ $project_terms->end_date }}</td>
                </tr>
                <tr>
                    <th>Travel</th>
                    <td>{{$project_terms->exp_travel}}</td>
                    <th>Parking</th>
                    <td>{{ $project_terms->exp_parking }}</td>
                    <th>Car Rental</th>
                    <td>{{ $project_terms->exp_car_rental }}</td>
                </tr>
                <tr>
                    <th>Flights</th>
                    <td>{{$project_terms->exp_flights}}</td>
                    <th>Other</th>
                    <td>{{ $project_terms->exp_other }}</td>
                    <th>Accommodation</th>
                    <td>{{ $project_terms->exp_accommodation }}</td>
                </tr>
                <tr>
                    <th>Out Of Town</th>
                    <td>{{$project_terms->exp_out_of_town}}</td>
                    <th>Toll</th>
                    <td>{{ $project_terms->exp_toll }}</td>
                    <th>Data</th>
                    <td>{{ $project_terms->exp_data }}</td>
                </tr>
                <tr>
                    <th>Hours Of Work</th>
                    <td>{{$project_terms->exp_hours_of_work}}</td>
                    <th></th>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection