@extends('adminlte.default')

@section('title') View Master Terms @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('master_term.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('master_term.edit', $terms[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Terms</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($terms as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Term</th>
                        <td colspan="3">{{ $result->term }}</td>
                    </tr>
                    <tr>
                        <th>Row Order</th>
                        <td>{{$result->row_order}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection