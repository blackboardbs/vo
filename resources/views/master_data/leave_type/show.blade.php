@extends('adminlte.default')

@section('title') View Master Data Leave Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('leave_type.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('leave_type.edit', $leave_type[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($leave_type as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Accrue</th>
                        <td><input type="checkbox" name="accrue" {{($result->accrue == '1' ? 'checked' : '')}} /></td>
                    </tr>
                    <tr>
                        <th>Cycle Length(Months)</th>
                        <td>{{$result->cycle_length}}</td>
                        <th>Maximum Length(Days)</th>
                        <td>{{$result->max_days}}</td>
                    </tr>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                        <th></th>
                        <td></td>

                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection