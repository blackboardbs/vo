@extends('adminlte.default')

@section('title') Edit Master Data Template Variable @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <a href="{{route('variable.index')}}" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</a>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('variable.update',$variable), 'method' => 'PATCH','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Display Name: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Example: Company Name"><i class="far fa-question-circle"></i></span>  <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('display_name', $variable->display_name, ['class'=>'form-control form-control-sm'. ($errors->has('display_name') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off']) }}
                        @foreach($errors->get('display_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Variable: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Example: company_name"><i class="far fa-question-circle"></i></span>  <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('variable', $variable->variable, ['class'=>'form-control form-control-sm'. ($errors->has('variable') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off']) }}
                        @foreach($errors->get('variable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Template Type: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('template_type_id',$template_type_dropdown,$variable->template_type_id,['class'=>'form-control form-control-sm '. ($errors->has('template_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Template Type'])}}
                        @foreach($errors->get('template_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$variable->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_dropdown'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })
    </script>
@endsection
