@extends('adminlte.default')

@section('title') Edit Master Invoice Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('invoice_status.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">

                {{Form::open(['url' => route('vendor_invoice_status.update',['vendor_invoice_status' => $vendor_invoice_status]), 'method' => 'put','class'=>'mt-3'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">{{ Form::text('description', $vendor_invoice_status->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown,$vendor_invoice_status->status_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                <table class="table table-borderless">
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </table>
                {{Form::close()}}

        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection