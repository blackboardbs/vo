@extends('adminlte.default')

@section('title') View Master Vendor Invoice Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('vendor_invoice_status.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('vendor_invoice_status.edit', $vendor_invoice_status)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Vendor Invoice Status</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">

                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$vendor_invoice_status->description}}</td>
                        <th>Status</th>
                        <td>{{$vendor_invoice_status->statusd->description}}</td>
                    </tr>
                </table>

        </div>
    </div>
@endsection