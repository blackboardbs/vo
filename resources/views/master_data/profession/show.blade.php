@extends('adminlte.default')

@section('title') Edit Master Data Profession @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('profession.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('profession.edit', $profession)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $profession->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('industry') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $status_drop_down, $profession->status_id, ['class'=>'form-control form-control-sm ','id'=>'status_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
