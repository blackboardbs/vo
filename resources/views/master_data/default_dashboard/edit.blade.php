@extends('adminlte.default')

@section('title') Edit Master Account @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('master_dashboard.update', $dashboard), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">

                <tr>
                    <th style="width: 15%">Roles <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td style="width: 35%">{{ Form::select('role_id', $roles_dropdown2, $dashboard->role_id, ['class'=>'form-control form-control-sm '. ($errors->has('role_id') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th style="width: 15%">Dashboard Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td style="width: 35%">{{Form::text('dashboard_name',$dashboard->dashboard_name,['class'=>'form-control form-control-sm'. ($errors->has('dashboard_name') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('dashboard_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::select('status_id', $status_dropdown, $dashboard->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>

            <h5>Your Dashboard Name</h5>

            <hr>
            <h5>Header Components</h5>
            <div class="row mt-4">

                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('Expired Users'), (in_array(strtoupper('Expired Users'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'expired_users'])}}
                            {{ Form::label('expired_users', 'Expired Users', ['class' => 'custom-control-label mr-3']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('usage: timesheets count'), (in_array(strtoupper('usage: timesheets count'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'timesheets_count'])}}
                            {{ Form::label('timesheets_count', 'Usage: Timesheets count', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    {{--@if(auth()->user()->isAn('admin') || auth()->user()->isAn('admin_manager') || auth()->user()->isAn('manager') || auth()->user()->isAn('consultant'))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('lp_dashboard[]', strtoupper('leave days'), (in_array(str_replace(' ', '_', strtoupper('leave days')), $user_prefs)), ['class' => 'custom-control-input small-component', 'id' => 'leave_days'])}}
                            {{ Form::label('leave_days', 'Leave Days', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif--}}
                </div>
                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('failed log in attempts'), (in_array(strtoupper('failed log in attempts'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'failed_logs'])}}
                            {{ Form::label('failed_logs', 'Failed Login Attempts', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('utilization past 3 weeks'), (in_array(strtoupper('utilization past 3 weeks'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'util_3_weeks'])}}
                            {{ Form::label('util_3_weeks', 'Utilization Past 3 Weeks', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('assignment outstanding hours'), (in_array(strtoupper('assignment outstanding hours'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'assignment_out_hours'])}}
                            {{ Form::label('assignment_out_hours', 'Assignment Outstanding Hours', ['class' => 'custom-control-label mr-2']) }}

                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('number of logins'), (in_array(strtoupper('number of logins'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'logins_number'])}}
                            {{ Form::label('logins_number', 'Number of Logins', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('utilization current week'), (in_array(strtoupper('utilization current week'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'util_current_week'])}}
                            {{ Form::label('util_current_week', 'Utilization Current Week', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('top_components[]', strtoupper('users to expire in 30 days'),(in_array(strtoupper('users to expire in 30 days'),explode(',', str_replace('_', ' ', $dashboard->top_component)))), ['class' => 'custom-control-input small-component', 'id' => 'users_to_expire'])}}
                            {{ Form::label('users_to_expire', 'Users to Expire in 30 Days', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="alert alert-warning mt-4" style="display: none"><i class="fas fa-exclamation-triangle fa-2x fa-pull-left"></i> <p class="text-bold">You can only select the maximum of 4 components</p></div>

            <hr>
            <h5>Charts and tables</h5>

            <div class="row">
                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('failed login attempts over 30 days trends'),(in_array(strtoupper('failed login attempts over 30 days trends'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'failed_trends'])}}
                            {{ Form::label('failed_trends', 'Failed login attempts over 30 days trend', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('number of logins chart'),(in_array(strtoupper('number of logins chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'logins_chart'])}}
                            {{ Form::label('logins_chart', 'Number of logins', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('utilization - current week'),(in_array(strtoupper('utilization - current week'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'util_1_week'])}}
                            {{ Form::label('util_1_week', 'Utilization – current week', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('outstanding timesheets'),(in_array(strtoupper('outstanding timesheets'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'outstanding_timesheets'])}}
                            {{ Form::label('outstanding_timesheets', 'Outstanding Timesheets', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('income assignment'),(in_array(strtoupper('income assignment'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'income_assignment'])}}
                            {{ Form::label('income_assignment', 'Open Assignment - Income', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('utilization Cost'),(in_array(strtoupper('utilization Cost'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'utilization_cost'])}}
                            {{ Form::label('utilization_cost', 'Utilization – Cost', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('cashflow'),(in_array(strtoupper('cashflow'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'cashflow'])}}
                            {{ Form::label('cashflow', 'Cashflow', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('debtor ageing'),(in_array(strtoupper('debtor ageing'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'debtor_ageing'])}}
                            {{ Form::label('debtor_ageing', 'Debtor Ageing', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 customers table'),(in_array(strtoupper('top 10 customers table'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_customers_table'])}}
                            {{ Form::label('top_10_customers_table', 'Top 10 Customers Table', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 locations chart'),(in_array(strtoupper('top 10 locations chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_locations_chart'])}}
                            {{ Form::label('top_10_locations_chart', 'Top 10 Locations Chart', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('year to date chart'),(in_array(strtoupper('year to date chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'year_to_date_chart'])}}
                            {{ Form::label('year_to_date_chart', 'Year To Date Chart', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('hours outstanding on assignments'),(in_array(strtoupper('hours outstanding on assignments'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'hours_out_assignment'])}}
                            {{ Form::label('hours_out_assignment', 'Hours Outstanding on Assignments', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('usage timesheets count'),(in_array(strtoupper('usage timesheets count'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'usage_timesheets'])}}
                            {{ Form::label('usage_timesheets', 'Usage: Timesheet count', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('utilization - past 3 weeks'),(in_array(strtoupper('utilization - past 3 weeks'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'utilization_3_weeks'])}}
                            {{ Form::label('utilization_3_weeks', 'Utilization – past 3 weeks', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('important dates'),(in_array(strtoupper('important dates'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'important_dates'])}}
                            {{ Form::label('important_dates', 'Important Dates', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('cost assignment'),(in_array(strtoupper('cost assignment'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'cost_assignment'])}}
                            {{ Form::label('cost_assignment', 'Open Assignment - Cost', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('income projects weekly'),(in_array(strtoupper('income projects weekly'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'income_projects_weekly'])}}
                            {{ Form::label('income_projects_weekly', 'Income Projects Weekly Utilization', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('planning and pipelines'),(in_array(strtoupper('planning and pipelines'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'planning_and_pipelines'])}}
                            {{ Form::label('planning_and_pipelines', 'Planning And Pipelines', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['super_admin', 'admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('creditors ageing'),(in_array(strtoupper('creditors ageing'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'creditors_ageing'])}}
                            {{ Form::label('creditors_ageing', 'Creditor Ageing', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 solutions table'),(in_array(strtoupper('top 10 solutions table'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_solutions_table'])}}
                            {{ Form::label('top_10_solutions_table', 'Top 10 Solutions Table', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 locations table'),(in_array(strtoupper('top 10 locations table'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_locations_table'])}}
                            {{ Form::label('top_10_locations_table', 'Top 10 Locations Table', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('year to date table'),(in_array(strtoupper('year to date table'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'year_to_date_table'])}}
                            {{ Form::label('year_to_date-table', 'Year To Date Table', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('number of failed logins'),(in_array(strtoupper('number of failed logins'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'failed_number'])}}
                            {{ Form::label('failed_number', 'Number of failed logins', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('users to expire in 30 days chart'),(in_array(strtoupper('users to expire in 30 days chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'to_expire'])}}
                            {{ Form::label('to_expire', 'Users to Expire in 30 days', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('leave not approved'),(in_array(strtoupper('leave not approved'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'leave_not_approved'])}}
                            {{ Form::label('leave_not_approved', 'Leave not Approved', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('consultant expenses'),(in_array(strtoupper('consultant expenses'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'consultant_expenses'])}}
                            {{ Form::label('consultant_expenses', 'Consultant Expenses', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor', 'vendor']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('notice board'),(in_array(strtoupper('notice board'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'notice_board'])}}
                            {{ Form::label('notice_board', 'Notice Board', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('utilization income'),(in_array(strtoupper('utilization income'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'utilization_income'])}}
                            {{ Form::label('utilization_income', 'Utilization – Income', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('cost projects weekly'),(in_array(strtoupper('cost projects weekly'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'cost_projects_weekly'])}}
                            {{ Form::label('cost_projects_weekly', 'Cost Projects Weekly Utilization', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 customers chart'),(in_array(strtoupper('top 10 customers chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_customers_chart'])}}
                            {{ Form::label('top_10_customers_chart', 'Top 10 Customers Chart', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('top 10 solutions chart'),(in_array(strtoupper('top 10 solutions chart'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'top_10_solutions_chart'])}}
                            {{ Form::label('top_10_solutions_chart', 'Top 10 Solutions Chart', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('charts_tables[]', strtoupper('monthly consulting'),(in_array(strtoupper('monthly consulting'), explode(',', str_replace('_', ' ', $dashboard->charts_tables)))), ['class' => 'custom-control-input', 'id' => 'monthly_consulting'])}}
                            {{ Form::label('monthly_consulting', 'Monthly Consulting', ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    @endif
                </div>
            </div>
            <hr>
            @if($errors->has('charts_tables') || $errors->has('top_components'))
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <hr>
            @endif
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        $(function () {
            $("input.small-component").change(function () {
                var maxAllowed = 4;
                var cnt = $("input.small-component:checked").length;

                if (cnt > maxAllowed) {
                    $(this).prop("checked", "");
                    $('.alert-warning').fadeIn();
                }else {
                    $('.alert-warning').fadeOut();
                }
            });
        })

        let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->dashboard_name.'",' !!} @endforeach];

        $( "#description" ).autocomplete({
            source: autocomplete_elements
        });
    </script>
@endsection