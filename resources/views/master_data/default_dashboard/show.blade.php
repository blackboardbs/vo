@extends('adminlte.default')

@section('title') View Master Default Dashboard @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('master_dashboard.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('master_dashboard.edit', $dashboard)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Name</th>
                        <td>{{$dashboard->dashboard_name}}</td>
                        <th>Role</th>
                        <td>{{isset($dashboard->roles)?$dashboard->roles->name:''}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$dashboard->status->description}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Top Components</th>
                        <td>
                            <ul class="list-group">
                                @forelse(explode(',', $dashboard->top_component) as $component)
                                    <li class="list-group-item">{{ucwords(str_replace("_"," ",$component))}}</li>
                                    @empty
                                @endforelse
                            </ul>
                        </td>
                        <th>Tables and Charts</th>
                        <td>
                            <ul class="list-group">
                                @forelse(explode(',', $dashboard->charts_tables) as $component)
                                    <li class="list-group-item">{{ucwords(str_replace("_"," ",$component))}}</li>
                                @empty
                                @endforelse
                            </ul>
                        </td>
                    </tr>
                </table>
        </div>
    </div>
@endsection