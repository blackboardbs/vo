@extends('adminlte.default')

@section('title') Edit Master Data Process Interview @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('process_interview.update',$process_interview), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">{{ Form::text('name', $process_interview->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown,$process_interview->status_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }

        .chosen-container{
            width: 100%!important;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {


            let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->name.'",' !!} @endforeach];

            $( "#name" ).autocomplete({
                source: autocomplete_elements
            });
        });
    </script>
@endsection
