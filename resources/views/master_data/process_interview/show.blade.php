@extends('adminlte.default')

@section('title') View Master Data Process Interview @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('process_interview.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('process_interview.edit', $process_interview)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Name</th>
                    <td>{{$process_interview->name}}</td>
                    <th>Status</th>
                    <td>{{$process_interview->status->description}}</td>
                </tr>
            </table>

        </div>
    </div>
@endsection