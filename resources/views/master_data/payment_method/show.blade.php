@extends('adminlte.default')

@section('title') View Master Payment Method @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('payment_method.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('payment_method.edit', $payment_method[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($payment_method as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection