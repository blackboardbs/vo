@extends('adminlte.default')

@section('title') Edit Master Country @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('master_country.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($country as $result)
                {{Form::open(['url' => route('master_country.update',['countryid' => $result]), 'method' => 'post','class'=>'mt-3'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('name', $result->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Code <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('code', $result->code, ['class'=>'form-control form-control-sm'. ($errors->has('code') ? ' is-invalid' : ''), 'id' => 'code', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('code') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{Form::select('status',$status_dropdown,$result->status,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2"></td>
                    </tr>
                </table>
                <table class="table table-borderless">
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </table>
                {{Form::close()}}
            @endforeach
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->name.'",' !!} @endforeach];

        $( "#name" ).autocomplete({
            source: autocomplete_elements
        });
    </script>
@endsection
