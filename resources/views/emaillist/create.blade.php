@extends('adminlte.default')
@section('title') Maillist @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="container-fluid">
                <ul class="nav nav-tabs">
                    <li><a href="">Candidates</a></li>
                    <li class="active"><a href="#">Customers</a></li>
                </ul>
                <hr>
                <div class="row">

                </div>
            </div>
        </div>
    </div>
@endsection