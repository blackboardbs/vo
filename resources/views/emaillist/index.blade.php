@extends('adminlte.default')
@section('title') Mail List @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('maillist.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Maillist</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Name</th>
                    <th>Schedule</th>
                    <th>Next Run Date</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($mail_lists as $mail_list)
                    <tr>
                        <td><a href="{{route('maillist.show',$mail_list)}}">{{$mail_list->name}}</a></td>
                        <td>{{$mail_list->schedule}}</td>
                        <td>{{$mail_list->next_run_date}}</td>
                        <td class="text-right">
                            <a href="{{route('maillist.edit',$mail_list)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['maillist.destroy', $profession],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data profession entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
