@extends('adminlte.default')

@section('title') Edit Master Data Profession @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $profession->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('industry') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection