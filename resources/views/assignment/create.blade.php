@extends('adminlte.default')
@section('title') Work Breakdown Structure @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('assignment.store'), 'method' => 'post','class'=>'mt-3', 'files' => true])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Create a new Assignment No.{{$assignment_nr}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Internal Owner:</th>
                    <td>{{Form::select('manager_id',$customer_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('manager_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Company:</th>
                    <td>{{Form::select('company_id',$customer_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('company_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>{{Form::select('customer_id',$customer_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('customer_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer Ref.:</th>
                    <td>{{Form::text('customer_ref',old('customer_ref'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Ref No'])}}
                        @foreach($errors->get('customer_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Resourcing</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Consultant 1</th>
                    <td>{{Form::select('consultant_1',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_1') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 2</th>
                    <td>{{Form::select('consultant_2',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_2') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 3</th>
                    <td>{{Form::select('consultant_3',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_3') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 4</th>
                    <td>{{Form::select('consultant_4',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_4') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_4') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 5</th>
                    <td>{{Form::select('consultant_5',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_5') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_5') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 6</th>
                    <td>{{Form::select('consultant_6',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_6') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_6') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 7</th>
                    <td>{{Form::select('consultant_7',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_7') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_7') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 8</th>
                    <td>{{Form::select('consultant_8',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_8') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_8') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 9</th>
                    <td>{{Form::select('consultant_9',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_9') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_9') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 10</th>
                    <td>{{Form::select('consultant_10',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_10') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_10') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 7</th>
                    <td>{{Form::select('consultant_7',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_7') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_7') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 8</th>
                    <td>{{Form::select('consultant_8',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_8') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_8') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant 9</th>
                    <td>{{Form::select('consultant_9',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_9') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_9') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Consultant 10</th>
                    <td>{{Form::select('consultant_10',$consultant_drop_down,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('consultant_10') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_10') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date</th>
                    <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12','placeholder'=>'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date</th>
                    <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12','placeholder'=>'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable</th>
                    <td>
                        {{Form::select('billable',[''=>'Please Select',0=>'No',1=>'Yes'],old('billable'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status</th>
                    <td>
                        {{Form::select('status_id',$status_drop_down,old('status_id'),['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Billable Expenses</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Travel</th>
                    <td>{{Form::textarea('travel',old('travel'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination.'])}}
                        @foreach($errors->get('travel') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Parking</th>
                    <td>{{Form::textarea('parking',old('parking'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred, including Airport parking.'])}}
                        @foreach($errors->get('parking') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Accommodation</th>
                    <td>{{Form::textarea('accommodation',old('accommodation'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'To be arranged by the client.'])}}
                        @foreach($errors->get('accommodation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Per Diem</th>
                    <td>{{Form::textarea('per_diem',old('per_diem'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>''])}}
                        @foreach($errors->get('per_diem') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Out of Town Allowance</th>
                    <td>{{Form::textarea('out_of_town',old('out_of_town'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'R300 per day in South Africa and US$ 80 per day internationally (including Zimbabwe, Botswana).'])}}
                        @foreach($errors->get('out_of_town') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Data</th>
                    <td>{{Form::textarea('data',old('data'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A'])}}
                        @foreach($errors->get('data') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Other</th>
                    <td colspan="3">{{Form::textarea('other',old('other'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.'])}}
                        @foreach($errors->get('other') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection