@extends('adminlte.default')
@section('title') Edit Assignment @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('assignment.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-caret-left"></i> Back</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @if(count($errors->all()))
                <div class="alert alert-danger alert-dismissible blackboard-alert col-md-6">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Error!</strong> Validation failed, please update your form
                </div>
            @endif
            {{Form::open(['url' => route('assignment.update', $assignment->id), 'method' => 'put','class'=>'mt-3', 'files' => true, 'autocomplete' => 'off'])}}
            <input  type="hidden" name="project_id" id="project_id" value="{{$project->id}}">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Project Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$project->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',$project->ref,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>
                        {{Form::select('customer_id',$customer_drop_down,$project->customer_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer PO:</th>
                    <td>{{Form::text('customer_po',$project->customer_po,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_po') ? ' is-invalid' : ''), 'placeholder' => 'Customer PO','disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_po') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Start Date</th>
                    <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id' => 'start_date', 'placeholder'=>'Start Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project End Date</th>
                    <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'placeholder'=>'End Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Type</th>
                    <td>{{Form::text('project_type',(isset($project->project_type)?$project->project_type->description:''),['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('project_type') ? ' is-invalid' : ''), 'placeholder'=>'Project Type', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('project_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project Status</th>
                    <td>{{Form::text('project_status',(isset($project->status)?$project->status->description:''),['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('project_status') ? ' is-invalid' : ''), 'placeholder'=>'Project Status', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('project_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment No.{{$assignment_nr}} for {{$resource->last_name}}, {{$resource->first_name}}</th>
                </tr>
                <tr>
                    <th>Assignment Start Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('start_date',$assignment->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assignment End Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('end_date',$assignment->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer PO:</th>
                    <td>{{Form::text('customer_po',$assignment->customer_po,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_po') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('customer_po') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>PO Hours: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('hours',$assignment->hours,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours') ? ' is-invalid' : ''),'placeholder'=>'Hours'])}}
                        @foreach($errors->get('hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('billable',[0=>'No', 1=>'Yes'],$assignment->billable,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Hours of Work per day:</th>
                    <td>{{Form::text('hours_of_work',$assignment->hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''),'placeholder'=>'0'])}}
                        @foreach($errors->get('hours_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assignment Role:</th>
                    <td>{{Form::text('role',$assignment->role,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('role') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Location:</th>
                    <td>{{Form::text('location',$assignment->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('location') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assignment Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('assignment_status',$status_drop_down,$assignment->assignment_status,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('assignment_status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('assignment_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Record Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('status',$recoord_status_dropdown,$assignment->status,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Scope: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">
                        {{Form::textarea('note_1',$assignment->note_1,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_1') ? ' is-invalid' : ''),'placeholder'=>'Scope'])}}
                        @foreach($errors->get('note_1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note_2',$assignment->note_2,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_2') ? ' is-invalid' : ''),'placeholder'=>'Note'])}}
                        @foreach($errors->get('note_2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Re-assign Consultant:</th>
                    <td>
                        {{Form::select('employee_id',$consultant_drop_down,$assignment->employee_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('employee_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('employee_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <tbody>
                <tr>
                    <th colspan="6" class="btn-dark" style="text-align: center;">Assignment Approval</th>
                </tr>
                <tr>
                    <th>Internal Owner: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('manager_id',$users_drop_down,$project->manager_id,['class'=>'form-control form-control-sm  col-sm-12  ', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('internal_owner_known_as',$assignment->internal_owner_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_owner_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('internal_owner_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td style="width: 150px;">
                        {{Form::select('project_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->project_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Product Owner</th>
                    <td>
                        {{Form::select('product_owner_id',$users_drop_down,$assignment->product_owner_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('product_owner_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('product_owner_known_as',$assignment->product_owner_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('product_owner_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('product_owner_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('product_owner_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->product_owner_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('product_owner_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Manager:</th>
                    <td>{{Form::select('project_manager_new_id',$users_drop_down,$assignment->project_manager_new_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project_manager_new_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('project_manager_new_known_as',$assignment->project_manager_new_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_manager_new_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project_manager_new_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('project_manager_new_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->project_manager_new_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project_manager_new_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Line Manager:</th>
                    <td>{{Form::select('line_manager_id',$users_drop_down,$assignment->line_manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('line_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('line_manager_known_as',$assignment->line_manager_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('line_manager_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('line_manager_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('line_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->line_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('line_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Claim Approver:</th>
                    <td>{{Form::select('assignment_approver',$users_drop_down,$assignment->assignment_approver,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('assignment_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('assignment_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('claim_approver_known_as',$assignment->claim_approver_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('claim_approver_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('claim_approver_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('claim_approver_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->claim_approver_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('claim_approver_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('claim_approver_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Resource Manager:</th>
                    <td>{{Form::select('resource_manager_id',$users_drop_down,$assignment->resource_manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('resource_manager_known_as',$assignment->resource_manager_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_manager_known_as') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_manager_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('resource_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->resource_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_ts_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <tbody>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Reporting</th>
                </tr>
                <tr>
                    <th>Report to:</th>
                    <td>
                        {{Form::text('report_to',$assignment->report_to,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to') ? ' is-invalid' : ''),'placeholder'=>'Report to', 'id' => 'report_to'])}}
                        @foreach($errors->get('report_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Copy from User:</th>
                    <td>
                        {{Form::select('copy_from_user', $consultant_drop_down, $assignment->copy_from_user, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('copy_from_user') ? ' is-invalid' : ''), 'id' => 'copy_from_user'])}}
                        @foreach($errors->get('copy_from_user') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Report to email:</th>
                    <td>
                        {{Form::text('report_to_email',$assignment->report_to_email,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to_email') ? ' is-invalid' : ''),'placeholder'=>'Email', 'id' => 'report_to_email'])}}
                        @foreach($errors->get('report_to_email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Report to phone:</th>
                    <td>
                        {{Form::text('report_phone',$assignment->report_phone,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_phone') ? ' is-invalid' : ''),'placeholder'=>'Phone', 'id' => 'report_phone'])}}
                        @foreach($errors->get('report_phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Vendor Agreement Template:</th>
                    <td>{{Form::select('vendor_template_id', $vendor_template_drop_down, $assignment->vendor_template_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('vendor_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('vendor_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Resource Agreement Template:</th>
                    <td>{{Form::select('resource_template_id', $resource_template_drop_down, $assignment->resource_template_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Planning Information</th>
                </tr>
                <tr>
                    <th>Planned Start Date</th>
                    <td>{{Form::text('planned_start_date',$assignment->planned_start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_start_date') ? ' is-invalid' : ''), 'id' => 'planned_start_date', 'placeholder'=>'Planned Start Date'])}}
                        @foreach($errors->get('planned_start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Planned End Date</th>
                    <td>{{Form::text('planned_end_date',$assignment->planned_end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_end_date') ? ' is-invalid' : ''), 'id' => 'planned_end_date', 'placeholder'=>'Planned End Date'])}}
                        @foreach($errors->get('planned_end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Max Time Per Day Before Exception</th>
                    <td>{{Form::select('max_time_per_day_hours',$hours_drop_down,$assignment->max_time_per_day_hours,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('max_time_per_day_hours') ? ' is-invalid' : ''), 'id' => 'max_time_per_day_hours', 'placeholder'=>'Hours'])}}
                        @foreach($errors->get('max_time_per_day_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::select('max_time_per_day_min',$minutes_drop_down,$assignment->max_time_per_day_min,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('max_time_per_day_min') ? ' is-invalid' : ''), 'id' => 'max_time_per_day_min', 'placeholder'=>'Minutes'])}}
                        @foreach($errors->get('max_time_per_day_min') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Capacity Allocation Per Day</th>
                    <td>{{Form::select('capacity_allocation_hours',$hours_drop_down,$assignment->capacity_allocation_hours,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('capacity_allocation_hours') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_hours', 'placeholder'=>'Hours'])}}
                        @foreach($errors->get('capacity_allocation_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::select('capacity_allocation_min',$minutes_drop_down,$assignment->capacity_allocation_min,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('capacity_allocation_min') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_min', 'placeholder'=>'Minutes'])}}
                        @foreach($errors->get('capacity_allocation_min') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <th>Capacity Allocation %</th>
                    <td>{{Form::text('capacity_allocation_percentage',$assignment->capacity_allocation_percentage,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('capacity_allocation_percentage') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_percentage', 'placeholder'=>'Capacity Allocation'])}}
                        @foreach($errors->get('capacity_allocation_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>

                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Statistics Information</th>
                </tr>
                <tr>
                    <th>Function:</th>
                    <td>
                        {{Form::select('function',$function_drop_down, $assignment->function,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('function') ? ' is-invalid' : ''),'placeholder'=>'Function'])}}
                        @foreach($errors->get('function') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>System:</th>
                    <td>
                        {{Form::select('system',$system_drop_down, $assignment->system,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('system') ? ' is-invalid' : ''),'placeholder'=>'System'])}}
                        @foreach($errors->get('system') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Country of Work:</th>
                    <td>
                        {{Form::select('country_id',$country_drop_down,$assignment->country_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('country_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>

                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Billable and Claimable Expenses</th>
                </tr>
                <tr>
                    <th>Travel</th>
                    <td>{{Form::textarea('travel',isset($expense->travel)?$expense->travel:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination.'])}}
                        @foreach($errors->get('travel') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Parking</th>
                    <td>{{Form::textarea('parking',isset($expense->parking)?$expense->parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred, including Airport parking.'])}}
                        @foreach($errors->get('parking') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Accommodation</th>
                    <td>{{Form::textarea('accommodation',isset($expense->accommodation)?$expense->accomodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'To be arranged by the client.'])}}
                        @foreach($errors->get('accommodation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Per Diem</th>
                    <td>{{Form::textarea('per_diem',isset($expense->per_diem)?$expense->per_diem:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>''])}}
                        @foreach($errors->get('per_diem') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Out of Town Allowance</th>
                    <td>{{Form::textarea('out_of_town',isset($expense->out_of_town)?$expense->out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'R300 per day in South Africa and US$ 80 per day internationally (including Zimbabwe, Botswana).'])}}
                        @foreach($errors->get('out_of_town') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Data</th>
                    <td>{{Form::textarea('data',isset($expense->data)?$expense->data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A'])}}
                        @foreach($errors->get('data') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Other</th>
                    <td colspan="3">{{Form::textarea('other',isset($expense->other)?$expense->other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.'])}}
                        @foreach($errors->get('other') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Rates</th>
                </tr>
                <tr>
                    <th style='font-weight: bold; width: 25%'>Internal Cost:</th>
                    <td style='font-weight: bold; width: 25%'>External Cost</td>
                    <th style='font-weight: bold; width: 25%'>Invoice Rate</th>
                    <td style='font-weight: bold; width: 25%'>Bonus Rate</td>
                </tr>
                <tr>
                    <td>
                        {{Form::text('internal_cost_rate',$assignment->internal_cost_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "internal_cost_rate"])}}
                        @foreach($errors->get('internal_cost_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('external_cost_rate',$assignment->external_cost_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "external_cost_rate"])}}
                        @foreach($errors->get('external_cost_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('invoice_rate',$assignment->invoice_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "invoice_rate"])}}
                        @foreach($errors->get('invoice_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('rate',$assignment->rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "rate"])}}
                        @foreach($errors->get('rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>
                        Fixed Labour Cost
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Fixed Price
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        {{Form::text('fixed_labour_cost',$assignment->fixed_labour_cost,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_labour_cost') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "fixed_labour_cost"])}}
                        @foreach($errors->get('fixed_labour_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        {{Form::text('fixed_price',$assignment->fixed_price,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "fixed_price"])}}
                        @foreach($errors->get('fixed_price') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Rates - Secondary Currency</th>
                </tr>
                <tr>
                    <th style='font-weight: bold; width: 25%'>Currency:</th>
                    <th colspan="3" style='font-weight: bold; width: 25%'>Exchange Rate:</th>
                </tr>
                <tr>
                    <td style='width: 25%'>
                        {{Form::select('currency_sec', $currencies, $assignment->currency_sec,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('currency_sec') ? ' is-invalid' : ''), 'placeholder'=>'Select Currency', 'id' => "currency_sec"])}}
                        @foreach($errors->get('currency_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th colspan="3" style='font-weight: bold; width: 25%' id="exchange-rate"></th>

                </tr>
                <tr>
                    <th style='font-weight: bold; width: 25%'>Internal Cost:</th>
                    <td style='font-weight: bold; width: 25%'>External Cost</td>
                    <th style='font-weight: bold; width: 25%'>Invoice Rate</th>
                    <td style='font-weight: bold; width: 25%'>Bonus Rate</td>
                </tr>
                <tr>
                    <td>
                        {{Form::text('internal_cost_rate_sec',$assignment->internal_cost_rate_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "internal_cost_rate_sec"])}}
                        @foreach($errors->get('internal_cost_rate_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('external_cost_rate_sec',$assignment->external_cost_rate_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "external_cost_rate_sec"])}}
                        @foreach($errors->get('external_cost_rate_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('invoice_rate_sec',$assignment->invoice_rate_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "invoice_rate_sec"])}}
                        @foreach($errors->get('invoice_rate_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('rate_sec',$assignment->rate_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "rate_sec"])}}
                        @foreach($errors->get('rate_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>
                        Fixed Labour Cost
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Fixed Price
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        {{Form::text('fixed_labour_cost_sec',$assignment->fixed_labour_cost_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_labour_cost_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "fixed_labour_cost_sec"])}}
                        @foreach($errors->get('fixed_labour_cost_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        {{Form::text('fixed_price_sec',$assignment->fixed_price_sec,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "fixed_price_sec"])}}
                        @foreach($errors->get('fixed_price_sec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Referral</th>
                </tr>
                <tr>
                    <th>Rate per Billable Hour:</th>
                    <td class="text-right">0.00</td>
                    <th>Rate per Non-Billable Hour:</th>
                    <td class="text-right">0.00</td>
                </tr>
                <tr>
                    <th>% of Invoice Rate:</th>
                    <td class="text-right">0.00</td>
                    <th>Assignment Fixed Cost:</th>
                    <td class="text-right">0.00</td>
                </tr>
                <tr>
                    <th>Due on Assignment Status:</th>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Referred Vendor:</th>
                    <td></td>
                    <th>or Referred User:</th>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center"><button class="btn-dark btn btn-sm">Process Once-off Commission</button></td>
                </tr>

                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="7" class="btn-dark" style="text-align: center;">Assignment Extentions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Extension Date:</th>
                    <th>Ext Hours</th>
                    <th>Ext Notes</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Ref</th>
                    <th>Actions</th>
                </tr>
                @foreach($assignment_extensions as $assignment_extension)
                <tr>
                    <td>{{$assignment_extension->extension_date}}</td>
                    <td>{{$assignment_extension->extension_hours}}</td>
                    <td>{{$assignment_extension->extension_notes}}</td>
                    <td>{{$assignment_extension->extension_start_date}}</td>
                    <td>{{$assignment_extension->extension_end_date}}</td>
                    <td>{{$assignment_extension->extension_ref}}</td>
                    <td>
                        <a href="{{route('assignment.editextension',$assignment_extension->id)}}" class="btn btn-success btn-sm">Edit</a>
                        {{--{{ Form::open(['method' => 'DELETE','route' => ['assignment.deleteextension', $assignment_extension->id],'style'=>'display:inline','class'=>'delete']) }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                        {{ Form::close() }}--}}
                    </td>
                </tr>
                @endforeach
                {{--<tr id="add_expense_text_fields">
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''),'placeholder' => 'Extension Date'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'0:00'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Notes'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'End Date'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>
                        {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Reference'])}}
                        @foreach($errors->get('extension_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                --}}
                <tr>
                    <td colspan="7" class="text-center">
                        <a href="{{route('assignment.addextension', $assignment->id)}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Add Extension</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script type="text/javascript">
        $(function (){
            $("#currency_sec").on("change", () => currencyConversion())
            $("#internal_cost_rate").on("blur", () => currencyConversion())
            $("#external_cost_rate").on("blur", () => currencyConversion())
            $("#invoice_rate").on("blur", () => currencyConversion())
            $("#rate").on("blur", () => currencyConversion())
            $("#fixed_labour_cost").on("blur", () => currencyConversion())
            $("#fixed_price").on("blur", () => currencyConversion())
        })

        function currencyConversion() {

            let currency = $("#currency_sec").val();
            let internal_cost_rate = $("#internal_cost_rate").val();
            let external_cost_rate = $("#external_cost_rate").val();
            let invoice_rate = $("#invoice_rate").val();
            let rate = $("#rate").val();
            let fixed_labour_cost = $("#fixed_labour_cost").val();
            let fixed_price = $("#fixed_price").val();

            fetch(`/exchange-rate?currency=${currency}&internal_cost_rate=${internal_cost_rate}&external_cost_rate=${external_cost_rate}&invoice_rate=${invoice_rate}&rate=${rate}&fixed_labour_cost=${fixed_labour_cost}&fixed_price=${fixed_price}`)
                .then(res => res.json())
                .then(data =>{
                if (data.rate > 0){
                    $("#exchange-rate").text((1/data.rate).toFixed(2))
                    $("#internal_cost_rate_sec").val(data.internal_cost_rate_sec);
                    $("#external_cost_rate_sec").val(data.external_cost_rate_sec);
                    $("#invoice_rate_sec").val(data.invoice_rate_sec);
                    $("#rate_sec").val(data.rate_sec);
                    $("#fixed_labour_cost_sec").val(data.fixed_labour_cost_sec);
                    $("#fixed_price_sec").val(data.fixed_price_sec);
                }
                console.log(data)
            }).catch(err => err.response);
        }
        $(function () {

        });
        $(function () {
            $("#max_time_per_day_hours").on('change', function () {
                if ($(this).val() == 24){
                    $('#max_time_per_day_min').val(0).attr('disabled', true).trigger('chosen:updated');
                }
            });
            $("#capacity_allocation_hours").on('change',function () {
                if ($(this).val() == 24){
                    $('#capacity_allocation_min').val(0).attr('disabled', true).trigger('chosen:updated');
                }
                let capacity_min = 0;
                if ($("#capacity_allocation_min").val() != '') capacity_min = $("#capacity_allocation_min").val();
                let capacity = (Number($(this).val()) + (Number(capacity_min)/60))/8;
                $("#capacity_allocation_percentage").val(Math.round(capacity*100));
            });
            $("#capacity_allocation_min").on('change', function () {
                let capacity_hrs = 0;
                if ($("#capacity_allocation_hours").val() != '') capacity_hrs =  $("#capacity_allocation_hours").val();
                let capacity_hours = ((Number($(this).val())/60) + Number(capacity_hrs))/8;
                $("#capacity_allocation_percentage").val(Math.round(capacity_hours*100));
            });

            $('#copy_from_user').on('change', function(){
                var data = {
                    user_id: $("#copy_from_user").val()
                };
                var user_id = $("#copy_from_user").val();

                if(user_id == 0){
                    $('#report_to').val('');
                    $('#report_to_email').val('');
                    $('#report_phone').val('');
                    return false;
                }

                axios.post('{{route('assignment.getuserdetails')}}', data)
                .then(function (data) {
                    $('#report_to').val(data['data'].first_name+' '+data['data'].last_name);
                    $('#report_to_email').val(data['data'].email);
                    $('#report_phone').val(data['data'].phone);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });

            });

        });
    </script>
@endsection
