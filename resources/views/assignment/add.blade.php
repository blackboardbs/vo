@extends('adminlte.default')
@section('title') Assignment @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void()" onclick="saveForm('editassignment')" class="btn btn-primary ml-1 mr-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive pb-3">
                <div style="border:1px solid #007bff;padding:15px;">
                    <div class="row no-gutters">
                        <div class="col-md-2">
                            <p><strong>Project Name</strong></p>
                            <p><strong>Customer</strong></p>
                            <p><strong>Project Start Date</strong></p>
                            <p><strong>Project Type</strong></p>
                        </div>
                        <div class="col-md-4">
                            <p>{{$project->name}}&nbsp;</p>
                            <p>{{$project->customer?->customer_name}}&nbsp;</p>
                            <p>{{$project->start_date}}&nbsp;</p>
                            <p>{{$project->project_type?->description}}&nbsp;</p>
                        </div>
                        <div class="col-md-2">
                            <p><strong>Project Ref. (Timesheet)</strong></p>
                            <p><strong>Customer PO</strong></p>
                            <p><strong>Project End Date</strong></p>
                            <p><strong>Project Status</strong></p>
                        </div>
                        <div class="col-md-4">
                            <p>{{$project->ref}}&nbsp;</p>
                            <p>{{$project->customer_po}}&nbsp;</p>
                            <p>{{$project->end_date}}&nbsp;</p>
                            <p>{{$project->status?->description}}&nbsp;</p>
                        </div>
                    </div>
            </div>
            @if(count($errors->all()))
                <div class="alert alert-danger alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Error!</strong> Validation failed, please update your form
                </div>
            @endif

            {{Form::open(['url' => route('assignment.store'), 'method' => 'post','class'=>'mt-3', 'files' => true, 'autocomplete' => 'off','id'=>'editassignment'])}}
            <input  type="hidden" name="project_id" id="project_id" value="{{$project->id}}">
            <input type="hidden" id="project_start_date" value="{{$project->start_date}}" />
            <input type="hidden" id="project_end_date" value="{{$project->end_date}}" />
            <input type="hidden" name="employee_id" value="{{$resource->id}}">
                <ul class="nav nav-tabs mt-3">
                    <li class="">
                        <a href="#basics" class="active" data-toggle="tab">Basics</a>
                    </li>
                    <li class="">
                        <a href="#assignment" class="" data-toggle="tab">Assignment</a>
                    </li>
                    <li class="">
                        <a href="#approval" class="" data-toggle="tab">Approval</a>
                    </li>
                    <li class="">
                        <a href="#reporting" class="" data-toggle="tab">Reporting</a>
                    </li>
                    <li class="">
                        <a href="#planning" class="" data-toggle="tab">Planning</a>
                    </li>
                    <li class="">
                        <a href="#statistics" class="" data-toggle="tab">Statistics</a>
                    </li>
                    <li class="">
                        <a href="#expenses" class="" data-toggle="tab">Expenses</a>
                    </li>
                    <li class="">
                        <a href="#costs" class="" data-toggle="tab">Costs</a>
                    </li>
                    <li class="">
                        <a href="#referrals_extention" class="" data-toggle="tab">Referrals and Extentions</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="basics">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <thead>
                                <tr class="bg-dark text-center">
                                    <th colspan="4">Assignment For {{$resource->name()}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Assignment start date. The project start date is defaulted."></i>Assignment Start Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Assignment Start Date:: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Assignment start date. The project start date is defaulted."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id' => 'start_date', 'placeholder'=>'Start Date'])}}
                                        @foreach($errors->get('start_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Assignment end date. The project end date is defaulted."></i>Assignment End Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Assignment End Date: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Assignment end date. The project end date is defaulted."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'placeholder'=>'End Date'])}}
                                        @foreach($errors->get('end_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Hours available to this assignment (part of the project). The field could be 0."></i>PO Hours: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        PO Hours: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Hours available to this assignment (part of the project). The field could be 0."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>{{Form::text('hours',old('hours'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours') ? ' is-invalid' : ''),'placeholder'=>'Hours'])}}
                                        @foreach($errors->get('hours') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Text that explains the high level scope of the assignment in the project for this consultant. This will print on the assignment agreement."></i>Scope: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Scope: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Text that explains the high level scope of the assignment in the project for this consultant. This will print on the assignment agreement."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td colspan="3">
                                        {{Form::textarea('note_1',old('note_1'),['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_1') ? ' is-invalid' : ''),'placeholder'=>'Scope'])}}
                                        @foreach($errors->get('note_1') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="assignment">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    {{-- <th style='width: 20%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Customer purchase order reference - This could be specific to the consultant assignment."></i>Customer PO:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Customer PO: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Customer purchase order reference - This could be specific to the consultant assignment."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td style="width: 30%">{{Form::text('customer_po',$project->customer_po,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_po') ? ' is-invalid' : ''), 'placeholder' => 'Customer PO'])}}
                                        @foreach($errors->get('customer_po') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th style='width: 20%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Yes if customer billable. No if internal or non-billable."></i>Billable: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Billable: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Yes if customer billable. No if internal or non-billable."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>
                                        {{Form::select('billable',$yes_or_no_dropdown,$project->billable?->value,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('billable') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Hours allowed to work or bill per day."></i>Hours of Work per day:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Hours of Work per day: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Hours allowed to work or bill per day."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('hours_of_work',$project->hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''),'placeholder'=>'0'])}}
                                        @foreach($errors->get('hours_of_work') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Project role for timesheet approvers. Select the user's name from the list. Only users with internal and customer roles are available for selection."></i>Assignment Role:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Assignment Role: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Project role for timesheet approvers. Select the user's name from the list. Only users with internal and customer roles are available for selection."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('role',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('role') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The status of a assignment determines if it needs to be available for planning and time capture. Only assignments with status In Progress or Confirmed are available for time capturing."></i>Assignment Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Assignment Status: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The status of a assignment determines if it needs to be available for planning and time capture. Only assignments with status In Progress or Confirmed are available for time capturing."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>
                                        {{Form::select('assignment_status',$project_status_dropdown,3,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('assignment_status') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('assignment_status') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Status of the record - Active is useable. Suspended and closed will be ignored by any reporting."></i>Record Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Record Status: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Status of the record - Active is useable. Suspended and closed will be ignored by any reporting."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>
                                        {{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('status') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('status') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Where will the project be executed from. E.g. Remote or Dubai office"></i>Location:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Location: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Where will the project be executed from. E.g. Remote or Dubai office."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('location',$project->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('location') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Additional text field"></i>Note:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Note: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Additional text field"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td colspan="3">
                                        {{Form::textarea('note_2',old('note_2'),['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_2') ? ' is-invalid' : ''),'placeholder'=>'Note'])}}
                                        @foreach($errors->get('note_2') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="approval">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    {{-- <th>Internal Owner: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Internal Owner: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the internal owner for the assignment from a list of users. The internal owner will automatically be added as an optional approver of timesheets for the assignment and will have access to view the assignment record."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td>{{Form::select('manager_id',$resource_managers_dropdown,$project->manager_id,['class'=>'form-control form-control-sm  col-sm-12  ', 'disabled'=>'disabled' , 'id' => 'internal_owner'])}}
                                        @foreach($errors->get('manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Option text to replace the default project role - to be displayed on the timesheet template"></i>Display Name:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Display Name: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Option text to replace the default project role - to be displayed on the timesheet template."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::text('internal_owner_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_owner_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('internal_owner_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Yes - display on timesheet template, No - ignore."></i>Timesheet Approver:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Timesheet Approver: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Yes - display on timesheet template, No - ignore."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td style="width: 150px;">
                                        {{Form::select('project_manager_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('project_manager_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Product Owner</th>
                                    <td>
                                        {{Form::select('product_owner_id',$resource_managers_dropdown, null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_id') ? ' is-invalid' : ''), 'placeholder' => "Select Product Owner"])}}
                                        @foreach($errors->get('product_owner_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Display Name:</th>
                                    <td>
                                        {{Form::text('product_owner_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('product_owner_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('product_owner_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Timesheet Approver:</th>
                                    <td>
                                        {{Form::select('product_owner_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('product_owner_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Project Manager:</th>
                                    <td>{{Form::select('project_manager_new_id',$resource_managers_dropdown, null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_id') ? ' is-invalid' : ''), 'placeholder' => "Select Product Manager"])}}
                                        @foreach($errors->get('project_manager_new_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Display Name:</th>
                                    <td>
                                        {{Form::text('project_manager_new_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_manager_new_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('project_manager_new_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Timesheet Approver:</th>
                                    <td>
                                        {{Form::select('project_manager_new_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('project_manager_new_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Line Manager:</th>
                                    <td>{{Form::select('line_manager_id',$resource_managers_dropdown, null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_id') ? ' is-invalid' : ''), 'placeholder' => "Select Line Manager"])}}
                                        @foreach($errors->get('line_manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Display Name:</th>
                                    <td>
                                        {{Form::text('line_manager_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('line_manager_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('line_manager_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Timesheet Approver:</th>
                                    <td>
                                        {{Form::select('line_manager_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('line_manager_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Claim Approver:</th>
                                    <td>{{Form::select('assignment_approver',$resource_managers_dropdown, $project->manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('assignment_approver') ? ' is-invalid' : ''), 'id' => 'claim_approver', 'placeholder' => "Select Claim Approver"])}}
                                        @foreach($errors->get('assignment_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Display Name:</th>
                                    <td>
                                        {{Form::text('claim_approver_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('claim_approver_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('claim_approver_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Timesheet Approver:</th>
                                    <td>
                                        {{Form::select('claim_approver_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('claim_approver_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('claim_approver_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Resource Manager:</th>
                                    <td>{{Form::select('resource_manager_id',$resource_managers_dropdown, null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_id') ? ' is-invalid' : ''), 'placeholder' => "Select Resource Manager"])}}
                                        @foreach($errors->get('resource_manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Display Name:</th>
                                    <td>
                                        {{Form::text('resource_manager_known_as', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_manager_known_as') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('resource_manager_known_as') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Timesheet Approver:</th>
                                    <td>
                                        {{Form::select('resource_manager_ts_approver', $yes_or_no_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_ts_approver') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('resource_manager_ts_approver') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="reporting">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."></i>Report to:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Report to: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::text('report_to', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to') ? ' is-invalid' : ''),'placeholder'=>'Report to', 'id' => 'report_to'])}}
                                        @foreach($errors->get('report_to') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select an existing user from the list to pre-fill the contact details"></i>Copy from User:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Copy from User: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select an existing user from the list to pre-fill the contact details"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('copy_from_user', $users_dropdown, null, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('copy_from_user') ? ' is-invalid' : ''), 'id' => 'copy_from_user', 'placeholder' => 'Consultant'])}}
                                        @foreach($errors->get('copy_from_user') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."></i>Report to email:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Report to email: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::text('report_to_email', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to_email') ? ' is-invalid' : ''),'placeholder'=>'Email', 'id' => 'report_to_email'])}}
                                        @foreach($errors->get('report_to_email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."></i>Report to phone:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Report to phone: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="User that the assigned consultant will report to during this project. This information prints on the assignment agreement."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::text('report_phone', null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_phone') ? ' is-invalid' : ''),'placeholder'=>'Phone', 'id' => 'report_phone'])}}
                                        @foreach($errors->get('report_phone') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the template to use as an addendum to the master agreement to be distributed to the Vendor."></i>Vendor Agreement Template:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Vendor Agreement Template: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the template to use as an addendum to the master agreement to be distributed to the Vendor."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('vendor_template_id', $vendor_template_dropdown, $config->vendor_template_id,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('vendor_template_id') ? ' is-invalid' : ''), 'id'=>'vendor_template_id'])}}
                                        @foreach($errors->get('vendor_template_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the template to use as an agreement to be distributed to the Consultant."></i>Resource Agreement Template:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Resource Agreement Template: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the template to use as an agreement to be distributed to the Consultant."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('resource_template_id', $templates_dropdown, $config->resource_template_id,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_template_id') ? ' is-invalid' : ''), 'id'=>'resource_template_id'])}}
                                        @foreach($errors->get('resource_template_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="planning">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The planned start date could be different from the assignment start date due to administration delays. This will affect the planning module."></i>Planned Start Date</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Planned Start Date: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The planned start date could be different from the assignment start date due to administration delays. This will affect the planning module."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('planned_start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_start_date') ? ' is-invalid' : ''), 'id' => 'planned_start_date', 'placeholder'=>'Planned Start Date'])}}
                                        @foreach($errors->get('planned_start_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The planned end date could be different from the assignment end date due to administration delays. This will affect the planning module."></i>Planned End Date</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Planned End Date: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The planned end date could be different from the assignment end date due to administration delays. This will affect the planning module."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('planned_end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_end_date') ? ' is-invalid' : ''), 'id' => 'planned_end_date', 'placeholder'=>'Planned End Date'])}}
                                        @foreach($errors->get('planned_end_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="A warning message is sent to the consultant and Administrator if the max number of hours is exceeded. Warnings can be configured under the Admin/ Setup / Warnings menu"></i>Max Time Per Day Before Exception</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Max Time Per Day Before Exception: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="A warning message is sent to the consultant and Administrator if the max number of hours is exceeded. Warnings can be configured under the Admin/ Setup / Warnings menu"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::select('max_time_per_day_hours',$hours_dropdown,8,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('max_time_per_day_hours') ? ' is-invalid' : ''), 'id' => 'max_time_per_day_hours', 'placeholder'=>'Hours'])}}
                                        @foreach($errors->get('max_time_per_day_hours') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                        {{Form::select('max_time_per_day_min',$minutes_dropdown,0,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('max_time_per_day_min') ? ' is-invalid' : ''), 'id' => 'max_time_per_day_min', 'placeholder'=>'Minutes'])}}
                                        @foreach($errors->get('max_time_per_day_min') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Capacity of this consultant to work on this project. This value will be the default when creating a Sprint."></i>Capacity Allocation Per Day</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Capacity Allocation Per Day: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Capacity of this consultant to work on this project. This value will be the default when creating a Sprint."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::select('capacity_allocation_hours',$hours_dropdown,8,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('capacity_allocation_hours') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_hours', 'placeholder'=>'Hours'])}}
                                        @foreach($errors->get('capacity_allocation_hours') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                        {{Form::select('capacity_allocation_min',$minutes_dropdown,0,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('capacity_allocation_min') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_min', 'placeholder'=>'Minutes'])}}
                                        @foreach($errors->get('capacity_allocation_min') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="% capacity available to work on this project."></i>Capacity Allocation %</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Capacity Allocation %: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="% capacity available to work on this project."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>{{Form::text('capacity_allocation_percentage',old('capacity_allocation_percentage'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('capacity_allocation_percentage') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_percentage', 'placeholder'=>'Capacity Allocation'])}}
                                        @foreach($errors->get('capacity_allocation_percentage') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="statistics">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select a function for reporting and statistics purposes"></i>Function:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Function: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select a function for reporting and statistics purposes"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('function',$business_function_dropdown, old('function'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('function') ? ' is-invalid' : ''),'placeholder'=>'Select Function'])}}
                                        @foreach($errors->get('function') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select a system for reporting and statistics purposes. The list of Systems can be maintained under the Admin / Setup / Master data menu"></i>System:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        System: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select a system for reporting and statistics purposes. The list of Systems can be maintained under the Admin / Setup / Master data menu"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('system',$skill_dropdown, old('system'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('system') ? ' is-invalid' : ''),'placeholder'=>'Select System'])}}
                                        @foreach($errors->get('system') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select a Country of work for reporting and statistics purposes"></i>Country of Work:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Country of Work: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select a Country of work for reporting and statistics purposes"><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    <td>
                                        {{Form::select('country_id',$countries_dropdown,null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('country_id') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('country_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="expenses">
                        <hr style="margin-top:-1px;"/>
                        {{-- <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Inherited from the project terms. It can be changed per consultant."></i> --}}
                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Inherited from the project terms. It can be changed per consultant."><i class="far fa-question-circle"></i></span></sup>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <tbody>
                                <tr>
                                    <th>Travel</th>
                                    <td>{{Form::textarea('travel',$project->expense?->travel,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('travel') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Parking</th>
                                    <td>{{Form::textarea('parking',$project->expense?->parking,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('parking') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Accommodation</th>
                                    <td>{{Form::textarea('accommodation',$project->expense?->accommodation,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('accommodation') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Per Diem</th>
                                    <td>{{Form::textarea('per_diem',$project->expense?->per_diem,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('per_diem') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Out of Town Allowance</th>
                                    <td>{{Form::textarea('out_of_town',$project->expense?->out_of_town,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('out_of_town') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th>Data</th>
                                    <td>{{Form::textarea('data',$project->expense?->data,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('data') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Other</th>
                                    <td colspan="3">{{Form::textarea('other',$project->expense?->other,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'])}}
                                        @foreach($errors->get('other') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="costs">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-bordered table-sm mt-3">
                                <thead>
                                <tr class="text-center bg-dark">
                                    {{-- <th colspan="4"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The primary currency is setup on the Configuration screen under the Admin/Setup/Config menu"></i>Rates - Primary Currency</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Rates - Primary Currency: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The primary currency is setup on the Configuration screen under the Admin/Setup/Config menu."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    {{-- <th style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Internal (staff) cost. This will be prefilled with the standard cost associated in the resource record - if it is used. Leave 0 if it is a vendor based resource."></i>Internal Cost:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Internal Cost: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Internal (staff) cost. This will be prefilled with the standard cost associated in the resource record - if it is used. Leave 0 if it is a vendor based resource."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    {{-- <td style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Cost per hour associated to the consultant where the consultant is a vendor based resource or independent contractor."></i>External Cost</td> --}}
                                    <th class="col-md-2 p-2" >
                                        External Cost: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Cost per hour associated to the consultant where the consultant is a vendor based resource or independent contractor."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    {{-- <th style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Rate charged per hour to the customer. Excluding taxes."></i>Invoice Rate</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Invoice Rate: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Rate charged per hour to the customer. Excluding taxes."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    {{-- <td style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Rate that will be used to calculate bonusses on. If the commission module is used."></i>Bonus Rate</td> --}}
                                    <th class="col-md-2 p-2" >
                                        Bonus Rate: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Rate that will be used to calculate bonusses on. If the commission module is used."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        {{Form::text('internal_cost_rate',$resource->resource?->standard_cost?->standard_cost_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "internal_cost_rate"])}}
                                        @foreach($errors->get('internal_cost_rate') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        {{Form::text('external_cost_rate',old('external_cost_rate'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "external_cost_rate"])}}
                                        @foreach($errors->get('external_cost_rate') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        {{Form::text('invoice_rate',old('invoice_rate'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "invoice_rate"])}}
                                        @foreach($errors->get('invoice_rate') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        {{Form::text('rate',old('rate'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "rate"])}}
                                        @foreach($errors->get('rate') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr class="text-center bg-dark">
                                    <th colspan="4">Rates - Secondary Currency</th>
                                </tr>
                                <tr>
                                    {{-- <th style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="If a secondary currency is used to bill a customer or pay a vendor."></i>Currency:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Currency: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="If a secondary currency is used to bill a customer or pay a vendor."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                    {{-- <th colspan="3" style='font-weight: bold; width: 25%'><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The exchange rate is looked up at the point of selection. All secondary prices will be recalculated based on the exchange rate, but could be changed."></i>Exchange Rate:</th> --}}
                                    <th class="col-md-2 p-2" >
                                        Exchange Rate: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The exchange rate is looked up at the point of selection. All secondary prices will be recalculated based on the exchange rate, but could be changed."><i class="far fa-question-circle"></i></span></sup>
                                    </th>
                                </tr>
                                <tr>
                                    <td style='width: 25%'>
                                        {{Form::select('currency_sec', $currencies, null,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('currency_sec') ? ' is-invalid' : ''), 'placeholder'=>'Select Currency', 'id' => "currency_sec"])}}
                                        @foreach($errors->get('currency_sec') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th colspan="3" style='font-weight: bold; width: 25%' id="exchange-rate"></th>

                                </tr>
                                <tr>
                                    <th style='font-weight: bold; width: 25%'>Internal Cost:</th>
                                    <td style='font-weight: bold; width: 25%'>External Cost</td>
                                    <th style='font-weight: bold; width: 25%'>Invoice Rate</th>
                                    <td style='font-weight: bold; width: 25%'>Bonus Rate</td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        {{Form::text('internal_cost_rate_sec',$resource->resource?->standard_cost?->standard_cost_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "internal_cost_rate_sec"])}}
                                        @foreach($errors->get('internal_cost_rate_sec') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        {{Form::text('external_cost_rate_sec',old('external_cost_rate_sec'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "external_cost_rate_sec"])}}
                                        @foreach($errors->get('external_cost_rate_sec') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        {{Form::text('invoice_rate_sec',old('invoice_rate_sec'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "invoice_rate_sec"])}}
                                        @foreach($errors->get('invoice_rate') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        {{Form::text('rate_sec',old('rate_sec'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('rate_sec') ? ' is-invalid' : ''), 'placeholder'=>'0.00', "id" => "rate_sec"])}}
                                        @foreach($errors->get('rate_sec') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="referrals_extention">
                        <hr style="margin-top:-1px;"/>
                        <div class="table-responsive-md">
                            <table class="table table-sm table-bordered mt-3">
                                <tbody>
                                <tr>
                                    <th>Rate per Billable Hour:</th>
                                    <td class="text-right">0.00</td>
                                    <th>Rate per Non-Billable Hour:</th>
                                    <td class="text-right">0.00</td>
                                </tr>
                                <tr>
                                    <th>% of Invoice Rate:</th>
                                    <td class="text-right">0.00</td>
                                    <th>Assignment Fixed Cost:</th>
                                    <td class="text-right">0.00</td>
                                </tr>
                                <tr>
                                    <th>Due on Assignment Status:</th>
                                    <td></td>
                                    <th></th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Referred Vendor:</th>
                                    <td></td>
                                    <th>or Referred User:</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-center"><button class="btn-dark btn btn-sm disabled">Process Once-off Commission</button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $(function (){
            $("#currency_sec").on("change", () => {
                let currency = $("#currency_sec").val();
                let internal_cost_rate = $("#internal_cost_rate").val();
                let external_cost_rate = $("#external_cost_rate").val();
                let invoice_rate = $("#invoice_rate").val();
                let rate = $("#rate").val();

                fetch(`/exchange-rate?currency=${currency}&internal_cost_rate=${internal_cost_rate}&external_cost_rate=${external_cost_rate}&invoice_rate=${invoice_rate}&rate=${rate}`).then(res => res.json()).then(data =>{
                    if (data.rate > 0){
                        $("#exchange-rate").text((1/data.rate).toFixed(2))
                        $("#internal_cost_rate_sec").val(data.internal_cost_rate_sec);
                        $("#external_cost_rate_sec").val(data.external_cost_rate_sec);
                        $("#invoice_rate_sec").val(data.invoice_rate_sec);
                        $("#rate_sec").val(data.rate_sec);
                    }
                    console.log(data)
                }).catch(err => err.response);
            })
        })

        var project_start_date = new Date($('#project_start_date').val());
        var project_end_date = new Date($('#project_end_date').val());

        $('#start_date').change(function(){
            var assignment_start_date = new Date($('#start_date').val());
            if(assignment_start_date < project_start_date){
                alert("Assignment start date can not be less than project start date, this value will be changed back to project start date");
                $('#start_date').val($('#project_start_date').val());
            }

            if(assignment_start_date > project_end_date){
                alert("Assignment start date can not be greater than project end date, this value will be changed back to project start date");
                $('#start_date').val($('#project_start_date').val());
            }

            // $('#start_date').val();
            console.log($('#start_date').val());
            console.log(assignment_start_date+' < '+project_start_date);
        });

        $('#end_date').change(function(){
            var assignment_end_date = new Date($('#end_date').val());
            if(assignment_end_date < project_start_date){
                alert("Assignment end date can not be less than project start date, this value will be changed back to project end date");
                $('#end_date').val($('#project_end_date').val());
            }

            if(assignment_end_date > project_end_date){
                alert("Assignment end date can not be greater than project end date, this value will be changed back to project start date");
                $('#end_date').val($('#project_end_date').val());
            }
        });

        $(function () {
            $("#max_time_per_day_hours").on('change', function () {
                if ($(this).val() == 24){
                    $('#max_time_per_day_min').val(0).attr('disabled', true).trigger('chosen:updated');
                }
            });
            $("#capacity_allocation_hours").on('change',function () {
                if ($(this).val() == 24){
                    $('#capacity_allocation_min').val(0).attr('disabled', true).trigger('chosen:updated');
                }
                let capacity_min = 0;
                if ($("#capacity_allocation_min").val() != '') capacity_min = $("#capacity_allocation_min").val();
                let capacity = (Number($(this).val()) + (Number(capacity_min)/60))/8;
                $("#capacity_allocation_percentage").val(Math.round(capacity*100));
            });
            $("#capacity_allocation_min").on('change', function () {
                let capacity_hrs = 0;
                if ($("#capacity_allocation_hours").val() != '') capacity_hrs =  $("#capacity_allocation_hours").val();
                let capacity_hours = ((Number($(this).val())/60) + Number(capacity_hrs))/8;
                $("#capacity_allocation_percentage").val(Math.round(capacity_hours*100));
            });

            $('#copy_from_user').on('change', function(){
                var data = {
                    user_id: $("#copy_from_user").val()
                };
                var user_id = $("#copy_from_user").val();

                if(user_id == 0){
                    $('#report_to').val('');
                    $('#report_to_email').val('');
                    $('#report_phone').val('');
                    return false;
                }

                axios.post('{{route('assignment.getuserdetails')}}', data)
                    .then(function (data) {
                        $('#report_to').val(data['data'].first_name+' '+data['data'].last_name);
                        $('#report_to_email').val(data['data'].email);
                        $('#report_phone').val(data['data'].phone);
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });

            });

        });

    </script>
@endsection
