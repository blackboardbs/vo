@extends('adminlte.default')
@section('title') Add Assignment Extension @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('assignment.saveextension', $assignment_id), 'method' => 'put','class'=>'mt-3', 'files' => true])}}
            <input  type="hidden" name="assignment_id" id="assignment_id" value="{{$assignment_id}}">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="6" class="btn-dark" style="text-align: center;">Extension</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Extension Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{Form::text('extension_date',now()->toDateString(),['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''),'placeholder' => 'Extension Date'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Ext Hours</th>
                        <td>
                            {{Form::text('extension_hours',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_hours') ? ' is-invalid' : ''), 'placeholder'=>'0'])}}
                            @foreach($errors->get('extension_hours') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Start <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{Form::text('extension_start_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date'])}}
                            @foreach($errors->get('extension_start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>End <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{Form::text('extension_end_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_end_date') ? ' is-invalid' : ''), 'placeholder'=>'End Date'])}}
                            @foreach($errors->get('extension_end_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Ext Notes</th>
                        <td>
                            {{Form::text('extension_notes',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_notes') ? ' is-invalid' : ''), 'placeholder'=>'Notes'])}}
                            @foreach($errors->get('extension_notes') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Ref</th>
                        <td>
                            {{Form::text('extension_ref',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_ref') ? ' is-invalid' : ''), 'placeholder'=>'Reference'])}}
                            @foreach($errors->get('extension_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script type="text/javascript">
        $(function () {
            $('.').css('width', '100%');
            $('.chosen-container').css('width', '48%');
        });
    </script>
@endsection