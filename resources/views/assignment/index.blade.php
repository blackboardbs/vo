@extends('adminlte.default')
@section('title') Assignments @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="assignment.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company', $company_drop_down, request()->company, ['class' => 'form-control w-100 search', 'id' => 'company_id', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('customer', $customer_drop_down, request()->customer, ['class' => 'form-control w-100 search', 'id' => 'customer_id', 'placeholder' => 'All'])}}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project', $project_drop_down, request()->project, ['class' => 'form-control w-100 search', 'id' => 'project', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource_id', $resource_drop_down, request()->resource_id, ['class' => 'form-control w-100 search', 'id' => 'resource_id', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('function_id', $business_function_dropdown, old('function_id', 0), ['class' => 'form-control w-100 search', 'id' => 'function_id', 'placeholder' => 'All'])}}
                        <span>Function</span>
                    </label>
                </div>
            </div>

            <div class="col-sm-4 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project_status_id', $status_drop_down, old('project_status_id', 0), ['class' => 'form-control w-100 search', 'id' => 'project_status_id', 'placeholder' => 'All'])}}
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="start_date" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}" class="datepicker start_date form-control col-sm-12 search w-100">
                        {{-- {{Form::text('start_date',null,['class'=>'datepicker start_date form-control col-sm-12 search'. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date','style'=>'width:100% !important;'])}} --}}
                        <span>Start Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="end_date" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}" class="datepicker end_date form-control col-sm-12 search w-100">
                        {{-- {{Form::text('start_date',null,['class'=>'datepicker start_date form-control col-sm-12 search'. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date','style'=>'width:100% !important;'])}} --}}
                        <span>End Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <a href="{{route('assignment.index')}}" class="btn btn-info col-md-12" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('consultant.first_name', 'Consultant')</th>
                    <th>@sortablelink('project.name', 'Project')</th>
                    <th>Customer</th>
                    <th>@sortablelink('function_desc.description', 'Function')</th>
                    <th class="text-right pl-3" nowrap>@sortablelink('hours', 'Hours Assigned')</th>
                    <th class="text-right pl-3" nowrap>Actual Bill Hours</th>
                    <th class="text-right pl-3" nowrap>Actual Non-Bill Hours</th>
                    <th class="text-right pl-3" nowrap>Hours Remaining</th>
                    <th class="text-right pl-3" nowrap>@sortablelink('Completion %')</th>
                    <th>@sortablelink('start_date', 'Start')</th>
                    <th>@sortablelink('end_date', 'End')</th>
                    <th>@sortablelink('project_status.description','Status')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($assignments as $assignment)
                    <tr>
                        <td><a href="{{route('assignment.show',$assignment)}}">{{$assignment->consultant?->name()}}</a></td>
                        <td>{{isset($assignment->project->name) ? $assignment->project->name: ''}}</td>
                        <td>{{isset($assignment->project->customer->customer_name) ? $assignment->project->customer->customer_name: ''}}</td>
                        <td>{{isset($assignment->function_desc->description)?$assignment->function_desc->description:''}}</td>
                        <td class="text-right">{{ number_format($assignment->hours,2) }}</td>
                        <td class="text-right">{{ number_format($worked_bill_hours[$assignment->id] ?? 0,2) }}</td>
                        <td class="text-right">{{ number_format($worked_non_bill_hours[$assignment->id] ?? 0,2) }}</td>
                        <td class="text-right">{{ number_format(isset($assignment->project) && $assignment->project->project_type_id == 1 ? ($assignment->hours - (($worked_bill_hours[$assignment->id] ?? 0))) : ($assignment->hours - (($worked_bill_hours[$assignment->id] ?? 0) + ($worked_non_bill_hours[$assignment->id] ?? 0))),2) }}</td>
                        <td class="text-right">{{ ($assignment->hours ? number_format((isset($completion_perc[$assignment->id]) ? $completion_perc[$assignment->id] : 0),2).'%' : number_format(0,2).'%') }}</td>
                        <td nowrap>{{$assignment->start_date}}</td>
                        <td nowrap>{{$assignment->end_date}}</td>
                        <td nowrap>{{isset($assignment->project_status->description)?$assignment->project_status->description:'' }}</td>
                        @if($can_update)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('assignment.edit',$assignment)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['assignment.destroy', $assignment],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm" {{$timesheet_count[$assignment->id] > 0  || $assignment->expense_tracking_count > 0 ? 'disabled' : ''}}><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No assignments match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="4" class="text-right">Total</th>
                    <th class="text-right">{{number_format($total_hours,2)}}</th>
                    <th class="text-right">{{number_format(array_sum($worked_bill_hours),2)}}</th>
                    <th class="text-right">{{number_format(array_sum($worked_non_bill_hours),2)}}</th>
                    <th class="text-right">{{number_format($total_remaining,2)}}</th>
                    <th class="text-right">{{number_format($total_percentage,2)}}%</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $assignments->firstItem() }} - {{ $assignments->lastItem() }} of {{ $assignments->total() }}
                        </td>
                        <td>
                            {{ $assignments->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script src="{{asset('js/filters.js')}}"></script>
    <script>
        $(document).on('ready', function () {

            let project_dropdown = @json(session('project_dropdown'));
            let resource_dropdown = @json(session('resource_dropdown'));
            //const url = new URl(window.location.toLocaleString())

            $("#resource").dropdownSearch({
                name: "resource",
                dropdown: resource_dropdown !== null ? resource_dropdown : []
            });
            $("#project").dropdownSearch({
                name: "project",
                dropdown: project_dropdown !== null ? project_dropdown : []
            });

            /*if (url.searchParams.get('start_date')){
                $("#start_date").val(url.searchParams.get('start_date'))
            }*/
        });
    </script>
@endsection
@section('extra-css')
    <style>
        li.page-item {

            display: none;
        }

        .page-item:first-child,
        .page-item:nth-child( 2 ),
        .page-item:nth-last-child( 2 ),
        .page-item:nth-child( 3 ),
        .page-item:nth-last-child( 3 ),
        .page-item:nth-child( 4 ),
        .page-item:nth-last-child( 4 ),
        .page-item:nth-child( 5 ),
        .page-item:nth-last-child( 5 ),
        .page-item:last-child,
        .page-item.active,
        .page-item.disabled {

            display: block;
        }
        .pagination{
            padding-left: 10px;
            margin-bottom: 0px !important;
        }

        .pagination>li:first-child {
            border-left:0px !important;
        }
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection