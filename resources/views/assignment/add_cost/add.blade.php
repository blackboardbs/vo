@extends('adminlte.default')
@section('title') Add assignment cost @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cost.store'), 'method' => 'post','class'=>'mt-3', 'files' => true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment No.{{$assignment->id}} for {{$assignment->resource->last_name}}, {{$assignment->resource->first_name}}</th>
                    {{ Form::hidden('ass_id', $assignment->id) }}
                    {{ Form::hidden('company', $assignment->resource->company_id) }}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$assignment->project->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',isset($assignment->project->ref)?$assignment->project->ref:'',['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant</th>
                    <td>{{Form::text('consultant',$assignment->resource->last_name.', '.$assignment->resource->last_name,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('consultant') ? ' is-invalid' : ''),'disabled' => 'disabled'])}}
                        @foreach($errors->get('consultant') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer</th>
                    <td>{{Form::text('customer',$assignment->project->customer->customer_name,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('customer') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer PO</th>
                    <td>{{Form::text('customer_po',$assignment->resource->last_name.', '.$assignment->resource->last_name,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('customer_po') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('customer_po') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3 mb-0">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Cost</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">
                            {{Form::text('description',old('description'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'placeholder'=>'Description'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Account: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('account',$account_dropdown,null,['class'=>' form-control form-control-sm col-sm-12'. ($errors->has('account') ? ' is-invalid' : ''),'placeholder' => 'Select Account', 'id' => 'account'])}}
                            @foreach($errors->get('account') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th style="width: 20%">Account Element: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td style="width: 30%">{{Form::select('account_element',['0' => 'Select Account Element'],0,['class'=>' form-control form-control-sm col-sm-12'. ($errors->has('account_element') ? ' is-invalid' : ''), 'id' => 'account_element'])}}
                            @foreach($errors->get('account_element') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Cost: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::text('cost',old('cost'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('cost') ? ' is-invalid' : ''),'placeholder' => 'Cost'])}}
                            @foreach($errors->get('cost') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Payment Frequency:</th>
                        <td>{{Form::text('payment_freq',old('payment_freq'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('payment_freq') ? ' is-invalid' : ''),'placeholder'=>'Payment Frequency'])}}
                            @foreach($errors->get('payment_freq') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Note:</th>
                        <td colspan="3">
                            {{Form::textarea('note',old('note'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('note') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'placeholder'=>'Note', 'rows' => 3])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Payment Method:</th>
                        <td>{{Form::select('payment_method',$payment_method_dropdown, 0,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('payment_method') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('payment_method') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="btn-dark">
                    <th style="width: 40%">Description</th>
                    <th style="width: 20%">Payment Freq.</th>
                    <th style="width: 20%">Payment Method.</th>
                    <th style="width: 20%">Cost</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assignment_costs as $assignment_cost)
                    <tr>
                        <td>{{$assignment_cost->description}}</td>
                        <td>{{$assignment_cost->payment_frequency}}</td>
                        <td>{{isset($assignment_cost->payment_meth->description)?$assignment_cost->payment_meth->description:''}}</td>
                        <td>{{$assignment_cost->cost}}</td>
                    </tr>
                    @empty
                @endforelse
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

            if($("#account").val() != ''){
            let option_change = '';
                axios.post('/getaccounttype', {
                    account: $("#account").val()
                })
                .then(function (response) {
                    $('#account_element').empty();
                    option_change += "<option value='0'>Select Account Element</option>";
                    $.each(response.data.account_element, function (key, value) {
                        option_change += "<option value='" + value.id + "'>" + value.description + "</option>";
                    });
                    $('#account_element').html(option_change).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            }

            $("#account").change(function () {
                let option_change = '';
                axios.post('/getaccounttype', {
                    account: $("#account").val()
                })
                .then(function (response) {
                    $('#account_element').empty();
                    option_change += "<option value='0'>Select Account Element</option>";
                    $.each(response.data.account_element, function (key, value) {
                        option_change += "<option value='" + value.id + "'>" + value.description + "</option>";
                    });
                    $('#account_element').html(option_change).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            });
        });

    </script>
@endsection