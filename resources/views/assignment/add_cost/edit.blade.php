@extends('adminlte.default')
@section('title') Edit assignment cost @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cost.store'), 'method' => 'post','class'=>'mt-3', 'files' => true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment No.{{$assignment_costs->assignment->id}} for {{$assignment_costs->assignment->resource->last_name}}, {{$assignment_costs->assignment->resource->first_name}}</th>
                    {{ Form::hidden('ass_id', $assignment_costs->assignment->id) }}
                    {{ Form::hidden('company', $assignment_costs->assignment->resource->company_id) }}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$assignment_costs->assignment->project->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',$assignment_costs->assignment->project->ref,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Consultant</th>
                    <td>{{Form::text('consultant',$assignment_costs->assignment->resource->last_name.', '.$assignment_costs->assignment->resource->first_name,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('consultant') ? ' is-invalid' : ''),'disabled' => 'disabled'])}}
                        @foreach($errors->get('consultant') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer</th>
                    <td>{{Form::text('customer',$assignment_costs->assignment->project->customer->customer_name,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('customer') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3 mb-0">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Cost</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">
                            {{Form::text('description',$assignment_costs->description,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'placeholder'=>'Description'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Account: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('account',$account_dropdown,$assignment_costs->account_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('account') ? ' is-invalid' : ''),'placeholder' => 'Select Account', 'id' => 'add_ass_cost_account'])}}
                            @foreach($errors->get('consultant') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th style="width: 20%">Account Element: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td style="width: 30%">{{Form::select('account_element',['0' => 'Select Account Element'],$assignment_costs->account_element_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('account_element') ? ' is-invalid' : ''), 'id' => 'ass_cost_account_element'])}}
                            @foreach($errors->get('account_element') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Cost: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::text('cost',$assignment_costs->cost,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('cost') ? ' is-invalid' : ''),'placeholder' => 'Cost'])}}
                            @foreach($errors->get('cost') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Payment Frequency:</th>
                        <td>{{Form::text('payment_freq',$assignment_costs->payment_frequency,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('payment_freq') ? ' is-invalid' : ''),'placeholder'=>'Payment Frequency'])}}
                            @foreach($errors->get('payment_freq') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Note:</th>
                        <td colspan="3">
                            {{Form::textarea('note',$assignment_costs->note,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('note') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'placeholder'=>'Note', 'rows' => 3])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Payment Method:</th>
                        <td>{{Form::select('payment_method',$payment_method_dropdown, $assignment_costs->payment_method,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('payment_method') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('payment_method') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown,$assignment_costs->status,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection


@section('extra-js')

    <script>
        $(function () {

            if($("#add_ass_cost_account").val() != ''){
            let option_change = '';
                axios.post('/getaccounttype', {
                    account: $("#add_ass_cost_account").val()
                })
                .then(function (response) {
                    $('#ass_cost_account_element').empty();
                    option_change += "<option value='0'>Select Account Element</option>";
                    $.each(response.data.account_element, function (key, value) {
                        option_change += "<option value='" + value.id + "'>" + value.description + "</option>";
                    });
                    $('#ass_cost_account_element').html(option_change).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            }

            $("#add_ass_cost_account").change(function () {
                let option_change = '';
                axios.post('/getaccounttype', {
                    account: $("#add_ass_cost_account").val()
                })
                .then(function (response) {
                    $('#ass_cost_account_element').empty();
                    option_change += "<option value='0'>Select Account Element</option>";
                    $.each(response.data.account_element, function (key, value) {
                        option_change += "<option value='" + value.id + "'>" + value.description + "</option>";
                    });
                    $('#ass_cost_account_element').html(option_change).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            });
        });

    </script>
@endsection