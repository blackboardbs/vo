<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead>
        <tr class="btn-dark">
            <th>Consultant</th>
            <th>Project</th>
            <th>Customer</th>
            <th>Function</th>
            <th>Hours</th>
            <th>Total Hours</th>
            <th>Completion %</th>
            <th>Start</th>
            <th>End</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assignments as $assignment)
            <tr>
                <td>{{$assignment->consultant?->name()}}</td>
                <td>{{$assignment->project?->name}}</td>
                <td>{{$assignment->project?->customer?->customer_name}}</td>
                <td>{{$assignment->function_desc?->description}}</td>
                <td>{{$assignment->hours}}</td>
                <td>{{$worked_hours[$assignment->id]}}</td>
                <td>{{number_format($completion_perc[$assignment->id])}}%</td>
                <td>{{$assignment->start_date}}</td>
                <td>{{$assignment->end_date}}</td>
                <td>{{$assignment->project_status?->description}}</td>
            </tr>
        @endforeach
        <tr>
            <th colspan="4" class="text-right">Total</th>
            <th class="text-right">{{$assignments->sum('hours')}}</th>
            <th class="text-right">{{array_sum($worked_hours)}}</th>
            <th class="text-right">{{$total_percentage}}%</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </tbody>
    </table>
</div>