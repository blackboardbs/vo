@extends('adminlte.default')
@section('title') Assignment @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="goBack()" class="btn btn-dark btn-sm float-right" style="margin-right:10px;"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('assignment.edit',$assignment->id)}}" class="btn btn-success btn-sm float-right"><i class="far fa-edit"></i> Edit Assignment</a>
                {{--<a href="{{($path == 1 ? url()->previous() : route('assignment.index'))}}" class="btn btn-dark btn-sm"><i class="fa fa-caret-left"></i> Back</a>--}}
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                <tr>
                    <th colspan="6" class="btn-dark" style="text-align: center;">Assignment Status</th>
                </tr>
                <tr>
                    <th>Record</th>
                    <td>{{isset($assignment->statusd->description)?$assignment->statusd->description: ''}}</td>
                    <th>Assignment</th>
                    <td>{{isset($project_status)?$project_status: ''}}</td>
                    <th>Billable</th>
                    <td>{{isset($assignment->billable) && $assignment->billable == 1 ? 'Yes' : 'No'}}</td>
                </tr>
                <tr>
                    <th>Function</th>
                    <td>{{isset($assignment_function)?$assignment_function:''}}</td>
                    <th>System</th>
                    <td>{{isset($assignment_system)?$assignment_system:''}}</td>
                    <th>Country</th>
                    <td>{{isset($assignment_country)?$assignment_country:''}}</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$completion_perc}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$completion_perc}}%">
                                {{$completion_perc}}%
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Total Billable Hours: {{$assignment->hours + $total_extension_hours}}</td>
                    <td>Billed Hours: {{($assignment->billable == 1)?number_format($total_hours,0,'.',','):number_format($total_non_billable,0,'.',',')}}</td>
                    <td>Remaining Hours: {{($assignment->billable == 1)?number_format(($assignment->hours + $total_extension_hours) - $total_hours,0,'.',','):number_format(($assignment->hours - $total_non_billable),0,'.',',')}}</td>
                    <td colspan="2">Complete: {{$completion_perc}}%</td>
                </tr>
                <tr>
                    <td colspan="6">This is an {{($assignment->billable == 1)?'Income':'Cost'}} project and measures {{($assignment->billable == 1)?'Billable':'Non-Billable'}} hours</td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><a href="{{route('assignment.edit',$assignment)}}" class="btn btn-sm btn-success">Edit Assignment</a></td>
                    <td colspan="3" class="text-right"><a href="{{route('project.edit',$project)}}" class="btn btn-sm btn-success">Edit Project</a></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Project Information</th>
                    {{--<th colspan="4" class="btn-dark" style="text-align: center;">Assignment No.{{$assignment_nr}} for {{$resource->last_name}}, {{$resource->first_name}}</th>--}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$project->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',$project->ref,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>
                        {{Form::select('customer_id',$customer_drop_down,$project->customer_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project Customer PO:</th>
                    <td>{{Form::text('customer_po',$project->customer_po,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_po') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Start Date</th>
                    <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project End Date</th>
                    <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Type</th>
                    <td>{{Form::text('manager_id',isset($project->project_type)?$project->project_type->description:'',['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project Status</th>
                    <td>{{Form::text('assignment_approver',isset($project->status)?$project->status->description:'',['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('assignment_approver') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('assignment_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment No.{{$assignment_nr}} for {{$resource->last_name}}, {{$resource->first_name}}</th>
                </tr>
                <tr>
                    <th>Assignment Start Date</th>
                    <td>{{Form::text('start_date',$assignment->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assignment End Date</th>
                    <td>{{Form::text('end_date',$assignment->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer PO:</th>
                    <td>{{Form::text('customer_po',$assignment->customer_po,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_po') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>PO Hours:</th>
                    <td>{{Form::text('hours',($assignment->hours + $total_extension_hours),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours') ? ' is-invalid' : ''),'placeholder'=>'Hours', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable:</th>
                    <td>
                        {{Form::select('billable',[0=>'No', 1=>'Yes'],$assignment->billable,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Hours of Work per day:</th>
                    <td>{{Form::text('hours_of_work',$assignment->hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''),'placeholder'=>'0', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('hours_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assignment Role:</th>
                    <td>{{Form::text('role',$assignment->role,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('role') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Location:</th>
                    <td>{{Form::text('location',$assignment->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('is_vendor') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('location') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assignment Status:</th>
                    <td>
                        {{Form::select('assignment_status',$project_drop_down,$assignment->assignment_status,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('assignment_status') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('assignment_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Record Status:</th>
                    <td>
                        {{Form::select('status',$status_drop_down,$assignment->status,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('status') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Scope:</th>
                    <td colspan="3">
                        {{Form::textarea('note_1',$assignment->note_1,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_1') ? ' is-invalid' : ''),'placeholder'=>'Scope', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('note_1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note_2',$assignment->note_2,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note_2') ? ' is-invalid' : ''),'placeholder'=>'Note', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('note_2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Re-assign Consultant:</th>
                    <td>
                        {{Form::select('employee_id',$consultant_drop_down,$assignment->employee_id,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('employee_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('employee_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                <tr>
                    <th colspan="6" class="btn-dark" style="text-align: center;">Assignment Approval</th>
                </tr>
                <tr>
                    <th>Internal Owner: <span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('manager_id',$users_drop_down, $project->manager_id,['class'=>'form-control form-control-sm  col-sm-12  ', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('internal_owner_known_as',$assignment->internal_owner_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_owner_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('internal_owner_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td style="width: 150px;">
                        {{Form::select('project_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->project_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Product Owner</th>
                    <td>
                        {{Form::select('product_owner_id',$users_drop_down,$assignment->product_owner_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('product_owner_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('product_owner_known_as',$assignment->product_owner_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('product_owner_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('product_owner_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('product_owner_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->product_owner_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('product_owner_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('product_owner_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Manager:</th>
                    <td>{{Form::select('project_manager_new_id',$users_drop_down,$assignment->project_manager_new_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_manager_new_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('project_manager_new_known_as',$assignment->project_manager_new_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_manager_new_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_manager_new_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('project_manager_new_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->project_manager_new_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('project_manager_new_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_manager_new_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Line Manager:</th>
                    <td>{{Form::select('line_manager_id',$users_drop_down,$assignment->line_manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('line_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('line_manager_known_as',$assignment->line_manager_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('line_manager_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('line_manager_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('line_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->line_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('line_manager_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('line_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Claim Approver:</th>
                    <td>{{Form::select('assignment_approver',$users_drop_down,$assignment->assignment_approver,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('assignment_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('assignment_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('claim_approver_known_as',$assignment->claim_approver_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('claim_approver_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('claim_approver_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('claim_approver_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->claim_approver_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('claim_approver_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('claim_approver_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Resource Manager:</th>
                    <td>{{Form::select('resource_manager_id', $users_drop_down,$assignment->resource_manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('resource_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Display Name:</th>
                    <td>
                        {{Form::text('resource_manager_known_as',$assignment->resource_manager_known_as,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_manager_known_as') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('resource_manager_known_as') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Timesheet Approver:</th>
                    <td>
                        {{Form::select('resource_manager_ts_approver', ['0' => 'No', '1' => 'Yes'], $assignment->resource_manager_ts_approver, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource_manager_ts_approver') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('resource_manager_ts_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Reporting</th>
                </tr>
                <tr>
                    <th>Report to:</th>
                    <td>
                        {{Form::text('report_to',$assignment->report_to,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to') ? ' is-invalid' : ''),'placeholder'=>'Report to', 'id' => 'report_to', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('report_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Copy from User:</th>
                    <td>
                        {{Form::select('copy_from_user', $consultant_drop_down, $assignment->copy_from_user, ['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('copy_from_user') ? ' is-invalid' : ''), 'id' => 'copy_from_user', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('copy_from_user') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Report to email:</th>
                    <td>
                        {{Form::text('report_to_email',$assignment->report_to_email,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_to_email') ? ' is-invalid' : ''),'placeholder'=>'Email', 'id' => 'report_to_email', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('report_to_email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Report to phone:</th>
                    <td>
                        {{Form::text('report_phone',$assignment->report_phone,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('report_phone') ? ' is-invalid' : ''),'placeholder'=>'Phone', 'id' => 'report_phone', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('report_phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                @if($is_able_role)
                    <tr>
                        <th>Vendor Agreement Template:</th>
                        <td>
                            <span class="form-control-sm col-sm-12 input-group" style="padding: 0px; margin: 0px;">
                                {{Form::select('vendor_template_id', $vendor_template_drop_down, $assignment->vendor_template_id > 0 ? $assignment->vendor_template_id : ($isVendorResource == true ? $confiVendorTemplate : null), ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('vendor_template_id') ? ' is-invalid' : ''), 'id'=>'vendor_template_id', 'disabled'=>'disabled'])}}
                                @foreach($errors->get('vendor_template_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                                <span class="input-group-append" onclick="submitTemplateVendor(1, {{$assignment->id}}, 1)">
                                    <button type="button" class="btn-sm btn-multiple">Preview Template</button>
                                </span>
                            </span>
                        </td>
                        <th>Resource Agreement Template:</th>
                        <td>
                        <span class="form-control-sm col-sm-12 input-group" style="padding: 0px; margin: 0px;">
                            {{Form::select('resource_template_id', $resource_template_drop_down, $assignment->resource_template_id > 0 ? $assignment->resource_template_id : $configResourceTemplate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_template_id') ? ' is-invalid' : ''), 'id'=>'resource_template_id', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('resource_template_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            <span class="input-group-append" onclick="submitTemplateResource(1, {{$assignment->id}}, 2)">
                                <button type="button" class="btn-sm btn-multiple">Preview Template</button>
                            </span>
                        </span>
                        </td>
                    </tr>
                @endif
                <tr>
                    <th>Assignment Issued Date:</th>
                    <td>{{Form::text('issue_date', $assignment->issue_date,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('issue_date') ? ' is-invalid' : ''), 'disabled' => 'disabled', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('issue_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assignment Approved Date:</th>
                    <td>{{Form::text('approved_date', $assignment->approved_date,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('approved_date') ? ' is-invalid' : ''), 'disabled' => 'disabled', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('approved_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                @if($is_able_role)
                    <tr>
                        <td colspan="3">
                            @php
                                $vendorTemplateSet = false;
                                if($isVendorResource == true){
                                    if(isset($confiVendorTemplate) && ($confiVendorTemplate > 0)){
                                        $vendorTemplateSet = true;
                                    }
                                    if($assignment->vendor_template_id > 0){
                                        $vendorTemplateSet = true;
                                    }
                                }


                            @endphp
                            @if($vendorTemplateSet)
                                <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#assignemntDigisignModal"><i class="fas fa-paper-plane"></i> Send Vendor Digisign</a>
                                {{-- <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#resourceDigisignModal"><i class="fas fa-paper-plane"></i> Send Resource Digisign</a>--}}
                            @else
                                {{--<a onclick="noVendorTemplate()" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-paper-plane"></i> Send Assignment Digisign</a>--}}
                            @endif
                        </td>
                        <td colspan="1">
                            {{--@if($isVendorResource)--}}
                                @if($assignment->resource_template_id > 0)
                                    <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#assignemntResourceDigisignModal"><i class="fas fa-paper-plane"></i> Send Assignment Digisign</a>
                                @else
                                    {{--<a onclick="noResourceTemplate()" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-paper-plane"></i> Send Assignment Digisign</a>--}}
                                @endif
                            {{--@endif--}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Planning Information</th>
                </tr>
                <tr>
                    <th>Planned Work Start Date:</th>
                    <td>{{Form::text('planned_start_date',$assignment->planned_start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_start_date') ? ' is-invalid' : ''), 'id' => 'planned_start_date', 'placeholder'=>'Planned Start Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('planned_start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Planned Work End Date:</th>
                    <td>{{Form::text('planned_end_date',$assignment->planned_end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('planned_end_date') ? ' is-invalid' : ''), 'id' => 'planned_end_date', 'placeholder'=>'Planned End Date', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('planned_end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Max Time Per Day Before Exception</th>
                    <td>{{Form::text('max_time_per_day_hours',(($assignment->max_time_per_day_hours < 10)?'0'.$assignment->max_time_per_day_hours:$assignment->max_time_per_day_hours).':'.(isset($assignment->max_time_per_day_min)?(($assignment->max_time_per_day_min < 10)?'0'.$assignment->max_time_per_day_min:$assignment->max_time_per_day_min):'00'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('max_time_per_day_hours') ? ' is-invalid' : ''), 'id' => 'max_time_per_day_hours', 'placeholder'=>'Hours', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('max_time_per_day_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Capacity Allocation Per Day</th>
                    <td>{{Form::text('capacity_allocation_hours',(($assignment->capacity_allocation_hours < 10)?'0'.$assignment->capacity_allocation_hours:$assignment->capacity_allocation_hours).':'.(isset($assignment->capacity_allocation_min)?(($assignment->capacity_allocation_min < 10)?'0'.$assignment->capacity_allocation_min:$assignment->capacity_allocation_min):'00'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('capacity_allocation_hours') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_hours', 'placeholder'=>'Hours', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('capacity_allocation_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <th>Capacity Allocation %</th>
                    <td>{{Form::text('capacity_allocation_percentage',isset($assignment->capacity_allocation_percentage)?$assignment->capacity_allocation_percentage.'%':'0%',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('capacity_allocation_percentage') ? ' is-invalid' : ''), 'id' => 'capacity_allocation_percentage', 'placeholder'=>'Capacity Allocation', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('capacity_allocation_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Billable and Claimable Expenses</th>
                </tr>
                <tr>
                    <th>Travel</th>
                    <td>{{Form::textarea('travel',isset($expense->travel)?$expense->travel:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination.', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('travel') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Parking</th>
                    <td>{{Form::textarea('parking',isset($expense->parking)?$expense->parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred, including Airport parking.', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('parking') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Accommodation</th>
                    <td>{{Form::textarea('accommodation',isset($expense->accommodation)?$expense->accomodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'To be arranged by the client.', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('accommodation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Per Diem</th>
                    <td>{{Form::textarea('per_diem',isset($expense->per_diem)?$expense->per_diem:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('per_diem') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Out of Town Allowance</th>
                    <td>{{Form::textarea('out_of_town',isset($expense->out_of_town)?$expense->out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'R300 per day in South Africa and US$ 80 per day internationally (including Zimbabwe, Botswana).', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('out_of_town') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Data</th>
                    <td>{{Form::textarea('data',isset($expense->data)?$expense->data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('data') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Other</th>
                    <td colspan="3">{{Form::textarea('other',isset($expense->other)?$expense->other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('other') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Planned Assignment Cost</th>
                </tr>
                <tr>
                    @if($is_able_role)
                        <th style='font-weight: bold; width: 25%'>Internal Cost:</th>
                        <th style='font-weight: bold; width: 25%'>External Cost</th>
                        <th style='font-weight: bold; width: 25%'>Invoice Rate</th>
                    @endif
                    <th style='font-weight: bold; width: 25%'>Bonus Rate</th>
                </tr>
                <tr>
                    @if($is_able_role)
                        <td>
                            {{Form::text('internal_cost_rate',$assignment->currency.' '.number_format($assignment->internal_cost_rate,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled'=>'disabled'])}}

                        </td>
                        <td>
                            {{Form::text('external_cost_rate',$assignment->currency.' '.number_format($assignment->external_cost_rate,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled'=>'disabled'])}}
                        </td>
                        <td>
                            {{Form::text('invoice_rate',$assignment->currency.' '.number_format($assignment->invoice_rate,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled'=>'disabled'])}}
                        </td>
                    @endif
                    <td>
                        {{Form::text('rate',$assignment->currency.' '.number_format($assignment->rate,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                @if($is_able_role)
                    <tr>
                        <th style='width: 25%'>Total Labour</th>
                        <th style='width: 25%'>Total Additional Cost</th>
                        <th style='width: 25%'>Total Invoice Amount</th>
                        <th style='width: 25%'>Total Profit</th>
                    </tr>
                    <tr>
                        <td>
                            {{Form::text('total_labour',$assignment->currency.' '.number_format(($assignment->internal_cost_rate + $assignment->external_cost_rate) * $assignment->hours ,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('internal_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::text('external_cost_rate',$assignment->currency.' '.number_format($total_addional_cost,2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('external_cost_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'placeholder'=>'0.00', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::text('invoice_rate',$assignment->currency.' '.number_format(($assignment->invoice_rate * $assignment->hours),2,'.',','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('invoice_rate') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'placeholder'=>'0.00', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::text('profit',$profit,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('profit') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('profit') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Fixed Labour Cost
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Fixed Price
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::text('fixed_labour_cost',$assignment->fixed_labour_cost,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_labour_cost') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('fixed_labour_cost') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            {{Form::text('fixed_price',$assignment->fixed_price,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price') ? ' is-invalid' : ''), 'placeholder'=>'0.00', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('fixed_price') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                @endif
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Add Planned Assignment Cost</th>
                </tr>
                <tr>
                    <th>Description</th>
                    <th>Payment Freq</th>
                    <th>Payment Method</th>
                    <th>Cost</th>
                </tr>
                @forelse($assignment_costs as $assignment_cost)
                    <tr>
                        <td>
                            <a href="{{ route('assignment.edit_ass_cost', $assignment_cost->id) }}">{{ $assignment_cost->description }}</a>
                        </td>
                        <td>
                            {{ $assignment_cost->payment_frequency }}
                        </td>
                        <td>
                            {{ isset($assignment_cost->payment_meth)?$assignment_cost->payment_meth->description:'' }}
                        </td>
                        <td>
                            {{ $assignment_cost->assignment->currency.' '.number_format($assignment_cost->cost,2,'.',',') }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">This assignment has no assignment cost.</td>
                    </tr>
                @endforelse
                @if($is_able_role)
                    <tr>
                        <td class="text-center" colspan="4"><a  class="btn btn-dark btn-sm" href="{{route('assignment.add_ass_cost', $assignment)}}">[Add Planned Assignment Cost]</a></td>
                    </tr>
                @endif






                {{--<tr>
                    <th>Country of Work:</th>
                    <td>
                        {{Form::select('country_id',$country_drop_down,$assignment->country_id,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('country_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>

                </tr>
                <tr>
                    <th>Function:</th>
                    <td>
                        {{Form::select('function', $function_drop_down, $assignment->function,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('function') ? ' is-invalid' : ''),'placeholder'=>'Function', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('function') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>System:</th>
                    <td>
                        {{Form::select('system', $system_drop_down,$assignment->system,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('system') ? ' is-invalid' : ''),'placeholder'=>'System', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('system') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>--}}



                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="5" class="btn-dark" style="text-align: center;">Actual Expense Summary</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Account</th>
                    <th>Element</th>
                    <th class="text-right">Expenses</th>
                    <th class="text-right">Exp Claim</th>
                    <th class="text-right">Exp Bill</th>
                </tr>
                @forelse($time_exp as $expense)
                    <tr>
                        <td>{{ isset($expense->account)?$expense->account->description:'' }}</td>
                        <td>{{ isset($expense->account_element)?$expense->account_element->description:'' }}</td>
                        <td class="text-right">{{ number_format($expense->amount,2,'.',',') }}</td>
                        <td class="text-right">{{ number_format($expense->claimable_expense ,2,'.',',') }}</td>
                        <td class="text-right">{{ number_format($expense->billable_expense ,2,'.',',') }}</td>
                    </tr>
                @empty
                @endforelse
                <tr>
                    <th colspan="2" class="text-right">TOTAL</th>
                    <th class="text-right">{{ number_format($total_expense_amount,2,'.',',') }}</th>
                    <th class="text-right">{{ number_format($total_expense_claim,2,'.',',') }}</th>
                    <th class="text-right">{{ number_format($total_expense_bill,2,'.',',') }}</th>
                </tr>
                </tbody>
            </table>
            @if($is_able_role)
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                    <tr>
                        <th colspan="11" class="btn-dark" style="text-align: center;">Actual Customer Invoice Detail (Account Receivable)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Customer Invoice Number</th>
                        <th>Invoice Date</th>
                        <th>Due Date</th>
                        <th>YearWk</th>
                        <th>Reference</th>
                        <th>Status</th>
                        <th class="text-right">Bill Hours</th>
                        <th class="text-right">Non Bill Hrs</th>
                        <th class="text-right">Inv Amount</th>
                        <th class="text-right">Exp Claim</th>
                        <th class="text-right">Exp Bill</th>
                    </tr>
                    @forelse($timesheets as $key => $timesheet)
                            <tr>
                                <td>{{ $timesheet->invoice_number }}</td>
                                <td>{{ $timesheet->invoice_date }}</td>
                                <td>{{ $timesheet->invoice_due_date }}</td>
                                <td>{{ $timesheet->year_week }}</td>
                                <td>{{ $timesheet->inv_ref }}</td>
                                <td>{{ isset($timesheet->bill_statusd->description)?$timesheet->bill_statusd->description:'' }}</td>
                                <td class="text-right">{{ number_format($timesheet->bill_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timesheet->non_bill_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timesheet->bill_hours * $assignment->invoice_rate ,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format(isset($time_exp[$key])?$time_exp[$key]->claimable_expense:0 ,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format(isset($time_exp[$key])?$time_exp[$key]->billable_expense:0 ,2,'.',',') }}</td>
                            </tr>
                    @empty
                    @endforelse
                    <tr>
                        <th colspan="6" class="text-uppercase">total</th>
                        <th class="text-right">{{number_format($total_billable_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_non_billable_hours,2,'.',',')}}</th>
                        <th class="text-right">{{ number_format(($total_billable_hours * $assignment->invoice_rate),2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_expense_claim,2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_expense_bill,2,'.',',') }}</th>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                    <tr>
                        <th colspan="11" class="btn-dark" style="text-align: center;">Actual Vendor Invoice Detail (Account Payable)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Vendor Invoice Number</th>
                        <th>Invoice Date</th>
                        <th>Due Date</th>
                        <th>YearWk</th>
                        <th>Reference</th>
                        <th>Status</th>
                        <th class="text-right">Bill Hours</th>
                        <th class="text-right">Non Bill Hrs</th>
                        <th class="text-right">Inv Amount</th>
                        <th class="text-right">Exp Claim</th>
                        <th class="text-right">Exp Bill</th>
                    </tr>
                    @forelse($timesheets as $key => $timesheet)
                        @if($assignment->resource->vendor)
                            <tr>
                                <td>{{ $timesheet->vendor_invoice_number }}</td>
                                <td>{{ $timesheet->vendor_invoice_date }}</td>
                                <td>{{ $timesheet->vendor_due_date }}</td>
                                <td>{{ $timesheet->year_week }}</td>
                                <td>{{ $timesheet->vendor_reference }}</td>
                                <td>{{ isset($timesheet->vendor_inv_status->description)?$timesheet->vendor_inv_status->description:'' }}</td>
                                <td class="text-right">{{ number_format($timesheet->bill_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timesheet->non_bill_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timesheet->bill_hours * $assignment->external_cost_rate ,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format(isset($time_exp[$key])?$time_exp[$key]->claimable_expense:0 ,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format(isset($time_exp[$key])?$time_exp[$key]->billable_expense:0 ,2,'.',',') }}</td>
                            </tr>
                        @endif
                    @empty
                    @endforelse
                    <tr>
                        <th colspan="6" class="text-uppercase">total</th>
                        <th class="text-right">{{ number_format($total_vendor_billable_hours,2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_vendor_non_billable_hours,2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_vendor_invoice_amount,2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_vendor_expense_claim,2,'.',',') }}</th>
                        <th class="text-right">{{ number_format($total_vendor_expense_bill,2,'.',',') }}</th>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-sm mt-3">
                    <thead>
                    <tr>
                        <th colspan="11" class="btn-dark" style="text-align: center;">Assignment Actual Profit and Loss Summary</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="text-right">Actual Expenses</th>
                        <th class="text-right">Actual Labour</th>
                        <th class="text-right">Actual Invoiced</th>
                        <th class="text-right">Profit/Loss Rand</th>
                        <th class="text-right">Profit%</th>

                    </tr>
                    @if($is_able_role)
                    @php
                        $profit_perc = (($total_billable_hours * ($assignment->external_cost_rate + $assignment->internal_cost_rate)) != 0 && (($total_billable_hours * $assignment->invoice_rate) != 0))? ($total_billable_hours * ($assignment->external_cost_rate + $assignment->internal_cost_rate))/($total_billable_hours * $assignment->invoice_rate):0;
                    @endphp
                    <tr>
                        <td class="text-right">{{ $assignment->currency.' '.number_format($total_expense_amount,2,'.',',') }}</td>
                        <td class="text-right">{{ $assignment->currency.' '.number_format(($total_billable_hours * ($assignment->external_cost_rate + $assignment->internal_cost_rate)),2,'.',',') }}</td>
                        <td class="text-right">{{ $assignment->currency.' '.number_format(($total_billable_hours * $assignment->invoice_rate),2,'.',',') }}</td>
                        <td class="text-right">{{ $assignment->currency.' '.number_format((($total_billable_hours * $assignment->invoice_rate) - ($total_billable_hours * ($assignment->external_cost_rate + $assignment->internal_cost_rate))),2,'.',',') }}</td>
                        <td class="text-right">{{number_format(($profit_perc * 100),0,'.',',')}}%</td>
                    </tr>
                        @endif
                    </tbody>
                </table>
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                        <tr>
                            <th colspan="4" class="btn-dark" style="text-align: center;">Assignment Referral</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Rate per Billable Hour:</th>
                        <td class="text-right">0.00</td>
                        <th>Rate per Non-Billable Hour:</th>
                        <td class="text-right">0.00</td>
                    </tr>
                    <tr>
                        <th>% of Invoice Rate:</th>
                        <td class="text-right">0.00</td>
                        <th>Assignment Fixed Cost:</th>
                        <td class="text-right">0.00</td>
                    </tr>
                    <tr>
                        <th>Due on Assignment Status:</th>
                        <td></td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Referred Vendor:</th>
                        <td></td>
                        <th>or Referred User:</th>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-center"><button class="btn-dark btn btn-sm">Process Once-off Commission</button></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                    <tr>
                        <th colspan="7" class="btn-dark" style="text-align: center;">Assignment Extentions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Extension Date:</th>
                        <th>Ext Hours</th>
                        <th>Ext Notes</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Ref</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($assignment_extensions as $assignment_extension)
                        <tr>
                            <td>{{$assignment_extension->extension_date}}</td>
                            <td>{{$assignment_extension->extension_hours}}</td>
                            <td>{{$assignment_extension->extension_notes}}</td>
                            <td>{{$assignment_extension->extension_start_date}}</td>
                            <td>{{$assignment_extension->extension_end_date}}</td>
                            <td>{{$assignment_extension->extension_ref}}</td>
                            <td>
                                <a href="{{route('assignment.editextension',$assignment_extension->id)}}" class="btn btn-success btn-sm">Edit</a>
                                {{--{{ Form::open(['method' => 'DELETE','route' => ['assignment.deleteextension', $assignment_extension->id],'style'=>'display:inline','class'=>'delete']) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}--}}
                            </td>
                        </tr>
                    @endforeach
                    {{--<tr id="add_expense_text_fields">
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''),'placeholder' => 'Extension Date'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'0:00'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Notes'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12 datepicker'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'End Date'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('extension_date',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('extension_date') ? ' is-invalid' : ''), 'placeholder'=>'Reference'])}}
                            @foreach($errors->get('extension_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    --}}
                    <tr>
                        <td colspan="7" class="text-center">
                            <a href="{{route('assignment.addextension', $assignment->id)}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Add Extension</a>
                        </td>
                    </tr>
                    </tbody>
                </table>



                {{--<br/>
                @if($r->isAn('admin') || $r->isAn('admin_manager') || $r->isAn('manager'))
                <table class="table table-bordered table-sm mt-3">
                    <tbody>
                    <tr>
                        <td style='width: 25%'><a class="btn btn-dark btn-sm" href="{{route('assignment.edit', $assignment)}}">[Edit Assignment]</a></td>
                        <td style='width: 25%'><a class="btn btn-dark btn-sm" href="{{route('project.edit', $project)}}">[Edit Project]</a></td>
                        <td style='width: 25%'><a class="btn btn-dark btn-sm" href="">[Print Assignment - Future]</a></td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                @endif


                <br/>
                @php
                    $teci = ($assignment->internal_cost_rate != 0) ? $total_hours * $assignment->internal_cost_rate : 0;
                    $tece = ($assignment->external_cost_rate != 0) ? $total_hours * $assignment->external_cost_rate : 0;
                    $tot_est_labor = ($assignment->internal_cost_rate + $assignment->external_cost_rate) * $assignment->hours;
                    $tot_act_profit = $total_invoice_amount - $tot_est_labor - $total_expense_amount;
                    $tot_act_profit_pc = ($total_invoice_amount != 0) ? (($tot_act_profit != 0 && $total_invoice_amount != 0) ? ($tot_act_profit / $total_invoice_amount) *100: 0)  : 0;
                @endphp
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                    <tr>
                        <th colspan="10" class="btn-dark" style="text-align: center;">Assignment Actual Profit and Loss Summary</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="text-center">Actual Expenses</th>
                        <th class="text-center">Estimated Labour</th>
                        <th class="text-center">Actual Invoiced</th>
                        <th class="text-center">Profit/Loss Rand</th>
                        <th class="text-center">Profit%</th>

                    </tr>
                    <tr>
                        <td class="text-center">{{ $assignment->currency.' '.number_format($total_expense_amount,2,'.',',') }}</td>
                        <td class="text-center">{{ $assignment->currency.' '.number_format($tot_est_labor,2,'.',',') }}</td>
                        <td class="text-center">{{ $assignment->currency.' '.number_format($total_invoice_amount,2,'.',',') }}</td>
                        <td class="text-center">{{ $assignment->currency.' '.number_format($tot_act_profit,2,'.',',') }}</td>
                        <td class="text-center">{{ $assignment->currency.' '.number_format($tot_act_profit_pc,2,'.',',') }}%</td>
                    </tr>
                    </tbody>
                </table>
                <br/>


                <br/>

                <br/>
            <table style="width: 100%; text-left: center;" border="0">
                <tbody>
                    <tr><td style="width: 20%;">Actual Expenses</td><td>as recorded on timesheets</td></tr>
                    <tr><td style="width: 20%;">Estimated Labour</td><td>timesheet hours x internal or external rate</td></tr>
                    <tr><td style="width: 20%;">Actual Invoiced</td><td>timesheet hours x invoice rate (Invoiced and current)</td></tr>
                    <tr><td style="width: 20%;">Profit</td><td>	Income - Expenses - Labor cost</td></tr>
                </tbody>
            </table>--}}
            @endif
        </div>
    </div>
    <!-- Resource Modal -->
    <div class="modal fade" id="resourceDigisignModal" tabindex="-1" role="dialog" aria-labelledby="resourceDigisignModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Digisign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => route('assignment.sendresourcedigisign', $assignment->id), 'method' => 'get']) !!}
                    <div class="form-group row">
                        <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                        <div class="col-sm-10">
                            <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                        </div>
                    </div>
                    <hr>
                    <div class="form-check">
                        <input type="checkbox" name="resource_email" value="{{$resource->email}}" checked class="form-check-input" id="resource_email">
                        <label class="form-check-label" for="resource_email">{{$resource->first_name.' '.$resource->last_name}} ({{$resource->email}})</label>
                    </div>
                    <hr>
                    <div class="form-group text-center">
                        {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="assignemntDigisignModal" tabindex="-1" role="dialog" aria-labelledby="assetDigisignModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Select Emails</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => route('assignment.sendsupplierdigisign', $assignment->id), 'method' => 'get']) !!}
                    <div class="form-group row">
                        <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                        <div class="col-sm-10">
                            <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                        </div>
                    </div>
                    <hr>
                    @if(isset($user_claim_approver))
                    <div class="form-check">
                        <input type="checkbox" name="user_claim_approver_email" value="{{$user_claim_approver->email}}" checked class="form-check-input" id="user_claim_approver_email">
                        <label class="form-check-label" for="user_claim_approver_email">{{$user_claim_approver->first_name.' '.$user_claim_approver->last_name}} ({{$user_claim_approver->email}})</label>
                    </div>
                    @endif
                    @if(isset($vendor_user))
                    <div class="form-check">
                        <input type="checkbox" name="user_claim_approver_email" value="{{$vendor_user->email}}" checked class="form-check-input" id="user_claim_approver_email">
                        <label class="form-check-label" for="user_claim_approver_email">{{$vendor_user->first_name.' '.$vendor_user->last_name}} ({{$vendor_user->email}})</label>
                    </div>
                    @endif
                    <hr>
                    <div class="form-group text-center">
                        {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="assignemntResourceDigisignModal" tabindex="-1" role="dialog" aria-labelledby="assignemntResourceDigisignLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Select Emails</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => route('assignment.sendemployeedigisign', $assignment->id), 'method' => 'get']) !!}
                    <div class="form-group row">
                        <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                        <div class="col-sm-10">
                            <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                        </div>
                    </div>
                    <hr>
                    @if(isset($user_claim_approver))
                        <div class="form-check">
                            <input type="checkbox" name="user_claim_approver_email" value="{{$user_claim_approver->email}}" checked class="form-check-input" id="user_claim_approver_email">
                            <label class="form-check-label" for="user_claim_approver_email">{{$user_claim_approver->first_name.' '.$user_claim_approver->last_name}} ({{$user_claim_approver->email}})</label>
                        </div>
                    @endif
                    {{--
                    @if(isset($vendor_user))
                        <div class="form-check">
                            <input type="checkbox" name="user_claim_approver_email" value="{{$vendor_user->email}}" checked class="form-check-input" id="user_claim_approver_email">
                            <label class="form-check-label" for="user_claim_approver_email">{{$vendor_user->first_name.' '.$vendor_user->last_name}} ({{$vendor_user->email}})</label>
                        </div>
                    @endif
                    --}}
                    <hr>
                    <div class="form-group text-center">
                        {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@section('extra-js')
    <script>
        function sendDocuments(assignment_id){
            var vendor_template_id = $('#vendor_template_id').val();
            var resource_template_id = $('#resource_template_id').val();
            /*if(resource_template_id == 0 || vendor_template_id == 0){
                alert("Please select Resource Document");
                return 0;
            }*/
            if(resource_template_id == 0){
                alert("Please select Resource Document");
                return 0;
            }
            $('#documents_feedback').html("<span style='color: blue'>Sending, Please Wait...</span>");
            axios.get('../../../assignment/senddocuments/'+assignment_id)
            .then(function (data) {
                console.log(data.data.message);
                console.log(data.data);
                $('#documents_feedback').html("<span style='color: green'>Docucments sent successfully.</span>");
            })
            .catch(function () {
                console.log("An Error occured!!!");
                $('#documents_feedback').html("<span style='color: red'>An Error occured!</span>");
            });
        }

        function submitTemplateVendor(template_type, assignment_id, name_type){
            var template_id = '{{$assignment->vendor_template_id}}';
            if(template_id == 0){
                alert('Template not selected');
                // $('#documents_feedback').html("<span style='color: red'>No vendor template selected for this assignment!</span>");
                return;
            }
            //window.open("../../../template/"+template_type+"/previewtemplate/"+template_id+"/"+assignment_id+"/"+name_type, "_blank");
            window.open("../../../template/gettemplate/"+template_id+"?id="+assignment_id+"&print=1", "_blank");
        }

        function submitTemplateResource(template_type, assignment_id, name_type){
            var template_id = $('#resource_template_id').val();
            if(template_id == 0){
                alert('Template not selected');
                // $('#documents_feedback').html("<span style='color: red'>No resource template selected for this assignment!</span>");
                return;
            }
            //window.open("../../../template/"+template_type+"/previewtemplate/"+template_id+"/"+assignment_id+"/"+name_type, "_blank");
            window.open("../../../template/gettemplate/"+template_id+"?id="+assignment_id+"&print=1", "_blank");
        }

        function goBack() {
            window.history.back();
        }

        function noVendorTemplate(){
            alert('Vendor template not selected');
        }

        function noResourceTemplate(){
            alert('Resource template not selected');
        }
    </script>
@endsection
