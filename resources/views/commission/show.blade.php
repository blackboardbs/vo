@extends('adminlte.default')

@section('title') View Commission @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('commission.edit', $comm[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Commission</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($comm as $result)
                <table class="table table-bordered table-sm">
                    <tr>

                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Minimum Rate</th>
                        <td>{{$result->min_rate}}</td>
                    </tr>
                    <tr>
                        <th>Hour 01</th>
                        <td>{{$result->hour01}}</td>
                        <th>Comm 01</th>
                        <td>{{$result->comm01}}</td>
                    </tr>
                    <tr>
                        <th>Hour 02</th>
                        <td>{{$result->hour02}}</td>
                        <th>Comm 02</th>
                        <td>{{$result->comm02}}</td>
                    </tr>
                    <tr>
                        <th>Hour 03</th>
                        <td>{{$result->hour03}}</td>
                        <th>Comm 03</th>
                        <td>{{$result->comm03}}</td>
                    </tr>
                    <tr>
                        <th>Hour 04</th>
                        <td>{{$result->hour04}}</td>
                        <th>Comm 04</th>
                        <td>{{$result->comm04}}</td>
                    </tr>
                    <tr>
                        <th>Hour 05</th>
                        <td>{{$result->hour05}}</td>
                        <th>Comm 05</th>
                        <td>{{$result->comm05}}</td>
                    </tr>
                    <tr>
                        <th>Hour 06</th>
                        <td>{{$result->hour06}}</td>
                        <th>Comm 06</th>
                        <td>{{$result->comm06}}</td>
                    </tr>
                    <tr>
                        <th>Hour 07</th>
                        <td>{{$result->hour07}}</td>
                        <th>Comm 07</th>
                        <td>{{$result->comm07}}</td>
                    </tr>
                    <tr>
                        <th>Hour 08</th>
                        <td>{{$result->hour08}}</td>
                        <th>Comm 08</th>
                        <td>{{$result->comm08}}</td>
                    </tr>
                    <tr>
                        <th>Hour 09</th>
                        <td>{{$result->hour09}}</td>
                        <th>Comm 09</th>
                        <td>{{$result->comm09}}</td>
                    </tr>
                    <tr>
                        <th>Hour 10</th>
                        <td>{{$result->hour10}}</td>
                        <th>Comm 10</th>
                        <td>{{$result->comm10}}</td>
                    </tr>
                    <tr>
                        <th>Hour 11</th>
                        <td>{{$result->hour11}}</td>
                        <th>Comm 11</th>
                        <td>{{$result->comm11}}</td>
                    </tr>
                    <tr>
                        <th>Hour 12</th>
                        <td>{{$result->hour12}}</td>
                        <th>Comm 12</th>
                        <td>{{$result->comm12}}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection
