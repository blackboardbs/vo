@extends('adminlte.default')

@section('title') Edit Commission @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($comm as $result)
            {{Form::open(['url' => route('commission.update',$result), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>

                    <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('description',$result->description,['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Minimum Rate</th>
                    <td>{{Form::text('min_rate',$result->min_rate,['class'=>'form-control form-control-sm'. ($errors->has('min_rate') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('min_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 01 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('hour01',$result->hour01,['class'=>'form-control form-control-sm'. ($errors->has('hour01') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour01') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 01 <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('comm01',$result->comm01,['class'=>'form-control form-control-sm'. ($errors->has('comm01') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm01') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 02</th>
                    <td>{{Form::text('hour02',$result->hour02,['class'=>'form-control form-control-sm'. ($errors->has('hour02') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour02') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 02</th>
                    <td>{{Form::text('comm02',$result->comm02,['class'=>'form-control form-control-sm'. ($errors->has('comm02') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm02') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 03</th>
                    <td>{{Form::text('hour03',$result->hour03,['class'=>'form-control form-control-sm'. ($errors->has('hour03') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour03') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 03</th>
                    <td>{{Form::text('comm03',$result->comm03,['class'=>'form-control form-control-sm'. ($errors->has('comm03') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm03') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 04</th>
                    <td>{{Form::text('hour04',$result->hour04,['class'=>'form-control form-control-sm'. ($errors->has('hour04') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour04') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 04</th>
                    <td>{{Form::text('comm04',$result->comm04,['class'=>'form-control form-control-sm'. ($errors->has('comm04') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm04') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 05</th>
                    <td>{{Form::text('hour05',$result->hour05,['class'=>'form-control form-control-sm'. ($errors->has('hour05') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour05') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 05</th>
                    <td>{{Form::text('comm05',$result->comm05,['class'=>'form-control form-control-sm'. ($errors->has('comm05') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm05') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 06</th>
                    <td>{{Form::text('hour06',$result->hour06,['class'=>'form-control form-control-sm'. ($errors->has('hour06') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour06') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 06</th>
                    <td>{{Form::text('comm06',$result->comm06,['class'=>'form-control form-control-sm'. ($errors->has('comm06') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm06') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 07</th>
                    <td>{{Form::text('hour07',$result->hour07,['class'=>'form-control form-control-sm'. ($errors->has('hour07') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour07') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 07</th>
                    <td>{{Form::text('comm07',$result->comm07,['class'=>'form-control form-control-sm'. ($errors->has('comm07') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm07') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 08</th>
                    <td>{{Form::text('hour08',$result->hour08,['class'=>'form-control form-control-sm'. ($errors->has('hour08') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour08') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 08</th>
                    <td>{{Form::text('comm08',$result->comm08,['class'=>'form-control form-control-sm'. ($errors->has('comm08') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm08') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 09</th>
                    <td>{{Form::text('hour09',$result->hour09,['class'=>'form-control form-control-sm'. ($errors->has('hour09') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour09') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 09</th>
                    <td>{{Form::text('comm09',$result->comm09,['class'=>'form-control form-control-sm'. ($errors->has('comm09') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm09') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 10</th>
                    <td>{{Form::text('hour10',$result->hour10,['class'=>'form-control form-control-sm'. ($errors->has('hour10') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour10') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 10</th>
                    <td>{{Form::text('comm10',$result->comm10,['class'=>'form-control form-control-sm'. ($errors->has('comm10') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm10') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 11</th>
                    <td>{{Form::text('hour11',$result->hour11,['class'=>'form-control form-control-sm'. ($errors->has('hour11') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour11') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 11</th>
                    <td>{{Form::text('comm11',$result->comm11,['class'=>'form-control form-control-sm'. ($errors->has('comm11') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm11') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hour 12</th>
                    <td>{{Form::text('hour12',$result->hour12,['class'=>'form-control form-control-sm'. ($errors->has('hour12') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('hour12') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Comm 12</th>
                    <td>{{Form::text('comm12',$result->comm12,['class'=>'form-control form-control-sm'. ($errors->has('comm12') ? ' is-invalid' : ''),'placeholder'=>''])}}
                        @foreach($errors->get('comm12') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$sidebar_process_statuses,$result->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : ''),'id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            {{Form::close()}}
                @endforeach
        </div>
    </div>
@endsection