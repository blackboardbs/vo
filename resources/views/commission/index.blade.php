@extends('adminlte.default')

@section('title') Commission @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('commission.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Commission</a>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('commission.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('description', 'Description')</th>
                    <th>- 01 -</th>
                    <th>- 02 -</th>
                    <th>- 03 -</th>
                    <th>- 04 -</th>
                    <th>- 05 -</th>
                    <th>- 06 -</th>
                    <th>- 07 -</th>
                    <th>- 08 -</th>
                    <th>- 09 -</th>
                    <th>- 10 -</th>
                    <th>- 11 -</th>
                    <th>- 12 -</th>
                    <th>@sortablelink('min_rate', 'Rate')</th>
                    <th>@sortablelink('statusd.description', 'Status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($comm as $referrer)
                    <tr>
                        <td><a href="{{route('commission.show',$referrer->id)}}">{{$referrer->description}}</a></td>
                        <td>{{$referrer->hour01}}/{{$referrer->comm01}}</td>
                        <td>{{$referrer->hour02}}/{{$referrer->comm02}}</td>
                        <td>{{$referrer->hour03}}/{{$referrer->comm03}}</td>
                        <td>{{$referrer->hour04}}/{{$referrer->comm04}}</td>
                        <td>{{$referrer->hour05}}/{{$referrer->comm05}}</td>
                        <td>{{$referrer->hour06}}/{{$referrer->comm06}}</td>
                        <td>{{$referrer->hour07}}/{{$referrer->comm07}}</td>
                        <td>{{$referrer->hour08}}/{{$referrer->comm08}}</td>
                        <td>{{$referrer->hour09}}/{{$referrer->comm09}}</td>
                        <td>{{$referrer->hour10}}/{{$referrer->comm10}}</td>
                        <td>{{$referrer->hour11}}/{{$referrer->comm11}}</td>
                        <td>{{$referrer->hour12}}/{{$referrer->comm12}}</td>
                        <td>{{$referrer->min_rate}}</td>
                        <td>{{$referrer->statusd->description}}</td>

                        <td>
                            <div class="d-flex">
                            <a href="{{route('commission.edit',$referrer)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>

                            {{ Form::open(['method' => 'DELETE','route' => ['commission.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No leave match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $comm->firstItem() }} - {{ $comm->lastItem() }} of {{ $comm->total() }}
                        </td>
                        <td>
                            {{ $comm->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $comm->appends(request()->except('page'))->links() }} --}}
        </div>
    </div>
@endsection
