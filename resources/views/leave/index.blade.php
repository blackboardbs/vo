@extends('adminlte.default')

@section('title') Leave @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('leave.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Leave</a>
            <x-export route="leave.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('leave_type',$leave_type_drop_down,request()->leave_type,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Leave Type</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('from',request()->from,['class'=>'datepicker form-control form-control-sm w-100 search', 'id' => 'from'])}}
                        <span>From</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('to',request()->to,['class'=>'datepicker form-control form-control-sm w-100 search', 'id' => 'to'])}}
                        <span>To</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100 search" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('leave.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('resource.first_name', 'Resource')</th>
                    <th>@sortablelink('leave_type.description', 'Leave Type')</th>
                    <th>@sortablelink('no_of_days', 'Days')</th>
                    <th>@sortablelink('date_from', 'Start')</th>
                    <th>@sortablelink('date_to', 'End')</th>
                    <th>@sortablelink('half_day', 'Half-day')</th>
                    <th class="text-center last px-3">Active Projects</th>
                    <th class="last px-3">@sortablelink('approve_date', 'Approval')</th>
                    <th class="last px-3">@sortablelink('status', 'Status')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($leave as $_leave)
                    <tr>
                        <td><a href="{{route('leave.show',$_leave->id)}}">{{$_leave->resource?->name()}}</a></td>
                        <td>{{$_leave->leave_type?->description}}</td>
                        <td class="text-center">{{$_leave->no_of_days}}</td>
                        <td>{{$_leave->date_from}}</td>
                        <td>{{$_leave->date_to}}</td>
                        <td>{{$_leave->half_day}}</td>
                        <td class="text-center">
                            @if(count($projects[$_leave->id]) > 0)
                                <button class="btn btn-sm {{$_leave->leave_status == '1' ? 'btn-info' : 'btn-warning'}} toolt" data-toggle="modal" data-target="#leave_{{$_leave->id}}"><i class="fas fa-running"></i><span class="tooltiptext">View Active Projects</button>
                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-lg" id="leave_{{$_leave->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Active projects</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <ul class="list-group text-left">
                                                    @foreach($projects[$_leave->id] as $proj)
                                                        <li class="list-group-item">{{$proj->project->name}} <span class="float-right">{{'From: '.$proj->start_date.' - '.$proj->end_date}}</span></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($_leave->leave_status == '1')
                                Approved
                            @elseif($_leave->leave_status == '2')
                                Declined
                            @else
                                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']) || $_leave->resource?->resource?->manager_id == auth()->user()->id)
                                    <span id="leave-status">
                                        <a href="{{route('leave.approve', $_leave)}}" class="btn btn-sm btn-success toolt" id="approve"><i class="fas fa-thumbs-up"></i><span class="tooltiptext">Approve</span></a>
                                        <a href="{{route('leave.decline', $_leave)}}" class="btn btn-sm btn-danger toolt ml-1" id="decline"><i class="fas fa-thumbs-down"></i><span class="tooltiptext">Decline</span></a>
                                    </span>
                                @endif
                            @endif
                        </td>
                        <td class="text-center">{{$_leave->statusd?->description}}</td>
                        @if($can_update)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('leave.edit',$_leave)}}" {{ ($_leave->leave_status == '1' || $_leave->leave_status == '2') ? 'disabled' : ''}} class="btn btn-success btn-sm mr-1 toolt{{ ($_leave->leave_status == '1' || $_leave->leave_status == '2') ? ' disabled' : ''}}"><i class="fas fa-pencil-alt"></i><span class="tooltiptext">Edit</span></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['leave.destroy', $_leave->id],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm toolt"><i class="fas fa-trash"></i><span class="tooltiptext">Delete</span></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No leave match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $leave->firstItem() }} - {{ $leave->lastItem() }} of {{ $leave->total() }}
                        </td>
                        <td>
                            {{ $leave->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $leave->appends(request()->except('page'))->links() }} --}}
        </div>
    </div>
@endsection