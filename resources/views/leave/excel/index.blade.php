<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Leave Type</th>
        <th>Days</th>
        <th>Start</th>
        <th>End</th>
        <th>Half-day</th>
        <th>Approval</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($leave as $_leave)
        <tr>
            <td>{{$_leave->resource?->name()}}</td>
            <td>{{$_leave->leave_type?->description}}</td>
            <td>{{$_leave->no_of_days}}</td>
            <td>{{$_leave->date_from}}</td>
            <td>{{$_leave->date_to}}</td>
            <td>{{$_leave->half_day}}</td>
            <td>
                @if($_leave->leave_status == '1')
                    Approved
                @elseif($_leave->leave_status == '2')
                    Declined
                @else
                    New
                @endif
            </td>
            <td>{{$_leave->statusd?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>