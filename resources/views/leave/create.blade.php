@extends('adminlte.default')

@section('title') Add Leave @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="leaveWarning()" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div style="min-height:45px;width:100%;height:100%;position: relative;" id="clock">
        <div class="clock" style="left:55%;margin-top:-30px;"></div>
        </div> 
        <div class="table-responsive" id="leave_content" style="display:none;">
        <input type="hidden" id="annual_leave" />        
        <input type="hidden" id="sick_leave" />
            {{Form::open(['url' => route('leave.store'), 'method' => 'post','class'=>'mt-3','autocomplete' => 'off','id'=>'leave'])}}
            <table class="table table-bordered table-sm">
                <tr>

                    <th>Resource <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td><select name="resource" id="resource" class="resource form-control form-control-sm  '. {{($errors->has('resource') ? ' is-invalid' : '')}}" onchange="getAllAnnualLeave()">
                            @if(auth()->check())
                                <option value="0">Resource</option>
                                @foreach($resource as $result)
                                    <option value="{{$result->id}}" {{(old('resource') == $result->id ? 'selected' : ($result->id == auth()->id() ? 'selected' : ''))}}>{{$result->full_name}}</option>
                                @endforeach
                            @else
                                <option value="0">Resource</option>
                            @foreach($resource as $result)
                                <option value="{{$result->id}}">{{$result->full_name}}</option>
                            @endforeach
                            @endif
                        </select>


                        @foreach($errors->get('resource') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Details</th>
                    <td>{{Form::text('contact_details',(old('contact_details') ? old('contact_details') : auth()->user()->phone),['class'=>'form-control form-control-sm'. ($errors->has('contact_details') ? ' is-invalid' : ''),'placeholder'=>'Contact Details','id'=>'contact'])}}
                        @foreach($errors->get('contact_details') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Start Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('date_from',old('date_from') ?? \Carbon\Carbon::now()->format('Y-m-d'),['id' => 'date_from', 'class'=>'date_from form-control form-control-sm'. ($errors->has('date_from') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('date_from') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>End Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('date_to',old('date_to') ?? \Carbon\Carbon::now()->format('Y-m-d'),['id' => 'date_to', 'class'=>'date_to form-control form-control-sm'. ($errors->has('date_to') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('date_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Leave Days <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <div class="row">
                            <div class="col-md-2">
                        {{Form::text('no_of_days',old('no_of_days'),['id' => 'no_of_days', 'class'=>'no_of_days form-control form-control-sm'. ($errors->has('no_of_days') ? ' is-invalid' : ''), 'readonly' => 'readonly'])}}
                        @foreach($errors->get('no_of_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                            </div>
                            <div class="col-md-3 mt-1">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="half-day" name="half-day" {{old('half-day') ? 'checked' : ''}} onchange="halfDay()">
                                    <label class="form-check-label" for="half-day">Include Half-day</label>
                                 </div>
                                <input type="hidden" name="halfdaydate" id="halfdaydate" value="{{old('halfdaydate')}}" />
                            </div>
                        </div>
                    </td>
                    <th>Leave Type <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('leave_type',$leave_type,1,['id'=>'leave_type','class'=>'form-control form-control-sm '. ($errors->has('leave_type') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('leave_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

            </table>
            {{Form::close()}}
        <div class="getassignment">

        </div>
        <div class="leave_balance alert alert-info" style="display:none">
            <strong class="pb-2">Leave Balance</strong><br />
            Annual Leave: <span id="leave_balance">0</span>
        </div>
        </div>
        <button type="button" class="btn btn-primary" id="showHalfDayModal" style="opacity:0" data-toggle="modal" data-target="#halfDayModal"></button>
        <!-- Modal -->
        <div class="modal fade" id="halfDayModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width:250px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Select Half-day</h5>
                <button type="button" class="close" onclick="closeHalfDay()">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    {!! Form::select('halfdaydates', [], null, ['class' => 'form-control', 'id' => 'halfdaydates']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeHalfDayModal" class="btn btn-secondary" style="opacity:0" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-secondary" onclick="closeHalfDay()">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="saveHalfDay()">Save</button>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            getassignments();
            // getAllAnnualLeave();
            if($('#date_from').val != '' && $('#date_to').val() != ''){
                getLeaveDays();
            }

            $('#date_from').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(d,i){
                    if(d !== i.lastVal){
                        $(this).change();
                    }
                }
            });
            $('#date_to').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(d,i){
                    if(d !== i.lastVal){
                        $(this).change();
                    }
                }
            });

            $('.resource').on('change',function(){
                getassignments();
            });

            $('.date_from').on('change',function(){
                if ($('.date_to').val().length > 0){
                    getassignments();
                }
            });

            $('.date_to').on('change',function(){
                if ($('.date_from').val().length > 0){
                    getassignments();
                }
            });

        });

        function halfDay(){
            if($('#half-day').is(':checked')){
                document.getElementById('showHalfDayModal').click();
            } else {
                getLeaveDays();
                $('#halfdaydate').val('');
            }
        }

        function closeHalfDay(){
            document.getElementById('closeHalfDayModal').click();
            getLeaveDays();
            $('#halfdaydate').val('');
            $('#half-day').attr('checked',false);
        }
        function saveHalfDay(){
            let days = $('#no_of_days').val();
            $('#no_of_days').val(days - 0.5);
            $('#halfdaydate').val($("#halfdaydates").val());
            document.getElementById('closeHalfDayModal').click();
        }

        function getLeaveDays(){
            var startDay = new Date($('#date_from').val());
            var start = startDay.getFullYear() + '-' + ('0' + (startDay.getMonth() + 1)).slice(-2) + '-' + ('0' + startDay.getDate()).slice(-2);
            var endDay = new Date($('#date_to').val());
            var end = endDay.getFullYear() + '-' + ('0' + (endDay.getMonth() + 1)).slice(-2) + '-' + ('0' + endDay.getDate()).slice(-2)

            axios.get('../../../leave/getleavedays/'+start+'/'+end)
                .then(function (data) {
                    //console.log(data.data.days)
                    $('#no_of_days').val(data.data.days);
                    $('#submit').attr('disabled', false);
                    $('#submit').html('Save');
                    let options = "<option value=''>Select Day</option>";
                data.data.dates.forEach((v, i) => options += `<option ${$('#halfdaydate').val() == v.date || start == v.date ? 'selected':null} value="${v.date}">${v.date}</option>`);
                $("#halfdaydates").html(options);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }

        function leaveWarning(){

            if($('#leave_type').val() == 1 && ($('#annual_leave').val() - $('#no_of_days').val()) < 0){
                            document.getElementById('showLeaveDialog').click();
                            $('#leaveExceeded').html("The leave request will cause you to exceed your Annual Leave allocation.")
                        } else if($('#leave_type').val() == 2 && ($('#sick_leave').val() - $('#no_of_days').val()) < 0){
                            document.getElementById('showLeaveDialog').click();
                            $('#leaveExceeded').html("The leave request will cause you to exceed your Sick Leave allocation.")
                        } else {
                            document.getElementById('hideLeaveDialog').click();
                            saveForm('leave')
                        }
        }

        function getAllAnnualLeave() {
            if($('.leave_balance').is(':visible')){
                $('#leave_balance').html('-')
            }
            let resource = $('#resource').val();
                    // $('.leave_balance').hide();

            axios.post('/leave/getallannualleave',{'resource':resource,'date':$('#date_to').val()})
                .then(function (data) {
                        $('#annual_leave').val(data.data.leave-data.data.total_leave_applied_for)
                        $('#sick_leave').val(data.data.sick_leave)
                        if(data.data.found){
                            $("#leave_balance").html((data.data.leave-data.data.total_leave_applied_for).toFixed(2));
                        }
                        $('.leave_balance').show();

                        $("#clock").hide();
                        $("#leave_content").show();
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }

        function getassignments(){
            $(".getassignment").html('');
            $(".getassignment").hide();


            var startDay = new Date($('.date_from').val());
            var start = startDay.getFullYear() + '-' + ('0' + (startDay.getMonth() + 1)).slice(-2) + '-' + ('0' + startDay.getDate()).slice(-2);
            var endDay = new Date($('.date_to').val());
            var end = endDay.getFullYear() + '-' + ('0' + (endDay.getMonth() + 1)).slice(-2) + '-' + ('0' + endDay.getDate()).slice(-2);

            axios.get('../../../leave/getleavedays/'+start+'/'+end)
            .then(function (data) {
                $('#no_of_days').val(data.data.days);
                let options = "<option value=''>Select Day</option>";
                data.data.dates.forEach((v, i) => options += `<option ${start == v.date ? 'selected':null} value="${v.date}">${v.date}</option>`);
                $("#halfdaydates").html(options);
                // $("#halfdaydates").val(start);
            })
            .catch(function (err) {
                console.log(err.response)
                console.log("An Error occured!!!");
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if($('.resource').val() > 0 && $('.date_from').val() != '' && $('.date_to').val() != '') {

                var date_from = new Date($('.date_from').val());
                var date_to = new Date($('.date_to').val());

                if(date_to < date_from){
                    alert('Date to can not be greater or equal to date from');
                    $('.date_to').val('');
                } else {
                    $.ajax({
                        url: '/getassignment',
                        type: "POST",
                        dataType: "json",
                        data: {date_from: start, date_to: end, resource: $('.resource').val()},
                        success: function (data) {
                            $('#contact').val(data.contact.phone);
                            if (data.count > 0) {
                                $(".getassignment").removeClass('alert-info').addClass('alert').addClass('alert-danger');
                                var str = '';

                                str += '<strong>Projects assigned to during leave period.</strong><br /><ul>';
                                $.each(data.projects, function (index, val) {
                                    str += '<li>' + val + '</li>';
                                });
                                str += '</ul>';

                                $(".getassignment").html(str);
                                $(".getassignment").show();
                                getAllAnnualLeave();
                                $("#clock").hide();
                                $("#leave_content").show();
                            } else {
                                $(".getassignment").removeClass('alert-danger').addClass('alert').addClass('alert-info');
                                $(".getassignment").html("Resource is not assigned to any projects during requested leave period");
                                $(".getassignment").show();
                                
                                getAllAnnualLeave();
                                $("#clock").hide();
                                $("#leave_content").show();
                            }
                        }
                    });
                }
            } else {
                                $("#clock").hide();
                                $("#leave_content").show();
                            }
        }

    </script>
@endsection
