@extends('adminlte.default')

@section('title') View Leave @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('leave.edit', $leave[0])}}" class="btn btn-success float-right" style="margin-left: 5px;">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($leave as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Resource</th>
                        <td>{{$result->resource->first_name}} {{$result->resource->last_name}}{{Form::hidden('resource',$result->resource->id,['class'=>'resource form-control form-control-sm'. ($errors->has('resource') ? ' is-invalid' : '')])}}</td>
                        <th>Leave Days</th>
                        <td>{{$result->no_of_days}}</td>
                    </tr>
                    <tr>
                        <th>Start Date</th>
                        <td>{{$result->date_from}}{{Form::hidden('date_from',$result->date_from,['class'=>'date_from datepicker form-control form-control-sm'. ($errors->has('date_from') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}</td>
                        <th>End Date</th>
                        <td>{{$result->date_to}}{{Form::hidden('date_to',$result->date_to,['class'=>'date_to datepicker form-control form-control-sm'. ($errors->has('date_to') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}</td>
                    </tr>
                    <tr>
                        <th>Contact Details</th>
                        <td>{{$result->contact_details}}</td>
                        <th>Leave Type</th>
                        <td>{{$result->leave_type->description}}</td>
                    </tr>
                    <tr>
                        <th>Leave Status</th>
                        <td>@if($result->leave_status == '1')
                                Approved
                            @elseif($result->leave_status == '2')
                                Declined
                            @else
                            @endif</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
        <div class="getassignment">

        </div>
    </div>
@endsection
@section('extra-js')
    <script>

        function getassignments(){
            $(".getassignment").html('');


            var startDay = new Date($('.date_from').val());
            var start = startDay.getFullYear() + '-' + ('0' + (startDay.getMonth() + 1)).slice(-2) + '-' + ('0' + startDay.getDate()).slice(-2);
            var endDay = new Date($('.date_to').val());
            var end = endDay.getFullYear() + '-' + ('0' + (endDay.getMonth() + 1)).slice(-2) + '-' + ('0' + endDay.getDate()).slice(-2);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;

            // Round down.
            $('.no_of_days').val( Math.floor(Math.abs(days)));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/getassignment',
                type:"POST",
                dataType:"json",
                data:{date_from:start,date_to:end,resource:$('.resource').val()},
                success:function(data){
                    if(data.count > 0){
                        $(".getassignment").addClass('alert').addClass('alert-danger');
                        var str = '';

                        str += '<strong>Ongoing projects during leave period.</strong><br /><ul>';
                        $.each(data.projects,function(index,val){
                            str += '<li>'+val+'</li>';
                        });
                        str += '</ul>';

                        $(".getassignment").html(str);
                    }
                }
            });
        };

        $(function(){
            getassignments();
        })

        function goBack() {
            window.history.back();
        }

    </script>
@endsection
