@extends('adminlte.default')

@section('title')  {{isset($_GET['application']) ? '' : 'Edit'}} Leave @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            @if(!isset($_GET['application']))
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('leave')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
            @else
            <button onclick="approveLeave({{$result->id}})" {{$result->leave_status == '1' ? 'disabled' : ''}} class="btn btn-success float-right" style="margin-top: -7px;">Approve</button>
            <button onclick="declineLeave({{$result->id}})" {{$result->leave_status == '2' ? 'disabled' : ''}} class="btn btn-danger float-right ml-1" style="margin-top: -7px;">Decline</button>
        
            <a href="/leave" class="btn btn-dark float-right ml-1" style="margin-top: -7px;">Exit</a>
            @endif
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div style="min-height:45px;width:100%;height:100%;position: relative;" id="clock">
        <div class="clock" style="left:55%;margin-top:-30px;"></div>
        </div> 
        <div class="table-responsive" id="leave_content" style="display:none;">
        <input type="hidden" id="annual_leave" value="{{ $annual_leave }}" />        
        <input type="hidden" id="sick_leave" value="{{ $sick_leave["total_leave"] }}" />
        <input type="hidden" id="original_leave_days" value="{{ $result->no_of_days }}" />
        <input type="hidden" id="original_sick_days" value="{{$sick_leave["cycle_days"] - $sick_leave["total_leave"]}}" />
            {{Form::open(['url' => route('leave.update',$result), 'method' => 'post','class'=>'mt-3','autocomplete' => 'off','id'=>'leave'])}}
            <table class="table table-bordered table-sm">
                <tr>

                    <th>Resource <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('resource',$resource,$result->emp_id,['class'=>'resource form-control form-control-sm '. ($errors->has('resource') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('resource') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Details</th>
                    <td>
                        <input type="text" name="contact_details" class="form-control form-control-sm {{($errors->has('contact_details') ? ' is-invalid' : '')}}" placeholder="Contact Details" value="{{$result->contact_details}}" {{isset($_GET['application']) || ($result->leave_status == '1' || $result->leave_status == '2') ? 'disabled' : ''}} />
                        <!-- {{Form::text('contact_details',$result->contact_details,['class'=>'form-control form-control-sm'. ($errors->has('contact_details') ? ' is-invalid' : ''),'placeholder'=>'Contact Details'])}} -->
                        @foreach($errors->get('contact_details') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>

                </tr>
                <tr>
                    <th>Start Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <input type="text" name="date_from" class="date_to form-control form-control-sm{{$errors->has('date_from') ? ' is-invalid' : ''}}" placeholder="Start Date" value="{{$result->date_from}}" {{isset($_GET['application']) || ($result->leave_status == '1' || $result->leave_status == '2') ? 'disabled' : ''}} id="date_from"/>
                        @foreach($errors->get('date_from') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <input type="text" name="date_to" class="date_to form-control form-control-sm{{$errors->has('date_to') ? ' is-invalid' : ''}}" placeholder="End Date" value="{{$result->date_to}}" {{isset($_GET['application']) || ($result->leave_status == '1' || $result->leave_status == '2') ? 'disabled' : ''}} id="date_to"/>
                        @foreach($errors->get('date_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Leave Days <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        
                        <div class="row">
                            <div class="col-md-2">
                        {{Form::text('no_of_days',$result->no_of_days,['id' => 'no_of_days', 'class'=>'no_of_days form-control form-control-sm'. ($errors->has('no_of_days') ? ' is-invalid' : ''), 'readonly' => 'readonly'])}}
                        @foreach($errors->get('no_of_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                            </div>
                            <div class="col-md-3 mt-1">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="half-day" name="half-day" {{old('half-day') || $result->half_day ? 'checked' : ''}} onchange="halfDay()" {{isset($_GET['application']) || ($result->leave_status == '1' || $result->leave_status == '2') ? 'disabled' : ''}}>
                                    <label class="form-check-label" for="half-day">Include Half-day</label>
                                 </div>
                                <input type="hidden" name="halfdaydate" id="halfdaydate" value="{{old('halfdaydate')}}" />
                            </div>
                        </div></td>
                    <th>Leave Type <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <select name="leave_type" id="leave_type" class="form-control form-control-sm {{$errors->has('leave_type') ? ' is-invalid' : ''}}" {{isset($_GET['application']) || ($result->leave_status == '1' || $result->leave_status == '2') ? 'disabled' : ''}}>
                            @foreach($leave_type as $key => $value)
                                <option value="{{$key}}" {{$key == $result->leave_type_id ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                        @foreach($errors->get('leave_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                @if(auth()->check())
                    @if(auth()->user()->isUser())
                    @else
                        <tr>
                            <th>Leave Status</th>
                            <td id="leave_status">@if($result->leave_status == '1')
                                Approved
                            @elseif($result->leave_status == '2')
                                Declined
                            @else
                            @endif
                            @foreach($errors->get('leave_status') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th></th>
                            <td>
                            </td>
                        </tr>
                    @endif
                @endif
            </table>
            {{Form::close()}}
        </div>
        <button type="button" class="btn btn-primary" id="showHalfDayModal" style="opacity:0" data-toggle="modal" data-target="#halfDayModal"></button>
        <!-- Modal -->
        <div class="modal fade" id="halfDayModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width:250px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Select Half-day</h5> 
                <button type="button" class="close" onclick="closeHalfDay()">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    {!! Form::select('halfdaydates', [], null, ['class' => 'form-control', 'id' => 'halfdaydates']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeHalfDayModal" class="btn btn-secondary" style="opacity:0" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-secondary" onclick="closeHalfDay()">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="saveHalfDay()">Save</button>
                </div>
            </div>
            </div>
        </div>
        <div class="getassignment">
        </div>
        <div class="leave_balance alert alert-info" style="display:none">
        Annual leave balance{{$result->leave_status != '1' && $result->leave_status != '2' ? ' (including requested days)' : ''}}: <span id="total_annual_leave">{{$annual_leave}}</span><br />
        Sick leave balance for {{$sick_leave["sick_cycle_start"]}} - {{$sick_leave["sick_cycle_end"]}}: <span id="total_sick_leave">{{$sick_leave["cycle_days"] - $sick_leave["total_leave"]}}</span> of {{$sick_leave["cycle_days"]}} days
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            getassignments();
            if($('#date_from').val != '' && $('#date_to').val() != ''){
                getLeaveDays();
            }

            $('#date_from').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(d,i){
                    if(d !== i.lastVal){
                        $(this).change();
                    }
                }
            });
            $('#date_to').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(d,i){
                    if(d !== i.lastVal){
                        $(this).change();
                    }
                }
            });

$('#leave_type').on('change',function(){
    if($('#leave_type').val() != 1){
        $('#total_annual_leave').html($('#original_leave_days').val());
                    getLeaveDays();
    }
    if($('#leave_type').val() != 2){
        $('#total_sick_leave').html($('#original_sick_days').val());
                    getLeaveDays();
    }
});



$('#date_from').on('change',function(){
                if ($('#date_to').val().length > 0){
                    getLeaveDays();
                }
            });

            $('#date_to').on('change',function(){
                if ($('#date_from').val().length > 0){
                    getLeaveDays();
                }
            });
        });

        function halfDay(){
            if($('#half-day').is(':checked')){
                getLeaveDays();
                document.getElementById('showHalfDayModal').click();
            } else {
                getLeaveDays();
                $('#halfdaydate').val('');
            }
        }

        function closeHalfDay(){
            document.getElementById('closeHalfDayModal').click();
            getLeaveDays();
            $('#halfdaydate').val('');
            $('#half-day').attr('checked',false);
        }
        function saveHalfDay(){
            let days = $('#no_of_days').val();
            $('#no_of_days').val(days - 0.5);
            $('#halfdaydate').val($("#halfdaydates").val());
            document.getElementById('closeHalfDayModal').click();
        }

        function getLeaveDays(){
            var startDay = new Date($('#date_from').val());
            var start = startDay.getFullYear() + '-' + ('0' + (startDay.getMonth() + 1)).slice(-2) + '-' + ('0' + startDay.getDate()).slice(-2);
            var endDay = new Date($('#date_to').val());
            var end = endDay.getFullYear() + '-' + ('0' + (endDay.getMonth() + 1)).slice(-2) + '-' + ('0' + endDay.getDate()).slice(-2)

            axios.get('../../../leave/getleavedays/'+start+'/'+end)
                .then(function (data) {
                    //console.log(data.data.days)
                    if($('#half-day').is(':checked')){
                        $('#no_of_days').val((data.data.days - 0.5));
                    } else {
                        $('#no_of_days').val(data.data.days);
                    }
                    if($('#leave_type').val() == 1){
                        if(data.data.days > $('#original_leave_days').val()){
                            $('#total_annual_leave').html($('#annual_leave').val() - (data.data.days - $('#original_leave_days').val()))
                        }

                        if($('#original_leave_days').val() > data.data.days){
                            var annualLeave = parseFloat($('#annual_leave').val());
                            var originalLeaveDays = parseFloat($('#original_leave_days').val());
                            var remainingDays = originalLeaveDays - parseFloat(data.data.days);
                            $('#total_annual_leave').html(annualLeave + remainingDays);
                        }

                        if($('#original_leave_days').val() == data.data.days){
                            $('#total_annual_leave').html($('#annual_leave').val());
                        }
                    }

                    if($('#leave_type').val() == 2){
                        if(data.data.days > $('#original_sick_days').val()){
                            $('#total_sick_leave').html($('#sick_leave').val() - (data.data.days - $('#original_sick_days').val()))
                        }

                        if($('#original_sick_days').val() > data.data.days){
                            var annualLeave = parseFloat($('#sick_leave').val());
                            var originalLeaveDays = parseFloat($('#original_sick_days').val());
                            var remainingDays = originalLeaveDays - parseFloat(data.data.days);
                            $('#total_sick_leave').html(annualLeave + remainingDays);
                        }

                        if($('#original_sick_days').val() == data.data.days){
                            $('#total_sick_leave').html($('#sick_leave').val());
                        }
                    }


                    $('#submit').attr('disabled', false);
                    $('#submit').html('Save');
                    let options = "<option value=''>Select Day</option>";
                data.data.dates.forEach((v, i) => options += `<option ${$('#halfdaydate').val() == v.date || start == v.date ? 'selected':null} value="${v.date}">${v.date}</option>`);
                $("#halfdaydates").html(options);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }

        function approveLeave(id){
            axios.get('/leave/'+id+'/approve')
                .then(function (data) {
                    toastr.success('<strong>Success!</strong> '+data.data.message);

                        toastr.options.timeOut = 1000;
                        $('#leave_status').html('Approved');
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }

        function declineLeave(id){
            axios.get('/leave/'+id+'/decline')
                .then(function (data) {
                    toastr.success('<strong>Success!</strong> '+data.data.message);

                        toastr.options.timeOut = 1000;
                        $('#leave_status').html('Declined');
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }

        function getassignments(){

            $(".getassignment").hide();
            $(".getassignment").html('');

            var startDay = new Date($('#date_from').val());
            var start = startDay.getFullYear() + '-' + ('0' + (startDay.getMonth() + 1)).slice(-2) + '-' + ('0' + startDay.getDate()).slice(-2);
            var endDay = new Date($('#date_to').val());
            var end = endDay.getFullYear() + '-' + ('0' + (endDay.getMonth() + 1)).slice(-2) + '-' + ('0' + endDay.getDate()).slice(-2);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;

            //$('.no_of_days').val( Math.floor(Math.abs(days)));
            $('#submit').attr('disabled', true);
            $('#submit').html('Loading ...');


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/getassignment',
                type:"POST",
                dataType:"json",
                data:{date_from:start,date_to:end,resource:$('.resource').val()},
                success:function(data){
                    if(data.count > 0){
                        $(".getassignment").addClass('alert').addClass('alert-danger');
                        var str = '';

                        str += '<strong>Projects assigned to during leave period.</strong><br /><ul>';
                        $.each(data.projects,function(index,val){
                            str += '<li>'+val+'</li>';
                        });
                        str += '</ul>';
                        
                        $(".getassignment").html(str);

                        $(".getassignment").show();
                        $(".leave_balance").show();
                        $("#clock").hide();
                        $("#leave_content").show();
                    } else {
                        $(".getassignment").addClass('alert').addClass('alert-info');
                        $(".getassignment").html("Resource is not assigned to any projects during requested leave period");
                        $(".getassignment").show();
                        $(".leave_balance").show();
                        $("#clock").hide();
                        $("#leave_content").show();
                    }
                }
            });
        }
    </script>
@endsection
