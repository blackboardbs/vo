@extends('adminlte.default')

@section('title')
View Billing Cycle
@endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top:7px;"><i class="fa fa-caret-left"></i> Back</button>
                @if($can_update)
                    <a href="{{route('billing.edit',$billing_cycle)}}" class="btn btn-success float-right ml-1" style="margin-top:7px;">Edit</a>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <table class="table table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">Billing Cycle Information</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th style="width: 25%">Billing Cycle Name:</th>
                    <td colspan="3">{{$billing_cycle->name}}</td>
                </tr>
                <tr>
                    <th>Billing Cycle Description:</th>
                    <td colspan="3">{{$billing_cycle->description}}</td>
                </tr>
                <tr>
                    <th>Maximum Weeks Allowed per Billing Period:</th>
                    <td colspan="3">{{$billing_cycle->max_weeks}}</td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td colspan="3">{{$billing_cycle->status->description??null}}</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-sm mb-0">
            <thead class="bg-dark">
                <tr>
                    <th>Billing Periods</th>
                </tr>
            </thead>
        </table>
        <div class="row">
            <div class="col-md-12">
                <blackboard-billing-period
                        :billing-periods="{{json_encode($billing_cycle->billingPeriods, true)}}"
                        :billing-cycle-id="{{$billing_cycle->id}}"
                        :edit-page="false"
                        :can_create="{{$can_create == true ? 1 : 0}}"
                        :can_update="{{$can_update == true ? 1 : 0}}"
                >
                </blackboard-billing-period>
            </div>

        </div>
    </div>
@endsection
