@extends('adminlte.default')

@section('title') Add Billing Cycle @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createBilling')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <hr />
    <div class="table-responsive">
    {{Form::open(['url' => route('billing.store'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off','id'=>'createBilling'])}}
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">Billing Cycle Information</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                   <th>Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::text('name', old('name'), ['class' => 'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('description',old('company_name'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
            <tr>
                <th>Max Weeks per Billing Period: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('max_weeks',$max_weeks_dropdown, 4, ['class'=>'form-control form-control-sm '. ($errors->has('max_weeks') ? ' is-invalid' : ''),'placeholder'=>'Select Max Weeks'])}}
                    @foreach($errors->get('max_weeks') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('status_id',$status_dropdown, 1, ['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''),'placeholder'=>'Select Status'])}}
                    @foreach($errors->get('status_id') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            </tbody>
        </table>
        {{Form::close()}}
    </div>
    </div>
@endsection
