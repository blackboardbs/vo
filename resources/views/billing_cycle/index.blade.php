@extends('adminlte.default')
@section('title') Billing Cycle @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($permission)
            <a href="{{route('billing.create')}}" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-plus"></i> Billing Cycle</a>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('billing.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name', 'Name')</th>
                    <th>Description</th>
                    <th>@sortablelink('updated_at', 'Update Date')</th>
                    <th class="text-right">Max Weeks</th>
                    <th>Status</th>
                    @if($permission)
                        <th class="last">Actions</th>
                    @endif
                </tr>
            </thead>
                <tbody>
                @forelse($billing_cycles as $cycle)
                    <tr>
                        <td><a href="{{route('billing.show', $cycle)}}">{{$cycle->name}}</a></td>
                        <td>{{$cycle->description}}</td>
                        <td>{{$cycle->updated_at->toDateString()}}</td>
                        <td class="text-right">{{$cycle->max_weeks}}</td>
                        <td>{{$cycle->status?->description}}</td>
                        @if($permission)
                            <td>
                                <div class="d-flex">
                                @if($cycle->is_standard)
                                        <a href="{{route('billing.edit', $cycle)}}" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                                @endif
                                <button class="btn btn-sm btn-danger mr-1 ml-1" onclick="checkIfCycleIsUsed({{$cycle->id}})"><i class="fas fa-trash"></i></button>
                                {{ Form::open(['method' => 'post','route' => ['billing.duplicate', $cycle],'class'=>'d-inline']) }}
                                {{-- {{ Form::submit('Duplicate', ['class' => 'btn btn-primary btn-sm']) }} --}}
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-copy"></i></button>
                                {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No billing cycle match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $billing_cycles->firstItem() }} - {{ $billing_cycles->lastItem() }} of {{ $billing_cycles->total() }}
                        </td>
                        <td>
                            {{ $billing_cycles->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        function checkIfCycleIsUsed(billing_cycle_id){
            axios.get('/api/billing/check_usage/'+billing_cycle_id)
                .then(response => {
                    let message = response.data.message;
                    if (message){
                        alert(message)
                    }else{
                        deleteBillingCycle(billing_cycle_id);
                    }
            }).catch(err => console.log(err));
        }

        function deleteBillingCycle(billing_cycle_id){
            if (confirm("Are you sure you want to delete")) {
                axios.delete('/billing/' + billing_cycle_id)
                    .then(response => {
                        alert(response.data.message)
                        window.location.href = '{{route('billing.index')}}';
                    }).catch(err => console.log(err.response));
            }
        }
    </script>
@endsection