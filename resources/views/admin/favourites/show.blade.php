@extends('admin.adminlte.default')

@section('title') View Favourites @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{ route('admin.favourites.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.favourites.edit', $favourite)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Module Name</th>
                        <td>{{$favourite->module_name}}</td>
                        <th>Button Function</th>
                        <td>{{$favourite->button_functions}}</td>
                    </tr>
                    <tr>
                        <th>Routes</th>
                        <td>{{$favourite->route}}</td>
                        <th>Status</th>
                        <td>{{$favourite->status?->description}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection