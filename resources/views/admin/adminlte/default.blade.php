<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>{{$page_title??config('app.name')}}</title>

        <link rel="icon" href="{!! global_asset('assets/favico.png') !!}" type="image/x-icon"/>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <!-- IonIcons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! global_asset('adminlte/dist/css/adminlte.min.css') !!}">
        <link rel="stylesheet" href="{!! global_asset('css/custom.css?v0.00002') !!}">
        <link rel="stylesheet" href="{!! global_asset('css/toastr.css') !!}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <style>
            /* hr{
                margin-top: 0.5rem;
                margin-bottom: 0.5rem;
            }
            .container-title {
    margin-top: 0px;
} */

.nav-sidebar .nav-item>.nav-link .right{
    margin-top:-4px !important;
}

.nav-sidebar .nav-item>.nav-link p{
    margin-bottom:0px !important;
}

#search,.nav-sidebar .nav-item:last-child{
    margin-bottom:10px;
}

li.page-item {

display: none;
}

.page-item:first-child,
.page-item:nth-child( 2 ),
.page-item:nth-last-child( 2 ),
.page-item:nth-child( 3 ),
.page-item:nth-last-child( 3 ),
.page-item:nth-child( 4 ),
.page-item:nth-last-child( 4 ),
.page-item:nth-child( 5 ),
.page-item:nth-last-child( 5 ),
.page-item:last-child,
.page-item.active,
.page-item.disabled {

display: block;
}
.pagination{
    padding-left: 10px;
    margin-bottom: 0px !important;
}

.pagination>li:first-child {
border-left:0px !important;
}

#toast-container > .toast {
  width: fit-content;
  white-space: nowrap;
}
            .ui-tooltip, .arrow:after {
                background: black !important;
                border: 0px solid transparent !important;
            }
            .ui-tooltip {
                padding: 5px 10px;
                color: white !important;
                font-size: 11px !important;
                font-weight:normal !important;
                border-radius: 5px;
                text-transform: uppercase;
                box-shadow: 0 0 7px black;
            }
            .arrow {
                width: 60px;
                height: 14px;
                overflow: hidden;
                position: absolute;
                left: 50%;
                margin-left: -35px;
                bottom: -10px;
            }
            .arrow.top {
                top: -16px;
                bottom: auto;
            }
            .arrow.left {
                left: 20%;
            }
            .arrow:after {
                content: "";
                position: absolute;
                left: 20px;
                top: -20px;
                width: 25px;
                height: 25px;
                box-shadow: 6px 5px 9px -9px black;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            .arrow.top:after {
                bottom: -20px;
                top: auto;
            }

            .clock{
	border-radius: 70px;
	border: 3px solid #000;
	position: absolute;
    background:#000;
  top: 65%;
  left: 50%;
  margin-left: -50px;
  margin-top: -50px;
  display: block;
  width: 30px;
  height: 30px;
}

.clock:after{
	content: "";
	position: absolute;
	background-color: #fff;
	top: 0px;
	left: 48%;
	height: 12px;
	width: 4px;
	border-radius: 5px;
	-webkit-transform-origin: 50% 97%;
			transform-origin: 50% 97%;
	-webkit-animation: grdAiguille 2s linear infinite;
			animation: grdAiguille 2s linear infinite;
}

@-webkit-keyframes grdAiguille{
    0%{-webkit-transform:rotate(0deg);}
    100%{-webkit-transform:rotate(360deg);}
}

@keyframes grdAiguille{
    0%{transform:rotate(0deg);}
    100%{transform:rotate(360deg);}
}

.clock:before{
	content: "";
	position: absolute;
	background-color: #fff;
	top: 4px;
	left: 48%;
	height: 8px;
	width: 4px;
	border-radius: 5px;
	-webkit-transform-origin: 50% 94%;
			transform-origin: 50% 94%;
	-webkit-animation: ptAiguille 12s linear infinite;
			animation: ptAiguille 12s linear infinite;
}

@-webkit-keyframes ptAiguille{
    0%{-webkit-transform:rotate(0deg);}
    100%{-webkit-transform:rotate(360deg);}
}

@keyframes ptAiguille{
    0%{transform:rotate(0deg);}
    100%{transform:rotate(360deg);}
}
        </style>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://cdn.rawgit.com/tonystar/float-label-css/v1.0.2/dist/float-label.min.css"/>
        @yield('extra-css')

        
        {{-- @if(strPOS($_SERVER['SERVER_NAME'], "team") !== false || strPOS($_SERVER['SERVER_NAME'], "wotrenstar") !== false)
            <style>
                .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active {
                    box-shadow: none;
                    color: #FFFFFF !important;
                    background: #343a40 !important;
                }

                .sidebar .header, [class*=sidebar-dark] .user-panel, [class*=sidebar-dark] .brand-link{
                    border-top:0px !important;
                }
                img#brand{
                    height:auto !important;
                    max-width:100% !important;
                }

                .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active, .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active:hover, .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active{
                    color:#FFFFFF !important;
                }

                .sidebar .nav-link:after {
                    display:block;
                    content: '';
                    border-bottom: solid 2px #343a40;
                    transform: scaleX(0);
                    transition: transform 250ms ease-in-out;
                }

                .nav-link:hover:after {
                    transform: scaleX(1);
                }

                .sidebar-dark-primary .nav-link:hover{
                    background-color: transparent !important;
                }
                .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active, .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active:hover{
                    background: #343a40 !important;
                }
                .btn-dark, .bg-dark, .content-wrapper th a, .pagination li.active span {
                    color: #fff !important;
                    background-color: #343a40 !important;
                    border-color: #343a40 !important;
                    box-shadow: 0 1px 1px rgba(0, 0, 0, .075) !important;
                }
            </style>
        @endif --}}
        <style>
            .last {
                width: 1%;
                white-space: nowrap;
            }

            .has-float-label select {
                background-position: right 0.5em bottom 0.75em;
                background-size: 8px 10px;
            }

            .form-group .input-group,.input-group{
                width: 100%;
                display: block  !important;
                flex-wrap: nowrap;
            }

            .has-float-label{
                display: block  !important;
            }
            .has-float-label label, .has-float-label>span {
    position: absolute;
    cursor: text;
    font-size: 75%;
    opacity: 1;
    -webkit-transition: all .2s;
    transition: all .2s;
    top: -0.65em;
    left: 0.4rem;
    z-index: 3;
    line-height: 1;
    padding: 0 5px;
    background: #fff;
}
.input-group .form-control, .input-group-addon, .input-group-btn {
    display: table-cell;
}

.has-float-label input, .has-float-label select, .has-float-label textarea {
    font-size: inherit;
    padding: 0.375rem 0.75rem;
    margin-bottom: 2px;
    border: 1px;
    border-radius: 0.25rem;
    border-bottom: 1px solid #ced4da;
    border: 1px solid #ced4da;
}

.has-float-label select {
background-color: #FFF;
}
.input-group .has-float-label {
    display: table-cell;
}

.nav-tabs li a{
    color:#343a40 !important;
}

.content-wrapper .container-fluid{
    padding-left: 25px !important;
    padding-right:25px !important;
}

.bg-secondary{
    background-color: #cccccc !important;
}
/* Tooltip text */
.toolt{
    position: relative;
}
.toolt .tooltiptext {
  visibility: hidden;
  min-width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  padding: 5px 10px;
  border-radius: 6px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 9999;
}

/* Show the tooltip text when you mouse over the tooltip container */
.toolt:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}

.toolt .disabled:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}

.toolt .tooltiptext {
  top: -5px;
  right: 105%;
  opacity:0;
}

.toolt .tooltiptext::after {
  content: " ";
  position: absolute;
  top: 50%;
  left: 100%; /* To the right of the tooltip */
  margin-top: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent transparent black;
}

/* .swal2-title:before{
    content: 'Confirm';
} */
.swal2-container{
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
}

.swal2-container.swal2-backdrop-show, .swal2-container.swal2-noanimation, .swal2-container.in {
}
.swal2-title{
    font-size: 1.25rem;
    line-height: 1.5;
    font-weight: 500;
    padding: 0.5rem;
    background-color:#dc3545 !important;
    color:#FFF !important;
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
}
.swal2-shown{
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    overflow: hidden;
    outline: 0;
    height: fit-content;
    display: block !important;
}

.swal2-show{
    display: block !important;
}
.swal2-container{
    position: absolute;
    display: flex;
    flex-direction: column;
    width: 450px !important;
    max-width: 450px;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.2);
    box-shadow: 0 0.25rem 0.5rem rgba(0,0,0,.5);
    outline: 0;
    z-index: 99999;
    transition: transform .3s ease-out;
    transform: translate(0,0);
    margin: 1.75rem auto;
}

.swal2-content{
    display: block;
    text-align: center;
    font-weight: 700;
    padding-bottom: 1.25rem;
}

.swal2-actions{
    text-align: center;
    padding-bottom: 2rem;
}

.swal2-confirm{
    margin-right: 0.25rem;
    padding: 0.25rem 0.5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: 0.2rem;
    color: #fff;
    border:0px;
    background-color: #007bff;
    border-color: #007bff;
    box-shadow: 0 1px 1px rgba(0,0,0,0);
}

.swal2-cancel{
    margin-right: 0.25rem;
    padding: 0.25rem 0.5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: 0.2rem;
    border:0px;
    box-shadow: 0 1px 1px rgba(0,0,0,0);
}


button[type=button]{border:none;outline: none;}

.dp__input_icon{
    transform: none !important;
    top:12% !important;
}
        </style>
@if(isset($config->background_color) && isset($config->font_color) && isset($config->font_color))
            <style>
                .sidebar-dark-primary {
                    background-color: #{!! $config->background_color !!} !important;
                }
                .sidebar .header, .sidebar-dark-primary .sidebar a {
                    color: #{!! $config->font_color !!} !important;
                }
                .sidebar .header, [class*=sidebar-dark] .user-panel, [class*=sidebar-dark] .brand-link {
                    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
                    /* border-top: 1px solid rgba(0, 0, 0, 0.1); */
                }
                .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active, .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active:hover, .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active {
                    background-color: #{!! $config->active_link !!};
                }
                .btn-dark, .bg-dark, .content-wrapper th a, .pagination li.active span {
                    color: #{!! $config->font_color !!} !important;
                    background-color: #{!! $config->background_color !!} !important;
                    border-color: #{!! $config->background_color !!} !important;
                    box-shadow: 0 1px 1px rgba(0,0,0,0.075);
                }
            </style>
        @endif
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to to the body tag
    to get the desired effect
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition sidebar-mini" style="min-height:100vh !important;">
    @if(Session::has('flash_projecterror'))
    <div class="alert alert-warning alert-dismissible" style="position:fixed;top:0;z-index:99999;left: 50%;
transform: translateX(-50%);">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{ session()->get('flash_projecterror') }}
                        {{Session::forget('flash_projecterror')}}
                    @endif
            </div>
        <div id="app" class="wrapper">

            @include('admin.adminlte.header')
            @include('admin.adminlte.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper main-container" style="padding-top: 10px;">
                @yield('header')

                @yield('content')
            </div>
            <!-- /.content-wrapper -->



            <!-- Control Sidebar -->
            <aside onmouseover="showScroller(this)" onmouseout="hideScroller(this)" class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->

            <!-- Main Footer -->
            <footer class="main-footer" style="margin-top:20px;height:50px;">
                <!-- To the right -->
                <div class="float-left">
                Copyright &copy; {{now()->year}} <a href="https://www.consulteaze.com" class="text-dark">www.consulteaze.com</a>. All rights reserved.
                </div>
                <!-- Default to the left -->
                <div class="float-right">
                    <img src="{!! global_asset('assets/consulteaze_logo.png') !!}" alt="Consulteaze" style="height:20px;margin-bottom: 10px;">
                </div>
            </footer>
            
    <button type="button" class="d-none" id="showConfirmDialog" data-toggle="modal" data-target="#confirmModal"></button>
            </a>
            <div class="modal fade" id="confirmModal">
                <div class="modal-dialog" style="width:450px !important;max-width:450px;">
                    <div class="modal-content" style="background-color:transparent;">
                        <div class="modal-header text-center bg-danger" style="border-bottom: 0px;padding:.5rem;">
                            <h5 class="modal-title">Warning</h5>
                            <a class="close" data-dismiss="modal" aria-label="Close" id="hideConfirmDialog">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body bg-white">
                            <div class="form-group text-center mx-3">
                                {{Form::label('popup', '',['id'=>'confirmMessage'])}}
    
                            </div>
                            <div class="form-group text-center">
                                <button type="button" class="btn btn-sm btn-danger mr-1" id="confirmOk">Yes</button>
                                <button type="button" class="btn btn-sm btn-default" id="confirmCancel">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @if(session()->has('subscriptionExp'))
    <button type="button" class="btn-danger btn-lg" id="showLicenseDialog" data-toggle="modal" data-target="#license"></button>
            <div class="modal fade show" id="license" tabindex="-1" role="dialog" aria-modal="true">
                <div class="modal-dialog" style="width:450px !important;max-width:450px;">
                    <div class="modal-content">
                            <a class="d-none" data-dismiss="modal" aria-label="Close" class="" id="hideLicenseDialog">
                            </a>
                        <div class="modal-body">
                                    {{ session()->get('subscriptionExp') }}
                        </div>
                        <div class="modal-footer text-center d-block">
                            <button type="button" class="btn btn-dark" id="closeL">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
        @if(session()->has('subscriptionQuotaPercentage'))
    <button type="button" class="btn-danger btn-lg" id="showQuotaPercentageDialog" data-toggle="modal" data-target="#quotaPerc"></button>
            <div class="modal fade show" id="quotaPerc" tabindex="-1" role="dialog" aria-modal="true">
                <div class="modal-dialog" style="width:450px !important;max-width:450px;">
                    <div class="modal-content">
                            <a class="d-none" data-dismiss="modal" aria-label="Close" class="" id="hideQuotaPercentageDialog">
                            </a>
                        <div class="modal-body">
                                    {!! session()->get('subscriptionQuotaPercentage') !!}
                        </div>
                        <div class="modal-footer text-center d-block">
                            <button type="button" class="btn btn-dark" id="closeQ">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED SCRIPTS -->
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    {{-- console.log('bootstrap') --}}
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.proto.js"></script> --}}

        @vite('resources/js/app.js')


        <!-- AdminLTE -->
        <script src="{!! global_asset('adminlte/dist/js/adminlte.js') !!}"></script>
        <script src="{!! global_asset('adminlte/dist/js/jscolor.js') !!}"></script>
        <script src="{!! global_asset('js/toastr.js') !!}"></script>
        <!-- OPTIONAL SCRIPTS -->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>


        <script>
            $(document).ready(function(){
                let logoHeight = $('.brand-link').height();
                let sidebarHeight = $("body").innerHeight();
                console.log((sidebarHeight));
                $('.sidebar').height((sidebarHeight-logoHeight));
                setTimeout("$('.tooltiptext').css('opacity',1)", 500);
                // $(".chosen-select").chosen();
                @if(Session::has('subscriptionExp'))
                document.getElementById('showLicenseDialog').click();
                @endif
                @if(Session::has('subscriptionQuotaPercentage'))
                document.getElementById('showQuotaPercentageDialog').click();
                @endif

                $('#closeL').on('click',function(){
                document.getElementById('hideLicenseDialog').click()
                        {{Session::forget('subscriptionExp')}}
        })
        $('#closeQ').on('click',function(){
                document.getElementById('hideQuotaPercentageDialog').click()
                        {{Session::forget('subscriptionQuotaPercentage')}}
        })
                    @if(Session::has('flash_success'))
                        toastr.success('<strong>Success!</strong> {{Session::get('flash_success')}}');

                        toastr.options.timeOut = 1000;
                        {{Session::forget('flash_success')}}
                    @endif
                    @if(Session::has('flash_info'))
                        toastr.info('<strong>Notice.</strong> {{Session::get('flash_info')}}');

                        toastr.options.timeOut = 1000;
                        {{Session::forget('flash_info')}}
                    @endif
                    @if(Session::has('flash_danger'))
                        toastr.error('<strong>Error!</strong> {{Session::get('flash_danger')}}');

                        toastr.options.timeOut = 1000;
                        {{Session::forget('flash_danger')}}
                    @endif
                    @if(Session::has('flash_warning'))
                        toastr.warning('<strong>Warning!</strong> {{Session::get('flash_warning')}}');

                        toastr.options.timeOut = 1000;
                        {{Session::forget('flash_warning')}}
                    @endif
   
                let g = $('main-container').css('min-height', $('main-container').height() + 17);
                // console.log($('main-container').css('min-height'))

                var active_elem = $(".nav-sidebar").find(".active");

                $(active_elem).parent('li').parent('ul').parent('li').addClass('menu-open');
                //$(active_elem).parent('li').parent('ul').addClass('test');
            });

            function handle(e){
                if(e.keyCode === 13){
                    e.preventDefault(); // Ensure it is only this code that runs

                
                    if($('#searchform').length){
                document.getElementById('searchform').submit()
                }
                
                if($('#filters').length){
                document.getElementById('filters').submit()
                }
                }
            }

            function saveForm(val){
                document.getElementById(val).submit()
            }
            
            function clearFilters(){
                
                if($('#searchform').length){
                document.getElementById('searchform').submit()
                }
                
                if($('#filters').length){
                document.getElementById('filters').submit()
                }
            }
            
        function submitSearch(){
            $('#searchform').submit()
            $('#filters').submit()
        }
        
        function confirmDialog(message, onConfirm){
                var fClose = function(){
                document.getElementById('hideConfirmDialog').click()
                };

                // var modal = $("#confirmModal");
                document.getElementById('showConfirmDialog').click()
                $("#confirmMessage").empty().append(message);
                $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
                $("#confirmCancel").unbind().one("click", fClose);
            }
           
            
            function changeTerms(){
            var data = {
                terms_id: $("select[name=terms_version]").val()
            };
            axios.post('{{route('project.getterms')}}', data)
                .then(function (data) {
                    $('#travel').html(data['data'].terms.exp_travel);
                    $('#parking').html(data['data'].terms.exp_parking);
                    $('#car_rental').html(data['data'].terms.exp_car_rental);
                    $('#flights').html(data['data'].terms.exp_flights);
                    $('#toll').html(data['data'].terms.exp_toll);
                    $('#accommodation').html(data['data'].terms.exp_accommodation);
                    $('#per_diem').html(data['data'].terms.exp_per_diem);
                    $('#out_of_town').html(data['data'].terms.exp_out_of_town);
                    $('#data').html(data['data'].terms.exp_data);
                    $('#other').html(data['data'].terms.exp_other);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }
            //Master Data search
            $(function() {
                
                $('#r').on('change',function(){
                    if($('#searchform').length){
                        if (! $('input[name="r"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "r")
                                .attr("value", $('#r').val())
                                .appendTo("#searchform");
                                submitSearch()
                        } else {
                            $('input[name="r"]').val($('#r').val())
                            submitSearch()
                        }
                    }
                    if($('#filters').length){
                        if (! $('input[name="r"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "r")
                                .attr("value", $('#r').val())
                                .appendTo("#filters");
                                submitSearch()
                        } else {
                            $('input[name="r"]').val($('#r').val())
                            submitSearch()
                        }
                    }
                    if($('#filter_form').length){
                        if (! $('input[name="r"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "r")
                                .attr("value", $('#r').val())
                                .appendTo("#filter_form");
                                submitSearch()
                        } else {
                            $('input[name="r"]').val($('#r').val())
                            submitSearch()
                        }
                    }
                })

                $('#s').on('change',function(){
                    if($('#searchform').length){
                        if (! $('input[name="s"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "s")
                                .attr("value", $('#s').val())
                                .appendTo("#searchform");
                                submitSearch()
                        } else {
                            $('input[name="s"]').val($('#s').val())
                            submitSearch()
                        }
                    }
                    if($('#filters').length){
                        if (! $('input[name="s"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "s")
                                .attr("value", $('#s').val())
                                .appendTo("#filters");
                                submitSearch()
                        } else {
                            $('input[name="s"]').val($('#s').val())
                            submitSearch()
                        }
                    }
                    if($('#filter_form').length){
                        if (! $('input[name="s"]').length) {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "s")
                                .attr("value", $('#s').val())
                                .appendTo("#filter_form");
                                submitSearch()
                        } else {
                            $('input[name="s"]').val($('#s').val())
                            submitSearch()
                        }
                    }
                })

                $("#search").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $(".searchable").filter(function() {
                        //$(".searchable a i").hide();
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                    });

                    $(".delete").on('submit',function(e){
        // Stop the form submitting
        e.preventDefault();
                    let YOUR_MESSAGE_STRING_CONST = "Are you sure you want to delete this record?";

                    confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                        // return true;
                        e.currentTarget.submit();
                        // $(this).parents('form:first').submit();
                    });
                    // return confirm("Are you sure you want to delete this record?");
                });

                
                $(".cancelInvoice").on('submit',function(e){
        // Stop the form submitting
        e.preventDefault();
                    let YOUR_MESSAGE_STRING_CONST = "Are you sure you want to cancel this invoice?";

                    confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                        // return true;
                        e.currentTarget.submit();
                        // $(this).parents('form:first').submit();
                    });
                    // return confirm("Are you sure you want to delete this record?");
                });
            })

            $(function() {
                $( ".datepicker" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    showWeek:true,yearRange: "1920:2019"
                });

                $( ".datepickerfrom" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    showWeek:true,
                    yearRange: "1920:2019",
                    onSelect: function(date){

                        var selectedDate = new Date(date);
                        var msecsInADay = 86400000;
                        var endDate = new Date(selectedDate.getTime() + msecsInADay);

                        //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
                        $(".datepickerto").datepicker( "option", "minDate", endDate );

                    }
                });

                $( ".datepickerto" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    showWeek:true,
                    yearRange: "1920:2019"
                });

                $('#generate').on('click', () => {
                    if(($("#v_period").val() != 0) || (($('#from-value').datepicker('getDate') != null) && ($("#to-value").datepicker('getDate') != null))){
                        return true;
                    }else {
                        $("#period_err").append('<p class="invalid-feedback" style="display:block;margin-bottom:0">Select either period or date range</p>');
                        $('#v_period, #from-value, #to-value').addClass(' is-invalid');
                        return false;
                    }

                })
            });

                $(function () {



                /*.on('change',function(){
                   var d = new Date($('#calendar_date').val());
                   var year = d.getFullYear();
                   $('.year').val(year);
                   var month = d.getMonth();
                    $('.month').val(month);
                   var day = d.getDate();
                    $('.day').val(day);

                    Date.prototype.getWeek = function() {
                        return Math.ceil((((this - year) / 86400000) + year+1)/7);
                    };

                    var week = d.getWeek();
                    alert(week);
                   var dayname = d.getDay();
                });*/
            });
            $(function (){
                const url = new URL(window.location.toLocaleString());
                let form = $('.search').closest('form');
                console.log(url.searchParams)
                url.searchParams.forEach((v, k) => {
                    if(k === 'q'){
                        $("#"+k).val(v.replaceAll('+', ' '))
                    }else {
                        $("#"+k).val(v)
                    }
                })
                $('.search').on('change', function (){
                    form.submit();
                });
            })

            $(function () {
                    let logo = $("#brand");
                    if (logo.width() > logo.height()){
                         if (logo.width() > 235) {
                             logo.css({
                                 width:"100%",
                                 height:'auto',
                             })
                         }
                    } else if (logo.height() < 85){
                        logo.css({
                            height:"85px",
                            width: 'auto',
                        })
                    } else {
                        //logo.css("height","85px")
                    }
                })

                function completeProject(projid){
                    axios.get('/project/getassignmenttasks/'+projid)
                        .then(function (data) {
                            let YOUR_MESSAGE_STRING_CONST = "Are you sure you want to complete this project?<br /><br />Number of open Assignments: "+data.data.assignments+"<br />Number of open Tasks: "+data.data.tasks;

                            confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                                window.location.href = '/project/'+projid+'/complete';
                            });
                        })
                        .catch(function () {
                            console.log("An Error occured!!!");
                        });
                }

                
                function completeAssignment(assid){
                    axios.get('/assignment/gettasks/'+assid)
                        .then(function (data) {
                            let YOUR_MESSAGE_STRING_CONST = "Are you sure you want to complete this assignment?<br /><br />Number of open Tasks: "+data.data.tasks;

                            confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                                window.location.href = '/assignment/'+assid+'/complete';
                            });
                        })
                        .catch(function () {
                            console.log("An Error occured!!!");
                        });
                }

            $(function () {
                

                function lightOrDark(color) {

                    // Variables for red, green, blue values
                    var r, g, b, hsp;

                    // Check the format of the color, HEX or RGB?
                    if (color.match(/^rgb/)) {

                        // If HEX --> store the red, green, blue values in separate variables
                        color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);

                        r = color[1];
                        g = color[2];
                        b = color[3];
                    }
                    else {

                        // If RGB --> Convert it to HEX: http://gist.github.com/983661
                        color = +("0x" + color.slice(1).replace(
                            color.length < 5 && /./g, '$&$&'));

                        r = color >> 16;
                        g = color >> 8 & 255;
                        b = color & 255;
                    }

                    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
                    hsp = Math.sqrt(
                        0.299 * (r * r) +
                        0.587 * (g * g) +
                        0.114 * (b * b)
                    );

                    // Using the HSP value, determine whether the color is light or dark
                    if (hsp>127.5) {
                        $(".nav-link").mouseenter(function (e) {
                            $(this).css({
                                backgroundColor: 'rgba(0, 0, 0, .1)',
                            })
                        }).mouseleave(function () {
                            $(this).removeAttr("style");
                        })
                    }
                    else {
                        $(".nav-item").mouseenter(function (e) {
                            $(this).css({
                                backgroundColor: 'rgba(255, 255, 255, .1)',
                            })
                        }).mouseleave(function () {
                            $(this).removeAttr("style");
                        })
                    }
                }

                let color = "{!! isset($config->background_color)? '#'.$config->background_color:'#212529'!!}";

                lightOrDark(color);
            })
            $(function () {
                $("#favourites").click(function() {
                    $("#favourites-wrapper").fadeIn(1200);
                });
                $("#favourites").mouseover(function() {
                    $("#favourites-wrapper").fadeIn(1200);
                });
                $(document).mouseup(function(e){
                    let container = $("#favourites-wrapper");

                    if(!container.is(e.target) && container.has(e.target).length === 0){
                        container.fadeOut(1200);
                    }
                });
            })

            $(function () {
                $("#search").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $(".nav-sidebar li").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                 });
            })

            

            
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/'),
                'routes' => collect(\Route::getRoutes())->mapWithKeys(function ($route) { return [$route->getName() => $route->uri()]; }),
                'jsPermissions' => (auth()->user() ? auth()->user()->roles : ''),
                'userDetails' => auth()->user()
            ]) !!};
        </script>
        <script src="{!! global_asset('adminlte/dist/js/custom.js') !!}"></script>
        @yield('extra-js')
    </body>
</html>
