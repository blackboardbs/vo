<!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" style="min-height: 3.5rem">
    
    @auth
      <top-nav :user-id="{{ Auth::user()->id }}" :user-avatar="'{{ route('user_avatar', ['q' => Auth::user()->avatar]) }}'"></top-nav>
    @endauth
  </nav>
  <!-- /.navbar -->

