  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="padding-bottom: 35px !important;">
    <!-- Brand Logo -->
    <div class="{{'text-'.($config->logo_placement??'left')}}">
      <a href="{{ url('/admin/') }}" class="brand-link">
        <img src="{{global_asset('assets/consulteaze_logo.png')}}" id="brand" alt="Consulteaze" style="opacity: .8; width: 100%;height: auto">
      </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar blackboard-scrollbar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @auth
          <img src="{{route('user_avatar',['q'=>Auth::user()->avatar])}}" class="img-circle elevation-2" alt="User Image">
          @endauth
        </div>
        <div class="info">
          @auth
          <a href="{{route('profile',Auth::user()->id)}}" class="d-block">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
          @endauth
        </div>
      </div>

      <!-- Sidebar Menu -->
      {{--@auth--}}
      <nav class="mt-2">
        <input type="text" id="search" class="form-control form-control-sm">
        <ul class="nav search-list nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="padding-bottom: 85px;">

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Master Data
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.account.index')}}" class="nav-link {{ \Request::route()->named('admin.account.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Account</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.accountelement.index')}}" class="nav-link {{ \Request::route()->named('admin.accountelement.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Account Element</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.anniversarytype.index')}}" class="nav-link {{ \Request::route()->named('admin.anniversarytype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Anniversary Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.assessmentmaster.index')}}" class="nav-link {{ \Request::route()->named('admin.assessmentmaster.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assessment KPI</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.assessmentmasterdetails.index')}}" class="nav-link {{ \Request::route()->named('admin.assessmentmasterdetails.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assessment Master Details</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.assetclass.index')}}" class="nav-link {{ \Request::route()->named('admin.assetclass.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Asset Class</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.assignmentstandardcost.index')}}" class="nav-link {{ \Request::route()->named('admin.assignmentstandardcost.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assignment Standard Cost</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.businessfunction.index')}}" class="nav-link {{ (\Request::route()->named('admin.businessfunction.*')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Business Function</p>
                </a>
              </li>
              {{--<li class="nav-item">
                <a href="{{route('admin.calendar.index')}}" class="nav-link {{ \Request::route()->named('admin.calendar.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Calendar</p>
                </a>
              </li>--}}
              <li class="nav-item">
                <a href="{{route('admin.costcenter.index')}}" class="nav-link {{ \Request::route()->named('admin.costcenter.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cost Center</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.contracttype.index')}}" class="nav-link {{ \Request::route()->named('admin.contracttype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contract Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.cvcompany.index')}}" class="nav-link {{ \Request::route()->named('admin.cvcompany.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>CV Company</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.customisedashboard.index')}}" class="nav-link {{ \Request::route()->named('admin.customisedashboard.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard Components</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.dashboard.index')}}" class="nav-link {{ \Request::route()->named('admin.dashboard.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Default Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.defaultfavs.index')}}" class="nav-link {{ \Request::route()->named('admin.defaultfavs.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Default Favourites</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.expensetype.index')}}" class="nav-link {{ \Request::route()->named('admin.expensetype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Expense Type</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.deliverytype.index')}}" class="nav-link {{ \Request::route()->named('admin.deliverytype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Task Delivery Type</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.favourites.index')}}" class="nav-link {{ \Request::route()->named('admin.favourites.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Favourites</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.industry.index')}}" class="nav-link {{ \Request::route()->named('admin.industry.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Industry</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.profession.index')}}" class="nav-link {{ \Request::route()->named('admin.profession.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profession</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.speciality.index')}}" class="nav-link {{ \Request::route()->named('admin.speciality.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Speciality</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.sprintstatus.index')}}" class="nav-link {{ request()->routeIs('admin.sprintstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sprint Status</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.medical.index')}}" class="nav-link {{ \Request::route()->named('admin.medical.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Medical Certificate Type</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.paymentbase.index')}}" class="nav-link {{ \Request::route()->named('admin.paymentbase.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Base</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.paymenttype.index')}}" class="nav-link {{ \Request::route()->named('admin.paymenttype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Type</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.processinterview.index')}}" class="nav-link {{ \Request::route()->named('admin.processinterview.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Process Interview</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.processstatus.index')}}" class="nav-link {{ \Request::route()->named('admin.processstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Process Status</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.prospectstatus.index')}}" class="nav-link {{ \Request::route()->named('admin.prospectstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Prospect Status</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.quotationterms.index')}}" class="nav-link {{ \Request::route()->named('admin.quotationterms.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Quotation Terms</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.relationship.index')}}" class="nav-link {{ \Request::route()->named('admin.relationship.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Relationship</p>
                </a>
              </li>
              <li class="nav-item ">
                <a href="{{route('admin.resourcelevel.index')}}" class="nav-link {{ \Request::route()->named('admin.resourcelevel.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Resource Level</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.resourceposition.index')}}" class="nav-link {{ \Request::route()->named('admin.resourceposition.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Resource Position</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.skilllevel.index')}}" class="nav-link {{ \Request::route()->named('admin.skilllevel.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Skill Level</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.solution.index')}}" class="nav-link {{ \Request::route()->named('admin.solution.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Solution</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.skill.index')}}" class="nav-link {{ \Request::route()->named('admin.skill.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Skill</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.onboardingstatus.index')}}" class="nav-link {{ \Request::route()->named('admin.onboardingstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Onboarding Status</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.title.index')}}" class="nav-link {{ \Request::route()->named('admin.title.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Title</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.templatestyles.index')}}" class="nav-link {{ \Request::route()->named('admin.templatestyles.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Template Styles</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.scoutingrole.index')}}" class="nav-link {{ \Request::route()->named('admin.scoutingrole.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Main Role</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.usertype.index')}}" class="nav-link {{ \Request::route()->named('admin.usertype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Type</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.vatrate.index')}}" class="nav-link {{ \Request::route()->named('admin.vatrate.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Vat Rate</p>
                </a>
              </li>
            </ul>
          </li>
          {{--@if(Auth::user()->email == 'support@consulteaze.com' || Auth::user()->email == 'stefan@blackboardbi.com')--}}
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Admin Master Data
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.accounttype.index')}}" class="nav-link {{ \Request::route()->named('admin.accounttype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Account Type</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{route('admin.country.index')}}" class="nav-link {{ \Request::route()->named('admin.country.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Country</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.invoicestatus.index')}}" class="nav-link {{ \Request::route()->named('admin.invoicestatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice Status</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.leavetype.index')}}" class="nav-link {{ \Request::route()->named('admin.leavetype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Leave Type</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.maritalstatus.index')}}" class="nav-link {{ \Request::route()->named('admin.maritalstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Marital Status</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.paymentmethod.index')}}" class="nav-link {{ \Request::route()->named('admin.paymentmethod.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Method</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.paymentterms.index')}}" class="nav-link {{ \Request::route()->named('admin.paymentterms.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Terms</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.projecttype.index')}}" class="nav-link {{ \Request::route()->named('admin.projecttype.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Type</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.projectstatus.index')}}" class="nav-link {{ \Request::route()->named('admin.projectstatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Status</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.projectterms.index')}}" class="nav-link {{ \Request::route()->named('admin.projectterms.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Terms</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.status.index')}}" class="nav-link {{ \Request::route()->named('admin.status.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Status</p>
                </a>
              </li>

              <li class="nav-item searchable">
                <a href="{{route('admin.systemhealth.index')}}" class="nav-link {{ \Request::route()->named('admin.systemhealth.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>System Health</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.term.index')}}" class="nav-link {{ \Request::route()->named('admin.term.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Terms</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.vendorinvoicestatus.index')}}" class="nav-link {{ \Request::route()->named('admin.vendorinvoicestatus.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Vendor Invoice Status</p>
                </a>
              </li>
              <li class="nav-item searchable">
                <a href="{{route('admin.template.index')}}" class="nav-link {{ \Request::route()->named('admin.template.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Template Type</p>
                </a>
              </li>
            </ul>
          </li>
          {{--@endif--}}

 </ul>

</nav>
{{--@endauth--}}
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
