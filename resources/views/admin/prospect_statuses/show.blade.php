@extends('admin.adminlte.default')

@section('title') View Master Data Prospect Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.prospectstatus.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.prospectstatus.edit', $prospectstatus)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$prospectstatus->description}}</td>
                    <th>Status</th>
                    <td>{{$prospectstatus->status?->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection