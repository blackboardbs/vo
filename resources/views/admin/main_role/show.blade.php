@extends('admin.adminlte.default')

@section('title') View Master User Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.scoutingrole.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.scoutingrole.edit', $scoutingrole)}}" class="btn btn-success float-right">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Name</th>
                    <td>{{$scoutingrole->name}}</td>
                    <th>Status</th>
                    <td>{{$scoutingrole->status->name}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection