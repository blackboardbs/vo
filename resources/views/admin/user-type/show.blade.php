@extends('admin.adminlte.default')

@section('title') View Master User Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.usertype.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.usertype.edit', $usertype)}}" class="btn btn-success float-right">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$usertype->description}}</td>
                    <th>Status</th>
                    <td>{{$usertype->status?->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection