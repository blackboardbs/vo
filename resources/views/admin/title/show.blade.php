@extends('admin.adminlte.default')

@section('title') View Master Title @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.title.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.title.edit', $title)}}" class="btn btn-success float-right">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$title->description}}</td>
                    <th>Status</th>
                    <td>{{$title->status?->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection