@extends('adminlte.default')

@section('title') Add Master Quotation Term @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('quotation_terms.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Quotation <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3"> {{Form::select('quotation',$quotation_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('quotation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Terms <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td style="width: 35%"> {{Form::select('terms_id',$terms_dropdown,2,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('terms_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>
                    <th>Term <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{ Form::textarea('term', old('term'), ['class'=>'form-control form-control-sm'. ($errors->has('term') ? ' is-invalid' : ''), 'rows' => 3]) }}
                        @foreach($errors->get('term') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td colspan="2">
                        <div class="form-group row mb-0">
                            {{ Form::label('freeze', 'Freeze', ['class' => 'col-sm-2 col-form-label']) }} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                            <div class="col-md-10">
                                {{ Form::text('freeze', old('freeze'), ['class'=>'form-control form-control-sm'. ($errors->has('freeze') ? ' is-invalid' : '')]) }}
                                @foreach($errors->get('freeze') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            {{ Form::label('status', 'Status', ['class' => 'col-sm-2 col-form-label']) }} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                            <div class="col-md-10">
                                {{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                                @foreach($errors->get('status') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    
@endsection