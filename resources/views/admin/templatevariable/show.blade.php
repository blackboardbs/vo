@extends('adminlte.default')

@section('title') View Master Data Template Variable @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('variable.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('variable.edit', $variable)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Status</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Display Name</th>
                    <td>{{$variable->display_name}}</td>
                    <th>Variable</th>
                    <td>{{$variable->variable}}</td>

                </tr>
                <tr>
                    <th>Template Type</th>
                    <td>{{$variable->templateType?->name}}</td>
                    <th>Status</th>
                    <td>{{$variable->status?->description}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection