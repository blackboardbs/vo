@extends('admin.adminlte.default')

@section('title') Master Data - Delivery Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button class="btn btn-dark float-right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Delivery Type</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form id="searchform" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <select class="form-control" name="status_filter" id="status_filter" style="width: 120px;">
                            <option value="0">All</option>
                            <option selected value="1">Active</option>
                            <option value="2">Suspended</option>
                            <option value="6">Closed</option>
                        </select>
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('admin.deliverytype.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('description','Description')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($delivery_type as $result)
                    <tr>
                        <td><a href="{{route('admin.deliverytype.show',$result)}}">{{$result->name}}</a></td>
                        <td>{{$result->status->description??null}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('admin.deliverytype.edit', $result)}}" class="btn btn-success btn-sm mr-1" ><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['admin.deliverytype.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data delivery type entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $delivery_type->firstItem() }} - {{ $delivery_type->lastItem() }} of {{ $delivery_type->total() }}
                        </td>
                        <td>
                            {{ $delivery_type->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Master Data Delivery Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{Form::open(['url' => route('admin.deliverytype.store'), 'method' => 'post','class'=>'mt-3', 'id' => 'main-form'])}}
                        <div class="form-group">
                            {{Form::label('name', 'Name')}}
                            {{ Form::text('name', old('name'), ['class'=>'form-control form-control-sm', 'id' => 'name', 'autocomplete' => 'off']) }}
                            <div id="name-feedback" class="invalid-feedback" style="display: none"></div>
                        </div>
                        <div class="form-group">
                            {{Form::label('status', 'Status')}}
                            {{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            <div id="status-feedback" class="invalid-feedback" style="display: none"></div>
                        </div>
                        <div class="form-group text-center">
                            {{Form::submit('Add Delivery Type',['class'=>'btn btn-sm btn-dark', 'id' => 'submit'])}}
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#status_filter').on('change', function() {
                this.form.submit();
            });
        });
    </script>    

    <script>
        $(document).ready(function (){

            $("#submit").on('click', function (){

                let name = $("#name").val();
                let status = $("#status_dropdown").val();
                if (name == ''){
                    $("#name-feedback")
                        .append("Name is required")
                        .show();
                    $("#name").addClass('is-invalid');

                    return false;
                }

                if (status == 0){
                    $("#status-feedback")
                        .append("You selected invalid status")
                        .show();
                    $("#status_dropdown").addClass('is-invalid');

                    return false;
                }
            })
        });
    </script>
@endsection
