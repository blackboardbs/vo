@extends('admin.adminlte.default')

@section('title') View Master Data Industry @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('admin.deliverytype.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.deliverytype.edit', $type)}}" class="btn btn-success float-right ml-1">Edit</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$type->name}}</td>
                        <th>Status</th>
                        <td>{{$type->status?->description}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection