@extends('admin.adminlte.default')

@section('title') View Master Account Element @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.accountelement.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.accountelement.edit', $account_element[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($account_element as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Account</th>
                        <td>{{$result->account->description}}</td>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                    </tr>
                    <tr>
                        <th>Account Element</th>
                        <td>{{$result->account_element}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection