@extends('admin.adminlte.default')

@section('title') View Master Data Asset Class @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.assetclass.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.assetclass.edit', $asserts_class[0])}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($asserts_class as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                    <tr>
                        <th>Est. Life Months</th>
                        <td>{{$result->est_life_month}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection