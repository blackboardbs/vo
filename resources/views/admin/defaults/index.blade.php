@extends('admin.adminlte.default')

@section('title') Add Default Favourites @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('admin.defaultfavs.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Default</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form id="searchform" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('default_favs.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Module Name</th>
                    <th>Role</th>
                    {{-- <th>Status</th> --}}
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($favourites as $result)
                    <tr>
                        <td><a href="{{route('default_favs.show', $result)}}">{{isset($result->favourites)?$result->favourites->module_name:''}}</a></td>
                        <td><a href="{{route('default_favs.show',$result)}}">{{isset($result->role)?$result->role->display_name:''}}</a></td>
                        {{-- <td>Default</td> --}}
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <td>
                            <div class="d-flex">
                            <a href="{{route('default_favs.edit',$result)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['default_favs.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No default favourites is available.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $favourites->firstItem() }} - {{ $favourites->lastItem() }} of {{ $favourites->total() }}
                        </td>
                        <td>
                            {{ $favourites->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
