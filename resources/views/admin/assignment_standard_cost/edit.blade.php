@extends('admin.adminlte.default')

@section('title') Edit Master Assignment Standard Cost @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('admin.assignmentstandardcost.update',$standard_cost), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">{{ Form::text('description', $standard_cost->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Standard Cost Rate <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{-- {{Form::text('standard_cost_rate',$standard_cost->standard_cost_rate,['class'=>'form-control form-control-sm'. ($errors->has('standard_cost_rate') ? ' is-invalid' : '')])}} --}}
                            {{ Form::number('standard_cost_rate', $standard_cost->standard_cost_rate, ['min' => 0, 'class' => 'form-control form-control-sm' . ($errors->has('standard_cost_rate') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('standard_cost_rate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Min Monthly Cost to Company <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">
                            {{-- {{ Form::text('min_rate', $standard_cost->min_rate, ['class'=>'form-control form-control-sm'. ($errors->has('min_rate') ? ' is-invalid' : '')]) }} --}}
                            {{ Form::number('min_rate', $standard_cost->min_rate, ['min' => 0, 'class' => 'form-control form-control-sm' . ($errors->has('min_rate') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('min_rate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Max Monthly Cost to Company <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">
                            {{-- {{ Form::text('max_rate',$standard_cost->max_rate, ['class'=>'form-control form-control-sm'. ($errors->has('max_rate') ? ' is-invalid' : '')]) }} --}}
                            {{ Form::number('max_rate', $standard_cost->max_rate, ['min' => 0, 'class' => 'form-control form-control-sm' . ($errors->has('max_rate') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('max_rate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td  colspan="3">{{Form::select('status',$status,$standard_cost->status_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->description.'",' !!} @endforeach];

        $( "#description" ).autocomplete({
            source: autocomplete_elements
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var inputElement = document.querySelector('input[name="standard_cost_rate"]');

            inputElement.addEventListener('input', function () {
                // Remove leading minus sign if present
                this.value = this.value.replace(/^(-)/, '');

                // Ensure the value is a valid number
                if (isNaN(this.value) || this.value === '') {
                    this.setCustomValidity('Please enter a non-negative number.');
                } else {
                    this.setCustomValidity('');
                }
            });
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var inputElement = document.querySelector('input[name="min_rate"]');

            inputElement.addEventListener('input', function () {
                // Remove leading minus sign if present
                this.value = this.value.replace(/^(-)/, '');

                // Ensure the value is a valid number
                if (isNaN(this.value) || this.value === '') {
                    this.setCustomValidity('Please enter a valid number.');
                } else {
                    this.setCustomValidity('');
                }
            });
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var inputElement = document.querySelector('input[name="max_rate"]');

            inputElement.addEventListener('input', function () {
                // Remove leading minus sign if present
                this.value = this.value.replace(/^(-)/, '');

                // Ensure the value is a valid number
                if (isNaN(this.value) || this.value === '' || parseFloat(this.value) < 0) {
                    this.setCustomValidity('Please enter a non-negative number.');
                } else {
                    this.setCustomValidity('');
                }
            });
        });
    </script>
@endsection