@extends('admin.adminlte.default')

@section('title') View Master Default Dashboard @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.customisedashboard.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.customisedashboard.edit', $dynamic_dashboard)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$dynamic_dashboard->description}}</td>
                        <th>Role</th>
                        <td>
                            <ul class="list-group-sm">
                                @forelse($roles as $role)
                                    <li class="list-group-item">{{$role}}</li>
                                @empty
                                @endforelse
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$dynamic_dashboard->status->description}}</td>
                        <th>Top Component</th>
                        <td>{{($dynamic_dashboard->top_component == 1)?"Yes":"No"}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection