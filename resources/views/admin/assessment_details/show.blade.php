@extends('admin.adminlte.default')

@section('title') View Master Assessment Details @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.assessmentmasterdetails.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.assessmentmasterdetails.edit', $assessment_details)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Assessment Master</th>
                        <td>{{$assessment_details->assess_master->description}}</td>
                        <th>Assessment Group</th>
                        <td>{{$assessment_details->assessment_group}}</td>
                    </tr>
                    <tr>
                        <th>Assessment Measure</th>
                        <td>{{$assessment_details->assessment_measure}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>
        </div>
    </div>
@endsection