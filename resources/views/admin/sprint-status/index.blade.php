@extends('admin.adminlte.default')

@section('title') Master Data - Sprint Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('admin.sprintstatus.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Sprint Status</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <select class="form-control w-100" name="status_filter" id="status_filter" style="width: 120px;">
                            <option value="0">All</option>
                            <option selected value="1">Active</option>
                            <option value="2">Suspended</option>
                            <option value="6">Closed</option>
                        </select>
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('admin.sprintstatus.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Description</th>
                    <th class="last pr-3">Status</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sprint_status as $result)
                    <tr>
                        <td><a href="{{route('admin.sprintstatus.show',$result)}}">{{$result->description}}</a></td>
                        <td>{{$result->status?->description}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('admin.sprintstatus.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['admin.sprintstatus.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No sprint status entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function() {
            $('#status_filter').on('change', function() {
                this.form.submit();
            });
        });
    </script>

@endsection