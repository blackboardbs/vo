@extends('admin.adminlte.default')

@section('title') Master Data - Resource Level @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('admin.resourcelevel.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Employee Level</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form id="searchform" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <select class="form-control" name="status_filter" id="status_filter" style="width: 120px;">
                            <option value="0">All</option>
                            <option selected value="1">Active</option>
                            <option value="2">Suspended</option>
                            <option value="6">Closed</option>
                        </select>
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('admin.resourcelevel.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('description','Description')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($emp_level as $result)
                    <tr>
                        <td><a href="{{route('admin.resourcelevel.show',$result)}}">{{$result->description}}</a></td>
                        <td>{{$result->statusd?->description}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('admin.resourcelevel.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['admin.resourcelevel.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data employee level entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $emp_level->firstItem() }} - {{ $emp_level->lastItem() }} of {{ $emp_level->total() }}
                        </td>
                        <td>
                            {{ $emp_level->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function() {
            $('#status_filter').on('change', function() {
                this.form.submit();
            });
        });
    </script>

@endsection