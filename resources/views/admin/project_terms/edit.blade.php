@extends('admin.adminlte.default')

@section('title') Edit Master Data Project Type @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('admin.projectterms.update',$project_terms), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Terms version <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::text('terms_version', $project_terms->terms_version, ['class'=>'form-control form-control-sm'. ($errors->has('terms_version') ? ' is-invalid' : ''), 'id' => 'terms_version', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('terms_version') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Start Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::text('start_date', $project_terms->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>End Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::text('end_date', $project_terms->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('end_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Travel <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_travel', $project_terms->exp_travel, ['class'=>'form-control form-control-sm'. ($errors->has('exp_travel') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_travel') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Parking <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_parking', $project_terms->exp_parking, ['class'=>'form-control form-control-sm'. ($errors->has('exp_parking') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_parking') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Car Rental <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('car_rental', $project_terms->exp_car_rental, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('car_rental') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('car_rental') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Flights <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_flights', $project_terms->exp_flights, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('exp_flights') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_flights') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Other <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_other', $project_terms->exp_other, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('exp_other') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_other') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Accommodation <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_accommodation', $project_terms->exp_accommodation, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('exp_accommodation') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_accommodation') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Out Of Town <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_out_of_town', $project_terms->exp_out_of_town, ['class'=>'form-control form-control-sm'. ($errors->has('exp_out_of_town') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_out_of_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Toll <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_toll', $project_terms->exp_toll, ['class'=>'form-control form-control-sm'. ($errors->has('exp_toll') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_toll') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Data <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_data', $project_terms->exp_data, ['class'=>'form-control form-control-sm'. ($errors->has('exp_data') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_data') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Hours Of work <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{ Form::textarea('exp_hours_of_work', $project_terms->exp_hours_of_work, ['class'=>'form-control form-control-sm'. ($errors->has('exp_hours_of_work') ? ' is-invalid' : ''), 'rows' => 3]) }}
                            @foreach($errors->get('exp_hours_of_work') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-css')
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $(function () {


            let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->terms_version.'",' !!} @endforeach];

            $( "#terms_version" ).autocomplete({
                source: autocomplete_elements
            });
        });
    </script>
@endsection
