@extends('admin.adminlte.default')

@section('title') View Master Project Status @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('admin.projectstatus.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.projectstatus.edit', $project_status[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Project Status</a>
            </div>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($project_status as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Status</th>
                        <td>{{$result->status?->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection