@extends('admin.adminlte.default')

@section('title') View Master Template Styles @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.templatestyles.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.templatestyles.edit', $style)}}" class="btn btn-success float-rightml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr class="bg-dark">
                    <th colspan="4">Timesheet Template</th>
                </tr>
                <tr>
                    <th>Template Name</th>
                    <td>{{$style->name}}</td>
                    <th>Account Type</th>
                    <td>{{$style->template()}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$style->status?->description}}</td>
                    <th>Digital Signature Consultant</th>
                    <td>{{$style->is_signed ? "Yes" : "No"}}</td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Logo</th>
                </tr>
                <tr>
                    <th>Logo Left</th>
                    <td>
                        @if(isset($style->left_logo) && !intval($style->left_logo))
                            <img src="/storage/template/{{$style->left_logo}}" alt="">
                        @else
                            {{$style->left_logo?$style->logo($style->left_logo).' Logo':null}}
                        @endif
                    </td>
                    <th>Logo Center</th>
                    <td>
                        @if(isset($style->center_logo) && !intval($style->center_logo))
                            <img src="/storage/template/{{$style->center_logo}}" alt="">
                        @else
                            {{$style->center_logo?$style->logo($style->center_logo).' Logo':null}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Logo Right</th>
                    <td>
                        @if(isset($style->right_logo) && !intval($style->right_logo))
                            <img src="/storage/template/{{$style->right_logo}}" alt="">
                        @else
                            {{$style->right_logo?$style->logo($style->right_logo).' Logo':null}}
                        @endif
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark {{($style->template_id == 4)?'d-none':''}}">
                    <th colspan="6">Header Labels</th>
                </tr>
                <tr {{($style->template_id == 4)?'class="d-none"':''}}>
                    <th>Resource Label</th>
                    <td>
                        {{$style->resource_label}}
                    </td>
                    <th>Project Name Label</th>
                    <td>{{$style->project_label}}</td>
                </tr>
                <tr {{($style->template_id == 4)?'class="d-none"':''}}>
                    <th>Start Date Label</th>
                    <td>
                        {{$style->start_date_label}}
                    </td>
                    <th>Project Name Label</th>
                    <td>{{$style->end_date_label}}
                        </td>
                </tr>
                <tr {{($style->template_id == 4)?'class="d-none"':''}}>
                    <th>PO Number Label</th>
                    <td>
                        {{$style->po_number_label}}
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr {{($style->template_id == 4)?'class="d-none"':''}}>
                    <th>Include Billing Period</th>
                    <td>{{$style->is_billing_period_included?"Yes":"No"}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Text Comment</th>
                    <td>
                        {!! $style->comment !!}
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Font and Colors</th>
                </tr>
                <tr>
                    <th>Header Font Style</th>
                    <td>{{$style->header_style === "em" ? "Italic": $style->header_style}}</td>
                    <th>Header Font Color</th>
                    <td style="background-color: {{$style->header_color??'#fff'}}"></td>
                </tr>
                <tr>
                    <th>Header Background Color</th>
                    <td style="background-color: {{$style->header_background??'#fff'}}"></td>
                    <th>Detail Font Style</th>
                    <td>{{$style->detail_style === "em" ? "Italic": $style->detail_style}}</td>
                </tr>
                <tr>
                    <th>Detail Font Color</th>
                    <td style="background-color: {{$style->detail_color??'#fff'}}"></td>
                    <th>Detail Background Color</th>
                    <td style="background-color: {{$style->detail_background??'#fff'}}"></td>
                </tr>
                <tr>
                    <th>Footer Font Style</th>
                    <td>{{$style->footer_style === "em" ? "Italic": $style->footer_style}}</td>
                    <th>Footer Font Color</th>
                    <td style="background-color: {{$style->footer_color??'#fff'}}"></td>
                </tr>
                <tr>
                    <th>Footer Background Color</th>
                    <td style="background-color: {{$style->footer_background??'#fff'}}"></td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection