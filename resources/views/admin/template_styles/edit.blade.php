@extends('admin.adminlte.default')

@section('title') Edit Master Template Styles @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('admin.templatestyles.update', $style), 'method' => 'patch','class'=>'mt-3', 'files' => true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr class="bg-dark">
                    <th colspan="6">Timesheet Template</th>
                </tr>
                <tr>
                    <th colspan="3">Template Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{ Form::text('name', $style->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'description', 'autocomplete' => 'off']) }}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Based on Template <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('template_id',$template_dropdown,$style->template_id,['class'=>'form-control form-control-sm '. ($errors->has('template_id') ? ' is-invalid' : ''), 'id'=>'template-id'])}}
                        @foreach($errors->get('template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th colspan="3">Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$style->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Digital Signature Consultant </th>
                    <td>{{Form::radio('is_signed', 1, $style->is_signed?true:false)}} Yes &nbsp; &nbsp; &nbsp; {{Form::radio('is_signed', 0, !$style->is_signed?true:false)}} No</td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="6">Logo</th>
                </tr>
                <tr>
                    <th colspan="3">Logo Left</th>
                    <td>
                        {{Form::select('left_logo',$logo_dropdown,is_int((int) $style->left_logo)?$style->left_logo:null,['class'=>'form-control form-control-sm '. ($errors->has('left_logo_file') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('left_logo') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>OR Upload Custom Logo</th>
                    <td>{{Form::file('left_logo_file',null,['class'=>'form-control form-control-sm '. ($errors->has('left_logo_file') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('left_logo_file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        <br>
                        @if(!intval($style->left_logo))
                            <img src="/storage/template/{{$style->left_logo}}" class="img-fluid" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Logo Center</th>
                    <td>
                        {{Form::select('center_logo',$logo_dropdown,is_int((int) $style->center_logo)?$style->center_logo:null,['class'=>'form-control form-control-sm '. ($errors->has('center_logo') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('center_logo') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>OR Upload Custom Logo</th>
                    <td>{{Form::file('center_logo_file',null,['class'=>'form-control form-control-sm '. ($errors->has('center_logo_file') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('center_logo_file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        <br>
                        @if(!intval($style->center_logo))
                            <img src="/storage/template/{{$style->center_logo}}" class="img-fluid" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Logo Right</th>
                    <td>
                        {{Form::select('right_logo',$logo_dropdown,is_int((int) $style->right_logo)?$style->right_logo:null,['class'=>'form-control form-control-sm '. ($errors->has('right_logo') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('right_logo') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>OR Upload Custom Logo</th>
                    <td>{{Form::file('right_logo_file',old("right_logo_file"),['class'=>'form-control form-control-sm '. ($errors->has('right_logo_file') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('right_logo_file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        <br>
                        @if(!intval($style->right_logo))
                            <img src="/storage/template/{{$style->right_logo}}" class="img-fluid" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Vertical Alignment</th>
                    <td>
                        {{Form::select('logos_vertical_alignment',['top' => 'Top', 'middle' => 'Middle', 'bottom' => 'Bottom'],$style->logos_vertical_alignment,['class'=>'form-control form-control-sm '. ($errors->has('logos_vertical_alignment') ? ' is-invalid' : ''), 'placeholder' => "Select Alignment"])}}
                        @foreach($errors->get('logos_vertical_alignment') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark header-info">
                    <th colspan="6">Header Labels</th>
                </tr>
                <tr class="header-info">
                    <th colspan="3">Resource Label</th>
                    <td>
                        {{Form::text('resource_label',$style->resource_label,['class'=>'form-control form-control-sm'. ($errors->has('resource_label') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_label') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project Name Label</th>
                    <td>{{Form::text('project_label',$style->project_label,['class'=>'form-control form-control-sm'. ($errors->has('project_label') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project_label') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr class="header-info">
                    <th colspan="3">Start Date Label</th>
                    <td>
                        {{Form::text('start_date_label',$style->start_date_label,['class'=>'form-control form-control-sm'. ($errors->has('start_date_label') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('start_date_label') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date Label</th>
                    <td>{{Form::text('end_date_label',$style->end_date_label,['class'=>'form-control form-control-sm'. ($errors->has('end_date_label') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('end_date_label') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr class="header-info">
                    <th colspan="3">PO Number Label</th>
                    <td>
                        {{Form::text('po_number_label',$style->po_number_label,['class'=>'form-control form-control-sm '. ($errors->has('po_number_label') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('po_number_label') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="header-info">
                    <th colspan="3">Include Billing Period</th>
                    <td>
                        {{Form::select('is_billing_period_included',[1 => 'Yes', 0 => 'No'], $style->is_billing_period_included,['class'=>'form-control form-control-sm '. ($errors->has('is_billing_period_included') ? ' is-invalid' : ''), 'placeholder' => 'Include Billing Period'])}}
                        @foreach($errors->get('is_billing_period_included') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="6">Text Comment</th>
                </tr>
                <tr>
                    <td colspan="6">
                        {{Form::textarea('comment', $style->comment, ['id' => 'comment'])}}
                        @foreach($errors->get('comment') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="6">Font and Color</th>
                </tr>
                <tr>
                    <th colspan="3">Header Font Color</th>
                    <td>
                        {{Form::text('header_color', $style->header_color, ['class' => 'form-control form-control-sm'. ($errors->has('header_color') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('header_color') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Header Background Color</th>
                    <td>
                        {{Form::text('header_background', $style->header_background, ['class' => 'form-control form-control-sm'. ($errors->has('header_background') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('header_background') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Detail Font Color</th>
                    <td>
                        {{Form::text('detail_color', $style->detail_color, ['class' => 'form-control form-control-sm'. ($errors->has('detail_color') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('detail_color') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Detail Background Color</th>
                    <td>
                        {{Form::text('detail_background', $style->detail_background, ['class' => 'form-control form-control-sm'. ($errors->has('detail_background') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('detail_background') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Footer Font Color</th>
                    <td>
                        {{Form::text('footer_color', $style->footer_color, ['class' => 'form-control form-control-sm'. ($errors->has('detail_color') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('footer_color') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Footer Background Color</th>
                    <td>
                        {{Form::text('footer_background', $style->footer_background, ['class' => 'form-control form-control-sm'. ($errors->has('footer_background') ? ' is-invalid' : ''), "data-jscolor" => "{}"])}}
                        @foreach($errors->get('footer_background') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script src="/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="{{asset("js/jscolor.min.js")}}"></script>
    <script>
        $(function () {


            tinyMCE.init({
                selector : "#comment",
                plugins: "textcolor",
                toolbar: "toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent' | forecolor"
            });
        } );

        $(function (){
            if ($("#template-id").val() == 4){
                $(".header-info").fadeOut();
            }
            $("#template-id").on('change', () =>{
                if ($("#template-id").val() == 4){
                    $(".header-info").fadeOut();
                }else {
                    $(".header-info").fadeIn();
                }
            })
        })
    </script>
@endsection