@extends('admin.adminlte.default')

@section('title') View Master Payment Base @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <a href="{{route('admin.paymentbase.index')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
                <a href="{{route('admin.paymentbase.edit', $payment_base)}}" class="btn btn-success float-right ml-1">Edit</a>
         </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Description</th>
                    <td>{{$payment_base->description}}</td>
                    <th>Status</th>
                    <td>{{$payment_base->status?->description??null}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection