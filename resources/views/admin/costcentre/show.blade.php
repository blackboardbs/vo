@extends('admin.adminlte.default')

@section('title') View Master Cost Center @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="d-flex justify-content-end">
            <a href="{{route('admin.costcenter.index')}}" class="btn btn-dark"><i class="fa fa-caret-left"></i> Back</a>
            <a href="{{route('admin.costcenter.edit', $cost[0])}}" class="btn btn-success ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    {{-- <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($cost as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Description</th>
                        <td>{{$result->description}}</td>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div> --}}
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cost as $result)
                        <tr>
                            <td>{{$result->description}}</td>
                            <td>{{$result->statusd->description}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection