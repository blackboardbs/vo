<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulteaze Admin Login</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
<div class="flex items-center justify-center min-h-screen bg-gray-100">
    <div class="w-full max-w-md px-8 py-10 bg-white rounded-lg shadow-md">
        <div class="mb-4 text-center">
            <h1 class="text-3xl font-bold text-gray-800"><img src="{{global_asset('assets/consulteaze_logo.png')}}"></h1>

        </div>
        <form action="{{route('admin.login')}}" method="POST">
            @csrf
            <div class="mb-6">
                <label for="email" class="text-sm text-gray-600 block mb-2">Email</label>
                <input type="text" id="email" name="email" class="w-full px-3 py-2 rounded-lg border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:ring-1">
            </div>
            <div class="mb-6">
                <label for="password" class="text-sm text-gray-600 block mb-2">Password</label>
                <input type="password" id="password" name="password" class="w-full px-3 py-2 rounded-lg border border-gray-300 focus:outline-none focus:ring-indigo-500 focus:ring-1">
            </div>
            @error('email')
                <p class="text-red-500 pb-3">{{$message}}</p>
            @enderror
            <!--<div class="flex justify-between items-center mb-6">
                <div class="flex items-center">
                    <input type="checkbox" id="remember-me" name="remember-me" class="w-4 h-4 text-indigo-600 rounded border-gray-300 focus:ring-indigo-500 focus:ring-1">
                    <label for="remember-me" class="text-sm text-gray-600 ml-2">Remember Me</label>
                </div>
                <a href="#" class="text-sm text-indigo-600 hover:underline">Forgot Password?</a>
            </div>-->
            <button type="submit" class="w-full px-3 py-2 rounded-lg bg-indigo-500 text-white font-bold shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-opacity-50">Login</button>
        </form>
    </div>
</div>
</body>
</html>