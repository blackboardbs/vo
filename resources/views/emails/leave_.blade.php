<!DOCTYPE html>
<html>
<head>
    <title>Leave Application.</title>
</head>
<body>
<p>Dear {{$teamManager->first_name}}</p>
<p>{{isset($leave->resource)?$leave->resource->first_name.' '.$leave->resource->last_name:''}} Applied for {{$leave->no_of_days}} Days {{isset($leave->leave_type)?$leave->leave_type->description:''}} leave on {{substr($leave->created_at,0,10)}}. </p>
<p>Start Date: {{$leave->date_from}}
   End Date: {{$leave->date_to}}
</p>
<p>Ongoing projects during leave period:</p>
<ol>
    @forelse($projects as $project)
        <li>{{isset($project->project)?$project->project->name:''}}</li>
        @empty
    @endforelse
</ol>
<p>Link to Leave record: <a href="http://{{tenant()->domains()?->first()?->domain}}/leave/{{$leave}}">{{isset($leave->resource)?$leave->resource->first_name.' '.$leave->resource->last_name:''}}</a></p>
<p>Visit the <a href="{{$helpPortal}}"><strong>Help Portal</strong></a> for more details on how to update this record.</p>
</body>
</html>