@component('mail::message')
# Dear {{$onboarding->user->appointmentManager->first_name[0]??null}}. {{$onboarding->user->appointmentManager->last_name}}

The body of your message.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
