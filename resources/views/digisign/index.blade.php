@extends('adminlte.default')
@section('title') Digital Signature @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">

        <form id="filter_form" class="form-inline mt-3" action="{{route('digisign.index')}}" method="get" autocomplete="off">
            <input type="hidden" name="r" value="{{isset($_GET['r']) ? $_GET['r'] : '15'}}" />
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('type',$document_type_drop_down,old('type'),['class'=>'form-control search', 'style'=>'width: 100%;'])}}
                <span>Document Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('resource',$resource_drop_down,old('resource'),['class'=>'form-control search', 'style'=>'width: 100%;'])}}
                <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('status',$digisign_status_drop_down,old('status'),['class'=>'form-control search', 'style'=>'width: 100%;'])}}
                <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::text('from',old('from'),['class'=>'datepicker form-control search', 'style'=>'width: 100%;'])}}
                <span>From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::text('to',old('to'),['class'=>'datepicker form-control search', 'style'=>'width: 100%;'])}}
                <span>To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('digisign.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name', 'Name')</th>
                    <th>@sortablelink('type.name', 'Document Type')</th>
                    <th>@sortablelink('owner.first_name', 'Resource')</th>
                    <th>@sortablelink('user.first_name', 'User')</th>
                    <th>@sortablelink('created_at', 'Date')</th>
                    {{--<th>@sortablelink('digisignstatus.name', 'Status')</th>--}}
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($documents as $document)
                    <tr>
                        <td>{{$document->name}}</td>
                        <td>{{isset($document->type->name)?$document->type->name:''}}</td>
                        <td>{{isset($document->owner->first_name)?$document->owner->first_name:''}} {{isset($document->owner->last_name)?$document->owner->last_name:''}}</td>
                        <td>{{isset($document->user->first_name)?$document->user->first_name:''}} {{isset($document->user->last_name)?$document->user->last_name:''}}</td>
                        <td>{{$document->created_at}}</td>
                        {{--<td>{{isset($document->digisignstatus->name)?$document->digisignstatus->name:''}}</td>--}}
                        <td>
                        @php
                                $url = 'tmp/'.$document->file
                            @endphp
                            @if(file_exists(($url)))
                                @if(!$document->digisign_status_id == 6)
                                    <span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Download File">
                                        <a href="/tmp/{{$document->file}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                                    </span>
                                    @if($document->digisign_status_id == 1 || $document->digisign_status_id == 2 || $document->digisign_status_id == 3 || $document->digisign_status_id == 7)
                                    <a href="{{route('document.pdfviewer', $document->id, $document->owner_id)}}" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-thumbs-up"></i> Sign</a>
                                    @endif
                                @else
                                    <span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Download File">
                                        <a href="/tmp/{{$document->file}}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                                    </span>
                                @endif
                            @else
                            <span class="text-capitalize" data-toggle="tooltip" data-placement="right" title="File does not exist"><a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm disabled"><i class="fa fa-download"></i></a></span>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No documents match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $documents->firstItem() }} - {{ $documents->lastItem() }} of {{ $documents->total() }}
                        </td>
                        <td>
                            {{ $documents->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('select').change(function () {
                $('#filter_form').submit();
            });
        });

        function clearFilters(){
            window.location = "/digisign";
        }
    </script>
@endsection
@section('extra-css')
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        li.active {
            border-left: 1px solid #ddd !important;
            border-top: 1px solid #ddd !important;
            border-right: 1px solid #ddd !important;
        }
    </style>
@endsection