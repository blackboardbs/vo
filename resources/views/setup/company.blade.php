@extends('adminlte.default')
@section('title') Web Office Setup @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <a id="modal_btn" style="display: none;" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#configuration_modal"><i class="fas fa-paper-plane"></i> Open Modal</a>
        <!-- Modal -->
        <div class="modal fade" id="configuration_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="configuration_label">Step 2 - Company</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{Form::open(['url' => route('setup.storecompany', $company), 'method' => 'post','class'=>'mt-3', 'id'=>'store_company_frm','files' => true])}}
                        <input type="hidden" name="is_demo" id="id_demo" value="0" />
                        <div class='row'>
                            <div class="col-md-12">
                                <p>
                                    Welcome to the Blackboard Web Office
                                </p>
                                <p>
                                    The Config Wizard will assist you to setup your Office in 4 easy steps. The setup will take about 5 minutes and will have help content videos to explain each step.
                                </p>
                                <p>
                                    You can watch the <a href="" target="_blank">introduction video</a> or click on the <a href="" target="_blank">help</a> icon to get started.
                                </p>
                                <p>
                                    Step 2 - Company Information
                                </p>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-12'>
                                <table class="table table-bordered table-sm mt-3">
                                    <tbody>
                                        <tr>
                                            <th>Company Name:</th>
                                            <td>
                                                {{Form::text('company_name', $company->company_name,['class'=>'form-control form-control-sm'. ($errors->has('company_name') ? ' is-invalid' : ''),'placeholder'=>'Company Name'])}}
                                                @foreach($errors->get('company_name') as $error)
                                                    <div class="invalid-feedback">
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Company Email:</th>
                                            <td>
                                                {{Form::text('email', $company->email,['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Company Email'])}}
                                                @foreach($errors->get('email') as $error)
                                                    <div class="invalid-feedback">
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Company First Name:</th>
                                            <td>
                                                {{Form::text('contact_firstname', $company->contact_firstname,['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Company First Name'])}}
                                                @foreach($errors->get('contact_firstname') as $error)
                                                    <div class="invalid-feedback">
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Company Last Name:</th>
                                            <td>
                                                {{Form::text('contact_lastname', $company->contact_lastname,['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Company Last Name'])}}
                                                @foreach($errors->get('contact_lastname') as $error)
                                                    <div class="invalid-feedback">
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Company Phone Number:</th>
                                            <td>
                                                {{Form::text('phone', $company->phone,['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Company Phone Number'])}}
                                                @foreach($errors->get('phone') as $error)
                                                    <div class="invalid-feedback">
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Company Logo:</th>
                                            <td>
                                                <div class="img-wrapper">
                                                    <img src="{{route('company_avatar',['q'=>Auth::user()->avatar])}}" id="blackboard-preview-large" />
                                                </div>
                                                {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Company Logo','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                                                @foreach($errors->get('avatar') as $error)
                                                    <div class="invalid-feedback">
                                                        {{ $error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                                <a onclick="skipConfiguration()" class="btn btn-sm bg-dark d-print-none">Skip Config <i class="fas fa-angle-double-right"></i></a> <a onclick="loadDemoData()" class="btn btn-sm bg-dark d-print-none">Load Demo Data <i class="fas fa-paper-plane"></i></a> <button type="submit" class="btn btn-sm bg-dark d-print-none">Next <i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    {{--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{{asset('slick-1.8.1/slick/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {

            $(".variable").slick({
                dots: true,
                infinite: true,
                variableWidth: true
            });

            document.getElementById("modal_btn").click();

            $('.slick-prev').click(function(){
                getModule();
            });

            $('.slick-next').click(function(){
                getModule();
            });

            $('.slick-slide').click(function(){
                var cliked_element = $(this).attr("industry-id");
                cliked_element = cliked_element - 1;
                var div_number = '0'+cliked_element;
                document.getElementById("slick-slide-control"+div_number).click();
                getModule();
            });

        });

        function loadDemoData(){
            var confirm_flag = confirm("Please not that demo data can not be used on a production environment. Are you sure you want to load demo data.");
            if(confirm_flag) {
                $('input[name="is_demo"]').val(1); //Using id does not work, weird
                $('#store_company_frm').submit();
            }
        }

        function skipConfiguration(){
            confirm("Are you sure you want to skip configuration");
            window.location = "/";
        }

        function getModule(){

            var active_element = document.getElementsByClassName("slick-active")[0].getAttribute("industry-id");

            let industry_id = active_element;
            axios.get('/setup/getmodules/'+industry_id, {
                industry_id: industry_id
            })
                .then(function (response) {

                    $('#modules_div').empty();

                    var counter = 1;

                    var module_html = '<div class="row">';

                    $.each(response.data.modules, function (key, value) {

                        var checked = '';
                        if(value.selected == 1){
                            checked = 'checked';
                        }

                        var selectable = '';
                        if(value.selectable == 0){
                            selectable = 'disabled';
                        }

                        module_html += '<div class="col-md-4">';
                        module_html += '   <div class="checkbox">';
                        module_html += '       <label><input id="industry_modules_'+value.module_id+'" name="industry_modules[]" type="checkbox" value="'+value.module_id+'" '+checked+' '+selectable+'> '+value.module.display_name+'</label>';
                        module_html += '   </div>';
                        module_html += '</div>';

                        counter++;

                        if((counter % 3 == 1) && (counter != 1)) {
                            module_html += '</div>';
                            module_html += '<div class="row">';
                        }
                    });

                    module_html += '</div>';

                    $('#modules_div').html(module_html);
                    $('#industry_id').val(industry_id);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    </script>
@endsection
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick-theme.css')}}"/>
    <style>

        .slick-list{
            width: 85%;
            margin-left: 50px;
        }

        .slick-arrow{
            background-color: #0c0c0c;
            border-radius: 50%;
        }

        .slick-prev {
            left: 0px;
        }

        .slick-next {
            right: 0px;
        }

        .slick-prev, .slick-next {
            width: 40px;
            height: 40px;
            radius: 4px !important;
        }

        .slick-slide{
            width: 250px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .slick-prev:hover,
        .slick-prev:focus,
        .slick-next:hover,
        .slick-next:focus
        {
            color: #fff;
            outline: none;
            background: #8e908c;
        }

        .slick-active{
            background-color: 	#800080;
            border-radius: 4px;
            color: #ffffff;
            padding: 10px;
        }

    </style>
@endsection