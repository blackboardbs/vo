@extends('adminlte.default')
@section('title') Web Office Setup @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <a id="modal_btn" style="display: none;" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#configuration_modal"><i class="fas fa-paper-plane"></i> Open Modal</a>
        <!-- Modal -->
        <div class="modal fade" id="configuration_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="configuration_label">Step 1 - Configuration</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{Form::open(['url' => route('setup.storestepone'), 'method' => 'post','class'=>'mt-3', 'id'=>'store_step_one_frm','files' => true])}}
                        <div class='row'>
                            <div class="col-md-12">
                                <p>
                                    Welcome to the Blackboard Web Office
                                </p>
                                <p>
                                    The Config Wizard will assist you to setup your Office in 4 easy steps. The setup will take about 5 minutes and will have help content videos to explain each step.
                                </p>
                                <p>
                                    You can watch the <a href="" target="_blank">introduction video</a> or click on the <a href="" target="_blank">help</a> icon to get started.
                                </p>
                                <p>
                                    Step 1 - Select an Industry
                                </p>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-12'>
                                <input type="hidden" id="industry_id" name="industry_id" value="1"/>
                                <input type="hidden" id="unselectable_ids" name="unselectable_ids" value=""/>
                                <section class="variable slider">
                                    <div industry-id="1" style="text-align: justify;">
                                        <h3>Consulting - Full</h3>
                                        <p>This is a type of business that provides consulting services at an hourly rate to customers against a project with pre-determined terms and conditions</p>
                                        <p>Resources are required to submit timesheets and record leave. Resources could be permanantly employed or 3rd party contractors. All functionality to manage Customers, Vendors, Resources, Projects and Recruitment is included.</p>
                                    </div>
                                    <div industry-id="2" style="text-align: justify;">
                                        <h3>Consulting - Basic</h3><p>This is a type of business that provides consulting services at an hourly rate to customers against a project with pre-determined terms and conditions</p>
                                        <p>Resources are required to submit timesheets, but only basic resourcing functionality required.</p>
                                        <p>Select this option to start with the basics and expand later.</p>
                                    </div>
                                    <div industry-id="3" style="text-align: justify;">
                                        <h3>Resource Management</h3>
                                        <p>This is for a recruitment company that requires Scouting and CV management. The CV template generator will be a big help to convert resources information into a pre defined or custom template to provide to clients.
                                    </div>
                                    <div industry-id="4" style="text-align: justify;">
                                        <h3>Recruitment</h3>
                                        <p>This is for a recruitment company that requires Scouting and CV management. The CV template generator will be a big help to convert resources information into a pre defined or custom template to provide to clients.
                                    </div>
                                    <div industry-id="5" style="text-align: justify;">
                                        <h3>Professional Services</h3>
                                        <p>This is for a recruitment company that requires Scouting and CV management. The CV template generator will be a big help to convert resources information into a pre defined or custom template to provide to clients.
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div id="modules_div">
                            <div class="row">
                                @php
                                    $counter = 1;
                                    $unselectable_ids = '';
                                @endphp
                                @foreach($modules as $module)

                                    @if($module->selectable == 0)
                                        @if($module->selected == 1)
                                            @php
                                                if(isset($module->module->id)){
                                                    $unselectable_ids .= ','.$module->module->id;
                                                }
                                            @endphp
                                        @endif
                                    @endif

                                    <div class="col-md-4">
                                        <div class="checkbox">
                                            <label><input id="industry_modules_{{isset($module->module->id)?$module->module->id:''}}" name="industry_modules[]" type="checkbox" value="{{isset($module->module->id)?$module->module->id:''}}" {{$module->selected == 1 ? 'checked' : ''}} {{$module->selectable == 0 ? 'disabled' : ''}}> {{isset($module->module->display_name)?$module->module->display_name:''}}</label>
                                        </div>
                                    </div>
                                    @php
                                        $counter++;
                                    @endphp
                                    @if(($counter % 3 == 1) && ($counter != 1))
                                        </div>
                                        <div class="row">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                 &nbsp;
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                                <a onclick="skipConfiguration()" class="btn btn-sm bg-dark d-print-none">Skip Config <i class="fas fa-angle-double-right"></i></a> <a onclick="loadDemoData()" class="btn btn-sm bg-dark d-print-none">Load Demo Data <i class="fas fa-paper-plane"></i></a> <button type="submit" class="btn btn-sm bg-dark d-print-none">Next <i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    {{--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{{asset('slick-1.8.1/slick/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {

            $('#unselectable_ids').val('<?php echo $unselectable_ids; ?>');

            $(".variable").slick({
                dots: true,
                infinite: true,
                variableWidth: true
            });

            document.getElementById("modal_btn").click();

            $('.slick-prev').click(function(){
                getModule();
            });

            $('.slick-next').click(function(){
                getModule();
            });

            $('.slick-slide').click(function(){
                var cliked_element = $(this).attr("industry-id");
                cliked_element = cliked_element - 1;
                var div_number = '0'+cliked_element;
                document.getElementById("slick-slide-control"+div_number).click();
                getModule();
            });

        });

        function loadDemoData(){
            var confirm_flag = confirm("Please not that demo data can not be used on a production environment. Are you sure you want to load demo data.");
            if(confirm_flag) {
                $('#store_step_one_frm').submit();
            }
        }

        function skipConfiguration(){
            confirm("Are you sure you want to skip configuration");
            window.location = "/";
        }

        function getModule(){

            var active_element = document.getElementsByClassName("slick-active")[0].getAttribute("industry-id");

            let industry_id = active_element;
            axios.get('/setup/getmodules/'+industry_id, {
                industry_id: industry_id
            })
            .then(function (response) {

                $('#modules_div').empty();

                var counter = 1;

                var module_html = '<div class="row">';

                $('#unselectable_ids').val('');

                $.each(response.data.modules, function (key, value) {

                    var checked = '';
                    if(value.selected == 1){
                        checked = 'checked';
                    }

                    var selectable = '';
                    if(value.selectable == 0){
                        selectable = 'disabled';
                        if(value.selected == 1){
                            $('#unselectable_ids').val($('#unselectable_ids').val()+','+value.module_id);
                        }
                    }

                    module_html += '<div class="col-md-4">';
                    module_html += '   <div class="checkbox">';
                    module_html += '       <label><input id="industry_modules_'+value.module_id+'" name="industry_modules[]" type="checkbox" value="'+value.module_id+'" '+checked+' '+selectable+'> '+value.module.display_name+'</label>';
                    module_html += '   </div>';
                    module_html += '</div>';

                    counter++;

                    if((counter % 3 == 1) && (counter != 1)) {
                        module_html += '</div>';
                        module_html += '<div class="row">';
                    }
                });

                module_html += '</div>';

                $('#modules_div').html(module_html);
                $('#industry_id').val(industry_id);
            })
            .catch(function (error) {
                console.log(error);
            });
        }

    </script>
@endsection
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick-theme.css')}}"/>
    <style>

        .slick-list{
            width: 85%;
            margin-left: 50px;
        }

        .slick-arrow{
            background-color: #0c0c0c;
            border-radius: 50%;
        }

        .slick-prev {
            left: 0px;
        }

        .slick-next {
            right: 0px;
        }

        .slick-prev, .slick-next {
            width: 40px;
            height: 40px;
            radius: 4px !important;
        }

        .slick-slide{
            width: 250px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .slick-prev:hover,
        .slick-prev:focus,
        .slick-next:hover,
        .slick-next:focus
        {
            color: #fff;
            outline: none;
            background: #8e908c;
        }

        .slick-active{
            background-color: 	#800080;
            border-radius: 4px;
            color: #ffffff;
            padding: 10px;
        }

    </style>
@endsection