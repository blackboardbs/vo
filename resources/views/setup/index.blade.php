@extends('adminlte.default')
@section('title') Consulteaze Setup @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="setup">
        <setup-component></setup-component>
    </div>
@endsection
@section('extra-css')
<style>
.setup{
    width: 100vw;
    height:100vh;
    background: rgba(0,0,0,0.7);
    position: fixed;
    top:0;
    left:0;
    z-index: 1000;
    display: flex;
    justify-content: center;
    align-items: center;
}

.main-sidebar{
    z-index:999 !important;
}
    </style>
@endsection