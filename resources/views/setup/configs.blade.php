@extends('adminlte.default')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Configs</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    &nbsp;
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <a id="modal_btn" style="display: none;" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#configuration_modal"><i class="fas fa-paper-plane"></i> Open Modal</a>
            <!-- Modal -->
            <div class="modal fade" id="configuration_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="configuration_label">Step 4 - Configs</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{Form::open(['url' => route('setup.configsupdate', $config->id), 'method' => 'put', 'files' => true])}}
                            {{--<div class="form-row">
                                <div class="form-group col-md-12">
                                    {{Form::label('site_id', 'Site')}}
                                    {{Form::select('site_id',$sites_drop_down ,$config->site_id, ['class'=>'form-control form-control-sm', 'placeholder' =>'Select Site'])}}
                                </div>
                            </div>--}}
                            <div class="ziehharmonika">
                                <h3>Site Information</h3>
                                <div>
                                    <div class="form-group">
                                        {{Form::label('site_title', 'Site title')}}
                                        <div class="input-group">
                                            {{Form::text('site_title', $config->site_title, ['class'=>'form-control form-control-sm'. ($errors->has('site_title') ? ' is-invalid' : ''),'placeholder'=>'Enter site title'])}}
                                            @foreach($errors->get('site_title') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('site_name', 'Site name')}}
                                        <div class="input-group">
                                            {{Form::text('site_name', $config->site_name, ['class'=>'form-control form-control-sm'. ($errors->has('site_name') ? ' is-invalid' : ''),'placeholder'=>'Enter site name'])}}
                                            @foreach($errors->get('site_name') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('admin_email', 'Admin email address')}}
                                        <div class="input-group">
                                            {{Form::email('admin_email', $config->admin_email, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_weeks_future') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('admin_email') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('absolute_path', 'Absolute Path')}}
                                        <div class="input-group">
                                            {{Form::text('absolute_path', $config->absolute_path, ['class'=>'form-control form-control-sm'. ($errors->has('absolute_path') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('absolute_path') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('home_page_message', 'Home page message')}}
                                        <div class="input-group">
                                            {{Form::text('home_page_message', $config->home_page_message, ['class'=>'form-control form-control-sm'. ($errors->has('home_page_message') ? ' is-invalid' : ''),'placeholder'=>'Enter home page message'])}}
                                            @foreach($errors->get('home_page_message') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <h3>Site Default Settings</h3>
                                <div>
                                    <div class="form-group">
                                        {{Form::label('company_id', 'Default Company')}}
                                        <div class="input-group">
                                            {{Form::select('company_id',$company, $config->company_id, ['class'=>'form-control form-control-sm'. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('company_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('cost_center_id', 'Cost Center')}}
                                        <div class="input-group">
                                            {{Form::select('cost_center_id',$cost_center, $config->cost_center_id, ['class'=>'form-control form-control-sm'. ($errors->has('cost_center_id') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('cost_center_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('auditing_records', 'Auditing Records')}}
                                        <div class="input-group input-group-sm">
                                            {{Form::number('auditing_records', $config->auditing_records, ['class'=>'form-control form-control-sm'. ($errors->has('auditing_records') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                            @foreach($errors->get('auditing_records') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Records</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3>Projects and Assignments</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('project_terms_id', 'Project Terms Version')}}
                                            {{Form::select('project_terms_id',$project_terms_drop_down ,$config->project_terms_id, ['class'=>'form-control form-control-sm'. ($errors->has('project_terms_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Terms'])}}
                                            @foreach($errors->get('project_terms_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('claim_approver_id', 'Claim Approver')}}
                                            {{Form::select('claim_approver_id',$resource_drop_down ,$config->claim_approver_id, ['class'=>'form-control form-control-sm'. ($errors->has('claim_approver_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Claim Approver'])}}
                                            @foreach($errors->get('claim_approver_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('vendor_template_id', 'Vendor Template')}}
                                            {{Form::select('vendor_template_id',$templates_drop_down ,$config->vendor_template_id, ['class'=>'form-control form-control-sm'. ($errors->has('vendor_template_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Template'])}}
                                            @foreach($errors->get('vendor_template_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('resource_template_id', 'Resource Template')}}
                                            {{Form::select('resource_template_id',$templates_drop_down ,$config->resource_template_id, ['class'=>'form-control form-control-sm'. ($errors->has('resource_template_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Template'])}}
                                            @foreach($errors->get('resource_template_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            {{Form::label('first_warning_days', 'Number Of Days Before First Warning')}}
                                            {{Form::text('first_warning_days', $config->first_warning_days, ['class'=>'form-control form-control-sm'. ($errors->has('first_warning_days') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('first_warning_days') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{Form::label('second_warning_days', 'Number Of Days Before Second Warning')}}
                                            {{Form::text('second_warning_days', $config->second_warning_days, ['class'=>'form-control form-control-sm'. ($errors->has('second_warning_days') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('second_warning_days') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{Form::label('last_warning_days', 'Number Of Days Before Last Warning')}}
                                            {{Form::text('last_warning_days', $config->last_warning_days, ['class'=>'form-control form-control-sm'. ($errors->has('second_warning_days') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('last_warning_days') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{Form::label('first_warning_percentage', 'Percentage Before First Warning')}}
                                            {{Form::select('first_warning_percentage', $percentage, $config->first_warning_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('first_warning_percentage') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('first_warning_days') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{Form::label('second_warning_percentage', 'Percentage Before Second Warning')}}
                                            {{Form::select('second_warning_percentage', $percentage, $config->second_warning_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('second_warning_percentage') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('second_warning_percentage') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{Form::label('last_warning_percentage', 'Percentage Before Last Warning')}}
                                            {{Form::select('last_warning_percentage', $percentage, $config->last_warning_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('last_warning_percentage') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('last_warning_percentage') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <h3>Timesheet</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('timesheet_weeks', 'Default number of previous weeks to display on timesheet creation')}}
                                            {{Form::text('timesheet_weeks',$config->timesheet_weeks, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_weeks') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('timesheet_weeks') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('timesheet_weeks_future', 'Timesheet Number of Weeks(Future)')}}
                                            {{Form::text('timesheet_weeks_future',$config->timesheet_weeks_future, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_weeks_future') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('timesheet_weeks_future') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <h3>Utilization</h3>
                                <div>
                                    <div class="form-group">
                                        {{Form::label('utilization_method', 'Utilization method')}}
                                        <div class="input-group input-group-sm">
                                            {{Form::select('utilization_method', ['0'=>'Select','1'=>'Manual','2'=>'Dynamic'], $config->utilization_method, ['class'=>'form-control form-control-sm'. ($errors->has('utilization_method') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('utilization_method') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('utilization_hours_per_week', 'Weekly hours for utilization per resource')}}
                                        <div class="input-group input-group-sm">
                                            {{Form::number('utilization_hours_per_week', $config->utilization_hours_per_week, ['class'=>'form-control form-control-sm'. ($errors->has('utilization_hours_per_week') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                            @foreach($errors->get('utilization_hours_per_week') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Hours Per Week</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('utilization_cost_hours_per_week', 'Weekly hours for utilization per cost resource')}}
                                        <div class="input-group input-group-sm">
                                            {{Form::number('utilization_cost_hours_per_week', $config->utilization_cost_hours_per_week, ['class'=>'form-control form-control-sm'. ($errors->has('utilization_cost_hours_per_week') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                            @foreach($errors->get('utilization_cost_hours_per_week') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Hours Per Week</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3>Tax</h3>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        {{Form::label('vat_rate_id', 'Default Tax Rate')}}
                                        {{Form::select('vat_rate_id',$vat_rate_drop_down ,$config->vat_rate_id, ['class'=>'form-control form-control-sm'. ($errors->has('vat_rate_id') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('vat_rate_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <h3>Invoice</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('invoice_body_msg', 'Default Invoice Email message')}}
                                            {{Form::textarea('invoice_body_msg',$config->invoice_body_msg, ['class'=>'form-control form-control-sm'. ($errors->has('invoice_body_msg') ? ' is-invalid' : ''), 'id' => 'area_editor'])}}
                                            @foreach($errors->get('invoice_body_msg') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <h3>Leave</h3>
                                <div>
                                    <div class="form-group">
                                        {{Form::label('days_to_approve_leave', 'Number of days to approve leave')}}
                                        <div class="input-group input-group-sm">
                                            {{Form::number('days_to_approve_leave', $config->days_to_approve_leave, ['class'=>'form-control form-control-sm','placeholder'=>'Enter selection'. ($errors->has('days_to_approve_leave') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('days_to_approve_leave') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Days</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            {{Form::label('annual_leave_days', 'Default Annual Leave Days')}}
                                            {{Form::text('annual_leave_days',$config->annual_leave_days, ['class'=>'form-control form-control-sm'. ($errors->has('annual_leave_days') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('annual_leave_days') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <h3>Assessment</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            {{Form::label('assessment_frequency', 'Assessment Frequency')}}
                                            {{Form::text('assessment_frequency',$config->assessment_frequency, ['class'=>'form-control form-control-sm'. ($errors->has('assessment_frequency') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('assessment_frequency') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-6">
                                            {{Form::label('assessment_master', 'Assessment KPI')}}
                                            {{Form::select('assessment_master[]', $assessment_master_dropdown ,explode(',',$config->assessment_master), ['class'=>'form-control form-control-sm '. ($errors->has('assessment_master') ? ' is-invalid' : ''), 'multiple'])}}
                                            @foreach($errors->get('assessment_master') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{Form::label('assessment_approver_user_id', 'Assessment Approver')}}
                                        <div class="input-group">
                                            {{Form::select('assessment_approver_user_id',$users, $config->assessment_approver_user_id, ['class'=>'form-control form-control-sm'. ($errors->has('assessment_approver_user_id') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('assessment_approver_user_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <h3>Calendar</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            {{Form::label('action_colour', 'Action Colour')}}
                                            {{Form::text('action_colour', $config->calendar_action_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('task_colour', 'Task Colour')}}
                                            {{Form::text('task_colour', $config->calendar_task_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('leave_colour', 'Leave Colour')}}
                                            {{Form::text('leave_colour', $config->calendar_leave_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('assignment_colour', 'Assignments Colour')}}
                                            {{Form::text('assignment_colour', $config->calendar_assignment_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('assessment_colour', 'Assessment Colour')}}
                                            {{Form::text('assessment_colour', $config->calendar_assessment_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('anniversary_colour', 'Anniversary Colour')}}
                                            {{Form::text('anniversary_colour', $config->calendar_anniversary_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('events_colour', 'Events Colour')}}
                                            {{Form::text('events_colour', $config->calendar_events_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('user_colour', 'User Colour')}}
                                            {{Form::text('user_colour', $config->calendar_user_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('cinvoice_colour', 'Customer Invoice Colour')}}
                                            {{Form::text('cinvoice_colour', $config->calendar_cinvoice_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('vinvoice_colour', 'Vendor Invoice Colour')}}
                                            {{Form::text('vinvoice_colour', $config->calendar_vinvoice_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('mcertificate_colour', 'Medical Certificate Colour')}}
                                            {{Form::text('mcertificate_colour', $config->calendar_medcert_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                    </div>
                                </div>

                                <h3>Custom Stylesheet</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            {{Form::label('use_custom_css', 'Use Custom Stylesheet',['style' => 'display:block'])}}
                                            {{Form::checkbox('background_color', false , ['class'=>'form-control form-control-sm'])}}
                                        </div>
                                        <div class="form-group col-md-3">
                                            {{Form::label('background_color', 'Background Colour', ['style'=>'display:block'])}}
                                            {{Form::text('background_color[]', isset($config->background_color)?explode(',',$config->background_color)[0]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'R'])}}
                                            {{Form::text('background_color[]', isset($config->background_color)?explode(',',$config->background_color)[1]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:32%;display:inline-block', 'placeholder'=>'G'])}}
                                            {{Form::text('background_color[]', isset($config->background_color)?explode(',',$config->background_color)[2]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'G'])}}
                                        </div>
                                        <div class="form-group col-md-3">
                                            {{Form::label('font_color', 'Font Colour', ['style'=>'display:block'])}}
                                            {{Form::text('font_color[]', isset($config->font_color)?explode(',', $config->font_color)[0]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'R'])}}
                                            {{Form::text('font_color[]', isset($config->font_color)?explode(',', $config->font_color)[1]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:32%;display:inline-block', 'placeholder'=>'G'])}}
                                            {{Form::text('font_color[]', isset($config->font_color)?explode(',', $config->font_color)[2]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'B'])}}
                                        </div>
                                        <div class="form-group col-md-3">
                                            {{Form::label('active_link', 'Active Link', ['style'=>'display:block'])}}
                                            {{Form::text('active_link[]', isset($config->active_link)?explode(',',$config->active_link)[0]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'R'])}}
                                            {{Form::text('active_link[]', isset($config->active_link)?explode(',',$config->active_link)[1]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:32%;display:inline-block', 'placeholder'=>'G'])}}
                                            {{Form::text('active_link[]', isset($config->active_link)?explode(',',$config->active_link)[2]:'', ['class'=>'form-control form-control-sm', 'style' => 'width:33%;display:inline-block', 'placeholder'=>'B'])}}
                                        </div>
                                        <div class="form-group col-md-3">
                                            <span style="display: block">{{Form::label('site_logo', 'Site Logo')}} <i class="far fa-question-circle" data-toggle="tooltip" data-html="true" title="This Logo will be displayed<br><img src='{{route('company_avatar',(isset($config->company)? ['q'=>$config->company->company_logo]:''))}}' width='180px'/>"></i></span>
                                            <input type="file" name="site_logo" class="form-control-file" id="site_logo">
                                        </div>
                                        <div class="form-group col-md-3">
                                            {{Form::label('placement', 'Logo Placement', ['style' => 'display:block'])}}
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="left" {{($config->logo_placement == 'left')?'checked':''}} value="left">
                                                <label class="form-check-label" for="left">Left</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="center" {{($config->logo_placement == 'center')?'checked':''}} value="center">
                                                <label class="form-check-label" for="center">Center</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="right" {{($config->logo_placement == 'right')?'checked':''}} value="right">
                                                <label class="form-check-label" for="right">Right</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <a href="{{route('default.configs')}}" class="btn btn-sm btn-secondary" style="margin-top: 20px;">Reset Default</a>
                                        </div>
                                    </div>
                                </div>

                                <h3>Cost Calculator</h3>
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            {{Form::label('cost_min_percent', 'Cost Min. Percent')}}
                                            {{Form::select('cost_min_percent',$percentage ,$config->cost_min_percent, ['class'=>'form-control form-control-sm'. ($errors->has('cost_min_percent') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('cost_min_percent') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-3">
                                            {{Form::label('cost_max_percent', 'Cost Max. Percent')}}
                                            {{Form::select('cost_max_percent', $percentage ,$config->cost_max_percent, ['class'=>'form-control form-control-sm'. ($errors->has('cost_max_percent') ? ' is-invalid' : '')])}}
                                            @foreach($errors->get('cost_max_percent') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('cost_min_colour', 'Cost Min. Colour')}}
                                            {{Form::text('cost_min_colour', $config->cost_min_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('cost_mid_colour', 'Cost Mid Colour')}}
                                            {{Form::text('cost_mid_colour', $config->cost_mid_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                        <div class="form-group col-md-2">
                                            {{Form::label('cost_max_colour', 'Max. Commission')}}
                                            {{Form::text('cost_max_colour', $config->cost_max_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        </div>
                                    </div>
                                </div>

                                @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager') || Auth::user()->isAn('manager'))
                                    <div class="form-group" style="display: block!important;">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-sm"><i class="fa fa-thumbs-up"></i> Done</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="{{asset('jodit-3.2.24/build/jodit.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/ziehharmonika.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,700,700i" rel="stylesheet">
    <style>
        .sidebar-dark-primary {
            background-color: {!! '#'.$config->background_color !!}!important;
        }
        .sidebar .header, .sidebar-dark-primary .sidebar a {
            color: {!! '#'.$config->font_color !!}!important;
        }
        .sidebar .header, [class*=sidebar-dark] .user-panel, [class*=sidebar-dark] .brand-link {
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
@endsection
@section('extra-js')

    <script src="{{asset('jodit-3.2.24/build/jodit.min.js')}}"></script>
    <script src="{{asset('jodit-3.2.24/examples/assets/prism.js')}}"></script>
    <script src="{{asset('js/ziehharmonika.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script>
        var editor = new Jodit('#area_editor', {
            textIcons: false,
            iframe: false,
            iframeStyle: '*,.jodit_wysiwyg {color:red;}',
            height: 300,
            defaultMode: Jodit.MODE_WYSIWYG,
            observer: {
                timeout: 100
            },
            uploader: {
                url: ''
            },
            filebrowser: {
                // buttons: ['list', 'tiles', 'sort'],
                ajax: {
                    url: ''
                }
            },
            commandToHotkeys: {
                'openreplacedialog': 'ctrl+p'
            }
            // buttons: ['symbol'],
            // disablePlugins: 'hotkeys,mobile'
        });
        $(function () {


            document.getElementById("modal_btn").click();

        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })

            // Accordion

            $('.ziehharmonika').ziehharmonika();
        })
    </script>
@endsection