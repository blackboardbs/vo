@extends('adminlte.default')
@section('title') Web Office Setup @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <a id="modal_btn" style="display: none;" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#configuration_modal"><i class="fas fa-paper-plane"></i> Open Modal</a>
        <!-- Modal -->
        <div class="modal fade" id="configuration_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="configuration_label">Step 3 - User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{Form::open(['url' => route('setup.storeuser'), 'method' => 'post','class'=>'mt-3', 'id'=>'store_user_frm','files' => true])}}
                        <input type="hidden" name="is_demo" id="id_demo" value="0" />
                        <div class='row'>
                            <div class="col-md-12">
                                <p>
                                    Welcome to the Blackboard Web Office
                                </p>
                                <p>
                                    The Config Wizard will assist you to setup your Office in 4 easy steps. The setup will take about 5 minutes and will have help content videos to explain each step.
                                </p>
                                <p>
                                    You can watch the <a href="" target="_blank">introduction video</a> or click on the <a href="" target="_blank">help</a> icon to get started.
                                </p>
                                <p>
                                    Step 3 - User Information
                                </p>
                            </div>
                        </div>
                        @if(isset($users))
                            <div class='row'>
                                <div class='col-md-12'>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-sm table-hover">
                                            <thead class="btn-dark">
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>
                                                    <abbr title="This is the amount of roles a user is assigned to.">Roles</abbr>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{$user->name()}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>
                                                        @foreach($user->roles as $roles)
                                                            {{$roles->display_name}} <br/>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class='row'>
                            <div class='col-md-12'>
                                <table class="table table-bordered table-sm mt-3">
                                    <tbody>
                                    {{--<tr>
                                        <th>User Name:</th>
                                        <td>
                                            {{Form::text('company_name', $company->company_name,['class'=>'form-control form-control-sm'. ($errors->has('company_name') ? ' is-invalid' : ''),'placeholder'=>'User Name'])}}
                                            @foreach($errors->get('company_name') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>--}}
                                    <tr>
                                        <th>User First Name:</th>
                                        <td>
                                            {{Form::text('first_name', old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'User First Name'])}}
                                            @foreach($errors->get('first_name') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>User Last Name:</th>
                                        <td>
                                            {{Form::text('last_name', old('last_name'),['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'User Last Name'])}}
                                            @foreach($errors->get('last_name') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>User Email:</th>
                                        <td>
                                            {{Form::text('email', old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'User Email'])}}
                                            @foreach($errors->get('email') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>User Phone Number:</th>
                                        <td>
                                            {{Form::text('phone', old('phone'),['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'User Phone Number'])}}
                                            @foreach($errors->get('phone') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Role:</th>
                                        <td>
                                            {{Form::select('role_id',$roles_drop_down,null,['class'=>'form-control form-control-sm '. ($errors->has('role_id') ? ' is-invalid' : ''),'id'=>'role_id'])}}
                                            @foreach($errors->get('role_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;&nbsp;
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                                </a> <button type="submit" class="btn btn-sm bg-dark d-print-none">Add User <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;&nbsp;
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                                <a onclick="skipConfiguration()" class="btn btn-sm bg-dark d-print-none">Skip Config <i class="fas fa-angle-double-right"></i></a> <a onclick="loadDemoData()" class="btn btn-sm bg-dark d-print-none">Load Demo Data <i class="fas fa-paper-plane"></i></a> <a href="{{route('setup.configs')}}" class="btn btn-sm bg-dark d-print-none">Next <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    {{--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{{asset('slick-1.8.1/slick/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {

            $(".variable").slick({
                dots: true,
                infinite: true,
                variableWidth: true
            });

            document.getElementById("modal_btn").click();

            $('.slick-prev').click(function(){
                getModule();
            });

            $('.slick-next').click(function(){
                getModule();
            });

            $('.slick-slide').click(function(){
                var cliked_element = $(this).attr("industry-id");
                cliked_element = cliked_element - 1;
                var div_number = '0'+cliked_element;
                document.getElementById("slick-slide-control"+div_number).click();
                getModule();
            });

        });

        function loadDemoData(){
            var confirm_flag = confirm("Please not that demo data can not be used on a production environment. Are you sure you want to load demo data.");
            if(confirm_flag) {
                $('input[name="is_demo"]').val(1); //Using id does not work, weird
                $('#store_user_frm').submit();
            }
        }

        function skipConfiguration(){
            confirm("Are you sure you want to skip configuration");
            window.location = "/";
        }

        function getModule(){

            var active_element = document.getElementsByClassName("slick-active")[0].getAttribute("industry-id");

            let industry_id = active_element;
            axios.get('/setup/getmodules/'+industry_id, {
                industry_id: industry_id
            })
                .then(function (response) {

                    $('#modules_div').empty();

                    var counter = 1;

                    var module_html = '<div class="row">';

                    $.each(response.data.modules, function (key, value) {

                        var checked = '';
                        if(value.selected == 1){
                            checked = 'checked';
                        }

                        var selectable = '';
                        if(value.selectable == 0){
                            selectable = 'disabled';
                        }

                        module_html += '<div class="col-md-4">';
                        module_html += '   <div class="checkbox">';
                        module_html += '       <label><input id="industry_modules_'+value.module_id+'" name="industry_modules[]" type="checkbox" value="'+value.module_id+'" '+checked+' '+selectable+'> '+value.module.display_name+'</label>';
                        module_html += '   </div>';
                        module_html += '</div>';

                        counter++;

                        if((counter % 3 == 1) && (counter != 1)) {
                            module_html += '</div>';
                            module_html += '<div class="row">';
                        }
                    });

                    module_html += '</div>';

                    $('#modules_div').html(module_html);
                    $('#industry_id').val(industry_id);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    </script>
@endsection
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('slick-1.8.1/slick/slick-theme.css')}}"/>
    <style>

        .slick-list{
            width: 85%;
            margin-left: 50px;
        }

        .slick-arrow{
            background-color: #0c0c0c;
            border-radius: 50%;
        }

        .slick-prev {
            left: 0px;
        }

        .slick-next {
            right: 0px;
        }

        .slick-prev, .slick-next {
            width: 40px;
            height: 40px;
            radius: 4px !important;
        }

        .slick-slide{
            width: 250px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .slick-prev:hover,
        .slick-prev:focus,
        .slick-next:hover,
        .slick-next:focus
        {
            color: #fff;
            outline: none;
            background: #8e908c;
        }

        .slick-active{
            background-color:  #800080;
            border-radius: 4px;
            color: #ffffff;
            padding: 10px;
        }

    </style>
@endsection