@extends('adminlte.default')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header bg-dark mb-2">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Recent Activities</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                &nbsp;
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">

                <div class="table-responsive">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Projects</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($projects as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.$activity["description"].' '.(isset($activity["subject"])?$activity["subject"]->name:'').'</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent projects yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>

                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Vendor Invoice</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($vendor_invoice as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.$activity["description"].'  an invoice number #'.$activity['subject']->id.'</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent vendor invoices yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>

                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Timesheets</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($timesheets as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.$activity["description"].' timesheet for '.(isset($activity["subject"])?substr($activity["subject"]->project->name,0,10).' ('.$activity["subject"]->year_week.')':'').'</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent timesheets yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>

                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">CV</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($cvs as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.substr($activity["consultant"],0,1).'. '.substr($activity["consultant"], strpos($activity["consultant"],' ') + 1).' '.$activity["description"].' '.substr($activity["subject"]->user->first_name,0,1).'. '.$activity["subject"]->user->last_name.'\'s CV</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent CVs yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Assignments</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($assignments as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.$activity["description"].' an assignment '.(isset($activity["subject"])?substr($activity["subject"]->project->name,0,10).'('.substr($activity["subject"]->resource->first_name,0,1).substr($activity["subject"]->resource->last_name,0,1).')':'').'</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent assignments yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Customer Invoice</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($customer_invoices as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.(($activity["description"] == 'created')?'Invoiced':$activity["description"]).' '.(isset($activity["subject"]->customer)?$activity["subject"]->customer->customer_name.' (#'.$activity["subject"]->customer->id.')':'').'</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent customer invoices yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Leave</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($leaves as $activity)
                                            <tr>
                                                <td>
                                                    @if(isset($activity["link"]))
                                                        <a href="{{$activity["link"]}}">
                                                            {{((($activity["description"] == 'approved') || ($activity["description"] == 'declined'))?(isset($activity["properties"]["name"])?$activity["properties"]["name"]:''):substr($activity["consultant"],0,1).'. '.substr($activity["consultant"], strpos($activity["consultant"], " ") + 1).' '.$activity["description"].' for '.(isset($activity["subject"])?$activity["subject"]->leave_type->description:''))}}
                                                        </a>
                                                    @else
                                                        {{isset($activity["properties"]["name"])?$activity["properties"]["name"]:''}}
                                                    @endif
                                                </td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There is no recent leave yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="card card-default">
                                <div class="card-header">
                                    <h3 class="card-title">Expenses</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: block;">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th>Activity</th>
                                            <th>Log time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($expenses as $activity)
                                            <tr>
                                                <td>{!! (isset($activity["link"]))?'<a href="'.$activity["link"].'">'.$activity["consultant"].' '.$activity["description"].' expense  on '.(isset($activity["subject"])?(isset($activity["subject"]->transaction_date)?$activity["subject"]->transaction_date:$activity["subject"]->date):'').'_(#'.(isset($activity["subject"])?$activity["subject"]->id:'').')</a>':(isset($activity["properties"]["name"])?$activity["properties"]["name"]:'') !!}</td>
                                                <td>{{$activity["time"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2" class="text-center">There are no recent expenses yet</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>

                        </div>
                    </div>
                </div>
                
             </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection