@extends('adminlte.default')

@section('title') Add Time @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet (SW RW_SASOL_BOS_2018)</div>
                <div class="col-md-4">Date from ( {{ $first_day_week }} ): <strong> {{ $start_week }} </strong> to ( {{ $end_day_week }} ): <strong> {{ $end_week }} </strong> </div>
                <div class="col-md-2 text-right">Wk {{ $yearwk }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-11 row"><div class="col-md-9">Time Allocation</div><div class="col-md-3">Walkenshaw, Stefan</div></div>
                <div class="col-md-1 text-right">Emp ID: 1</div>
            </div>
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr class="bg-dark">
                    <th>Client</th>
                    <th>Project Name</th>
                    <th>Description of Work</th>
                    <th>Bill</th>
                    <th>Mon 20</th>
                    <th>Tue 21</th>
                    <th>Wed 22</th>
                    <th>Thu 23</th>
                    <th>Fri 24</th>
                    <th>Sat 25</th>
                    <th>Sun 26</th>
                    <th>Total</th>
                    <th>Travel (mileage)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>RangeWave</td>
                    <td>Sasol Energy Support 2018_2019</td>
                    <td>Energy Support</td>
                    <td>Yes</td>
                    <td>8:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>8:00</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td colspan="4">TOTAL</td>
                    <td>8:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>8:00</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table>
            {{Form::open(['url' => route('task.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="10" class="text-center">Insert a new line in this weekly timesheet.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Client:</th>
                    <td colspan="5">{{Form::text('client',$data1->company_name,['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Project:</th>
                    <td colspan="4">{{Form::text('project',$data2->name,['class'=>'form-control form-control-sm'. ($errors->has('project') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td colspan="9">{{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Time Allocation</th>
                    <th>Mon 20</th>
                    <th>Tue 21</th>
                    <th>Wed 22</th>
                    <th>Thu 23</th>
                    <th>Fri 24</th>
                    <th>Sat 25</th>
                    <th>Sun 26</th>
                    <td>Travel (mileage)</td>
                    <td>Billable</td>
                </tr>
                <tr>
                    <th>Hours</th>
                    <td>{{Form::select('mon',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('mon') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('tue',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('tue') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('tue') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('wed',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('wed') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('wed') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('thu',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('thu') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('thu') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('fri',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('fri') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('fri') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('sat',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('sat') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('sat') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('sun',$data3,0,['class'=>'form-control form-control-sm '. ($errors->has('sun') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('sun') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::text('milage',0,['class'=>'form-control form-control-sm '. ($errors->has('milage') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('milage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('mon',[0 => 'No', 1 => 'Yes'],0,['class'=>'form-control form-control-sm'. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('mon') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Minutes</th>
                    <td>{{Form::select('mon',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('mon') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('mon') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('tue',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('tue') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('tue') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('wed',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('wed') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('wed') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('thu',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('thu') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('thu') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('fri',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('fri') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('fri') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('sat',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('sat') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('sat') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td>{{Form::select('sun',$data4,0,['class'=>'form-control form-control-sm '. ($errors->has('sun') ? ' is-invalid' : ''),'placeholder'=>'Select Minutes'])}}
                        @foreach($errors->get('sun') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection
