<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{$employee->last_name.', '.$employee->first_name}}'s Timesheet</title>
    <!-- <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'> -->
    <style>
        body{
            margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}
        }

        .signature {
            font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
            color: #000000;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }

    </style>
</head>
    <body class="hold-transition sidebar-mini">
        <div id="app" class="wrapper">
            <div class="container">
                <div id="timesheets_div">
                    <table class="table table-bordered table-sm mt-3">
                        <thead>
                        <tr>
                            <th colspan="6">
                                {{--<img src="{!! asset('assets/templates/eim_template_header.png') !!}" style="border: none; width: 250px;">--}}
                                {{isset($timesheet->company)?$timesheet->company->company_name:''}}
                            </th>
                            <td class="header-text">
                                Timesheet
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent; border-bottom: 1px solid transparent;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Name</th><td colspan="6">{{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}} {{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}}</td>
                        </tr>
                        <tr>
                            <th>Week</th><td>{{$timesheet->week}}</td><td>From (Monday)</td><td colspan="4">{{$timesheet->first_day_of_week}}</td>
                        </tr>
                        <tr>
                            <th colspan="2">&nbsp;</th><td>To (Sunday)</td><td colspan="4">{{$timesheet->last_day_of_week}}</td>
                        </tr>
                        <tr>
                            <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent; border-bottom: 1px solid transparent;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Date</th><th>Project code</th><th>Project name</th><th>Description</th><th>Hours</th><th>Billable</th><th>Non Billable</th>
                        </tr>
                        @foreach($timeline_weekly_details as $timeline_weekly_detail)
                            @if($timeline_weekly_detail['is_billable'] == 1)
                                <tr>
                                    <td>{{$timeline_weekly_detail['date']}}</td><td>{{$timeline_weekly_detail['project_code']}}</td><td>{{$timeline_weekly_detail['project_name']}}</td><td>{{$timeline_weekly_detail['description_of_work']}}</td><td>{{$timeline_weekly_detail['total_minutes_to_time']}}</td><td>{{$timeline_weekly_detail['total_minutes_to_time']}}</td><td>-</td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{$timeline_weekly_detail['date']}}</td><td>{{$timeline_weekly_detail['project_code']}}</td><td>{{$timeline_weekly_detail['project_name']}}</td><td>{{$timeline_weekly_detail['description_of_work']}}</td><td>{{$timeline_weekly_detail['total_minutes_to_time']}}</td><td>-</td><td>{{$timeline_weekly_detail['total_minutes_to_time']}}</td>
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <th colspan="4">TOTAL (weekly billable hours)</th><td></td><th>{{$total_time_billable}}</th><td></td>
                        </tr>
                        <tr>
                            <th colspan="4">TOTAL (weekly billable hours)</th><td></td><td></td><th>{{$total_time_non_billable}}</th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>

                    @foreach($digisign_users as $digisign_user)
                        @php $type = ''; @endphp
                        @if($digisign_user->user_number == 1)
                            @php
                                $type = 'Resource';
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 2)
                            @php
                                $type = $assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner';
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 3)
                            @php
                                $type = $assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner';
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 4)
                            @php
                                $type = $assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager';
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 5)
                            @php
                                $type = $assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager';
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 6)
                            @php
                                $type = $assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver'
                            @endphp
                        @endif
                        @if($digisign_user->user_number == 7)
                            @php
                                $type = $assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager'
                            @endphp
                        @endif
                        {{--@if($digisign_user->user_number == 2)
                            @php $type = 'Product Owner'; @endphp
                        @endif
                        @if($digisign_user->user_number == 3)
                            @php $type = 'Report to'; @endphp
                        @endif--}}
                        @if($digisign_user->user_number > 7)
                            @php $type = 'Name'; @endphp
                        @endif
                        {{--<tr style="width: 100%;">
                            <td style="width: 30%; border-top: 1px solid transparent; border-bottom: 1px solid #343a40" class="signature">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td>
                            <td style="width: 45%; border-top: 1px solid transparent;">&nbsp</td>
                            <td style="width: 25%; border-top: 1px solid transparent; border-bottom: 1px solid #343a40" class="text-left">{{$digisign_user->signed == 1 ? $digisign_user->user_sign_date : ''}}</td>
                        </tr>
                        <tr style="width: 100%;">
                            <td style="width: 30%;">{{$type}}</td>
                            <td style="width: 45%; border-top: 1px solid transparent;">&nbsp</td>
                            <td style="width: 25%;" class="text-left">Date</td>
                        </tr>--}}

                        <br/>
                        <table class="table table-bordered table-sm mt-3">
                            <tbody>
                                <tr>
                                    <th style="width: 170px; border: 1px solid transparent">{{$type}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;" class="signature">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;">{{$digisign_user->signed == 1 ? $digisign_user->user_sign_date : ''}}</th>
                                </tr>
                            </tbody>
                        </table>
                        <br/>

                    @endforeach

                </div>
            </div>
        </div>
    </body>
</html>
