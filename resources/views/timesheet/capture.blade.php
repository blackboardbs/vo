@extends('adminlte.default')

@section('title') Capture Time @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet</div>
                <div class="col-md-4">Date from ( {{ $first_day_week }} ): <strong> {{ $start_week }} </strong> to ( {{ $end_day_week }} ): <strong> {{ $end_week }} </strong> </div>
                <div class="col-md-2 text-right">Wk {{ $yearwk }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-11 row"><div class="col-md-9">Time Allocation</div><div class="col-md-3">Walkenshaw, Stefan</div></div>
                <div class="col-md-1 text-right">Emp ID: 1</div>
            </div>
            {{--{{Form::open(['url' => route('task.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Task Number:</th>
                    <td>{{Form::text('task_number',old('task_number'),['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'placeholder'=>'Task Number'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Subject:</th>
                    <td>{{Form::text('subject',old('subject'),['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''),'placeholder'=>'Subject'])}}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td colspan="3">{{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Due Date:</th>
                    <td>{{Form::text('due_date',old('due_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Due Date'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    --}}{{--<th>Task User:</th>
                    <td>{{Form::select('task_user',$task_user_dropdown,null,['class'=>'form-control form-control-sm'])}}
                        @foreach($errors->get('task_user') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>--}}{{--
                <tr>
                    <th>Rejection Reason:</th>
                    <td>{{Form::text('rejection_reason',old('rejection_reason'),['class'=>'form-control form-control-sm'. ($errors->has('rejection_reason') ? ' is-invalid' : ''),'placeholder'=>'Rejection Reason'])}}
                        @foreach($errors->get('rejection_reason') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Rejection Date:</th>
                    <td>{{Form::text('rejection_date',old('rejection_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('rejection_date') ? ' is-invalid' : ''),'placeholder'=>'Rejection Date'])}}
                        @foreach($errors->get('rejection_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email Task:</th>
                    <td>{{Form::checkbox('email_task')}}
                        @foreach($errors->get('email_task') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    --}}{{--<th>Status:</th>
                    <td>{{Form::select('status',$sidebar_process_statuses,old('status'),['class'=>'form-control form-control-sm','id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>--}}{{--
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<button type="submit" class="btn btn-sm">Cancel</button></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}--}}

            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><a href="{{ route('timesheet.addtime') }}" class="btn btn-dark btn-sm"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Time</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
