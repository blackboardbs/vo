@extends('adminlte.default')

@section('title') Timesheet @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
                @php
                    $date = new DateTime($timesheet->last_day_of_month);
                @endphp
                <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
                <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ isset($employee->last_name)?$employee->last_name:''.', '.isset($employee->first_name)?$employee->first_name:'' }}</span></div><div class="col-md-1 text-right">Emp ID: {{isset($employee->id)?$employee->id:''}}</div>
            </div>
            <table class="table" border="1">
                <tbody>
                    <tr class="bg-dark">
                        <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
                        @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                            @if($i > $timesheet->last_date_of_month)
                                @break
                            @endif

                            @php
                                $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            @endphp

                            <th>{{$date->format('D')}} {{$i}}</th>
                        @endfor
                        <th>TOTAL</th><th>Travel (Mileage)</th>
                    </tr>
                    <tr>
                        <td colspan="4">TOTAL</td>
                        @php
                            $day_number = 1;
                        @endphp
                        @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                            @if($i > $timesheet->last_date_of_month)
                                @break
                            @endif

                            @php
                                $hour = 0;
                                $minute = 0;
                                foreach($timelines as $timeline):
                                    $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                    $_hour = $timeline[strtolower($date->format('D'))];
                                    $_minute = $timeline[strtolower($date->format('D')).'_m'];
                                    $hour += $_hour;
                                    $minute += $_minute;
                                endforeach;
                                $a_hours = $minute/60;
                                $b_hours = (int)$a_hours;
                                $a_minutes = $a_hours - $b_hours * 60;
                            @endphp

                            <td rowspan="2">{{$hour}}:{{$minute}}</td>
                        @endfor
                        <td>{{'0:00'}}</td><td>0</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><a href="{{ route('timesheet.createtimeline', $timesheet->id) }}" class="btn btn-dark btn-sm"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Time</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
