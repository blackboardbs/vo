@extends('adminlte.default')
@section('title') Add Attachment @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('timesheet.saveattachment', $timesheet->id), 'method' => 'put', 'class'=>'mt-3', 'files' => true])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="10" class="text-center">Attachement</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Description:</th>
                        <td>
                            {{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'id' => 'description'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>File:</th>
                        <td>
                            <input type="file" name="file" id="file" class="form-control form-control-sm {{$errors->has('file') ? ' is-invalid' : ''}}">
                            @foreach($errors->get('file') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection