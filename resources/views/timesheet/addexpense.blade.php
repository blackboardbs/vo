@extends('adminlte.default')

@section('title') Add Expense @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="row bg-dark p-2 no-gutters">
            <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
            @php
                $date = new DateTime($timesheet->last_day_of_month);

                $total_time = 0;
                $total_mileage = 0;
                function minutesToTime($minutes){
                    $hours = floor($minutes/60);
                    $minutes = $minutes % 60;
                    $negation_number = '';
                    if($hours < 0){
                        $hours *= -1;
                        $negation_number = '-';
                    }

                    if($minutes < 0){
                        $minutes *= -1;
                        $negation_number = '-';
                    }
                    $hour_str = $hours < 10 ? '0'.$hours : $hours;
                    $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                    return $negation_number.$hour_str.':'.$minutes_str;
                }
            @endphp
            <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
            <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
        </div>
        <div class="row no-gutters p-2">
            <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
        </div>
        <table class="table" border="1">
            <tbody>
            <tr class="bg-dark">
                <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                    @if($i > $timesheet->last_date_of_month)
                        @break
                    @endif

                    @php
                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                    @endphp

                    <th>{{$date->format('D')}} {{$i}}</th>
                @endfor
                <th>TOTAL</th><th>Travel (Mileage)</th>
            </tr>
            @foreach($timelines as $timeline)
                <tr>
                    <td>{{$timesheet->customer->customer_name}}</td><td>{{$timesheet->project->name}}</td><td>{{$timeline['description_of_work']}}</td><td>{{$timeline['is_billable'] == 1 ? 'Yes' : 'No'}}</td>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif
                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        @endphp
                        <td>{{$timeline[strtolower($date->format('D'))]}}:{{$timeline[strtolower($date->format('D')).'_m']}}</td>
                    @endfor
                    @php
                        $total_time += $timeline['total'] * 60 + $timeline['total_m'];
                        $total_mileage += $timeline['mileage'];
                    @endphp
                    <td>{{minutesToTime($timeline['total'] * 60 + $timeline['total_m'])}}</td><td>{{$timeline['mileage']}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4">TOTAL</td>
                @php
                    $day_number = 1;
                @endphp
                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                    @if($i > $timesheet->last_date_of_month)
                        @break
                    @endif

                    @php
                        $hour = 0;
                        $minute = 0;
                        foreach($timelines as $timeline):
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            $_hour = $timeline[strtolower($date->format('D'))];
                            $_minute = $timeline[strtolower($date->format('D')).'_m'];
                            $hour += $_hour;
                            $minute += $_minute;
                        endforeach;
                    @endphp

                    <td rowspan="2">{{minutesToTime($hour * 60 + $minute)}}</td>
                @endfor
                <td>{{minutesToTime($total_time)}}</td><td>{{$total_mileage}}</td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm">
            <thead class="btn-dark">
            <tr class="bg-dark">
                <th colspan="7">Expenses</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Date</td>
                <td>Description</td>
                <td>Account / *Element</td>
                <td>Status</td>
                <td>Bill</td>
                <td>Claim</td>
                <td>Amount</td>
            </tr>
            <tr>
                <td colspan="6" class="text-uppercase">total to be billed to client</td>
                <td>0:00</td>
            </tr>
            <tr>
                <td colspan="6" class="text-uppercase">total claimed by consultant</td>
                <td>0:00</td>
            </tr>
            </tbody>
        </table>

        {{Form::open(['url' => route('timesheet.saveexpense', $timesheet), 'method' => 'post','class'=>'mt-3'])}}
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th colspan="4" class="bg-dark">Insert a new expense line in this weekly timesheet.</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th style="width: 20%">Date</th>
                    <td style="width: 30%">
                        {{Form::text('date',date('Y-m-d'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('date') ? ' is-invalid' : ''),'placeholder'=>'Date'])}}
                        @foreach($errors->get('date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Billable</th>
                    <td>
                        {{Form::select('billable',[0 => 'No', 1 => 'Yes'], 0,['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style="width: 20%">Claim</th>
                    <td>
                        {{Form::select('claim',[0 => 'No', 1 => 'Yes'],0,['class'=>'form-control form-control-sm'. ($errors->has('claim') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('claim') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description</th>
                    <td>
                        {{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => 'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style="width: 20%">Amount</th>
                    <td>
                        {{Form::text('amount',old('amount'),['class'=>'form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'Amount'])}}
                        @foreach($errors->get('amount') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Account</th>
                    <td>
                        {{Form::select('account',$account_dropdown, null,['class'=>'form-control form-control-sm'. ($errors->has('account') ? ' is-invalid' : ''), 'placeholder' => 'Select Account', 'id' => 'expense_account'])}}
                        @foreach($errors->get('account') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style="width: 20%">Client</th>
                    <td>
                        {{Form::text('client_id',$timesheet->customer->customer_name,['class'=>'datepicker form-control form-control-sm'. ($errors->has('client_id') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('client_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Element</th>
                    <td>
                        {{Form::select('element',[0 => 'Select Element'], null,['class'=>'form-control form-control-sm'. ($errors->has('element') ? ' is-invalid' : ''),'id' => 'expense_element', 'style' => 'display:none'])}}
                        @foreach($errors->get('element') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style="width: 20%">Project</th>
                    <td>
                        {{Form::text('project_id',$timesheet->project->name,['class'=>'datepicker form-control form-control-sm'. ($errors->has('project_id') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
            </tr>
            </tbody>
        </table>
        {{Form::close()}}
    </div>
@endsection
