<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead>
        <tr class="btn-dark">
            <th>ID</th>
            <th>Resource</th>
            <th>YearWk</th>
            <th>Wk Start</th>
            <th>Customer</th>
            <th>Project</th>
            <th>Bill</th>
            <th>No Bill</th>
            <th>Total Hour</th>
            <th>Exp</th>
            @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                <th>CIS</th>
                <th>VIS</th>
                <th>CIR</th>
                <th>VIR</th>
            @endif
            <th>Claim Amount</th>
            @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                <th>Detail</th>
            @endif
            <th>Invoice Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($timesheets as $timesheet)
            @php
                $invoice_amount_total += $invoice_amounts[$timesheet->id];
                $total_claim += (isset($timesheet->time_exp[0]->amount)?$timesheet->time_exp[0]->amount:0);
            @endphp
            <tr>
                <td>{{$timesheet->id}}</td>
                <td>{{$timesheet->employee?->name()}}</td>
                <td>{{$timesheet->year_week}}</td>
                <td>{{$timesheet->first_day_of_week}}</td>
                <td>{{$timesheet->customer?->customer_name}}</td>
                <td>{{$timesheet->project?->name}}</td>
                <td>{{_minutes_to_time($timesheet->minutes_bill)}}</td>
                <td>{{ _minutes_to_time($timesheet->minutes_no_bill) }}</td>
                <td>{{ _minutes_to_time($timesheet->total_hours * 60 + $timesheet->total_minutes) }}</td>
                <td>
                    @if((count($timesheet->time_exp) > 0) && !isset($timesheet->time_exp[0]->paid_date) && !isset($timesheet->time_exp[0]->claim_auth_date))
                        <span class="badge badge-danger px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense not approved and not paid"><i class="fas fa-times"></i></span>
                    @elseif((count($timesheet->time_exp) > 0) && !isset($timesheet->time_exp[0]->paid_date) && isset($timesheet->time_exp[0]->claim_auth_date))
                        <span class="badge badge-warning px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense is approved but not paid"><i class="fas fa-check"></i></span>
                    @elseif((count($timesheet->time_exp) > 0) && isset($timesheet->time_exp[0]->paid_date) && isset($timesheet->time_exp[0]->claim_auth_date))
                        <span class="badge badge-success px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense is approved and paid"><i class="fas fa-check-double"></i></span>
                    @endif
                </td>
                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <td class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="{{isset($timesheet->bill_statusd->description)?$timesheet->bill_statusd->description:'Please select invoice status'}}">
                        <a class="badge px-2 {{(!isset($timesheet->bill_statusd->description)?'btn-warning':'btn-primary')}} " href="{{route('timesheet.maintain', $timesheet)}}" style="color: white !important">
                            {{isset($timesheet->bill_statusd->description) ? substr($timesheet->bill_statusd->description,0,1) : '?'}}
                        </a></td>
                    <td class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="{{isset($timesheet->vendor_inv_status->description)? $timesheet->vendor_inv_status->description:''}}">
                        <a class="badge px-2 {{(!isset($timesheet->vendor_inv_status->description)?'btn-warning':'btn-primary')}}" href="{{route('timesheet.maintain', $timesheet)}}" style="color: white !important">
                            {{isset($timesheet->vendor_inv_status->description) ? substr($timesheet->vendor_inv_status->description,0,1) : ''}}
                        </a></td>
                    <td>{{$timesheet->cust_inv_ref}}</td>
                    <td>{{$timesheet->vendor_invoice_ref}}</td>
                @endif
                <td class="text-right">{{(count($timesheet->time_exp) > 0)?number_format($timesheet->time_exp[0]->amount,2,'.',','):number_format(0,2,'.',',')}}</td>
                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <td class="text-left">
                        {{$timesheet->invoice_note}}
                    </td>
                @endif
                <td class="text-right" @if(!isset($_GET['inv_amount']) || ($_GET['inv_amount'] != 1)) style="display: none;" @endif>{{number_format($invoice_amounts[$timesheet->id], 2, '.', ',')}}</td>
            </tr>
        @endforeach
        @if(count($timesheets) > 0)
            <tr>
                <th>TOTAL</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class="text-center">{{_minutes_to_time($total_billable_hours)}}</th>
                <th class="text-center">{{_minutes_to_time($total_non_billable_hours)}}</th>
                <th class="text-center">{{_minutes_to_time($total_hours)}}</th>
                <th>&nbsp;</th>
                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                @endif
                <th class="text-right">{{number_format($total_claim,2,'.', ',')}}</th>
                <th>&nbsp;</th>
                <th class="text-right" @if(!isset($_GET['inv_amount']) || ($_GET['inv_amount'] != 1)) style="display: none;" @endif>{{number_format($invoice_amount_total,2,'.', ',')}}</th>

            </tr>
        @endif
        </tbody>
    </table>
</div>