<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{$employee->last_name.', '.$employee->first_name}}'s Timesheet</title>
    <style>
        body{
            margin:0;
            font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:1rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff
        }
        .table,hr{
            margin-bottom:1rem
        }
        .container{
            width:98%;
            padding-right:7.5px;
            padding-left:7.5px;
            margin-right:auto;
            margin-left:auto
        }
        hr{
            margin-top:1rem;
            border:0;
            border-top:1px solid rgba(0,0,0,.1)
        }
        .table-responsive{
            display:block;
            width:100%;
            overflow-x:auto;
            -webkit-overflow-scrolling:touch;
            -ms-overflow-style:-ms-autohiding-scrollbar
        }
        .bg-dark,.bg-dark a{
            color:#fff!important
        }
        .p-2{
            padding:.5rem!important
        }
        .bg-dark{
            background-color:#343a40!important
        }.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{
                     position:relative;
                     width:100%;
                     min-height:1px;
                     padding-right:7.5px;
                     padding-left:7.5px
                 }
        .row{
            display:flex;
            flex-wrap:wrap;
            margin-right:-7.5px;
            margin-left:-7.5px
        }
        .no-gutters{
            margin-right:0;
            margin-left:0
        }
        .no-gutters>.col,.no-gutters>[class*=col-]{
            padding-right:0;
            padding-left:0
        }
        *,::after,::before{
            box-sizing:border-box
        }
        .text-right{
            text-align:right!important
        }
        .table{
            width:100%;
            max-width:100%;
            background-color:transparent
        }
        table{
            border-collapse:collapse
        }
        .table td,.table th{
            padding:.75rem;
            vertical-align:top;
            border-top:1px solid #dee2e6
        }
        th{
            text-align:inherit
        }
        .table-responsive>.table-bordered{
            border:0
        }
        .table-bordered,.table-bordered td,.table-bordered th{
            border:1px solid #dee2e6
        }
        .table-sm td,.table-sm th{
            padding:.3rem
        }
        .table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{
            border:none
        }
        .table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{
            border:0
        }
        .text-center{
            text-align:center!important
        }
        .mr-2,.mx-2{
            margin-right:.5rem!important
        }
        .text-uppercase{
            text-transform:uppercase!important
        }
        .resource-col{
            display: inline-block;
            margin-top: 5px;
        }
        .resource-col-3{
            width: 33%;
        }
        .resource-col-2{
            width: 48%;
            margin-right: 15px
        }
        .resource-p-m-3{
            padding: 3px 10px;
            margin-top: 20px;
        }
        .resource-signature{
            border: 1px solid #212529;
            height: 40px;
            margin-bottom: 10px;
        }
        @media (min-width:768px){
            .table-responsive{
                overflow-x:inherit!important
            }
            .col-md-4{
                flex:0 0 33.333333%;
                max-width:33.333333%
            }
            .col-md-2{
                flex:0 0 16.666667%;
                max-width:16.666667%
            }
            .col-md-5{
                flex:0 0 41.666667%;
                max-width:41.666667%
            }
            .col-md-6{
                flex:0 0 50%;
                max-width:50%
            }
            .col-md-1{
                flex:0 0 8.333333%;
                max-width:8.333333%
            }
        }
    </style>

</head>
    <body class="hold-transition sidebar-mini">
        <div id="app" class="wrapper">
            <div class="container">
                <h3>{{isset($timesheet->company)?$timesheet->company->company_name:''}}</h3>
                <hr />
                    <div class="row bg-dark p-2 no-gutters">
                        <div class="col-md-12">Weekly Timesheet ({{ isset($timesheet->project) ? $timesheet->project->ref : ''}})</div>
                        @php
                            $date = new DateTime($timesheet->last_day_of_month);

                            $total_time = 0;
                            $total_mileage = 0;
                            function minutesToTime($minutes){
                                $hours = floor($minutes/60);
                                $minutes = $minutes % 60;
                                $negation_number = '';
                                if($hours < 0){
                                    $hours *= -1;
                                    $negation_number = '-';
                                }

                                if($minutes < 0){
                                    $minutes *= -1;
                                    $negation_number = '-';
                                }
                                $hour_str = $hours < 10 ? '0'.$hours : $hours;
                                $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                                return $negation_number.$hour_str.':'.$minutes_str;
                            }
                        @endphp
                        <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
                        <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
                    </div>
                    <div class="row no-gutters p-2">
                        <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
                    </div>
                    <table class="table" border="1">
                        <tbody>
                        <tr class="bg-dark">
                            <th>Client</th>
                            <th>Project Name</th>
                            <th>Description of Work</th>
                            <th>Bill</th>
                            @for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                                @if($i > $timesheet->last_date_of_month)
                                    @break
                                @endif

                                @php
                                    $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                @endphp

                                <th>{{$date->format('D')}} {{$i}}</th>
                            @endfor
                            <th>TOTAL</th><th>Travel (Mileage)</th>
                        </tr>
                        @foreach($timelines as $timeline)
                            @php $total_minutes = 0; @endphp
                            <tr>
                                <td>{{isset($timesheet->customer->customer_name) ? $timesheet->customer->customer_name : ''}}</td>
                                <td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td>
                                <td>{{(isset($timeline->task->description))?($timeline->task->description.(isset($timeline->description_of_work)?' - ':''). $timeline->description_of_work):$timeline->description_of_work }}</td>
                                <td>{{$timeline->is_billable == 1 ? 'Yes' : 'No'}}</td>
                                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                                    @if($i > $timesheet->last_date_of_month)
                                        @break
                                    @endif
                                    @php
                                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                        $total_minutes += $timeline[strtolower($date->format('D'))] * 60 + $timeline[strtolower($date->format('D')).'_m'];
                                    @endphp
                                    <td>{{minutesToTime($timeline[strtolower($date->format('D'))] * 60 + $timeline[strtolower($date->format('D')).'_m'])}}</td>
                                @endfor
                                @php
                                    $total_time += $total_minutes;
                                    $total_mileage += $timeline->mileage;
                                @endphp
                                <td>{{minutesToTime($total_minutes)}}</td><td>{{$timeline->mileage}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">TOTAL</td>
                            @php
                                $day_number = 1;
                            @endphp
                            @for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                                @if($i > $timesheet->last_date_of_month)
                                    @break
                                @endif

                                @php
                                    $hour = 0;
                                    $minute = 0;
                                    foreach($timelines as $timeline):
                                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                        $_hour = $timeline[strtolower($date->format('D'))];
                                        $_minute = $timeline[strtolower($date->format('D')).'_m'];
                                        $hour += $_hour;
                                        $minute += $_minute;
                                    endforeach;
                                @endphp

                                <td>{{minutesToTime($hour * 60 + $minute)}}</td>
                            @endfor
                            <td>{{minutesToTime($total_time)}}</td><td>{{$total_mileage}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">TOTAL IN DECIMAL</td>
                            @php
                                $day_number = 1;
                            @endphp
                            @for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                                @if($i > $timesheet->last_date_of_month)
                                    @break
                                @endif

                                <td>{{number_format((($hour * 60 + $minute)/60),2,',','.')}}</td>
                            @endfor
                            <td>{{number_format(($total_time/60),2,',','.')}}</td><td>{{$total_mileage}}</td>
                        </tr>
                        </tbody>
                    </table>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant']))
                        <table class="table table-bordered table-sm">
                            <thead class="btn-dark">
                            <tr class="bg-dark">
                                <th colspan="7">Expenses</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Date</td>
                                <td>Description</td>
                                <td>Account / *Element</td>
                                <td>Status</td>
                                <td>Bill</td>
                                <td>Claim</td>
                                <td>Amount</td>
                            </tr>
                            @forelse($time_exp as $expense)
                                <tr>
                                    <td>{{$expense->date}}</td>
                                    <td>{{$expense->description}}</td>
                                    <td>{{isset($expense->account)? (isset($expense->account->description)?$expense->account->description:''): ''.' / '. (isset($expense->account_element) ? (isset($expense->account_element->description)?$expense->account_element->description:''):'')}}</td>
                                    <td>{{($expense->paid_date == null)?'Un Paid':'Paid'}}</td>
                                    <td>{{ $expense->is_billable->value == 0 ? 'No' : 'Yes' }}</td>
                                    <td>{{ $expense->claim->value == 0 ? 'No' : 'Yes' }}</td>
                                    <td>{{number_format($expense->amount,2,'.',',')}}</td>
                                </tr>
                            @empty
                            @endforelse
                            <tr>
                                <td colspan="6" class="text-uppercase">total to be billed to client</td>
                                <td>{{number_format($total_exp_bill,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="6" class="text-uppercase">total claimed by consultant</td>
                                <td>{{number_format($total_exp_claim,2,'.',',')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    @endif
                    {{--<table class="table table-borderless">
                        <tbody>
                        <tr>
                            <td colspan="4" class="text-center mr-2">
                                @if($timesheet->timesheet_lock != 1)
                                    @if(Auth::user()->hasAnyRole(['admin','admin_manager','manager','consultant','contractor']))
                                        <a href="{{ route('timesheet.createtimeline', $timesheet) }}" class="btn btn-sm btn-dark  mr-2"><i class="fa fa-clock-o" aria-hidden="true"></i> Add Time</a>
                                        <a href="{{route('timesheet.createexpense', $timesheet)}}" class="btn btn-sm btn-dark  mr-2"><i class="fa fa-money" aria-hidden="true"></i> Add Expense</a>
                                        <a href="{{route('timesheet.edittimeline', [$timesheet, 0])}}" class="btn btn-sm btn-dark  mr-2"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Time</a>
                                        <a href="{{route('timesheet.editexpense', [$timesheet, 0])}}" class="btn btn-sm btn-dark  mr-2"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Expenses</a>
                                    @endif
                                    <a href="{{route('timesheet.print', $timesheet)}}" class="btn btn-sm btn-dark"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                                @else
                                    <a href="{{route('timesheet.print', $timesheet)}}" target="_blank" class="btn btn-sm btn-dark"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead class="btn-dark">
                        <tr class="bg-dark">
                            <th colspan="3" class="text-center">Attachment(s) for this Timesheet</th>
                        </tr>
                        <tr class="bg-dark">
                            <th>Date</th>
                            <th>Description</th>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td colspan="3" class="text-center"><a href="" class="btn btn-sm btn-dark"><i class="fa fa-paperclip" aria-hidden="true"></i> New Attachment</a></td>
                        </tr>
                        </tbody>
                    </table>

                    <a href="" class="btn btn-sm btn-dark mb-3"><i class="fa fa-chevron-left" aria-hidden="true"></i> <i class="fa fa-calendar" aria-hidden="true"></i> Navigate Timesheet</a>

                    @permission(['timesheet_daily_activity_1','timesheet_daily_activity_2','timesheet_daily_activity_3','timesheet_daily_activity_4','timesheet_daily_activity_5','timesheet_daily_activity_la_1','timesheet_daily_activity_la_2','timesheet_daily_activity_la_3','timesheet_daily_activity_la_4','timesheet_daily_activity_la_5','timesheet_daily_activity_lo_1','timesheet_daily_activity_lo_2','timesheet_daily_activity_lo_3','timesheet_daily_activity_lo_4','timesheet_daily_activity_lo_5','timesheet_daily_activity_or_1','timesheet_daily_activity_or_2','timesheet_daily_activity_or_3','timesheet_daily_activity_or_4','timesheet_daily_activity_or_5','timesheet_daily_activity_ar_1','timesheet_daily_activity_ar_2','timesheet_daily_activity_ar_3','timesheet_daily_activity_ar_4','timesheet_daily_activity_ar_5'])
                    <table class="table table-bordered table-sm">
                        <thead class="btn-dark">
                        <tr class="bg-dark">
                            <th colspan="6" class="text-center">Daily Activity for week: {{$timesheet->year_week}}</th>
                        </tr>
                        <tr class="bg-dark">
                            <th>Date</th>
                            <th>Activity</th>
                            <th>Next</th>
                            <th>Issues/Risks</th>
                            <th>Hours</th>
                            <th>Send</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    {{Form::open(['url' =>'', 'method' => 'post','class'=>'mt-3'])}}
                    <table class="table table-bordered table-sm">
                        <tbody>
                        <tr>
                            <th style="width: 20%">Date</th>
                            <td style="width: 30%">{{Form::text('date',date('Y-m-d'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('date') ? ' is-invalid' : ''),'placeholder'=>'Date'])}}
                                @foreach($errors->get('date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Group Task No</th>
                            <td>{{Form::select('group_task_no',$timelines_drop_down, 0,['class'=>'form-control form-control-sm'. ($errors->has('group_task_no') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('group_task_no') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Activity</th>
                            <td>{{Form::textarea('activity',old('activity'),['class'=>'form-control form-control-sm'. ($errors->has('activity') ? ' is-invalid' : ''),'placeholder'=>'Activity', 'rows'=> 3])}}
                                @foreach($errors->get('activity') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Hours</th>
                            <td>{{Form::select('hours',$hours, null,['class'=>'form-control form-control-sm'. ($errors->has('hours') ? ' is-invalid' : ''), 'placeholder' => 'Select Hours'])}}
                                @foreach($errors->get('hours') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Next</th>
                            <td>{{Form::textarea('next',old('next'),['class'=>'form-control form-control-sm'. ($errors->has('next') ? ' is-invalid' : ''),'placeholder'=>'Next', 'rows'=> 3])}}
                                @foreach($errors->get('next') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Send</th>
                            <td>{{Form::select('send',[0 => 'No', 1 => 'Yes'], 0,['class'=>'form-control form-control-sm'. ($errors->has('send') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('send') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Issue / Risk</th>
                            <td colspan="3">{{Form::textarea('issue_risk',old('issue_risk'),['class'=>'form-control form-control-sm'. ($errors->has('issue_risk') ? ' is-invalid' : ''),'placeholder'=>'Issue/Risk', 'rows'=> 3])}}
                                @foreach($errors->get('issue_risk') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-borderless">
                        <tbody>
                        <tr>
                            <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a type="reset" href="{{route('timesheet.index')}}" class="btn btn-danger btn-sm">Cancel</a></td>
                        </tr>
                        </tbody>
                    </table>
                    {{Form::close()}}
                    @endpermission--}}

                {{--@foreach($digisign_users as $digisign_user)
                    @php $type = ''; @endphp
                    @if($digisign_user->user_number == 1)
                        @php
                            $type = 'Resource';
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 2)
                        @php
                            $type = $assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner';
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 3)
                        @php
                            $type = $assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner';
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 4)
                        @php
                            $type = $assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager';
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 5)
                        @php
                            $type = $assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager';
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 6)
                        @php
                            $type = $assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver'
                        @endphp
                    @endif
                    @if($digisign_user->user_number == 7)
                        @php
                            $type = $assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager'
                        @endphp
                    @endif

                    @if($digisign_user->user_number > 7)
                        @php $type = 'Name'; @endphp
                    @endif

                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$type}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;" class="signature">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;">{{$digisign_user->signed == 1 ? $digisign_user->user_sign_date : ''}}</th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>

                @endforeach--}}


                {{--@if($assignment->project_manager_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif
                @if($assignment->product_owner_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif
                @if($assignment->project_manager_new_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif
                @if($assignment->line_manager_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif
                @if($assignment->claim_approver_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif
                @if($assignment->resource_manager_ts_approver == 1)
                    <br/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th style="width: 170px; border: 1px solid transparent">{{$assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager'}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                @endif--}}

                    {{--<table class="table">
                        <tr>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border-top: 1px solid transparent; border-bottom: 1px solid #343a40">&nbsp;</td>
                            <td style="border-top: 1px solid transparent; border-bottom: 1px solid #343a40 ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent">&nbsp;</td>
                            <td style="border-top: 1px solid transparent; border-bottom: 1px solid #343a40 ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent; border-bottom: 1px solid #343a40 ">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border-top: 1px solid transparent ">Approval</td>
                            <td style="border-top: 1px solid transparent; text-align: right ">Date</td>
                            <td style="border-top: 1px solid transparent ">&nbsp;</td>
                            <td style="border-top: 1px solid transparent ">Resource</td>
                            <td style="border-top: 1px solid transparent; text-align: right ">Date</td>
                        </tr>
                    </table>--}}

                @foreach($digisign_users as $digi_sign)
                    @if(sizeof($digisign_users) >= 3)
                        <div class="resource-col resource-col-3">
                            <div class="bg-dark resource-p-m-3">
                                @switch($digi_sign->user_number)
                                    @case(1)
                                        <strong>Resource</strong>
                                        @break
                                    @case(2)
                                        <strong>Internal Owner</strong>
                                        @break
                                    @case(3)
                                        <strong>Product Owner</strong>
                                        @break
                                    @case(4)
                                        <strong>Project Manager</strong>
                                        @break
                                    @case(5)
                                        <strong>Line Manager</strong>
                                        @break
                                    @case(6)
                                        <strong>Claim Approver</strong>
                                        @break
                                    @case(7)
                                        <strong>Resource Manager</strong>
                                        @break
                                @endswitch
                            </div>
                            <div class="resource-signature">

                            </div>
                            <strong>{{$digi_sign->user??null}}</strong>
                        </div>
                    @elseif(sizeof($digisign_users) == 2)
                        <div class="resource-col resource-col-2">
                            <div class="bg-dark resource-p-m-3">
                                @switch($digi_sign->user_number)
                                    @case(1)
                                    <strong>Resource</strong>
                                    @break
                                    @case(2)
                                    <strong>Internal Owner</strong>
                                    @break
                                    @case(3)
                                    <strong>Product Owner</strong>
                                    @break
                                    @case(4)
                                    <strong>Project Manager</strong>
                                    @break
                                    @case(5)
                                    <strong>Line Manager</strong>
                                    @break
                                    @case(6)
                                    <strong>Claim Approver</strong>
                                    @break
                                    @case(7)
                                    <strong>Resource Manager</strong>
                                    @break
                                @endswitch
                            </div>
                            <div class="resource-signature">

                            </div>
                            <strong>{{$digi_sign->user??null}}</strong>
                        </div>
                    @else
                        <div class="bg-dark resource-p-m-3">
                            @switch($digi_sign->user_number)
                                @case(1)
                                <strong>Resource</strong>
                                @break
                                @case(2)
                                <strong>Internal Owner</strong>
                                @break
                                @case(3)
                                <strong>Product Owner</strong>
                                @break
                                @case(4)
                                <strong>Project Manager</strong>
                                @break
                                @case(5)
                                <strong>Line Manager</strong>
                                @break
                                @case(6)
                                <strong>Claim Approver</strong>
                                @break
                                @case(7)
                                <strong>Resource Manager</strong>
                                @break
                            @endswitch
                        </div>
                        <div class="resource-signature">

                        </div>
                        <strong>{{$digi_sign->user??null}}</strong>
                    @endif
                @endforeach
            </div>
        </div>
    </body>
</html>
