@extends('adminlte.default')
@section('title') View Timesheet @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet ({{ isset($timesheet->project) ? $timesheet->project->ref : ''}})</div>
                @php
                    $date = new DateTime($timesheet->last_day_of_month);

                    $total_time = 0;
                    $total_mileage = 0;
                    function minutesToTime($minutes){
                        $hours = floor($minutes/60);
                        $minutes = $minutes % 60;
                        $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                        $hour_str = $hours < 10 ? '0'.$hours : $hours;
                        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                        return $negation_number.$hour_str.':'.$minutes_str;
                    }
                @endphp
                <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} </strong> to {{ '('.$date->format('D').'): '}} <strong> {{  $timesheet->last_day_of_week }} </strong> </div>
                <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-5">Hours Available on Assignment {{$project_name}}: {{ minutesToTime($assignment_hours_left)}}</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
            </div>
            <table class="table" border="1">
                <tbody>
                <tr class="bg-dark">
                    <th>Client</th>
                    <th>Project Name</th>
                    <th>Description of Work</th>
                    <th>Bill</th>
                    {{--@for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        @endphp

                        <th>{{$date->format('D')}} {{$i}}</th>
                    @endfor--}}
                    @foreach($week_days as $day)
                        <th>{{$day}}</th>
                    @endforeach
                    <th>TOTAL</th><th>Travel (Mileage)</th>
                </tr>
                @foreach($timelines as $timeline)
                    @php
                        $total_minutes = 0;
                        $description_of_work = isset($timeline->task->description)?$timeline->task->description:'';
                        if($description_of_work != ''){
                            if($timeline->description_of_work != '')
                                $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                        else{
                            $description_of_work = $timeline->description_of_work;
                        }
                    @endphp
                    <tr>
                        <td>{{isset($timesheet->customer->customer_name) ? $timesheet->customer->customer_name : ''}}</td>
                        <td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td>
                        <td>{{ $description_of_work }}</td>
                        <td>{{$timeline->is_billable == 1 ? 'Yes' : 'No'}}</td>
                        {{--@for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                            <td>{{$i}}</td>
                            @if($i > $timesheet->last_date_of_month)
                                @break
                            @endif
                            @php
                                $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                $total_minutes += $timeline[strtolower($date->format('D'))] * 60 + $timeline[strtolower($date->format('D')).'_m'];
                            @endphp
                            <td>{{minutesToTime($timeline[strtolower($date->format('D'))] * 60 + $timeline[strtolower($date->format('D')).'_m'])}}</td>
                        @endfor
                        @php
                            $total_time += $total_minutes;
                            $total_mileage += $timeline->mileage;
                        @endphp--}}

                        @foreach($week_days as $day)
                            <td>{{minutesToTime(($timeline[strtolower(substr($day, 0, 3))]*60) + $timeline[strtolower(substr($day, 0, 3)).'_m'])}}</td>
                        @endforeach
                        <td>{{minutesToTime(($timeline->total*60)+$timeline->total_m)}}</td><td>{{$timeline->mileage}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">TOTAL</td>
                    @php
                        $day_number = 1;
                    @endphp
                    {{--@for($i = $timesheet->first_date_of_week; $i <= $timesheet->last_date_of_week; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $hour = 0;
                            $minute = 0;
                            foreach($timelines as $timeline):
                                $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                $_hour = $timeline[strtolower($date->format('D'))];
                                $_minute = $timeline[strtolower($date->format('D')).'_m'];
                                $hour += $_hour;
                                $minute += $_minute;
                            endforeach;
                        @endphp

                        <td rowspan="2">{{minutesToTime($hour * 60 + $minute)}}</td>
                    @endfor--}}
                    @foreach($week_days as $day)
                        <td>{{minutesToTime(($timelines->sum(strtolower(substr($day, 0, 3)))*60)+$timelines->sum(strtolower(substr($day, 0, 3)).'_m'))}}</td>
                    @endforeach
                    <td>{{minutesToTime(($timelines->sum('total')*60) + $timelines->sum('total_m'))}}</td><td>{{$total_mileage}}</td>
                </tr>
                </tbody>
            </table>
            @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor']))
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr class="bg-dark">
                        <th colspan="7">Expenses</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Date</td>
                        <td>Description</td>
                        <td>Account / *Element</td>
                        <td>Status</td>
                        <td>Bill</td>
                        <td>Claim</td>
                        <td>Amount</td>
                    </tr>
                    @forelse($time_exp as $expense)
                        <tr>
                            <td>{{$expense->date}}</td>
                            <td>{{$expense->description}}</td>
                            <td>{{$expense->account?->description.' / '.$expense->account_element?->description}}</td>
                            <td>{{($expense->paid_date == null)?'Un Paid':'Paid'}}</td>
                            <td>{{($expense->is_billable == 0)?'No':'Yes'}}</td>
                            <td>{{($expense->claim == 0)?'No':'Yes'}}</td>
                            <td>{{number_format($expense->amount,2,'.',',')}}</td>
                        </tr>
                        @empty 
                    @endforelse
                    <tr>
                        <td colspan="6" class="text-uppercase">total to be billed to client</td>
                        <td>{{number_format($total_exp_bill,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="text-uppercase">total claimed by consultant</td>
                        <td>{{number_format($total_exp_claim,2,'.',',')}}</td>
                    </tr>
                </tbody>
            </table>
            @endif
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center mr-2">
                            @if($timesheet->timesheet_lock != 1)
                                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor']))
                                <a href="{{ route('timesheet.createtimeline', $timesheet) }}" class="btn btn-sm btn-primary  mr-2"><i class="fas fa-business-time"></i> Add Time</a>
                                <a href="{{route('timesheet.edittimeline', [$timesheet, 0])}}" class="btn btn-sm btn-success  mr-5"><i class="fas fa-edit"></i> Edit Time</a>
                                <a href="{{route('timesheet.createexpense', $timesheet)}}" class="btn btn-sm btn-primary  mr-2"><i class="fas fa-money-check-alt"></i> Add Expense</a>
                                <a href="{{route('timesheet.editexpense', [$timesheet, 0])}}" class="btn btn-sm btn-success  mr-5"><i class="fas fa-edit"></i> Edit Expenses</a>
                                @endif
                                <a href="{{route('timesheet.show',['timesheet' => $timesheet, 'print' => 1])}}" class="btn btn-sm btn-dark" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                                <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Digisign</a>
                                <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal2"><i class="fas fa-envelope"></i> Email</a>
                            @else
                                <a href="{{route('timesheet.show',['timesheet' => $timesheet->id, 'print' => 1])}}" target="_blank" class="btn btn-sm btn-dark"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                                <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Digisign</a>
                                <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal2"><i class="fas fa-envelope"></i> Email</a>
                            @endif
                        </td>
                        <td class="text-center mr-2" style="vertical-align: top;">
                            <form class="form-inline searchform" id="searchform">
                            {{Form::select('cfts_tid',[1 => 'Standard Template', 2 => 'Weekly Template', 3 => 'Arbour Template'], $timesheet_template_id,['id' => 'cfts_tempalte_id', 'class'=>'form-control form-control-sm  search'. ($errors->has('cfts_tid') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('cfts_tid') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>

            @isset($billing_cycle_id)
                <blackboard-combined-timesheets
                        employee-id="{{$timesheet->employee_id}}"
                        project-id="{{$timesheet->project_id}}"
                        custom-template="{{$use_custom_template}}"
                        billing-cycle-id="{{$billing_cycle_id}}"
                        billing-period-id="{{$current_billing_period_id}}">
                </blackboard-combined-timesheets>
            @endisset
            <table class="table" border="1">
                <thead>
                <tr class="bg-dark">
                    <th colspan="13" class="text-center">More Timesheets for this Project</th>
                </tr>
                </thead>
                <tbody>
                <tr class="bg-dark">
                    <th>Week No</th><th>Date</th><th>Bill</th><th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                    <th>Sun</th>
                    <th>Total</th>
                    <th>Travel</th>
                    <th class="text-right">Action</th>
                </tr>
                @forelse($project_timelines as $tline)
                    <tr class="{{!$tline->is_billable?'bg-secondary':null}}">
                        <td>{{substr(($tline->year_week??"0000"), 0, 4)}}-{{substr(($tline->year_week??"00"), 4, 2)}}</td>
                        <td>{{$tline->first_day_of_week??"0000-00-00"}}</td>
                        <td>{{($tline->is_billable == 1)?"Yes":"No"}}</td>
                        <td>{{substr($tline->monday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->tuesday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->wednesday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->thursday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->friday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->saturday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->sunday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->total??"00:00",0,5)}}</td>
                        <td class="text-right">{{$tline->travel??0}}</td>
                        <td class="text-right">
                            <a class="btn btn-info btn-sm" href="{{route('timesheet.show',$tline->id)}}">View</a>
                            @if($tline->timesheet_lock != 1)
                                {{--@if($have_timelines[$timesheet->id])
                                    <a href="{{route('timesheet.edittimeline', [$timesheet, 0])}}" class="btn btn-success btn-sm">Edit</a>
                                @endif--}}
                                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                                    @if($tline->timeline_id)
                                        <a href="{{route('timesheet.edittimeline',[$timesheet, explode(",",$tline->timeline_id)[0]])}}" class="btn btn-success btn-sm">Edit</a>
                                        {{ Form::open(['method' => 'DELETE','route' => ['timesheet.destroytimeline', $tline->id],'style'=>'display:inline','class'=>'delete']) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                        {{ Form::close() }}
                                    @else
                                        <a href="{{route('timesheet.createtimeline', $tline->id)}}" class="btn btn-success btn-sm">Edit</a>
                                        {{ Form::open(['method' => 'DELETE','route' => ['timesheet.destroy', $tline->id],'style'=>'display:inline','class'=>'delete']) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                        {{ Form::close() }}
                                    @endif
                                @endif
                            @else
                                <span class="badge badge-dark px-4 py-2"><i class="fas fa-lock"></i></span>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="13" class="text-center">You have no previous timesheets on this project</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr class="bg-dark">
                        <th colspan="3" class="text-center">Attachment(s) for this Timesheet</th>
                    </tr>
                    <tr class="bg-dark">
                        <th>Date</th><th>Description</th><th>File</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($attachments as $attachment)
                        <tr>
                            <td>{{$attachment->created_at}}</td><td>{{$attachment->name}}</td><td><a href="{{route('getattachment', $attachment->file)}}" target="_blank">View</a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="text-center"><a href="{{route('timesheet.addattachment', $timesheet)}}" class="btn btn-sm btn-dark"><i class="fa fa-paperclip" aria-hidden="true"></i> New Attachment</a></td>
                    </tr>
                </tbody>
            </table>

            <a href="{{route('timesheet.show', $previous_timesheet_id)}}" class="btn btn-sm btn-dark mb-3"><i class="fa fa-chevron-left" aria-hidden="true"></i> <i class="fa fa-calendar" aria-hidden="true"></i> Navigate Timesheet</a> <a href="{{route('timesheet.show', $next_timesheet_id)}}" class="btn btn-sm btn-dark mb-3"> Navigate Timesheet <i class="fa fa-calendar" aria-hidden="true"></i> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Select Emails</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => route('timesheet.senddigisign', $timesheet->id), 'method' => 'post']) !!}
                        <div class="form-group row">
                            <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                            <div class="col-sm-10">
                                <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                            </div>
                        </div>
                        <hr>

                        <div class="form-check">
                            <input type="checkbox" name="resource_email" value="{{$employee->email}}" checked class="form-check-input" id="resource_email">
                            <label class="form-check-label" for="resource_email"><b>Resource</b>: {{$employee->first_name.' '.$employee->last_name}} - {{$employee->email}}</label>
                        </div>

                        @if($assignment->project_manager_ts_approver == 1 && isset($project_manager_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="project_manager_email" value="{{$project_manager_ts_user->email}}" checked class="form-check-input" id="project_manager_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner'}}</b>: {{$project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name}} - {{$project_manager_ts_user->email}}</label>
                            </div>
                        @endif

                        @if($assignment->product_owner_ts_approver == 1 && isset($project_owner_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="product_owner_email" value="{{$project_owner_ts_user->email}}" checked class="form-check-input" id="product_owner_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner'}}</b>: {{$project_owner_ts_user->first_name.' '.$project_owner_ts_user->last_name}} - {{$project_owner_ts_user->email}}</label>
                            </div>
                        @endif

                        @if($assignment->project_manager_new_ts_approver == 1 && isset($project_manager_new_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="project_manager_new_email" value="{{$project_manager_new_ts_user->email}}" checked class="form-check-input" id="project_manager_new_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager'}}</b>: {{$project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name}} - {{$project_manager_new_ts_user->email}}</label>
                            </div>
                        @endif

                        @if($assignment->line_manager_ts_approver == 1 && isset($line_manager_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="line_manager_email" value="{{$line_manager_ts_user->email}}" checked class="form-check-input" id="line_manager_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager'}}</b>: {{$line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name}} - {{$line_manager_ts_user->email}}</label>
                            </div>
                        @endif

                        @if($assignment->claim_approver_ts_approver == 1 && isset($claim_approver_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="claim_approver_email" value="{{$claim_approver_ts_user->email}}" checked class="form-check-input" id="claim_approver_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver'}}</b>: {{$claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name}} - {{$claim_approver_ts_user->email}}</label>
                            </div>
                        @endif

                        @if($assignment->resource_manager_ts_approver == 1 && isset($resource_manager_ts_user->email))
                            <div class="form-check">
                                <input type="checkbox" name="resource_manager_email" value="{{$resource_manager_ts_user->email}}" checked class="form-check-input" id="resource_manager_email">
                                <label class="form-check-label" for="product_owner_email"><b>{{$assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager'}}</b>: {{$resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name}} - {{$resource_manager_ts_user->email}}</label>
                            </div>
                        @endif

                        {{--<div class="form-check">
                            <input type="checkbox" name="resource_email" value="{{$employee->email}}" checked class="form-check-input" id="resource_email">
                            <label class="form-check-label" for="resource_email">{{$employee->first_name.' '.$employee->last_name}} ({{$employee->email}})</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" name="report_to_email" value="{{$assignment->report_to_email}}" checked class="form-check-input" id="report_to_email">
                            <label class="form-check-label" for="report_to_email">{{$assignment->report_to}} ({{$assignment->report_to_email}})</label>
                        </div>
                        @if(isset($product_ownder))
                        <div class="form-check">
                            <input type="checkbox" name="product_owner_email" value="{{$product_ownder->email}}" checked class="form-check-input" id="product_owner_email">
                            <label class="form-check-label" for="product_owner_email">{{$product_ownder->first_name.' '.$product_ownder->last_name}} ({{$product_ownder->email}})</label>
                        </div>
                        @endif--}}
                        <hr>
                        <div class="form-group row">
                            <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel2">Select Emails or Enter An Email</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => route('timesheet.emailtimesheet', $timesheet->id), 'method' => 'post']) !!}
                        <div class="form-group row">
                            <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                            <div class="col-sm-10">
                                <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject_send_email}}" placeholder="Email Subject" required>
                            </div>
                        </div>
                        {{--<hr>
                        <div class="form-check">
                            <input type="checkbox" name="resource_email" value="{{$employee->email}}" checked class="form-check-input" id="resource_email">
                            <label class="form-check-label" for="resource_email">{{$employee->first_name.' '.$employee->last_name}} ({{$employee->email}})</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" name="report_to_email" value="{{$assignment->report_to_email}}" checked class="form-check-input" id="report_to_email">
                            <label class="form-check-label" for="report_to_email">{{$assignment->report_to}} ({{$assignment->report_to_email}})</label>
                        </div>
                        @if(isset($product_ownder))
                        <div class="form-check">
                            <input type="checkbox" name="product_owner_email" value="{{$product_ownder->email}}" checked class="form-check-input" id="product_owner_email">
                            <label class="form-check-label" for="product_owner_email">{{$product_ownder->first_name.' '.$product_ownder->last_name}} ({{$product_ownder->email}})</label>
                        </div>
                        @endif--}}
                        <hr>
                        <div class="form-group row">
                            <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email" required>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            {!! Form::submit('Send Email', ['class' => 'btn btn-sm btn-dark']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('extra-js')
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        function goBack() {
            window.history.back();
        }
        $(function () {
            $('#user_email').multiple_emails({
                position: 'top', // Display the added emails above the input
                theme: 'bootstrap', // Bootstrap is the default theme
                checkDupEmail: true // Should check for duplicate emails added
            });

            $('#current_emails').text($('#user_email').val());

            $('#user_email').change( function(){
                $('#current_emails').text($(this).val());
            });
        })
    </script>

@endsection

@section('extra-css')
    <style>
        @media print {
            .col-print-4 {width:33%; float:left;}
            .col-print-12{width:100%; float:left;}
        }
    </style>
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
@endsection
