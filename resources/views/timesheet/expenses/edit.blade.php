@extends('adminlte.default')

@section('title') Edit Expense @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="row bg-dark p-2 no-gutters">
            <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
            @php
                $date = new DateTime($timesheet->last_day_of_month);

                $total_time = 0;
                $total_mileage = 0;
                function minutesToTime($minutes){
                    $hours = floor($minutes/60);
                    $minutes = $minutes % 60;
                    $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                    $hour_str = $hours < 10 ? '0'.$hours : $hours;
                    $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                    return $negation_number.$hour_str.':'.$minutes_str;
                }
            @endphp
            <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
            <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
        </div>
        <div class="row no-gutters p-2">
            <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
        </div>
        <table class="table" border="1">
            <tbody>
            <tr class="bg-dark">
                <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                    @if($i > $timesheet->last_date_of_month)
                        @break
                    @endif

                    @php
                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                    @endphp

                    <th>{{$date->format('D')}} {{$i}}</th>
                @endfor
                <th>TOTAL</th><th>Travel (Mileage)</th>
            </tr>
            @foreach($timelines as $timeline)
                <tr>
                    <td>{{$timesheet->customer->customer_name}}</td><td>{{$timesheet->project->name}}</td><td>{{$timeline['description_of_work']}}</td><td>{{$timeline['is_billable'] == 1 ? 'Yes' : 'No'}}</td>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif
                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        @endphp
                        <td>{{$timeline[strtolower($date->format('D'))]}}:{{$timeline[strtolower($date->format('D')).'_m']}}</td>
                    @endfor
                    @php
                        $total_time += $timeline['total'] * 60 + $timeline['total_m'];
                        $total_mileage += $timeline['mileage'];
                    @endphp
                    <td>{{minutesToTime($timeline['total'] * 60 + $timeline['total_m'])}}</td><td>{{$timeline['mileage']}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4">TOTAL</td>
                @php
                    $day_number = 1;
                @endphp
                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                    @if($i > $timesheet->last_date_of_month)
                        @break
                    @endif

                    @php
                        $hour = 0;
                        $minute = 0;
                        foreach($timelines as $timeline):
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            $_hour = $timeline[strtolower($date->format('D'))];
                            $_minute = $timeline[strtolower($date->format('D')).'_m'];
                            $hour += $_hour;
                            $minute += $_minute;
                        endforeach;
                    @endphp

                    <td rowspan="2">{{minutesToTime($hour * 60 + $minute)}}</td>
                @endfor
                <td>{{minutesToTime($total_time)}}</td><td>{{$total_mileage}}</td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm">
            <thead class="btn-dark">
            <tr class="bg-dark">
                <th colspan="8">Expenses</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Date</td>
                <td>Description</td>
                <td>Account / *Element</td>
                <td>Status</td>
                <td>Bill</td>
                <td>Claim</td>
                <td>Amount</td>
                <td>Actions</td>
            </tr>
            @php
                $total_billed = 0;
                $total_consultant = 0;
            @endphp
            @foreach($expenses as $expense)
                <tr>
                    <td>{{isset($expense->date)?$expense->date:''}}</td>
                    <td>{{isset($expense->description)?$expense->description:''}}</td>
                    <td>{{isset($expense->account_element->description)?$expense->account_element->description:''}}</td>
                    <td>{{isset($expense->status)?$expense->status:''}}</td>
                    <td>{{$expense->is_billable == 1?'Yes':'No'}}</td>
                    <td>{{$expense->claim == 1?'Yes':'No'}}</td>
                    <td>{{isset($expense->amount)?$expense->amount:''}}</td>
                    <td>
                        <a href="{{route('timesheet.editexpense',[$timesheet, $expense->id])}}" class="btn btn-success btn-sm">Edit</a>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['timesheet.destroyexpense', $expense->id], 'style'=>'display:inline', 'class'=>'delete']) }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                        {{ Form::close() }}
                    </td>
                    @php
                        if($expense->is_billable == 1)
                        $total_billed += $expense->amount;
                        if($expense->claim == 1)
                            $total_consultant += $expense->amount;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <td colspan="6" class="text-uppercase">total to be billed to client</td>
                <td>{{number_format($total_billed,2,'.',',')}}</td><td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6" class="text-uppercase">total claimed by consultant</td>
                <td>{{number_format($total_consultant,2,'.',',')}}</td><td>&nbsp;</td>
            </tr>
            </tbody>
        </table>

        {{Form::open(['url' => route('timesheet.updateexpense', $edit_expense), 'method' => 'put','class'=>'mt-3'])}}
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th colspan="4" class="bg-dark">Insert a new expense line in this weekly timesheet.</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th style="width: 20%">Date</th>
                <td style="width: 30%">
                    {{Form::text('date',$edit_expense->date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('date') ? ' is-invalid' : ''),'placeholder'=>'Date'])}}
                    @foreach($errors->get('date') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Billable <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    {{Form::select('is_billable',[0 => 'No', 1 => 'Yes'], $edit_expense->is_billable,['class'=>'form-control form-control-sm'. ($errors->has('is_billable') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('is_billable') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="width: 20%">Claim <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    {{Form::select('claim',[0 => 'No', 1 => 'Yes'],$edit_expense->claim,['class'=>'form-control form-control-sm'. ($errors->has('claim') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('claim') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    {{Form::text('description',$edit_expense->description,['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => 'Description'])}}
                    @foreach($errors->get('description') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="width: 20%">Amount</th>
                <td>
                    {{Form::text('amount',$edit_expense->amount,['class'=>'form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'Amount'])}}
                    @foreach($errors->get('amount') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Account <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    {{Form::select('account_id',$account_dropdown, $edit_expense->account_id,['class'=>'form-control form-control-sm'. ($errors->has('account_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Account', 'id' => 'expense_account'])}}
                    @foreach($errors->get('account_id') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="width: 20%">Client</th>
                <td>
                    {{Form::text('client_id',$timesheet->customer->customer_name,['class'=>'form-control form-control-sm'. ($errors->has('client_id') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                    @foreach($errors->get('client_id') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th>Element <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>
                    {{Form::select('element',$account_elements_drop_down, $edit_expense->account_element_id,['class'=>'form-control form-control-sm'. ($errors->has('element') ? ' is-invalid' : ''),'id' => 'expense_element'])}}
                    @foreach($errors->get('element') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="width: 20%">Project</th>
                <td>
                    {{Form::text('project_id',$timesheet->project->name,['class'=>'datepicker form-control form-control-sm'. ($errors->has('project_id') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                    @foreach($errors->get('project_id') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </td>
                <th></th>
                <td></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
            </tr>
            </tbody>
        </table>
        {{Form::close()}}
    </div>
@endsection
