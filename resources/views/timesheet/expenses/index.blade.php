@extends('adminlte.default')

@section('title') View Task @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('timesheet.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet</div>
                <div class="col-md-4">Date from ( {{ $first_day_week }} ): <strong> {{ $start_week }} </strong> to ( {{ $end_day_week }} ): <strong> {{ $end_week }} </strong> </div>
                <div class="col-md-2 text-right">Wk {{ $yearwk }} </div>
            </div>
            <div class="row no-gutters p-2 border border-bottom">
                <div class="col-md-11 row"><div class="col-md-9">Time Allocation</div><div class="col-md-3">Walkenshaw, Stefan</div></div>
                <div class="col-md-1 text-right">Emp ID: 1</div>
            </div>
            
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr class="bg-dark">
                    <th>Client</th>
                    <th>Project Name</th>
                    <th>Description of Work</th>
                    <th>Bill</th>
                    <th>Mon 20</th>
                    <th>Tue 21</th>
                    <th>Wed 22</th>
                    <th>Thu 23</th>
                    <th>Fri 24</th>
                    <th>Sat 25</th>
                    <th>Sun 26</th>
                    <th>Total</th>
                    <th>Travel (mileage)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>RangeWave</td>
                    <td>Sasol Energy Support 2018_2019</td>
                    <td>Energy Support</td>
                    <td>Yes</td>
                    <td>8:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>8:00</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td colspan="4">TOTAL</td>
                    <td>8:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>0:00</td>
                    <td>8:00</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr class="bg-dark">
                    <th colspan="7">Expenses</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Date</td>
                    <td>Description</td>
                    <td>Account / *Element</td>
                    <td>Status</td>
                    <td>Bill</td>
                    <td>Claim</td>
                    <td>Amount</td>
                </tr>
                <tr>
                    <td colspan="6" class="text-uppercase">total to be billed to client</td>
                    <td>0:00</td>
                </tr>
                <tr>
                    <td colspan="6" class="text-uppercase">total claimed by consultant</td>
                    <td>0:00</td>
                </tr>
                </tbody>
            </table>

            {{Form::open(['url' => route('task.store'), 'method' => 'post','class'=>'mt-3'])}}
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th colspan="4" class="bg-dark">Insert a new expense line in this weekly timesheet.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th style="width: 20%">Date</th>
                            <td style="width: 30%">{{Form::text('date',$today,['class'=>'datepicker form-control form-control-sm'. ($errors->has('date') ? ' is-invalid' : ''),'placeholder'=>'Date'])}}
                                @foreach($errors->get('date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Billable</th>
                            <td>{{Form::select('billable',[0 => 'No', 1 => 'Yes'], 0,['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('billable') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Claim</th>
                            <td>{{Form::select('claim',[0 => 'No', 1 => 'Yes'],0,['class'=>'form-control form-control-sm'. ($errors->has('claim') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('claim') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Description</th>
                            <td>{{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => 'Description'])}}
                                @foreach($errors->get('description') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Amount</th>
                            <td>{{Form::text('amount',old('amount'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'Amount'])}}
                                @foreach($errors->get('amount') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Account</th>
                            <td>{{Form::select('account',$data2, null,['class'=>'form-control form-control-sm'. ($errors->has('account') ? ' is-invalid' : ''), 'placeholder' => 'Select Account', 'id' => 'expense_account'])}}
                                @foreach($errors->get('account') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Client</th>
                            <td>{{Form::text('amount','RangeWave',['class'=>'datepicker form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                                @foreach($errors->get('amount') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th>Element</th>
                            <td>{{Form::select('element',[0 => 'Select Element'], null,['class'=>'form-control form-control-sm'. ($errors->has('element') ? ' is-invalid' : ''),'id' => 'expense_element', 'style' => 'display:none'])}}
                                @foreach($errors->get('element') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                        </tr>
                        <tr>
                            <th style="width: 20%">Project</th>
                            <td>{{Form::text('project','Sasol BOS Phase5',['class'=>'datepicker form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                                @foreach($errors->get('amount') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach</td>
                            <th></th>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<button type="submit" class="btn btn-sm">Cancel</button></td>
                    </tr>
                    </tbody>
                </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
