@extends('adminlte.default')
@section('title') Edit Timesheet @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('timesheet.update', $timesheet), 'method' => 'put','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Create a new weekly timesheet</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Select an Employee <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td style="width: 30%">{{Form::select('employee_id',$employee, $timesheet->employee_id,['class'=>'form-control form-control-sm '. ($errors->has('employee_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('employee_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Select a Week: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td style="width: 30%">{{Form::select('year_week',$week_dropdown, $timesheet->year_week,['class'=>'form-control form-control-sm '. ($errors->has('year_week') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('year_week') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Client: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('client_id',$customer_dropdown, $timesheet->customer_id,['class'=>'form-control form-control-sm '. ($errors->has('client_id') ? ' is-invalid' : ''),'id' => 'client_id'])}}
                        @foreach($errors->get('client_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('project_id',$project_drop_down, $timesheet->project_id,['class'=>'form-control form-control-sm '. ($errors->has('project') ? ' is-invalid' : ''),'id' => 'project_id'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Update</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection