@extends('adminlte.default')

@section('title') Timesheet @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="{{route('timesheet.index')}}" class="btn btn-info float-right" style="margin-left: 5px;">Close</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    
    @php
    $date = new DateTime($timesheet->last_day_of_month);

    $total_time = 0;
    $total_mileage = 0;
    function minutesToTime($minutes){
        $minutes = is_numeric($minutes)?(int) $minutes:0;
        $hours = floor($minutes/60);
        $minutes = $minutes % 60;
        $negation_number = '';
        if($hours < 0){
            $hours *= -1;
            $negation_number = '-';
        }

        if($minutes < 0){
            $minutes *= -1;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
        return $negation_number.$hour_str.':'.$minutes_str;
    }
@endphp
    <blackboard-timesheet 
    :timelines="{{$timelines}}" 
    :timesheet="{{ $timesheet }}" 
    :is-billable="{{ $is_billable }}" 
    :assignment-hours-left="{{ $assignment_hours_left }}"
    :customer-name="{{ json_encode($timesheet->customer->customer_name) }}"
    :project-name="{{ json_encode($project_name) }}"
    :project-ref="{{ json_encode($project_ref) }}"
    :time-exp="{{ $time_exp }}"
    :employee="{{ $employee }}"
    :billing-cycle="{{ $billing_cycle_id }}"
    :custom-template="{{ ($use_custom_template ? $use_custom_template : 0) }}"
    :current-billing-period="{{ $current_billing_period_id }}"
    :template-id="{{ $template_id }}"
    :exp-approver="{{ $is_approver }}"
    :permissions="{{json_encode($permissions)}}"
    :configs="{{$configs}}"></blackboard-timesheet>
</div>
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
        </div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 600px;margin-left:auto;margin-right:auto;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Emails</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => route('timesheet.senddigisign', $timesheet->id), 'method' => 'post']) !!}
                <div class="form-group row">
                    <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                    </div>
                </div>
                <hr>

                <div class="form-check">
                    <input type="checkbox" name="resource_email" value="{{$employee->email}}" checked class="form-check-input" id="resource_email">
                    <label class="form-check-label" for="resource_email"><b>Resource</b>: {{$employee->first_name.' '.$employee->last_name}} - {{$employee->email}}</label>
                </div>

                @if(isset($assignment->project_manager_ts_approver) && $assignment->project_manager_ts_approver == 1 && isset($project_manager_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="project_manager_email" value="{{$project_manager_ts_user->email}}" checked class="form-check-input" id="project_manager_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner'}}</b>: {{$project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name}} - {{$project_manager_ts_user->email}}</label>
                    </div>
                @endif

                @if(isset($assignment->product_owner_ts_approver) && $assignment->product_owner_ts_approver == 1 && isset($project_owner_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="product_owner_email" value="{{$project_owner_ts_user->email}}" checked class="form-check-input" id="product_owner_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner'}}</b>: {{$project_owner_ts_user->first_name.' '.$project_owner_ts_user->last_name}} - {{$project_owner_ts_user->email}}</label>
                    </div>
                @endif

                @if(isset($assignment->project_manager_new_ts_approve) && $assignment->project_manager_new_ts_approver == 1 && isset($project_manager_new_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="project_manager_new_email" value="{{$project_manager_new_ts_user->email}}" checked class="form-check-input" id="project_manager_new_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager'}}</b>: {{$project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name}} - {{$project_manager_new_ts_user->email}}</label>
                    </div>
                @endif

                @if(isset($assignment->line_manager_ts_approver) && $assignment->line_manager_ts_approver == 1 && isset($line_manager_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="line_manager_email" value="{{$line_manager_ts_user->email}}" checked class="form-check-input" id="line_manager_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager'}}</b>: {{$line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name}} - {{$line_manager_ts_user->email}}</label>
                    </div>
                @endif

                @if(isset($assignment->claim_approver_ts_approver) && $assignment->claim_approver_ts_approver == 1 && isset($claim_approver_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="claim_approver_email" value="{{$claim_approver_ts_user->email}}" checked class="form-check-input" id="claim_approver_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver'}}</b>: {{$claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name}} - {{$claim_approver_ts_user->email}}</label>
                    </div>
                @endif

                @if(isset($assignment->resource_manager_ts_approver) && $assignment->resource_manager_ts_approver == 1 && isset($resource_manager_ts_user->email))
                    <div class="form-check">
                        <input type="checkbox" name="resource_manager_email" value="{{$resource_manager_ts_user->email}}" checked class="form-check-input" id="resource_manager_email">
                        <label class="form-check-label" for="product_owner_email"><b>{{$assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager'}}</b>: {{$resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name}} - {{$resource_manager_ts_user->email}}</label>
                    </div>
                @endif

                {{--<div class="form-check">
                    <input type="checkbox" name="resource_email" value="{{$employee->email}}" checked class="form-check-input" id="resource_email">
                    <label class="form-check-label" for="resource_email">{{$employee->first_name.' '.$employee->last_name}} ({{$employee->email}})</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" name="report_to_email" value="{{$assignment->report_to_email}}" checked class="form-check-input" id="report_to_email">
                    <label class="form-check-label" for="report_to_email">{{$assignment->report_to}} ({{$assignment->report_to_email}})</label>
                </div>
                @if(isset($product_ownder))
                <div class="form-check">
                    <input type="checkbox" name="product_owner_email" value="{{$product_ownder->email}}" checked class="form-check-input" id="product_owner_email">
                    <label class="form-check-label" for="product_owner_email">{{$product_ownder->first_name.' '.$product_ownder->last_name}} ({{$product_ownder->email}})</label>
                </div>
                @endif--}}
                <hr>
                <div class="form-group row">
                    <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                    </div>
                </div>
                <hr>
                <div class="form-group text-center">
                    {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

        
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {


            $("#task_id").on('change', () => {
                axios.get('/task-delivery-type/'+$("#task_id").val())
                    .then(res => {
                        $("#task_delivery_type_id").val(res.data.task_delivery_type_id);
                    }).catch(error => {
                    console.log(error.response);
                })
            })
        });

        function minutesToTime(minutes){
            var hours = Math.trunc(minutes/60);
            var minutes = minutes % 60;
            var hour_str = hours < 10 ? '0'+hours : hours;
            var minutes_str = minutes < 10 ? '0'+minutes : minutes;
            return hour_str+':'+minutes_str;
        }

        $("#year_week").on('change', () =>{

            axios.get('/timesheet/checktimesheet?project={{$timesheet->project_id}}&employee={{$timesheet->employee_id}}&year_week='+$("#year_week").val())
                .then(response => {
                    let timesheet = response.data.timesheet;
                    if (timesheet.length){
                        let feedback = "";

                        timesheet.forEach((item, i) => {
                            feedback += "<div class='alert alert-info mt-3'>"
                                + "<p>Week "+item.year_week+" already has a timesheet for project "+item.project+" for "+item.time+" hours</p>"
                                + "<a href='/timesheet/"+item.id+"/createtimeline' class='btn btn-sm btn-dark text-decoration-none'>Edit</a> existing timesheet or <button onclick='storeTimesheet()' class='btn btn-sm btn-dark'>create</button> an addition/new timesheet."
                                + "</div>"
                        })
                        $("#timesheet-exist").html(feedback);
                    }else {
                        storeTimesheet();
                    }
                    //this.timesheet_for_this_week = response.data.timesheet;
                    //console.log(response.data)
                })
                .catch(err => console.log(err.response.data))

        })

        $("#template_id").on('change', () => $("#template_form").submit());

        function storeTimesheet() {
            axios.post('{{route('timesheet.store')}}', {
                employee_id: '{{$timesheet->employee_id}}',
                company_id: '{{$timesheet->company_id}}',
                project_id: '{{$timesheet->project_id}}',
                client_id: '{{$timesheet->customer_id}}',
                year_week: $("#year_week").val(),
                from_timeline: true
            })
                .then(function (response) {
                    window.location = "/timesheet/show/"+response.data.timesheet_id;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    </script>
@endsection
