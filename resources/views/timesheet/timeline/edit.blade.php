@extends('adminlte.default')

@section('title') Edit Timeline @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
                @php
                    $date = new DateTime($timesheet->last_day_of_month);

                    $total_time = 0;
                    $total_mileage = 0;
                    function minutesToTime($minutes){
                        $minutes = is_numeric($minutes)?(int) $minutes:0;
                        $hours = floor($minutes/60);
                        $minutes = $minutes % 60;
                        $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                        $hour_str = $hours < 10 ? '0'.$hours : $hours;
                        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                        return $negation_number.$hour_str.':'.$minutes_str;
                    }
                @endphp
                <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
                <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-5">Hours Available on Assignment {{$project_name}}: {{ minutesToTime($assignment_hours_left)}}</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
            </div>

            {{Form::open(['url' => route('timesheet.updatetimeline', $edit_timeline->id), 'method' => 'put','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="10" class="text-center">Edit Timeline in this weekly timesheet.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Client:</th>
                    <td colspan="5">{{Form::text('client',$timesheet->customer->customer_name,['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th colspan="2">Project:</th>
                    <td colspan="2">
                        {{Form::text('project',$timesheet->project->name,['class'=>'form-control form-control-sm'. ($errors->has('project') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Task:</th>
                    <td colspan="5">
                        <div class="row">
                            <div class="col-md-10">
                                {{Form::select('task_id', $task_drop_down,$edit_timeline->task_id,['class'=>'form-control form-control-sm '. ($errors->has('task_id') ? ' is-invalid' : ''),'placeholder'=>'Select Task', 'id' => 'task_id'])}}
                                @foreach($errors->get('task_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-2 d-flex justify-content-end">
                                <button type="button" class="btn btn-sm btn-dark mr-2" data-toggle="modal" data-target="#new-task">
                                    <i class="fa fa-plus"></i> Add Task
                                </button>
                            </div>
                        </div>
                    </td>
                    <th colspan="2">Description:</th>
                    <td colspan="2">
                        {{Form::text('description_of_work', $edit_timeline->description_of_work, ['class'=>'form-control form-control-sm'. ($errors->has('description_of_work') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="bg-dark">
                    <th>Client Allocation</th>
                    @foreach($week_days as $day)
                        <th style="min-width: 90px; !important;">{{$day}}</th>
                    @endforeach
                    <th>Travel (Mileage)</th><th>Billable</th>
                </tr>
                <tr>
                    <th>Hours</th>
                    @foreach($week_days as $day)
                        <td>
                            {{Form::select(strtolower(substr($day, 0,3)),$hours,$edit_timeline[strtolower(substr($day, 0,3))],['class'=>'form-control form-control-sm '. ($errors->has(strtolower(substr($day, 0,3))) ? ' is-invalid' : '')])}}
                            @foreach($errors->get(strtolower(substr($day, 0,3))) as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    @endforeach
                    <td>
                        {{Form::text('mileage',$edit_timeline->mileage,['class'=>'form-control form-control-sm'. ($errors->has('mileage') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('mileage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td rowspan="2">
                        {{Form::select('is_billable',[0 => 'No', 1 => 'Yes'],$edit_timeline->is_billable,['class'=>'form-control form-control-sm'. ($errors->has('is_billable') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('is_billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Minutes</th>
                    @foreach($week_days as $day)
                        <td>
                            {{Form::select(strtolower(substr($day, 0,3)).'_m',$minutes,$edit_timeline[strtolower(substr($day, 0,3)).'_m'],['class'=>'form-control form-control-sm '. ($errors->has(strtolower(substr($day, 0,3)).'_m') ? ' is-invalid' : '')])}}
                            @foreach($errors->get(strtolower(substr($day, 0,3)).'_m') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    @endforeach
                    <td>
                        {{Form::select('task_delivery_type_id',$task_delivery_type_id, $edit_timeline->task_delivery_type_id,['class'=>'form-control form-control-sm'. ($errors->has('mileage') ? ' is-invalid' : ''), 'id' => 'task_delivery_type_id'])}}
                        @foreach($errors->get('task_delivery_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>

                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center">{!! (count($week_days) < 7 )? '<span style="color: red;">Please note that this is split week for week '.$timesheet->week.' and a timesheet for the remainder of the week should be created if not already created.</span>':'' !!}</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a>&nbsp;<a type="reset" href="{{ route('timesheet.createtimeline', $timesheet) }}" class="btn btn-danger btn-sm">Add New Timeline</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}

            <div class="row text-center mb-4">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    {{ Form::open(['url' => route('timeline.print', $timesheet->id), 'method' => 'post','class'=>'m-0', 'id' => 'template_form']) }}
                    <div class="input-group">
                        {!! Form::select('template_id', [1 => 'Standard Template', 2 => 'Weekly Template', 3 => 'Arbour Template'], $template_id, ['class' => 'form-control form-control-sm', 'id' => 'template_id']) !!}
                        <div class="input-group-append">
                            {!! Form::button('<i class="fas fa-print"></i> Print', ['type' => 'submit', 'class' => 'btn btn-sm btn-dark']) !!}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <div class="col-md-3">
                    {{Form::select('year_week', $year_week_drop_down, null, ['class' => 'form-control form-control-sm', 'style' => 'display:inline-block', 'id' => 'year_week', 'placeholder' => 'Select week for new timesheet'])}}
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-12 m-t-2" id="timesheet-exist"></div>
            </div>

            <table class="table" border="1">
                <tbody>
                <tr class="bg-dark">
                    <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
                    @foreach($week_days as $day)
                        <th>{{$day}}</th>
                    @endforeach
                    <th>TOTAL</th><th>Travel (Mileage)</th><th>Actions</th>
                </tr>
                @foreach($timelines as $timeline)
                    @php
                        $total_minutes = 0;
                        $description_of_work = isset($timeline->task->description)?$timeline->task->description:'';
                        if($description_of_work != ''){
                            if($timeline->description_of_work != '')
                                $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                        else{
                            $description_of_work = $timeline->description_of_work;
                        }

                    @endphp
                    <tr>
                        <td>{{$timesheet->customer->customer_name}}</td>
                        <td>{{$timesheet->project->name}}</td>
                        <td>{{ $description_of_work }}</td>
                        <td>{{$timeline->is_billable == 1 ? 'Yes' : 'No'}}</td>
                        @foreach($week_days as $day)
                            <td>{{$timeline[strtolower(substr($day, 0,3))] < 10 ? '0'.$timeline[strtolower(substr($day, 0,3))] : $timeline[strtolower(substr($day, 0,3))]}}:{{$timeline[strtolower(substr($day, 0,3)).'_m'] < 10 ? '0'.$timeline[strtolower(substr($day, 0,3)).'_m'] : $timeline[strtolower(substr($day, 0,3)).'_m']}}</td>
                        @endforeach
                        <td>{{minutesToTime((($timeline->total*60)+$timeline->total_m))}}</td><td>{{$timeline->mileage}}</td>
                        <td>
                            <a href="{{route('timesheet.edittimeline',[$timesheet, $timeline['id']])}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE', 'route' => ['timesheet.destroytimeline', $timeline['id']], 'style'=>'display:inline', 'class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">TOTAL</td>
                    @php
                        $day_number = 1;
                    @endphp
                    @foreach($week_days as $day)
                        <td rowspan="2">
                            {{minutesToTime($timelines->sum(function ($line) use($day){ return (($line[strtolower(substr($day, 0, 3))]*60) + $line[strtolower(substr($day, 0, 3)).'_m']);}))}}
                        </td>
                    @endforeach
                    <td>{{isset($day)?minutesToTime($timelines->sum(function ($line) use($day){ return (($line->total*60) + $line->total_m);})):"0:00"}}</td><td>{{$total_mileage}}</td><td>&nbsp;</td>
                </tr>
                </tbody>
            </table>

            @isset($billing_cycle_id)
                <blackboard-combined-timesheets
                        employee-id="{{$timesheet->employee_id}}"
                        project-id="{{$timesheet->project_id}}"
                        custom-template="{{$use_custom_template}}"
                        billing-cycle-id="{{$billing_cycle_id}}"
                        billing-period-id="{{$current_billing_period_id}}"></blackboard-combined-timesheets>
            @endisset

            <table class="table" border="1">
                <thead>
                <tr class="bg-dark">
                    <th colspan="13" class="text-center">More Timesheets for this Project</th>
                </tr>
                </thead>
                <tbody>
                <tr class="bg-dark">
                    <th>Week No</th><th>Date</th><th>Bill</th><th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                    <th>Sun</th>
                    <th>Total</th>
                    <th>Travel</th>
                    <th class="text-right">Action</th>
                </tr>
                @forelse($project_timelines as $tline)
                    <tr class="{{!$tline->is_billable?'bg-secondary':null}}">
                        <td>{{substr(($tline->year_week??"0000"), 0, 4)}}-{{substr(($tline->year_week??"00"), 4, 2)}}</td>
                        <td>{{$tline->first_day_of_week??"0000-00-00"}}</td>
                        <td>{{($tline->is_billable == 1)?"Yes":"No"}}</td>
                        <td>{{substr($tline->monday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->tuesday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->wednesday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->thursday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->friday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->saturday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->sunday??"00:00",0,5)}}</td>
                        <td>{{substr($tline->total??"00:00",0,5)}}</td>
                        <td class="text-right">{{$tline->travel??0}}</td>
                        <td class="text-right">
                            <a class="btn btn-info btn-sm" href="{{route('timesheet.show',$tline->id)}}">View</a>
                            @if($tline->timesheet_lock != 1)
                                {{--@if($have_timelines[$timesheet->id])
                                    <a href="{{route('timesheet.edittimeline', [$timesheet, 0])}}" class="btn btn-success btn-sm">Edit</a>
                                @endif--}}
                                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                                    @if($tline->timeline_id)
                                        <a href="{{route('timesheet.edittimeline',[$timesheet, explode(",",$tline->timeline_id)[0]])}}" class="btn btn-success btn-sm">Edit</a>
                                        {{ Form::open(['method' => 'DELETE','route' => ['timesheet.destroytimeline', $tline->id],'style'=>'display:inline','class'=>'delete']) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                        {{ Form::close() }}
                                    @else
                                        <a href="{{route('timesheet.createtimeline', $tline->id)}}" class="btn btn-success btn-sm">Edit</a>
                                        {{ Form::open(['method' => 'DELETE','route' => ['timesheet.destroy', $tline->id],'style'=>'display:inline','class'=>'delete']) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                        {{ Form::close() }}
                                    @endif
                                @endif
                            @else
                                <span class="badge badge-dark px-4 py-2"><i class="fas fa-lock"></i></span>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="13" class="text-center">You have no previous timesheets on this project</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="modal fade" id="new-task" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Task For {{$timesheet->project->name??null}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => route('task.store', ["from" => "timeline"]), 'method' => 'post','class'=>'mt-3', "id" => "task-store"]) !!}
                        <div class="form-group">
                            {!! Form::hidden('project', $timesheet->project_id) !!}
                            {!! Form::hidden('consultant', $timesheet->employee_id) !!}
                            {!! Form::label('description', 'Description') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                            {!! Form::text('description', old('description'), ['class' => 'form-control form-control-sm'.($errors->has('description') ? ' is-invalid' : ''), 'id' => "description"]) !!}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            {!! Form::label('hours_planned', 'Planned Hours') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                            {!! Form::text('hours_planned', old('hours_planned')??0, ['class' => 'form-control form-control-sm'.($errors->has('hours_planned') ? ' is-invalid' : ''), 'id' => "hours_planned"]) !!}
                            @foreach($errors->get('hours_planned') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('start_date', 'Start Date') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                                {!! Form::text('start_date', now()->toDateString(), ['class' => 'form-control form-control-sm datepicker'.($errors->has('start_date') ? ' is-invalid' : ''), 'id' => "start_date"]) !!}
                                @foreach($errors->get('start_date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('end_date', 'End Date') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                                {!! Form::text('end_date', now()->toDateString(), ['class' => 'form-control form-control-sm datepicker'.($errors->has('end_date') ? ' is-invalid' : ''), 'id' => "end_date"]) !!}
                                @foreach($errors->get('end_date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('epic_id', 'Epic') !!}
                                {!! Form::select('epic_id', [], null, ['class' => 'form-control form-control-sm ', 'id' => "epic_id", 'placeholder' => "Select Epic", "data-type"=>"Feature"]) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('feature_id', 'Feature') !!}
                                {!! Form::select('feature_id', [], null, ['class' => 'form-control form-control-sm ', 'id' => "feature_id", 'placeholder' => "Select Feature", "disabled"]) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('user_story_id', 'User Story') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                                {!! Form::select('user_story_id', [], null, ['class' => 'form-control form-control-sm '.($errors->has('user_story_id') ? ' is-invalid' : ''), 'id' => "user_story_id", 'placeholder' => "Select User Story", "disabled"]) !!}
                                @foreach($errors->get('user_story_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('status', 'Status') !!} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                                {!! Form::select('status', $status_drop_down, 3, ['class' => 'form-control form-control-sm'.($errors->has('status') ? ' is-invalid' : ''), 'id' => "status_id", 'placeholder' => "Select Status"]) !!}
                                @foreach($errors->get('status') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('billable', 'Billable') !!}
                                {!! Form::select('billable', $yes_or_no_dropdown, 1, ['class' => 'form-control form-control-sm', 'id' => "billable", 'placeholder' => "Select Billable"]) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('on_kanban', 'Display On Kanban') !!}
                                {!! Form::select('on_kanban', $yes_or_no_dropdown, 1, ['class' => 'form-control form-control-sm', 'id' => "on_kanban", 'placeholder' => "Select Display on kanban"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('note', 'Note') !!}
                            {!! Form::textarea('note', old('note'), ['class' => 'form-control form-control-sm', 'id' => "note", 'rows' => 4]) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="save-task">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            let dropdowns = JSON.parse(JSON.stringify({!! $epics_drop_down !!}));
            let epics = "<option disabled selected>Select Epic</option>";
            let features = "<option disabled selected>Select Feature</option>";
            let user_stories = "<option disabled selected>Select User Story</option>";
            let select_epic = $("#epic_id");
            let select_feature = $("#feature_id");
            let select_user_story = $("#user_story_id");

            dropdowns.forEach(v => epics += "<option value='"+v.id+"'>"+v.name+"</option>");
            select_epic.html(epics).trigger("chosen:updated");

            select_epic.on('change', function(){
                dropdowns.forEach(v => {
                    if (select_epic.val() == v.id)
                        v.features.forEach(items => features += "<option value='"+items.id+"'>"+items.name+"</option>")
                })

                select_feature.attr("disabled", false)
                    .html(features)
                    .trigger("chosen:updated");
            })

            select_feature.on("change", function (){
                dropdowns.forEach(v => v.features.forEach(feature => {
                    if (feature.id == select_feature.val())
                        feature.user_stories.forEach(story => user_stories += "<option value='"+story.id+"'>"+story.name+"</option>")
                }))

                select_user_story.attr("disabled", false)
                    .html(user_stories)
                    .trigger("chosen:updated");
            })

            $("#save-task").on("click", () => {
                if ($("#description").val().length === 0){
                    $("#descrition").addClass("is-invalid")
                    alert("Description is required")
                    return false;
                }

                if($("#user_story_id").val().length === 0){
                    $("#user_story_id").addClass("is-invalid")
                    alert("User Story Id is required")
                    return false;
                }

                if($("#start_date").val().length === 0){
                    $("#start_date").addClass("is-invalid")
                    alert("Start Date is required")
                    return false;
                }

                if($("#end_date").val().length === 0){
                    $("#end_date").addClass("is-invalid")
                    alert("End Date is required")
                    return false;
                }

                if($("#hours_planned").val() < 1){
                    $("#hours_planned").addClass("is-invalid")
                    alert("Planned hours is required")
                    return false;
                }

                if($("#status_id").val() < 1){
                    $("#status_id").addClass("is-invalid")
                    alert("Status is required")
                    return false;
                }

                $("#task-store").submit()
            })
        })

        $(function () {


            $("#task_id").on('change', () => {
                axios.get('/task-delivery-type/'+$("#task_id").val())
                    .then(res => {
                        $("#task_delivery_type_id").val(res.data.task_delivery_type_id);
                    }).catch(error => {
                    console.log(error.response);
                })
            })
        });

        function minutesToTime(minutes){
            var hours = Math.trunc(minutes/60);
            var minutes = minutes % 60;
            var hour_str = hours < 10 ? '0'+hours : hours;
            var minutes_str = minutes < 10 ? '0'+minutes : minutes;
            return hour_str+':'+minutes_str;
        }

        $("#year_week").on('change', () =>{

            axios.get('/timesheet/checktimesheet?project={{$timesheet->project_id}}&employee={{$timesheet->employee_id}}&year_week='+$("#year_week").val())
                .then(response => {
                    let timesheet = response.data.timesheet;
                    if (timesheet.length){
                        let feedback = "";

                        timesheet.forEach((item, i) => {
                            feedback += "<div class='alert alert-info mt-3'>"
                                + "<p>Week "+item.year_week+" already has a timesheet for project "+item.project+" for "+item.time+" hours</p>"
                                + "<a href='/timesheet/"+item.id+"/createtimeline' class='btn btn-sm btn-dark text-decoration-none'>Edit</a> existing timesheet or <button onclick='storeTimesheet()' class='btn btn-sm btn-dark'>create</button> an addition/new timesheet."
                                + "</div>"
                        })
                        $("#timesheet-exist").html(feedback);
                    }else {
                        storeTimesheet();
                    }
                    //this.timesheet_for_this_week = response.data.timesheet;
                    //console.log(response.data)
                })
                .catch(err => console.log(err.response.data))

        })

        $("#template_id").on('change', () => $("#template_form").submit());

        function storeTimesheet() {
            axios.post('{{route('timesheet.store')}}', {
                employee_id: '{{$timesheet->employee_id}}',
                company_id: '{{$timesheet->company_id}}',
                project_id: '{{$timesheet->project_id}}',
                client_id: '{{$timesheet->customer_id}}',
                year_week: $("#year_week").val(),
                from_timeline: true
            })
                .then(function (response) {
                    window.location = "/timesheet/"+response.data.timesheet_id+"/createtimeline";
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    </script>
@endsection
