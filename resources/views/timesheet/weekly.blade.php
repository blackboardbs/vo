@section('weekly_timesheet')
    <div class="row bg-dark p-2 no-gutters">
        <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
        @php
            $date = new DateTime($timesheet->last_day_of_month);

            $total_time = 0;
            $total_mileage = 0;
            function minutesToTime($minutes){
                $hours = floor($minutes/60);
                $minutes = $minutes % 60;
                $negation_number = '';
                if($hours < 0){
                    $hours *= -1;
                    $negation_number = '-';
                }

                if($minutes < 0){
                    $minutes *= -1;
                    $negation_number = '-';
                }
                $hour_str = $hours < 10 ? '0'.$hours : $hours;
                $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                return $negation_number.$hour_str.':'.$minutes_str;
            }
        @endphp
        <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
        <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
    </div>
    <div class="row no-gutters p-2">
        <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
    </div>
    <table class="table" border="1">
        <tbody>
        <tr class="bg-dark">
            <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
            @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                @if($i > $timesheet->last_date_of_month)
                    @break
                @endif

                @php
                    $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                @endphp

                <th>{{$date->format('D')}} {{$i}}</th>
            @endfor
            <th>TOTAL</th><th>Travel (Mileage)</th>
        </tr>
        @foreach($timelines as $timeline)
            <tr>
                <td>{{$timesheet->customer->customer_name}}</td><td>{{$timesheet->project->name}}</td><td>{{$timeline['description_of_work']}}</td><td>{{$timeline['is_billable'] == 1 ? 'Yes' : 'No'}}</td>
                @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                    @if($i > $timesheet->last_date_of_month)
                        @break
                    @endif
                    @php
                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                    @endphp
                    <td>{{$timeline[strtolower($date->format('D'))]}}:{{$timeline[strtolower($date->format('D')).'_m']}}</td>
                @endfor
                @php
                    $total_time += $timeline['total'] * 60 + $timeline['total_m'];
                    $total_mileage += $timeline['mileage'];
                @endphp
                <td>{{minutesToTime($timeline['total'] * 60 + $timeline['total_m'])}}</td><td>{{$timeline['mileage']}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="4">TOTAL</td>
            @php
                $day_number = 1;
            @endphp
            @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                @if($i > $timesheet->last_date_of_month)
                    @break
                @endif

                @php
                    $hour = 0;
                    $minute = 0;
                    foreach($timelines as $timeline):
                        $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        $_hour = $timeline[strtolower($date->format('D'))];
                        $_minute = $timeline[strtolower($date->format('D')).'_m'];
                        $hour += $_hour;
                        $minute += $_minute;
                    endforeach;
                @endphp

                <td rowspan="2">{{minutesToTime($hour * 60 + $minute)}}</td>
            @endfor
            <td>{{minutesToTime($total_time)}}</td><td>{{$total_mileage}}</td>
        </tr>
        </tbody>
    </table>
@endsection