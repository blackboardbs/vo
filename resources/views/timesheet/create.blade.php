@extends('adminlte.default')
@section('title') Add Timesheet @endsection
@section('header')
    {{-- <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('createTimesheet')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div> --}}
@endsection
@section('content')
    {{-- <div class="container-fluid">
        <hr /> --}}
        <create-timesheet
                :employees="{{json_encode($employee)}}"
                :creator-id="{{(app('request')->input('resource') ? app('request')->input('resource') : $creator_id)}}" 
                :project-id="{{(app('request')->input('project') ? app('request')->input('project') : 0)}}"
        >
        </create-timesheet>
    {{-- </div> --}}
@endsection