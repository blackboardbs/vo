@extends('adminlte.default')

@section('title') Add Time @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="row bg-dark p-2 no-gutters">
                <div class="col-md-6">Weekly Timesheet ({{$timesheet->project->ref}})</div>
                @php
                    $date = new DateTime($timesheet->last_day_of_month);

                    $total_time = 0;
                    $total_mileage = 0;
                    function minutesToTime($minutes){
                        $hours = floor($minutes/60);
                        $minutes = $minutes % 60;
                        $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                        $hour_str = $hours < 10 ? '0'.$hours : $hours;
                        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                        return $negation_number.$hour_str.':'.$minutes_str;
                    }
                @endphp
                <div class="col-md-4">Date from ( Mon ): <strong> {{ $timesheet->first_day_of_week }} to {{ $timesheet->last_date_of_week >= 7 ? '( Sun ): '.$timesheet->last_day_of_week : '('.$date->format('D').'): '.$timesheet->last_day_of_month }} </strong> </div>
                <div class="col-md-2 text-right">Week {{ $timesheet->week }} </div>
            </div>
            <div class="row no-gutters p-2">
                <div class="col-md-5">Time Allocation</div><div class="col-md-6"><span class="pull-right">{{ $employee->last_name.', '.$employee->first_name }}</span></div><div class="col-md-1 text-right">Emp ID: {{$employee->id}}</div>
            </div>
            <table class="table" border="1">
                <tbody>
                <tr class="bg-dark">
                    <th>Client</th><th>Project Name</th><th>Description of Work</th><th>Bill</th>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        @endphp

                        <th>{{$date->format('D')}} {{$i}}</th>
                    @endfor
                    <th>TOTAL</th><th>Travel (Mileage)</th>
                </tr>
                @foreach($timelines as $timeline)
                    <tr>
                        <td>{{$timesheet->customer->customer_name}}</td><td>{{$timesheet->project->name}}</td><td>{{$timeline['description_of_work']}}</td><td>{{$timeline['is_billable'] == 1 ? 'Yes' : 'No'}}</td>
                        @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                            @if($i > $timesheet->last_date_of_month)
                                @break
                            @endif
                            @php
                                $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            @endphp
                            <td>{{$timeline[strtolower($date->format('D'))]}}:{{$timeline[strtolower($date->format('D')).'_m']}}</td>
                        @endfor
                        @php
                            $total_time += $timeline['total'] * 60 + $timeline['total_m'];
                            $total_mileage += $timeline['mileage'];
                        @endphp
                        <td>{{minutesToTime($timeline['total'] * 60 + $timeline['total_m'])}}</td><td>{{$timeline['mileage']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">TOTAL</td>
                    @php
                        $day_number = 1;
                    @endphp
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $hour = 0;
                            $minute = 0;
                            foreach($timelines as $timeline):
                                $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                                $_hour = $timeline[strtolower($date->format('D'))];
                                $_minute = $timeline[strtolower($date->format('D')).'_m'];
                                $hour += $_hour;
                                $minute += $_minute;
                            endforeach;
                        @endphp

                        <td rowspan="2">{{minutesToTime($hour * 60 + $minute)}}</td>
                    @endfor
                    <td>{{minutesToTime($total_time)}}</td><td>{{$total_mileage}}</td>
                </tr>
                </tbody>
            </table>
            {{Form::open(['url' => route('timesheet.savetimeline', $timesheet->id), 'method' => 'put','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="10" class="text-center">Insert a new line in this weekly timesheet.</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Client:</th>
                    <td colspan="5">{{Form::text('client',$timesheet->customer->customer_name,['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project:</th>
                    <td colspan="4">
                        {{Form::text('project',$timesheet->project->name,['class'=>'form-control form-control-sm'. ($errors->has('project') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td colspan="9">{{Form::text('description_of_work',old('description_of_work'),['class'=>'form-control form-control-sm'. ($errors->has('description_of_work') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="bg-dark">
                    <th>Client Allocation</th>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                        @endphp

                        <th>{{$date->format('D')}} {{$i}}</th>
                    @endfor
                    <th>Travel (Mileage)</th><th>Billable</th>
                </tr>
                <tr>
                    <th>Hours</th>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            $day = strtolower($date->format('D'));
                        @endphp

                        <td>
                            {{Form::select($day,$hours,0,['class'=>'form-control form-control-sm '. ($errors->has($day) ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                            @foreach($errors->get($day) as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    @endfor
                    <td rowspan="2">
                        {{Form::text('mileage',0,['class'=>'form-control form-control-sm '. ($errors->has('mileage') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('mileage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td rowspan="2">
                        {{Form::select('is_billable',[0 => 'No', 1 => 'Yes'],0,['class'=>'form-control form-control-sm'. ($errors->has('is_billable') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                        @foreach($errors->get('is_billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Minutes</th>
                    @for($i = $timesheet->first_date_of_week; $i <= $timesheet->first_date_of_week + 6; $i++)
                        @if($i > $timesheet->last_date_of_month)
                            @break
                        @endif

                        @php
                            $date = new DateTime($timesheet->year.'-'.$timesheet->month.'-'.$i);
                            $day_m = strtolower($date->format('D'));
                        @endphp

                        <td>
                            {{Form::select($day_m.'_m',$minutes,0,['class'=>'form-control form-control-sm '. ($errors->has($day_m.'_m') ? ' is-invalid' : ''),'placeholder'=>'Select Hours'])}}
                            @foreach($errors->get($day_m.'_m') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    @endfor
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $document.ready(function(){
        });

        function minutesToTime(minutes){
            var hours = Math.trunc(minutes/60);
            var minutes = minutes % 60;
            var negation_number = '';
            if(hours < 0){
                hours *= -1;
                negation_number = '-';
            }

            if(minutes < 0){
                minutes *= -1;
                negation_number = '-';
            }
            var hour_str = hours < 10 ? '0'+hours : hours;
            var minutes_str = minutes < 10 ? '0'+minutes : minutes;
            return negation_number+hour_str+':'+minutes_str;
        }

    </script>
@endsection
