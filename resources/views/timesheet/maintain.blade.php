@extends('adminlte.default')

@section('title') Timesheet Maintenance @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <p class="bg-gray-light mb-0 p-2">This option is to be used for maintenance of Timesheets only.</p>

        <div class="table-responsive">
            {{Form::open(['url' => route('timesheet.store_maintain', $time), 'method' => 'patch','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <td colspan="2">Weekly Timesheet {{ '('.$time->version.')' }}</td>
                    <td class="text-right">Date from (Mon): {{ $time->first_day_of_week }} to (Sun): {{ $time->last_day_of_week }}</td>
                    <td class="text-right">Wk {{$time->week}}</td>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <th colspan="3" class="text-right">{{ $time->user->last_name.', '.$time->user->first_name }}</th>
                    <th class="text-right">Emp ID: {{ $time->employee_id }}</th>
                </tr>

                <tr>
                    <th>Client</th>
                    <td style="width: 30%">{{Form::select('client',$customer, $time->customer_id,['class'=>'form-control form-control-sm '. ($errors->has('client') ? ' is-invalid' : ''), 'id' => 'customer_id'])}}
                        @foreach($errors->get('client') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Project</th>
                    <td style="width: 30%">{{Form::select('project',$project, $time->project_id,['class'=>'form-control form-control-sm '. ($errors->has('project') ? ' is-invalid' : ''), 'id' => 'project_drop'])}}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Version</th>
                    <td>{{Form::text('version',$time->version,['class'=>'form-control form-control-sm'. ($errors->has('version') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('version') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Billing Status</th>
                    <td>{{Form::select('invoice_status',$invoice,$time->bill_status,['class'=>'form-control form-control-sm '. ($errors->has('invoice_status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('invoice_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td colspan="3">{{Form::text('note',$time->invoice_note,['class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>
                    <th>Invoice Date</th>
                    <td>{{Form::text('invoice_date',$time->invoice_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('invoice_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Due Date</th>
                    <td>{{Form::text('due_date',$time->due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Invoice Reference</th>
                    <td>{{Form::text('inv_ref',$time->inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('inv_ref') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('inv_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Customer Inv Ref</th>
                    <td>{{Form::text('cust_inv_ref',$time->cust_inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('cust_inv_ref') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('cust_inv_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Expenses Paid</th>
                    <td>{{Form::select('exp_paid',[0 => 'No', 1 => 'Yes'],$time->exp_paid,['class'=>'form-control form-control-sm'. ($errors->has('exp_paid') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('exp_paid') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td colspan="2" class="{{ ($amount > 0 && $time->exp_paid == 0) ? 'bg-danger' : '' }} align-content-center">{{ $expam }}</td>
                </tr>
                <tr>
                    <th>Vendor Paid Date</th>
                    <td>{{Form::text('ext_lab_paid_date',$time->ext_labour_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('ext_lab_paid_date') ? ' is-invalid' : ''), 'placeholder' => 'YYYY/MM/DD'])}}
                        @foreach($errors->get('ext_lab_paid_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td colspan="2" class="{{ ($ext_lab_cost_out == 1) ? 'bg-danger' : '' }}">{{ ($ext_lab_cost_out == 1 && $ext_lab_cost == 0) ? 'Not Applicable' : 'External Labour Cost Outstanding @ '.$assignment->currency.' '.$assignment->external_cost_rate.' ph'}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{Form::select('status',$status_dropdown,$time->status,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Record Locked</th>
                    <td>{{Form::select('tlock',[0 => 'Open', 1 => 'Locked'],$time->timesheet_lock,['class'=>'form-control form-control-sm'. ($errors->has('tlock') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('tlock') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Vendor invoice number</th>
                    <td>{{Form::text('vendor_inv_number',$time->vendor_invoice_number, ['class'=>'form-control form-control-sm'. ($errors->has('vendor_inv_number') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('vendor_inv_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                    <th>Vendor invoice status</th>
                    <td>{{Form::select('vendor_inv_status',$invoice_status,$time->vendor_invoice_status,['class'=>'form-control form-control-sm '. ($errors->has('vendor_inv_status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('vendor_inv_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Vendor Due Date</th>
                    <td>{{Form::text('vendor_due_date',$time->vendor_due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_due_date') ? ' is-invalid' : ''), 'placeholder' => 'YYYY/MM/DD'])}}
                        @foreach($errors->get('vendor_due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    {{--<th>Vendor Paid Date</th>
                    <td>{{Form::text('vendor_paid_date',$time->vendor_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_paid_date') ? ' is-invalid' : ''), 'placeholder' => 'YYYY/MM/DD'])}}
                        @foreach($errors->get('vendor_paid_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>--}}
                    <th>CF Timesheet</th>
                    <td>{{Form::text('cf_timesheet_id',$time->cf_timesheet_id,['class'=>'form-control form-control-sm'. ($errors->has('cf_timesheet_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('cf_timesheet_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    {{--<th>AR Invoice Number</th>
                    <td>{{Form::text('ar_invoice_number',$time->ar_invoice_number,['class'=>'form-control form-control-sm'. ($errors->has('ar_invoice_number') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('ar_invoice_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>--}}
                    {{--<th>CF Timesheet</th>
                    <td>{{Form::text('cf_timesheet_id',$time->cf_timesheet_id,['class'=>'form-control form-control-sm'. ($errors->has('cf_timesheet_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('cf_timesheet_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>--}}
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
        
        $(function () {
            let option = '';
            axios.post('/maintain_projects', {
                customer_id: '{{$time->customer_id}}'
            })
                .then(function (response) {
                    for(var i = 0; i < response.data.length; i++){
                        option += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                    }
                    $('#project_drop').html(option).val({{$time->project_id}}).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            let option_change = '';

            $("#customer_id").on('change', function () {
                axios.post('/maintain_projects', {
                    customer_id: $("#customer_id").val()
                })
                    .then(function (response) {
                        for(var i = 0; i < response.data.length; i++){
                            option_change += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                        }
                        $('#project_drop').html(option_change).trigger("chosen:updated");
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })
    </script>
@endsection