@extends('adminlte.default')

@section('title') Timesheet Maintenance @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <p class="bg-gray-light mb-0 p-2">This option is to be used for maintenance of Timesheets only.</p>

        <div class="table-responsive">
            {{Form::open(['url' => route('timesheet.store_maintain', $time), 'method' => 'patch','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="2">Weekly Timesheet {{ '('.(isset($time->project)?$time->project->ref:'').')' }}</th>
                    <th colspan="2">Date from (Mon): {{ $time->first_day_of_week }} to (Sun): {{ $time->last_day_of_week }}</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <th colspan="3">{{ $time->user->last_name.', '.$time->user->first_name }}</th>
                    <th class="text-right">Emp ID: {{ $time->employee_id }}</th>
                </tr>

                <tr>
                    <th>Client</th>
                    <td style="width: 30%">{{isset($time->customer)? $time->customer->customer_name : ''}}</td>
                    <th>Project</th>
                    <td style="width: 30%">{{Form::select('project',$project, $time->project_id,['class'=>'form-control form-control-sm '. ($errors->has('project') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Company</th>
                    <td style="width: 30%">{{isset($time->company)? $time->company->company_name : ''}}</td>
                    <th>YearWk</th>
                    <td style="width: 30%">{{$time->year_week}}</td>
                </tr>
                <tr>
                    <th>Custom Timesheet Ref</th>
                    <td style="width: 30%">{{$time->cf_timesheet_id }}</td>
                    <th>Record Status</th>
                    <td>{{Form::select('status',$status_dropdown,$time->status,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Record Locked</th>
                    <td>{{Form::select('tlock',[0 => 'Open', 1 => 'Locked'],$time->timesheet_lock,['class'=>'form-control form-control-sm'. ($errors->has('tlock') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('tlock') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Timesheet Billable</th>
                    <td>{{($time->is_billable == 1) ? 'Yes' : 'No'}}</td>

                </tr>
                <tr>
                    <th>Note</th>
                    <td>{{Form::text('note',$time->invoice_note,['class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Customer (Accounts Receivable)</th>
                </tr>
                <tr>
                    <th>WO Cust Invoice No</th>
                    <td>{{$time->invoice_number}}</td>
                    <th>Invoice Date</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{$time->invoice_date}}
                            {{Form::hidden('invoice_date',$time->invoice_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('invoice_date',$time->invoice_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('invoice_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Financial AR Invoice No</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{$time->inv_ref}}
                            {{Form::hidden('inv_ref',$time->inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('inv_ref') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('inv_ref',$time->inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('inv_ref') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('inv_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                    <th>Due Date</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{$time->invoice_due_date}}
                            {{Form::hidden('due_date',$time->invoice_due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('due_date',$time->invoice_due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('due_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Customer Reference</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{$time->cust_inv_ref}}
                            {{Form::hidden('cust_inv_ref',$time->cust_inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('cust_inv_ref') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('cust_inv_ref',$time->cust_inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('cust_inv_ref') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cust_inv_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                    <th>Actual Payment Date</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{$time->invoice_paid_date}}
                            {{Form::hidden('invoice_paid_date',$time->invoice_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_paid_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('invoice_paid_date',$time->invoice_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_paid_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('invoice_paid_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                <tr>
                    <th>Customer Invoice Status</th>
                    <td>
                        @if($time->bill_status == 3)
                            {{isset($time->bill_statusd)?$time->bill_statusd->description:''}}
                            {{Form::hidden('invoice_status',$time->bill_status,['class'=>'form-control form-control-sm'. ($errors->has('invoice_status') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::select('invoice_status',$invoice,$time->bill_status,['class'=>'form-control form-control-sm '. ($errors->has('invoice_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('invoice_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                    </td>
                    <th></th>
                    <td class="{{($time->invoice_paid_date == null  && ($hours * $assignment->invoice_rate) > 0)?'bg-danger':''}}">
                        {!! ($time->invoice_paid_date == null && ($hours * $assignment->invoice_rate) > 0) ? 'Outstanding amount R'.number_format($hours * $assignment->invoice_rate ,2,'.',',').' at Rate R'.number_format($assignment->invoice_rate,2,'.',',') : '' !!}
                    </td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Vendor (Account Payable)</th>
                </tr>
                <tr>
                    <th>WO Vendor Invoice No</th>
                    <td>{{$time->vendor_invoice_number}}</td>
                    <th>Invoice Date</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{$time->vendor_invoice_date}}
                            {{Form::hidden('vendor_invoice_date',$time->vendor_invoice_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_invoice_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('vendor_invoice_date',$time->vendor_invoice_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_invoice_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_invoice_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Financial AP Invoice No</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{$time->vendor_invoice_ref}}
                            {{Form::hidden('vendor_invoice_ref',$time->vendor_invoice_ref,['class'=>'form-control form-control-sm'. ($errors->has('vendor_invoice_ref') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('vendor_invoice_ref',$time->vendor_invoice_ref,['class'=>'form-control form-control-sm'. ($errors->has('vendor_invoice_ref') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_invoice_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                    <th>Due Date</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{$time->vendor_due_date}}
                            {{Form::hidden('vendor_due_date',$time->vendor_due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_due_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('vendor_due_date',$time->vendor_due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_due_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_due_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Vendor Reference</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{$time->vendor_reference}}
                            {{Form::hidden('vendor_reference',$time->vendor_reference,['class'=>'form-control form-control-sm'. ($errors->has('vendor_reference') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('vendor_reference',$time->vendor_reference,['class'=>'form-control form-control-sm'. ($errors->has('vendor_reference') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_reference') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                    <th>Actual Payment Date</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{$time->vendor_paid_date}}
                            {{Form::hidden('vendor_paid_date',$time->vendor_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_paid_date') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::text('vendor_paid_date',$time->vendor_paid_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('vendor_paid_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_paid_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                </tr>

                <tr>
                    <th>Vendor invoice status</th>
                    <td>
                        @if($time->vendor_invoice_status == 2)
                            {{isset($time->vendor_inv_status)?$time->vendor_inv_status->description:''}}
                            {{Form::hidden('vendor_inv_status',$time->vendor_invoice_status,['class'=>'form-control form-control-sm'. ($errors->has('vendor_inv_status') ? ' is-invalid' : '')])}}
                        @else
                            {{Form::select('vendor_inv_status',$vendor_invoice_status,$time->vendor_invoice_status,['class'=>'form-control form-control-sm '. ($errors->has('vendor_inv_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_inv_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                    </td>
                    <th></th>
                    <td class="{{($time->vendor_paid_date == null && ($hours * $assignment->external_cost_rate) > 0)?'bg-danger':''}}">
                        {!! ($time->vendor_paid_date == null && ($hours * $assignment->external_cost_rate) > 0) ? 'Outstanding amount R'.number_format($hours * $assignment->external_cost_rate ,2,'.',',').' at Rate '.number_format($assignment->external_cost_rate ,2,'.',',') : '' !!}
                    </td>
                </tr>
                @if($amount)
                    <tr class="bg-dark">
                        <th colspan="4">Consultant</th>
                    </tr>

                    <tr>
                        <th>Expenses Paid</th>
                        <td>
                            @if($time->exp_paid)
                               Paid
                            @else
                               Not Paid
                            @endif
                        </td>
                        <td colspan="2" class="{{ ((!$time->exp_paid) ? 'bg-danger' : 'bg-success') }} align-content-center">{{ $expam }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}

            <table class="table table-bordered table-sm" id="expenses">
                <thead>
                    <tr class="bg-dark">
                        <th colspan="7">Expenses - <small>Use this section to mark expense claims as paid to consultant</small></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Claim Date</th>
                        <th>Description</th>
                        <th>Account/Account Element</th>
                        <th>Bill</th>
                        <th>Claim</th>
                        <th class="text-right">Amount</th>
                        <th class="text-center">Paid Date</th>
                    </tr>
                    @forelse($expenses_approve as $expense)
                    <tr>
                        <td>{{$expense->date}}</td>
                        <td>{{$expense->description}}</td>
                        <td>{{isset($expense->account)?$expense->account->description:''}}/{{isset($expense->account_element)?$expense->account_element->description:''}}</td>
                        <td>{{($expense->is_billable == 1)?'Yes':'No'}}</td>
                        <td>{{($expense->claim == 1)?'Yes':'No'}}</td>
                        <td class="text-right">{{number_format($expense->amount,2,'.',',')}}</td>
                        <td class="text-center">
                            @if($expense->claim_auth_date == null)
                                <a href="{{route('expense.approve', $expense)}}" class="btn btn-sm btn-success">Approve Claim</a>
                            @elseif($expense->paid_date == null)
                                <a href="{{route('expense.paid', $expense)}}" class="btn btn-sm btn-success">Paid</a>
                            @else
                                {{$expense->paid_date}}
                            @endif
                        </td>
                    </tr>
                    @empty
                    @endforelse
                    <tr>
                        <td colspan="7">Claim not authorised. The claim must be authorised by Stefan Walkenshaw</td>
                    </tr>
                    <tr>
                        <td colspan="6">Total To Be Billed To Client</td>
                        <th class="text-right">{{number_format($total_exp_bill,2,'.',',')}}</th>
                    </tr>
                    <tr>
                        <td colspan="6">Total To Be Claimed By Consultant</td>
                        <th class="text-right">{{number_format($total_exp_claim,2,'.',',')}}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
        
        $(function () {
            let option = '';
            axios.post('/maintain_projects', {
                customer_id: '{{$time->customer_id}}'
            })
                .then(function (response) {
                    for(var i = 0; i < response.data.length; i++){
                        option += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                    }
                    $('#project_drop').html(option).val({{$time->project_id}}).trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });
            let option_change = '';

            $("#customer_id").on('change', function () {
                axios.post('/maintain_projects', {
                    customer_id: $("#customer_id").val()
                })
                    .then(function (response) {
                        for(var i = 0; i < response.data.length; i++){
                            option_change += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                        }
                        $('#project_drop').html(option_change).trigger("chosen:updated");
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })
    </script>
@endsection