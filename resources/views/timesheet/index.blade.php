@extends('adminlte.default')

@section('title') Timesheets @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor']))
        <a href="{{route('timesheet.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Timesheet</a>
            <x-export route="timesheet.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-2 searchform" id="searchform">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            @if(Auth::user()->hasAnyRole(['admin', 'admin_manager','manager']))
            <div class="row pt-2 pb-0" style="flex-wrap: nowrap;flex:auto;">
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="company" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($company_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['company']) && $key == $_GET['company'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        <span>Company</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="customer" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($customer_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['customer']) && $key == $_GET['customer'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('customer',$customer_drop_down,old('customer'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="project_status" class="form-control search w-100" onChange="submitSearch()">
                                <option value="0" selected>All</option>
                                <option {{(isset($_GET['project_status']) && "1" == $_GET['project_status'] ? 'selected' : '')}} value="1">Active</option>
                                <option {{(isset($_GET['project_status']) && "2" == $_GET['project_status'] ? 'selected' : '')}} value="2">Not Active</option>
                            </select>
                        <span>Project Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="project" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($project_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['project']) && $key == $_GET['project'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('project',$project_drop_down,old('project'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Projects</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="employee" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($employee_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['employee']) && $key == $_GET['employee'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('employee',$employee_drop_down,old('employee'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Resource</span>
                    </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="wf" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($week_from_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['wf']) && $key == $_GET['wf'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('wf', $week_from_drop_down, old('wf'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Week From</span>
                        </label>
                    </div>
                </div>
                
            </div>
            <div class="row pt-4 mt-0 pb-2 w-100" style="flex-wrap: nowrap;flex:auto;">
                <div class="col-md-2 col-sm-12" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="wt" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($week_to_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['wt']) && $key == $_GET['wt'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('wt', $week_to_drop_down, old('wt'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Week To</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="cir" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($cust_inv_ref_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['cir']) && $key == $_GET['cir'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('cir', $cust_inv_ref_drop_down, old('cir'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Customer Invoice Ref</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="cis" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($is_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['cis']) && $key == $_GET['cis'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('cis', $is_drop_down, old('cis'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Customer Invoice Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="vir" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($vendor_inv_ref_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['vir']) && $key == $_GET['vir'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('ir',$vendor_inv_ref_drop_down,old('ir'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Vendor Invoice Ref</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="vis" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($vis_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['vis']) && $key == $_GET['vis'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('vis', $is_drop_down, old('vis'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Vendor Invoice Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="inv_amount" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ([0 => 'No Invoice Amount', 1 => 'Show Invoice Amount'] as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['inv_amount']) && $key == $_GET['inv_amount'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('inv_amount', [0 => 'No Invoice Amount', 1 => 'Show Invoice Amount'], old('inv_amount'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Show Inv Amount</span>
                        </label>
                    </div>
                </div>
                
                <div class="col-md-2" style="max-width:16% !important">
                    <a href="{{route('timesheet.index')}}" class="btn btn-info col-md-12" type="submit">Clear Filters</a>
                </div>
            </div>
            @else
            <div class="row pt-2 pb-0" style="flex-wrap: nowrap;flex:auto;">
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="company" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($company_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['company']) && $key == $_GET['company'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('company',$company_drop_down,old('company'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Company</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="customer" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($customer_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['customer']) && $key == $_GET['customer'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('customer',$customer_drop_down,old('customer'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="project_status" class="form-control search w-100" onChange="submitSearch()">
                                <option value="0" selected>All</option>
                                <option value="1">Active</option>
                                <option value="2">Not Active</option>
                            </select>
                        <span>Project Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="project" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($project_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['project']) && $key == $_GET['project'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('project',$project_drop_down,old('project'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Projects</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="employee" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($employee_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['employee']) && $key == $_GET['employee'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('employee',$employee_drop_down,old('employee'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Resource</span>
                    </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="wf" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($week_from_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['wf']) && $key == $_GET['wf'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('wf', $week_from_drop_down, old('wf'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Week From</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:14% !important">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="wt" class="form-control search w-100" onChange="submitSearch()">
                                @foreach ($week_to_drop_down as $key => $item)
                                    <option value="{{$key}}" {{(isset($_GET['wt']) && $key == $_GET['wt'] ? 'selected' : '')}}>{{$item}}</option>
                                @endforeach
                            </select>
                        {{-- {{Form::select('wt', $week_to_drop_down, old('wt'),['class'=>'form-control search ', 'style'=>'width: 100%;'])}} --}}
                        <span>Week To</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm" style="max-width:16% !important">
                    <a href="{{route('timesheet.index')}}" class="btn btn-info col-md-12" type="submit">Clear Filters</a>
                </div>
            </div>
            @endif
        </form>

        <hr class="mt-1">
        @if (\Session::has('error_detected'))
            <p class="alert alert-danger">{!! \Session::get('error_detected') !!}</p>
        @endif

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('id','ID')</th>
                    <th>Resource</th>
                    <th nowrap>@sortablelink('year_week','YearWk')</th>
                    <th nowrap>@sortablelink('first_day_of_week', 'Wk Start')</th>
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Bill</th>
                    <th nowrap>No Bill</th>
                    <th nowrap>Total Hour</th>
                    <th  class="invoice-status" data-toggle="tooltip" data-placement="top" title="Expenses">Exp</th>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <th class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="Invoice Status">CIS</th>
                        <th class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="Vendor Invoice Status">VIS</th>
                        <th nowrap>CIR</th>
                        <th nowrap>VIR</th>
                    @endif
                    <th nowrap>Claim Amount</th>
                    <!-- @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <th>Detail</th>
                    @endif -->
                    <th @if(!isset($_GET['inv_amount']) || ($_GET['inv_amount'] != 1)) style="display: none;" @endif nowrap>Invoice Amount</th>
                    <th>Status</th>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <th>Actions</th>
                    @else
                    <th>Actions</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @php
                    function minutesToTime($minutes){
                        $hours = floor($minutes/60);
                        $minutes = $minutes % 60;
                        $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                        $hour_str = $hours < 10 ? '0'.$hours : $hours;
                        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                        return $negation_number.$hour_str.':'.$minutes_str;
                    }

                @endphp
                @forelse($timesheets as $timesheet)
                    @php
                        $invoice_amount_total += $invoice_amounts[$timesheet->id];
                        $total_claim += (isset($timesheet->time_exp[0]->amount)?$timesheet->time_exp[0]->amount:0);
                    @endphp
                    <tr>
                        <td>{{$timesheet->id}}</td>
                        {{--<td><a href="{{route('timesheet.show',$timesheet)}}">{{$timesheet->employee_id}}</a></td>--}}
                        <td><a href="{{route('timeline.show',$timesheet)}}">{{$timesheet->resource}}</a></td>
                        <td>{{$timesheet->year_week}}</td>
                        <td>{{$timesheet->first_day_of_week}}</td>
                        <td>{{$timesheet->customer?->customer_name}}</td>
                        <td>{{$timesheet->project?->name}}</td>
                        <td class="text-center">{{ minutesToTime($timesheet->minutes_bill) }}</td>
                        <td class="text-center">{{ minutesToTime($timesheet->minutes_no_bill) }}</td>
                        <td class="text-center">{{ minutesToTime($timesheet->total_hours * 60 + $timesheet->total_minutes) }}</td>
                        <td>
                            @if((count($timesheet->time_exp) > 0) && !isset($timesheet->time_exp[0]->paid_date) && !isset($timesheet->time_exp[0]->claim_auth_date))
                                <span class="badge badge-danger px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense not approved and not paid"><i class="fas fa-times"></i></span>
                            @elseif((count($timesheet->time_exp) > 0) && !isset($timesheet->time_exp[0]->paid_date) && isset($timesheet->time_exp[0]->claim_auth_date))
                                <span class="badge badge-warning px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense is approved but not paid"><i class="fas fa-check"></i></span>
                            @elseif((count($timesheet->time_exp) > 0) && isset($timesheet->time_exp[0]->paid_date) && isset($timesheet->time_exp[0]->claim_auth_date))
                                <span class="badge badge-success px-2 invoice-status"  data-toggle="tooltip" data-placement="top" title="Expense is approved and paid"><i class="fas fa-check-double"></i></span>
                            @endif
                        </td>
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                            <td class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="{{$timesheet->bill_statusd?->description}}">
                                <a class="badge px-2 {{(!isset($timesheet->bill_statusd->description)?'btn-warning':'btn-primary')}} " href="{{route('timesheet.maintain', $timesheet)}}" style="color: white !important">
                                    {{isset($timesheet->bill_statusd->description) ? substr($timesheet->bill_statusd->description,0,1) : '?'}}
                            </a></td>
                            <td class="invoice-status text-center" data-toggle="tooltip" data-placement="top" title="{{$timesheet->vendor_inv_status?->description}}">
                                <a class="badge px-2 {{(!isset($timesheet->vendor_inv_status->description)?'btn-warning':'btn-primary')}}" href="{{route('timesheet.maintain', $timesheet)}}" style="color: white !important">
                                    {{isset($timesheet->vendor_inv_status->description) ? substr($timesheet->vendor_inv_status->description,0,1) : ''}}
                                </a></td>
                            <td>{{$timesheet->cust_inv_ref}}</td>
                            <td>{{$timesheet->vendor_invoice_ref}}</td>
                        @endif
                        <td class="text-right">{{(count($timesheet->time_exp) > 0)?number_format($timesheet->time_exp[0]->amount,2,'.',','):number_format(0,2,'.',',')}}</td>
                        <!-- @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <td class="text-left">
                            {{$timesheet->invoice_note}}
                        </td>
                        @endif -->
                        <td class="text-right" @if(!isset($_GET['inv_amount']) || ($_GET['inv_amount'] != 1)) style="display: none;" @endif>{{number_format($invoice_amounts[$timesheet->id], 2, '.', ',')}}</td>
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                            <td class="text-center">
                                @if($timesheet->timesheet_lock == 1)
                                    <a class="toggleLockButton" type="button" data-timesheet-id="{{ $timesheet }}">
                                        <span class="badge badge-warning px-2"><i class="fas fa-lock"></i></span>
                                    </a>
                                @else
                                    <a class="toggleLockButton" type="button" data-timesheet-id="{{ $timesheet }}">
                                        <span class="badge badge-warning px-2"><i class="fas fa-unlock"></i></span>
                                    </a>
                                @endif
                            </td>
                        @else
                            <td class="text-center">
                                @if($timesheet->timesheet_lock == 1)
                                    <span class="badge badge-warning px-2"><i class="fas fa-lock"></i></span>
                                @else
                                    <span class="badge badge-warning px-2"><i class="fas fa-unlock"></i></span>
                                @endif
                            </td>
                        @endif
                        {{-- <td>{{isset($timesheet->status->description)?$timesheet->status->description:''}}</td> --}}
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <td class="text-center" nowrap>
                            @if($timesheet->timesheet_lock != 1)
                                @if($have_timelines[$timesheet->id])
                                    <a href="{{route('timeline.show',$timesheet)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                @else
                                    <a href="javascript:void(0)" class="btn btn-success btn-sm disabled mr-1"><i class="fas fa-pencil-alt"></i></a>
                                @endif
                                {{--<a href="{{route('timesheet.edit',$timesheet)}}" class="btn btn-success btn-sm">Edit</a>--}}
                                {{ Form::open(['method' => 'DELETE','route' => ['timesheet.destroy', $timesheet],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-sm btn-danger" {{ ($timesheet->total_hours * 60 + $timesheet->total_minutes) > 0 || (count($timesheet->time_exp) > 0) || (isset($timesheet->vendor_inv_status->description) && substr($timesheet->vendor_inv_status->description,0,1) == 'I') || (isset($timesheet->bill_statusd->description) && substr($timesheet->bill_statusd->description,0,1) == 'I') ? 'disabled' : '' }}><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                            @endif
                        </td>
                        @else
                        <td></td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No timesheets match those criteria.</td>
                    </tr>
                @endforelse
                @if(count($timesheets) > 0)
                    <tr>
                        <th>TOTAL</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th class="text-center">{{minutesToTime($total_billable_hours)}}</th>
                        <th class="text-center">{{minutesToTime($total_non_billable_hours)}}</th>
                        <th class="text-center">{{minutesToTime($total_hours)}}</th>
                        <th>&nbsp;</th>
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        @endif
                        <th class="text-right">{{number_format($total_claim,2,'.', ',')}}</th>
                        <th class="text-right" @if(!isset($_GET['inv_amount']) || ($_GET['inv_amount'] != 1)) style="display: none;" @endif>{{number_format($invoice_amount_total,2,'.', ',')}}</th>
                        <th>&nbsp;</th>
                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <th>&nbsp;</th>
                        @endif
                        {{-- <th>&nbsp;</th> --}}
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $timesheets->firstItem() }} - {{ $timesheets->lastItem() }} of {{ $timesheets->total() }}
                        </td>
                        <td>
                            {{ $timesheets->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
@endsection

@section('extra-js')

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>--}}
   {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha256-fzFFyH01cBVPYzl16KT40wqjhgPtq6FFUB6ckN2+GGw=" crossorigin="anonymous"></script>--}}
    <script>

        

        function toggleTimesheetLock(timesheet) {
            let parameters = {
                timesheet: timesheet.id,
                lock_status: timesheet.timesheet_lock == 1 ? 0 : 1
            };
            
            axios.post('/timesheet/changelock', parameters)
                .then(response => {
                    toastr.success('<strong>Success!</strong> Lock status successfully changed');
                    toastr.options.timeOut = 1000;
                    window.location.reload();
                })
                .catch(function (err) {
                    console.log(err);
                });
        }

        $(function () {

            $('.toggleLockButton').on('click', function (event) {
                event.preventDefault();
                
                var timesheetId = $(this).data('timesheet-id');  // Use timesheet ID here
                var confirmation = "Are you sure you want to toggle the lock on this timesheet?";

                confirmDialog(confirmation, function () {
                    toggleTimesheetLock(timesheetId);
                });

                // if (confirmation) {
                //     toggleTimesheetLock(timesheetId);
                // }
            });

            $('.invoice-status').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })


    </script>
@endsection
