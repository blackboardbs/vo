@extends('adminlte.default')

@section('title') Expense Tracking @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($actions['create'])
            <a href="{{route('exptracking.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Expense</a>
            <x-export route="exptracking.index"></x-export>
        @endif
        @if($actions['update'])
            <button class="btn btn-danger float-right mr-2" id="reject" disabled  data-toggle="modal" data-target="#expense_rejection"><i class="fas fa-thumbs-down"></i> Reject</button>
            <button class="btn btn-success float-right mr-2" id="approve" disabled><i class="fas fa-thumbs-up"></i> Approve</button>
            <button class="btn btn-success float-right mr-2" id="process_payment"disabled  data-toggle="modal" data-target="#exampleModal"><i class="fas fa-university"></i> Process Payment</button>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="mt-3 form-inline searchform" id="searchform">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company_id', $company_dropdown, old('company_id', 0), ['class' => 'form-control w-100 search', 'id' => 'company_id'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('account_id',$account_dropdown,old('account_id', 0),['class'=>'form-control w-100 search', 'id' => 'account_id'])}}
                        <span>Account</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('account_element_id',[],old('account_element_id'),['class'=>'form-control w-100 search', 'id' => 'account_element_id', 'placeholder' => "All"])}}
                        <span>Account Element</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource_id',[],old('resource_id'),['class'=>'form-control w-100 search', 'id' => 'resource_id', 'placeholder' => "All"])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('approval_status_id',$expense_approval_status,old('approval_status_id')??1,['class'=>'form-control w-100 search', 'id' => 'approval_status_id', 'placeholder' => "All"])}}
                        <span>Approval Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('payment_status_id',$expense_payment_status,old('payment_status_id')??1,['class'=>'form-control w-100 search', 'id' => 'payment_status_id', 'placeholder' => "All"])}}
                        <span>Payment Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('expense_type_id',$expense_type_dropdown,old('expense_type_id'),['class'=>'form-control w-100 search', 'id' => 'expense_type_id', 'placeholder' => "All"])}}
                        <span>Expense Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('approver_id',[],old('approver_id'),['class'=>'form-control w-100 search', 'id' => 'approver_id', 'placeholder' => "All"])}}
                        <span>Approved By</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('claimable_id',$yes_or_no_dropdown,old('claimable_id'),['class'=>'form-control w-100 search', 'id' => 'claimable_id', 'placeholder' => "All"])}}
                        <span>Claimable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('billable_id',$yes_or_no_dropdown,old('billable_id'),['class'=>'form-control w-100 search', 'id' => 'billable_id', 'placeholder' => "All"])}}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('assignment_id',$assignment_drop_down,old('assignment_id'),['class'=>'form-control w-100 search', 'id' => 'assignment_id', 'placeholder' => "All"])}}
                        <span>Assignment</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('payment_reference',old('payment_reference'),['class'=>'form-control form-control-sm w-100 search', 'id' => 'payment_reference'])}}
                        <span>Payment Reference</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20%;">
                <a href="{{route('exptracking.index')}}" class="btn btn-info w-100">Clear Filter</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            {{--<form id="expense_form" method="post">--}}
            {!! Form::open(['id'=>'expense_form', 'method' => 'post']) !!}
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('resource.first_name','Name')</th>
                    <th>@sortablelink('transaction_date', 'Date')</th>
                    <th>@sortablelink('account.id','Account')</th>
                    <th>@sortablelink('account_element.id','Element')</th>
                    <th>@sortablelink('assignment.project_id','Assignment')</th>
                    <th>@sortablelink('yearwk','Week')</th>
                    <th>@sortablelink('claimable','Claimable')</th>
                    <th>@sortablelink('billable','Billable')</th>
                    <th>Type</th>
                    <th>@sortablelink('payment_reference','Payment Reference')</th>
                    <th>@sortablelink('document','Document')</th>
                    <th class="text-right">@sortablelink('amount','Amount')</th>
                    @if($actions['update'])
                        <th>
                            <input type="checkbox" id="select_all" >
                            Select All
                        </th>
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @php
                    $page_total_exp = 0;
                @endphp
                @forelse($expenses as $referrer)
                @php
                    $page_total_exp += floatval($referrer->amount);
                @endphp
                    <tr>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->resource?->resource_name}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->transaction_date}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->account?->description}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->account_element?->description}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->assignment?->project?->name}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->yearwk}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->claimable?->name}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->billable?->name}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->expense_type?->description}}</a></td>
                        <td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->payment_reference}}</a></td>
                        <td>{!! ($referrer->document != null ? '<a href="'.route('exp_tracking',['q'=>$referrer->document]).'" target="_blank">View File</a>' : '') !!}</td>
                        <td class="text-right"><a href="{{route('exptracking.show',$referrer)}}">{{number_format(floatval($referrer->amount),2,'.',',')}}</a></td>
                        {{--<td><a href="{{route('exptracking.show',$referrer)}}">{{$referrer->status->description}}</a></td>--}}
                        @if($actions['update'])
                            <td>
                                @if($referrer->approval_status == 1 && $referrer->claimable?->value == 1 && $referrer->payment_status != 2)
                                    <input type="checkbox" name="expense_id[]" class="expenses" value="{{$referrer->id}}">
                                @endif
                                @if($referrer->approval_status == 2 && $referrer->payment_status != 2)
                                    <input type="checkbox" name="expense_id[]" class="expenses" value="{{$referrer->id}}">
                                @endif
                            </td>

                            <td>
                                <div class="d-flex">
                            @if($referrer->approval_status == 1 && $referrer->claimable?->value == 1 && $referrer->payment_status != 2)
                                <a href="{{route('exptracking.approve', $referrer)}}" class="btn btn-sm btn-primary mr-1"><i class="fas fa-check"></i></a>
                                {{--<a href="{{route('exptracking.reject', $referrer)}}" class="btn btn-sm btn-secondary">Reject</a>--}}
                                <button type="button" class="btn btn-sm btn-danger launch_single_rejection mr-1" data-id="{{$referrer->id}}" data-toggle="modal" data-target="#reject_single"><i class="fas fa-times"></i></button>
                            @endif
                            @if($referrer->approval_status == 2 && $referrer->payment_status != 2)
                                {{--<a href="{{route('exptracking.pay', $referrer)}}" class="btn btn-sm btn-secondary">Pay</a>--}}
                                <button type="button" class="btn btn-sm btn-secondary single-pay" data-id="{{$referrer->id}}" data-toggle="modal" data-target="#single_pay">Pay</button>
                            @endif
                            @if($referrer->approval_status == 3)
                                <span class="badge badge-info">Rejected</span>
                            @elseif(!isset($referrer->payment_date))
                                <a href="{{route('exptracking.edit',$referrer)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                <a href="{{route('exptracking.delete', $referrer)}}" class="btn btn-sm btn-danger delete"><i class="fas fa-trash"></i></a>
                            @else
                                <span class="badge badge-info">Paid</span>
                            @endif
                                {{--{{ Form::open(['method' => 'DELETE','route' => ['exptracking.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}--}}
                                </div>
                                
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No results match the search criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="10"></th>
                    <th>TOTAL</th>
                    <th class="text-right">{{number_format(floatval($page_total_exp),2,'.',',')}}</th>
                    <th colspan="2"></th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $expenses->firstItem() }} - {{ $expenses->lastItem() }} of {{ $expenses->total() }}
                        </td>
                        <td>
                            {{ $expenses->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Payment Reference</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                {{Form::label('payment_reference', 'Payment Reference')}}
                                {{Form::text('payment_reference', old('payment_reference'), ['class' => 'form-control form-control-sm'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('payment_date', 'Payment Date')}}
                                {{Form::text('payment_date', old('payment_date'), ['class' => 'form-control form-control-sm datepicker'])}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="confirm_payment_process">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="expense_rejection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Rejection Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                {{Form::label('rejection_message', 'Rejection Message')}}
                                {{Form::textarea('rejection_message', old('rejection_message'), ['class' => 'form-control form-control-sm'])}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="confirm_rejection">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="modal fade" id="reject_single" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Rejection Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{Form::hidden('expense_id', old('expense_id'), ['id' => 'rejection_single_id'])}}
                            <div class="form-group">
                                {{Form::label('rejection_single_message', 'Rejection Message')}}
                                {{Form::textarea('rejection_single_message', old('rejection_single_message'), ['class' => 'form-control form-control-sm', 'id'=> 'rejection_single_message'])}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="confirm_single_rejection">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="single_pay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Payment Reference</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{Form::hidden('single_pay_id', old('single_pay_id'), ['id' => 'single_pay_id'])}}
                            <div class="form-group">
                                {{Form::label('single_payment_reference', 'Payment Reference')}}
                                {{Form::text('single_payment_reference', old('single_payment_reference'), ['class' => 'form-control form-control-sm', 'id' => 'single_payment_reference'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('single_payment_date', 'Payment Date')}}
                                {{Form::text('single_payment_date', old('single_payment_date'), ['class' => 'form-control form-control-sm datepicker', 'id'=>'single_payment_date'])}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="confirm_single_payment_process">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('extra-js')

    <script>
        $(document).on('ready', function (){
            const url = new URL(window.location.toLocaleString());
            const account_element_dropdown = @json($account_element_dropdown);
            const resource_dropdown = @json($resource_dropdown);
            const resource_ids = @json($resource_ids);
            let dropdown = [];
            let options = "<option value='0'>Select Resource</option>";
            let resource_option = "<option value='0' selected='selected' disabled>Select Resource</option>";

            account_element_dropdown.sort((a, b) => {
                let textA = a.description.toUpperCase();
                let textB = b.description.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });

            dropdown = url.searchParams.get('account_id') && (url.searchParams.get('account_id') != 0)
                ? account_element_dropdown.filter(item => item.account_id == url.searchParams.get('account_id'))
                : account_element_dropdown;

            dropdown.forEach(v => options += `<option value='${v.id}'>${v.description}</option>`)

            if(url.searchParams.get('account_id') && url.searchParams.get('account_id') > 0){
            getElements(url.searchParams.get('account_id'))
                }

            if(url.searchParams.get('account_element_id')){
            $("#account_element_id").html(options).val(url.searchParams.get('account_element_id'))
            } else {
            $("#account_element_id").val('');                
            }
            if(url.searchParams.get('resource_id')){
                $("#resource_id").html(filterDrops(resource_dropdown, resource_ids)).attr('placeholder', 'Select Resource').val(url.searchParams.get('resource_id'));
            } else {
                $("#resource_id").html(filterDrops(resource_dropdown, resource_ids)).attr('placeholder', 'Select Resource');
            }
if(url.searchParams.get('approver_id')){
            $("#approver_id").html(filterDrops(resource_dropdown, @json($approved_by_ids))).attr('placeholder', 'Select Resource').val(url.searchParams.get('approver_id'));
} else {
    $("#approver_id").html(filterDrops(resource_dropdown, @json($approved_by_ids))).attr('placeholder', 'Select Resource');
}

        })

        function filterDrops(source_array, filter_array) {
            let resource_option = "<option value='0'>All</option>";
            filter_array.forEach(res => source_array.forEach((v) => {
                if (res == v.id){
                    resource_option += `<option value="${v.id}">${v.full_name}</option>`
                }
            }))

            return resource_option;
        }

        $(function () {
            $('#account').on('change', function (e) {
                axios.get('/account_element/get/'+$(this).val())
                    .then(function (response) {

                        jQuery.each( response.data, function( i, val ) {
                            $("#account_element_id").append("<option value='"+i+"'>"+val+"</option>").trigger("chosen:updated");
                        });
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
            })
        })

        function getElements(val){
            fetch('/account_element/get/'+val)
                    .then(response => response.json()).then(d => {
                        for(let v in d){
                            $("#account_element_id").append("<option value='"+v+"'>"+d[v]+"</option>").trigger("chosen:updated");
                        };
                    })
        }
        
        $(function () {
            $("#approve").on('click', function (e) {
                e.preventDefault();
                if (!confirm('Are you sure you want to approve these expenses?')){
                    return false;
                }
                $("#expense_form").attr("action","{{url('exptracking/batchapprove')}}").submit();
            })

            $("#confirm_payment_process").on('click', function (e) {
                e.preventDefault();
                $("#expense_form").attr("action","{{url('exptracking/batchpay')}}").submit();
            })

            $("#confirm_rejection").on('click', function (e) {
                e.preventDefault();
                $("#expense_form").attr("action","{{url('exptracking/batchreject')}}").submit();
            })

            $(".delete").on("click", function(){
                if(!confirm("Are you sure you want to delete this record?")) return false;
            });

            $("#expense_form").on('click', function (event) {
                if ($(event.target).is('input')) {
                    $("#approve, #process_payment, #reject").attr('disabled', false)
                }
            })
        })

        $(document).on("click", ".launch_single_rejection", function () {
            let expenseId = $(this).data('id');
            $(".modal-body #rejection_single_id").val( expenseId );
        });

        $(document).on("click", ".single-pay", function () {
            let expenseId = $(this).data('id');
            $(".modal-body #single_pay_id").val( expenseId );
            console.log(expenseId)
        });

        $(function (){
            $("#confirm_single_rejection").on('click', function (){
                let expenseID = $("#rejection_single_id").val();
                let rejection_message = $("#rejection_single_message").val();
                axios.get('/exptracking/'+expenseID+'/reject',{
                    params: {
                        message: rejection_message
                    }
                })
                    .then(function (response) {
                        location.reload();
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    });
            })
        });

        $(function (){
            $("#confirm_single_payment_process").on('click', function (){
                let expenseID = $("#single_pay_id").val();
                let reference = $("#single_payment_reference").val();
                let payment_date = $("#single_payment_date").val();
                axios.get('/exptracking/'+expenseID+'/pay',{
                    params: {
                        reference: reference,
                        payment_date: payment_date
                    }
                })
                    .then(function (response) {
                        location.reload();
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    });
            })
        });

        $(() => {
            $('#select_all').on('click',function(){
                let expenses = []
                if(this.checked){
                    $('.expenses').each(function(){
                        this.checked = true;
                        expenses.push(this.value)
                    });
                    session('{{route('expense.session.store')}}', expenses)
                }else{
                    $('.expenses').each(function(){
                        this.checked = false;
                        expenses.push(this.value)
                    });
                    session('{{route('expense.session.pull')}}', expenses)
                }
            });

            $(".expenses").on('change', e => {
                if (e.target.checked){
                    session('{{route('expense.session.store')}}', [e.target.value])
                }else {
                    session('{{route('expense.session.pull')}}', [e.target.value])
                }
            })

            $('.expenses').on('click',function(){
                selectedAll()
            });

            selectedAll()
        })

        function selectedAll(){
            if($('.expenses:checked').length === $('.expenses').length){
                $('#select_all').prop('checked',true);
            }else{
                $('#select_all').prop('checked',false);
            }
        }

        function session(url, exp_id){
            axios.post(url, {ids: exp_id})
                .then(response => {
                    console.log(response)
                }).catch(error => {
                console.log(error)
            })
        }

    </script>
@endsection