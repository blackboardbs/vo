@extends('adminlte.default')

@section('title') View Expense Tracking @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('exptracking.edit', $expenses[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Expense Tracking</a>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive" id="no-more-tables">
            @foreach($expenses as $result)
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Resource</th>
                    <td data-title="Resource:">{{$result->resource?->name()}}</td>
                    <th>Date</th>
                    <td data-title="Date:">{{$result->transaction_date}}</td>
                </tr>
                <tr>
                    <th>Account:</th>
                    <td data-title="Account:">{{$result->account?->description}}</td>
                    <th>Account Element:</th>
                    <td data-title="Account Element:">{{$result->account_element?->description}}</td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td colspan="3" data-title="Description:">{{$result->description}}</td>
                </tr>
                <tr>
                    <th>Assignment:</th>
                    <td data-title="Assignment:">{{$result->assignment?->project?->name}}</td>
                    <th>Week:</th>
                    <td data-title="Week:">{{$result->yearwk}}</td>
                </tr>
                <tr>
                    <th>Claimable:</th>
                    <td data-title="Claimable:">{{Form::checkbox('claimable',1,$result->claimable?->value)}}</td>
                    <th>Billable:</th>
                    <td data-title="Billable:">{{Form::checkbox('claimable',1,$result->billable?->value)}}</td>
                </tr>
                <tr>
                    <th>Payment Reference:</th>
                    <td data-title="Payment Reference:">{{($result->payment_reference != null ? $result->payment_reference : '&nbsp;')}}</td>
                    <th>Document:</th>
                    <td data-title="Document:">{!! ($result->document != null ? '<a href="'.route('exp_tracking',['q'=>$result->document]).'" target="_blank">View File</a>' : '&nbsp;')!!}</td>
                </tr>
                <tr>
                    <th>Amount:</th>
                    <td data-title="Amount:">{{$result->amount}}</td>
                    <th>Status:</th>
                    <td data-title="Status:">{{$result->status?->description}}</td>
                </tr>
                <tr>
                    <th>Cost Center:</th>
                    <td data-title="Cost Center:">{{$result->cost_center_description?->description}}</td>
                    <th>Approved By:</th>
                    <td data-title="Approved By:">{{ $result->approver?->name() }}</td>
                </tr>
                <tr>
                    <th>Approved On:</th>
                    <td data-title="Approved On:">{{($result->approved_on != null ? $result->approved_on : '&nbsp;')}}</td>
                    <th>Approval Status:</th>
                    <td data-title="Approval Status:">
                        @if($result->approval_status == 1)
                            New
                        @elseif($result->approval_status == 2)
                            Approved
                        @elseif($result->approval_status == 3)
                            Rejected
                        @elseif($result->approval_status == 4)
                            Updated
                        @else
                            Unknown status
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Payment Date:</th>
                    <td data-title="Payment Date:">{{$result->payment_date}}</td>
                    <th>Status:</th>
                    <td data-title="Status:">
                        @if($result->payment_status == 1)
                            New
                        @elseif($result->payment_status == 2)
                            Paid
                            @else
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Expense Type:</th>
                    <td>{{$result->expense_type?->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
                @endforeach
        </div>
    </div>
@endsection