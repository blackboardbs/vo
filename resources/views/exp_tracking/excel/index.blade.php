<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Date</th>
        <th>Account</th>
        <th>Element</th>
        <th>Assignment</th>
        <th>Week</th>
        <th>Claimable</th>
        <th>Billable</th>
        <th>Type</th>
        <th>Payment Reference</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($expenses as $referrer)
        <tr>
            <td>{{$referrer->resource?->resource_name}}</td>
            <td>{{$referrer->transaction_date}}</td>
            <td>{{$referrer->account?->description}}</td>
            <td>{{$referrer->account_element?->description}}</td>
            <td>{{$referrer->assignment?->project?->name}}</td>
            <td>{{$referrer->yearwk}}</td>
            <td>{{$referrer->claimable?->name}}</td>
            <td>{{$referrer->billable?->name}}</td>
            <td>{{$referrer->expense_type?->description}}</td>
            <td>{{$referrer->payment_reference}}</td>
            <td>{{round((int)$referrer->amount, 2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>