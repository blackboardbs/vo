@extends('adminlte.default')

@section('title') Edit Expense Tracking @endsection

@section('header')
<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
    <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
    <a href="javascript:void(0)" onclick="saveForm('exptracking')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
    <div class="row">
        @isset($flash_info)
            <div class="alert alert-info alert-dismissible blackboard-alert">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <strong>Notice.</strong> {{$flash_info}}
            </div>
        @endisset
    </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
    <hr />
    <div class="table-responsive" id="no-more-tables">
        {{Form::open(['url' => route('exptracking.update',$exp_tracking), 'method' => 'PATCH','class'=>'mt-3','files'=>'true','autocomplete' => 'off','id'=>'exptracking'])}}
        <table class="table table-bordered table-sm">
            <tbody>
            <tr>
                <th>Account:</th>
                <td data-title="Account:">{{Form::select('account',$sidebar_process_account,$exp_tracking->account_id,['class'=>'form-control form-control-sm ','id'=>'account'])}}

                    <div class="invalid-feedback" id="account_error">

                    </div>
                    </td>
                <th>Account Element:</th>
                <td data-title="Account Element:">{{Form::hidden('haccount_element_id',$exp_tracking->account_element_id,array('id'=>'haccount_element_id'))}}
                    <select name="account_element" id="account_element" class="form-control form-control-sm ">
                        <option value="0">Account Element</option>

                    </select>

                    <div class="invalid-feedback" id="account_element_error">

                    </div>
                    </td>
            </tr>
            <tr>
                <th>Assignment:</th>
                <td data-title="Assignment:">{{Form::select('assignment',$assignment,$exp_tracking->assignment_id,['class'=>'form-control form-control-sm ','id'=>'assignment', 'placeholder' => 'Please select...'])}}</td>
                <th>Week:</th>
                <td data-title="Week:"><select name="yearwk" class="form-control form-control-sm " id="week">
                        @foreach($yearwk as $key => $value)
                            <option value="{{$value}}" {{($exp_tracking->yearwk == $value ? 'selected' : '')}}>{{$value}}</option>
                        @endforeach
                    </select>

                        <div class="invalid-feedback" id="week_error">

                        </div>
                </td>
            </tr>
            <tr>
                <th>Description:</th>
                <td data-title="Description:">{{Form::text('description',$exp_tracking->description,['class'=>'form-control form-control-sm','placeholder'=>'Description','id'=>'description'])}}

                        <div class="invalid-feedback" id="description_error">

                        </div>
                </td>
                <th>Company:</th>
                <td data-title="Company:">{{Form::select('company',$sidebar_process_company,$exp_tracking->company_id,['class'=>'form-control form-control-sm ', 'id' => 'company'])}}

                        <div class="invalid-feedback" id="company_error">

                        </div>
                </td>
            </tr>
            <tr>
                <th>Resource</th>
                <td data-title="Resource:">{{Form::select('resource',$resource,$exp_tracking->employee_id,['class'=>'form-control form-control-sm ','id'=>'resource'])}}

                        <div class="invalid-feedback" id="resource_error">

                        </div>
                </td>
                <th>Date</th>
                <td data-title="Date:">{{Form::text('exp_date',$exp_tracking->transaction_date,['class'=>'datepicker form-control form-control-sm','id'=>'date'])}}

                        <div class="invalid-feedback" id="date_error">

                        </div>
                </td>
            </tr>
            <tr>
                <th>Claimable:</th>
                <td data-title="Claimable:">{{Form::checkbox('claimable',1,$exp_tracking->claimable?->value)}}</td>
                <th>Billable:</th>
                <td data-title="Billable:">{{Form::checkbox('billable',1,$exp_tracking->billable?->value)}}</td>
            </tr>
            <tr>
                <th>Payment Reference:</th>
                <td data-title="Payment Reference:">{{Form::text('payment',$exp_tracking->payment_reference,['class'=>'form-control form-control-sm','placeholder'=>'Payment Reference','id'=>'payment'])}}

                    <div class="invalid-feedback" id="payment_error">

                    </div>
                </td>
                <th>Document:</th>
                <td data-title="Document:">
                    {{Form::file('document',['class'=>'form-control form-control-sm'. ($errors->has('document') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
            @if($exp_tracking->document !== null) <small>Leave empty to keep <a href="{{route('exp_tracking',['q'=>$exp_tracking->document])}}" target="_blank">previous file.</a></small> @else @endif
                    {{Form::hidden('old_document',$exp_tracking->document)}}

                </td>
            </tr>
            <tr>
                <th>Amount:</th>
                <td data-title="Amount:">{{Form::text('amount',$exp_tracking->amount,['class'=>'form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'0.00','id'=>'amount'])}}

                    <div class="invalid-feedback" id="amount_error">

                    </div>
                </td>
                <th>Status:</th>
                <td data-title="Status:">{{Form::select('status',$sidebar_process_statuses,$exp_tracking->status_id,['class'=>'form-control form-control-sm ','id'=>'status'])}}

                    <div class="invalid-feedback" id="status_error">

                    </div>
                </td>
            </tr>
            <tr>
                <th>Cost Center:</th>
                <td data-title="Cost Center:">{{Form::select('cost_center',$cost_center_dropdown, $exp_tracking->cost_center,['class'=>'form-control form-control-sm '. ($errors->has('cost_center') ? ' is-invalid' : ''),'placeholder'=>'Cost Center','id'=>'cost_center'])}}

                        <div class="invalid-feedback" id="status_error">

                        </div>
                </td>
                <th>Approved By:</th>
                <td data-title="Approved By:">{{Form::select('approved_by',$resource,$exp_tracking->approved_by,['class'=>'form-control form-control-sm ','placeholder' => 'Approved By','id'=>'approve_by'])}}</td>
            </tr>
            <tr>
                {{----}}
                <th>Approval Status:</th>
                <td data-title="Approval Status:">{{Form::select('approval_status', $approval_status_dropdown ,$exp_tracking->approval_status,['class'=>'form-control form-control-sm '. ($errors->has('approval_status') ? ' is-invalid' : ''),'placeholder' => 'Approval Status'])}}</td>
                <th>Payment Status:</th>
                <td data-title="Payment Status:">{{Form::select('payment_status', [1 => 'New', 2 => 'Paid'] ,$exp_tracking->payment_status,['class'=>'form-control form-control-sm '. ($errors->has('payment_status') ? ' is-invalid' : ''),'placeholder' => 'Payment Status'])}}</td>
            </tr>
            <tr>
                <th>Approved On:</th>
                <td data-title="Approved On:">{{Form::text('approved_on',$exp_tracking->approved_on,['class'=>'form-control form-control-sm datepicker'. ($errors->has('approved_on') ? ' is-invalid' : ''),'placeholder'=>'Approved On'])}}</td>
                <th>Payment Date:</th>
                <td data-title="Payment Date:">{{Form::text('payment_date',$exp_tracking->payment_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('payment_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Date'])}}</td>
            </tr>
            <tr>
                <th>Expense Type:</th>
                <td>
                    {{Form::select('expense_type_id', $expense_type_dropdown, $exp_tracking->expense_type_id, ['class'=>'form-control form-control-sm ', 'placeholder' => "Select Expense Type"])}}
                </td>
                <th></th>
                <td></td>
            </tr>
            </tbody>
        </table>
        {{Form::close()}}
    </div>
</div>
@endsection

@section('extra-js')

    <script>
    $('document').ready(function() {
        var account = $('#account').val();

        if(account) {
            $.ajax({
                url: '/api/plan_exp/account_element/'+account,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('#account_element').empty();


                    $.each(data, function(key, value){
                        let selected  = '';

                        if(key == '{{ $exp_tracking->account_element_id }}' ){
                            selected = 'selected';
                        }

                        $('#account_element').append('<option value="'+ key +'"' + selected + '>' + value + '</option>');

                    });


                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");

                }
            });
        }


        $('#account').change(function() {
            var account = $('#account').val();

            if(account) {
                $.ajax({
                    url: '/api/plan_exp/account_element/'+account,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('#account_element').empty();


                        $.each(data, function(key, value){
                            let selected  = '';

                            if(key == '{{ $exp_tracking->account_element_id }}' ){
                                selected = 'selected';
                            }

                            $('#account_element').append('<option value="'+ key +'"' + selected + '>' + value + '</option>');

                        });


                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");

                    }
                });
            } else {
                $('select[name="account_element"]').empty();
            }
        });
        
        $('#cost_center').change(function() {
            var account = $('#cost_center').val();

            if(account) {
                $.ajax({
                    url: '/cost_approve/get/'+account,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('#approve_by').empty();


                        $.each(data, function(key, value){

                            $('#approve_by').append('<option value="'+ key +'">' + value + '</option>');

                        });


                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");

                    }
                });
            } else {
                $('select[name="approve_by"]').empty();
            }
        });

        $(function () {
            $('.get_company').on('change', function () {
                axios.post('/get_ass_company', {
                    assignment_id: $('.get_company').val()
                })
                    .then(function (response) {
                        console.log(response)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                axios.post('/get_resource', {
                    assignment_id: $('.get_company').val()
                })
                    .then(function (response) {
                        console.log(response)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        });

        $('#btn-submit').on('click',function(e){
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let account = $("#account").val();
            let account_element = $("#account_element").val();
            let description = $("#description").val();
            let company = $("#company").val();
            let amount = $("#amount").val();
            let resource = $("#resource").val();
            let date = $("#date").val();
            let status = $("#status").val();
            let cost_center = $("#cost_center").val();
            let week = $("#week").val();

            $.ajax({
                type: "POST",
                url: '/exptracking/validate',
                data: {
                    account:account,
                    account_element:account_element,
                    description:description,
                    company:company,
                    amount:amount,
                    resource:resource,
                    date:date,
                    status:status,
                    cost_center:cost_center,
                    week:week
                },
                success: function(data) {
                    if(data.errors) {
                        $('.invalid-feedback').hide();
                        $('.form-control').removeClass('is-invalid');
                        $.each(data.errors, function (key, value) {
                            $('#' + key).addClass('is-invalid');
                            $("#" + key + "_error").show();
                            $("#" + key + "_error").text(value);
                        });
                    }
                    if(data.success){
                        /*return true;*/
                        $('#exptracking').submit();
                    }
                }
            });
        })
    });
</script>
@endsection
