@extends('adminlte.default')

@section('title') Add Expense Tracking @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right d-none d-lg-block d-md-block">
        <button onclick="history.back()" class="btn btn-dark ml-1" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('exptracking')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
        <div class="row">
            @isset($flash_info)
                <div class="alert alert-info alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Notice.</strong> {{$flash_info}}
                </div>
            @endisset
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid" style="overflow: hidden">
        <hr />
        <div class="table-responsive" id="no-more-tables" style="min-height: 650px;overflow-x:hidden !important;">
            {{Form::open(['url' => route('exptracking.store'), 'method' => 'post','class'=>'mt-3','files'=>'true','autocomplete' => 'off','id'=>'exptracking'])}}
            <div class="row p-0 pb-3">
                <div class="col-md-12 col-sm-12 bg-dark px-3 py-1">Complete the form below.</div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-12 pb-2">Account: <small class="text-muted d-none d-sm-inline">(required)</small></div>
                <div class="col-md-4 col-sm-12 pb-2" data-title="Account:">{{Form::select('account',$account_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('account') ? ' is-invalid' : ''), 'id'=>'account'])}}
                    @foreach($errors->get('account') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
                <div class="col-md-2 col-sm-12 pb-2">Account Element: <small class="text-muted d-none d-sm-inline">(required)</small></div>
                <div class="col-md-4 col-sm-12 pb-2" data-title="Account Element:"><select name="account_element" id="account_element" class="form-control form-control-sm {{$errors->has('account_element') ? ' is-invalid' : ''}}">
                        <option value="0">Account Element</option>

                    </select>
                    @foreach($errors->get('account_element') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-12 pb-2">Assignment:</div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Assignment:">{{Form::select('assignment',$assignment,0,['class'=>'form-control form-control-sm  get_company','id'=>'assignment'])}}

                            <div class="invalid-feedback">

                            </div>
                    </div>
                    <div class="col-md-2 col-sm-12 pb-2">Week<span class="assignment-dependant" style="display: none"> <small class="text-muted d-none d-sm-inline">(required)</small></span> </div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Week:"><select name="yearwk" class="form-control form-control-sm {{$errors->has('yearwk') ? ' is-invalid' : ''}}" id="week">
                        @foreach($yearwk as $key => $value)
                            <option value="{{$value}}">{{$value}}</option>
                        @endforeach
                        </select>

                        @foreach($errors->get('yearwk') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-12 pb-2">Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Description:">{{Form::text('description',old('description'),['class'=>'form-control form-control-sm '. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description','id'=>'description'])}}

                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-2 col-sm-12 pb-2">Company: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Company:">{{Form::select('company',$company_dropdown,$conf?->company_id,['class'=>'form-control form-control-sm'. ($errors->has('company') ? ' is-invalid' : ''), 'id' => 'company'])}}

                        @foreach($errors->get('company') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-12 pb-2">Resource<span class="assignment-dependant" style="display: none"> <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></span> </div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Resource:">{{Form::select('resource',$resource,auth()->user()->id,['class'=>'form-control form-control-sm'. ($errors->has('resource') ? ' is-invalid' : ''),'id'=>'resource'])}}

                        @foreach($errors->get('resource') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-2 col-sm-12 pb-2">Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Date:">{{Form::text('exp_date',old('exp_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('exp_date') ? ' is-invalid' : ''),'id'=>'date'])}}

                        @foreach($errors->get('exp_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-11 pb-2">Claimable:</div>
                    <div class="col-md-4 col-sm-1 pb-2" data-title="Claimable:">{{Form::checkbox('claimable',1,true)}}</div>
                    <div class="col-md-2 col-sm-11 pb-2">Billable:</div>
                    <div class="col-md-2 col-sm-1 pb-2" data-title="Billable:">{{Form::checkbox('billable')}}</div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-12 pb-2">Document:</div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Document:">{{Form::file('document',['class'=>'form-control form-control-sm','placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                        @foreach($errors->get('document') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</div>
                    {{--<th>Payment Reference:</th>
                    <td data-title="Payment Reference:">{{Form::text('payment',old('payment'),['class'=>'form-control form-control-sm','placeholder'=>'Payment Reference','id'=>'payment'])}}</td>--}}
                    <div class="col-md-2 col-sm-12 pb-2">Amount: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></div>
                    <div class="col-md-4 col-sm-12 pb-2" data-title="Amount:">
                        {{Form::text('amount',old('amount'),['class'=>'form-control form-control-sm'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'0.00','id'=>'amount'])}}
                        @foreach($errors->get('amount') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::hidden('payment_status', 1)}}
                        {{Form::hidden('status',1)}}
                        {{ Form::hidden('cost_center', $conf?->cost_center_id) }}
                        {{Form::hidden('approval_status', 1)}}

                        <div class="invalid-feedback" id="amount_error">

                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-2 col-sm-12 pb-2">Expense Type:</div>
                    <div class="col-md-4 col-sm-12 pb-2">
                        {{Form::select('expense_type_id', $expense_type_dropdown, 1, ['class'=>'form-control form-control-sm ', 'placeholder' => "Select Expense Type"])}}
                    </div>
            </div>
            <div class="row d-md-none d-sm-block d-xs-block pt-3">
                <div class="col-sm-12 text-center">
                    <button onclick="history.back()" class="btn btn-dark ml-1" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
                    <a href="javascript:void(0)" onclick="saveForm('exptracking')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection



@section('extra-js')
    <script>
        $(document).ready(function (){
            if ($("#account").val() > 0){
                accountElement()
            }

            $("#account").on('change', () => {
                accountElement()
            })

            function accountElement() {
                const account_element = @json($account_element_dropdown);
                let options = `<option>Select Option</option>`
                account_element.forEach(v => {
                    if (v.account_id == $("#account").val()){
                        options += `<option value="${v.id}" ${(v.id == "{{old('account_element')}}") ? 'selected': null}>${v.description}</option>`
                    }
                })

                $("#account_element").html(options);
            }
        })
    </script>

@endsection

