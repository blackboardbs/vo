<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_from_3_weeks_ago < 30) ? 'bg-danger': (($utilization_from_3_weeks_ago >= 30 && $utilization_from_3_weeks_ago < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3>{{number_format($utilization_from_3_weeks_ago,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – past 3 weeks</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-pie"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_this_week < 30) ? 'bg-danger': (($utilization_this_week >= 30 && $utilization_this_week < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3>{{number_format($utilization_this_week,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – current week</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-pie"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ number_format($assignment_outstanding_hours,0,'.',',') }}</h3>

                <p>Assignment Hours Available</p>
            </div>
            <div class="icon">
                <i class="fas fa-hourglass-half"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{$outstanding_timesheet_number}}</h3>

                <p>Outstanding Timesheets</p>
            </div>
            <div class="icon">
                <i class="fas fa-calendar-alt"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-6">

        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Open Assignments</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>
                    <tr>
                        <th>Consultant</th>
                        <th>Assignment</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Hours</th>
                        <th>Bill</th>
                        <th>NBill</th>
                        <th>Hrs Out</th>
                        <th>Comp%</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($open_assignments as $assignment)
                        @if($assignment->end_date >= $date)
                            <tr>
                                <td>{{isset($assignment->consultant) ? substr($assignment->consultant->first_name,0,1).' '.$assignment->consultant->last_name : ''}}</td>
                                <td>{{isset($assignment->project) ? $assignment->project->name : ''}}</td>
                                <td>{{$assignment->start_date}}</td>
                                <td>{{$assignment->end_date}}</td>
                                <td>{{$assignment->hours}}</td>
                                <td>{{number_format($assignment->billable_hours,0,'.',',')}}</td>
                                <td>{{number_format($assignment->non_billable_hours,0,'.',',')}}</td>
                                <td>{{($assignment->project_type == 1) ? number_format(($assignment->hours - $assignment->billable_hours),0,'.',',') : number_format(($assignment->hours - $assignment->non_billable_hours),0,'.',',')}}</td>
                                <td>{{($assignment->project_type == 1) ? number_format((($assignment->billable_hours)/$assignment->hours) * 100 ,0,'.',',') .'%' : number_format((($assignment->non_billable_hours)/$assignment->hours)*100 ,0,'.',',') .'%'}}</td>
                            </tr>
                        @endif
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Outstanding Timesheet</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>
                        <tr>
                            <th>Consultant</th>
                            <th>YearWk</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($plan_util as $key => $util)
                        @if($outstanding_timesheets[$key] == null)
                            <tr>
                                <td>{{$user->first_name.' '.$user->last_name}}</td>
                                <td>{{$util->yearwk}}</td>
                            </tr>
                        @endif
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>