<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ $expired_users }}</h3>

                <p>Expired Users</p>
            </div>
            <div class="icon">
                <i class="fas fa-user-times"></i>
            </div>
            <a href="{{route('users.expired')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{$users_to_expire_30}}</h3>

                <p>Users to Expire in 30 days</p>
            </div>
            <div class="icon">
                <i class="fas fa-user-minus"></i>
            </div>
            <a href="{{route('users.expires_in_30')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{$failed_logins}}</h3>

                <p>Failed login attempts</p>
            </div>
            <div class="icon">
                <i class="fas fa-user-secret"></i>
            </div>
            <a href="{{route('logins.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{$timesheets_count}}</h3>

                <p>Usage: Timesheet count</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-line"></i>
            </div>
            <a href="{{route('timesheet.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Number of logins</h4>
            </div>
            @if(count($logs) > 0)
                <div class="card-body">
                    <div class="position-relative mb-4">
                        <canvas id="loginsChart" height="200"></canvas>
                    </div>
                </div>
            @else
                <h5>No Data to show yet</h5>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Number of failed logins</h4>
            </div>
            @if(count($logs) > 0)
                <div class="card-body">
                    <div class="position-relative mb-4">
                        <canvas id="loginFailsChart" height="200"></canvas>
                    </div>
                </div>
            @else
                <h5>No Data to show yet</h5>
            @endif
        </div>
    </div>
</div>

@section('extra-js')
    <script>
        $(function () {
            var loginsCanvasChart = document.getElementById("loginsChart");
            var loginsChart = new Chart(loginsCanvasChart, {
                type: 'bar',
                data: {
                    labels: [@forelse($logs as $log) @if($log->login_status == 1) @php $date = date_create($log->date) @endphp "{{date_format($date, 'd F Y')}}", @endif @empty @endforelse],
                    datasets: [{
                        label: '',
                        data: [@forelse($login_success as $success_count) "{{ $success_count }}", @empty @endforelse],
                        backgroundColor: '#212529',
                        borderColor: '#212529',
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            },
                            gridLines: {
                                display:false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display:false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        })


        $(function () {
            var failsCanvasChart = document.getElementById("loginFailsChart");
            var failsChart = new Chart(failsCanvasChart, {
                type: 'bar',
                data: {
                    labels: [@forelse($logs as $log) @if($log->login_status == 0) @php $date = date_create($log->date) @endphp "{{date_format($date, 'd F Y')}}", @endif @empty @endforelse],
                    datasets: [{
                        label: '',
                        data: [@forelse($login_fail as $fail_count) "{{ $fail_count }}", @empty @endforelse],
                        backgroundColor: '#212529',
                        borderColor: '#212529',
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            },
                            gridLines: {
                                display:false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display:false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        })
    </script>
@endsection

@section('extra-css')
    <style>
        .content-wrapper a {
            color: #eaf0f4 !important;
        }
    </style>
@endsection