<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_from_3_weeks_ago < 30) ? 'bg-danger': (($utilization_from_3_weeks_ago >= 30 && $utilization_from_3_weeks_ago < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3>{{number_format($utilization_from_3_weeks_ago,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – past 3 weeks</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-pie"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_this_week < 30) ? 'bg-danger': (($utilization_this_week >= 30 && $utilization_this_week < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3>{{number_format($utilization_this_week,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – current week</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-pie"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ number_format($assignment_outstanding_hours,0,'.',',') }}</h3>

                <p>Assignment Hours Available</p>
            </div>
            <div class="icon">
                <i class="fas fa-hourglass-half"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($leave_days > 5) ? 'bg-success' : (($leave_days <= 0) ? 'bg-danger' : 'bg-warning')}}">
            <div class="inner">
                <h3>{{$leave_days}}</h3>

                <p>Leave days</p>
            </div>
            <div class="icon">
                <i class="fas fa-calendar-alt"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-6">
        @if(count($plan_util) > 0)
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Outstanding Timesheet</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <tbody>
                    @forelse($plan_util as $key => $util)
                        @if($outstanding_timesheets[$key] == null)
                            <tr>
                                <td>{{$user->first_name.' '.$user->last_name}}</td>
                                <td>{{$util->yearwk}}</td>
                            </tr>
                        @endif
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
       {{-- @if(count($events) > 0)
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Events</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <tbody>
                    @forelse($events as $event)
                        <tr>
                            <td>{{$event->event_date}}</td>
                            <td>{{$event->event}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif--}}

    </div>
    <div class="col-6">
        @if(count($expenses) > 0)
            <div class="card">
                <div class="card-header  bg-gray-light">
                    <h4>Expense Claims</h4>
                </div>
                <div class="card-body">
                    <table class="table no-border table-striped">
                        <thead>
                        <tr>
                            <th>Approver</th>
                            <th>Consultant</th>
                            <th>Project</th>
                            <th>Year Week</th>
                            <th>Approved Date</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($expenses as $expense)
                            <tr>
                                <td>{{($expense->claim_auth_user != 0) ? $expense->claim_approver->first_name.' '.$expense->claim_approver->lasr_name : ''}}</td>
                                <td>{{isset($expense->timesheet->user) ? $expense->timesheet->user->first_name.' '.$expense->timesheet->user->last_name : ''}}</td>
                                <td>{{isset($expense->timesheet->project) ? $expense->timesheet->project->name : ''}}</td>
                                <td>{{isset($expense->timesheet) ? $expense->timesheet->year_week : '' }}</td>
                                <td>{{$expense->claim_auth_date}}</td>
                                <td>{{number_format($expense->amount,2,'.',',')}}</td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        @if(count($assignment_to_expire) > 0)
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Assignments</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>
                    <tr>
                        <th>Assignment</th>
                        <th>End Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($assignment_to_expire as $assignment)
                        <tr>
                            <td>{{isset($assignment->project) ? $assignment->project->name : ''}}</td>
                            <td>{{$assignment->end_date}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($leave_not_approved) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Leave not Approved</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>
                        <tr>
                            <th>Resource</th>
                            <th>Leave Type</th>
                            <th>Days</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Approval</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($leave_not_approved as $leave)
                        <tr>
                            <td>{{isset($leave->resource) ? $leave->resource->first_name.' '.$leave->resource->last_name:''}}</td>
                            <td>{{isset($leave->leave_type) ? $leave->leave_type->description : ''}}</td>
                            <td>{{ $leave->no_of_days }}</td>
                            <td>{{ $leave->date_from }}</td>
                            <td>{{$leave->date_to}}</td>
                            <td><a href="{{route('leave.show', $leave)}}" class="btn btn-dark btn-sm"><i class="far fa-eye"></i> view</a></td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($important_dates) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Important Dates</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">

                    <tbody>
                    @forelse($important_dates as $date)
                        @php $anniversary_date = date_create($date->anniversary_date)@endphp
                        <tr>
                            <td>{{ date_format($anniversary_date, 'F, d') }}</td>
                            <td>{{isset($date->anniversary_type)? $date->name.'\'s '.$date->anniversary_type->description : ''}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
</div>