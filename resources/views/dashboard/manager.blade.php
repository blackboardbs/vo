<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_from_3_weeks_ago < 30) ? 'bg-danger': (($utilization_from_3_weeks_ago >= 30 && $utilization_from_3_weeks_ago < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3 id="util_3">{{round($utilization_from_3_weeks_ago,2)}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – past 3 weeks</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-line"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($utilization_this_week < 30) ? 'bg-danger': (($utilization_this_week >= 30 && $utilization_this_week < 60 ) ? 'bg-warning' : 'bg-success')}}">
            <div class="inner">
                <h3 id="util_1">{{round($utilization_this_week,2)}}<sup style="font-size: 20px">%</sup></h3>

                <p>Utilization – current week</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-line"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ number_format($assignment_hours,0,'.',',') }}</h3>

                <p>Assignment Hours Available</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-line"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box {{($leave_days > 0) ? 'bg-warning' : 'bg-info'}}">
            <div class="inner">
                <h3>{{$leave_days}}</h3>

                <p>Leave days</p>
            </div>
            <div class="icon">
                <i class="fas fa-chart-line"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-lg-6">
        @if(count($outstanding_timesheet) > 0)
            <div class="card">
                <div class="card-header bg-gray-light">
                    <h4>Timesheets Outstanding</h4>
                </div>
                <div class="card-body">
                    <table class="table no-border table-striped">
                        <thead>
                        <tr>
                            <th>Consultant</th>
                            <th>YearWk</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($outstanding_timesheet as $outstanding)
                            @if($outstanding != null)
                                @php $name = explode('-', $outstanding) @endphp
                                <tr>
                                    <td>{{$name[0]}}</td>
                                    <td>{{substr($outstanding, strpos($outstanding, '-') + 1)}}</td>
                                </tr>
                            @endif
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        @if(count($events) > 0)
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Events</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <tbody>
                        @forelse($events as $event)
                            <tr>
                                <td>{{$event->event_date}}</td>
                                <td>{{$event->event}}</td>
                            </tr>
                            @empty
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($leaves) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Leave not Approved</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>

                        <tr>
                            <th>Resource</th>
                            <th>Leave Type</th>
                            <th>Days</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Approval</th>
                        </tr>

                    </thead>
                    <tbody>
                    @forelse($leaves as $leave)
                        <tr>
                            <td>{{isset($leave->resource) ? $leave->resource->first_name.' '.$leave->resource->last_name:''}}</td>
                            <td>{{isset($leave->leave_type) ? $leave->leave_type->description : ''}}</td>
                            <td>{{ $leave->no_of_days }}</td>
                            <td>{{ $leave->date_from }}</td>
                            <td>{{$leave->date_to}}</td>
                            <td><a href="{{route('leave.update', $leave)}}" class="btn btn-outline-primary btn-sm"><i class="far fa-eye"></i> view</a></td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($vendor_inv_outstanding) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Outstanding Vendor Invoice</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>

                        <tr>
                            <th>Invoice Date</th>
                            <th>Invoice Due Date</th>
                            <th>Invoice Value</th>
                            <th>Invoice Statust</th>
                        </tr>

                    </thead>
                    <tbody>
                    @forelse($vendor_inv_outstanding as $invoice)
                        <tr>
                            <td><a href="{{route('view.invoice',$invoice)}}">{{$invoice->vendor_invoice_date}}</a></td>
                            <td><a href="{{route('view.invoice',$invoice)}}">{{$invoice->vendor_due_date}}</a></td>
                            <td><a href="{{route('view.invoice',$invoice)}}">{{$invoice->vendor_invoice_value}}</a></td>
                            <td><a href="{{route('view.invoice',$invoice)}}">{{($invoice->vendor_invoice_status == 1) ? 'Invoiced' : 'Un-allocated'}}</a></td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
    <div class="col-lg-6">
        @if(count($expense_claims) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Expense Claims</h4>
            </div>
            <div class="card-body">
                <table class="table no-border">
                    <thead>
                    <tr>
                        <th>Approver</th>
                        <th>Consultant</th>
                        <th>Project</th>
                        <th>YearWk</th>
                        <th>Approved Date</th>
                        <th class="text-right">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($expense_claims as $claim)
                        <tr>
                            <td>{{isset($claim->claim_approver) ? substr($claim->claim_approver->first_name,0,1).'. '.$claim->claim_approver->last_name:''}}</td>
                            <td>{{isset($claim->timesheet->user) ? substr($claim->timesheet->user->first_name,0,1).'. '.$claim->timesheet->user->last_name : ''}}</td>
                            <td>{{ isset($claim->timesheet->project) ? $claim->timesheet->project->name:'' }}</td>
                            <td>{{ isset($claim->timesheet) ? $claim->timesheet->year_week : '' }}</td>
                            <td>{{$claim->claim_auth_date}}</td>
                            <td class="text-right">{{ number_format($claim->amount,2,'.',',') }}</td>
                        </tr>
                    @empty
                    @endforelse
                    <tr class="bg-gray-light">
                        <th>TOTAL</th>
                        <th colspan="5" class="text-right">{{number_format($expenses_total,2,'.',',')}}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($important_dates) > 0)
        <div class="card">
            <div class="card-header  bg-gray-light">
                <h4>Important Dates</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">

                    <tbody>
                    @forelse($important_dates as $date)
                        @php $anniversary_date = date_create($date->anniversary_date)@endphp
                        <tr>
                            <td>{{ date_format($anniversary_date, 'F, d') }}</td>
                            <td>{{isset($date->anniversary_type)? $date->name.'\'s '.$date->anniversary_type->description : ''}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @if(count($assignments) > 0)
        <div class="card">
            <div class="card-header bg-gray-light">
                <h4>Assignments</h4>
            </div>
            <div class="card-body">
                <table class="table no-border table-striped">
                    <thead>
                        <tr>
                            <th>Assignment</th>
                            <th>End Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($assignments as $assignment)
                        <tr>
                            <td>{{isset($assignment->project) ? $assignment->project->name : ''}}</td>
                            <td>{{$assignment->end_date}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
</div>

@section("extra-js")
    <script>
        function animateValue(id, start, end, duration) {
            // assumes integer values for start and end

            var obj = document.getElementById(id);
            var range = end - start;
            // no timer shorter than 50ms (not really visible any way)
            var minTimer = 50;
            // calc step time to show all interediate values
            var stepTime = Math.abs(Math.floor(duration / range));

            // never go below minTimer
            stepTime = Math.max(stepTime, minTimer);

            // get current time and calculate desired end time
            var startTime = new Date().getTime();
            var endTime = startTime + duration;
            var timer;

            function run() {
                var now = new Date().getTime();
                var remaining = Math.max((endTime - now) / duration, 0);
                var value = Math.round(end - (remaining * range));
                obj.innerHTML = value + "<sup style=\"font-size: 20px\">%</sup>";
                if (value == end) {
                    clearInterval(timer);
                }
            }

            timer = setInterval(run, stepTime);
            run();
        }

        animateValue("util_3", 0, {{round($utilization_from_3_weeks_ago)}}, 1000);
        animateValue("util_1", 0, {{round($utilization_this_week)}}, 1000);
    </script>
@endsection