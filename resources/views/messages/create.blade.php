@extends('adminlte.default')

@section('title') Add Message @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createMessage')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('messages.store'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off','id'=>'createMessage'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th rowspan="2" style="width: 25%">Message</th>
                    <td rowspan="2" colspan="3">{{ Form::textarea('message', old('message'), ['class'=>'form-control form-control-sm'. ($errors->has('message') ? ' is-invalid' : ''), 'rows' => 3]) }}
                        @foreach($errors->get('message') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Message Date</th>
                    <td>{{Form::text('message_date',old('message_date'),['class'=>'form-control datepicker form-control-sm'. ($errors->has('message_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('message_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{Form::select('status_id',$status_dropdown,\App\Enum\Status::ACTIVE->value,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection