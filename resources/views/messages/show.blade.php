@extends('adminlte.default')

@section('title') View Message @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('messages.edit',$message->id)}}" class="btn btn-success float-right ml-1" style="margin-top:-7px;">Edit</a>
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top:-7px;"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>
                        <th rowspan="2">Message</th>
                        <td rowspan="2">{{$message->message}}</td>
                        <th>Message Date</th>
                        <td>{{$message->message_date}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$message->status?->description}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection