@extends('adminlte.default')

@section('title') View Template @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            {{--<a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>--}}
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <tbody>
                <tr>
                    <th>Name</th>
                    <td>{{$template->name}}</td>
                    <th>Template Type</th>
                    <td>{{$template->temptype?->name}}</td>
                </tr>
                <tr>
                    <td colspan="4">{!! $templateBody !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
</div>
@endsection