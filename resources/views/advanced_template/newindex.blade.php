@extends('adminlte.default')
@section('title') Templates @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('advanced_templates.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Template</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form id="filter_form" class="form-inline mt-3" action="{{route('template.index')}}" method="get">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('type',$template_type_drop_down,(isset($_GET['type']) ? $_GET['type'] : null),['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                <span>Type</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3">
                <a href="{{ route('advanced_templates.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>Name</th>
                    <th>Template Type</th>
                    <th>Added</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($templates as $template)
                    <tr>
                        <td><a href="{{route('advanced_templates.show', $template->id)}}">{{$template->name}}</a></td>
                        <td>{{$template->temptype?->name}}</td>
                        <td>{{$template->created_at->diffForHumans()}}</td>
                        <td class="last">
                            <a href="{{route('advanced_templates.edit',$template)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['advanced_templates.destroy', $template],'style'=>'display:inline']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No templates match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $templates->firstItem() }} - {{ $templates->lastItem() }} of {{ $templates->total() }}
                        </td>
                        <td>
                            {{ $templates->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('select').change(function () {
                $('#filter_form').submit();
            });
        });
    </script>
@endsection