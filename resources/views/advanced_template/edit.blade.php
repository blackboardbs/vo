@extends('adminlte.default')

@section('title') Edit Template @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            {{--<a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>--}}
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />

        <create-template :template-data="{{json_encode($template)}}" />

    {{--{{Form::open(['url' => route('advanced_templates.update',$template), 'method' => 'put','files'=>true,'id'=>'saveForm'])}}

    <div class="form-group mt-3">
        {{Form::label('name', 'Name')}}
        {{Form::text('name',$template->name,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
        @foreach($errors->get('name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>
    <div class="form-group mt-3">
        {{Form::label('template_type_id', 'Template Type')}}
        {{Form::select('template_type_id',$template_type_drop_down,$template->template_type_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('template_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Template Type'])}}
        @foreach($errors->get('template_type_id') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>
    <div class="form-group mt-3">
        <div class="msg">
            <div id="variables" name="variables"></div>
        </div>
    </div>
    <div class="form-group">
        {{Form::label('file', 'File')}}
        {{Form::file('file',['class'=>'form-control form-control-sm'. ($errors->has('file') ? ' is-invalid' : ''),'placeholder'=>'File'])}}
        @foreach($errors->get('file') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small>
            Leave empty to keep <a href="{{route('advanced_template',['q'=>$template->file])}}" target="_blank" download="{{$template->name}}.{{$template->type()}}">previous file</a>
        </small>
    </div>

    {{Form::close()}}--}}
</div>
@endsection
@section('extra-js')
<script>

    $(function () {
        var template_id = $("select[name=template_type_id]").val();
        axios.post(template_id+'/getvariables', {})
            .then(function (data) {

                $('#variables').html(data['data'].template_fields);
                $('#variables').prepend('<b>The template engine will populate relevant details with the markers below</b>');
                $(".msg").addClass('alert').addClass('alert-dark');
            })
            .catch(function () {
                console.log("An Error occurred!!!");
            });
    });

    $("select[name=template_type_id]").change(function () {

        /*$('#variables').html('');
        $(".msg").removeClass('alert').removeClass('alert-info');*/
        var template_id = $("select[name=template_type_id]").val();
        axios.post(template_id+'/getvariables', {})
            .then(function (data) {

                $('#variables').html(data['data'].template_fields);
                $('#variables').prepend('<b>The template engine will populate relevant details with the markers below</b>');
                $(".msg").addClass('alert').addClass('alert-dark');
            })
            .catch(function () {
                console.log("An Error occurred!!!");
            });
    })
</script>
@endsection()