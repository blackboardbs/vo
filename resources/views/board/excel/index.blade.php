<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($boards as $board)
        <tr>
            <td>{{$board->name}}</td>
            <td>{{$board->status?->name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>