@extends('adminlte.default')
@section('title') Consulteaze Account @endsection
@section('header')
<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <hr />
</div>
@endsection

@section('content')
<div class="container-fluid">
    <div id="loader" style="min-height:150px;width:100%;height:100%;position: relative;display:none">
        <div class="clock"></div>
    </div>
    <div id="myAcc" class="table-responsive">
        <div class="alert alert-info">
            <strong>Subscription Status:</strong>
            {{ $owner->subscription_cancelled == 1 ? 'PENDING CANCELLATION' : strtoupper($license["subscription"]["subscription_status"]) }}
            <div class="d-flex float-right mb-2" style="margin-top:-3px;">
                @php
                if($license["subscription"]["subscription_tier"] == "TRIAL" && $owner->subscription_cancelled != 1){
                echo '<a href="javascript:void(0)" onclick="updateModal(\'upgrade\',2)"
                    class="btn btn-sm btn-success toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Upgrade</span></a>';
                }
                if($license["subscription"]["subscription_tier"] == "CLASSIC" && $owner->subscription_cancelled != 1){
                echo '<a href="javascript:void(0)" onclick="updateModal(\'upgrade\',3)"
                    class="btn btn-sm btn-success toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Upgrade</span></a>';
                }
                if($license["subscription"]["subscription_tier"] == "STANDARD" && $owner->subscription_cancelled != 1){
                echo '<a href="javascript:void(0)" onclick="updateModal(\'downgrade\',2)"
                    class="btn btn-sm btn-warning ml-1 mr-1 toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Downgrade</span></a>';
                echo '<a href="javascript:void(0)" onclick="updateModal(\'upgrade\',4)"
                    class="btn btn-sm btn-warning ml-1 toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Upgrade</span></a>';
                }
                if($license["subscription"]["subscription_tier"] == "PREMIUM" && $owner->subscription_cancelled != 1){
                echo '<a href="javascript:void(0)" onclick="updateModal(\'downgrade\',3)"
                    class="btn btn-sm btn-warning ml-1 mr-1 toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Downgrade</span></a>';
                echo '<a href="javascript:void(0)" onclick="updateModal(\'upgrade\',4)"
                    class="btn btn-sm btn-warning ml-1 toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Upgrade</span></a>';
                }
                if(strtoupper($license["subscription"]["subscription_status"]) == "ACTIVE" &&
                $owner->subscription_cancelled != 1){
                echo '<a href="javascript:void(0)" onclick="cancelModal()"
                    class="btn btn-sm btn-danger ml-1 toolt text-decoration-none"
                    style="text-decoration:none !important;"><span class="tooltip-text bottom">Cancel</span></a>';
                }
                @endphp
            </div>
        </div>
        <table id="myAcc2" class="table">
            <tr class="bg-dark">
                <th colspan="6" class="pl-2">License Information</th>
            </tr>
            <tr>
                <th class="pl-2">Subscription Type</th>
                <td>{{ $license["subscription"]["subscription_tier"] }}</td>
                <th>Licensed Login Users</th>
                <td>{{ ($packages[$license["subscription"]["subscription_tier"]]["users"]) }}</td>
                <th>Storage</th>
                <td>{{ ($packages[$license["subscription"]["subscription_tier"]]["storage"]) }} GB</td>
            </tr>
            <tr>
                <th class="pl-2">Last Billing Date</th>
                <td>{{ Carbon\Carbon::parse($license["subscription"]["billed_at"])->format('Y-m-d') }}</td>
                <th>Licenses Used</th>
                <td>{{ $login_users }}</td>
                <th>Storage Used</th>
                <td>{{ $storageUsed }} %</td>
            </tr>
            <tr>
                <th class="pl-2">Expiry Date</th>
                <td>{{ Carbon\Carbon::parse($license["subscription"]["expires_at"])->format('Y-m-d') }}</td>
                <th>Licenses Available</th>
                <td>{{ ($packages[$license["subscription"]["subscription_tier"]]["users"]-$login_users) }} </td>
                <th>Storage Available</th>
                <td>{{ (100 - $storageUsed) }} %</td>
            </tr>
        </table>
        <br />

    </div>

    <button type="button" class="btn-danger btn-lg d-none" id="showLicenseDialog" data-toggle="modal"
        data-target="#license"></button>
    <div class="modal fade show" id="license" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog" style="width:550px !important;max-width:550px;">
            <div class="modal-content"><a class="d-none" data-dismiss="modal" aria-label="Close" class=""
                    id="closeCancelDialog">
                </a>
                <div class="modal-body">

                    We're sorry to see you go.<br />
                    Could you please let us know why you would like to cancel your subscription?<br />
                    <br />
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="radio" value="Missing features needed" name="reason"
                            id="flexRadioDefault1">
                        <label class="form-check-label" for="flexRadioDefault1">
                            Missing features needed
                        </label>
                    </div>
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="radio"
                            value="Not sure how to use the tools full potential" name="reason" id="flexRadioDefault2">
                        <label class="form-check-label" for="flexRadioDefault2">
                            Not sure how to use the tools full potential
                        </label>
                    </div>
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="radio" value="The product is expensive" name="reason"
                            id="flexRadioDefault3">
                        <label class="form-check-label" for="flexRadioDefault3">
                            The product is expensive
                        </label>
                    </div>
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="radio" value="Found a alternative system" name="reason"
                            id="flexRadioDefault4">
                        <label class="form-check-label" for="flexRadioDefault4">
                            Found a alternative system
                        </label>
                    </div>
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="radio" value="Other" name="reason" id="flexRadioDefault5">
                        <label class="form-check-label" for="flexRadioDefault5">
                            Other
                        </label>
                    </div>

                    <div class="mb-3 mt-3 reasond" style="display:none;">
                        <textarea name="reasonOther" class="form-control" id="exampleFormControlTextarea1" rows="3"
                            disabled="disabled"></textarea>
                    </div>
                </div>
                <div class="modal-footer text-center d-block">
                    <button type="button" class="btn btn-dark" id="closeL" onclick="cancel()">Submit</button>
                    <button type="button" class="btn btn-dark" id="closeL" onclick="closeCancelModal()">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn-danger btn-lg d-none" id="showUpdateDialog" data-toggle="modal"
        data-target="#update"></button>
    <div class="modal fade show" id="update" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog" style="width:550px !important;max-width:550px;">
            <div class="modal-content"><a class="d-none" data-dismiss="modal" aria-label="Close" class=""
                    id="closeUpdateDialog">
                </a>
                <div class="modal-body">
                    Please select the desired plan you would like to <span id="updatePackage"></span> to.<br /><br />
                    Upon success your subscription will renew on
                    {{ Carbon\Carbon::parse($owner->updated_at)->format('d')}}
                    {{ Carbon\Carbon::now()->format('M Y')}}.<br /><br />
                    <a href="https://www.consulteaze.com/comparison/" target="_blank">Click here</a> to see the plan
                    comparison.
                    <div class="ml-3 mt-3 mb-3">
                        @foreach($packages as $key => $value)
                        @if($key != $license["subscription"]["subscription_tier"] && $key != 'TRIAL' && $key != 'PREMIUM')
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="updatePackage"
                                    id="updatePackage{{$value['id']}}" value="{{$value['id']}}"
                                    onclick="premiumSelected({{$value['id']}})"
                                    @if((int)$value['id']==(int)($packages[$license["subscription"]["subscription_tier"]]['id']
                                    + 1)) checked @endif />
                                <label class="form-check-label" for="updatePackage{{$value['id']}}">
                                    {{ $key }} <span
                                        class="{{((strtolower($key) == 'classic') ? 'et_pb_pricing_table_1' : ((strtolower($key) == 'standard') ? 'et_pb_pricing_table_2' : ''))}}"><span
                                            class="et_pb_dollar_sign"></span>{{(strtolower($key) != 'premium') ? '/' : ''}}<span
                                            class="et_pb_sum"></span>
                                        {{(strtolower($key) != 'premium') ? 'per month' : ''}}</span>
                                </label>
                            </div>
                        @endif
                        @endforeach
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="updatePackage" value="4"
                                id="upgradePremium" onclick="premiumSelected('4')" />
                            <label class="form-check-label" for="upgradePremium">
                                PREMIUM
                            </label>
                        </div>
                    </div>
                    <div class="alert alert-info" id="premiumSub" style="display:none;">Upgrading to the Premium package
                        will direct you to the Contact Us page on the Consulteaze website. After completing and
                        submitting the form with a detailed message a consultant will contact you to discuss your
                        specific requirements.</div>
                </div>
                <div class="modal-footer text-center d-block">
                    <button type="button" class="btn btn-dark" id="closeL" onclick="update()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn-danger btn-lg d-none" id="showUpdatePremiumDialog" data-toggle="modal"
        data-target="#updatePremium"></button>
    <div class="modal fade show" id="updatePremium" tabindex="-1" role="dialog" aria-modal="true">
        <div class="modal-dialog" style="width:850px !important;max-width:850px;">
            <div class="modal-content"><a data-dismiss="modal" aria-label="Close" class="d-none"
                    id="closeUpdatePremiumDialog">
                </a>
                <div class="modal-body" style="height:95vh;overflow:auto;">
                    Well done on your decision to upgrade to a Consulteaze Premium plan!<br />
                    <br />
                    A representative will contact you to discuss your specific requirement and to provide you with a
                    customised quotation.<br />
                    <br />
                    Please provide us with additional information to assist you better:<br />
                    <br />
                    <form>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">User:</label>
                            <div class="col-sm-8">
                                <input type="input" id="premiumUser" class="form-control" value="{{ auth()->user()->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Phone Number:</label>
                            <div class="col-sm-8">
                                <input type="input" id="premiumPhone" class="form-control" value="{{ auth()->user()->phone }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            @php
                            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
                            $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                            $CurPageURL = $protocol . $_SERVER['HTTP_HOST'];
                            @endphp
                            <label class="col-sm-4 col-form-label">Instance / Tenant:</label>
                            <div class="col-sm-8">
                                <input type="input" id="premiumInstance" class="form-control" value="{{ $CurPageURL }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Interested Functions:</label>
                        </div>
                        <div style="margin-top:5px;" class="form-group row ml-2 ">
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Timesheets" /> Timesheets
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Projects" /> Projects
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Resource Planning" /> Resource Planning
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Invoicing" /> Invoicing
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Reporting" /> Reporting
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Recruitment" /> Recruitment
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Sales pipeline" /> Sales pipeline
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Contracts" /> Contracts
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Expenses" /> Expenses
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Human resources" /> Human resources
                            </div>
                            <div style="width:33%;float:left;">
                                <input type="checkbox" class="theClass" value="Other" /> Other
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Business Category:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="premiumCategory">
                                    <option value="IT Consulting Practice">IT Consulting Practice</option>
                                    <option value="Financial Service">Financial Service</option>
                                    <option value="Engineering">Engineering</option>
                                    <option value="Mining">Mining</option>
                                    <option value="Retail">Retail</option>
                                    <option value="Medical">Medical</option>
                                    <option value="Law">Law</option>
                                    <option value="HR">HR</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Organization Size:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="premiumSize">
                                    <option value="">Organisation Size</option>
                                    <option value="1 - 10">1 - 10</option>
                                    <option value="11 - 30">11 - 30</option>
                                    <option value="30 - 50">30 - 50</option>
                                    <option value="50 - 100">50 - 100</option>
                                    <option value="100 - 250">100 - 250</option>
                                    <option value="250 - 500">250 - 500</option>
                                    <option value="500+">500+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Additional Notes:</label>
                        </div>
                        <div class="form-group row">
                            <textarea class="col-sm-12 form-control" id="premiumNotes" rows="5"></textarea>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">What are the main reasons for using Consulteaze?</label>
                        </div>
                        <div class="form-group row">
                            <textarea class="col-sm-12 form-control" id="premiumUse" rows="5"></textarea>
                        </div>
                        <div class="form-group row text-center">
                            <button type="button" style="margin:0px auto;" class="btn btn-dark" id="closeL" onclick="premiumUpgrade()">Submit</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('extra-js')
<script>
$(function() {
    $("input[name='reason']").click(function() {
        if ($(this).val() == "Other") {
            if ($(this).prop('checked', true)) {
                $(".reasond").show();
                $("#exampleFormControlTextarea1").prop("disabled", false)
            }
        } else {
            $("#exampleFormControlTextarea1").prop("disabled", true)
            $(".reasond").hide();
        }
    });

    let country = '';
    let prod = [];
    $.get("https://ipinfo.io", function(response) {
        country = response.country
    }, "jsonp");

    $.get("https://subscription.consulteaze.com/api/prices-list", function(response) {
        if (country != '') {
            $.each(response.prices_list.data, function(index, value) {
                let tier = value.name.toLowerCase();
                prod[value.name.toLowerCase()] = [];
                prod[value.name.toLowerCase()][value.unit_price.currency_code] = value
                    .unit_price.amount;
                $.each(value.unit_price_overrides, function(index, value3) {
                    prod[tier][value3.unit_price.currency_code] = value3.unit_price
                        .amount;
                })
            });
            if (country == 'ZA') {
                $('.et_pb_pricing_table_1 .et_pb_sum').html(prod['classic']['ZAR'] / 100);
                $('.et_pb_pricing_table_1 .et_pb_dollar_sign').html('ZAR');
                $('.et_pb_pricing_table_2 .et_pb_sum').html(prod['standard']['ZAR'] / 100);
                $('.et_pb_pricing_table_2 .et_pb_dollar_sign').html('ZAR');
            } else {
                $('.et_pb_pricing_table_1 .et_pb_sum').html(prod['classic']['USD'] / 100);
                $('.et_pb_pricing_table_1 .et_pb_dollar_sign').html('USD');
                $('.et_pb_pricing_table_2 .et_pb_sum').html(prod['standard']['USD'] / 100);
                $('.et_pb_pricing_table_2 .et_pb_dollar_sign').html('USD');
            }
        } else {
            $('.et_pb_pricing_table_1 .et_pb_sum').html(19000 / 100);
            $('.et_pb_pricing_table_1 .et_pb_dollar_sign').html('ZAR');
            $('.et_pb_pricing_table_2 .et_pb_sum').html(45000 / 100);
            $('.et_pb_pricing_table_2 .et_pb_dollar_sign').html('ZAR');
        }
    }, "json");

})

function cancelModal() {
    document.getElementById('showLicenseDialog').click();
}

function closeCancelModal() {
    document.getElementById('closeCancelDialog').click();
}

function premiumSelected(pac) {
    if (pac == '4') {
        document.getElementById('premiumSub').style.display = "block"
    } else {
        document.getElementById('premiumSub').style.display = "none"
    }
}

function updateModal(val, pac) {
    if (pac == 4) {
        document.getElementById('premiumSub').style.display = "block"
    } else {
        document.getElementById('premiumSub').style.display = "none"
    }
    document.getElementById('showUpdateDialog').click();
    $('input:radio[name="updatePackage"]').each(function() {

        if ($(this).val() == pac) {
            $(this).prop('checked', true)
        }

    });
    document.getElementById('updatePackage').innerHTML = val;
}

function updatePremiumModal() {
    document.getElementById('showUpdatePremiumDialog').click();
}

function cancel() {
    document.getElementById('myAcc').style.display = "none"
    document.getElementById('myAcc2').style.display = "none"
    document.getElementById('loader').style.display = "block"
    document.getElementById('closeCancelDialog').click();
    axios.get('/api/subscription/cancel').then(resp => {

        if (resp.status == '200') {
            axios.post('/api/subscription/cancelemail', {
                'reason': $("input[name='reason']:checked").val(),
                'reasonOther': $("textarea[name='reasonOther']").val()
            }).then(resp => {
                if (resp.status == 200) {

                    toastr.success('<strong>Success!</strong> Subscription successfully updated.');

                    toastr.options.timeOut = 1000;
                } else {
                    toastr.error('<strong>Error!</strong> An error occured.');

                    toastr.options.timeOut = 1000;
                }
            })
        } else {
            toastr.error('<strong>Error!</strong> An error occured.');

            toastr.options.timeOut = 1000;
        }
        location.reload();
    });
}

function update(val) {
    let updateId = 0;

    $('input:radio[name="updatePackage"]:checked').each(function() {
        // if($(this).prop('checked', true)){
        updateId = $(this).val();
        // }
    });
    if (updateId == 4) {
        document.getElementById('closeUpdateDialog').click();
        updatePremiumModal();
    } else {
        document.getElementById('myAcc').style.display = "none"
        document.getElementById('myAcc2').style.display = "none"
        document.getElementById('loader').style.display = "block"

        document.getElementById('closeUpdateDialog').click();
        axios.post('/api/subscription/update', {
            'updateId': updateId
        }).then(resp => {
            if (resp.status == 200) {

                toastr.success('<strong>Success!</strong> Subscription successfully updated.');

                toastr.options.timeOut = 1000;
            } else {
                toastr.error('<strong>Error!</strong> An error occured.');

                toastr.options.timeOut = 1000;
            }
            location.reload();
        });

    }
}

function premiumUpgrade(val) {
    let premiumUser = $('#premiumUser').val();
    let premiumPhone = $('#premiumPhone').val();
    let premiumInstance = $('#premiumInstance').val();
    let premiumInterest = $('.theClass:checkbox:checked').map(function() {
        return this.value;
    }).get();
    let premiumCategory = $('#premiumCategory').val();
    let premiumSize = $('#premiumSize').val();
    let premiumNotes = $('#premiumNotes').val();
    let premiumUse = $('#premiumUse').val();

    axios.post('/myaccount/upgrade-to-premium', { user: premiumUser, phone: premiumPhone, instance: premiumInstance, interest: premiumInterest, category: premiumCategory, size: premiumSize, notes: premiumNotes, use: premiumUse })
    .then(function (data) {
        document.getElementById('closeUpdatePremiumDialog').click();
        if(data.data.success == 'success'){
            toastr.success('<strong>Success!</strong> Message successfully sent.');

            toastr.options.timeOut = 1000;
        }
                })
                .catch(function () {
                    toastr.error('<strong>Error!</strong> An error occured while trying to send the message.');

                        toastr.options.timeOut = 1000;
                    console.log("An Error occured!!!");
                });;
}

function upgrade() {
    $('#upgrade').submit();
}
</script>
@endsection