@extends('adminlte.default')

@section('title') Consulting per month report (R21) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.planning'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::select('dates', $dates_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Date from - Date to']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <span class="btn btn-success">Billable</span> <span class="btn btn-warning mr-1">Non-Billable</span><span class="btn btn-default">Leave</span>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th colspan="2">Start: {{$tdayss}}</th>
                    @forelse($timesheet_days as $timesheet_day)
                        <th class="text-center {{($timesheet_day->weekend == 'Y') ? 'bg-gray-light' : ''}}">{{ $timesheet_day->day }}</th>
                        @empty 
                    @endforelse
                </tr>
                </thead>
                <tbody>
                    @forelse($timesheet_31 as $key => $plan)
                        <tr>
                            <td>{{ substr($plan->first_name,0,1).substr($plan->last_name,0,1) }}</td>
                            <td><a href="{{ route('project.show', $plan->project_id) }}">{{ $plan->project_id.' - '.$projects[$key] }}</a></td>
                            @forelse($timesheet_days as $key => $timesheet_day)
                                <td class=" {{ (($timesheet_day->date <= $plan->end_date) ? ($timesheet_day->weekend == 'Y') ? 'bg-gray-light' : ((($leave_days[$key] != null) && ($plan->employee_id == $leave_days[$key]->emp_id)) ? '' : ( ($plan->billable != 1) ? 'alert alert-warning' : 'alert alert-success')) : '') }}"> &nbsp;</td>
                                @empty
                            @endforelse
                        </tr>
                        @empty
                        <tr>
                            <td colspan="33" class="text-center">Nothing to show here</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="33">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Leave</th>
                        <td  colspan="2" class="bg-info text-center">Annual Leave</td>
                        <td colspan="6" class="bg-primary text-center">Maternity Leave</td>
                        <td colspan="6" class="bg-purple text-center">Study Leave</td>
                        <td colspan="6" class="bg-danger text-center">Sick Leave</td>
                        <td colspan="6" class="bg-unpaid text-center">Unpaid Leave</td>
                        <td colspan="6" class="bg-warning text-center">Compasionate</td>

                    </tr>
                    <tr>
                        <td colspan="33">&nbsp;</td>
                    </tr>
                    @forelse($timesheet_31 as $index => $plan)
                        <tr>
                            <td><a href="{{route('resource.show', $plan->employee_id)}}">{{ substr($plan->first_name,0,1).substr($plan->last_name,0,1) }}</a></td>
                            <td><a href="{{route('leave.index')}}">Leave</a></td>
                            @forelse($timesheet_days as $key => $timesheet_day)
                                <td class="{{($timesheet_day->weekend == 'Y') ? 'bg-gray-light' : ((($leave_days[$key] != null) && ($plan->employee_id == $leave_days[$key]->emp_id)) ? $leave_type[$plan->employee_id][$key] : '')}} ">&nbsp;</td>
                            @empty
                            @endforelse
                        </tr>
                    @empty
                        <tr>
                            <td colspan="33" class="text-center">Nothing to show here</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {{--{{ $timesheet->links() }}--}}
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .bg-purple{
            background-color: #6f42c1;
            color: #FFFFFF;
        }
        .bg-unpaid{
            background-color: #fd7e14;
            color: #FFFFFF;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
