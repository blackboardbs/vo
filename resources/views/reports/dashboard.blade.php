@extends('adminlte.default')

@section('title')  Operational Dashboard for Blackboard Group @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Open Assignments</th>
                    <th>Consultant</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Hours</th>
                    <th>Bill</th>
                    <th>N-Bill</th>
                    <th>Hrs Out</th>
                    <th>Comp%</th>
                    <th>R Out</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($assignments as $key => $assignment)
                        @php
                            $out = $assignment->ahours - $assignment->bhours - $assignment->nbhours;
                            $value_out = ($assignment->ahours - $assignment->bhours) * $assignment->rate;
                            $total_value_out += $value_out;
                            $currency = $assignment->currency;
                            $comp = ($assignment->project->project_type_id  == 1) ?  (($assignment->bhours != 0) ? number_format((($assignment->bhours /$assignment->ahours) * 100),0,',',',') : 0) : (($assignment->nbhours != 0) ? number_format((($assignment->nbhours /$assignment->ahours) * 100),0,',',',') : 0);
                        @endphp
                        <tr>
                            <td>{{ isset($assignment->project) ? substr($assignment->project->name,0,30) : '' }}</td>
                            <td>{{ isset($assignment->consultant) ? $assignment->consultant->first_name.' '.$assignment->consultant->last_name : '' }}</td>
                            <td>{{ $assignment->start_date }}</td>
                            <td>{{ $assignment->end_date }}</td>
                            <td>{{ number_format($assignment->ahours,0,'.',',') }}</td>
                            <td>{{ number_format($assignment->bhours,0,'.',',') }}</td>
                            <td>{{ number_format($assignment->nbhours,0,'.',',') }}</td>
                            <td>{{ number_format($out,0,'.',',') }}</td>
                            <td class="{{ ($comp < 30) ? 'bg-success' : (($comp >= 30 && $comp < 50) ? 'bg-warning' : 'bg-danger') }}">{{ $comp.'%'  }}</td>
                            {{--<td>@if($assignment->bhours == 0 || $assignment->ahours == 0) 0% @else {{ number_format(((($assignment->bhours + $assignment->nbhours)/$assignment->ahours) * 100),0,',',',').'%' }} @endif</td>--}}
                            <td>@if($value_out < 0) 0 @else {{ number_format($value_out,0,',',',') }} @endif</td>
                        </tr>
                        @empty 
                    @endforelse
                    @if(count($assignments) > 0)
                        <tr>
                            <th class="text-center bg-success">< 30%</th>
                            <th class="text-center bg-warning">> 30% 50% <</th>
                            <th class="text-center bg-danger">> 50%</th>
                            <th colspan="6">Total Assignment Value Outstanding: </th>
                            <th>{{ $currency.' '.number_format($total_value_out,0,',',',') }}</th>
                        </tr>
                    @endif
                </tbody>
            </table>

            {{$assignments->links()}}

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Utilization</th>
                    <th class="text-center">Week-3</th>
                    <th class="text-center">Week-2</th>
                    <th class="text-center">Week-1</th>
                    <th class="text-center">Ave 3wk Util %</th>
                    <th class="text-center">This Week</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td class="text-center">{{ number_format($yrwkp3,0,'.',',') }}</td>
                        <td class="text-center">{{ number_format($yrwkp2,0,'.',',') }}</td>
                        <td class="text-center">{{ number_format($yrwkp1,0,'.',',') }}</td>
                        <td class="text-center"></td>
                        <td class="text-center">{{ number_format($yearwk,0,'.',',') }}</td>
                    </tr>
                    @forelse($emp_util as $key => $emp)
                        @php
                            $total_yrwkp1 += isset($yrwkp1_hours[$key]->hours) ? $yrwkp1_hours[$key]->hours : 0;
                            $total_yrwkp2 += isset($yrwkp2_hours[$key]->hours) ? $yrwkp2_hours[$key]->hours : 0;
                            $total_yrwkp3 += isset($yrwkp3_hours[$key]->hours) ? $yrwkp3_hours[$key]->hours : 0;
                            $total_yearwk_hour += isset($yearwk_hours[$key]->hours) ? $yearwk_hours[$key]->hours : 0;
                        @endphp
                        <tr>
                            <td>{{ isset($emp->resource)? $emp->resource->last_name.', '.$emp->resource->first_name : '' }}</td>
                            <td class="text-center">{{ number_format((isset($yrwkp3_hours[$key]) ? $yrwkp3_hours[$key]->hours : 0),0,'.',',') }}</td>
                            <td class="text-center">{{ number_format((isset($yrwkp2_hours[$key]) ? $yrwkp2_hours[$key]->hours : 0),0,'.',',') }}</td>
                            <td class="text-center">{{ number_format((isset($yrwkp1_hours[$key]) ? $yrwkp1_hours[$key]->hours : 0),0,'.',',') }}</td>
                            <td class="text-center">@if($plan3 == 0)
                                    {{ number_format(0,0,',',',').'%' }}
                                @else
                                    {{ number_format(((((isset($yrwkp1_hours[$key]) ? $yrwkp1_hours[$key]->hours : 0) + (isset($yrwkp2_hours[$key]) ? $yrwkp2_hours[$key]->hours : 0) + (isset($yrwkp3_hours[$key]) ? $yrwkp3_hours[$key]->hours : 0))/$plan3)*100),0,',',',').'%' }}
                            @endif</td>
                            <td>{{ number_format((isset($yearwk_hours[$key]) ? $yearwk_hours[$key]->hours : 0),0,'.', ',') }}</td>
                        </tr>
                        @empty
                    @endforelse
                    @if(count($emp_util) > 0)
                        <tr>
                            <th>Total Hours</th>
                            <th class="text-center">{{ number_format($total_yrwkp3,0,'.',',') }}</th>
                            <th class="text-center">{{ number_format($total_yrwkp2,0,'.',',') }}</th>
                            <th class="text-center">{{ number_format($total_yrwkp1,0,'.',',') }}</th>
                            <th class="text-center">{{ number_format($g_total_util,0,'.',',') }}</th>
                            <th class="text-center">{{ number_format($total_yearwk_hour,0,'.',',') }}</th>
                        </tr>
                        <tr>
                            <th class="text-center"><50%</th>
                            <th class="text-center"> >50% 65% </th>
                            <th class="text-center"> >65% 80%</th>
                            <th class="text-center"><80%</th>
                            <th class="text-right" colspan="2">Utilization is calculated at 40 hours per resource per week.</th>
                        </tr>
                    @endif
                </tbody>
            </table>

            {{ $emp_util->links() }}
            @php
                $mtot = $total_value - $exp_total;
                $mtot05 = $total_value05 - $exp05;
                $mtot10 = $total_value10 - $exp10;
                $mtot20 = $total_value20 - $exp20;
                $mtot30 = $total_value30 - $exp30;
                $mtot30p = $total_value30p - $exp30p;
                $cct = ($mtot < 0) ? "r" : "g";
                $cct05 = ($mtot05 < 0) ? "r" : "g";
                $cct10 = ($mtot10 < 0) ? "r" : "g";
                $cct20 = ($mtot20 < 0) ? "r" : "g";
                $cct30 = ($mtot30 < 0) ? "r" : "g";
                $cct30p = ($mtot30p < 0) ? "r" : "g";
            @endphp
            <div class="col-12 mt-5 bg-dark py-1"><strong>Weekly Utilisation</strong></div>
            <canvas id="weekly-utilization" width="780" height="300"></canvas>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Cashflow</th>
                    <th>Total</th>
                    <th>5</th>
                    <th>10</th>
                    <th>20</th>
                    <th>30</th>
                    <th>30+</th>
                </tr>
                </thead>
                <tbody>

                        <tr>
                            <td>Invoiced</td>
                            <td>{{ number_format($total_value,2,'.',',') }}</td>
                            <td>{{ number_format($total_value05,2,'.',',') }}</td>
                            <td>{{ number_format($total_value10,2,'.',',') }}</td>
                            <td>{{ number_format($total_value20,2,'.',',') }}</td>
                            <td>{{ number_format($total_value30,2,'.',',') }}</td>
                            <td>{{ number_format($total_value30p,2,'.',',') }}</td>
                        </tr>

                        <tr>
                            <td>Not Invoiced (incl. Exp)</td>
                            <td>{{ number_format($non_total_value,2,'.',',') }}</td>
                            <td>{{ number_format($non_total_value05,2,'.',',') }}</td>
                            <td>{{ number_format($non_total_value10,2,'.',',') }}</td>
                            <td>{{ number_format($non_total_value20,2,'.',',') }}</td>
                            <td>{{number_format($non_total_value30,2,'.',',')}}</td>
                            <td>{{ number_format($non_total_value30p,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td>Planned Expenses (30 days)</td>
                            <td>{{ number_format($exp_total,2,'.',',') }}</td>
                            <td>{{ number_format($exp05,2,'.',',') }}</td>
                            <td>{{ number_format($exp10,2,'.',',') }}</td>
                            <td>{{ number_format($exp20,2,'.',',') }}</td>
                            <td>{{ number_format($exp30,2,'.',',') }}</td>
                            <td>{{ number_format($exp30p,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td>Nett Cashflow</td>
                            <td class="{{($mtot < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot,2,'.',',') }}</td>
                            <td class="{{($mtot05 < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot05 ,2,'.',',') }}</td>
                            <td class="{{($mtot10 < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot10,2,'.',',') }}</td>
                            <td class="{{($mtot20 < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot20,2,'.',',') }}</td>
                            <td class="{{($mtot30 < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot30 ,2,'.',',') }}</td>
                            <td class="{{($mtot30p < 0) ? 'bg-danger' : 'bg-success'}}">{{ number_format($mtot30p,2,'.',',') }}</td>
                        </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Debtor Ageing</th>
                    <th>Total</th>
                    <th>Current</th>
                    <th>30</th>
                    <th>60</th>
                    <th>90</th>
                    <th>90+</th>
                </tr>
                </thead>
                <tbody>

                @forelse($debtor_customer as $customer)
                    @php

                    @endphp
                    <tr>
                        <td>{{ $customer->customer->customer_name }}</td>
                        <td>{{ number_format($dt,2,'.',',') }}</td>
                        <td>{{ number_format($dc,2,'.',',') }}</td>
                        <td>{{ number_format($d30,2,'.',',') }}</td>
                        <td>{{ number_format($d60,2,'.',',') }}</td>
                        <td>{{ number_format($d90,2,'.',',') }}</td>
                        <td>{{ number_format($d90p,2,'.',',') }}</td>
                    </tr>
                    @empty
                @endforelse
                <tr class="bg-gray-light">
                    <td>Total</td>
                    <td>{{ number_format($dg,2,'.',',') }}</td>
                    <td>{{ number_format($dgc,2,'.',',') }}</td>
                    <td>{{ number_format($dg30,2,'.',',') }}</td>
                    <td>{{ number_format($dg60,2,'.',',') }}</td>
                    <td>{{ number_format($dg90,2,'.',',') }}</td>
                    <td>{{ number_format($dg90p,2,'.',',') }}</td>
                </tr>

                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Planning & PipelineDebtor Ageing</th>
                    <th>Solution</th>
                    <th>Hours</th>
                    <th>Value</th>
                    <th>Resources</th>
                    <th>Start</th>
                    <th>% Chance</th>
                </tr>
                </thead>
                <tbody>

                @forelse($prospects as $prospect)
                    <tr>
                        <td>{{ $prospect->prospect_name }}</td>
                        <td>{{ $prospect->solution }}</td>
                        <td>{{ $prospect->hours }}</td>
                        <td>{{ number_format($prospect->value,2,'.',',') }}</td>
                        <td>{{ $prospect->resources }}</td>
                        <td>{{ $prospect->est_start_date }}</td>
                        <td>{{ $prospect->chance.'%' }}</td>
                    </tr>
                @empty
                    <tr><td colspan="7" class="text-center">Nothing to show here</td></tr>
                @endforelse

                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                    <tr class="bg-dark">
                        <th>Planned Income and Expenses</th>
                        <th>This Month</th>
                        <th>Month +1</th>
                        <th>Month +2</th>
                        <th>Month +3</th>
                    </tr>
                    <tr>
                        <th colspan="5">Income</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="pl-5">Consulting Income</td>
                        <td>{{ number_format($total_value,2,'.',',') }}</td>
                        <td>{{ number_format($total_value,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <td class="pl-5">Refundable Expenses</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <td class="pl-5"></td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                        <td>{{ number_format(0,2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <th colspan="5">Expenses</th>
                    </tr>
                    @forelse($exp_months as $exp_month)
                        <tr>
                           <td class="pl-5">{{ $exp_month->account->description }}</td>
                            <td>{{ number_format($exp_month->exp_this_month,2,'.',',') }}</td>
                            <td>{{ number_format($exp_month->exp_month1,2,'.',',') }}</td>
                            <td>{{ number_format($exp_month->exp_month2,2,'.',',') }}</td>
                            <td>{{ number_format($exp_month->exp_month3,2,'.',',') }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" class="text-center">Nothing to show here</td>
                        </tr>
                    @endforelse
                    @if(count($exp_months) > 0)
                        <tr>
                            <th class="pl-5"></th>
                            <th>{{ number_format($et,2,'.',',') }}</th>
                            <th>{{ number_format($e1,2,'.',',') }}</th>
                            <th>{{ number_format($e2,2,'.',',') }}</th>
                            <th>{{ number_format($e3,2,'.',',') }}</th>
                        </tr>
                    @endif
                    <tr>
                        <th>Profit / (Loss)</th>
                        <th>{{ number_format(($total_value - $et),2,'.',',') }}</th>
                        <th>{{ number_format(($total_value - $e1),2,'.',',') }}</th>
                        <th>{{ number_format((0 - $e2),2,'.',',') }}</th>
                        <th>{{ number_format((0 - $e3),2,'.',',') }}</th>
                    </tr>

                </tbody>
            </table>

            <canvas id="top-10" width="780" height="250"></canvas>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Top 10 Customers (12 months)</th>
                    <th>Hours</th>
                    <th>Value</th>
                    <th>Ave Rate</th>
                </tr>
                </thead>
                <tbody>

                @forelse($top_10 as $result)
                    <tr>
                        <td>{{ $result->customer->customer_name }}</td>
                        <td>{{ number_format($result->hours,0,'.',',') }}</td>
                        <td>R {{ number_format($result->amount,2,'.',',') }}</td>
                        <td>@if($result->hours == 0) 0 @else {{ number_format(($result->amount / $result->hours),0,'.',',') }} @endif</td>
                    </tr>
                @empty
                @endforelse

                </tbody>
            </table>

            <canvas id="system" width="780" height="150"></canvas>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Top 10 Solutions (12 months)</th>
                    <th>Hours</th>
                    <th>Value</th>
                    <th>Ave Rate</th>
                </tr>
                </thead>
                <tbody>

                @forelse($system as $result)
                    <tr>
                        <td>{{ $result->system_desc->description }}</td>
                        <td>{{ number_format($result->hours,0,'.',',') }}</td>
                        <td>R {{ number_format($result->amount,2,'.',',') }}</td>
                        <td>@if($result->hours == 0) 0 @else {{ number_format(($result->amount / $result->hours),0,'.',',') }} @endif</td>
                    </tr>
                @empty
                @endforelse

                </tbody>
            </table>

            <canvas id="location" width="780" height="150"></canvas>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th>Location (12 months)</th>
                    <th>Hours</th>
                    <th>Value</th>
                    <th>Ave Rate</th>
                </tr>
                </thead>
                <tbody>

                @forelse($location as $result)
                    <tr>
                        <td>{{ substr($result->location->name,0,30) }}</td>
                        <td>{{ number_format($result->hours,0,'.',',') }}</td>
                        <td>R {{ number_format($result->amount,2,'.',',') }}</td>
                        <td>@if($result->hours == 0) 0 @else {{ number_format(($result->amount / $result->hours),0,'.',',') }} @endif</td>
                    </tr>
                @empty
                @endforelse

                </tbody>
            </table>

            <div class="row mt-5 no-gutters">
                <div class="col-md-5 col-sm-5">
                    <canvas id="monthly_consulting" width="780" height="300"></canvas>
                </div>
                <div class="col-md-5 col-sm-5">
                    <canvas id="year_to_date" width="780" height="300"></canvas>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .dots_table td p.n{
            margin: 0;
            padding: 0em;
            size: auto;
            line-height: 0em;
            font-size: 0.6em;
        }
    </style>
@endsection
@section('extra-js')

    <script src="{{asset('adminlte/plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        let weeklyUtilizationCanvas = document.getElementById("weekly-utilization");
        Chart.defaults.scale.gridLines.display = false;
        let weeklyData = {
            label: "Hours",
            data: [@forelse($get_yearwk_hours as $yearwk_hours) {{ floor($yearwk_hours->hours) }}, @empty  @endforelse],
            backgroundColor: '#343a40'
        };
        let weeklyUtilizationBar = new Chart(weeklyUtilizationCanvas, {
            type: 'bar',
            data: {
                labels: [@forelse($get_yearwk_hours as $yearwk_hours) '{{ substr($yearwk_hours->year_week,4,2) }}', @empty @endforelse],
                datasets: [weeklyData]
            }
        })
    </script>
    <script>
        let yearToDateCanvas = document.getElementById("year_to_date");
        Chart.defaults.scale.gridLines.display = false;
        let yearToDateHoursData = {
            label: "Hours",
            data: [{{ $this_year_week->hours }}, {{ $last_year_week->hours }}, {{ $last_of_last_year_week->hours }}],
            backgroundColor: 'rgba(0, 123, 255, 1)'
        };
        let yearToDateValueData = {
            label: "Value",
            data: [{{ $this_year_week->amount }}, {{ $last_year_week->amount }}, {{ $last_of_last_year_week->amount }}],
            backgroundColor: 'rgba(23, 162, 184, 1)'
        };
        let yearToDateAveData = {
            label:"Average Rate",
            data: [{{ $this_year_week_averager }}, {{ $last_year_week_averager }}, {{ $last_of_last_year_week_averager }},],
            backgroundColor: 'rgba(32, 201, 151, 1)'
        };
        let yearToDateLine = new Chart(yearToDateCanvas, {
            type: 'bar',
            data: {
                labels: [new Date().getFullYear(), new Date().getFullYear() - 1, new Date().getFullYear() - 2],
                datasets: [yearToDateHoursData, yearToDateValueData, yearToDateAveData]
            }
        })
    </script>

    <script>
        let locationCanvas = document.getElementById("location");
        Chart.defaults.scale.gridLines.display = false;
        let locationData = {
            label: "Amount brought in by top 10 locations (12 months)",
            data: [@forelse($location as $amount) {{ floor($amount->amount) }}, @empty  @endforelse],
            backgroundColor: 'rgba(51, 53, 56, 1)'
        };
        let locationBar = new Chart(locationCanvas, {
            type: 'horizontalBar',
            data: {
                labels: [@forelse($location as $country) '{{ $country->location->name }}', @empty @endforelse],
                datasets: [locationData]
            }
        })
    </script>

    <script>
        let topTenCanvas = document.getElementById("top-10");
        Chart.defaults.scale.gridLines.display = false;
        let topTenData = {
            label: "Amount brought in by top ten Customers (12 months)",
            data: [@forelse($top_10 as $hours) {{ floor($hours->amount) }}, @empty  @endforelse],
            backgroundColor: 'rgba(91,192,222, 1)'
        };
        let barChart = new Chart(topTenCanvas, {
            type: 'horizontalBar',
            data: {
                labels: [@forelse($top_10 as $customer) '{{ $customer->customer->customer_name }}', @empty @endforelse],
                datasets: [topTenData]
            }
        })
    </script>
    <script>
        let systemCanvas = document.getElementById("system");
        Chart.defaults.scale.gridLines.display = false;
        let systemData = {
            label: "Amount brought in by top 10 Solutions (12 months)",
            data: [@forelse($system as $amount) {{ floor($amount->amount) }}, @empty  @endforelse],
            backgroundColor: 'rgba(255, 99, 132, 1)'
        };
        let systemBar = new Chart(systemCanvas, {
            type: 'horizontalBar',
            data: {
                labels: [@forelse($system as $system) '{{ $system->system_desc->description }}', @empty @endforelse],
                datasets: [systemData]
            }
        })
    </script>
    <script>
        let monthlyCanvas = document.getElementById("monthly_consulting");
        Chart.defaults.scale.gridLines.display = false;
        let lastYearData = {
            label: new Date().getFullYear() - 2,
            data: [@forelse($two_years_ago as $amount)
                    @if($amount != null)
                        @if($amount->amount == null)
                            {{ 0 }},
                        @else
                            {{ floor($amount->amount) }},
                        @endif
                    @else
                        {{ 0 }},
                    @endif
                @empty
                @endforelse],
            backgroundColor: 'rgba(0, 123, 255, 1)'
        };
        let yearAgoData = {
            label: new Date().getFullYear() - 1,
            data: [@forelse($one_year_ago as $amount)
                @if($amount != null)
                @if($amount->amount == null)
                {{ 0 }},
                @else
                {{ floor($amount->amount) }},
                @endif
                @else
                {{ 0 }},
                @endif
                @empty
                @endforelse],
            backgroundColor: 'rgba(23, 162, 184, 1)'
        };
        let thisYearData = {
            label:new Date().getFullYear(),
            data: [@forelse($this_year as $amount)
                @if($amount != null)
                @if($amount->amount == null)
                {{ 0 }},
                @else
                {{ floor($amount->amount) }},
                @endif
                @else
                {{ 0 }},
                @endif
                @empty
                @endforelse],
            backgroundColor: 'rgba(32, 201, 151, 1)'
        };
        let monthlyBar = new Chart(monthlyCanvas, {
            type: 'bar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                datasets: [thisYearData, yearAgoData, lastYearData]
            }
        })
    </script>

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
