@extends('adminlte.default')
@section('title') Reports @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Report Code</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><a href="{{route('reports.consultant_assignment')}}">R30_32</a></td>
                    <td><a href="{{route('reports.consultant_assignment')}}">Assignment Report</a></td>
                    <td><a href="{{route('reports.consultant_assignment')}}">List consultant assignments</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('payroll.summary')}}">R361</a></td>
                    <td><a href="{{route('payroll.summary')}}">Payroll Summary</a></td>
                    <td><a href="{{route('payroll.summary')}}">Payroll Summary</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.summary_report')}}">R40</a></td>
                    <td><a href="{{route('reports.summary_report')}}">Summary report</a></td>
                    <td><a href="{{route('reports.summary_report')}}">List summary report</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.dashboard')}}">Dashboard</a></td>
                    <td><a href="{{route('reports.dashboard')}}">Dashboard</a></td>
                    <td><a href="{{route('reports.dashboard')}}">Dashboard</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.assignmentplanning')}}">R61</a></td>
                    <td><a href="{{route('reports.assignmentplanning')}}">Assignment Planning</a></td>
                    <td><a href="{{route('reports.assignmentplanning')}}">List Assignment Planning Reports</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('projecthourssammary')}}">R50</a></td>
                     <td><a href="{{route('projecthourssammary')}}">Project Hours Summary</a></td>
                    <td><a href="{{route('projecthourssammary')}}">List Project Hours Summary</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('projectvaluesammary')}}">R51</a></td>
                    <td><a href="{{route('projectvaluesammary')}}">Project Value Summary</a></td>
                    <td><a href="{{route('projectvaluesammary')}}">List Project Value Summary</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('resourcedetails')}}">R52</a></td>
                    <td><a href="{{route('resourcedetails')}}">Resource Detail</a></td>
                    <td><a href="{{route('resourcedetails')}}">List Resource Detail</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.resource')}}">R55</a></td>
                    <td><a href="{{route('reports.resource')}}">Resource Analysis</a></td>
                    <td><a href="{{route('reports.resource')}}">List Resource Analysis Reports</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('consultant360')}}">R360</a></td>
                    <td><a href="{{route('consultant360')}}">Consultant 360 Degree</a></td>
                    <td><a href="{{route('consultant360')}}">Show Every Detail About a Consultant</a></td>
                </tr>
                <tr>
                      <td><a href="{{route('reports.forecastanalysis')}}">R54</a></td>
                    <td><a href="{{route('reports.forecastanalysis')}}">Forecast Analysis</a></td>
                    <td><a href="{{route('reports.forecastanalysis')}}">List Forecast Analysis</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.assignmentstatus')}}">R60</a></td>
                    <td><a href="{{route('reports.assignmentstatus')}}">Assignment Status</a></td>
                    <td><a href="{{route('reports.assignmentstatus')}}">List Assignment Status</a></td>
                </tr>
                <tr>
                    <td><a href="{{route('reports.projectbilling')}}">PM07</a></td>
                    <td><a href="{{route('reports.projectbilling')}}">Project Billing</a></td>
                    <td><a href="{{route('reports.projectbilling')}}">Analysis of hours available per project</a></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection