@extends('adminlte.default')

@section('title') Time Summary Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('report.timesummary.export', request()->all())}}" class="btn btn-info ml-1"><i class="fas fa-file-excel"></i>c Export In Excel</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('report.timesummary'),'method' => 'get', 'class' => 'mb-0 mt-3', 'id' => 'filters']) !!}
        <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="form-row">
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Company</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search ', 'placeholder' => 'All']) !!}
                            <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Project</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('resource', $resource_drop_down, request()->resource , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('billable', [0=>"No", 1=>"Yes"], request()->billable , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Billable</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('epic', $epics_dropdown, request()->epic , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Epic</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('feature', $features_dropdown, request()->feature , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Feature</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('user_story', $user_stories_dropdown, request()->user_story , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>User Story</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('task',$task_drop_down, request()->task , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Task</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('invoice_status',$invoice_statuses_dropdown, request()->invoice_status , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Invoice Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('from_week', $weeks_dropdown, request()->from_week , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Week From</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('to_week', $weeks_dropdown, request()->to_week , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Week To</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('from_month', $months_dropdown, request()->from_month , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Month From</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('to_month',$months_dropdown, request()->to_month , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Month To</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('cost_center', $cost_center_dropdown, request()->cost_center , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                            <span>Cost Center</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('billing_cycle_id', $billing_cycle_dropdown, request()->billing_cycle_id, ['class' => 'form-control search', 'id' => 'billing_cycle_id', 'placeholder' => 'All']) !!}
                            <span>Billing Cycle</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('billing_period_id', [], request()->billing_period_id, ['class' => 'form-control search', 'id' => 'billing_period_id', 'placeholder' => 'All']) !!}
                            <span>Billing Period</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2 mb-1">
                    <a href="{{route('report.timesummary')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
            </div>
        {!! Form::close() !!}
        <hr class="mt-0" />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead class="bg-dark">
                    <tr>
                        <th class="text-capitalize text-nowrap">Resource</th>
                        <th class="text-capitalize text-nowrap">Customer</th>
                        <th class="text-capitalize text-nowrap">Project</th>
                        @foreach($weeks as $week)
                            <th class="text-right text-nowrap">{{$week}}</th>
                        @endforeach
                        <th class="text-right text-nowrap">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($reports as $report)
                        <tr>
                            <td>{{$report["employee"]}}</td>
                            <td>{{$report["customer"]}}</td>
                            <td>{{$report["project"]}}</td>
                            @foreach($weeks as $week)
                                <td class="text-right text-bold">
                                    {{(isset($report["weeks"][$week]) && $report["weeks"][$week] > 0) ?number_format(($report["weeks"][$week]/60),2):'0.00'}}
                                </td>
                            @endforeach
                            <td class="text-right text-bold">
                                {{$report["total"] > 0 ? number_format(($report["total"]/60),2):'0.00'}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="13">No time summary available</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="3" class="text-right text-bold">Total</td>
                        @foreach($weeks as $week)
                            <td class="text-right text-bold">
                                {{number_format($grand_total[$week], 2)}}
                            </td>
                        @endforeach
                        <td class="text-right text-bold">{{number_format(array_sum(array_values($grand_total)), 2)}}</td>
                    </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $reports->firstItem() }} - {{ $reports->lastItem() }} of {{ $reports->total() }}
                        </td>
                        <td>
                            {{ $reports->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        if (!getUrlParameters("billing_cycle_id")){
            $("#billing_period_id_chosen").hide();
        }else {
            getBillingPeriods();
        }

        $("body").on('change', '#billing_cycle_id', () => {
            getBillingPeriods();
            $("#billing_period_id_chosen").show();
        });

        function getBillingPeriods() {
            axios.get('/billing/'+$("#billing_cycle_id").val())
                .then(response => {
                    let option = '<option value="0">Select Billing Period</option>';
                    let selected = '{{request()->billing_period_id}}';
                    if (response.data.billing_cycle.billing_periods.length){
                        response.data.billing_cycle.billing_periods.forEach((v, i) => {
                            option += '<option value="'+v.id+'" '+(v.id == selected ? "selected" : "")+'>'+v.period_name+'</option>';
                        })
                        $("#billing_period_id").html(option).trigger("chosen:updated")
                    }
                }).catch(err => console.log(err.response));
        }

        function getUrlParameters(sParam) {
            let sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        }
    </script>
@endsection
