@extends('adminlte.default')

@section('title') Consulting per month report (R21) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.consultant_monthly_report'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->teamteamteamteamteamteamteamteam , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td class="row">
                   {!! Form::text('date_from', request()->date_from , ['class' => 'form-control form-control-sm col-md-6 datepicker', 'placeholder' => 'Date from']) !!}
                   {!! Form::text('date_to', request()->date_to , ['class' => 'form-control form-control-sm col-md-6 datepicker', 'placeholder' => 'Date to']) !!}
                </td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::submit('Submit', ['class' => 'btn btn-sm btn-dark']) !!} <a href="{{route('reports.consultant_monthly_report')}}" class="btn btn-sm btn-secondary">Clear Filters</a></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Period</th>
                    <th>Consultant</th>
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Hours</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($timesheet as $consulting)
                        <tr>
                            <td>{{ ($consulting->month < 10) ? $consulting->year.'/0'.$consulting->month : $consulting->year.'/'.$consulting->month }}</td>
                            <td>{{ $consulting->user->last_name.' '.$consulting->user->first_name }}</td>
                            <td>{{ $consulting->customer->customer_name }}</td>
                            <td>{{ $consulting->project->name }}</td>
                            <td>{{ number_format($consulting->hours,0,'.',',') }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" class="text-center">Nothing to show</td>
                        </tr>
                    @endforelse
                    @if(count($timesheet) > 0)
                        <tr>
                            <th class="text-right" colspan="4">TOTAL</th>
                            <th>{{ number_format($total_hours,0,'.',',') }}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            {{ $timesheet->appends(request()->except('page'))->links() }}
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
