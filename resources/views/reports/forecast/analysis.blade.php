@extends('adminlte.default')
@section('title') Forecast Analysis @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                    {{Form::select('project',$projects,request()->project,['class'=>' form-control search', 'style'=>'width: 100%;','placeholder'=>'All'])}}
                    <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                    {{Form::select('resource',$resource,request()->resource,['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                    {{Form::select('year',[2024 => 2024,2023 => 2023,2022 => 2022,2021 => 2021,2020 => 2020,2019 => 2019, 2018 => 2018, 2017 => 2017], old('year'),['class'=>' form-control search', 'id' => 'year', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    <span>Year</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                <a href="{{route('reports.forecastanalysis')}}" class="btn w-100 btn-info" type="submit">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Hours
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <div class="row">
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($actual_hours_total, 2, '.', ',')}}<p> Actual</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($forecast_hours_total, 2, '.', ',')}}<p> Forecast</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($po_hours_total, 2, '.', ',')}}<p> Budget</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($planned_hours_total, 2, '.', ',')}}<p> Planned Hours</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($variance_hours_total, 2, '.', ',')}}<p> Variance</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Budget
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <div class="row">
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($actual_spend_total, 2, '.', ',')}}<p> Actual</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($forecast_spend_total, 2, '.', ',')}}<p> Forecast</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($budget_spend_total, 2, '.', ',')}}<p> Budget</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($planned_spend_total, 2, '.', ',')}}<p> Planned</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{number_format($variance_spend_total, 2, '.', ',')}}<p> Variance</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Project Figures
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <div class="row">
                                    <div class="col-md-6 text-center"><p class="figures">{{$project_counter}}<p> Projects</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{$resource_counter}}</p> Resources</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{$vendor_counter}}</p> Vendors</div>
                                    <div class="col-md-6 text-center"><p class="figures">{{$customer_counter}}</p> Customers</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Hours by Month
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="actual_hour_chart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Spend by Month
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="spend_hour_chart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .figures{
            font-size: 30px;
            margin-bottom: 0px;
            font-weight: bold;
        }
    </style>
@endsection
@section('extra-js')
    <script src="{!! asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>

    <script>
        $(function () {

        });

        $('.start_date').change(function (){
            $(this).closest('form').submit();
        });

        $('.end_date').change(function (){
            $(this).closest('form').submit();
        });


        var vendor_chart = document.getElementById("actual_hour_chart");
        if (vendor_chart) {
            var loginsSuccChart = new Chart(vendor_chart, {
                type: 'bar',
                data: {
                    labels: [@foreach($total_actual_hour_month as $key => $value) "{!!date('F', strtotime(($key.'01'))).' '.substr($key, 0, 4) !!}", @endforeach],
                    datasets: [{
                        label: 'Actual',
                        data: [@foreach($total_actual_hour_month as $key => $value) "{!!$value!!}", @endforeach],
                        backgroundColor: '#10FF97',
                        borderColor: '#191950',
                    },
                    {
                        label: 'Forecast',
                        data: [@foreach($total_forecust_hour_month as $key => $value) "{!!$value!!}", @endforeach],
                        backgroundColor: '#FFD400',
                        borderColor: '#191950',
                    },
                    {
                        label: 'Budget Hours',
                        data: [@foreach($total_actual_hour_month as $key => $value) "{!!$value!!}", @endforeach],
                        borderColor: '#FD8E8C',
                        type: 'line'
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: true,
                    }
                }
            });
        }

        var spend_by_month_chart = document.getElementById("spend_hour_chart");
        if (spend_by_month_chart) {
            var loginsSuccChart = new Chart(spend_by_month_chart, {
                type: 'bar',
                data: {
                    labels: [@foreach($total_actual_hour_month as $key => $value) "{!!date('F', strtotime(($key.'01'))).' '.substr($key, 0, 4) !!}", @endforeach],
                    datasets: [{
                        label: 'Actual',
                        data: [@foreach($total_actual_hour_month_spend as $key => $value) "{!!$value!!}", @endforeach],
                        backgroundColor: '#10FF97',
                        borderColor: '#191950',
                    },
                    {
                        label: 'Forecast',
                        data: [@foreach($total_forecust_hour_month_spend as $key => $value) "{!!$value!!}", @endforeach],
                        backgroundColor: '#FFD400',
                        borderColor: '#191950',
                    },
                    {
                        label: 'Budget Hours',
                        data: [@foreach($total_actual_hour_month_spend as $key => $value) "{!!$value!!}", @endforeach],
                        borderColor: '#FD8E8C',
                        type: 'line'
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: true,
                    }
                }
            });
        }

    </script>
@endsection