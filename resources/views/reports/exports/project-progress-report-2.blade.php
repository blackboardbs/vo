<table class="table table-bordered table-sm mt-3">
    <thead class="bg-dark">
    <tr>
        <th class="align-middle">Resource</th>
        <th class="align-middle">Epic</th>
        <th class="align-middle">Feature</th>
        <th class="align-middle">Bill</th>
        <th class="text-right align-middle">Planned Hours</th>
        <th class="text-right align-middle">Actual Hours</th>
    </tr>
    </thead>
    <tbody>
    @foreach($export["data"] as $progress)
        <tr>
            <td>{{$progress["resource"]??''}}</td>
            <td>{{$progress["epic"] != ''? $progress["epic"] : 'No (task/user story/feature/epic)'}}</td>
            <td>{{$progress["feature"] != '' ? $progress["feature"] : 'No (task/user story/feature)'}}</td>
            <td>{{$progress["billable"]?"Yes":"No"}}</td>
            <td class="text-right">{{number_format($progress["planned_hours"],2)}}</td>
            <td class="text-right">{{number_format($progress["total_hours"],2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
