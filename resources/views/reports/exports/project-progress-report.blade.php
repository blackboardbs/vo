<table class="table table-bordered table-sm mt-3">
    <thead class="bg-dark">
    <tr>
        <th class="align-top">Resource</th>
        <th class="align-top">Epic</th>
        <th class="align-top">Feature</th>
        <th class="align-top">User Story</th>
        <th class="align-top">Task</th>
        <th class="align-top">Status</th>
        <th class="align-top">Bill</th>
        <th class="text-right align-top">Planned Hours</th>
        <th class="text-right align-top">Actual Hours</th>
    </tr>
    </thead>
    <tbody>
    @foreach($export["data"] as $progress)
        <tr>
        <td>{{$progress["resource"]??''}}</td>
        <td>{{$progress["epic"] != '' ? $progress["epic"] : 'No (task/user story/feature/epic)'}}</td>
        <td>{{$progress["feature"] != '' ? $progress["feature"] : 'No (task/user story/feature)'}}</td>
        <td>{{$progress["user_story"] != '' ? $progress["user_story"] : 'No (task/user story)'}}</td>
        <td>{{$progress["task_description"] != '' ? $progress["task_description"] : "No task"}}</td>
        <td>{{$progress["task_status"] != '' ? $progress["task_status"] : 'No task'}}</td>
        <td>{{$progress["billable"] == 1?"Yes":"No"}}</td>
        <td class="text-right">{{number_format($progress["planned_hours"],2)}}</td>
        <td class="text-right">{{number_format($progress["total_hours"],2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
