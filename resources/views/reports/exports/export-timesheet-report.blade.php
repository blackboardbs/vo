<table class="table table-bordered table-sm mt-3">
    <thead class="bg-dark">
    <tr>
        <th>Resource</th>
        <th>Project Name</th>
        <th>Task</th>
        <th>Description of work</th>
        <th>Wk</th>
        <th>Bill</th>
        <th>Mon</th>
        <th>Tue</th>
        <th>Wed</th>
        <th>Thu</th>
        <th>Fri</th>
        <th>Sat</th>
        <th>Sun</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @forelse($timelines['timelines'] as $timeline)
        <tr>
            <td>{{$timeline->timesheet->user->resource??''}}</td>
            <td>{{$timeline->timesheet->project->name??''}}</td>
            <td>{{$timeline->task->description??''}}</td>
            <td>{{$timeline->description_of_work}}</td>
            <td>{{$timeline->timesheet->year_week??''}}</td>
            <td>{{$timeline->is_billable?"Yes":"No"}}</td>
            <td class="text-right">{{$timeline->time($timeline->monday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->tuesday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->wednesday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->thursday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->friday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->saturday)}}</td>
            <td class="text-right">{{$timeline->time($timeline->sunday)}}</td>
            <th class="text-right">{{$timeline->time($timeline->total)}}</th>
        </tr>
    @empty
        <tr>
            <td class="text-center" colspan="13">No timesheets available</td>
        </tr>
    @endforelse

    @if(count($timelines['timelines']) > 0)
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th class="text-right text-uppercase">total</th>
            <th class="text-right">{{$timeline->time($totals['totals']['monday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['tuesday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['wednesday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['thursday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['friday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['saturday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['sunday'])}}</th>
            <th class="text-right">{{$timeline->time($totals['totals']['total'])}}</th>
        </tr>
    @endif
    </tbody>
</table>
