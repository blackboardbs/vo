<table class="table table-bordered table-sm mt-3">
    <thead class="bg-dark">
    <tr>
        <th class="align-top">Resource</th>
        <th class="align-top">Epic</th>
        <th class="align-top">Feature</th>
        <th class="align-top">User Story</th>
        <th class="align-top">Bill</th>
        <th class="text-right">Planned Hours</th>
        <th class="text-right">Actual Hours</th>
    </tr>
    </thead>
    <tbody>
    @foreach($export["data"] as $progress)
        <tr>
            <td>{{$progress["resource"]??''}}</td>
            <td>{{$progress["epic"] != ''? $progress["epic"] : 'No (task/user story/feature/epic)'}}</td>
            <td>{{$progress["feature"] != '' ? $progress["feature"] : 'No (task/user story/feature)'}}</td>
            <td>{{$progress["user_story"] != '' ? $progress["user_story"] : 'No (task/user story)'}}</td>
            <td>{{$progress["billable"]?"Yes":"No"}}</td>
            <td class="text-right">{{number_format($progress["planned_hours"],2)}}</td>
            <td class="text-right">{{number_format($progress["total_hours"],2)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
