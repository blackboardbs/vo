<table class="table table-bordered table-sm mt-3">
    <thead class="bg-dark">
    <tr>
        <th class="align-top">Resource</th>
        <th class="align-top">Epic</th>
        <th class="align-top">Feature</th>
        <th class="align-top">User Story</th>
        <th class="align-top">Task</th>
        <th class="align-top">Delivery Type</th>
        <th class="align-top">Status</th>
        <th class="align-top">Bill</th>
        <th class="text-right align-top">Planned Hours</th>
        <th class="text-right align-top">Actual Hours</th>
        <th class="text-right align-top">Variance</th>
    </tr>
    </thead>
    <tbody>
    @foreach($project_progress as $progress)
        <tr>
            <td>{{$progress["resource"]??''}}</td>
            <td>{{$progress["user_story"]->feature->epic->name??null}}</td>
            <td>{{$progress["user_story"]->feature->name??null}}</td>
            <td>{{$progress["user_story"]->name??null}}</td>
            <td>{{$progress["description"]??null}}</td>
            <td>{{$progress["delivery_type"]??null}}</td>
            <td>{{$progress["taskstatus"]->description??null}}</td>
            <td>{{$progress["billable"]?"Yes":"No"}}</td>
            <td class="text-right">{{$progress["planned_hours"]}}</td>
            <td class="text-right">{{$progress["actual_hours"]}}</td>
            <td class="text-right">{{$progress["variance"]}}</td>
        </tr>
    @endforeach
    @if(!$project_progress->isEmpty())
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="text-uppercase text-right">total</th>
            <th class="text-right">{{_minutes_to_time($project_progress->sum('planned_hours_raw'))}}</th>
            <th class="text-right">{{_minutes_to_time($project_progress->sum('actual_hours_raw'))}}</th>
            <th class="text-right">{{_minutes_to_time($project_progress->sum('variance_raw'))}}</th>
        </tr>
    @endif
    </tbody>
</table>
