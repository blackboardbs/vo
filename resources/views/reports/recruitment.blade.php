@extends('adminlte.default')

@section('title') Recruitment Reports @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                    <tr class="btn-dark">
                        <th>Report Code</th>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="{{route('reports.scouting')}}">R53</a></td>
                        <td><a href="{{route('reports.scouting')}}">Scouting</a></td>
                        <td><a href="{{route('reports.scouting')}}">List Scouting Reports</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.scoutinganalysis')}}">R56</a></td>
                        <td><a href="{{route('reports.scoutinganalysis')}}">Scouting Analysis</a></td>
                        <td><a href="{{route('reports.scoutinganalysis')}}">List Scouting Analysis Reports</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.jobspecsammary')}}">R57</a></td>
                        <td><a href="{{route('reports.jobspecsammary')}}">Job Specs Summary</a></td>
                        <td><a href="{{route('reports.jobspecsammary')}}">List Job Specs Summary</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            var value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection
