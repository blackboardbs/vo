@extends('adminlte.default')

@section('title') Reports @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Report Code</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="{{route('reports.consultant_bonus')}}">R33</a></td>
                        <td><a href="{{route('reports.consultant_bonus')}}">Consultant bonus</a></td>
                        <td><a href="{{route('reports.consultant_bonus')}}">List consultant bonus</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection
