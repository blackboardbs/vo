@extends('adminlte.default')

@section('title') Function Summary Report (R40) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.summary_report"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $resource_drop_down, request()->employee , ['class' => 'form-control w-100 search']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_drop_down, request()->team , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control w-100 search']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_from', request()->date_from , ['class' => 'form-control datepicker w-100 search']) !!}
                        <span>Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_to', request()->date_to , ['class' => 'form-control datepicker w-100 search']) !!}
                        <span>Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control w-100 search']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <a href="{{route('reports.summary_report')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr class="bg-dark">
                        <th>Function</th>
                        <th>Period</th>
                        <th class="text-right">Hours</th>
                        <th class="text-right">Value</th>
                        <th class="text-right">%</th>
                    </tr>
                </thead>
                <tbody>

                    @forelse($functions as $function)

                        <tr>
                            <td>{{isset($function->function_desc)?$function->function_desc->description:''}}</td>
                            <td>{{$function->year.'/'.(($function->month < 10)?'0'.$function->month:$function->month)}}</td>
                            <td class="text-right">{{number_format($function->hours,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($function->value,2,'.',',')}}</td>
                            <td class="text-right">{{($function->value > 0 && $total_value > 0)?number_format((($function->value/$total_value)*100),0,'.',','):0}}%</td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="10" class="text-center">Nothing to show.</td>
                            </tr>
                    @endforelse
                <tr>
                    <th colspan="2" class="text-right">TOTAL</th>
                    <th class="text-right">{{number_format($total_hours,2,'.',',')}}</th>
                    <th class="text-right">{{number_format($total_value,2,'.',',')}}</th>
                    <th></th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $functions->firstItem() }} - {{ $functions->lastItem() }} of {{ $functions->total() }}
                        </td>
                        <td>
                            {{ $functions->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
