@extends('adminlte.default')

@section('title') Assignment Status Report (R61) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.assignmentstatus"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-3 col-sm mb-1" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company',$company_drop_down,request()->company,['class'=>' form-control search', 'id' => 'company','style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project',$project_drop_down,request()->project,['class'=>' form-control search', 'id' => 'project', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource',$employee_drop_down,request()->resource,['class'=>' form-control search', 'id' => 'employee', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('year',[2024 => 2024,2023 => 2023,2022 => 2022,2021 => 2021,2020 => 2020,2019 => 2019, 2018 => 2018, 2017 => 2017], request()->year,['class'=>' form-control search', 'id' => 'year', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Year</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width: 20%;">
                <div class="form-group input-group">
                <a href="{{route('reports.assignmentstatus')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="limiter">
            <div style="max-width: 100% !important;">
                <div class=" js-pscroll">
                    <div class="table-container">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="row100 head bg-dark">
                                    <th style="width: 300px;" class="">Customer</th>
                                    <th style="width: 300px;" class="">Project</th>
                                    <th class="">Assignment ID</th>
                                    <th class="">Assignment Status</th>
                                    <th class="">Billable</th>
                                    <th class="">Start</th>
                                    <th class="">End</th>
                                    <th class="">Capacity</th>
                                    <th class="">PO Hours</th>
                                    <th class="">Actual Hours</th>
                                    <th class="">Hours Left to Spend</th>
                                    <th class="" style="width: 300px;">Days before Assignment End Date</th>
                                    <th class=" text-right">%Completion</th>
                                    <th class=" text-right">Planned Labour</th>
                                    <th class=" text-right">Planned Expenses</th>
                                    <th class=" text-right">Planned Income</th>
                                    <th class=" text-right">Planned Profit</th>
                                    <th class=" text-right">Planned Profit%</th>
                                    <th class=" text-right">Actual Labour</th>
                                    <th class=" text-right">Actual Expenses</th>
                                    <th class=" text-right">Actual Income</th>
                                    <th class=" text-right">Actual Profit</th>
                                    <th class=" text-right">Actual Profit%</th>
                                    <th class=" text-right">Labour Variance</th>
                                    <th class=" text-right">Income variance</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total_po_hours = 0;
                                    $total_actual_hours = 0;
                                    $total_hours_left_to_spend = 0;
                                    $total_days_before_assignment_end_date = 0;
                                    $total_completion_percentage = 0;
                                    $total_planned_labour = 0;
                                    $total_expense_amount = 0;
                                    $total_planned_income = 0;
                                    $total_planned_profit = 0;
                                    $total_planned_profit_percentage = 0;
                                    $total_actual_labour = 0;
                                    $total_actual_expenses = 0;
                                    $total_actual_income = 0;
                                    $total_actual_profit = 0;
                                    $total_actual_profit_percentage = 0;
                                    $total_labour_variance = 0;
                                    $total_income_variance = 0;
                                @endphp
                                @forelse($assignments as $key => $assignment)
                                    @php

                                        $po_hours = $assignment->hours;
                                        $actual_hours_ = $actual_hours[$key]->hours;
                                        $hours_left_to_spend = $assignment->hours - $actual_hours_;
                                        $completion_percentage = $assignment->hours == 0 ? 0 : $actual_hours[$key]->hours / $assignment->hours  * 100;
                                        $planned_labour = $assignment->hours * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
                                        $planned_income = $assignment->hours * $assignment->invoice_rate;

                                        $planned_profit = ($assignment->hours * $assignment->invoice_rate) - ($assignment->hours * ($assignment->internal_cost_rate + $assignment->external_cost_rate)) - $total_expense_amount_array[$key];
                                        $planned_profit_percentage = ($assignment->hours * $assignment->invoice_rate) == 0 ? 0 : $planned_profit / ($assignment->hours * $assignment->invoice_rate) * 100;
                                        $actual_labour = $actual_hours_ * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
                                        $actual_expenses = $total_actual_expense_claim_array[$key];
                                        $actual_income = $actual_hours_ * $assignment->invoice_rate;
                                        $actual_profit = $actual_income - $actual_labour - $actual_expenses;
                                        $actual_profit_percentage = $actual_income == 0 ? 0 : $actual_profit / $actual_income * 100;
                                        $labour_variance = $planned_labour - $actual_labour;
                                        $income_variance = $planned_income - $actual_income;

                                        $total_po_hours += $po_hours;
                                        $total_actual_hours += $actual_hours_;
                                        $total_hours_left_to_spend += $hours_left_to_spend;
                                        $total_days_before_assignment_end_date += $days_before_assignment_end_date_array[$key];
                                        $total_completion_percentage += $completion_percentage;
                                        $total_planned_labour += $planned_labour;
                                        $total_expense_amount += $total_expense_amount_array[$key];
                                        $total_planned_income += $planned_income;
                                        $total_planned_profit += $planned_profit;
                                        $total_planned_profit_percentage += $planned_profit_percentage;
                                        $total_actual_labour += $actual_labour;
                                        $total_actual_expenses += $actual_expenses;
                                        $total_actual_income += $actual_income;
                                        $total_actual_profit += $actual_profit;
                                        $total_actual_profit_percentage += $actual_profit_percentage;
                                        $total_labour_variance += $labour_variance;
                                        $total_income_variance += $income_variance;

                                    @endphp
                                    <tr class="row100 body">
                                        <td class="">{{ $customer_name[$key] }}</td>
                                        <td class="">{{ $assignment->project->name }}</td>
                                        <td class="">{{$assignment->id}}</td>
                                        <td class="">{{isset($assignment->statusd->description)?$assignment->statusd->description:''}}</td>
                                        <td class="">{{ $assignment->billable == 1 ? 'Yes' : 'No' }}</td>
                                        <td class="">{{ $assignment->start_date }}</td>
                                        <td class="">{{ $assignment->end_date }}</td>
                                        <td class=" text-right">{{ $capacity_allocation_hours_array[$key] }}</td>
                                        <td class=" text-right">{{ $po_hours }}</td>
                                        <td class=" text-right">{{ number_format($actual_hours_,2,'.',',') }}</td>
                                        <td class=" text-right">{{ number_format($hours_left_to_spend,2,'.',',') }}</td>
                                        <td class=" text-right">{{ $days_before_assignment_end_date_array[$key] }}</td>
                                        <td class=" text-right">{{ number_format($completion_percentage, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($planned_labour,2,'.',',') }}</td>
                                        <td class=" text-right">{{ number_format($total_expense_amount_array[$key],2,'.',',') }}</td>
                                        <td class=" text-right">{{ number_format($planned_income,2,'.',',') }}</td>
                                        <td class=" text-right">{{ number_format($planned_profit, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($planned_profit_percentage, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($actual_labour, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($actual_expenses, 2, '.', ',') }}</td>
                                        <td class=" text-rightAss">{{ number_format($actual_income, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($actual_profit, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($actual_profit_percentage, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($labour_variance, 2, '.', ',') }}</td>
                                        <td class=" text-right">{{ number_format($income_variance, 2, '.', ',') }}</td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td class="text-center" colspan="25">No consultant expenses available</td>
                                    </tr>
                                @endforelse
                                <tr class="bg-dark">
                                    <td ></td>
                                    <th  colspan="7"></th>
                                    <th  class=" text-right">{{ number_format($total_po_hours,2,'.',',') }}</th>
                                    <th  class=" text-right">{{number_format($total_actual_hours,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_hours_left_to_spend,2,'.',',')}}</th>
                                    <th  class=" text-right">{{$total_days_before_assignment_end_date}}</th>
                                    <th  class=" text-right">{{number_format($total_completion_percentage,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_planned_labour,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_expense_amount,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_planned_income,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_planned_profit,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_planned_profit_percentage,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_actual_labour,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_actual_expenses,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_actual_income,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_actual_profit,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_actual_profit_percentage,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_labour_variance,2,'.',',')}}</th>
                                    <th  class=" text-right">{{number_format($total_income_variance,2,'.',',')}}</th>
                                </tr>
                                </tbody>
                        </table>
                        <div class=" col-md-12 text-center" style="align-items: center;">      
                            <table class="tabel tabel-borderless" style="margin: 0 auto">
                                <tr>
                                    <td style="vertical-align: center;">
                                        Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $assignments->firstItem() }} - {{ $assignments->lastItem() }} of {{ $assignments->total() }}
                                    </td>
                                    <td>
                                        {{ $assignments->appends(request()->except('page'))->links() }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <style>
        .table-container {
            overflow-x: scroll;
            max-width: 100%;
        }

        .table-container table {
            table-layout: fixed;
            width: auto;
        }

        .table-container th,
        .table-container td {
            white-space: nowrap;
            padding: 8px;
        }

        .table-container th:first-child{
            position: sticky;
            left: 0;
            z-index: 2;
            background-color: #1460C9;
            border-right: 1px solid #dee2e6;
        }

        .table-container td:first-child{
            position: sticky;
            left: 0;
            z-index: 2;
            background-color: #ffffff;
            border-right: 1px solid #dee2e6;
        }

    </style>
@endsection
@section('extra-js')

    <script src="{{asset('dist/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/select2/select2.min.js')}}"></script>
    <script src="{{asset('dist/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });

        $('.js-pscroll').each(function(){
            var ps = new PerfectScrollbar(this);

            $(window).on('resize', function(){
                ps.update();
            })

            $(this).on('ps-x-reach-start', function(){
                $(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
            });

            $(this).on('ps-scroll-x', function(){
                $(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
            });

        });
    </script>
@endsection
