@extends('adminlte.default')
@section('title') Resource Analysis @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.resource"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-4 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project',$projects,request()->project,['class'=>' form-control w-100 search'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource',$resource,request()->resource,['class'=>' form-control w-100 search'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm mb-1">
                <div class="form-group input-group">
                <a href="{{route('reports.resource')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Number of Resources by Role
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="resource_role_chart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Number of Resources by Projects
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="resource_project_chart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>@sortablelink('project.name', 'Project')</th>
                            <th>@sortablelink('consultant.first_name', 'Consultant')</th>
                            <th>@sortablelink('manager.first_name', 'Coach / Line Manager')</th>
                            <th>@sortablelink('', 'Actual Hours')</th>
                            <th>@sortablelink('', 'Forecast Hours')</th>
                            <th>@sortablelink('', 'Planned Hours')</th>
                            <th>@sortablelink('', 'Variance Hours')</th>
                            <th>@sortablelink('', 'Actual Spend')</th>
                            <th>@sortablelink('', 'Forecast Spend')</th>
                            <th>@sortablelink('', 'Budget')</th>
                            <th>@sortablelink('', 'Budget Variance')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total_actual_hours = 0;
                            $total_forecast_hours = 0;
                            $total_planned_hours = 0;
                            $total_variance = 0;
                            $total_forecast_spend = 0;
                            $total_actual_spent = 0;
                            $total_forecast_spent = 0;
                            $total_total_project_budget = 0;
                        @endphp
                        @forelse($assignments as $index => $assignment)
                            @php
                                $total_actual_hours += $actual_hours[$index]->hours;
                                $total_forecast_hours += $hours_forecast[$index];
                                $total_planned_hours += $actual_hours[$index]->hours + $hours_forecast[$index];
                                $total_variance += $variance_po[$index];
                                $total_actual_spent += $actual_hours[$index]->hours * $invoice_rate[$index];
                                $total_forecast_spent += $hours_forecast[$index] * $invoice_rate[$index];
                                $total_total_project_budget += $total_project_budget[$index]->value;
                            @endphp
                            <tr>
                                <td>{{isset($assignment->project->name) ? $assignment->project->name: ''}}</td>
                                <td>{{isset($assignment->consultant->first_name)? $assignment->consultant->first_name.' '.$assignment->consultant->last_name: 'Consultant' }}</td>
                                <td>{{ $assignment->report_to }}</td>
                                <td style="text-align: right;">{{number_format($actual_hours[$index]->hours,2,'.',',')}}</td>
                                <td style="text-align: right;">{{number_format($hours_forecast[$index],2,'.',',')}}</td>
                                <td style="text-align: right;">{{ number_format($actual_hours[$index]->hours + $hours_forecast[$index],2,'.',',') }}</td>
                                <td style="text-align: right;">{{ number_format($variance_po[$index] ,2,'.',',') }}</td>
                                <td style="text-align: right;">{{number_format(($actual_hours[$index]->hours * $invoice_rate[$index]) ,2,'.',',')}}</td>
                                <td style="text-align: right;">{{number_format(($hours_forecast[$index] * $invoice_rate[$index]),2,'.',',')}}</td>
                                <td style="text-align: right;">{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
                                <td style="text-align: right;">{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No assignments match those criteria.</td>
                            </tr>
                        @endforelse
                        <tr class="btn-dark">
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;">{{number_format($total_actual_hours,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_forecast_hours,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_planned_hours,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_variance,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_actual_spent,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_forecast_spent,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_total_project_budget,2,'.',',')}}</th>
                            <th style="text-align: right;">{{number_format($total_total_project_budget,2,'.',',')}}</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td class="text-right" style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $assignments->firstItem() }} - {{ $assignments->lastItem() }} of {{ $assignments->total() }}
                        </td>
                        <td class="text-left">
                            {{ $assignments->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script src="{!! asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
    <script>

        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });

        var resourcer_chart = document.getElementById("resource_role_chart");
        if (resourcer_chart) {
            var resourceRoleChart = new Chart(resourcer_chart, {
                type: 'horizontalBar',
                data: {
                    labels: [@foreach($resource_roles_counter as $resource_role_counter) "{!! $resource_role_counter->role !!}", @endforeach],
                    datasets: [{
                        label: '',
                        data: [@foreach($resource_roles_counter as $resource_role_counter) "{!! $resource_role_counter->counter !!}", @endforeach],
                        backgroundColor: '#191950',
                        borderColor: '#191950',
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false,
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        }

        var vendor_chart = document.getElementById("resource_project_chart");
        if (vendor_chart) {
            var loginsSuccChart = new Chart(vendor_chart, {
                type: 'horizontalBar',
                data: {
                    labels: [@foreach($project_resources_counter as $project_resource_counter) "{!! $project_resource_counter->name !!}", @endforeach],
                    datasets: [{
                        label: '',
                        data: [@foreach($project_resources_counter as $project_resource_counter) "{!! count(explode(',', $project_resource_counter->consultants)) !!}", @endforeach],
                        backgroundColor: '#191950',
                        borderColor: '#191950',
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false,
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        }

    </script>
@endsection