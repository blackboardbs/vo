@extends('adminlte.default')

@section('title') Expenses Report (R29) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.consultant_expenses"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
            
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('from', old('from'), ['class' => 'form-control search w-100 datepicker']) !!}
                        <span>Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('to', old('to'), ['class' => 'form-control search w-100 datepicker']) !!}
                        <span>Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('claimable', ['0' => 'No', '1' => 'Yes'], request()->claimable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Claimable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('payment_status', ['0' => 'Not Paid', '1' => 'Paid'], request()->payment_status , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Payment Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1">
                <div class="form-group input-group">
                        <a href="{{route('reports.consultant_expenses')}}" class="btn w-100 btn-info">Clear Filters</a> </td>
                </div>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr class="bg-dark">
                        <th colspan="11">Resource</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total = 0;
                        $total_claim = 0;
                        $total_bill = 0;
                    @endphp
                    @forelse($resources as $index => $results)
                                <tr>
                                    <td>{{$results->first_name.' '.$results->last_name}}</td>
                                    <td colspan="10">
                                        <table class="table">
                                            <thead>
                                                <tr class="bg-secondary">
                                                    <th>Project</th>
                                                    <th>Transaction Date</th>
                                                    <th>Account</th>
                                                    <th>Account Element</th>
                                                    <th>Description</th>
                                                    <th class="text-right">Claim Amount</th>
                                                    <th class="text-right">Billable Amount</th>
                                                    <th class="text-center">Claim Approved Date</th>
                                                    <th class="text-center">Claim Paid Date</th>
                                                    <th class="text-right">Outstanding Claim Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $total_billable_amount = 0;
                                                    $total_claimable_amount = 0;
                                                    $total_outstanding_claim_amount = 0;
                                                @endphp
                                                @forelse($expenses[$index] as $expense)
                                                    <tr>
                                                        <td><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{isset($expense->timesheet->project)?$expense->timesheet->project->name:''}}</a></td>
                                                        <td><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{$expense->date}}</a></td>
                                                        <td><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{isset($expense->account)?$expense->account->description:''}}</a></td>
                                                        <td><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{isset($expense->account_element)?$expense->account_element->description:''}}</a></td>
                                                        <td><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{$expense->description}}</a></td>
                                                        <td class="text-right"><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{($expense->claim->value == 1)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</a></td>
                                                        <td class="text-right"><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{($expense->is_billable->value == 1)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</a></td>
                                                        <td class="text-center"><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{($expense->claim_auth_date != null)?$expense->claim_auth_date:'0000-00-00'}}</a></td>
                                                        <td class="text-center"><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{($expense->paid_date != null)?$expense->paid_date:'0000-00-00'}}</a></td>
                                                        <td class="text-right"><a href="{{route('timesheet.maintain', $expense->timesheet_id)}}#expenses">{{($expense->claim->value == 1 && $expense->paid_date == null)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</a></td>
                                                    </tr>
                                                    @php
                                                        $total_billable_amount += ($expense->is_billable->value == 1)?$expense->amount:0;
                                                        $total_claimable_amount += ($expense->claim->value == 1)?$expense->amount:0;
                                                        $total_outstanding_claim_amount += ($expense->claim->value == 1 && $expense->paid_date == null)?$expense->amount:0;
                                                    @endphp
                                                    @empty
                                                @endforelse
                                                <tr>
                                                    <th colspan="5" class="text-right">TOTAL</th>
                                                    <th class="text-right">{{number_format($total_claimable_amount, 2,'.',',')}}</th>
                                                    <th class="text-right">{{number_format($total_billable_amount, 2,'.',',')}}</th>
                                                    <th colspan="2"></th>
                                                    <th class="text-right">{{number_format($total_outstanding_claim_amount, 2,'.',',')}}</th>
                                                </tr>
                                            </tbody>
                                        </table>

                                        {{ $expenses[$index]->appends(request()->except(substr($results->first_name,0,4).'_'.substr($results->last_name,0,1)))->links() }}
                                    </td>
                                </tr>
                        @empty
                        <tr>
                            <td class="text-center" colspan="8">No consultant expenses available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {{--{{ $timesheet->appends(request()->except('page'))->links() }}--}}
        </div>

    </div>
@endsection
