@extends('adminlte.default')

@section('title') Project Summary report (R25) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.customer_project_summary"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '60')}}" />
                <div class="col-sm-3 col-sm mb-3" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Company</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:16%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Project</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:16%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('employee', $resource_drop_down, request()->employee , ['class' => 'form-control search w-100 ']) !!}
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('team', $team_drop_down, request()->team , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Team</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Year Month</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::text('date_from', request()->date_from , ['class' => 'form-control search w-100 datepicker']) !!}
                            <span>Date From</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::text('date_to', request()->date_to , ['class' => 'form-control search w-100 datepicker']) !!}
                            <span>Date To</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:16%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Billable</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:16%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('project_status', $project_status_dropdown, request()->project_status , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Project Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('project_type', $project_type_dropdown, request()->project_type , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Project Type</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:17%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <a href="{{route('reports.customer_project_summary')}}" class="btn btn-info w-100">Clear Filters</a>
                        </label>
                    </div>
                </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th class="top-align">Project</th>
                    <th class="top-align">Customer</th>
                    <th class="top-align">PO Number</th>
                    <th class="top-align">Status</th>
                    <th class="top-align">Start Date</th>
                    <th class="top-align">End Date</th>
                    <th class="top-align">Billable Hours</th>
                    <th class="top-align">Non-Billable Hours</th>
                    <th class="top-align">PO Hours</th>
                    <th class="top-align">PO Value</th>
                    <th class="top-align">Outstanding Hours</th>
                    <th class="top-align">Outstanding Value</th>
                    <th class="top-align">Completion %</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($projects as $project)
                        <tr>
                            <td>{{ isset($project->project) ? $project->project->name : '' }}</td>
                            <td>{{isset($project->customer)?$project->customer->customer_name:''}}</td>
                            <td>{{isset($project->project)?$project->project->customer_po:''}}</td>
                            <td>{{isset($project->project->status)?$project->project->status->description:''}}</td>
                            <td style="width: 100px">{{isset($project->project)?$project->project->start_date:''}}</td>
                            <td style="width: 100px">{{isset($project->project)?$project->project->end_date:''}}</td>
                            <td class="text-right">{{number_format($project->billable_hours,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($project->non_billable_hours,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($project->hours,2,'.',',')}}</td>
                            <td class="text-right">{{number_format(($project->hours * $project->invoice_rate),2,'.',',')}}</td>
                            <td class="text-right">{{number_format(($project->hours - $project->billable_hours),2,'.',',')}}</td>
                            <td class="text-right">{{(($project->hours - $project->billable_hours) >= 0)?number_format((($project->hours - $project->billable_hours)*$project->invoice_rate),2,'.',','):number_format(0,2,'.',',')}}</td>
                            <td class="text-right">{{($project->billable_hours > 0 && $project->hours > 0)?number_format((($project->billable_hours/$project->hours)*100),0,'.',','):0}}%</td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="13" class="text-center">Nothing to show</td>
                            </tr>
                    @endforelse

                    <tr>
                        <th colspan="6" class="text-right pr-4">TOTAL</th>
                        <th class="text-right">{{ number_format($total_billable_hours,2,'.',',') }}</th>
                        <th class="text-right">{{number_format($total_non_billable_hour,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_po_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_po_value,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_outstanding_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_outstanding_value,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_percentage,0,'.',',')}}%</th>
                    </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $projects->firstItem() }} - {{ $projects->lastItem() }} of {{ $projects->total() }}
                        </td>
                        <td>
            {{ $projects->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .top-align{
            vertical-align: top!important;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
