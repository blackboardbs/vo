@extends('adminlte.default')

@section('title') Landing Page Dashboard Configs @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#exampleModal">
                    </i> Create Dashboard
                </button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <th>Dashboard Name</th>
                <th>Status</th>
                <th>Preview</th>
                <th class="last">Actions</th>
                </thead>
                <tbody>
                @forelse($list_dashboards as $dashboards)
                    <tr>
                        <td>{{$dashboards->dashboard_name}}</td>
                        <td>{{ $dashboards->status?->description }}</td>
                        <td><a href="/dashboard?status={{$dashboards->status_id}}&name={{$dashboards->dashboard_name}}" target="_blank" class="btn btn-sm btn-secondary"><i class="fas fa-search-plus"></i> Preview</a></td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('custom_dashboard.edit', $dashboards->dashboard_name)}}" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['delete.dashboard', $dashboards->dashboard_name],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">No dashboards yet</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Your Dashboard</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{Form::open(['url' => route('preferences.dashboard'), 'method' => 'post', 'id' => 'config_form'])}}
                        <h5>Your Dashboard Name</h5>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{Form::label('dashboard_name', 'Dashboard Name')}}
                                {{Form::text('dashboard_name', old('dashboard_name'), ['class' => 'form-control'. ($errors->has('dashboard_name') ? ' is-invalid' : ''), 'id' => 'dashboard_name', 'placeholder' => 'Dashboard Name'])}}
                                @foreach($errors->get('dashboard_name') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group col-md-6">
                                {{Form::label('status_id', 'Status')}}
                                {{Form::select('status_id',$status_drop_down, 1, ['class' => 'form-control', 'id' => 'status_id'])}}
                            </div>
                        </div>

                        <hr>
                        <h5>Header Components</h5>
                        <div class="row mt-4">
                            @forelse($dynamic_components as $component)
                                @if(in_array(auth()->user()->roles->first()->id, explode('|', $component->roles)) && ($component->top_component))
                                    <div class="form-group col-md-4 mb-0">
                                        <div class="custom-control custom-checkbox">
                                            {{Form::checkbox('lp_dashboard[]', strtoupper($component->description).'_'.$component->top_component, (in_array(str_replace(' ', '_', strtoupper($component->description).'_'.$component->top_component), $user_prefs)), ['class' => 'custom-control-input small-component', 'id' => str_replace(" ", "_", strtolower($component->description)).'_'.$component->top_component])}}
                                            {{ Form::label(str_replace(" ", "_", strtolower($component->description).'_'.$component->top_component), ucwords($component->description), ['class' => 'custom-control-label mr-2']) }}
                                        </div>
                                    </div>
                                @endif
                                @empty
                            @endforelse
                        </div>
                        <div class="alert alert-warning mt-4" style="display: none"><i class="fas fa-exclamation-triangle fa-2x fa-pull-left"></i> <p class="text-bold">You can only select the maximum of 4 components</p></div>

                        <hr>
                        <h5>Charts and tables</h5>

                        <div class="row">
                            @forelse($dynamic_components as $component)
                                @if(in_array(auth()->user()->roles->first()->id, explode('|', $component->roles)) && !($component->top_component))
                                    <div class="form-group col-md-4 mb-0">
                                        <div class="custom-control custom-checkbox">
                                            {{Form::checkbox('lp_dashboard[]', strtoupper($component->description), (in_array(str_replace(' ', '_', strtoupper($component->description)), $user_prefs)), ['class' => 'custom-control-input', 'id' => str_replace(" ", "_", strtolower($component->description))])}}
                                            {{ Form::label(str_replace(" ", "_", strtolower($component->description)), ucwords($component->description), ['class' => 'custom-control-label mr-2']) }}
                                        </div>
                                    </div>
                                @endif
                            @empty
                            @endforelse
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-secondary mt-2" id="config"><i class="fas fa-cog"></i> Configure</button>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $("input.small-component").change(function () {
                var maxAllowed = 4;
                var cnt = $("input.small-component:checked").length;

                if (cnt > maxAllowed) {
                    $(this).prop("checked", "");
                    $('.alert-warning').fadeIn();
                }else {
                    $('.alert-warning').fadeOut();
                }
            });
        });

        $(document).ready(function () {

            $("#config").on('click', function () {
                if ($('#dashboard_name').val() == ''){
                    $('#dashboard_name').addClass("is-invalid");
                    return false;
                }else{
                    $('#config_form').submit();
                }
            })
        })
    </script>
@endsection