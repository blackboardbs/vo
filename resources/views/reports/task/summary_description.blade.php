@extends('adminlte.default')
@section('title') Task Summary - Task Description @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <a href="{{route('reports.task_summary_description', request()->all())}}&export" class="btn btn-info ml-1"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company', $company_drop_down, request()->company, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('customer', $customer_drop_down, request()->customer, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project', $project_drop_down, request()->project, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('employee', $resource_drop_down, request()->employee, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('task', $task_drop_down, request()->task, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pf', $period_from_drop_down, request()->pf, ['class'=>'form-control search', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pt', $period_to_drop_down, request()->pt, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mf', $year_month_drop_down, request()->mf, ['class'=>'form-control search', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mt', $year_month_drop_down, request()->mt, ['class'=>'form-controlsearch ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('b', [-1 => 'All', 0 => 'No', 1 => 'Yes'], request()->b, ['class'=>'form-control search ', 'style'=>'width: 100%;'])}}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('is', $invoice_status_drop_down, request()->is, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('ts', $task_status_drop_down, request()->ts, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Task Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('cct', $cost_centre_drop_down, request()->cct, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <a href="{{route('reports.task_summary_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Resource</th>
                    <th style="width: 15% !important;">Task</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Hours Billable</th>
                    <th>Hours Non-Billable</th>
                    <th>Hours Planned</th>
                    <th>Task Status</th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr>
                        <td>{{isset($task['timesheet']->customer->customer_name)? $task['timesheet']->customer->customer_name: '' }}</td>
                        <td>{{isset($task['timesheet']->project->name)? $task['timesheet']->project->name: '' }}</td>
                        <td>{{isset($task['timesheet']->resource->user->first_name)? $task['timesheet']->resource->user->first_name: '' }} {{isset($task['timesheet']->resource->user->last_name)? $task['timesheet']->resource->user->last_name: '' }}</td>
                        <td>{{$task['description_of_work']}}</td>
                        <td>{{$task['start_date']}}</td>
                        <td>{{$task['end_date']}}</td>
                        <td>{{$task['is_billable'] == 1 ? $task['total_minutes_to_time'] : '-'}}</td>
                        <td>{{$task['is_billable'] == 0 ? $task['total_minutes_to_time'] : '-'}}</td>
                        <td>{{$task['hours_planned'] != '' ? $task['hours_planned']: '-'}}</td>
                        <td>{{$task['task_status'] != '' ? $task['task_status']: ''}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="5"></th>
                    <th>Total</th>
                    <th>{{$timeline_minutes_total_billable}}</th>
                    <th>{{$timeline_minutes_total_non_billable}}</th>
                    <th>{{$hours_planned_total}}</th>
                    <th></th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
