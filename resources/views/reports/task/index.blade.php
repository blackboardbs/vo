@extends('adminlte.default')
@section('title') Task Reports @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Report Code</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    <tr><td><a href="{{route('reports.task_detail')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R304</a></td><td><a href="{{route('reports.task_detail')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Time report - Task Detail</a></td><td><a href="{{route('reports.task_detail')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Time report - Task Detail</a></td></tr>
                    <tr><td><a href="{{route('reports.task_finance')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R305</a></td><td><a href="{{route('reports.task_finance')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Time report - Finance</a></td><td><a href="{{route('reports.task_finance')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Time report - Finance</a></td></tr>
                    {{-- <tr><td><a href="{{route('reports.task_list')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R306</a></td><td><a href="{{route('reports.task_list')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task List</a></td><td><a href="{{route('reports.task_list')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task List</a></td></tr> --}}
                    <tr><td><a href="{{route('reports.task_list_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R307</a></td><td><a href="{{route('reports.task_list_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task List - Description</a></td><td><a href="{{route('reports.task_list_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task List - Description</a></td></tr>
                    <tr><td><a href="{{route('reports.task_utilisation')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R308</a></td><td><a href="{{route('reports.task_utilisation')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Utilisation per task for period - Task Level</a></td><td><a href="{{route('reports.task_utilisation')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Utilisation per task for period - Task Level</a></td></tr>
                    <tr>
                        <td><a href="{{route('reports.task_summary_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">R309</a></td>
                        <td><a href="{{route('reports.task_summary_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task Summary - Task Description</a></td>
                        <td><a href="{{route('reports.task_summary_description')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}">Task Summary - Task Description</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.timesheet')}}">PM01</a></td>
                        <td><a href="{{route('report.timesheet')}}">Timesheet Report</a></td>
                        <td><a href="{{route('report.timesheet')}}">List Timesheet Report</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.projectprogress')}}">PM02</a></td>
                        <td><a href="{{route('report.projectprogress')}}">Project Progress</a></td>
                        <td><a href="{{route('report.projectprogress')}}">List Project Progress</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.projectprogress2')}}">PM03</a></td>
                        <td><a href="{{route('report.projectprogress2')}}">Project Progress Summary Level 2</a></td>
                        <td><a href="{{route('report.projectprogress2')}}">List Project Progress Summary Level 2</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.projectprogress3')}}">PM04</a></td>
                        <td><a href="{{route('report.projectprogress3')}}">Project Progress Summary Level 3</a></td>
                        <td><a href="{{route('report.projectprogress3')}}">List Project Progress Summary Level 3</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.timesummary')}}">PM05</a></td>
                        <td><a href="{{route('report.timesummary')}}">Timesheet Summary</a></td>
                        <td><a href="{{route('report.timesummary')}}">List Timesheet Summary</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('report.projectdelivery')}}">PM06</a></td>
                        <td><a href="{{route('report.projectdelivery')}}">Project Delivery Report</a></td>
                        <td><a href="{{route('report.projectdelivery')}}">List Project Delivery Report</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection
