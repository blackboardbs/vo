@extends('adminlte.default')
@section('title') Time report - Task List @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <a href="{{route('reports.task_list', request()->all())}}&export" class="btn btn-dark ml-1" type="submit">Export In Excel</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('cc', $customer_dropdown, request()->cc, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('p', $project_drop_down, request()->p, ['class'=>'form-control search w-100', 'id' => 'project'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('c', $company_dropdown, request()->c, ['class'=>'form-control search w-100'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('r', $users_dropdown, request()->r, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('t', [], request()->t, ['class'=>'form-control search w-100', 'id' => 'tasks'])}}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pf', $weeks_dropdown, request()->pf, ['class'=>'form-control search w-100'])}}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pt', $weeks_dropdown, request()->pt, ['class'=>'form-control search w-100'])}}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mf', $year_month_drop_down, request()->mf, ['class'=>'form-control search w-100'])}}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mt', $year_month_drop_down, request()->mt, ['class'=>'form-control search w-100'])}}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('b', $yes_or_no_dropdown, request()->b, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('is', $invoice_status_drop_down, request()->is, ['class'=>'form-control search w-100'])}}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('cct', $cost_centre_drop_down, request()->cct, ['class'=>'form-control search w-100'])}}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width:20%;">
                <a href="{{route('reports.task_list')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Resource</th>
                    <th style="width: 15% !important;">Task</th>
                    <th>Hours Billable</th>
                    <th>Hours Non-Billable</th>
                    <th>Hours Planned</th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr>
                        <td><a href="{{route('customer.show', $task->customer_id??0)}}">{{$task->customer}}</a></td>
                        <td><a href="{{route('project.show', $task->project_id)}}">{{$task->project}}</a></td>
                        <td><a href="{{route('profile',$task->employee_id)}}">{{$task->resource}}</a></td>
                        <td><a href="{{route('task.view',$task->task_id)}}">{{$task->task_description}}</a></td>
                        <td>{{_minutes_to_time($task->billable_hours*60)}}</td>
                        <td>{{_minutes_to_time($task->non_billable_hours*60)}}</td>
                        <td>{{$task->hours_planned}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="3"></th>
                    <th>Total</th>
                    <th>{{_minutes_to_time($tasks->sum('billable_hours')*60)}}</th>
                    <th>{{_minutes_to_time($tasks->sum('non_billable_hours')*60)}}</th>
                    <th>{{$tasks->sum('hours_planned')}}</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function (){
            const url = new URL(window.location.href);

            if(url.searchParams.get('p')){
                fetch(`/api/tasksdropdown/${$("#project").val()}`)
                    .then(res => res.json())
                    .then(response => {
                        let options = `<option>All</option>`;
                        response.forEach((v, i) => {
                            if(v.id == url.searchParams.get('t')){
                                options += `<option value="${v.id}" selected>${v.description}</option>`;
                            }else{
                                options += `<option value="${v.id}">${v.description}</option>`;
                            }
                        })

                        $("#tasks").html(options);
                    })
                    .catch(err => console.log(err))
            }
        })
    </script>
@endsection
