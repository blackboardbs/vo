<table class="table table-bordered table-sm table-hover">
    <thead>
    <tr class="btn-dark">
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Task</th>
        <th>Hours Billable</th>
        <th>Hours Non-Billable</th>
        <th>Hours Planned</th>
    </tr>
    </thead>
    <tbody>
        @forelse($tasks as $task)
            <tr>
                <td>{{$task->customer_name}}</td>
                <td>{{$task->project_name}}</td>
                <td>{{$task->resource}}</td>
                <td>{{$task->description}}</td>
                <td>{{_minutes_to_time($task->billable_minutes)}}</td>
                <td>{{_minutes_to_time($task->non_billable_minutes)}}</td>
                <td>{{number_format($task->planned_hours)}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="100%" class="text-center">No tasks match those criteria.</td>
            </tr>
        @endforelse
        <tr>
            <th colspan="3"></th>
            <th>Total</th>
            <th>{{_minutes_to_time($tasks->sum('billable_minutes'))}}</th>
            <th>{{_minutes_to_time($tasks->sum('non_billable_minutes'))}}</th>
            <th>{{$tasks->sum('planned_hours')}}</th>
        </tr>
    </tbody>
</table>
