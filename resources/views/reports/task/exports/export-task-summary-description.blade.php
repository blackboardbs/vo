<table class="table table-bordered table-sm table-hover">
    <thead>
    <tr class="btn-dark">
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Task</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Hours Billable</th>
        <th>Hours Non-Billable</th>
        <th>Hours Planned</th>
        <th>Task Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tasks['tasks'] as $task)
        <tr>
            <td>{{isset($task['timesheet']->customer->customer_name)? $task['timesheet']->customer->customer_name: '' }}</td>
            <td>{{isset($task['timesheet']->project->name)? $task['timesheet']->project->name: '' }}</td>
            <td>{{isset($task['timesheet']->resource->user->first_name)? $task['timesheet']->resource->user->first_name: '' }} {{isset($task['timesheet']->resource->user->last_name)? $task['timesheet']->resource->user->last_name: '' }}</td>
            <td>{{$task['description_of_work']}}</td>
            <td>{{$task['start_date']}}</td>
            <td>{{$task['end_date']}}</td>
            <td>{{$task['is_billable'] == 1 ? $task['total_minutes_to_time'] : '-'}}</td>
            <td>{{$task['is_billable'] == 0 ? $task['total_minutes_to_time'] : '-'}}</td>
            <td>{{$task['hours_planned'] != '' ? $task['hours_planned']: '-'}}</td>
            <td>{{$task['task_status'] != '' ? $task['task_status']: ''}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="5"></th>
        <th>Total</th>
        <th>{{$tasks['timeline_minutes_total_billable']}}</th>
        <th>{{$tasks['timeline_minutes_total_non_billable']}}</th>
        <th>{{$tasks['hours_planned_total']}}</th>
        <th></th>
    </tr>
    </tbody>
</table>
