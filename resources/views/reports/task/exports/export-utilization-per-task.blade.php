<table class="table table-bordered table-sm table-hover">
    <thead>
    <tr class="btn-dark">
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Task</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Hours Billable</th>
        <th>Hours Non-Billable</th>
        <th>Hours Planned</th>
        <th>Task Status</th>
    </tr>
    </thead>
    <tbody>
    @forelse($tasks as $task)
        <tr>
            <td>{{$task->customer}}</td>
            <td>{{$task->project}}</td>
            <td>{{$task->resource}}</td>
            @if($task->task_id)
                <td>{{$task->task_description}}</td>
            @else
                <td>{{$task->task_description}}</td>
            @endif
            <td>{{$task->task_start_date}}</td>
            <td>{{$task->task_end_date}}</td>
            <td>{{_minutes_to_time($task->billable_minutes)}}</td>
            <td>{{_minutes_to_time($task->non_billable_minutes)}}</td>
            <td>{{$task->hours_planned}}</td>
            <td>{{$task->task_status}}</td>
        </tr>
    @empty
        <tr>
            <td colspan="100%" class="text-center">No tasks match those criteria.</td>
        </tr>
    @endforelse
    <tr>
        <th colspan="5"></th>
        <th>Total</th>
        <th>{{_minutes_to_time($tasks->sum('billable_minutes'))}}</th>
        <th>{{_minutes_to_time($tasks->sum('non_billable_minutes'))}}</th>
        <th>{{$tasks->sum('hours_planned')}}</th>
        <th></th>
    </tr>
    </tbody>
</table>
