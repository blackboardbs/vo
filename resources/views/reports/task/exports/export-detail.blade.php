<table class="table table-bordered table-sm table-hover">
    <thead>
    <tr>
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Task</th>
        <th>Date</th>
        <th>Hours Billable</th>
        <th>Hours Non-Billable</th>
    </tr>
    </thead>
    <tbody>
    @forelse($tasks as $task)
        <tr>
            <td>{{$task->customer_name}}</td>
            <td>{{$task->project_name}}</td>
            <td>{{$task->resource}}</td>
            @if($task->task_id)
                <td>{{$task->description()}}</td>
            @else
                <td>{{$task->description_of_work}}</td>
            @endif
            <td>{{$task->timesheet_date}}</td>
            <td>{{_minutes_to_time($task->billable_minutes)}}</td>
            <td>{{_minutes_to_time($task->non_billable_minutes)}}</td>
        </tr>
    @empty
        <tr>
            <td colspan="100%" class="text-center">No tasks match those criteria.</td>
        </tr>
    @endforelse
    <tr>
        <th colspan="4"></th>
        <th>Total</th>
        <th>{{_minutes_to_time($tasks->sum('billable_minutes'))}}</th>
        <th>{{_minutes_to_time($tasks->sum('non_billable_minutes'))}}</th>
    </tr>
    </tbody>
</table>
