<table class="table table-bordered table-sm table-hover">
    <thead>
    <tr class="btn-dark">
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Task</th>
        <th>Hours Billable</th>
        <th>Hours Non-Billable</th>
        <th>Hours Planned</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
        <tr>
            <td>{{$task->customer_name}}</td>
            <td>{{$task->project_name}}</td>
            <td>{{$task->resource}}</td>
            <td>{{$task->task_description}}</td>
            <td>{{_minutes_to_time($task->billable_hours*60)}}</td>
            <td>{{_minutes_to_time($task->non_billable_hours*60)}}</td>
            <td>{{$task->hours_planned}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="3"></th>
        <th>Total</th>
        <th>{{_minutes_to_time($tasks->sum('billable_hours')*60)}}</th>
        <th>{{_minutes_to_time($tasks->sum('non_billable_hours')*60)}}</th>
        <th>{{$tasks->sum('hours_planned')}}</th>
    </tr>
    </tbody>
</table>
