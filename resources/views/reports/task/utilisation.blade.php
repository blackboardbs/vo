@extends('adminlte.default')
@section('title') Utilisation per task for period - Task Level @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <a href="{{route('reports.task_utilisation', request()->all())}}&export" class="btn btn-info float-right ml-1"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="row w-100">
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company', $company_drop_down, request()->company, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('customer', $customer_drop_down, request()->customer, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project', $project_drop_down, request()->project, ['class'=>'form-control search w-100', 'id' => 'project', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('employee', $resource_drop_down, request()->employee, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('task', $task_drop_down, request()->task, ['class'=>'form-control search w-100', 'id' => 'tasks', 'placeholder' => 'All'])}}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            </div>
            <div class="row w-100">
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pf', $weeks_dropdown, request()->pf, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('pt', $weeks_dropdown, request()->pt, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mf', $year_month_drop_down, request()->mf, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('mt', $year_month_drop_down, request()->mt, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('b', $yes_or_no_dropdown, request()->b, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            </div>
            <div class="row w-100">
            <div class="col-sm col-sm mt-3" style="max-width:25% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('is', $invoice_status_drop_down, request()->is, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:25% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('ts', $task_status_drop_down, request()->ts, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Task Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:25% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('cct', $cost_centre_drop_down, request()->cct, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-3" style="max-width:25% !important">
                <a href="{{route('reports.task_utilisation')}}?mf={{date('Y-m')}}&mt={{date('Y-m')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Resource</th>
                    <th style="width: 15% !important;">Task</th>
                    <th style="width: 8% !important;">Start Date</th>
                    <th style="width: 8% !important;">End Date</th>
                    <th>Hours Billable</th>
                    <th>Hours Non-Billable</th>
                    <th>Hours Planned</th>
                    <th>Task Status</th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr>
                        <td><a href="{{route('customer.show', $task->customer_id??0)}}">{{$task->customer}}</a></td>
                        <td><a href="{{route('project.show', $task->project_id)}}">{{$task->project}}</a></td>
                        <td><a href="{{route('profile', $task->employee_id)}}">{{$task->resource}}</a></td>
                        @if($task->task_id)
                            <td><a href="{{route('task.view', $task->task_id)}}">{{$task->task_description}}</a></td>
                        @else
                            <td>{{$task->task_description}}</td>
                        @endif
                        <td>{{$task->task_start_date}}</td>
                        <td>{{$task->task_end_date}}</td>
                        <td>{{_minutes_to_time($task->billable_minutes)}}</td>
                        <td>{{_minutes_to_time($task->non_billable_minutes)}}</td>
                        <td>{{$task->hours_planned}}</td>
                        <td>{{$task->task_status}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="5"></th>
                    <th>Total</th>
                    <th>{{_minutes_to_time($tasks->sum('billable_minutes'))}}</th>
                    <th>{{_minutes_to_time($tasks->sum('non_billable_minutes'))}}</th>
                    <th>{{$tasks->sum('hours_planned')}}</th>
                    <th></th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $tasks->firstItem() }} - {{ $tasks->lastItem() }} of {{ $tasks->total() }}
                        </td>
                        <td>
            {{ $tasks->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

{{-- @section('extra-js')

    <script>
        $(document).ready(function (){
            const url = new URL(window.location.href);

            if(url.searchParams.get('p')){
                fetch(`/api/tasksdropdown/${$("#project").val()}`)
                    .then(res => res.json())
                    .then(response => {
                        let options = `<option>All</option>`;
                        response.forEach((v, i) => {
                            if(v.id == url.searchParams.get('t')){
                                options += `<option value="${v.id}" selected>${v.description}</option>`;
                            }else{
                                options += `<option value="${v.id}">${v.description}</option>`;
                            }
                        })

                        $("#tasks").html(options);
                    })
                    .catch(err => console.log(err))
            }
        })
    </script>
@endsection --}}
