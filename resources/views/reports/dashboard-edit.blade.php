@extends('adminlte.default')

@section('title') Landing Page Dashboard Configs @endsection

@section('header')
<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
        <div class="btn-group mr-2">
            <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
        </div>
    </div>
</div>
@endsection

@section('content')
    <div class="container-fluid">
        {{Form::open(['url' => route('update.lpdashboard'), 'method' => 'patch', 'id' => 'config_form'])}}
        <h5>Your Dashboard Name</h5>
        <div class="form-row">
            <div class="form-group col-md-6">
                {{Form::hidden('dash_name', $dash_name)}}
                {{Form::label('dashboard_name', 'Dashboard Name')}}
                {{Form::text('dashboard_name', $dash_name, ['class' => 'form-control'. ($errors->has('dashboard_name') ? ' is-invalid' : ''), 'id' => 'dashboard_name', 'placeholder' => 'Dashboard Name'])}}
                @foreach($errors->get('dashboard_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group col-md-6">
                {{Form::label('status_id', 'Status')}}
                {{Form::select('status_id',$status_drop_down, $status, ['class' => 'form-control', 'id' => 'status_id'])}}
            </div>
        </div>
        <hr>
        <h5>Header Components</h5> 
        <div class="row mt-4">
            @forelse($dynamic_components as $component)
                @if(in_array(auth()->user()->roles->first()->id, explode('|', $component->roles)) && ($component->top_component))
                    <div class="form-group col-md-4 mb-0">
                        <div class="custom-control custom-checkbox">
                            {{Form::checkbox('lp_dashboard[]', strtoupper($component->description).'_'.$component->top_component, (in_array(str_replace(' ', '_', strtoupper($component->description).'_'.$component->top_component), $user_prefs)), ['class' => 'custom-control-input small-component', 'id' => str_replace(" ", "_", strtolower($component->description)).'_'.$component->top_component])}}
                            {{ Form::label(str_replace(" ", "_", strtolower($component->description).'_'.$component->top_component), ucwords($component->description), ['class' => 'custom-control-label mr-2']) }}
                        </div>
                    </div>
                @endif
            @empty
            @endforelse
        </div>
        <div class="alert alert-warning mt-4" style="display: none"><i class="fas fa-exclamation-triangle fa-2x fa-pull-left"></i> <p class="text-bold">You can only select the maximum of 4 components</p></div>

        <hr>
        <h5>Charts and tables</h5>

        <div class="row">
            @forelse($dynamic_components as $component)
                @if(in_array(auth()->user()->roles->first()->id, explode('|', $component->roles)) && !($component->top_component))
                    @if ($component->description != 'Candidate Status' && $component->description != 'Distribution Lists')
                        <div class="form-group col-md-4 mb-0">
                            <div class="custom-control custom-checkbox">
                                {{Form::checkbox('lp_dashboard[]', strtoupper($component->description), (in_array(str_replace(' ', '_', strtoupper($component->description)), $user_prefs)), ['class' => 'custom-control-input', 'id' => str_replace(" ", "_", strtolower($component->description))])}}
                                {{ Form::label(str_replace(" ", "_", strtolower($component->description)), ucwords($component->description), ['class' => 'custom-control-label mr-2']) }}
                            </div>
                        </div>
                    @endif
                @endif
            @empty
            @endforelse
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-sm btn-secondary mt-2" id="config"><i class="far fa-save"></i> Update</button>
            </div>
        </div>

        {{Form::close()}}

    </div>
@endsection
@section('extra-js')
    <script>
        $(document).ready(function () {
            $("input.small-component").change(function () {
                var maxAllowed = 4;
                var cnt = $("input.small-component:checked").length;

                if (cnt > maxAllowed) {
                    $(this).prop("checked", "");
                    $('.alert-warning').fadeIn();
                }else {
                    $('.alert-warning').fadeOut();
                }
            });
        });

        $(document).ready(function () {

            $("#config").on('click', function () {
                if ($('#dashboard_name').val() == ''){
                    $('#dashboard_name').addClass("is-invalid");
                    return false;
                }else{
                    $('#config_form').submit();
                }
            })
        })
    </script>
@endsection