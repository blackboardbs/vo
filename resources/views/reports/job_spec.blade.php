@extends('adminlte.default')
@section('title') Job Spec Summary (R57) @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="reports.jobspecsammary"></x-export>
        <button onclick="history.back()" class="btn btn-dark float-right mr-1"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
        <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '60')}}" />
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="bg-dark">
                        <th>Reference Number</th>
                        <th>Role</th>
                        <th>Customer</th>
                        <th>Client</th>
                        <th>Position Type</th>
                        <th>Placement By</th>
                        <th>Application Closing Date</th>
                        <th>BEE</th>
                        <th>Location</th>
                        <th>Job Status</th>
                        <th class="text-right">No of CV's Submitted</th>
                        <th>Recruiter</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($job_specs as $spec)
                        <tr>
                            <td>{{$spec->reference_number}}</td>
                            <td>{{isset($spec->positionrole)?$spec->positionrole->name:null}}</td>
                            <td>{{isset($spec->customer->customer_name)?$spec->customer->customer_name:null}}</td>
                            <td>{{$spec->client}}</td>
                            <td>{{isset($spec->positiontype->description)?$spec->positiontype->description:null}}</td>
                            <td>{{$spec->placement_by}}</td>
                            <td>{{$spec->applicaiton_closing_date}}</td>
                            <td>{{($spec->b_e_e == 1)?"Yes":"No"}}</td>
                            <td>{{$spec->location}}</td>
                            <td>{{($spec->status_id == 1)?"Open":"Closed"}}</td>
                            <td class="text-right">{{$cv_count[$spec->id]}}</td>
                            <td>{{($spec->recruiter)?$spec->recruiter->first_name:null}}</td>
                        </tr>
                        @empty 
                        <tr>
                            <td class="text-center" colspan="12">There's no job spec summary available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td class="text-right" style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $job_specs->firstItem() }} - {{ $job_specs->lastItem() }} of {{ $job_specs->total() }}
                        </td>
                        <td class="text-left">
                            {{ $job_specs->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
