<div id="timesheet_api_render" name="timesheet_api_render" class="container-fluid">

    <input id="port" type="text" hidden value="{{$port['port']}}">
    <form class="form-inline mt-3 searchform1" id="searchform1" autocomplete="off">
        <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
        <div class="row w-100">
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('company', $dropdownData['company_drop_down'], request()->company , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Company</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('customer', $dropdownData['customer_drop_down'], request()->customer , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Customer</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('project', $dropdownData['project_drop_down'], request()->project , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Project</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('resource', $dropdownData['resource_drop_down'], request()->resource , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Resource</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('epic', $dropdownData['epic_dropdown'], request()->epic , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Epics</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('feature', $dropdownData['feature_dropdown'], request()->feature , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Features</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('user_story', $dropdownData['user_stories_dropdown'], request()->user_story , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>User Stories</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('task',$dropdownData['task_drop_down'], request()->task , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Tasks</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('invoice_status', $dropdownData['invoice_status_dropdown'], request()->invoice_status , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Invoice Status</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('from_week', $dropdownData['weeks_dropdown'], request()->from_week , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Week From</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('to_week', $dropdownData['weeks_dropdown'], request()->to_week , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Week To</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('from_month', $dropdownData['months_dropdown'], request()->from_month , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Month From</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('to_month',$dropdownData['months_dropdown'], request()->to_month , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Month To</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('cost_center', $dropdownData['cost_center_dropdown'], request()->cost_center , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Cost Center</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('billable', $yes_or_no_dropdown, request()->billable , ['class' => 'form-control filter search w-100', 'placeholder' => 'All']) !!}
                    <span>Billable</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('billing_cycle_id', $dropdownData['billing_cycle_dropdown'], request()->billing_cycle_id, ['class' => 'form-control filter search w-100', 'id' => 'billing_cycle_id', 'placeholder' => 'All']) !!}
                    <span>Billing Cycle</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {!! Form::select('billing_period_id', [], request()->billing_period_id, ['class' => 'form-control filter search w-100', 'id' => 'billing_period_id', 'placeholder' => 'All']) !!}
                    <span>Billing Period</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
            <div class="form-group input-group">
                <a href="{{route('api.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </div>
    </div>
    </form>
    <hr />
    <div class="table-responsive">
        <table class="table table-bordered table-sm mt-3">
            <thead class="bg-dark">
                <div>
                    <small>Preview of latest 15 results</small>
                </div>
                <tr>
                    <th class="text-capitalize text-center text-nowrap">Resource</th>
                    <th class="text-capitalize text-center text-nowrap">Project Name</th>
                    <th class="text-capitalize text-center text-nowrap">Task</th>
                    <th class="text-capitalize text-center text-nowrap">Description of work</th>
                    <th class="text-capitalize text-center text-nowrap">Wk</th>
                    <th class="text-capitalize text-center text-nowrap">Bill</th>
                    <th class="text-capitalize text-center text-nowrap">Mon</th>
                    <th class="text-capitalize text-center text-nowrap">Tue</th>
                    <th class="text-capitalize text-center text-nowrap">Wed</th>
                    <th class="text-capitalize text-center text-nowrap">Thu</th>
                    <th class="text-capitalize text-center text-nowrap">Fri</th>
                    <th class="text-capitalize text-center text-nowrap">Sat</th>
                    <th class="text-capitalize text-center text-nowrap">Sun</th>
                    <th class="text-capitalize text-center text-nowrap">Total</th>
                </tr>
            </thead>
            <tbody>
                @forelse($timelines as $timeline)
                    <tr>
                        <td>{{$timeline->timesheet->user->resource??''}}</td>
                        <td>{{$timeline->timesheet->project->name??''}}</td>
                        <td>{{$timeline->task->description??''}}</td>
                        <td>{{$timeline->description_of_work}}</td>
                        <td>{{$timeline->timesheet->year_week??''}}</td>
                        <td>{{$timeline->is_billable?"Yes":"No"}}</td>
                        <td class="text-right">{{$timeline->time($timeline->monday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->tuesday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->wednesday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->thursday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->friday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->saturday)}}</td>
                        <td class="text-right">{{$timeline->time($timeline->sunday)}}</td>
                        <th class="text-right">{{$timeline->time($timeline->total)}}</th>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="13">No timesheets available</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div> 
</div>

<script>
    $(document).ready(function() {
        var urlParams = new URLSearchParams(window.location.search);  // Initialize URL parameters globally

        function updateUrlParams() {
            var apiBase = 'https://{{tenant()->domains()?->first()?->domain}}';
            var port = $('#port').val();
            var apiEndpoint = apiBase + ':' + port + '/report/timesheet_api';
            var serializedData = $('#searchform1').serializeArray();

            // Clear existing parameters to avoid duplicates
            urlParams = new URLSearchParams();

            // Append new values from the form
            serializedData.forEach(function(item) {
                if (item.value && item.value !== 'All') {
                    urlParams.set(item.name, item.value);
                }
            });

            var updatedUrl = apiEndpoint + '?' + urlParams.toString();
            $('#url').val(updatedUrl);
            // console.log('Updated URL:', updatedUrl);
        }

        function applyFilters() {
            $('#loader').show();  // Show the loader when the request starts

            var formData = $('#searchform1').serialize();
            // console.log('Serialized Form Data:', formData);

            $.ajax({
                url: '{{ route("api.timesheetApi") }}',
                type: 'GET',
                data: formData,
                success: function(response) {
                    // console.log('AJAX response:', response);

                    // Update the table with filtered data
                    $('#timesheet_api_render').html(response.html);

                    // Assuming response.html updates the table correctly
                    // For dropdowns, assuming response has properties like response.dropdownData
                    if (response.dropdownData) {
                        $.each(response.dropdownData, function(key, options) {
                            var selectElement = $('select[name="' + key + '"]');
                            selectElement.empty(); // Clear existing options

                            $.each(options, function(index, value) {
                                selectElement.append($('<option>').text(value.text).attr('value', value.id));
                            });
                        });
                    }
                    $('#loader').hide();  // Hide the loader when the request is successful
                },
                error: function(xhr, status, error) {
                    console.error('AJAX error:', xhr.responseText); // Debug AJAX error
                    $('#loader').hide();  // Hide the loader even if there is an error
                }
            });
        }


        $('.filter').change(function() {
            updateUrlParams();
            applyFilters(); // Make sure to call applyFilters to update data
        });


        // Update the URL immediately on page load if needed
        updateUrlParams();
    });
</script>





