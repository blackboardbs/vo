@extends('adminlte.default')

@section('title') Reports @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Report Code</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="{{route('reports.customer_summary_report')}}">R23</a></td>
                        <td><a href="{{route('reports.customer_summary_report')}}">Customer Summary</a></td>
                        <td><a href="{{route('reports.customer_summary_report')}}">List customer summary</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.customer_report')}}">R24</a></td>
                        <td><a href="{{route('reports.customer_report')}}">Customer detail report</a></td>
                        <td><a href="{{route('reports.customer_report')}}">List customer detail report</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.customer_project_summary')}}">R25</a></td>
                        <td><a href="{{route('reports.customer_project_summary')}}">Project Summary report</a></td>
                        <td><a href="{{route('reports.customer_project_summary')}}">List all project summary report</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection
