@extends('adminlte.default')

@section('title')  Customer Billing (R42) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.customer_billing'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::select('dates', $dates_dropdown, request()->dates , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Date from - Date to']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Customer</th>
                    <th>Project</th>
                    <th>Consultant</th>
                    <th>Week</th>
                    <th>Invoice Dt</th>
                    <th>Bill St</th>
                    <th>Hours</th>
                    <th>Rate</th>
                    <th>Value</th>
                    <th>Expenses</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($timesheets as $timesheet)
                        <tr>
                            <td>{{ substr($timesheet->customer->customer_name,0,10) }}</td>
                            <td>{{ substr($timesheet->project->name,0,20) }}</td>
                            <td>{{ substr($timesheet->user->last_name.', '.$timesheet->user->first_name,0,10) }}</td>
                            <td>{{ $timesheet->yearwk }}</td>
                            <td>{{ $timesheet->invoice_date }}</td>
                            <td>{{ $timesheet->invoice_status->description }}</td>
                            <td>{{ number_format($timesheet->hours, 0,'.1',',') }}</td>
                            <td>{{ number_format($timesheet->rate,2,'.',',') }}</td>
                            <td>{{ number_format(($timesheet->hours * $timesheet->rate),2,'.',',') }}</td>
                            <td>{{ number_format($timesheet->expenses,2,'.',',') }}</td>
                        </tr>
                        @empty 
                    @endforelse
                    <tr>
                        <td colspan="7"></td>
                        <th>TOTAL</th>
                        <th>{{ number_format($total_value, 2, '.', ',') }}</th>
                        <th>{{ number_format($total_expenses, 2, '.', ',') }}</th>
                    </tr>
                </tbody>
            </table>
            {{ $timesheets->links() }}
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
