@extends('adminlte.default')

@section('title')  Customer detail report (R24) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
                <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Company</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Project</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('employee', $resource_drop_down, request()->employee , ['class' => 'form-control search w-100 ']) !!}
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('team', $team_drop_down, request()->team , ['class' => 'form-control search w-100 ', 'placeholder' => 'Team']) !!}
                            <span>Team</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Year Month</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::text('date_from', request()->date_from , ['class' => 'form-control search w-100 datepicker']) !!}
                            <span>Date From</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::text('date_to', request()->date_to , ['class' => 'form-control search w-100 datepicker']) !!}
                            <span>Date To</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                            <span>Billable</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <a href="{{route('reports.customer_report')}}" class="btn btn-info w-100">Clear Filters</a>
                        </label>
                    </div>
                </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th colspan="9">Customer Name</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customer_name as $index => $customer)
                    <tr>
                        <td style="width: 200px">{{$customer}}</td>
                        <td colspan="9">
                            <table class="table" style="margin-bottom:0">
                                <thead>
                                <tr class="bg-secondary">
                                    <th style="vertical-align: top;">Project</th>
                                    <th style="vertical-align: top;">YearMonth</th>
                                    <th style="vertical-align: top;" class="text-right">Billable Hours</th>
                                    <th style="vertical-align: top;" class="text-right">Non-Billable Hours</th>
                                    <th style="vertical-align: top;" class="text-right">PO Hours</th>
                                    <th style="vertical-align: top;" class="text-right">PO Value</th>
                                    <th style="vertical-align: top;" class="text-right">Outstanding Hours</th>
                                    <th style="vertical-align: top;" class="text-right">Outstanding Value</th>
                                    <th style="vertical-align: top;" class="text-right">Completion %</th>
                                    <th style="vertical-align: top;">Resource</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($project_name as $key => $project)
                                    @if($index == $project["customer_id"])
                                        <tr>
                                            <td>{!! $project["project"].'<br>' !!}</td>
                                            <td>
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        {!! $cus_sum["period"].'<br>' !!}
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>
                                            <td class="text-right">
                                                @php $total_billable_hours = 0; $total_value = 0; @endphp
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        {!! number_format($cus_sum["billable_hours"],2,'.',',').'<br>' !!}
                                                        @php
                                                            $total_billable_hours += $cus_sum["billable_hours"];
                                                            $total_value += ($cus_sum["billable_hours"] * $cus_sum["invoice_rate"])
                                                        @endphp
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>
                                            <td class="text-right">
                                                @php $total_non_billable_hours = 0 @endphp
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        {!! number_format($cus_sum["non_billable_hours"],2,'.',',').'<br>' !!}
                                                        @php $total_non_billable_hours += $cus_sum["non_billable_hours"] @endphp
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>
                                            <td class="text-right">
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        @php $po_hours = $cus_sum["po_hours"] @endphp
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>
                                            <td class="text-right">
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        @php $po_value = $cus_sum["po_value"] @endphp
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>
                                            <td>

                                            </td>
                                            <td class="text-right">

                                            </td>
                                            <td>

                                            </td>
                                            <td style="width: 200px">
                                                @forelse($customer_summary as $cus_sum)
                                                    @if($key == $cus_sum["project_id"])
                                                        {!! $cus_sum["resource"].'<br>' !!}
                                                    @endif
                                                @empty
                                                @endforelse
                                            </td>

                                        </tr>
                                        <tr>
                                            <th colspan="2" class="text-right">TOTAL</th>
                                            <th class="text-right">{{number_format($total_billable_hours,2,'.',',')}}</th>
                                            <th class="text-right">{{number_format($total_non_billable_hours,2,'.',',')}}</th>
                                            <th class="text-right">{{number_format($po_hours,2,'.',',')}}</th>
                                            <th class="text-right">{{number_format($po_value,2,'.',',')}}</th>
                                            <th class="text-right">{{number_format(($po_hours - $total_billable_hours),2,'.',',')}}</th>
                                            <th class="text-right">{{(($po_value - $total_value) < 0)?number_format(0,2,'.',','):number_format(($po_value - $total_value),2,'.',',')}}</th>
                                            <th class="text-right">{{($total_billable_hours != 0 && $po_hours != 0)?number_format((($total_billable_hours/$po_hours)*100),0,'.',','):number_format(0 ,0,'.',',')}}%</th>
                                            <th></th>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        </td>

                    </tr>
                @empty
                    <tr>
                        <td colspan="9" class="text-center">No customer detail report match your criteria</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
