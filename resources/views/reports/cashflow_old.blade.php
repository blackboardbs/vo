@extends('adminlte.default')

@section('title')  Cashflow (R34) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.cashflow"></x-export>
    </div> 
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $users_dropdown, request()->employee , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_from', request()->date_from , ['class' => 'form-control search w-100 datepicker']) !!}
                        <span>Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_to', request()->date_to , ['class' => 'form-control search datepicker w-100']) !!}
                        <span>Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <a href="{{route('reports.cashflow')}}" class="btn w-100 btn-info">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Customer Invoiced, not paid and due in x days</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Project</th>
                            <th>Resource</th>
                            <th>Yearwk</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">30+</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($invoiced as $invoice)
                            <tr>
                                <td>{{isset($invoice->customer->customer_name)?$invoice->customer->customer_name:null}}</td>
                                <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
                                <td>{{isset($invoice->user)?substr($invoice->user->first_name,0,1).'. '.$invoice->user->last_name:null}}</td>
                                <td><a href="{{route('timeline.show', $invoice->id)}}">{{$invoice->year_week}}</a></td>
                                <td class="text-right">{{number_format(($invoice->five_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->ten_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->twenty_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->thirty_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->thirty_days_plus * $invoice->rate), 2, '.', ',')}}</td>
                            </tr>
                            @php
                                $total_5_invoiced += ($invoice->five_days * $invoice->rate);
                                $total_10_invoiced += ($invoice->ten_days * $invoice->rate);
                                $total_20_invoiced += ($invoice->twenty_days * $invoice->rate);
                                $total_30_invoiced += ($invoice->thirty_days * $invoice->rate);
                                $total_30_plus_invoiced += ($invoice->thirty_days_plus * $invoice->rate);
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">All invoices are paid</th>
                            </tr>
                        @endforelse
                        <tr>
                            <th colspan="4" class="text-right">Total</th>
                            <th class="text-right">{{number_format($total_5_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_10_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_20_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_30_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_30_plus_invoiced,2,'.',',')}}</th>
                        </tr>
                        </tbody>
                    </table>
                    {{$invoiced->appends(request()->except('invoiced'))->links()}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Customer Not invoiced - billable amount including expenses</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Project</th>
                            <th>Resource</th>
                            <th>Yearwk</th>
                            <th class="text-right">Expenses (Bill)</th>
                            <th class="text-right">Labour</th>
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($not_invoiced as $invoice)
                            <tr>
                                <td>{{isset($invoice->customer->customer_name)?$invoice->customer->customer_name:null}}</td>
                                <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
                                <td>{{isset($invoice->user)?$invoice->user->first_name.' '.$invoice->user->last_name:null}}</td>
                                <td><a href="{{route('timeline.show', $invoice['id'])}}">{{$invoice['year_week']}}</a></td>
                                <td class="text-right">{{number_format((isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0),2,'.',',')}}</td>
                                <td class="text-right">{{number_format(($invoice->hours * $invoice->rate),2,'.',',')}}</td>
                                <td class="text-right">{{number_format((($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0)),2,'.',',')}}</td>
                            </tr>
                            @php
                                $total_expenses += (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0);
                                $total_labour += ($invoice->hours * $invoice->rate);
                                $total += (($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0));
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">All invoices are paid</th>
                            </tr>
                        @endforelse
                            <tr>
                                <th colspan="4" class="text-right">Total</th>
                                <th class="text-right">{{number_format($total_expenses,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_labour,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                    {{$not_invoiced->appends(request()->except('invoiced'))->links()}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Vendor Invoiced, not paid and due in x days</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Vendor</th>
                            <th>Project</th>
                            <th>Resource</th>
                            <th>Yearwk</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">30+</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($vendor_invoiced as $invoice)
                            <tr>
                                <td>{{isset($invoice->user->vendor->vendor_name)?$invoice->user->vendor->vendor_name:null}}</td>
                                <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
                                <td>{{isset($invoice->user)?substr($invoice->user->first_name,0,1).'. '.$invoice->user->last_name:null}}</td>
                                <td><a href="{{route('timeline.show', $invoice['id'])}}">{{$invoice['year_week']}}</a></td>
                                <td class="text-right">{{number_format(($invoice->five_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->ten_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->twenty_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->thirty_days * $invoice->rate), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->thirty_days_plus * $invoice->rate), 2, '.', ',')}}</td>
                            </tr>
                            @php
                                $vendor_total_5_invoiced += ($invoice->five_days * $invoice->rate);
                                $vendor_total_10_invoiced += ($invoice->ten_days * $invoice->rate);
                                $vendor_total_20_invoiced += ($invoice->twenty_days * $invoice->rate);
                                $vendor_total_30_invoiced += ($invoice->thirty_days * $invoice->rate);
                                $vendor_total_30_plus_invoiced += ($invoice->thirty_days_plus * $invoice->rate);
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">All invoices are paid</th>
                            </tr>
                        @endforelse
                        <tr>
                            <th colspan="4" class="text-right">Total</th>
                            <th class="text-right">{{number_format($vendor_total_5_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($vendor_total_10_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($vendor_total_20_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($vendor_total_30_invoiced,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($vendor_total_30_plus_invoiced,2,'.',',')}}</th>
                        </tr>
                        </tbody>
                    </table>
                    {{$vendor_invoiced->appends(request()->except('vendor_invoiced'))->links()}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Vendor Not invoiced - billable amount including expenses</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Vendor</th>
                            <th>Project</th>
                            <th>Resource</th>
                            <th>Yearwk</th>
                            <th class="text-right">Expenses (Claim)</th>
                            <th class="text-right">Labour</th>
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($vendor_not_invoiced as $invoice)
                            <tr>
                                <td>{{isset($invoice->user->vendor->vendor_name)?$invoice->user->vendor->vendor_name:null}}</td>
                                <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
                                <td>{{isset($invoice->user)?$invoice->user->first_name.' '.$invoice->user->last_name:null}}</td>
                                <td><a href="{{route('timeline.show', $invoice['id'])}}">{{$invoice['year_week']}}</a></td>
                                <td class="text-right">{{number_format((isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0),2,'.',',')}}</td>
                                <td class="text-right">{{number_format(($invoice->hours * $invoice->rate),2,'.',',')}}</td>
                                <td class="text-right">{{number_format((($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0)),2,'.',',')}}</td>
                            </tr>
                            @php
                                $vendor_total_expenses += (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0);
                                $vendor_total_labour += ($invoice->hours * $invoice->rate);
                                $vendor_total += (($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0));
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">All invoices are paid</th>
                            </tr>
                        @endforelse
                            <tr>
                                <th colspan="4" class="text-right">Total</th>
                                <th class="text-right">{{number_format($vendor_total_expenses,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($vendor_total_labour,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($vendor_total,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                    {{$vendor_not_invoiced->appends(request()->except('vendor_not_invoiced'))->links()}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Expense Tracking not linked to projects</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Account</th>
                            <th>Account Element</th>
                            <th>Resource</th>
                            <th>Yearwk</th>
                            <th class="text-right">Expenses (Claim)</th>
                            <th class="text-right">Expenses (Bill)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($expense_tracking as $expense)
                            <tr>
                                <td>{{($expense->account)?$expense->account->description:''}}</td>
                                <td>{{($expense->account_element)?$expense->account_element->description:''}}</td>
                                <td>{{($expense->resource)?substr($expense->resource->first_name,0,1).'. '.$expense->resource->last_name : ''}}</td>
                                <td><a href="{{route('exptracking.show', $expense)}}">{{$expense->yearwk??''}}</a></td>
                                <td class="text-right">{{number_format($expense->claim_expense,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->billable_expense,2,'.',',')}}</td>
                            </tr>
                            @php
                                $total_claim_expense += $expense->claim_expense;
                                $total_bill_expense += $expense->billable_expense;
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">No expenses are available.</th>
                            </tr>
                        @endforelse
                        <tr>
                            <th colspan="4" class="text-right">Total</th>
                            <th class="text-right">{{number_format($total_claim_expense,2,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_bill_expense,2,'.',',')}}</th>
                        </tr>
                        </tbody>
                    </table>
                    {{$expense_tracking->appends(request()->except('expense_tracking'))->links()}}
                </div>
            </div>

             <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Other Planned Expenses (next 60 days)</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Account</th>
                            <th>Account Element</th>
                            <th class="text-right">Total</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">30+</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($planned_expenses as $expense)
                            <tr>
                                <td>{{isset($expense->account->description)?$expense->account->description:null}}</td>
                                <td>{{isset($expense->account_element->description)?$expense->account_element->description:null}}</td>
                                <td class="text-right">{{number_format(($expense->five_days + $expense->ten_days + $expense->twenty_days + $expense->thirty_days + $expense->thirty_days_plus),2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->five_days,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->ten_days,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->twenty_days,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->thirty_days,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($expense->thirty_days_plus,2,'.',',')}}</td>
                            </tr>
                            @php
                                $planned_expense_5 += $expense->five_days;
                                $planned_expense_10 += $expense->ten_days;
                                $planned_expense_20 += $expense->twenty_days;
                                $planned_expense_30 += $expense->thirty_days;
                                $planned_expense_30_plus += $expense->thirty_days_plus;
                            @endphp
                        @empty
                            <tr>
                                <th colspan="9" class="text-center">No expenses are available.</th>
                            </tr>
                        @endforelse
                            <tr>
                                <th colspan="2" class="text-right">Total</th>
                                <th class="text-right">{{number_format(($planned_expense_5 + $planned_expense_10 + $planned_expense_20 + $planned_expense_30 + $planned_expense_30_plus),2,'.',',')}}</th>
                                <th class="text-right">{{number_format($planned_expense_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($planned_expense_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($planned_expense_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($planned_expense_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($planned_expense_30_plus,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                    {{$planned_expenses->appends(request()->except('planned_expense'))->links()}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Commission Payable (next 30 days)</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th>Description</th>
                            <th class="text-right">< 30 Days</th>
                            <th class="text-right">30+ Days</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Commission Last Month</td>
                                <td class="text-right">{{number_format($commission_last_month,2,'.',',')}}</td>
                                <td class="text-right">{{number_format(0,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td>Commission Month to Date</td>
                                <td class="text-right">{{number_format(0,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($commission_this_month,2,'.',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
