@extends('adminlte.default')

@section('title') Project Delivery Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('report.projectdelivery', request()->all())}}{{count(request()->all()) > 0 ? '&' : '?'}}export" class="btn btn-info ml-1" type="submit"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('report.projectdelivery'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />        
        <div class="row w-100">
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search ', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:16% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource', $resource_drop_down2, request()->resource , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:16% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', [0=>"No", 1=>"Yes"], request()->billable , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('epic', $epic_drop_down, request()->epic , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Epic</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row w-100">
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('feature', $feature_drop_down, request()->feature , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Feature</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:16% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('user_story', $user_stories_drop_down, request()->user_story , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>User Story</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('task',$task_drop_down, request()->task , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('invoice_status', $invoice_status_drop_down, request()->invoice_status , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:16% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_week', $weeks_drop_down, request()->from_week , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:17% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_week', $weeks_drop_down, request()->to_week , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row w-100">
            <div class="col-sm col-sm mt-2" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_month', $months_drop_down, request()->from_month , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_month',$months_drop_down, request()->to_month , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_drop_down, request()->cost_center , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:20% !important">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('task_delivery_type_id', $delivery_type, request()->task_delivery_type_id , ['class' => 'form-control search', 'placeholder' => 'All']) !!}
                        <span>Task Delivery Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm col-sm mt-2" style="max-width:20% !important">
                <a href="{{route('report.projectdelivery')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </div>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead class="bg-dark">
                    <tr>
                        <th class="align-top">Resource</th>
                        <th class="align-top">Epic</th>
                        <th class="align-top">Feature</th>
                        <th class="align-top">User Story</th>
                        <th class="align-top">Task</th>
                        <th class="align-top">Delivery Type</th>
                        <th class="align-top">Status</th>
                        <th class="align-top">Bill</th>
                        <th class="text-right align-top">Planned Hours</th>
                        <th class="text-right align-top">Actual Hours</th>
                        <th class="text-right align-top">Variance</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($project_progress as $progress)
                        <tr>
                            <td>{{$progress["resource"]??''}}</td>
                            <td>{{$progress["user_story"]->feature->epic->name??'No (task/user story/feature/epic)'}}</td>
                            <td>{{$progress["user_story"]->feature->name??'No (task/user story/feature)'}}</td>
                            <td>{{$progress["user_story"]->name??'No (task/user story)'}}</td>
                            <td>{{$progress["description"]??"No task"}}</td>
                            <td>{{$progress["delivery_type"]??"No delivery type"}}</td>
                            <td>{{$progress["taskstatus"]->description??'No task'}}</td>
                            <td>{{$progress["billable"]?"Yes":"No"}}</td>
                            <td class="text-right">{{$progress["planned_hours"]}}</td>
                            <td class="text-right">{{$progress["actual_hours"]}}</td>
                            <td class="text-right">{{$progress["variance"]}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="10">There's no projects to show</td>
                        </tr>
                    @endforelse
                    @if(!$project_progress->isEmpty())
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-uppercase text-right">total</th>
                            <th class="text-right">{{_minutes_to_time($project_progress->sum('planned_hours_raw'))}}</th>
                            <th class="text-right">{{_minutes_to_time($project_progress->sum('actual_hours_raw'))}}</th>
                            <th class="text-right">{{_minutes_to_time($project_progress->sum('variance_raw'))}}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '60'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $project_progress->firstItem() }} - {{ $project_progress->lastItem() }} of {{ $project_progress->total() }}
                        </td>
                        <td>
                            {{ $project_progress->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });
    </script>
@endsection
