@extends('adminlte.default')

@section('title')  Payroll Summary (R361) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="payroll.summary"></x-export>
    </div>
    <hr>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="col-sm-2 col-sm" style="max-width:15%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company',$company_drop_down,request()->company,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm" style="max-width:14%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource_type',$resource_type_drop_down,request()->resource_type,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Resource Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm" style="max-width:15%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource',$resource_drop_down,request()->resource,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm" style="max-width:14%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('team',$team_drop_down, request()->team,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm" style="max-width:14%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('leave',$year_month_drop_down,request()->leave,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Leave</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm" style="max-width:14%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('commission',$year_month_drop_down,request()->commission,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Commission</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-1" style="max-width:14%;">
                <a href="{{route('payroll.summary')}}"  class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead class="bg-dark">
                <tr>
                    <th>Name</th>
                    <th>Emp No.</th>
                    <th class="text-right">Open Balance</th>
                    <th class="text-right">Accrued</th>
                    <th class="text-right">Taken</th>
                    <th class="text-right">Closing Balance</th>
                    <th class="text-right">Sick Leave</th>
                    <th class="text-right">Unpaid Leave</th>
                    <th class="text-right">Other Leave</th>
                    <th class="text-right">Commission</th>
                    <th class="text-right">Expenses Not Approved</th>
                    <th class="text-right">Expenses Approved Not Paid</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($resources as $resource)
                        <tr>
                            <td><a href="/leave_statement?resource_id={{$resource->user->id}}&year_month={{ \Carbon\Carbon::now()->format('Ym'); }}">{{isset($resource->user)?$resource->user->first_name.' '.$resource->user->last_name:''}}</a></td>
                            <td>{{$resource->resource_no}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['opening_balance'],2)}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['accrued'],2)}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['taken'],2)}}</td>
                            <td class="text-right">{{round(($payroll_summary[$resource->user_id]['closing_balance']),2)}}</td>
                            <td class="text-right toolt {{$payroll_summary[$resource->user_id]['sick']['leave_taken']>$configs->sick_leave_days_per_cycle?'bg-danger font-weight-bold':''}}">{{number_format($payroll_summary[$resource->user_id]['sick']['leave_taken'],2,'.',',')}}<span class="tooltiptext">{{$payroll_summary[$resource->user_id]['sick']['sick_cycle_start']}} - {{$payroll_summary[$resource->user_id]['sick']['sick_cycle_end']}}</span></td>
                            <td class="text-right toolt {{$payroll_summary[$resource->user_id]['unpaid']['leave_taken']>0?'font-weight-bold':''}}">{{number_format($payroll_summary[$resource->user_id]['unpaid']['leave_taken'],2,'.',',')}}<span class="tooltiptext">{{$payroll_summary[$resource->user_id]['unpaid']['sick_cycle_start']}} - {{$payroll_summary[$resource->user_id]['unpaid']['sick_cycle_end']}}</span></td>
                            <td class="text-right toolt {{$payroll_summary[$resource->user_id]['other']['leave_taken']>0?'font-weight-bold':''}}">{{number_format($payroll_summary[$resource->user_id]['other']['leave_taken'],2,'.',',')}}<span class="tooltiptext">{{$payroll_summary[$resource->user_id]['other']['sick_cycle_start']}} - {{$payroll_summary[$resource->user_id]['other']['sick_cycle_end']}}</span></td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['commission'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['expense_claim_not_approved'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['expense_claim_approved'],2,'.',',')}}</td>
                            <td>
                                @if($payroll_summary[$resource->user_id]['expense_claim_approved'] && $payroll_summary[$resource->user_id]['expense_claim_not_approved'])
                                    <button class="btn btn-sm btn-info mr-2"><i class="fas fa-eye"></i> View</button>
                                @endif
                                @if($payroll_summary[$resource->user_id]['expense_claim_not_approved'])
                                        @php
                                            $time_exp = [];
                                            $exp_trac = [];
                                        @endphp

                                        <div class="dropdown d-inline">
                                            <button type="button" class="btn btn-sm btn-info mr-2 dropdown-toggle" data-toggle="dropdown">
                                                <i class="fas fa-eye"></i> View
                                            </button>
                                            <div class="dropdown-menu">
                                                @foreach($payroll_summary[$resource->user_id]['expenses'] as $expenses)
                                                    @if(isset($expenses['te']))
                                                        @foreach($expenses['te'] as $item)
                                                            @php $time_exp[] = $item->id @endphp
                                                            <a class="dropdown-item" href="{{route('timesheet.show', $item->id)}}">{{$item->description}} ({{$item->year_week}})</a>
                                                        @endforeach
                                                    @endif
                                                    @if(isset($expenses['et']))
                                                        @foreach($expenses['et'] as $item)
                                                            @php $exp_trac[] = $item->id @endphp
                                                                <a class="dropdown-item" href="{{route('exptracking.show', $item->id)}}">{{$item->description}} ({{$item->year_week}})</a>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                                    @if($payroll_summary[$resource->user_id]['expenses_approved'] == 5)
                                                        <div class="dropdown-divider"></div>
                                                        <a href="{{route('exptracking.index', ['resource' => $resource->user_id])}}" class="dropdown-item">More</a>
                                                    @endif
                                            </div>
                                        </div>
                                    <a href="{{route('payrollsummary.approve', ['te' => $time_exp, 'et' => $exp_trac])}}" class="btn btn-sm btn-warning mr-2"><i class="fas fa-thumbs-up"></i> Approve</a>
                                @endif
                                @if($payroll_summary[$resource->user_id]['expense_claim_approved'])
                                    @php
                                        $time_exp_approved = [];
                                        $exp_tracK_approved = [];
                                    @endphp
                                        <div class="dropdown d-inline">
                                            <button type="button" class="btn btn-sm btn-info mr-2 dropdown-toggle" data-toggle="dropdown">
                                                <i class="fas fa-eye"></i> View
                                            </button>
                                            <div class="dropdown-menu">
                                                @foreach($payroll_summary[$resource->user_id]['expenses_approved'] as $expenses)
                                                    @if(isset($expenses['te']))
                                                        @foreach($expenses['te'] as $item)
                                                            @php $time_exp_approved[] = $item->id @endphp
                                                            <a class="dropdown-item" href="{{route('timesheet.show', $item->id)}}">{{$item->description}} ({{$item->year_week}})</a>
                                                        @endforeach
                                                    @endif
                                                    @if(isset($expenses['et']))
                                                        @foreach($expenses['et'] as $item)
                                                            @php $exp_tracK_approved[] = $item->id @endphp
                                                            <a class="dropdown-item" href="{{route('exptracking.show', $item->id)}}">{{$item->description}} ({{$item->year_week}})</a>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                                @if($payroll_summary[$resource->user_id]['expenses_approved'] == 5)
                                                    <div class="dropdown-divider"></div>
                                                    <a href="{{route('exptracking.index', ['resource' => $resource->user_id])}}" class="dropdown-item">More</a>
                                                @endif
                                            </div>
                                        </div>
                                    <a href="{{route('payrollsummary.pay', ['te' => $time_exp_approved, 'et' => $exp_tracK_approved])}}" class="btn btn-sm btn-success mr-2"><i class="fas fa-money-bill-wave"></i> Pay</a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">Nothing to see here</td>
                        </tr>
                    @endforelse
                    <tr class="bg-gray">
                        <th colspan="9"></th>
                        <th class="text-right">{{number_format($total_commission,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_expense_not_approved,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_expense_approved,2,'.',',')}}</th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $resources->firstItem() }} - {{ $resources->lastItem() }} of {{ $resources->total() }}
                        </td>
                        <td>
            {{ $resources->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script src="{!! asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
