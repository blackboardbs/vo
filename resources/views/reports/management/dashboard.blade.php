@extends('adminlte.default')

@section('title')  Operational Dashboard for Blackboard Group @endsection

@section('header')
	<div class="container-fluid container-title">
		<h3>@yield('title')</h3>
		<div class="btn-toolbar float-right">
			<div class="btn-group mr-2">
				<button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
			</div>
		</div>
	</div>
@endsection

@section('content')
	<div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-6 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
						{!! Form::select('company', $company_dropdown, null , ['class' => 'form-control search w-100 ', 'placeholder' => 'Company']) !!}
						<span>Company</span>
					</label>
				</div>
			</div>
            <div class="col-sm-6 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
						{!! Form::select('billable', [1 => 'Yes', 0 => 'No'], null , ['class' => 'form-control search w-100 ', 'placeholder' => 'Billable']) !!}
						<span>Billable</span>
					</label>
				</div>
			</div>
		</form>
		<hr />
		<div class="table-responsive">
			<table class="table table-bordered table-sm mt-3">
				<thead>
				<tr class="bg-secondary">
					<th colspan="10">Assignments</th>
				</tr>
				<tr class="bg-dark">
					<th>Project Name</th>
					<th>Consultant</th>
					<th>Start</th>
					<th>End</th>
					<th class="text-right">Hours</th>
					<th class="text-right">Bill</th>
					<th class="text-right">N-Bill</th>
					<th class="text-right">Hrs Out</th>
					<th class="text-right">Comp%</th>
					<th class="text-right">R Out</th>
				</tr>
				</thead>
				<tbody>
					@forelse($assignments as $key => $assignment)
						@php

						@endphp
						<tr>
							<td><a href="{{route('assignment.show',$assignment->id)}}">{{isset($assignment->project)?$assignment->project->name:''}}</a></td>
							<td>{{isset($assignment->consultant)?$assignment->consultant->first_name.' '.$assignment->consultant->last_name:''}}</td>
							<td>{{$assignment->start_date}}</td>
							<td class="{{($assignment->end_date < now()->toDateString())?'bg-danger':''}}">{{$assignment->end_date}}</td>
							<td class="text-right">{{number_format($assignment->hours,0,'.',',')}}</td>
							<td class="text-right">{{number_format($assignment->billable_hours,0,'.',',')}}</td>
							<td class="text-right">{{number_format($assignment->non_billable_hours,0,'.',',')}}</td>
							<td class="text-right">{{($assignment->billable == 1)?number_format($assignment->hours - $assignment->billable_hours ,0,'.',','):number_format($assignment->hours - $assignment->non_billable_hours ,0,'.',',') }}</td>
							<td class="text-right {{($completion_perc[$key] > 0 && $completion_perc[$key] < 30)?'bg-success':(($completion_perc[$key] >= 30 && $completion_perc[$key] < 65)?'bg-warning':(($completion_perc[$key] >= 65)?'bg-danger':''))}}">{{ number_format($completion_perc[$key] ,0,'.',',')  }}%</td>
							<td class="text-right">{{number_format($outstanding_amount[$key] ,2,'.',',')}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="10" class="text-center">No Open Assignments</td>
						</tr>
					@endforelse
					<tr>
						<td class="text-center bg-success"><30%</td>
						<td class="text-center bg-warning">>30% 65%<</td>
						<td class="text-center bg-danger">>65%</td>
						<td colspan="6" class="text-right">Total Assignment Value Outstanding</td>
						<th class="text-right">{{number_format($total_outstanding_amount ,2,'.',',')}}</th>
					</tr>
				</tbody>
			</table>
			{{$assignments->appends(request()->except('open_assignments'))->links()}}

			<table class="table table-bordered table-sm mt-3" id="utilization">
				<thead>
					<tr class="bg-secondary">
						<th colspan="6">Income</th>
					</tr>
					<tr>
						<th>Consultant</th>
						<th class="text-center">Week-3</th>
						<th class="text-center">Week-2</th>
						<th class="text-center">Week-1</th>
						<th class="text-center">Ave 3 Week<br> Util %</th>
						<th class="text-center">This Week</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<th></th>
					<th class="text-center {{(isset($weeks['planned3']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week3']:'000000'}}</th>
					<th class="text-center {{(isset($weeks['planned2']))?'':'bg-danger'}}">{{isset($weeks['week2'])?$weeks['week2']:'000000'}}</th>
					<th class="text-center {{(isset($weeks['planned1']))?'':'bg-danger'}}">{{isset($weeks['week1'])?$weeks['week1']:'000000'}}</th>
					<th></th>
					<th class="text-center {{(isset($weeks['planned']))?'':'bg-danger'}}">{{isset($weeks['week1'])?$weeks['week1']:'000000'}}</th>
				</tr>
				@forelse($income_utilization as $utilization)
					<tr>
						<td>{{$utilization["resource"]}}</td>
						<td class="text-center">{{isset($utilization["week3"])?number_format($utilization["week3"],0,'.',','):0}}</td>
						<td class="text-center">{{isset($utilization["week2"])?number_format($utilization["week2"],0,'.',','):0}}</td>
						<td class="text-center">{{isset($utilization["week1"])?number_format($utilization["week1"],0,'.',','):0}}</td>
						<td class="text-center {{(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 50)?'bg-danger':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 50 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 65)?'bg-warning':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 65 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 80)?'bg-yellow':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100),0,'.',',')}}%</td>
						<td class="text-center">{{number_format((isset($utilization["week"])?$utilization["week"]:0),0,'.',',')}}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6" class="text-center">No Results</td>
					</tr>
				@endforelse
				<tr>
					<th>Total Hours</th>
					<th class="text-center">{{isset($total_income["total_week_3"])?$total_income["total_week_3"]:0}}</th>
					<th class="text-center">{{isset($total_income["total_week_2"])?$total_income["total_week_2"]:0}}</th>
					<th class="text-center">{{isset($total_income["total_week_1"])?$total_income["total_week_1"]:0}}</th>
					<th class="text-center">{{ (isset($total_income["total_week_3"])?$total_income["total_week_3"]:0) + (isset($total_income["total_week_2"])?$total_income["total_week_2"]:0) + (isset($total_income["total_week_1"])?$total_income["total_week_1"]:0) }}</th>
					<th class="text-center">{{isset($total_income["total_week"])?$total_income["total_week"]:0}}</th>
				</tr>
				<tr>
					<th>Total Util %</th>
					<th class="text-center {{(((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) < 50)?'bg-danger':((((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) >= 50 && ((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) < 65)?'bg-warning':((((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) >= 65 && ((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) < 80)?'bg-yellow':((((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_income["percentage_week3"])?$total_income["percentage_week3"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) < 50)?'bg-danger':((((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) >= 50 && ((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) < 65)?'bg-warning':((((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) >= 65 && ((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) < 80)?'bg-yellow':((((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_income["percentage_week2"])?$total_income["percentage_week2"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) < 50)?'bg-danger':((((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) >= 50 && ((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) < 65)?'bg-warning':((((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) >= 65 && ((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) < 80)?'bg-yellow':((((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_income["percentage_week1"])?$total_income["percentage_week1"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{((isset($total_income["total_percentage"])?$total_income["total_percentage"]:0) < 50)?'bg-danger':(((isset($total_income["total_percentage"])?$total_income["total_percentage"]:0) >= 50 && (isset($total_income["total_percentage"])?$total_income["total_percentage"]:0) < 65)?'bg-warning':(((isset($total_income["total_percentage"])?$total_income["total_percentage"]:0) >= 65 && $total_income["total_percentage"] < 80)?'bg-yellow':(((isset($total_income["total_percentage"])?$total_income["total_percentage"]:0) >= 80)?'bg-success':'')))}}">{{number_format((isset($total_income["total_percentage"])?$total_income["total_percentage"]:0),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 50)?'bg-danger':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) >= 50 && ((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 65)?'bg-warning':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) >= 65 && ((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 80)?'bg-yellow':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100),0,'.',',')}}%</th>
				</tr>
				<tr>
					<td colspan="6" class="text-danger">{{(isset($weeks["planned3"]) && isset($weeks["planned2"]) && isset($weeks["planned1"]) && isset($weeks["planned"]))?'':'Weeks in red are not planned for'}}</td>
				</tr>
				<tr>
					<td class="text-center bg-danger"><50</td>
					<td class="text-center bg-warning"> >50 65< </td>
					<td class="text-center bg-yellow"> >65 80< </td>
					<td class="text-center bg-success"> >80 </td>
					<td class="text-right" colspan="2">Utilization is calculated at {{isset($weeks["week_hours"])?$weeks["week_hours"]->wk_hours:40}} hours per resource per week</td>
				</tr>
				</tbody>
			</table>

			<table class="table table-bordered table-sm mt-3">
				<thead>
				<tr class="bg-secondary">
					<th colspan="6">Cost</th>
				</tr>
				<tr>
					<th>Consultant</th>
					<th class="text-center">Week-3</th>
					<th class="text-center">Week-2</th>
					<th class="text-center">Week-1</th>
					<th class="text-center">Ave 3 Week<br> Util %</th>
					<th class="text-center">This Week</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th></th>
					<th class="text-center {{($weeks['planned3'])?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week3']:'000000'}}</th>
					<th class="text-center {{($weeks['planned2'])?'':'bg-danger'}}">{{isset($weeks['week2'])?$weeks['week2']:'000000'}}</th>
					<th class="text-center {{($weeks['planned1'])?'':'bg-danger'}}">{{isset($weeks['week1'])?$weeks['week1']:'000000'}}</th>
					<th></th>
					<th class="text-center {{($weeks['planned'])?'':'bg-danger'}}">{{isset($weeks['week'])?$weeks['week']:'000000'}}</th>
				</tr>
				@forelse($cost_utilization as $utilization)
					<tr>
						<td>{{isset($utilization["resource"])?$utilization["resource"]:''}}</td>
						<td class="text-center">{{number_format(isset($utilization["week3"])?$utilization["week3"]:0)}}</td>
						<td class="text-center">{{number_format(isset($utilization["week2"])?$utilization["week2"]:0)}}</td>
						<td class="text-center">{{number_format(isset($utilization["week1"])?$utilization["week1"]:0)}}</td>
						<td class="text-center {{(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 50)?'bg-danger':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 50 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 65)?'bg-warning':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 65 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 80)?'bg-yellow':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100))}}%</td>
						<td class="text-center">{{number_format(isset($utilization["week"])?$utilization["week"]:0)}}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6" class="text-center">No Results</td>
					</tr>
				@endforelse
				<tr>
					<th>Total Hours</th>
					<th class="text-center">{{number_format(isset($total_cost["total_week_3"])?$total_cost["total_week_3"]:0)}}</th>
					<th class="text-center">{{number_format(isset($total_cost["total_week_2"])?$total_cost["total_week_2"]:0)}}</th>
					<th class="text-center">{{number_format(isset($total_cost["total_week_1"])?$total_cost["total_week_1"]:0)}}</th>
					<th class="text-center">{{ number_format(((isset($total_cost["total_week_3"])?$total_cost["total_week_3"]:0) + (isset($total_cost["total_week_2"])?$total_cost["total_week_2"]:0) + (isset($total_cost["total_week_1"])?$total_cost["total_week_1"]:0))) }}</th>
					<th class="text-center">{{number_format(isset($total_cost["total_week"])?$total_cost["total_week"]:0)}}</th>
				</tr>
				<tr>
					<th>Total Utilization %</th>
					<th class="text-center {{(((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100),0,'.',',')}}%</th>
					<th class="text-center {{((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 50)?'bg-danger':(((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) >= 50 && (isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 65)?'bg-warning':(($total_cost["total_percentage"] >= 65 && (isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 80)?'bg-yellow':(((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) >= 80)?'bg-success':'')))}}">{{number_format((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0),0,'.',',')}}%</th>
					<th class="text-center {{(((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100),0,'.',',')}}%</th>
				</tr>
				<tr>
					<td colspan="6" class="text-danger">{{($weeks["planned3"] && $weeks["planned2"] && $weeks["planned1"] && $weeks["planned"])?'':'Weeks in red are not planned for'}}</td>
				</tr>
				<tr>
					<td class="text-center bg-danger"><50</td>
					<td class="text-center bg-warning"> >50 65< </td>
					<td class="text-center bg-yellow"> >65 80< </td>
					<td class="text-center bg-success"> >80 </td>
					<td class="text-right" colspan="2">Utilization is calculated at {{isset($weeks["week_hours"])?$weeks["week_hours"]->cost_wk_hours:40}} hours per resource per week</td>
				</tr>
				</tbody>
			</table>

			<div class="row mt-5">
				<div class="col-lg-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Income Projects Weekly Utilization</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>

							</div>
						</div>
						<div class="card-body">
							<div class="chart">
								<canvas id="weekly_income_utilization" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-warning">
						<div class="card-header">
							<h3 class="card-title">Cost Projects Weekly Utilization</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>

							</div>
						</div>
						<div class="card-body">
							<div class="chart">
								<canvas id="weekly_cost_utilization" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
				</div>
			</div>

			{{--<table class="table table-bordered table-sm mt-3">
				<thead>
					<tr class="bg-dark">
						<th>Cashflow</th>
						<th class="text-center">Total</th>
						<th class="text-center">5</th>
						<th class="text-center">10</th>
						<th class="text-center">20</th>
						<th class="text-center">30</th>
						<th class="text-center">30+</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Invoiced</td>
						<td class="text-right">{{number_format($total_invoiced,2,'.',',')}}</td>
						<td class="text-right">{{number_format($due_in_5_days,2,'.',',')}}</td>
						<td class="text-right">{{number_format($due_in_10_days,2,'.',',')}}</td>
						<td class="text-right">{{number_format($due_in_20_days,2,'.',',')}}</td>
						<td class="text-right">{{number_format($due_in_30_days,2,'.',',')}}</td>
						<td class="text-right">{{number_format($due_in_30p_days,2,'.',',')}}</td>
					</tr>
					<tr>
						<td>Not Invoice (Inc. Exp)</td>
						<td class="text-right">{{number_format($not_invoiced,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0,2,'.',',')}}</td>
					</tr>
					<tr>
						<td>Planned Expenses (30 days)</td>
						<td class="text-right">{{number_format($total_planned_expenses,2,'.',',')}}</td>
						<td class="text-right">{{number_format($planned_expenses_5,2,'.',',')}}</td>
						<td class="text-right">{{number_format($planned_expenses_10,2,'.',',')}}</td>
						<td class="text-right">{{number_format($planned_expenses_20,2,'.',',')}}</td>
						<td class="text-right">{{number_format($planned_expenses_30,2,'.',',')}}</td>
						<td class="text-right">{{number_format($planned_expenses_30p,2,'.',',')}}</td>
					</tr>
					@php
						$nett = ($total_invoiced + $not_invoiced) - $total_planned_expenses;
						$nett5 = $due_in_5_days - $planned_expenses_5;
						$nett10 = $due_in_10_days - $planned_expenses_10;
						$nett20 = $due_in_20_days - $planned_expenses_20;
						$nett30 = $due_in_30_days - $planned_expenses_30;
						$nett30p = $due_in_30p_days - $planned_expenses_30p;
					@endphp
					<tr>
						<td>Nett Cashflow</td>
						<td class="text-right {{($nett < 0)?'bg-danger':'bg-success'}}">{{number_format($nett ,2,'.',',')}}</td>
						<td class="text-right {{($nett5 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett5 ,2,'.',',')}}</td>
						<td class="text-right {{($nett10 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett10,2,'.',',')}}</td>
						<td class="text-right {{($nett20 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett20,2,'.',',')}}</td>
						<td class="text-right {{($nett30 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett30,2,'.',',')}}</td>
						<td class="text-right {{($nett30p < 0)?'bg-danger':'bg-success'}}">{{number_format($nett30p,2,'.',',')}}</td>
					</tr>
				</tbody>
			</table>
--}}
			<br>

			<table class="table table-bordered table-sm mt-3">
				<thead>
					<tr class="bg-dark">
						<th>Debtor Ageing</th>
						<th class="text-right">Total</th>
						<th class="text-right">30</th>
						<th class="text-right">60</th>
						<th class="text-right">90</th>
						<th class="text-right">90+</th>
					</tr>
				</thead>
				<tbody>
					@forelse($debtor_ageing as  $debtor)
						<tr>
							<td><a href="{{route('customer.invoice', ['customer' => $debtor["customer_id"], 'invoice_status' => 1])}}">{{isset($debtor["customer"])?$debtor["customer"]:''}}</a></td>
							<td class="text-right">{{number_format((
								(isset($debtor["total_due_30"])?$debtor["total_due_30"]:0) +
								(isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
								(isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
								(isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
								),2,'.',',')}}</td>
							<td class="text-right">{{number_format((isset($debtor["total_due_30"])?$debtor["total_due_30"]:0),2,'.',',')}}</td>
							<td class="text-right">{{number_format((isset($debtor["total_due_60"])?$debtor["total_due_60"]:0),2,'.',',')}}</td>
							<td class="text-right">{{number_format((isset($debtor["total_due_90"])?$debtor["total_due_90"]:0),2,'.',',')}}</td>
							<td class="text-right">{{number_format((isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0),2,'.',',')}}</td>
						</tr>
						@php
							$grand_total_debtor += (
								(isset($debtor["total_due_30"])?$debtor["total_due_30"]:0) +
								(isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
								(isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
								(isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
								);
							$grand_total_30_debtor += (isset($debtor["total_due_30"])?$debtor["total_due_30"]:0);
							$grand_total_60_debtor += (isset($debtor["total_due_60"])?$debtor["total_due_60"]:0);
							$grand_total_90_debtor += (isset($debtor["total_due_90"])?$debtor["total_due_90"]:0);
							$grand_total_90_plus_debtor += (isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0);
						@endphp
						@empty
					@endforelse
					<tr class="bg-gray-light">
						<th>TOTAL</th>
						<th class="text-right">{{number_format($grand_total_debtor, 2,'.',',')}}</th>
						<th class="text-right">{{number_format($grand_total_30_debtor ,2,'.',',')}}</th>
						<th class="text-right">{{number_format($grand_total_60_debtor, 2,'.',',')}}</th>
						<th class="text-right">{{number_format($grand_total_90_debtor, 2,'.',',')}}</th>
						<th class="text-right">{{number_format($grand_total_90_plus_debtor, 2,'.',',')}}</th>
					</tr>
				</tbody>
			</table>
			{{--{{$debtor_ageing->links()}}--}}

			<br>

			<table class="table table-bordered table-sm mt-3">
				<thead>
				<tr class="bg-dark">
					<th>Creditors Ageing</th>
					<th class="text-right">Total</th>
					<th class="text-right">30</th>
					<th class="text-right">60</th>
					<th class="text-right">90</th>
					<th class="text-right">90+</th>
				</tr>
				</thead>
				<tbody>
				@forelse($creditors_ageing as  $creditor)
					<tr>
						<td><a href="{{route('vendor.invoice', ['customer' => $creditor["customer_id"], 'invoice_status' => 1])}}">{{isset($creditor["customer"])?$creditor["customer"]:''}}</a></td>
						<td class="text-right">{{number_format((
								(isset($creditor["total_due_30"])?$creditor["total_due_30"]:0) +
								(isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
								(isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
								(isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
								),2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($creditor["total_due_30"])?$creditor["total_due_30"]:0),2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($creditor["total_due_60"])?$creditor["total_due_60"]:0),2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($creditor["total_due_90"])?$creditor["total_due_90"]:0),2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0),2,'.',',')}}</td>
					</tr>
					@php
						$grand_total_creditor += (
                            (isset($creditor["total_due_30"])?$creditor["total_due_30"]:0) +
                            (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
                            (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
                            (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
                            );
                        $grand_total_30_creditor += (isset($creditor["total_due_30"])?$creditor["total_due_30"]:0);
                        $grand_total_60_creditor += (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0);
                        $grand_total_90_creditor += (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0);
                        $grand_total_90_plus_creditor += (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0);
					@endphp
				@empty
				@endforelse
				<tr class="bg-gray-light">
					<th>TOTAL</th>
					<th class="text-right">{{number_format($grand_total_creditor, 2,'.',',')}}</th>
					<th class="text-right">{{number_format($grand_total_30_creditor ,2,'.',',')}}</th>
					<th class="text-right">{{number_format($grand_total_60_creditor, 2,'.',',')}}</th>
					<th class="text-right">{{number_format($grand_total_90_creditor, 2,'.',',')}}</th>
					<th class="text-right">{{number_format($grand_total_90_plus_creditor, 2,'.',',')}}</th>
				</tr>
				</tbody>
			</table>

			<br>

			<table class="table table-bordered table-sm mt-3">
				<thead>
					<tr class="bg-dark">
						<th>Planning & Pipeline</th>
						<th>Solution</th>
						<th class="text-right">Hours</th>
						<th class="text-right">Value</th>
						<th>Resources</th>
						<th class="text-right">Start</th>
						<th class="text-center">% Chance</th>
					</tr>
				</thead>
				<tbody>
					@forelse($pipelines as $pipeline)
						<tr>
							<td><a href="{{route('prospects.show', $pipeline)}}">{{$pipeline->prospect_name}}</a></td>
							<td>{{$pipeline->solution}}</td>
							<td class="text-right">{{number_format($pipeline->hours,0,'.',',')}}</td>
							<td class="text-right">{{number_format($pipeline->value,2,'.',',')}}</td>
							<td>{{$pipeline->resources}}</td>
							<td class="text-right">{{$pipeline->est_start_date}}</td>
							<td class="text-center {{($pipeline->chance >= 8)?'bg-success':(($pipeline->chance > 5 && $pipeline->chance < 8)?'bg-warning':(($pipeline->chance < 5)?'bg-danger':''))}}">{{$pipeline->chance * 10}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="7" class="text-center">No projects in the pipeline</td>
						</tr>
					@endforelse
				</tbody>
			</table>

			<br>

			<table class="table table-bordered table-sm mt-3">
				<thead>
					<tr class="bg-dark">
						<th>Planned Income and Expenses</th>
						<th class="text-right">Total</th>
						<th class="text-right">5</th>
						<th class="text-right">10</th>
						<th class="text-right">20</th>
						<th class="text-right">30</th>
						<th class="text-right">30+</th>
					</tr>
				</thead>
				<tbody>
					<tr class="bg-secondary">
						<th colspan="7" class="text-center">Income</th>
					</tr>
					<tr>
						<td class="pl-2">Customer Invoices (Incl Tax)</td>
						<td class="text-right">{{number_format(($planned_income["five_days"] + $planned_income["ten_days"] + $planned_income["twenty_days"] + $planned_income["thirty_days"] + $planned_income["thirty_days_plus"]) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["five_days"])?($planned_income["five_days"] + $planned_income["five_days_tax"]):0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["ten_days"])?($planned_income["ten_days"] + $planned_income["ten_days_tax"]):0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["twenty_days"])?($planned_income["twenty_days"] + $planned_income["twenty_days_tax"]):0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["thirty_days"])?($planned_income["thirty_days"] + $planned_income["thirty_days_tax"]):0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["thirty_days_plus"])?($planned_income["thirty_days_plus"] + $planned_income["thirty_days_plus_tax"]):0) ,2,'.',',')}}</td>
					</tr>
					<tr>
						<td class="pl-2">Customers Not Invoiced</td>
						<td class="text-right">{{number_format(($planned_income_not_invoiced),2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(($planned_income_not_invoiced) ,2,'.',',')}}</td>
					</tr>
					<tr class="bg-secondary">
						<th colspan="7" class="text-center">Expenses</th>
					</tr>
					<tr>
						<td class="pl-2">Tax - VAT</td>
						<td class="text-right">{{number_format((($planned_income["five_days_tax"] + $planned_income["ten_days_tax"] + $planned_income["twenty_days_tax"] + $planned_income["thirty_days_tax"] + $planned_income["thirty_days_plus_tax"])) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["five_days_tax"])?$planned_income["five_days_tax"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["ten_days_tax"])?$planned_income["ten_days_tax"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["twenty_days_tax"])?$planned_income["twenty_days_tax"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["thirty_days_tax"])?$planned_income["thirty_days_tax"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($planned_income["thirty_days_plus_tax"])?$planned_income["thirty_days_plus_tax"]:0) ,2,'.',',')}}</td>
					</tr>
					<tr>
						<td class="pl-2">Vendor Invoices (Incl Tax)</td>
						@php $total_vendor = (($invoiced_vendor["five_days"] + $invoiced_vendor["five_days_tax"]) + ($invoiced_vendor["ten_days"] + $invoiced_vendor["ten_days_tax"]) + ($invoiced_vendor["twenty_days"] + $invoiced_vendor["twenty_days_tax"]) + ($invoiced_vendor["thirty_days"] + $invoiced_vendor["thirty_days_tax"]) + ($invoiced_vendor["thirty_days_plus"] + $invoiced_vendor["thirty_days_plus_tax"])); @endphp
						<td class="text-right">{{number_format($total_vendor,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($invoiced_vendor["five_days"])?$invoiced_vendor["five_days"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($invoiced_vendor["ten_days"])?$invoiced_vendor["ten_days"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($invoiced_vendor["twenty_days"])?$invoiced_vendor["twenty_days"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($invoiced_vendor["thirty_days"])?$invoiced_vendor["thirty_days"]:0) ,2,'.',',')}}</td>
						<td class="text-right">{{number_format((isset($invoiced_vendor["thirty_days_plus"])?$invoiced_vendor["thirty_days_plus"]:0) ,2,'.',',')}}</td>
					</tr>
					<tr>
						<td class="pl-2">Vendor Not Invoiced</td>
						<td class="text-right">{{number_format($not_invoiced_vendor ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format(0 ,2,'.',',')}}</td>
						<td class="text-right">{{number_format($not_invoiced_vendor ,2,'.',',')}}</td>
					</tr>
					<tr>
						<th colspan="7">Planned Expenses</th>
					</tr>
					@forelse($planned_expenses as $expenses)
						<tr>
							<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{{(isset($expenses->account)?$expenses->account->description:null)}}</td>
							<td class="text-right">{{number_format(($expenses->five_days + $expenses->ten_days + $expenses->twenty_days + $expenses->thirty_days + $expenses->thirty_days_plus),2,'.',',')}}</td>
							<td class="text-right">{{number_format($expenses->five_days,2,'.',',')}}</td>
							<td class="text-right">{{number_format($expenses->ten_days,2,'.',',')}}</td>
							<td class="text-right">{{number_format($expenses->twenty_days,2,'.',',')}}</td>
							<td class="text-right">{{number_format($expenses->thirty_days,2,'.',',')}}</td>
							<td class="text-right">{{number_format($expenses->thirty_days_plus,2,'.',',')}}</td>
						</tr>
					@empty
					@endforelse
					<tr>
						<th colspan="7">Expense Claims</th>
					</tr>
					@forelse($expense_claim as $claim)
						<tr>
							<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{{isset($claim->account)?$claim->account->description:''}}</td>
							<td class="text-right">{{number_format($claim->amount,2,'.',',')}}</td>
							<td class="text-right">{{number_format($claim->amount,2,'.',',')}}</td>
							<td class="text-right">0.00</td>
							<td class="text-right">0.00</td>
							<td class="text-right">0.00</td>
							<td class="text-right">0.00</td>
						</tr>
						@php
							$total_month_plus += $claim->amount;
						@endphp
						@empty
					@endforelse	
						{{--<tr>
							<th class="pl-2">Total</th>
							<th class="text-right">{{number_format($total_this_month,2,'.',',')}}</th>
							<th class="text-right">{{number_format($total_month_plus,2,'.',',')}}</th>
							<th class="text-right">{{number_format($total_month_plus_2,2,'.',',')}}</th>
							<th class="text-right">{{number_format($total_month_plus_3,2,'.',',')}}</th>
						</tr>--}}
					<tr>
						<th class="pl-2">Profit/(Loss)</th>

						<th class="text-right">{{number_format(
    							(($planned_income["five_days"] + $planned_income["ten_days"] + $planned_income["twenty_days"] + $planned_income["thirty_days"] + $planned_income["thirty_days_plus"]) + $planned_income_not_invoiced) - ($total_vendor + $expense_claim->sum('amount') + ((isset($expenses->five_days)?$expenses->five_days:0) + (isset($expenses->ten_days)?$expenses->ten_days:0) + (isset($expenses->twenty_days)?$expenses->twenty_days:0) + (isset($expenses->thirty_days)?$expenses->thirty_days:0) + (isset($expenses->thirty_days_plus)?$expenses->thirty_days_plus:0)))
							,2 , '.', ',')}}
						</th>
						<th class="text-right">{{number_format(($planned_income["five_days"] - (($invoiced_vendor["five_days"] + $invoiced_vendor["five_days_tax"]) + $planned_expenses->sum('five_days') + $expense_claim->sum('amount'))),2,'.',',')}}</th>
						<th class="text-right">{{number_format(($planned_income["ten_days"] - (($invoiced_vendor["ten_days"] + $invoiced_vendor["ten_days_tax"]) + $planned_expenses->sum('ten_days') + $expense_claim->sum('amount'))),2, '.',',')}}</th>
						<th class="text-right">{{number_format(($planned_income["twenty_days"] - (($invoiced_vendor["twenty_days"] + $invoiced_vendor["twenty_days_tax"]) + $planned_expenses->sum('twenty_days') + $expense_claim->sum('amount'))),2, '.', ',')}}</th>
						<th class="text-right">{{number_format(($planned_income["thirty_days"] - (($invoiced_vendor["thirty_days"] + $invoiced_vendor["thirty_days_tax"]) + $planned_expenses->sum('thirty_days') + $expense_claim->sum('amount'))),2, '.', ',')}}</th>
						<th class="text-right">{{number_format((($planned_income["thirty_days_plus"] + $planned_income_not_invoiced) - (($invoiced_vendor["thirty_days_plus"] + $invoiced_vendor["thirty_days_plus_tax"]) + $not_invoiced_vendor + $planned_expenses->sum('thirty_days_plus') + $expense_claim->sum('amount'))),2, '.', ',')}}</th>
					</tr>
				</tbody>
			</table>
			<br>

		</div>

		<div class="row mt-5">
			<div class="col-md-6">
				<div class="card card-secondary">
					<div class="card-header">
						<h3 class="card-title">Top 10 Customers</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>

						</div>
					</div>
					<div class="card-body">
						<div class="chart">
							<canvas id="top_10_customers" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-sm">
					<thead>
						<tr class="bg-dark">
							<th>Top 10 Customers (12 months)</th>
							<th class="text-right">Hours</th>
							<th class="text-right">Value</th>
							<th class="text-center">Ave Rate</th>
						</tr>
					</thead>
					<tbody>
						@forelse($top_10_customers as $customer)
							<tr>
								<td>{{isset($customer->customer)?$customer->customer->customer_name:''}}</td>
								<td class="text-right">{{number_format($customer->hours,0,'.',',')}}</td>
								<td class="text-right">{{number_format($customer->value,2,'.',',')}}</td>
								<td class="text-center">{{($customer->hours != 0 && $customer->value != 0)?number_format($customer->value/$customer->hours,0,'.',','):0}}</td>
							</tr>
							@empty
								<tr>
									<td colspan="4" class="text-center">No Customers</td>
								</tr>
						@endforelse
					</tbody>
				</table>
			</div>

			<div class="col-md-6">
				<table class="table table-bordered table-sm">
					<thead>
					<tr class="bg-dark">
						<th>Top 10 Solutions (12 months)</th>
						<th class="text-right">Hours</th>
						<th class="text-right">Value</th>
						<th class="text-center">Ave Rate</th>
					</tr>
					</thead>
					<tbody>
					@forelse($top_10_solutions as $solution)
						<tr>
							<td>{{isset($solution->system_desc)?$solution->system_desc->description:''}}</td>
							<td class="text-right">{{number_format($solution->hours,0,'.',',')}}</td>
							<td class="text-right">{{number_format($solution->value,2,'.',',')}}</td>
							<td class="text-center">{{($solution->hours != 0 && $solution->value != 0)?number_format($solution->value/$solution->hours,0,'.',','):0}}</td>
						</tr>
					@empty
						<tr>
							<td colspan="4" class="text-center">No Customers</td>
						</tr>
					@endforelse
					</tbody>
				</table>
			</div>

			<div class="col-md-6">
				<div class="card card-secondary">
					<div class="card-header">
						<h3 class="card-title">Top 10 Solutions</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>

						</div>
					</div>
					<div class="card-body">
						<div class="chart">
							<canvas id="top_10_solutions" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
			</div>

			<div class="col-md-6">
				<div class="card card-secondary">
					<div class="card-header">
						<h3 class="card-title">Top 10 Locations</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>

						</div>
					</div>
					<div class="card-body">
						<div class="chart">
							<canvas id="top_10_locationss" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-sm">
					<thead>
					<tr class="bg-dark">
						<th>Top 10 Locations (12 months)</th>
						<th class="text-right">Hours</th>
						<th class="text-right">Value</th>
						<th class="text-center">Ave Rate</th>
					</tr>
					</thead>
					<tbody>
					@forelse($top_10_locations as $location)
						<tr>
							<td>{{isset($location->location)?$location->location->name:''}}</td>
							<td class="text-right">{{number_format($location->hours,0,'.',',')}}</td>
							<td class="text-right">{{number_format($location->value,2,'.',',')}}</td>
							<td class="text-center">{{($location->hours != 0 && $location->value != 0)?number_format($location->value/$location->hours,0,'.',','):0}}</td>
						</tr>
					@empty
						<tr>
							<td colspan="4" class="text-center">No Customers</td>
						</tr>
					@endforelse
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12">
			<div class="card card-secondary">
				<div class="card-header">
					<h3 class="card-title">Monthly Consulting</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>

					</div>
				</div>
				<div class="card-body">
					<div class="chart">
						<canvas id="consulting_canvas" style="height: 450px; width: 572px;" width="715" height="357"></canvas>
					</div>
				</div>
				<!-- /.card-body -->
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="card card-secondary">
					<div class="card-header">
						<h3 class="card-title">Year-to-Date Consulting</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>

						</div>
					</div>
					<div class="card-body">
						<div class="chart">
							<canvas id="year_to_date" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-sm">
					<thead>
					<tr class="bg-dark">
						<th>Year To Date</th>
						<th class="text-right">Hours</th>
						<th class="text-right">Value</th>
						<th class="text-right">% Change from Last Year</th>
						<th class="text-center">Ave Rate</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>{{$year_to_date_1->year??'0000'}}</td>
						<td class="text-right">{{number_format(($year_to_date_1->hours??0),0,'.',',')}}</td>
						<td class="text-right">{{number_format(($year_to_date_1->value??0),2,'.',',')}}</td>
						<td class="text-right">{{number_format($change_from_last_year,0,'.',',')}}%</td>
						<td class="text-center">{{number_format($avg_rate,0,'.',',')}}</td>
					</tr>
					<tr>
						<td>{{$year_to_date_2->year??'0000'}}</td>
						<td class="text-right">{{number_format(($year_to_date_2->hours??0),0,'.',',')}}</td>
						<td class="text-right">{{number_format(($year_to_date_2->value??0),2,'.',',')}}</td>
						<td class="text-right">{{number_format($change_from_last_year_1,0,'.',',')}}%</td>
						<td class="text-center">{{number_format($avg_rate_1,0,'.',',')}}</td>
					</tr>
					<tr>
						<td>{{$year_to_date_3->year??'0000'}}</td>
						<td class="text-right">{{number_format(($year_to_date_3->hours??0),0,'.',',')}}</td>
						<td class="text-right">{{number_format(($year_to_date_3->value??0),2,'.',',')}}</td>
						<td class="text-right">{{number_format($change_from_last_year_2,0,'.',',')}}%</td>
						<td class="text-center">{{number_format($avg_rate_2,0,'.',',')}}</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
@endsection
@section('extra-css')
	<link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
	<style>
		.dots_table td p.n{
			margin: 0;
			padding: 0em;
			size: auto;
			line-height: 0em;
			font-size: 0.6em;
		}
		.bg-yellow{
			background-color: yellow!important;
		}
	</style>
@endsection
@section('extra-js')
	<script src="{{asset('chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
	<script src="{{asset('adminlte/plugins/chart.js/Chart.min.js')}}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
	<script>
		$(document).ready(function () {
			$("#assignment_filter select").on('change', function () {
				$("#assignment_filter").submit();
			})

			$(function () {
				var weekly_income_utilization = document.getElementById("weekly_income_utilization");
				if (weekly_income_utilization){
					new Chart(weekly_income_utilization, {
						type: 'bar',
						data: {
							labels: [@forelse($weekly_income_utilization as $income_uti) "{{$income_uti->week}}",@empty @endforelse],
							datasets: [{
								label: '',
								data: [@forelse($weekly_income_utilization as $income_uti_hours) "{{ $income_uti_hours->hours }}", @empty @endforelse],
								backgroundColor: 'rgba(0, 123, 255,.8)',
								borderColor: 'rgba(0, 123, 255,.8)',
							}]
						},
						options: {
							maintainAspectRatio: false,
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero:true
									},
									gridLines: {
										display:false
									}
								}],
								xAxes: [{
									gridLines: {
										display:false
									},
									ticks: {
										autoSkip: false
									}
								}]
							},
							legend: {
								display: false,
							}
						}
					});
				}
			})

			$(function () {
				var weekly_cost_utilization = document.getElementById("weekly_cost_utilization");
				if (weekly_cost_utilization){
					new Chart(weekly_cost_utilization, {
						type: 'bar',
						data: {
							labels: [@forelse($weekly_cost_utilization as $cost_uti) "{{$cost_uti->week}}",@empty @endforelse],
							datasets: [{
								label: '',
								data: [@forelse($weekly_cost_utilization as $cost_uti_hours) "{{ $cost_uti_hours->hours }}", @empty @endforelse],
								backgroundColor: 'rgba(255, 193, 7,.8)',
								borderColor: 'rgba(255, 193, 7,.8)',
							}]
						},
						options: {
							maintainAspectRatio: false,
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero:true
									},
									gridLines: {
										display:false
									}
								}],
								xAxes: [{
									gridLines: {
										display:false
									},
									ticks: {
										autoSkip: false
									}
								}]
							},
							legend: {
								display: false,
							}
						}
					});
				}
			})

			$(function () {
				var top10CustomersCanvasChart = document.getElementById("top_10_customers");
				var topCustomersSuccChart = new Chart(top10CustomersCanvasChart, {
					type: 'doughnut',
					data: {
						labels: [@forelse($top_10_customers as $customer) "{{$customer->customer->customer_name}}",@empty @endforelse],
						datasets: [{
							label: '',
							data: [@forelse($top_10_customers as $top_customers) "{{ $top_customers->value }}", @empty @endforelse],
							backgroundColor: ["#8000ff","#bfff00", "#ff00ff", "#00ff80", "#ffbf00","#ffff00","#80ff00", "#00ffbf", "#bf00ff"],
						}]
					},
					options: {
						maintainAspectRatio: false,
					}
				});
			})

			$(function () {
				var top10SolutionsCanvasChart = document.getElementById("top_10_solutions");
				var topCSolutionsSuccChart = new Chart(top10SolutionsCanvasChart, {
					type: 'doughnut',
					data: {
						labels: [@forelse($top_10_solutions as $solution) "{{(isset($solution->system_desc->description) ? $solution->system_desc->description : '')}}",@empty @endforelse],
						datasets: [{
							label: '',
							data: [@forelse($top_10_solutions as $top_solution) "{{ $top_solution->value }}", @empty @endforelse],
							backgroundColor: ["#e83e8c","#dc3545", "#fd7e14", "#ffc107", "#28a745","#20c997","#17a2b8", "#007bff", "#6610f2"],
						}]
					},
					options: {
						maintainAspectRatio: false,
					}
				});
			})

			$(function () {
				var top10LocationsCanvasChart = document.getElementById("top_10_locationss");
				var topLocationsChart = new Chart(top10LocationsCanvasChart, {
					type: 'doughnut',
					data: {
						labels: [@forelse($top_10_locations as $location) "{{(isset($location->location->name) ? $location->location->name : '')}}",@empty @endforelse],
						datasets: [{
							label: '',
							data: [@forelse($top_10_locations as $top_location) "{{ $top_location->value }}", @empty @endforelse],
							backgroundColor: ["#0abda0", "#d4dca9","#ebf2ea", "#bf9d7a", "#80add7","#20c997","#17a2b8", "#007bff", "#6610f2"],
						}]
					},
					options: {
						maintainAspectRatio: false,
					}
				});
			})

			$(function () {
				var consultingCanvasChart = document.getElementById("consulting_canvas");
				var thisYear = {
					label: '{{isset($monthly_consulting_1[0]->year)?$monthly_consulting_1[0]->year:''}}',
					data: [@forelse($monthly_value_1 as $value) "{{$value}}", @empty @endforelse],
					backgroundColor: 'rgb(0,142,204)',
					borderColor: 'rgb(0,142,204)',
				};

				var lastYear = {
					label: '{{isset($monthly_consulting_2[0]->year)?$monthly_consulting_2[0]->year:''}}',
					data: [@forelse($monthly_value_2 as $value) "{{$value}}", @empty @endforelse],
					backgroundColor: '#513cdc',
					borderColor: '#513cdc',
				};

				var lastOfLastYear = {
					label: '{{isset($monthly_consulting_3[0]->year)?$monthly_consulting_3[0]->year:''}}',
					data: [@forelse($monthly_value_3 as $value) "{{$value}}", @empty @endforelse],
					backgroundColor: '#d525c9',
					borderColor: '#d525c9',
				};

				var consultingData = {
					labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
					datasets: [lastOfLastYear, lastYear, thisYear]
				};

				var monthlyConsulting = {
					maintainAspectRatio: false,
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero:true,
								callback: function (data) {
									return numeral(data).format('0,0')
								}
							},
							gridLines: {
								display:false
							},
							stacked: true
						}],
						xAxes: [{
							gridLines: {
								display:false
							},
							ticks: {
								autoSkip: false
							},
							stacked: true
						}]
					},
				};

				var barChart = new Chart(consultingCanvasChart, {
					type: 'bar',
					data: consultingData,
					options: monthlyConsulting
				});
			})

			$(function () {
				var yearToDateCanvasChart = document.getElementById("year_to_date");
				var yearToDateChart = new Chart(yearToDateCanvasChart, {
					type: 'horizontalBar',
					data: {
						labels: ["{{isset($year_to_date_3->year)?$year_to_date_3->year:''}}", "{{isset($year_to_date_2->year)?$year_to_date_2->year:''}}", "{{isset($year_to_date_1->year)?$year_to_date_1->year:''}}"],
						datasets: [{
							label: 'Value',
							data: ["{{isset($year_to_date_3->value)?round($year_to_date_3->value,2):0}}", "{{isset($year_to_date_2->value)?round($year_to_date_2->value,2):0}}", "{{isset($year_to_date_1->value)?round($year_to_date_1->value,2):0}}",],
							backgroundColor: 'rgba(255, 193, 7,.8)',
							borderColor: 'rgba(255, 193, 7,.8)',
						},
							{
								label: 'Hours',
								data: ["{{isset($year_to_date_3->hours)?round($year_to_date_3->hours,2):0}}","{{isset($year_to_date_2->hours)?round($year_to_date_2->hours,2):0}}", "{{isset($year_to_date_1->hours)?round($year_to_date_1->hours,2):0}}"],
								backgroundColor: 'rgba(255, 193, 7,.0)',
								borderColor: 'rgba(255, 193, 7,.0)',
							}]
					},
					options: {
						maintainAspectRatio: false,
							scales: {
							yAxes: [{
								ticks: {
									beginAtZero:true
								},
								gridLines: {
									display:false
								}
							}],
							xAxes: [{
								gridLines: {
									display:false
								},
								ticks: {
									stepSize: 100000,
									callback: function(value) {
										var ranges = [
											{ divider: 1e6, suffix: 'M' },
											{ divider: 1e3, suffix: 'k' }
										];
										function formatNumber(n) {
											for (var i = 0; i < ranges.length; i++) {
												if (n >= ranges[i].divider) {
													return (n / ranges[i].divider).toString() + ranges[i].suffix;
												}
											}
											return n;
										}
										return formatNumber(value);
									}
								}
							}]
						},
						legend: {
							display: false,
						}
					}
				});
			})
		})


	</script>
@endsection
