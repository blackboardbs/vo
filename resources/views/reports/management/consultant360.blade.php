@extends('adminlte.default')

@section('title')  Consultant 360 Degree (R360) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="consultant360"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_drop_down, request()->team , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource', $resource_drop_down, request()->resource , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('period', $yearmonth_dropdown, $date->now()->subMonths(1)->year.(($date->now()->subMonths(2)->month < 10)?'0'.$date->now()->subMonths(2)->month:$date->now()->subMonths(2)->month) , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource_type', $resource_type_dropdown, request()->resource_type , ['class' => 'form-control w-100 search ', 'placeholder' => 'All']) !!}
                        <span>Resource Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project_type', $project_type_dropdown, request()->project_type , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Project Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('report_to', $resource_drop_down, request()->report_to , ['class' => 'form-control w-100 search ', 'placeholder' => 'All']) !!}
                        <span>Report To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_dropdown, request()->cost_center , ['class' => 'form-control w-100 search ', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                <a href="{{route('consultant360')}}" class="btn w-100 btn-info">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Resource</th>
                    <th>Utilized From</th>
                    <th>Utilized Until</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($resources as $resource)
                        <tr>
                            <td><a href="{{route('consultant360details', $resource)}}">{{isset($resource->user)?$resource->user->first_name.' '.$resource->user->last_name:''}}</a></td>
                            <td>{{$resource->util_date_from}}</td>
                            <td>{{$resource->util_date_to}}</td>
                        </tr>
                        @empty
                            <tr><td class="text-center">You currently have no users</td></tr>
                    @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td class="text-right" style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $resources->firstItem() }} - {{ $resources->lastItem() }} of {{ $resources->total() }}
                        </td>
                        <td class="text-left">
                            {{ $resources->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
