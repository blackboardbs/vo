@extends('adminlte.default')

@section('title')  Project Hours Summary (R50) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="projecthourssammary"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-4 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('year', $year_dropdown, $date->now()->year , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Year</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('period_from', $year_month_dropdown, $date->now()->year.$date->now()->month , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Period From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('period_to', $year_month_dropdown, $date->now()->year.$date->now()->month , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Period To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', [1 => 'Yes', 0 => 'No'], 1 , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                <a href="{{route('projecthourssammary')}}" class="btn w-100 btn-info">Clear Filters</a>
                </div>
            </div>
        </form>
        
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>@sortablelink('name', 'Project')</th>
                    <th>Customer</th>
                    <th class="text-right">Actual Hours</th>
                    <th class="text-right">Forecast Hours</th>
                    <th class="text-right">PO Hours</th>
                    <th class="text-right">Total Planned Hours</th>
                    <th class="text-right">Estimated Variance On Hours</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($projects as $index => $project)
                        <tr>
                            <td><a href="{{route('project.show',$project)}}">{{$project->name}}</a></td>
                            <td><a href="{{route('customer.show',$project->customer_id)}}">{{isset($project->customer)?$project->customer->customer_name:''}}</a></td>
                            <td class="text-right">{{isset($timesheets[$index])?number_format($timesheets[$index]->hours,2,'.',','):0}}</td>
                            <td class="text-right">{{isset($forecast_hours[$index])?number_format($forecast_hours[$index],2,'.',','):0}}</td>
                            <td class="text-right">{{isset($po_hours[$index])?number_format($po_hours[$index],2,'.',','):0}}</td>
                            <td class="text-right">{{number_format((isset($timesheets[$index])?$timesheets[$index]->hours:0) + (isset($forecast_hours[$index])?$forecast_hours[$index]:0) ,2,'.',',')}}</td>
                            <td class="text-right">{{number_format((isset($po_hours[$index])?$po_hours[$index]:0) - ((isset($timesheets[$index])?$timesheets[$index]->hours:0) + (isset($forecast_hours[$index])?$forecast_hours[$index]:0)) ,2,'.',',')}}</td>
                        </tr>
                        @empty
                    @endforelse
                <tr>
                    <th colspan="2" class="text-right">TOTAL</th>
                    <th class="text-right">{{number_format($total_hours,2,'.',',')}}</th>
                    <th class="text-right">{{number_format($total_forecast_hours,2,'.',',')}}</th>
                    <th class="text-right">{{number_format($total_po_hours,2,'.',',')}}</th>
                    <th class="text-right">{{number_format(($total_hours + $total_forecast_hours),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($total_po_hours - ($total_hours + $total_forecast_hours) ,2,'.',',')}}</th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $projects->firstItem() }} - {{ $projects->lastItem() }} of {{ $projects->total() }}
                        </td>
                        <td>
                            {{ $projects->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection
