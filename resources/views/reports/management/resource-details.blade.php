@extends('adminlte.default')

@section('title')  Resource Detail (R52) @endsection

@section('header')
<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>

    <x-export route="resourcedetails"></x-export>
</div>
@endsection

@section('content')
<div class="container-fluid">
    {!! Form::open(['url' => route('resourcedetails'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
    <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
    {!! Form::close() !!}
    <hr />

    <div class="limiter">
        <div class="container-table100">
            <div class="wrap-table100">
                <div class="table100 ver1">

                    <div class="wrap-table100-nextcols js-pscroll" style="overflow-y:auto">
                        <div class="table100-nextcols">
                            <table>
                                <thead>
                                <tr class="row100 head bg-dark">
                                <th class="cell100 column1">Resource</th>
                                    <th class="cell100 column2">Status</th>
                                    <th class="cell100 column3">Vendor Name</th>
                                    <th class="cell100 column3">Billing Period</th>
                                    <th class="cell100 column3">Role</th>
                                    <th class="cell100 column3">Level</th>
                                    <th class="cell100 column3">Technical Rating</th>
                                    <th class="cell100 column3">Business Skill</th>
                                    <th class="cell100 column3">Line Manager</th>
                                    <th class="cell100 column3">Scrum Manager</th>
                                    <th class="cell100 column3">Product Owner</th>
                                    <th class="cell100 column3">Project</th>
                                    <th class="cell100 column3">Customer</th>
                                    <th class="cell100 column3 text-right">Opex/Capex</th>
                                    <th class="cell100 column3">Customer PO</th>
                                    <th class="cell100 column3">Assignment PO</th>
                                    <th class="cell100 column3 text-right">PO Hours</th>
                                    <th class="cell100 column3 text-right">Working Days</th>
                                    <th class="cell100 column3 text-right">Working Hours</th>
                                    <th class="cell100 column3 text-right">Variance on PO</th>
                                    <th class="cell100 column3 text-right">Hours Actual</th>
                                    <th class="cell100 column3 text-right">Hours Forecast</th>
                                    <th class="cell100 column3">Assignment Start Date</th>
                                    <th class="cell100 column3">Assignment End Date</th>
                                    <th class="cell100 column3 text-right">Assigned Months</th>
                                    <th class="cell100 column3 text-right">Plan Hours Per Month (avg)</th>
                                    <th class="cell100 column3 text-right">Rate</th>
                                    <th class="cell100 column3 text-right">PO Value</th>
                                    <th class="cell100 column3 text-right">Planned Spend</th>
                                    <th class="cell100 column3 text-right">Variance on PO</th>
                                    <th class="cell100 column3 text-right">Actual Spend</th>
                                    <th class="cell100 column3 text-right">Forecast Spend</th>
                                    <th class="cell100 column3 text-right">Total Planned Spend</th>
                                    <th class="cell100 column3 text-right">Estimated Variance on PO</th>
                                    <th class="cell100 column3 text-right">Total Project Budget</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($assignments as $index => $assignment)
                                    <tr>
                                        <td class="cell100 column1" nowrap>{{isset($assignment->resource)?$assignment->resource->first_name.' '.$assignment->resource->last_name:''}}</td>
                                        <td class="cell100 column2" nowrap>{{isset($assignment->project_status)?$assignment->project_status->description:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->resource->vendor)?$assignment->resource->vendor->vendor_name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->resource->vendor)?$assignment->resource->vendor->payment_terms_days:''}}</td>
                                        <td class="cell100 column3" nowrap>{{$assignment->role}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->resource_user->resource_level_desc)?$assignment->resource_user->resource_level_desc->description:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->resource_user->technical_rating)?$assignment->resource_user->technical_rating->name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->resource_user->business_skill)?$assignment->resource_user->business_skill->name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{$assignment->report_to}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->project->manager)?$assignment->project->manager->first_name.' '.$assignment->project->manager->last_name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->product_owner)?$assignment->product_owner->first_name.' '.$assignment->product_owner->last_name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->project)?$assignment->project->name:''}}</td>
                                        <td class="cell100 column3" nowrap>{{isset($assignment->project->customer)?$assignment->project->customer->customer_name:''}}</td>
                                        <td class="cell100 column3 text-right" nowrap>{{isset($assignment->project)?number_format((int)$assignment->project->opex_project,2,'.',','):''}}</td>
                                        <td class="cell100 column3">{{isset($assignment->project)?$assignment->project->customer_po:''}}</td>
                                        <td class="cell100 column3">{{$assignment->customer_po}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($assignment->hours,2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($working_days[$index],0,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($working_hours[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format(($assignment->hours - $working_hours[$index]),2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($actual_hours[$index]->hours,2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($hours_forecast[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3">{{$assignment->start_date}}</td>
                                        <td class="cell100 column3">{{$assignment->end_date}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($assignment_months[$index],0,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{($assignment_months[$index] > 0)?number_format(($assignment->hours/$assignment_months[$index]),2,'.',','):number_format(0 ,2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($invoice_rate[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($po_value[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($planned_spend[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format(($po_value[$index] - $planned_spend[$index]),2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format(($actual_hours[$index]->hours * $invoice_rate[$index]) ,2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format(($hours_forecast[$index] * $invoice_rate[$index]),2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($planned_spend[$index],2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format(($po_value[$index] - $planned_spend[$index]),2,'.',',')}}</td>
                                        <td class="cell100 column3 text-right">{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                                <table class="tabel tabel-borderless" style="margin: 0 auto">
                                    <tr>
                                        <td class="text-right" style="vertical-align: center;">
                                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $assignments->firstItem() }} - {{ $assignments->lastItem() }} of {{ $assignments->total() }}
                                        </td>
                                        <td class="text-left">
                                            {{ $assignments->appends(request()->except('page'))->links() }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('extra-css')
<link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('dist/animate/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('dist/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('dist/perfect-scrollbar/perfect-scrollbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<style>
    .container-table100 {
        max-width: 100%!important;
    }
    .table100 td {
        padding-top: 10px!important;
        padding-bottom: 10px!important;
    }
</style>
@endsection
@section('extra-js')
<script src="{{asset('chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('dist/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/select2/select2.min.js')}}"></script>
<script src="{{asset('dist/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script>
    $('.js-pscroll').each(function(){
        var ps = new PerfectScrollbar(this);

        $(window).on('resize', function(){
            ps.update();
        })

        $(this).on('ps-x-reach-start', function(){
            $(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
        });

        $(this).on('ps-scroll-x', function(){
            $(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
        });

    });




</script>
<script src="{{asset('js/main.js')}}"></script>
<script>
    $(".").chosen();
    $('.chosen-container').css({
        width: '100%',
    });

    $('#filter_results select').on('change', () => {
        $('#filter_results').submit();
    });
</script>
@endsection
