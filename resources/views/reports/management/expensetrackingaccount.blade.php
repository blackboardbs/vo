@extends('adminlte.default')

@section('title')  Expense Tracking Summary by Account (R28) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="expensetrackinginsight.account"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down , request()->company, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                       {!! Form::select('account', $account_drop_down, request()->account, ['class' => 'search w-100 form-control ', 'placeholder' => 'All', 'id' => 'account']) !!}
                       <span>Account</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('account_element', [], request()->account_element, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All', 'id' => 'account_element']) !!}
                        <span>Account Element</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('assignment', $assignment_drop_down, request()->assignment, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Assignment</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('claimable', [1 => 'Yes', 0 => 'No'], request()->claimable, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Claimable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', [1 => 'Yes', 0 => 'No'], request()->billable, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('week_from', $weeks_drop_down, request()->week_from, ['class' => 'search w-100 form-control ', 'placeholder' => 'All']) !!}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('week_to', $weeks_drop_down, request()->week_to, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Work To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('payment_reference', request()->payment_reference, ['class' => 'search w-100 form-control filter']) !!}
                        <span>Payment Reference</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_drop_down, request()->cost_center, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource', $resource_drop_down, request()->resource, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('approved_by', $approved_by_drop_down, request()->approved_by, ['class' => 'search w-100 form-control filter ', 'placeholder' => 'All']) !!}
                        <span>Approved By</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('approval_status', [1 => 'New', 2 => 'Approved', 3 => 'Rejected', 4 => 'Updated'] , request()->approval_status, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Approval Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('payment_status', [1 => 'New', 2 => 'Paid'], request()->payment_status, ['class' => 'search w-100 form-control  filter', 'placeholder' => 'All']) !!}
                        <span>Payment Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('approved_on_from', request()->approved_on_from, ['class' => 'search w-100 form-control datepicker']) !!}
                        <span>Approved On From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('approved_on_to', request()->approved_on_to, ['class' => 'search w-100 form-control datepicker filter']) !!}
                        <span>Approved On To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('payment_date_from', request()->payment_date_from, ['class' => 'search form-control datepicker w-100']) !!}
                        <span>Payment From Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('payment_date_to', request()->payment_date_to, ['class' => 'form-control-sm form-control datepicker filter w-100']) !!}
                        <span>Payment To Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <a href="{{route('expensetrackinginsight.index')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
            </div>
</form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Account</th>
                    <th class="text-right">Amount</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($expensetrackings as $expense)
                        <tr>
                            <td>{{isset($expense->account->description)?$expense->account->description:''}}</td>
                            <td class="text-right">{{number_format($expense->amount,2,'.',',')}}</td>
                        </tr>
                        @empty
                            <tr><td class="text-center">There's no expense tracking</td></tr>
                    @endforelse
                    <tr>
                        <th class="text-right">Total</th>
                        <th class="text-right">{{number_format($total_amount,2,'.',',')}}</th>
                    </tr>
                </tbody>
            </table>
            {{$expensetrackings->appends(request()->except('page'))->links()}}
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results .filter').on('change', () => {
            $('#filter_results').submit();
        });

        $(function () {
            $('#account').on('change', function (e) {
                axios.get('/account_element/get/'+$(this).val())
                    .then(function (response) {

                        jQuery.each( response.data, function( i, val ) {
                            $("#account_element").append("<option value='"+i+"'>"+val+"</option>").trigger("chosen:updated");
                        });
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
            })
        })
    </script>
@endsection
