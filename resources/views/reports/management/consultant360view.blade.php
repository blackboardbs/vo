@extends('adminlte.default')

@section('title')  Consultant 360 Degree (R360) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('consultant360'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            {{--<tr>
                <td>{!! Form::select('company', $company_dropdown, null , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, null , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('year', $year_dropdown, $date->now()->year , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Year']) !!}</td>

            </tr>
            <tr>
                <td>{!! Form::select('period_from', $year_month_dropdown, $date->now()->year.$date->now()->month , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Year Month - From']) !!}</td>
                <td>{!! Form::select('period_to', $year_month_dropdown, $date->now()->year.$date->now()->month , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Year Month - To']) !!}</td>
                <td>{!! Form::select('billable', [1 => 'Yes', 0 => 'No'], 1 , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>--}}
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-secondary">
                    <th colspan="4">Resource</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Resource Name:</th>
                        <td>{{isset($resource->user)?$resource->user->first_name.' '.$resource->user->last_name:''}}</td>
                        <th>Resource Number:</th>
                        <td>{{$resource->resource_no}}</td>
                    </tr>
                    <tr>
                        <th>Employee Status:</th>
                        <td>{{isset($resource->status)?$resource->status->description:''}}</td>
                        <th>Vendor Name:</th>
                        <td>{{isset($resource->user->vendor)?$resource->user->vendor->vendor_name:''}}</td>
                    </tr>
                    <tr>
                        <th>Join Date:</th>
                        <td>{{$resource->join_date}}</td>
                        <th>Team:</th>
                        <td>{{isset($resource->team)?$resource->team->team_name:''}}</td>
                    </tr>
                    <tr>
                        <th>Position:</th>
                        <td>{{isset($resource->resource_position_desc)?$resource->resource_position_desc->description:''}}</td>
                        <th>Resource Type:</th>
                        <td>{{isset($resource->resource_type_desc)?$resource->resource_type_desc->description:''}}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-secondary">
                    <th colspan="4">Profitability</th>
                </tr>
                </thead>
            </table>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Margin Per Month - Last 12 Month</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="margin-per-month" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Utilization Per Week - Last 26 Weeks</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="util-per-week" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-sm mt-5">
                        <thead>
                        <tr class="bg-secondary">
                            <th colspan="3">Resource Profitability</th>
                        </tr>
                        <tr class="bg-dark">
                            <th></th>
                            <th class="text-right">This Year ({{$date->now()->year}})</th>
                            <th class="text-right">Last Year ({{$date->now()->subYears(1)->year}})</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Total Income</th>
                            <td class="text-right">{{number_format($current_year_income,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($prev_year_income,2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Total Cost To Company</th>
                            <td class="text-right">{{number_format($current_year_c_t_c,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($last_year_c_t_c,2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Profit/Loss Value</th>
                            <td class="text-right">{{number_format(($current_year_income - $current_year_c_t_c),2,'.',',')}}</td>
                            <td class="text-right">{{number_format(($prev_year_income - $last_year_c_t_c),2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Profit/Loss %</th>
                            <td class="text-right">{{(($current_year_income > 0 || $current_year_income < 0) && ($current_year_c_t_c > 0 || $current_year_c_t_c < 0))?number_format((($current_year_income - $current_year_c_t_c)/$current_year_income) * 100):0}}%</td>
                            <td class="text-right">{{(($prev_year_income > 0 || $prev_year_income < 0) && ($last_year_c_t_c > 0 || $last_year_c_t_c < 0))?number_format((($prev_year_income - $last_year_c_t_c)/$prev_year_income) * 100):0}}%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-sm mt-5">
                        <thead>
                        <tr class="bg-secondary">
                            <th colspan="2">Cost To Company</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Current CTC Rate</th>
                            <td class="text-right">{{number_format((isset($current_c_t_c_rate)?($current_c_t_c_rate->internal_cost_rate + $current_c_t_c_rate->external_cost_rate):0),2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Average CTC Rate</th>
                            <td class="text-right">{{number_format($c_t_c_rate_average,2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <th>Average Open Assignment Invoice Rate</th>
                            <td class="text-right">{{number_format($avg_invoice_rate,2,'.', ',')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-secondary">
                    <th colspan="9">Continuity</th>
                </tr>
                <tr>
                    <th colspan="9">Open Assignments - Billable</th>
                </tr>
                <tr class="bg-dark">
                    <th>Project</th>
                    <th>PO Number</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th class="text-right">Invoice Rate</th>
                    <th class="text-right">PO Hours</th>
                    <th class="text-right">Actual Hours</th>
                    <th class="text-right">LTS Hours</th>
                    <th class="text-right">LTS Income Value</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($open_assignments_billable as $assignment)
                        <tr>
                            <td>{{isset($assignment->project)?$assignment->project->name.' ('.$assignment->id.')':''}}</td>
                            <td>{{$assignment->customer_po}}</td>
                            <td>{{$assignment->start_date}}</td>
                            <td>{{$assignment->end_date}}</td>
                            <td class="text-right">{{number_format($assignment->invoice_rate,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($assignment->hours,2,'.', ',')}}</td>
                            <td class="text-right">{{number_format($assignment->bill_hours,2,'.',',')}}</td>
                            <td class="text-right">{{number_format(($assignment->hours - $assignment->bill_hours),2,'.',',')}}</td>
                            <td class="text-right">{{(($assignment->hours - $assignment->bill_hours) > 0)?number_format((($assignment->hours - $assignment->bill_hours) * $assignment->invoice_rate),2,'.',','):number_format(0,2,'.',',')}}</td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="9" class="text-center">No open billable assignment</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <thead>
                <tr>
                    <th colspan="9">Open Assignments - Non-Billable</th>
                </tr>
                <tr class="bg-dark">
                    <th>Project</th>
                    <th>PO Number</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th class="text-right">Labour Rate</th>
                    <th class="text-right">PO Hours</th>
                    <th class="text-right">Actual Hours</th>
                    <th class="text-right">LTS Hours</th>
                    <th class="text-right">LTS Income Value</th>
                </tr>
                </thead>
                <tbody>
                @forelse($open_assignments_non_bill as $assignment)
                    <tr>
                        <td>{{isset($assignment->project)?$assignment->project->name.' ('.$assignment->id.')':''}}</td>
                        <td>{{$assignment->customer_po}}</td>
                        <td>{{$assignment->start_date}}</td>
                        <td>{{$assignment->end_date}}</td>
                        <td class="text-right">{{number_format(($assignment->internal_cost_rate + $assignment->external_cost_rate),2,'.',',')}}</td>
                        <td class="text-right">{{number_format($assignment->hours,2,'.', ',')}}</td>
                        <td class="text-right">{{number_format($assignment->bill_hours,2,'.',',')}}</td>
                        <td class="text-right">{{number_format(($assignment->hours - $assignment->bill_hours),2,'.',',')}}</td>
                        <td class="text-right">{{(($assignment->hours - $assignment->bill_hours) > 0)?number_format((($assignment->hours - $assignment->bill_hours) * ($assignment->internal_cost_rate + $assignment->external_cost_rate)),2,'.',','):number_format(0,2,'.',',')}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9" class="text-center">No open billable assignment</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-sm mt-5">
                        <thead>
                        <tr class="bg-secondary">
                            <th colspan="2">Outstanding Timesheets</th>
                        </tr>

                        </thead>
                        <tbody>
                        @forelse($outstanding_timesheets as $timesheet)
                            <tr>
                                <th>YearWeek No:</th>
                                <td>{{$timesheet}}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2" class="text-center">You have no outstanding timesheet</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-sm mt-5">
                        <thead>
                        <tr class="bg-secondary">
                            <th colspan="2">Utilization</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Utilized flag:</th>
                            <td>{{($resource->util == 1)?"Yes":"No"}}</td>
                        </tr>
                        <tr>
                            <th>Start Date:</th>
                            <td>{{$resource->util_date_from}}</td>
                        </tr>
                        <tr>
                            <th>End Date:</th>
                            <td>{{$resource->util_date_to}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-secondary">
                    <th colspan="2">Payroll</th>
                </tr>
                </thead>
                <tbody>
                    <tr class="bg-dark">
                        <th colspan="4">Leave</th>
                    </tr>
                    <tr>
                        <th>Open Balance</th>
                        <td class="text-right" id="o_balance">{{isset($leave_balance->opening_balance)?number_format($leave_balance->opening_balance,2,'.',','):number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Accural</th>
                        <td class="text-right" id="occural">{{isset($leave_balance->leave_per_cycle)?round($leave_balance->leave_per_cycle/12 ,2):number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Leave Taken</th>
                        <td class="text-right" id="l_taken">{{isset($leave)?number_format($leave,2,'.',','):number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Closing Balance</th>
                        <td class="text-right" id="c_balance">{{(isset($leave_balance) && isset($leave))?number_format(($leave_balance->opening_balance + ($leave_balance->leave_per_cycle/12) - $leave),2,'.',','):number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr class="bg-dark">
                        <th colspan="2">Commission</th>
                    </tr>
                    <tr>
                        <th>Commission Scheme</th>
                        <td class="text-right" id="c_scheme">{{isset($resource->commission)?$resource->commission->description:''}}</td>
                    </tr>
                    <tr>
                        <th>Min Rate</th>
                        <td class="text-right" id="m_rate">{{isset($resource->commission)?$resource->commission->min_rate:''}}</td>
                    </tr>
                    <tr>
                        <th>Average Billable Rate</th>
                        <td class="text-right" id="a_b_rate">{{number_format($avg_bill_rate, 2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Billable Hours</th>
                        <td class="text-right" id="b_hoiours">{{number_format($total_billable_hours,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Commission Value</th>
                        <td class="text-right" id="b_hoiours">{{number_format($commission_value,2,'.',',')}}</td>
                    </tr>
                </tbody>
            </table>
            <table  class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-dark">
                    <th colspan="4">Expense Claims</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Claims Not Approved</th>
                        <td class="text-right">{{number_format($total_exp_not_approved,2,'.',',')}}</td>
                        <th>Number of Claims</th>
                        <td class="text-right">{{number_format($time_exp_not_approved,0,'.',',')}}</td>
                    </tr>
                    <tr>
                        <th>Claims Not Paid</th>
                        <td class="text-right">{{number_format($total_exp_not_paid,2,'.',',')}}</td>
                        <th>Number of Claims</th>
                        <td class="text-right">{{number_format($time_exp_not_paid,0,'.',',')}}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-5">
                <thead>
                <tr class="bg-secondary">
                    <th colspan="4">Compliance</th>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">CV Up To Date</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Personal Information</th>
                        <td class="text-right">{{number_format($personal_info_perce,0,'.',',')}}%</td>
                        <th>Availability</th>
                        <td class="text-right">{{number_format($availability,0,'.',',')}}%</td>
                    </tr>
                    <tr>
                        <th>Skill</th>
                        <td class="text-right">{{number_format($skill,0,'.',',')}}%</td>
                        <th>Qualification</th>
                        <td class="text-right">{{number_format($qualification,0,'.',',')}}%</td>
                    </tr>
                    <tr>
                        <th>Experience</th>
                        <td class="text-right">{{number_format($experience,0,'.',',')}}%</td>
                        <th>Industry Experience</th>
                        <td class="text-right">{{number_format($industry_experience,0,'.',',')}}%</td>
                    </tr>
                    <tr>
                        <th colspan="2">TOTAL</th>
                        <td class="text-right" colspan="2">{{number_format(((($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience) > 0)?($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience)/6:0),0,'.',',')}}%</td>
                    </tr>
                    <tr>
                        <th colspan="4">&nbsp;</th>
                    </tr>
                    <tr>
                        <th>Resource Profile</th>
                        <td class="text-right">{{number_format($resource_profile_perce,0,'.',',')}}%</td>
                        <th>Timesheets complete on Time</th>
                        <td class="text-right">TBC</td>
                    </tr>
                    <tr>
                        <th>Assessment This Month</th>
                        <td class="text-right"></td>
                        <th>Assessment Score</th>
                        <td class="text-right">TBC</td>
                    </tr>
                    <tr>
                        <th>Leave Application 7 days before Start</th>
                        <td class="text-right"></td>
                        <th></th>
                        <td class="text-right"></td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>
@endsection

@section('extra-js')

    <script src="{!! asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });

        $(function () {
            var marginPerMonthCanvasChart = document.getElementById("margin-per-month");
            if (marginPerMonthCanvasChart) {
                var marginPerChart = new Chart(marginPerMonthCanvasChart, {
                    type: 'bar',
                    data: {
                        labels: [@forelse($margin_months as $year_month) "{{$year_month['year_month']}}", @empty @endforelse],
                        datasets: [{
                            label: '',
                            data: [@forelse($margin_months as $year_month) "{{$year_month['rate_perc']}}", @empty @endforelse],
                            backgroundColor: 'rgba(23, 162, 184,.8)',
                            borderColor: 'rgba(23, 162, 184,.8)',
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    userCallback: function(value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end
                                        value = value.toString();
                                        value = value.split(/(?=(?:...)*$)/);

                                        // Convert the array to a string and format the output
                                        value = value.join('.');
                                        return value + '%';
                                    }
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    autoSkip: false
                                }
                            }]
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            }
        })

        $(function () {
            var utilPerWeekCanvasChart = document.getElementById("util-per-week");
            if (utilPerWeekCanvasChart) {
                var utilPerWeekChart = new Chart(utilPerWeekCanvasChart, {
                    type: 'bar',
                    data: {
                        labels: [@forelse($year_week as $year_week) "{{$year_week}}", @empty @endforelse],
                        datasets: [{
                            label: '',
                            data: [@forelse($util_perc as $perc) "{{$perc}}", @empty @endforelse],
                            backgroundColor: 'rgba(23, 162, 184,.8)',
                            borderColor: 'rgba(23, 162, 184,.8)',
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    userCallback: function(value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end
                                        value = value.toString();
                                        value = value.split(/(?=(?:...)*$)/);

                                        // Convert the array to a string and format the output
                                        value = value.join('.');
                                        return value + '%';
                                    }
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    autoSkip: false
                                }
                            }]
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            }
        })
    </script>
    <script>
        $(function () {
            $("#year_month").on('change', function () {
                axios.get("{{route('consultant360payroll', $resource->user_id)}}", {
                    params: {
                        year_month: $("#year_month").val(),
                        resource_id: {{$resource->user_id}},
                    }
                })
                    .then(function (response) {
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })
    </script>
@endsection
