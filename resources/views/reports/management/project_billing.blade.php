@extends('adminlte.default')

@section('title')  Project Billing (PM07) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.projectbilling"></x-export>
    </div>
    <hr>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company',$company_drop_down,request()->company,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('customer',$customer_drop_down,request()->customer,['class'=>' form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('q', request()->q,['class'=>' form-control w-100 search'])}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <a href="{{route('reports.projectbilling')}}"  class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead class="bg-dark">
                <tr>
                    <th>Project Name</th>
                    <th>Customer Name</th>
                    <th>Customer PO</th>
                    <th class="text-right">PO Hours</th>
                    <th class="text-right">Assigned Hours</th>
                    <th class="text-right">Billable</th>
                    <th class="text-right">Non Billable</th>
                    <th class="text-right">Total Hours</th>
                    <th class="text-right">Previous Open Balance</th>
                    <th class="text-right">Previous Hours</th>
                    <th class="text-right">Current Open Balance</th>
                    <th class="text-right">Current Hours</th>
                    <th class="text-right">Hours Remaining</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($projects as $project)
                        <tr>
                            <td>{{ $project->project_name }}</td>
                            <td>{{ $project->customer_name }}</td>
                            <td>{{ $project->customer_po }}</td>
                            <td class="text-right">{{ number_format($project->po_hours,2) }}</td>
                            <td class="text-right">{{ number_format($project->assigned_hours,2) }}</td>
                            <td class="text-right">{{ number_format($project->billable,2) }}</td>
                            <td class="text-right">{{ number_format($project->non_billable,2) }}</td>
                            <td class="text-right">{{ number_format($project->total_hours,2) }}</td>
                            <td class="text-right">{{ number_format($project->previous_open_balance,2) }}</td>
                            <td class="text-right">{{ number_format($project->previous_hours,2) }}</td>
                            <td class="text-right">{{ number_format($project->current_open_balance,2) }}</td>
                            <td class="text-right">{{ number_format($project->current_hours,2) }}</td>
                            <td class="text-right">{{ number_format($project->hours_remaining,2) }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">Nothing to see here</td>
                        </tr>
                    @endforelse
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Totals</th>
                        <th class="text-right">
                            {{ number_format($projects->sum('po_hours'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('assigned_hours'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('billable'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('non_billable'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('total_hours'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('previous_open_balance'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('previous_hours'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('current_open_balance'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('current_hours'),2)}}
                        </th>
                        <th class="text-right">
                            {{ number_format($projects->sum('hours_remaining'),2)}}
                        </th>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $projects->firstItem() }} - {{ $projects->lastItem() }} of {{ $projects->total() }}
                        </td>
                        <td>
                            {{ $projects->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
