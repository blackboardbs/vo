<table>
    <thead>
    <tr>
        <th>Account</th>
        <th>Element</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($expensetrackings as $expense)
        <tr>
            <td>{{$expense->account?->description}}</td>
            <td>{{$expense->account_element?->description}}</td>
            <td>{{number_format($expense->amount,2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="2">Total</th>
        <th>{{number_format($total_amount,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>