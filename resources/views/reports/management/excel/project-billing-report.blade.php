<table class="table table-sm table-striped">
    <thead class="bg-dark">
    <tr>
        <th>Company Name</th>
        <th>Project Name</th>
        <th>Customer Name</th>
        <th>Customer PO</th>
        <th>Original Hours</th>
        <th>Billable</th>
        <th>Non Billable</th>
        <th>Total Hours</th>
        <th>Previous Open Balance</th>
        <th>Previous Hours</th>
        <th>Current Open Balance</th>
        <th>Current Hours</th>
        <th>Hours Remaining</th>
    </tr>
    </thead>
    <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{ $project->company_name }}</td>
                <td>{{ $project->project_name }}</td>
                <td>{{ $project->customer_name }}</td>
                <td>{{ $project->customer_po }}</td>
                <td class="text-right">{{ number_format($project->original_hours,2) }}</td>
                <td class="text-right">{{ number_format($project->billable,2) }}</td>
                <td class="text-right">{{ number_format($project->non_billable,2) }}</td>
                <td class="text-right">{{ number_format($project->total_hours,2) }}</td>
                <td class="text-right">{{ number_format($project->previous_open_balance,2) }}</td>
                <td class="text-right">{{ number_format($project->previous_hours,2) }}</td>
                <td class="text-right">{{ number_format($project->current_open_balance,2) }}</td>
                <td class="text-right">{{ number_format($project->current_hours,2) }}</td>
                <td class="text-right">{{ number_format($project->hours_remaining,2) }}</td>
            </tr>
        @endforeach
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>Totals</th>
            <th class="text-right">
                {{ number_format($projects->sum('original_hours'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('billable'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('non_billable'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('total_hours'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('previous_open_balance'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('previous_hours'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('current_open_balance'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('current_hours'),2)}}
            </th>
            <th class="text-right">
                {{ number_format($projects->sum('hours_remaining'),2)}}
            </th>
    </tbody>
</table>