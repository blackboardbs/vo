<table>
    <thead>
    <tr>
                    <th>Name</th>
                    <th>Emp No.</th>
                    <th class="text-right">Open Balance</th>
                    <th class="text-right">Accrued</th>
                    <th class="text-right">Taken</th>
                    <th class="text-right">Closing Balance</th>
                    <th class="text-right">Sick Leave</th>
                    <th class="text-right">Unpaid Leave</th>
                    <th class="text-right">Other Leave</th>
                    <th class="text-right">Commission</th>
                    <th class="text-right">Expenses Not Approved</th>
                    <th class="text-right">Expenses Approved Not Paid</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($resources as $resource)
                        <tr>
                            <td>{{isset($resource->user)?$resource->user->first_name.' '.$resource->user->last_name:''}}</td>
                            <td>{{$resource->resource_no}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['opening_balance'],2)}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['accrued'],2)}}</td>
                            <td class="text-right">{{round($payroll_summary[$resource->user_id]['taken'],2)}}</td>
                            <td class="text-right">{{round(($payroll_summary[$resource->user_id]['closing_balance'] - $payroll_summary[$resource->user_id]['taken']),2)}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['sick']['leave_taken'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['unpaid']['leave_taken'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['other']['leave_taken'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['commission'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['expense_claim_not_approved'],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($payroll_summary[$resource->user_id]['expense_claim_approved'],2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="6"></th>
        <th>{{number_format($total_commission,2,'.',',')}}</th>
        <th>{{number_format($total_expense_not_approved,2,'.',',')}}</th>
        <th>{{number_format($total_expense_approved,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>