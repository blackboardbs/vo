<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Status</th>
        <th>Vendor Name</th>
        <th>Billing Period</th>
        <th>Role</th>
        <th>Level</th>
        <th>Technical Rating</th>
        <th>Business Skill</th>
        <th>Line Manager</th>
        <th>Scrum Manager</th>
        <th>Product Owner</th>
        <th>Project</th>
        <th>Customer</th>
        <th>Opex/Capex</th>
        <th>Customer PO</th>
        <th>Assignment PO</th>
        <th>PO Hours</th>
        <th>Working Days</th>
        <th>Working Hours</th>
        <th>Variance on PO</th>
        <th>Hours Actual</th>
        <th>Hours Forecast</th>
        <th>Assignment Start Date</th>
        <th>Assignment End Date</th>
        <th>Assigned Months</th>
        <th>Plan Hours Per Month (avg)</th>
        <th>Rate</th>
        <th>PO Value</th>
        <th>Planned Spend</th>
        <th>Variance on PO</th>
        <th>Actual Spend</th>
        <th>Forecast Spend</th>
        <th>Total Planned Spend</th>
        <th>Estimated Variance on PO</th>
        <th>Total Project Budget</th>
    </tr>
    </thead>
    <tbody>
    @foreach($assignments as $index => $assignment)
        <tr>
            <td>{{$assignment->resource?->name()}}</td>
            <td>{{$assignment->project_status?->description}}</td>
            <td>{{$assignment->resource?->vendor?->vendor_name}}</td>
            <td>{{$assignment->resource?->vendor?->payment_terms_days}}</td>
            <td>{{$assignment->role}}</td>
            <td>{{$assignment->resource_user?->resource_level_desc?->description}}</td>
            <td>{{$assignment->resource_user?->technical_rating?->name}}</td>
            <td>{{$assignment->resource_user?->business_skill?->name}}</td>
            <td>{{$assignment->report_to}}</td>
            <td>{{$assignment->project?->manager?->name()}}</td>
            <td>{{$assignment->product_owner?->name()}}</td>
            <td>{{$assignment->project?->name}}</td>
            <td>{{$assignment->project?->customer?->customer_name}}</td>
            <td>{{number_format((int)$assignment->project?->opex_project,2,'.',',')}}</td>
            <td>{{$assignment->project->customer_po}}</td>
            <td>{{$assignment->customer_po}}</td>
            <td>{{number_format($assignment->hours,2,'.',',')}}</td>
            <td>{{number_format($working_days[$index]??0,0,'.',',')}}</td>
            <td>{{number_format($working_hours[$index],2,'.',',')}}</td>
            <td>{{number_format(($assignment->hours - $working_hours[$index]),2,'.',',')}}</td>
            <td>{{number_format($actual_hours[$index]->hours,2,'.',',')}}</td>
            <td>{{number_format($hours_forecast[$index],2,'.',',')}}</td>
            <td>{{$assignment->start_date}}</td>
            <td>{{$assignment->end_date}}</td>
            <td>{{number_format($assignment_months[$index],0,'.',',')}}</td>
            <td>{{($assignment_months[$index] > 0)?number_format(($assignment->hours/$assignment_months[$index]),2,'.',','):number_format(0 ,2,'.',',')}}</td>
            <td>{{number_format($invoice_rate[$index],2,'.',',')}}</td>
            <td>{{number_format($po_value[$index],2,'.',',')}}</td>
            <td>{{number_format($planned_spend[$index],2,'.',',')}}</td>
            <td>{{number_format(($po_value[$index] - $planned_spend[$index]),2,'.',',')}}</td>
            <td>{{number_format(($actual_hours[$index]->hours * $invoice_rate[$index]) ,2,'.',',')}}</td>
            <td>{{number_format(($hours_forecast[$index] * $invoice_rate[$index]),2,'.',',')}}</td>
            <td>{{number_format($planned_spend[$index],2,'.',',')}}</td>
            <td>{{number_format(($po_value[$index] - $planned_spend[$index]),2,'.',',')}}</td>
            <td>{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>