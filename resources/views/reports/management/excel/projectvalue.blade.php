<table>
    <thead>
    <tr>
        <th>Project</th>
        <th>Customer</th>
        <th>Actual Value</th>
        <th>Forecast Value</th>
        <th>Total Value</th>
        <th>PO Value</th>
        <th>Estimated Variance Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach($projects as $index => $project)
        <tr>
            <td>{{$project->name}}</td>
            <td>{{$project->customer?->customer_name}}</td>
            <td>{{isset($timesheets[$index])?number_format($timesheets[$index]->value,2,'.',','):0}}</td>
            <td>{{isset($forecast_value[$index])?number_format($forecast_value[$index],2,'.',','):0}}</td>
            <td>{{number_format((isset($timesheets[$index])?$timesheets[$index]->value:0) + (isset($forecast_value[$index])?$forecast_value[$index]:0) ,2,'.',',')}}</td>
            <td>{{isset($po_value[$index])?number_format($po_value[$index],2,'.',','):0}}</td>
            <td>{{number_format((isset($po_value[$index])?$po_value[$index]:0) - ((isset($timesheets[$index])?$timesheets[$index]->value:0) + (isset($forecast_value[$index])?$forecast_value[$index]:0)) ,2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="2">TOTAL</th>
        <th>{{number_format($total_actual_value,2,'.',',')}}</th>
        <th>{{number_format($total_forecast_value,2,'.',',')}}</th>
        <th>{{number_format(($total_actual_value + $total_forecast_value),2,'.',',')}}</th>
        <th>{{number_format($total_po_value,2,'.',',')}}</th>
        <th>{{number_format(($total_po_value - ($total_actual_value + $total_forecast_value)),2,'.',',')}}</th>
    </tr>
    </tbody>
</table>