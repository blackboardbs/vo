<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Utilized From</th>
        <th>Utilized Until</th>
    </tr>
    </thead>
    <tbody>
    @foreach($resources as $resource)
        <tr>
            <td>{{$resource->user?->name()}}</td>
            <td>{{$resource->util_date_from}}</td>
            <td>{{$resource->util_date_to}}</td>
        </tr>
    @endforeach
    </tbody>
</table>