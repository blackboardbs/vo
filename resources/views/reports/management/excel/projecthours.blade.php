<table>
    <thead>
    <tr>
        <th>Project</th>
        <th>Customer</th>
        <th>Actual Hours</th>
        <th>Forecast Hours</th>
        <th>PO Hours</th>
        <th>Total Planned Hours</th>
        <th>Estimated Variance On Hours</th>
    </tr>
    </thead>
    <tbody>
    @foreach($projects as $index => $project)
        <tr>
            <td>{{$project->name}}</td>
            <td>{{$project->customer?->customer_name}}</td>
            <td>{{isset($timesheets[$index])?number_format($timesheets[$index]->hours,2,'.',','):0}}</td>
            <td>{{isset($forecast_hours[$index])?number_format($forecast_hours[$index],2,'.',','):0}}</td>
            <td>{{isset($po_hours[$index])?number_format($po_hours[$index],2,'.',','):0}}</td>
            <td>{{number_format((isset($timesheets[$index])?$timesheets[$index]->hours:0) + (isset($forecast_hours[$index])?$forecast_hours[$index]:0) ,2,'.',',')}}</td>
            <td>{{number_format((isset($po_hours[$index])?$po_hours[$index]:0) - ((isset($timesheets[$index])?$timesheets[$index]->hours:0) + (isset($forecast_hours[$index])?$forecast_hours[$index]:0)) ,2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="2">TOTAL</th>
        <th>{{number_format($total_hours,2,'.',',')}}</th>
        <th>{{number_format($total_forecast_hours,2,'.',',')}}</th>
        <th>{{number_format($total_po_hours,2,'.',',')}}</th>
        <th>{{number_format(($total_hours + $total_forecast_hours),2,'.',',')}}</th>
        <th>{{number_format($total_po_hours - ($total_hours + $total_forecast_hours) ,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>