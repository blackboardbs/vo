@extends('adminlte.default')
@section('title') Scouting Report (R42) @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                Process Status<br/>
                {{Form::select('process_status',$process_statuses_drop_down,old('process_status'),['class'=>' form-control form-control-sm search', 'id' => 'process_status','style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Interview Status<br/>
                {{Form::select('process_interview',$process_interview_drop_down,old('process_interview'),['class'=>' form-control form-control-sm search', 'id' => 'process_interview', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Role<br/>
                {{Form::select('role',$role_drop_down,old('role'),['class'=>' form-control form-control-sm search', 'id' => 'role', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Vendor<br/>
                {{Form::select('vendor',$vendor_drop_down,old('vendor'),['class'=>' form-control form-control-sm search', 'id' => 'vendor', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Resource Level<br/>
                {{Form::select('resource_level',$resource_level_drop_down,old('resource_level'),['class'=>' form-control form-control-sm search', 'id' => 'resource_level', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Business Rating<br/>
                {{Form::select('businees_rating',$business_rating_drop_down,old('businees_rating'),['class'=>' form-control form-control-sm search', 'id' => 'businees_rating', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Technical Rating<br/>
                {{Form::select('technical_rating',$tecnical_rating_drop_down,old('technical_rating'),['class'=>' form-control form-control-sm search', 'id' => 'technical_rating', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                <a href="{{route('reports.scoutinganalysis')}}" style="margin-top: 23px; color: black !important; border: none !important;" class="btn btn" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Contract Status Count
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="contract_status_chart" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Interview Status Count
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="interview_status_chart" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="background-color: #191950; cursor: move;">
                            <h3 class="card-title">
                                Number of Resources by Contracting House
                            </h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="chart-responsive">
                                <canvas id="vendor_chart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script src="{!! asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
    <script>

        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        {!! $process_status_counter['declined'] !!},
                        {!! $process_status_counter['contract_end'] !!},
                        {!! $process_status_counter['pending'] !!},
                        {!! $process_status_counter['opportunity'] !!},
                        {!! $process_status_counter['offer_not_accepted'] !!},
                        {!! $process_status_counter['resigned'] !!}
                    ],
                    backgroundColor: [
                        "#191950",
                        "#0D47A1",
                        "#01B8AA",
                        "#689FB0",
                        "#374649",
                        "#FD625E",
                    ],
                }],
                labels: [
                    "Declined",
                    "Contract End",
                    "Pending",
                    "Opportunity",
                    "Offer Not Accepted",
                    "Resigned"
                ]
            },
            options: {
                responsive: true
            }
        };

        var config_interview = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        {!! $interview_status_counter['interviewed'] !!},
                        {!! $interview_status_counter['to_interview'] !!},
                        {!! $interview_status_counter['aes_experience'] !!}
                    ],
                    backgroundColor: [
                        "#191950",
                        "#0D47A1",
                        "#01B8AA"
                    ],
                }],
                labels: [
                    "Declined",
                    "Contract End",
                    "Pending"
                ]
            },
            options: {
                responsive: true
            }
        };

        var vendor_chart = document.getElementById("vendor_chart");
        if (vendor_chart) {
            var loginsSuccChart = new Chart(vendor_chart, {
                type: 'bar',
                data: {
                    labels: [@foreach($vendors_counter as $vendor) "{!!$vendor->vendor_name!!}", @endforeach],
                    datasets: [{
                        label: '',
                        data: [@foreach($vendors_counter as $vendor) "{!!$vendor->counter!!}", @endforeach],
                        backgroundColor: '#191950',
                        borderColor: '#191950',
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        }

        window.onload = function() {
            var contract_status = document.getElementById("contract_status_chart").getContext("2d");
            window.myPie = new Chart(contract_status, config);

            var interview_status = document.getElementById("interview_status_chart").getContext("2d");
            window.myPie = new Chart(interview_status, config_interview);
        };

    </script>
@endsection