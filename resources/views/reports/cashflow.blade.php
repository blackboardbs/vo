@extends('adminlte.default')

@section('title')  Cashflow (R34) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.cashflow"></x-export>
    </div> 
@endsection

@section('content')
    <div class="container-fluid">
        {{-- <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $users_dropdown, request()->employee , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_from', request()->date_from , ['class' => 'form-control search w-100 datepicker']) !!}
                        <span>Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_to', request()->date_to , ['class' => 'form-control search datepicker w-100']) !!}
                        <span>Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <a href="{{route('reports.cashflow')}}" class="btn w-100 btn-info">Clear Filters</a>
                </div>
            </div>
        </form> --}}
        <hr />
        <div class="table-responsive">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" style="font-weight: bold;">Cashflow Forecast Summary</h4>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr class="bg-dark">
                            <th></th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span style="font-weight: bold;">Income</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Customer Invoices</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_invoices_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Customer not Invoiced</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($customer_not_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Other Income Planned</td>
                                <td class="text-right">{{number_format(($other_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($other_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Assignment Income</td>
                                <td class="text-right">{{number_format(($assignment_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Pipeline</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th class="text-right"><span style="font-weight: bold;">Total Income</span></th>
                                <th class="text-right">{{number_format($total_income_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_150,2,'.',',')}}</th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><span style="font-weight: bold;">Expenses</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Vendor Invoices</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Vendor not Invoiced</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($vendor_not_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Assignment Expense</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($assignment_expense_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Other Expenses</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach ($other_expense as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                {{-- <td>{{$invoice->reference}}</td> --}}
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>Pipeline</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($pipeline_expenses_total['150']), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th class="text-right"><span style="font-weight: bold;">Total Expenses</span></th>
                                <th class="text-right">{{number_format($total_expense_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_150,2,'.',',')}}</th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><span style="font-weight: bold;">Net Flow</span></td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_total), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($overdue_total), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_5), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_10), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_20), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_30), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_60), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_90), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_120), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($total_150), 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Cumulative Flow</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($cumalative_flow_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" style="font-weight: bold;">Cashflow Forecast Detail</h4>
                </div>
                <div class="card-body">
                    <h5 style="font-weight: bold;">Income</h5>
                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Customer Invoices</span>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Ref</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($customer_invoiced as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->reference}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_invoices_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Customer not Invoiced</span>
                        <tr class="bg-dark">
                            <th colspan="2">Customer</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($customer_not_invoiced as $invoice)
                            <tr>
                                <td colspan="2">{{$invoice->name}}</td>
                                {{-- <td></td> --}}
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($customer_not_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Other Income Planned</span>
                        <tr class="bg-dark">
                            <th>Account</th>
                            <th></th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($other_income as $invoice)
                            <tr>
                                <td colspan="2">{{$invoice->name}}</td>
                                {{-- <td></td> --}}
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Assignment Income</span>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Project</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($assignment_income as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->reference}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Pipeline</span>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Ref</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($pipeline_income as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->reference}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_income_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr class="bg-dark">
                                <th colspan="2"></th>
                                <th>Total</th>
                                <th>Overdue</th>
                                <th class="text-right">5</th>
                                <th class="text-right">10</th>
                                <th class="text-right">20</th>
                                <th class="text-right">30</th>
                                <th class="text-right">60</th>
                                <th class="text-right">90</th>
                                <th class="text-right">120</th>
                                <th class="text-right">150</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total Income</span></th>
                                <th class="text-right">{{number_format($total_income_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_150,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                    <br>

                    <!-- Expenses -->
                    <h5 style="font-weight: bold;">Expenses</h5>
                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Vendor Invoices</span>
                        <tr class="bg-dark">
                            <th>Vendor</th>
                            <th>Ref</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($vendor_invoiced as $invoice)
                                <tr>
                                    <td>{{$invoice->name}}</td>
                                    <td>{{$invoice->reference}}</td>
                                    <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Vendor not Invoiced</span>
                        <tr class="bg-dark">
                            <th colspan="2">Vendor</th>
                            {{-- <th></th> --}}
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($vendor_not_invoiced as $invoice)
                                <tr>
                                    <td colspan="2">{{$invoice->name}}</td>
                                    {{-- <td>{{$invoice->reference}}</td> --}}
                                    <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                    <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                                </tr>
                            @empty
                                <td colspan="12">&nbsp;</td>
                            @endforelse
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['total']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['overdue']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['5']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['10']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['20']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['30']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['60']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['90']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['120']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($vendor_not_invoiced_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Other Expenses Planned</span>
                        <tr class="bg-dark">
                            <th colspan="2">Account</th>
                            {{-- <th></th> --}}
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($other_expense as $invoice)
                            <tr>
                                <td colspan="2">{{$invoice->name}}</td>
                                {{-- <td>{{$invoice->reference}}</td> --}}
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($other_expense_total['total']), 2, '.', ',')}}</td>
                                <td style="font-weight: bold;" class="text-right">{{number_format(($other_expense_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($other_expense_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Assignment Expense</span>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Project</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($assignment_expense as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->reference}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($assignment_expense_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Expense Tracking and Employee Claims</span>
                        <tr class="bg-dark">
                            <th colspan="2">Account</th>
                            {{-- <th></th> --}}
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($expense_tracking_claims as $invoice)
                            <tr>
                                <td colspan="2">{{$invoice->name}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($expense_tracking_claims_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-sm">
                        <thead>
                        <br>
                        <span style="font-weight: bold;">Pipeline</span>
                        <tr class="bg-dark">
                            <th>Customer</th>
                            <th>Ref</th>
                            <th>Total</th>
                            <th>Overdue</th>
                            <th class="text-right">5</th>
                            <th class="text-right">10</th>
                            <th class="text-right">20</th>
                            <th class="text-right">30</th>
                            <th class="text-right">60</th>
                            <th class="text-right">90</th>
                            <th class="text-right">120</th>
                            <th class="text-right">150</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($pipeline_expenses as $invoice)
                            <tr>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->reference}}</td>
                                <td class="text-right">{{number_format(($invoice->total), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice->overdue), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[5]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[10]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[20]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[30]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[60]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[90]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[120]), 2, '.', ',')}}</td>
                                <td class="text-right">{{number_format(($invoice[150]), 2, '.', ',')}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['total']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['overdue']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['5']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['10']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['20']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['30']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['60']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['90']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['120']), 2, '.', ',')}}</td>
                                <td class="text-right" style="font-weight: bold;">{{number_format(($pipeline_expenses_total['150']), 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr class="bg-dark">
                                <th colspan="2"></th>
                                <th>Total</th>
                                <th>Overdue</th>
                                <th class="text-right">5</th>
                                <th class="text-right">10</th>
                                <th class="text-right">20</th>
                                <th class="text-right">30</th>
                                <th class="text-right">60</th>
                                <th class="text-right">90</th>
                                <th class="text-right">120</th>
                                <th class="text-right">150</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total Expenses</span></th>
                                <th class="text-right">{{number_format($total_expense_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_150,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                    <br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr class="bg-dark">
                                <th colspan="2"></th>
                                <th>Total</th>
                                <th>Overdue</th>
                                <th class="text-right">5</th>
                                <th class="text-right">10</th>
                                <th class="text-right">20</th>
                                <th class="text-right">30</th>
                                <th class="text-right">60</th>
                                <th class="text-right">90</th>
                                <th class="text-right">120</th>
                                <th class="text-right">150</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total Income</span></th>
                                <th class="text-right">{{number_format($total_income_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_income_150,2,'.',',')}}</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total Expenses</span></th>
                                <th class="text-right">{{number_format($total_expense_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_overdue,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_expense_150,2,'.',',')}}</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-right"><span style="font-weight: bold;">Total</span></th>
                                <th class="text-right">{{number_format($total_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($overdue_total,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_5,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_10,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_20,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_30,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_60,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_90,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_120,2,'.',',')}}</th>
                                <th class="text-right">{{number_format($total_150,2,'.',',')}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
