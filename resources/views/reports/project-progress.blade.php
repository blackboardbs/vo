@extends('adminlte.default')

@section('title') Project Progress Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('report.projectprogress', request()->all())}}{{count(request()->all()) > 0 ? '&' : '?'}}export" class="btn btn-info ml-1" type="submit"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
        <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '60')}}" />
        <div class="form-row w-100">
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Customers</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                            {!! Form::select('resource', $resource_drop_down2, request()->resource , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', [0=>"No", 1=>"Yes"], request()->billable , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('epic', $epic_drop_down, request()->epic , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Epic</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('feature', $feature_drop_down, request()->feature , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Feature</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('user_story', $user_stories_drop_down, request()->user_story , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>User Story</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('task',$task_drop_down, request()->task , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('invoice_status', $invoice_status_drop_down, request()->invoice_status , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_week', $weeks_drop_down, request()->from_week , ['class' => 'form-control search w-100 ', 'placeholder' => 'All']) !!}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_week', $weeks_drop_down, request()->to_week , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_month', $months_drop_down, request()->from_month , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_month',$months_drop_down, request()->to_month , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_dropdown, request()->cost_center , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 mb-1">
                <a href="{{route('report.projectprogress')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead class="bg-dark">
                    <tr>
                        <th class="align-top">Resource</th>
                        <th class="align-top">Epic</th>
                        <th class="align-top">Feature</th>
                        <th class="align-top">User Story</th>
                        <th class="align-top">Task</th>
                        <th class="align-top">Status</th>
                        <th class="align-top">Bill</th>
                        <th class="text-right align-top">Planned Hours</th>
                        <th class="text-right align-top">Actual Hours</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $planned_hours = 0;
                        $total = 0;
                    @endphp
                    @forelse($data as $progress)
                        <tr>
                            <td>{{$progress["resource"]??''}}</td>
                            <td>{{$progress["epic"] != '' ? $progress["epic"] : 'No (task/user story/feature/epic)'}}</td>
                            <td>{{$progress["feature"] != '' ? $progress["feature"] : 'No (task/user story/feature)'}}</td>
                            <td>{{$progress["user_story"] != '' ? $progress["user_story"] : 'No (task/user story)'}}</td>
                            <td>{{$progress["task_description"] != '' ? $progress["task_description"] : "No task"}}</td>
                            <td>{{$progress["task_status"] != '' ? $progress["task_status"] : 'No task'}}</td>
                            <td>{{$progress["billable"] == 1?"Yes":"No"}}</td>
                            <td class="text-right">{{number_format($progress["planned_hours"],2)}}</td>
                            <td class="text-right">{{number_format($progress["total_hours"],2)}}</td>
                        </tr>
                        @php
                            $planned_hours += $progress["planned_hours"];
                            $total += $progress["total_hours"];
                        @endphp
                    @empty
                        <tr>
                            <td class="text-center" colspan="9">There are no projects to show</td>
                        </tr>
                    @endforelse
                    @if(count($data) > 0)
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-uppercase text-right">total</th>
                            <th class="text-right">{{ number_format($planned_hours,2) }}</th>
                            <th class="text-right">{{ number_format($total,2) }}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '60'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $data->firstItem() }} - {{ $data->lastItem() }} of {{ $data->total() }}
                        </td>
                        <td>
                            {{ $data->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });
    </script>
@endsection
