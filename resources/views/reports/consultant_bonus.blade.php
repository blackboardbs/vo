@extends('adminlte.default')

@section('title')  Consulting Bonus per month report (R33) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{-- {!! Form::open(['url' => route('reports.consultant_bonus'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!} --}}
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control w-100 search']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $teams_dropdown, request()->team , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', $yes_or_no_dropdown, request()->billable , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-sm mb-3">
                <div class="form-group input-group">
                    <a href="{{route('reports.consultant_bonus')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </form>
        {{-- {!! Form::close() !!} --}}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>YearMonth</th>
                    <th>Customer Name</th>
                    <th>Assignment</th>
                    <th class="text-right">Hours</th>
                    <th class="text-right">Rate</th>
                    <th class="text-right">Value</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($commission["timesheets"] as $key => $timesheet)
                            @if($key == 0 || $timesheet->employee_id != $commission["timesheets"][$key - 1]->employee_id)
                                @php
                                    $total_hours = 0;
                                    $total_value = 0;
                                @endphp
                                <tr>
                                    <th colspan="6" class="bg-gray-light">{{ $timesheet->user->last_name.', '.$timesheet->user->first_name }}</th>
                                </tr>
                                <tr>
                                    <td>{{ $timesheet->year.'/'.$timesheet->month }}</td>
                                    <td>{{ ($timesheet->project != null) ?  $timesheet->project->customer->customer_name : 'Customer'  }}</td>
                                    <td><a href="{{route('assignment.show', $timesheet->id)}}">{{ ($timesheet->project != null) ? $timesheet->project->name.' ('.$timesheet->id.')' : 'Project X' }}</a></td>
                                    <td class="text-right">{{ number_format($timesheet->hours,0,'.',',') }}</td>
                                    <td class="text-right">{{ number_format($timesheet->rate,2,'.','.') }}</td>
                                    <td class="text-right">{{ number_format($timesheet->rate * $timesheet->hours, 2, '.', ',') }}</td>
                                </tr>
                                @php
                                    $total_hours += $timesheet->hours;
                                    $total_value += ($timesheet->hours * $timesheet->rate);
                                @endphp
                            @else
                                <tr>
                                    <td>{{ $timesheet->year.'/'.$timesheet->month }}</td>
                                    <td>{{ ($timesheet->project != null) ?  $timesheet->project->customer->customer_name : 'Customer'  }}</td>
                                    <td><a href="{{route('assignment.show', $timesheet->id)}}">{{ ($timesheet->project != null) ? $timesheet->project->name.' ('.$timesheet->id.')' : 'Project X' }}</a></td>
                                    <td class="text-right">{{ number_format($timesheet->hours,0,'.',',') }}</td>
                                    <td class="text-right">{{ number_format($timesheet->rate,2,'.','.') }}</td>
                                    <td class="text-right">{{ number_format($timesheet->rate * $timesheet->hours, 2, '.', ',') }}</td>
                                </tr>
                                @php
                                    $total_hours += $timesheet->hours;
                                    $total_value += ($timesheet->hours * $timesheet->rate);
                                @endphp
                            @endif
                            @if(isset($commission["timesheets"][$key + 1]->employee_id)?($timesheet->employee_id != $commission["timesheets"][$key + 1]->employee_id):'' || $key == (count($commission["timesheets"]) - 1))
                                <tr>
                                    <th colspan="3" class="text-right">TOTAL</th>
                                    <th class="text-right">{{ number_format($total_hours,0,'.',',') }}</th>
                                    <th class="text-right">{{ number_format((($total_hours > 0)?$total_value/$total_hours:0), 2, '.', ',') }}</th>
                                    <th class="text-right">{{ number_format($total_value,2,'.',',') }}</th>
                                </tr>
                            <tr>
                                <td colspan="6">
                                    <table class="table table-bordered table-sm mt-0">
                                        <thead>
                                        <tr>
                                            <th colspan="13">Commission Calc.</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <th>Hours</th>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour01??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour02??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour03??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour04??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour05??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour06??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour07??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour08??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour09??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour10??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour11??0 }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->hour12??0 }}</td>
                                        </tr>
                                        <tr>
                                            <th>Comm. %</th>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm01??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm02??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm03??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm04??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm05??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm06??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm07??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm08??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm09??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm10??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm11??"0"."%" }}</td>
                                            <td class="text-right">{{ $timesheet->resource->commission->comm12??"0"."%" }}</td>
                                        </tr>

                                        <tr>
                                            <th>Value</th>
                                            <td class="text-right">{{ number_format($commission["commission1"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission2"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission3"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission4"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission5"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission6"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission7"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission8"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission9"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission10"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission11"][$timesheet->employee_id],0,'.',',') }}</td>
                                            <td class="text-right">{{ number_format($commission["commission12"][$timesheet->employee_id],0,'.',',') }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="13">Minimum Rate = R {{ number_format($timesheet->resource->commission->min_rate??0, 2, '.', ',') }}</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @endif
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>

    </div>
@endsection
