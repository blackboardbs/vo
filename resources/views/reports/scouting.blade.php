@extends('adminlte.default')
@section('title') Scouting Report (R53) @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="reports.scouting"></x-export>
        <a href="{{route('scoutingpdf')}}" class="btn btn-dark mr-1 float-right" target="_blank"><i class="fa fa-download"></i> PDF</a>
        <button onclick="history.back()" class="btn btn-dark float-right mr-1"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
		<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('process_status',$process_statuses_drop_down,old('process_status'),['class'=>' form-control search', 'id' => 'process_status','style'=>'width: 100%;'])}}
                        <span>Process Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('process_interview',$process_interview_drop_down,old('process_interview'),['class'=>' form-control search', 'id' => 'process_interview', 'style'=>'width: 100%;'])}}
                        <span>Interview Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('role',$role_drop_down,old('role'),['class'=>' form-control search', 'id' => 'role', 'style'=>'width: 100%;'])}}
                        <span>Role</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('vendor',$vendor_drop_down,old('vendor'),['class'=>' form-control search', 'id' => 'vendor', 'style'=>'width: 100%;'])}}
                        <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource_level',$resource_level_drop_down,old('resource_level'),['class'=>' form-control search', 'id' => 'resource_level', 'style'=>'width: 100%;'])}}
                        <span>Resource Level</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('businees_rating',$business_rating_drop_down,old('businees_rating'),['class'=>' form-control search', 'id' => 'businees_rating', 'style'=>'width: 100%;'])}}
                        <span>Business Rating</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('technical_rating',$tecnical_rating_drop_down,old('technical_rating'),['class'=>' form-control search', 'id' => 'technical_rating', 'style'=>'width: 100%;'])}}
                        <span>Technical Rating</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <a href="{{route('reports.scouting')}}" class="btn w-100 btn-info">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('first_name', 'Resource Name')</th>
                    <th>@sortablelink('email', 'Email')</th>
                    <th>@sortablelink('processstatus.name', 'Process Status')</th>
                    <th>@sortablelink('interviewstatus.name', 'Interview Status')</th>
                    <th>@sortablelink('role.name', 'Role')</th>
                    <th>@sortablelink('vendor.vendor_name','Vendor')</th>
                    <th>@sortablelink('technicalrating.name', 'Technical Rating')</th>
                    <th>@sortablelink('businessrating.name', 'Business Rating')</th>
                    <th>@sortablelink('resourcelevel.description', 'Resource Level')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($scoutings as $scouting)
                    <tr>
                        <td><a href="{{route('scouting.show',$scouting)}}">{{$scouting->resource_first_name}} {{$scouting->resource_last_name}}</a></td>
                        <td>{{$scouting->email}}</td>
                        <td>{{isset($scouting->processstatus->name)?$scouting->processstatus->name:''}}</td>
                        <td>{{isset($scouting->interviewstatus->name)?$scouting->interviewstatus->name:''}}</td>
                        <td>{{isset($scouting->role->name)?$scouting->role->name:''}}</td>
                        <td>{{isset($scouting->vendor->vendor_name)?$scouting->vendor->vendor_name:''}}</td>
                        <td>{{isset($scouting->businessrating->name)?$scouting->businessrating->name:''}}</td>
                        <td>{{isset($scouting->technicalrating->name)?$scouting->technicalrating->name:''}}</td>
                        <td>{{isset($scouting->resourcelevel->description)?$scouting->resourcelevel->description:''}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td class="text-right" style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $scoutings->firstItem() }} - {{ $scoutings->lastItem() }} of {{ $scoutings->total() }}
                        </td>
                        <td class="text-left">
                            {{ $scoutings->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection