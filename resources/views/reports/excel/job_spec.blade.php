<table>
    <thead>
    <tr>
        <th>Reference Number</th>
        <th>Role</th>
        <th>Customer</th>
        <th>Client</th>
        <th>Position Type</th>
        <th>Placement By</th>
        <th>Application Closing Date</th>
        <th>BEE</th>
        <th>Location</th>
        <th>Job Status</th>
        <th>No of CV's Submitted</th>
        <th>Recruiter</th>
    </tr>
    </thead>
    <tbody>
    @foreach($job_specs as $spec)
        <tr>
            <td>{{$spec->reference_number}}</td>
            <td>{{isset($spec->positionrole)?$spec->positionrole->name:null}}</td>
            <td>{{isset($spec->customer->customer_name)?$spec->customer->customer_name:null}}</td>
            <td>{{$spec->client}}</td>
            <td>{{isset($spec->positiontype->description)?$spec->positiontype->description:null}}</td>
            <td>{{$spec->placement_by}}</td>
            <td>{{$spec->applicaiton_closing_date}}</td>
            <td>{{($spec->b_e_e == 1)?"Yes":"No"}}</td>
            <td>{{$spec->location}}</td>
            <td>{{($spec->status_id == 1)?"Open":"Closed"}}</td>
            <td>{{$cv_count[$spec->id]}}</td>
            <td>{{($spec->recruiter)?$spec->recruiter->first_name:null}}</td>
        </tr>
    @endforeach
    </tbody>
</table>