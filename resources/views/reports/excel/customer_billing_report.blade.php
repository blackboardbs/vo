<table>
    <thead>
    <tr>
        <th>Customer Name</th>
        <th>Project Name</th>
        <th>Resource</th>
        <th>Assignment</th>
        <th>PO Number</th>
        <th>Rate</th>
        <th>PO Hours</th>
        <th>PO Value</th>
        <th>Billable Hours</th>
        <th>Non-Billable Hours</th>
        <th>Billable Labour Value</th>
        <th>Billable Expenses Value</th>
        <th>Billable Claims Value</th>
        <th>Outstanding Hours</th>
        <th>Outstanding Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cust_billing_report as $key => $cust_bill_report)
        <tr>
            <td>{{ $cust_bill_report->customer?->customer_name }}</td>
            <td>{{ $cust_bill_report->project?->name }}</td>
            <td>{{ $cust_bill_report->employee?->name() }}</td>
            <td>{{ $cust_bill_report->id }}</td>
            <td>{{ $cust_bill_report->customer_po }}</td>
            <td>{{ $cust_bill_report->invoice_rate }}</td>
            <td>{{ number_format($cust_bill_report->hours,2,'.',',') }}</td>
            <td>{{ number_format(($cust_bill_report->hours * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
            <td>{{ number_format($cust_bill_report->bhours,2,'.',',') }}</td>
            <td>{{ number_format($cust_bill_report->nbhours,2,'.',',') }}</td>
            <td>{{ number_format(($cust_bill_report->bhours * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
            <td>{{number_format($cust_bill_exp[$key],2,'.',',')}}</td>
            <td>{{number_format($cust_bill_exp_tracking[$key],2,'.',',')}}</td>
            <td>{{ number_format(($cust_bill_report->hours - $cust_bill_report->bhours),2,'.',',') }}</td>
            <td>{{ ((($cust_bill_report->hours - $cust_bill_report->bhours) * $cust_bill_report->invoice_rate) < 0)? number_format(0 ,2,'.',','):number_format((($cust_bill_report->hours - $cust_bill_report->bhours) * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
        </tr>
    @endforeach

    <tr>
        <th colspan="6">Total</th>
        <th>{{number_format($total_po_hours,2,'.',',')}}</th>
        <th>{{number_format($total_po_value,2,'.',',')}}</th>
        <th>{{number_format($total_bill_hours,2,'.',',')}}</th>
        <th>{{number_format($total_non_bill_hours,2,'.',',')}}</th>
        <th>{{number_format($total_bill_value,2,'.',',')}}</th>
        <th>{{number_format($total_bill_exp,2,'.',',')}}</th>
        <th>{{number_format($total_claim_exp,2,'.',',')}}</th>
        <th>{{number_format($total_outstanding_hours,2,'.',',')}}</th>
        <th>{{number_format($total_outstanding_value,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>