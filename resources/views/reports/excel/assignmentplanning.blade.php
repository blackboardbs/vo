<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Project</th>
        <th>Assignment ID</th>
        <th>Start</th>
        <th>End</th>
        <th>PO Hours</th>
        <th>Actual Hours</th>
        <th>Forecasts Hours</th>
        <th>Planned Hours</th>
        <th>Variance</th>
        <th>Period</th>
        @foreach($year_week_array as $period)
            <th>{{$period}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @php
        $total_po_hours = 0;
        $total_actual_hours = 0;
        $total_hours_forecast = 0;
        $total_planned_hours = 0;
        $total_variance = 0;
    @endphp
    @foreach($assignments as $key => $assignment)
        @php
            $total_po_hours += $assignment->hours;
            $total_actual_hours += $actual_hours[$key]->hours;
            $total_hours_forecast += $hours_forecast[$key];
            $total_planned_hours += $actual_hours[$key]->hours + $hours_forecast[$key];
            $total_variance += $variance_po[$key];
        @endphp
        <tr>
            <td>{{$assignment->resource?->name()}}</td>
            <td>{{ $assignment->project->name }}</td>
            <td>{{$assignment->id}}</td>
            <td>{{ $assignment->start_date }}</td>
            <td>{{ $assignment->end_date }}</td>
            <td>{{ $assignment->hours }}</td>
            <td>{{ number_format($actual_hours[$key]->hours,2,'.',',') }}</td>
            <td>{{ number_format($hours_forecast[$key],0,'.',',') }}</td>
            <td>{{number_format($actual_hours[$key]->hours + $hours_forecast[$key],2,'.',',')}}</td>
            <td>{{ number_format($variance_po[$key] ,2,'.',',') }}</td>
            <td>
                {{--{{isset($forecast_column_data[$key])?$forecast_column_data[$key]:''}}--}}
                <span style="color: blue">[PO Hours]</span> <span style="color: green">[Actual hours]</span> <span style="color: red">[Forecast Hours]</span>
            </td>
            @foreach($assignment_actual_hours_array[$key] as $index2 => $assignment_actual_hours)
                <th>
                    <span style="color: green">{{number_format($assignment_actual_hours->hours ,2,'.',',') != 0? '['. number_format($assignment_actual_hours->hours ,2,'.',',') .']' : ''}}</span> <span style="color: red">{{$assignment_forecast_hours_array[$key][$index2] != 0 || $assignment_forecast_hours_array[$key][$index2] ? '[' .$assignment_forecast_hours_array[$key][$index2] .']' : ''}}</span>
                </th>
            @endforeach
        </tr>
    @endforeach
    <tr>
        <th colspan="4"></th>
        <th>{{ $total_po_hours }}</th>
        <th>{{ number_format($total_actual_hours,2,'.',',') }}</th>
        <th>{{ $total_hours_forecast }}</th>
        <th>{{ $total_planned_hours }}</th>
        <th>{{ $total_variance }}</th>
        <th></th>
        @foreach($year_week_array as $period)
            <th></th>
        @endforeach
    </tr>
    </tbody>
</table>