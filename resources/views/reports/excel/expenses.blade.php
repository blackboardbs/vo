<table>
    <thead>
    <tr>
        <th colspan="11">Resource</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total = 0;
        $total_claim = 0;
        $total_bill = 0;
    @endphp
    @foreach($resources as $index => $results)
        <tr>
            <td>{{$results->name()}}</td>
            <td colspan="10">
                <table>
                    <thead>
                    <tr>
                        <th>Project</th>
                        <th>Transaction Date</th>
                        <th>Account</th>
                        <th>Account Element</th>
                        <th>Description</th>
                        <th>Claim Amount</th>
                        <th>Billable Amount</th>
                        <th>Claim Approved Date</th>
                        <th>Claim Paid Date</th>
                        <th>Outstanding Claim Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $total_billable_amount = 0;
                        $total_claimable_amount = 0;
                        $total_outstanding_claim_amount = 0;
                    @endphp
                    @forelse($expenses[$index] as $expense)
                        <tr>
                            <td>{{$expense->timesheet?->project?->name}}</td>
                            <td>{{$expense->date}}</td>
                            <td>{{$expense->account?->description}}</td>
                            <td>{{$expense->account_element?->description}}</td>
                            <td>{{$expense->description}}</td>
                            <td>{{($expense->claim == 1)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</td>
                            <td>{{($expense->is_billable == 1)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</td>
                            <td>{{($expense->claim_auth_date != null)?$expense->claim_auth_date:'0000-00-00'}}</td>
                            <td>{{($expense->paid_date != null)?$expense->paid_date:'0000-00-00'}}</td>
                            <td>{{($expense->claim == 1 && $expense->paid_date == null)?number_format($expense->amount,2,'.',','):number_format(0,2,'.',',')}}</td>
                        </tr>
                        @php
                            $total_billable_amount += ($expense->is_billable == 1)?$expense->amount:0;
                            $total_claimable_amount += ($expense->claim == 1)?$expense->amount:0;
                            $total_outstanding_claim_amount += ($expense->claim == 1 && $expense->paid_date == null)?$expense->amount:0;
                        @endphp
                    @empty
                    @endforelse
                    <tr>
                        <th colspan="5">TOTAL</th>
                        <th>{{number_format($total_claimable_amount, 2,'.',',')}}</th>
                        <th>{{number_format($total_billable_amount, 2,'.',',')}}</th>
                        <th colspan="2"></th>
                        <th>{{number_format($total_outstanding_claim_amount, 2,'.',',')}}</th>
                    </tr>
                    </tbody>
                </table>

                {{ $expenses[$index]->appends(request()->except(substr($results->first_name,0,4).'_'.substr($results->last_name,0,1)))->links() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>