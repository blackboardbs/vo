<table>
    <thead>
    <tr>
        <th>Function</th>
        <th>Period</th>
        <th>Hours</th>
        <th>Value</th>
        <th>%</th>
    </tr>
    </thead>
    <tbody>

    @foreach($functions as $function)

        <tr>
            <td>{{$function->function_desc?->description}}</td>
            <td>{{$function->year.'/'.(($function->month < 10)?'0'.$function->month:$function->month)}}</td>
            <td>{{number_format($function->hours,2,'.',',')}}</td>
            <td>{{number_format($function->value,2,'.',',')}}</td>
            <td>{{($function->value > 0 && $total_value > 0)?number_format((($function->value/$total_value)*100),0,'.',','):0}}%</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="2">TOTAL</th>
        <th>{{number_format($total_hours,2,'.',',')}}</th>
        <th>{{number_format($total_value,2,'.',',')}}</th>
        <th></th>
    </tr>
    </tbody>
</table>