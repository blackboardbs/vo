<table>
    <thead>
    <tr>
        <th>Project</th>
        <th>Consultant</th>
        <th>Coach / Line Manager</th>
        <th>Actual Hours</th>
        <th>Forecast Hours</th>
        <th>Planned Hours</th>
        <th>Variance Hours</th>
        <th>Actual Spend</th>
        <th>Forecast Spend</th>
        <th>Budget</th>
        <th>Budget Variance</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_actual_hours = 0;
        $total_forecast_hours = 0;
        $total_planned_hours = 0;
        $total_variance = 0;
        $total_forecast_spend = 0;
        $total_actual_spent = 0;
        $total_forecast_spent = 0;
        $total_total_project_budget = 0;
    @endphp
    @foreach($assignments as $index => $assignment)
        @php
            $total_actual_hours += $actual_hours[$index]->hours;
            $total_forecast_hours += $hours_forecast[$index];
            $total_planned_hours += $actual_hours[$index]->hours + $hours_forecast[$index];
            $total_variance += $variance_po[$index];
            $total_actual_spent += $actual_hours[$index]->hours * $invoice_rate[$index];
            $total_forecast_spent += $hours_forecast[$index] * $invoice_rate[$index];
            $total_total_project_budget += $total_project_budget[$index]->value;
        @endphp
        <tr>
            <td>{{isset($assignment->project->name) ? $assignment->project->name: ''}}</td>
            <td>{{isset($assignment->consultant->first_name)? $assignment->consultant->first_name.' '.$assignment->consultant->last_name: 'Consultant' }}</td>
            <td>{{ $assignment->report_to }}</td>
            <td>{{number_format($actual_hours[$index]->hours,2,'.',',')}}</td>
            <td>{{number_format($hours_forecast[$index],2,'.',',')}}</td>
            <td>{{ number_format($actual_hours[$index]->hours + $hours_forecast[$index],2,'.',',') }}</td>
            <td>{{ number_format($variance_po[$index] ,2,'.',',') }}</td>
            <td>{{number_format(($actual_hours[$index]->hours * $invoice_rate[$index]) ,2,'.',',')}}</td>
            <td>{{number_format(($hours_forecast[$index] * $invoice_rate[$index]),2,'.',',')}}</td>
            <td>{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
            <td>{{number_format($total_project_budget[$index]->value,2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th>Total</th>
        <th></th>
        <th></th>
        <th>{{number_format($total_actual_hours,2,'.',',')}}</th>
        <th>{{number_format($total_forecast_hours,2,'.',',')}}</th>
        <th>{{number_format($total_planned_hours,2,'.',',')}}</th>
        <th>{{number_format($total_variance,2,'.',',')}}</th>
        <th>{{number_format($total_actual_spent,2,'.',',')}}</th>
        <th>{{number_format($total_forecast_spent,2,'.',',')}}</th>
        <th>{{number_format($total_total_project_budget,2,'.',',')}}</th>
        <th>{{number_format($total_total_project_budget,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>