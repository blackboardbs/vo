<table>
    <thead>
    <tr>
        <th colspan="9">Customer Invoiced, not paid and due in x days</th>
    </tr>
    <tr>
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Yearwk</th>
        <th>5</th>
        <th>10</th>
        <th>20</th>
        <th>30</th>
        <th>30+</th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoiced as $invoice)
        <tr>
            <td>{{isset($invoice->customer->customer_name)?$invoice->customer->customer_name:null}}</td>
            <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
            <td>{{isset($invoice->user)?substr($invoice->user->first_name,0,1).'. '.$invoice->user->last_name:null}}</td>
            <td>{{$invoice->year_week}}</td>
            <td>{{number_format(($invoice->five_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->ten_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->twenty_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->thirty_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->thirty_days_plus * $invoice->rate), 2, '.', ',')}}</td>
        </tr>
        @php
            $total_5_invoiced += ($invoice->five_days * $invoice->rate);
            $total_10_invoiced += ($invoice->ten_days * $invoice->rate);
            $total_20_invoiced += ($invoice->twenty_days * $invoice->rate);
            $total_30_invoiced += ($invoice->thirty_days * $invoice->rate);
            $total_30_plus_invoiced += ($invoice->thirty_days_plus * $invoice->rate);
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th class="text-right">{{number_format($total_5_invoiced,2,'.',',')}}</th>
        <th class="text-right">{{number_format($total_10_invoiced,2,'.',',')}}</th>
        <th class="text-right">{{number_format($total_20_invoiced,2,'.',',')}}</th>
        <th class="text-right">{{number_format($total_30_invoiced,2,'.',',')}}</th>
        <th class="text-right">{{number_format($total_30_plus_invoiced,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="7">Customer Not invoiced - billable amount including expenses</th>
    </tr>
    <tr>
        <th>Customer</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Yearwk</th>
        <th>Expenses (Bill)</th>
        <th>Labour</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($not_invoiced as $invoice)
        <tr>
            <td>{{isset($invoice->customer->customer_name)?$invoice->customer->customer_name:null}}</td>
            <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
            <td>{{isset($invoice->user)?$invoice->user->first_name.' '.$invoice->user->last_name:null}}</td>
            <td>{{$invoice['year_week']}}</td>
            <td>{{number_format((isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0),2,'.',',')}}</td>
            <td>{{number_format(($invoice->hours * $invoice->rate),2,'.',',')}}</td>
            <td>{{number_format((($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0)),2,'.',',')}}</td>
        </tr>
        @php
            $total_expenses += (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0);
            $total_labour += ($invoice->hours * $invoice->rate);
            $total += (($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0));
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>{{number_format($total_expenses,2,'.',',')}}</th>
        <th>{{number_format($total_labour,2,'.',',')}}</th>
        <th>{{number_format($total,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="9">Vendor Invoiced, not paid and due in x days</th>
    </tr>
    <tr>
        <th>Vendor</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Yearwk</th>
        <th>5</th>
        <th>10</th>
        <th>20</th>
        <th>30</th>
        <th>30+</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vendor_invoiced as $invoice)
        <tr>
            <td>{{isset($invoice->user->vendor->vendor_name)?$invoice->user->vendor->vendor_name:null}}</td>
            <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
            <td>{{isset($invoice->user)?substr($invoice->user->first_name,0,1).'. '.$invoice->user->last_name:null}}</td>
            <td>{{$invoice['year_week']}}</td>
            <td>{{number_format(($invoice->five_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->ten_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->twenty_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->thirty_days * $invoice->rate), 2, '.', ',')}}</td>
            <td>{{number_format(($invoice->thirty_days_plus * $invoice->rate), 2, '.', ',')}}</td>
        </tr>
        @php
            $vendor_total_5_invoiced += ($invoice->five_days * $invoice->rate);
            $vendor_total_10_invoiced += ($invoice->ten_days * $invoice->rate);
            $vendor_total_20_invoiced += ($invoice->twenty_days * $invoice->rate);
            $vendor_total_30_invoiced += ($invoice->thirty_days * $invoice->rate);
            $vendor_total_30_plus_invoiced += ($invoice->thirty_days_plus * $invoice->rate);
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>{{number_format($vendor_total_5_invoiced,2,'.',',')}}</th>
        <th>{{number_format($vendor_total_10_invoiced,2,'.',',')}}</th>
        <th>{{number_format($vendor_total_20_invoiced,2,'.',',')}}</th>
        <th>{{number_format($vendor_total_30_invoiced,2,'.',',')}}</th>
        <th>{{number_format($vendor_total_30_plus_invoiced,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="7">Vendor Not invoiced - billable amount including expenses</th>
    </tr>
    <tr>
        <th>Vendor</th>
        <th>Project</th>
        <th>Resource</th>
        <th>Yearwk</th>
        <th>Expenses (Claim)</th>
        <th>Labour</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vendor_not_invoiced as $invoice)
        <tr>
            <td>{{isset($invoice->user->vendor->vendor_name)?$invoice->user->vendor->vendor_name:null}}</td>
            <td>{{isset($invoice->project->name)?$invoice->project->name:null}}</td>
            <td>{{isset($invoice->user)?$invoice->user->first_name.' '.$invoice->user->last_name:null}}</td>
            <td>{{$invoice['year_week']}}</td>
            <td>{{number_format((isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0),2,'.',',')}}</td>
            <td>{{number_format(($invoice->hours * $invoice->rate),2,'.',',')}}</td>
            <td>{{number_format((($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0)),2,'.',',')}}</td>
        </tr>
        @php
            $vendor_total_expenses += (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0);
            $vendor_total_labour += ($invoice->hours * $invoice->rate);
            $vendor_total += (($invoice->hours * $invoice->rate) + (isset($invoice->time_exp[0]->time_expense)?$invoice->time_exp[0]->time_expense:0));
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>{{number_format($vendor_total_expenses,2,'.',',')}}</th>
        <th>{{number_format($vendor_total_labour,2,'.',',')}}</th>
        <th>{{number_format($vendor_total,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="6">Expense Tracking not linked to projects</th>
    </tr>
    <tr>
        <th>Account</th>
        <th>Account Element</th>
        <th>Resource</th>
        <th>Yearwk</th>
        <th>Expenses (Claim)</th>
        <th>Expenses (Bill)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($expense_tracking as $expense)
        <tr>
            <td>{{($expense->account)?$expense->account->description:''}}</td>
            <td>{{($expense->account_element)?$expense->account_element->description:''}}</td>
            <td>{{($expense->resource)?substr($expense->resource->first_name,0,1).'. '.$expense->resource->last_name : ''}}</td>
            <td>{{$expense->yearwk??''}}</td>
            <td>{{number_format($expense->claim_expense,2,'.',',')}}</td>
            <td>{{number_format($expense->billable_expense,2,'.',',')}}</td>
        </tr>
        @php
            $total_claim_expense += $expense->claim_expense;
            $total_bill_expense += $expense->billable_expense;
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>{{number_format($total_claim_expense,2,'.',',')}}</th>
        <th>{{number_format($total_bill_expense,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="8">Other Planned Expenses (next 60 days)</th>
    </tr>
    <tr>
        <th>Account</th>
        <th>Account Element</th>
        <th>Total</th>
        <th>5</th>
        <th>10</th>
        <th>20</th>
        <th>30</th>
        <th>30+</th>
    </tr>
    </thead>
    <tbody>
    @foreach($planned_expenses as $expense)
        <tr>
            <td>{{isset($expense->account->description)?$expense->account->description:null}}</td>
            <td>{{isset($expense->account_element->description)?$expense->account_element->description:null}}</td>
            <td>{{number_format(($expense->five_days + $expense->ten_days + $expense->twenty_days + $expense->thirty_days + $expense->thirty_days_plus),2,'.',',')}}</td>
            <td>{{number_format($expense->five_days,2,'.',',')}}</td>
            <td>{{number_format($expense->ten_days,2,'.',',')}}</td>
            <td>{{number_format($expense->twenty_days,2,'.',',')}}</td>
            <td>{{number_format($expense->thirty_days,2,'.',',')}}</td>
            <td>{{number_format($expense->thirty_days_plus,2,'.',',')}}</td>
        </tr>
        @php
            $planned_expense_5 += $expense->five_days;
            $planned_expense_10 += $expense->ten_days;
            $planned_expense_20 += $expense->twenty_days;
            $planned_expense_30 += $expense->thirty_days;
            $planned_expense_30_plus += $expense->thirty_days_plus;
        @endphp
    @endforeach
    <tr>
        <th></th>
        <th>Total</th>
        <th>{{number_format(($planned_expense_5 + $planned_expense_10 + $planned_expense_20 + $planned_expense_30 + $planned_expense_30_plus),2,'.',',')}}</th>
        <th>{{number_format($planned_expense_5,2,'.',',')}}</th>
        <th>{{number_format($planned_expense_10,2,'.',',')}}</th>
        <th>{{number_format($planned_expense_20,2,'.',',')}}</th>
        <th>{{number_format($planned_expense_30,2,'.',',')}}</th>
        <th>{{number_format($planned_expense_30_plus,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th colspan="3">Commission Payable (next 30 days)</th>
    </tr>
    <tr>
        <th>Description</th>
        <th>< 30 Days</th>
        <th>30+ Days</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Commission Last Month</td>
        <td>{{number_format($commission_last_month,2,'.',',')}}</td>
        <td>{{number_format(0,2,'.',',')}}</td>
    </tr>
    <tr>
        <td>Commission Month to Date</td>
        <td>{{number_format(0,2,'.',',')}}</td>
        <td>{{number_format($commission_this_month,2,'.',',')}}</td>
    </tr>
    </tbody>
</table>