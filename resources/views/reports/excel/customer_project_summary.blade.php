<table>
    <thead>
    <tr>
        <th>Project</th>
        <th>Customer</th>
        <th>PO Number</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Billable Hours</th>
        <th>Non-Billable Hours</th>
        <th>PO Hours</th>
        <th>PO Value</th>
        <th>Outstanding Hours</th>
        <th>Outstanding Value</th>
        <th>Completion %</th>
    </tr>
    </thead>
    <tbody>
    @foreach($projects as $project)
        <tr>
            <td>{{$project->project?->name}}</td>
            <td>{{$project->customer?->customer_name}}</td>
            <td>{{$project->project?->customer_po}}</td>
            <td>{{$project->project?->status?->description}}</td>
            <td>{{$project->project?->start_date}}</td>
            <td>{{$project->project?->end_date}}</td>
            <td>{{number_format($project->billable_hours,2,'.',',')}}</td>
            <td>{{number_format($project->non_billable_hours,2,'.',',')}}</td>
            <td>{{number_format($project->hours,2,'.',',')}}</td>
            <td>{{number_format(($project->hours * $project->invoice_rate),2,'.',',')}}</td>
            <td>{{number_format(($project->hours - $project->billable_hours),2,'.',',')}}</td>
            <td>{{(($project->hours - $project->billable_hours) >= 0)?number_format((($project->hours - $project->billable_hours)*$project->invoice_rate),2,'.',','):number_format(0,2,'.',',')}}</td>
            <td>{{($project->billable_hours > 0 && $project->hours > 0)?number_format((($project->billable_hours/$project->hours)*100),0,'.',','):0}}%</td>
        </tr>
    @endforeach

    <tr>
        <th colspan="6">TOTAL</th>
        <th>{{ number_format($total_billable_hours,2,'.',',') }}</th>
        <th>{{number_format($total_non_billable_hour,2,'.',',')}}</th>
        <th>{{number_format($total_po_hours,2,'.',',')}}</th>
        <th>{{number_format($total_po_value,2,'.',',')}}</th>
        <th>{{number_format($total_outstanding_hours,2,'.',',')}}</th>
        <th>{{number_format($total_outstanding_value,2,'.',',')}}</th>
        <th>{{number_format($total_percentage,0,'.',',')}}%</th>
    </tr>
    </tbody>
</table>