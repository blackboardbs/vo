<table>
    <thead>
    <tr>
        <th>Customer</th>
        <th>Project</th>
        <th>Assignment ID</th>
        <th>Assignment Status</th>
        <th>Billable</th>
        <th>Start</th>
        <th>End</th>
        <th>Capacity</th>
        <th>PO Hours</th>
        <th>Actual Hours</th>
        <th>Hours Left to Spend</th>
        <th>Days before Assignment End Date</th>
        <th>%Completion</th>
        <th>Planned Labour</th>
        <th>Planned Expenses</th>
        <th>Planned Income</th>
        <th>Planned Profit</th>
        <th>Planned Profit%</th>
        <th>Actual Labour</th>
        <th>Actual Expenses</th>
        <th>Actual Income</th>
        <th>Actual Profit</th>
        <th>Actual Profit%</th>
        <th>Labour Variance</th>
        <th>Income variance</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_po_hours = 0;
        $total_actual_hours = 0;
        $total_hours_left_to_spend = 0;
        $total_days_before_assignment_end_date = 0;
        $total_completion_percentage = 0;
        $total_planned_labour = 0;
        $total_expense_amount = 0;
        $total_planned_income = 0;
        $total_planned_profit = 0;
        $total_planned_profit_percentage = 0;
        $total_actual_labour = 0;
        $total_actual_expenses = 0;
        $total_actual_income = 0;
        $total_actual_profit = 0;
        $total_actual_profit_percentage = 0;
        $total_labour_variance = 0;
        $total_income_variance = 0;
    @endphp
    @foreach($assignments as $key => $assignment)
        @php

            $po_hours = $assignment->hours;
            $actual_hours_ = $actual_hours[$key]->hours;
            $hours_left_to_spend = $assignment->hours - $actual_hours_;
            $completion_percentage = $assignment->hours == 0 ? 0 : $actual_hours[$key]->hours / $assignment->hours  * 100;
            $planned_labour = $assignment->hours * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            $planned_income = $assignment->hours * $assignment->invoice_rate;

            $planned_profit = ($assignment->hours * $assignment->invoice_rate) - ($assignment->hours * ($assignment->internal_cost_rate + $assignment->external_cost_rate)) - $total_expense_amount_array[$key];
            $planned_profit_percentage = ($assignment->hours * $assignment->invoice_rate) == 0 ? 0 : $planned_profit / ($assignment->hours * $assignment->invoice_rate) * 100;
            $actual_labour = $actual_hours_ * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            $actual_expenses = $total_actual_expense_claim_array[$key];
            $actual_income = $actual_hours_ * $assignment->invoice_rate;
            $actual_profit = $actual_income - $actual_labour - $actual_expenses;
            $actual_profit_percentage = $actual_income == 0 ? 0 : $actual_profit / $actual_income * 100;
            $labour_variance = $planned_labour - $actual_labour;
            $income_variance = $planned_income - $actual_income;

            $total_po_hours += $po_hours;
            $total_actual_hours += $actual_hours_;
            $total_hours_left_to_spend += $hours_left_to_spend;
            $total_days_before_assignment_end_date += $days_before_assignment_end_date_array[$key];
            $total_completion_percentage += $completion_percentage;
            $total_planned_labour += $planned_labour;
            $total_expense_amount += $total_expense_amount_array[$key];
            $total_planned_income += $planned_income;
            $total_planned_profit += $planned_profit;
            $total_planned_profit_percentage += $planned_profit_percentage;
            $total_actual_labour += $actual_labour;
            $total_actual_expenses += $actual_expenses;
            $total_actual_income += $actual_income;
            $total_actual_profit += $actual_profit;
            $total_actual_profit_percentage += $actual_profit_percentage;
            $total_labour_variance += $labour_variance;
            $total_income_variance += $income_variance;

        @endphp
        <tr>
            <td>{{ $customer_name[$key] }}</td>
            <td>{{ $assignment->project->name }}</td>
            <td>{{$assignment->id}}</td>
            <td>{{isset($assignment->statusd->description)?$assignment->statusd->description:''}}</td>
            <td>{{ $assignment->billable == 1 ? 'Yes' : 'No' }}</td>
            <td>{{ $assignment->start_date }}</td>
            <td>{{ $assignment->end_date }}</td>
            <td>{{ $capacity_allocation_hours_array[$key] }}</td>
            <td>{{ $po_hours }}</td>
            <td>{{ number_format($actual_hours_,2,'.',',') }}</td>
            <td>{{ number_format($hours_left_to_spend,2,'.',',') }}</td>
            <td>{{ $days_before_assignment_end_date_array[$key] }}</td>
            <td>{{ number_format($completion_percentage, 2, '.', ',') }}</td>
            <td>{{ number_format($planned_labour,2,'.',',') }}</td>
            <td>{{ number_format($total_expense_amount_array[$key],2,'.',',') }}</td>
            <td>{{ number_format($planned_income,2,'.',',') }}</td>
            <td>{{ number_format($planned_profit, 2, '.', ',') }}</td>
            <td>{{ number_format($planned_profit_percentage, 2, '.', ',') }}</td>
            <td>{{ number_format($actual_labour, 2, '.', ',') }}</td>
            <td>{{ number_format($actual_expenses, 2, '.', ',') }}</td>
            <td>{{ number_format($actual_income, 2, '.', ',') }}</td>
            <td>{{ number_format($actual_profit, 2, '.', ',') }}</td>
            <td>{{ number_format($actual_profit_percentage, 2, '.', ',') }}</td>
            <td>{{ number_format($labour_variance, 2, '.', ',') }}</td>
            <td>{{ number_format($income_variance, 2, '.', ',') }}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="8"></th>
        <th>{{ number_format($total_po_hours,2,'.',',') }}</th>
        <th>{{number_format($total_actual_hours,2,'.',',')}}</th>
        <th>{{number_format($total_hours_left_to_spend,2,'.',',')}}</th>
        <th>{{$total_days_before_assignment_end_date}}</th>
        <th>{{number_format($total_completion_percentage,2,'.',',')}}</th>
        <th>{{number_format($total_planned_labour,2,'.',',')}}</th>
        <th>{{number_format($total_expense_amount,2,'.',',')}}</th>
        <th>{{number_format($total_planned_income,2,'.',',')}}</th>
        <th>{{number_format($total_planned_profit,2,'.',',')}}</th>
        <th>{{number_format($total_planned_profit_percentage,2,'.',',')}}</th>
        <th>{{number_format($total_actual_labour,2,'.',',')}}</th>
        <th>{{number_format($total_actual_expenses,2,'.',',')}}</th>
        <th>{{number_format($total_actual_income,2,'.',',')}}</th>
        <th>{{number_format($total_actual_profit,2,'.',',')}}</th>
        <th>{{number_format($total_actual_profit_percentage,2,'.',',')}}</th>
        <th>{{number_format($total_labour_variance,2,'.',',')}}</th>
        <th>{{number_format($total_income_variance,2,'.',',')}}</th>
    </tr>
    </tbody>
</table>