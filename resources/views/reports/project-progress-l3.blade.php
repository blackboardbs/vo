@extends('adminlte.default')

@section('title') Project Progress Summary Level 3 @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('report.projectprogress3', request()->all())}}{{count(request()->all()) > 0 ? '&' : '?'}}export" class="btn btn-info ml-1" type="submit"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
        <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '60')}}" />
        <div class="form-row w-100">
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource', $resource_drop_down2, request()->resource , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', [0=>"No", 1=>"Yes"], request()->billable , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('epic', $epic_drop_down, request()->epic , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Epic</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('feature', $feature_drop_down, request()->feature , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Feature</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('user_story', $user_stories_drop_down, request()->user_story , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>User Story</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('task',$task_drop_down, request()->task , ['class' => 'form-control  search w-100', 'placeholder' => 'All']) !!}
                        <span>Task</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('invoice_status', $invoice_status_drop_down, request()->invoice_status , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_week', $weeks_drop_down, request()->from_week , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_week', $weeks_drop_down, request()->to_week , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_month', $months_drop_down, request()->from_month , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_month',$months_drop_down, request()->to_month , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_drop_down, request()->cost_center , ['class' => 'form-control w-100 search', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-2">
                <div class="form-group input-group">
                    <a href="{{route('report.projectprogress3')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead class="bg-dark">
                    <tr>
                        <th class="align-top">Resource</th>
                        <th class="align-top">Epic</th>
                        <th class="align-top">Feature</th>
                        <th class="align-top">User Story</th>
                        <th class="align-top last">Bill</th>
                        <th class="text-right last">Planned Hours</th>
                        <th class="text-right last">Actual Hours</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $planned_hours = 0;
                        $total = 0;
                    @endphp
                    @forelse($data as $progress)
                        <tr>
                            <td>{{$progress["resource"]??''}}</td>
                            <td>{{$progress["epic"] != '' ? $progress["epic"] : 'No (task/user story/feature/epic)'}}</td>
                            <td>{{$progress["feature"] != '' ? $progress["feature"] : 'No (task/user story/feature)'}}</td>
                            <td>{{$progress["user_story"] != '' ? $progress["user_story"] : 'No (task/user story)'}}</td>
                            <td>{{$progress["billable"] == 1?"Yes":"No"}}</td>
                            <td class="text-right">{{ number_format($progress["planned_hours"]??0,2) }}</td>
                            <td class="text-right">{{ number_format($progress["total_hours"],2) }}</td>
                        </tr>
                        @php
                            $planned_hours += $progress["planned_hours"];
                            $total += $progress["total_hours"];
                        @endphp
                    @empty
                        <tr>
                            <td class="text-center" colspan="7">There are no projects to show</td>
                        </tr>
                    @endforelse
                    @if(count($data) > 0)
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-uppercase text-right">total</th>
                            <th class="text-right">{{ number_format($planned_hours,2) }}</th>
                            <th class="text-right">{{ number_format($total,2) }}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">        
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '60'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $data->firstItem() }} - {{ $data->lastItem() }} of {{ $data->total() }}
                        </td>
                        <td>
                            {{ $data->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });
        if (!getUrlParameters("billing_cycle_id")){
            $("#billing_period_id_chosen").hide();
        }else {
            getBillingPeriods();
        }

        $("body").on('change', '#billing_cycle_id', () => {
            getBillingPeriods();
            $("#billing_period_id_chosen").show();
        });

        function getBillingPeriods() {
            axios.get('/billing/'+$("#billing_cycle_id").val())
                .then(response => {
                    let option = '<option value="0">Select Billing Period</option>';
                    let selected = '{{request()->billing_period_id}}';
                    if (response.data.billing_cycle.billing_periods.length){
                        response.data.billing_cycle.billing_periods.forEach((v, i) => {
                            if (v.period_name.includes(v.year)){
                                option += '<option value="'+v.id+'" '+(v.id == selected ? "selected" : "")+'>'+v.period_name+'</option>';
                            } else {
                                option += '<option value="'+v.id+'" '+(v.id == selected ? "selected" : "")+'>'+v.period_name+" "+v.year+'</option>';
                            }
                        })
                        $("#billing_period_id").html(option).trigger("chosen:updated")
                    }
                }).catch(err => console.log(err.response));
        }

        function getUrlParameters(sParam) {
            let sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        }
    </script>
@endsection
