@extends('adminlte.default')

@section('title') Financial Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Report Code</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="{{route('expensetrackinginsight.index')}}">R27</a></td>
                        <td><a href="{{route('expensetrackinginsight.index')}}">Expense Tracking</a></td>
                        <td><a href="{{route('expensetrackinginsight.index')}}">Expense Tracking Summary by Element</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('expensetrackinginsight.account')}}">R28</a></td>
                        <td><a href="{{route('expensetrackinginsight.account')}}">Expense Tracking</a></td>
                        <td><a href="{{route('expensetrackinginsight.account')}}">Expense Tracking Summary by Account</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.consultant_expenses')}}">R29</a></td>
                        <td><a href="{{route('reports.consultant_expenses')}}">Expenses</a></td>
                        <td><a href="{{route('reports.consultant_expenses')}}">List consultant expenses</a></td>
                    </tr>
                    <tr>
                        <td><a href="{{route('reports.cashflow')}}">R34</a></td>
                        <td><a href="{{route('reports.cashflow')}}">Cashflow</a></td>
                        <td><a href="{{route('reports.cashflow')}}">List cash</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $("#search-1").on("keyup", function() {
            let value = $(this).val().toLowerCase();

            $("table tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    </script>
@endsection
