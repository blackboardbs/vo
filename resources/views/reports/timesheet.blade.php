@extends('adminlte.default')

@section('title') Timesheet Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <a href="{{route('report.timesheet', request()->all())}}{{(empty(request()->all()) ? '?' : '&')}}export" class="btn btn-info ml-1"><i class="fas fa-file-excel"></i> Export In Excel</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="row w-100">
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('resource', $resource_drop_down, request()->resource , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('epic', $epic_dropdown, request()->epic , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Epics</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('feature', $feature_dropdown, request()->feature , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Features</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('user_story', $user_stories_dropdown, request()->user_story , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>User Stories</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('task',$task_drop_down, request()->task , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Tasks</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('invoice_status', $invoice_status_dropdown, request()->invoice_status , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Invoice Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_week', $weeks_dropdown, request()->from_week , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Week From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_week', $weeks_dropdown, request()->to_week , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Week To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('from_month', $months_dropdown, request()->from_month , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Month From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('to_month',$months_dropdown, request()->to_month , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Month To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('cost_center', $cost_center_dropdown, request()->cost_center , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Cost Center</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', $yes_or_no_dropdown, request()->billable , ['class' => 'form-control search w-100', 'placeholder' => 'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billing_cycle_id', $billing_cycle_dropdown, request()->billing_cycle_id, ['class' => 'form-control search w-100', 'id' => 'billing_cycle_id', 'placeholder' => 'All']) !!}
                        <span>Billing Cycle</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billing_period_id', [], request()->billing_period_id, ['class' => 'form-control search w-100', 'id' => 'billing_period_id', 'placeholder' => 'All']) !!}
                        <span>Billing Period</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%">
                <div class="form-group input-group">
                    <a href="{{route('report.timesheet')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead class="bg-dark">
                    <tr>
                        <th class="text-capitalize text-center text-nowrap">Resource</th>
                        <th class="text-capitalize text-center text-nowrap">Project Name</th>
                        <th class="text-capitalize text-center text-nowrap">Task</th>
                        <th class="text-capitalize text-center text-nowrap">Description of work</th>
                        <th class="text-capitalize text-center text-nowrap">Wk</th>
                        <th class="text-capitalize text-center text-nowrap">Bill</th>
                        <th class="text-capitalize text-center text-nowrap">Mon</th>
                        <th class="text-capitalize text-center text-nowrap">Tue</th>
                        <th class="text-capitalize text-center text-nowrap">Wed</th>
                        <th class="text-capitalize text-center text-nowrap">Thu</th>
                        <th class="text-capitalize text-center text-nowrap">Fri</th>
                        <th class="text-capitalize text-center text-nowrap">Sat</th>
                        <th class="text-capitalize text-center text-nowrap">Sun</th>
                        <th class="text-capitalize text-center text-nowrap">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($timelines as $timeline)
                        <tr>
                            <td>{{$timeline->timesheet->user->resource??''}}</td>
                            <td>{{$timeline->timesheet->project->name??''}}</td>
                            <td>{{$timeline->task->description??''}}</td>
                            <td>{{$timeline->description_of_work}}</td>
                            <td>{{$timeline->timesheet->year_week??''}}</td>
                            <td>{{$timeline->is_billable?"Yes":"No"}}</td>
                            <td class="text-right">{{$timeline->time($timeline->monday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->tuesday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->wednesday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->thursday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->friday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->saturday)}}</td>
                            <td class="text-right">{{$timeline->time($timeline->sunday)}}</td>
                            <th class="text-right">{{$timeline->time($timeline->total)}}</th>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="13">No timesheets available</td>
                        </tr>
                    @endforelse

                    @if(!$timelines->isEmpty())
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th class="text-right text-uppercase">total</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('monday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('tuesday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('wednesday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('thursday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('friday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('saturday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('sunday'))}}</th>
                            <th class="text-right">{{$timeline->time($timelines->sum('total'))}}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $timelines->firstItem() }} - {{ $timelines->lastItem() }} of {{ $timelines->total() }}
                        </td>
                        <td>
                            {{ $timelines->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $('.chosen-container').css({
            width: '100%',
        });

        if (!getUrlParameters("billing_cycle_id")){
            $("#billing_period_id_chosen").hide();
        }else {
            getBillingPeriods();
        }

        $("body").on('change', '#billing_cycle_id', () => {
            getBillingPeriods();
            $("#billing_period_id_chosen").show();
        });

        function getBillingPeriods() {
            axios.get('/billing/'+$("#billing_cycle_id").val())
                .then(response => {
                    let option = '<option value="0">Select Billing Period</option>';
                    let selected = '{{request()->billing_period_id}}';
                    if (response.data.billing_cycle.billing_periods.length){
                        response.data.billing_cycle.billing_periods.forEach((v, i) => {
                            option += '<option value="'+v.id+'" '+(v.id == selected ? "selected" : "")+'>'+v.period_name+'</option>';
                        })
                        $("#billing_period_id").html(option).trigger("chosen:updated")
                    }
                }).catch(err => console.log(err.response));
        }

        function getUrlParameters(sParam) {
            let sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        }
    </script>
@endsection
