@extends('adminlte.default')

@section('title') Assignment report (R30) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.consultant_assignment'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::select('dates', $dates_dropdown, request()->dates , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Date from - Date to']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Consultant</th>
                    <th>Project</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Rate</th>
                    <th>Hours</th>
                    <th>Value</th>
                    <th>T/S Bill</th>
                    <th>Non-Bill</th>
                    <th>Hours Out</th>
                    <th>Value Out</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $total = 0;
                    $assignment_hours = 0;
                    $total_amount = 0;
                    $total_timesheet_billable = 0;
                    $total_timesheet_non_billable = 0;
                    $total_hours_out = 0;
                    $total_value_out = 0;
                @endphp
                @forelse($timesheet as $results)
                    @php
                        $vout = (($results->ahours - $results->bhours) < 0) ? 0 :($results->ahours - $results->bhours) * $results->invoice_rate;
                        $assignment_hours += $results->ahours;
                        $total_amount += $results->aamount;
                        $total_timesheet_billable += $results->bhours;
                        $total_timesheet_non_billable += $results->nbhours;
                        $total_hours_out += ($results->ahours - $results->bhours);
                        $total_value_out += $vout;
                    @endphp
                        <tr>
                            <td>{{ $results->resource->last_name.', '.$results->resource->first_name }}</td>
                            <td>{{ $results->project->name }}</td>
                            <td>{{ $results->start_date }}</td>
                            <td>{{ $results->end_date }}</td>
                            <td class="text-right">{{ $results->invoice_rate }}</td>
                            <td class="text-right">{{ $results->ahours }}</td>
                            <td class="text-right">{{ number_format($results->aamount,2,'.',',') }}</td>
                            <td class="text-right">{{ number_format($results->bhours,0,'.',',') }}</td>
                            <td class="text-right">{{ number_format($results->nbhours,0,'.',',') }}</td>
                            <td class="text-right">{{ number_format(($results->ahours - $results->bhours),0,'.',',') }}</td>
                            <td class="text-right">{{ (($results->ahours - $results->bhours) < 0 ) ? number_format(0,2,'.',',') : number_format((($results->ahours - $results->bhours) * $results->invoice_rate),2,'.',',') }}</td>
                        </tr>

                @empty
                    <tr>
                        <td class="text-center" colspan="8">No consultant expenses available</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="5">TOTAL</th>
                    <th class="text-right">{{ $assignment_hours }}</th>
                    <th class="text-right">{{ number_format($total_amount,2,'.',',') }}</th>
                    <th class="text-right">{{ $total_timesheet_billable }}</th>
                    <th class="text-right">{{ $total_timesheet_non_billable }}</th>
                    <th class="text-right">{{ $total_hours_out }}</th>
                    <th class="text-right">{{ number_format($total_value_out,2,'.',',') }}</th>
                </tr>
                </tbody>
            </table>
            {{ $timesheet->links() }}
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
