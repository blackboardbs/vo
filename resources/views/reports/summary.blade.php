@extends('adminlte.default')

@section('title') Summary Report (R41) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.consultant_summary'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::select('dates', $dates_dropdown, request()->dates , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Date from - Date to']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Period</th>
                    <th class="text-right">Bill Hr</th>
                    <th class="text-right">Non-Bill Hr</th>
                    <th class="text-right">Util %</th>
                    <th class="text-right">Value</th>
                    <th class="text-right">Ave Rate</th>
                    <th class="text-right">Leave</th>
                    <th class="text-right">Comm.</th>
                    <th class="text-right">Expenses</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($employee as  $resource)
                        <tr>
                            <th class="bg-gray-light" colspan="9">{{isset($resource->user) ? $resource->user->last_name.', '.$resource->user->first_name : 'Resource Name'}}</th>
                        </tr>
                        @php
                            $total_hours_bill = 0;
                            $total_hours_non_bill = 0;
                            $total_amount = 0;
                            $tatal_expenses = 0;
                        @endphp
                        @forelse($reports[$resource->employee_id] as $index =>$report)

                            <tr>
                                <td>{{ ($report->month < 10) ? $report->year.'0'.$report->month : $report->year.$report->month }}</td>
                                <td class="text-right">{{ number_format($report->hours_bill,0,'.','.') }}</td>
                                <td class="text-right">{{ number_format($report->hours_no_bill,0,'.',',') }}</td>
                                <td class="text-right">{{ number_format(($report->hours_bill == 0) ? 0 : (($report->hours_bill/160) * 100) ,0,'.',',') }}</td>
                                <td class="text-right">{{ number_format($report->hours_bill * $report->invoice_rate ,2,'.',',') }}</td>
                                <td class="text-right">{{ ($report->hours_bill == 0) ? number_format(0,0,'.',',') : number_format($report->amount / $report->hours_bill ,0,'.',',') }}</td>
                                <td class="text-right"></td>
                                <td class="text-right"></td>
                                <td class="text-right">{{number_format($expenses[$resource->employee_id][$index]->expense_claim,2,'.',',')}}</td>
                            </tr>
                            @php
                                $total_hours_bill += $report->hours_bill;
                                $total_hours_non_bill += $report->hours_no_bill;
                                $total_amount += $report->amount;
                                $tatal_expenses += $report->expense_claim;
                            @endphp
                            @empty
                        @endforelse
                        <tr>
                            <th>TOTAL</th>
                            <th class="text-right">{{number_format($total_hours_bill,0,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_hours_non_bill,0,'.',',')}}</th>
                            <th class="text-right">{{number_format(($total_hours_bill == 0) ? 0 : round((($total_hours_bill/(160 * count($reports[$resource->employee_id])))* 100),0),0,'.',',')}}</th>
                            <th class="text-right">{{number_format($total_amount,2,'.',',')}}</th>
                            <th class="text-right">{{number_format((($total_hours_bill == 0) ? 0 : $total_amount/$total_hours_bill),2,'.',',')}}</th>
                            <th class="text-right"></th>
                            <th class="text-right"></th>
                            <th class="text-right">{{number_format($tatal_expenses,2,'.',',')}}</th>
                        </tr>
                        <tr><td colspan="9">&nbsp;</td></tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">No summaries available.</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
