@extends('adminlte.default')

@section('title') Expenses Not Paid (R28) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(['url' => route('reports.expenses_not_paid'),'method' => 'get', 'class' => 'mb-0', 'id' => 'filter_results']) !!}
        <table class="table table-sm">
            <tr>
                <td>{!! Form::select('company', $company_dropdown, request()->company , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Company']) !!}</td>
                <td>{!! Form::select('team', $team_dropdown, request()->team , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Team']) !!}</td>
                <td>{!! Form::select('employee', $employee_dropdown, request()->employee , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Employee']) !!}</td>
                <td>{!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control form-control-sm ', 'placeholder' => 'YearMonth']) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::select('dates', $dates_dropdown, request()->dates , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Date from - Date to']) !!}</td>
                <td>{!! Form::select('customer', $customer_dropdown, request()->customer , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Customers']) !!}</td>
                <td>{!! Form::select('project', $project_dropdown, request()->project , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Projects']) !!}</td>
                <td>{!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control form-control-sm ', 'placeholder' => 'Billable']) !!}</td>
            </tr>
        </table>
        {!! Form::close() !!}
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Consultant</th>
                    <th>Project</th>
                    <th>YearWk - Time ID</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($timesheets as $timesheet)
                        <tr>
                            <td>{{ $timesheet->timesheet->employee->first_name.', '.$timesheet->timesheet->employee->last_name }}</td>
                            <td>{{ $timesheet->timesheet->project->name }}</td>
                            <td>{{ $timesheet->timesheet->year_week.' - '.$timesheet->timesheet_id }}</td>
                            <td>{{ number_format($timesheet->exp_amount,2,'.',',') }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">Nothing to show</td>
                        </tr>
                    @endforelse
                    @if(count($timesheets) > 0)
                        <tr>
                            <th colspan="3" class="text-right">TOTAL</th>
                            <th>{{ number_format($total_amount,2,'.',',') }}</th>
                        </tr>
                    @endif
                </tbody>
            </table>
            {{ $timesheets->links() }}
        </div>

    </div>
@endsection

@section('extra-js')

    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });
    </script>
@endsection
