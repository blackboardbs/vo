@extends('adminlte.default')

@section('title')  Assignment Report (R30_32) @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="reports.consultant_assignment"></x-export>
        <button onclick="history.back()" class="btn btn-dark float-right mr-1"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('company', $company_drop_down, request()->company , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('customer', $customer_drop_down, request()->customer , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('project', $project_drop_down, request()->project , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('employee', $resource_drop_down, request()->employee , ['class' => 'form-control w-100 search']) !!}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('team', $team_drop_down, request()->team , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Team</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('yearmonth', $yearmonth_dropdown, request()->yearmonth , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Year Month</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_from', request()->date_from , ['class' => 'form-control datepicker w-100 search']) !!}
                        <span>Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::text('date_to', request()->date_to , ['class' => 'form-control datepicker w-100 search']) !!}
                        <span>Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {!! Form::select('billable', ['1' => 'Yes', '0' => 'No'], request()->billable , ['class' => 'form-control w-100 search','placeholder'=>'All']) !!}
                        <span>Billable</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-3" style="max-width:20%">
                <div class="form-group input-group">
                    <a href="{{route('reports.consultant_assignment')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>Customer Name</th>
                    <th>Project Name</th>
                    <th>Resource</th>
                    <th class="text-right">Assignment</th>
                    <th>PO Number</th>
                    <th class="text-right">Rate</th>
                    <th>PO Hours</th>
                    <th class="text-right">PO Value</th>
                    <th class="text-right">Billable Hours</th>
                    <th class="text-right">Non-Billable Hours</th>
                    <th class="text-right">Billable Labour Value</th>
                    <th class="text-right">Billable Expenses Value</th>
                    <th class="text-right">Billable Claims Value</th>
                    <th class="text-right">Outstanding Hours</th>
                    <th class="text-right">Outstanding Value</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($cust_billing_report as $key => $cust_bill_report)
                        <tr>
                            <td>{{ isset($cust_bill_report->customer)?$cust_bill_report->customer->customer_name:'' }}</td>
                            <td>{{ isset($cust_bill_report->project)?$cust_bill_report->project->name:'' }}</td>
                            <td>{{ isset($cust_bill_report->employee)?$cust_bill_report->employee->last_name.' '.$cust_bill_report->employee->first_name:'' }}</td>
                            <td class="text-right">{{ $cust_bill_report->id }}</td>
                            <td>{{ $cust_bill_report->customer_po }}</td>
                            <td>{{ $cust_bill_report->invoice_rate }}</td>
                            <td>{{ number_format($cust_bill_report->hours,2,'.',',') }}</td>
                            <td class="text-right">{{ number_format(($cust_bill_report->hours * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
                            <td class="text-right">{{ number_format($cust_bill_report->bhours,2,'.',',') }}</td>
                            <td class="text-right">{{ number_format($cust_bill_report->nbhours,2,'.',',') }}</td>
                            <td class="text-right">{{ number_format(($cust_bill_report->bhours * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
                            <td class="text-right">{{number_format($cust_bill_exp[$key],2,'.',',')}}</td>
                            <td class="text-right">{{number_format($cust_bill_exp_tracking[$key],2,'.',',')}}</td>
                            <td class="text-right">{{ number_format(($cust_bill_report->hours - $cust_bill_report->bhours),2,'.',',') }}</td>
                            <td class="text-right">{{ ((($cust_bill_report->hours - $cust_bill_report->bhours) * $cust_bill_report->invoice_rate) < 0)? number_format(0 ,2,'.',','):number_format((($cust_bill_report->hours - $cust_bill_report->bhours) * $cust_bill_report->invoice_rate),2,'.',',') }}</td>
                        </tr>

                        @empty
                        <tr>
                            <td colspan="8" class="text-center">Nothing to show here</td>
                        </tr>
                    @endforelse

                    <tr>
                        <th colspan="6" class="text-right">Total</th>
                        <th>{{number_format($total_po_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_po_value,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_bill_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_non_bill_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_bill_value,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_bill_exp,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_claim_exp,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_outstanding_hours,2,'.',',')}}</th>
                        <th class="text-right">{{number_format($total_outstanding_value,2,'.',',')}}</th>
                    </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $cust_billing_report->firstItem() }} - {{ $cust_billing_report->lastItem() }} of {{ $cust_billing_report->total() }}
                        </td>
                        <td>
                            {{ $cust_billing_report->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection
