    @extends('adminlte.default')

@section('title') Assignment Planning Report (R61) @endsection

@section('header')
<div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <x-export route="reports.assignmentplanning"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('company',$company_drop_down,request()->company,['class'=>' form-control w-100 search', 'id' => 'company', 'placeholder' => 'All'])}}
                        <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project',$project_drop_down,request()->project,['class'=>' form-control w-100 search', 'id' => 'project', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource',$employee_drop_down,request()->resource,['class'=>' form-control w-100 search', 'id' => 'employee', 'placeholder' => 'All'])}}
                        <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('year',[2024 => 2024,2023 => 2023,2022 => 2022,2021 => 2021,2020 => 2020, 2018 => 2018, 2017 => 2017], request()->year,['class'=>'form-control w-100 search', 'id' => 'year', 'placeholder' => 'All'])}}
                        <span>Year</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mb-1" style="max-width:20%;">
                <div class="form-group input-group">
                    <a href="{{route('reports.assignmentplanning')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
            </div>
        </form>
        <hr />
        <div class="limiter">
            <div style="max-width: 100% !important;">
                <div class=" js-pscroll">
                    <div class="table-container">
                        <table class="table table-bordered">
                            <thead>
                                <tr class=" bg-dark">
                                    <th nowrap>Project</th>
                                    <th >Resource</th>
                                    <th >Assignment ID</th>
                                    <th >Start</th>
                                    <th >End</th>
                                    <th class=" text-right">PO Hours</th>
                                    <th class=" text-right">Actual Hours</th>
                                    <th class=" text-right">Forecasts Hours</th>
                                    <th class=" text-right">Planned Hours</th>
                                    <th class=" text-right">Variance</th>
                                    <th >Period</th>
                                    @foreach($year_week_array as $period)
                                        <th class=" text-right">{{$period}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $total_po_hours = 0;
                                $total_actual_hours = 0;
                                $total_hours_forecast = 0;
                                $total_planned_hours = 0;
                                $total_variance = 0;
                            @endphp
                                @forelse($assignments as $key => $assignment)
                                @php
                                $total_po_hours += $assignment->hours;
                                $total_actual_hours += $actual_hours[$key]->hours;
                                $total_hours_forecast += $hours_forecast[$key];
                                $total_planned_hours += $actual_hours[$key]->hours + $hours_forecast[$key];
                                $total_variance += $variance_po[$key];
                                @endphp
                                <tr class="">
                                    <td  style="width: 300px;">{{ $assignment->project->name }}</td>
                                    <td >
                                        {{isset($assignment->resource)?$assignment->resource->first_name.' '.$assignment->resource->last_name:''}}
                                    </td>
                                    <td >{{$assignment->id}}</td>
                                    <td >{{ $assignment->start_date }}</td>
                                    <td >{{ $assignment->end_date }}</td>
                                    <td class=" text-right">{{ $assignment->hours }}</td>
                                    <td class=" text-right">{{ number_format($actual_hours[$key]->hours,2,'.',',') }}</td>
                                    <td class=" text-right">{{ number_format($hours_forecast[$key],0,'.',',') }}</td>
                                    <td class=" text-right">{{number_format($actual_hours[$key]->hours + $hours_forecast[$key],2,'.',',')}}</td>
                                    <td class=" text-right">{{ number_format($variance_po[$key] ,2,'.',',') }}</td>
                                    <td >
                                        {{--{{isset($forecast_column_data[$key])?$forecast_column_data[$key]:''}}--}}
                                        <span style="color: blue">[PO Hours]</span> <span style="color: green">[Actual hours]</span> <span style="color: red">[Forecast Hours]</span>
                                    </td>
                                    @foreach($assignment_actual_hours_array[$key] as $index2 => $assignment_actual_hours)
                                        <th class=" text-right">
                                            <span style="color: green">{{number_format($assignment_actual_hours->hours ,2,'.',',') != 0? '['. number_format($assignment_actual_hours->hours ,2,'.',',') .']' : ''}}</span> <span style="color: red">{{$assignment_forecast_hours_array[$key][$index2] != 0 && $assignment_forecast_hours_array[$key][$index2] != '' ? '[' .$assignment_forecast_hours_array[$key][$index2] .']' : ''}}</span>
                                        </th>
                                    @endforeach
                                </tr>

                            @empty
                                <tr>
                                    <td class="text-center" colspan="8">No consultant expenses available</td>
                                </tr>
                            @endforelse
                                <tr class=" bg-dark">
                                    <th   colspan="5"></th>
                                    <th  class=" text-right">{{ $total_po_hours }}</th>
                                    <th  class=" text-right">{{ number_format($total_actual_hours,2,'.',',') }}</th>
                                    <th  class=" text-right">{{ $total_hours_forecast }}</th>
                                    <th  class=" text-right">{{ $total_planned_hours }}</th>
                                    <th  class=" text-right">{{ $total_variance }}</th>
                                    <th  class=" text-right"></th>
                                    @foreach($year_week_array as $period)
                                        <th ></th>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                        <div class=" col-md-12 text-center" style="align-items: center;">      
                            <table class="tabel tabel-borderless" style="margin: 0 auto">
                                <tr>
                                    <td style="vertical-align: center;">
                                        Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $assignments->firstItem() }} - {{ $assignments->lastItem() }} of {{ $assignments->total() }}
                                    </td>
                                    <td>
                                        {{ $assignments->appends(request()->except('page'))->links() }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <style>
        .table-container {
            overflow-x: scroll;
            max-width: 100%;
        }

        .table-container table {
            table-layout: fixed;
            width: auto;
        }

        .table-container th,
        .table-container td {
            white-space: nowrap;
            padding: 8px;
        }

        .table-container th:first-child,
        .table-container td:first-child {
            position: sticky;
            left: 0;
            z-index: 1;
        }
        .table-container td:first-child {
            background-color: #fff;
        }

        .table-container th:first-child {
            border-right: 1px solid #dee2e6;
        }

        .table-container td:first-child {
            border-right: 1px solid #dee2e6;
        }

    </style>
@endsection
@section('extra-js')

    <script src="{{asset('dist/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/select2/select2.min.js')}}"></script>
    <script src="{{asset('dist/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script>
        $(".").chosen();
        $('.chosen-container').css({
            width: '100%',
        });

        $('#filter_results select').on('change', () => {
            $('#filter_results').submit();
        });

        $('.js-pscroll').each(function(){
            var ps = new PerfectScrollbar(this);

            $(window).on('resize', function(){
                ps.update();
            })

            $(this).on('ps-x-reach-start', function(){
                $(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
            });

            $(this).on('ps-scroll-x', function(){
                $(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
            });

        });
    </script>
@endsection
