@extends('adminlte.default')

@section('title') View Task @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @foreach($task as $result)
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Task Number:</th>
                    <td>{{$result->task_number}}</td>
                    <th>Subject:</th>
                    <td>{{$result->subject}}</td>
                </tr>
                <tr>
                    <th>Description:</th>
                    <td colspan="3">{{$result->description}}</td>
                </tr>
                <tr>
                    <th>Due Date:</th>
                    <td>{{$result->due_date}}</td>
                    <th>Task User:</th>
                    <td>{{$result->user->first_name}} {{$result->user->last_name}}</td>
                </tr>
                <tr>
                    <th>Rejection Reason:</th>
                    <td>{{$result->rejection_reason}}</td>
                    <th>Rejection Date:</th>
                    <td>{{$result->rejection_date}}</td>
                </tr>
                <tr>
                    <th>Email Task:</th>
                    <td>{{Form::checkbox('email_task',1,$result->email_task)}}</td>
                    <th>Status:</th>
                    <td>{{$result->status->description}}</td>
                </tr>
                </tbody>
            </table>
            @endforeach
        </div>
    </div>
@endsection
