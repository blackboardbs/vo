@extends('adminlte.default')

@section('title') Add Task @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('resource_task.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Task Number:</th>
                    <td>{{Form::text('task_number',old('task_number'),['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'placeholder'=>'Task Number'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Subject: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('subject',old('subject'),['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''),'placeholder'=>'Subject'])}}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('description',old('description'),['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Due Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('due_date',$date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Due Date'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Task User: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('task_user',$task_user_dropdown,old('task_user'),['class'=>'form-control form-control-sm '])}}
                        @foreach($errors->get('task_user') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email Task:</th>
                    <td>{{Form::checkbox('email_task')}}
                        @foreach($errors->get('email_task') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$sidebar_process_statuses,1,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection