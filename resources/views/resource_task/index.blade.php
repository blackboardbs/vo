@extends('adminlte.default')

@section('title') Tasks @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('resource_task.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Task</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-25','placeholder'=>'Search...'])}}
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('task_number', 'Task Number')</th>
                    <th>@sortablelink('subject', 'Subject')</th>
                    <th>@sortablelink('description', 'Description')</th>
                    <th>@sortablelink('user.first_name', 'Task User')</th>
                    <th>@sortablelink('due_date','Due Date')</th>
                    <th>@sortablelink('status.description','Status')</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($task as $result)
                    <tr>
                        <td><a href="{{route('resource_task.show',$result)}}">{{$result->task_number}}</a></td>
                        <td>{{$result->subject}}</td>
                        <td>{{$result->description}}</td>
                        <td>{{$result->user?->name()}}</td>
                        <td>{{$result->due_date}}</td>
                        <td>{{$result->status?->description}}</td>
                        <td>
                            <a href="{{route('resource_task.edit',$result)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['resource_task.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $task->links() }}
        </div>
    </div>
@endsection
