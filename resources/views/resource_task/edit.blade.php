@extends('adminlte.default')

@section('title') Edit Task @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @foreach($task as $result)
            {{Form::open(['url' => route('resource_task.update',$result), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Task Number:</th>
                    <td>{{Form::text('task_number',$result->task_number,['class'=>'form-control form-control-sm'. ($errors->has('task_number') ? ' is-invalid' : ''),'placeholder'=>'Task Number'])}}
                        @foreach($errors->get('task_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Subject: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('subject',$result->subject,['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''),'placeholder'=>'Subject'])}}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('description',$result->description,['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Due Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('due_date',$result->due_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Due Date'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Task User: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('task_user',$task_user_dropdown,$result->user->id,['class'=>'form-control form-control-sm '])}}
                        @foreach($errors->get('task_user') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Rejection Reason:</th>
                    <td>{{Form::text('rejection_reason',$result->rejection_reason,['class'=>'form-control form-control-sm'. ($errors->has('rejection_reason') ? ' is-invalid' : ''),'placeholder'=>'Rejection Reason'])}}
                        @foreach($errors->get('rejection_reason') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Rejection Date:</th>
                    <td>{{Form::text('rejection_date',$result->rejection_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('rejection_date') ? ' is-invalid' : ''),'placeholder'=>'Rejection Date'])}}
                        @foreach($errors->get('rejection_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email Task:</th>
                    <td>{{Form::checkbox('email_task',1,$result->email_task)}}
                        @foreach($errors->get('email_task') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$sidebar_process_statuses,$result->status_id,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
                @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection