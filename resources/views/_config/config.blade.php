@php
    if(!function_exists('rc')){
        function rc($number, $min_value = 10, $max_value = 20, $min_color , $mid_color , $max_color){
            if ($number <= $max_value && $number > $min_value){
                if ($mid_color == null) $mid_color = 'ffc107';
                echo 'style="background-color: #'.$mid_color.'"';
            }else if($number <= $min_value){
                if ($min_color == null) $min_color = 'dc3545';
                echo 'style="background-color: #'.$min_color.'"';
            }else{
                if ($min_color == null) $max_color ='28a745';
                echo 'style="background-color: #'.$max_color.'"';
            }
        }
    }

    if(!function_exists('minutes_to_time')){
        function minutes_to_time($minutes_) {
            $hours = floor($minutes_ / 60);
            $minutes = floor($minutes_  % 60);
            $hours = ($hours < 10)?'0'.$hours:$hours;
            $minutes = ($minutes < 10)?'0'.$minutes:$minutes;
            return "$hours:$minutes";
        }
    }
@endphp