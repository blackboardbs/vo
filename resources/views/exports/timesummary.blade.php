<table>
    <thead>
    <tr>
        <th colspan="3"></th>
        <th colspan="{{count($weeks)}}"><strong>Weeks</strong></th>
        <th></th>
    </tr>
    <tr>
        <th><strong>Resource</strong></th>
        <th><strong>Client</strong></th>
        <th><strong>Project</strong></th>
        @foreach($weeks as $week)
            <th><strong>{{$week}}</strong></th>
        @endforeach
        <th style="text-align: end"><strong>Total</strong></th>
    </tr>
    </thead>
    <tbody>
    @forelse($reports as $report)
        <tr>
            <td>{{$report["employee"]}}</td>
            <td>{{$report["customer"]}}</td>
            <td>{{$report["project"]}}</td>
            @foreach($weeks as $week)
                <td class="text-right text-bold">{{(isset($report["weeks"][$week]) && $report["weeks"][$week] > 0) ?number_format(($report["weeks"][$week]/60),2):'0.00'}}</td>
            @endforeach
            <td class="text-right text-bold">{{$report["total"] > 0 ? number_format(($report["total"]/60),2):'0.00'}}</td>
        </tr>
    @empty
        <tr>
            <td class="text-center" colspan="13">No time summary available</td>
        </tr>
    @endforelse
    <tr>
        <td colspan="3" class="text-right text-bold"><strong>Total</strong></td>
        @foreach($weeks as $week)
            <td class="text-right text-bold"><strong>{{number_format($grand_total[$week], 2)}}</strong></td>
        @endforeach
        <td class="text-right text-bold"><strong>{{number_format(array_sum(array_values($grand_total)), 2)}}</strong></td>
    </tr>
    </tbody>
</table>
