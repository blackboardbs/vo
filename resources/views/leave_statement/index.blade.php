@extends('adminlte.default')

@section('title') Leave Statement @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
            {{ Form::open(['url' => route('leave_statement.index'), 'method' => 'get','id'=>'searchform','class'=>'form-inline mt-3','autocomplete'=>'off'])}}
            Resource &nbsp;
            {{Form::select('resource_id',$resource_drop_down,(isset($_GET['resource_id']) ? $_GET['resource_id'] : '' ),['class'=>'resource form-control form-control-sm  col-sm-12 '. ($errors->has('resource_id') ? ' is-invalid' : '')])}}
            &nbsp; from &nbsp;
        @if(app('request')->input('f'))
            <select name="f" class="from form-control form-control-sm ">
                <option value="{{app('request')->input('f')}}">{{app('request')->input('f')}}</option>
                @else
                    <select name="f" class="from form-control form-control-sm " disabled>
                        <option value="">Please Select</option>
                        @foreach($yearmonth as $y)
                            <option value="{{$y->yearmonth}}">{{$y->yearmonth}}</option>
                        @endforeach
                        @endif
                    </select>
            &nbsp; to &nbsp;
                    @if(app('request')->input('t'))
            <select name="t" class="to form-control form-control-sm ">
                        <option value="{{app('request')->input('t')}}">{{app('request')->input('t')}}</option>
                    @else
                    <select name="t" class="to form-control form-control-sm " disabled>
                        <option value="">Please Select</option>
                    @endif
                </select>
            <button type="submit" class="btn btn-sm btn-dark ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
            {{Form::close()}}

        <hr>
                <div class="msg">

                    {{--@if(isset($joindate) && $joindate == '0')
                    <div class="alert alert-danger">A leave statement could not be generated because there is no join date for the selected resource. Please update their profile by clicking <a href="{{url('/resource')}}/{{$resid}}/edit" style="color:#FFF;font-weight:bold;">here</a>.</div>
                    @else

                    @endif--}}
                </div>
                    @if(app('request')->input('resource_id'))
                    <div class="table-responsive">
                        @if($annual_leave_statement != '')
                        <h4>Annual Leave</h4>
                        @endif
                        @if($annual_leave_statement)
                            <table class="table table-bordered table-sm table-hover">
                                <thead class="btn-dark">
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Days</th>

                                </tr>
                                </thead>
                                <tbody>
                                @forelse($annual_leave_statement as $result)

                                    <tr>
                                        <td>{{$result["date"]}}</td>
                                        <td>{{$result["description"]}}</td>
                                        <td>{{$result["days"]}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">No Records To Show</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <div class="table-responsive">
                        @if(count($sick_leave) != '0')
                        <h4>Sick Leave</h4>
                        @endif
                        @if($sick_leave)
                            <table class="table table-bordered table-sm table-hover">
                                <thead class="btn-dark">
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Days</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($sick_leave as $result)
                                    @if(count($result["statement"]) > 0)
                                        @forelse($result["statement"] as $result2)

                                            <tr>
                                                <td>{{$result2["date"]}}</td>
                                                <td>{{$result2["description"]}}</td>
                                                <td>{{$result2["days"]}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">No Records To Show</td>
                                            </tr>
                                            Annual Leave           @endforelse
                                    @else
                                        <tr>
                                            <td colspan="3">No Records To Show</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                @foreach($other_leave as $result)
                {{--@foreach($key as $result)--}}
                            @if(count($result["statement"]) > 0 && count($other_leave) != 0)
                    <div class="table-responsive">
                        <h4>{{$result["title"]}}</h4>

                            <table class="table table-bordered table-sm table-hover">
                                <thead class="btn-dark">
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Days</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(count($result["statement"]) > 0)
                                @forelse($result["statement"] as $result2)

                                    <tr>
                                        <td>{{$result2["date"]}}</td>
                                        <td>{{$result2["description"]}}</td>
                                        <td>{{$result2["days"]}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">No Records To Show</td>
                                    </tr>
                                @endforelse
                                @else
                                    <tr>
                                        <td colspan="3">No Records To Show</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                    </div>
                        @endif
                {{--@endforeach--}}
                @endforeach
                        @endif
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            var from = $('.from').val();

            if(from != '') {

                fromDrop(from);

                $(".to").prop("disabled",false);
                $('.to').chosen().trigger("chosen:updated");
            }

            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '200px');

            $(".from").on("change",function(){
                var from = $('.from').val();

                fromDrop(from);
            })

            $(".resource").on("change",function(){
                var joindate = checkResource($(".resource").val());

                if($(".from").val() != '' && $(".to").val() != '') {
                    //$("#searchform").submit();
                    $('.to').chosen().trigger("chosen:updated");
                }
            })

            $(".to").on("change",function(){
                if($(".from").val() != '' && $(".resource").val() != '') {
                    //$("#searchform").submit();
                    $('.to').chosen().trigger("chosen:updated");
                }
            })
        });

        function checkResource(val){
            var resourceid = val;

            if(resourceid != '' || resourceid > 0){
                $.ajax({
                    url: '/leave_from/checkjoindate/'+resourceid,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {
                        if(data.result == '0'){
                            $(".to").prop("disabled",true);
                            $('.to').chosen().trigger("chosen:updated");
                            $(".from").prop("disabled",true);
                            $('.from').chosen().trigger("chosen:updated");
                            $(".table-responsive").empty();
                            $(".msg").html('<div class="alert alert-danger">A leave statement could not be generated because there is no join date for the selected resource. Please update their profile by clicking <a href="{{url('/resource')}}/'+ resourceid +'/edit" style="color:#FFF;font-weight:bold;">here</a>.</div>')
                        }

                        if(data.result != '0'){
                            $(".from").prop("disabled",false);
                            $('.from').chosen().trigger("chosen:updated");
                            $(".msg").empty();
                            /*$(".msg").html('<div class="alert alert-danger">'+data.join_date+'</div>');*/
                        }
                    },
                    complete: function(){

                    }
                });
            }
        }

        function fromDrop(val){
            var from = val;
            var to = $(".to").val();

            if(from) {
                $.ajax({
                    url: '/leave_from/get/'+from,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },
                    success:function(data) {
                        $('.to').empty();
                        $.each(data, function(key,value){
                            $.each(value, function(key,value1) {
                                $('.to').append('<option value="' + value1 + '">' + value1 + '</option>');
                            })
                        });
                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                        $(".to").val(to);
                        $(".to").prop("disabled",false);
                        $('.to').chosen().trigger("chosen:updated");
                    }
                });
            } else {
                $('.to').empty();
            }
        }
    </script>
@endsection