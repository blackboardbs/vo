@extends('adminlte.default')
@section('title') Leave Statement @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{-- <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button> --}}
        <button id="backButton" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
                <div class="col-sm-3 col-sm">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('resource_id', $resource_drop_down, (isset($_GET['resource_id']) ? $_GET['resource_id'] : '' ), ['class'=>'search resource form-control w-100 col-sm-12 '. ($errors->has('resource_id') ? ' is-invalid' : '')])}}
                    <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 col-sm">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('leave', $year_months, (isset($_GET['leave']) ? $_GET['leave'] : '' ), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Period</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{route('leave_statement.index')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
        </form>
        <hr>
        <div class="table-responsive">
            <h4>Annual Leave</h4>
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Days</th>
                    </tr>
                </thead>
                <tbody>
                @if($resource_balance_found == true)
                    <tr>
                        <td>{{$selected_period_start_date}}</td>
                        <td>Opening Balance</td>
                        <td>{{$annual_leave["opening_balance"]}}</td>
                    </tr>
                    <tr>
                        <td>{{$selected_period_end_date}}</td>
                        <td>Leave Allocation</td>
                        <td>{{$annual_leave["days_accrued"]}}</td>
                    </tr>
                    <tr>
                        <td>{{$selected_period_end_date}}</td>
                        <td>Leave Taken</td>
                        <td>{{$annual_leave["leave_taken"]}}</td>
                    </tr>
                    @if(count($annual_leave["pay_out"]) > 0)
                    <tr>
                        <td>{{$annual_leave["pay_out"]["pay_out_date"]}}</td>
                        <td>Leave Paid Out</td>
                        <td>{{$annual_leave["pay_out"]["pay_out_leave"]}}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>{{$selected_period_end_date}}</td>
                        <td>Closing Balance</td>
                        <td>
                        @if(count($annual_leave["pay_out"]) > 0)
                            {{$annual_leave["closing_balance"] - ($annual_leave["pay_out"]["pay_out_leave"])}}
                        @else
                            {{$annual_leave["closing_balance"]}}
                        @endif
                        </td>
                    </tr>
                @else
                    <tr>
                        <td colspan="3">No leave was found for this resource, please check if leave balance was set for the selected resource</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
         $(document).ready(function() {
            $('#backButton').click(function() {
                // Set a flag in sessionStorage before going back
                sessionStorage.setItem('refreshPage', 'true');
                history.back();
            });

            // Check if the flag is set in sessionStorage
            if (sessionStorage.getItem('refreshPage') === 'true') {
                // Remove the flag and refresh the page
                sessionStorage.removeItem('refreshPage');
                location.reload();
            }
        });
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '200px');
        });
    </script>
@endsection