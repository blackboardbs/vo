@extends('adminlte.default')

@section('title') View Vendor @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <a href="{{route('vendors.edit',$vendor->id)}}" class="btn btn-success btn-sm mr-1" style="margin-left:5px;"><i class="far fa-edit"></i> Edit Vendor</a>
            <div class="dropdown mr-1">
                <a class="btn btn-dark dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-file" aria-hidden="true"></i> Documents
                </a>
                <div class="dropdown-menu">
                    <a href="{{route('vendor.print', $vendor->id)}}" target="_blank" class="dropdown-item"><i class="fa fa-print"></i> Print Agreement</a>
                    <a href="{{route('vendor.send', $vendor->id)}}" class="dropdown-item"><i class="fa fa-envelope"></i> Send Agreement</a>
                    <a href="{{route('vendor.print.nda', $vendor->id)}}" class="dropdown-item" target="_blank"><i class="fa fa-print"></i> Print NDA</a>
                    <a href="{{route('vendor.send.nda', $vendor->id)}}" class="dropdown-item"><i class="fa fa-envelope"></i>  Send NDA</a>
                </div>
            </div>
            <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Vendor Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="5">Vendor Logo:</th>
                    <td rowspan="5"><div class="img-wrapper"><img src="{{route('vendor_avatar',['q'=>$vendor->vendor_logo])}}" id="company-preview-large" /></div></td>
                    <th>Vendor Name:</th>
                    <td>{{$vendor->vendor_name}}</td>
                </tr>
                <tr>
                    <th>Registration Number:</th>
                    <td>{{$vendor->business_reg_no}}</td>
                </tr>
                <tr>
                    <th>VAT Number:</th>
                    <td>{{$vendor->vat_no}}</td>
                </tr>
                <tr>
                    <th>Account Manager:</th>
                    <td>{{$vendor->account_managerd?->name()}}</td>
                </tr>
                <tr>
                    <th>Assignment Approver:</th>
                    <td>{{ $vendor->assignment_approverd?->name()}}</td>
                </tr>
                <tr>
                    <th>Payment Terms:</th>
                    <td>{{$vendor->payment_terms}}</td>
                    <th>Payment Terms Days:</th>
                    <td>{{$vendor->payment_terms_days}}</td>
                </tr>
                <tr>
                    <th>Billing Period</th>
                    <td>{{$vendor->billing_period}}</td>
                    <th>Status:</th>
                    <td>{{$vendor->status?->description}}</td>
                </tr>
                <tr>
                    <th>Contact Firstname:</th>
                    <td>{{$vendor->contact_firstname}}</td>
                    <th>Contact Lastname:</th>
                    <td>{{$vendor->contact_lastname}}</td>
                </tr>
                <tr>
                    <th>Contact Birthday:</th>
                    <td>{{$vendor->contact_birthday}}</td>
                    <th>InvoiceContact</th>
                    <td>{{$vendor->invoiceContact?->full_name}}</td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{$vendor->phone}}</td>
                    <th>Cell:</th>
                    <td>{{$vendor->cell}}</td>
                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{$vendor->fax}}</td>
                    <th>Cell 2:</th>
                    <td>{{$vendor->cell2}}</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{$vendor->email}}</td>
                    <th>Web:</th>
                    <td>{{$vendor->web}}</td>
                </tr>
                <tr>
                    <th>Postal Address:</th>
                    <td>{{$vendor->postal_address_line1}}<br />
                        {{$vendor->postal_address_line2}}<br />
                        {{$vendor->postal_address_line3}}<br />
                        {{$vendor->city_suburb}}<br />
                        {{$vendor->postal_zipcode}}
                    </td>
                    <th>Street Address:</th>
                    <td>{{$vendor->street_address_line1}}<br />
                        {{$vendor->street_address_line2}}<br />
                        {{$vendor->street_address_line3}}<br />
                        {{$vendor->street_city_suburb}}<br />
                        {{$vendor->street_zipcode}}

                    </td>
                </tr>
                <tr>
                    <th>State/Province:</th>
                    <td>{{$vendor->state_province}}</td>
                    <th>Country:</th>
                    <td>{{ isset($vendor->country->name) ? $vendor->country->name : ''}}</td>
                </tr>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{isset($vendor->bbbee_leveld->description) ? $vendor->bbbee_leveld->description : ''}}</td>
                    <th>BBBEE Certificate Expiry:</th>
                    <td>{{$vendor->bbbee_certificate_expiry}}</td>
                </tr>
                <tr>

                    <th>Supply Chain Appointment Manager:</th>
                    <td>{{isset($vendor->supplyChainAppointmentManager)?$vendor->supplyChainAppointmentManager->name():null}}</td>
                </tr>
                <tr>
                    <th>Onboarding Vendor:</th>
                    <td>{{$vendor->onboarding_vendor}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Templates</th>
                </tr>
                <tr>
                    <th>Service Agreement Template:</th>
                    <td>{{$vendor->serviceAgreement?->name}}</td>
                    <th>NDA Template</th>
                    <td>{{$vendor->NDATemplate?->name}}</td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Banking Details</th>
                </tr>
                <tr>
                    <th>Bank Name:</th>
                    <td>{{$vendor->bank_name}}</td>
                    <th>Branch Code:</th>
                    <td>{{$vendor->branch_code}}</td>
                </tr>
                <tr>
                    <th>Account Number:</th>
                    <td>{{$vendor->bank_acc_no}}</td>
                    <th>Branch Name:</th>
                    <td>{{$vendor->branch_name}}</td>
                </tr>
                <tr>
                    <th>Account Name:</th>
                    <td>{{$vendor->account_name}}</td>
                    <th>Swift Code:</th>
                    <td>{{$vendor->swift_code}}</td>
                </tr>
                <tr>
                    <th>Bank Account Type:</th>
                    <td>{{$vendor->bankAccountType?->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4">Documents</th>
                </tr>
                <tr class="bg-dark">
                    <th>Name</th>
                    <th>File</th>
                    <th>Owner</th>
                    <th>Action</th>
                </tr>
                @forelse($vendor->documents as $document) 
                    <tr>
                        <td>{{$document->name}}</td>
                        <td>{{$document->file}}</td>
                        <td>{{$document->user?->name()}}</td>
                        <td><a href="{{route('client.agreement', $document->file)}}" target="_blank" class="btn btn-sm btn-outline-success" ><i class="fa fa-eye"></i></a></td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="4">This vendor has no documents</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="pdf-viewer"></div>
        </div>
    </div>
@endsection

@section('extra-css')
    <style>
        a.btn-outline-success{
            color: #28a745!important;
        }
        a.btn-outline-success:hover{
            color: #ffffff!important;
        }
    </style>
@endsection
