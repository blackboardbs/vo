<table>
    <thead>
    <tr>
        <th>Vendor Name</th>
        <th>Contact Person</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Cell</th>
        <th>Added</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vendor as $_vendor)
        <tr>
            <td>{{$_vendor->vendor_name}}</td>
            <td>{{$_vendor->contact_firstname}} {{$_vendor->contact_lastname}}</td>
            <td>{{$_vendor->email}}</td>
            <td>{{$_vendor->phone}}</td>
            <td>{{$_vendor->cell}}</td>
            <td>{{$_vendor->created_at}}</td>
            <td>{{$_vendor->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>