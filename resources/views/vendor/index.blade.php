@extends('adminlte.default')

@section('title') Vendor @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('vendors.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Vendor</a>
            <x-export route="vendors.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <a href="{{route('project.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('vendor_name', 'Vendor Name')</th>
                    <th>@sortablelink('contact_firstname', 'Contact Person')</th>
                    <th>@sortablelink('email', 'Email')</th>
                    <th>@sortablelink('phone', 'Phone')</th>
                    <th>@sortablelink('cell', 'Cell')</th>
                    <th>@sortablelink('created_at', 'Added')</th>
                    <th>@sortablelink('status.status_id', 'Status')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($vendor as $_vendor)
                    <tr>
                        <td><a href="{{route('vendors.show',$_vendor->id)}}">{{$_vendor->vendor_name}}</a></td>
                        <td>{{$_vendor->contact_firstname}} {{$_vendor->contact_lastname}}</td>
                        <td>{{$_vendor->email}}</td>
                        <td>{{$_vendor->phone}}</td>
                        <td>{{$_vendor->cell}}</td>
                        <td>{{$_vendor->created_at}}</td>
                        <td>{{isset($_vendor->status->description)?$_vendor->status->description:''}}</td>
                        @if($can_update)
                        <td>
                            <div class="d-flex">
                            <a href="{{route('vendors.edit',$_vendor)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['vendors.destroy', $_vendor->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm" {{($_vendor->users_count > 0 || $_vendor->invoices_count > 0 ? 'disabled' : '')}}><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No vendors match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $vendor->firstItem() }} - {{ $vendor->lastItem() }} of {{ $vendor->total() }}
                        </td>
                        <td>
                            {{ $vendor->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
