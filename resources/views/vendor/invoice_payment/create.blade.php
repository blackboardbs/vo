@extends('adminlte.default')

@section('title') Process Invoice Payment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('process'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Payment Process Date</th>
                    <td colspan="3">{{ Form::text('vendor_paid_date', $date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('vendor_paid_date') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('vendor_paid_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                    {{Form::hidden('vendor_id', $id)}}
                </tr>
            </table>
            <table class="table table-borderless">
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Process</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection