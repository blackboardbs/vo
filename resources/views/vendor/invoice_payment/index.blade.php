@extends('adminlte.default')

@section('title') Invoice Function @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('function_newinvoice.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> New Invoice</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>
            {{Form::close()}}
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="bg-dark">
                    <th>@sortablelink('vendor_invoice_number', 'Invoice #')</th>
                    <th>@sortablelink('vendor_invoice_ref', 'Invoice Ref')</th>
                    <th>@sortablelink('vendor_invoice_date', 'Invoice Date')</th>
                    <th>@sortablelink('vendor_due_date', 'Due Date')</th>
                    <th>@sortablelink('vendor_paid_date', 'Paid Date')</th>
                    <th>@sortablelink('vendor_invoice_value', 'Invoice Value')</th>
                    <th>@sortablelink('invoice_status.description', 'Vendor Invoice Status')</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($vendor_invoices as $vendor_invoice)
                    <tr>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_number}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_ref}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_date}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_due_date}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_paid_date}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_value}}</a></td>
                        <td><a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->invoice_status->description}}</a></td>
                        <td>
                            <a href="{{route('vendor.edit',$vendor_invoice)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['delete.invoice', $vendor_invoice->id],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No vendors match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $vendor_invoices->links() }}
        </div>
    </div>
@endsection
