@extends('adminlte.default')

@section('title') Pro-forma Vendor Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="float-right">
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Generate Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive" style="min-height: 500px">
            {{Form::open(['url' => route('proforma.calculate'), 'method' => 'post','class'=>'mt-3', 'id' => 'saveForm'])}}
            <pro-forma-invoice
                    :consultants="{{$consultant_dropdown}}"
                    :errors="{{$errors}}"
                    :old="{{json_encode(old())}}"
                    :is-customer="false"
            ></pro-forma-invoice>
            {{Form::close()}}
        </div>
    </div>
@endsection
