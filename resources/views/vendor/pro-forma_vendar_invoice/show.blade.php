@extends('adminlte.default')

@section('title') View Pro-forma Vendor Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-3 pl-2">
                <h4>Vendor Invoice</h4>
                <h5>{{$company->company_name}}</h5>
                <p class="mb-0">{{ $company->postal_address_line1 }}</p>
                <p class="mb-0">{{ $company->postal_address_line2 }}</p>
                @if($company->postal_address3 !== null)
                    <p class="mb-0">{{ $company->postal_address_line3 }}</p>
                @endif
                <p class="mb-0">{{ $company->state_province }}</p>
                <p class="mb-0">{{ $company->postal_zipcode }}</p>
                <p class="mb-0">{{ $company->phone }}</p>
                <p class="mb-0">{{ $company->email }}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no }}</p>
            </div>
            <div class="col-md-3">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{$vendor->vendor_name}}</p>
                <p class="mb-0">
                        {{$vendor->postal_address_line1}}
                    @empty($vendor->postal_address_line1)
                        {{$vendor->street_address_line1}}
                    @endempty
                <p class="mb-0">
                                {{$vendor->postal_address_line2}}
                    @empty($vendor->postal_address_line2)
                        {{$vendor->street_address_line2}}
                    @endempty

                </p>
                <p class="mb-0">
                        {{$vendor->postal_address_line3}}
                    @empty($vendor->postal_address_line3)
                        {{$vendor->street_address_line3}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendor->city_suburb}}
                    @empty($vendor->city_suburb)
                        {{$vendor->street_city_suburb}}
                    @endempty
                </p>
                <p class="mb-0">{{$vendor->state_province}}</p>
                <p class="mb-0">
                    {{$vendor->postal_zipcode}}
                    @empty($vendor->postal_zipcode)
                        {{ $vendor->street_zipcode }}
                    @endempty
                </p>
            </div>
            <div class="col-md-3">
                {{--<div class="text-right mb-3"><img src="{{route('company_avatar', ['q'=> $company->company_logo])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> $company->company_logo])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{$invoice_no}}</div>
                    <div class="col-md-6">Date:</div>
                    <div class="col-md-6 text-right">{{now()->toDateString()}}</div>
                    <div class="col-md-6">Page:</div>
                    <div class="col-md-6 text-right">1/1</div>
                    <div class="col-md-4">Reference:</div>
                    <div class="col-md-8 text-right">{{$project->ref}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ ' ' }}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $project->end_date }}</div>
                    <div class="col-md-6">Vendor Contact:</div>
                    <div class="col-md-6 text-right">{{ $vendor->contact_firstname." ".$vendor->contact_lastname }}</div>
                    <div class="col-md-6">Vat Number:</div>
                    <div class="col-md-6 text-right">{{ $vendor->vat_no }}</div>
                </div>
            </div>
            <div class="col-md-9 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-9 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark row no-gutters">
                            <th class="col-5">Description</th>
                            <th class="col-1 text-right">Qty</th>
                            <th class="col-1 text-right">Excl. Price</th>
                            <th class="col-1 text-right">VAT(%)</th>
                            <th class="col-2 text-right">Exclusive Total</th>
                            <th class="col-2 text-right">Inclusive Total</th>
                        </tr>
                        </thead>
                        <tbody>

                           @foreach($lineItems as $item)
                               <tr class="row no-gutters">
                                   <td class="col-5">{{ $item->first()?->description }}</td>
                                   <td class="col-1 text-right">{{number_format($item->sum('time'), 2)}}</td>
                                   <td class="col-1 text-right">{{$item->first()?->currency.' '. number_format($item->first()?->rate,2)}}</td>
                                   <td class="col-1 text-right">{{ $item->first()?->vat }}%</td>
                                   <td class="col-2 text-right">{{ $item->first()?->currency .' '.number_format($item->sum('exc_price') ,2)}}</td>
                                   <td class="col-2 text-right">{{ $item->first()?->currency .' '.number_format($item->sum('inc_price'),2) }}</td>
                               </tr>
                           @endforeach
                            <tr class="row no-gutters">
                                <td class="col-12">&nbsp;</td>
                            </tr>
                            @if($expenses->isNotEmpty())
                                <tr class="row no-gutters bg-dark">
                                    <th class="col-12">Expenses</th>
                                </tr>
                            @endif

                               @foreach($expenses as $expense)
                                   <tr class="row no-gutters">
                                       <td class="col-5">{{$expense->date." ".$expense->description}}</td>
                                       <td class="col-1 text-right">1</td>
                                       <td class="col-1"></td>
                                       <td class="col-1 text-right">0%</td>
                                       <td class="col-2 text-right">{{($timesheet->first()?->currency??"R")." ".number_format($expense->amount,2)}}</td>
                                       <td class="col-2 text-right">{{($timesheet->first()?->currency??"R")." ".number_format($expense->amount,2)}}</td>
                                   </tr>
                               @endforeach
                           <tr class="row no-gutters">
                               <td colspan="col-12">&nbsp;</td>
                           </tr>
                           <tr class="row no-gutters">
                               <th class="col-4">Payment Information</th>
                               <td class="col-4">{{ ($period == 0) ? $date_range : $period }}{{$invoice_message ? ', '.$invoice_message : null}}</td>
                               <th class="text-right col-2">Total Exclusive:</th>
                               <td class="text-right col-2">{{($timesheet->first()?->currency??"R").  number_format($total_exclusive ,2) }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <td class="col-12">{{ $vendor->vendor_name }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <td class="col-8">{{ $vendor->bank_name }}</td>
                               <th class="text-right col-2">Total VAT:</th>
                               <td class="text-right col-2">{{($timesheet->first()?->currency??"R"). number_format($total_vat,2,'.',',') }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <td class="col-12">Branch Code: {{ $vendor->branch_code }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <td class="col-8">Account Number: {{ $vendor->bank_acc_no }}</td>
                               <th class="text-right col-2">Total:</th>
                               <td class="text-right col-2">{{($timesheet->first()?->currency??"R"). number_format($total_inclusive,2,'.',',') }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <td class="col-12">Reference: {{ $company->company_name }}</td>
                           </tr>
                           <tr class="row no-gutters">
                               <th class="col-12 text-center">Thank you for your business!</th>
                           </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
