<table>
    <thead>
    <tr>
        <th>Vendor Name</th>
        <th>Total</th>
        <th>Current</th>
        <th>30 Days</th>
        <th>60 Days</th>
        <th>90+ Days</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vendor_invoices as $result)
        <tr>
            <td>{{$result->vendor?->vendor_name}}</td>
            <td>{{$result->currency." ".number_format(($result->current + $result->thirty_days + $result->sixty_days + $result->ninety_days_plus),2,'.',',')}}</td>
            <td>{{$result->currency." ".number_format($result->current,2,'.',',')}}</td>
            <td>{{$result->currency." ".number_format($result->thirty_days,2,'.',',')}}</td>
            <td>{{$result->currency." ".number_format($result->sixty_days,2,'.',',')}}</td>
            <td>{{$result->currency." ".number_format($result->ninety_days_plus,2,'.',',')}}</td>
        </tr>
    @endforeach
    <tr>
        <th>Total</th>
        <th>
            @foreach ($currencies as $currency)
                {{$currency}}: {{number_format(($total[$currency]+$total30[$currency]+$total60[$currency]+$total90[$currency]),2,'.',',')}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total30 as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total60 as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total90 as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
    </tr>
    </tbody>
</table>