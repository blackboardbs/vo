<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vendor Statement</title>
    <style>
        /*! CSS Used from: http://127.0.0.1:8000/adminlte/dist/css/adminlte.min.css */
        :root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#ffffff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;}
        *,::after,::before{box-sizing:border-box;}
        html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-ms-overflow-style:scrollbar;-webkit-tap-highlight-color:transparent;}
        aside,footer,nav{display:block;}
        body{margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;}
        hr{box-sizing:content-box;height:0;overflow:visible;}
        h3,h5{margin-top:0;margin-bottom:.5rem;}
        p{margin-top:0;margin-bottom:1rem;}
        ul{margin-top:0;margin-bottom:1rem;}
        ul ul{margin-bottom:0;}
        strong{font-weight:bolder;}
        small{font-size:80%;}
        a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects;}
        a:hover{color:#0056b3;text-decoration:none;}
        a:not([href]):not([tabindex]){color:inherit;text-decoration:none;}
        a:not([href]):not([tabindex]):focus,a:not([href]):not([tabindex]):hover{color:inherit;text-decoration:none;}
        a:not([href]):not([tabindex]):focus{outline:0;}
        img{vertical-align:middle;border-style:none;}
        table{border-collapse:collapse;}
        th{text-align:inherit;}
        button{border-radius:0;}
        button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
        button,input{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
        button,input{overflow:visible;}
        button{text-transform:none;}
        button,html [type=button]{-webkit-appearance:button;}
        [type=button]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none;}
        h3,h5{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit;}
        h3{font-size:1.75rem;}
        h5{font-size:1.25rem;}
        hr{margin-top:1rem;margin-bottom:1rem;border:0;border-top:1px solid rgba(0,0,0,.1);}
        small{font-size:80%;font-weight:400;}
        .img-fluid{max-width:100%;height:auto;}
        .img-thumbnail{padding:.25rem;background-color:#fff;border:1px solid #dee2e6;border-radius:.25rem;box-shadow:0 1px 2px rgba(0,0,0,.075);max-width:100%;height:auto;}
        .container-fluid{width:100%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto;}
        .row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px;}
        .col-lg-12,.col-md-12,.col-md-3,.col-md-5,.col-md-6,.col-md-7,.col-md-9,.col-sm-6,.col-xl-9{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px;}
        @media (min-width:576px){
            .col-sm-6{flex:0 0 50%;max-width:50%;}
        }
        @media (min-width:768px){
            .col-md-3{flex:0 0 25%;max-width:25%;}
            .col-md-5{flex:0 0 41.666667%;max-width:41.666667%;}
            .col-md-6{flex:0 0 50%;max-width:50%;}
            .col-md-7{flex:0 0 58.333333%;max-width:58.333333%;}
            .col-md-9{flex:0 0 75%;max-width:75%;}
            .col-md-12{flex:0 0 100%;max-width:100%;}
        }
        @media (min-width:992px){
            .col-lg-12{flex:0 0 100%;max-width:100%;}
        }
        @media (min-width:1200px){
            .col-xl-9{flex:0 0 75%;max-width:75%;}
        }
        .table{width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent;}
        .table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;}
        .table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6;}
        .table .table{background-color:#fff;}
        .table-sm td,.table-sm th{padding:.3rem;}
        .table-striped tbody tr:nth-of-type(odd){background-color:rgba(0,0,0,.05);}
        .table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar;}
        .form-control{display:block;width:100%;padding:.375rem .75rem;font-size:1rem;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;box-shadow:inset 0 0 0 transparent;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
        @media screen and (prefers-reduced-motion:reduce){
            .form-control{transition:none;}
        }
        .form-control::-ms-expand{background-color:transparent;border:0;}
        .form-control:focus{color:#495057;background-color:#fff;border-color:#80bdff;outline:0;box-shadow:inset 0 0 0 transparent,0 0 0 .2rem rgba(0,123,255,.25);}
        .form-control::placeholder{color:#6c757d;opacity:1;}
        .form-control:disabled{background-color:#e9ecef;opacity:1;}
        .form-control-sm{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;border-radius:.2rem;}
        .btn{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
        @media screen and (prefers-reduced-motion:reduce){
            .btn{transition:none;}
        }
        .btn:focus,.btn:hover{text-decoration:none;}
        .btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
        .btn:disabled{opacity:.65;box-shadow:none;}
        .btn-secondary{color:#fff;background-color:#6c757d;border-color:#6c757d;box-shadow:0 1px 1px rgba(0,0,0,.075);}
        .btn-secondary:hover{color:#fff;background-color:#5a6268;border-color:#545b62;}
        .btn-secondary:focus{box-shadow:0 1px 1px rgba(0,0,0,.075),0 0 0 .2rem rgba(108,117,125,.5);}
        .btn-secondary:disabled{color:#fff;background-color:#6c757d;border-color:#6c757d;}
        .btn-dark{color:#fff;background-color:#343a40;border-color:#343a40;box-shadow:0 1px 1px rgba(0,0,0,.075);}
        .btn-dark:hover{color:#fff;background-color:#23272b;border-color:#1d2124;}
        .btn-dark:focus{box-shadow:0 1px 1px rgba(0,0,0,.075),0 0 0 .2rem rgba(52,58,64,.5);}
        .btn-dark:disabled{color:#fff;background-color:#343a40;border-color:#343a40;}
        .btn-sm{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;border-radius:.2rem;}
        .dropdown{position:relative;}
        .dropdown-toggle::after{display:inline-block;width:0;height:0;margin-left:.255em;vertical-align:.255em;content:"";border-top:.3em solid;border-right:.3em solid transparent;border-bottom:0;border-left:.3em solid transparent;}
        .dropdown-toggle:empty::after{margin-left:0;}
        .dropdown-menu{position:absolute;top:100%;left:0;z-index:1000;display:none;float:left;min-width:10rem;padding:.5rem 0;margin:.125rem 0 0;font-size:1rem;color:#212529;text-align:left;list-style:none;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.15);border-radius:.25rem;box-shadow:0 .5rem 1rem rgba(0,0,0,.175);}
        .dropdown-menu-right{right:0;left:auto;}
        .dropdown-divider{height:0;margin:.5rem 0;overflow:hidden;border-top:1px solid #e9ecef;}
        .dropdown-item{display:block;width:100%;padding:.25rem 1rem;clear:both;font-weight:400;color:#212529;text-align:inherit;white-space:nowrap;background-color:transparent;border:0;}
        .dropdown-item:focus,.dropdown-item:hover{color:#16181b;text-decoration:none;background-color:#f8f9fa;}
        .dropdown-item:active{color:#fff;text-decoration:none;background-color:#007bff;}
        .dropdown-item:disabled{color:#6c757d;background-color:transparent;}
        .dropdown-header{display:block;padding:.5rem 1rem;margin-bottom:0;font-size:.875rem;color:#6c757d;white-space:nowrap;}
        .btn-group{position:relative;display:inline-flex;vertical-align:middle;}
        .btn-group>.btn{position:relative;flex:0 1 auto;}
        .btn-group>.btn:hover{z-index:1;}
        .btn-group>.btn:active,.btn-group>.btn:focus{z-index:1;}
        .btn-group .btn+.btn{margin-left:-1px;}
        .btn-toolbar{display:flex;flex-wrap:wrap;justify-content:flex-start;}
        .btn-group>.btn:first-child{margin-left:0;}
        .btn-group>.btn:not(:last-child):not(.dropdown-toggle){border-top-right-radius:0;border-bottom-right-radius:0;}
        .btn-group>.btn:not(:first-child){border-top-left-radius:0;border-bottom-left-radius:0;}
        .nav{display:flex;flex-wrap:wrap;padding-left:0;margin-bottom:0;list-style:none;}
        .nav-link{display:block;padding:.5rem 1rem;}
        .nav-link:focus,.nav-link:hover{text-decoration:none;}
        .navbar{position:relative;display:flex;flex-wrap:wrap;align-items:center;justify-content:space-between;padding:.5rem .5rem;}
        .navbar-nav{display:flex;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none;}
        .navbar-nav .nav-link{padding-right:0;padding-left:0;}
        .navbar-nav .dropdown-menu{position:static;float:none;}
        .navbar-expand{flex-flow:row nowrap;justify-content:flex-start;}
        .navbar-expand .navbar-nav{flex-direction:row;}
        .navbar-expand .navbar-nav .dropdown-menu{position:absolute;}
        .navbar-expand .navbar-nav .nav-link{padding-right:1rem;padding-left:1rem;}
        .navbar-light .navbar-nav .nav-link{color:rgba(0,0,0,.5);}
        .navbar-light .navbar-nav .nav-link:focus,.navbar-light .navbar-nav .nav-link:hover{color:rgba(0,0,0,.7);}
        .card{position:relative;display:flex;flex-direction:column;min-width:0;word-wrap:break-word;background-color:#fff;background-clip:border-box;border:0 solid rgba(0,0,0,.125);border-radius:.25rem;}
        .badge{display:inline-block;padding:.25em .4em;font-size:75%;font-weight:700;line-height:1;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25rem;}
        .badge:empty{display:none;}
        .badge-warning{color:#1f2d3d;background-color:#ffc107;}
        .list-group{display:flex;flex-direction:column;padding-left:0;margin-bottom:0;}
        .list-group-item{position:relative;display:block;padding:.75rem 1.25rem;margin-bottom:-1px;background-color:#fff;border:1px solid rgba(0,0,0,.125);}
        .list-group-item:first-child{border-top-left-radius:.25rem;border-top-right-radius:.25rem;}
        .list-group-item:last-child{margin-bottom:0;border-bottom-right-radius:.25rem;border-bottom-left-radius:.25rem;}
        .list-group-item:focus,.list-group-item:hover{z-index:1;text-decoration:none;}
        .list-group-item:disabled{color:#6c757d;background-color:#fff;}
        .bg-dark{background-color:#343a40!important;}
        .bg-white{background-color:#fff!important;}
        .border{border:1px solid #dee2e6!important;}
        .border-bottom{border-bottom:1px solid #dee2e6!important;}
        .border-right-0{border-right:0!important;}
        .border-bottom-0{border-bottom:0!important;}
        .border-left-0{border-left:0!important;}
        .border-dark{border-color:#343a40!important;}
        .d-none{display:none!important;}
        .d-block{display:block!important;}
        .d-flex{display:flex!important;}
        .flex-column{flex-direction:column!important;}
        .float-right{float:right!important;}
        .position-absolute{position:absolute!important;}
        .shadow-sm{box-shadow:0 .125rem .25rem rgba(0,0,0,.075)!important;}
        .mt-0{margin-top:0!important;}
        .mt-2{margin-top:.5rem!important;}
        .mr-2{margin-right:.5rem!important;}
        .mt-3{margin-top:1rem!important;}
        .card,.mb-3{margin-bottom:1rem!important;}
        .mt-5{margin-top:3rem!important;}
        .p-3{padding:1rem!important;}
        .pb-3{padding-bottom:1rem!important;}
        .ml-auto{margin-left:auto!important;}
        .text-right{text-align:right!important;}
        .text-center{text-align:center!important;}
        .text-dark{color:#343a40!important;}
        a.text-dark:focus,a.text-dark:hover{color:#1d2124!important;}
        @media print{
            *,::after,::before{text-shadow:none!important;box-shadow:none!important;}
            a:not(.btn){text-decoration:underline;}
            thead{display:table-header-group;}
            img,tr{page-break-inside:avoid;}
            h3,p{orphans:3;widows:3;}
            h3{page-break-after:avoid;}
            body{min-width:992px!important;}
            .navbar{display:none;}
            .badge{border:1px solid #000;}
            .table{border-collapse:collapse!important;}
            .table td,.table th{background-color:#fff!important;}
        }
        .wrapper,body,html{min-height:100%;overflow-x:hidden;}
        .wrapper{position:relative;}
        @media (min-width:768px){
            .content-wrapper,.main-footer,.main-header{transition:margin-left .3s ease-in-out;margin-left:250px;z-index:3000;}
        }
        @media screen and (min-width:768px) and (prefers-reduced-motion:reduce){
            .content-wrapper,.main-footer,.main-header{transition:none;}
        }
        @media (max-width:991.98px){
            .content-wrapper,.content-wrapper:before,.main-footer,.main-footer:before,.main-header,.main-header:before{margin-left:0;}
        }
        .content-wrapper{background:#f4f6f9;}
        .main-sidebar{position:fixed;top:0;left:0;bottom:0;}
        .main-sidebar,.main-sidebar:before{transition:margin-left .3s ease-in-out,width .3s ease-in-out;width:250px;}
        @media screen and (prefers-reduced-motion:reduce){
            .main-sidebar,.main-sidebar:before{transition:none;}
        }
        @media (max-width:991.98px){
            .main-sidebar,.main-sidebar:before{box-shadow:none!important;margin-left:-250px;}
        }
        .main-footer{padding:15px;color:#555;border-top:1px solid #dee2e6;background:#fff;}
        .main-header{z-index:1000;}
        .main-header .navbar-nav .nav-item{margin:0;}
        .main-header .nav-link{position:relative;height:2.5rem;}
        .navbar-badge{position:absolute;top:9px;right:5px;font-size:.6rem;font-weight:300;padding:2px 4px;}
        .brand-link{padding:.8125rem .5rem;font-size:1.25rem;display:block;line-height:1.5;white-space:nowrap;}
        .brand-link:hover{color:#fff;text-decoration:none;}
        [class*=sidebar-dark] .brand-link{color:rgba(255,255,255,.8);border-bottom:1px solid #4b545c;}
        .main-sidebar{z-index:1100;height:100vh;overflow-y:hidden;}
        .sidebar{padding-bottom:0;padding-top:0;padding-left:.5rem;padding-right:.5rem;overflow-y:auto;height:calc(100% - 4rem);}
        .user-panel{position:relative;}
        [class*=sidebar-dark] .user-panel{border-bottom:1px solid #4f5962;}
        .user-panel,.user-panel .info{overflow:hidden;white-space:nowrap;}
        .user-panel .image{padding-left:.8rem;display:inline-block;}
        .user-panel img{width:2.1rem;height:auto;}
        .user-panel .info{display:inline-block;padding:5px 5px 5px 10px;}
        .nav-sidebar .nav-item>.nav-link{margin-bottom:.2rem;}
        .nav-sidebar .nav-item>.nav-link .right{transition:transform ease-in-out .3s;}
        @media screen and (prefers-reduced-motion:reduce){
            .nav-sidebar .nav-item>.nav-link .right{transition:none;}
        }
        .nav-sidebar .nav-link>p>.right{position:absolute;right:1rem;top:12px;}
        .nav-sidebar>.nav-item{margin-bottom:0;}
        .nav-sidebar>.nav-item .nav-icon{text-align:center;width:1.6rem;font-size:1.2rem;margin-right:.2rem;}
        .nav-sidebar .nav-treeview{display:none;list-style:none;padding:0;}
        .nav-sidebar .nav-treeview>.nav-item>.nav-link>.nav-icon{width:1.6rem;}
        .nav-sidebar .nav-link p{display:inline-block;margin:0;}
        .sidebar-dark-primary{background-color:#343a40;}
        .sidebar-dark-primary .user-panel a:hover{color:#fff;}
        .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link:active,.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link:focus{color:#c2c7d0;}
        .sidebar-dark-primary .nav-sidebar>.nav-item:hover>.nav-link{color:#fff;background-color:rgba(255,255,255,.1);}
        .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-treeview{background:0 0;}
        .sidebar-dark-primary .sidebar a{color:#c2c7d0;}
        .sidebar-dark-primary .sidebar a:hover{text-decoration:none;}
        .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link{color:#c2c7d0;}
        .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link:hover{color:#fff;background-color:rgba(255,255,255,.1);}
        @media (min-width:992px){
            .sidebar-mini .nav-sidebar,.sidebar-mini .nav-sidebar .nav-link{white-space:nowrap;overflow:hidden;}
        }
        .nav-sidebar{position:relative;}
        .nav-sidebar:hover{overflow:visible;}
        .nav-sidebar .nav-item>.nav-link{position:relative;}
        .sidebar .nav-link p,.sidebar .user-panel .info{transition:margin-left .3s linear,opacity .5s ease;}
        @media screen and (prefers-reduced-motion:reduce){
            .sidebar .nav-link p,.sidebar .user-panel .info{transition:none;}
        }
        .control-sidebar{position:absolute;top:2.5rem;z-index:830;}
        .control-sidebar,.control-sidebar:before{width:250px;right:-250px;bottom:0;transition:right .3s ease-in-out;}
        @media screen and (prefers-reduced-motion:reduce){
            .control-sidebar,.control-sidebar:before{transition:none;}
        }
        .control-sidebar:before{top:0;display:block;position:fixed;content:" ";z-index:-1;}
        .control-sidebar-dark{color:#c2c7d0;}
        .control-sidebar-dark,.control-sidebar-dark:before{background:#343a40;}
        .dropdown-menu-lg{min-width:280px;max-width:300px;padding:0;}
        .dropdown-menu-lg .dropdown-divider{margin:0;}
        .dropdown-menu-lg .dropdown-item{padding:.5rem 1rem;}
        .dropdown-footer,.dropdown-header{text-align:center;display:block;padding:.5rem 1rem;font-size:.875rem;}
        .card{box-shadow:0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);}
        .bg-dark{color:#fff!important;}
        .bg-gray{color:#000;background-color:#adb5bd;}
        .bg-gray-light{background-color:#f2f4f5;color:#1f2d3d!important;}
        .bg-white{background-color:#fff;color:#1f2d3d!important;}
        .img-circle{border-radius:50%;}
        @media print{
            .main-header,.main-sidebar{display:none!important;}
            .content-wrapper,.main-footer{margin-left:0!important;min-height:0!important;-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0);}
            .table-responsive{overflow:auto;}
            .table-responsive>.table tr td,.table-responsive>.table tr th{white-space:normal!important;}
        }
        .elevation-2{box-shadow:0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);}
        .elevation-4{box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22);}
        /*! CSS Used from: http://127.0.0.1:8000/css/custom.css?v0.00002 */
        .content-wrapper a{color:#357ca5!important;}
        a.btn{color:#FFF!important;}
        h3{display:inline-block;width:fit-content;}
        .container-title{margin-top:10px;}
        .content-wrapper{background:#FFFFFF!important;}
        .nav{padding-left:0;margin-bottom:0;list-style:none;}
        .nav > li{position:relative;display:block;}
        .nav > li > a{position:relative;display:block;padding:5px 15px;}
        .nav > li > a:hover,.nav > li > a:focus{text-decoration:none;background-color:#fff;}
        @media (min-width: 768px){
            .table-responsive{overflow-x:inherit!important;}
        }
        .blackboard-avatar{-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;}
        .blackboard-avatar-navbar-img{height:32px;width:32px;}
        .nav-sidebar>.nav-item .nav-icon{font-size:1rem!important;}
        .nav-sidebar .nav-item>.nav-link{font-size:0.95rem;}
        .sidebar .header{color:#c2c7d0!important;border-bottom:1px solid #4f5962;border-top:1px solid #4f5962;padding:5px 10px;font-size:14px;}
        .nav-sidebar>.nav-item .nav-treeview .nav-icon{font-size:0.5rem!important;}
        .blackboard-scrollbar::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0, 0, 0, 0.3);background-color:#F5F5F5;overflow:hidden;}
        .blackboard-scrollbar::-webkit-scrollbar{width:0px;background-color:#F5F5F5;overflow:hidden;.:: -webkit-scrollbar-thumb;blackboard-scrollbar      background-color:#555;border:2px solid #555555;}
        #favourites{z-index:200;left:50%;top:-2px;transform:translateX(-50%);width:80px;height:80px;text-align:center;border-radius:50%;padding-top:48px;margin-top:-49px;cursor:pointer;}
        #favourites-wrapper{position:absolute;display:none;z-index:201;left:50%;top:-2px;transform:translateX(-50%);}
        @media only screen and (max-width: 800px){
            .main-footer{text-align:center;}
            .main-footer div{width:100%;}
        }
        /*! CSS Used fontfaces */
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7qsDJT9g.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7jsDJT9g.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7rsDJT9g.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7ksDJT9g.woff2) format('woff2');unicode-range:U+0370-03FF;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7osDJT9g.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7psDJT9g.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
        @font-face{font-family:'Source Sans Pro';font-style:italic;font-weight:400;src:local('Source Sans Pro Italic'), local('SourceSansPro-Italic'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK1dSBYKcSV-LCoeQqfX1RYOo3qPZ7nsDI.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmhduz8A.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwkxduz8A.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmxduz8A.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlBduz8A.woff2) format('woff2');unicode-range:U+0370-03FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmBduz8A.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmRduz8A.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNa7lqDY.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qPK7lqDY.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNK7lqDY.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qO67lqDY.woff2) format('woff2');unicode-range:U+0370-03FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qN67lqDY.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNq7lqDY.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmhduz8A.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwkxduz8A.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmxduz8A.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlBduz8A.woff2) format('woff2');unicode-range:U+0370-03FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmBduz8A.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmRduz8A.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
        @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v13/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
        .component{
            display: inline-block;
            width: 49%;
            padding: 0 7px;
            vertical-align: top;
        }
        p{
            margin-bottom: 3px!important;
        }
        @media screen {
            table {
                page-break-inside: auto
            }

            tr {
                page-break-before: always !important;
            }

            thead {
                display: table-header-group
            }

            tbody {
                page-break-inside: avoid;
                page-break-after: auto
            }

            tfoot {
                display: table-footer-group
            }
        }
    </style>
</head>
<body>
<div class="content-wrapper">
    <div class="container-fluid container-title">
        <h3>Vendor Statement</h3>
    </div>
</div>
<div class="container-fluid">
    <hr>
    <div class="row">
        <div class="header-wrapper">
            <div class="component">
                <h5>Debtor Statement</h5>
                <div class="component">
                    <p class="text-right">As At</p>
                    <p>
                        <strong>{{isset($vendor_invoices[0]->company->company_name)?$vendor_invoices[0]->company->company_name:null}}</strong>
                    </p>
                    <p>Registration</p>
                    <p>VAT No. </p>
                    <p>Contact</p>
                    <p>Address <br>
                        @isset($vendor_invoices[0]->company->postal_address_line2) {!! '<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->postal_address_line3) {!! '<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->city_suburb) {!! '<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->state_province) {!! '<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->postal_zipcode) {!! '<br>' !!} @endisset
                    </p>
                    <p>Tel</p>
                    <p>Fax</p>
                    <p>Email</p>
                </div>
                <div class="component">
                    <p>: {{$statement_date->format('d-M-Y')}}</p>
                    <p>&nbsp;</p>
                    <p>: {{isset($vendor_invoices[0]->company->business_reg_no)?$vendor_invoices[0]->company->business_reg_no:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->vat_no)?$vendor_invoices[0]->company->vat_no:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->contact_firstname)?$vendor_invoices[0]->company->contact_firstname:null}} {{isset($vendor_invoices[0]->company->contact_lastname)?$vendor_invoices[0]->company->contact_lastname:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->postal_address_line1)?$vendor_invoices[0]->company->postal_address_line1:null}}<br>
                        @isset($vendor_invoices[0]->company->postal_address_line2) {!! '&nbsp; '.$vendor_invoices[0]->company->postal_address_line2.'<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->postal_address_line3) {!! '&nbsp; '.$vendor_invoices[0]->company->postal_address_line3.'<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->city_suburb) {!! '&nbsp; '.$vendor_invoices[0]->company->city_suburb.'<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->state_province) {!! '&nbsp; '.$vendor_invoices[0]->company->state_province.'<br>' !!} @endisset
                        @isset($vendor_invoices[0]->company->postal_zipcode) {!! '&nbsp; '.$vendor_invoices[0]->company->postal_zipcode.'<br>' !!} @endisset
                    </p>
                    <p>: {{isset($vendor_invoices[0]->company->phone)?$vendor_invoices[0]->company->phone:(isset($vendor_invoices[0]->company->cell)?$vendor_invoices[0]->company->cell:null)}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->fax)?$vendor_invoices[0]->company->fax:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->email)?$vendor_invoices[0]->company->email:null}}
                    </p>
                </div>
            </div>
            <div class="component">
                <div class="text-right" style="margin-bottom: 10px">
                    <img class="img-thumbnail" src="{{storage_path('/app/avatars/vendor/'.(isset($vendor_invoices[0]->vendor->vendor_logo)?$vendor_invoices[0]->vendor->vendor_logo:'default.png'))}}" alt="{{$vendor_invoices[0]->vendor->vendor_name}}" style="max-height: 150px">
                </div>
                <div class="component">
                    <p><strong>Banking Details</strong></p>
                    <p>Bank Account Name</p>
                    <p>Bank Name </p>
                    <p>Branch Name </p>
                    <p>Account No </p>
                    <p>Swift Code </p>
                </div>
                <div class="component">
                    <p>&nbsp;</p>
                    <p>: {{isset($vendor_invoices[0]->company->account_name)?$vendor_invoices[0]->company->account_name:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->bank_name)?$vendor_invoices[0]->company->bank_name:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->branch_name)?$vendor_invoices[0]->company->branch_name:(isset($vendor_invoices[0]->company->branch_code)?$vendor_invoices[0]->company->branch_code:null)}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->bank_acc_no)?$vendor_invoices[0]->company->bank_acc_no:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->company->swift_code)?$vendor_invoices[0]->company->swift_code:null}}</p>
                </div>
                <p>{{isset($vendor_invoices[0]->company->invoice_text)?$vendor_invoices[0]->company->invoice_text:null}}</p>
            </div>
            <div class="mt-5">
                <h5>Vendor</h5>
                <hr class="mt-0">
            </div>
            <div class="component">
                <p><strong>{{isset($vendor_invoices[0]->vendor->vendor_name)?$vendor_invoices[0]->vendor->vendor_name:null}}</strong></p>
                @if($vendor_invoices[0]->vendor->postal_address_line2) {!! '<p>'.$vendor_invoices[0]->vendor->postal_address_line2.'</p>' !!} @endif
                @if($vendor_invoices[0]->vendor->postal_address_line3) {!! '<p>'.$vendor_invoices[0]->vendor->postal_address_line3.'</p>' !!} @endif
                @if($vendor_invoices[0]->vendor->city_suburb) {!! '<p>'.$vendor_invoices[0]->vendor->city_suburb.'</p>' !!} @endif
                @if($vendor_invoices[0]->vendor->state_province) {!! '<p>'.$vendor_invoices[0]->vendor->state_province.'</p>' !!} @endif
                @if($vendor_invoices[0]->vendor->postal_zipcode) {!! '<p>'.$vendor_invoices[0]->vendor->postal_zipcode.'</p>' !!} @endif
                <p>{{($vendor_invoices[0]->vendor->country)?' '.$vendor_invoices[0]->vendor->country->name:null}}</p>
            </div>
            <div class="component">
                <div class="component">
                    <p>VAT No. </p>
                    <p>Vendor Code </p>
                    <p>Contact </p>
                    <p>Email </p>
                    <p>Tel
                    </p>
                </div>
                <div class="component">
                    <p>: {{isset($vendor_invoices[0]->vendor->vat_no)?$vendor_invoices[0]->vendor->vat_no:null}}</p>
                    <p>: {{$vendor_invoices[0]->vendor_id}}</p>
                    <p>: {{isset($vendor_invoices[0]->vendor->contact_firstname)?$vendor_invoices[0]->vendor->contact_firstname:null}} {{isset($vendor_invoices[0]->vendor->contact_lastname)?$vendor_invoices[0]->vendor->contact_lastname:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->vendor->email)?$vendor_invoices[0]->vendor->email:null}}</p>
                    <p>: {{isset($vendor_invoices[0]->vendor->phone)?$vendor_invoices[0]->vendor->phone:$vendor_invoices[0]->vendor->cell}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive mt-5">
        <table class="table table-sm table-striped">
            <thead class="bg-dark">
            <tr>
                <th>Doc #</th>
                <th>Trans Type</th>
                <th>Status</th>
                <th>Date</th>
                <th>Description</th>
                <th class="text-right">Nett Amount</th>
                <th class="text-right">Running Balance</th>
            </tr>
            </thead>
            <tbody>
            @if((int) $total_by_month->opening_balance)
                <tr>
                    <td colspan="3">Opening Balance</td>
                    <td colspan="3">{{$opening_bal_date->toDateString()}}</td>
                    <td class="text-right">{{number_format($total_by_month->opening_balance,2,'.',',')}}</td>
                </tr>
            @endif
            @forelse($vendor_invoices as $invoice)
                <tr>
                    <td colspan="7">Invoice {{$invoice->id}}</td>
                </tr>
                @if($invoice->vendor_invoice_status == 1)
                    <tr>
                        <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->vendor_invoice_ref}}</td>
                        <td>Sales Invoice</td>
                        <td>Open</td>
                        <td>{{$invoice->vendor_invoice_date}}</td>
                        <td>Sales Invoice {{$invoice->vendor_reference}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                    </tr>
                @endif
                @if($invoice->vendor_invoice_status == 2)
                    <tr>
                        <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->vendor_invoice_ref}}</td>
                        <td>Sales Invoice</td>
                        <td>Paid</td>
                        <td>{{$invoice->vendor_paid_date}}</td>
                        <td>Sales Invoice {{$invoice->vendor_reference}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->vendor_invoice_ref}}</td>
                        <td>Receipt</td>
                        <td></td>
                        <td>{{$invoice->vendor_paid_date}}</td>
                        <td>Payment for Invoice {{$invoice->id}}</td>
                        <td class="text-right">-{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                        <td class="text-right">{{number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{number_format(0,2,'.',',')}}</td>
                    </tr>
                @endif
                @if($invoice->vendor_invoice_status == 3)
                    <tr>
                        <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->vendor_invoice_ref}}</td>
                        <td>Sales Invoice</td>
                        <td>Cancelled</td>
                        <td>{{$invoice->vendor_paid_date}}</td>
                        <td>Sales Invoice {{$invoice->vendor_reference}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->vendor_invoice_value}}</td>
                        <td>Credit Note</td>
                        <td></td>
                        <td>{{$invoice->vendor_paid_date}}</td>
                        <td>Invoice {{$invoice->id}} Cancelled</td>
                        <td class="text-right">-{{number_format($invoice->vendor_invoice_value,2,'.',',')}}</td>
                        <td class="text-right">{{number_format(0,2,'.',',')}}</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{number_format(0,2,'.',',')}}</td>
                    </tr>
                @endif
            @empty
            @endforelse
            <tr style="page-break-after: always !important;">
                <td colspan="2"></td>
                <td colspan="5">
                    <table class="table table-sm table-striped">
                        <thead class="bg-gray">
                        <tr>
                            <th class="text-left">Total Amount Due</th>
                            <th class="text-right">Current</th>
                            <th class="text-right">30 Days</th>
                            <th class="text-right">60 Days</th>
                            <th class="text-right">90 + Days</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">{{number_format(($total_by_month->current + $total_by_month->thirty_days + $total_by_month->sixty_days + $total_by_month->ninety_days_plus),2,'.',',')}}</td>
                            <td class="text-right">{{number_format($total_by_month->current,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($total_by_month->thirty_days, 2,'.',',')}}</td>
                            <td class="text-right">{{number_format($total_by_month->sixty_days,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($total_by_month->ninety_days_plus,2,'.',',')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>