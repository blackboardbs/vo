@extends('adminlte.default')

@section('title') Prepare Vendor Statements @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="vendorstatements.index"></x-export>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <form class="mt-3 searchform" id="searchform">
        <div class="row">
            <div class="col-md-3" style="max-width:16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('company',$company_dropdown,request()->input('company', $config?->company_id) ,['class'=>'form-control  search'])}}
                <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width:16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                <div class="position-relative w-100">
                    <input class="form-control form-control-sm w-100" id="vendor" placeholder="Select Vendor">
                    <div class="position-absolute w-100 bg-gray-light shadow rounded list"></div>
                </div>
                <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width:16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('period_from',$dates_drop_down,(isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month),['class'=>'form-control search', 'placeholder' => 'Period From'])}}
                <span>From</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width:16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('period_to',$dates_drop_down,(isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month),['class'=>'form-control search', 'placeholder' => 'Period To'])}}
                <span>To</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width:16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}"  onkeypress="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 align-bottom" style="max-width:15%;">
                <a href="{{ route('vendorstatements.index') }}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead>
            <tr class="btn-dark">
                <th>Vendor Name</th>
                <th class="text-right">Total</th>
                <th class="text-right">Current</th>
                <th class="text-right">30 Days</th>
                <th class="text-right">60 Days</th>
                <th class="text-right">90+ Days</th>
                {{--@if($can_update)--}}
                    <th class="last">Action</th>
                {{--@endif--}}
            </tr>
            </thead>
            <tbody>
            @forelse($vendor_invoices as $result)
                <tr>
                    <td>{{isset($result->vendor->vendor_name)?$result->vendor->vendor_name:null}}</td>
                    <td class="text-right">{{$result->currency." ".number_format(($result->current + $result->thirty_days + $result->sixty_days + $result->ninety_days_plus),2,'.',',')}}</td>
                    <td class="text-right">{{$result->currency." ".number_format($result->current,2,'.',',')}}</td>
                    <td class="text-right">{{$result->currency." ".number_format($result->thirty_days,2,'.',',')}}</td>
                    <td class="text-right">{{$result->currency." ".number_format($result->sixty_days,2,'.',',')}}</td>
                    <td class="text-right">{{$result->currency." ".number_format($result->ninety_days_plus,2,'.',',')}}</td>
                    {{--@if($can_update)--}}
                    <td>
                        <a href="{{route('vendorstatements.show', [
                            'vendorstatement' => $result->vendor_id,
                            'period_from' => (isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month),
                            'period_to' => (isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month),
                            'currency' => $result->currency
                        ])}}" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i></a>
                    </td>
                   {{-- @endif--}}
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No data match those criteria.</td>
                </tr>
            @endforelse
            <tr>
                <th>Total</th>
                <th class="text-right">
                    @foreach ($currencies as $currency)
                        {{$currency}}: {{number_format(($total[$currency]+$total30[$currency]+$total60[$currency]+$total90[$currency]),2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total30 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total60 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total90 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                {{--@if($can_update)--}}
                <td>
                </td>
               {{-- @endif--}}
            </tr>
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $vendor_invoices->firstItem() }} - {{ $vendor_invoices->lastItem() }} of {{ $vendor_invoices->total() }}
                    </td>
                    <td>
                        {{ $vendor_invoices->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
        {{-- {{ $vendor_invoices->appends(request()->except('page'))->links() }} --}}
    </div>
</div>
@endsection

@section('extra-js')
    <script src="{{asset('js/filters.js')}}"></script>
    <script>
        $(document).ready(function (){
            const vendor_dropdowns = @json(session('vendor_dropdown'));
            const url = new URL(window.location.toLocaleString());

            $("#vendor").dropdownSearch({
                name: "vendor",
                dropdown: vendor_dropdowns !== null ? vendor_dropdowns : []
            });

            if (url.searchParams.get('vendor_id')){
                let customer = `<input type="hidden" name="vendor_id" value="${url.searchParams.get('vendor_id')}" >`;
                $("#searchform").append(customer)
            }
        })
    </script>
@endsection
@section('extra-css')
    <style>
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection
