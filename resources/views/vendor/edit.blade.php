@extends('adminlte.default')

@section('title') Edit Vendor @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('vendors.update',$vendor), 'method' => 'patch','class'=>'mt-3', 'files' => true, 'autocomplete' => 'off','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Vendor Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="5">Vendor Logo:</th>
                    <td rowspan="5"><div class="img-wrapper"><img src="{{route('vendor_avatar',['q'=>$vendor->vendor_logo])}}" id="blackboard-preview-large" /></div>
                        {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Company Logo','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                        @foreach($errors->get('avatar') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th>Vendor Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('vendor_name',$vendor->vendor_name,['class'=>'form-control form-control-sm'. ($errors->has('vendor_name') ? ' is-invalid' : ''),'placeholder'=>'Vendor Name'])}}
                        @foreach($errors->get('vendor_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Registration Number:</th>
                    <td>{{Form::text('business_reg_no',$vendor->business_reg_no,['class'=>'form-control form-control-sm'. ($errors->has('business_reg_no') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}
                        @foreach($errors->get('business_reg_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>VAT Number:</th>
                    <td>{{Form::text('vat_no',$vendor->vat_no,['class'=>'form-control form-control-sm'. ($errors->has('vat_no') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Account Manager: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('account_manager',$users_dropdown,$vendor->account_manager,['class'=>'form-control form-control-sm '. ($errors->has('account_manager') ? ' is-invalid' : ''), 'placeholder' => 'Select Account Manager'])}}
                        @foreach($errors->get('account_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Assignment Approver: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('assignment_approver',$users_dropdown,$vendor->assignment_approver,['class'=>'form-control form-control-sm '. ($errors->has('assignment_approver') ? ' is-invalid' : ''), 'placeholder' => "Select Assignment Approver"])}}
                        @foreach($errors->get('assignment_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Payment Terms Text:</th>
                    <td>{{Form::text('payment_terms',$vendor->payment_terms,['class'=>'form-control form-control-sm'. ($errors->has('payment_terms') ? ' is-invalid' : ''),'placeholder'=>'Payment Terms'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Payment Terms Days:</th>
                    <td>{{Form::select('payment_terms_days',$payment_terms_days_dropdown, $vendor->payment_terms_days,['class'=>'form-control form-control-sm '. ($errors->has('payment_terms_days') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('payment_terms_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Billing Period: </th>
                    <td>{{Form::text('billing_period',$vendor->billing_period,['class'=>'form-control form-control-sm'. ($errors->has('billing_period') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billing_period') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Firstname:</th>
                    <td>{{Form::text('contact_firstname',$vendor->contact_firstname,['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Lastname:</th>
                    <td>{{Form::text('contact_lastname',$vendor->contact_lastname,['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}
                        @foreach($errors->get('contact_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Birthday:</th>
                    <td>{{Form::text('contact_birthday',$vendor->contact_birthday,['class'=>'datepicker form-control form-control-sm'. ($errors->has('contact_birthday') ? ' is-invalid' : ''),'placeholder'=>'Contact Birthday'])}}
                        @foreach($errors->get('contact_birthday') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Invoice Contact</th>
                    <td>{{Form::select('invoice_contact_id',$invoice_contact_dropdown, $vendor->invoice_contact_id,['class'=>'datepicker form-control form-control-sm'. ($errors->has('invoice_contact_id') ? ' is-invalid' : ''),'placeholder'=>'Select Invoice Contact'])}}
                        @foreach($errors->get('invoice_contact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',$vendor->phone,['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell:</th>
                    <td>{{Form::text('cell',$vendor->cell,['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('cell') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{Form::text('fax',$vendor->fax,['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell 2:</th>
                    <td>{{Form::text('cell2',$vendor->cell2,['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                        @foreach($errors->get('cell2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('email',$vendor->email,['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Web:</th>
                    <td>{{Form::text('web',$vendor->web,['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}
                        @foreach($errors->get('web') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Postal Address:</th>
                    <td>{{Form::text('postal_address_line1',$vendor->postal_address_line1,['class'=>'form-control form-control-sm'. ($errors->has('postal_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 1'])}}
                        @foreach($errors->get('postal_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line2',$vendor->postal_address_line2,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 2'])}}
                        @foreach($errors->get('postal_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line3',$vendor->postal_address_line3,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 3'])}}
                        @foreach($errors->get('postal_address_line3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('city_suburb',$vendor->city_suburb,['class'=>'form-control form-control-sm input-pad'. ($errors->has('city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_zipcode',$vendor->postal_zipcode,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('postal_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                    <th>Street Address:</th>
                    <td>{{Form::text('street_address_line1',$vendor->street_address_line1,['class'=>'form-control form-control-sm'. ($errors->has('street_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 1'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line2',$vendor->street_address_line2,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 2'])}}
                        @foreach($errors->get('street_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line3',$vendor->street_address_line3,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 3'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_city_suburb',$vendor->street_city_suburb,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('street_city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_zipcode',$vendor->street_zipcode,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('street_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                </tr>
                <tr>
                    <th>State/Province:</th>
                    <td>{{Form::text('state_province',$vendor->state_province,['class'=>'form-control form-control-sm'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                        @foreach($errors->get('state_province') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Country:</th>
                    <td>{{Form::select('country_id',$countries_dropdown,$vendor->country_id,['class'=>'form-control form-control-sm '. ($errors->has('country_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_countries'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{Form::select('bbbee_level',$bbbee_level_dropdown,$vendor->bbbee_level,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BBBEE Certificate Expiry:</th>
                    <td>{{Form::text('bbbee_certificate_expiry',$vendor->bbbee_certificate_expiry,['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Supply Chain Appointment Manager:</th>
                    <td>{{Form::select('supply_chain_appointment_manager_id',$appointment_manager_dropdown,$vendor->supply_chain_appointment_manager_id,['class'=>'form-control form-control-sm '. ($errors->has('supply_chain_appointment_manager_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Supply Chain Appointment Manager'])}}
                        @foreach($errors->get('supply_chain_appointment_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach</td>
                    <th>Onboard New Vendor :</th>
                    <td>{{Form::checkbox('onboarding',1,$vendor->onboarding,['class'=>'form-check-input ml-0'. ($errors->has('onboarding') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('onboarding') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th colspan="4" class="btn-dark">Templates</th>
                </tr>
                <tr>
                    <th>Service Agreement Template:</th>
                    <td>{{Form::select('service_agreement_id',$templates_dropdown,$vendor->service_agreement_id,['class'=>'form-control form-control-sm '. ($errors->has('service_agreement_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('service_agreement_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                    <th>NDA Template:</th>
                    <td>{{Form::select('nda_template_id',$nda_templates_dropdown,$vendor->nda_template_id,['class'=>'form-control form-control-sm '. ($errors->has('nda_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('nda_template_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Banking Details</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Bank Name:</th>
                    <td>{{Form::text('bank_name',$vendor->bank_name,['class'=>'form-control form-control-sm'. ($errors->has('bank_name') ? ' is-invalid' : ''),'placeholder'=>'Bank Name'])}}
                        @foreach($errors->get('bank_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Branch Code:</th>
                    <td>{{Form::text('branch_code',$vendor->branch_code,['class'=>'form-control form-control-sm'. ($errors->has('branch_code') ? ' is-invalid' : ''),'placeholder'=>'Branch Code'])}}
                        @foreach($errors->get('branch_code') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Account Number:</th>
                    <td>{{Form::text('bank_acc_no',$vendor->bank_acc_no,['class'=>'form-control form-control-sm'. ($errors->has('bank_acc_no') ? ' is-invalid' : ''),'placeholder'=>'Account Number'])}}
                        @foreach($errors->get('bank_acc_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Branch Name:</th>
                    <td>{{Form::text('branch_name',$vendor->branch_name,['class'=>'form-control form-control-sm'. ($errors->has('branch_name') ? ' is-invalid' : ''),'placeholder'=>'Branch Name'])}}
                        @foreach($errors->get('branch_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Account Name:</th>
                    <td>{{Form::text('account_name',$vendor->account_name,['class'=>'form-control form-control-sm'. ($errors->has('account_name') ? ' is-invalid' : ''),'placeholder'=>'Account Name'])}}
                        @foreach($errors->get('account_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Swift Code:</th>
                    <td>{{Form::text('swift_code',$vendor->swift_code,['class'=>'form-control form-control-sm'. ($errors->has('swift_code') ? ' is-invalid' : ''),'placeholder'=>'Swift Code'])}}
                        @foreach($errors->get('swift_code') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Bank Account Type:</th>
                    <td>{{Form::select('bank_account_type_id',$bank_account_type_drop_down,$vendor->bank_account_type_id,['class'=>'form-control form-control-sm '. ($errors->has('bank_account_type_id') ? ' is-invalid' : ''),'placeholder'=>'Bank Account Type'])}}
                        @foreach($errors->get('bank_account_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
