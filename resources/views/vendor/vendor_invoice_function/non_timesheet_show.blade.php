@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <a href="{{ route('vendor.invoice') }}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container" id="invoice_div">
        <hr />
        {{--{!! Form::open(['url' => route('nontimesheetinvoice.send_vendor', $vendorInvoice->id), 'method' => 'post', 'class' => 'mb-4', 'id' => 'save_invoice']) !!}
--}}
        <div class="row no-gutters">
            <div class="col-md-4 pl-2">
                <h4>Vendor Tax Invoice</h4>
                @if($vendorInvoice->vendor != null)
                    <h5>{{isset($vendorInvoice->vendor->vendor_name)?$vendorInvoice->vendor->vendor_name:null}}</h5>
                    <p class="mb-0">
                        {{isset($vendorInvoice->vendor->postal_address_line1)?$vendorInvoice->vendor->postal_address_line1:null}}
                        @empty($vendorInvoice->vendor->postal_address_line1)
                            {{isset($vendorInvoice->vendor->street_address_line1)?$vendorInvoice->vendor->street_address_line1:null}}
                        @endempty
                    </p>
                    <p class="mb-0">
                        {{isset($vendorInvoice->vendor->postal_address_line2)?$vendorInvoice->vendor->postal_address_line2:null}}
                        @empty($vendorInvoice->vendor->postal_address_line2)
                            {{isset($vendorInvoice->vendor->street_address_line2)?$vendorInvoice->vendor->street_address_line2:null}}
                        @endempty

                    </p>
                    <p class="mb-0">
                        {{isset($vendorInvoice->vendor->postal_address_line3)?$vendorInvoice->vendor->postal_address_line3:null}}
                        @empty($vendorInvoice->vendor->postal_address_line3)
                            {{isset($vendorInvoice->vendor->street_address_line3)?$vendorInvoice->vendor->street_address_line3:null}}
                        @endempty
                    </p>
                    <p class="mb-0">
                        {{$vendorInvoice->vendor->city_suburb}}
                        @empty($vendorInvoice->vendor->city_suburb)
                            {{$vendorInvoice->vendor->street_city_suburb}}
                        @endempty
                    </p>
                    <p class="mb-0">{{$vendorInvoice->vendor->state_province}}</p>
                    <p class="mb-0">
                        {{$vendorInvoice->vendor->postal_zipcode}}
                        @empty($vendorInvoice->vendor->postal_zipcode)
                            {{ $vendorInvoice->vendor->street_zipcode }}
                        @endempty
                    </p>
                    <p class="mb-0">{{$vendorInvoice->vendor->phone}}</p>
                    <p class="mb-0">{{$vendorInvoice->vendor->email}}</p>
                    <p class="mb-0">Contact: {{ $vendorInvoice->vendor->contact_firstname." ".$vendorInvoice->vendor->contact_lastname }}</p>
                    <p class="mb-0">Vat No: {{ $vendorInvoice->vendor->vat_no }}</p>
                @endif
            </div>
            <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{$vendorInvoice->company->company_name}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->postal_address_line1}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->postal_address_line2}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->postal_address_line3}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->state_province}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->postal_zipcode}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->phone}}</p>
                <p class="mb-0">{{ $vendorInvoice->company->email}}</p>
                <p class="mb-0">VAT NO: {{ $vendorInvoice->company->vat_no}}</p>
            </div>
            <div class="col-md-4">
                {{--<div class="text-right mb-3"><img src="{{route('vendor_avatar',['q'=>$vendorInvoice->vendor->vendor_logo])}}" alt="{{$vendorInvoice->vendor->vendor_name}} Logo" style="max-height: 150px"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('vendor_avatar', ['q'=> (isset($vendorInvoice->vendor->vendor_logo)?$vendorInvoice->vendor->vendor_logo:'default.png')])}}" alt="{{$vendorInvoice->vendor->vendor_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{$vendorInvoice->vendor_invoice_number}}</div>
                    <div class="col-md-6">Invoice Date:</div>
                    <div class="col-md-6 text-right">{{$vendorInvoice->date}}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $vendorInvoice->vendor_invoice_date }}</div>
                    <div class="col-md-4">Vendor Reference:</div>
                    <div class="col-md-8 text-right">{{$vendorInvoice->vendor_reference}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ isset($vendorInvoice->vendor)? $vendorInvoice->vendor->account_managerd->first_name.' '.$vendorInvoice->vendor->account_managerd->last_name : '' }}</div>
                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Description</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right">Excl. Price</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">VAT%</th>
                            <th class="text-right">Exclusive Total</th>
                            <th class="text-right">Inclusive Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total_quantity = 0;
                            $total_exclusive = 0;
                            $total_vat = 0;
                            $total_invoice_amount = 0;
                        @endphp
                        @forelse($vendorInvoiceItems as $vendorInvoiceItem)
                            @php
                                $total_quantity += $vendorInvoiceItem->quantity;
                                $total_exclusive += ($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount;
                                $total_vat += (($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100);
                                $total_invoice_amount += (($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) + ((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100));
                            @endphp
                            <tr>
                                <td>{{$vendorInvoiceItem->description}}</td>
                                <td class="text-right">{{$vendorInvoiceItem->quantity}}</td>
                                <td class="text-right">{{number_format(($vendorInvoiceItem->price_excl),2,'.',',') }}</td>
                                <td class="text-right">{{number_format($vendorInvoiceItem->discount_amount,2,'.',',')}}</td>
                                <td class="text-right">{{number_format($vendorInvoiceItem->vat_percentage,2,'.',',')}}</td>
                                <td class="text-right">{{number_format(($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount,2,'.',',')}}</td>
                                <td class="text-right">{{number_format((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) + ((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100)),2,'.',',')}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No invoice items found.</td>
                            </tr>
                        @endforelse
                        <tr><th class="text-right">TOTAL</th><th colspan="6">{{number_format($total_quantity,2,'.',',')}}</th></tr>
                        {{--@forelse($timesheets as $index => $timeline)
                            <tr>
                                <td>Timesheet for {{ $timeline->employee->first_name.' '.$timeline->employee->last_name }} for week {{ $timeline->year_week }} on assignment {{ $timeline->project->name }} {{isset($timeline->customer_po)?' customer po number '.$timeline->customer_po:''}}</td>
                                {{Form::hidden('start_date[]', $timeline->start_date) }}
                                {{Form::hidden('end_date[]', $timeline->start_date) }}
                                {{Form::hidden('quantity[]', $timeline->billable_hours) }}
                                {{Form::hidden('customer_po[]', $timeline->customer_po)}}
                                <td>{{ number_format($timeline->billable_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timeline->external_cost_rate,2,'.',',') }} </td>
                                {{Form::hidden('rate[]', $timeline->external_cost_rate)}}
                                <td class="text-right">{{$vat_rate[$index]}}%</td>
                                <td class="text-right">R {{number_format($timeline->billable_hours * $timeline->external_cost_rate ,2,'.',',') }}</td>
                                <td class="text-right">R {{ number_format(($timeline->billable_hours * $timeline->external_cost_rate * (($vat_rate[$index] != 0) ? $vat_rate[$index]/100 : 0) ) + ($timeline->billable_hours * $timeline->external_cost_rate),2,'.',',') }}</td>
                                {{Form::hidden('price[]', ($timeline->billable_hours * $timeline->external_cost_rate * (($vat_rate[$index] != 0) ? $vat_rate[$index]/100 : 0)) + ($timeline->billable_hours * $timeline->external_cost_rate))}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">Nothing to show here</td>
                            </tr>
                        @endforelse
                        <tr>
                            <th class="text-right">TOTAL HOURS</th>
                            <th colspan="5">{{number_format($total_hours,2,'.',',')}}</th>
                        </tr>--}}

                        {{--@if(count($time_exp))
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="btn-dark">
                                <th colspan="6">Expenses</th>
                            </tr>
                        @endif

                        @foreach($time_exp as $expense)
                            <tr>
                                <td>Expenses for {{ isset($expense->timesheet->employee)?$expense->timesheet->employee->first_name.' '.$expense->timesheet->employee->last_name:'' }} for week {{ $expense->timesheet->year_week }} on assignment {{ $expense->timesheet->project->name }} for {{ $expense->description }}.</td>
                                <td>1</td>
                                <td class="text-right">{{ number_format($expense->amount,2,'.',',') }}</td>
                                <td class="text-right">0.00%</td>
                                <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                {{Form::hidden('time_exp_id[]', $expense->id) }}
                            </tr>
                        @endforeach--}}

                        <tr>
                            <th colspan="7">&nbsp;</th>
                        </tr>

                        <tr class="btn-dark">
                            <th colspan="7">Summary</th>
                        </tr>
                        <tr>
                            <th>Payment Information</th>
                            <td colspan="4">{{ $vendorInvoice->vendor_invoice_note }}</td>
                            <th class="text-right">Total Exclusive:</th>
                            <td class="text-right">{{$vendorInvoice->currency}} {{ number_format(($total_exclusive) ,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">{{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="5">{{  isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->bank_name : '' }}</td>
                            <th class="text-right">Total VAT:</th>
                            <td class="text-right">{{$vendorInvoice->currency}} {{ number_format($total_vat,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">Branch Code: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->branch_code : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="5">Account Number: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->bank_acc_no : '' }}</td>
                            <th class="text-right">Total:</th>
                            <td class="text-right">{{$vendorInvoice->currency}} {{ number_format(($total_invoice_amount),2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">Reference: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <th colspan="7" class="text-center">Thank you for your business!</th>
                        </tr>

                        </tbody>
                    </table>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 col-print-12">
                            @if(request()->recurring)
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        {!! Form::open(['url' => route('nontimesheetinvoice.unallocate_vendor', $vendorInvoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                                        <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="{{route('vendor.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fa fa-clock-o"></i> Done</a>
                                    </div>
                                </div>
                            @elseif($vendorInvoice->vendor_invoice_status == 1)
                                <div class="row">
                                    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('admin_manager') || auth()->user()->hasRole('manager'))
                                        <div class="col-md-3" style="max-width:20%">
                                            {!! Form::open(['url' => route('nontimesheetinvoice.unallocate_vendor', $vendorInvoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                                            <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="col-md-3 text-center" style="max-width:20%">
                                            <a href="{{route('process.payment', $vendorInvoice->id)}}" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-university"></i> Process Payment</a>
                                        </div>
                                        <div class="col-md-3 text-center" style="max-width:20%">
                                            <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                        </div>
                                        
                                        <div class="col-md-3 text-center" style="max-width:20%">
                                        <a href="{{route('vendor.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-save"></i> Save Invoice</a>
                                        </div>
                                        <div class="col-md-3 text-right" style="max-width:20%">
                                            <a href="{{route('nontimesheetinvoice.send_vendor', ['vendorInvoice' => $vendorInvoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                        </div>
                                    @else
                                        <div class="col-md-3">
                                            {!! Form::open(['url' => route('unallocate.invoice', $invoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                                            <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                        <a href="{{route('vendor.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-save"></i> Save Invoice</a>
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <a href="{{route('nontimesheetinvoice.send_vendor', ['vendorInvoice' => $vendorInvoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                        </div>
                                    @endif
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="{{route('nontimesheetinvoice.send_vendor', ['vendorInvoice' => $vendorInvoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{--<div class="col-md-12 text-right">
                            <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                        </div>--}}
                    </div>
                </div>
            </div>

        {!! Form::open(['url' => route('nontimesheetinvoice.send_vendor', $vendorInvoice->id), 'method' => 'post', 'class' => 'mb-4', 'id' => 'save_invoice']) !!}
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                            <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="email-form">
                                <div class="form-group row">
                                    <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject" value="Vendor Tax Invoice">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-check">
                                    <input type="checkbox" name="customer_email" value="{{$vendorInvoice->vendor->email}}" checked class="form-check-input" id="customer_email">
                                    <label class="form-check-label" for="customer_email">{{$vendorInvoice->vendor->email}}</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" name="company_email" value="{{isset($vendorInvoice->company)?$vendorInvoice->company->email:''}}" checked class="form-check-input" id="company_email">
                                    <label class="form-check-label" for="company_email">{{isset($vendorInvoice->company)?$vendorInvoice->company->email:''}}</label>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group text-center">
                                    <button class="btn btn-dark btn-sm" id="btn-send"><i class="fas fa-paper-plane"></i> Send Invoice</button>
                                </div>
                            </div>
                            <div id="loader-wrapper">
                                <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--{!! Form::close() !!}--}}
    </div>
@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{global_asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endsection

@section("extra-js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{global_asset('js/multiple-emails.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })

            $(function () {
                $('#user_email').multiple_emails({
                    position: 'top', // Display the added emails above the input
                    theme: 'bootstrap', // Bootstrap is the default theme
                    checkDupEmail: true // Should check for duplicate emails added
                });

                $('#current_emails').text($('#user_email').val());

                $('#user_email').change( function(){
                    $('#current_emails').text($(this).val());
                });
            })

            $(function () {
                $("#btn-send").on('click', function () {
                    $("#save_invoice")
                        .append('<input type="hidden" name="send" value="1" /> ')
                        .append($("#customer_email"))
                        .append($("#company_email"))
                        .append($("#user_email"))
                        .append($("#subject"))
                        .submit();

                        $("#email-form").hide();
                        $("#loader-wrapper").fadeIn();
                        $(".default").hide();
                        $(".sending").fadeIn();
                })
            })
        });

    </script>
@endsection
