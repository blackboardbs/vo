<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,
        html {
            font-family: arial, sans-serif;
            font-size: 0.9rem
        }

        .container {
            width: 100%;
            margin: 0 auto
        }

        .cols-3 {
            width: 32.8%;
            display: inline-block
        }

        .pull-right {
            float: right
        }

        .text-right {
            text-align: right
        }

        .text-left {
            text-align: left
        }

        .clearfix {
            clear: both
        }

        .pb-3, .py-3 {
            padding-bottom: 1rem!important;
        }
        .btn-dark {
            color: #fff;
            background-color: #343a40;
            border-color: #343a40;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
        }

        .table {
            border-collapse: collapse;
            width: 100%
        }

        td,
        th {
            border-bottom: 1px solid #ddd;
            padding-top: 14px;
            padding-bottom: 14px
        }

        tr:hover {
            background-color: #ddd
        }

        .bg-dark,
        .bg-dark:hover {
            color: #fff;
            background-color: #343a40!important;
            border-color: #343a40;
            box-shadow: 0 1px 1px rgba(0, 0, 0, .075)
        }

        hr {
            border-top: 1px solid #ddd;
            margin-top: 10px
        }

        p {
            margin: 5px 0
        }
        .inv-img-wrapper img, .img-wrapper img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin: 0 auto;
        }
        .right-side{
            display: inline-block;
            width: 49%;
            margin-bottom: 5px;
        }
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-4 cols-3 pl-2">
                <h4>Vendor Tax Invoice</h4>
                <h5>{{$vendorInvoice->vendor->vendor_name}}</h5>
                <p class="mb-0">
                    {{$vendorInvoice->vendor->postal_address_line1}}
                    @empty($vendorInvoice->vendor->postal_address_line1)
                        {{$vendorInvoice->vendor->street_address_line1}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendorInvoice->vendor->postal_address_line2}}
                    @empty($vendorInvoice->vendor->postal_address_line2)
                        {{$vendorInvoice->vendor->street_address_line2}}
                    @endempty

                </p>
                <p class="mb-0">
                    {{$vendorInvoice->vendor->postal_address_line3}}
                    @empty($vendorInvoice->vendor->postal_address_line3)
                        {{$vendorInvoice->vendor->street_address_line3}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendorInvoice->vendor->city_suburb}}
                    @empty($vendorInvoice->vendor->city_suburb)
                        {{$vendorInvoice->vendor->street_city_suburb}}
                    @endempty
                </p>
                <p class="mb-0">{{$vendorInvoice->vendor->state_province}}</p>
                <p class="mb-0">
                    {{$vendorInvoice->vendor->postal_zipcode}}
                    @empty($vendorInvoice->vendor->postal_zipcode)
                        {{ $vendorInvoice->vendor->street_zipcode }}
                    @endempty
                </p>
                <p class="mb-0">{{ $vendorInvoice->vendor->phone }}</p>
                <p class="mb-0">{{ $vendorInvoice->vendor->email }}</p>
                <p class="mb-0">Contact: {{ $vendorInvoice->vendor->contact_firstname." ".$vendorInvoice->vendor->contact_lastname }}</p>
                <p class="mb-0">Vat No: {{ $vendorInvoice->vendor->vat_no }}</p>
            </div>
            <div class="col-md-4 cols-3">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{ isset($vendorInvoice->company->company_name) ? $vendorInvoice->company->company_name : '' }}</p>
                <p class="mb-0">{{ isset($vendorInvoice->company->postal_address_line1) ? $vendorInvoice->company->postal_address_line1: '' }}</p>
                <p class="mb-0">{{ isset($vendorInvoice->company->postal_address_line2) ? $vendorInvoice->company->postal_address_line2 : '' }}</p>
                @if($vendorInvoice->company->postal_address3 != null)
                    <p class="mb-0">{{ isset($vendorInvoice->company->postal_address_line3) ? $vendorInvoice->company->postal_address_line3 : ''}}</p>
                @endif
                <p class="mb-0">{{ isset($vendorInvoice->company->state_province) ? $vendorInvoice->company->state_province : '' }}</p>
                <p class="mb-0">{{ $vendorInvoice->company->postal_zipcode }}</p>
                <p class="mb-0">{{ $vendorInvoice->company->phone }}</p>
                <p class="mb-0">{{ $vendorInvoice->company->email }}</p>
                <p class="mb-0">VAT NO: {{ $vendorInvoice->company->vat_no }}</p>
            </div>
            <div class="col-md-4 cols-3">
                {{--<div class="text-right mb-3"><img src="{{route('vendor_avatar',['q'=>$vendorInvoice->vendor->vendor_logo])}}" alt="{{$vendorInvoice->vendor->vendor_name}} Logo"  style="max-height: 150px;"></div>--}}
                <div class="row text-right mb-3">
                    <div class="inv-img-wrapper" style="margin-left: auto;">
                        @if(is_file(storage_path('/app/avatars/vendor/'.$vendorInvoice->vendor->vendor_logo)))
                            <img class="pull-right" src="{{storage_path('/app/avatars/vendor/'.$vendorInvoice->vendor->vendor_logo)}}" alt="{{$vendorInvoice->vendor->vendor_name}} Logo" style="max-height: 150px;" align="right">
                        @else
                            <img class="pull-right" src="{{public_path("/assets/consulteaze_logo.png")}}" alt="{{$vendorInvoice->vendor->vendor_name}} Logo" style="max-height: 150px;" align="right">
                        @endif

                    </div>
                </div>
                <div class="row no-gutter clearfix">
                    <div>
                        <div class="right-side">Invoice Number:</div>
                        <div class="right-side text-right float-right">{{ $vendorInvoice->vendor_invoice_number }}</div>
                    </div>
                    <div>
                        <div class="right-side">Invoice Date:</div>
                        <div class="right-side text-right">{{$vendorInvoice->vendor_invoice_date}}</div>
                    </div>

                    <div>
                        <div class="right-side">Due Date:</div>
                        <div class="right-side text-right">{{ $vendorInvoice->vendor_due_date }}</div>
                    </div>
                    <div>
                        <div class="right-side">Vendor Reference:</div>
                        <div class="right-side text-right">{{$vendorInvoice->vendor_reference}}</div>
                    </div>
                    <div>
                        <div class="right-side">Sales Rep:</div>
                        <div class="right-side text-right">{{ isset($vendorInvoice->vendor)? $vendorInvoice->vendor->account_managerd->first_name.' '.$vendorInvoice->vendor->account_managerd->last_name : '' }}</div>
                    </div>

                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th class="text-left">Description</th>
                            <th class="text-right" style="width: 80px">Qty</th>
                            <th class="text-right" style="width: 80px">Excl.<br>Price</th>
                            <th class="text-right" style="width: 80px">Discount<br>Amount</th>
                            <th class="text-right"  style="width: 80px">VAT%</th>
                            <th class="text-right"  style="width: 120px">Exclusive<br>Total</th>
                            <th class="text-right"  style="width: 120px">Inclusive<br>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total_quantity = 0;
                            $total_exclusive = 0;
                            $total_vat = 0;
                            $total_invoice_amount = 0;
                        @endphp
                        @forelse($vendorInvoiceItems as $vendorInvoiceItem)
                        @php
                            $total_quantity += $vendorInvoiceItem->quantity;
                            $total_exclusive += ($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount;
                            $total_vat += (($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100);
                            $total_invoice_amount += (($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) + ((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100));
                        @endphp
                        <tr>
                            <td>{{$vendorInvoiceItem->description}}</td>
                            <td class="text-right">{{$vendorInvoiceItem->quantity}}</td>
                            <td class="text-right">{{number_format(($vendorInvoiceItem->price_excl),2,'.',',') }}</td>
                            <td class="text-right">{{number_format($vendorInvoiceItem->discount_amount,2,'.',',')}}</td>
                            <td class="text-right">{{number_format($vendorInvoiceItem->vat_percentage,2,'.',',')}}</td>
                            <td class="text-right">{{number_format(($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount,2,'.',',')}}</td>
                            <td class="text-right">{{number_format((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) + ((($vendorInvoiceItem->price_excl * $vendorInvoiceItem->quantity)-$vendorInvoiceItem->discount_amount) * ($vendorInvoiceItem->vat_percentage / 100)),2,'.',',')}}</td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="6">No invoice items found.</td>
                            </tr>
                        @endforelse
                        <tr><th class="text-left">TOTAL</th><th class="text-right">{{number_format($total_quantity,2,'.',',')}}</th><td colspan="5">&nbsp;</td></tr>
                        {{--@forelse($vendor_invoice_lines as $key => $vendor_invoice_line)
                            <tr>
                                <td>{!! $vendor_invoice_line->line_text !!}</td>
                                <td class="text-right">{{ $vendor_invoice_line->quantity }}</td>
                                <td class="text-right">{{ number_format($vendor_invoice_line->price,2,'.',',') }}</td>
                                <td class="text-right">{{number_format($vat_rate[$key],2,'.',',')}}%</td>
                                <td class="text-right">R{{ number_format($vendor_invoice_line->quantity * $vendor_invoice_line->price ,2,'.',',') }}</td>
                                <td class="text-right">R{{ number_format($vendor_invoice_line->quantity * $vendor_invoice_line->price * (($vat_rate[$key] > 0)?$vat_rate[$key]/100:0) + ($vendor_invoice_line->quantity * $vendor_invoice_line->price) ,2,'.',',') }}</td>
                            </tr>
                        @empty
                        @endforelse
                        <tr>
                            <th class="text-right">TOTAL HOURS &nbsp; &nbsp;</th>
                            <th class="text-right">{{number_format($total_hours,2,'.',',')}}</th>
                            <th colspan="4"></th>
                        </tr>--}}

                        {{--@php $tote = 0; @endphp
                        @if(count($vendor_invoice_line_exp) > 0)
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="bg-dark">
                                <th colspan="6" class="text-left">Expenses</th>
                            </tr>
                        @endif
                        @forelse($vendor_invoice_line_exp as $index => $expense)
                            @isset($expense)
                                <tr>
                                    <td>{!! wordwrap($expense->line_text, 75, "<br />\n") !!}</td>
                                    <td>1</td>
                                    <td>R {{ number_format($expense->amount,2,".",",") }}</td>
                                    <td class="text-center">{{number_format(0,2,'.',',')}}%</td>
                                    <td class="text-right">R {{ number_format($expense->amount,2,".",",") }}</td>
                                    <td class="text-right">R {{ number_format($expense->amount,2,".",",") }}</td>
                                </tr>
                                @php
                                    $tote += $expense->amount;
                                @endphp
                            @endisset
                        @empty
                        @endforelse--}}
                        <tr>
                            <th colspan="7">&nbsp;</th>
                        </tr>
                        <tr class="btn-dark">
                            <th colspan="7" class="text-left">Summary</th>
                        </tr>
                        <tr>
                            <th class="text-left">Payment Information</th>
                            <td colspan="4">{{ $vendorInvoice->vendor_invoice_notes }}</td>
                            <th class="text-right">Total Exclusive:</th>
                            <td class="text-right">R {{ number_format(($total_exclusive) ,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">{{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="5">{{  isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->bank_name : '' }}</td>
                            <th class="text-right">Total VAT:</th>
                            <td class="text-right">R {{ number_format($total_vat,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">Branch Code: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->branch_code : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="5">Account Number: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->bank_acc_no : '' }}</td>
                            <th class="text-right">Total:</th>
                            <td class="text-right">R {{ number_format($total_invoice_amount,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="7">Reference: {{ isset($vendorInvoice->vendor) ? $vendorInvoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <th colspan="7" class="text-center">Thank you for your business!</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
