<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }
        #invoice-info{
            width: 25%;
            height: 100%;
            display: inline-block;
            background-color: #343a40!important;
            color: #fff!important;
            text-align: center!important;
            padding-right: 7.5px;
            padding-left: 7.5px;
        }
        #invoice-items{
            width: 71%;
            display: inline-block;
            vertical-align: top;
            padding-left: 1rem;
        }
        .bg-dark{
            background-color: #343a40!important;
            color: #fff!important;
        }
        .img-thumbnail {
            padding: .25rem;
            background-color: #fff;
            border: 1px solid #dee2e6;
            border-radius: .25rem;
            box-shadow: 0 1px 2px rgba(0,0,0,.075);
            max-width: 100%;
            height: auto;
            display: block;
            margin: 0.5rem auto;
        }
        .h4, h4 {
            font-size: 1.5rem;
        }
        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.2;
            color: inherit;
        }
        .h5, h5 {
            font-size: 1.25rem;
        }
        .mt-4, .my-4 {
            margin-top: 1.5rem!important;
        }
        .mt-5, .my-5 {
            margin-top: 3rem!important;
        }
        p {
            line-height: 1.4rem!important;
            margin-bottom: 0.7rem;
        }
        b, strong {
            font-weight: bolder;
        }
        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible;
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0,0,0,.1);
        }
        .hr-center{
            margin-left: auto;
            margin-right: auto;
        }
        h1{
            letter-spacing: 0.8rem;
            text-transform: uppercase!important;
            font-size: 2.5rem;
            margin-top: 0;
        }
        .title{
            margin-top: 6rem;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            margin-top: 3rem;
            background-color: transparent;
            border-collapse: collapse;
        }
        .table>thead>tr{
            background-color: #343a40!important;
            color: #fff!important;
        }
        .table thead th {
            vertical-align: top;
            border-bottom: 2px solid #dee2e6;

        }
        .table td, .table th {
            padding: .75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        th {
            text-align: inherit;
        }
        .table td, .table th {
            padding: .75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .text-right{
            text-align: right;
        }
        .text-left{
            text-align: left;
        }
        .text-center{
            text-align: center;
        }
        .text-uppercase{
            text-transform: uppercase;
        }
    </style>
</head>

<body>
    <div id="invoice-info">
        <img class="img-thumbnail" src="{{storage_path('/app/avatars/vendor/'.($vendor->vendor_logo??'default.png'))}}" alt="{{$vendor->vendor_name??null}} Logo" style="max-height: 150px; max-width: 195px" >
        <h4>{{$vendor->vendor_name??null}}</h4>
        <h5 class="mt-4">
            Vendor Invoice To: <br>
            {{substr($vendor->account_managerd->first_name??'Initial',0,1)}}. {{$vendor->account_managerd->last_name??'Last Name'}}
        </h5>
        <div class="mt-4">
            @isset($company->postal_address_line1)
                <p style="margin-bottom: 0"><strong>{{$company->postal_address_line1}}</strong></p>
            @endisset
            @isset($company->postal_address_line2)
                <p style="margin-bottom: 0">{{$company->postal_address_line2}}</p>
            @endisset
            @isset($company->postal_address_line3)
                <p style="margin-bottom: 0">{{$company->postal_address_line3}}</p>
            @endisset
            @isset($company->state_province)
                <p style="margin-bottom: 0">{{$company->state_province}}</p>
            @endisset
            @isset($company->postal_zipcode)
                <p style="margin-bottom: 0">{{$company->postal_zipcode}}</p>
            @endisset
            <p>
                {{$company->phone??$invoice->company->cell}}<br>
                 {{$company->email}}<br>
                {{$company->vat_no}}
            </p>
        </div>
        <div class="mt-4">
            <hr class="hr-center" style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);" />
            <h5>Invoice Details</h5>
            <p><strong>Invoice No: </strong>{{$invoice->id}}</p>
            <p><strong>Invoice Date: </strong><br>{{\Carbon\Carbon::parse($invoice->vendor_invoice_date)->format("d F Y")}}</p>

            <p><strong>Invoice Due Date: </strong><br>{{\Carbon\Carbon::parse($invoice->vendor_due_date)->format("d F Y")}}</p>
            <p><strong>Vendor Reference: </strong><br>{{$invoice->vendor_reference}}</p>
            <p><strong>Invoice Reference: </strong><br>{{$invoice->vendor_invoice_ref}}</p>
            <p><strong>Invoice Note: </strong><br>{{$invoice->vendor_invoice_note}}</p>
        </div>
        <div class="mt-4">
            <hr class="hr-center" style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);" />
            <h5>Payment Information</h5>
            <p><strong>Bank Name: </strong><br>{{$vendor->bank_name??null}}</p>
            <p><strong>Account No: </strong><br>{{$vendor->bank_acc_no??null}}</p>
            <p class="mb-4"><strong>Branch Code: </strong><br>{{$vendor->branch_code??null}}</p>
        </div>

        <div class="mt-4">
            <hr class="hr-center" style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);" />
            <h5>Our Office</h5>
            @isset($vendor->postal_address_line1)
                <p style="margin-bottom: 0"><strong>{{$vendor->postal_address_line1}}</strong></p>
            @endisset
            @isset($vendor->postal_address_line2)
                <p style="margin-bottom: 0">{{$vendor->postal_address_line2}}</p>
            @endisset
            @isset($vendor->postal_address_line3)
                <p style="margin-bottom: 0">{{$vendor->postal_address_line3}}</p>
            @endisset
            @isset($vendor->state_province)
                <p style="margin-bottom: 0">{{$vendor->state_province}}</p>
            @endisset
            @isset($vendor->postal_zipcode)
                <p style="margin-bottom: 0">{{$vendor->postal_zipcode}}</p>
            @endisset
            <p>
                {{$vendor->phone??$invoice->vendor->cell}}<br>
                {{$vendor->email??null}}<br>
                {{$vendor->vat_no??null}}
            </p>
        </div>
    </div>
    <div id="invoice-items">
        <h1 class="title">vendor tax invoice</h1>
        <hr style="width: 10%;border-top: 3px solid rgba(0,0,0,.6);margin-left:0;">
        <table class="table">
            <thead class="bg-dark">
            <tr>
                <th class="text-left">Description</th>
                <th>Qty</th>
                <th>Excl. Price</th>
                <th>Vat (%)</th>
                <th>Excl. Total</th>
                <th>Incl. Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($line_items as $line_item)
                <tr>
                    <td>{{$line_item['line_text']}}</td>
                    <td class="text-right">{{$line_item['hours']}}</td>
                    <td class="text-right">{{$line_item['consultant_rate']}}</td>
                    <td class="text-right">{{$line_item['vat']}}</td>
                    <td class="text-right">{{$line_item['exclude_vat']}}</td>
                    <td class="text-right">{{$line_item['vat_included']}}</td>
                </tr>
            @endforeach
            <tr>
                <th class="text-left text-uppercase">TOTAL HOURS</th>
                <th colspan="5" class="text-left">{{number_format($total_hours, 2,'.',',')}}</th>
            </tr>
            @if(!$expenses->isEmpty())
                <tr class="bg-dark">
                    <th class="text-center" colspan="6"><h3 style="letter-spacing: 1rem; margin-bottom: 0;text-transform: uppercase">Expenses</h3></th>
                </tr>
                @foreach($expenses as $expense)
                    <tr>
                        <td colspan="3">{{$expense['line_text']}}</td>
                        <td colspan="3" class="text-right">{{$expense['amount']}}</td>
                    </tr>
                @endforeach
            @endif
            <tr>
                <td colspan="3" class="text-left">
                    <strong>Sub Total</strong><br>
                    Tax (Vat):
                </td>
                <td colspan="3" class="text-right">
                    <strong>R{{number_format(($total_vat_excluded + $total_expenses), 2,'.',',')}}</strong><br>
                    R{{number_format(($invoice->vendor_invoice_value - ($total_vat_excluded + $total_expenses)), 2, '.', ',')}}
                </td>
            </tr>
            <tr>
                <th class="text-uppercase text-left" colspan="3">grand total</th>
                <th colspan="3" class="text-right">R{{number_format(($invoice->vendor_invoice_value), 2, '.', ',')}}</th>
            </tr>
            </tbody>
        </table>
        <hr>
        <div class="row">
            <div class="col-md-12 text-uppercase mt-5">
                <h3>Thank you</h3>
                <h3>For your business.</h3>
            </div>
        </div>
        <hr>
    </div>
</body>
</html>
