@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        {{--<div class="row text-right mb-3"></div>--}}
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-xl-2 bg-dark text-center">
                <div class="inv-img-wrapper text-center my-2 clearfix">
                    <img class="img-thumbnail text-center" src="{{route('vendor_avatar', ['q'=> $vendor->vendor_logo])}}" alt="{{$vendor->vendor_name}} Logo" style="max-height: 150px;" align="right">
                </div>
                <h4 class="text-center">{{$vendor->vendor_name}}</h4>
                <div class="mt-4">
                    <h5>
                        Vendor Invoice To: <br>
                        {{isset($invoice->vendor->invoice_contact) ? substr(($invoice->vendor->invoice_contact->first_name??''),0,1).'. '.($invoice->vendor->invoice_contact->last_name??'') : ''}}
                    </h5>
                </div>
                <div class="mt-4">
                    @isset($company->postal_address_line1)
                        <p style="margin-bottom: 0"><strong>{{$company->postal_address_line1}}</strong></p>
                    @endisset
                    @isset($company->postal_address_line2)
                        <p style="margin-bottom: 0">{{$company->postal_address_line2}}</p>
                    @endisset
                    @isset($company->postal_address_line3)
                        <p style="margin-bottom: 0">{{$company->postal_address_line3}}</p>
                    @endisset
                    @isset($company->state_province)
                        <p style="margin-bottom: 0">{{$company->state_province}}</p>
                    @endisset
                    @isset($company->postal_zipcode)
                        <p style="margin-bottom: 0">{{$company->postal_zipcode}}</p>
                    @endisset
                    <p>
                        {{--<i class="fa fa-phone mr-2" aria-hidden="true"></i>--}} {{$company->phone??$company->cell}}<br>
                        {{--<i class="fas fa-globe mr-2"></i>--}} {{$company->email}}<br>
                        {{$company->vat_no}}
                    </p>
                </div>

                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Invoice Details</h5>
                    <p><strong>Invoice No: </strong>{{$placeholder_invoice_number}}</p>
                    <p><strong>Invoice Date: </strong><br>{{\Carbon\Carbon::parse(session('invoice_date'))->format("d F Y")}}</p>

                    <p><strong>Invoice Due Date: </strong><br>{{\Carbon\Carbon::parse(session('invoice_due_date'))->format("d F Y")}}</p>
                    <p><strong>Vendor Reference: </strong><br>{{session('vendor_reference')}}</p>
                    <p><strong>Invoice Reference: </strong><br>{{session('invoice_reference')}}</p>
                    <p><strong>Invoice Note: </strong><br>{{session('invoice_note')}}</p>
                </div>
                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Payment Information</h5>
                    <p><strong>Bank Name: </strong><br>{{$vendor->bank_name}}</p>
                    <p><strong>Account No: </strong><br>{{$vendor->bank_acc_no}}</p>
                    <p class="mb-4"><strong>Branch Code: </strong><br>{{$vendor->branch_code}}</p>
                </div>

                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Office</h5>
                    @isset($vendor->postal_address_line1)
                        <p style="margin-bottom: 0"><strong>{{$vendor->postal_address_line1}}</strong></p>
                    @endisset
                    @isset($vendor->postal_address_line2)
                        <p style="margin-bottom: 0">{{$vendor->postal_address_line2}}</p>
                    @endisset
                    @isset($vendor->postal_address_line3)
                        <p style="margin-bottom: 0">{{$vendor->postal_address_line3}}</p>
                    @endisset
                    @isset($vendor->state_province)
                        <p style="margin-bottom: 0">{{$vendor->state_province}}</p>
                    @endisset
                    @isset($vendor->postal_zipcode)
                        <p style="margin-bottom: 0">{{$vendor->postal_zipcode}}</p>
                    @endisset
                    <p>
                        {{--<i class="fa fa-phone mr-2" aria-hidden="true"></i>--}} {{$vendor->phone??$vendor->cell}}<br>
                        {{--<i class="fas fa-globe mr-2"></i>--}} {{$vendor->email}}<br>
                        {{$vendor->vat_no}}
                    </p>
                </div>
            </div>
            <div class="col-md-9 col-xl-10">
                <div class="my-5">&nbsp;</div>
                <div class="mt-5 pl-3">
                    <h1 class="text-uppercase" style="letter-spacing: 1rem">Vendor Tax Invoice</h1>
                    <hr style="width: 5%;border-top: 3px solid rgba(0,0,0,.6);margin-left:0;">
                    <div class="table-responsive mt-5">
                        <table class="table">
                            <thead class="bg-dark">
                                <tr>
                                    <th>Description</th>
                                    <th>Qty</th>
                                    <th>Excl. Price</th>
                                    <th>Vat (%)</th>
                                    <th>Exclusive Total</th>
                                    <th>Inclusive Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoice_line_items as $line_item)
                                    <tr>
                                        <td>{{$line_item['description']}}</td>
                                        <td class="text-right">{{number_format($line_item['hours'],2,'.',',')}}</td>
                                        <td class="text-right">{{($line_item["currency"]??$currency).' '.number_format($line_item['consultant_rate'],2,'.',',')}}</td>
                                        <td class="text-right">{{number_format($line_item['vat']*100,2,'.',',')}}%</td>
                                        <td class="text-right">{{($line_item["currency"]??$currency).' '.number_format($line_item['line_item_total_no_vat'],2,'.',',')}}</td>
                                        <td class="text-right">{{($line_item["currency"]??$currency).' '.number_format($line_item['line_item_total_vat'],2,'.',',')}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th class="text-right text-uppercase">TOTAL HOURS</th>
                                    <th class="text-right">{{number_format($invoice_line_items->sum('hours'), 2,'.',',')}}</th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">{{($line_item["currency"]??$currency).' '.number_format($invoice_line_items->sum('line_item_total_no_vat'), 2,'.',',')}}</th>
                                    <th class="text-right">{{($line_item["currency"]??$currency).' '.number_format($invoice_line_items->sum('line_item_total_vat'), 2,'.',',')}}</th>
                                </tr>
                                @if(!$invoice_expense_line_items->isEmpty())
                                    <tr class="bg-dark">
                                        <th class="text-center" colspan="6">Expenses</th>
                                    </tr>
                                    @foreach($invoice_expense_line_items as $expense)
                                        <tr>
                                            <td colspan="5">{{$expense['description']}}</td>
                                            <td class="text-right">{{($line_item["currency"]??$currency).' '.number_format($expense['amount'], 2,'.', ',')}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td colspan="5">
                                        <strong>Sub Total</strong><br>
                                        Tax (Vat):
                                    </td>
                                    <td class="text-right">
                                        <strong>{{($line_item["currency"]??$currency).' '.number_format(($invoice_line_items->sum('line_item_total_no_vat') + $invoice_expense_line_items->sum('amount')), 2, '.', ',')}}</strong><br>
                                        {{($line_item["currency"]??$currency).' '.number_format(($invoice_line_items->sum('line_item_total_vat') - $invoice_line_items->sum('line_item_total_no_vat')), 2, '.', ',')}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-uppercase" colspan="5">grand total</th>
                                    <th class="text-right">{{($line_item["currency"]??$currency).' '.number_format(($invoice_line_items->sum('line_item_total_vat') + $invoice_expense_line_items->sum('amount')), 2, '.', ',')}}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-uppercase mt-5">
                            <h3>Thank you</h3><br>
                            <h3>For your business.</h3>
                        </div>
                    </div>
                    <hr>
                    <div class="row mt-5">
                        <div class="col-md-6 text-left"><a href="{{route('save.invoice', ["currency_type" => $currency_type])}}" class="btn btn-dark btn-sm"><i class="far fa-save"></i> Save Invoice</a></div>
                        <div class="col-md-6 text-right"><a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a></div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                                    <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="email-form">
                                        <div class="form-group row">
                                            <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-check">
                                            <input type="checkbox" name="customer_email" value="{{$vendor->email}}" checked class="form-check-input" id="customer_email">
                                            <label class="form-check-label" for="customer_email">{{$vendor->email}}</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" name="company_email" value="{{$company->email}}" checked class="form-check-input" id="company_email">
                                            <label class="form-check-label" for="company_email">{{$company->email}}</label>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group text-center">
                                            <button class="btn btn-dark btn-sm" id="btn-send"><i class="fas fa-paper-plane"></i> Send Invoice</button>
                                        </div>
                                    </div>
                                    <div class="text-center" id="sending-email" style="display: none">
                                        <img class="img-fluid" src="{{asset('assets/email.gif')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('css/jquery.multi-emails.css')}}">
    <style>
        img {
            float: none;
        }
        p{
            line-height: 1.4rem!important;
            margin-bottom: 0.7rem;
        }
    </style>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="{{global_asset('js/jquery.multi-emails.js')}}"></script>
    <script>
        $(function(){
        $("#user_email").multiEmails();


        $("#btn-send").on('click', function () {

            $("#email-form").hide();
            $("#sending-email").show();

            axios.get('/vendor/invoice/save', {
                params: {
                    send: 1,
                    subject: $("#subject").val(),
                    vendorEmail: ($("#customer_email").is(':checked'))?$("#customer_email").val():null,
                    companyEmail: ($("#company_email").is(':checked'))?$("#company_email").val():null,
                    multiEmails: $("#user_email").val(),
                    currency_type: "{{$currency_type}}"
                }
            })
                .then(function (response) {
                    $(".modal-body").html(
                        '<div class="alert alert-success">Your Invoice has been sent successfully</div>'
                    )

                    setTimeout(() => window.location = '/vendor/invoice/view', 1000);
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        });
    })
    </script>
@endsection
