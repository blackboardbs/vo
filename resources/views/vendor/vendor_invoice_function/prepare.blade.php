@extends('adminlte.default')

@section('title') Prepare Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('gen')" class="btn btn-primary ml-2">Generate Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('generate.invoice'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off','id'=>'gen'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Generating Invoice</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Financial AP Invoice No: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Account payable invoice from company's financial system"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('invoice_ref', old('invoice_ref'),['class'=>'form-control form-control-sm'. ($errors->has('invoice_ref') ? ' is-invalid' : ''),'placeholder'=>'Invoice Reference'])}}
                            @foreach($errors->get('invoice_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Invoice Date:</th>
                        <td>{{Form::text('invoice_date',$invoice_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('invoice_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                            @foreach($errors->get('invoice_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Vendor Reference: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Vendor's reference E.g. Vendor's invoice number"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('vendor_ref', old('vendor_ref'),['class'=>'form-control form-control-sm'. ($errors->has('vendor_ref') ? ' is-invalid' : ''),'placeholder'=>'Vendor Reference'])}}
                            @foreach($errors->get('vendor_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Payment Due Date:</th>
                        <td>{{Form::text('due_date',$payment_due_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                            @foreach($errors->get('due_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>

                    </tr>
                    <tr>
                        <th>Invoice Notes: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Note to be displayed on invoice footer"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('invoice_notes',old('Invoice_notes'),['class'=>'form-control form-control-sm'. ($errors->has('Invoice_notes') ? ' is-invalid' : ''),'placeholder'=>'Invoice Notes'])}}
                            @foreach($errors->get('Invoice_notes') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Currency:</th>
                        <td>{{Form::select('currency_type', [0 => "Local", 1 => "Foreign"], 0, ["class" => "form-control form-control-sm"])}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td></td>
                        <th>Vat Rate:</th>
                        <td>
                            {{Form::select("vat_rate_id", $vat_rate_dropdown, $vat_rate_id, ["class" => "form-control form-control-sm"])}}
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
            @if(count($expenses))
                <blackboard-invoice-expenses :expenses="{{json_encode($expenses)}}"></blackboard-invoice-expenses>
            @endif
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })
    </script>
@endsection