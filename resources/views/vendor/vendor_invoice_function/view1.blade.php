@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('edit.invoice',$vendor_invoice->id)}}" class="btn btn-success btn-sm float-right" style="margin-left:5px;"><i class="far fa-edit"></i> Edit Vendor Invoice</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-4 pl-2">
                <h4>Vendor Tax Invoice</h4>
                <h5>{{$vendor_invoice->vendor->vendor_name}}</h5>
                <p class="mb-0">
                    {{$vendor_invoice->vendor->postal_address_line1}}
                    @empty($vendor_invoice->vendor->postal_address_line1)
                        {{$vendor_invoice->vendor->street_address_line1}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendor_invoice->vendor->postal_address_line2}}
                    @empty($vendor_invoice->vendor->postal_address_line2)
                        {{$vendor_invoice->vendor->street_address_line2}}
                    @endempty

                </p>
                <p class="mb-0">
                    {{$vendor_invoice->vendor->postal_address_line3}}
                    @empty($vendor_invoice->vendor->postal_address_line3)
                        {{$vendor_invoice->vendor->street_address_line3}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendor_invoice->vendor->city_suburb}}
                    @empty($vendor_invoice->vendor->city_suburb)
                        {{$vendor_invoice->vendor->street_city_suburb}}
                    @endempty
                </p>
                <p class="mb-0">{{$vendor_invoice->vendor->state_province}}</p>
                <p class="mb-0">
                    {{$vendor_invoice->vendor->postal_zipcode}}
                    @empty($vendor_invoice->vendor->postal_zipcode)
                        {{ $vendor_invoice->vendor->street_zipcode }}
                    @endempty
                </p>
                <p class="mb-0">{{ $vendor_invoice->vendor->phone }}</p>
                <p class="mb-0">{{ $vendor_invoice->vendor->email }}</p>
                <p class="mb-0">Contact: {{ $vendor_invoice->vendor->contact_firstname." ".$vendor_invoice->vendor->contact_lastname }}</p>
                <p class="mb-0">Vat No: {{ $vendor_invoice->vendor->vat_no }}</p>
            </div>
            <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{ isset($company->company_name) ? $company->company_name : '' }}</p>
                <p class="mb-0">{{ isset($company->postal_address_line1) ? $company->postal_address_line1: '' }}</p>
                <p class="mb-0">{{ isset($company->postal_address_line2) ? $company->postal_address_line2 : '' }}</p>
                @if($company->postal_address3 != null)
                    <p class="mb-0">{{ isset($company->postal_address_line3) ? $company->postal_address_line3 : ''}}</p>
                @endif
                <p class="mb-0">{{ isset($company->state_province) ? $company->state_province : '' }}</p>
                <p class="mb-0">{{ $company->postal_zipcode }}</p>
                <p class="mb-0">{{ $company->phone }}</p>
                <p class="mb-0">{{ $company->email }}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no }}</p>
            </div>
            <div class="col-md-4">
                {{--<div class="text-right mb-3"><img src="{{route('vendor_avatar',['q'=>$vendor_invoice->vendor->vendor_logo])}}" alt="{{$vendor_invoice->vendor->vendor_name}} Logo"  style="max-height: 150px;"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('vendor_avatar', ['q'=> (isset($vendor_invoice->vendor->vendor_logo)?$vendor_invoice->vendor->vendor_logo:'default.png')])}}" alt="{{$vendor_invoice->vendor->vendor_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{ $vendor_invoice->id }}</div>
                    <div class="col-md-6">Invoice Date:</div>
                    <div class="col-md-6 text-right">{{$vendor_invoice->vendor_invoice_date}}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $vendor_invoice->vendor_due_date }}</div>
                    <div class="col-md-4">Vendor Reference:</div>
                    <div class="col-md-8 text-right">{{$vendor_invoice->vendor_reference}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ isset($vendor_invoice->vendor)? $vendor_invoice->vendor->account_managerd->first_name.' '.$vendor_invoice->vendor->account_managerd->last_name : '' }}</div>

                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Description</th>
                            <th>Qty</th>
                            <th>Excl. Price</th>
                            <th class="text-center">VAT%</th>
                            <th class="text-right">Exclusive Total</th>
                            <th class="text-right">Inclusive Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($vendor_invoice_lines as $key => $vendor_invoice_line)
                            <tr>
                                <td>{!! wordwrap($vendor_invoice_line->line_text, 75, "<br />\n") !!}</td>
                                <td>{{ $vendor_invoice_line->quantity }}</td>
                                <td>{{ number_format($vendor_invoice_line->price,2,'.',',') }}</td>
                                <td>{{number_format($vat_rate[$key],2,'.',',')}}%</td>
                                <td class="text-right">R{{ number_format($vendor_invoice_line->quantity * $vendor_invoice_line->price ,2,'.',',') }}</td>
                                <td class="text-right">R{{ number_format($vendor_invoice_line->quantity * $vendor_invoice_line->price * (($vat_rate[$key] > 0)?$vat_rate[$key]/100:0) + ($vendor_invoice_line->quantity * $vendor_invoice_line->price) ,2,'.',',') }}</td>
                            </tr>
                        @empty
                        @endforelse
                        <tr>
                            <th class="text-right">TOTAL HOURS</th>
                            <th colspan="5">{{number_format($total_hours,2,'.',',')}}</th>
                        </tr>
                        @php $tote = 0; @endphp
                        @if(count($vendor_invoice_line_exp) > 0)
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="bg-dark">
                                <th colspan="6">Expenses</th>
                            </tr>
                        @endif
                        @forelse($vendor_invoice_line_exp as $index => $expense)
                            @isset($expense)
                                <tr>
                                    <td>{!! wordwrap($expense->line_text, 75, "<br />\n") !!}</td>
                                    <td>1</td>
                                    <td>R {{ number_format($expense->amount,2,".",",") }}</td>
                                    <td class="text-center">{{number_format(0,2,'.',',')}}%</td>
                                    <td class="text-right">R {{ number_format($expense->amount,2,".",",") }}</td>
                                    <td class="text-right">R {{ number_format($expense->amount,2,".",",") }}</td>
                                </tr>
                                @php
                                    $tote += $expense->amount;
                                @endphp
                            @endisset
                        @empty
                        @endforelse
                        <tr>
                            <th colspan="6">&nbsp;</th>
                        </tr>
                        <tr class="btn-dark">
                            <th colspan="6">Summary</th>
                        </tr>
                        <tr>
                            <th>Payment Information</th>
                            <td colspan="3">{{ $vendor_invoice->vendor_invoice_notes }}</td>
                            <th class="text-right">Total Exclusive:</th>
                            <td class="text-right">R {{ number_format(($tote + $total_value_line) ,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6">{{ isset($vendor_invoice->vendor) ? $vendor_invoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">{{  isset($vendor_invoice->vendor) ? $vendor_invoice->vendor->bank_name : '' }}</td>
                            <th class="text-right">Total VAT:</th>
                            <td class="text-right">R {{ number_format($total_vat,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6">Branch Code: {{ isset($vendor_invoice->vendor) ? $vendor_invoice->vendor->branch_code : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Account Number: {{ isset($vendor_invoice->vendor) ? $vendor_invoice->vendor->bank_acc_no : '' }}</td>
                            <th class="text-right">Total:</th>
                            <td class="text-right">R {{ number_format($total_vat + $total_value_line + $tote,2,'.',',') }}</td>
                            {{ Form::hidden('invoice_value', ($total_vat + $total_value_line + $tote)) }}
                        </tr>
                        <tr>
                            <td colspan="6">Reference: {{ isset($vendor_invoice->vendor) ? $vendor_invoice->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <th colspan="6" class="text-center">Thank you for your business!</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-print-12">
            @if($vendor_invoice->vendor_invoice_status == 1)
                <div class="row">
                    @if(auth()->user()->isAn('admin') || auth()->user()->isAn('admin_manager') || auth()->user()->isAn('manager'))
                        <div class="col-md-3">
                            {!! Form::open(['url' => route('unallocate.invoice', $vendor_invoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                            <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="{{route('process.payment', $vendor_invoice->id)}}" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-university"></i> Process Payment</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                        </div>
                        {{--<div class="col-md-3 text-center"></div>--}}
                        <div class="col-md-3 text-right">
                            <button onclick="window.print()" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</button>
                        </div>
                    @else
                        <div class="col-md-4">
                            {!! Form::open(['url' => route('unallocate.invoice', $vendor_invoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                            <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                        </div>
                        {{--<div class="col-md-3 text-center"></div>--}}
                        <div class="col-md-4 text-right">
                            <button onclick="window.print()" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</button>
                        </div>
                    @endif
                </div>
            @else
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button onclick="window.print()" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</button>
                    </div>
                </div>
            @endif

        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                        <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="email-form">
                            {!! Form::open(['url' => route('view.invoice', $vendor_invoice), 'method' => 'post']) !!}
                            <div class="form-group row">
                                <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                </div>
                            </div>
                            <hr>
                            @if(isset($vendor_invoice->vendor->email))
                            <div class="form-check">
                                <input type="checkbox" name="customer_email" value="{{isset($vendor_invoice->vendor)?strtolower($vendor_invoice->vendor->email):''}}" checked class="form-check-input" id="customer_email">
                                <label class="form-check-label" for="customer_email">{{isset($vendor_invoice->vendor)?strtolower($vendor_invoice->vendor->email):''}}</label>
                            </div>
                            @endif
                            @if(trim($company->email) != '')
                            <div class="form-check">
                                <input type="checkbox" name="company_email" value="{{$company->email}}" checked class="form-check-input" id="company_email">
                                <label class="form-check-label" for="company_email">{{$company->email}}</label>
                            </div>
                            @endif
                            <hr>
                            <div class="form-group row">
                                <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group text-center">
                                {!! Form::submit('Send Invoice', ['class' => 'btn btn-sm btn-dark', 'id'=>'send-btn']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="loader-wrapper">
                            <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra-js')
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        function goBack() {
            window.history.back();
        }
        $(function () {
            $('#user_email').multiple_emails({
                position: 'top', // Display the added emails above the input
                theme: 'bootstrap', // Bootstrap is the default theme
                checkDupEmail: true // Should check for duplicate emails added
            });

            $('#current_emails').text($('#user_email').val());

            $('#user_email').change( function(){
                $('#current_emails').text($(this).val());
            });
        })

        $(function () {
            $("#send-btn").on('click', function () {
                $("#email-form").hide();
                $("#loader-wrapper").fadeIn();
                $(".default").hide();
                $(".sending").fadeIn();
            });
        })
    </script>

@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
        @media print {
            .col-print-4 {width:33%; float:left;}
            .col-print-12{width:100%; float:left;}
        }
    </style>
@endsection
