@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        {{--<div class="row text-right mb-3"></div>--}}
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-xl-2 bg-dark text-center">
                <div class="inv-img-wrapper text-center my-2 clearfix">
                    <img class="img-thumbnail text-center" src="{{route('vendor_avatar', ['q'=> $invoice->vendor->vendor_logo??'default.png'])}}" alt="{{$invoice->vendor?->vendor_name??null}} Logo" style="max-height: 150px;" align="right">
                </div>
                <h4 class="text-center">{{$invoice->vendor?->vendor_name??null}}</h4>
                <div class="mt-4">
                    <h5>
                        Vendor Invoice To: <br>
                        {{isset($invoice->vendor?->invoice_contact) ? substr(($invoice->vendor?->invoice_contact?->first_name??''),0,1).'. '.($invoice->vendor?->invoice_contact?->last_name??'') : ''}}
                    </h5>
                </div>
                <div class="mt-4">
                    @isset($invoice->company?->postal_address_line1)
                        <p style="margin-bottom: 0"><strong>{{$invoice->company?->postal_address_line1}}</strong></p>
                    @endisset
                    @isset($invoice->company->postal_address_line2)
                        <p style="margin-bottom: 0">{{$invoice->company?->postal_address_line2}}</p>
                    @endisset
                    @isset($invoice->company->postal_address_line3)
                        <p style="margin-bottom: 0">{{$invoice->company?->postal_address_line3}}</p>
                    @endisset
                    @isset($invoice->company->state_province)
                        <p style="margin-bottom: 0">{{$invoice->company?->state_province}}</p>
                    @endisset
                    @isset($invoice->company->postal_zipcode)
                        <p style="margin-bottom: 0">{{$invoice->company?->postal_zipcode}}</p>
                    @endisset
                    <p>
                        {{--<i class="fa fa-phone mr-2" aria-hidden="true"></i>--}} {{$invoice->company?->phone??$invoice->company?->cell}}<br>
                        {{--<i class="fas fa-globe mr-2"></i>--}} {{$invoice->company?->email}}<br>
                        {{$invoice->company?->vat_no}}
                    </p>
                </div>

                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Invoice Details</h5>
                    <p><strong>Invoice No: </strong>{{$invoice->vendor_invoice_number??$invoice->id}}</p>
                    <p><strong>Invoice Date: </strong><br>{{\Carbon\Carbon::parse($invoice->vendor_invoice_date)->format("d F Y")}}</p>

                    <p><strong>Invoice Due Date: </strong><br>{{\Carbon\Carbon::parse($invoice->vendor_due_date)->format("d F Y")}}</p>
                    <p><strong>Vendor Reference: </strong><br>{{$invoice->vendor_reference}}</p>
                    <p><strong>Invoice Reference: </strong><br>{{$invoice->vendor_invoice_ref}}</p>
                    <p><strong>Invoice Note: </strong><br>{{$invoice->vendor_invoice_note}}</p>
                </div>
                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Payment Information</h5>
                    <p><strong>Bank Name: </strong><br>{{$invoice->vendor->bank_name??null}}</p>
                    <p><strong>Account No: </strong><br>{{$invoice->vendor->bank_acc_no??null}}</p>
                    <p class="mb-4"><strong>Branch Code: </strong><br>{{$invoice->vendor->branch_code??null}}</p>
                </div>

                <div class="mt-4">
                    <hr style="width: 30%;border-top: 3px solid rgba(255,255,255,.4);">
                    <h5>Office</h5>
                    @isset($invoice->vendor->postal_address_line1)
                        <p style="margin-bottom: 0"><strong>{{$invoice->vendor->postal_address_line1}}</strong></p>
                    @endisset
                    @isset($invoice->vendor->postal_address_line2)
                        <p style="margin-bottom: 0">{{$invoice->vendor->postal_address_line2}}</p>
                    @endisset
                    @isset($invoice->vendor->postal_address_line3)
                        <p style="margin-bottom: 0">{{$invoice->vendor->postal_address_line3}}</p>
                    @endisset
                    @isset($invoice->vendor->state_province)
                        <p style="margin-bottom: 0">{{$invoice->vendor->state_province}}</p>
                    @endisset
                    @isset($invoice->vendor->postal_zipcode)
                        <p style="margin-bottom: 0">{{$invoice->vendor->postal_zipcode}}</p>
                    @endisset
                    <p>
                        {{--<i class="fa fa-phone mr-2" aria-hidden="true"></i>--}} {{$invoice->vendor->phone??$invoice->vendor->cell}}<br>
                        {{--<i class="fas fa-globe mr-2"></i>--}} {{$invoice->vendor->email??null}}<br>
                        {{$invoice->vendor->vat_no??null}}
                    </p>
                </div>
            </div>
            <div class="col-md-9 col-xl-10">
                <div class="my-5">&nbsp;</div>
                <div class="mt-5 pl-3">
                    <h1 class="text-uppercase" style="letter-spacing: 1rem">Vendor Tax Invoice</h1>
                    <hr style="width: 5%;border-top: 3px solid rgba(0,0,0,.6);margin-left:0;">
                    <div class="table-responsive mt-5">
                        <table class="table">
                            <thead class="bg-dark">
                            <tr>
                                <th>Description</th>
                                <th>Qty</th>
                                <th style="white-space: nowrap">Excl. Price</th>
                                <th style="white-space: nowrap">Vat (%)</th>
                                <th style="white-space: nowrap">Exclusive Total</th>
                                <th style="white-space: nowrap">Inclusive Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoice->vendorInvoiceLine as $line_item)
                                <tr>
                                    <td>{{$line_item['line_text']}}</td>
                                    <td class="text-right">{{number_format($line_item['quantity'],2,'.',',')}}</td>
                                    <td class="text-right">{{($invoice->currency??"ZAR").' '.number_format($line_item['price'],2,'.',',')}}</td>
                                    @if($line_item['price'] > 0 && $line_item['quantity'] > 0)
                                        <td class="text-right">{{number_format((($line_item['invoice_line_value'] - ($line_item['quantity'] *$line_item['price']))/($line_item['quantity'] *$line_item['price']))*100)}}%</td>
                                    @else
                                        <td class="text-right">{{$invoice->vatRate ? $invoice->vatRate?->vat_rate : '0'}}%</td>
                                    @endif
                                    <td class="text-right">{{($invoice->currency??"ZAR").' '.number_format(($line_item['quantity'] * $line_item['price']),2,'.',',')}}</td>
                                    <td class="text-right">{{($invoice->currency??"ZAR").' '.number_format($line_item['invoice_line_value'],2,'.',',')}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th class="text-right text-uppercase">TOTAL HOURS</th>
                                <th class="text-right">{{number_format($invoice->vendorInvoiceLine->sum('quantity'), 2,'.',',')}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            @if(!$invoice->vendorInvoiceLineExpense->isEmpty())
                                <tr class="bg-dark">
                                    <th class="text-center" colspan="6"><h3 style="letter-spacing: 1rem; margin-bottom: 0;text-transform: uppercase">Expenses</h3></th>
                                </tr>
                                @foreach($invoice->vendorInvoiceLineExpense as $expense)
                                    <tr>
                                        <td colspan="5">{{$expense['line_text']}}</td>
                                        <td class="text-right">{{($invoice->currency??"ZAR").' '.number_format($expense['amount'], 2,'.', ',')}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                <td colspan="5">
                                    <strong>Sub Total</strong><br>
                                    Tax (Vat):
                                </td>
                                <td class="text-right">
                                    <strong>{{($invoice->currency??"ZAR").' '.number_format(($total_without_tax + $invoice->vendorInvoiceLineExpense->sum('amount')), 2, '.', ',')}}</strong><br>
                                    {{($invoice->currency??"ZAR").' '.number_format(($invoice->vendor_invoice_value - ($total_without_tax + $invoice->vendorInvoiceLineExpense->sum('amount'))), 2, '.', ',')}}
                                </td>
                            </tr>
                            <tr>
                                <th class="text-uppercase" colspan="5">grand total</th>
                                <th class="text-right">{{($invoice->currency??"ZAR").' '.number_format($invoice->vendor_invoice_value, 2, '.', ',')}}</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-uppercase mt-5">
                            <h3>Thank you</h3><br>
                            <h3>For your business.</h3>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12 col-print-12">
                        @if($invoice->vendor_invoice_status == 1)
                            <div class="row">
                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('admin_manager') || auth()->user()->hasRole('manager'))
                                    <div class="col-md-3">
                                        {!! Form::open(['url' => route('unallocate.invoice', $invoice->id), 'method' => 'post', 'class' => 'mb-3 cancelInvoice']) !!}
                                        <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <a href="{{route('process.payment', $invoice->id)}}" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-university"></i> Process Payment</a>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                    </div>
                                    {{--<div class="col-md-3 text-center"></div>--}}
                                    <div class="col-md-3 text-right">
                                        <a href="{{route('invoice.document', ['vendor_invoice' => $invoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                    </div>
                                @else
                                    <div class="col-md-4">
                                        {!! Form::open(['url' => route('unallocate.invoice', $invoice->id), 'method' => 'post', 'class' => 'mb-3']) !!}
                                        <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                    </div>
                                    {{--<div class="col-md-3 text-center"></div>--}}
                                    <div class="col-md-4 text-right">
                                        <a href="{{route('invoice.document', ['vendor_invoice' => $invoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                    </div>
                                @endif
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{route('invoice.document', ['vendor_invoice' => $invoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                                </div>
                            </div>
                        @endif

                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                                    <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="email-form">
                                        <div class="form-group row">
                                            <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-check">
                                            <input type="checkbox" name="customer_email" value="{{$invoice->vendor?->email}}" checked class="form-check-input" id="customer_email">
                                            <label class="form-check-label" for="customer_email">{{$invoice->vendor?->email}}</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" name="company_email" value="{{$invoice->company?->email}}" checked class="form-check-input" id="company_email">
                                            <label class="form-check-label" for="company_email">{{$invoice->company?->email}}</label>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group text-center">
                                            <button class="btn btn-dark btn-sm" id="btn-send"><i class="fas fa-paper-plane"></i> Send Invoice</button>
                                        </div>
                                    </div>
                                    <div class="text-center" id="sending-email" style="display: none">
                                        <img class="img-fluid" src="{{global_asset('assets/email.gif')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{global_asset('css/jquery.multi-emails.css')}}">
    <style>
        img {
            float: none;
        }
        p{
            line-height: 1.4rem!important;
            margin-bottom: 0.7rem;
        }
    </style>
@endsection
@section('extra-js')
    <script src="{{global_asset('js/jquery.multi-emails.js')}}"></script>
    <script>

        $(function(){
        $("#user_email").multiEmails();


        $("#btn-send").on('click', function () {
            $("#email-form").hide();
            $("#sending-email").show();

            axios.get('{{route('invoice.document', $invoice)}}', {
                params: {
                    show: 1,
                    subject: $("#subject").val(),
                    vendorEmail: ($("#customer_email").is(':checked'))?$("#customer_email").val():null,
                    companyEmail: ($("#company_email").is(':checked'))?$("#company_email").val():null,
                    multiEmails: $("#user_email").val(),
                }
            })
                .then(function (response) {
                    $(".modal-body").html(
                        '<div class="alert alert-success">Your Invoice has been sent successfully</div>'
                    )

                    setTimeout(() => window.location = '/vendor/invoice/view', 1000);
                    console.log(response.data);
                })
                .catch(function (error) {
                    $(".modal-body").html(
                        '<div class="alert alert-danger">Something went wrong and we couldn\'t send your Invoice</div>'
                    )
                    setTimeout(() => window.location.reload(), 1500)
                    console.log(error.response);
                });
        });
    });
    </script>
@endsection
