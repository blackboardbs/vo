<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,
        html {
            font-family: arial, sans-serif;
            font-size: 0.9rem
        }

        .container {
            width: 100%;
            margin: 0 auto
        }

        .cols-3 {
            width: 32.8%;
            display: inline-block
        }

        .pull-right {
            float: right
        }

        .text-right {
            text-align: right
        }

        .text-left {
            text-align: left
        }

        .clearfix {
            clear: both
        }

        .pb-3, .py-3 {
            padding-bottom: 1rem!important;
        }
        .btn-dark {
            color: #fff;
            background-color: #343a40;
            border-color: #343a40;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
        }

        .table {
            border-collapse: collapse;
            width: 100%
        }

        td,
        th {
            border-bottom: 1px solid #ddd;
            padding-top: 14px;
            padding-bottom: 14px
        }

        tr:hover {
            background-color: #ddd
        }

        .bg-dark,
        .bg-dark:hover {
            color: #fff;
            background-color: #343a40!important;
            border-color: #343a40;
            box-shadow: 0 1px 1px rgba(0, 0, 0, .075)
        }

        hr {
            border-top: 1px solid #ddd;
            margin-top: 10px
        }

        p {
            margin: 5px 0
        }
        .inv-img-wrapper img, .img-wrapper img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin: 0 auto;
        }
        .right-side{
            display: inline-block;
            width: 49%;
            margin-bottom: 5px;
        }
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-4 cols-3 pl-2">
                <h4>Vendor Tax Invoice</h4>
                <h5>{{$vendor->vendor_name??null}}</h5>
                @isset($vendor->postal_address_line1)
                    <p style="margin-bottom: 0"><strong>{{$vendor->postal_address_line1}}</strong></p>
                @endisset
                @isset($vendor->postal_address_line2)
                    <p style="margin-bottom: 0">{{$vendor->postal_address_line2}}</p>
                @endisset
                @isset($vendor->postal_address_line3)
                    <p style="margin-bottom: 0">{{$vendor->postal_address_line3}}</p>
                @endisset
                @isset($vendor->state_province)
                    <p style="margin-bottom: 0">{{$vendor->state_province}}</p>
                @endisset
                @isset($vendor->postal_zipcode)
                    <p style="margin-bottom: 0">{{$vendor->postal_zipcode}}</p>
                @endisset
                <p>
                    {{$vendor->phone??$invoice->vendor->cell}}<br>
                    {{$vendor->email??null}}<br>
                    {{$vendor->vat_no??null}}
                </p>
            </div>
            <div class="col-md-4 cols-3">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                @isset($company->postal_address_line1)
                    <p style="margin-bottom: 0"><strong>{{$company->postal_address_line1}}</strong></p>
                @endisset
                @isset($company->postal_address_line2)
                    <p style="margin-bottom: 0">{{$company->postal_address_line2}}</p>
                @endisset
                @isset($company->postal_address_line3)
                    <p style="margin-bottom: 0">{{$company->postal_address_line3}}</p>
                @endisset
                @isset($company->state_province)
                    <p style="margin-bottom: 0">{{$company->state_province}}</p>
                @endisset
                @isset($company->postal_zipcode)
                    <p style="margin-bottom: 0">{{$company->postal_zipcode}}</p>
                @endisset
                <p>
                    {{$company->phone??$invoice->company->cell}}<br>
                    {{$company->email}}<br>
                    {{$company->vat_no}}
                </p>
            </div>
            <div class="col-md-4 cols-3">
                {{--<div class="text-right mb-3"><img src="{{route('vendor_avatar',['q'=>$vendor_invoice->vendor->vendor_logo])}}" alt="{{$vendor_invoice->vendor->vendor_name}} Logo"  style="max-height: 150px;"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{$logo}}" alt="{{$vendor->vendor_name??null}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter clearfix">
                    <div>
                        <div class="right-side">Invoice Number:</div>
                        <div class="right-side text-right float-right">{{$invoice->vendor_invoice_number??$invoice->id}}</div>
                    </div>
                    <div>
                        <div class="right-side">Invoice Date:</div>
                        <div class="right-side text-right">{{$invoice->vendor_invoice_date}}</div>
                    </div>

                    <div>
                        <div class="right-side">Due Date:</div>
                        <div class="right-side text-right">{{ $invoice->vendor_due_date }}</div>
                    </div>
                    <div>
                        <div class="right-side">Vendor Reference:</div>
                        <div class="right-side text-right">{{$invoice->vendor_reference}}</div>
                    </div>
                    <div>
                        <div class="right-side">Sales Rep:</div>
                        <div class="right-side text-right">{{ $vendor->account_managerd->name()??'' }}</div>
                    </div>

                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th class="text-left">Description</th>
                            <th class="text-right" style="width: 80px">Qty</th>
                            <th class="text-right" style="width: 80px">Excl.<br>Price</th>
                            <th class="text-right"  style="width: 80px">VAT%</th>
                            <th class="text-right"  style="width: 120px">Exclusive<br>Total</th>
                            <th class="text-right"  style="width: 120px">Inclusive<br>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($line_items as $line_item)
                            <tr>
                                <td>{!! $line_item['line_text'] !!}</td>
                                <td class="text-right">{{ $line_item['hours'] }}</td>
                                <td class="text-right">{{ $line_item['consultant_rate'] }}</td>
                                <td class="text-right">{{$line_item['vat']}}</td>
                                <td class="text-right">{{ $line_item['exclude_vat'] }}</td>
                                <td class="text-right">{{ $line_item['vat_included'] }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th class="text-right">TOTAL HOURS &nbsp; &nbsp;</th>
                            <th class="text-right">{{number_format($total_hours, 2,'.',',')}}</th>
                            <th colspan="4"></th>
                        </tr>
                        @if(!$expenses->isEmpty())
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="bg-dark">
                                <th colspan="6" class="text-left">Expenses</th>
                            </tr>
                        @endif
                        @foreach($expenses as $expense)
                            <tr>
                                <td class="text-left">{!! $expense['line_text'] !!}</td>
                                <td class="text-center">1</td>
                                <td class="text-right">{{ ($invoice->currency)." ".$expense['amount'] }}</td>
                                <td class="text-right">{{number_format(0,2,'.',',')}}%</td>
                                <td class="text-right">{{ ($invoice->currency)." ".$expense['amount'] }}</td>
                                <td class="text-right">{{ ($invoice->currency)." ".$expense['amount'] }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th colspan="6">&nbsp;</th>
                        </tr>
                        <tr class="btn-dark">
                            <th colspan="6" class="text-left">Summary</th>
                        </tr>
                        <tr>
                            <th class="text-left">Payment Information</th>
                            <td colspan="3">{{ $invoice->vendor_invoice_note }}</td>
                            <th class="text-right">Total Exclusive:</th>
                            <td class="text-right">{{($invoice->currency??$currency)." ".number_format(($total_vat_excluded + $total_expenses), 2,'.',',')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6">{{$vendor->vendor_name??null}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">{{$vendor->bank_name??null}}</td>
                            <th class="text-right">Total VAT:</th>
                            <td class="text-right">{{($invoice->currency??$currency)." ".number_format(($invoice->vendor_invoice_value - ($total_vat_excluded + $total_expenses)), 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6">Branch Code: {{$vendor->branch_code??null}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Account Number: {{$vendor->bank_acc_no??null}}</td>
                            <th class="text-right">Total:</th>
                            <td class="text-right">{{($invoice->currency??$currency)." ".number_format(($invoice->vendor_invoice_value), 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6">Reference: {{$invoice->vendor_invoice_ref}}</td>
                        </tr>
                        <tr>
                            <th colspan="6" class="text-center">Thank you for your business!</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
