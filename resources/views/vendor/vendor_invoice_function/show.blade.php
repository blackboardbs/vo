@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        {!! Form::open(['url' => route('save.invoice'), 'method' => 'post', 'class' => 'mb-4', 'id' => 'save_invoice']) !!}

        @forelse($timesheets_id as $time)
            {{Form::hidden('time_id[]', $time)}}
        @empty
        @endforelse
        {{Form::hidden('vendor_due_date', $due_date) }}
        {{Form::hidden('cust_inv_ref', $invoice_ref) }}
        {{Form::hidden('vendor_invoice_note', $invoice_notes) }}
        {{Form::hidden('emp_id', $timesheets[0]->employee->id) }}
        {{Form::hidden('ass_id', $timesheets[0]->ass_id) }}
        {{Form::hidden('vendor_reference', $vendor_reference)}}
        {{Form::hidden('consultant', ($timesheets[0]->employee->first_name.', '.$timesheets[0]->employee->last_name)) }}
        {{Form::hidden('project_name', $timesheets[0]->project->name) }}
        {{Form::hidden('vendor_id', $timesheets[0]->employee->vendor_id)}}
        {{Form::hidden('invoice_date', $date)}}

        <div class="row no-gutters">
            <div class="col-md-4 pl-2">
                <h4>Vendor Tax Invoice</h4>
                @if($timesheets[0]->employee->vendor != null)
                    <h5>{{isset($timesheets[0]->employee->vendor->vendor_name)?$timesheets[0]->employee->vendor->vendor_name:null}}</h5>
                    <p class="mb-0">
                        {{isset($timesheets[0]->employee->vendor->postal_address_line1)?$timesheets[0]->employee->vendor->postal_address_line1:null}}
                        @empty($timesheets[0]->employee->vendor->postal_address_line1)
                            {{isset($timesheets[0]->employee->vendor->street_address_line1)?$timesheets[0]->employee->vendor->street_address_line1:null}}
                        @endempty
                    </p>
                    <p class="mb-0">
                        {{isset($timesheets[0]->employee->vendor->postal_address_line2)?$timesheets[0]->employee->vendor->postal_address_line2:null}}
                        @empty($timesheets[0]->employee->vendor->postal_address_line2)
                            {{isset($timesheets[0]->employee->vendor->street_address_line2)?$timesheets[0]->employee->vendor->street_address_line2:null}}
                        @endempty

                    </p>
                    <p class="mb-0">
                        {{isset($timesheets[0]->employee->vendor->postal_address_line3)?$timesheets[0]->employee->vendor->postal_address_line3:null}}
                        @empty($timesheets[0]->employee->vendor->postal_address_line3)
                            {{isset($timesheets[0]->employee->vendor->street_address_line3)?$timesheets[0]->employee->vendor->street_address_line3:null}}
                        @endempty
                    </p>
                    <p class="mb-0">
                        {{$timesheets[0]->employee->vendor->city_suburb}}
                        @empty($timesheets[0]->employee->vendor->city_suburb)
                            {{$timesheets[0]->employee->vendor->street_city_suburb}}
                        @endempty
                    </p>
                    <p class="mb-0">{{$timesheets[0]->employee->vendor->state_province}}</p>
                    <p class="mb-0">
                        {{$timesheets[0]->employee->vendor->postal_zipcode}}
                        @empty($timesheets[0]->employee->vendor->postal_zipcode)
                            {{ $timesheets[0]->employee->vendor->street_zipcode }}
                        @endempty
                    </p>
                    <p class="mb-0">{{$timesheets[0]->employee->vendor->phone}}</p>
                    <p class="mb-0">{{$timesheets[0]->employee->vendor->email}}</p>
                    <p class="mb-0">Contact: {{ $timesheets[0]->employee->vendor->contact_firstname." ".$timesheets[0]->employee->vendor->contact_lastname }}</p>
                    <p class="mb-0">Vat No: {{ $timesheets[0]->employee->vendor->vat_no }}</p>
                @endif
            </div>
            <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{$company->company_name}}</p>
                <p class="mb-0">{{ $company->postal_address_line1}}</p>
                <p class="mb-0">{{ $company->postal_address_line2}}</p>
                <p class="mb-0">{{ $company->postal_address_line3}}</p>
                <p class="mb-0">{{ $company->state_province}}</p>
                <p class="mb-0">{{ $company->postal_zipcode}}</p>
                <p class="mb-0">{{ $company->phone}}</p>
                <p class="mb-0">{{ $company->email}}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no}}</p>
            </div>
            <div class="col-md-4">
                {{--<div class="text-right mb-3"><img src="{{route('vendor_avatar',['q'=>$timesheets[0]->employee->vendor->vendor_logo])}}" alt="{{$timesheets[0]->employee->vendor->vendor_name}} Logo" style="max-height: 150px"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('vendor_avatar', ['q'=> (isset($timesheets[0]->employee->vendor->vendor_logo)?$timesheets[0]->employee->vendor->vendor_logo:'default.png')])}}" alt="{{$timesheets[0]->employee->vendor->vendor_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{$invoice_number}}</div>
                    <div class="col-md-6">Invoice Date:</div>
                    <div class="col-md-6 text-right">{{$date}}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $due_date }}</div>
                    <div class="col-md-4">Vendor Reference:</div>
                    <div class="col-md-8 text-right">{{$vendor_reference}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ isset($timesheets[0]->employee->vendor)? $timesheets[0]->employee->vendor->account_managerd->first_name.' '.$timesheets[0]->employee->vendor->account_managerd->last_name : '' }}</div>
                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Description</th>
                            <th>Qty</th>
                            <th class="text-right">Excl. Price</th>
                            <th class="text-right">VAT%</th>
                            <th class="text-right">Exclusive Total</th>
                            <th class="text-right">Inclusive Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($timesheets as $index => $timeline)
                            <tr>
                                <td>Timesheet for {{ $timeline->employee->first_name.' '.$timeline->employee->last_name }} for week {{ $timeline->year_week }} on assignment {{ $timeline->project->name }} {{isset($timeline->customer_po)?' customer po number '.$timeline->customer_po:''}}</td>
                                {{Form::hidden('start_date[]', $timeline->start_date) }}
                                {{Form::hidden('end_date[]', $timeline->start_date) }}
                                {{Form::hidden('quantity[]', $timeline->billable_hours) }}
                                {{Form::hidden('customer_po[]', $timeline->customer_po)}}
                                <td>{{ number_format($timeline->billable_hours,2,'.',',') }}</td>
                                <td class="text-right">{{ number_format($timeline->external_cost_rate,2,'.',',') }} </td>
                                {{Form::hidden('rate[]', $timeline->external_cost_rate)}}
                                <td class="text-right">{{$vat_rate[$index]}}%</td>
                                <td class="text-right">R {{number_format(($timeline->billable_hours * $timeline->external_cost_rate) ,2,'.',',') }}</td>
                                <td class="text-right">R {{ number_format(($timeline->billable_hours * $timeline->external_cost_rate * (($vat_rate[$index] != 0) ? $vat_rate[$index]/100 : 0) ) + ($timeline->billable_hours * $timeline->external_cost_rate),2,'.',',') }}</td>
                                {{Form::hidden('price[]', ($timeline->billable_hours * $timeline->external_cost_rate * (($vat_rate[$index] != 0) ? $vat_rate[$index]/100 : 0)) + ($timeline->billable_hours * $timeline->external_cost_rate))}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">Nothing to show here</td>
                            </tr>
                        @endforelse
                        <tr>
                            <th class="text-right">TOTAL HOURS</th>
                            <th colspan="5">{{number_format($total_hours,2,'.',',')}}</th>
                        </tr>

                        @if(count($time_exp))
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="btn-dark">
                                <th colspan="6">Expenses</th>
                            </tr>
                        @endif

                        @foreach($time_exp as $expense)
                            <tr>
                                <td>Expenses for {{ isset($expense->timesheet->employee)?$expense->timesheet->employee->first_name.' '.$expense->timesheet->employee->last_name:'' }} for week {{ $expense->timesheet->year_week }} on assignment {{ $expense->timesheet->project->name }} for {{ $expense->description }}.</td>
                                <td>1</td>
                                <td class="text-right">{{ number_format($expense->amount,2,'.',',') }}</td>
                                <td class="text-right">0.00%</td>
                                <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                {{Form::hidden('time_exp_id[]', $expense->id) }}
                            </tr>
                        @endforeach

                        <tr>
                            <th colspan="6">&nbsp;</th>
                        </tr>

                        <tr class="btn-dark">
                            <th colspan="6">Summary</th>
                        </tr>
                        <tr>
                            <th>Payment Information</th>
                            <td colspan="3">{{ $invoice_notes }}</td>
                            <th class="text-right">Total Exclusive:</th>
                            <td class="text-right">R {{ number_format(($total + $total_exclusive) ,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6">{{ isset($timesheets[0]->employee->vendor) ? $timesheets[0]->employee->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">{{  isset($timesheets[0]->employee->vendor) ? $timesheets[0]->employee->vendor->bank_name : '' }}</td>
                            <th class="text-right">Total VAT:</th>
                            <td class="text-right">R {{ number_format($total_vat,2,'.',',') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6">Branch Code: {{ isset($timesheets[0]->employee->vendor) ? $timesheets[0]->employee->vendor->branch_code : '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Account Number: {{ isset($timesheets[0]->employee->vendor) ? $timesheets[0]->employee->vendor->bank_acc_no : '' }}</td>
                            <th class="text-right">Total:</th>
                            <td class="text-right">R {{ number_format(($total_vat + $total_exclusive + $total),2,'.',',') }}</td>
                            {{ Form::hidden('invoice_value', ($total_vat + $total_exclusive + $total)) }}
                        </tr>
                        <tr>
                            <td colspan="6">Reference: {{ isset($timesheets[0]->employee->vendor) ? $timesheets[0]->employee->vendor->vendor_name : '' }}</td>
                        </tr>
                        <tr>
                            <th colspan="6" class="text-center">Thank you for your business!</th>
                        </tr>

                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left"><button class="btn btn-dark btn-sm"><i class="far fa-save"></i> Save Invoice</button></div>
                        <div class="col-md-6 text-right"><a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a></div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                            <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="email-form">
                                <div class="form-group row">
                                    <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-check">
                                    <input type="checkbox" name="customer_email" value="{{$timesheets[0]->employee->vendor->email}}" checked class="form-check-input" id="customer_email">
                                    <label class="form-check-label" for="customer_email">{{$timesheets[0]->employee->vendor->email}}</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" name="company_email" value="{{isset($timesheets[0]->company)?$timesheets[0]->company->email:''}}" checked class="form-check-input" id="company_email">
                                    <label class="form-check-label" for="company_email">{{isset($timesheets[0]->company)?$timesheets[0]->company->email:''}}</label>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group text-center">
                                    <button class="btn btn-dark btn-sm" id="btn-send"><i class="fas fa-paper-plane"></i> Send Invoice</button>
                                </div>
                            </div>
                            <div id="loader-wrapper">
                                <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endsection

@section("extra-js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{global_asset('js/multiple-emails.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })

            $(function () {
                $('#user_email').multiple_emails({
                    position: 'top', // Display the added emails above the input
                    theme: 'bootstrap', // Bootstrap is the default theme
                    checkDupEmail: true // Should check for duplicate emails added
                });

                $('#current_emails').text($('#user_email').val());

                $('#user_email').change( function(){
                    $('#current_emails').text($(this).val());
                });
            })

            $(function () {
                $("#btn-send").on('click', function () {
                    $("#save_invoice")
                        .append('<input type="hidden" name="send" value="1" /> ')
                        .append($("#customer_email"))
                        .append($("#company_email"))
                        .append($("#user_email"))
                        .append($("#subject"))
                        .submit();

                        $("#email-form").hide();
                        $("#loader-wrapper").fadeIn();
                        $(".default").hide();
                        $(".sending").fadeIn();
                })
            })
        });
    </script>
@endsection
