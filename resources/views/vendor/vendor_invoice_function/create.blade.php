@extends('adminlte.default')

@section('title') Active Timesheets @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
                <a href="javascript:void()" onclick="saveForm('timesheet')" class="btn btn-primary ml-2" style="margin-top: -7px;">Prepare</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        {!! Form::open(['url' => route('function_newinvoice.create'), 'id'=>'filters', 'method' => 'get']) !!}
        <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
        @if(isset($_GET['project']) && $_GET['project'] > 0)
        <div class="row">
            <div class="col-md-2" style="max-width: 16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('week', $year_weeks, (isset($_GET['week']) ? $_GET['week'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_week']) !!}
                    <span>Week</span>
                    </label>
                </div>
            </div>
           <div class="col-md-2" style="max-width: 16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('vendor', $vendor_drop, (isset($_GET['vendor']) ? $_GET['vendor'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_vendor']) !!}
                    <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="max-width: 16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('project', $project, (isset($_GET['project']) ? $_GET['project'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_project']) !!}
                    <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" id="billing-cycle" style="max-width: 16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {!! Form::select('billing-period', [], request()->input("billing-period"), ['class' => 'form-control search', 'id' => 'billing-period']) !!}
                <span>Billing Period</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="max-width: 16.67%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('employees', $employees, (isset($_GET['employees']) ? $_GET['employees'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_employee']) !!}
                    <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="max-width: 15%;">
                <a href="{{ route('function_newinvoice.create') }}" class="btn btn-info w-100" type="submit">Clear Filters</a></div>
        </div>
        @else<div class="row w-100">
            <div class="col-md-2" style="min-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('week', $year_weeks, (isset($_GET['week']) ? $_GET['week'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_week']) !!}
                    <span>Week</span>
                    </label>
                </div>
            </div>
           <div class="col-md-2" style="min-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('vendor', $vendor_drop, (isset($_GET['vendor']) ? $_GET['vendor'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_vendor']) !!}
                    <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="min-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('project', $project, (isset($_GET['project']) ? $_GET['project'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_project']) !!}
                    <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="min-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">{!! Form::select('employees', $employees, (isset($_GET['employees']) ? $_GET['employees'] : null), ['placeholder' => 'All', 'class' => 'form-control ', 'id' => 'filter_employee']) !!}
                    <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="min-width: 20%;">
                <a href="{{ route('function_newinvoice.create') }}" class="btn btn-info w-100" type="submit">Clear Filters</a></div>
        </div>
        @endif
        {!! Form::close() !!}
        <div class="table-responsive">
            {!! Form::open(['url' => route('prepare.invoice'), 'id' => 'timesheet']) !!}

            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr class="bg-dark">
                        <th><input type="checkbox" id="select_all"> @sortablelink('Select All')</th>
                        <th>@sortablelink('#')</th>
                        <th>@sortablelink('Employee')</th>
                        <th>@sortablelink('Project')</th>
                        <th>@sortablelink('Vendor Name')</th>
                        <th>@sortablelink('Year Week')</th>
                        <th class="text-right">@sortablelink('Bill')</th>
                        <th class="text-right">@sortablelink('No Bill')</th>
                        <th class="last text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($timesheets as $key => $timesheet)
                        <tr>
                            @php
                                $billable_hours = explode('.', (($timesheet->billable_hours * 60) + $timesheet->billable_minutes)/60)[0];
                                $billable_minutes = (($timesheet->billable_hours * 60) + $timesheet->billable_minutes)%60;
                                $non_billable_hours = explode('.', (($timesheet->non_billable_hours * 60) + $timesheet->non_billable_minutes)/60)[0];
                                $non_billable_minutes = (($timesheet->non_billable_hours * 60) + $timesheet->non_billable_minutes)%60;
                            @endphp
                            <td><input type="checkbox" name="timesheet[]" class="timesheet_select" value="{{$timesheet->id}}" {{(isset($selected_timesheets) && in_array($timesheet->id,$selected_timesheets) != false ? 'checked' : '')}} /> </td>
                            <td>{{ $timesheet->id }}</td>
                            <td><a href="{{ route('timeline.show', $timesheet) }}">{{ $timesheet->employee->first_name.', '.$timesheet->employee->last_name }}</a> </td>
                            <td>{{ $timesheet->project->name }}</td>
                            <td>{{ ($timesheet->employee->vendor != null) ? $timesheet->employee->vendor->vendor_name : 'User is not linked to vendor' }}</td>
                            <td>{{ $timesheet->year_week }}</td>
                            <td class="text-right">{{(($billable_hours < 10)?'0'.$billable_hours : $billable_hours).':'.(($billable_minutes < 10)?'0'.$billable_minutes : $billable_minutes)}}</td>
                            <td class="text-right">{{(($non_billable_hours < 10)?'0'.$non_billable_hours : $non_billable_hours).':'.(($non_billable_minutes < 10)?'0'.$non_billable_minutes : $non_billable_minutes)}}</td>
                            <td>
                                @if($timesheet->external_cost_rate == '' || $timesheet->external_cost_rate == 0.00)
                                    <a href="{{route('assignment.show',$timesheet->ass_id)}}" class="btn btn-sm btn-warning">No External Cost</a>
                                @endif
                            </td>
                        </tr>
                        @php
                            $total_billable_hours += $billable_hours;
                            $total_billable_minutes += $billable_minutes;
                            $total_non_billable_hours += $non_billable_hours;
                            $total_non_billable_minutes += $non_billable_minutes;
                            $actual_total_bill_hours = explode('.',(($total_billable_hours *60) + $total_billable_minutes)/60)[0];
                            $actual_total_bill_min = (($total_billable_hours * 60) + $total_billable_minutes) % 60;
                            $actual_total_non_bill_hours = explode('.',(($total_non_billable_hours *60) + $total_non_billable_minutes)/60)[0];
                            $actual_total_non_bill_min = (($total_non_billable_hours * 60) + $total_non_billable_minutes) % 60;
                        @endphp
                    @empty
                        <tr>
                            <td colspan="17" class="text-center">No open timesheet.</td>
                        </tr>
                    @endforelse
                    @if(count($timesheets) > 0)
                        <tr>
                            <td colspan="5"></td>
                            <th>TOTAL HOURS</th>
                            <th class="text-right">{{(($actual_total_bill_hours < 10)?'0'.$actual_total_bill_hours:$actual_total_bill_hours).':'.(($actual_total_bill_min < 10)?'0'.$actual_total_bill_min:$actual_total_bill_min)}}</th>
                            <th class="text-right">{{(($actual_total_non_bill_hours < 10)?'0'.$actual_total_non_bill_hours:$actual_total_non_bill_hours).':'.(($actual_total_non_bill_min < 10)?'0'.$actual_total_non_bill_min:$actual_total_non_bill_min)}}</th>
                            <th></th>
                        </tr>
                    @endif
                    
                </tbody>
            </table>
            {{-- <div class="text-center">
                {!! Form::submit('Prepare Invoice', ['class' => 'btn btn-sm', 'disabled' => 'disabled', 'id' => 'prepare']) !!}
            </div> --}}
            {!! Form::close() !!}
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $timesheets->firstItem() }} - {{ $timesheets->lastItem() }} of {{ $timesheets->total() }}
                        </td>
                        <td>
                            {{ $timesheets->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
                {{-- {{ $timesheets->appends(request()->except('page'))->links() }} --}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {



            if ($("#filter_project").val() || '{{request()->project}}'){
                axios.get('/billingcycle-id-invoice/{{request()->project}}')
                    .then(response => {
                        let options = "<option value=''>Select Billing Period</option>";
                        response.data.forEach((v, i) => options += `<option ${getUrlVars()["billing-period"] == v.id ? 'selected':null} value="${v.id}">${v.name}</option>`);
                        $("#billing-period").html(options)
                    })
                    .catch(err => console.log(err.response));
                $("#billing-cycle").css({
                    display: "flex"
                })
            }



            $('.timesheet_select').each(function (){
                if($(this).is(':checked')){
                    $("#prepare").prop('disabled', false);
                }
            })
        });

        $(() => {
            $('#select_all').on('click',function(){

                let timesheet_id = []
                if(this.checked){
                    $('.timesheet_select').each(function(){
                        this.checked = true;
                        timesheet_id.push(this.value)
                    });
                    session('{{route('session.store')}}', timesheet_id)
                }else{
                    $('.timesheet_select').each(function(){
                        this.checked = false;
                        timesheet_id.push(this.value)
                    });
                    session('{{route('session.pull')}}', timesheet_id)
                }
            });

            $(".timesheet_select").on('change', e => {
                if (e.target.checked){
                    session('{{route('session.store')}}', [e.target.value])
                }else {
                    session('{{route('session.pull')}}', [e.target.value])
                }
            })

            $('.timesheet_select').on('click',function(){
                selectedAll()
            });

            selectedAll()
        })

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }

        function selectedAll(){
            if($('.timesheet_select:checked').length === $('.timesheet_select').length && $('.timesheet_select').length !== 0){
                $('#select_all').prop('checked',true);
            }else{
                $('#select_all').prop('checked',false);
            }
        }

        function session(url, timesheets_id){
            axios.post(url, {ids: timesheets_id})
                .then(response => {
                    $("#prepare").prop('disabled', !response.data.ids.length);
                }).catch(error => {
                console.log(error)
            })
        }
    </script>
@endsection
