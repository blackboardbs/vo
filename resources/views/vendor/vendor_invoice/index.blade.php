@extends('adminlte.default')

@section('title') Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('nontimesheetinvoice.vendor_create', ['recurring' => 1])}}" class="btn btn-success float-right ml-2"><i class="fa fa-plus"></i> Recurring Invoice</a>
            <a href="{{route('nontimesheetinvoice.vendor_create')}}" class="btn btn-success float-right ml-2"><i class="fa fa-plus"></i> Non Timesheet Invoice</a>
            <a href="{{route('function_newinvoice.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> New Invoice</a>
            <x-export route="vendor.invoice"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="form-row">
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    <div class="position-relative w-100">
                        <input class="form-control form-control-sm w-100" id="vendor" placeholder="Select Vendor">
                        <div class="position-absolute w-100 bg-gray-light shadow rounded list"></div>
                    </div>
                    <span>Vendor</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('invoice_status', App\Enum\InvoiceStatusEnum::keyValuePair(),request()->invoice_status,['class'=>'form-control search', 'placeholder' => "Select Invoice Status"])}}
                    <span>Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                        <input type="text" name="filter_from" class="datepicker form-control w-100" value="{{(isset($_GET['filter_from']) ? $_GET['filter_from'] : '')}}">
    <span>From</span>
</label>
</div>
</div>
<div class="col-md-3" style="max-width:16.67%;">
<div class="form-group input-group">
<label class="has-float-label">
    <input type="text" name="filter_to" class="datepicker form-control w-100" value="{{(isset($_GET['filter_to']) ? $_GET['filter_to'] : '')}}">
<span>To</span>
</label>
</div>
</div>
<div class="col-md-3" style="max-width:16.67%;">
<div class="form-group input-group">
<label class="has-float-label">
    <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}"  onkeypress="handle(event)" />
    <span>Matching</span>
</label>
</div>
</div>
<div class="col-md-3 align-bottom" style="max-width:15%;">
<a href="{{ route('vendor.invoice') }}" class="btn btn-info w-100" type="submit">Clear Filters</a>
</div>
</div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="bg-dark">
                    <th>@sortablelink('id', 'Invoice #')</th>
                    <th>@sortablelink('vendor.vendor_name', 'Vendor Name')</th>
                    <th>@sortablelink('vendor_invoice_ref', 'Invoice Ref')</th>
                    <th>@sortablelink('vendor_invoice_date', 'Invoice Date')</th>
                    <th>@sortablelink('vendor_due_date', 'Due Date')</th>
                    <th>@sortablelink('vendor_paid_date', 'Paid Date')</th>
                    <th class="text-right">@sortablelink('vendor_invoice_value', 'Invoice Value')</th>
                    <th>@sortablelink('invoice_status.description', 'Vendor Invoice Status')</th>
                    <th class="text-center">Type</th>
                    <th>@sortablelink('created_at', 'Added')</th>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($vendor_invoices as $vendor_invoice)
                    <tr>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->id}}</a>@else{{$vendor_invoice->id}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor?->vendor_name}}</a>@else{{$vendor_invoice->vendor?->vendor_name}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_ref}}</a>@else{{$vendor_invoice->vendor_invoice_ref}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_invoice_date}}</a>@else{{$vendor_invoice->vendor_invoice_date}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_due_date}}</a>@else{{$vendor_invoice->vendor_due_date}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->vendor_paid_date}}</a>@else{{$vendor_invoice->vendor_paid_date}}@endif</td>
                        <td class="text-right">@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{($vendor_invoice->currency??"ZAR")." ".number_format($vendor_invoice->vendor_invoice_value,2,'.',',')}}</a>@else{{($vendor_invoice->currency??"ZAR")." ".number_format($vendor_invoice->vendor_invoice_value,2,'.',',')}}@endif</td>
                        <td>@if($vendor_invoice->invoiceType() !== "RI")<a href="{{route('view.invoice',$vendor_invoice->id)}}">{{$vendor_invoice->invoice_status?->description}}</a>@else{{$vendor_invoice->invoice_status?->description}}@endif</td>
                        <td class="text-center"><span class="badge badge-primary">{{ $vendor_invoice->invoiceType() }}</span></td>
                        <td>{{ $vendor_invoice->created_at }}</td>
                        @if($can_update)
                            <td>
                                @if($vendor_invoice->vendor_invoice_status != 3)
                                    @if($vendor_invoice->invoiceType() == "RI")
                                        <a href="{{route('edit.invoice',['invoice_id' => $vendor_invoice, 'recurring' => 1])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    @else
                                        <a href="{{route('edit.invoice',$vendor_invoice)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                @endif
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No vendors match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th>Total</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <th></th>
                    <th class="text-right">
                        @foreach($total as $key=>$value)
                            {{$key}}: {{number_format($value,2,'.',',')}}<br />
                        @endforeach
                    </th>
                    <td></td>
                    <td></td>
                    @if($can_update)
                        <td>
                        </td>
                    @endif
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $vendor_invoices->firstItem() }} - {{ $vendor_invoices->lastItem() }} of {{ $vendor_invoices->total() }}
                        </td>
                        <td>
                            {{ $vendor_invoices->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $vendor_invoices->appends(request()->except('page'))->links() }} --}}
        </div>

    </div>
@endsection

@section('extra-js')
<script src="{{global_asset('js/filters.js')}}"></script>
<script>
$(document).ready(function (){
const vendor_dropdowns = @json(session('vendor_dropdown'));
const url = new URL(window.location.toLocaleString());

$("#vendor").dropdownSearch({
name: "vendor",
dropdown: vendor_dropdowns !== null ? vendor_dropdowns : []
});

if (url.searchParams.get('vendor_id')){
let customer = `<input type="hidden" name="vendor_id" value="${url.searchParams.get('vendor_id')}" >`;
$("#searchform").append(customer)
}
})
</script>
@endsection
@section('extra-css')
<style>
.list{
display: none;
z-index: 1000;
}
.data-list:hover{
background-color: #0e90d2;
color: #f9f9f9!important;
}
</style>
@endsection
