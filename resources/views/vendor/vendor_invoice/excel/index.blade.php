<table>
    <thead>
    <tr>
        <th>Invoice #</th>
        <th>Vendor Name</th>
        <th>Invoice Ref</th>
        <th>Invoice Date</th>
        <th>Due Date</th>
        <th>Paid Date</th>
        <th>Invoice Value</th>
        <th>Vendor Invoice Status</th>
        <th>Added</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vendor_invoices as $vendor_invoice)
        <tr>
            <td>{{$vendor_invoice->id}}</td>
            <td>{{$vendor_invoice->vendor?->vendor_name}}</td>
            <td>{{$vendor_invoice->vendor_invoice_ref}}</td>
            <td>{{$vendor_invoice->vendor_invoice_date}}</td>
            <td>{{$vendor_invoice->vendor_due_date}}</td>
            <td>{{$vendor_invoice->vendor_paid_date}}</td>
            <td>{{($vendor_invoice->currency??"ZAR")." ".number_format($vendor_invoice->vendor_invoice_value,2,'.',',')}}</td>
            <td>{{$vendor_invoice->invoice_status?->description}}</td>
            <td>{{ $vendor_invoice->created_at }}</td>
        </tr>
    @endforeach
    <tr>
        <th>Total</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th></th>
        <th class="text-right">
            @foreach($total as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
        <td></td>
        <td></td>
        @if($can_update)
            <td>
            </td>
        @endif
    </tr>
    </tbody>
</table>