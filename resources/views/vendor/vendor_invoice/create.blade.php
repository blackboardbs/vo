@extends('adminlte.default')

@section('title') Add Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('vendor.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Vendor Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Vendor Name:</th>
                    <td>{{Form::text('vendor_name',old('vendor_name'),['class'=>'form-control form-control-sm'. ($errors->has('vendor_name') ? ' is-invalid' : ''),'placeholder'=>'Vendor Name'])}}
                        @foreach($errors->get('vendor_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Registration Number:</th>
                    <td>{{Form::text('business_reg_no',old('business_reg_no'),['class'=>'form-control form-control-sm'. ($errors->has('business_reg_no') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}
                        @foreach($errors->get('business_reg_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>VAT Number:</th>
                    <td>{{Form::text('vat_no',old('vat_no'),['class'=>'form-control form-control-sm'. ($errors->has('vat_no') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Account Manager:</th>
                    <td>{{Form::select('account_manager',$account_manager_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('account_manager') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('account_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Assignment Approver:</th>
                    <td>{{Form::select('assignment_approver',$assignment_approver_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('assignment_approver') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('assignment_approver') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Payment Terms:</th>
                    <td>{{Form::text('payment_terms',old('payment_terms'),['class'=>'form-control form-control-sm'. ($errors->has('payment_terms') ? ' is-invalid' : ''),'placeholder'=>'Payment Terms'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Payment Terms Days:</th>
                    <td>{{Form::text('payment_terms_days',old('payment_terms_days'),['class'=>'form-control form-control-sm'. ($errors->has('payment_terms_days') ? ' is-invalid' : ''),'placeholder'=>'Payment Terms Days'])}}
                        @foreach($errors->get('payment_terms_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Firstname:</th>
                    <td>{{Form::text('contact_firstname',old('contact_firstname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Lastname:</th>
                    <td>{{Form::text('contact_lastname',old('contact_lastname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}
                        @foreach($errors->get('contact_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Birthday:</th>
                    <td>{{Form::text('contact_birthday',old('contact_birthday'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('contact_birthday') ? ' is-invalid' : ''),'placeholder'=>'Contact Birthday'])}}
                        @foreach($errors->get('contact_birthday') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',old('phone'),['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell:</th>
                    <td>{{Form::text('cell',old('cell'),['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('cell') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{Form::text('fax',old('fax'),['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell 2:</th>
                    <td>{{Form::text('cell2',old('cell2'),['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                        @foreach($errors->get('cell2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{Form::text('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Web:</th>
                    <td>{{Form::text('web',old('web'),['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}
                        @foreach($errors->get('web') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Postal Address:</th>
                    <td>{{Form::text('postal_address_line1',old('postal_address_line1'),['class'=>'form-control form-control-sm'. ($errors->has('postal_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 1'])}}
                        @foreach($errors->get('postal_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line2',old('postal_address_line2'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 2'])}}
                        @foreach($errors->get('postal_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line3',old('postal_address_line3'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 3'])}}
                        @foreach($errors->get('postal_address_line3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('city_suburb',old('city_suburb'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_zipcode',old('postal_zipcode'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('postal_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                    <th>Street Address:</th>
                    <td>{{Form::text('street_address_line1',old('street_address_line1'),['class'=>'form-control form-control-sm'. ($errors->has('street_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 1'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line2',old('street_address_line2'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 2'])}}
                        @foreach($errors->get('street_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line3',old('street_address_line3'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 3'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_city_suburb',old('street_city_suburb'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('street_city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_zipcode',old('street_zipcode'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('street_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                </tr>
                <tr>
                    <th>State/Province:</th>
                    <td>{{Form::text('state_province',old('state_province'),['class'=>'form-control form-control-sm'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                        @foreach($errors->get('state_province') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Country:</th>
                    <td>{{Form::select('country_id',$countries_dropdown,'1',['class'=>'form-control form-control-sm '. ($errors->has('country_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_countries'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{Form::select('bbbee_level',$bbbee_level_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BBBEE Certificate Expiry:</th>
                    <td>{{Form::text('bbbee_certificate_expiry',old('bbbee_certificate_expiry'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>

                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection
