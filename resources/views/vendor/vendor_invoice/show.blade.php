@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-3 pl-2">
                <h4>Vendor Invoice</h4>
                <h5>{{$data->company->company_name}}</h5>
                <p class="mb-0">{{ $data->company->postal_address_line1 }}</p>
                <p class="mb-0">{{ $data->company->postal_address_line2 }}</p>
                @if($data->company->postal_address3 !== null)
                    <p class="mb-0">{{ $data->company->postal_address_line3 }}</p>
                @endif
                <p class="mb-0">{{ $data->company->state_province }}</p>
                <p class="mb-0">{{ $data->company->postal_zipcode }}</p>
                <p class="mb-0">{{ $data->company->phone }}</p>
                <p class="mb-0">{{ $data->company->email }}</p>
                <p class="mb-0">VAT NO: {{ $data->company->vat_no }}</p>
            </div>
            <div class="col-md-3">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{$vendor->customer->customer_name}}</p>
                <p class="mb-0">
                        {{$vendor->customerpostal_address_line1}}
                    @empty($vendor->customer->postal_address_line1)
                        {{$vendor->customer->street_address_line1}}
                    @endempty
                <p class="mb-0">
                        {{$vendor->customer->postal_address_line2}}
                    @empty($vendor->customer->postal_address_line2)
                        {{$vendor->customer->street_address_line2}}
                    @endempty

                </p>
                <p class="mb-0">
                        {{$vendor->customer->postal_address_line3}}
                    @empty($vendor->customer->postal_address_line3)
                        {{$vendor->customer->street_address_line3}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$vendor->customer->city_suburb}}
                    @empty($vendor->customer->city_suburb)
                        {{$vendor->customer->street_city_suburb}}
                    @endempty
                </p>
                <p class="mb-0">{{$vendor->customer->state_province}}</p>
                <p class="mb-0">
                    {{$vendor->customer->postal_zipcode}}
                    @empty($vendor->customer->postal_zipcode)
                        {{ $vendor->customer->street_zipcode }}
                    @endempty
                </p>
            </div>
            <div class="col-md-3">
                {{--<div class="text-right mb-3"><img src="{{route('company_avatar', ['q'=> $data->company->company_logo])}}" alt="{{$data->company->company_name}} Logo"  style="max-height: 150px;"></div>--}}
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> $company->company_logo])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{$invoice_no}}</div>
                    <div class="col-md-6">Date:</div>
                    <div class="col-md-6 text-right">{{$date}}</div>
                    <div class="col-md-6">Page:</div>
                    <div class="col-md-6 text-right">1/1</div>
                    <div class="col-md-4">Reference:</div>
                    <div class="col-md-8 text-right">{{$project->wbs->project_ref}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ ' ' }}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $project->wbs->end_date }}</div>
                    <div class="col-md-6">Vendor Contact:</div>
                    <div class="col-md-6 text-right">{{ $vendor->customer->contact_firstname." ".$vendor->customer->contact_lastname }}</div>
                    <div class="col-md-6">Vat Number:</div>
                    <div class="col-md-6 text-right">{{ $vendor->customer->vat_no }}</div>
                </div>
            </div>
            <div class="col-md-9 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-9 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark row">
                            <th class="col-6">Description</th>
                            <th class="col-1">Qty</th>
                            <th class="col-2">Excl. Price</th>
                            <th class="col-2">Inclusive Total</th>
                            <th class="col-1">Paid</th>
                        </tr>
                        </thead>
                        <tbody>
                           <tr class="row">
                                <td class="col-6">{{ $project->wbs->project_name.' - '.$vendor->customer->customer_name }}</td>
                                <td class="col-1">{{ $bhours}}</td>
                                <td class="col-2">{{ $assignments->currency .' '.$assignments->external_cost_rate }}</td>
                               <td class="col-2">{{ number_format($ex_total,2,".",",") }}</td>
                               <td class="col-1">{{ $paid }}</td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr class="row">
                                <td class="col-6">Expenses</td>
                                <td class="col-1"></td>
                                <td class="col-2" class="border border-bottom border-dark">Claim</td>
                                <td class="col-2">Outstanding</td>
                                <td class="col-1">Paid Date</td>
                            </tr>
                           @php
                            $tot = 0;
                            $toth = 0;
                            $totv = 0;
                            $tote = 0;
                            $totee = 0;
                            $totee_out = 0;
                            $tothh = 0;
                            $tot_vat = 0;
                            $amount_out = 0;
                            //$in_total = $value * 1.14;
			                 //$vat = $ex_total * 0.14;
                           @endphp

                           @forelse($invoiceas as $invoicea)
                               @php
                                    $tote += $invoicea->amount;
                                    $totee_out += $amount_out;
                                   if ($invoicea->paid_date <> '0000-00-00'){
                                        $amount_out = 0;
                                   }else{
                                       $amount_out = $invoicea->amount;
                                   }
                                   if ($invoicea->claim_auth_user <> 0){
                                        $es = "A";
                                       if ($invoicea->paid_date <> "0000-00-00")
                                           { $es = "P";
                                       }
                                   }
                               @endphp
                                <tr class="row">
                                    <td class="col-6">{{ $invoicea->date.' '.$assignments->resource->emp_pref_name.' '.$assignments->resource->emp_lastname.' '.$invoicea->description }}</td>
                                    <td class="col-1"></td>
                                    <td class="col-2">{{ number_format($invoicea->amount,2,".",",") }}</td>
                                    <td class="col-1">{{ number_format($amount_out,2,".",",") }} </td>
                                    <td class="col-1">{{ $es }}</td>
                                    <td class="col-1">{{ $invoicea->paid_date }}</td>
                                </tr>
                                @empty
                            @endforelse
                                @php
                                   $totee = 0;
                                   $total = $tote + $ex_total;
                                   $totee += $tote;
                                @endphp
                               <tr class="row">
                                   <td class="col-2"></td>
                                   <td class="col-5 text-center">{{ $inv_message }}</td>
                                   <th class="col-2 text-left">Total Claim</th>
                                   <td class="col-1"></td>
                                   <td class="col-1 text-right">{{ number_format($totee,2,".",",") }}</td>
                               </tr>
                               <tr class="row">
                                   <td class="col-6"></td>
                                   <td class="col-1"></td>
                                   <th class="text-left col-2">Total Claim Not Paid</th>
                                   <td class="col-1"></td>
                                   <td class="col-1 text-right">{{ number_format($totee_out,2,".",",") }}</td>
                               </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endsection
