@extends('adminlte.default')
@section('title') Edit Customer Preference @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('update_customer_preference_form')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customerpreference.update', $customer_preference), 'method' => 'put','class'=>'mt-3', 'id'=>'update_customer_preference_form','files' => true,'autocomplete'=>'on'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Create Customer Preference</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Customer:</th>
                        <td>
                            {{Form::select('customer_id', $customer_drop_down, $customer_preference->customer_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_id') ? ' is-invalid' : ''), 'placeholder'=>'Please select'])}}
                            @foreach($errors->get('customer_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th style='width: 20%'>Skill:</th>
                        <td>
                            {{Form::select('skill_id[]', $skill_drop_down, explode('|', $customer_preference->skill_id), ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('skill_id') ? ' is-invalid' : ''), 'multiple'])}}
                            @foreach($errors->get('skill_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Profession:</th>
                        <td>
                            {{Form::select('profession_id', $profession_drop_down, $customer_preference->profession_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('profession_id') ? ' is-invalid' : ''), 'placeholder'=>'Please select', 'id' => 'profession_id'])}}
                            @foreach($errors->get('profession_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th style='width: 20%'>Speciality:</th>
                        <td>
                            {{Form::select('speciality_id', $speciality_drop_down, $customer_preference->speciality_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'placeholder'=>'Please select', 'id' => 'speciality_id'])}}
                            @foreach($errors->get('speciality_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Credit Check:</th>
                        <td>
                            {{Form::select('credit_check', [0 => 'No', 1 => 'Yes'], $customer_preference->credit_check,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('credit_check') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                            @foreach($errors->get('credit_check') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Criminal Record:</th>
                        <td>
                            {{Form::select('criminal_record', [0 => 'No', 1 => 'Yes'], $customer_preference->criminal_record,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('criminal_record') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                            @foreach($errors->get('criminal_record') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    <tr>
                    <tr>
                        <th>Note:</th>
                        <td colspan="3">
                            {{Form::textarea('note', $customer_preference->note, ['size' => '30x5', 'class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('note') ? ' is-invalid' : ''), 'placeholder'=>''])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $('#consultants_chosen').css('width', '100%');
        $('.chosen-container').css('width', '100%');

        $('#profession_id').change(function () {

            var profession_id = $('#profession_id').val();

            axios.post('/cv/getspeciality/' + profession_id)
            .then(function (response) {
                var options_html = '<option>Please select</option>';
                $.each(response.data.speciality, function (key, value) {
                    options_html += '<option value=' + key + '>' + value + '</option>';
                });

                $('#speciality_id').html(options_html);
                $('#speciality_id').trigger("chosen:updated");
            })
            .catch(function (error) {
                console.log(error);
            });

        });
    </script>
@endsection