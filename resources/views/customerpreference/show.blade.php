@extends('adminlte.default')
@section('title') View Customer Preference @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Create Customer Preference</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Customer:</th>
                    <td>
                        {{Form::select('customer_id', $customer_drop_down, $customer_preference->customer_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Skill:</th>
                    <td>
                        {{Form::select('skill_id', $skill_drop_down, explode('|', $customer_preference->skill_id), ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('skill_id') ? ' is-invalid' : ''), 'disabled'=>'disabled', 'multiple'])}}
                        @foreach($errors->get('skill_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Profession:</th>
                    <td>
                        {{Form::select('profession_id', $profession_drop_down, $customer_preference->profession_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('profession_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('profession_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Speciality:</th>
                    <td>
                        {{Form::select('speciality_id', $speciality_drop_down, $customer_preference->speciality_id, ['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('speciality_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Credit Check:</th>
                    <td>
                        {{Form::select('credit_check', [0 => 'No', 1 => 'Yes'], $customer_preference->credit_check,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('credit_check') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('credit_check') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Criminal Record:</th>
                    <td>
                        {{Form::select('criminal_record', [0 => 'No', 1 => 'Yes'], $customer_preference->criminal_record,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('criminal_record') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('criminal_record') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                <tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note', $customer_preference->note, ['size' => '30x5', 'class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('note') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $('#consultants_chosen').css('width', '100%');
        $('.chosen-container').css('width', '100%');
    </script>
@endsection