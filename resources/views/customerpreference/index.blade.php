@extends('adminlte.default')
@section('title') Customer Preference @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('customerpreference.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Preference</a>
        @endif
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        {{--<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                Customber<br/>
                {{Form::select('c',$customer_drop_down,old('c'),['class'=>' form-control form-control-sm search', 'id' => 'c','style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Reference Number<br/>
                {{Form::text('r',old('r'),['class'=>'form-control form-control-sm search', 'id' => 'r', 'style'=>'width: 100%;', 'placeholder' => ''])}}
            </div>
            <div class="col-sm-3 col-sm">
                Profession<br/>
                {{Form::select('p',$profession_drop_down,old('p'),['class'=>' form-control form-control-sm search', 'id' => 'p', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Speciality<br/>
                {{Form::select('sp',$speciality_drop_down,old('sp'),['class'=>' form-control form-control-sm search', 'id' => 'sp', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                 Position Type<br/>
                {{Form::select('e',$position_type_drop_down,old('e'),['class'=>' form-control form-control-sm search', 'id' => 'e', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Job Origin<br/>
                {{Form::select('o',$job_origin_drop_down,old('o'),['class'=>' form-control form-control-sm search', 'id' => 'o', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Job Status<br/>
                {{Form::select('s',$customer_preference_status_drop_down,old('s'),['class'=>' form-control form-control-sm search', 'id' => 's', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Rate Minimum<br/>
                {{Form::select('rm',$money_drop_down,old('rm'),['class'=>' form-control form-control-sm search', 'id' => 'rm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Rate Maximum<br/>
                {{Form::select('rx',$money_drop_down,old('rx'),['class'=>' form-control form-control-sm search', 'id' => 'rx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Salary Minimum<br/>
                {{Form::select('sm',$money_drop_down,old('sm'),['class'=>' form-control form-control-sm search', 'id' => 'sm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Salary Maximum<br/>
                {{Form::select('sx',$money_drop_down,old('sx'),['class'=>' form-control form-control-sm search', 'id' => 'sx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                <a href="{{route('jobspec.index')}}" type="submit" class="btn btn" style="margin-top: 23px; color: black !important; border: none !important;">Clear Filters</a>
            </div>
        </form>--}}
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('id', 'Preference Number')</th>
                    <th>@sortablelink('customer', 'Customer')</th>
                    <th>@sortablelink('profession', 'Profession')</th>
                    <th>@sortablelink('speciality', 'Speciality')</th>
                    <th>@sortablelink('skill', 'Skill')</th>
                    <th>@sortablelink('credit_check', 'Credit Check')</th>
                    <th>@sortablelink('criminal_record', 'Criminal Record')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                <?php $counter = 0; ?>
                @forelse($customer_preferences as $customer_preference)
                    <tr>
                        <td><a href="{{route('customerpreference.show',$customer_preference)}}">{{$customer_preference->id}}</a></td>
                        <td>{{$customer_preference->customer?->customer_name}}</td>
                        <td>{{$customer_preference->profession?->name}}</td>
                        <td>{{$customer_preference->speciality?->name}}</td>
                        <td>{{isset($skills[$counter])?$skills[$counter]:''}}</td>
                        <td>{{$customer_preference->credit_check == 1?'Yes':'No'}}</td>
                        <td>{{$customer_preference->criminal_record == 1?'Yes':'No'}}</td>
                        @if($can_update)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('customerpreference.edit',$customer_preference)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['customerpreference.destroy', $customer_preference],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                        <?php $counter++; ?>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $scoutings->appends(request()->except('page'))->links() }}--}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection