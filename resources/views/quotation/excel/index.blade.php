<table>
    <thead>
    <tr>
        <th>Customer</th>
        <th>Customer ref</th>
        <th>Start</th>
        <th>Duration</th>
        <th>Consultant</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($quotations as $quotation)
        <tr>
            <td>{{$quotation->customer?->customer_name}}</td>
            <td>{{$quotation->customer_ref}}</td>
            <td>{{$quotation->start_date}}</td>
            <td>{{$quotation->duration}}</td>
            <td>{{$quotation->consultant?->name()}}</td>
            <td>{{$quotation->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>