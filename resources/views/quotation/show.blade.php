@extends('adminlte.default')

@section('title')
    View Quotation
@endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('quotation.edit', $quotation[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Quotation</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
@forelse($quotation as $referrer)


        <table class="table table-bordered table-sm mt-3">
            <tbody>
            <tr>
                <th>Customer</th>
                <td>{{isset($referrer->customer)?$referrer->customer->customer_name:''}}</td>
                <th>Customer Ref</th>
                <td>{{$referrer->customer_ref}}</td>
            </tr>
            <tr>
                <th>Contact First Name</th>
                <td>{{$referrer->contact_firstname}}</td>
                <th>Contact Last Name</th>
                <td>{{$referrer->contact_firstname}}</td>
            </tr>
            <tr>
                <th>Phone</th>
                <td>{{$referrer->phone}}</td>
                <th>Cell</th>
                <td>{{$referrer->cell}}</td>
            </tr>
            <tr>
                <th>Fax</th>
                <td>{{$referrer->fax}}</td>
                <th>Email</th>
                <td>{{$referrer->email}}</td>
            </tr>
            <tr>
                <th>Postal Address Line 1</th>
                <td>{{$referrer->postal_address_line1}}</td>
                <th>Postal Address Line 2</th>
                <td>{{$referrer->postal_address_line2}}</td>
            </tr>
            <tr>
                <th>Postal Address Line 3</th>
                <td>{{$referrer->postal_address_line3}}</td>
                <th>Suburb</th>
                <td>{{isset($referrer->suburb)?$referrer->suburb->name:''}}</td>
            </tr>
            <tr>
                <th>State/Province</th>
                <td>{{$referrer->state_province_id}}</td>
                <th>Postal/Zip Code</th>
                <td>{{$referrer->postal_zipcode}}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{{isset($referrer->country)?$referrer->country->name:''}}</td>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th>Consultant</th>
                <td>{{isset($referrer->consultant)?$referrer->consultant->first_name.' '.$referrer->consultant->last_name:''}}</td>
                <th>Rate Per Hour</th>
                <td>{{$referrer->hour_rate}}</td>
            </tr>
            <tr>
                <th>Duration</th>
                <td>{{$referrer->duration}}</td>
                <th>Amount</th>
                <td>{{$referrer->amount}}</td>
            </tr>
            <tr>
                <th>Start Date</th>
                <td>{{$referrer->start_date}}</td>
                <th>End Date</th>
                <td>{{$referrer->end_date}}</td>
            </tr>
            <tr>
                <th>Scope Of Work</th>
                <td>{{$referrer->scope_of_work}}</td>
                <th>Exp </th>
                <td>{{$referrer->exp_id}}</td>
            </tr>
            <tr>
                <th>Terms</th>
                <td>{{isset($referrer->terms)?$referrer->terms->term:''}}</td>
                <th>Update </th>
                <td>{{$referrer->update_id}}</td>
            </tr>
            <tr>
                <th>Status</th>
                <td>{{isset($referrer->status)?$referrer->status->description:''}}</td>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
        @empty
        @endforelse
    </div>
@endsection
