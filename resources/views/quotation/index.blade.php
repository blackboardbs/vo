@extends('adminlte.default')
@section('title') Quotation @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('quotation.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Quotation</a>
        <x-export route="quotation.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>

            <div class="col-md-3" style="max-width: 20%;">
                <a href="{{route('quotation.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('customer.customer_name', 'Customer')</th>
                    <th>@sortablelink('customer_ref', 'Customer ref')</th>
                    <th>@sortablelink('start_date', 'Start')</th>
                    <th>@sortablelink('duration', 'Duration')</th>
                    <th>@sortablelink('consultant.first_name', 'Consultant')</th>
                    <th>@sortablelink('status.description','Status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($quotations as $quotation)
                    <tr>
                        <td><a href="{{route('quotation.show',$quotation)}}">{{$quotation->customer?->customer_name}}</a></td>
                        <td>{{$quotation->customer_ref}}</td>
                        <td>{{$quotation->start_date}}</td>
                        <td>{{$quotation->duration}}</td>
                        <td>{{$quotation->consultant?->name()}}</td>
                        <td>{{$quotation->status?->description}}</td>
                        <td>
                            <div class="d-flex">
                                <a href="{{route('quotation.edit',$quotation)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['quotation.destroy', $quotation],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No quotations match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $quotations->links() }}
        </div>
    </div>
@endsection
