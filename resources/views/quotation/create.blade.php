@extends('adminlte.default')
@section('title') Add Quotation @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createQuote')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('quotation.store'), 'method' => 'post','class'=>'mt-3', 'files' => true,'id'=>'createQuote'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Create a Quotation</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Quotation No:</th>
                    <td>
                        {{Form::text('id',$quote_nr,['class'=>'form-control form-control-sm  col-sm-4'. ($errors->has('id') ? ' is-invalid' : ''), 'disabled' => 'disabled', 'placeholder'=>'Quotation No'])}}
                        @foreach($errors->get('id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('customer_id',$customer_drop_down,null,['class'=>'form-control form-control-sm   col-sm-6'. ($errors->has('customer_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer Ref No:</th>
                    <td>{{Form::text('customer_ref',old('customer_ref'),['class'=>'form-control form-control-sm  col-sm-6'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Ref No'])}}
                        @foreach($errors->get('customer_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                <tr>
                <tr>
                    <th>Consultant:</th>
                    <td>{{Form::select('consultant_emp_id',$consultant_drop_down,null,['class'=>'form-control  form-control-sm col-sm-6'. ($errors->has('consultant_emp_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('consultant_emp_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Hr Rate:</th>
                    <td>
                        {{Form::text('hour_rate',old('hour_rate'),['class'=>'form-control form-control-sm col-sm-6'. ($errors->has('hour_rate') ? ' is-invalid' : ''),'placeholder'=>'Hr Rate'])}}
                        @foreach($errors->get('hour_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Duration:</th>
                    <td>
                        {{Form::text('duration',old('duration'),['class'=>'form-control form-control-sm col-sm-6'. ($errors->has('duration') ? ' is-invalid' : ''),'placeholder'=>'hours/day'])}}
                        @foreach($errors->get('duration') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Approx. R: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::text('amount',old('amount'),['class'=>'form-control form-control-sm col-sm-6'. ($errors->has('amount') ? ' is-invalid' : ''),'placeholder'=>'Approx. R'])}}
                        @foreach($errors->get('amount') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>{{Form::text('start_date',old('start_date'),['class'=>'datepicker form-control form-control-sm col-sm-3'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>End Date:</th>
                    <td>{{Form::text('end_date',old('end_date'),['class'=>'datepicker form-control form-control-sm col-sm-3'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                <tr>
                <tr>
                    <th>Scope of work: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::textarea('scope_of_work',old('scope_of_work'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('scope_of_work') ? ' is-invalid' : ''),'placeholder'=>'Scope of work'])}}
                        @foreach($errors->get('scope_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Select Terms</th>
                    <td colspan="3">
                        {{Form::select('terms_version',$terms_drop_down,$default_project_terms->id,['id'=>'terms_version', 'class'=>'form-control '. ($errors->has('terms_version') ? ' is-invalid' : ''), 'onchange' => 'changeTerms()'])}}
                    </td>
                </tr>
                <tr>
                    <th>Expenses travel:</th>
                    <td>{{Form::textarea('travel',isset($default_project_terms->exp_travel)?$default_project_terms->exp_travel:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('travel') ? ' is-invalid' : ''),'placeholder'=>'Expense travel', 'id' => 'travel'])}}
                        @foreach($errors->get('travel') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Parking:</th>
                    <td>{{Form::textarea('parking',isset($default_project_terms->exp_parking)? $default_project_terms->exp_parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('parking') ? ' is-invalid' : ''),'placeholder'=>'Expense parking','id'=>'parking'])}}
                        @foreach($errors->get('parking') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Car Rental:</th>
                    <td>{{Form::textarea('car_rental',isset($default_project_terms->exp_car_rental)?$default_project_terms->exp_car_rental:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('car_rental') ? ' is-invalid' : ''),'placeholder'=>'Car Rental', 'id' => 'car_rent'])}}
                        @foreach($errors->get('car_rental') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Flights:</th>
                    <td>{{Form::textarea('flights',isset($default_project_terms->exp_flights)?$default_project_terms->exp_flights:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('flights') ? ' is-invalid' : ''),'placeholder'=>'Flights', 'id' => 'flights'])}}
                        @foreach($errors->get('flights') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Other:</th>
                    <td>{{Form::textarea('other',isset($default_project_terms->exp_other)?$default_project_terms->exp_other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('other') ? ' is-invalid' : ''),'placeholder'=>'Other', 'id' => 'other'])}}
                        @foreach($errors->get('other') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Accomodation:</th>
                    <td>{{Form::textarea('accommodation',isset($default_project_terms->exp_accommodation)?$default_project_terms->exp_accommodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9','placeholder'=>'Accomodation', 'id' => 'accommodation'])}}
                        @foreach($errors->get('accommodation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Out of Town:</th>
                    <td>{{Form::textarea('out_of_town',isset($default_project_terms->exp_out_of_town)?$default_project_terms->exp_out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9','placeholder'=>'Out of town', 'id' => 'out_of_town'])}}
                        @foreach($errors->get('out_of_town') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Toll:</th>
                    <td>{{Form::textarea('toll',isset($default_project_terms->exp_toll)?$default_project_terms->exp_toll:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('toll') ? ' is-invalid' : ''),'placeholder'=>'Toll', 'id' => 'toll'])}}
                        @foreach($errors->get('toll') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Data:</th>
                    <td>{{Form::textarea('data',isset($default_project_terms->exp_data)?$default_project_terms->exp_data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-9'. ($errors->has('data') ? ' is-invalid' : ''),'placeholder'=>'Data', 'id' => 'data'])}}
                        @foreach($errors->get('data') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_drop_down,1,['class'=>'form-control form-control-sm col-sm-4 '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });

        function changeTerms(){
            var data = {
                terms_id: $("select[name=terms_version]").val()
            };
            axios.post('{{route('project.getterms')}}', data)
                .then(function (data) {
                    $('#travel').html(data['data'].terms.exp_travel);
                    $('#parking').html(data['data'].terms.exp_parking);
                    $('#car_rent').html(data['data'].terms.exp_car_rental);
                    $('#flights').html(data['data'].terms.exp_flights);
                    $('#accommodation').html(data['data'].terms.exp_accommodation);
                    $('#per_diem').html(data['data'].terms.exp_per_diem);
                    $('#out_of_town').html(data['data'].terms.exp_out_of_town);
                    $('#toll').html(data['data'].terms.exp_toll);
                    $('#data').html(data['data'].terms.exp_data);
                    $('#other').html(data['data'].terms.exp_other);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }
    </script>
@endsection
