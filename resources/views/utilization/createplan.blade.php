@extends('adminlte.default')

@section('title') Add Utilization @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('saveUtil')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li><a href="{{route('utilization.create','week')}}">Week</a></li>
            <li class="active"><a href="{{route('utilization.create','plan')}}">Range</a></li>
        </ul>
        <hr />
        <div class="table-responsive">
            @foreach($errors->all() as $error)
                {{$error}}<br>
            @endforeach
            {{Form::open(['url' => route('utilization.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off','id'=>'saveUtil'])}}
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Start Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('start_date',old('start_date'),['id'=>'start_date','class'=>'datepicker start_date form-control form-control-sm'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder' => 'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>End Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('end_date',old('end_date'),['class'=>'datepicker end_date form-control form-control-sm'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder' => 'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>No of Billable Resources: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('res_no',$no_resources,['class'=>'form-control form-control-sm'. ($errors->has('res_no') ? ' is-invalid' : ''),'placeholder'=>'No of Resources'])}}
                        @foreach($errors->get('res_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Weekly billable hours:</th>
                    <td>{{Form::text('wk_hours',$utilization_hours,['class'=>'form-control form-control-sm'. ($errors->has('wk_hours') ? ' is-invalid' : ''),'placeholder'=>'Weekly billable hours'])}}
                        @foreach($errors->get('wk_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>No of Cost Resources: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('cost_res_no',$no_cost_resources,['class'=>'form-control form-control-sm'. ($errors->has('cost_res_no') ? ' is-invalid' : ''),'placeholder'=>'No of Cost Resources'])}}
                        @foreach($errors->get('cost_res_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Weekly cost hours:</th>
                    <td>{{Form::text('cost_wk_hours',$utilization_cost_hours,['class'=>'form-control form-control-sm'. ($errors->has('cost_wk_hours') ? ' is-invalid' : ''),'placeholder'=>'Weekly cost hours'])}}
                        @foreach($errors->get('cost_wk_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown, \App\Enum\Status::ACTIVE->value,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_status'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        li.active {
            border-left: 1px solid #ddd !important;
            border-top: 1px solid #ddd !important;
            border-right: 1px solid #ddd !important;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function(){

        $('#start_date').datepicker({
            onSelect: function(dateText, datepicker) {
                var date = new Date(dateText);

                var currentWeekDay = date.getDay();
                if(currentWeekDay == 0) {
                    var lessDays = 6;
                } else {
                    var lessDays = currentWeekDay - 1;
                }
                var wkStart = new Date(new Date(date).setDate(date.getDate() - lessDays));
                var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));

                var month = wkEnd.getMonth()+2;

                $('.end_date').val(wkEnd.getFullYear()+'-'+(month < 10 ? '0'+month : month )+'-'+(wkEnd.getDate() < 10 ? '0'+wkEnd.getDate() : wkEnd.getDate() ));

            }
        });

    })
    </script>
@endsection
