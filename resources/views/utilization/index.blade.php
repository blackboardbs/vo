@extends('adminlte.default')

@section('title') Utilization @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
            <a href="{{route('utilization.create',['type' => 'week'])}}" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-plus"></i> Utilization</a>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('from',old('from'),['class'=>'datepicker form-control form-control-sm w-100 search', 'id' => 'from'])}}
                        <span>Between</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::text('to',old('to'),['class'=>'datepicker form-control form-control-sm w-100 search', 'id' => 'to'])}}
                <span>And</span>
                    </label>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <button type="reset" value="Reset" onclick="clearFilters()" class="btn btn-info w-100">Clear Filters</button>
            </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('id', '#')</th>
                <th>@sortablelink('yearwk', 'Start of Week')</th>
                <th>@sortablelink('yearwk', 'Yearwk')</th>
                <th>@sortablelink('res_no', 'No. of Resources')</th>
                <th>@sortablelink('wk_hours', 'Wk. Hours')</th>
                <th>@sortablelink('cost_res_no', 'No. of Cost Resources')</th>
                <th>@sortablelink('cost_wk_hours', 'Wk. Cost Hours')</th>
                <th>@sortablelink('status.description', 'Status')</th>
                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                <th class="last">Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @forelse($utilization as $_utilization)
                <tr>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->id}}</a></td>
                    <td>
                        @php
                            $date = new \Carbon\Carbon();
                            $date->setISODate(substr($_utilization->yearwk,0,4),substr($_utilization->yearwk,4,2));
                            echo '<a href="'.route('utilization.show',$_utilization->id).'">'.$date->startOfWeek()->format('Y-m-d').'</a>';
                        @endphp
                    </td>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->yearwk}}</a></td>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->res_no}}</a></td>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->wk_hours}}</a></td>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->cost_res_no}}</a></td>
                    <td><a href="{{route('utilization.show',$_utilization->id)}}">{{$_utilization->cost_wk_hours}}</a></td>
                    <td>{{$_utilization->status?->description}}</td>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                    <td>
                        <div class="d-flex">
                        <a href="{{route('utilization.edit',$_utilization->id)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                        {{ Form::open(['method' => 'DELETE','route' => ['utilization.destroy', $_utilization->id],'style'=>'display:inline','class'=>'delete']) }}
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        {{ Form::close() }}
                        </div>
                    </td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No referrers match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $utilization->firstItem() }} - {{ $utilization->lastItem() }} of {{ $utilization->total() }}
                    </td>
                    <td>
                        {{ $utilization->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
@endsection
