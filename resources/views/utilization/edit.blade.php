@extends('adminlte.default')

@section('title') Edit Utilization @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
    <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
    <a href="javascript:void(0)" onclick="saveForm('editUtil')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            {{Form::open(['url' => route('utilization.update',$utilization->id), 'method' => 'patch','class'=>'mt-3','id'=>'editUtil'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Yearwk: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('yearwk',$utilization_dropdown,$utilization->yearwk,['class'=>'form-control form-control-sm '. ($errors->has('yearwk') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('yearwk') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach </td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$utilization->status_id,['class'=>'form-control form-control-sm ','id'=>'dropdown_process_status'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>

                <tr>
                    <th>No of Billable Resources: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('res_no',$utilization->res_no,['class'=>'form-control form-control-sm'. ($errors->has('no_resources') ? ' is-invalid' : ''),'placeholder'=>'No of Resources'])}}
                        @foreach($errors->get('res_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Weekly billable hours: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('wk_hours',$utilization->wk_hours,['class'=>'form-control form-control-sm'. ($errors->has('wk_hours') ? ' is-invalid' : ''),'placeholder'=>'Weekly billable hours'])}}
                        @foreach($errors->get('wk_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>

                    <th>No of Cost Resources: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('cost_res_no',$utilization->cost_res_no,['class'=>'form-control form-control-sm'. ($errors->has('cost_res_no') ? ' is-invalid' : ''),'placeholder'=>'No of Cost Resources'])}}
                        @foreach($errors->get('cost_res_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Weekly cost hours: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('cost_wk_hours',$utilization->cost_wk_hours,['class'=>'form-control form-control-sm'. ($errors->has('cost_wk_hours') ? ' is-invalid' : ''),'placeholder'=>'Weekly cost hours'])}}
                        @foreach($errors->get('cost_wk_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}

        </div>
    </div>
@endsection