@extends('adminlte.default')

@section('title') View Utilization @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('utilization.edit', $utilization)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;">Edit</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('utilization.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm table-hover">
                <thead class="thead-light">

                </thead>
                <tbody>
                <tr>
                    <th>Yearwk:</th>
                    <td>{{$utilization->yearwk}}</td>
                    <th>No of Billable Resources:</th>
                    <td>{{$utilization->res_no}}</td>
                </tr>

                <tr>
                    <th>Weekly billable hours:</th>
                    <td>{{$utilization->wk_hours}}</td>
                    <th>No of Cost Resources:</th>
                    <td>{{$utilization->cost_res_no}}
                    </td>
                </tr>
                <tr>
                    <th>Weekly cost hours:</th>
                    <td>{{$utilization->cost_wk_hours}}</td>
                    <th>Status:</th>
                    <td>{{$utilization->status->description}}</td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}

        </div>
    </div>
@endsection
