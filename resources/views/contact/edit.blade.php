@extends('adminlte.default')

@section('title') Edit Contact @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('editContact')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('contact.update',$contact->id), 'method' => 'patch','class'=>'mt-3','id'=>'editContact'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>First Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('first_name', $contact->first_name, ['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''), 'id' => 'first_name', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('first_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Last Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('last_name', $contact->last_name, ['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''), 'id' => 'last_name', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('last_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contact Email <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('email', $contact->email, ['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''), 'id' => 'email', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Contact Number <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{ Form::text('contact_number', $contact->contact_number, ['class'=>'form-control form-control-sm'. ($errors->has('contact_number') ? ' is-invalid' : ''), 'id' => 'contact_number', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('contact_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contact Birthday</th>
                        <td>
                            {{ Form::text('birthday', $contact->birthday, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('birthday') ? ' is-invalid' : ''), 'id' => 'birthday', 'autocomplete' => 'off']) }}
                            @foreach($errors->get('birthday') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>
                            {{Form::select('status_id',$status_dropdown,$contact->status_id,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2"></td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {


        });
    </script>
@endsection
