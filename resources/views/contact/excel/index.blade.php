<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead>
        <tr class="btn-dark">
            <th>Full Name</th>
            <th>Email</th>
            <th>Contact Number</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{{$contact->full_name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->contact_number}}</td>
                <td>{{$contact->status?->description}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>