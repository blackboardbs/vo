@extends('adminlte.default')

@section('title') View Contact @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('contact.edit', $contact)}}" class="btn btn-success float-right ml-1" style="margin-top: -7px;">Edit</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>First Name </th>
                    <td>
                        {{ $contact->first_name }}
                    </td>
                    <th>Last Name </th>
                    <td>
                        {{ $contact->last_name }}
                    </td>
                </tr>
                <tr>
                    <th>Contact Email </th>
                    <td>
                        {{ $contact->email }}
                    </td>
                    <th>Contact Number </th>
                    <td>
                        {{ $contact->contact_number }}
                    </td>
                </tr>
                <tr>
                    <th>Contact Birthday </th>
                    <td>
                        {{ $contact->birthday  }}
                    </td>
                    <th>Status </th>
                    <td>
                        {{ $contact->status?->description }}
                    </td>
                    <td colspan="2"></td>
                </tr>
            </table>
        </div>
    </div>
@endsection