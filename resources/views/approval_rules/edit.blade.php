@extends('adminlte.default')

@section('title') Edit Approver Rule @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('editAppR')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('approvalroles.update', $approvalrole), 'method' => 'patch','class'=>'mt-3','autocomplete' => 'off','id'=>'editAppR'])}}
            <table class="table table-bordered table-sm">
                <tr>

                    <th>Approvers <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('approver_id', $approvers, $approvalrole->approver_id, ['class' => 'form-control form-control-sm '. ($errors->has('date_to') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('approver_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Substitute Approvers <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <div class="position-relative">
                            {{Form::text('substitute_approver', $approvalrole->substitute_approver, ['class' => 'form-control form-control-sm'. ($errors->has('substitute_approver_id') ? ' is-invalid' : ''), 'id' => 'substitute_approvers'])}}
                            @foreach($errors->get('substitute_approver_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::hidden('substitute_approver_id', $approvalrole->substitute_approver_id, ['id' => 'substitute_approver_id'])}}
                            <div class="position-absolute" style="top: 40; left: 0;width: 100%">
                                <ul class="list-group shadow-sm" id="substitutes" style="max-height: 320px;overflow-y: auto;width: 100%"></ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Start Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('date_from',$approvalrole->date_from,['id' => 'date_from', 'class'=>'date_from form-control form-control-sm datepicker'. ($errors->has('date_from') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('date_from') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>End Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('date_to',$approvalrole->date_to,['id' => 'date_to', 'class'=>'date_to form-control form-control-sm datepicker'. ($errors->has('date_to') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('date_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown, $approvalrole->status_id, ['id' => 'date_from', 'class'=>'date_from form-control form-control-sm datepicker'. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>

            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        #substitutes li:hover{
            cursor: pointer;
            background-color: #f9f9f9;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {


            $("#substitute_approvers").on('keyup', () =>{
                if ($("#substitute_approvers").val().length >= 3){
                    axios.get('/api/users?q='+$("#substitute_approvers").val())
                        .then(response => {
                            let usersList = "";
                            response.data.users.forEach((v, i) => {
                                let li = `<li class='list-group-item' onclick='selectedUser(${JSON.stringify(v)})'>${v.full_name}</li>`;
                                usersList += li;
                            })
                            $("#substitutes").html(usersList);
                            console.log(response)
                        })
                }
            });
        })

        function selectedUser(user){
            $("#substitute_approvers").val(user.full_name);
            $("#substitute_approver_id").val(user.id);
            $("#substitutes").on('click', 'li', () => {
                $("#substitutes").fadeOut()
            })
        }
    </script>
@endsection
