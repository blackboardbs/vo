@extends('adminlte.default')

@section('title') Approval Rules @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('approvalroles.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Approval Rule</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('approvalroles.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
            {{--Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>--}}
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                    <tr>
                        <th>Approver</th>
                        <th>Substitute</th>
                        <th>Date From</th>
                        <th>Date To</th>
                        <th>Created Date</th>
                        <th>Status</th>
                        <th class="last">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($approver_rules as $approver)
                        <tr>
                            <td><a href="{{route('approvalroles.show', $approver)}}">{{($approver->approver->first_name??null)." ".($approver->approver->last_name??null)}}</a></td>
                            <td>{{$approver->substitute_approver}}</td>
                            <td>{{$approver->date_from}}</td>
                            <td>{{$approver->date_to}}</td>
                            <td>{{$approver->created_at->toDateString()}}</td>
                            <td>{{$approver->status->description}}</td>
                            <td>
                                <div class="d-flex">
                                <a href="{{route('approvalroles.edit',$approver)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['approvalroles.destroy', $approver->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                </div>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="7">No approval rules available</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $approver_rules->firstItem() }} - {{ $approver_rules->lastItem() }} of {{ $approver_rules->total() }}
                        </td>
                        <td>
            {{ $approver_rules->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{$approver_rules->links()}}
        </div>
    </div>
@endsection