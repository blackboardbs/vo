@extends('adminlte.default')

@section('title') View Approval Role @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('approvalroles.edit', $approvalrole)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Approver</th>
                    <td>{{$approvalrole->substitute_approver}}</td>
                    <th>Substitute Approver</th>
                    <td>{{$approvalrole->substituteApprover->first_name??null}} {{$approvalrole->substituteApprover->last_name??null}}</td>
                </tr>
                <tr>
                    <th>Start Date</th>
                    <td>{{$approvalrole->date_from}}</td>
                    <th>End Date</th>
                    <td>{{$approvalrole->date_to}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$approvalrole->status->description??null}}</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>

        function goBack() {
            window.history.back();
        }

    </script>
@endsection
