@extends('adminlte.default')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-0">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Calendar</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <a href="{{route('board.index')}}" class="btn btn-sm btn-dark float-right" id="blackboard-calendar-event-btn" name="testbutton" style="margin-left: 10px;"><i class="fa fa-list"></i> View Boards</a>
                    <a href="{{route('action.create')}}" class="btn btn-sm btn-dark float-right" id="blackboard-calendar-event-btn" name="testbutton" style="margin-left: 10px;"><i class="fa fa-plus"></i> Calendar Action</a>
                    <button class="btn btn-sm btn-dark float-right" id="blackboard-calendar-event-btn" name="testbutton" data-toggle="modal" data-target="#blackboard-calendar-event-modal"><i class="fa fa-plus"></i> Calendar Event</button>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <hr />
            <div class="row">
                <div class="col-xs-12 col-sm-2 legend">
                    <div class="card">
                        <div class="card-header no-border">
                            <h2>Filter &amp; Legend</h2>
                        </div>
                        <div class="card-body">
                        <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_action_colour}}">Action</button>
                            <input type="checkbox" class="hidden calFilter" id="action" value="action" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'action') !== false ? 'checked="checked"' : '')}}>
                        </span>
                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_task_colour}}">Tasks</button>
                            <input type="checkbox" class="hidden calFilter" id="task" value="task" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'task') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_leave_colour}}">Leave</button>
                            <input type="checkbox" class="hidden calFilter" id="leave" value="leave" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'leave') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_assignment_colour}}">Assignments</button>
                            <input type="checkbox" class="hidden calFilter" id="assignment" value="assignment" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'assignment') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_assessment_colour}}">Assessment</button>
                            <input type="checkbox" class="hidden calFilter" id="assessment" value="assessment" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'assessment') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_anniversary_colour}}">Anniversaries</button>
                            <input type="checkbox" class="hidden calFilter" id="anniversary" value="anniversary" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'anniversary') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_events_colour}}">Events</button>
                            <input type="checkbox" class="hidden calFilter" id="cale" value="cale" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'cale') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_user_colour}}">Users</button>
                            <input type="checkbox" class="hidden calFilter" id="user" value="user" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'user') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_cinvoice_colour}}">Customer Invoices</button>
                            <input type="checkbox" class="hidden calFilter" id="cinvoice" value="cinvoice" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'cinvoice') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_vinvoice_colour}}">Vendor Invoices</button>
                            <input type="checkbox" class="hidden calFilter" id="vinvoice" value="vinvoice" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'vinvoice') !== false ? 'checked="checked"' : '')}}>
                        </span>

                        <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_medcert_colour}}">Medical Certificates</button>
                            <input type="checkbox" class="hidden calFilter" id="medcert" value="medcert" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'certificates') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <span class="button-checkbox">
                            <button type="button" class="btn btn-sm" data-color="{{$config?->calendar_public_holidays_colour}}">Public Holidays</button>
                            <input type="checkbox" class="hidden calFilter" id="holidays" value="holidays" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'holidays') !== false ? 'checked="checked"' : '')}}>
                        </span>

                            <input type="hidden" class="action_colour" value="{{$config?->calendar_action_colour}}" />
                            <input type="hidden" class="task_colour" value="{{$config?->calendar_task_colour}}" />
                            <input type="hidden" class="leave_colour" value="{{$config?->calendar_leave_colour}}" />
                            <input type="hidden" class="assignment_colour" value="{{$config?->calendar_assignment_colour}}" />
                            <input type="hidden" class="assessment_colour" value="{{$config?->calendar_assessment_colour}}" />
                            <input type="hidden" class="anniversary_colour" value="{{$config?->calendar_anniversary_colour}}" />
                            <input type="hidden" class="medcert_colour" value="{{$config?->calendar_medcert_colour}}" />
                            <input type="hidden" class="user_colour" value="{{$config?->calendar_user_colour}}" />
                            <input type="hidden" class="cinvoice_colour" value="{{$config?->calendar_cinvoice_colour}}" />
                            <input type="hidden" class="vinvoice_colour" value="{{$config?->calendar_vinvoice_colour}}" />
                            <input type="hidden" class="events_colour" value="{{$config?->calendar_events_colour}}" />
                            <input type="hidden" class="holidays_colour" value="{{$config?->calendar_public_holidays_colour}}" />
                        </div>
                        <div class="card-footer text-center">
                            <a href="javascript:void(0)" onclick="showLegend()"><i class="fas fa-caret-down legar"></i></a>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-10">

                    <div id="calendar">
                    </div>

                    <div id="blackboard-calendar-event-modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title text-left primecolor">Add Event</h3>
                                    <button id="blackboard-modal-close" type="button" class="close" data-dismiss="modal">×</button>
                                </div>
                                <div class="modal-body" style="overflow: hidden;">
                                    <div id="success-msg" class="blackboard-hide">
                                        <div class="alert alert-info alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Success!</strong> Event added successfully.
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        {{--<form method="POST" id="add_calendar_event">
                                            {{ csrf_field() }}
                                            <div class="form-group has-feedback">
                                                <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control form-control-sm" placeholder="Title">
                                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                <span class="text-danger">
                                                    <strong id="title-error"></strong>
                                                </span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" id="event_date" name="event_date" value="{{ date("Y-m-d") }}" class="datepicker form-control form-control-sm" placeholder="Event Date">
                                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                <span class="text-danger">
                                                    <strong id="start-date-error"></strong>
                                                </span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                {{Form::select('status',$status_drop_down,1,['class'=>'form-control form-control-sm'. ($errors->has('status') ? ' is-invalid' : ''),'id'=>'status'])}}
                                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                <span class="text-danger">
                                                    <strong id="status-error"></strong>
                                                </span>
                                            </div>
                                            --}}{{--<div class="form-group has-feedback">
                                                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
                                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                <span class="text-danger">
                                                    <strong id="email-error"></strong>
                                                </span>
                                            </div>--}}{{--
                                            <div class="form-group text-center">
                                                <button type="button" id="submitbtn" name="submitbtn" class="btn btn-sm btn-dark" onclick="storeEvent();">Submit</button>
                                            </div>
                                        </form>--}}
                                        {{Form::open(['url' => route('calendarevents.store'), 'method' => 'post','class'=>'mt-3', 'files' => true, 'autocomplete' => 'off'])}}
                                            <table class="table table-bordered table-sm mt-3">
                                                <tbody>
                                                <tr>
                                                    <th>Title:</th>
                                                    <td>{{Form::text('title', old('title'), ['class' => 'form-control form-control-sm'])}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Event Date:</th>
                                                    <td>{{Form::text('event_date',old('event_date'),['class'=>'form-control form-control-sm datepicker'. ($errors->has('event_date') ? ' is-invalid' : '')])}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Roles:</th>
                                                    <td>{{Form::select('role_ids[]',$user_role_drop_down,0,['class'=>'form-control form-control-sm '. ($errors->has('role_ids') ? ' is-invalid' : ''),'multiple', 'id' => 'role_ids'])}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Teams:</th>
                                                    <td>{{Form::select('team_ids[]',$team_drop_down,0,['class'=>'form-control form-control-sm '. ($errors->has('team_ids') ? ' is-invalid' : ''),'multiple', 'id' => 'team_ids'])}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Users:</th>
                                                    <td>{{Form::select('user_ids[]',$user_drop_down,0,['class'=>'form-control form-control-sm '. ($errors->has('user_ids') ? ' is-invalid' : ''),'multiple', 'id' => 'user_ids'])}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Projects:</th>
                                                    <td>{{Form::select('project_ids[]',$project_drop_down,0,['class'=>'form-control form-control-sm '. ($errors->has('project_ids') ? ' is-invalid' : ''),'multiple', 'id' => 'project_ids'])}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Details:</th>
                                                    <td>{{Form::textarea('details',old('details'),['class'=>'form-control form-control-sm'. ($errors->has('details') ? ' is-invalid' : ''), 'rows'=>3, 'id' => 'details'])}}</td>

                                                </tr>
                                                <tr>
                                                    <th>Document Name:</th>
                                                    <td id="file-wrapper">
                                                        {{Form::text('document_name[]',old('document_name'),['class'=>'form-control mb-2 form-control-sm document_name'. ($errors->has('document_name') ? ' is-invalid' : '')])}}
                                                        <input type="file" name="file[]"  class="form-control pl-0 form-control-sm file" multiple>
                                                        <span class="btn btn-sm btn-primary" id="add_file"><i class="fas fa-plus"></i> Add More</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Status:</th>
                                                    <td>{{Form::select('status_id',$status_drop_down,1,['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status'])}}</td>

                                                </tr>
                                                </tbody>
                                            </table>

                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        {{Form::close()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@section('extra-css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css'/>
    <style>
        /*.fc-widget-content:first-of-type, .fc-widget-header:first-of-type {
            border-left: 1px solid #dddddd !important;
        }

        .fc-widget-content:last-of-type, .fc-widget-header:last-of-type{
            border-right:1px solid #dddddd !important;
        }*/

        .fc button, .fc table, body .fc{
            font-size:0.95em !important;
        }
        .fc-day-number{
            font-size: 14px !important;
        }

        .ui-widget-header,.fc-prev-button,.fc-next-button,.fc-today-button,.fc-listDay-button,.fc-month-button,.fc-agendaWeek-button,.fc-agendaDay-button {
            border: 1px solid #1d2124;
            background: #23272b url(images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x;
            color: #FFFFFF;
            font-weight: bold;
        }

        .fc-toolbar{
            padding:1rem 0px;
        }

        .fc-toolbar h2,.card-header h2{
            font-size:18px;
            text-align: center;
        }

        .card-body{
            padding:0em 0.5em 0.5em;
        }

        .blackboard-hide{
            display: none;
        }

        .hidden{
            display:none;
        }
        .legend .btn{
            width:100%;
            text-align: left;
            margin-top:5px;
        }
        .btn-{{$config?->calendar_action_colour}}{
            background: #{{$config?->calendar_action_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_task_colour}}{
            background: #{{$config?->calendar_task_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_leave_colour}}{
            background: #{{$config?->calendar_leave_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_assignment_colour}}{
            background: #{{$config?->calendar_assignment_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_assessment_colour}}{
            background: #{{$config?->calendar_assessment_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_anniversary_colour}}{
            background: #{{$config?->calendar_anniversary_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_events_colour}}{
            background: #{{$config?->calendar_events_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_user_colour}}{
            background: #{{$config?->calendar_user_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_cinvoice_colour}}{
            background: #{{$config?->calendar_cinvoice_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_vinvoice_colour}}{
            background: #{{$config?->calendar_vinvoice_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_medcert_colour}}{
            background: #{{$config?->calendar_medcert_colour}};
            color:#FFFFFF;
        }

        .btn-{{$config?->calendar_public_holidays_colour}}{
            background: #{{$config?->calendar_public_holidays_colour}};
            color:#FFFFFF;
        }

        .card-footer{
            display: none;
        }

        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)
        {

            .fc-toolbar .fc-center {
                /*display: block;*/
                position: absolute;
                margin: 0px auto;
                text-align: center;
            }

            .fc-toolbar .fc-center h2{
                text-align: center;
            }

            .fc-today-button{
                display:none;
            }

            .fc-toolbar .fc-right,.fc-toolbar .fc-left{
                position: relative;
                margin-top:35px;
                top:0px;
            }

            #calendar{
                padding-bottom: 30px;
            }

            .card-body{
                display: none;
            }

            .card-footer{
                display: block;
            }

            .fc-scroller{
                height: auto !important;
            }
        }
    </style>
@endsection
@section('extra-js')

    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js'></script>
    <script>
        $(function () {

            $('#add_file').on('click', function () {
                $('.document_name').hide();
                $('.file').hide();
                $(this).before(
                    '<input type="text" name="document_name[]" class="form-control mb-2 form-control-sm document_name"/>' +
                    '<input type="file" name="file[]" class="form-control pl-0 form-control-sm file" multiple />'
                );
            })
        });
        $(document).ready(function() {

            $('.button-checkbox').each(function () {

                // Settings
                var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: 'far fa-check-square'
                        },
                        off: {
                            icon: 'far fa-square'
                        }
                    };

                // Event Handlers
                $button.on('click', function () {
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    updateDisplay();
                });
                $checkbox.on('change', function () {
                    updateDisplay();
                });

                // Actions
                function updateDisplay() {
                    var isChecked = $checkbox.is(':checked');

                    // Set the button's state
                    $button.data('state', (isChecked) ? "on" : "off");

                    // Set the button's icon
                    $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);

                    // Update the button's color
                    if (isChecked) {
                        $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                    }
                    else {
                        $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                    }
                }

                // Initialization
                function init() {

                    updateDisplay();

                    // Inject the icon if applicable
                    if ($button.find('.state-icon').length == 0) {
                        $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                    }
                }
                init();
            });

            calendar = $('#calendar').fullCalendar({
                themeSystem:'jquery-ui',
                defaultView: Cookies.get('fullCalendarCurrentView') || 'month',
                defaultDate: Cookies.get('fullCalendarCurrentDate') || null,
                eventRender: function(event, element, view) {
                    if (event.risk == "leave") {
                        element.css('background-color', '#'+ $(".leave_colour").val());
                        element.css('border-color', '#'+ $(".leave_colour").val());
                    }
                    if (event.risk == "task") {
                        element.css('background-color', '#'+ $(".task_colour").val());
                        element.css('border-color', '#'+ $(".task_colour").val());
                    }
                    if (event.risk == "action") {
                        element.css('background-color', '#'+ $(".action_colour").val());
                        element.css('border-color', '#'+ $(".action_colour").val());
                    }
                    if (event.risk == "assignment") {
                        element.css('background-color', '#'+ $(".assignment_colour").val());
                        element.css('border-color', '#'+ $(".assignment_colour").val());
                    }
                    if (event.risk == "assessment") {
                        element.css('background-color', '#'+ $(".assessment_colour").val());
                        element.css('border-color', '#'+ $(".assessment_colour").val());
                    }
                    if (event.risk == "anniversary") {
                        element.css('background-color', '#'+ $(".anniversary_colour").val());
                        element.css('border-color', '#'+ $(".anniversary_colour").val());
                    }
                    if (event.risk == "cale") {
                        element.css('background-color', '#'+ $(".events_colour").val());
                        element.css('border-color', '#'+ $(".events_colour").val());
                    }
                    if (event.risk == "vinvoice") {
                        element.css('background-color', '#'+ $(".vinvoice_colour").val());
                        element.css('border-color', '#'+ $(".vinvoice_colour").val());
                    }
                    if (event.risk == "cinvoice") {
                        element.css('background-color', '#'+ $(".cinvoice_colour").val());
                        element.css('border-color', '#'+ $(".cinvoice_colour").val());
                    }
                    if (event.risk == "medcert") {
                        element.css('background-color', '#'+ $(".medcert_colour").val());
                        element.css('border-color', '#'+ $(".medcert_colour").val());
                    }
                    if (event.risk == "user") {
                        element.css('background-color', '#'+ $(".user_colour").val());
                        element.css('border-color', '#'+ $(".user_colour").val());
                    }
                    if (event.risk == "holidays") {
                        element.css('background-color', '#'+ $(".holidays_colour").val());
                        element.css('border-color', '#'+ $(".holidays_colour").val());
                    }
                    element.css('color', '#FFFFFF');
                    return filter(event); // Only show if appropriate checkbox is checked
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'listDay,month,agendaWeek,agendaDay'
                },
                editable: false,
                events: get_event(),
                selectable: false,
                selectHelper: false,
                viewRender: function(view) {
                    Cookies.remove(view.name, { path: '' });
                    Cookies.remove(view.intervalStart.format(), { path: '' });
                    Cookies.set('fullCalendarCurrentView', view.name, {path: ''});
                    Cookies.set('fullCalendarCurrentDate', view.intervalStart.format(), {path: ''});
                },
                select: function (start, end, allDay) {
                    $("#start_date").val($.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss"));
                    $("#end_date").val($.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss"));
                    $("#blackboard-calendar-event-btn").click();
                    calendar.fullCalendar('unselect');
                    localStorage.setItem('Default_FullCalendar_View', start);
                },
                eventDrop: function (event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'edit-event.php',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                        type: "POST",
                        success: function (response) {
                            displayMessage("Updated Successfully");
                        }
                    });
                },
                eventClick: function (event) {
                    if(event.url){
                        localStorage.setItem('Default_FullCalendar_View', event.start);
                        window.location.href = event.url;
                        return false;
                    }
                },
            })


            $('input:checkbox.calFilter').on('change', function() {
                $('#calendar').fullCalendar('rerenderEvents');
            });
        });

        function filter(calEvent) {
            var vals = [];
            $('input:checkbox.calFilter:checked').each(function() {
                vals.push($(this).val());
            });
            return vals.indexOf(event.risk) !== -1;
        }

        function storeEvent(){
            //Clear form errors
            $('#title-error').html("");
            $('#email-error').html("");
            $('#start-date-error').html("");
            $('#end-date-error').html("");
            $('#submitbtn').html("Processing ...");
            $("#submitbtn").attr("disabled", "disabled");

            var post_data = {
                title: $("#title").val(),
                event_date: $("#event_date").val(),
                status: $("#status").val()
            };

            axios.post('{{route('calendarevents.store')}}', post_data)
                .then(function (response) {
                    if(response.data.errors) {
                        if(response.data.errors.title){
                            $( '#title-error' ).html( response.data.errors.title[0] );
                        }
                        if(response.data.errors.password){
                            $( '#event-date-error' ).html( data.errors.event_date[0] );
                        }
                    }
                    if(response.data.success) {
                        $('#success-msg').removeClass('blackboard-hide');
                        $( '#title' ).val("");
                        $( '#event_date' ).val("");
                        $( '#status' ).val("1");

                        setTimeout(function() {
                            $("#blackboard-modal-close").click();
                            $('#success-msg').addClass('blackboard-hide');
                        }, 2000);
                        location.reload();
                        calendar.fullCalendar('renderEvent',
                            {
                                id: response.data.id,
                                title: response.data.title,
                                start: response.data.event_date,
                                end: response.data.event_date,
                                allDay: true
                            },
                            true
                        );
                    }
                    enableSubmitBtn();
                })
                .catch(function () {
                    alert("Error: There was a problem with this request.");
                    enableSubmitBtn();
                });



        }

        function enableSubmitBtn(){
            $('#submitbtn').html("Submit");
            $("#submitbtn").removeAttr("disabled", "disabled");
        }

        function filter(event) {
            var vals = [];
            $('input:checkbox.calFilter:checked').each(function() {
                vals.push($(this).val());
            });
            return vals.indexOf(event.risk) !== -1;
        }

        function get_event() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: "get_events",
                data: {   },
                success: function(event)
                {
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', event);
                    $('#calendar').fullCalendar('rerenderEvents');
                }
            });
        }

        function showLegend(){
            $('.card-body').toggle('slow');
        }
    </script>
@endsection
