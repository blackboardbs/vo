@extends('adminlte.default')

@section('title') Edit Logbook @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($logbook as $result)
            {{Form::open(['url' => route('logbook.update',$result), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm table-hover">
                <tr>
                    <th>Date <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('log_date',$result->log_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('log_date') ? ' is-invalid' : ''),'placeholder' => 'Log Date'])}}
                        @foreach($errors->get('log_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Resource <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('resource',$resource,$result->emp_id,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('resource') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td colspan="3">{{Form::text('description',$result->description,['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Opening km</th>
                    <td>{{Form::text('opening_km',$result->open_reading,['class'=>'form-control form-control-sm'. ($errors->has('opening_km') ? ' is-invalid' : ''),'placeholder'=>'Opening km'])}}
                        @foreach($errors->get('opening_km') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>km</th>
                    <td>{{Form::text('km',$result->km,['class'=>'form-control form-control-sm'. ($errors->has('km') ? ' is-invalid' : ''),'placeholder'=>'km'])}}
                        @foreach($errors->get('km') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fuel Cost</th>
                    <td>{{Form::text('fuel_cost',$result->fuel_cost,['class'=>'form-control form-control-sm'. ($errors->has('fuel_cost') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                        @foreach($errors->get('fuel_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Maintenance Cost</th>
                    <td>{{Form::text('maint_cost',$result->maint_cost,['class'=>'form-control form-control-sm'. ($errors->has('maint_cost') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                        @foreach($errors->get('maint_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$sidebar_process_statuses,$result->status_id,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            {{Form::close()}}
                @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection