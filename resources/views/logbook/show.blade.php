@extends('adminlte.default')

@section('title') View Logbook @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($logbook as $result)
            <table class="table table-bordered table-sm table-hover">
                <tr>
                    <th>Date</th>
                    <td>{{$result->log_date}}</td>
                    <th>Resource</th>
                    <td>{{$result->resource->emp_firstname}} {{$result->resource->emp_lastname}}</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td colspan="3">{{$result->description}}</td>
                </tr>
                <tr>
                    <th>Opening km</th>
                    <td>{{$result->open_reading}}</td>
                    <th>km</th>
                    <td>{{$result->km}}</td>
                </tr>
                <tr>
                    <th>Fuel Cost</th>
                    <td>{{$result->fuel_cost}}</td>
                    <th>Maintenance Cost</th>
                    <td>{{$result->maint_cost}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$result->status->description}}</td>
                    <th></th>
                    <td></td>
                </tr>
            </table>
            @endforeach
        </div>
    </div>
@endsection
