@extends('adminlte.default')

@section('title') Logbook @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('logbook.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Logbook</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('logbook.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('log_date','Date')</th>
                    <th>@sortablelink('description','Description')</th>
                    <th>@sortablelink('open_reading','Open km')</th>
                    <th>@sortablelink('km','km')</th>
                    <th>@sortablelink('fuel_cost','Fuel')</th>
                    <th>@sortablelink('maint_cost','Maint')</th>
                    <th>@sortablelink('status_id','Status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($logbook as $referrer)
                    <tr>
                        <td><a href="{{route('logbook.show',$referrer)}}">{{$referrer->log_date}}</a></td>
                        <td>{{$referrer->description}}</td>
                        <td>{{$referrer->open_reading}}</td>
                        <td>{{$referrer->km}}</td>
                        <td>{{$referrer->fuel_cost}}</td>
                        <td>{{$referrer->maint_cost}}</td>
                        <td>{{$referrer->status?->description}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('logbook.edit',$referrer)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['logbook.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No logbook entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $logbook->links() }}
        </div>
    </div>
@endsection
