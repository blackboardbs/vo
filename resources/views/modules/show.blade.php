@extends('adminlte.default')
@section('title') View {{$module->display_name}} Permissions <i class="far fa-question-circle" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="{{$module->help_text}}"></i> @endsection
@section('header')
<div class="container-fluid container-title">
<h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="{{route('module.edit', $module)}}" class="btn btn-success float-right ml-1" style="margin-left: 5px;">Edit</a>
    </div>
</div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive mt-3">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Role Name</th>
                    <th>List of all records <i class="far fa-question-circle" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="Allows a user to see a list of all records and options to either Delete, Edit or Create records based on the Permission."></i></th>
                    <th>List of own and team records <i class="far fa-question-circle" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="Allows a user to see a list of his own records and records of team members, if the user is a manager of a team and options to either Delete, Edit or Create records based on the Permission."></i></th>
                    <th>All records <i class="far fa-question-circle" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="Allow the user to edit, view, create or delete a selected record for any user."></i></th>
                    <th>Own and team records <i class="far fa-question-circle" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="Allow the user to edit, view, create or delete a selected record for records belonging to the user or the user’s team members."></i></th>
                </tr>
                </thead>
                <tbody>
                    @forelse($roles as $role)
                    <tr>
                        <td>{{$role->display_name}}</td>
                        <td>
                            {{Form::select('list_all_records_'.$role->id, $role_abilities, isset($role->simplepermissions->list_all_records)?$role->simplepermissions->list_all_records:4,['class'=>' form-control form-control-sm search', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::select('list_own_and_team_records_'.$role->id, $role_abilities, isset($role->simplepermissions->list_own_and_team_records)?$role->simplepermissions->list_own_and_team_records:4,['class'=>' form-control form-control-sm search', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::select('show_all_records_'.$role->id, $role_abilities, isset($role->simplepermissions->show_all_records)?$role->simplepermissions->show_all_records:4,['class'=>' form-control form-control-sm search', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                        <td>
                            {{Form::select('show_own_and_team_records_'.$role->id, $role_abilities, isset($role->simplepermissions->show_own_and_team_records)?$role->simplepermissions->show_own_and_team_records:4,['class'=>' form-control form-control-sm search', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="100%" class="text-center">No Roles match those criteria.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
        $(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })
        })
    </script>
@endsection
