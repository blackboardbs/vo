@extends('adminlte.default')
@section('title') {{$module->display_name}} Permissions @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            {{Form::open(['url' => route('module.update',$module->id), 'method' => 'patch','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Role Name</th>
                    <th>List of all records</th>
                    <th>List of own and team records</th>
                    <th>All records</th>
                    <th>Own and team records</th>
                </tr>
                </thead>
                <tbody>
                @forelse($roles as $role)
                    <tr>
                        <td>{{$role->display_name}}</td>
                        <td>
                            {{Form::select('list_all_records_'.$role->id, $role_abilities, isset($role->simplepermissions->list_all_records)?$role->simplepermissions->list_all_records:4,['class'=>' form-control form-control-sm', 'style'=>'width: 100%;'])}}
                        </td>
                        <td>
                            {{Form::select('list_own_and_team_records_'.$role->id, $role_abilities, isset($role->simplepermissions->list_own_and_team_records)?$role->simplepermissions->list_own_and_team_records:4,['class'=>' form-control form-control-sm', 'style'=>'width: 100%;'])}}
                        </td>
                        <td>
                            {{Form::select('show_all_records_'.$role->id, $role_abilities, isset($role->simplepermissions->show_all_records)?$role->simplepermissions->show_all_records:4,['class'=>' form-control form-control-sm', 'style'=>'width: 100%;'])}}
                        </td>
                        <td>
                            {{Form::select('show_own_and_team_records_'.$role->id, $role_abilities, isset($role->simplepermissions->show_own_and_team_records)?$role->simplepermissions->show_own_and_team_records:4,['class'=>' form-control form-control-sm', 'style'=>'width: 100%;'])}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No Roles match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection