@extends('adminlte.default')
@section('title') System Modules Permissions @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('module',$module_dropdowns,old('module'),['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                        <span>System Modules</span>
                    </label>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>#</th>
                    <th>@sortablelink('display_name', 'Name')</th>
                    <th>@sortablelink('status.description','Status')</th>
                    <th class="last">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($system_modules as $system_module)
                    <tr>
                        <td>{{$system_module->id}}</td>
                        <td><a href="{{route('module.show', $system_module)}}">{{$system_module->display_name}}</a></td>
                        <td>{{$system_module->status?->description}}</td>
                        <td><a href="{{route('module.edit',$system_module)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No modules match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $system_modules->firstItem() }} - {{ $system_modules->lastItem() }} of {{ $system_modules->total() }}
                        </td>
                        <td>
                            {{ $system_modules->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection