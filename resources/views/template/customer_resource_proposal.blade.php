<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Akckowledgement of receipt of Asset</title>
    <!-- <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'> -->
    <style>
        body{
            margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}
        }

        .signature {
            font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
            color: #000000;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }

    </style>
</head>
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container">
        <table>
            <tbody>
            <tr>
                <td style="width: 70%;">&nbsp;</td>
                <td>
                    <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                </td>
            </tr>
            </tbody>
        </table>
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->postal_address_line1)?$customer->postal_address_line1:''}}</td>
            </tr>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->postal_address_line2)?$customer->postal_address_line2:''}}</td>
            </tr>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->postal_address_line3)?$customer->postal_address_line3:''}}</td>
            </tr>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->street_city_suburb)?$customer->street_city_suburb:''}}</td>
            </tr>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->postal_zipcode)?$customer->postal_zipcode:''}}</td>
            </tr>
            <tr>
                <td style="width: 70%"></td><td>{{isset($customer->email)?$customer->email:''}}</td>
            </tr>
            </tbody>
        </table>
        <p>{{isset($quotation_subject)?$quotation_subject:''}}</p>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>Company:</td><td>{{isset($customer->customer_name)?$customer->customer_name:''}}</td><td>Date:</td><td>{{isset($quotation->created_at)?$quotation->created_at:''}}</td>
            </tr>
            <tr>
                <td>Attention:</td><td>{{(isset($customer->contact_firstname)?$customer->contact_firstname:'').' '.(isset($customer->contact_lastname)?$customer->contact_lastname:'')}}</td><td>Ref #:</td><td>{{isset($quotation->customer_ref)?$quotation->customer_ref:''}}</td>
            </tr>
            <tr>
                <td>Email:</td><td>{{isset($customer->email)?$customer->email:''}}</td><td>Prepared By:</td><td>{{isset($prepared_by)?$prepared_by:''}}</td>
            </tr>
            <tr>
                <td>Reference:</td><td>{{isset($quotation_subject)?$quotation_subject:''}}</td><td>Telephone:</td><td>{{isset($customer->phone)?$customer->phone:''}}</td>
            </tr>
            </tbody>
        </table>
        <p>Thank you for giving me the opportunity to provide you with this proposal.</p>
        <p>Blackboard BI will provide a {POSITION}} resource for {{isset($quotation->duration)?$quotation->duration:''}} to {{isset($location)?$locaiton:''}}, for {{isset($quotation_subject)?$quotation_subject:''}}. The resource will be charged at {{isset($quotation->hour_rate)?$quotation->hour_rate:''}} per hour excluding VAT. The estimated value is {{isset($quotation->amount)?$quotation->amount:''}}. The resource will be available 8 hours per day on normal business weekdays. The proposal is based on a start date of {{isset($quotation->start_date)?$quotation->start_date:''}} and end date {{isset($quotation->end_date)?$quotation->end_date:''}}. Provision will be made for annual leave during the cause of the contract period and would be pre-arranged with the customer.</p>
        <p>The proposed resource is {{isset($consultant_name)?$consultant_name:''}} Please find his CV attached to this proposal.</p>
        <p>{{isset($additional_text)?$additional_text:''}}</p>
        <p>Please contact me for any queries.</p>
        <p>Yours Sincerely</p>
        <p>{{isset($signature)?$signature:''}}</p>
        <p>{{isset($position)?$position:''}}</p>
    </div>
</div>
</body>
</html>
