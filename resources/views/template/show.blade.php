@extends('adminlte.default')
@section('title') Work Breakdown Structure @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('project.update', $project->id), 'method' => 'put','class'=>'mt-3', 'files' => true])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Project</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project No:</th>
                    <td>
                        {{Form::text('name',$wbs_nr,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>Created Date</th>
                    <td>
                        {{Form::text('ref',$project->created_at,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>{{Form::select('customer_id',$customer_drop_down,$project->customer_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                    <th>Customer Ref.:</th>
                    <td>{{Form::text('customer_ref',$project->customer_ref,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled','placeholder'=>'Customer Ref No'])}}
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$project->name,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',$project->ref,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                <tr>
                    <th>Own Project Lead:</th>
                    <td>{{Form::select('manager_id',$consultant_drop_down,$project->manager_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                    <th>Company:</th>
                    <td>{{Form::select('company_id',$customer_drop_down,$project->company_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Start Date:</th>
                    <td>
                        {{Form::text('name',$project->start_date,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>End Date</th>
                    <td>
                        {{Form::text('ref',$project->end_date,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <td style='width: 33%'><a href="{{route('project.copy', $project->id)}}">[Copy Project]</a></td>
                        <td><a href="{{route('project.edit', $project)}}">[Edit Project]</a></td>
                        <td style='width: 33%'>[Print Project]</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Consultants Assigned</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            @forelse($consultants_selected as $key => $value)
                                @if(!in_array($key, $consultant_have_assignments))
                                    <div class="col-sm-3"><a class="btn btn-dark btn-sm" href="{{route('assignment.add', Array($project->id, $key))}}"><i class="fa fa-plus"></i> {{$value}}</a></div>
                                @else
                                    <div class="col-sm-3">{{$value}}</div>
                                @endif
                            @empty
                                No Consultants selected
                            @endforelse
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Billable Expenses</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Travel</th>
                    <td>{{Form::textarea('travel',isset($expense->travel)?$expense->travel:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Point-to-point shuttle service can be arranged at the discretion of the client in order to avert airport parking expenses. If own transport is used then millage will be charged at R3.30 per km from Office to destination.'])}}
                    </td>
                    <th>Parking</th>
                    <td>{{Form::textarea('parking',isset($expense->parking)?$expense->parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred, including Airport parking.'])}}
                    </td>
                </tr>
                <tr>
                    <th>Accommodation</th>
                    <td>{{Form::textarea('accommodation',isset($expense->accommodation)?$expense->accommodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'To be arranged by the client.'])}}
                    </td>
                    <th>Per Diem</th>
                    <td>{{Form::textarea('per_diem',isset($expense->per_diem)?$expense->per_diem:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>''])}}
                    </td>
                </tr>
                <tr>
                    <th>Out of Town Allowance</th>
                    <td>{{Form::textarea('out_of_town',isset($expense->out_of_town)?$expense->out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'R300 per day in South Africa and US$ 80 per day internationally (including Zimbabwe, Botswana).'])}}
                    </td>
                    <th>Data</th>
                    <td>{{Form::textarea('data',isset($expense->data)?$expense->data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A'])}}
                    </td>
                </tr>
                <tr>
                    <th>Other</th>
                    <td colspan="3">{{Form::textarea('other',isset($expense->other)?$expense->other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.'])}}
                    </td>
                </tr>
                <tr>
                    <th>Billing Note</th>
                    <td colspan="3">
                        {{Form::textarea('other',isset($expense->notes)?$expense->notes:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.'])}}
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<button type="cancel" class="btn btn-sm">Cancel</button></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
