@extends('adminlte.default')
@section('title') Template @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('template.update', $template->id), 'method' => 'put','class'=>'mt-3', 'files' => true])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Edit a Template</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Template Name:</th>
                    <td>
                        {{Form::text('name',$template->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Templage Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Template Type:</th>
                    <td>
                        {{Form::select('template_type_id',$template_type_drop_down,$template->template_type_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('template_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Template Type', 'onchange'=>'getVariables()'])}}
                        @foreach($errors->get('template_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                    <td colspan="2">
                        <div style="font-style: italic;">Copy and paste the following fields:</div>
                        <div id="variables" name="variables"></div>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Content:</th>
                </tr>
                <tr>
                    <td colspan="2">
                        {{Form::textarea('content',$template->content,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('content') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('content') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<button type="cancel" class="btn btn-sm">Cancel</button></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script src="{{asset('third-party/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');
            getVariables();
        });

        tinymce.init({
            selector: 'textarea',
            path_absolute : "/",
            relative_urls: false,
            convert_urls : false,
            paste_data_images: true,
            height: 500,
            branding: false,
            theme: 'modern',
            plugins: 'paste print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            //plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | image media |removeformat',
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '/imageupload',
            file_picker_types: 'image',
            external_filemanager_path:"{{url('tinymce/filemanager')}}/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "{{url('tinymce')}}/filemanager/plugin.min.js"}
        });

        function getVariables(){
            var template_id = $("select[name=template_type_id]").val();
            axios.post('../'+template_id+'/getvariables', {})
            .then(function (data) {
                $('#variables').html(data['data'].template_fields);
            })
            .catch(function () {
                console.log("An Error occurred!!!");
            });
        }

    </script>
@endsection