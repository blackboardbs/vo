@extends('adminlte.default')
@section('title') Template @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('template.update', $template->id), 'method' => 'put','class'=>'mt-3', 'files' => true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Edit a Template</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Template Name:</th>
                    <td>
                        {{Form::text('name',$template->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Templage Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Template Type:</th>
                    <td>
                        {{Form::select('template_type_id',$template_type_drop_down,$template->template_type_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('template_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Template Type', 'onchange'=>'getVariables()'])}}
                        @foreach($errors->get('template_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                    <td colspan="2">
                        <div style="font-style: italic;">Copy and paste the following fields:</div>
                        <div id="variables" name="variables"></div>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Content:</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea id="area_editor" name="area_editor">{{$template->content}}</textarea>
                        @foreach($errors->get('area_editor') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="{{asset('jodit-3.2.24/build/jodit.min.css')}}"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,700,700i" rel="stylesheet">
@endsection
@section('extra-js')

    <script>
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');
        });

        function getVariables(){
            var template_id = $("select[name=template_type_id]").val();
            axios.post('../'+template_id+'/getvariables', {})
                .then(function (data) {
                    $('#variables').html(data['data'].template_fields);
                })
                .catch(function () {
                    console.log("An Error occurred!!!");
                });
        }

    </script>


    <script src="{{asset('jodit-3.2.24/build/jodit.min.js')}}"></script>
    <script src="{{asset('jodit-3.2.24/examples/assets/prism.js')}}"></script>
    <script>
        var editor = new Jodit('#area_editor', {
            textIcons: false,
            iframe: false,
            iframeStyle: '*,.jodit_wysiwyg {color:red;}',
            height: 300,
            defaultMode: Jodit.MODE_WYSIWYG,
            observer: {
                timeout: 100
            },
            uploader: {
                url: ''
            },
            filebrowser: {
                // buttons: ['list', 'tiles', 'sort'],
                ajax: {
                    url: ''
                }
            },
            commandToHotkeys: {
                'openreplacedialog': 'ctrl+p'
            }
            // buttons: ['symbol'],
            // disablePlugins: 'hotkeys,mobile'
        });
    </script>
@endsection