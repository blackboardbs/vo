<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Assessment</title>
    <style>
        body{
            margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}
        }

        .signature {
            font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
            color: #000000;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }

        .grey_heading{
            background-color: rgba(142, 144, 140, 0.33);
            font-weight: bold;
            border-top: 1px solid transparent;
        }

    </style>
</head>
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="width: 50%;">
                    <img style="width:80%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                </td>
                <td style="font-weight: bold; vertical-align: bottom;">
                    <h2>Assessment</h2>
                </td>
            </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%; border: 2px solid #ffffff;">
            <tbody>
            <tr><td style="width: 25%;">Name</td><td style="width: 25%;">{{(isset($resource->first_name)?$resource->first_name:'').' '.(isset($resource->last_name)?$resource->last_name:'')}}</td><td style="width: 25%;">Emp No</td><td style="width: 25%;">{{isset($resource->id)?$resource->id:''}}</td></tr>
            <tr><td>Position</td><td></td><td></td><td></td></tr>
            <tr><td>Assessment Date</td><td>{{isset($assessment_header->assessment_date)?$assessment_header->assessment_date:''}}</td><td>Next Assessment</td><td>{{isset($assessment_header->next_assessment)?$assessment_header->next_assessment:''}}</td></tr>
            <tr><td>Assessment Period</td><td>{{isset($assessment_header->assessment_period_start)?$assessment_header->assessment_period_start:''}}</td><td>To</td><td>{{isset($assessment_header->assessment_period_end)?$assessment_header->assessment_period_end:''}}</td></tr>
            <tr><td>Client/Project</td><td>{{isset($customer->customer_name)?$customer->customer_name:''}}</td><td>Assessed By</td><td>{{(isset($assessed_by->first_name)?$assessed_by->first_name:'').' '.(isset($assessed_by->last_name)?$assessed_by->last_name:'')}}</td></tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="5" style="width: 100%;">Previous Plan</td></tr>
            <tr><td class="grey_heading" style="width: 4%;">#</td><td class="grey_heading" style="width: 24%;">Task</td><td class="grey_heading" style="width: 24%;">Competency Level *</td><td class="grey_heading" style="width: 24%;">Comfort Level *</td><td class="grey_heading" style="width: 24%;">Notes</td></tr>
            <tr><td style="width: 4%;">&nbsp;</td><td style="width: 24%;">&nbsp;</td><td style="width: 24%;"></td><td style="width: 24%;"></td><td style="width: 24%;"></td></tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="5" style="width: 100%;">This Assessment Period</td></tr>
            <tr><td class="grey_heading" style="width: 4%;">#</td><td class="grey_heading" style="width: 24%;">Task</td><td class="grey_heading" style="width: 24%;">Competency Level *</td><td class="grey_heading" style="width: 24%;">Comfort Level *</td><td class="grey_heading" style="width: 24%;">Notes</td></tr>
            @php $counter = 1; @endphp
            @foreach($this_assessment_period_activities as $this_assessment_period_activity)
                <tr><td style="width: 4%;">{{$counter}}</td><td style="width: 24%;">{{$this_assessment_period_activity->assessment_task_description}}</td><td style="width: 24%;">{{$this_assessment_period_activity->competency_level}}</td><td style="width: 24%;">{{$this_assessment_period_activity->comfort_level}}</td><td style="width: 24%;">{{$this_assessment_period_activity->notes}}</td></tr>
                @php $counter++; @endphp
            @endforeach
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="3" style="width: 100%;">Next Assessment Period</td></tr>
            <tr><td class="grey_heading" style="width: 4%;">#</td><td class="grey_heading" style="width: 36%;">Task</td><td class="grey_heading" style="width: 60%;">Notes</td></tr>
            @php $counter = 1; @endphp
            @foreach($next_assessment_period_activities as $next_assessment_period_activity)
                <tr><td style="width: 4%;">{{$counter}}</td><td style="width: 36%;">{{$next_assessment_period_activity->assessment_task_description}}</td><td style="width: 60%;">{{$next_assessment_period_activity->notes}}</td></tr>
                @php $counter++; @endphp
            @endforeach
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="2" style="width: 100%;">Assessment Concerns</td></tr>
            <tr><td class="grey_heading" style="width: 4%;">#</td><td class="grey_heading" style="width: 96%;">Concerns/Arear of improvements</td></tr>
            @php $counter = 1; @endphp
            @foreach($assessment_concerns as $assessment_concern)
                <tr><td style="width: 4%;">{{$counter}}</td><td style="width: 96%;">{{$assessment_concern->notes}}</td></tr>
                @php $counter++; @endphp
            @endforeach
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="2" style="width: 100%;">General Notes</td></tr>
            <tr><td class="grey_heading" style="width: 4%;">#</td><td class="grey_heading" style="width: 96%;">Concerns/Arear of improvements</td></tr>
            @php $counter = 1; @endphp
            @foreach($assessment_notes as $assessment_note)
                <tr><td style="width: 4%;">{{$counter}}</td><td style="width: 96%;">{{$assessment_note->notes}}</td></tr>
                @php $counter++; @endphp
            @endforeach
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="6" style="width: 100%;">Assessment Measures</td></tr>
            <tr><td colspan="2" style="width: 45%; font-weight: bold;">Professional Behaviour</td><td style="width: 10%; font-weight: bold;">Always</td><td style="width: 10%; font-weight: bold;">Sometimes</td><td style="width: 10%; font-weight: bold;">Never</td><td style="width: 25%; font-weight: bold;">Notes</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">On time</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Dress appropriate, personal hygiene</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Professional conduct</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td colspan="2" style="width: 45%; font-weight: bold;">Working behaviour</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Complete tasks on time and in full</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Works in Team when required</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Knowledge sharing</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Timesheets on time</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            <tr><td style="width: 20%;">&nbsp;</td><td style="width: 25%;">Respect company, customer and colleagues</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 10%;">&nbsp;</td><td style="width: 25%;">&nbsp;</td></tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%;">
            <tbody>
            <tr><td class="grey_heading" colspan="4" style="width: 100%;">Acceptance</td></tr>
            <tr><td colspan="4" style="width: 100%;">&nbsp;</td></tr>
            <tr><td style="width: 30%; border: 1px solid grey; height: 70px;"></td><td style="width: 20%; border-top: 1px solid transparent;">&nbsp;</td><td style="width: 20%; border-top: 1px solid transparent;"></td><td style="width: 30%; border: 1px solid black;">&nbsp;</td></tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
