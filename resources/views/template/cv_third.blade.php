<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>CURRICULUM VITAE</title>
    <!-- <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'> -->
    <style>
        body{
            margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}
        }

        .signature {
            font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
            color: #000000;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }

        .keep-together {
            page-break-inside: avoid;
        }

        .break-before {
            page-break-before: always;
        }

        .break-after {
            page-break-after: always;
        }

    </style>
</head>
<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        <div class="container">
            <table>
                <tbody>
                    <tr>
                        <td style="width: 70%;">&nbsp;</td>
                        <td>
                            <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp</p>
            <h1 style="text-align: center; margin-top: 270px;">CURRICULUM VITAE</h1>
            <h2 style="text-align: center;">Candidate {{$cv->id}}</h2>
            <h3 style="text-align: center;">{{$main_skill}}</h3>
            <h3 style="text-align: center;">{{$main_qualification}}</h3>
            <p class="break-after">&nbsp;</p>
            <p></p>
            <table>
                <tbody>
                <tr>
                    <td style="width: 70%;">&nbsp;</td>
                    <td>
                        <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                    </td>
                </tr>
                </tbody>
            </table>
            <p style="text-align: left; font-size: 18px;">PERSONAL DETAILS</p>
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <th style="width: 33%;">Surname</th>
                        <td style="width: 33%;">

                        </td>
                        <td style="width: 34%;">&nbsp;</td>
                    </tr>
                <tr>
                    <th>Name</th>
                    <td>

                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>Nationality</th>
                    <td>
                        {{$cv->nationality}}
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>Language Proficiency </th>
                    <td>
                        {{$cv->home_language}}, {{$cv->other_language}}
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>Birthdate </th>
                    <td>
                        {{$cv->date_of_birth}}
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p style="font-size: 18px;">OVERVIEW</p>
            <hr>
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td style="width: 100%;">
                            {{isset($cv->note)?$cv->note:''}}
                        </td>
                    </tr>
                </tbody>
            </table>
            <p></p>
            <p style="font-size: 18px;">TERTIARY EDUCATION</p>
            <hr>
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <th style="width: 50%; font-weight: bold;">
                            Qualification
                        </th>
                        <th style="width: 40%;">
                            Institution
                        </th>
                        <th style="width: 10%;">
                            Year
                        </th>
                    </tr>
                    @foreach($cv->qualification as $qualification)
                    <tr>
                        <td>
                            {{$qualification->qualification}}
                        </td>
                        <td>
                            {{$qualification->institution}}
                        </td>
                        <td>
                            {{$qualification->year_complete}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            {{$qualification->subjects}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{--<p class="break-after">&nbsp;</p>--}}
            <p></p>
            {{--<table>
                <tbody>
                    <tr>
                        <td style="width: 70%;">&nbsp;</td>
                        <td>
                            <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                        </td>
                    </tr>
                </tbody>
            </table>--}}
            <p>&nbsp;</p>
            <p style="font-size: 18px;">SKILLS</p>
            <hr>
            <table style="width: 100%;">
                <tbody>
                <tr>
                    <th style="width: 50%;">
                        Skill
                    </th>
                    <th style="width: 20%;">
                        Tool
                    </th>
                    <th style="width: 15%;">
                        Level
                    </th>
                    <th style="width: 15%;">
                        Years
                    </th>
                </tr>
                @foreach($cv->skill as $skill)
                    <tr>
                        <td>
                            {{$skill->res_skill}}
                        </td>
                        <td>
                            {{isset($tool[$skill->tool_id])?$tool[$skill->tool_id]:''}}
                        </td>
                        <td>
                            {{isset($skill_level[$skill->skill_level])?$skill_level[$skill->skill_level]:''}}
                        </td>
                        <td>
                            {{$skill->years_experience}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{--<p class="break-after">&nbsp;</p>--}}
            <p></p>
            {{--<table>
                <tbody>
                <tr>
                    <td style="width: 70%;">&nbsp;</td>
                    <td>
                        <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                    </td>
                </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>--}}
            <p style="font-size: 18px;">EMPLOYMENT HISTORY</p>
            <hr>
            @foreach($cv->experience as $experience)
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <th style="width: 30%;">
                            Company
                        </th>
                        <td style="width: 70%;">
                            {{$experience->company}}
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 30%;">
                            Period
                        </th>
                        <td style="width: 70%;">
                            {{$experience->period_start}} - {{$experience->period_end}}
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 30%;">
                            Role
                        </th>
                        <td style="width: 70%;">
                            {{$experience->role}}
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 30%;">
                            Responsibilities
                        </th>
                        <td style="width: 70%;">
                            {!! $experience->responsibility !!}
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 30%;">
                            Tools
                        </th>
                        <td style="width: 70%;">
                            {{$experience->tools}}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                @endforeach
            </div>
        </div>
    </body>
</html>
