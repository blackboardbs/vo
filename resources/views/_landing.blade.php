<div class="col-lg-12">
    <div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-calendar" value="{{route('calendar.index')}}">
            <label class="form-check-label" for="land-calendar">
                Calendar
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-exptracking" value="{{route('exptracking.index')}}" {{(isset($landing_page) && $landing_page->page == route('exptracking.index'))?" checked":null}}>
            <label class="form-check-label" for="land-exptracking">
                Expense Tracking
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-project" value="{{route('project.index')}}" {{(isset($landing_page) && $landing_page->page == route('project.index'))?" checked":null}}>
            <label class="form-check-label" for="land-project">
                Project
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-assignment" value="{{route('assignment.index')}}" {{(isset($landing_page) && $landing_page->page == route('assignment.index'))?" checked":null}}>
            <label class="form-check-label" for="land-assignment">
                Assignment
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-timesheet" value="{{route('timesheet.index')}}" {{(isset($landing_page) && $landing_page->page == route('timesheet.index'))?" checked":null}}>
            <label class="form-check-label" for="land-timesheet">
                Timesheet
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-kanban" value="{{url('/kanban/'.(\App\Models\Project::latest()->first()->id??(\App\Models\Timesheet::latest()->first()->project_id??0)).'?project_id=0&on_kanban=1')}}" {{(isset($landing_page) && $landing_page->page == url('/kanban/'.(\App\Models\Project::latest()->first()->id??(\App\Models\Timesheet::latest()->first()->project_id??0)).'?project_id=0&on_kanban=1'))?" checked":null}}>
            <label class="form-check-label" for="land-kanban">
                Kanban Board
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-my-work-week" value="{{url('/my-work-week')}}" {{(isset($landing_page) && $landing_page->page == url('/my-work-week'))?" checked":null}}>
            <label class="form-check-label" for="land-my-work-week">
                My Work Week
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-resource" value="{{route('resource.index')}}" {{(isset($landing_page) && $landing_page->page == route('resource.index'))?" checked":null}}>
            <label class="form-check-label" for="land-resource">
                Resource Profile
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-customer-invoice" value="{{route('customer.invoice')}}" {{(isset($landing_page) && $landing_page->page == route('customer.invoice'))?" checked":null}}>
            <label class="form-check-label" for="land-customer-invoice">
                Customer Invoice
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-vendor-invoice" value="{{route('vendor.invoice')}}" {{(isset($landing_page) && $landing_page->page == route('vendor.invoice'))?" checked":null}}>
            <label class="form-check-label" for="land-vendor-invoice">
                Vendor Invoice
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-reports-manager" value="{{route('reports.manager')}}" {{(isset($landing_page) && $landing_page->page == route('reports.manager'))?" checked":null}}>
            <label class="form-check-label" for="land-reports-manager">
                Insight Management Reports
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-reports-task" value="{{route('task.reports')}}" {{(isset($landing_page) && $landing_page->page == route('task.reports'))?" checked":null}}>
            <label class="form-check-label" for="land-reports-task">
                Insight Task Reports
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-users" value="{{route('users.index')}}" {{(isset($landing_page) && $landing_page->page == route('users.index'))?" checked":null}}>
            <label class="form-check-label" for="land-users">
                Admin Users
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-module" value="{{route('module.index')}}" {{(isset($landing_page) && $landing_page->page == route('module.index'))?" checked":null}}>
            <label class="form-check-label" for="land-module">
                Admin Roles
            </label>
        </div>
        <div class="form-check form-check-inline col-md-3">
            <input class="form-check-input" type="radio" name="page" id="land-module" value="{{route('system_health.index')}}" {{(isset($landing_page) && $landing_page->page == route('system_health.index'))?" checked":null}}>
            <label class="form-check-label" for="land-module">
                System Health
            </label>
        </div>
    </div>
</div>
