@extends('adminlte.default')

@section('title') View Favourites @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('default_favs.edit', $favourite)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <tr>

                        <th>Module Name</th>
                        <td>{{isset($favourite->favourites)?$favourite->favourites->module_name:''}}</td>
                        <th>Role</th>
                        <td>{{isset($favourite->role)?$favourite->role->display_name:''}}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection