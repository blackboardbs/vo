@extends('adminlte.default')

@section('title') Add Default Favourite @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('default_favs.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm" id="default-table">
                <tr>
                    <th>Module Name</th>
                    <td>{{ Form::select('module_name', $favourites,null, ['class'=>'form-control form-control-sm '. ($errors->has('module_name') ? ' is-invalid' : ''), 'placeholder'=>'Select Module', 'id'=>'module_id']) }}
                        @foreach($errors->get('module_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Role</th>
                    <td>{{Form::select('role_id',$roles,null,['class'=>'form-control form-control-sm '. ($errors->has('role_id') ? ' is-invalid' : ''), 'placeholder'=>'Select Role', 'id'=>'role_id'])}}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    {{--<td class="text-right"><a href="#" id="add-default" class="btn btn-sm btn-dark"><i class="fas fa-plus"></i> Add Default</a></td>--}}
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        /*$(function () {
            $("#add-default").on('click', function (e) {
                e.preventDefault();
                let favourites = $("#module_id").val();
                let role = $("#role_id").val();

                axios.post('{{route('default_favs.store')}}', {
                    favourite: favourites,
                    role: role
                })
                    .then(function (response) {
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })*/
    </script>
@endsection