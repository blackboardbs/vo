<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Leave Type</th>
        <th>Opening Balance</th>
        <th>Balance Date</th>
        <th>Leave Per Cycle</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($leavebalances as $result)
        <tr>
            <td>{{$result->resource?->name()}}</td>
            <td>{{$result->leave_type?->description}}</td>
            <td>{{$result->opening_balance}}</td>
            <td>{{$result->balance_date}}</td>
            <td>{{$result->leave_per_cycle}}</td>
            <td>{{$result->statusd?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>