@extends('adminlte.default')

@section('title') View Leave Balance @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('leave_balance.edit', $leave_balance[0])}}" class="btn btn-success float-right" style="margin-left: 5px;">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">


        <hr>

        @foreach($leave_balance as $result)
            <div class="table-responsive">
                <div id="balancetype" class="form-row col-md-12" style="padding:5px;"><strong>Balance Date:</strong>&nbsp;&nbsp;{{$result->balance_date}}</div>
                <div id="resource" class="form-row col-md-12" style="padding:5px;"><strong>Resource:</strong>&nbsp;&nbsp;{{$result->resource->first_name}} {{$result->resource->last_name}}
                </div>
                <div id="leave_type" class="form-row col-md-12" style="padding:5px;">
                    <strong>Leave Type:</strong>&nbsp;&nbsp;{{$result->leave_type->description}}</div>
            </div>
            <br />
            <table class="leave-table table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th>Entitlement per cycle:</th>
                    <th>Cycle Start:</th>
                    <th>Opening Balance:</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$result->leave_per_cycle}}</td>
                    <td>{{$result->cycle_start}}</td>
                    <td>{{$result->opening_balance}}</td>
                </tr>
                </tbody>
            </table>
            @if($result->pay_out == 1)
            <table class="leave-table table table-bordered table-sm mt-3">
                <thead>
                    <tr class="btn-dark">
                        <td colspan="6">Termination</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-md-2">Pay out leave balance</td>
                        <td class="col-md-2"><div class="form-check form-check-inline"><input type="checkbox" class="form-check-input disabled" disabled name="pay_out" id="pay_out" {{ $result->pay_out == 1 ? 'checked' : '' }} /></div></td>
                        <td class="col-md-2">Date</td>
                        <td class="col-md-2">{{ $result->pay_out_date }}</td>
                        <td class="col-md-2">Days paid out</td>
                        <td class="col-md-2">{{ $result->pay_out_balance }}</td>
                    </tr>
                </tbody>
            </table>
            @endif
        @endforeach
    </div>
@endsection
