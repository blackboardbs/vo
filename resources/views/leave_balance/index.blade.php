@extends('adminlte.default')

@section('title') Leave Balance @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('leave_balance.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Leave Balance</a>
        <x-export route="leave_balance.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('leave_balance.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('resource.first_name', 'Resource')</th>
                    <th>@sortablelink('leave_type.description', 'Leave Type')</th>
                    <th>@sortablelink('opening_balance', 'Opening Balance')</th>
                    <th>@sortablelink('balance_date', 'Balance Date')</th>
                    <th>@sortablelink('leave_per_cycle', 'Leave Per Cycle')</th>
                    <th>@sortablelink('statusd.description', 'Status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($leavebalances as $result)
                    <tr>
                        <td><a href="{{route('leave_balance.show',$result->id)}}">{{$result->resource?->first_name}} {{$result->resource?->last_name}}</a></td>
                        <td>{{$result->leave_type?->description}}</td>
                        <td>{{$result->opening_balance}}</td>
                        <td>{{$result->balance_date}}</td>
                        <td>{{$result->leave_per_cycle}}</td>
                        <td>{{$result->statusd?->description}}</td>

                        <td>
                            <div class="d-flex">
                                <a href="{{route('leave_balance.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['leave_balance.destroy', $result->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7" class="text-center">There's no leave balance available</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $leavebalances->firstItem() }} - {{ $leavebalances->lastItem() }} of {{ $leavebalances->total() }}
                        </td>
                        <td>
                            {{ $leavebalances->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $leavebalances->appends(['s' => $leavebalances->perPage(),'q' => \Illuminate\Support\Facades\Request::get('q')])->links() }} --}}
        </div>
    </div>
@endsection
