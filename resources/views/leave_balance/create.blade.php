@extends('adminlte.default')

@section('title') Create Leave Balance @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('leave')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">


        <hr>

        <div class="table-responsive">

            {{Form::open(['url' => route('leavebalance.store'), 'method' => 'post','class'=>'form-inline mt-3','autocomplete' => 'off','id'=>'leave'])}}
            <div id="balancetype" class="form-row col-md-12" style="padding:5px;"><strong>Balance Date:</strong>&nbsp;&nbsp;{{$balance_date}}{{Form::hidden('balance_date',$balance_date,['class'=>'balance_date form-control form-control-sm'. ($errors->has('balance_date') ? ' is-invalid' : ''),'placeholder'=>''])}}</div>
            <div id="resource" class="form-row col-md-12" style="padding:5px;"><strong>Resource:</strong>&nbsp;&nbsp;
                <select name="resource" class="resource form-control form-control-sm  '. {{($errors->has('resource') ? ' is-invalid' : '')}}">
                    <option value="0">Resource</option>
                    @foreach($resource as $result)
                        <option value="{{$result->id}}">{{$result->full_name}}</option>
                    @endforeach
                </select>
                @foreach($errors->get('resource') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div id="leave_type" class="form-row col-md-12" style="padding:0 5px 15px;display: none;">
                <strong>Leave Type:</strong>&nbsp;&nbsp;{{Form::select('leave_type',$leave,null,['class'=>'leave_type form-control form-control-sm '])}}
            </div>
            <table class="leave-table table table-bordered table-sm" style="display: none;">
                <thead class="btn-dark">
                <tr>
                    <th>Entitlement per cycle:</th>
                    <th>Cycle Start:</th>
                    {{--<th>Next Cycle Start:</th>--}}
                    <th>Opening Balance:</th>
                    {{--<th>Accrual this period:</th>
                    <th>Taken this period:</th>--}}
                    <th>Status:</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{Form::text('entitled',$annual_leave_days,['class'=>'entitled form-control form-control-sm'. ($errors->has('entitled') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>
                    <td>{{Form::text('start',old('start'),['class'=>'datepicker start form-control form-control-sm'. ($errors->has('start') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>
                    {{--<td>{{Form::text('end',old('end'),['class'=>'datepicker end form-control form-control-sm'. ($errors->has('end') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>--}}
                    <td>{{Form::text('opening',old('opening'),['class'=>'opening form-control form-control-sm'. ($errors->has('opening') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>
                    {{--<td>{{Form::text('accrued',old('accrued'),['class'=>'accrued form-control form-control-sm'. ($errors->has('accrued') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>
                    <td>{{Form::text('taken',old('taken'),['class'=>'taken form-control form-control-sm'. ($errors->has('taken') ? ' is-invalid' : ''),'placeholder'=>''])}}</td>--}}
                    <td>
                        {{Form::select('status',$status_dropdown,'1',['class'=>'statusd form-control form-control-sm'. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{--<div id="statusd" class="form-row col-md-12" style="padding:0 5px 15px;display: none;">
                <strong>Status:</strong>&nbsp;&nbsp;

            </div>--}}
            <div class="alert alert-warning" style="display: none">
                <i class="fas fa-exclamation-triangle fa-3x fa-pull-left"></i> <p>Please note that the opening balance was calculated using <span class="join_date text-bold"></span> as a resource join date. If this is not correct you can change it
                    <a class="resource">here.</a></p>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $('.resource').change(function() {
            $('#leave_type').show();
            getLeave();
        });

        $('.leave_type').change(function() {
            $('.leave-table').show();
            $('#statusd').show();
            getLeave();
        });

        function getLeave(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(".resource").val();
            var leave_type = $(".leave_type").val();
            if(id > 0 && leave_type > 0) {
                $.ajax({
                    url: '/getopeningbalance',
                    type: "POST",
                    data: {resource_id: id, leave_type: leave_type},
                    success: function (data) {
                        $('.alert').show();
                        $('.entitled').val(data.total_leave);
                        $('.start').val(data.cycle_start);
                        $('.end').val(data.cycle_end);
                        $('.opening').val(data.diff_in_months);
                        $('.accrued').val(data.total_accrued);
                        $('.taken').val(data.leave_taken);
                        $('.closing').val(data.total_accrued - data.leave_taken);
                        $('.join_date').text(data.join_date);
                        $('.resource').attr("href", window.location.origin + '/resource/' + data.resource_id + '/edit?leave_bal=1')
                    }
                });
            }
        }
    </script>
@endsection
