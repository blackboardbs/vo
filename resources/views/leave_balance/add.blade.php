@extends('adminlte.default')
@section('title') Create Leave Balance @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('leave')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('leave_balance.store'), 'method' => 'post','class'=>'mt-3', 'files' => true,'id'=>'leave'])}}
            <table class="leave-table table table-bordered table-sm">
                <thead>
                    <tr class="btn-dark">
                        <td colspan="4">Leave Balance</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Resource:</td>
                        <td>
                            {{Form::select('resource',$resource_drop_down,old('resource'),['id'=>'resource','class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('resource') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('resource') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            <input type="hidden" name="resource_termination_date" id="resource_termination_date" />
                        </td>
                        <td>Opening Balance:</td>
                        <td>
                            {{Form::text('opening_balance', old('opening_balance', 0),['class'=>'opening form-control form-control-sm'. ($errors->has('opening_balance') ? ' is-invalid' : ''),'placeholder'=>'', 'style'=>'width: 100%;'])}}
                            @foreach($errors->get('opening_balance') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Balance Date:</td>
                        <td>
                            {{Form::text('balance_date',old('balance_date'),['class'=>'datepicker entitled form-control form-control-sm'. ($errors->has('balance_date') ? ' is-invalid' : ''),'placeholder'=>'', 'style'=>'width: 100%;'])}}
                            @foreach($errors->get('balance_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>Leave per cycle:</td>
                        <td>
                            {{Form::text('leave_per_cycle', $annual_leave_days,['id'=>'leave_per_cycle','class'=>'start form-control form-control-sm'. ($errors->has('leave_per_cycle') ? ' is-invalid' : ''),'placeholder'=>'', 'style'=>'width: 100%;'])}}
                            @foreach($errors->get('leave_per_cycle') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>
                            {{Form::select('status',$status_dropdown,'1',['class'=>'statusd form-control form-control-sm'. ($errors->has('status') ? ' is-invalid' : ''), 'style'=>'width: 100%;'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2"></td>
                    </tr>
                </tbody>
            </table>
            <div class="alert alert-warning" style="display: none">
                <i class="fas fa-exclamation-triangle fa-3x fa-pull-left"></i> <p>Please note that the opening balance was calculated using <span class="join_date text-bold"></span> as a resource join date. If this is not correct you can change it
                    <a class="resource">here.</a></p>
            </div>
            <div id="termination_alert" class="alert alert-warning" style="display: none">
                <i class="fas fa-exclamation-triangle fa-pull-left mt-1"></i> <p>Termination date on the resource profile is different from the termination date on the leave balance</p>
            </div>
            <table class="leave-table table table-bordered table-sm mt-3">
                <thead>
                    <tr class="btn-dark">
                        <td colspan="6">Termination</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-md-2">Pay out leave balance</td>
                        <td class="col-md-2"><div class="form-check form-check-inline"><input type="checkbox" class="form-check-input" name="pay_out" id="pay_out" /></div></td>
                        <td class="col-md-2">Date</td>
                        <td class="col-md-2"><input type="text" name="pay_out_date" id="pay_out_date" class="form-control form-control-sm datepicker" /></td>
                        <td class="col-md-2">Days paid out</td>
                        <td class="col-md-2"><input type="text" name="pay_out_leave" id="pay_out_leave" class="form-control form-control-sm" /></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

<script>
    $(document).ready(function() {

        if($('#pay_out').is(':checked')){
            if($('#resource_termination_date').val() != '' && $('#resource_termination_date').val() != $('#pay_out_date').val()){
                $('#termination_alert').show();
            } else {
                $('#termination_alert').hide();
            }
        }

        $('#pay_out').change(function() {
            if ($(this).is(':checked')) {
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today = yyyy + '-' + mm + '-' + dd;
                $('#pay_out_date').val(today);


                axios.post('/leave/getallannualleave',{'resource':$('#resource').val(),'date':$('#pay_out_date').val()})
                .then(function (data) {
                    $('#pay_out_leave').val(data.data.leave)
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
                


                if($('#resource_termination_date').val() != '' && $('#resource_termination_date').val() != today){
                    $('#termination_alert').show();
                } else {
                    $('#termination_alert').hide();
                }
            } else {
                $('#pay_out_date').val('');
                $('#pay_out_leave').val('');
            }
        });

        $('#resource').change(function() {
            var selectedValue = $(this).val();

            if (selectedValue) {
                axios.get('/leave_balance/' + selectedValue + '/getresource')
                    .then(function(response) {
                        if(response.data.termination_date){
                        var today = new Date(response.data.termination_date);
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        var formattedDate = yyyy + '-' + mm + '-' + dd;
                        }
                        $('#resource_termination_date').val(formattedDate ? formattedDate : '');
                        if($('#pay_out').is(':checked')){
                            if($('#resource_termination_date').val() != '' && $('#resource_termination_date').val() != $('#pay_out_date').val()){
                                $('#termination_alert').show();
                            } else {
                                $('#termination_alert').hide();
                            }
                        }
                    })
                    .catch(function(error) {
                        console.error('Error fetching data:', error);
                    });

                axios.post('/leave/getallannualleave',{'resource':$('#resource').val(),'date':$('#pay_out_date').val()})
                    .then(function (data) {
                        $('#pay_out_leave').val(data.data.leave)
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });
            }
        });
    });
</script>

@endsection()
