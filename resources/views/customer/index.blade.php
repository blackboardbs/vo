@extends('adminlte.default')

@section('title') Customer @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('customer.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Customer</a>
            <x-export route="customer.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="hidden" name="r" class="form-control w-100" value="{{isset($_GET['r']) ? $_GET['r'] : ''}}" />
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>

            <div class="col-md-3" style="max-width: 20%;">
                <a href="{{route('customer.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('customer_name','Customer Name')</th>
                    <th>@sortablelink('email')</th>
                    <th>@sortablelink('phone')</th>
                    <th>@sortablelink('cell')</th>
                    <th>@sortablelink('created_at','Added')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($customer as $result)
                    <tr>
                        <td><a href="{{route('customer.show',$result)}}">{{$result->customer_name}}</a></td>
                        <td>{{$result->email}}</td>
                        <td>{{$result->phone}}</td>
                        <td>{{$result->cell}}</td>
                        <td>{{$result->created_at}}</td>
                        <td>{{$result->statusd?->description}}</td>
                        @if($can_update)
                        <td>
                            <div class="d-flex">
                            <a href="{{route('customer.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['customer.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm" {{($result->projects_count > 0 || $result->users_count > 0 || $result->quotations_count > 0 || $result->invoices_count > 0 ? 'disabled' : '')}}><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No companies match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $customer->firstItem() }} - {{ $customer->lastItem() }} of {{ $customer->total() }}
                        </td>
                        <td>
                            {{ $customer->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
