<table>
    <thead>
    <tr>
        <th>Invoice #</th>
        <th>Customer</th>
        <th>Invoice Ref</th>
        <th>Invoice Date</th>
        <th>Due Date</th>
        <th>Paid Date</th>
        <th>Invoice Value</th>
        <th>Customer Invoice Status</th>
        <th>Added</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customer_invoices as $customer_invoice)
        <tr>    <td>{{$customer_invoice->id}}</td>
            <td>{{$customer_invoice->customer?->customer_name}}</td>
            <td>{{$customer_invoice->inv_ref}}</td>
            <td>{{$customer_invoice->invoice_date}}</td>
            <td>{{$customer_invoice->due_date}}</td>
            <td>{{$customer_invoice->paid_date}}</td>
            <td>{{($customer_invoice->currency??"ZAR")." ".number_format($customer_invoice->invoice_value,2,'.',',')}}</td>
            <td>{{$customer_invoice->invoice_status?->description}}</td>
            <td>{{ $customer_invoice->created_at }}</td>
        </tr>
    @endforeach
    <tr>
        <th>Total</th>    <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th class="text-right">
            @foreach($total as $key=>$value)
                {{$key}}: {{number_format($value,2,'.',',')}}<br />
            @endforeach
        </th>
        <td></td>
        <td></td>
        @if($can_update)
            <td>
            </td>
        @endif
    </tr>
    </tbody>
</table>