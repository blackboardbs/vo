@extends('adminlte.default')

@section('title') Customer Invoices @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('nontimesheetinvoice.create', ['recurring' => 1])}}" class="btn btn-success float-right ml-1">
                <i class="fa fa-plus"></i> Recurring Invoice</a>
            <a href="{{route('nontimesheetinvoice.create')}}" class="btn btn-success float-right ml-1"><i class="fa fa-plus"></i> Non Timesheet Invoice</a>
            <a href="{{route('customer.invoice_create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> New Invoice</a>
            <x-export route="customer.invoice"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="mt-3" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="form-row">
                <div class="col-sm-3 col-sm" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    <div class="position-relative w-100">
                        <input class="form-control form-control-sm w-100" id="customer" placeholder="Select Customer" value="{{(isset($_GET['customer_id']) ? $_GET['customer_id'] : '')}}">
                        <div class="position-absolute w-100 bg-gray-light shadow rounded list"></div>
                    </div>
                    <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('invoice_status',App\Enum\InvoiceStatusEnum::keyValuePair(),request()->invoice_status,['class'=>'form-control search', 'placeholder' => "Select Invoice Status"])}}
                    <span>Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <input type="text" name="filter_from" class="datepicker form-control w-100" value="{{(isset($_GET['filter_from']) ? $_GET['filter_from'] : '')}}">
                    {{-- {{ Form::text('filter_from', old('filter_from'), ['class' => 'form-control datepicker search','placeholder'=>'Invoice from', 'id' => 'filter_from']) }} --}}
                    <span>From</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <input type="text" name="filter_to" class="datepicker form-control w-100" value="{{(isset($_GET['filter_to']) ? $_GET['filter_to'] : '')}}">
                    {{-- {{ Form::text('filter_to', old('filter_to'), ['class' => 'form-control datepicker2 search','placeholder'=>'Invoice to', 'id' => 'filter_to']) }} --}}
                    <span>To</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67%;">
                        <div class="form-group input-group">
                            <label class="has-float-label">
                                <input type="text" name="q" class="form-control w-100 search" value="{{(isset($_GET['q']) ? $_GET['q'] : '')}}">
                                <span>Matching</span>
                            </label>
                        </div>
                </div>
                <div class="col-md-3" style="max-width:15%;">
                    <a href="{{ route('customer.invoice') }}" class="btn btn-info w-100">Clear Filter</a>
                </div>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="bg-dark">
                    <th>@sortablelink('id', 'Invoice #')</th>
                    <th>@sortablelink('customer.customer_name', 'Customer')</th>
                    <th>@sortablelink('inv_ref', 'Reference')</th>
                    <th>@sortablelink('invoice_date', 'Invoiced')</th>
                    <th>@sortablelink('due_date', 'Due Date')</th>
                    <th>@sortablelink('paid_date', 'Paid Date')</th>
                    <th class="text-right">@sortablelink('invoice_value', 'Value')</th>
                    <th>@sortablelink('invoice_status.description', 'Status')</th>
                    <th class="text-center">Type</th>
                    <th>@sortablelink('created_at', 'Added')</th>
                    @if($can_create)
                    <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($customer_invoices as $customer_invoice)
                    <tr>    <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->id}}</a>@else {{$customer_invoice->id}} @endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->customer?->customer_name}}</a>@else{{$customer_invoice->customer?->customer_name}}@endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->inv_ref}}</a>@else{{$customer_invoice->inv_ref}}@endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->invoice_date}}</a>@else{{$customer_invoice->invoice_date}}@endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->due_date}}</a>@else{{$customer_invoice->due_date}}@endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->paid_date}}</a>@else{{$customer_invoice->paid_date}}@endif</td>
                            <td class="text-right">@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{($customer_invoice->currency??"ZAR")." ".number_format($customer_invoice->invoice_value,2,'.',',')}}</a>@else{{($customer_invoice->currency??"ZAR")." ".number_format($customer_invoice->invoice_value,2,'.',',')}}@endif</td>
                            <td>@if($customer_invoice->invoiceType() !== "RI")<a href="{{route('customer.invoice_show',$customer_invoice->id)}}">{{$customer_invoice->invoice_status?->description}}</a>@else{{$customer_invoice->invoice_status?->description}}@endif</td>
                            <td class="text-center"><span class="badge badge-primary">{{ $customer_invoice->invoiceType() }}</span></td>
                            <td>{{ $customer_invoice->created_at }}</td>
                            @if($can_update)
                            <td>
                                @if($customer_invoice->bill_status != 3)
                                    @if($customer_invoice->invoiceType() == "RI")
                                        <a href="{{route('customer.invoice_edit',['invoice_id' => $customer_invoice, 'recurring' => 1])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    @else
                                        <a href="{{route('customer.invoice_edit',$customer_invoice)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    @endif
                                @endif
                            </td>
                            @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No customer invoice match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                        <th>Total</th>    <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th class="text-right">
                            @foreach($total as $key=>$value)
                            {{$key}}: {{number_format($value,2,'.',',')}}<br />
                            @endforeach
                        </th>
                        <td></td>
                        <td></td>
                        @if($can_update)
                        <td>
                        </td>
                        @endif
                </tr>
                </tbody>
            </table>
            @if(Session::has('message'))
                <p class="text-center text-danger mb-0 border border-danger py-1">{{Session::get('message')}}</p>
            @endif
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $customer_invoices->firstItem() }} - {{ $customer_invoices->lastItem() }} of {{ $customer_invoices->total() }}
                        </td>
                        <td>
                            {{ $customer_invoices->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $customer_invoices->appends(request()->except('page'))->links() }} --}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script src="{{global_asset('js/filters.js')}}"></script>
    <script>
        $(document).ready(function(){
            const customers = @json(session('customer_dropdown'));
            const url = new URL(window.location.toLocaleString());

            $("#customer").dropdownSearch({
                name: "customer",
                dropdown: customers !== null ? customers : []
            });

            if (url.searchParams.get('customer_id')){
                let customer = `<input type="hidden" name="customer_id" value="${url.searchParams.get('customer_id')}" >`;
                $("#searchform").append(customer)
            }

            $(function () {
                $( ".datepicker2" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    showWeek:true,
                    yearRange: "2017:+10"
                });
            })
        })
    </script>
@endsection

@section('extra-css')
    <style>
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection
