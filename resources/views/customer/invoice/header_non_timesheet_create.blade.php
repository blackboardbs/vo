@extends('adminlte.default')
@section('title') Prepare Invoice @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('nontimesheet')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @if($invoiceTypeID == 1)
            {{Form::open(['url' => route('nontimesheetinvoice.store'), 'id'=>'nontimesheet','method' => 'post','class'=>'mt-3', 'autocomplete' => 'off'])}}
            @endif
            @if($invoiceTypeID == 2)
            {{Form::open(['url' => route('nontimesheetinvoice.store_vendor'), 'id'=>'nontimesheet', 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off'])}}
            @endif
            <invoice-header-component
                    :errors="{{$errors}}"
                    :old="{{ json_encode(Session::getOldInput()) }}"
                    :invoicetypeid="{{$invoiceTypeID}}"
                    :currencies="{{json_encode($currency_drop_down)}}"
                    :recurring="{{request()->input('recurring', 0)}}"
                    :newInvoice="1"
                    :currency="{{\App\Models\Config::first()->currency??'ZAR'}}"
            />
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>

        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            });
        });
    </script>
@endsection
