@extends('adminlte.default')

@section('title') Edit Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary float-right ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.invoice_update',['invoice_id' => $customer_invoice]), 'method' => 'PATCH','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Edit Customer Invoice</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Financial AR Invoice No: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Accounts receivable invoice number from company's Financial system"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('invoice_ref', $customer_invoice->inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('invoice_ref') ? ' is-invalid' : ''),'placeholder'=>'Invoice Reference'])}}
                        @foreach($errors->get('invoice_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Invoice Date:</th>
                    <td>{{Form::text('invoice_date',$customer_invoice->invoice_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('invoice_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                        @foreach($errors->get('invoice_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Customer Reference: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Reference received from customer E.g. Order number or project reference"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('customer_ref', $customer_invoice->cust_inv_ref,['class'=>'form-control form-control-sm'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Reference'])}}
                        @foreach($errors->get('customer_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Payment Due Date:</th>
                    <td>{{Form::text('due_date',$customer_invoice->due_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    </tr>
                </tr>
                <tr>
                    <th>Invoice Notes: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Note to be displayed on invoice footer"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('invoice_notes',$customer_invoice->invoice_notes,['class'=>'form-control form-control-sm'. ($errors->has('Invoice_notes') ? ' is-invalid' : ''),'placeholder'=>'Invoice Notes'])}}
                        @foreach($errors->get('Invoice_notes') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="{{asset('chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
    <script>
        $(function () {

        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })
    </script>
@endsection
