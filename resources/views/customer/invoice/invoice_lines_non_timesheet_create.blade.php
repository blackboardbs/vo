@extends('adminlte.default')
@section('title') Prepare Invoice @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.invoice_generate'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark">Generating Invoice</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Customer: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Customer"><i class="far fa-question-circle"></i></span></th>
                        <td>
                            {{Form::select('customer', $customer_drop_down, null, ['id'=>'customer', 'class'=>'form-control '. ($errors->has('customer') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('customer') as $error)
                                <div class="invalid-feedback">
                                    {{ $error }}
                                </div>
                            @endforeach
                        </td>
                        <th colspan="2">&nbsp;</th>
                    </tr>
                    <tr>
                        <th>Project:</th>
                        <td>
                            {{Form::select('project', $project_drop_down, null, ['id'=>'project', 'class'=>'form-control '. ($errors->has('project') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('project') as $error)
                                <div class="invalid-feedback">
                                    {{ $error }}
                                </div>
                            @endforeach
                        </td>
                        <th>Resource</th>
                        <td>
                            {{Form::select('resource', $resource_drop_down, null, ['id'=>'resource', 'class'=>'form-control '. ($errors->has('resource') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('resource') as $error)
                                <div class="invalid-feedback">
                                    {{ $error }}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Financial AR Invoice No: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Accounts receivable invoice number from company's Financial system"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('invoice_ref', old('invoice_ref'),['class'=>'form-control form-control-sm'. ($errors->has('invoice_ref') ? ' is-invalid' : ''),'placeholder'=>'Invoice Reference'])}}
                            @foreach($errors->get('invoice_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Invoice Date:</th>
                        <td>{{Form::text('invoice_date',$invoice_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('invoice_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                            @foreach($errors->get('invoice_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Customer Reference: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Reference received from customer E.g. Order number or project reference"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('customer_ref', old('customer_ref'),['class'=>'form-control form-control-sm'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Reference'])}}
                            @foreach($errors->get('customer_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Payment Due Date:</th>
                        <td>{{Form::text('due_date',$payment_due_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                            @foreach($errors->get('due_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Invoice Notes: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Note to be displayed on invoice footer"><i class="far fa-question-circle"></i></span></th>
                        <td>{{Form::text('invoice_notes',old('Invoice_notes'),['class'=>'form-control form-control-sm'. ($errors->has('Invoice_notes') ? ' is-invalid' : ''),'placeholder'=>'Invoice Notes'])}}
                            @foreach($errors->get('Invoice_notes') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            {{--<table class="table table-sm table-bordered">
                <tbody id="invoice_data">
                    <tr>
                        <th class="btn-dark">Line</th><th class="btn-dark">Description</th><th class="btn-dark">Qty</th><th class="btn-dark">Excl. Price</th><th class="btn-dark">VAT Code</th><th class="btn-dark">VAT%</th><th class="btn-dark">Discount Amount</th><th class="btn-dark">Total</th><th class="btn-dark"></th>
                    </tr>
                </tbody>
            </table>--}}
            <invoice-items invoice_id="{{$invoiceNumber}}" invoice_type_id="{{$invoiceTypeID}}"></invoice-items>
            {{--<table class="table table-sm table-borderless">
                <tbody id="invoice_data">
                    <tr>
                        <td colspan="4" class="text-center"><a onclick="addInvoiceLine()" class="btn btn-sm btn-dark">Add Invoice Lines</a></td>
                    </tr>
                </tbody>
            </table>--}}
            <table id="submit_button_block" class="table table-borderless" style="display: none;">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm btn-dark">Generate Invoice</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        let invoice_lines = 0;
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                        .addClass( "arrow" )
                        .addClass( feedback.vertical )
                        .addClass( feedback.horizontal )
                        .appendTo( this );
                    }
                },
            });
        });
    </script>
@endsection
