@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('customer.invoice_edit',$customer_invoice->id)}}" class="btn btn-success float-right ml-1">Edit Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-4 pl-2 col-print-4">
                {{--<h4>Customer {{ isset($customer_invoice->customer->vat_no) ? 'Tax' : '' }} Invoice</h4>--}}
                <h4>Customer Tax Invoice</h4>
                <h5>{{ $company->company_name }}</h5>
                <p class="mb-0">{{ isset($company->postal_address_line1) ? $company->postal_address_line1: '' }}</p>
                <p class="mb-0">{{ isset($company->postal_address_line2) ? $company->postal_address_line2 : '' }}</p>
                @if($company->postal_address3 !== null)
                    <p class="mb-0">{{ isset($company->postal_address_line3) ? $company->postal_address_line3 : ''}}</p>
                @endif
                <p class="mb-0">{{ isset($company->state_province) ? $company->state_province : '' }}</p>
                <p class="mb-0">{{ $company->postal_zipcode }}</p>
                <p class="mb-0">{{ $company->phone }}</p>
                <p class="mb-0">{{ $company->email }}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no }}</p>
            </div>
           <div class="col-md-4 col-print-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 text-bold">TO:</p>
               <p class="mb-0">{{isset($customer_invoice->customer->customer_name) ? $customer_invoice->customer->customer_name : '<CustomerName>'}}</p>
                <p class="mb-0">
                {{isset($customer_invoice->customer->postal_address_line1) ? $customer_invoice->customer->postal_address_line1 : $customer_invoice->customer->street_address_line1}}
                </p>
                <p class="mb-0">
                    {{isset($customer_invoice->customer->postal_address_line2) ? $customer_invoice->customer->postal_address_line2 : $customer_invoice->customer->street_address_line2}}

                </p>
                <p class="mb-0">
                    {{isset($customer_invoice->customer->postal_address_line3) ? $customer_invoice->customer->postal_address_line3 : $customer_invoice->customer->street_address_line3}}
                </p>
                <p class="mb-0">
                    {{isset($customer_invoice->customer->city_suburb) ? $customer_invoice->customer->city_suburb : $customer_invoice->customer->street_city_suburb}}

                </p>
                <p class="mb-0">{{isset($customer_invoice->customer->state_province) ? $customer_invoice->customer->state_province : ''}}</p>
                <p class="mb-0">
                    {{isset($customer_invoice->customer->postal_zipcode) ? $customer_invoice->customer->postal_zipcode : $customer_invoice->customer->street_zipcode}}
                </p>
               <p class="mb-0">Vat No: {{ isset($customer_invoice->customer->vat_no) ? $customer_invoice->customer->vat_no : '' }}</p>
               <p class="mb-0">Contact: {{ $contact_person->name }}</p>
            </div>
             <div class="col-md-4 col-print-4">
                 <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> (isset($company->company_logo)?$company->company_logo:'default.png')])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                 <div class="row no-gutter">
                     <div class="col-md-6">Invoice Number:</div>
                     <div class="col-md-6 text-right">{{ $customer_invoice->customer_invoice_number?? $customer_invoice->id}}</div>
                     <div class="col-md-6">Invoice Date:</div>
                     <div class="col-md-6 text-right">{{$customer_invoice->invoice_date}}</div>
                     <div class="col-md-6">Due Date:</div>
                     <div class="col-md-6 text-right">{{ $customer_invoice->due_date }}</div>
                     <div class="col-md-6">Page:</div>
                     <div class="col-md-6 text-right">1/1</div>
                     <div class="col-md-4">Customer Reference:</div>
                     <div class="col-md-8 text-right">{{$customer_invoice->cust_inv_ref}}</div>
                     <div class="col-md-6">Sales Rep:</div>
                     <div class="col-md-6 text-right">{{ isset($customer_invoice->customer->accountm) ? $customer_invoice->customer->accountm->first_name.' '.$customer_invoice->customer->accountm->last_name : '' }}</div>
                 </div>
             </div>
             <div class="col-md-12 border-bottom border-dark pb-3 col-print-12"></div>
             <div class="table-responsive">
                 <div class="table-responsive col-12 px-0">
                     <table class="table table-sm table-hover">
                         <thead>
                         <tr class="btn-dark">
                             <th>Description</th>
                             <th>Qty</th>
                             <th>Excl. Price</th>
                             <th>Discount Amount</th>
                             <th class="text-center">VAT%</th>
                             <th class="text-right">Exclusive Total</th>
                             <th class="text-right">Inclusive Total</th>
                         </tr>
                         </thead>
                         <tbody>
                         @forelse($customer_invoice_lines as $key => $customer_invoice_line)
                             <tr>
                                 <td>{!! wordwrap($customer_invoice_line->line_text, 75, "<br />\n") !!}</td>
                                 <td>{{ $customer_invoice_line->quantity }}</td>
                                 <td>{{ $customer_invoice->currency.' '.number_format($customer_invoice_line->price,2,'.',',') }}</td>
                                 <td>{{ $customer_invoice->currency.' '.number_format($customer_invoice_line->discount_amount,2,'.',',') }}</td>
                                 <td class="text-center">{{ round($vat_rate*100) }}%</td>
                                 <td class="text-right">{{ $customer_invoice->currency.' '.number_format($customer_invoice_line->invoice_line_value,2,'.',',') }}</td>
                                 <td class="text-right">{{ $customer_invoice->currency.' '.number_format(($customer_invoice_line->invoice_line_value * (1 + $vat_rate)),2,'.',',') }}</td>
                             </tr>
                             @empty
                         @endforelse
                         <tr>
                             <th class="text-right">TOTAL HOURS</th>
                             <th colspan="7">{{number_format($total_hours,2,'.',',')}}</th>
                         </tr>
                         @if(count($customer_invoice_line_exp) > 0)
                             <tr>
                                 <td colspan="7">&nbsp;</td>
                             </tr>
                             <tr class="btn-dark">
                                 <th colspan="7">Expenses</th>
                             </tr>
                         @endif
                            @forelse($customer_invoice_line_exp as $index => $expense)
                                 @isset($expense)
                                     <tr>
                                         <td colspan="2">{!! wordwrap($expense->line_text, 75, "<br />\n") !!}</td>
                                         <td>1</td>
                                         <td>{{ $customer_invoice->currency.' '.$expense->amount }}</td>
                                         <td>0.00%</td>
                                         <td class="text-right">{{ $customer_invoice->currency.' '.number_format( $expense->amount,2,'.',',') }}</td>
                                         <td class="text-right">{{ $customer_invoice->currency.' '.number_format( $expense->amount,2,'.',',') }}</td>
                                     </tr>
                                     @endisset
                                 @empty
                             @endforelse
                             <tr>
                                 <th colspan="7">&nbsp;</th>
                             </tr>

                             <tr class="btn-dark">
                                 <th colspan="7">Summary</th>
                             </tr>
                             <tr>
                                 <th>Payment Information</th>
                                 <td colspan="4">{{ $customer_invoice->invoice_notes }}</td>
                                 <th class="text-right">Total Exclusive:</th>
                                 <td class="text-right">{{ $customer_invoice->currency.' '.number_format(($total_value_line + $total_expenses) ,2,'.',',') }}</td>
                             </tr>
                         <tr>
                             <td colspan="7">{{ $company->company_name }}</td>
                         </tr>
                         <tr>
                             <td colspan="5">{{ $company->bank_name }}</td>
                             <th class="text-right">Total VAT:</th>
                             <td class="text-right">{{ $customer_invoice->currency.' '.number_format($total_value_line_tax,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="7">Branch Code: {{ $company->branch_code }}</td>
                         </tr>
                         <tr>
                             <td colspan="5">Account Number: {{ $company->bank_acc_no }}</td>
                             <th class="text-right">Total:</th>
                             <td class="text-right">{{ $customer_invoice->currency.' '.number_format(($total_value_line_tax + $total_value_line + $total_expenses),2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="7">Reference: {{ isset($customer_invoice->customer->customer_name) ? $customer_invoice->customer->customer_name : '<CustomerName>' }}</td>
                         </tr>
                             <tr>
                                 <th colspan="7" class="text-center">Thank you for your business!</th>
                             </tr>
                     </tbody>
                 </table>
             </div>
         </div>
        </div>

        <div class="col-md-12 col-print-12">
            @if($customer_invoice->bill_status == 1)
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::open(['url' => route('customer.invoice_unallocate', $customer_invoice->id), 'method' => 'post', 'class' => 'mb-3 cancelInvoice']) !!}
                                <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-3 text-center">
                        <a href="{{route('customer.invoice_payment', $customer_invoice->id)}}" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-university"></i> Process Payment</a>
                    </div>
                    <div class="col-md-3 text-center">
                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                    </div>
                    {{--<div class="col-md-3 text-center"></div>--}}
                    <div class="col-md-3 text-right">
                        <a href="{{route('customer.invoice_show', ['customer_invoice' => $customer_invoice, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-6 text-left">
                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                    </div>
                    <div class="col-md-6 text-right">
                            <a href="{{route('customer.invoice_show', ['customer_invoice' => $customer_invoice, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                    </div>
                </div>
            @endif

        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                        <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="email-form">
                            {!! Form::open(['url' => route('customer.invoice_show', $customer_invoice), 'method' => 'post']) !!}
                            <div class="form-group row">
                                <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                </div>
                            </div>
                            <hr>
                            @if(isset($customer_invoice->customer->email))
                            <div class="form-check">
                                <input type="checkbox" name="customer_email" value="{{isset($customer_invoice->customer)?strtolower($customer_invoice->customer->email):''}}" checked class="form-check-input" id="customer_email">
                                <label class="form-check-label" for="customer_email">{{isset($customer_invoice->customer)?strtolower($customer_invoice->customer->email):''}}</label>
                            </div>
                            @endif
                            @if(trim($company->email) != '')
                            <div class="form-check">
                                <input type="checkbox" name="company_email" value="{{$company->email}}" checked class="form-check-input" id="company_email">
                                <label class="form-check-label" for="company_email">{{$company->email}}</label>
                            </div>
                            @endif
                            <hr>
                            <div class="form-group row">
                                <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group text-center">
                                {!! Form::submit('Send Invoice', ['class' => 'btn btn-sm btn-dark', 'id' => 'send-btn']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="loader-wrapper">
                            <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('extra-js')
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        function goBack() {
            window.history.back();
        }
        $(function () {
            $('#user_email').multiple_emails({
                position: 'top', // Display the added emails above the input
                theme: 'bootstrap', // Bootstrap is the default theme
                checkDupEmail: true // Should check for duplicate emails added
            });

            $('#current_emails').text($('#user_email').val());

            $('#user_email').change( function(){
                $('#current_emails').text($(this).val());
            });
        });

        $(function () {
            $("#send-btn").on('click', function () {
                $("#email-form").hide();
                $("#loader-wrapper").fadeIn();
                $(".default").hide();
                $(".sending").fadeIn();
            });
        })
    </script>

@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
        @media print {
            .col-print-4 {width:33%; float:left;}
            .col-print-12{width:100%; float:left;}
        }
    </style>
@endsection
