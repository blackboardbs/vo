@extends('adminlte.default')

@section('title') View Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        {!! Form::open(['url' => route('customer.invoice_save', ['save' => 1]), 'method' => 'post', 'class' => 'mb-3']) !!}

        @forelse($timesheets_id as $time)
            {{Form::hidden('time_id[]', $time)}}
            @empty
        @endforelse
        {{Form::hidden('due_date', $due_date) }}
        {{Form::hidden('invoice_ref', $invoice_ref) }}
        {{Form::hidden('invoice_notes', $invoice_notes) }}
        {{Form::hidden('invoice_date', $invoice_date)}}
        {{Form::hidden('customer_id', $timesheets[0]->customer_id)}}
        {{Form::hidden('customer_ref', $customer_ref)}}
        {{Form::hidden('currency', ($timesheets[0]->currency??$currency))}}
        {{Form::hidden('currency_type', $currency_type)}}
        {{Form::hidden('vat_rate_id', $vat_rate_id)}}

        <div class="row no-gutters">
            <div class="col-md-4">
                {{--<h4>Customer {{ isset($timesheets[0]->customer->vat_no)?'Tax':'' }} Invoice</h4>--}}
                <h4>Customer Tax Invoice</h4>
                <h5>{{$company->company_name}}</h5>
                <p class="mb-0">{{ $company->postal_address_line1 }}</p>
                <p class="mb-0">{{ $company->postal_address_line2 }}</p>
                @if($company->postal_address3 !== null)
                    <p class="mb-0">{{ $company->postal_address_line3 }}</p>
                @endif
                <p class="mb-0">{{ $company->state_province }}</p>
                <p class="mb-0">{{ $company->postal_zipcode }}</p>
                <p class="mb-0">{{ $company->phone }}</p>
                <p class="mb-0">{{ $company->email }}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no }}</p>
            </div>
           <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 text-bold">TO:</p>
                @if($timesheets[0]->customer != null)
                   <p class="mb-0">{{$timesheets[0]->customer->customer_name}}</p>
                   <p class="mb-0">
                   {{$timesheets[0]->customer->postal_address_line1}}
                   @empty($timesheets[0]->customer->postal_address_line1)
                       {{$timesheets[0]->customer->street_address_line1}}
                   @endempty
                   <p class="mb-0">
                       {{$timesheets[0]->customer->postal_address_line2}}
                       @empty($timesheets[0]->customer->postal_address_line2)
                           {{$timesheets[0]->customer->street_address_line2}}
                       @endempty

                   </p>
                   <p class="mb-0">
                       {{$timesheets[0]->customer->postal_address_line3}}
                       @empty($timesheets[0]->customer->postal_address_line3)
                           {{$timesheets[0]->customer->street_address_line3}}
                       @endempty
                   </p>
                   <p class="mb-0">
                       {{$timesheets[0]->customer->city_suburb}}
                       @empty($timesheets[0]->customer->city_suburb)
                           {{$timesheets[0]->customer->street_city_suburb}}
                       @endempty
                   </p>
                   <p class="mb-0">{{$timesheets[0]->customer->state_province}}</p>
                   <p class="mb-0">
                       {{$timesheets[0]->customer->postal_zipcode}}
                       @empty($timesheets[0]->customer->postal_zipcode)
                           {{ $timesheets[0]->customer->street_zipcode }}
                       @endempty
                   </p>
                    <p class="mb-0">Vat No: {{ $timesheets[0]->customer->vat_no }}</p>
                    <p class="mb-0">Contact: {{$contact_person->name}}</p>
               @endif
            </div>
             <div class="col-md-4">
                 <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> $company->company_logo])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                 <div class="row no-gutter">
                     <div class="col-md-6">Invoice Number:</div>
                     <div class="col-md-6 text-right">{{$invoice_number}}</div>
                     <div class="col-md-6">Invoice Date:</div>
                     <div class="col-md-6 text-right">{{$invoice_date}}</div>
                     <div class="col-md-6">Due Date:</div>
                     <div class="col-md-6 text-right">{{ $due_date }}</div>
                     <div class="col-md-6">Page:</div>
                     <div class="col-md-6 text-right">1/1</div>
                     <div class="col-md-4">Customer Reference:</div>
                     <div class="col-md-8 text-right">{{$customer_ref}}</div>
                     <div class="col-md-6">Sales Rep:</div>
                     <div class="col-md-6 text-right">{{ isset($timesheets[0]->customer->accountm) ? $timesheets[0]->customer->accountm->first_name.' '.$timesheets[0]->customer->accountm->last_name : '' }}</div>
                 </div>
             </div>
             <div class="col-md-12 border-bottom border-dark pb-3"></div>
             <div class="table-responsive">
                 <div class="table-responsive col-12 px-0">
                     <table class="table table-sm table-hover">
                         <thead>
                         <tr class="btn-dark">
                             <th>Description</th>
                             <th>Qty</th>
                             <th>Excl. Price</th>
                             <th>VAT%</th>
                             <th class="text-right">Exclusive Total</th>
                             <th class="text-right">Inclusive Total</th>
                         </tr>
                         </thead>
                         <tbody>
                         @forelse($timesheets as $key => $timesheet)
                             @forelse($timesheet->timeline as $timeline)
                                 <tr>
                                     @switch ($conf)
                                        @case("standardinvoice")
                                            <td>Timesheet for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null}} for week {{$timesheet->year_week}} on assignment {{isset($timesheet->project->name)?$timesheet->project->name:null}}</td>
                                            {{--<td>{{isset($timeline->description_of_work)?$timeline->description_of_work:(isset($timeline->task->description)?$timeline->task->description:null)}}</td>--}}
                                     @break;
                                        @case('invoicebytask')
                                            <td>{{(isset($timeline->description_of_work)?$timeline->description_of_work.' - '.(isset($timeline->task)?$timeline->task->description:null):(isset($timeline->task)?$timeline->task->description:null))}}</td>
                                     @break;
                                        @case('invoicebyweek')
                                            <td>Wk{{$timesheet->year_week.' :'.$timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week}}</td>
                                            {{Form::hidden('text_line[]', ('Wk'.$timesheet->year_week.' :'.$timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week))}}
                                     @break;
                                        @default
                                            <td>{{$timesheet->year_week.' - '.(isset($timeline->description_of_work)?$timeline->description_of_work:(isset($timeline->task->description)?$timeline->task->description:null)).': '.(isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null)}}</td>
                                     @endswitch

                                     <td>{{ number_format(($timeline->billable_hours + ($timeline->billable_minutes/60)),2,'.',',') }}</td>
                                     <td>{{ ($timesheet->currency??$currency)." ".number_format((float) ($timesheet->invoice_rate??0),2,'.',',') }}</td>
                                     <td>{{  number_format($vat_rate[$key]).'%' }}</td>
                                     <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format((round(($timeline->billable_hours + ($timeline->billable_minutes/60)) ,2)* (float) ($timesheet->invoice_rate??0)),2,'.',',') }}</td>
                                     <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format((((round(($timeline->billable_hours + ($timeline->billable_minutes/60)),2) * (float) ($timesheet->invoice_rate??0)) * ($vat_rate[$key]/100)) + (round(($timeline->billable_hours + ($timeline->billable_minutes/60)),2) * (float) ($timesheet->invoice_rate??0)) ),2,'.',',') }}</td>
                                 </tr>
                                 @empty
                             @endforelse
                             @empty
                             <tr>
                                 <td colspan="6" class="text-center">Nothing to show here</td>
                             </tr>
                         @endforelse
                         <tr><th class="text-right">TOTAL HOURS</th><th colspan="5">{{round($total_hours,2)}}</th></tr>

                         @forelse($timesheets as $key => $timesheet)
                             @if(count($timesheet->time_exp) > 0)
                                 <tr>
                                     <td colspan="6">&nbsp;</td>
                                 </tr>
                                 <tr class="btn-dark">
                                     <th colspan="6">Expenses</th>
                                 </tr>
                             @endif
                             @forelse($timesheet->time_exp as $index => $expense)
                                 <tr>
                                     <td>Expenses for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null}} for week {{ $timesheet->year_week }} on assignment {{ isset($timesheet->project->name)?$timesheet->project->name:null }} for {{ $expense->description }}.</td>
                                     <td>1</td>
                                     <td>{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                                     <td>0.00%</td>
                                     <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                                     <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                                     {{Form::hidden('time_exp_id[]', $expense->id) }}
                                 </tr>
                                 @empty
                             @endforelse
                         @empty
                         @endforelse
                         <tr>
                             <th colspan="6">&nbsp;</th>
                         </tr>

                         <tr class="btn-dark">
                             <th colspan="6">Summary</th>
                         </tr>
                         <tr>
                             <th>Payment Information</th>
                             <td colspan="3">{{ $invoice_notes }}</td>
                             <th class="text-right">Total Exclusive:</th>
                             <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format(($total + $total_exclusive) ,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="6">{{ $company->company_name }}</td>
                         </tr>
                         <tr>
                             <td colspan="4">{{ $company->bank_name }}</td>
                             <th class="text-right">Total VAT:</th>
                             <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($total_vat,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="6">Branch Code: {{ $company->branch_code }}</td>
                         </tr>
                         <tr>
                             <td colspan="4">Account Number: {{ $company->bank_acc_no }}</td>
                             <th class="text-right">Total:</th>
                             <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($total_vat + $total_exclusive + $total,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="6">Reference: {{ $timesheets[0]->customer->customer_name }}</td>
                         </tr>
                         <tr>
                             <th colspan="6" class="text-center">Thank you for your business!</th>
                         </tr>

                     </tbody>
                 </table>
                     <div class="col-md-12 text-left"><button class="btn btn-dark btn-sm"><i class="far fa-save"></i> Save Invoice</button></div>
             </div>
         </div>

        </div>

        {!! Form::close() !!}
            {!! Form::open(['url' => route('customer.invoice_send', ['send' => 1]), 'method' => 'post', 'class' => 'mb-3']) !!}

            @forelse($timesheets_id as $time)
                {{Form::hidden('time_id[]', $time)}}
            @empty
            @endforelse
            {{Form::hidden('due_date', $due_date) }}
            {{Form::hidden('invoice_ref', $invoice_ref) }}
            {{Form::hidden('invoice_notes', $invoice_notes) }}
            {{Form::hidden('invoice_date', $invoice_date)}}
            {{Form::hidden('customer_id', $timesheets[0]->customer_id)}}
            {{Form::hidden('customer_ref', $customer_ref)}}
            {{Form::hidden('currency', ($timesheets[0]->currency??$currency))}}
            {{Form::hidden('currency_type', $currency_type)}}
            {{Form::hidden('vat_rate_id', $vat_rate_id)}}
            <div class="col-md-4 text-right float-right" style="margin-top: -52px"><a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a></div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                            <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="email-form">
                                <div class="form-group row">
                                    <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-check">
                                    <input type="checkbox" name="customer_email" value="{{isset($timesheets[0]->customer)?strtolower($timesheets[0]->customer->email):''}}" checked class="form-check-input" id="customer_email">
                                    <label class="form-check-label" for="customer_email">{{isset($timesheets[0]->customer)?strtolower($timesheets[0]->customer->email):''}}</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" name="company_email" value="{{$company->email}}" checked class="form-check-input" id="company_email">
                                    <label class="form-check-label" for="company_email">{{$company->email}}</label>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group text-center">
                                    <button class="btn btn-dark btn-sm" id="send-btn"><i class="fas fa-paper-plane"></i> Send Invoice</button>
                                </div>
                            </div>
                            <div id="loader-wrapper">
                                <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section("extra-css")
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endsection

@section("extra-js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })

            $(function () {
                $('#user_email').multiple_emails({
                    position: 'top', // Display the added emails above the input
                    theme: 'bootstrap', // Bootstrap is the default theme
                    checkDupEmail: true // Should check for duplicate emails added
                });

                $('#current_emails').text($('#user_email').val());

                $('#user_email').change( function(){
                    $('#current_emails').text($(this).val());
                });
            })

            $(function () {
                $("#send-btn").on('click', function () {
                    $("#email-form").hide();
                    $("#loader-wrapper").fadeIn();
                    $(".default").hide();
                    $(".sending").fadeIn();
                });
            })
        })
    </script>
@endsection
