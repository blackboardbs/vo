@extends('adminlte.default')

@section('title') View Invoice  @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('customer.invoice')}}" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        {!! Form::open(['url' => route('customer.invoice_save', ['save' => 1]), 'method' => 'post', 'class' => 'mb-3']) !!}

        <div class="row no-gutters">
            <div class="col-md-4">
                {{--<h4>Customer {{ isset($customerInvoice->customer->vat_no)?'Tax':'' }} Invoice</h4>--}}
                <h4>Customer Tax Invoice</h4>
                <h5>{{isset($customerInvoice->company->company_name)?$customerInvoice->company->company_name:''}}</h5>
                <p class="mb-0">{{ isset($customerInvoice->company->postal_address_line1)?$customerInvoice->company->postal_address_line1:'' }}</p>
                <p class="mb-0">{{ isset($customerInvoice->company->postal_address_line2)?$customerInvoice->company->postal_address_line2:'' }}</p>
                @if($customerInvoice->company->postal_address3 !== null)
                    <p class="mb-0">{{ isset($customerInvoice->company->postal_address_line3)?$customerInvoice->company->postal_address_line3:'' }}</p>
                @endif
                <p class="mb-0">{{ isset($customerInvoice->company->state_province)?$customerInvoice->company->state_province:'' }}</p>
                <p class="mb-0">{{ isset($customerInvoice->company->postal_zipcode)?$customerInvoice->company->postal_zipcode:'' }}</p>
                <p class="mb-0">{{ isset($customerInvoice->company->phone)?$customerInvoice->company->phone:'' }}</p>
                <p class="mb-0">{{ isset($customerInvoice->company->email)?$customerInvoice->company->email:'' }}</p>
                <p class="mb-0">VAT NO: {{ isset($customerInvoice->company->vat_no)?$customerInvoice->company->vat_no:'' }}</p>
            </div>
           <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 text-bold">TO:</p>
                @if($customerInvoice->customer != null)
                   <p class="mb-0">{{$customerInvoice->customer->customer_name}}</p>
                   <p class="mb-0">
                   {{$customerInvoice->customer->postal_address_line1}}
                   @empty($customerInvoice->customer->postal_address_line1)
                       {{$customerInvoice->customer->street_address_line1}}
                   @endempty
                   <p class="mb-0">
                       {{$customerInvoice->customer->postal_address_line2}}
                       @empty($customerInvoice->customer->postal_address_line2)
                           {{$customerInvoice->customer->street_address_line2}}
                       @endempty

                   </p>
                   <p class="mb-0">
                       {{$customerInvoice->customer->postal_address_line3}}
                       @empty($customerInvoice->customer->postal_address_line3)
                           {{$customerInvoice->customer->street_address_line3}}
                       @endempty
                   </p>
                   <p class="mb-0">
                       {{$customerInvoice->customer->city_suburb}}
                       @empty($customerInvoice->customer->city_suburb)
                           {{$customerInvoice->customer->street_city_suburb}}
                       @endempty
                   </p>
                   <p class="mb-0">{{$customerInvoice->customer->state_province}}</p>
                   <p class="mb-0">
                       {{$customerInvoice->customer->postal_zipcode}}
                       @empty($customerInvoice->customer->postal_zipcode)
                           {{ $customerInvoice->customer->street_zipcode }}
                       @endempty
                   </p>
                    <p class="mb-0">Vat No: {{ $customerInvoice->customer->vat_no }}</p>
                    <p class="mb-0">Contact: {{$contact_person->name}}</p>
               @endif
            </div>
             <div class="col-md-4">
                 <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> $customerInvoice->company->company_logo])}}" alt="{{$customerInvoice->company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                 <div class="row no-gutter">
                     <div class="col-md-6">Invoice Number:</div>
                     <div class="col-md-6 text-right">{{$customerInvoice->customer_invoice_number}}</div>
                     <div class="col-md-6">Invoice Date:</div>
                     <div class="col-md-6 text-right">{{$customerInvoice->invoice_date}}</div>
                     <div class="col-md-6">Due Date:</div>
                     <div class="col-md-6 text-right">{{ $customerInvoice->due_date }}</div>
                     <div class="col-md-6">Page:</div>
                     <div class="col-md-6 text-right">1/1</div>
                     <div class="col-md-4">Customer Reference:</div>
                     <div class="col-md-8 text-right">{{$customerInvoice->cust_inv_ref}}</div>
                     <div class="col-md-6">Sales Rep:</div>
                     <div class="col-md-6 text-right">{{ isset($customerInvoice->customer->accountm) ? $customerInvoice->customer->accountm->first_name.' '.$customerInvoice->customer->accountm->last_name : '' }}</div>
                 </div>
             </div>
             <div class="col-md-12 border-bottom border-dark pb-3"></div>
             <div class="table-responsive">
                 <div class="table-responsive col-12 px-0">
                     <table class="table table-sm table-hover">
                         <thead>
                         <tr class="btn-dark">
                             <th>Description</th>
                             <th class="text-right">Qty</th>
                             <th class="text-right">Excl. Price</th>
                             <th class="text-right">Discount Amount</th>
                             <th class="text-right">VAT%</th>
                             <th class="text-right">Exclusive Total</th>
                             <th class="text-right">Inclusive Total</th>
                         </tr>
                         </thead>
                         <tbody>
                         @php
                             $total_quantity = 0;
                             $total_exclusive = 0;
                             $total_discount = 0;
                             $total_vat = 0;
                             $total_invoice_amount = 0;
                         @endphp
                         @forelse($customerInvoiceItems as $customerInvoiceItem)
                             @php
                                 $total_quantity += $customerInvoiceItem->quantity;
                                 $total_exclusive += ($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount;
                                 $total_vat += (($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100);
                                 $total_invoice_amount += (($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) + ((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100));
                             @endphp
                             <tr>
                                 <td>{{$customerInvoiceItem->description}}</td>
                                 <td class="text-right">{{$customerInvoiceItem->quantity}}</td>
                                 <td class="text-right">{{number_format($customerInvoiceItem->price_excl,2,'.',',') }}</td>
                                 <td class="text-right">{{number_format($customerInvoiceItem->discount_amount,2,'.',',') }}</td>
                                 <td class="text-right">{{number_format($customerInvoiceItem->vat_percentage,2,'.',',')}}</td>
                                 <td class="text-right">{{number_format(($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount,2,'.',',')}}</td>
                                 <td class="text-right">{{number_format((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) + ((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100)),2,'.',',')}}</td>
                             </tr>
                         @empty
                            <tr>
                                <td colspan="6">No invoice items found.</td>
                            </tr>
                         @endforelse
                         <tr><th class="text-right">TOTAL</th><th colspan="6">{{number_format($total_quantity,2,'.',',')}}</th></tr>
                         {{--@forelse($timesheets as $key => $timesheet)
                             @if(count($timesheet->time_exp) > 0)
                                 <tr>
                                     <td colspan="6">&nbsp;</td>
                                 </tr>
                                 <tr class="btn-dark">
                                     <th colspan="6">Expenses</th>
                                 </tr>
                             @endif
                             @forelse($timesheet->time_exp as $index => $expense)
                                 <tr>
                                     <td>Expenses for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null}} for week {{ $timesheet->year_week }} on assignment {{ isset($timesheet->project->name)?$timesheet->project->name:null }} for {{ $expense->description }}.</td>
                                     <td>1</td>
                                     <td>{{ number_format($expense->amount,2,'.',',') }}</td>
                                     <td>0.00%</td>
                                     <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                     <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                                     {{Form::hidden('time_exp_id[]', $expense->id) }}
                                 </tr>
                                 @empty
                             @endforelse
                         @empty
                         @endforelse--}}
                         <tr>
                             <th colspan="7">&nbsp;</th>
                         </tr>

                         <tr class="btn-dark">
                             <th colspan="7">Summary</th>
                         </tr>
                         <tr>
                             <th>Payment Information</th>
                             <td colspan="4">{{ $customerInvoice->invoice_notes }}</td>
                             <th class="text-right">Total Exclusive:</th>
                             <td class="text-right">{{$customerInvoice->currency}} {{ number_format(($total_exclusive) ,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="7">{{ $customerInvoice->company?->company_name }}</td>
                         </tr>
                         <tr>
                             <td colspan="5">{{ $customerInvoice->company?->bank_name }}</td>
                             <th class="text-right">Total VAT:</th>
                             <td class="text-right">{{$customerInvoice->currency}} {{ number_format($total_vat,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="7">Branch Code: {{ $customerInvoice->company?->branch_code }}</td>
                         </tr>
                         <tr>
                             <td colspan="5">Account Number: {{ $customerInvoice->company?->bank_acc_no }}</td>
                             <th class="text-right">Total:</th>
                             <td class="text-right">{{$customerInvoice->currency}} {{ number_format($total_invoice_amount,2,'.',',') }}</td>
                         </tr>
                         <tr>
                             <td colspan="7">Reference: {{ $customerInvoice->customer?->customer_name }}</td>
                         </tr>
                         <tr>
                             <th colspan="7" class="text-center">Thank you for your business!</th>
                         </tr>

                     </tbody>
                 </table>
             </div>
         </div>
    </div>

    {!! Form::close() !!}
        <div class="col-md-12 col-print-12">
            @if(request()->recurring)
                <div class="row">
                    <div class="col-md-6 text-left">
                        {!! Form::open(['url' => route('customer.invoice_unallocate', $customerInvoice->id), 'method' => 'post', 'class' => 'mb-3 cancelInvoice']) !!}
                        <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{route('customer.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fa fa-clock-o"></i> Done</a>
                    </div>
                </div>
            @elseif($customerInvoice->bill_status == 1)
                <div class="row">
                    <div class="col-md-3" style="max-width:20%">
                        {!! Form::open(['url' => route('customer.invoice_unallocate', $customerInvoice->id), 'method' => 'post', 'class' => 'mb-3 cancelInvoice']) !!}
                        <button type="submit" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-ban"></i> Cancel Invoice</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-3 text-center" style="max-width:20%">
                        <a href="{{route('customer.invoice_payment', $customerInvoice->id)}}" class="btn btn-sm bg-dark d-print-none"><i class="fas fa-university"></i> Process Payment</a>
                    </div>
                    <div class="col-md-3 text-center" style="max-width:20%">
                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                    </div>
                    
                    <div class="col-md-3 text-center" style="max-width:20%">
                                        <a href="{{route('customer.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-save"></i> Save Invoice</a>
                                        </div>
                    <div class="col-md-3 text-right" style="max-width:20%">
                        <a href="{{route('nontimesheetinvoice.generate', ['invoice_id' => $customerInvoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-4 text-left">
                        <a href="#" class="btn btn-sm bg-dark d-print-none" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Invoice</a>
                    </div>
                    
                    <div class="col-md-4 text-center">
                                        <a href="{{route('customer.invoice')}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-save"></i> Save Invoice</a>
                                        </div>
                    <div class="col-md-4 text-right">
                            <a href="{{route('nontimesheetinvoice.generate', ['invoice_id' => $customerInvoice->id, 'print' => 1])}}" class="btn btn-sm btn-dark d-print-none"><i class="fas fa-print"></i> Print</a>
                    </div>
                </div>
            @endif

        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                        <h5 class="modal-title sending" id="exampleModalLabel" style="display: none">Sending your invoice...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="email-form">
                            {!! Form::open(['url' => route('nontimesheetinvoice.send', $customerInvoice), 'method' => 'post']) !!}
                            <div class="form-group row">
                                <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="subject" class="form-control form-control-sm" id="subject" placeholder="Email Subject">
                                </div>
                            </div>
                            <hr>
                            @if(isset($customerInvoice->customer->email))
                            <div class="form-check">
                                <input type="checkbox" name="customer_email" value="{{isset($customerInvoice->customer)?strtolower($customerInvoice->customer->email):''}}" checked class="form-check-input" id="customer_email">
                                <label class="form-check-label" for="customer_email">{{isset($customerInvoice->customer)?strtolower($customerInvoice->customer->email):''}}</label>
                            </div>
                            @endif
                            @if(trim($customerInvoice->company->email) != '')
                            <div class="form-check">
                                <input type="checkbox" name="company_email" value="{{$customerInvoice->company->email}}" checked class="form-check-input" id="company_email">
                                <label class="form-check-label" for="company_email">{{$customerInvoice->company->email}}</label>
                            </div>
                            @endif
                            <hr>
                            <div class="form-group row">
                                <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group text-center">
                                {!! Form::submit('Send Invoice', ['class' => 'btn btn-sm btn-dark', 'id' => 'send-btn']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="loader-wrapper">
                            <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("extra-css")
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
        #loader-wrapper{
            display: none;
        }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endsection

@section("extra-js")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })

            $(function () {
                $('#user_email').multiple_emails({
                    position: 'top', // Display the added emails above the input
                    theme: 'bootstrap', // Bootstrap is the default theme
                    checkDupEmail: true // Should check for duplicate emails added
                });

                $('#current_emails').text($('#user_email').val());

                $('#user_email').change( function(){
                    $('#current_emails').text($(this).val());
                });
            })

            $(function () {
                $("#send-btn").on('click', function () {
                    $("#email-form").hide();
                    $("#loader-wrapper").fadeIn();
                    $(".default").hide();
                    $(".sending").fadeIn();
                });
            })
        })
    </script>
@endsection
