<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,html{font-family:arial,sans-serif;font-size: 0.9rem}.container{width:100%;margin:0 auto}.cols-3{width:31.3%;display:inline-block}.p-x{padding-left:1rem;padding-right:1rem}.p-y{padding-top:1rem;padding-bottom:1rem}.pull-right{float:right}.text-right{text-align:right}.text-left{text-align:left}.clearfix{clear:both}.table{border-collapse:collapse;width:100%}td,th{border-bottom:1px solid #ddd;padding-top:14px;padding-bottom:14px}tr:hover{background-color:#ddd}.bg-dark,.bg-dark:hover{color:#fff;background-color:#343a40!important;border-color:#343a40;box-shadow:0 1px 1px rgba(0,0,0,.075)}hr{border-top:1px solid #ddd;margin-top:10px}p{margin:5px 0}
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container container-title" style="padding-top: 15px">
        {{--<h2>Customer {{isset($customerInvoice->customer->vat_no)?'Tax':''}} Invoice</h2>--}}
        <h2>Customer Tax Invoice</h2>
    </div>
    <div class="container">
        <hr />

        <div class="cols-3 p-y">
            <h4>{{$customerInvoice->company->company_name}}</h4>
            <p>{{ $customerInvoice->company->postal_address_line1 }}</p>
             {!! isset($customerInvoice->company->postal_address_line2) ? '<p>'.$customerInvoice->company->postal_address_line2.'</p>':''  !!}
            @if($customerInvoice->company->postal_address3 !== null)
                <p class="mb-0">{{ $customerInvoice->company->postal_address_line3 }}</p>
            @endif
            <p>{{$customerInvoice->company->state_province}}</p>
            <p>{{ $customerInvoice->company->postal_zipcode }}</p>
            <p>{{ $customerInvoice->company->phone }}</p>
            <p>{{ $customerInvoice->company->email }}</p>
            <p>VAT NO: {{ $customerInvoice->company->vat_no }}</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <div class="cols-3 p-x p-y">
            
            <h4>TO:</h4>
            @if($customerInvoice->customer != null)
                <p>{{$customerInvoice->customer->customer_name}}</p>
                <p>{{isset($customerInvoice->customer->postal_address_line1) ? $customerInvoice->customer->postal_address_line1 : $customerInvoice->customer->street_address_line1}}</p>
                <p>{{isset($customerInvoice->customer->postal_address_line2) ? $customerInvoice->customer->postal_address_line2 : $customerInvoice->customer->street_address_line2}}</p>
                <p>{{isset($customerInvoice->customer->postal_address_line3) ? $customerInvoice->customer->postal_address_line3 : $customerInvoice->customer->street_address_line3}}</p>
                <p>{{isset($customerInvoice->customer->city_suburb) ? $customerInvoice->customer->city_suburb : $customerInvoice->customer->street_city_suburb}}</p>
                <p>{{$customerInvoice->customer->state_province}}</p>
                <p>{{isset($customerInvoice->customer->postal_zipcode) ? $customerInvoice->customer->postal_zipcode : $customerInvoice->customer->street_zipcode}}</p>
                <p>Vat No: {{ $customerInvoice->customer->vat_no }}</p>
                <p>Contact: {{ $contact_person->name }}</p>
            @endif
        </div>
        <div class="cols-3 p-y">
            <div class="row text-right mb-3">
                <div class="inv-img-wrapper" style="margin-left: auto;">
                    @if(is_file(storage_path('/app/avatars/company/'.$customerInvoice->company->company_logo)))
                        <img class="pull-right" src="{{storage_path('/app/avatars/company/'.$customerInvoice->company->company_logo)}}" alt="{{$customerInvoice->company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @else
                        <img class="pull-right" src="{{public_path('/assets/consulteaze_logo.png')}}" alt="{{$customerInvoice->company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @endif

                </div>
            </div>
            <div class="clearfix"></div>
            <p>Invoice Number:<span class="text-right pull-right">{{ $customerInvoice->customer_invoice_number??$customerInvoice->id }}</span></p>
            <p>Invoice Date: <span class="text-right pull-right">{{$customerInvoice->invoice_date}}</span></p>
            <p>Due Date: <span class="text-right pull-right">{{$customerInvoice->due_date}}</span></p>
            <p>Customer Reference: <span class="pull-right text-right">{{$customerInvoice->cust_inv_ref}}</span></p>
            <p>Sales Rep: <span class="pull-right text-right">{{ isset($customerInvoice->customer->accountm) ? $customerInvoice->customer->accountm->first_name.' '.$customerInvoice->customer->accountm->last_name : '' }}</span></p>
        </div>

        <div class="p-y">
            <table class="table">
                <thead class="bg-dark">
                    <tr>
                        <th class="text-left">Description</th>
                        <th class="text-right" style="width: 80px">Qty</th>
                        <th class="text-right" style="width: 80px">Excl.<br>Price</th>
                        <th class="text-right" style="width: 80px">Discount<br>Amount</th>
                        <th class="text-right"  style="width: 80px">VAT%</th>
                        <th class="text-right"  style="width: 120px">Exclusive<br>Total</th>
                        <th class="text-right"  style="width: 120px">Inclusive<br>Total</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total_quantity = 0;
                    $total_exclusive = 0;
                    $total_vat = 0;
                    $total_invoice_amount = 0;
                @endphp
                @forelse($customerInvoiceItems as $customerInvoiceItem)
                    @php
                        $total_quantity += $customerInvoiceItem->quantity;
                        $total_exclusive += $customerInvoiceItem->price_excl * $customerInvoiceItem->quantity;
                        $total_vat += (($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100);
                        $total_invoice_amount += (($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) + ((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100));
                    @endphp
                    <tr>
                        <td>{{$customerInvoiceItem->description}}</td>
                        <td class="text-right">{{number_format($customerInvoiceItem->quantity,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($customerInvoiceItem->price_excl,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($customerInvoiceItem->discount_amount,2,'.',',')}}</td>
                        <td class="text-right">{{number_format($customerInvoiceItem->vat_percentage,2,'.',',')}}</td>
                        <td class="text-right">{{number_format(($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount,2,'.',',')}}</td>
                        <td class="text-right">{{number_format((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) + ((($customerInvoiceItem->price_excl * $customerInvoiceItem->quantity)-$customerInvoiceItem->discount_amount) * ($customerInvoiceItem->vat_percentage / 100)) ,2,'.',',')}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No invoice items found.</td>
                    </tr>
                @endforelse
                <tr>
                    <th class="text-left">TOTAL</th><th class="text-right">{{number_format($total_quantity,2,'.',',')}}</th><td colspan="5">&nbsp;</td>
                </tr>
                {{--@forelse($timesheets as $key => $timesheet)
                    @if(count($timesheet->time_exp) > 0)
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr class="btn-dark">
                            <th colspan="6">Expenses</th>
                        </tr>
                    @endif
                    @forelse($timesheet->time_exp as $index => $expense)
                        <tr>
                            <td>Expenses for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null}} for week {{ $timesheet->year_week }} on assignment {{ isset($timesheet->project->name)?$timesheet->project->name:null }} for {{ $expense->description }}.</td>
                            <td>1</td>
                            <td>{{ number_format($expense->amount,2,'.',',') }}</td>
                            <td>0.00%</td>
                            <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                            <td class="text-right">{{ 'R '.number_format($expense->amount,2,'.',',') }}</td>
                            {{Form::hidden('time_exp_id[]', $expense->id) }}
                        </tr>
                    @empty
                    @endforelse
                @empty
                @endforelse--}}

                </tbody>
            </table>
            <table class="table">
                <tbody>
                <tr>
                    <th colspan="6">&nbsp;</th>
                </tr>

                <tr class="btn-dark">
                    <th colspan="6" class="bg-dark">Summary</th>
                </tr>
                <tr>
                    <th class="text-left p-x">Payment Information</th>
                    <td colspan="3">{{ $customerInvoice->invoice_notes }}</td>
                    <th class="text-right">Total Exclusive:</th>
                    <td class="text-right p-x">{{$customerInvoice->currency}} {{ number_format(($total_exclusive) ,2,'.',',') }}</td>
                </tr>
                <tr>
                    <td colspan="6" class="p-x">{{ $customerInvoice->company->company_name }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="p-x">{{ $customerInvoice->company->bank_name }}</td>
                    <th class="text-right">Total VAT:</th>
                    <td class="text-right p-x">{{$customerInvoice->currency}} {{ number_format($total_vat,2,'.',',') }}</td>
                </tr>
                <tr>
                    <td colspan="6" class="p-x">Branch Code: {{ $customerInvoice->company->branch_code }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="p-x">Account Number: {{ $customerInvoice->company->bank_acc_no }}</td>
                    <th class="text-right">Total:</th>
                    <td class="text-right p-x">{{$customerInvoice->currency}} {{ number_format($total_invoice_amount,2,'.',',') }}</td>
                    {{ Form::hidden('invoice_value', ($total_invoice_amount)) }}
                </tr>
                <tr>
                    <td colspan="6" class="p-x">Reference: {{ $customerInvoice->customer->customer_name }}</td>
                </tr>
                <tr>
                    <th colspan="6" class="text-center">Thank you for your business!</th>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
</body>
</html>
