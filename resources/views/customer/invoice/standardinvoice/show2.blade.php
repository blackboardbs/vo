<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,html{font-family:arial,sans-serif;font-size: 0.9rem}.container{width:100%;margin:0 auto}.cols-3{width:31.3%;display:inline-block}.p-x{padding-left:1rem;padding-right:1rem}.p-y{padding-top:1rem;padding-bottom:1rem}.pull-right{float:right}.text-right{text-align:right}.text-left{text-align:left}.clearfix{clear:both}.table{border-collapse:collapse;width:100%}td,th{border-bottom:1px solid #ddd;padding-top:14px;padding-bottom:14px}tr:hover{background-color:#ddd}.bg-dark,.bg-dark:hover{color:#fff;background-color:#343a40!important;border-color:#343a40;box-shadow:0 1px 1px rgba(0,0,0,.075)}hr{border-top:1px solid #ddd;margin-top:10px}p{margin:5px 0}
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container container-title" style="padding-top: 15px">
        {{--<h2>Customer {{isset($timesheets[0]->customer->vat_no)?'Tax':''}} Invoice</h2>--}}
        <h2>Customer Tax Invoice</h2>
    </div>
    <div class="container">
        <hr />

        <div class="cols-3 p-y">
            <h4>{{$company->company_name}}</h4>
            <p>{{ $company->postal_address_line1 }}</p>
             {!! isset($company->postal_address_line2) ? '<p>'.$company->postal_address_line2.'</p>':''  !!}
            @if($company->postal_address3 !== null)
                <p class="mb-0">{{ $company->postal_address_line3 }}</p>
            @endif
            <p>{{$company->state_province}}</p>
            <p>{{ $company->postal_zipcode }}</p>
            <p>{{ $company->phone }}</p>
            <p>{{ $company->email }}</p>
            <p>VAT NO: {{ $company->vat_no }}</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <div class="cols-3 p-x p-y">
            
            <h4>TO:</h4>
            @if($timesheets[0]->customer != null)
                <p>{{$timesheets[0]->customer->customer_name}}</p>
                <p>{{isset($timesheets[0]->customer->postal_address_line1) ? $timesheets[0]->customer->postal_address_line1 : $timesheets[0]->customer->street_address_line1}}</p>
                <p>{{isset($timesheets[0]->customer->postal_address_line2) ? $timesheets[0]->customer->postal_address_line2 : $timesheets[0]->customer->street_address_line2}}</p>
                <p>{{isset($timesheets[0]->customer->postal_address_line3) ? $timesheets[0]->customer->postal_address_line3 : $timesheets[0]->customer->street_address_line3}}</p>
                <p>{{isset($timesheets[0]->customer->city_suburb) ? $timesheets[0]->customer->city_suburb : $timesheets[0]->customer->street_city_suburb}}</p>
                <p>{{$timesheets[0]->customer->state_province}}</p>
                <p>{{isset($timesheets[0]->customer->postal_zipcode) ? $timesheets[0]->customer->postal_zipcode : $timesheets[0]->customer->street_zipcode}}</p>
                <p>Vat No: {{ $timesheets[0]->customer->vat_no }}</p>
                <p>Contact: {{ $contact_person->name }}</p>
            @endif
        </div>
        <div class="cols-3 p-y">
            <div class="row text-right mb-3">
                <div class="inv-img-wrapper" style="margin-left: auto;">
                @if(is_file(storage_path('/app/avatars/company/'.$company->company_logo)))
                        <img class="pull-right" src="{{storage_path('/app/avatars/company/'.$company->company_logo)}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @else
                        <img class="pull-right" src="{{public_path('/assets/consulteaze_logo.png')}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <p>Invoice Number:<span class="text-right pull-right">{{$invoice_number}}</span></p>
            <p>Invoice Date: <span class="text-right pull-right">{{$date}}</span></p>
            <p>Due Date: <span class="text-right pull-right">{{$due_date}}</span></p>
            <p>Customer Reference: <span class="pull-right text-right">{{$customer_ref}}</span></p>
            <p>Sales Rep: <span class="pull-right text-right">{{ isset($timesheets[0]->customer->accountm) ? $timesheets[0]->customer->accountm->first_name.' '.$timesheets[0]->customer->accountm->last_name : '' }}</span></p>
        </div>

        <div class="p-y">
            <table class="table">
                <thead class="bg-dark">
                    <tr>
                        <th>Description</th>
                        <th class="text-right" style="width: 80px">Qty</th>
                        <th class="text-right" style="width: 80px">Excl.<br>Price</th>
                        <th class="text-right"  style="width: 80px">VAT%</th>
                        <th class="text-right"  style="width: 120px">Exclusive<br>Total</th>
                        <th class="text-right"  style="width: 120px">Inclusive<br>Total</th>
                    </tr>
                </thead>
                <tbody>

                @forelse($timesheets as $key => $timesheet)
                    @forelse($timesheet->timeline as $timeline)
                        <tr>
                            @switch ($conf)
                                @case("standardinvoice")
                                    <td>Timesheet for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->first_name:null}} for week {{$timesheet->year_week}} on assignment {{isset($timesheet->project->name)?$timesheet->project->name:null}}</td>
                                   {{-- <td>{{isset($timeline->description_of_work)?$timeline->description_of_work:(isset($timeline->task->description)?$timeline->task->description:null)}}</td>--}}
                                    @break;
                                @case('invoicebytask')
                                    <td>{{(isset($timeline->description_of_work)?$timeline->description_of_work.' - '.(isset($timeline->task)?$timeline->task->description:null):(isset($timeline->task)?$timeline->task->description:null))}}</td>
                                    @break;
                                @case('invoicebyweek')
                                    <td>Wk{{$timesheet->year_week.' :'.$timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week}}</td>
                                    {{Form::hidden('text_line[]', ('Wk'.$timesheet->year_week.' :'.$timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week))}}
                                    @break;
                                @default
                                    <td>{{$timesheet->year_week.' - '.(isset($timeline->description_of_work)?$timeline->description_of_work:(isset($timeline->task->description)?$timeline->task->description:null)).': '.(isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null)}}</td>
                            @endswitch

                            <td>{{ number_format(($timeline->billable_hours + ($timeline->billable_minutes/60)),2,'.',',') }}</td>
                            <td>{{ ($timesheet->currency??$currency)." ".number_format($timesheet->invoice_rate,2,'.',',') }}</td>
                            <td>{{  $vat_rate[$key].'%' }}</td>
                            <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format((($timeline->billable_hours + ($timeline->billable_minutes/60)) * $timesheet->invoice_rate),2,'.',',') }}</td>
                            <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format((((($timeline->billable_hours + ($timeline->billable_minutes/60)) * $timesheet->invoice_rate) * ($vat_rate[$key]/100)) + (($timeline->billable_hours + ($timeline->billable_minutes/60)) * $timesheet->invoice_rate) ),2,'.',',') }}</td>
                        </tr>
                    @empty
                    @endforelse
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Nothing to show here</td>
                    </tr>
                @endforelse
                <tr><th class="text-right">TOTAL HOURS</th><th>{{number_format($total_hours)}}</th><th colspan="4"></th></tr>
                @forelse($timesheets as $key => $timesheet)
                    @if(count($timesheet->time_exp) > 0)
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr class="btn-dark">
                            <th colspan="6">Expenses</th>
                        </tr>
                    @endif
                    @forelse($timesheet->time_exp as $index => $expense)
                        <tr>
                            <td>Expenses for {{isset($timesheet->employee)?$timesheet->employee->first_name.' '.$timesheet->employee->last_name:null}} for week {{ $timesheet->year_week }} on assignment {{ isset($timesheet->project->name)?$timesheet->project->name:null }} for {{ $expense->description }}.</td>
                            <td>1</td>
                            <td>{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                            <td>0.00%</td>
                            <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                            <td class="text-right">{{ ($timesheet->currency??$currency)." ".number_format($expense->amount,2,'.',',') }}</td>
                            {{Form::hidden('time_exp_id[]', $expense->id) }}
                        </tr>
                    @empty
                    @endforelse
                @empty
                @endforelse

                </tbody>
            </table>
            <table class="table">
                <tbody>
                <tr>
                    <th colspan="6">&nbsp;</th>
                </tr>

                <tr class="btn-dark">
                    <th colspan="6" class="bg-dark">Summary</th>
                </tr>
                <tr>
                    <th class="text-left p-x">Payment Information</th>
                    <td colspan="3">{{ $invoice_notes }}</td>
                    <th class="text-right">Total Exclusive:</th>
                    <td class="text-right p-x">{{ ($timesheet->currency??$currency)." ".number_format(($total + $total_exclusive) ,2,'.',',') }}</td>
                </tr>
                <tr>
                    <td colspan="6" class="p-x">{{ $company->company_name }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="p-x">{{ $company->bank_name }}</td>
                    <th class="text-right">Total VAT:</th>
                    <td class="text-right p-x">{{ ($timesheet->currency??$currency)." ".number_format($total_vat,2,'.',',') }}</td>
                </tr>
                <tr>
                    <td colspan="6" class="p-x">Branch Code: {{ $company->branch_code }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="p-x">Account Number: {{ $company->bank_acc_no }}</td>
                    <th class="text-right">Total:</th>
                    <td class="text-right p-x">{{ ($timesheet->currency??$currency)." ".number_format($total_vat + $total_exclusive + $total,2,'.',',') }}</td>
                    {{ Form::hidden('invoice_value', ($total_vat + $total_exclusive + $total)) }}
                </tr>
                <tr>
                    <td colspan="6" class="p-x">Reference: {{ $timesheets[0]->customer->customer_name }}</td>
                </tr>
                <tr>
                    <th colspan="6" class="text-center">Thank you for your business!</th>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
</body>
</html>
