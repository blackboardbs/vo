<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,html{font-family:arial,sans-serif;font-size: 0.9rem}.container{width:100%;margin:0 auto}.cols-3{width:31.3%;display:inline-block}.p-x{padding-left:1rem;padding-right:1rem}.p-y{padding-top:1rem;padding-bottom:1rem}.pull-right{float:right}.text-right{text-align:right}.text-left{text-align:left}.clearfix{clear:both}.table{border-collapse:collapse;width:100%}td,th{border-bottom:1px solid #ddd;padding-top:14px;padding-bottom:14px}tr:hover{background-color:#ddd}.bg-dark,.bg-dark:hover{color:#fff;background-color:#343a40!important;border-color:#343a40;box-shadow:0 1px 1px rgba(0,0,0,.075)}hr{border-top:1px solid #ddd;margin-top:10px}p{margin:5px 0}
        .inv-img-wrapper img, .img-wrapper img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin: 0 auto;
        }
        .right-side{
            display: inline-block;
            width: 49%;
            margin-bottom: 5px;
        }
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container container-title" style="padding-top: 15px">
        {{--<h2>Customer {{isset($timesheets[0]->customer->vat_no)?'Tax':''}} Invoice</h2>--}}
        <h2>Customer Tax Invoice</h2>
    </div>
    <div class="container">
        <hr />

        <div class="cols-3 p-y">
            <h4>{{$company->company_name}}</h4>
            <p>{{ $company->postal_address_line1 }}</p>

            @if($company->postal_address2 !== null)
                <p>{{ $company->postal_address_line2 }}</p>
            @endif
            @if($company->postal_address3 !== null)
                <p class="mb-0">{{ $company->postal_address_line3 }}</p>
            @endif
            <p>{{$company->state_province}}</p>
            <p>{{ $company->postal_zipcode }}</p>
            <p>{{ $company->phone }}</p>
            <p>{{ $company->email }}</p>
            <p>VAT NO: {{ $company->vat_no }}</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <div class="cols-3 p-x p-y">
            
            <h4>TO:</h4>
                <p>{{isset($customer_invoice->customer->customer_name) ? $customer_invoice->customer->customer_name : '<CustomerName>'}}</p>
                <p>{{isset($customer_invoice->customer->postal_address_line1) ? $customer_invoice->customer->postal_address_line1 : ($customer_invoice->customer->street_address_line1??null)}}</p>
                <p>{{isset($customer_invoice->customer->postal_address_line2) ? $customer_invoice->customer->postal_address_line2 : ($customer_invoice->customer->street_address_line2??null)}}</p>
                <p>{{isset($customer_invoice->customer->postal_address_line3) ? $customer_invoice->customer->postal_address_line3 : ($customer_invoice->customer->street_address_line3??null)}}</p>
                <p>{{isset($customer_invoice->customer->city_suburb) ? $customer_invoice->customer->city_suburb : ($customer_invoice->customer->street_city_suburb??null)}}</p>
                <p>{{isset($customer_invoice->customer->state_province) ? $customer_invoice->customer->state_province : ''}}</p>
                <p>{{isset($customer_invoice->customer->postal_zipcode) ? $customer_invoice->customer->postal_zipcode : ($customer_invoice->customer->street_zipcode??null)}}</p>
                <p>Vat No: {{ isset($customer_invoice->customer->vat_no) ? $customer_invoice->customer->vat_no : '' }}</p>
                <p>Contact: {{ isset($customer_invoice->customer) ? $customer_invoice->customer->contact_firstname.' '.$customer_invoice->customer->contact_lastname : '' }}</p>
        </div>
        <div class="cols-3 p-y">
            {{--<div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{public_path('storage/app/avatars/company/'.(isset($company->company_logo)?$company->company_logo:public_path('/assets/consulteaze_logo.png')))}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>--}}
            <div class="row text-right mb-3">
                <div class="inv-img-wrapper" style="margin-left: auto;">
                @if(is_file(storage_path('/app/avatars/company/'.$company->company_logo)))
                        <img class="pull-right" src="{{storage_path('/app/avatars/company/'.$company->company_logo)}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @else
                        <img class="pull-right" src="{{public_path('/assets/consulteaze_logo.png')}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right">
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <p>Invoice Number:<span class="text-right pull-right">{{ $customer_invoice->customer_invoice_number??$customer_invoice->id }}</span></p>
            <p>Invoice Date: <span class="text-right pull-right">{{$customer_invoice->invoice_date}}</span></p>
            <p>Due Date: <span class="text-right pull-right">{{ $customer_invoice->due_date }}</span></p>
            <p>Customer Reference: <span class="pull-right text-right">{{$customer_invoice->cust_inv_ref}}</span></p>
            <p>Sales Rep: <span class="pull-right text-right">{{ isset($customer_invoice->customer->accountm) ? $customer_invoice->customer->accountm->first_name.' '.$customer_invoice->customer->accountm->last_name : '' }}</span></p>
        </div>

        <div class="p-y">
            <table class="table">
                <thead class="bg-dark">
                    <tr cl>
                        <th class="text-right">Description</th>
                        <th class="text-right" style="width: 80px">Qty</th>
                        <th class="text-right" style="width: 80px">Excl.<br>Price</th>
                        <th class="text-right"  style="width: 80px">VAT%</th>
                        <th class="text-right"  style="width: 120px">Exclusive<br>Total</th>
                        <th class="text-right"  style="width: 120px">Inclusive<br>Total</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($customer_invoice_lines as $key => $customer_invoice_line)
                    <tr>
                        <td>{!! $customer_invoice_line->line_text !!}</td>
                        <td class="text-right">{{ number_format($customer_invoice_line->quantity, 2, '.', ',') }}</td>
                        <td class="text-right">{{ ($customer_invoice->currency??"ZAR")." ".number_format($customer_invoice_line->price,2,'.',',') }}</td>
                        <td class="text-right">{{ $vat_rate*100 }}%</td>
                        <td class="text-right">{{ ($customer_invoice->currency??"ZAR")." ".number_format($customer_invoice_line->invoice_line_value,2,'.',',') }}</td>
                        <td class="text-right">{{ ($customer_invoice->currency??"ZAR")." ".number_format(($customer_invoice_line->invoice_line_value * (1+$vat_rate)),2,'.',',') }}</td>
                    </tr>
                @empty
                @endforelse
                <tr>
                    <th class="text-right">TOTAL HOURS &nbsp; &nbsp; </th>
                    <th class="text-right">{{number_format($total_hours,2,'.',',')}}</th>
                    <th colspan="4"></th>
                </tr>
                @if(count($customer_invoice_line_exp) > 0)
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr class="btn-dark">
                        <th colspan="6">Expenses</th>
                    </tr>
                @endif
                @forelse($customer_invoice_line_exp as $index => $expense)
                    @isset($expense)
                        <tr>
                            <td>{!! $expense->line_text !!}</td>
                            <td>1</td>
                            <td>{{ ($customer_invoice->currency??"ZAR")." ".$expense->amount }}</td>
                            <td>0.00%</td>
                            <td class="text-right">{{ ($customer_invoice->currency??"ZAR")." ".number_format( $expense->amount,2,'.',',') }}</td>
                            <td class="text-right">{{ ($customer_invoice->currency??"ZAR")." ".number_format( $expense->amount,2,'.',',') }}</td>
                        </tr>
                    @endisset
                @empty
                @endforelse
                </tbody>
            </table>
            <table class="table">
                <tbody>
                    <tr>
                        <th colspan="6">&nbsp;</th>
                    </tr>

                    <tr class="btn-dark">
                        <th colspan="6" class="bg-dark">Summary</th>
                    </tr>
                    <tr>
                        <th class="text-left p-x">Payment Information</th>
                        <td colspan="3">{{ $customer_invoice->invoice_notes }}</td>
                        <th class="text-right">Total Exclusive:</th>
                        <td class="text-right p-x">{{ ($customer_invoice->currency??"ZAR")." ".number_format(($total_value_line + $total_expenses) ,2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="p-x">{{ $company->company_name }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="p-x">{{ $company->bank_name }}</td>
                        <th class="text-right">Total VAT:</th>
                        <td class="text-right p-x">{{ ($customer_invoice->currency??"ZAR")." ".number_format($total_value_line_tax,2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="p-x">Branch Code: {{ $company->branch_code }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="p-x">Account Number: {{ $company->bank_acc_no }}</td>
                        <th class="text-right">Total:</th>
                        <td class="text-right p-x">{{ ($customer_invoice->currency??"ZAR")." ".number_format(($total_value_line_tax + $total_value_line + $total_expenses),2,'.',',') }}</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="p-x">Reference: {{ isset($customer_invoice->customer->customer_name) ? $customer_invoice->customer->customer_name : '<CustomerName>' }}</td>
                    </tr>
                    <tr>
                        <th colspan="6" class="text-center">Thank you for your business!</th>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
</body>
</html>
