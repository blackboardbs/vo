@extends('adminlte.default')

@section('title') Prepare Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('gen')" class="btn btn-primary ml-1" style="margin-top: -7px;">Generate Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.invoice_generate'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off','id'=>'gen'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Generating Invoice</th>
                </tr>
                </thead>
                <tbody>
                @foreach($timesheet??[] as $item)
                    {{ Form::hidden('time[]', $item) }}
                @endforeach
                <tr>
                    <th>Financial AR Invoice No: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Accounts receivable invoice number from company's Financial system"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('invoice_ref', old('invoice_ref'),['class'=>'form-control form-control-sm'. ($errors->has('invoice_ref') ? ' is-invalid' : ''),'placeholder'=>'Invoice Reference'])}}
                        @foreach($errors->get('invoice_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Invoice Date:</th>
                    <td>{{Form::text('invoice_date',$invoice_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('invoice_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                        @foreach($errors->get('invoice_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>
                    <th>Customer Reference: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Reference received from customer E.g. Order number or project reference"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('customer_ref', old('customer_ref'),['class'=>'form-control form-control-sm'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Reference'])}}
                        @foreach($errors->get('customer_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Payment Due Date:</th>
                    <td>{{Form::text('due_date',$payment_due_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('due_date') ? ' is-invalid' : ''),'placeholder'=>'Payment Due Date'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>
                    <th>Invoice Notes: <span style="cursor: pointer" data-toggle="tooltip" data-placement="right" title="Note to be displayed on invoice footer"><i class="far fa-question-circle"></i></span></th>
                    <td>{{Form::text('invoice_notes',old('Invoice_notes'),['class'=>'form-control form-control-sm'. ($errors->has('Invoice_notes') ? ' is-invalid' : ''),'placeholder'=>'Invoice Notes'])}}
                        @foreach($errors->get('Invoice_notes') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Currency:</th>
                    <td>
                        {{Form::select("currency_type", [0 => "Local", 1 => "Foreign"], 0, ["class" => "form-control form-control-sm"])}}
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <th>Vat Rate:</th>
                    <td>
                        {{Form::select("vat_rate_id", $vat_rate_dropdown, $vat_rate_id, ["class" => "form-control form-control-sm"])}}
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })
    </script>
@endsection