@extends('adminlte.default')
@section('title') Prepare Invoice @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <!-- @if($invoiceTypeID == 1)
                {{Form::open(['url' => route('nontimesheetinvoice.update', $customerInvoice['id']), 'method' => 'PATCH','class'=>'mt-3', 'autocomplete' => 'off', 'id' => 'nontimesheet'])}}
            @else
                {{Form::open(['url' => route('nontimesheetinvoice.update_vendor', $customerInvoice['id']), 'method' => 'PATCH','class'=>'mt-3', 'autocomplete' => 'off', 'id' => 'nontimesheet'])}}
            @endif -->
            <invoice-header-component
                errors="{{$errors}}"
                :old="{{ json_encode($customerInvoice) }}"
                invoicetypeid="{{$invoiceTypeID}}"
                :currencies="{{json_encode($currency_drop_down)}}"
                :recurring="{{request()->input('recurring', 0)}}" 
                :newinvoice="0"
            ></invoice-header-component>
            <!-- {{Form::close()}} -->


            @if($invoiceTypeID == 1)
                {{Form::open(['url' => route('nontimesheetinvoice.generate', $customerInvoice["id"]), 'method' => 'get','class'=>'mt-3', 'autocomplete' => 'off'])}}
            @endif
            @if($invoiceTypeID == 2)
                {{Form::open(['url' => route('nontimesheetinvoice.generate_vendor', $customerInvoice["id"]), 'method' => 'get','class'=>'mt-3', 'autocomplete' => 'off'])}}
            @endif
                <invoice-items
                        :recurring="{{request()->input('recurring', 0)}}"
                        :vat-rates="{{$vat_rates}}"
                        invoice_id="{{$invoiceNumber}}"
                        invoice_type_id="{{$invoiceTypeID}}"
                        :default-vat="{{$default_vat->vat_rate_id}}"
                        :status_id="{{isset($customerInvoice['bill_status'])?$customerInvoice['bill_status']:$customerInvoice->bill_status}}"
                />
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            });
        });
    </script>
@endsection
