@extends('adminlte.default')

@section('title') Process Invoice Payment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary float-right ml-1">Process</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.process_payment'), 'method' => 'put','class'=>'mt-3', 'autocomplete' => 'off','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Payment Process Date</th>
                    <td colspan="3">{{ Form::text('paid_date', $date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('paid_date') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('paid_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                    {{Form::hidden('invoice_id', $id)}}
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endsection