@extends('adminlte.default')

@section('title') Active Timesheets @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('timesheet')" class="btn btn-primary ml-1" style="margin-top: -7px;">Prepare Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        {!! Form::open(['url' => route('customer.invoice_create'), 'id'=>'filters', 'method' => 'get']) !!}
        <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
        <div class="row">
            <input type="hidden" name="s" value="{{(isset($_GET['s']) ? $_GET['s'] : '15')}}" />
            @if(isset($_GET['project']) && $_GET['project'] != 0)
                <div class="col-md-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('client', $customers, (isset($_GET['client']) ? $_GET['client'] : 0), ['class' => 'form-control ', 'id' => 'filter_client']) !!} --}}
                            <select name="client" class="form-control" id="filter_client">
                                <option value="0">All</option>
                                @foreach($customers as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['client']) && $_GET['client'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <select name="project" class="form-control" id="filter_project">
                                <option value="0">All</option>
                                @foreach($project as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['project']) && $_GET['project'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Project</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('employees', $employees, (isset($_GET['employees']) ? $_GET['employees'] : 0), ['class' => 'form-control ', 'id' => 'filter_employee']) !!} --}}
                            <select name="employees" class="form-control" id="filter_employee">
                                <option value="0">All</option>
                                @foreach($employees as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['employees']) && $_GET['employees'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" id="billing-cycle" style="display: none;max-width:16.67% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('billing-period', [], request()->input("billing-period"), ['class' => 'form-control search', 'id' => 'billing-period']) !!} --}}
                            <select name="billing-period" class="form-control" id="billing-period">
                                <option value="0">All</option>
                            </select>
                            <span>Billing Period</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:16.67% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('week', $year_weeks, (isset($_GET['week']) ? $_GET['week'] : 0), ['class' => 'form-control ', 'id' => 'filter_week']) !!}
                            <span>Week</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:16% !important;">
                    <a href="/customer_invoice/create" class="btn btn-info w-100">Clear Filters</a>
                </div>
            @else
                <div class="col-md-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('client', $customers, (isset($_GET['client']) ? $_GET['client'] : 0), ['class' => 'form-control filter_client', 'id' => 'filter_client']) !!} --}}
                            <select name="client" class="form-control" id="filter_client">
                                <option value="0">All</option>
                                @foreach($customers as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['client']) && $_GET['client'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Customer</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('project', $project, (isset($_GET['project']) ? $_GET['project'] : 0), ['class' => 'form-control ', 'id' => 'filter_project']) !!} --}}
                            <select name="project" class="form-control" id="filter_project">
                                <option value="0">All</option>
                                @foreach($project as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['project']) && $_GET['project'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Project</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{-- {!! Form::select('employees', $employees, (isset($_GET['employees']) ? $_GET['employees'] : 0), ['class' => 'form-control ', 'id' => 'filter_employee']) !!} --}}
                            <select name="employees" class="form-control" id="filter_employee">
                                <option value="0">All</option>
                                @foreach($employees as $key => $value)
                                    <option value="{{$key}}" {{isset($_GET['employees']) && $_GET['employees'] == $key ? 'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {!! Form::select('week', $year_weeks, (isset($_GET['week']) ? $_GET['week'] : 0), ['class' => 'form-control ', 'id' => 'filter_week']) !!}
                            <span>Week</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:20% !important;">
                    <a href="/customer_invoice/create" class="btn btn-info w-100">Clear Filters</a>
                </div>
            @endif
        </div>
        {!! Form::close() !!}
        <div class="table-responsive">
            {!! Form::open(['url' => route('customer.invoice_prepare'), 'id' => 'timesheet']) !!}

            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th><input type="checkbox" id="select_all"> @sortablelink('Select All')</th>
                    <th>@sortablelink('Customer Name')</th>
                    <th>@sortablelink('Company')</th>
                    <th>@sortablelink('Resource')</th>
                    <th>@sortablelink('Project')</th>
                    <th>@sortablelink('Year Week')</th>
                    <th>@sortablelink('Week Start Date')</th>
                    <th>@sortablelink('Bill')</th>
                    <th>@sortablelink('No Bill')</th>
                    <th>@sortablelink('Expenses')</th>
                    <th>@sortablelink('Status')</th>
                </tr>
                </thead>
                <tbody>
                    @include('_config.config')
                    @forelse($timesheets as $key => $timesheet)
                        <tr>
                            <td><input type="checkbox" name="timesheet[]" class="timesheet_select" value="{{$timesheet->id}}" {{(session()->has('invoice_timesheets') && in_array($timesheet->id, session('invoice_timesheets')))?"checked":''}} /> {{ $timesheet->id }}</td>
                            <td><a href="{{ route('timeline.show', $timesheet) }}">{{ $timesheet->customer?->customer_name}}</a></td>
                            <td><a href="{{ route('timeline.show', $timesheet) }}">{{ $timesheet->company?->company_name}}</a></td>
                            <td><a href="{{ route('timeline.show', $timesheet) }}">{{ $timesheet->employee?->name()}}</a> </td>
                            <td>{{ $timesheet->project?->name }}</td>
                            <td>{{ $timesheet->year_week }}</td>
                            <td>{{ $timesheet->first_day_of_week }}</td>
                            <td>{{_minutes_to_time(($timesheet->billable_hours * 60) + $timesheet->billable_minutes)}}</td>
                            <td>{{_minutes_to_time(($timesheet->non_billable_hours * 60) + $timesheet->non_billable_minutes)}}</td>
                            <td>@forelse($time_exp[$key] as $expense)
                                    @if($expense != null && $expense->claim_auth_date != null)
                                        <span class="badge badge-success mr-1"> {{ substr(strtoupper($expense->description), 0, 1) }} | <i class="fa fa-check" aria-hidden="true"></i></span>
                                    @else
                                        <span class="badge badge-danger mr-1"> {{ substr(strtoupper($expense->description), 0, 1) }} | <i class="fa fa-times" aria-hidden="true"></i></span>
                                    @endif
                                    @empty
                                @endforelse
                            </td>
                            <td>{{ $timesheet->status?->description }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="18" class="text-center">No open timesheet.</td>
                        </tr>
                    @endforelse
                    @if(count($timesheets) > 0)
                        <tr>
                            <td colspan="6"></td>
                            <th>TOTAL HOURS</th>
                            <th>{{_minutes_to_time($total_billable_minutes)}}</th>
                            <th>{{_minutes_to_time($total_non_billable_minutes)}}</th>
                            <th colspan="2"></th>
                        </tr>
                    @endif
                    
                </tbody>
            </table>
            {!! Form::close() !!}
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $timesheets->firstItem() }} - {{ $timesheets->lastItem() }} of {{ $timesheets->total() }}
                        </td>
                        <td>
                            {{ $timesheets->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {
            if (($("#filter_project").val() || '{{request()->project}}') || $("#filter_project").val() != 0){
                axios.get('/billingcycle-id-invoice/{{request()->project}}')
                    .then(response => {
                        let options = "<option value=''>Select Billing Period</option>";
                        response.data.forEach((v, i) => options += `<option ${getUrlVars()["billing-period"] == v.id ? 'selected':null} value="${v.id}">${v.name}</option>`);
                        $("#billing-period").html(options)
                    })
                    .catch(err => console.log(err.response));

                $("#billing-cycle").css({
                    display: "flex"
                })
            }

            $("#filter_client").on('change', function () {
                $('#filters').submit();
            })

            $('.timesheet_select').each(function (){
                if($(this).is(':checked')){
                    $("#prepare").prop('disabled', false);
                }
            })
        });


        $(() => {
            $('#select_all').on('click',function(){
                let timesheet_id = []
                if(this.checked){
                    $('.timesheet_select').each(function(){
                        this.checked = true;
                        timesheet_id.push(this.value)
                    });
                    session('{{route('session.store')}}', timesheet_id)
                }else{
                    $('.timesheet_select').each(function(){
                        this.checked = false;
                        timesheet_id.push(this.value)
                    });
                    session('{{route('session.pull')}}', timesheet_id)
                }
            });

            $(".timesheet_select").on('change', e => {
                if (e.target.checked){
                    session('{{route('session.store')}}', [e.target.value])
                }else {
                    session('{{route('session.pull')}}', [e.target.value])
                }
            })

            $('.timesheet_select').on('click',function(){
                selectedAll()
            });

            selectedAll()
        })

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }

        function selectedAll(){
            if($('.timesheet_select:checked').length === $('.timesheet_select').length){
                $('#select_all').prop('checked',true);
            }else{
                $('#select_all').prop('checked',false);
            }
        }

        function session(url, timesheets_id){
            axios.post(url, {ids: timesheets_id})
                .then(response => {
                    $("#prepare").prop('disabled', !response.data.ids);
                }).catch(error => {
                console.log(error)
            })
        }

    </script>
@endsection
