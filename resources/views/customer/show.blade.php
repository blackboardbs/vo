@extends('adminlte.default')

@section('title') Customer @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right mr-1"><i class="fa fa-caret-left"></i> Back</button>
                <div class="dropdown">
                    <a class="btn btn-dark dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-file" aria-hidden="true"></i> Documents
                    </a>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('customer.print', $customer->id)}}" target="_blank"><i class="fa fa-print"></i> Print Agreement</a>
                        <a class="dropdown-item" href="{{route('customer.send', $customer->id)}}"><i class="fa fa-envelope"></i> Send Agreement</a>
                        <a class="dropdown-item" href="{{route('customer.print.nda', $customer->id)}}" target="_blank"><i class="fa fa-print"></i> Print NDA</a>
                        <a class="dropdown-item" href="{{route('customer.send.nda', $customer->id)}}"><i class="fa fa-envelope"></i> Send NDA</a>
                    </div>
                </div>
                <a href="{{route('customerpreference.index')}}" class="btn btn-success float-right" style="margin-left:5px;">Preference</a>
                <a href="{{route('customer.edit',$customer->id)}}" class="btn btn-success float-right" style="margin-left:5px;">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark">Customer Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowspan="5">Company Logo:</th>
                        <td rowspan="5"><img src="{{route('customer_avatar',['q'=>$customer->logo])}}" style="max-height: 200px; height: 100%" alt=""></td>
                        <th>Customer Name:</th>
                        <td>{{$customer->customer_name}}</td>
                    </tr>
                    <tr>
                        <th>Registration Number:</th>
                        <td>{{$customer->business_reg_no}}</td>
                    </tr>
                    <tr>
                        <th>VAT Number:</th>
                        <td>{{$customer->vat_no}}</td>
                    </tr>
                    <tr>
                        <th>Account Manager:</th>
                        <td>{{$customer->accountm?->name()}}</td>
                    </tr>
                    <tr>
                        <th>Invoice Contact</th>
                        <td>{{$customer->invoiceContact?->full_name}}</td>
                    </tr>
                    <tr>
                        <th>Contact Firstname:</th>
                        <td>{{$customer->contact_firstname}}</td>
                        <th>Contact Lastname:</th>
                        <td>{{$customer->contact_lastname}}</td>
                    </tr>
                    <tr>
                        <th>Contact Birthday:</th>
                        <td>{{$customer->contact_birthday}}</td>
                        <th>Customer Number:</th>
                        <td>{{$customer->id}}</td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>{{$customer->phone}}</td>
                        <th>Cell:</th>
                        <td>{{$customer->cell}}</td>
                    </tr>
                    <tr>
                        <th>Fax:</th>
                        <td>{{$customer->fax}}</td>
                        <th>Cell 2:</th>
                        <td>{{$customer->cell2}}</td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>{{$customer->email}}</td>
                        <th>Web:</th>
                        <td>{{$customer->web}}</td>
                    </tr>
                    <tr>
                        <th>Postal Address:</th>
                        <td>{{$customer->postal_address_line1}}<br />
                            {{$customer->postal_address_line2}}<br />
                            {{$customer->postal_address_line3}}<br />
                            {{$customer->city_suburb}}<br />
                            {{$customer->postal_zipcode}}
                        </td>
                        <th>Street Address:</th>
                        <td>{{$customer->street_address_line1}}<br />
                            {{$customer->street_address_line2}}<br />
                            {{$customer->street_address_line3}}<br />
                            {{$customer->street_city_suburb}}<br />
                            {{$customer->street_zipcode}}
                        </td>
                    </tr>
                    <tr>
                        <th>State/Province:</th>
                        <td>{{$customer->state_province}}</td>
                        <th>Country:</th>
                        <td>{{$customer->country?->name}}</td>
                    </tr>
                    <tr>
                        <th>Medical Certificate Type:</th>
                        <td>{{$customer->medical_certificate_type > 0 ? $customer->medical_certificate->description : ''}}</td>
                        <th>Medical Certificate Text:</th>
                        <td>{{$customer->medical_certificate_text}}</td>
                    </tr>
                    <tr>
                        <th>Billing Note:</th>
                        <td>{{$customer->billing_note}}</td>
                        <th>Billing Cycle:</th>
                        <td>{{$customer->billingCycle?->name}}</td>
                    </tr>
                    <tr>
                        <th>Payment Terms:</th>
                        <td>{{$customer->payment_terms}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>BBBEE Level:</th>
                        <td>{{$customer->bbbee_level}}</td>
                        <th>BBBEE Certificate Expiry:</th>
                        <td>{{$customer->bbbee_certificate_expiry}}</td>
                    </tr>
                    <tr>
                        <th>BBBEE Level Preference:</th>
                        <td>{{$customer->bbbee_level_preference}}</td>
                        <th>Status:</th>
                        <td>{{$customer->statusd?->description}}</td>
                    </tr>
                    <tr>
                        <th>Placement Agreement %:</th>
                        <td>{{$customer->commission}}</td>
                        <th>Customer Onboarding:</th>
                        <td>{{$customer->customer_onboarding}}</td>
                    </tr>
                    <tr>
                        <th>Appointment Manager:</th>
                        <td>{{$customer->appointmentManager?->name()}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Service Agreement (Service Provider)</th>
                        <td>{{$customer->serviceAgreement?->name}}</td>
                        <th>NDA Template:</th>
                        <td>{{$customer->NDATemplate?->name}}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-borderless table-sm">
                <tbody>
                <tr>
                    <th colspan="5" class="btn-dark">Documents</th>
                </tr>
                <tr>
                    <th>Document</th>
                    <th>File Name</th>
                    <th>Upload Date</th>
                    <th>Uploaded by</th>
                </tr>
                @forelse($customer->documents->merge($documents) as $document)
                    <tr>
                        <td><a href="{{route('provider.agreement', last(explode('/', $document->file)))}}">{{$document->name}}</a></td>
                        <td>{{$document->file}}</td>
                        <td>{{$document->created_at}}</td>
                        <td>{{$document->user?->name()}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No documents found.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>
@endsection
