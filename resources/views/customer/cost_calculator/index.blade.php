@extends('adminlte.default')

@section('title') Cost Calculator @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('costCalculate')" class="btn btn-primary ml-1" style="margin-top: -7px;">Calculate</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>

        <div class="table-responsive">
            @include('_config.config')
            {!! Form::open(['url' => route('cost_calculator.calculate',$commission), 'method' => 'post','class'=>'mt-3','id'=>'costCalculate']) !!}


            <table class="table table-bordered table-sm table-hover mb-0">
                <thead>
                <tr>
                    <th colspan="4"></th>
                    <th colspan="2">+5% Expenses</th>
                    <th colspan="2">+5% Bill Rate</th>
                </tr>
                </thead>
                <tbody>

                    <tr>
                        <th>{!! Form::label('package', 'Package') !!}</th>
                        <td>{!! Form::text('package', $package, ['class' => 'form-control']) !!}</td>
                        <th>Per Month</th>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ $pack1 }}</td>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ $pack2 }}</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>{!! Form::label('bill_rate', 'Bill Rate') !!}</th>
                        <td>{!! Form::text('bill_rate', $rate, ['class' => 'form-control']) !!}</td>
                        <th>Per Hour</th>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ number_format($bill1,2,".",",") }}</td>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ number_format($bill2,2,".",",") }}</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>CTC ({{ $ctc_rate }}hr)</th>
                        <td class="text-right">{{ number_format($ctc,2,".",",") }}</td>
                        <th>Per Hour</th>
                        <td class="text-right" {{ rc(floor($ctc1_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ctc_margin,2,".",",") }}%</td>
                        <td class="text-right">{{ number_format($ctc1,2,".",",") }}</td>
                        <td class="text-right" {{ rc(floor($ctc1_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ctc1_margin,2,".",",") }}%</td>
                        <td class="text-right">{{ number_format($ctc2,2,".",",") }}</td>
                        <td class="text-right" {{ rc(floor($ctc2_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ctc2_margin,2,".",",") }}%</td>
                    </tr>
                    <tr>
                        <th>Break Even Rate ({{ $ber_rate }}hr)</th>
                        <td class="text-right">{{ number_format($ber,2,".",",") }}</td>
                        <th>Per Hour</th>
                        <td class="text-right" {{ rc(floor($ber_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ber_margin,2,".",",") }}%</td>
                        <td class="text-right">{{ number_format($ber1,2,".",",") }}</td>
                        <td class="text-right" {{ rc(floor($ber1_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ber1_margin,2,".",",") }}%</td>
                        <td class="text-right">{{ number_format($ber2,2,".",",") }}</td>
                        <td class="text-right" {{ rc(floor($ber2_margin),$configs->cost_min_percent??0,$configs->cost_max_percent??0,$configs?->cost_min_colour,$configs?->cost_mid_colour,$configs?->cost_max_colour) }}>{{ number_format($ber2_margin,2,".",",") }}%</td>
                    </tr>
                    <tr>
                        <th>Pref, Billing Rate ({{ $pbr_rate }}hr)</th>
                        <td class="text-right">{{ number_format($pbr,2,".",",") }}</td>
                        <th>Per Hour</th>
                        <td class="text-right">{{ number_format($pbr_margin,2,".",",") }}%</td>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Profit 160</th>
                        <td class="text-right">{{ number_format($profit160,2,".",",") }}</td>
                        <th>Per Month</th>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ number_format($profit1160,2,".",",") }}</td>
                        <td>Per Month</td>
                        <td class="text-right">{{ number_format($profit2160,2,".",",") }}</td>
                        <td>Per Month</td>
                    </tr>
                    <tr>
                        <th>Profit 140</th>
                        <td class="text-right">{{ number_format($profit140,2,".",",") }}</td>
                        <th>Per Month</th>
                        <td>&nbsp;</td>
                        <td class="text-right">{{ number_format($profit1140,2,".",",") }}</td>
                        <td>Per Month</td>
                        <td class="text-right">{{ number_format($profit2140,2,".",",") }}</td>
                        <td>Per Month</td>
                    </tr>
                    <tr><td colspan="8">&nbsp;</td></tr>
                    <tr>
                        <th>{!! Form::label('commission', 'Commission') !!}</th>
                        <td colspan="2">{!! Form::select('commission', $comm_dropdown, null, ['class' => 'form-control']) !!}</td>
                        <th colspan="2">Minimum Rate</th>
                        <td class="text-right" colspan="3">{{ $commission->min_rate }}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm table-hover">
                <tr>
                    <th class="text-right">{{ $commission->hour01 }}</th>
                    <th class="text-right">{{ $commission->hour02 }}</th>
                    <th class="text-right">{{ $commission->hour03 }}</th>
                    <th class="text-right">{{ $commission->hour04 }}</th>
                    <th class="text-right">{{ $commission->hour05 }}</th>
                    <th class="text-right">{{ $commission->hour06 }}</th>
                    <th class="text-right">{{ $commission->hour07 }}</th>
                    <th class="text-right">{{ $commission->hour08 }}</th>
                    <th class="text-right">{{ $commission->hour09 }}</th>
                    <th class="text-right">{{ $commission->hour10 }}</th>
                </tr>
                <tr>
                    <td class="text-right">{{ $commission->comm01 }}%</td>
                    <td class="text-right">{{ $commission->comm02 }}%</td>
                    <td class="text-right">{{ $commission->comm03 }}%</td>
                    <td class="text-right">{{ $commission->comm04 }}%</td>
                    <td class="text-right">{{ $commission->comm05 }}%</td>
                    <td class="text-right">{{ $commission->comm06 }}%</td>
                    <td class="text-right">{{ $commission->comm07 }}%</td>
                    <td class="text-right">{{ $commission->comm08 }}%</td>
                    <td class="text-right">{{ $commission->comm09 }}%</td>
                    <td class="text-right">{{ $commission->comm10 }}%</td>
                </tr>
                <tr>
                    <td class="text-right">{{ $cr01 }}</td>
                    <td class="text-right">{{ $cr02 }}</td>
                    <td class="text-right">{{ $cr03 }}</td>
                    <td class="text-right">{{ $cr04 }}</td>
                    <td class="text-right">{{ $cr05 }}</td>
                    <td class="text-right">{{ $cr06 }}</td>
                    <td class="text-right">{{ $cr07 }}</td>
                    <td class="text-right">{{ $cr08 }}</td>
                    <td class="text-right">{{ $cr09 }}</td>
                    <td class="text-right">{{ $cr10 }}</td>
                </tr>
                
            </table>
            {{-- <div class="text-center">{!! Form::submit('Calculate', ['class' => 'btn btn-default mt-2']) !!}</div> --}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
