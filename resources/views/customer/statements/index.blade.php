@extends('adminlte.default')

@section('title') Prepare Customer Statements @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="customerstatements.index"></x-export>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <form class="mt-3 searchform" id="searchform">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('company',$company_dropdown,(isset($_GET['company'])?$_GET['company']:$config?->company_id),['class'=>'form-control search', 'id' => 'company'])}}
                <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                <div class="position-relative w-100">
                    <input class="form-control form-control-sm w-100" id="customer" placeholder="Select Customer">
                    <div class="position-absolute w-100 bg-gray-light shadow rounded list"></div>
                </div>
                <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('period_from',$dates_drop_down,(isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month),['class'=>'form-control search', 'placeholder' => 'Period From'])}}
                <span>From</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('period_to',$dates_drop_down,(isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month),['class'=>'form-control search', 'placeholder' => 'Period To'])}}
                <span>To</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{ route('customerstatements.index') }}" class="btn btn-info w-100">Clear Filter</a>
            </div>
        </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead>
            <tr class="btn-dark">
                <th>Company Name</th>
                <th class="text-right">Total</th>
                <th class="text-right">Current</th>
                <th class="text-right">30 Days</th>
                <th class="text-right">60 Days</th>
                <th class="text-right">90+ Days</th>
                {{--@if($can_update)--}}
                    <th class="last">Action</th>
                {{--@endif--}}
            </tr>
            </thead>
            <tbody>
            @forelse($customer_invoices as $result)
                <tr>
                    <td>{{isset($result->customer->customer_name)?$result->customer->customer_name:null}}</td>
                    <td class="text-right">{{($result->currency??$config->currency)." ".number_format(($result->current + $result->thirty_days + $result->sixty_days + $result->ninety_days_plus),2,'.',',')}}</td>
                    <td class="text-right">{{($result->currency??$config->currency)." ".number_format($result->current,2,'.',',')}}</td>
                    <td class="text-right">{{($result->currency??$config->currency)." ".number_format($result->thirty_days,2,'.',',')}}</td>
                    <td class="text-right">{{($result->currency??$config->currency)." ".number_format($result->sixty_days,2,'.',',')}}</td>
                    <td class="text-right">{{($result->currency??$config->currency)." ".number_format($result->ninety_days_plus,2,'.',',')}}</td>
                    {{--@if($can_update)--}}
                    <td>
                        @if($can_create)
                            <a href="{{route('customerstatements.show', [
                                'customerstatement' => $result->customer_id,
                                'period_from' => (isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month),
                                'period_to' => (isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month),
                                'currency' => $result->currency
                            ])}}" class="btn btn-dark btn-sm"><i class="fas fa-plus"></i></a>
                        @endif
                    </td>
                   {{-- @endif--}}
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No data match those criteria.</td>
                </tr>
            @endforelse
            <tr>
                <th>Total</th>
                <th class="text-right">
                    @foreach ($currencies as $currency)
                        {{$currency}}: {{number_format(($total[$currency]+$total30[$currency]+$total60[$currency]+$total90[$currency]),2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total30 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total60 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                <th class="text-right">
                    @foreach($total90 as $key=>$value)
                    {{$key}}: {{number_format($value,2,'.',',')}}<br />
                    @endforeach
                </th>
                {{--@if($can_update)--}}
                <td>
                </td>
               {{-- @endif--}}
            </tr>
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $customer_invoices->firstItem() }} - {{ $customer_invoices->lastItem() }} of {{ $customer_invoices->total() }}
                    </td>
                    <td>
                        {{ $customer_invoices->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
        {{-- {{ $customer_invoices->appends(request()->except('page'))->links() }} --}}
    </div>
</div>
@endsection

@section('extra-js')
    <script src="{{asset('js/filters.js')}}"></script>
    <script>
        $(document).ready(function (){
            const customers = @json(session('customer_dropdown'));
            const url = new URL(window.location.toLocaleString());

            $("#customer").dropdownSearch({
                name: "customer",
                dropdown: customers !== null ? customers : []
            });

            if (url.searchParams.get('customer_id')){
                let customer = `<input type="hidden" name="customer_id" value="${url.searchParams.get('customer_id')}" >`;
                $("#searchform").append(customer)
            }
        });
    </script>
@endsection

@section('extra-css')
    <style>
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection
