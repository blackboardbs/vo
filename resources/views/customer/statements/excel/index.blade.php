<table>
    <thead>
    <tr>
        <th>Company Name</th>
        <th>Total</th>
        <th>Current</th>
        <th>30 Days</th>
        <th>60 Days</th>
        <th>90+ Days</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customer_invoices as $result)
        <tr>
            <td>{{$result->customer->customer_name}}</td>
            <td>{{($result->currency??$config->currency)." ".round(($result->current + $result->thirty_days + $result->sixty_days + $result->ninety_days_plus),2)}}</td>
            <td>{{($result->currency??$config->currency)." ".round($result->current,2)}}</td>
            <td>{{($result->currency??$config->currency)." ".round($result->thirty_days,2)}}</td>
            <td>{{($result->currency??$config->currency)." ".round($result->sixty_days,2)}}</td>
            <td>{{($result->currency??$config->currency)." ".round($result->ninety_days_plus,2)}}</td>
        </tr>
    @endforeach
    <tr>
        <th>Total</th>
        <th>
            @foreach ($currencies as $currency)
                {{$currency}}: {{round(($total[$currency]+$total30[$currency]+$total60[$currency]+$total90[$currency]),2)}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total as $key=>$value)
                {{$key}}: {{round($value,2)}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total30 as $key=>$value)
                {{$key}}: {{round($value,2)}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total60 as $key=>$value)
                {{$key}}: {{round($value,2)}}<br />
            @endforeach
        </th>
        <th>
            @foreach($total90 as $key=>$value)
                {{$key}}: {{round($value,2)}}<br />
            @endforeach
        </th>
        {{--@if($can_update)--}}
        <td>
        </td>
        {{-- @endif--}}
    </tr>
    </tbody>
</table>