@extends('adminlte.default')

@section('title')
Customer Statement
@endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('customerstatements.show', [
                            'customerstatement' => $customer_invoices[0]->customer_id,
                            'period_from' => (isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month),
                            'period_to' => (isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month),
                            'print' => 1,
                            'currency' => request('currency')
                        ])}}" class="btn btn-dark btn-sm mr-2"><i class="fas fa-print"></i> Print</a>
                <button class="btn btn-dark btn-sm mr-2" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-envelope"></i> Email</button>
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="row">
            <div class="col-xl-9 col-lg-12">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h5>Debtor Statement</h5>
                <div class="row">
                    <div class="col-md-5 text-right">As At</div>
                    <div class="col-md-6">: {{$statement_date->format('d-M-Y')}}</div>
                </div>
                <div class="row">
                    <div class="col-md-5"><p><strong>{{isset($customer_invoices[0]->company->company_name)?$customer_invoices[0]->company->company_name:null}}</strong></p></div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <p>Registration<br>
                            VAT No. <br>
                            Contact <br>
                            Address <br>
                            @if($customer_invoices[0]->company->postal_address_line2) {!! '<br>' !!} @endif
                            @if($customer_invoices[0]->company->postal_address_line3) {!! '<br>' !!} @endif
                            @if($customer_invoices[0]->company->city_suburb) {!! '<br>' !!} @endif
                            @if($customer_invoices[0]->company->state_province) {!! '<br>' !!} @endif
                            @if($customer_invoices[0]->company->postal_zipcode) {!! '<br>' !!} @endif
                            Tel <br>
                            Fax <br>
                            Email <br>
                        </p>
                    </div>
                    <div class="col-md-7">
                        <p>: {{isset($customer_invoices[0]->company->business_reg_no)?$customer_invoices[0]->company->business_reg_no:null}}
                            <br>: {{isset($customer_invoices[0]->company->vat_no)?$customer_invoices[0]->company->vat_no:null}}
                            <br>: {{isset($customer_invoices[0]->company->contact_firstname)?$customer_invoices[0]->company->contact_firstname:null}} {{isset($customer_invoices[0]->company->contact_lastname)?$customer_invoices[0]->company->contact_lastname:null}}
                            <br>: {{isset($customer_invoices[0]->company->postal_address_line1)?$customer_invoices[0]->company->postal_address_line1:null}}
                            <br>@if($customer_invoices[0]->company->postal_address_line2) {!! '&nbsp; '.$customer_invoices[0]->company->postal_address_line2.'<br>' !!} @endif
                             @if($customer_invoices[0]->company->postal_address_line3) {!! '&nbsp; '.$customer_invoices[0]->company->postal_address_line3.'<br>' !!} @endif
                             @if($customer_invoices[0]->company->city_suburb) {!! '&nbsp; '.$customer_invoices[0]->company->city_suburb.'<br>' !!} @endif
                             @if($customer_invoices[0]->company->state_province) {!! '&nbsp; '.$customer_invoices[0]->company->state_province.'<br>' !!} @endif
                             @if($customer_invoices[0]->company->postal_zipcode) {!! '&nbsp; '.$customer_invoices[0]->company->postal_zipcode.'<br>' !!} @endif
                            : {{($customer_invoices[0]->company->phone)?$customer_invoices[0]->company->phone:$customer_invoices[0]->company->cell}}
                            <br>: {{($customer_invoices[0]->company->fax)?$customer_invoices[0]->company->fax:null}}
                            <br>: {{($customer_invoices[0]->company->email)?$customer_invoices[0]->company->email:null}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-9">&nbsp;</div>
                    <div class="col-md-3 text-right"> <img class="img-thumbnail" src="{{route('company_avatar', ['q'=> (isset($customer_invoices[0]->company->company_logo)?$customer_invoices[0]->company->company_logo:null)])}}" alt="{{isset($customer_invoices[0]->company->company_name)?$customer_invoices[0]->company->company_name:null}} Logo"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><strong>Banking Details</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <p>Bank Account Name<br>
                            Bank Name <br>
                            Branch Name <br>
                            Account No <br>
                            Swift Code <br>
                        </p>
                    </div>
                    <div class="col-md-7">
                        <p>: {{isset($customer_invoices[0]->company->account_name)?$customer_invoices[0]->company->account_name:null}}
                            <br>: {{isset($customer_invoices[0]->company->bank_name)?$customer_invoices[0]->company->bank_name:null}}
                            <br>: {{isset($customer_invoices[0]->company->branch_name)?$customer_invoices[0]->company->branch_name:$customer_invoices[0]->company->branch_code}}
                            <br>: {{isset($customer_invoices[0]->company->bank_acc_no)?$customer_invoices[0]->company->bank_acc_no:null}}
                            <br>: {{isset($customer_invoices[0]->company->swift_code)?$customer_invoices[0]->company->swift_code:null}}

                        </p>
                    </div>
                    <div class="col-md-12">
                        <p>{{($customer_invoices[0]->company->invoice_text)?$customer_invoices[0]->company->invoice_text:null}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <strong>Customer</strong>
                <hr class="mt-0">
            </div>
            <div class="col-md-6">
                <p>
                    <strong>{{isset($customer_invoices[0]->customer->customer_name)?$customer_invoices[0]->customer->customer_name:null}}</strong>
                    <br>@if($customer_invoices[0]->customer->postal_address_line2) {!! $customer_invoices[0]->customer->postal_address_line2.'<br>' !!} @endif
                    @if($customer_invoices[0]->customer->postal_address_line3) {!! $customer_invoices[0]->customer->postal_address_line3.'<br>' !!} @endif
                    @if($customer_invoices[0]->customer->city_suburb) {!! $customer_invoices[0]->customer->city_suburb.'<br>' !!} @endif
                    @if($customer_invoices[0]->customer->state_province) {!! $customer_invoices[0]->customer->state_province.'<br>' !!} @endif
                    @if($customer_invoices[0]->customer->postal_zipcode) {!! $customer_invoices[0]->customer->postal_zipcode.'<br>' !!} @endif
                    {{($customer_invoices[0]->customer->country)?' '.$customer_invoices[0]->customer->country->name:null}}
                </p>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-5">
                        <p>
                            VAT No. <br>
                            Customer Code <br>
                            Contact <br>
                            Email <br>
                            Tel
                        </p>
                    </div>
                    <div class="col-md-7">
                        <p>
                           : {{isset($customer_invoices[0]->customer->vat_no)?$customer_invoices[0]->customer->vat_no:null}}
                            <br>: {{$customer_invoices[0]->customer_id}}<br>
                            : {{isset($customer_invoices[0]->customer->contact_firstname)?$customer_invoices[0]->customer->contact_firstname:null}} {{isset($customer_invoices[0]->customer->contact_lastname)?$customer_invoices[0]->customer->contact_lastname:null}}
                            <br>: {{isset($customer_invoices[0]->customer->email)?$customer_invoices[0]->customer->email:null}}
                            <br>: {{isset($customer_invoices[0]->customer->phone)?$customer_invoices[0]->customer->phone:$customer_invoices[0]->customer->cell}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead class="bg-dark">
                    <tr>
                        <th>Doc #</th>
                        <th>Trans Type</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th class="text-right">Nett Amount</th>
                        <th class="text-right">Running Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @if((int) $total_by_month?->opening_balance)
                        <tr class="bg-gray">
                            <td colspan="3">Opening Balance</td>
                            <td colspan="3">{{$opening_bal_date->toDateString()}}</td>
                            <td class="text-right">{{request('currency').' '.number_format($total_by_month->opening_balance,2,'.',',')}}</td>
                        </tr>
                    @endif
                    @forelse($customer_invoices as $invoice)
                        <tr>
                            <td colspan="7">Invoice {{$invoice->id}}</td>
                        </tr>
                        @if($invoice->bill_status == 1)
                            <tr>
                                <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->inv_ref}}</td>
                                <td>Sales Invoice</td>
                                <td>Open</td>
                                <td>{{$invoice->invoice_date}}</td>
                                <td>Sales Invoice {{$invoice->cust_inv_ref}}</td>
                                <td class="text-right">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                                <td class="text-right">{{request('currency').' '.number_format($invoice->invoice_value,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                            </tr>
                        @endif
                        @if($invoice->bill_status == 2)
                            <tr>
                                <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->inv_ref}}</td>
                                <td>Sales Invoice</td>
                                <td>Paid</td>
                                <td>{{$invoice->paid_date}}</td>
                                <td>Sales Invoice {{$invoice->cust_inv_ref}}</td>
                                <td class="text-right">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                                <td class="text-right">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->inv_ref}}</td>
                                <td>Receipt</td>
                                <td></td>
                                <td>{{$invoice->paid_date}}</td>
                                <td>Payment for Invoice {{$invoice->id}}</td>
                                <td class="text-right">-{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                                <td class="text-right">{{request('currency')." ".number_format(0,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{request('currency')." ".number_format(0,2,'.',',')}}</td>
                            </tr>
                        @endif
                        @if($invoice->bill_status == 3)
                            <tr>
                                <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->inv_ref}}</td>
                                <td>Sales Invoice</td>
                                <td>Cancelled</td>
                                <td>{{$invoice->paid_date}}</td>
                                <td>Sales Invoice {{$invoice->cust_inv_ref}}</td>
                                <td class="text-right">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                                <td class="text-right">{{request('currency')." ".number_format($invoice->invoice_value,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td>&nbsp; <i class="fas fa-chevron-right"></i> {{$invoice->inv_ref}}</td>
                                <td>Credit Note</td>
                                <td></td>
                                <td>{{$invoice->paid_date}}</td>
                                <td>Invoice {{$invoice->id}} Cancelled</td>
                                <td class="text-right">{{request('currency')}} -{{number_format($invoice->invoice_value,2,'.',',')}}</td>
                                <td class="text-right">{{request('currency')." ".number_format(0,2,'.',',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="2"  class="text-right border border-dark border-bottom-0 border-left-0 border-right-0">{{request('currency')." ".number_format(0,2,'.',',')}}</td>
                            </tr>
                        @endif
                        @empty
                    @endforelse
                <tr>
                    <td colspan="2"></td>
                    <td colspan="5">
                        <table class="table table-sm table-striped">
                            <thead class="bg-gray">
                                <tr>
                                    <th class="text-center">Total Amount Due</th>
                                    <th class="text-center">Current</th>
                                    <th class="text-center">30 Days</th>
                                    <th class="text-center">60 Days</th>
                                    <th class="text-center">90 + Days</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center">{{request('currency')." ".number_format(($total_by_month?->current + $total_by_month?->thirty_days + $total_by_month?->sixty_days + $total_by_month?->ninety_days_plus),2,'.',',')}}</td>
                                <td class="text-center">{{request('currency')." ".number_format($total_by_month?->current,2,'.',',')}}</td>
                                <td class="text-center">{{request('currency')." ".number_format($total_by_month?->thirty_days, 2,'.',',')}}</td>
                                <td class="text-center">{{request('currency')." ".number_format($total_by_month?->sixty_days,2,'.',',')}}</td>
                                <td class="text-center">{{request('currency')." ".number_format($total_by_month?->ninety_days_plus,2,'.',',')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Email Customer Statement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => route('statement.send', $customer_invoices[0]->customer->id), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off', 'id' => 'send_statement']) !!}
                        <div class="form-check">
                            <input type="checkbox" name="customer_email" value="{{$customer_invoices[0]->customer->email}}" checked class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">{{$customer_invoices[0]->customer->email}}</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="text" name="user_email" class="form-control" id="user_email" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        {!! Form::hidden('send', 1) !!}
                        {!! Form::hidden('currency', request('currency')) !!}
                        {!! Form::hidden('period_from', (isset($_GET['period_from'])?$_GET['period_from']:now()->subMonths(3)->year.now()->subMonths(3)->month)) !!}
                        {!! Form::hidden('period_to', (isset($_GET['period_to'])?$_GET['period_to']:now()->year.now()->month)) !!}
                        <button type="submit" class="btn btn-sm btn-dark" id="mailing"><i class="fas fa-paper-plane"></i> Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section("extra-css")
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }

    </style>
@endsection
@section("extra-js")
    <script src="{{asset('js/multiple-emails.js')}}"></script>
    <script>
        $(function () {
            $('#user_email').multiple_emails({
                position: 'top', // Display the added emails above the input
                theme: 'bootstrap', // Bootstrap is the default theme
                checkDupEmail: true // Should check for duplicate emails added
            });

            $('#current_emails').text($('#user_email').val());

            $('#user_email').change( function(){
                $('#current_emails').text($(this).val());
            });
        })
    </script>
@endsection
