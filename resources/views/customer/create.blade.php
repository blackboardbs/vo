@extends('adminlte.default')

@section('title') Add Customer @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createCustomer')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.store'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off', 'files' => true,'id'=>'createCustomer'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Customer Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="5">Customer Logo:</th>
                    <td rowspan="5">
                        <div class="img-wrapper mb-3" style="display: none"><img src="{{route('customer_avatar',['q'=>Auth::user()->avatar])}}" id="blackboard-preview-small"/></div>
                        <div class="input-group">
                            <div class="custom-file w-100">
                                <input type="file" name="logo" class="custom-file-input" id="customer-logo">
                                <label class="custom-file-label" for="customer-logo">Choose file</label>
                            </div>
                        </div>
                    </td>
                    <th>Customer Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('customer_name',old('customer_name'),['class'=>'form-control form-control-sm '. ($errors->has('customer_name') ? ' is-invalid' : ''),'placeholder'=>'Customer Name'])}}
                        @foreach($errors->get('customer_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Registration Number:</th>
                    <td>{{Form::text('business_reg_no',old('business_reg_no'),['class'=>'form-control form-control-sm'. ($errors->has('business_reg_no') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}
                        @foreach($errors->get('business_reg_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>VAT Number:</th>
                    <td>{{Form::text('vat_no',old('vat_no'),['class'=>'form-control form-control-sm'. ($errors->has('vat_no') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Account Manager: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('account_manager',$users_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('account_manager') ? ' is-invalid' : ''), 'placeholder' => 'Select Account Manager'])}}
                        @foreach($errors->get('account_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Contact Firstname: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('contact_firstname',old('contact_firstname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Lastname: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('contact_lastname',old('contact_lastname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}
                        @foreach($errors->get('contact_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Birthday:</th>
                    <td>{{Form::text('contact_birthday',old('contact_birthday'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('contact_birthday') ? ' is-invalid' : ''),'placeholder'=>'Contact Birthday'])}}
                        @foreach($errors->get('contact_birthday') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Invoice Contact</th>
                    <td>{{Form::select('invoice_contact_id',$invoice_contact_dropdown, null ,['class'=>'form-control form-control-sm'. ($errors->has('invoice_contact_id') ? ' is-invalid' : ''),'placeholder'=>'Select Invoice Contact'])}}
                        @foreach($errors->get('invoice_contact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',old('phone'),['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell:</th>
                    <td>{{Form::text('cell',old('cell'),['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('cell') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{Form::text('fax',old('fax'),['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell 2:</th>
                    <td>{{Form::text('cell2',old('cell2'),['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                        @foreach($errors->get('cell2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{Form::text('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Web:</th>
                    <td>{{Form::text('web',old('web'),['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}
                        @foreach($errors->get('web') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                <th>Postal Address:</th>
                <td>{{Form::text('postal_address_line1',old('postal_address_line1'),['class'=>'form-control form-control-sm'. ($errors->has('postal_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 1'])}}
                    @foreach($errors->get('postal_address_line1') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('postal_address_line2',old('postal_address_line2'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 2'])}}
                    @foreach($errors->get('postal_address_line2') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('postal_address_line3',old('postal_address_line3'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 3'])}}
                    @foreach($errors->get('postal_address_line3') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('city_suburb',old('city_suburb'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                    @foreach($errors->get('city_suburb') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('postal_zipcode',old('postal_zipcode'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                    @foreach($errors->get('postal_zipcode') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach

                </td>
                <th>Street Address:</th>
                <td>{{Form::text('street_address_line1',old('street_address_line1'),['class'=>'form-control form-control-sm'. ($errors->has('street_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 1'])}}
                    @foreach($errors->get('street_address_line1') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('street_address_line2',old('street_address_line2'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 2'])}}
                    @foreach($errors->get('street_address_line2') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('street_address_line3',old('street_address_line3'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 3'])}}
                    @foreach($errors->get('street_address_line1') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('street_city_suburb',old('street_city_suburb'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                    @foreach($errors->get('street_city_suburb') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                    {{Form::text('street_zipcode',old('street_zipcode'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                    @foreach($errors->get('street_zipcode') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach

                </td>
                </tr>
                <tr>
                    <th>State/Province:</th>
                    <td>{{Form::text('state_province',old('state_province'),['class'=>'form-control form-control-sm'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                        @foreach($errors->get('state_province') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Country: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('country_id',$sidebar_process_countries,'1',['class'=>'form-control form-control-sm '. ($errors->has('country_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_countries'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Billing Note:</th>
                    <td>{{Form::text('billing_note',old('billing_note'),['class'=>'form-control form-control-sm'. ($errors->has('billing_note') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billing_note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Billing Cycle:</th>
                    <td>{{Form::select('billing_cycle_id',$billing_cycle_dropdown, null,['class'=>'form-control form-control-sm '. ($errors->has('billing_cycle_id') ? ' is-invalid' : ''), 'placeholder' => 'Billing Cycle'])}}
                        @foreach($errors->get('billing_cycle_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Payment Terms Days:</th>
                    <td>{{Form::select('payment_terms_days',$payment_terms_days_dropdown, 30,['class'=>'form-control form-control-sm '. ($errors->has('payment_terms_days') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('payment_terms_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Medical Certificate Type:</th>
                    <td>{{Form::select('medical_certificate_type',$medical_certificate_type_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('medical_certificate_type') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('medical_certificate_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Medical Certificate Text:</th>
                    <td>{{Form::text('medical_certificate_text',old('medical_certificate_text'),['class'=>'form-control form-control-sm'. ($errors->has('medical_certificate_text') ? ' is-invalid' : ''),'placeholder'=>'Medical Certificate Text'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{Form::select('bbbee_level',$bbbee_level_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BBBEE Certificate Expiry:</th>
                    <td>{{Form::text('bbbee_certificate_expiry',old('bbbee_certificate_expiry'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level Preference:</th>
                    <td>{{Form::select('bbbee_level_pref',$bbbee_level_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level_pref') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level_pref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$status_dropdown, \App\Enum\Status::ACTIVE->value,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Placement Agreement %:</th>
                    <td>{{Form::text('commission', old('commission'),['class'=>'form-control form-control-sm'. ($errors->has('commission') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('commission') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Customer Onboarding:</th>
                    <td>{{Form::checkbox('onboarding',1,false,['class'=>'form-check-input ml-0'. ($errors->has('onboarding') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('onboarding') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>

                    <th>Appointment Manager:</th>
                    <td>{{Form::select('appointment_manager_id',$appointment_manager_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('appointment_manager_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Appointment Manager'])}}
                        @foreach($errors->get('appointment_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach</td>
                    <th>Document:</th>
                    <td>
                        <input type="file" id="documents" name="documents[]" class="{{$errors->has('documents') ? ' is-invalid' : ''}}" multiple="multiple"/>
                        @foreach($errors->get('documents') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Service Agreement (Service Provider)</th>
                    <td>{{Form::select('service_agreement_id',$provider_master_service_agreement, $default_service_agreement,['class'=>'form-control form-control-sm '. ($errors->has('service_agreement_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('service_agreement_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>NDA Template</th>
                    <td>{{Form::select('nda_template_id', $nda_templates_dropdown, null,['class'=>'form-control form-control-sm '. ($errors->has('nda_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('nda_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <td></td>
                </tr>

                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {


            $("#customer-logo").on('change', function (){
                $("#blackboard-preview-small").attr('src', window.URL.createObjectURL(this.files[0]));
                $(".img-wrapper").show();
            })
        });
    </script>
@endsection
