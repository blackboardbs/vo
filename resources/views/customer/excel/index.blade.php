<table>
    <thead>
    <tr>
        <th>Customer Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Cell</th>
        <th>Added</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customer as $result)
        <tr>
            <td>{{$result->customer_name}}</td>
            <td>{{$result->email}}</td>
            <td>{{$result->phone}}</td>
            <td>{{$result->cell}}</td>
            <td>{{$result->created_at}}</td>
            <td>{{$result->statusd?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>