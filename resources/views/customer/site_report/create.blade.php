@extends('adminlte.default')

@section('title') Add Site Report @endsection

@section('content')
        @if(!$is_template || $is_template == 0)
        <site-report-header :is-template="0" :site-report-data="{}" :site-report-customers="{{$customers}}" :weeks="{{json_encode($weeks)}}" :reviewweeks="{{ json_encode($reviewWeeks) }}" :planweeks="{{ json_encode($planWeeks) }}" :reviewweeksend="{{ json_encode($reviewWeeksEnd) }}" :planweeksend="{{ json_encode($planWeeksEnd) }}" :configs="{{$configs}}"></site-report-header>
        @else
        <site-report-header :is-template="1" :site-report-data="{{$site_report}}" :site-report-customers="{{$customers}}" :weeks="{{json_encode($weeks)}}" :reviewweeks="{{ json_encode($reviewWeeks) }}" :planweeks="{{ json_encode($planWeeks) }}" :reviewweeksend="{{ json_encode($reviewWeeksEnd) }}" :planweeksend="{{ json_encode($planWeeksEnd) }}" :configs="{{$configs}}"></site-report-header>
        @endif
@endsection