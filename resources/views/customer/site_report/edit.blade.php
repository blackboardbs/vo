@extends('adminlte.default')

@section('title') Edit Site Report @endsection

@section('content')
        <site-report-header :site-report-data="{{$site_report}}" :site-report-customers="{{$customers}}" :weeks="{{json_encode($weeks)}}" :reviewweeks="{{ json_encode($reviewWeeks) }}" :planweeks="{{ json_encode($planWeeks) }}" :reviewweeksend="{{ json_encode($reviewWeeksEnd) }}" :planweeksend="{{ json_encode($planWeeksEnd) }}" :configs="{{$configs}}"></site-report-header>
@endsection