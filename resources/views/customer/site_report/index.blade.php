@extends('adminlte.default')

@section('title') Site Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <a href="{{route('site_report.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Site Report</a>
        @if(count($templates) > 1)
        <button type="button" class="btn btn-dark ml-2" onclick="showCreateReport()"><i class="fa fa-plus"></i> Site Report from Template</button>
        @else
        <button type="button" class="btn btn-dark ml-2 disabled" disabled><i class="fa fa-plus"></i> Site Report from Template</button>
        @endif
        <button type="button" class="d-none" id="createFromTemplateBtn" data-toggle="modal" data-target="#createFromTemplate"></button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="mt-3" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="form-row">
                <div class="col-sm-3 col-sm" style="max-width:16.67%;">
                    <div class="form-group input-group">
                        <label class="has-float-label"><input type="text" name="q" class="form-control w-100 search" value="{{(isset($_GET['q']) ? $_GET['q'] : '')}}">
                                <span>Matching</span>
                            </label>
                        </div>
                </div>
                <div class="col-md-3" style="max-width:15%;">
                    <a href="{{ route('site_report.index') }}" class="btn btn-info w-100">Clear Filter</a>
                </div>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                <th>@sortablelink('ref', 'Reference')</th>
                <th>@sortablelink('project_id', 'Project')</th>
                    <th>@sortablelink('customer_id', 'Customer')</th>
                    <th>@sortablelink('employee_id', 'Consultant')</th>
                    <th>@sortablelink('review_period', 'Review Period')</th>
                    <th>@sortablelink('plan_period', 'Plan Period')</th>
                    <th class="last">@sortablelink('template_id', 'Template ID')</th>
                    <th class="last">@sortablelink('template_name', 'Template Name')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($site_reports as $site_report)
                    <tr>
                        <td><a href="{{route('site_report.show',$site_report->id)}}">{{$site_report->ref}}</a></td>
                        <td>{{$site_report->project?->name}}</td>
                        <td>{{$site_report->customer?->customer_name}}</td>
                        <td>{{$site_report->resource?->first_name.' '.$site_report->resource?->last_name}}</td>
                        <td>{{$site_report->review_date_from}} to {{ $site_report->review_date_to }}</td>
                        <td>{{$site_report->plan_date_from}} to {{ $site_report->plan_date_to }}</td>
                        <td>{{$site_report->template_id}}</td>
                        <td>{{$site_report->template?->name}}</td>
                        <td>
                            <div class="d-flex">
                            <!-- <a href="{{route('site_report.copy',$site_report->id)}}" class="btn btn-warning btn-sm" style="width:32px;"><i class="fas fa-copy"></i></a> -->
                            <a href="{{route('site_report.show',$site_report)}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                            <a href="{{route('site_report.edit',$site_report)}}" class="btn btn-success btn-sm ml-1"><i class="fas fa-pencil-alt"></i></a>

                            {{ Form::open(['method' => 'DELETE','route' => ['site_report.destroy', $site_report],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No Site Report match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $site_reports->firstItem() }} - {{ $site_reports->lastItem() }} of {{ $site_reports->total() }}
                        </td>
                        <td>
                            {{ $site_reports->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal fade" id="createFromTemplate">
                <div class="modal-dialog" style="width:600px !important;max-width:450px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Create a Site Report from a Template</h5>
                            <a class="close" data-dismiss="modal" aria-label="Close" id="closeCreateFromTemplate">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body bg-white">
                            <div class="form row">
                                <label class="col-sm-3 col-form-label">Template:</label>
                                <div class="col-sm-9">
                                    <select name="template_id" id="template_id" class="form-control">
                                        @foreach($templates as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group text-center">
                                <a href="javascript:void(0)" class="btn btn-dark mr-2" onclick="createSiteReport()">Create</a>
                                <a href="javascript:void(0)" class="btn btn-default" data-dismiss="modal">Close</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
<script>
        function createSiteReport(){
            let err = 0;
            if($('#template_id').val() == 0){
                $('#template_id').addClass('is-invalid');
                err++;
            } else {
                $('#template_id').removeClass('is-invalid');
            }

            if(err == 0){
                window.location.href = '/site_report/create?template='+$('#template_id').val();
            }
        }

        function showCreateReport(){
            $('#template_id').val(0);
            $('#template_id').removeClass('is-invalid');
            $('#createFromTemplateBtn').click();
        }
</script>
@endsection