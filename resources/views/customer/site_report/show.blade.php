@extends('adminlte.default')

@section('title') Site Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="/site_report/pdf/{{$site_report->id}}?print=1" target="_blank" class="btn btn-info ml-1">Print</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">

            <div class="col-md-12" style="position: relative;">
                @if($site_report->include_company_logo == 1 || $site_report->include_customer_logo)
                <div class="row col-md-12 p-0 mb-2">
                    <div class="col-md-6 pr-1">
                        @if(is_file(storage_path('/app/avatars/company/'.$company->company_logo)))
                            @if($site_report->include_company_logo == 1)
                            <img class="pull-left" src="/storage/avatar/company?q={{$company->company_logo}}" alt="{{$company->company_name}} Logo" style="max-height:50px;" align="left">
                            @endif
                        @else
                        @if($site_report->include_company_logo == 1)
                            <img class="pull-left" src="{{public_path('/assets/consulteaze_logo.png')}}" alt="{{$company->company_name}} Logo" style="max-height: 50px;" align="left">
                            @endif
                        @endif
                    </div>
                    <div class="col-md-6 pl-1">
                        @if(is_file(storage_path('/app/avatars/customer/'.$customer->logo)))
                            @if($site_report->include_customer_logo == 1)
                            <img class="pull-right" src="/storage/avatar/customer?q={{$customer->logo}}" alt="{{$customer->customer_name}} Logo" style="max-height: 50px;" align="right">
                            @endif
                        @endif
                    </div>
                </div>
                @endif
                <div class="row col-md-12 p-0">
                    <div class="col-md-8 p-0">
                        <h2 class="form-inline"> {{ $site_report->site_report_title }} : {{ $site_report->project_name }} </h2>
                    </div>
                    <div class="col-md-4 p-0 text-right form-horizontal">
                        @if($site_report->include_consultant_name == 1)
                        <div class="form-group d-flex mb-1" style="flex-direction: row-reverse;white-space: nowrap;"> {{ $site_report->consultant_name }}<span class="font-weight-bold">Consultant:</span></div>
                        @endif
                        @if($site_report->include_report_to == 1)
                        <div class="form-group d-flex mb-1" style="flex-direction: row-reverse;white-space: nowrap;" > {{ $site_report->report_to }}<span class="font-weight-bold">Report to:</span></div>
                        @endif
                        <div class="form-group d-flex" style="flex-direction: row-reverse;white-space: nowrap;"> {{ $site_report->ref }}<span class="font-weight-bold">Ref:</span></div>
                    </div>
                </div>
                <div class="col-md-12 p-0">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">Activities for {{ $site_report->review_date_from }} to {{ $site_report->review_date_to }} </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr class="bg-dark">
                                <th class="last">#</th>
                                <th style="width:35%">Activities</th>
                                @if($site_report->include_consultant == 1)
                                <th>Consultant</th>
                                @endif
                                <th>Status</th>
                                @if($site_report->include_task_hours == 1)
                                <th>Hours</th>
                                @endif
                                <th style="width:20%">Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $actual_counter = 1;
                        @endphp
                        @forelse($site_report->actual as $actual)
                            <tr>
                            <td class="text-center">{{ $actual_counter++ }}</td>
                            <td>{{ $actual->task }}</td>
                            @if($site_report->include_consultant == 1)
                            <td>{{ $actual->employee_name }}</td>
                            @endif
                            <td>{{ $actual->task_status }}</td>
                            @if($site_report->include_task_hours == 1)
                            <td>{{ $actual->hours_billable }}</td>
                            @endif
                            <td>{!! $actual->notes !!}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">There are no activities to display.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 p-0">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">Planned Activities for {{ $site_report->plan_date_from }} to {{ $site_report->plan_date_to }} </div>
                    </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-dark">
                            <th class="last">#</th>
                            <th style="width:35%">Activities</th>
                            @if($site_report->include_consultant == 1)
                            <th>Consultant</th>
                            @endif
                            <th>Status</th>
                            @if($site_report->include_task_hours == 1)
                            <th>Hours</th>
                            @endif
                            <th style="width:20%">Notes</th>
                        </tr>
                    </thead>
                        <tbody>
                        @php
                            $planned_counter = 1;
                        @endphp
                        @forelse($site_report->planned as $plan)
                            <tr>
                            <td class="text-center">{{ $planned_counter++ }}</td>
                            <td>{{ $plan->task }}</td>
                            @if($site_report->include_consultant == 1)
                            <td>{{ $plan->employee_name }}</td>
                            @endif
                            <td>{{ $plan->task_status }}</td>
                            @if($site_report->include_task_hours == 1)
                            <td>{{ $plan->hours_planned }}</td>
                            @endif
                            <td>{!! $plan->notes !!}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">There are no planned activities to display.</td>
                            </tr>
                        @endforelse
                        </tbody>
                </table>
                </div>
                <div class="row col-md-12 p-0 m-0">
                @if($site_report->include_summary_hours == 1)
                <div class="col-md-6 pl-0">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">PO Hours tracking as at {{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->as_at : '' }}</div>
                    </div>
                    <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-dark">
                            <th></th>
                            <th style="width:30%">Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>PO Hours Budgeted</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->po_hours_budgeted : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Billed Hours</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->po_billed_hours : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Hours Unbilled (Previous Weeks)</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_unbilled_previous : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Hours Unbilled (Last Week)</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_unbilled_last : '' }}</td>
                        </tr>
                        <tr>
                            <td>Total PO Hours Remaining</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_remaining : '' }}</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                @else
                <div class="col-md-6 pl-0">
                </div>
                @endif
                
                @if($site_report->include_risks == 1)
                <div class="col-md-6 pr-0">
                    <div class="font-weight-bold">Risks</div>
                    <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-dark">
                            <th>Description</th>
                            <th style="width:30%">Likelihood</th>
                            <th style="width:30%">Impact</th>
                        </tr>
                    </thead>
                        <tbody>
                        @forelse($site_report->risks as $risk)
                            <tr>
                            <td>{{ $risk->name }}</td>
                            <td>{{ $risk->likelihood }}</td>
                            <td>{{ $risk->impact }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">There are no risks to display.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                @else
                <div class="col-md-6 pl-0">
                </div>
                @endif
                </div>
                <div class="row col-md-12 p-0 m-0">
                    {!! $site_report->site_report_footer !!}
                </div>
            </div>
        </div>
    </div>
@endsection
