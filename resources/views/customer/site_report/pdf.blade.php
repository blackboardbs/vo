<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Web Office</title>

    <style>
        body,html{font-family:arial,sans-serif;font-size: 0.9rem}
        .container{width:100%;margin:0 auto}
        .cols-3{width:31.3%;display:inline-block}
        .p-x{padding-left:1rem;padding-right:1rem}
        .p-y{padding-top:1rem;padding-bottom:1rem}
        .pull-right{float:right}
        .text-right{text-align:right}
        .text-center{text-align:center}
        .text-left{text-align:left}
        .clearfix{clear:both}
        .table{border-collapse:collapse;width:100%}
        td,th{border-bottom:1px solid #ddd;text-align:left;}
        tr:hover{background-color:#ddd}
        .bg-dark,.bg-dark:hover{color: #{!! $config->font_color !!} !important;
                    background-color: #{!! $config->background_color !!} !important;
                    border-color: #{!! $config->background_color !!} !important;;box-shadow:0 1px 1px rgba(0,0,0,.075)}
        hr{border-top:1px solid #ddd;margin-top:10px}
        .col-md-6{ width:50%; }
        p{margin:5px 0}
        .inv-img-wrapper{
            min-width: 200px;
            max-height: 150px;
        }
        /* table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        } */

        .img-wrapper{
            width: 200px;
        }
        .inv-img-wrapper img,.img-wrapper img{
            max-width: 100%;
            max-height: 100%;
            display:block;margin:0 auto;
        }
        .row{
            display:flex;
        }
        .pl-1{
            padding-left: 1rem;
        }
        .pr-1{
            padding-right: 1rem;
        }
        .mb-2{
            margin-bottom: 1.5rem;
        }
        .p-0{
            padding:0px;
        }
        .p-col-12{
            width: 100%;
        }
        .m-0{
            margin:0px;
        }
        .font-weight-bold{
            font-weight:bold
        }
        .last {
                width: 1%;
                white-space: nowrap;
            }
            .pt-2{
                padding-top:1.5rem;
            }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
    <div class="container-fluid">
        <div class="table-responsive">

            <div class="col-md-12" style="position: relative;">
                @if($site_report->include_company_logo == 1 || $site_report->include_customer_logo)
                <div class="row col-md-12 p-0 mb-2">
                    <div class="col-md-6 pr-1">
                        @if(is_file(storage_path('/app/avatars/company/'.$company->company_logo)))
                            @if($site_report->include_company_logo == 1)
                            <img class="pull-left" src="{{storage_path('/app/avatars/company/'.$company->company_logo)}}" alt="{{$company->company_name}} Logo" style="max-height:50px;" align="left">
                            @endif
                        @else
                        @if($site_report->include_company_logo == 1)
                            <img class="pull-left" src="{{public_path('/assets/consulteaze_logo.png')}}" alt="{{$company->company_name}} Logo" style="max-height: 50px;" align="left">
                            @endif
                        @endif
                    </div>
                    <div class="col-md-6 pl-1">
                        @if(is_file(storage_path('/app/avatars/customer/'.$customer->logo)))
                            @if($site_report->include_customer_logo == 1)
                            <img class="pull-right" src="{{storage_path('/app/avatars/customer/'.$customer->logo)}}" alt="{{$customer->customer_name}} Logo" style="max-height: 50px;" align="right">
                            @endif
                        @endif
                    </div>
                </div>
                @endif
                <div class="row col-md-12 p-0">
                    <div class="col-md-8 p-0" style="width:70%">
                        <h2 class="form-inline"> {{ $site_report->site_report_title }} : {{ $site_report->project_name }} </h2>
                    </div>
                    <div class="col-md-4 p-0 text-right form-horizontal" style="width:30%">
                        @if($site_report->include_consultant_name == 1)
                        <div class="form-group d-flex mb-1" style="flex-direction: row-reverse;white-space: nowrap;"><span class="font-weight-bold">Consultant:</span> {{ $site_report->consultant_name }}</div>
                        @endif
                        @if($site_report->include_report_to == 1)
                        <div class="form-group d-flex mb-1" style="flex-direction: row-reverse;white-space: nowrap;" ><span class="font-weight-bold">Report to:</span> {{ $site_report->report_to }}</div>
                        @endif
                        <div class="form-group d-flex" style="flex-direction: row-reverse;white-space: nowrap;"><span class="font-weight-bold">Ref:</span> {{ $site_report->ref }}</div>
                    </div>
                </div>
                <div class="col-md-12 p-0">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">Activities for {{ $site_report->review_date_from }} to {{ $site_report->review_date_to }} </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr class="bg-dark">
                                <th class="last">#</th>
                                <th style="width:35%">Activities</th>
                                @if($site_report->include_consultant == 1)
                                <th>Consultant</th>
                                @endif
                                <th>Status</th>
                                @if($site_report->include_task_hours == 1)
                                <th>Hours</th>
                                @endif
                                <th style="width:20%">Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $actual_counter = 1;
                        @endphp
                        @forelse($site_report->actual as $actual)
                            <tr>
                            <td class="text-center">{{ $actual_counter++ }}</td>
                            <td>{{ $actual->task }}</td>
                            @if($site_report->include_consultant == 1)
                            <td>{{ $actual->employee_name }}</td>
                            @endif
                            <td>{{ $actual->task_status }}</td>
                            @if($site_report->include_task_hours == 1)
                            <td>{{ $actual->hours_billable }}</td>
                            @endif
                            <td>{!! $actual->notes !!}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">There are no activities to display.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 p-0 pt-2">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">Planned Activities for {{ $site_report->plan_date_from }} to {{ $site_report->plan_date_to }} </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr class="bg-dark">
                                <th class="last">#</th>
                                <th style="width:35%">Activities</th>
                                @if($site_report->include_consultant == 1)
                                <th>Consultant</th>
                                @endif
                                <th>Status</th>
                                @if($site_report->include_task_hours == 1)
                                <th>Hours</th>
                                @endif
                                <th style="width:20%">Notes</th>
                            </tr>
                        </thead>
                            <tbody>
                            @php
                                $planned_counter = 1;
                            @endphp
                            @forelse($site_report->planned as $plan)
                                <tr>
                                <td class="text-center">{{ $planned_counter++ }}</td>
                                <td>{{ $plan->task }}</td>
                                @if($site_report->include_consultant == 1)
                                <td>{{ $plan->employee_name }}</td>
                                @endif
                                <td>{{ $plan->task_status }}</td>
                                @if($site_report->include_task_hours == 1)
                                <td>{{ $plan->hours_planned }}</td>
                                @endif
                                <td>{!! $plan->notes !!}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">There are no planned activities to display.</td>
                                </tr>
                            @endforelse
                            </tbody>
                    </table>
                </div>
                <div class="row col-md-12 p-0 m-0 pt-2">
                @if($site_report->include_summary_hours == 1)
                <div class="col-md-6 pr-1">
                    <div class="form-inline">
                        <div class="form-group font-weight-bold">PO Hours tracking as at {{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->as_at : '' }}</div>
                    </div>
                    <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-dark">
                            <th></th>
                            <th style="width:30%">Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>PO Hours Budgeted</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->po_hours_budgeted : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Billed Hours</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]?->po_billed_hours : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Hours Unbilled (Previous Weeks)</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_unbilled_previous : '' }}</td>
                        </tr>
                        <tr>
                            <td>PO Hours Unbilled (Last Week)</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_unbilled_last : '' }}</td>
                        </tr>
                        <tr>
                            <td>Total PO Hours Remaining</td>
                            <td>{{ isset($site_report->hours_summary[0]) ? $site_report->hours_summary[0]->po_hours_remaining : '' }}</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                @else
                <div class="col-md-6 pl-0">
                </div>
                @endif
                
                @if($site_report->include_risks == 1)
                <div class="col-md-6 pl-1">
                    <div class="font-weight-bold">Risks</div>
                    <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-dark">
                            <th>Description</th>
                            <th style="width:30%">Likelihood</th>
                            <th style="width:30%">Impact</th>
                        </tr>
                    </thead>
                        <tbody>
                        @forelse($site_report->risks as $risk)
                            <tr>
                            <td>{{ $risk->name }}</td>
                            <td>{{ $risk->likelihood }}</td>
                            <td>{{ $risk->impact }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">There are no risks to display.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                @else
                <div class="col-md-6 pl-0">
                </div>
                @endif
                </div>
                <div class="row col-md-12 p-0 m-0">
                    {!! $site_report->site_report_footer !!}
                </div>
            </div>
        </div>
    </div>

    </div>
</body>
</html>
