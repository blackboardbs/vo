@extends('adminlte.default')

@section('title') Edit Customer @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('editCustomer')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('customer.update',$customer), 'method' => 'patch','class'=>'mt-3', 'autocomplete' => 'off', 'files' => true,'id'=>'editCustomer'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Customer Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="5">Customer Logo:</th>
                    <td rowspan="5">
                        <div class="img-wrapper mb-3"><img src="{{route('customer_avatar',['q'=>$customer->logo])}}" id="blackboard-preview-small"/></div>
                        <div class="input-group">
                            <div class="custom-file w-100">
                                <input type="file" name="logo" class="custom-file-input" id="customer-logo">
                                <label class="custom-file-label" for="customer-logo">Choose file</label>
                            </div>
                        </div>
                    </td>
                    <th>Customer Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('customer_name',$customer->customer_name,['class'=>'form-control form-control-sm'. ($errors->has('customer_name') ? ' is-invalid' : ''),'placeholder'=>'Customer Name'])}}
                        @foreach($errors->get('customer_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Registration Number:</th>
                    <td>{{Form::text('business_reg_no',$customer->business_reg_no,['class'=>'form-control form-control-sm'. ($errors->has('business_reg_no') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}
                        @foreach($errors->get('business_reg_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>VAT Number:</th>
                    <td>{{Form::text('vat_no',$customer->vat_no,['class'=>'form-control form-control-sm'. ($errors->has('vat_no') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Account Manager: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('account_manager',$users_dropdown,$customer->account_manager,['class'=>'form-control form-control-sm '. ($errors->has('account_manager') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('account_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Contact Firstname: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('contact_firstname',$customer->contact_firstname,['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Contact Lastname: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('contact_lastname',$customer->contact_lastname,['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}
                        @foreach($errors->get('contact_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Contact Birthday:</th>
                    <td>{{Form::text('contact_birthday',$customer->contact_birthday,['class'=>'datepicker form-control form-control-sm'. ($errors->has('contact_birthday') ? ' is-invalid' : ''),'placeholder'=>'Contact Birthday'])}}
                        @foreach($errors->get('contact_birthday') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Invoice Contact</th>
                    <td>{{Form::select('invoice_contact_id',$invoice_contact_dropdown, $customer->invoice_contact_id ,['class'=>'form-control form-control-sm'. ($errors->has('invoice_contact_id') ? ' is-invalid' : ''),'placeholder'=>'Select Invoice Contact'])}}
                        @foreach($errors->get('invoice_contact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',$customer->phone,['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell:</th>
                    <td>{{Form::text('cell',$customer->cell,['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('cell') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{Form::text('fax',$customer->fax,['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell 2:</th>
                    <td>{{Form::text('cell2',$customer->cell2,['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                        @foreach($errors->get('cell2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{Form::text('email',$customer->email,['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Web:</th>
                    <td>{{Form::text('web',$customer->web,['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}
                        @foreach($errors->get('web') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Postal Address:</th>
                    <td>{{Form::text('postal_address_line1',$customer->postal_address_line1,['class'=>'form-control form-control-sm'. ($errors->has('postal_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 1'])}}
                        @foreach($errors->get('postal_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line2',$customer->postal_address_line2,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 2'])}}
                        @foreach($errors->get('postal_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_address_line3',$customer->postal_address_line3,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Line 3'])}}
                        @foreach($errors->get('postal_address_line3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('city_suburb',$customer->city_suburb,['class'=>'form-control form-control-sm input-pad'. ($errors->has('city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_zipcode',$customer->postal_zipcode,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('postal_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                    <th>Street Address:</th>
                    <td>{{Form::text('street_address_line1',$customer->street_address_line1,['class'=>'form-control form-control-sm'. ($errors->has('street_address_line1') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 1'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line2',$customer->street_address_line2,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line2') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 2'])}}
                        @foreach($errors->get('street_address_line2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_address_line3',$customer->street_address_line3,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_address_line3') ? ' is-invalid' : ''),'placeholder'=>'Street Address Line 3'])}}
                        @foreach($errors->get('street_address_line1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_city_suburb',$customer->street_city_suburb,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('street_city_suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('street_zipcode',$customer->street_zipcode,['class'=>'form-control form-control-sm input-pad'. ($errors->has('street_zipcode') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('street_zipcode') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                </tr>
                <tr>
                    <th>State/Province:</th>
                    <td>{{Form::text('state_province',$customer->state_province,['class'=>'form-control form-control-sm'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                        @foreach($errors->get('state_province') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Country: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('country_id',$sidebar_process_countries,$customer->country_id,['class'=>'form-control form-control-sm '. ($errors->has('country_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_countries'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Billing Note:</th>
                    <td>{{Form::text('billing_note',$customer->billing_note,['class'=>'form-control form-control-sm'. ($errors->has('billing_note') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billing_note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Billing Cycle:</th>
                    <td>{{Form::select('billing_cycle_id',$billing_cycle_dropdown, $customer->billing_cycle_id,['class'=>'form-control form-control-sm '. ($errors->has('billing_cycle_id') ? ' is-invalid' : ''), 'placeholder'=>'Billing Cycle'])}}
                        @foreach($errors->get('billing_cycle_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Payment Terms Days:</th>
                    <td>{{Form::select('payment_terms_days',$payment_terms_days_dropdown, $customer->payment_terms,['class'=>'form-control form-control-sm '. ($errors->has('payment_terms_days') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('payment_terms_days') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Medical Certificate Type:</th>
                    <td>{{Form::select('medical_certificate_type',$medical_certificate_type_dropdown,$customer->medical_certificate_type,['class'=>'form-control form-control-sm '. ($errors->has('medical_certificate_type') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('medical_certificate_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Medical Certificate Text:</th>
                    <td>{{Form::text('medical_certificate_text',$customer->medical_certificate_text,['class'=>'form-control form-control-sm'. ($errors->has('medical_certificate_text') ? ' is-invalid' : ''),'placeholder'=>'Medical Certificate Text'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{Form::select('bbbee_level',$bbbee_level_dropdown,$customer->bbbee_level,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BBBEE Certificate Expiry:</th>
                    <td>{{Form::text('bbbee_certificate_expiry',$customer->bbbee_certificate_expiry,['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry'])}}
                        @foreach($errors->get('vat_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>BBBEE Level Preference:</th>
                    <td>{{Form::select('bbbee_level_pref',$bbbee_level_dropdown,$customer->bbbee_level_preference,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level_pref') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('bbbee_level_pref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$status_dropdown,$customer->status,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Placement Agreement %:</th>
                    <td>{{Form::text('commission', $customer->commission,['class'=>'form-control form-control-sm'. ($errors->has('commission') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('commission') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Customer Onboarding:</th>
                    <td>{{Form::checkbox('onboarding',1,$customer->onboarding,['class'=>'form-check-input ml-0'. ($errors->has('onboarding') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('onboarding') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                    <th>Appointment Manager:</th>
                    <td>{{Form::select('appointment_manager_id',$appointment_manager_dropdown,$customer->appointment_manager_id,['class'=>'form-control form-control-sm '. ($errors->has('appointment_manager_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Appointment Manager'])}}
                        @foreach($errors->get('appointment_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Service Agreement (Service Provider)</th>
                    <td>{{Form::select('service_agreement_id',$provider_master_service_agreement, $customer->service_agreement_id,['class'=>'form-control form-control-sm '. ($errors->has('service_agreement_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('service_agreement_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>NDA Template</th>
                    <td>{{Form::select('nda_template_id',$nda_templates_dropdown, $customer->nda_template_id,['class'=>'form-control form-control-sm '. ($errors->has('nda_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('nda_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
            <table class="table table-borderless table-sm">
                <tbody>
                    <tr>
                        <th colspan="5" class="btn-dark">Documents</th>
                    </tr>
                    <tr>
                        <th>Document</th>
                        <th>File Name</th>
                        <th>Upload Date</th>
                        <th>Uploaded by</th>
                        <th>Action</th>
                    </tr>
                    @forelse($documents as $document)
                        <tr>
                            <td>{{ Html::link($document->file, $document->name) }}</td>
                            <td>{{$document->file}}</td>
                            <td>{{$document->created_at}}</td>
                            <td>{{isset($document->user->first_name)?$document->user->first_name:''}} {{isset($document->user->last_name)?$document->user->last_name:''}}</td>
                            <td>
                                <a href="{{route('document.edit', $document->id)}}?reference_id={{$document->reference_id}}&document_type_id={{$document->document_type_id}}" class="btn btn-success btn-sm">Edit</a>
                                {{ Form::open(['method' => 'DELETE', 'route' => ['document.destroy', $document->id],'style'=>'display:inline','class'=>'delete']) }}
                                <input type="hidden" id="reference_id" name="reference_id" value="{{$document->reference_id}}"/>
                                <input type="hidden" id="document_type_id" name="document_type_id" value="{{$document->document_type_id}}"/>
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">No documents found.</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="5" style="text-align: center">
                            <a href="{{route('document.create')}}?reference_id={{$customer->id}}&document_type_id=10" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Document</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {


            $("#customer-logo").on('change', function (){
                $("#blackboard-preview-small").attr('src', window.URL.createObjectURL(this.files[0]));
                $(".img-wrapper").show();
            })
        });
    </script>
@endsection
