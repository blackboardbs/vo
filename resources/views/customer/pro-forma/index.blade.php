@extends('adminlte.default')

@section('title') Pro-forma Customer Invoice @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        {{-- <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button> --}}
        <a href="javascript:void(0)" onclick="saveForm('gen')" class="btn btn-primary ml-1" style="margin-top: -7px;">Generate Invoice</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive" style="min-height: 500px">
            {{Form::open(['url' => route('customer.proformacalculate'), 'method' => 'post','class'=>'mt-3','id'=>'gen'])}}
                <pro-forma-invoice
                        :customers="{{$customers}}"
                        :errors="{{$errors}}"
                        :old="{{json_encode(old())}}"
                        :is-customer="true"
                ></pro-forma-invoice>
            {{Form::close()}}

        </div>
    </div>
@endsection
