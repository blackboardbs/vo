@extends('adminlte.default')

@section('title') View Pro-forma Customer Invoice @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="row no-gutters">
            <div class="col-md-4 pl-2">
                <h4>Customer Invoice</h4>
                <h5>{{$company->company_name}}</h5>
                <p class="mb-0">{{ $company->postal_address_line1 }}</p>
                <p class="mb-0">{{ $company->postal_address_line2 }}</p>
                @if($company->postal_address3 !== null)
                    <p class="mb-0">{{ $company->postal_address_line3 }}</p>
                @endif
                <p class="mb-0">{{ $company->state_province }}</p>
                <p class="mb-0">{{ $company->postal_zipcode }}</p>
                <p class="mb-0">{{ $company->phone }}</p>
                <p class="mb-0">{{ $company->email }}</p>
                <p class="mb-0">VAT NO: {{ $company->vat_no }}</p>
            </div>
            <div class="col-md-4">
                <p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p><p class="mb-0 ">&nbsp;</p>
                <p class="mb-0 ">TO:</p>
                <p class="mb-0">{{$customer->customer_name}}</p>
                <p class="mb-0">
                        {{$customer->postal_address_line1}}
                    @empty($customer->postal_address_line1)
                        {{$customer->street_address_line1}}
                    @endempty
                <p class="mb-0">
                        {{$customer->postal_address_line2}}
                    @empty($customer->postal_address_line2)
                        {{$customer->street_address_line2}}
                    @endempty

                </p>
                <p class="mb-0">
                        {{$customer->postal_address_line3}}
                    @empty($customer->postal_address_line3)
                        {{$customer->street_address_line3}}
                    @endempty
                </p>
                <p class="mb-0">
                    {{$customer->city_suburb}}
                    @empty($customer->city_suburb)
                        {{$customer->street_city_suburb}}
                    @endempty
                </p>
                <p class="mb-0">{{$customer->state_province}}</p>
                <p class="mb-0">
                    {{$customer->postal_zipcode}}
                    @empty($customer->postal_zipcode)
                        {{ $customer->street_zipcode }}
                    @endempty
                </p>
            </div>
            <div class="col-md-4">
                <div class="row text-right mb-3"><div class="inv-img-wrapper" style="margin-left: auto;"><img class="pull-right" src="{{route('company_avatar', ['q'=> $company->company_logo])}}" alt="{{$company->company_name}} Logo" style="max-height: 150px;" align="right"></div></div>
                <div class="row no-gutter">
                    <div class="col-md-6">Invoice Number:</div>
                    <div class="col-md-6 text-right">{{$invoice_no}}</div>
                    <div class="col-md-6">Date:</div>
                    <div class="col-md-6 text-right">{{$date}}</div>
                    <div class="col-md-6">Page:</div>
                    <div class="col-md-6 text-right">1/1</div>
                    <div class="col-md-4">Reference:</div>
                    <div class="col-md-8 text-right">{{ $timesheet->first()?->project?->project_ref}}</div>
                    <div class="col-md-6">Sales Rep:</div>
                    <div class="col-md-6 text-right">{{ ' ' }}</div>
                    <div class="col-md-6">Due Date:</div>
                    <div class="col-md-6 text-right">{{ $timesheet->first()?->project?->end_date }}</div>
                    <div class="col-md-6">customer Contact:</div>
                    <div class="col-md-6 text-right">{{ $customer->contact_firstname." ".$customer->contact_lastname }}</div>
                    <div class="col-md-6">Vat Number:</div>
                    <div class="col-md-6 text-right">{{ $customer->vat_no }}</div>
                </div>
            </div>
            <div class="col-md-12 border-bottom border-dark pb-3"></div>
            <div class="table-responsive">
                <div class="table-responsive col-12 px-0">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Description</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right">Excl. Price</th>
                            <th class="text-right">VAT(%)</th>
                            <th class="text-right">Exclusive Total</th>
                            <th class="text-right">Inclusive Total</th>
                        </tr>
                        </thead>
                        <tbody>
                           @forelse($lineItems as $line)
                               <tr>
                                   <td>{{$line->first()?->description}}</td>
                                   <td class="text-right">{{number_format($line->sum('time'), 2)}}</td>
                                   <td class="text-right">{{$line->first()?->currency.' '.number_format($line->first()?->rate, 2)}}</td>
                                   <td class="text-right">{{ number_format($line->first()?->vat).'%' }}</td>
                                   <td class="text-right">{{ $line->first()?->currency.' '. number_format($line->sum('exc_price'), 2)}}</td>
                                   <td class="text-right">{{$line->first()?->currency.' '. number_format($line->sum('inc_price'), 2)}}</td>
                               </tr>
                               @empty
                           @endforelse

                           @if($expenses->isNotEmpty())
                               <tr>
                                   <td colspan="6">&nbsp;</td>
                               </tr>
                               <tr class="btn-dark">
                                   <th colspan="6">Expenses</th>
                               </tr>
                           @endif

                           @forelse($expenses as $key => $expense)
                               <tr>
                                   <td>{{ $expense->date.' '.$expense->description }}</td>
                                   <td>1</td>
                                   <td>R {{ $expense->amount }}</td>
                                   <td>0%</td>
                                   <td class="text-right">R {{ number_format($expense->amount,2) }}</td>
                                   <td class="text-right">R {{ number_format($expense->amount,2) }}</td>
                               </tr>
                               @empty
                           @endforelse
                           <tr>
                               <td colspan="6">&nbsp;</td>
                           </tr>
                           <tr>
                               <th>Payment Information</th>
                               <td colspan="3">{{ ($period == 0) ? $date_range : $period.', '.$invoice_message }}</td>
                               <th class="text-right">Total Exclusive:</th>
                               <td class="text-right">R {{ number_format($exclusive_total ,2) }}</td>
                           </tr>
                           <tr>
                               <td colspan="6">{{ $company->company_name }}</td>
                           </tr>
                            <tr>
                                <td colspan="4">{{ $company->bank_name }}</td>
                                <th class="text-right">Total VAT:</th>
                                <td class="text-right">R {{ number_format($total_vat,2) }}</td>
                            </tr>
                           <tr>
                               <td colspan="6">Branch Code: {{ $company->branch_code }}</td>
                           </tr>
                           <tr>
                               <td colspan="4">Account Number: {{ $company->bank_acc_no }}</td>
                               <th class="text-right">Total:</th>
                               <td class="text-right">R {{ number_format($inclusive_total,2) }}</td>
                           </tr>
                           <tr>
                               <td colspan="6">Reference: {{ $customer->customer_name }}</td>
                           </tr>
                           <tr>
                               <th colspan="6" class="text-center">Thank you for your business!</th>
                           </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
