@extends('adminlte.default')

@section('title') API Requests @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('api.requests')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <p>List of requests made to API endpoints</p>
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>IP</th>
                        <th>URL</th>
                        <th>Request Made</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($api_requests as $api)
                        <tr>
                            <td>{{$api->id}}</td>
                            <td>{{$api->user}}</td>
                            <td>{{$api->request_from}}</td>
                            <td>{{$api->request_url}}</td>
                            <td>{{$api->created_at->toDateString()}}</td>
                        </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="7">No API requests available</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $api_requests->firstItem() }} - {{ $api_requests->lastItem() }} of {{ $api_requests->total() }}
                        </td>
                        <td>
                        {{ $api_requests->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{$api_requests->links()}}
        </div>
    </div>
@endsection