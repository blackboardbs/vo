@extends('adminlte.default')

@section('title') API Setup @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button class="btn btn-primary btn-sm" style="float: right;" onclick="history.back()">Back</button>
    </div>
@endsection 

@section('content')
    <br>
    <div class="col-md-12">
        <div class="container-fluid">
            <ul class="nav nav-tabs" id="myApiTab">
                <li class="active"><a data-toggle="tab" href="#apisetup">Setup</a></li>
                <li><a data-toggle="tab" href="#apirequests">Requests</a></li>
            </ul>
            <hr style="margin-top: -1px !important">
            <div class="tab-content">
                <div id="apisetup" class="tab-pane active">
                    <div class="container container-title">
                        <input id="port" type="text" hidden value="{{$port}}">
                        <form action="" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="col-md-6" style="display: inline-block">
                                    <h4 data-toggle="modal" data-target="#exampleModalCenter">How it works<sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select an API, apply filters, and use the generated URL with your Bearer Token to make a request. Click for examples."><i class="far fa-question-circle"></i></span></sup></h4>
                                    <div class="input-group">
                                        <label for="API">API</label>
                                        <select class="form-control api-input" id="api_selection" name="api_selection" onchange="toggleApiDetails()">
                                            <option value="none">Select...</option>
                                            <option value="api1">Timesheet Report</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: inline-block;">
                                    <label for="URL">URL</label>
                                    <div class="input-group" style="display:flex !important; width: 90%;">
                                        <input class="form-control api-input" type="text" id="url" name="url" readonly>
                                        <button data-toggle="tooltip" data-placement="top" title="Click to copy" type="button" class="copy-btn btn btn-secondary"><i class="fas fa-clipboard"></i></button>
                                    </div>
                                    <label for="access_token">Authorization Bearer Token</label>
                                    <div class="input-group" style="display:flex !important; width: 90%;">
                                        <input class="form-control api-input masked-input" type="text" id="access_token" name="access_token" value="{{$token->api_token}}" readonly>
                                        <button data-toggle="tooltip" data-placement="top" title="Click to copy" type="button" class="copy-btn btn btn-secondary"><i class="fas fa-clipboard"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group" style="margin-top: 2%;">
                                        <div id="ct_loader" style="display: none;">
                                            <div class="wrapper">
                                                <svg>
                                                    <text x="50%" y="50%" dy=".35em" text-anchor="middle">
                                                        <tspan class="blue">c</tspan><tspan class="grey">e</tspan>
                                                    </text>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="apiDetails" id="api1Details">
                                            <span>The Timesheet report view provides timesheet lines with hours and attirbutes. It excludes financial values</span>
                                            <div style="margin-top: 1%;" id="timesheet_api_render" name="timesheet_api_render" class="container-fluid"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="apirequests" class="tab-pane">
                    <div class="container container-title">
                        {{-- <form class="form-inline mt-3 searchform" id="searchform">
                            <div class="col-md-3">
                                <div class="form-group input-group">
                                    <label class="has-float-label">
                                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                                        <span>Matching</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('api.index')}}" class="btn btn-info w-100">Clear Filters</a>
                            </div>
                        </form> --}}
                        <div class="table-responsive">
                            <p>List of requests made to API endpoints</p>
                            <table class="table table-bordered table-sm table-hover">
                                <thead class="btn-dark">
                                    <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>IP</th>
                                        <th>URL</th>
                                        <th>Request Made</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($api_requests as $api)
                                        <tr>
                                            <td>{{$api->id}}</td>
                                            <td>{{$api->api_user->email}}</td>
                                            <td>{{$api->request_from}}</td>
                                            <td>{{$api->request_url}}</td>
                                            <td>{{$api->created_at->toDateString()}}</td>
                                        </tr>
                                        @empty
                                            <tr>
                                                <td class="text-center" colspan="7">No API requests available</td>
                                            </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                                <table class="tabel tabel-borderless" style="margin: 0 auto">
                                    <tr>
                                        <td style="vertical-align: center;">
                                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $api_requests->firstItem() }} - {{ $api_requests->lastItem() }} of {{ $api_requests->total() }}
                                        </td>
                                        <td>
                                        {{ $api_requests->appends(request()->except('page'))->links() }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            {{$api_requests->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">API Request Examples</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                  <h5>Postman</h5>
                  <span>Enter the URL and add the Token in the Authorization tab as a Bearer Token.</span>
                  <div class="container-fluid">
                    <img src="/assets/apiExamples/postman.png" alt="Postman" style="width: 90%">
                  </div>
                </div>
                <br>
                <div class="col-md-12">
                  <h5>PowerBI</h5>
                  <span>Import from web, enter the URL, and add the Token as an Authorization header prefixed with 'Bearer '.</span>
                  <div class="container-fluid">
                    <img src="/assets/apiExamples/powerbi.png" alt="PowerBI" style="width: 50%">
                  </div>
                </div>
              </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('extra-css')
    <style>
        .api-input{
            width: 400px !important;
        }

        .masked-input {
            -webkit-text-security: disc;
            -moz-text-security: disc;
            text-security: disc;
        }

        .masked-input:hover {
            -webkit-text-security: none; 
            -moz-text-security: none;
            text-security: none;
        }

        .apiDetails {
            display: none;
        }
        

        @import url("https://fonts.googleapis.com/css2?family=Yantramanav&display=swap");

        #ct_loader {
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 1000;
            background-color: #FFFFFF;
        }

        svg {
            font-family: "Yantramanav", sans-serif;
            width: 100%; height: 100%;
            margin-top: 14%; 
            margin-left: 7%;
        }
        svg text {
            font-size: 100px;
        }
        @keyframes strokeBlue {
            0%   {
                fill: rgba(72,138,204,0); stroke: rgba(54,95,160,1);
                stroke-dashoffset: 25%; stroke-dasharray: 0 50%; stroke-width: 2;
            }
            70%  {fill: rgba(72,138,204,0); stroke: rgba(54,95,160,1); }
            80%  {fill: rgba(72,138,204,0); stroke: rgba(54,95,160,1); stroke-width: 3; }
            100% {
                fill: rgba(72,138,204,1); stroke: rgba(54,95,160,0);
                stroke-dashoffset: -25%; stroke-dasharray: 50% 0; stroke-width: 0;
            }
        }

        @keyframes strokeGrey {
            0%   {
                fill: rgba(169,169,169,0); stroke: grey;
                stroke-dashoffset: 25%; stroke-dasharray: 0 50%; stroke-width: 2;
            }
            70%  {fill: rgba(169,169,169,0); stroke: grey; }
            80%  {fill: rgba(169,169,169,0); stroke: grey; stroke-width: 3; }
            100% {
                fill: rgba(169,169,169,1); stroke: grey;
                stroke-dashoffset: -25%; stroke-dasharray: 50% 0; stroke-width: 0;
            }
        }

        svg text .blue {
            stroke: #365FA0;
            animation: strokeBlue 2.5s infinite alternate;
        }
        svg text .grey {
            stroke: grey;
            animation: strokeGrey 2.5s infinite alternate;
        }

        .wrapper {background-color: #FFFFFF;}

    </style>

    
@endsection

@section('extra-js')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const copyButtons = document.querySelectorAll('.copy-btn');
            copyButtons.forEach(function(button) {
                button.addEventListener('click', function() {
                    const input = this.previousElementSibling;
                    const inputValue = input.value;
                    navigator.clipboard.writeText(inputValue).then(function() {
                        button.innerHTML = '<i class="fas fa-check"></i>';
                        setTimeout(() => {
                            button.innerHTML = '<i class="fas fa-clipboard"></i>';
                        }, 1000);
                    }, function(err) {
                        console.error('Unable to copy input value', err);
                    });
                });
            });
        });

        function toggleApiDetails() {
            var selectBox = document.getElementById("api_selection");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;

            var allApiDetailsDivs = document.getElementsByClassName("apiDetails");
            for (var i = 0; i < allApiDetailsDivs.length; i++) {
                allApiDetailsDivs[i].style.display = "none";
            }

            if (selectedValue !== "none") {
                var selectedApiDetailsDiv = document.getElementById(selectedValue + "Details");
                if (selectedApiDetailsDiv) {
                    selectedApiDetailsDiv.style.display = "block";
                }
            }

            var selectValue = selectedValue;
            var textInput = document.getElementById('url');

            if (selectValue === 'api1') {
                textInput.value = 'https://{{tenant()->domains()?->first()?->domain}}/report/timesheet_api?';
                applyFilters();
            } else if (selectValue === 'api2') {
                textInput.value = '';
            } else if (selectValue === 'api3') {
                textInput.value = '';
            }
        }

        function generate_url(selectElement) {
            var textInput = document.getElementById('url');
            var currentText = textInput.value;
            var selectedOptionName = selectElement.name;
            var selectedOptionText = selectElement.value;
            var newText = currentText + '/' + selectedOptionText;
            textInput.value = newText;
        }

        function applyFilters() {
            $('#ct_loader').show();
            var formData = $('#searchform1').serialize();
            $.ajax({
                url: '{{ route("api.timesheetApi") }}',
                type: 'GET',
                data: formData,
                success: function(response) {

                    // Update table with filtered data
                    $('#timesheet_api_render').html(response.html);
                    
                    // Update dropdowns with new data
                    if (response.dropdownData) {
                        $.each(response.dropdownData, function(key, value) {

                            var selectElement = $('[name="' + key + '"]');
                            selectElement.empty();
                            $.each(value, function(optionKey, optionValue) {
                                selectElement.append($('<option>').text(optionValue).attr('value', optionKey));
                            });
                        });
                    }
                    setTimeout(function() {
                        $('#ct_loader').hide();
                    }, 1500);
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            // Handle tab change and save to localStorage
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                localStorage.setItem('activeApiTab', $(e.target).attr('href'));
            });

            // Retrieve and activate the stored tab
            var activeTab = localStorage.getItem('activeApiTab');
            if (activeTab) {
                $('#myApiTab a[href="' + activeTab + '"]').tab('show');
                $('#myApiTab a[href="' + activeTab + '"]').parent('li').addClass('active');
            } 

            // Handle manual tab clicks
            $('#myApiTab li a').on('click', function() {
                $('#myApiTab li').removeClass('active');
                $(this).parent('li').addClass('active');
            });
        });

    </script>
@endsection
