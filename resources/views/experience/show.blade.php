@extends('adminlte.default')

@section('title') View Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('experience.edit', ['experienceid' => $experience,'cvid' => $cvid, 'res_id' => $experience->res_id])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Experience</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @php
                $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            @endphp
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Company:</th>
                        <td>{{$experience->company}}</td>
                        <th>or Select:</th>
                        <td>{{isset($experience->company_idd) ? $experience->company_idd->description : ''}}</td>
                    <tr>
                        <th>Period Start:</th>
                        <td>{{($months[substr($experience->period_end, 4, 6)]??0).' '.substr($experience->period_start, 0, 4)}}</td>
                        <th>Period End:</th>
                        <td>{{($months[substr($experience->period_end, 4, 6)]??0).' '.substr($experience->period_end, 0, 4)}}<br />
                            Current: {{( $experience->current_project == '0' ? 'No' : 'Yes' )}}</td>
                    </tr>
                    <tr>
                        <th>Project:</th>
                        <td colspan="3">{{$experience->project}}</td>
                    </tr>
                    <tr>
                        <th>Role:</th>
                        <td colspan="3">{{$experience->role}}</td>
                    </tr>
                    <tr>
                        <th>Responsibility:</th>
                        <td colspan="3">{{ $experience->responsibility }}</td>
                    </tr>
                    <tr>
                        <th>Skills or Tools:</th>
                        <td colspan="3">{{ $experience->tools }}</td>
                    </tr>
                    <tr>
                        <th>Reason for leaving:</th>
                        <td colspan="3">{{ $experience->reason_for_leaving }}</td>
                    </tr>
                    <tr>
                        <th>Row Order:</th>
                        <td>{{$experience->row_order}}</td>
                        <th>Status:</th>
                        <td>{{$experience->status->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
