@extends('adminlte.default')

@section('title') Edit Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @foreach($experience as $result)
            {{Form::open(['url' => route('experience.update',['experienceid' => $result,'cvid' => $cv, 'res_id' => $result->res_id]), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Company: <small class="text-muted d-none d-sm-inline">(required without select)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('company',$result->company,['class'=>'form-control form-control-sm'. ($errors->has('company') ? ' is-invalid' : ''),'placeholder'=>'Company'])}}
                        @foreach($errors->get('company') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>or Select: <small class="text-muted d-none d-sm-inline">(required without company)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('company_id',$company_drop_down,$result->company_id,['class'=>'form-control form-control-sm '. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('company_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                <tr>
                    <th>Period Start: <small class="text-muted d-none d-sm-inline">(required without current)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>


                        <select name="start_year" style="float: left;display:inline-block;" class="col-sm-6 form-control form-control-sm  {{($errors->has('start_year') ? ' is-invalid' : '')}}">
                            <option value="{{substr($result->period_start,0,4)}}">{{substr($result->period_start,0,4)}}</option>
                            @for($i = date('Y'); $i>1970; --$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>

                        <select name="start_month" style="float: right;display:inline-block;" class="col-sm-6 form-control form-control-sm  {{($errors->has('start_month') ? ' is-invalid' : '')}}">
                            <option value="{{substr($result->period_start,4,2)}}">{{substr($result->period_start,4,2)}}</option>
                            @for($i = 1; $i<13; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <div class="clearfix"></div>
                        @foreach($errors->get('start_year') as $error)
                            <div class="invalid-feedback col-sm-6" style="float: left;display:inline-block;">
                                {{$error}}
                            </div>
                        @endforeach
                        @foreach($errors->get('start_month') as $error)
                            <div class="invalid-feedback col-sm-6" style="float: right !important;display:inline-block;" >
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Period End:</th>
                    <td><select name="end_year" style="float: left;display:inline-block;" class="col-sm-6 form-control form-control-sm  {{($errors->has('end_year') ? ' is-invalid' : '')}}">
                            <option value="{{substr($result->period_end,0,4)}}">{{substr($result->period_end,0,4)}}</option>
                            @for($i = date('Y'); $i>1970; --$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>

                        <select name="end_month" style="float: right;display:inline-block;" class="col-sm-6 form-control form-control-sm  {{($errors->has('end_month') ? ' is-invalid' : '')}}">
                            <option value="{{substr($result->period_end,4,2)}}">{{substr($result->period_end,4,2)}}</option>
                            @for($i = 1; $i<13; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <div class="clearfix"></div>
                        @foreach($errors->get('end_year') as $error)
                            <div class="invalid-feedback col-sm-6" style="float: left;display:inline-block;">
                                {{$error}}
                            </div>
                        @endforeach
                        @foreach($errors->get('end_month') as $error)
                            <div class="invalid-feedback col-sm-6" style="float: right !important;display:inline-block;">
                                {{$error}}
                            </div>
                        @endforeach
                        Current: {{Form::select('current',array(['0'=>'No','1'=>'Yes']),$result->current_project,['class'=>'form-control form-control-sm'. ($errors->has('current') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('current') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Project:</th>
                    <td colspan="3">{{ Form::textarea('project', $result->project, ['size' => '50x5', 'class'=>'form-control form-control-sm'. ($errors->has('project') ? ' is-invalid' : ''),'placeholder'=>'Project']) }}
                        @foreach($errors->get('project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Role:</th>
                    <td colspan="3">{{Form::text('role',$result->role,['class'=>'form-control form-control-sm'. ($errors->has('role') ? ' is-invalid' : ''),'placeholder'=>'Role'])}}
                        @foreach($errors->get('role') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Responsibility: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{ Form::textarea('responsibility', $result->responsibility, ['size' => '50x5', 'class'=>'form-control form-control-sm'. ($errors->has('responsibility') ? ' is-invalid' : ''),'placeholder'=>'Responsibility']) }}
                        @foreach($errors->get('responsibility') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Skills or Tools:</th>
                    <td colspan="3">{{ Form::textarea('tools', $result->tools, ['size' => '50x5', 'class'=>'form-control form-control-sm'. ($errors->has('tools') ? ' is-invalid' : ''),'placeholder'=>'Skills or Tools']) }}
                        @foreach($errors->get('tools') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Reason for leaving:</th>
                    <td colspan="3">{{ Form::textarea('reason_for_leaving', $result->reason_for_leaving, ['size' => '50x5', 'class'=>'form-control form-control-sm'. ($errors->has('reason_for_leaving') ? ' is-invalid' : ''),'placeholder'=>'Reason for leaving']) }}
                        @foreach($errors->get('reason_for_leaving') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Row Order:</th>
                    <td>{{Form::select('row_order',$row_order,$result->row_order,['class'=>'form-control form-control-sm '. ($errors->has('row_order') ? ' is-invalid' : '')])}}
                        {{--<select name="row_order" class="form-control form-control-sm  {{($errors->has('tools') ? ' is-invalid' : '')}}">
                            <option value="{{$result->row_order}}">{{$result->row_order}}</option>
                            @for($i = 0; $i<30; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>--}}
                        @foreach($errors->get('row_order') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$result->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection
