@extends('adminlte.default')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header mb-2">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Welcome to Consulteaze!</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                &nbsp;
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
@if(Auth::user()->hasRole('admin'))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="alert alert-info mb-3" role="alert">
                You have successfully configured your personalised tenant and can now start using Consulteaze. Here are a few tips we want to share with you before you start.
                </div>
                <strong>Welcome Aboard!</strong><br />
                Now that you're part of the Consulteaze family, let's get your ship sailing smoothly with some quick and breezy master data entry.<br />
                <br />
                <strong>Why Bother?</strong><br />
                Because personalized is always better! By adding your vendors and customers, you'll unlock the full potential of our tools and make your journey even more tailored to YOUR needs.<br />
                <br />
                Ready? Set. Go!
             </div>
            <!-- /.col-md-12 -->
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card w-100 mb-2" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-body">
                      <h5 class="card-title mb-2">Complete Information</h5>
                      <p class="card-text">
                        Although you've created customers, vendors and users there is more information that should be maintained in order to ensure that Consulteaze is used to its full potential. Select the links below to update information.
                      </p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="/configs">Configs</a></li>
                            <li class="list-group-item"><a href="/customer">Customers</a></li>
                            <li class="list-group-item"><a href="/vendors">Vendors</a></li>
                            <li class="list-group-item"><a href="/contact">Contacts</a></li>
                            <li class="list-group-item"><a href="/users">Users</a></li>
                        </ul>
                    </div>
                  </div>
                <div class="card w-100 mb-2" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-body">
                      <h5 class="card-title mb-2">Next Steps</h5>
                      <p class="card-text">
                      Consulteaze requires a project and resource assignment in order to capture timesheets. This is necessary for billing and exception reporting, or managing planned tasks against a project.
                    </p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="/project/create">Create a Project</a></li>
                        <li class="list-group-item"><a href="/project">Create an Assignment (But first create a project)</a></li>
                        <li class="list-group-item"><a href="/sprint/create">Create a Sprint (optional)</a></li>
                        <li class="list-group-item"><a href="/kanban/0">Create a Task (optional)</a></li>
                        <li class="list-group-item"><a href="/timesheet/create">Create a Timesheet (But first create an Assignment)</a></li>
                    </ul>
                    </div>
                  </div>
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-body">
                      <h5 class="card-title mb-2">Other Functions Available To Resources</h5>
                      <p class="card-text">
                      </p>
                      <ul class="list-group list-group-flush">
                          <li class="list-group-item"><a href="/leave/create">Capture Leave (optional)</a></li>
                          <li class="list-group-item"><a href="/assetreg/create">Capture Equipment (optional)</a></li>
                          <li class="list-group-item"><a href="/assessment/create">Performance Assessment (optional)</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            <!-- /.col-md-12 -->
            <div class="col-lg-4">
              <strong>Navigate like a Pro!</strong><br />
              Head to the "Master Data" section – it's your backstage entrance.<br />
              We promise it's easy peasy. Just drop in names, contact info, and anything else that'll make these entries pop.<br />
              Add those special touches! You know, details like payment terms, custom fields, or any unique quirks your vendors or customers have.<br />
              <br />
              <strong>Save, Cheers and Repeat!</strong><br />
              Hit that "Save" button like a champion.<br />
              Rinse and repeat for all your contacts.<br />
              <br />
              <strong>Pro Tips</strong><br />
              Copy-paste to the rescue: Have a spreadsheet? Copy-paste like a wizard. We'll do the heavy lifting.<br />
              <br />
              <br />
              <strong>Oh, Forgot Something?</strong><br />
              No biggie! You can always pop back and make corrections whenever you want.<br />
              <br />
              <strong>Done and Dusted?</strong><br />
              Give yourself a pat on the back! Your software is now YOURS – tailor-fitted and ready for your adventures.<br />
              <br />
              Remember, if you ever need a hand or just want to share your triumphs, our support crew is waiting for you.<br />
              <br />
              Happy Sailing!
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mt-3 mb-3">
                <h4>System Administration Tasks</h4>
             </div>
            <!-- /.col-md-12 -->
        </div>
        <div class="row">
            <div class="col-lg-8 d-flex align-items-stretch w-100">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header">
                        <h5 class="card-title">Whitelist Our Home Harbor</h5>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-lg-6 d-flex align-items-stretch w-100">
                        Not receiving any emails from Consulteaze? Be sure to whitelist the ‘consulteaze.com’ domain in your email administration to ensure important alerts and messages can safely dock in your inbox. 
                        </div>
                        <div class="col-lg-6 align-items-stretch w-100">
                        Below is a screenshot of how to whitelist the domain using Microsoft Outlook.<br /><br />
                        <a href="/assets/whitelist.png" target="_blank" style="text-decoration:none;"><img src="/assets/whitelist.png" style="max-width:300px" /></a>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex align-items-stretch w-100">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header">
                        <h5 class="card-title">Keeping Our Ship Fit: The System Health Dashboard</h5>
                    </div>
                    <div class="card-body">
                    Welcome to the heart of the ship – the System Health Dashboard!<br /><br />
                    This dashboard acts as our ship's compass, guiding us to where the waters may be a bit choppy. From here you can monitor the ship’s gauges and see actionable insights. <br /><br />
                    The System Health Dashboard is also your Captain’s Log, showing for instance closed projects with open tasks. A ship sails better when all tasks are neatly tied up – just like closing the chapter of a great voyage

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mt-3 mb-3">
                <h4>Additional Help Resources</h4>
             </div>
            <!-- /.col-md-12 -->
        </div>
        <div class="row">
            <div class="col-lg-6 d-flex align-items-stretch w-100">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header text-white bg-dark">
                        <h5 class="card-title">Customers</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="">Manage Customers</a><div class="float-right"><a href="https://youtu.be/S8UHujvaEhc" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/customer" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Create a Customer</a><div class="float-right"><a href="https://youtu.be/S8UHujvaEhc" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/customer/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Edit a Customer</a><div class="float-right"><a href="https://youtu.be/S8UHujvaEhc" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/customer" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        </ul>
                    </div>
                  </div>
             </div>
             <div class="col-lg-6 d-flex align-items-stretch">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header text-white bg-dark">
                        <h5 class="card-title">Vendors</h5>
                    </div>
                    <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="">Manage Vendors</a><div class="float-right"><a href="https://youtu.be/K0Q2Vknv5LE" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/vendors" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Create a Vendor</a><div class="float-right"><a href="https://youtu.be/K0Q2Vknv5LE" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/vendors/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Edit a Vendor</a><div class="float-right"><a href="https://youtu.be/K0Q2Vknv5LE" class="btn btn-sm btn-primary mr-2" target="_blank"><i class="fas fa-play"></i> Video</a><a href="/vendors" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                    </ul>
                    </div>
                  </div>
              </div>
              <div class="col-lg-6 d-flex align-items-stretch w-100">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header text-white bg-dark">
                        <h5 class="card-title">Contacts</h5>
                    </div>
                    <div class="card-body">
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="">Manage Contacts</a><div class="float-right"><a href="https://youtu.be/ETNlbxSgM10" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/contact" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        <li class="list-group-item"><a href="">Create a Contact</a><div class="float-right"><a href="https://youtu.be/ETNlbxSgM10" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/contact/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        <li class="list-group-item"><a href="">Edit a Contact</a><div class="float-right"><a href="https://youtu.be/ETNlbxSgM10" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/contact" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="col-lg-6 d-flex align-items-stretch w-100">
                 <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header text-white bg-dark">
                        <h5 class="card-title">Users</h5>
                    </div>
                     <div class="card-body">
                       <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="">Manage Users</a><div class="float-right"><a href="https://youtu.be/ETaYlFSymJE" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/users" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        <li class="list-group-item"><a href="">Create a User</a><div class="float-right"><a href="https://youtu.be/ETaYlFSymJE" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/users/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        <li class="list-group-item"><a href="">Edit a User</a><div class="float-right"><a href="https://youtu.be/ETaYlFSymJE" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/users" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                       </ul>
                     </div>
                   </div>
                </div>
                <div class="col-lg-6 d-flex align-items-stretch w-100">
                  <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                     <div class="card-header text-white bg-dark">
                         <h5 class="card-title">Projects</h5>
                     </div>
                      <div class="card-body">
                        <ul class="list-group list-group-flush">
                         <li class="list-group-item"><a href="">Manage Projects</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/project" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                         <li class="list-group-item"><a href="">Create a Project</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/project/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                         <li class="list-group-item"><a href="">Edit a Project</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/project" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        </ul>
                      </div>
                    </div>
                 </div>
                 <div class="col-lg-6 d-flex align-items-stretch w-100">
                   <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                      <div class="card-header text-white bg-dark">
                          <h5 class="card-title">Assignments</h5>
                      </div>
                       <div class="card-body">
                         <ul class="list-group list-group-flush">
                          <li class="list-group-item"><a href="">Manage Assignments</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/assignment" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                          <li class="list-group-item"><a href="">Create an Assignment</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/project" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                          <li class="list-group-item"><a href="">Edit an Assignment</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/assignment" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                         </ul>
                       </div>
                     </div>
                  </div>
                  <div class="col-lg-6 d-flex align-items-stretch w-100">
                    <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                       <div class="card-header text-white bg-dark">
                           <h5 class="card-title">Timesheets</h5>
                       </div>
                        <div class="card-body">
                          <ul class="list-group list-group-flush">
                           <li class="list-group-item"><a href="">Managing Timesheets</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Creating a Timesheet</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Editing a Timesheet</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Printing a Timesheet</a><div class="float-right"><a href="" target="_blank" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Adding expenses on a Timesheet</a><div class="float-right"><a href="" target="_blank" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        </ul>
                        </div>
                      </div>
                   </div>
                   <div class="col-lg-6 d-flex align-items-stretch w-100">
                     <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                        <div class="card-header text-white bg-dark">
                            <h5 class="card-title">Tasks</h5>
                        </div>
                         <div class="card-body">
                           <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="">Manage Tasks</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/kanban/0?project_id=0&on_kanban=1" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Create a Task</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/kanban/0?project_id=0&on_kanban=1" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                            <li class="list-group-item"><a href="">Edit a Task</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/kanban/0?project_id=0&on_kanban=1" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           </ul>
                         </div>
                       </div>
                    </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
@endif
@if(Auth::user()->hasAnyRole(['consultant','admin_manager','manager','vendor','contractor','recruitment']))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="alert alert-info mb-3" role="alert">
                Your user was created and you signed in on Consulteaze. There are a few tips we want to share with you before you start.
                </div>
                <strong>Welcome Aboard!</strong><br />
                Now that you're part of the Consulteaze family, let's get your ship sailing smoothly with some quick and breezy master data entry.<br /><br />
                
                <strong>Next Steps</strong><br />
                Consulteaze is a structured system that requires agreements to exist for transactions to take place.<br /><br />
                      Your administrator will have to create a project, assign you as a resource and create tasks on the project in order for you to capture timesheets. <br /><br />
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" style="border:0px !important;"><a href="/timesheet/create">Create a Timesheet</a></li>
                    </ul><br />
                    
                <strong>Other Functions Available To Consultants</strong><br />
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" style="border:0px !important;"><a href="/leave/create">Capture Leave (Optional)</a></li>
                        <li class="list-group-item" style="border:0px !important;"><a href="/assessment/create">Performance Assessment (Optional)</a></li>
                    </ul>
             </div>
            <!-- /.col-md-12 -->
        </div>
        
        <div class="row">
            <div class="col-lg-12 mt-3 mb-3">
                <h4>Additional Help Resources</h4>
             </div>
            <!-- /.col-md-12 -->
        </div>
        <div class="row">
          @if(Auth::user()->hasRole('vendor') && Auth::user()->hasAnyRole(['contractor','consultant']))
             <div class="col-lg-6 d-flex align-items-stretch">
                <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header text-white bg-dark">
                        <h5 class="card-title">Vendors</h5>
                    </div>
                    <div class="card-body">
                    <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="">Create a Vendor Invoice</a><div class="float-right"><a href="" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/vendors/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                    </ul>
                    </div>
                  </div>
              </div>
              @endif
                  <div class="col-lg-6 d-flex align-items-stretch w-100">
                    <div class="card w-100" style="border:1px solid rgba(0,0,0,.125) !important;">
                       <div class="card-header text-white bg-dark">
                           <h5 class="card-title">Timesheets</h5>
                       </div>
                        <div class="card-body">
                          <ul class="list-group list-group-flush">
                           <li class="list-group-item"><a href="">Managing Timesheets</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Creating a Timesheet</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet/create" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Editing a Timesheet</a><div class="float-right"><a href="https://youtu.be/5f8BLssJ7tc" target="_blank" class="btn btn-sm btn-primary mr-2"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Printing a Timesheet</a><div class="float-right"><a href="" target="_blank" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                           <li class="list-group-item"><a href="">Adding expenses on a Timesheet</a><div class="float-right"><a href="" target="_blank" class="btn btn-sm btn-primary mr-2 disabled"><i class="fas fa-play"></i> Video</a><a href="/timesheet" class="btn btn-sm btn-primary"><i class="fas fa-link"></i> Action</a></div></li>
                        </ul>
                        </div>
                      </div>
                   </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
@endif
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('extra-css')
<style>
    .list-group-item{
        padding:.5rem 1.25rem !important;
    }
.disabled{
    cursor:not-allowed !important;
}
    </style>
    @endsection