@extends('adminlte.default')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Configs</h1>
                </div><!-- /.col -->
                <div class="col-sm-6 text-right">
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
                        <div class="form-group" style="display: block!important;">
                            <div class="input-group">
                                <a href="javascript:void(0)" onclick="submitForm('configForm')" class="btn btn-primary" id="save">Save</a>
                            </div>
                        </div>
                    @endif
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                           {{$error}}
                        </div>
                    @endforeach
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#info" class="active" data-toggle="tab">Site Information</a>
                        </li>
                        <li class="">
                            <a href="#default" class="" data-toggle="tab">Default Settings</a>
                        </li>
                        <li class="">
                            <a href="#projects" class="" data-toggle="tab">Projects And Assignments</a>
                        </li>
                        <li class="">
                            <a href="#timesheet" class="" data-toggle="tab">Timesheet</a>
                        </li>
                        <li class="">
                            <a href="#utilization" class="" data-toggle="tab">Utilization</a>
                        </li>
                        <li class="">
                            <a href="#tax" class="" data-toggle="tab">Tax</a>
                        </li>
                        <li class="">
                            <a href="#invoice" class="" data-toggle="tab">Invoice</a>
                        </li>
                        <li class="">
                            <a href="#leave" class="" data-toggle="tab">Leave</a>
                        </li>
                        <li class="">
                            <a href="#assessment" class="" data-toggle="tab">Assessment</a>
                        </li>
                        <li class="">
                            <a href="#calendar" class="" data-toggle="tab">Calendar</a>
                        </li>
                        <li class="">
                            <a href="#stylesheet" class="" data-toggle="tab">Stylesheet</a>
                        </li>
                        <li class="">
                            <a href="#cost" class="" data-toggle="tab">Cost Calculator</a>
                        </li>
                        <li class="">
                            <a href="#recruitment" class="" data-toggle="tab">Recruitment</a>
                        </li>
                        <li class="">
                            <a href="#onboarding" class="" data-toggle="tab">Onboarding</a>
                        </li>
                        <li class="">
                            <a href="#prospect" class="" data-toggle="tab">Prospect</a>
                        </li>
                        <li class="">
                            <a href="#systemhealth" class="" data-toggle="tab">System Health</a>
                        </li>
                        <li class="">
                            <a href="#site_report" class="" data-toggle="tab">Site Report</a>
                        </li>
                    </ul>
                        {{Form::open(['url' => (isset($site_config)?route('configs.update', $site_config->id):route('configs.store')), 'method' => 'put', 'files' => true,'id'=>'configForm'])}}
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="info">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('site_title', $site_config?->site_title, ['class'=>'form-control form-control-sm'. ($errors->has('site_title') ? ' is-invalid' : ''),'placeholder'=>'Enter site title'])}}
                                        <span>Site Title</span>
                                        </label>
                                        @foreach($errors->get('site_title') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('site_name', $site_config?->site_name, ['class'=>'form-control form-control-sm'. ($errors->has('site_name') ? ' is-invalid' : ''),'placeholder'=>'Enter site name'])}}
                                        <span>Site Name</span>
                                        </label>
                                        @foreach($errors->get('site_name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::email('admin_email', $site_config?->admin_email, ['class'=>'form-control form-control-sm'. ($errors->has('admin_email') ? ' is-invalid' : '')])}}
                                        <span>Admin Email Address</span>
                                        </label>
                                        @foreach($errors->get('admin_email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('home_page_message', $site_config?->home_page_message, ['class'=>'form-control form-control-sm'. ($errors->has('home_page_message') ? ' is-invalid' : ''),'placeholder'=>'Enter home page message'])}}
                                        <span>Home Page Message</span>
                                        </label>
                                        @foreach($errors->get('home_page_message') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('company_id',$company_dropdown, $site_config?->company_id, ['class'=>'form-control'. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                                        <span>Default Company</span>
                                        </label>
                                        @foreach($errors->get('company_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('cost_center_id',$cost_center_dropdown, $site_config?->cost_center_id, ['class'=>'form-control'. ($errors->has('cost_center_id') ? ' is-invalid' : '')])}}
                                        <span>Cost Center</span>
                                        </label>
                                        @foreach($errors->get('cost_center_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::number('auditing_records', $site_config?->auditing_records, ['class'=>'form-control'. ($errors->has('auditing_records') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                        <span>Auditing Records</span>
                                        </label>
                                        @foreach($errors->get('auditing_records') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="projects">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('project_terms_id',$project_terms_dropdown ,$site_config?->project_terms_id, ['class'=>'form-control'. ($errors->has('project_terms_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Terms'])}}
                                        <span>Project Terms Vesion</span>
                                        </label>
                                        @foreach($errors->get('project_terms_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('claim_approver_id',$users_dropdown ,$site_config?->claim_approver_id, ['class'=>'form-control'. ($errors->has('claim_approver_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Claim Approver'])}}
                                        <span>Claim Approver</span>
                                        </label>
                                        @foreach($errors->get('claim_approver_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('vendor_template_id',$vendor_template_dropdown ,$site_config?->vendor_template_id, ['class'=>'form-control'. ($errors->has('vendor_template_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Template'])}}
                                        <span>Vendor Template</span>
                                        </label>
                                        @foreach($errors->get('vendor_template_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('resource_template_id',$templates_dropdown ,$site_config?->resource_template_id, ['class'=>'form-control'. ($errors->has('resource_template_id') ? ' is-invalid' : ''), 'placeholder' =>'Select Template'])}}
                                        <span>Resource Template</span>
                                        </label>
                                        @foreach($errors->get('resource_template_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('first_warning_days', $site_config?->first_warning_days, ['class'=>'form-control'. ($errors->has('first_warning_days') ? ' is-invalid' : '')])}}
                                        <span>Number Of Days Before First Warning</span>
                                        </label>
                                        @foreach($errors->get('first_warning_days') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('second_warning_days', $site_config?->second_warning_days, ['class'=>'form-control form-control-sm'. ($errors->has('second_warning_days') ? ' is-invalid' : '')])}}
                                        <span>Number Of Days Before Second Warning</span>
                                        </label>
                                        @foreach($errors->get('second_warning_days') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('last_warning_days', $site_config?->last_warning_days, ['class'=>'form-control form-control-sm'. ($errors->has('second_warning_days') ? ' is-invalid' : '')])}}
                                        <span>Number Of Days Before Last Warning</span>
                                        </label>
                                        @foreach($errors->get('last_warning_days') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('first_warning_percentage', $percentage, $site_config?->first_warning_percentage, ['class'=>'form-control'. ($errors->has('first_warning_percentage') ? ' is-invalid' : '')])}}
                                        <span>Percentage Before First Warning</span>
                                        </label>
                                        @foreach($errors->get('first_warning_days') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('second_warning_percentage', $percentage, $site_config?->second_warning_percentage, ['class'=>'form-control'. ($errors->has('second_warning_percentage') ? ' is-invalid' : '')])}}
                                        <span>Percentage Before Second Warning</span>
                                        </label>
                                        @foreach($errors->get('second_warning_percentage') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('last_warning_percentage', $percentage, $site_config?->last_warning_percentage, ['class'=>'form-control'. ($errors->has('last_warning_percentage') ? ' is-invalid' : '')])}}
                                        <span>Percendage Before Last Warning</span>
                                        </label>
                                        @foreach($errors->get('last_warning_percentage') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('timesheet_template_id', [1 => 'Standard Template', 2 => 'Weekly Template', 3 => 'Arbour Template'], $site_config?->timesheet_template_id, ['class'=>'form-control'. ($errors->has('timesheet_template_id') ? ' is-invalid' : '')])}}
                                        <span>Default Timesheet Template</span>
                                        </label>
                                        @foreach($errors->get('timesheet_template_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('client_service_agreement_id', $client_master_service_agreement, $site_config?->client_service_agreement_id, ['class'=>'form-control'. ($errors->has('client_service_agreement_id') ? ' is-invalid' : '')])}}
                                        <span>Default Master Service Agreement (Client)</span>
                                        </label>
                                        @foreach($errors->get('client_service_agreement_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                            {{Form::select('provider_service_agreement_id', $provider_master_service_agreement, $site_config->provider_service_agreement_id, ['class'=>'form-control'. ($errors->has('provider_service_agreement_id') ? ' is-invalid' : '')])}}
                                            <span>Default Master Service Agreement (Service Provider)</span>
                                        </label>
                                        @foreach($errors->get('provider_service_agreement_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('currency', $currencies, $site_config?->currency, ['class'=>'form-control'. ($errors->has('currency') ? ' is-invalid' : '')])}}
                                        <span>Default Currency</span>
                                        </label>
                                        @foreach($errors->get('currency') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('kanban_view', ['all'=> 'Project','user_story'=>'User Story','consultant'=>'Consultant','priority'=>'Priority'], $site_config?->kanban_view, ['class'=>'form-control'. ($errors->has('currency') ? ' is-invalid' : '')])}}
                                        <span>Kanban View</span>
                                        </label>
                                        @foreach($errors->get('currency') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        {{Form::number('kanban_nr_of_tasks', $site_config?->kanban_nr_of_tasks, ['class'=>'form-control form-control-sm'. ($errors->has('kanban_nr_of_tasks') ? ' is-invalid' : '')])}}
                                        <span>Number Of Tasks To Show Per Status</span>
                                        </label>
                                        @foreach($errors->get('kanban_nr_of_tasks') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                            {{Form::select('billing_cycle_id', $billing_cycle_dropdown, $site_config?->billing_cycle_id, ['class'=>'form-control'. ($errors->has('billing_cycle_id') ? ' is-invalid' : '')])}}
                                            <span>Default Billing Cycle</span>
                                        </label>
                                        @foreach($errors->get('billing_cycle_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <label class="has-float-label">
                                        <div class="col-md-12">
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showTimeframe" id="inlineCheckbox3" @if($site_config?->kanban_show_timeframe == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal" for="inlineCheckbox3">Show Dates</font>
                                            </div>
                                            <div class="form-check form-check-inline mt-2">
                                                <input class="form-check-input" type="checkbox" name="showHours" id="inlineCheckbox4" @if($site_config?->kanban_show_hours == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal" for="inlineCheckbox4">Show Hours</font>
                                            </div>
                                            <div class="form-check form-check-inline mt-2">
                                                <input class="form-check-input" type="checkbox" name="showActuals" id="inlineCheckbox5" @if($site_config?->kanban_show_actuals == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal" for="inlineCheckbox5">Show Actual</font>
                                            </div>
                                        </div>
                                        <span>Display Dates And Hours</span>
                                        </label>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <label class="has-float-label">
                                        <div class="col-md-12">
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showBacklog" id="inlineCheckboxk1" @if($site_config?->kanban_show_backlog == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show Backlog</font>
                                              </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showPlanned" id="inlineCheckboxk2" @if($site_config?->kanban_show_planned == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show Planned</font>
                                              </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showInProgress" id="inlineCheckboxk3" @if($site_config?->kanban_show_in_progress == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show In Progress</font>
                                              </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showDone" id="inlineCheckboxk4" @if($site_config?->kanban_show_done == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show Done</font>
                                              </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showImpediment" id="inlineCheckboxk5" @if($site_config?->kanban_show_impediment == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show Impediment</font>
                                              </div>
                                              <div class="form-check form-check-inline mt-2">
                                                <input class="form-check-input" type="checkbox" name="showCompleted" id="inlineCheckboxk6" @if($site_config?->kanban_show_completed == 1) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal">Show Completed</font>
                                              </div>
                                        </div>
                                        <span>Display columns on Project Kanban</span>
                                        </label>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="timesheet">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('timesheet_weeks',$site_config?->timesheet_weeks, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_weeks') ? ' is-invalid' : '')])}}
                                        <span>Default number of previous weeks to display on timesheet creation</span>
                                        </label>
                                        @foreach($errors->get('timesheet_weeks') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('timesheet_weeks_future',$site_config?->timesheet_weeks_future, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_weeks_future') ? ' is-invalid' : '')])}}
                                        <span>Timesheet Number of Weeks(Future)</span>
                                        </label>
                                        @foreach($errors->get('timesheet_weeks_future') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('default_custom_template',[1 => "Yes", 0 => "No"],$site_config?->default_custom_template, ['class'=>'form-control'. ($errors->has('default_custom_template') ? ' is-invalid' : '')])}}
                                        <span>Default Custom Template</span>
                                        </label>
                                        @foreach($errors->get('default_custom_template') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('show_delivery_type',[1 => "Yes", 0 => "No"],$site_config?->show_delivery_type, ['class'=>'form-control'. ($errors->has('show_delivery_type') ? ' is-invalid' : '')])}}
                                        <span>Include Delivery Type As A Selection On Timesheets By Default</span>
                                        </label>
                                        @foreach($errors->get('show_delivery_type') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="utilization">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('utilization_method', ['0'=>'Select','1'=>'Manual','2'=>'Dynamic'], $site_config?->utilization_method, ['class'=>'form-control'. ($errors->has('utilization_method') ? ' is-invalid' : '')])}}
                                        <span>Utilization Method</span>
                                        </label>
                                        @foreach($errors->get('utilization_method') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
    
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::number('utilization_hours_per_week', $site_config?->utilization_hours_per_week, ['class'=>'form-control form-control-sm'. ($errors->has('utilization_hours_per_week') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                        <span>Weekly hours for utilization per resource</span>
                                        </label>
                                        @foreach($errors->get('utilization_hours_per_week') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::number('utilization_cost_hours_per_week', $site_config?->utilization_cost_hours_per_week, ['class'=>'form-control form-control-sm'. ($errors->has('utilization_cost_hours_per_week') ? ' is-invalid' : ''),'placeholder'=>'Enter selection'])}}
                                        <span>Weekly hours for utilization per cost resource</span>
                                        @foreach($errors->get('utilization_cost_hours_per_week') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tax">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('vat_rate_id',$vat_rate_drop_down ,$site_config?->vat_rate_id, ['class'=>'form-control'. ($errors->has('vat_rate_id') ? ' is-invalid' : '')])}}
                                        <span>Default Tax Rate</span>
                                        </label>
                                        @foreach($errors->get('vat_rate_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="invoice">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('default_invoice_template', [
                                                'standardinvoice' => 'Standard Invoice',
                                                'invoicebytask' => 'Invoice By Task',
                                                'invoicebyweek' => 'Invoice By Week',
                                                'invoicebytaskbyresource' => 'Invoice By Task By Resource'
                                            ],($site_config->default_invoice_template??'standardinvoice'), ['class' => 'form-control', 'placeholder' => 'Select Default Template'])}}
                                            <span>Default Invoice Template</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::textarea('invoice_body_msg',$site_config?->invoice_body_msg, ['class'=>'form-control form-control-sm'. ($errors->has('invoice_body_msg') ? ' is-invalid' : ''), 'id' => 'area_editor'])}}
                                        <span>Default Invoice Email message</span>
                                        </label>
                                        @foreach($errors->get('invoice_body_msg') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_invoice_prefix', $site_config?->customer_invoice_prefix, ['class'=>'form-control form-control-sm'. ($errors->has('customer_invoice_prefix') ? ' is-invalid' : '')])}}
                                        <span>Customer Invoice Pre-Fix</span>
                                        </label>
                                        @foreach($errors->get('customer_invoice_prefix') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_invoice_format', $site_config?->customer_invoice_format, ['class'=>'form-control form-control-sm'. ($errors->has('customer_invoice_format') ? ' is-invalid' : '')])}}
                                        <span>Customer Invoice Format</span>
                                        </label>
                                        @foreach($errors->get('customer_invoice_format') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_invoice_start_number', $site_config?->customer_invoice_start_number, ['class'=>'form-control form-control-sm'. ($errors->has('customer_invoice_start_number') ? ' is-invalid' : '')])}}
                                        <span>Customer Invoice Start Number</span>
                                        </label>
                                        @foreach($errors->get('customer_invoice_start_number') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('vendor_invoice_prefix', $site_config?->vendor_invoice_prefix, ['class'=>'form-control form-control-sm'. ($errors->has('vendor_invoice_prefix') ? ' is-invalid' : '')])}}
                                        <span>Vendor Invoice Pre-Fix</span>
                                        </label>
                                        @foreach($errors->get('vendor_invoice_prefix') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('vendor_invoice_format', $site_config?->vendor_invoice_format, ['class'=>'form-control form-control-sm'. ($errors->has('vendor_invoice_format') ? ' is-invalid' : '')])}}
                                        <span>Vendor Invoice Format</span>
                                        </label>
                                        @foreach($errors->get('vendor_invoice_format') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('vendor_invoice_start_number', $site_config?->vendor_invoice_start_number, ['class'=>'form-control form-control-sm'. ($errors->has('vendor_invoice_start_number') ? ' is-invalid' : '')])}}
                                        <span>Vendor Invoice Start Number</span>
                                        </label>
                                        @foreach($errors->get('vendor_invoice_start_number') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_pro_forma_invoice_prefix', $site_config?->customer_pro_forma_invoice_prefix, ['class'=>'form-control form-control-sm'. ($errors->has('customer_pro_forma_invoice_prefix') ? ' is-invalid' : '')])}}
                                        <span>Customer Pro-Forma Invoice Pre-Fix</span>
                                        </label>
                                        @foreach($errors->get('customer_pro_forma_invoice_prefix') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_pro_forma_invoice_format', $site_config?->customer_pro_forma_invoice_format, ['class'=>'form-control form-control-sm'. ($errors->has('customer_pro_forma_invoice_format') ? ' is-invalid' : '')])}}
                                        <span>Customer Pro-Forma Invoice Format</span>
                                        </label>
                                        @foreach($errors->get('customer_pro_forma_invoice_format') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <label class="has-float-label">
                                        {{Form::text('customer_pro_forma_invoice_start_number', $site_config?->customer_pro_forma_invoice_start_number, ['class'=>'form-control form-control-sm'. ($errors->has('currency') ? ' is-invalid' : '')])}}
                                        <span>Customer Pro-Forma Invoice Start Number</span>
                                        </label>
                                        @foreach($errors->get('customer_pro_forma_invoice_start_number') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="leave">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                            <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::number('days_to_approve_leave', $site_config?->days_to_approve_leave, ['class'=>'form-control form-control-sm','placeholder'=>'Enter selection'. ($errors->has('days_to_approve_leave') ? ' is-invalid' : '')])}}
                                        <span>Number of days to approve leave</span>
                                        </label>
                                        @foreach($errors->get('days_to_approve_leave') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('annual_leave_days',$site_config?->annual_leave_days, ['class'=>'form-control form-control-sm'. ($errors->has('annual_leave_days') ? ' is-invalid' : '')])}}
                                        <span>Default Annual Leave Days</span>
                                        </label>
                                        @foreach($errors->get('annual_leave_days') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::number('sick_leave_cycle_months', $site_config?->sick_leave_cycle_months ?? 36, ['class'=>'form-control form-control-sm','placeholder'=>'Enter selection'. ($errors->has('sick_leave_cycle_months') ? ' is-invalid' : '')])}}
                                        <span>Sick leave cycle months</span>
                                        </label>
                                        @foreach($errors->get('sick_leave_cycle_months') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('sick_leave_days_per_cycle',$site_config?->sick_leave_days_per_cycle ?? 30, ['class'=>'form-control form-control-sm'. ($errors->has('sick_leave_days_per_cycle') ? ' is-invalid' : '')])}}
                                        <span>Sick leave days per cycle</span>
                                        </label>
                                        @foreach($errors->get('sick_leave_days_per_cycle') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="assessment">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::text('assessment_frequency',$site_config?->assessment_frequency, ['class'=>'form-control form-control-sm'. ($errors->has('assessment_frequency') ? ' is-invalid' : '')])}}
                                        <span>Assessment Frequency</span>
                                        </label>
                                        @foreach($errors->get('assessment_frequency') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('assessment_master[]', $assessment_master_dropdown ,explode(',',$site_config?->assessment_master), ['class'=>'form-control form-control-sm '. ($errors->has('assessment_master') ? ' is-invalid' : ''), 'multiple'])}}
                                        <span>Assessment KPI</span>
                                        </label>
                                        @foreach($errors->get('assessment_master') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group mt-3 row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::select('assessment_approver_user_id',$users_dropdown, $site_config?->assessment_approver_user_id, ['class'=>'form-control'. ($errors->has('assessment_approver_user_id') ? ' is-invalid' : '')])}}
                                        <span>Assessment Approver</span>
                                        </label>
                                        @foreach($errors->get('assessment_approver_user_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="calendar">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-2">
                                <div class="form-group mt-3 row">
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('action_colour', $site_config?->calendar_action_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Action Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('task_colour', $site_config?->calendar_task_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Task Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('leave_colour', $site_config?->calendar_leave_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Leave Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('assignment_colour', $site_config?->calendar_assignment_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Assignment Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('assessment_colour', $site_config?->calendar_assessment_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Assessment Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('anniversary_colour', $site_config?->calendar_anniversary_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Anniversary Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                        {{Form::text('events_colour', $site_config?->calendar_events_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Events Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                        {{Form::text('user_colour', $site_config?->calendar_user_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>User Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                        {{Form::text('cinvoice_colour', $site_config?->calendar_cinvoice_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Customer Invoice Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                        {{Form::text('vinvoice_colour', $site_config?->calendar_vinvoice_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Vendor Invoice Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                        {{Form::text('mcertificate_colour', $site_config?->calendar_medcert_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Medical Certificate Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <label class="has-float-label">
                                            {{Form::text('public_holidays_colour', $site_config?->calendar_public_holidays_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                            <span>Public Holidays Colour</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="stylesheet">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        <div class="col-md-12 pt-2">
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                {{Form::checkbox('background_color', false , ['class'=>'form-control form-control-sm'])}}
                                            </div>
                                        </div>
                                        <span>Use Custom Stylesheet</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('background_color', $site_config?->background_color, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Background Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('font_color', $site_config?->font_color, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Font Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('active_link', $site_config?->active_link, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Active Link</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pt-3 mt-3">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        <input type="file" name="site_logo" class="form-control-file col-md-11 d-none" onchange="readURL(this);">
                                        <small class="pl-3 pt-2" id="site_logo_name">{{$site_config?->site_logo}}</small>
                                        <span>Site Logo<font class="col-md-1"><i class="far fa-question-circle" data-toggle="tooltip" data-html="true" title="This Logo will be displayed<br><img src='{{route('company_avatar',['q'=>$site_config?->site_logo, 'site_logo' => true])}}' width='180px'/>"></i></font></span>
                                        </label>
                                        <img class="p-3" id="site_logo" src='{{route('company_avatar',['q'=>$site_config?->site_logo, 'site_logo' => true])}}' width='300px'/>
                                    </div>
                                    <!-- <div class="col-md-3 mt-3">
                                        <label class="has-float-label">
                                        <div class="col-md-12 pt-2">
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="left" {{($site_config?->logo_placement == 'left')?'checked':''}} value="left">
                                                <font class="form-check-label" for="left">Left</font>
                                            </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="center" {{($site_config?->logo_placement == 'center')?'checked':''}} value="center">
                                                <font class="form-check-label" for="center">Center</font>
                                            </div>
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="radio" name="logo_placement" id="right" {{($site_config?->logo_placement == 'right')?'checked':''}} value="right">
                                                <font class="form-check-label" for="right">Right</font>
                                            </div>
                                        </div>
                                        <span>Logo Placement</span>
                                        </label>
                                    </div> -->
                                        </div>
                                        </div>
                            <div class="col-md-12 pt-3 mt-3">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <a href="{{route('default.configs')}}" class="btn btn-sm btn-secondary" style="margin-top: 20px;">Restore Default</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="cost">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::select('cost_min_percent',$percentage ,$site_config?->cost_min_percent, ['class'=>'form-control'. ($errors->has('cost_min_percent') ? ' is-invalid' : '')])}}
                                        <span>Cost Min. Percent</span>
                                        </label>
                                        @foreach($errors->get('cost_min_percent') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::select('cost_max_percent', $percentage ,$site_config?->cost_max_percent, ['class'=>'form-control'. ($errors->has('cost_max_percent') ? ' is-invalid' : '')])}}
                                        <span>Cost Max. Percent</span>
                                        </label>
                                        @foreach($errors->get('cost_max_percent') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cost_min_colour', $site_config?->cost_min_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Cost Min. Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cost_mid_colour', $site_config?->cost_mid_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Cost Mid Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cost_max_colour', $site_config?->cost_max_colour, ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Cost Max Colour</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="recruitment">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('recruitment_default_email', $site_config?->recruitment_default_email, ['class'=>'form-control form-control-sm'. ($errors->has('recruitment_default_email') ? ' is-invalid' : '')])}}
                                        <span>Recruitment Default Email</span>
                                        </label>
                                        @foreach($errors->get('recruitment_default_email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('recruitment_default_contact_number', $site_config?->recruitment_default_contact_number, ['class'=>'form-control form-control-sm'. ($errors->has('recruitment_default_contact_number') ? ' is-invalid' : '')])}}
                                        <span>Recruitment Default Contact Number</span>
                                        </label>
                                        @foreach($errors->get('recruitment_default_contact_number') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::select('recruitment_default_cv_id', $cv_template_drop_down, isset($site_config->recruitment_default_cv_id)?$site_config->recruitment_default_cv_id:2, ['class'=>'form-control'. ($errors->has('recruitment_default_cv_id') ? ' is-invalid' : '')])}}
                                        <span>Default CV Template</span>
                                        </label>
                                        @foreach($errors->get('recruitment_default_cv_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::select('recruitment_default_company_id', $company_dropdown, $site_config?->recruitment_default_company_id??$default_company?->id, ['class'=>'form-control'. ($errors->has('recruitment_default_company_id') ? ' is-invalid' : '')])}}
                                        <span>Recruitment Default Company</span>
                                        </label>
                                        @foreach($errors->get('recruitment_default_company_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mt-3">
                                    <strong>Target Number of Open Job Specifications</strong>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('ojs_minimum_qty', (isset($site_config->ojs_minimum_qty)?$site_config->ojs_minimum_qty:10), ['class'=>'form-control form-control-sm'. ($errors->has('ojs_minimum_qty') ? ' is-invalid' : '')])}}
                                        <span>Minimum Qty</span>
                                        </label>
                                        @foreach($errors->get('ojs_minimum_qty') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('ojs_minimum_color', (isset($site_config->ojs_minimum_color)?$site_config->ojs_minimum_color:"ff0000"), ['class'=>'form-control form-control-sm jscolor'. ($errors->has('ojs_minimum_color') ? ' is-invalid' : '')])}}
                                        <span>Minimum Color</span>
                                        </label>
                                        @foreach($errors->get('ojs_minimum_color') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('ojs_between_color', (isset($site_config->ojs_between_color)?$site_config->ojs_between_color:"ffff00"), ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Between Color</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('ojs_target_qty', (isset($site_config->ojs_target_qty)?$site_config->ojs_target_qty:20), ['class'=>'form-control form-control-sm'])}}
                                        <span>Target Qty</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('ojs_target_color', (isset($site_config->ojs_target_color)?$site_config->ojs_target_color:'00ff00'), ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Target Color</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mt-3">
                                    <strong>CV Submit Month Target</strong>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_minimum_qty', $site_config?->cvsm_minimum_qty, ['class'=>'form-control form-control-sm'. ($errors->has('cvsm_minimum_qty') ? ' is-invalid' : '')])}}
                                        <span>Minimum Qty</span>
                                        </label>
                                        @foreach($errors->get('cvsm_minimum_qty') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_minimum_color', ($site_config->cvsm_minimum_color??"ff0000"), ['class'=>'form-control form-control-sm jscolor'. ($errors->has('cvsm_minimum_color') ? ' is-invalid' : '')])}}
                                        <span>Minimum Colour</span>
                                        </label>
                                        @foreach($errors->get('cvsm_minimum_color') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_between_color', $site_config->cvsm_between_color??"ffff00", ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Between Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_target_qty', $site_config?->cvsm_target_qty, ['class'=>'form-control form-control-sm'])}}
                                        <span>Target Qty</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_target_color', $site_config->cvsm_target_color??'00ff00', ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Target Colour</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mt-3">
                                    <strong>CV Submit Week Target</strong>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsw_minimum_qty', $site_config?->cvsw_minimum_qty, ['class'=>'form-control form-control-sm'. ($errors->has('cvsw_minimum_qty') ? ' is-invalid' : '')])}}
                                        <span>Minimum Qty</span>
                                        </label>
                                        @foreach($errors->get('cvsw_minimum_qty') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsw_minimum_color', ($site_config->cvsw_minimum_color??"ff0000"), ['class'=>'form-control form-control-sm jscolor'. ($errors->has('cvsw_minimum_color') ? ' is-invalid' : '')])}}
                                        <span>Minimum Colour</span>
                                        </label>
                                        @foreach($errors->get('cvsw_minimum_color') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsw_between_color', $site_config->cvsw_between_color??"ffff00", ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Between Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsw_target_qty', $site_config?->cvsw_target_qty, ['class'=>'form-control form-control-sm'])}}
                                        <span>Target Qty</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('cvsw_target_color', $site_config->cvsw_target_color??'00ff00', ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Target Colour</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mt-3">
                                    <strong>New Scouts Added Month Target</strong>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_minimum_qty', $site_config?->nsam_minimum_qty, ['class'=>'form-control form-control-sm'. ($errors->has('nsam_minimum_qty') ? ' is-invalid' : '')])}}
                                        <span>Minimum Qty</span>
                                        </label>
                                        @foreach($errors->get('nsam_minimum_qty') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('nsam_minimum_color', ($site_config->nsam_minimum_color??"ff0000"), ['class'=>'form-control form-control-sm jscolor'. ($errors->has('nsam_minimum_color') ? ' is-invalid' : '')])}}
                                        <span>Minimum Colour</span>
                                        </label>
                                        @foreach($errors->get('nsam_minimum_color') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsam_between_color', $site_config->nsam_between_color??"ffff00", ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Between Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsam_target_qty', $site_config?->nsam_target_qty, ['class'=>'form-control form-control-sm'])}}
                                        <span>Target Qty</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsam_target_color', $site_config->nsam_target_color??'00ff00', ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Target Colour</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mt-3">
                                <strong>New Scouts Added Week Target</strong>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('cvsm_minimum_qty', $site_config?->nsaw_minimum_qty, ['class'=>'form-control form-control-sm'. ($errors->has('nsaw_minimum_qty') ? ' is-invalid' : '')])}}
                                        <span>Minimum Qty</span>
                                        </label>
                                        @foreach($errors->get('nsaw_minimum_qty') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-3">
                                        <label class="has-float-label">
                                        {{Form::text('nsaw_minimum_color', ($site_config->nsaw_minimum_color??"ff0000"), ['class'=>'form-control form-control-sm jscolor'. ($errors->has('nsaw_minimum_color') ? ' is-invalid' : '')])}}
                                        <span>Minimum Colour</span>
                                        </label>
                                        @foreach($errors->get('nsaw_minimum_color') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsaw_between_color', $site_config->nsaw_between_color??"ffff00", ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Between Colour</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsaw_target_qty', $site_config?->nsaw_target_qty, ['class'=>'form-control form-control-sm'])}}
                                        <span>Target Qty</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                        {{Form::text('nsaw_target_color', $site_config?->nsaw_target_color??'00ff00', ['class'=>'form-control form-control-sm jscolor'])}}
                                        <span>Target Colour</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                        {{Form::textarea('cv_submission_text', $site_config?->cv_submission_text, ['class'=>'form-control form-control-sm'. ($errors->has('cv_submission_text') ? ' is-invalid' : ''), 'rows' => 3])}}
                                        <span>CV Submission text message</span>
                                        </label>
                                        @foreach($errors->get('cv_submission_text') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="onboarding">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <label class="has-float-label">
                                            {{Form::select('hr_user_id', $user_onboarding_drop_down, $site_config?->hr_user_id, ['class'=>'form-control'. ($errors->has('hr_user_id') ? ' is-invalid' : ''), 'id' => 'hr_user_id'])}}
                                            <span>HR User</span>
                                        </label>
                                            @foreach($errors->get('hr_email_id') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                    </div>
                                    <div class="col-md-6">
                                        <label class="has-float-label">
                                        {{Form::select('hr_approval_manager_id', $managers_drop_down, $site_config?->hr_approval_manager_id, ['class'=>'form-control '. ($errors->has('hr_approval_manager_id') ? ' is-invalid' : '')])}}
                                        <span>HR Approval Manager</span>
                                        </label>
                                        @foreach($errors->get('hr_approval_manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="has-float-label">
                                            {{Form::text('supply_chain_email', $site_config?->supply_chain_email, ['class'=>'form-control'. ($errors->has('supply_chain_email') ? ' is-invalid' : ''), 'id' => 'supply_chain_email'])}}
                                            <span>Supply Chain Email</span>
                                        </label>
                                            @foreach($errors->get('supply_chain_email') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('supply_chain_approval_manager_id', $managers_drop_down, $site_config?->supply_chain_approval_manager_id, ['class'=>'form-control'. ($errors->has('supply_chain_approval_manager_id') ? ' is-invalid' : '')])}}
                                        <span>Supply Chain Approval Manager</span>
                                        </label>
                                        @foreach($errors->get('supply_chain_approval_manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="has-float-label">
                                        {{Form::text('customer_email', $site_config?->customer_email, ['class'=>'form-control form-control-sm'. ($errors->has('customer_email') ? ' is-invalid' : ''), 'id' => 'customer_email'])}}
                                        <span>Customer Email</span>
                                        </label>
                                        @foreach($errors->get('customer_email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="has-float-label">
                                        {{Form::select('customer_approval_manager_id', $managers_drop_down, $site_config?->customer_approval_manager_id, ['class'=>'form-control'. ($errors->has('customer_approval_manager_id') ? ' is-invalid' : '')])}}
                                        <span>Customer Approval Manager</span>
                                        </label>
                                        @foreach($errors->get('customer_approval_manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </div>
    
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="prospect">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                    <div class="col-md-8 mt-3">
                                        <label class="has-float-label">
                                        <div class="col-md-12">
                                            @foreach($prospect_status as $ps)
                                            <div class="form-check form-check-inline mr-4 mt-2">
                                                <input class="form-check-input" type="checkbox" name="showprospect[]" value="{{$ps->id}}" id="inlineCheckbox{{$ps->id}}" @if(in_array($ps->id,explode(',',$site_config?->prospect_columns)??[])) checked="checked" @endif>
                                                <font class="form-check-label font-weight-normal" for="inlineCheckbox{{$ps->id}}">{{$ps->description}}</font>
                                            </div>
                                            @endforeach
                                        </div>
                                        <span>Show Prospect Status on Prospect Kanban</span>
                                        </label>
                                    </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="systemhealth">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                            <input type="number" name="sh_good_min" value="{{$site_config->sh_good_min}}" class="form-control" />
                                            <span>Good Min.</span>
                                        </label>
                                        <label class="has-float-label">
                                            <input type="number" name="sh_good_max" value="{{$site_config->sh_good_max}}" class="form-control" />
                                            <span>Good Max.</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                            <input type="number" name="sh_fair_min" value="{{$site_config->sh_fair_min}}" class="form-control" />
                                            <span>Fair Min.</span>
                                        </label>
                                        <label class="has-float-label">
                                            <input type="number" name="sh_fair_max" value="{{$site_config->sh_fair_max}}" class="form-control" />
                                            <span>Fair Max.</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="has-float-label">
                                            <input type="number" name="sh_bad_min" value="{{$site_config->sh_bad_min}}" class="form-control" />
                                            <span>Bad Min.</span>
                                        </label>
                                        <label class="has-float-label">
                                            <input type="number" name="sh_bad_max" value="{{$site_config->sh_bad_max}}" class="form-control" />
                                            <span>Bad Max.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="site_report">
                            <hr style="margin-top:-1px;"/>
                            <div class="col-md-12 pt-3">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                            <input type="text" name="site_report_title" value="{{$site_config->site_report_title}}" class="form-control" />
                                            <span>Default Report Title</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row pl-3">
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_consultants" id="incl_consultants" {{$site_config?->include_consultants == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_consultants">Include Consultants</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_task_hours" id="incl_task_hours" {{$site_config?->include_task_hours == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_task_hours">Include Task Hours</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_hours_summary" id="incl_hours_summary" {{$site_config?->include_hours_summary == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_hours_summary">Include Hours Summary</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_consultant_name" id="incl_consultant_name" {{$site_config?->include_consultant_name == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_consultant_name">Include Consultant Name</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_report_to" id="incl_report_to" {{$site_config?->include_report_to == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_report_to">Include Report To</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_risks" id="incl_risks" {{$site_config?->include_risks == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_risks">Include Risks</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_cust_logo" id="incl_cust_logo" {{$site_config?->include_customer_logo == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_cust_logo">Display Customer Logo</font>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline mr-4 mt-2">
                                            <input class="form-check-input" type="checkbox" name="incl_comp_logo" id="incl_comp_logo" {{$site_config?->include_company_logo == 1 ? 'checked="checked"' : ''}}>
                                            <font class="form-check-label font-weight-normal" for="incl_comp_logo">Display Company Logo</font>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pt-3 mt-2">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label class="has-float-label">
                                            <textarea name="site_report_footer" class="form-control">{{ $site_config?->site_report_footer != '' ? $site_config?->site_report_footer : 'Contact [Own Project manager] for more information with reference [Site report record number].'}}</textarea>
                                            <span>Default Footer Text</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{global_asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="{{global_asset('jodit-3.2.24/build/jodit.min.css')}}"/>
    <link rel="stylesheet" href="{{global_asset('css/ziehharmonika.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,700,700i" rel="stylesheet">
@endsection
@section('extra-js')

    <script src="{{global_asset('jodit-3.2.24/build/jodit.min.js')}}"></script>
    <script src="{{global_asset('jodit-3.2.24/examples/assets/prism.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    <script>
        function submitForm(val){
            console.log(val)
            $('#'+val).submit()
        }
        var editor = new Jodit('#area_editor', {
            textIcons: false,
            iframe: false,
            iframeStyle: '*,.jodit_wysiwyg {color:red;}',
            height: 300,
            defaultMode: Jodit.MODE_WYSIWYG,
            observer: {
                timeout: 100
            },
            uploader: {
                url: ''
            },
            filebrowser: {
                ajax: {
                    url: ''
                }
            },
            commandToHotkeys: {
                'openreplacedialog': 'ctrl+p'
            }

        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })

        function readURL(input) {
            if (input.files && input.files[0]) {
                console.log(input.files[0])
                $('#site_logo_name').html(input.files[0].name)
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#site_logo')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
