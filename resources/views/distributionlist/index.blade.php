@extends('adminlte.default')
@section('title') Distribution List @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('distributionlist.create')}}" class="btn btn-dark float-right" style="margin-left: 10px;"><i class="fa fa-plus"></i> Create</a>
    </div>
@endsection
<!-- Main content -->
@section('content')
    <div class="container-fluid">

        <form id="filter_form" class="form-inline mt-3" method="get" autocomplete="off">
            <div class="col-sm-3 col-sm mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm mt-2">
                <a href="{{ route('distributionlist.index') }}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table id="look_up_table" class="table table-bordered table-sm table-hover">
                    <thead>
                    <tr class="btn-dark">
                        <th>Name</th>
                        <th>Content</th>
                        <th>Destination</th>
                        <th>Run Frequency</th>
                        <th>Last Run Date</th>
                        <th>Next Run Date</th>
                        <th>Status</th>
                        <th class="text-right last">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($distributionLists as $distributionList)
                        <tr>
                            <td>{{$distributionList->name}}</td>
                            <td>{{isset($distributionListType[$distributionList->from_query_type_id]) ? $distributionListType[$distributionList->from_query_type_id] : ''}}</td>
                            <td>{{isset($distributionListType[$distributionList->to_query_type_id]) ? $distributionListType[$distributionList->to_query_type_id] : ''}}</td>
                            <td>{{$distributionList->frequency?->name}}</td>
                            <td>{{$distributionList->last_run_date}}</td>
                            <td>{{$distributionList->next_run_date}}</td>
                            <td>{{$distributionList->status?->description}}</td>
                            <td class="text-right">
                                <div class="d-flex">
                                    <a href="{{route('distributionlist.show',$distributionList)}}" class="btn btn-info btn-sm mr-1"><i class="fas fa-eye"></i></a>
                                    <a href="{{route('distributionlist.edit',$distributionList)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['distributionlist.destroy', $distributionList->id],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        </tr>
                    @empty
                        <p>No results match your search criteria.</p>
                    @endforelse
                    </tbody>
                </table>
                <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                    <table class="tabel tabel-borderless" style="margin: 0 auto">
                        <tr>
                            <td style="vertical-align: center;">
                                Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $distributionLists->firstItem() }} - {{ $distributionLists->lastItem() }} of {{ $distributionLists->total() }}
                            </td>
                            <td>
                                {{ $distributionLists->appends(request()->except('page'))->links() }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{!! asset('adminlte/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
    <style>
        .row{
            width: 100%;
        }
        .form-inline label {
            justify-content: left;
        }

        .dataTables_info{
            width: 750px;
        }

        .search_button{
            position: fixed;
            bottom: 60px;
            left: 257px;
            color: #FFF;
            text-align: center;
        }

        .pagination{
            float: right !important;
        }

        .active{
            background-color: #0e90d2 !important;
        }

        .row{
            width: 100%;
        }

        .dataTables_length{
            float: left !important;
        }

        .dataTables_filter{
            float: right !important;
        }

        .content-wrapper .pagination>li.active>a {
            color: #fff !important;
            text-decoration: none;
        }

        .pagination>li.active>a {
            text-decoration: none;
            z-index: 1;
            color: #fff;
            background-color: #000000 !important;
            border-color: #000000;
        }

    </style>
@endsection
