<!-- Start Block - Job Spec Modal -->
<div id="jobSpecModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Job Spec Filter</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
                    <div class="col-sm-3 col-sm">
                        Customer<br/>
                        {{Form::select('c',$customer_drop_down, old('c'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'c','style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Reference Number<br/>
                        {{Form::text('r',old('r'),['class'=>'form-control form-control-sm search-job-spec', 'id' => 'r', 'style'=>'width: 100%;', 'placeholder' => ''])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Profession<br/>
                        {{Form::select('p',$profession_drop_down,old('p'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'p', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Speciality<br/>
                        {{Form::select('sp',$speciality_drop_down,old('sp'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'sp', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Position Type<br/>
                        {{Form::select('e',$position_type_drop_down,old('e'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'e', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Job Origin<br/>
                        {{Form::select('o',$job_origin_drop_down,old('o'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'o', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Job Status<br/>
                        {{Form::select('s',$job_spec_status_drop_down,old('s'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 's', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Skills<br/>
                        {{Form::select('skill',$skills_drop_down,old('skill'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'skill', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Priority<br/>
                        {{Form::select('pri',$priority_drop_down,old('pri'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'pri', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        BEE<br/>
                        {{Form::select('bee',[1 => "Yes", 2 => "No"],old('bee'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'bee', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Recruiter<br/>
                        {{Form::select('recruiter',$recruiters_drop_down,old('recruiter'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'recruiter', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Rate Minimum<br/>
                        {{Form::select('rm',$money_drop_down,old('rm'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'rm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Rate Maximum<br/>
                        {{Form::select('rx',$money_drop_down,old('rx'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'rx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Salary Minimum<br/>
                        {{Form::select('sm',$money_drop_down,old('sm'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'sm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        Salary Maximum<br/>
                        {{Form::select('sx',$money_drop_down,old('sx'),['class'=>' form-control form-control-sm search-job-spec', 'id' => 'sx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
                    </div>
                    <div class="col-sm-3 col-sm">
                        <a href="#" onclick="clearJobSpecFilter()" type="submit" class="btn btn-default btn-sm col-sm-12" style="margin-top: 23px; color: black !important; border: none !important;">Clear Filters</a>
                    </div>
                </form>
                <br/>
                <input type="hidden" id="post_data_job" name="post_data_job" value="" />
                <input type="hidden" id="post_values_job" name="post_values_job" value="" />
                <div style="max-height: 450px; overflow: scroll;">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Reference Number</th>
                            <th>Position Type</th>
                            <th>Placement By</th>
                            <th>Application Close Date</th>
                            <th>Position Description</th>
                            <th>Job Status</th>
                        </tr>
                        </thead>
                        <tbody id="job-filter-content">
                        @forelse($job_specs as $job_spec)
                            <tr>
                                <td><a href="{{route('jobspec.show',$job_spec)}}">{{$job_spec->reference_number}}</a></td>
                                <td>{{isset($job_spec->positiontype->description)?$job_spec->positiontype->description:''}}</td>
                                <td>{{$job_spec->placement_by}}</td>
                                <td>{{$job_spec->applicaiton_closing_date}}</td>
                                <td>{{$job_spec->position_description}}</td>
                                <td>{{isset($job_spec->status->name)?$job_spec->status->name:''}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No records match those criteria.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer" style="display: unset;">
                <a onclick="saveJobSpecFilter()" type="button" class="btn btn-default pull-left" data-dismiss="modal">Save</a>
                <button type="button" class="btn btn-default pull-right text-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Block - Job Spec Modal -->
