<div id="customerModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Filter</h4>
            </div>
            @include('distributionlist.customer_modal_body')
            <div class="modal-footer" style="display: unset;">
                <a onclick="saveCustomerFilter()" type="button" class="btn btn-default pull-left" data-dismiss="modal">Save</a>
                <button type="button" class="btn btn-default pull-right text-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
