<div class="modal-body">
    <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
        <div class="col-sm-3 col-sm">
            Skill<br/>
            {{Form::select('ss', $skills_drop_down,old('ss'),['class'=>'form-control form-control-sm search-candidates ', 'style'=>'width: 100%;', 'id' => 'ss', 'multiple'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Main Role<br/>
            {{Form::select('role', $role_drop_down, old('role'),['class'=>'form-control form-control-sm search-candidates  col-sm-12'. ($errors->has('role') ? ' is-invalid' : ''), 'multiple', 'id' => 'role'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Profession<br/>
            {{Form::select('c_profession', $profession_drop_down, old('c_profession'),['class'=>' search-candidates form-control form-control-sm', 'id' => 'c_profession', 'style'=>'width: 100%;', 'multiple'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Speciality<br/>
            {{Form::select('c_speciality',$speciality_drop_down, old('c_speciality'),['class'=>' search-candidates form-control form-control-sm', 'id' => 'c_speciality', 'style'=>'width: 100%;', 'multiple'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Resource Type<br/>
            {{Form::select('c_rt',$resource_type_drop_down,old('c_rt'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_rt'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Place Options<br/>
            {{Form::select('c_po',[-1 => 'All', 1 => 'Permanent', 2 => 'Temporary', 3 => 'Contracting'], old('c_po'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_po'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Availability<br/>
            {{Form::select('c_a', $availability_drop_down, old('c_a'), ['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_a'])}}
        </div>
        <div class="col-sm-3 col-sm">
            BBBEE Race<br/>
            {{Form::select('c_bbe',$bbbee_race_drop_down,old('c_bbe'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_bbe'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Industry<br/>
            {{Form::select('c_i',$industry_drop_down,old('c_i'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_i'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Skill Level<br/>
            {{Form::select('c_skl',$skill_drop_down,old('c_skl'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_skl'])}}
        </div>
        <div class="col-sm-3 col-sm">
            Disability<br/>
            {{Form::select('c_d',[-1 => 'Select Disability', 1 => 'No', 2 => 'Yes'],old('c_d'),['class'=>'form-control search-candidates form-control-sm ', 'style'=>'width: 100%;','multiple', 'id' => 'c_d'])}}
        </div>
        <div class="col-sm-3 col-sm">
            <div style="margin-top: 23px;" class="input-group input-group-sm">
                {{Form::text('c_question',old('c_question'),['class'=>'form-control form-control-sm','placeholder'=>'Search...', 'id' => 'c_question'])}}
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-sm">
            <a href="#" onclick="clearCandidateFilter()" type="submit" class="btn btn-default btn-sm col-sm-12" style="margin-top: 23px; color: black !important; border: none !important;">Clear Filters</a>
        </div>
    </form>
    <br/>
    <input type="hidden" id="post_data" name="post_data" value="" />
    <input type="hidden" id="post_values" name="post_values" value="" />
    <div class="table-responsive" style="max-height: 450px; overflow: scroll;">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Quality</th>
                <th>Availability</th>
                <th>Phone</th>
            </tr>
            </thead>
            <tbody id="candidate-filter-content">
            @forelse($cv as $consultant)
                <tr>
                    <td><a href="{{route('cv.show',['cv' => $consultant->id,'resource' => $consultant->user_id])}}">{{$consultant->user->first_name}} {{$consultant->user->last_name}}</a></td>
                    <td>{{$consultant->user->email}}</td>
                    <td>{{isset($consultant->role->name)?$consultant->role->name:''}}</td>
                    <td>{{$consultant->quality->count() != null && $consultant->quality->count() > 0?$consultant->quality[$consultant->quality->count() - 1]->quality_percentage.'%':''}}</td>
                    @php
                        $latest_date = null;
                        $date = date("Y-m-d");
                        foreach($consultant->availability as $availability):
                            if($latest_date == null){
                                $latest_date = $availability->avail_date;
                            }
                            else{
                                if(date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))){
                                    $latest_date = $availability->avail_date;
                                }
                            }
                        endforeach;
                        $color = '#fff';

                        if(($latest_date != null) && date('Y-m-d') >= date('Y-m-d', strtotime($latest_date))){
                            $color = '#01FF70';
                        }

                        if(($latest_date != null) && date('Y-m-d', strtotime($date.'- 30 days')) >= date('Y-m-d', strtotime($latest_date))){
                            $color = 'orange';
                        }
                    @endphp
                    <td style="background-color: {{ $color }};">
                        @if($latest_date != null)
                            {{$latest_date}}
                        @endif
                    </td>
                    <td>{{isset($consultant->user->phone)?$consultant->user->phone:''}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No cv's match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
