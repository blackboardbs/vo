<div class="modal-body">
    <br/>
    <div style="max-height: 450px; overflow: scroll;">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                Customer<br/>
                {{Form::select('cust_customer',$customer_drop_down,old('cust_customer'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_customer','style'=>'width: 100%;', 'multiple' => 'multiple'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Profession<br/>
                {{Form::select('cust_profession',$profession_drop_down,old('cust_profession'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_profession', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Speciality<br/>
                {{Form::select('cust_speciality',$speciality_drop_down,old('cust_speciality'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_speciality', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Skills<br/>
                {{Form::select('cust_skills',$role_drop_down,old('cust_skills'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_skills', 'style'=>'width: 100%;', 'multiple' => 'multiple'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Criminal Record<br/>
                {{Form::select('cust_criminal_record', ['' => 'All', 0 => 'No', 1 => 'Yes'],old('cust_criminal_record'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_criminal_record', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Credit Check<br/>
                {{Form::select('cust_credit_check', ['' => 'All', 0 => 'No', 1 => 'Yes'],old('cust_credit_check'),['class'=>' form-control form-control-sm search-customer', 'id' => 'cust_credit_check', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                <a href="#" onclick="clearCustomerPreference()" type="submit" class="btn btn-default btn-sm col-sm-12" style="margin-top: 23px; color: black !important; border: none !important;">Clear Filters</a>
            </div>
        </form>
        <br/>
        <input type="hidden" id="post_data_customer" name="post_data_customer" value="" />
        <input type="hidden" id="post_values_customer" name="post_values_customer" value="" />
        <table class="table table-bordered table-sm table-hover">
            <thead>
                <tr class="btn-dark">
                    <th>Preference Number</th>
                    <th>Customer</th>
                    <th>Profession</th>
                    <th>Speciality</th>
                    <th>Skill</th>
                    <th>Credit Check</th>
                    <th>Criminal Record</th>
                </tr>
            </thead>
            <tbody id="customer-filter-content">
            @forelse($customer_preferences as $customer_preference)
                <tr>
                    <td><a href="{{route('customerpreference.show',$customer_preference)}}">{{$customer_preference->id}}</a></td>
                    <td>{{isset($customer_preference->customer->customer_name)?$customer_preference->customer->customer_name:''}}</td>
                    <td>{{isset($customer_preference->profession->name)?$customer_preference->profession->name:''}}</td>
                    <td>{{isset($customer_preference->speciality->name)?$customer_preference->speciality->name:''}}</td>
                    <td>{{isset($customer_preference->skill->name)?$customer_preference->skill->name:''}}</td>
                    <td>{{$customer_preference->credit_check == 1?'Yes':'No'}}</td>
                    <td>{{$customer_preference->criminal_record == 1?'Yes':'No'}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No records match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
