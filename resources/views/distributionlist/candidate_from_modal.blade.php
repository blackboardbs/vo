<!-- Start Block - Job Spec Modal -->
<div id="candidatesFromModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Candidates Filter</h4>
            </div>
            @include('distributionlist.candidate_modal')
            <div class="modal-footer" style="display: unset;">
                <a onclick="saveCandidateFilter()" type="button" class="btn btn-default pull-left" data-dismiss="modal">Save</a>
                <button type="button" class="btn btn-default pull-right text-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Block - Job Spec Modal -->
