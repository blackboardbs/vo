<!-- Start Block - Job Spec Modal -->
<div id="EmailModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Job Spec Filter</h4>
            </div>
            <div class="modal-body">
                <div style="max-height: 450px; overflow: scroll;">
                    <table class="table table-bordered table-sm table-hover">
                        <tbody>
                            <tr>
                                <th>Select Template:</th>
                                <td>
                                    {{Form::select('template_id', $distributionListDropDown, old('template_id'),['class'=>'form-control form-control-sm search-job-spec', 'id' => 'template_id', 'style'=>'width: 100%;', 'onchange' => 'selectTemplate(this)'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>From:</th>
                                <td>
                                    {{Form::text('from_md', old('from_md'),['class'=>'form-control form-control-sm search-job-spec', 'id' => 'from_md', 'style'=>'width: 100%;', 'placeholder' => ''])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Subject:</th>
                                <td>
                                    {{Form::text('subject_md', old('subject_md'),['class'=>'form-control form-control-sm search-job-spec', 'id' => 'subject_md', 'style'=>'width: 100%;', 'placeholder' => ''])}}
                                </td>
                            </tr>
                            <tr>
                                <th>From:</th>
                                <td>
                                    {{--{{Form::textarea('email_body_md', old('email_body_md'), ['size' => '20x30', 'class'=>'email_body_md form-control form-control-sm search-job-spec', 'id' => 'email_body_md', 'style'=>'width: 100%;', 'placeholder' => ''])}}--}}
                                    <textarea id="email_body_md" name="email_body_md" class="email_body_class">{{old('email_body_md')}}</textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer" style="display: unset;">
                <a onclick="saveEmail()" type="button" class="btn btn-default pull-left" data-dismiss="modal">Save</a>
                <button type="button" class="btn btn-default pull-right text-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Block - Job Spec Modal -->
