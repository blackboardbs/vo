@extends('adminlte.default')
@section('title') Edit Distribution List @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('distributionlist.update', $distributionList), 'method' => 'put','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <input type="hidden" name="to_query" id="to_query" value="{{$distributionList->to_query}}"/>
            <input type="hidden" name="from_query" id="from_query" value="{{$distributionList->from_query}}"/>
            <input type="hidden" name="from" id="from" value="{{$distributionList->from}}" />
            <input type="hidden" name="subject" id="subject" value="{{$distributionList->subject}}" />
            <input type="hidden" name="email_body" id="email_body" value="{{$distributionList->email_body}}" />
            <input type="hidden" name="candidateFilterValue" id="candidateFilterValue" value="1" />
            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}" />
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Name:</th>
                        <td>
                            {{Form::text('name', $distributionList->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name'])}}
                            @foreach($errors->get('industry') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status:</th>
                        <td>
                            {{Form::select('status_id', $status_drow_down, $distributionList->status_id, ['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'id' => 'status_id', 'placeholder' => 'Select'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Content Type:</th>
                        <td colspan="3">
                            {{Form::select('from_query_type_id', [1 => 'Candidates List', 2 => 'Jobs List', 4 => 'Upload List', 5 => 'Upload Html Template'], $distributionList->from_query_type_id, ['class'=>'form-control form-control-sm'. ($errors->has('from_query_type_id') ? ' is-invalid' : ''), 'id' => 'from_query_type_id', 'placeholder' => 'Please select...', 'onchange' => 'selectContentType(this)'])}}
                            @foreach($errors->get('from_query_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Content:</th>
                        <td colspan="3">
                            <table class="table table-borderless table-sm" style="margin-bottom: 0rem;">
                                <tbody>
                                    <tr>
                                        <td>
                                            {{Form::textarea('from_query_filters', $distributionList->from_query_filters, ['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('from_query_filters') ? ' is-invalid' : ''), 'id' => 'from_query_filters', 'readonly' => 'readonly'])}}
                                            @foreach($errors->get('from_query_filters') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td style="width: 20%">
                                            <a id="jobSpecModalButton" class="btn btn-default btn-sm" data-toggle="modal" data-target="#jobSpecModal" style="display: none;">Create Job Spec List</a>
                                            <a id="CandidateFromModalButton" class="btn btn-default btn-sm" data-toggle="modal" data-target="#candidatesFromModal" style="display: none;">Create Candidate List</a>
                                            {{Form::file('from_list',['class'=>'form-control form-control-sm'. ($errors->has('from_list') ? ' is-invalid' : ''), 'id' => 'from_list', 'placeholder'=>'Upload List', 'style' => 'display: none;'])}}
                                            @foreach($errors->get('from_list') as $error)
                                                <div class="invalid-feedback">
                                                    {{ $error}}
                                                </div>
                                            @endforeach

                                            {{Form::file('html_file',['class'=>'form-control form-control-sm'. ($errors->has('html_file') ? ' is-invalid' : ''), 'id' => 'html_file', 'placeholder'=>'Upload Html File', 'style' => 'display: none;'])}}
                                            @foreach($errors->get('html_file') as $error)
                                                <div class="invalid-feedback">
                                                    {{ $error}}
                                                </div>
                                            @endforeach
                                            {{--<a class="btn btn-sm btn-default" id="dropzoneButton" name="dropzoneButton" data-toggle="modal" data-target="#dropzoneModal" style="display: none;">Upload Html Template</a>--}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>Destination Type:</th>
                        <td colspan="3">
                            {{Form::select('to_query_type_id', [1 => 'Candidates List', 3 => 'Customers List', 4 => 'Upload List'], $distributionList->to_query_type_id, ['class'=>'form-control form-control-sm'. ($errors->has('to_query_type_id') ? ' is-invalid' : ''), 'id' => 'to_query_type_id', 'placeholder' => 'Please select...', 'onchange' => 'selectDestinationType(this)'])}}
                            @foreach($errors->get('to_query_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Destination:</th>
                        <td colspan="3">
                            <table class="table table-borderless table-sm" style="margin-bottom: 0rem;">
                                <tbody>
                                <tr>
                                    <td>
                                        {{Form::textarea('to_query_filters', $distributionList->to_query_filters, ['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('to_query_filters') ? ' is-invalid' : ''), 'id' => 'to_query_filters', 'readonly' => 'readonly'])}}
                                        @foreach($errors->get('to_query_filters') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td style="width: 20%">
                                        <a id="candidateDestModalButton" class="btn btn-default btn-sm"data-toggle="modal" data-target="#candidatesFromModal" style="display: none;">Create Candidate List</a>
                                        <a id="customerModalButton" class="btn btn-default btn-sm"data-toggle="modal" data-target="#customerModal" style="display: none;">Create Customer List</a>
                                        {{Form::file('to_list',['class'=>'form-control form-control-sm'. ($errors->has('to_list') ? ' is-invalid' : ''), 'id' => 'to_list', 'placeholder'=>'Upload List', 'style' => 'display: none;'])}}
                                        @foreach($errors->get('to_list') as $error)
                                            <div class="invalid-feedback">
                                                {{ $error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>Run Frequency:</th>
                        <td>
                            {{Form::select('run_frequency_id', $run_frequency_drow_down, $distributionList->run_frequency_id, ['class'=>'form-control form-control-sm'. ($errors->has('run_frequency_id') ? ' is-invalid' : ''), 'id' => 'run_frequency_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('run_frequency_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Next Run Date:</th>
                        <td style="width: 25%;">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td style="width: 40%;">
                                            {{Form::text('next_run_date', date('Y-m-d', strtotime($distributionList->next_run_date)), ['class'=>'form-control form-control-sm'. ($errors->has('next_run_date') ? ' is-invalid' : ''), 'id' => 'next_run_date', 'autocomplete' => 'off'])}}
                                            @foreach($errors->get('next_run_date') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{Form::select('hour', $hours_drop_down,date('H', strtotime($distributionList->next_run_date)), ['class'=>'form-control form-control-sm'. ($errors->has('hour') ? ' is-invalid' : ''), 'id' => 'hour', 'placeholder' => 'Hour'])}}
                                            @foreach($errors->get('hour') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{Form::select('minute', $minutes_drop_down, date('i', strtotime($distributionList->next_run_date)), ['class'=>'form-control form-control-sm'. ($errors->has('minute') ? ' is-invalid' : ''), 'id' => 'minute', 'placeholder' => 'Minute'])}}
                                            @foreach($errors->get('minute') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td colspan="3">
                            <a class="btn btn-default btn-sm" data-toggle="modal" data-target="#EmailModal">Email Template</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
    <!-- Start Block - Email Modal -->
    @include('distributionlist.email_body')
    <!-- End Block - Email Modal -->

    <!-- Start Block - Job Spec Modal -->
    @include('distributionlist.job_modal')
    <!-- End Block - Job Spec Modal -->

    <!-- Start Block - Candidate from Modal -->
    @include('distributionlist.candidate_from_modal')
    <!-- End Block - Candidate from Modal -->

    <!-- Start Block - Candidate from Modal -->
    {{--@include('distributionlist.candidate_to_modal')--}}
    <!-- End Block - Candidate from Modal -->

    <!-- Start Block - Customer Modal -->
    @include('distributionlist.customer_modal')
    <!-- End Block - Customer Modal -->

@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .modal-header {
            display: unset;
        }
    </style>
@endsection
@section('extra-js')
    {{--<script src="{!! asset('dropzone/dropzone.js') !!}"></script>
    <script src="{!! asset('tinymce2/js/tinymce/tinymce.min.js') !!}"></script>--}}
    <script src="{{ asset('dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('tinymce2/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('chosen/chosen.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('chosen/docsupport/init.js') }}" type="text/javascript" charset="utf-8"></script>
    <script>
        $(function(){

            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            $('#next_run_date').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('.search-job-spec').change(function(){
                filterJobSpec();
            });

            $('.search-candidates').change(function(){
                filterCandidates();
            });

            $('.search-customer').change(function(){
                getCustomerPreference();
            });

            $('#CandidateFromModalButton').on('click', function () {
                $('#candidateFilterValue').val(1);
            });

            $('#candidateDestModalButton').on('click', function () {
                $('#candidateFilterValue').val(2);
            });

            /*Dropzone.options.myAwesomeDropzone = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 7, // MB
                accept: function(file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    }
                    else { done(); }
                }
            };*/

            $('#html_file').change(function() {

                let formData = new FormData();
                let files = $('#html_file')[0].files[0];
                let _token = $('#_token').val();
                formData.append('file', files);
                formData.append('_token', _token);

                console.log(formData);

                $.ajax({
                    url: '{!! route('distributionlist.dropzoneupload', 2) !!}',
                    type: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        console.log(response);
                        if(response != 0){
                            alert('file uploaded');
                        }
                        else{
                            alert('file not uploaded');
                        }
                    },
                });

            });

        });

        function filterJobSpec(){

            var data = {
                customer: $('#c').val(),
                reference_number: $('#r').val(),
                profession: $('#p').val(),
                speciality: $('#sp').val(),
                position_type: $('#e').val(),
                job_origin: $('#o').val(),
                job_status: $('#s').val(),
                rate_minimum: $('#rm').val(),
                skills: $('#skill').val(),
                bee: $('#bee').val(),
                priority: $('#pri').val(),
                recruiter: $('#recruiter').val(),
                rate_maximum: $('#rx').val(),
                salary_minimum: $('#sm').val(),
                salary_maximum: $('#sx').val()
            };

            axios.post('/distributionlist/getjobspec', { data })
            .then(function (response) {
                console.log(response.data.post_data.data);
                console.log(response.data.searched_for);
                $('#post_data_job').val(JSON.stringify(response.data.post_data.data));
                $('#post_values_job').val(JSON.stringify(response.data.searched_for));
                let html_content = '';
                $.each(response.data.jobSpecs, function(key, value){
                    html_content += '<tr><td>'+value.reference_number+'</td><td>'+value.position_description+'</td><td>'+value.placement_by+'</td><td>'+value.applicaiton_closing_date+'</td><td>'+value.position_description+'</td><td>'+value.reference_number+'</td></tr>';
                });
                $('#job-filter-content').html(html_content);

            })
            .catch(function (error) {
                console.log(error);
            });
        }

        function clearJobSpecFilter(){
            $('#c').val('');
            $('#c').trigger("chosen:updated");
            $('#r').val('');
            $('#r').trigger("chosen:updated");
            $('#p').val('');
            $('#p').trigger("chosen:updated");
            $('#sp').val('');
            $('#sp').trigger("chosen:updated");
            $('#e').val('');
            $('#e').trigger("chosen:updated");
            $('#o').val('');
            $('#o').trigger("chosen:updated");
            $('#s').val('');
            $('#s').trigger("chosen:updated");
            $('#rm').val('');
            $('#rm').trigger("chosen:updated");
            $('#skill').val('');
            $('#skill').trigger("chosen:updated");
            $('#bee').val('');
            $('#bee').trigger("chosen:updated");
            $('#pri').val('');
            $('#pri').trigger("chosen:updated");
            $('#recruiter').val('');
            $('#recruiter').trigger("chosen:updated");
            $('#rx').val('');
            $('#rx').trigger("chosen:updated");
            $('#sm').val('');
            $('#sm').trigger("chosen:updated");
            $('#sx').val('');
            $('#sx').trigger("chosen:updated");

            filterJobSpec();
        }

        function saveJobSpecFilter(){
            console.log($('#post_values_job').val());
            $('#from_query').val($('#post_data_job').val());
            $('#from_query_filters').val($('#post_values_job').val());

            $('#jobSpecModal').modal('hide');
        }

        function filterCandidates(){

            var data = {
                ss: $('#ss').val(),
                role: $('#role').val(),
                p: $('#c_profession').val(),
                sp: $('#c_speciality').val(),
                rt: $('#c_rt').val(),
                po: $('#c_po').val(),
                a: $('#c_a').val(),
                bbe: $('#c_bbe').val(),
                i: $('#c_i').val(),
                skl: $('#c_skl').val(),
                d: $('#c_d').val(),
                q: $('#c_question').val()
            };

            axios.post('/distributionlist/getcanditates', { data })
            .then(function (response) {
                console.log(response);
                let html_content = '';
                $.each(response.data.cv, function(key, value){
                    html_content += '<tr><td>'+value.user.first_name+' '+value.user.first_name+'</td><td>'+value.user.email+'</td><td></td><td></td><td></td><td>'+value.user.phone+'</td></tr>';
                });
                console.log(response.data);
                $('#candidate-filter-content').html(html_content);
                $('#post_data').val(JSON.stringify(response.data.post_data));
                $('#post_values').val(JSON.stringify(response.data.searched_for));
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        function clearCandidateFilter(){
            $('#ss').val('');
            $('#ss').trigger("chosen:updated");
            $('#role').val('');
            $('#role').trigger("chosen:updated");
            $('#c_profession').val('');
            $('#c_profession').trigger("chosen:updated");
            $('#c_speciality').val('');
            $('#c_speciality').trigger("chosen:updated");
            $('#c_rt').val('');
            $('#c_rt').trigger("chosen:updated");
            $('#c_a').val('');
            $('#c_a').trigger("chosen:updated");
            $('#c_a').val('');
            $('#c_a').trigger("chosen:updated");
            $('#c_bbe').val('');
            $('#c_bbe').trigger("chosen:updated");
            $('#c_i').val('');
            $('#c_i').trigger("chosen:updated");
            $('#c_skl').val('');
            $('#c_skl').trigger("chosen:updated");
            $('#c_d').val('');
            $('#c_d').trigger("chosen:updated");
            $('#c_po').val('');
            $('#c_po').trigger("chosen:updated");
            $('#c_question').val('');
            filterCandidates();
        }

        function saveCandidateFilter(){

            let value = $('#candidateFilterValue').val();

            if(value == 1){
                $('#from_query').val($('#post_data').val());
                $('#from_query_filters').val($('#post_values').val());
            }

            if(value == 2){
                $('#to_query').val($('#post_data').val());
                $('#to_query_filters').val($('#post_values').val());
            }

            clearCandidateFilter();

            $('#candidatesFromModal').modal('hide');
        }

        function saveEmail(){
            let editor = tinymce.get('email_body_md');

            $('#from').val($('#from_md').val());
            $('#subject').val($('#subject_md').val());
            $('#email_body').val(editor.getContent());
            $('#EmailModal').modal('hide');
        }

        function getCustomerPreference(){
            var data = {
                cust_customer: $('#cust_customer').val(),
                cust_profession: $('#cust_profession').val(),
                cust_speciality: $('#cust_speciality').val(),
                cust_skills: $('#cust_skills').val(),
                cust_criminal_record: $('#cust_criminal_record').val(),
                cust_credit_check: $('#cust_credit_check').val()
            };

            axios.post('/distributionlist/getcustomerpreference', { data })
            .then(function (response) {
                console.log(response);
                let html_content = '';
                $.each(response.data.customer_preferences, function(key, value){
                    html_content += '<tr><td>'+value.id+'</td><td>'+(value.customer.customer_name != undefined ?value.customer.customer_name:'')+'</td><td>'+(value.profession.name != undefined ? value.profession.name : '')+'</td><td>'+(value.speciality.name != undefined ? value.speciality.name : '') +'</td><td></td><td>'+(value.credit_check == 1?'Yes':'No')+'</td><td>'+(value.criminal_record == 1?'Yes':'No')+'</td></tr>';
                });
                console.log(response.data);
                $('#customer-filter-content').html(html_content);
                $('#post_data_customer').val(JSON.stringify(response.data.post_data));
                $('#post_values_customer').val(JSON.stringify(response.data.searched_for));
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        function clearCustomerPreference() {
            $('#cust_customer').val('');
            $('#cust_customer').trigger("chosen:updated");
            $('#cust_profession').val('');
            $('#cust_profession').trigger("chosen:updated");
            $('#cust_speciality').val('');
            $('#cust_speciality').trigger("chosen:updated");
            $('#cust_skills').val('');
            $('#cust_skills').trigger("chosen:updated");
            $('#cust_criminal_record').val('');
            $('#cust_criminal_record').trigger("chosen:updated");
            $('#cust_credit_check').val('');
            $('#cust_credit_check').trigger("chosen:updated");
            getCustomerPreference();
        }

        function saveCustomerFilter(){

            $('#to_query').val($('#post_data_customer').val());
            $('#to_query_filters').val($('#post_values_customer').val());

            $('#customerModal').modal('hide');
        }

        function selectContentType(element){

            let template_id = element.options[element.selectedIndex].value;

            if(template_id == 1){
                $('#jobSpecModalButton').hide();
                $('#CandidateFromModalButton').show();
                $('#from_list').hide();
                $('#html_file').hide();
            } else if(template_id == 2) {
                $('#jobSpecModalButton').show();
                $('#CandidateFromModalButton').hide();
                $('#from_list').hide();
                $('#html_file').hide();
            } else if(template_id == 4) {
                $('#jobSpecModalButton').hide();
                $('#CandidateFromModalButton').hide();
                $('#html_file').hide();
                $('#from_list').show();
            } else if(template_id == 5) {
                $('#jobSpecModalButton').hide();
                $('#CandidateFromModalButton').hide();
                $('#from_list').hide();
                $('#html_file').show();
            } else {
                $('#jobSpecModalButton').hide();
                $('#CandidateFromModalButton').hide();
                $('#from_list').hide();
            }

        }

        function selectDestinationType(element){

            let template_id = element.options[element.selectedIndex].value;

            if(template_id == 1){
                $('#candidateDestModalButton').show();
                $('#customerModalButton').hide();
                $('#to_list').hide();
            } else if(template_id == 3) {
                $('#candidateDestModalButton').hide();
                $('#customerModalButton').show();
                $('#to_list').hide();
            } else if(template_id == 4) {
                $('#candidateDestModalButton').hide();
                $('#customerModalButton').hide();
                $('#to_list').show();
            } else {
                $('#candidateDestModalButton').hide();
                $('#customerModalButton').hide();
                $('#to_list').hide();
            }

        }

        function selectTemplate(element){
            // let selectedValue = $('#template_id :selected').val();
            let template_id = element.options[element.selectedIndex].value;

            axios.get('/distributionlist/getemailtemplate/'+template_id, {})
            .then(function (response) {
                // console.log(response.data.emailTemplate.email_body);
                $('#from_md').val(response.data.emailTemplate.from);
                $('#subject_md').val(response.data.emailTemplate.subject);
                /*$('#email_body_md').val(response.data.emailTemplate.email_body);*/
                /*$('#email_body_md').html(response.data.emailTemplate.email_body);*/

                var activeEditor = tinyMCE.get('email_body_md');
                activeEditor.setContent(response.data.emailTemplate.email_body);

                /*editor.insertContent( response.data.emailTemplate.email_body );*/
            })
            .catch(function (error) {
                console.log(error);
            });

        }

        let editor = tinymce.init({selector:'textarea#email_body_md'});
    </script>
@endsection
@section('extra-css')
    <style>
        @media (min-width: 992px) {
            .modal-lg {
                max-width: 950px;
            }
        }

        .modal-header {
            display: unset;
        }
    </style>
@endsection
