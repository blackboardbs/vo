<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <title>{{config('app.name')}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{!! global_asset('assets/favico.png') !!}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="{{global_asset('css/proud.css')}}">
    <style>
        .password-request {
            margin-top: 0;
            color: #007bff;
            text-decoration: underline;
        }
    </style>
</head>

<body id="gradient-selector">
<div class="content p-3">
        @if(session()->has('license_error'))
            <div class="modal fade show" id="license" tabindex="-1" role="dialog" aria-modal="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                                    {!! session()->get('license_error') !!}
                        </div>
                        <div class="modal-footer text-center d-block">
                            <button type="button" class="btn btn-dark" id="closeL">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <div class="card">
        <div class="card-body px-4 pb-4 pt-2">
            <div class="text-center">
                <a href="{{url('/')}}" class="header-link"><img src="{!! $logo??global_asset('assets/consulteaze_logo.png') !!}" onerror="this.src='{{global_asset('assets/consulteaze_logo.png')}}'" alt="{{($config->company->company_name??'Ofsy')}}" id="brand-info" style="max-height: 90px"/></a>
                <h5><span class="badge badge-secondary" style="text-transform: uppercase">-&nbsp;{{($config->company->company_name??'Consulteaze')}}&nbsp;-</span></h5>
            </div>
            <hr>
            @yield('proud-form')
        </div>
        @yield('proud-footer')
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script>
    $(function () {

        @if(session()->has('license_error'))
            showModal('show');
        @endif

        $('#closeL').on('click',function(){
            showModal('hide');
        })

        function showModal(val){
            $("#license").modal(val);
        }

        function lightOrDark(color) {

            // Variables for red, green, blue values
            var r, g, b, hsp;

            // Check the format of the color, HEX or RGB?
            if (color.match(/^rgb/)) {

                // If HEX --> store the red, green, blue values in separate variables
                color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);

                r = color[1];
                g = color[2];
                b = color[3];
            }
            else {

                // If RGB --> Convert it to HEX: http://gist.github.com/983661
                color = +("0x" + color.slice(1).replace(
                    color.length < 5 && /./g, '$&$&'));

                r = color >> 16;
                g = color >> 8 & 255;
                b = color & 255;
            }

            // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
            hsp = Math.sqrt(
                0.299 * (r * r) +
                0.587 * (g * g) +
                0.114 * (b * b)
            );

            // Using the HSP value, determine whether the color is light or dark
            if (hsp>127.5) {
                return 1;
            }else {
                return 0;
            }
        }

        function LightenDarkenColor(col, amt) {

            var usePound = false;

            if (col[0] == "#") {
                col = col.slice(1);
                usePound = true;
            }

            var num = parseInt(col,16);

            var r = (num >> 16) + amt;

            if (r > 255) r = 255;
            else if  (r < 0) r = 0;

            var b = ((num >> 8) & 0x00FF) + amt;

            if (b > 255) b = 255;
            else if  (b < 0) b = 0;

            var g = (num & 0x0000FF) + amt;

            if (g > 255) g = 255;
            else if (g < 0) g = 0;

            return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);

        }

        $(function () {
            let logo = $("#brand-info");
            if (logo.width() > 450){
                logo.css("width","100%")
            } else if (logo.height() > 90){
                logo.css("height","90px")
            } else {
                //logo.css("height","90px")
            }
        })

        let darker = LightenDarkenColor("#{{$config->background_color??'4361ee'}}", -90);
        let lighter = LightenDarkenColor("#{{$config->background_color??'4361ee'}}", 90);
        if (lightOrDark("#{{$config->background_color??'4361ee'}}")) {
            $('#gradient-selector').css('background', 'linear-gradient(90deg,#{{$config->background_color??'4361ee'}} ,'+darker+')')
        }else {
            $('#gradient-selector').css('background', 'linear-gradient(90deg,#{{$config->background_color??'4361ee'}} ,'+lighter+')')
        }
    })
</script>
</body>
</html>