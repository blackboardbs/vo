@extends('adminlte.default')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">System Health</h1>
            </div>
            <div class="col-sm-6 text-right">
                <h5 class="m-0 text-dark">Overall Health: {{number_format(($calc/($total > 0 ? $total : 1))*100,2)}}%</h5>
            </div>
        </div>
            <hr />
    </div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            @foreach ($systems as $system)
            
                    <system-health :category="{{$system}}" :ranges={{json_encode(['sh_good_min'=>$config->sh_good_min,'sh_good_max'=>$config->sh_good_max,'sh_fair_min'=>$config->sh_fair_min,'sh_fair_max'=>$config->sh_fair_max,'sh_bad_min'=>$config->sh_bad_min,'sh_bad_max'=>$config->sh_bad_max])}}></system-health>
            @endforeach
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection