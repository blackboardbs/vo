@extends('adminlte.default')
@section('title') Add Action @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        <button type="button" class="btn btn-success float-right mr-1 ml-1" data-toggle="modal" data-target="#exampleModal">
            Add Task
        </button>
            <a href="javascript:void(0)" onclick="saveForm('createAction')" class="btn btn-primary ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('action.store'), 'method' => 'post','class'=>'mt-3', 'enctype' => 'multipart/form-data','id'=>'createAction'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Subject <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('subject', old('subject'), ['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('description', old('description'), ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Action Status</th>
                    <td>
                        {{Form::select('status_id',$action_status_drop_down, 1,['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Due Date</th>
                    <td>
                        {{Form::text('due_date',old('due_date'),['class'=>'form-control datepicker form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''), 'autocomplete'=>'off'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assigned User</th>
                    <td>
                        {{Form::select('assigned_user_id',$users_drop_down,old('assigned_user_id'),['class'=>' form-control form-control-sm'. ($errors->has('assigned_user_id') ? ' is-invalid' : ''),'id'=>'assigned_user_id'])}}
                        @foreach($errors->get('assigned_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Users</th>
                    <td>
                        {{Form::select('related_users[]',$users_drop_down,old('related_users'),['class'=>'chosen-select form-control form-control-sm'. ($errors->has('related_users') ? ' is-invalid' : ''),'id'=>'related_users', 'multiple' => 'multiple'])}}
                        @foreach($errors->get('related_users') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Team</th>
                    <td>
                        {{Form::select('team_id',$teams_drop_down,old('team_id'),['class'=>' form-control form-control-sm'. ($errors->has('team_id') ? ' is-invalid' : ''),'id'=>'team_id'])}}
                        @foreach($errors->get('team_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Dependency</th>
                    <td>
                        {{Form::select('dependency_id',$actions_drop_down,old('dependency_id'),['class'=>' form-control form-control-sm'. ($errors->has('dependency_id') ? ' is-invalid' : ''),'id'=>'dependency_id'])}}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                {{--<tr>
                    <td>Acceptance Date</td>
                    <td>
                        {{Form::text('acceptance_date', old('acceptance_date'), ['class'=>'form-control datepicker form-control-sm'. ($errors->has('acceptance_date') ? ' is-invalid' : ''), 'id'=>'acceptance_date', 'autocomplete'=>'off'])}}
                        @foreach($errors->get('acceptance_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>Completed Date</td>
                    <td>
                        {{Form::text('completed_date', old('completed_date'), ['class'=>'form-control datepicker form-control-sm'. ($errors->has('completed_date') ? ' is-invalid' : ''), 'id'=>'completed_date', 'autocomplete'=>'off'])}}
                        @foreach($errors->get('completed_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Reject Reason</td>
                    <td>
                        {{Form::select('rejection_reason_id', $rejection_reason_drop_down, old('rejection_reason_id'),['class'=>' form-control form-control-sm'. ($errors->has('rejection_reason_id') ? ' is-invalid' : ''),'id'=>'rejection_reason_id'])}}
                        @foreach($errors->get('rejection_reason_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>Reject Timestamp</td>
                    <td>
                        {{Form::text('reject_timestamp', old('reject_timestamp'), ['class'=>'datepicker form-control form-control-sm'. ($errors->has('reject_timestamp') ? ' is-invalid' : ''),'id'=>'reject_timestamp'])}}
                        @foreach($errors->get('reject_timestamp') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>--}}
                <tr>
                    <th>Related Project</th>
                    <td>
                        {{Form::select('related_project_id',$projects_drop_down,old('related_project_id'),['class'=>' form-control form-control-sm'. ($errors->has('related_project_id') ? ' is-invalid' : ''),'id'=>'related_project_id'])}}
                        @foreach($errors->get('related_project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Task</th>
                    <td>
                        {{Form::select('related_task_id', $tasks_drop_down, old('related_task_id'), ['class'=>' form-control form-control-sm'. ($errors->has('related_task_id') ? ' is-invalid' : ''),'id'=>'related_task_id'])}}
                        @foreach($errors->get('related_task_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="bg-gray-light" style="cursor: pointer" id="handler">
                    <th colspan="4" class="text-center">Related Information <span class="expand-icon float-right">+</span><span class="expand-icon float-right" style="display: none">-</span></th>
                </tr>
                <tr style="display: none" id="hidden_info">
                    <td colspan="4">
                        <table class="table table-bordered mb-0">
                            <tr>
                                <th>Related Customer</th>
                                <td>
                                    {{Form::select('related_customer_id',$customers_drop_down,old('related_customer_id'),['class'=>' form-control form-control-sm'. ($errors->has('related_customer_id') ? ' is-invalid' : ''),'id'=>'related_customer_id'])}}
                                    @foreach($errors->get('related_customer_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Related Job Spec</th>
                                <td>
                                    {{Form::select('related_job_spec_id',$job_specs_drop_down,old('related_job_spec_id'),['class'=>' form-control form-control-sm'. ($errors->has('related_job_spec_id') ? ' is-invalid' : ''),'id'=>'related_job_spec_id'])}}
                                    @foreach($errors->get('related_job_spec_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Related Customer Invoice</th>
                                <td>
                                    {{Form::select('related_customer_invoice_id',$customer_invoices_drop_down,old('related_customer_invoice_id'),['class'=>' form-control form-control-sm'. ($errors->has('related_customer_invoice_id') ? ' is-invalid' : ''),'id'=>'related_customer_invoice_id'])}}
                                    @foreach($errors->get('related_customer_invoice_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Related Vendor Invoice</th>
                                <td>
                                    {{Form::select('related_vendor_invoice_id',$vendor_invoices_drop_down,old('related_vendor_invoice_id'),['class'=>' form-control form-control-sm'. ($errors->has('related_vendor_invoice_id') ? ' is-invalid' : ''),'id'=>'related_vendor_invoice_id'])}}
                                    @foreach($errors->get('related_vendor_invoice_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Related Prospect</th>
                                <td>
                                    {{Form::select('prospect_id', $prospectDropDown, old('prospect_id'),['class'=>'form-control  form-control-sm'. ($errors->has('prospect_id') ? ' is-invalid' : ''),'id'=>'prospect_id'])}}
                                    @foreach($errors->get('prospect_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Vendor</th>
                                <td>
                                    {{Form::select('vendor_id',$vendors_drop_down,old('vendor_id'),['class'=>' form-control form-control-sm'. ($errors->has('vendor_id') ? ' is-invalid' : ''),'id'=>'vendor_id'])}}
                                    @foreach($errors->get('vendor_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <th>Board</th>
                    <td>
                        {{Form::select('board_id',$boards_drop_down,old('board_id'),['class'=>' form-control form-control-sm'. ($errors->has('board_id') ? ' is-invalid' : ''),'id'=>'board_id'])}}
                        @foreach($errors->get('board_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint</th>
                    <td>
                        {{Form::select('sprint_id',$sprints_drop_down,old('sprint_id'),['class'=>' form-control form-control-sm'. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Effort</th>
                    <td>
                        {{Form::text('estimated_effort',old('estimated_effort'),['class'=>'form-control form-control-sm'. ($errors->has('estimated_effort') ? ' is-invalid' : ''),'id'=>'estimated_effort'])}}
                        @foreach($errors->get('estimated_effort') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Actual Hours</th>
                    <td>
                        {{Form::text('actual_hours',old('actual_hours'),['class'=>'form-control form-control-sm'. ($errors->has('actual_hours') ? ' is-invalid' : ''),'id'=>'actual_hours'])}}
                        @foreach($errors->get('actual_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Detail</th>
                    <td colspan="3">
                        {{Form::textarea('detail',old('detail'),['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('detail') ? ' is-invalid' : ''),'id'=>'detail'])}}
                        @foreach($errors->get('detail') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Link</th>
                    <td colspan="3">
                        {{Form::text('link',old('link'),['class'=>'form-control form-control-sm'. ($errors->has('link') ? ' is-invalid' : ''),'id'=>'link'])}}
                        @foreach($errors->get('link') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Document</th>
                    <td colspan="3">
                        <input type="file" id="file" name="file[]" class='form-control form-control-sm {{$errors->has('file') ? 'is-invalid' : ''}}' multiple="multiple"/>
                        @foreach($errors->get('file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Priority</th>
                    <td>
                        {{Form::select('priority_id',['' => 'Please select...', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],null,['class'=>'form-control form-control-sm '. ($errors->has('priority_id') ? ' is-invalid' : ''),'id'=>'priority_id'])}}
                        @foreach($errors->get('priority_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Send Notification Assigned?</th>
                    <td>
                        <label for="send_notification_assigned_yes">Yes</label>
                        <input type="radio" name="send_notification_assigned" value="1" id="send_notification_assigned_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_assigned_no">No</label>
                        <input type="radio" name="send_notification_assigned" checked value="0" id="send_notification_assigned_no">
                        {{--{{Form::select('send_notification_assigned',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_assigned') ? ' is-invalid' : ''),'id'=>'send_notification_assigned'])}}
                        @foreach($errors->get('send_notification_assigned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                </tr>
                <tr>
                    <th>Send notification created?</th>
                    <td>
                        <label for="send_notification_created_yes">Yes</label>
                        <input type="radio" name="send_notification_created" value="1" id="send_notification_created_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_created_no">No</label>
                        <input type="radio" name="send_notification_created" checked value="0" id="send_notification_created_no">
                        {{--{{Form::select('send_notification_created',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_created') ? ' is-invalid' : ''),'id'=>'send_notification_created'])}}
                        @foreach($errors->get('send_notification_created') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                    <th>Send Notification related?</th>
                    <td>
                        <label for="send_notification_related_yes">Yes</label>
                        <input type="radio" name="send_notification_related" value="1" id="send_notification_related_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_related_no">No</label>
                        <input type="radio" name="send_notification_related" checked value="0" id="send_notification_related_no">
                        {{--{{Form::select('send_notification_related',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_related') ? ' is-invalid' : ''),'id'=>'send_notification_related'])}}
                        @foreach($errors->get('send_notification_related') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>

        {{----------------------- CREATE TASK -----------------------}}

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create A Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-sm mt-3">
                            <tbody>
                            <tr>
                                <th style='width: 20%'>Description:</th>
                                <td style="width: 30%;">
                                    {{Form::text('description',old('description'),['class'=>'form-control form-control-sm col-sm-12', 'placeholder'=>'Description', 'id' => 'description'])}}
                                </td>
                                <th style='width: 20%'>Dependency</th>
                                <td style="width: 30%;">
                                    {{Form::select('dependency_task_id',$tasks_drop_down, null,['class'=>'form-control form-control-sm  col-sm-12  ', 'id' => 'dependency_task_id'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Hours</th>
                                <td>
                                    {{Form::text('hours_planned',old('hours_planned'),['class'=>'form-control form-control-sm col-sm-12','placeholder'=>'Hours', 'id' => 'hours_planned'])}}
                                </td>
                                <th>Consultant</th>
                                <td>
                                    {{Form::select('consultant', $users_drop_down,null,['class'=>'form-control form-control-sm col-sm-12 ', 'placeholder' => 'Select Consultant', 'id' => 'consultant'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Customer</th>
                                <td>
                                    {{Form::select('customer',$customers_drop_down, null,['class'=>'form-control form-control-sm col-sm-12', 'id' => 'customer'])}}
                                </td>
                                <th>Project</th>
                                <td>
                                    {{Form::select('project', $projects_drop_down,null,['class'=>'form-control form-control-sm col-sm-12', 'id' => 'project'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>
                                    {{Form::text('start_date',old('start_date'),['class'=>'datepicker form-control form-control-sm col-sm-12','placeholder'=>'Start Date', 'id' => 'start_date'])}}
                                </td>
                                <th>End Date</th>
                                <td>
                                    {{Form::text('end_date',old('end_date'),['class'=>'datepicker form-control form-control-sm col-sm-12','placeholder'=>'End Date', 'id' => 'end_date'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Billable</th>
                                <td>
                                    {{Form::select('billable', [0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm col-sm-12', 'placeholder' => 'Select', 'id' => 'billable'])}}
                                </td>
                                <th>Status</th>
                                <td>
                                    {{Form::select('task_status',$task_status_drop_down, 1,['class'=>'form-control form-control-sm col-sm-12', 'placeholder' => 'Select Task Status', 'id' => 'task_status'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Sprint</th>
                                <td>
                                    {{Form::select('task_sprint_id',$sprints_drop_down, old('task_sprint_id'),['class'=>'form-control form-control-sm col-sm-12', 'id' => 'task_sprint_id'])}}
                                </td>
                                <th>Weight</th>
                                <td>
                                    {{Form::text('weight', old('weight'),['class'=>'form-control form-control-sm col-sm-12', 'placeholder' => 'Weight', 'id' => 'weight'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>User Story</th>
                                <td colspan="3">
                                    {{Form::select('user_story_id',$user_stories_drop_down, null,['class'=>'form-control form-control-sm col-sm-12', 'placeholder' => 'Please select...', 'id' => 'user_story_id'])}}
                                </td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td colspan="3">
                                    {{Form::textarea('note',null,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'note'])}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save-task">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .field {
            animation: shake 0.5s;

        }

        @keyframes shake {
            0% { transform: translate(1px, 1px) rotate(0deg); }
            10% { transform: translate(-1px, -2px) rotate(-1deg); }
            20% { transform: translate(-3px, 0px) rotate(1deg); }
            30% { transform: translate(3px, 2px) rotate(0deg); }
            40% { transform: translate(1px, -1px) rotate(1deg); }
            50% { transform: translate(-1px, 2px) rotate(-1deg); }
            60% { transform: translate(-3px, 1px) rotate(0deg); }
            70% { transform: translate(3px, 1px) rotate(-1deg); }
            80% { transform: translate(-1px, -1px) rotate(1deg); }
            90% { transform: translate(1px, 2px) rotate(0deg); }
            100% { transform: translate(1px, -2px) rotate(-1deg); }
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });

        $(function () {
            $("#related_project_id").on('change', () => {
                axios.get('/action/' + $("#related_project_id").val() +"/tasks")
                    .then(function (response) {
                        // handle success
                            let related_tasks = $("#related_task_id").html("<option value='0'>Please select...</option>");
                            response.data.tasks.forEach((task, index) => {
                                related_tasks.append("<option value='" + task.id + "'>" + task.name + "</option>");
                            });
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
            });
        })
        $(function () {
            $("#handler").on('click', () => {
                $(".expand-icon").toggle();
                $("#hidden_info").fadeToggle("slow","linear");
            })
        })

        $(function (){
            $("#related_project_id").change(() => {
                axios.get('/action/project/' + $("#related_project_id").val())
                    .then(function (response) {
                        results = response.data.project;
                        $("#project").val(results.id);
                        $("#start_date").val(results.start_date);
                        $("#end_date").val(results.end_date);
                        $("#billable").val(results.billable);
                        $("#customer").val(results.customer_id);
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
            });

            $("#save-task").on('click', () => {

                let taskFields = {
                    description: $("#description").val(),
                    dependance: $("#dependency_task_id").val(),
                    hours: $("#hours_planned").val(),
                    consultant: $("#consultant").val(),
                    customer: $("#customer").val(),
                    project: $("#project").val(),
                    start_date: $("#start_date").val(),
                    end_date: $("#end_date").val(),
                    is_billable: $("#billable").val(),
                    task_status: $("#task_status").val(),
                    task_sprint: $("#task_sprint_id").val(),
                    weight: $("#weight").val(),
                    user_story: $("#user_story_id").val(),
                    note: $("#note").val()
                };
                console.log(taskFields);
                if (taskFields.description == '') {
                    $("#description").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.hours == '') {
                    $("#hours_planned").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.consultant == '') {
                    $("#consultant").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.start_date == '') {
                    $("#start_date").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.end_date == '') {
                    $("#end_date").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.task_status == '') {
                    $("#task_status").css('border', '1px solid red').addClass('field');
                    return
                }
                if (taskFields.user_story == '') {
                    $("#user_story_id").css('border', '1px solid red').addClass('field');
                    return
                }

                axios.post('/action/store/task', {
                    taskFields: taskFields
                })
                    .then(function (response) {
                        $("#exampleModal, .modal-backdrop").hide();
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })
    </script>
@endsection