<!-- Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="configuration_label">Reject Action</h5>
                <button id="closeRejectModalBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::open(['url' => route('action.reject', $action->id), 'method' => 'post','class'=>'mt-3'])}}
                <div class='row'>
                    <div class="col-md-12">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td colspan="2">Reject this action?</td>
                            </tr>
                            <tr>
                                <td>Rejection Reason</td>
                                <td>
                                    {{Form::select('reject_reason_id', $rejection_reason_drop_down, old('reject_reason_id'),['class'=>' form-control form-control-sm'. ($errors->has('reject_reason_id') ? ' is-invalid' : ''),'id'=>'reject_reason_id'])}}
                                    @foreach($errors->get('reject_reason_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        &nbsp;
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button id="accept_btn" type="submit" class="btn  btn-success">Submit</button> <a onclick="closeRejectModal()" class="btn-default btn">Cancel</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>