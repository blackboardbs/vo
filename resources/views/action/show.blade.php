@extends('adminlte.default')
@section('title') View Action @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        
        @if($actions_flag == true)
            <button class="btn btn-success ml-1" style="color: #ffffff;" data-toggle="modal" data-target="#acceptModal"> Accept</button>
            <button class="btn btn-danger ml-1" style="color: #ffffff;" data-toggle="modal" data-target="#rejectModal"> Reject</button>
        @endif
        @if($actions_complete_flag)
            <button class="btn btn-success ml-1" style="color: #ffffff;" data-toggle="modal" data-target="#completeModal"> Complete</button>
        @endif
            <a href="{{route('action.edit', $action->id)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
        
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Subject <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('subject', $action->subject, ['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''), 'readonly' => 'readonly']) }}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('description', $action->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''), 'readonly' => 'readonly']) }}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Action Status</th>
                    <td>
                        {{Form::select('status_id',$action_status_drop_down, $action->status_id,['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'readonly' => 'readonly', 'id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Due Date</th>
                    <td>
                        {{Form::text('due_date',$action->due_date,['class'=>'form-control datepicker form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''), 'autocomplete'=>'off', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assigned User</th>
                    <td>
                        {{Form::select('assigned_user_id',$users_drop_down,$action->assigned_user_id,['class'=>' form-control form-control-sm'. ($errors->has('assigned_user_id') ? ' is-invalid' : ''),'id'=>'assigned_user_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('assigned_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Users</th>
                    <td>
                        {{Form::select('related_users[]',$users_drop_down, explode('|', $action->related_users),['class'=>' form-control form-control-sm'. ($errors->has('related_users') ? ' is-invalid' : ''),'id'=>'related_users', 'multiple' => 'multiple', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_users') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Team</th>
                    <td>
                        {{Form::select('team_id',$teams_drop_down,$action->team_id,['class'=>' form-control form-control-sm'. ($errors->has('team_id') ? ' is-invalid' : ''),'id'=>'team_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('team_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Vendor</th>
                    <td>
                        {{Form::select('vendor_id',$vendors_drop_down,$action->vendor_id,['class'=>' form-control form-control-sm'. ($errors->has('vendor_id') ? ' is-invalid' : ''),'id'=>'vendor_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('vendor_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Acceptance Date</td>
                    <td>
                        {{Form::text('acceptance_date', $action->acceptance_date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('acceptance_date') ? ' is-invalid' : ''), 'id'=>'acceptance_date', 'autocomplete'=>'off', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('acceptance_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>Completed Date</td>
                    <td>
                        {{Form::text('completed_date', $action->completed_date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('completed_date') ? ' is-invalid' : ''), 'id'=>'completed_date', 'autocomplete'=>'off', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('completed_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Reject Reason</td>
                    <td>
                        {{Form::select('rejection_reason_id', $rejection_reason_drop_down, $action->rejection_reason_id,['class'=>' form-control form-control-sm'. ($errors->has('rejection_reason_id') ? ' is-invalid' : ''),'id'=>'rejection_reason_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('rejection_reason_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td>Reject Timestamp</td>
                    <td>
                        {{Form::text('reject_timestamp', $action->reject_timestamp, ['class'=>'datepicker form-control form-control-sm'. ($errors->has('reject_timestamp') ? ' is-invalid' : ''),'id'=>'reject_timestamp', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('reject_timestamp') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Related Project</th>
                    <td>
                        {{Form::select('related_project_id',$projects_drop_down,$action->related_project_id,['class'=>' form-control form-control-sm'. ($errors->has('related_project_id') ? ' is-invalid' : ''),'id'=>'related_project_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Task</th>
                    <td>
                        {{Form::select('related_task_id', $tasks_drop_down, $action->related_task_id, ['class'=>' form-control form-control-sm'. ($errors->has('related_task_id') ? ' is-invalid' : ''),'id'=>'related_task_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_task_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Related Customer</th>
                    <td>
                        {{Form::select('related_customer_id',$customers_drop_down,$action->related_customer_id,['class'=>' form-control form-control-sm'. ($errors->has('related_customer_id') ? ' is-invalid' : ''),'id'=>'related_customer_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Job Spec</th>
                    <td>
                        {{Form::select('related_job_spec_id',$job_specs_drop_down,$action->related_job_spec_id,['class'=>' form-control form-control-sm'. ($errors->has('related_job_spec_id') ? ' is-invalid' : ''),'id'=>'related_job_spec_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_job_spec_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Related Customer Invoice</th>
                    <td>
                        {{Form::select('related_customer_invoice_id',$customer_invoices_drop_down,$action->related_customer_invoice_id,['class'=>' form-control form-control-sm'. ($errors->has('related_customer_invoice_id') ? ' is-invalid' : ''),'id'=>'related_customer_invoice_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_customer_invoice_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Vendor Invoice</th>
                    <td>
                        {{Form::select('related_vendor_invoice_id',$vendor_invoices_drop_down,$action->related_vendor_invoice_id,['class'=>' form-control form-control-sm'. ($errors->has('related_vendor_invoice_id') ? ' is-invalid' : ''),'id'=>'related_vendor_invoice_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('related_vendor_invoice_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Related Prospect</th>
                    <td>
                        {{Form::select('prospect_id', $prospectDropDown, $action->prospect_id,['class'=>'form-control  form-control-sm'. ($errors->has('prospect_id') ? ' is-invalid' : ''),'id'=>'prospect_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('prospect_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Dependency</th>
                    <td>
                        {{Form::select('dependency_id',$actions_drop_down,$action->dependency_id,['class'=>' form-control form-control-sm'. ($errors->has('dependency_id') ? ' is-invalid' : ''),'id'=>'dependency_id', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Board</th>
                    <td>
                        {{Form::select('board_id',$boards_drop_down,$action->board_id,['class'=>' form-control form-control-sm'. ($errors->has('board_id') ? ' is-invalid' : ''),'id'=>'board_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('board_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint</th>
                    <td>
                        {{Form::select('sprint_id',$sprints_drop_down,$action->sprint_id,['class'=>' form-control form-control-sm'. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Effort</th>
                    <td>
                        {{Form::text('estimated_effort',$action->estimated_effort,['class'=>'form-control form-control-sm'. ($errors->has('estimated_effort') ? ' is-invalid' : ''),'id'=>'estimated_effort', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('estimated_effort') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Actual Hours</th>
                    <td>
                        {{Form::text('actual_hours',$action->actual_hours,['class'=>'form-control form-control-sm'. ($errors->has('actual_hours') ? ' is-invalid' : ''),'id'=>'actual_hours', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('actual_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Detail</th>
                    <td colspan="3">
                        {{Form::textarea('detail',$action->detail,['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('detail') ? ' is-invalid' : ''),'id'=>'detail', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('detail') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Link</th>
                    <td colspan="3">
                        {{Form::text('link',$action->link,['class'=>'form-control form-control-sm'. ($errors->has('link') ? ' is-invalid' : ''),'id'=>'link', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('link') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Documents</th>
                    <td colspan="3">
                        @if(isset($documents))
                        <table class="table table-sm table-borderless">
                            @foreach($documents as $document)
                                <tr><td>{{ Html::link($document->file, $document->name, ['target' => '_blank']) }}</td></tr>
                            @endforeach
                        </table>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Priority</th>
                    <td>
                        {{Form::select('priority_id',['' => 'Please select...', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],$action->priority_id,['class'=>'form-control form-control-sm '. ($errors->has('priority_id') ? ' is-invalid' : ''),'id'=>'priority_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('priority_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Send Notification Assigned?</th>
                    <td>
                        {{Form::select('send_notification_assigned',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],$action->send_notification_assigned,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_assigned') ? ' is-invalid' : ''),'id'=>'send_notification_assigned', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('send_notification_assigned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Send notification created?</th>
                    <td>
                        {{Form::select('send_notification_created',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],$action->send_notification_created,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_created') ? ' is-invalid' : ''),'id'=>'send_notification_created', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('send_notification_created') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Send Notification related?</th>
                    <td>
                        {{Form::select('send_notification_related',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],$action->send_notification_related,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_related') ? ' is-invalid' : ''),'id'=>'send_notification_related', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('send_notification_related') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
        </div>

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.accept_modal')
        <!-- End Block - Candidate email CV Modal -->

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.reject_modal')
        <!-- End Block - Candidate email CV Modal -->

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.complete_modal')
        <!-- End Block - Candidate email CV Modal -->
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });

        function closeAcceptModal(){
            $('#closeAcceptModalBtn').click();
        }

        function closeRejectModal(){
            $('#closeRejectModalBtn').click();
        }

        function closeCompleteModal(){
            $('#closeCompleteModalBtn').click();
        }
    </script>
@endsection