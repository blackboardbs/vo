@extends('adminlte.default')
@section('title') View Action @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('editAction')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('action.update', $action), 'method' => 'put','class'=>'mt-3', 'enctype' => 'multipart/form-data','id'=>'editAction'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Subject <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('subject', $action->subject, ['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('subject') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{ Form::text('description', $action->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Action Status</th>
                    <td>
                        {{Form::select('status_id',$action_status_drop_down, $action->status_id, ['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Due Date</th>
                    <td>
                        {{Form::text('due_date',$action->due_date,['class'=>'form-control datepicker form-control-sm'. ($errors->has('due_date') ? ' is-invalid' : ''), 'autocomplete'=>'off'])}}
                        @foreach($errors->get('due_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Assigned User</th>
                    <td>
                        {{Form::select('assigned_user_id',$users_drop_down,$action->assigned_user_id,['class'=>' form-control form-control-sm'. ($errors->has('assigned_user_id') ? ' is-invalid' : ''),'id'=>'assigned_user_id'])}}
                        @foreach($errors->get('assigned_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Users</th>
                    <td>
                        {{Form::select('related_users[]',$users_drop_down, explode('|', $action->related_users),['class'=>' form-control form-control-sm'. ($errors->has('related_users') ? ' is-invalid' : ''),'id'=>'related_users', 'multiple' => 'multiple'])}}
                        @foreach($errors->get('related_users') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Team</th>
                    <td>
                        {{Form::select('team_id',$teams_drop_down,$action->team_id,['class'=>' form-control form-control-sm'. ($errors->has('team_id') ? ' is-invalid' : ''),'id'=>'team_id'])}}
                        @foreach($errors->get('team_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Dependency</th>
                    <td>
                        {{Form::select('dependency_id',$actions_drop_down,$action->dependency_id,['class'=>' form-control form-control-sm'. ($errors->has('dependency_id') ? ' is-invalid' : ''),'id'=>'dependency_id'])}}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Acceptance Date</th>
                    <td>
                        {{Form::text('acceptance_date', $action->acceptance_date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('acceptance_date') ? ' is-invalid' : ''), 'id'=>'acceptance_date', 'autocomplete'=>'off'])}}
                        @foreach($errors->get('acceptance_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Completed Date</th>
                    <td>
                        {{Form::text('completed_date', $action->completed_date, ['class'=>'form-control datepicker form-control-sm'. ($errors->has('completed_date') ? ' is-invalid' : ''), 'id'=>'completed_date', 'autocomplete'=>'off'])}}
                        @foreach($errors->get('completed_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Reject Reason</th>
                    <td>
                        {{Form::select('rejection_reason_id', $rejection_reason_drop_down, $action->rejection_reason_id,['class'=>' form-control form-control-sm'. ($errors->has('rejection_reason_id') ? ' is-invalid' : ''),'id'=>'rejection_reason_id'])}}
                        @foreach($errors->get('rejection_reason_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Reject Timestamp</th>
                    <td>
                        {{Form::text('reject_timestamp', $action->reject_timestamp, ['class'=>'datepicker form-control form-control-sm'. ($errors->has('reject_timestamp') ? ' is-invalid' : ''),'id'=>'reject_timestamp'])}}
                        @foreach($errors->get('reject_timestamp') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Related Project</th>
                    <td>
                        {{Form::select('related_project_id',$projects_drop_down,$action->related_project_id,['class'=>' form-control form-control-sm'. ($errors->has('related_project_id') ? ' is-invalid' : ''),'id'=>'related_project_id'])}}
                        @foreach($errors->get('related_project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Related Task</th>
                    <td>
                        {{Form::select('related_task_id', $tasks_drop_down, $action->related_task_id, ['class'=>' form-control form-control-sm'. ($errors->has('related_task_id') ? ' is-invalid' : ''),'id'=>'related_task_id'])}}
                        @foreach($errors->get('related_task_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="bg-gray-light" style="cursor: pointer" id="handler">
                    <th colspan="4" class="text-center">Related Information <span class="expand-icon">[ + ]</span><span class="expand-icon" style="display: none">[ - ]</span></th>
                </tr>
                <tr style="display: none" id="hidden_info">
                    <td colspan="4">
                        <table class="table table-bordered mb-0">
                            <tr>
                                <th>Related Customer</th>
                                <td>
                                    {{Form::select('related_customer_id',$customers_drop_down,$action->related_customer_id,['class'=>' form-control form-control-sm'. ($errors->has('related_customer_id') ? ' is-invalid' : ''),'id'=>'related_customer_id'])}}
                                    @foreach($errors->get('related_customer_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Related Job Spec</th>
                                <td>
                                    {{Form::select('related_job_spec_id',$job_specs_drop_down,$action->related_job_spec_id,['class'=>' form-control form-control-sm'. ($errors->has('related_job_spec_id') ? ' is-invalid' : ''),'id'=>'related_job_spec_id'])}}
                                    @foreach($errors->get('related_job_spec_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Related Customer Invoice</th>
                                <td>
                                    {{Form::select('related_customer_invoice_id',$customer_invoices_drop_down,$action->related_customer_invoice_id,['class'=>' form-control form-control-sm'. ($errors->has('related_customer_invoice_id') ? ' is-invalid' : ''),'id'=>'related_customer_invoice_id'])}}
                                    @foreach($errors->get('related_customer_invoice_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Related Vendor Invoice</th>
                                <td>
                                    {{Form::select('related_vendor_invoice_id',$vendor_invoices_drop_down,$action->related_vendor_invoice_id,['class'=>' form-control form-control-sm'. ($errors->has('related_vendor_invoice_id') ? ' is-invalid' : ''),'id'=>'related_vendor_invoice_id'])}}
                                    @foreach($errors->get('related_vendor_invoice_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Related Prospect</th>
                                <td>
                                    {{Form::select('prospect_id', $prospectDropDown, $action->prospect_id,['class'=>'form-control  form-control-sm'. ($errors->has('prospect_id') ? ' is-invalid' : ''),'id'=>'prospect_id'])}}
                                    @foreach($errors->get('prospect_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Vendor</th>
                                <td>
                                    {{Form::select('vendor_id',$vendors_drop_down,$action->vendor_id,['class'=>' form-control form-control-sm'. ($errors->has('vendor_id') ? ' is-invalid' : ''),'id'=>'vendor_id'])}}
                                    @foreach($errors->get('vendor_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <th>Board</th>
                    <td>
                        {{Form::select('board_id',$boards_drop_down,$action->board_id,['class'=>' form-control form-control-sm'. ($errors->has('board_id') ? ' is-invalid' : ''),'id'=>'board_id'])}}
                        @foreach($errors->get('board_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint</th>
                    <td>
                        {{Form::select('sprint_id',$sprints_drop_down,$action->sprint_id,['class'=>' form-control form-control-sm'. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Effort</th>
                    <td>
                        {{Form::text('estimated_effort',$action->estimated_effort,['class'=>'form-control form-control-sm'. ($errors->has('estimated_effort') ? ' is-invalid' : ''),'id'=>'estimated_effort'])}}
                        @foreach($errors->get('estimated_effort') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Actual Hours</th>
                    <td>
                        {{Form::text('actual_hours',$action->actual_hours,['class'=>'form-control form-control-sm'. ($errors->has('actual_hours') ? ' is-invalid' : ''),'id'=>'actual_hours'])}}
                        @foreach($errors->get('actual_hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Detail</th>
                    <td colspan="3">
                        {{Form::textarea('detail',$action->detail,['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('detail') ? ' is-invalid' : ''),'id'=>'detail'])}}
                        @foreach($errors->get('detail') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Link</th>
                    <td colspan="3">
                        {{Form::text('link',$action->link,['class'=>'form-control form-control-sm'. ($errors->has('link') ? ' is-invalid' : ''),'id'=>'link'])}}
                        @foreach($errors->get('link') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Documents</th>
                    <td colspan="3">
                        @if(isset($documents))
                        <table class="table table-sm table-borderless">
                            @foreach($documents as $document)
                                <tr>
                                    <td>
                                        {{ Form::text('file_name_'.$document->id, $document->name, ['class'=>'form-control form-control-sm'. ($errors->has('file_name_'.$document->id) ? ' is-invalid' : ''), 'id' => 'file_name_'.$document->id]) }}
                                        @foreach($errors->get('file_name_'.$document->id) as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td style="text-align: right; width: 20%;">
                                        {{ Html::link($document->file, 'View', ['class' => 'btn-sm btn btn-success', 'target' => '_blank']) }}
                                        <a class="btn btn-warning btn-sm" onclick="updateDocument({{$document->id}})" style="color: #ffffff;">Update</a>
                                        <a class="btn btn-danger btn-sm" onclick="deleteDocument({{$document->id}})">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2">
                                    <input type="file" id="file" name="file[]" class='form-control form-control-sm {{$errors->has('file') ? 'is-invalid' : ''}}' multiple="multiple"/>
                                    @foreach($errors->get('file') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Priority</th>
                    <td>
                        {{Form::select('priority_id',['' => 'Please select...', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],$action->priority_id,['class'=>'form-control form-control-sm '. ($errors->has('priority_id') ? ' is-invalid' : ''),'id'=>'priority_id'])}}
                        @foreach($errors->get('priority_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Send Notification Assigned?</th>
                    <td>
                        <label for="send_notification_assigned_yes">Yes</label>
                        <input type="radio" name="send_notification_assigned" value="1" {{$action->send_notification_assigned && isset($action->send_notification_assigned) ? "checked":""}} id="send_notification_assigned_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_assigned_no">No</label>
                        <input type="radio" name="send_notification_assigned" value="0" {{!$action->send_notification_assigned && isset($action->send_notification_assigned) ? "checked":""}} id="send_notification_assigned_no">
                        {{--{{Form::select('send_notification_assigned',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_assigned') ? ' is-invalid' : ''),'id'=>'send_notification_assigned'])}}
                        @foreach($errors->get('send_notification_assigned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                </tr>
                <tr>
                    <th>Send notification created?</th>
                    <td>
                        <label for="send_notification_created_yes">Yes</label>
                        <input type="radio" name="send_notification_created" value="1" {{$action->send_notification_created && isset($action->send_notification_created) ? "checked":""}} id="send_notification_created_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_created_no">No</label>
                        <input type="radio" name="send_notification_created" value="0" {{!$action->send_notification_created && isset($action->send_notification_created) ? "checked":""}} id="send_notification_created_no">
                        {{--{{Form::select('send_notification_created',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_created') ? ' is-invalid' : ''),'id'=>'send_notification_created'])}}
                        @foreach($errors->get('send_notification_created') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                    <th>Send Notification related?</th>
                    <td>
                        <label for="send_notification_related_yes">Yes</label>
                        <input type="radio" name="send_notification_related" value="1" {{$action->send_notification_related && isset($action->send_notification_related) ? "checked":""}} id="send_notification_related_yes"> &nbsp; &nbsp; &nbsp; &nbsp;
                        <label for="send_notification_related_no">No</label>
                        <input type="radio" name="send_notification_related" value="0" {{!$action->send_notification_related && isset($action->send_notification_related) ? "checked":""}} id="send_notification_related_no">
                        {{--{{Form::select('send_notification_related',['' => 'Please select...', 0 => 'No', 1 => 'Yes'],null,['class'=>'form-control form-control-sm '. ($errors->has('send_notification_related') ? ' is-invalid' : ''),'id'=>'send_notification_related'])}}
                        @foreach($errors->get('send_notification_related') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach--}}
                    </td>
                </tr>
            </table>
            {{Form::close()}}
            @if(isset($documents))
                @foreach($documents as $document)
                    {{ Form::open(['method' => 'POST', 'route' => ['document.updatename', $document->id],'style'=>'display:inline', 'id'=>'document_frm_update_'.$document->id]) }}
                    <input type="hidden" id="document_name_{{$document->id}}" name="document_name_{{$document->id}}" />
                    <input type="hidden" id="reference_id" name="reference_id" value="{{$document->reference_id}}"/>
                    <input type="hidden" id="document_type_id" name="document_type_id" value="{{$document->document_type_id}}"/>
                    {{ Form::close() }}

                    {{ Form::open(['method' => 'DELETE', 'route' => ['document.destroy', $document->id],'style'=>'display:inline','class'=>'delete', 'id'=>'document_frm_'.$document->id]) }}
                    <input type="hidden" id="reference_id" name="reference_id" value="{{$document->reference_id}}"/>
                    <input type="hidden" id="document_type_id" name="document_type_id" value="{{$document->document_type_id}}"/>
                    {{ Form::close() }}
                @endforeach
            @endif
        </div>

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.accept_modal')
        <!-- End Block - Candidate email CV Modal -->

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.reject_modal')
        <!-- End Block - Candidate email CV Modal -->

        <!-- Start Block - Candidate email CV Modal -->
        @include('action.complete_modal')
        <!-- End Block - Candidate email CV Modal -->
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });

        function closeAcceptModal(){
            $('#closeAcceptModalBtn').click();
        }

        function closeRejectModal(){
            $('#closeRejectModalBtn').click();
        }

        function closeCompleteModal(){
            $('#closeCompleteModalBtn').click();
        }

        function updateDocument(document_id) {
            let name = $('#file_name_'+document_id).val();
            $('#document_name_'+document_id).val(name);
            $('#document_frm_update_'+document_id).submit();
        }

        function deleteDocument(document_id) {
            $('#document_frm_'+document_id).submit();
        }
        $(function () {
            $("#related_project_id").on('change', () => {
                if ($("#related_task_id").val() == 0){
                    axios.get('/action/' + $("#related_project_id").val() +"/tasks")
                        .then(function (response) {
                            // handle success
                            let related_tasks = $("#related_task_id").html("<option value='0'>Please select...</option>");
                            response.data.tasks.forEach((task, index) => {
                                related_tasks.append("<option value='" + task.id + "'>" + task.name + "</option>");
                            });
                            related_tasks.trigger("chosen:updated");
                        })
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                        })
                }
            });
        })

        $(function () {
            $("#handler").on('click', () => {
                $(".expand-icon").toggle();
                $("#hidden_info").fadeToggle("slow","linear");
            })
        })
    </script>
@endsection