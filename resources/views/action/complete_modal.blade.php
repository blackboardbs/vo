<!-- Modal -->
<div class="modal fade" id="completeModal" tabindex="-1" role="dialog" aria-labelledby="completeModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="configuration_label">Accept Action</h5>
                <button id="closeCompleteModalBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::open(['url' => route('action.complete', $action->id), 'method' => 'post','class'=>'mt-3'])}}
                <div class='row'>
                    <div class="col-md-12">
                        <p>Are you sure you want to complete this action?</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button id="accept_btn" type="submit" class="btn btn-success">Yes</button> <a onclick="closeCompleteModal()" class="btn-default btn">No</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>