@extends('adminlte.default')
@section('title') Action @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('action.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Action</a>
            <a href="{{route('board.create')}}" class="btn btn-success btn-sm float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> Board</a>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li><a href="{{route('action.index')}}">Actions - List</a></li>
            <li class="active"><a href="#">Actions - Kanban Board</a></li>
        </ul>
        <hr>
        <form id="frmAction">
            <input type="hidden" id="fv" name="fv" value="{{isset($_GET['fv']) ? $_GET['fv'] : 0}}" />
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-3 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('resource',[],null,['class'=>'form-control search w-100', 'id' => 'resources'])}}
                    <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('sprint',$sprint_dropdown,null,['class'=>'form-control search ', 'style'=>'width: 100%;'])}}
                    <span>Sprint</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    
                <div class="form-group input-group">
                    <label class="has-float-label">
                    {{Form::select('board',$board_dropdown,null,['class'=>'form-control search ', 'style'=>'width: 100%;'])}}
                    <span>Board</span>
                    </label>
                </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <a href="{{route('action.kanban')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
                {{-- <div class="col-md-4 col-sm-12 text-right">
                    <a onclick="filter()" id="filterBtn" class="btn btn-sm btn-default"><i class="fa fa-filter"></i></a>
                </div> --}}
            </div>
            @if(isset($_GET['fv']) && ($_GET['fv'] == 1))
            <div class="row more-filters" style="margin-bottom: 10px;">
                <div class="col-md-2 col-sm-12">
                    {{Form::select('team',$teams_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::text('date_from',old('date_from'),['class'=>'form-control form-control-sm datepicker search', 'style'=>'width: 100%;','placeholder'=>'Date From', 'autocomplete' => 'off'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::text('date_to',old('date_to'),['class'=>'form-control form-control-sm datepicker search', 'style'=>'width: 100%;','placeholder'=>'Date To', 'autocomplete' => 'off'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('customer',$customers_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('project',$projects_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('vendor',$vendors_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
            </div>
            <div class="row more-filters" style="margin-bottom: 10px;">
                <div class="col-md-2 col-sm-12">
                    {{Form::select('status[]',$action_status_filter,null,['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;', 'multiple', 'placeholder'=>'Status'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('prospect',$prospect_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('ru',$users_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;', 'multiple'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rp',$projects_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rt',$tasks_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rjs',$job_specs_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
            </div>
            <div class="row more-filters" style="margin-bottom: 10px;">
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rc',$customers_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rci',$customer_invoices_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{Form::select('rvi',$vendor_invoices_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-md-2 col-sm-12">
                    <button class="btn btn-sm btn-dark"><i class="fas fa-filter"></i> Filter</button>
                </div>
            </div>
            @endif
        </form>
        <hr>
        <div class="main-wrapper">
            <action-component parameters="{{json_encode($vueParameters)}}"></action-component>
        </div>
    </div>

    {{--<button id="launch_modal" style="display: none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_task_modal">
        Launch modal
    </button>--}}

@endsection
@section('extra-js')

    <script>
        $(document).ready(function(){
            const resources_dropdown = @json($resource_dropdown);
            let options = `<option value="${null}" selected disabled>Select Option</option>`;
            resources_dropdown.forEach(v => options += `<option value="${v.id}">${v.full_name}</option>`)

            $("#resources").html(options);
            $("#resource").attr('placeholder', 'Select Resource')
        });

        function filter(){
            let value = $('#fv').val();
            if(value == 0){
                $('#fv').val(1);
            } else {
                $('#fv').val(0);
            }

            // location.reload();
            $('#frmAction').submit();
        }

        $(function() {



            $( ".sortable" ).sortable({
                connectWith: ".connectedSortable",
                receive: function( event, ui ) {
                    var board_id = $(this).attr("board-id");
                    var action_id = ui.item.attr('action-id');

                    var template_id = $("select[name=template_type_id]").val();
                    axios.post('/action/move/'+action_id, {
                        'status': board_id
                    })
                    .then(function (data) {
                        // location.reload();
                        console.log(data);
                    })
                    .catch(function () {
                        console.log("An Error occurred!!!");
                    });
                }
            }).disableSelection();

            $('.add-button').click(function() {
                var txtNewItem = $('#new_text').val();
                $(this).closest('div.task-board').find('ul').append('<li class="card">'+txtNewItem+'</li>');
            });
        });

        function addAction(status){
            $('#task_status_id').val(status);

            $('#description').val('');
            $('#task_type_id').val('');
            $('#hours_planned').val('');
            $('#consultant').val('');
            $('#new_start_date').val('');
            $('#new_end_date').val('');
            $('#billable').val('');
            $('#note').val('');

            $('#launch_modal').click();
        }

        function saveTask() {

            var project_id = $('#project_id').val();
            var tast_status_id = $('#task_status_id').val();
            var description = $('#description').val();
            var task_type_id = $('#task_type_id').val();
            var hours_planned = $('#hours_planned').val();
            var consultant = $('#consultant').val();
            var new_start_date = $('#new_start_date').val();
            var new_end_date = $('#new_end_date').val();
            var billable = $('#billable').val();
            var note = $('#note').val();

            if(description == ""){
                alert("Please fill in the task description");
                return;
            }

            if(task_type_id == ""){
                alert("Please select task type");
                return;
            }

            axios.post('/task/storetask', {
                'project_id': project_id,
                'tast_status_id': tast_status_id,
                'description': description,
                'task_type_id': task_type_id,
                'hours_planned': hours_planned,
                'consultant': consultant,
                'new_start_date': new_start_date,
                'new_end_date': new_end_date,
                'billable': billable,
                'note': note,
            })
                .then(function (result) {
                    $('#ul_list_'+tast_status_id).append('<li task-id="'+result.data.id+'" project-id="'+result.data.project_id+'" class="card"><span class="task-type">'+result.data.type+'</span>'+result.data.description+'</li>');
                    $('#btn_clode_modal').click();
                })
                .catch(function () {
                    console.log("An Error occurred!!!");
                });

        }

    </script>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .modal-dialog {
            width: 100%;
            height: 100%;
            margin-left: 280px;
            /*margin: 0;
            padding: 0;*/
        }

        .modal-content {
            height: auto;
            min-height: 90%;
            min-width: 90% !important;
            width: 1200px !important;
            border-radius: 0;
        }

        .col-sm-2 {
            margin-bottom: 10px;
        }

        .board-heading {
            font-weight: bold !important;
            margin: 10px;
        }

        .add-card {
            margin: 10px;
        }

        .add-card-link{
            cursor: pointer;
        }

        .task-board {
            background-color: #E2E4E6;;
            margin: 10px;
            max-width: 23%;
            /*border-radius: 5px;*/
            /*min-height: 250px;*/
            height: 100%;
        }

        .card{
            background-color: white;
            /* border-radius: 4px; */
            border-radius: unset;
            cursor: pointer;
            font-size: 15px;
        }

        .task-type {
            text-align: right;
            color: #0e90d2;
            font-size: 12px;
            font-weight: bold;
        }

        .sortable {
            margin: 0;
            padding: 2px;
            /*border-radius: 5px;*/
        }

        .sortable li {
            margin: 3px 3px 3px 3px;
            padding: 0.5em;
        }

        .ui-draggable, .ui-droppable {
            background-position: top;
        }

    </style>
@endsection