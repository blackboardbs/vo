@extends('adminlte.default')
@section('title') Action @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('action.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Action</a>
            <a href="{{route('board.create')}}" class="btn btn-success float-right mr-2"><i class="fa fa-plus"></i> Board</a>
            <x-export route="action.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#">Actions - List</a></li>
            <li><a href="{{route('action.kanban')}}">Actions - Kanban Board</a></li>
            @if(strpos(url()->previous(), '?project_id') !== false)
                <li><a href="{{url()->previous()}}">Back To Tasks</a></li>
            @else
                <li><a href="{{route('project.index')}}">Projects</a></li>
            @endif
        </ul>
        <hr>
        <form id="frmAction">
            <input type="hidden" id="fv" name="fv" value="{{isset($_GET['fv']) ? $_GET['fv'] : 0}}" />
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-3 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('resource',[],null,['class'=>'form-control search w-100', 'id' => 'resources'])}}
                            <span>Resource</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('sprint',$sprint_dropdown,null,['class'=>'form-control search ', 'style'=>'width: 100%;'])}}
                            <span>Sprint</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">

                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('board',$board_dropdown,null,['class'=>'form-control search ', 'style'=>'width: 100%;'])}}
                            <span>Board</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <a href="{{route('action.index')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
                {{-- <div class="col-md-4 col-sm-12 text-right">
                    <a onclick="filter()" id="filterBtn" class="btn btn-sm btn-default"><i class="fa fa-filter"></i></a>
                </div> --}}
            </div>
            @if(isset($_GET['fv']) && ($_GET['fv'] == 1))
                <div class="row more-filters" style="margin-bottom: 10px;">
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('team',$team_dropdown,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::text('date_from',old('date_from'),['class'=>'form-control form-control-sm datepicker search', 'style'=>'width: 100%;','placeholder'=>'Date From', 'autocomplete' => 'off'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::text('date_to',old('date_to'),['class'=>'form-control form-control-sm datepicker search', 'style'=>'width: 100%;','placeholder'=>'Date To', 'autocomplete' => 'off'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('customer',$customers_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('project',$projects_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('vendor',$vendors_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                </div>
                <div class="row more-filters" style="margin-bottom: 10px;">
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('status',$action_status_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;',])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('prospect',$prospect_filter,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('ru',$users_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}} {{--, 'multiple'--}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rp',$projects_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rt',$tasks_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rjs',$job_specs_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                </div>
                <div class="row more-filters" style="margin-bottom: 10px;">
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rc',$customers_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rci',$customer_invoices_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{Form::select('rvi',$vendor_invoices_drop_down,null,['class'=>'form-control form-control-sm search ', 'style'=>'width: 100%;'])}}
                    </div>
                </div>
            @endif
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th style="width: 15%;">Subject</th>
                    <th>Descritption</th>
                    <th>Due Date</th>
                    <th>Assigned User</th>
                    <th>Acceptance Date</th>
                    <th>Completed Date</th>
                    <th>Status</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($actions as $action)
                    <tr>
                        <td><a href="{{route('action.show', $action)}}">{{$action->subject}}</a></td>
                        <td>{{$action->description}}</td>
                        <td>{{$action->due_date}}</td>
                        <td>{{$action->assignedUser?->name()}}</td>
                        <td>{{$action->acceptance_date}}</td>
                        <td>{{$action->completed_date}}</td>
                        <td>{{$action->status?->name}}</td>
                        @if($can_create)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('action.edit',$action)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['action.destroy', $action],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No actions match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $actions->links() }}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function(){
            const resources_dropdown = @json($resource_dropdown);
            let options = `<option value="${null}" selected disabled>Select Option</option>`;
            resources_dropdown.forEach(v => options += `<option value="${v.id}">${v.full_name}</option>`)

            $("#resources").html(options);
            $("#resource").attr('placeholder', 'Select Resource')
        });
        function filter(){
            let value = $('#fv').val();
            if(value == 0){
                $('#fv').val(1);
            } else {
                $('#fv').val(0);
            }

            // location.reload();
            $('#frmAction').submit();
        }
    </script>
@endsection