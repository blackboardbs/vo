<table>
    <thead>
    <tr>
        <th>Subject</th>
        <th>Descritption</th>
        <th>Due Date</th>
        <th>Assigned User</th>
        <th>Acceptance Date</th>
        <th>Completed Date</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($actions as $action)
        <tr>
            <td>{{isset($action->subject)? $action->subject: ''}}</td>
            <td>{{$action->description}}</td>
            <td>{{$action->due_date}}</td>
            <td>{{$action->assignedUser?->name()}}</td>
            <td>{{isset($action->acceptance_date)?$action->acceptance_date:'-'}}</td>
            <td>{{isset($action->completed_date)?$action->completed_date:'-'}}</td>
            <td>{{isset($action->status->name)?$action->status->name:'' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>