@extends('adminlte.default')
@section('title') Edit Job Spec @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('create_scouting_form')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('jobspec.update', $job_spec), 'method' => 'put','class'=>'mt-3', 'id'=>'create_scouting_form','files' => true,'autocomplete'=>'on'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Create Job Spec</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Reference Number:</th>
                    <td>
                        {{Form::text('reference_number', $job_spec->reference_number,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('reference_number') ? ' is-invalid' : ''), 'placeholder'=>'Reference Number', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('reference_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Placement By:</th>
                    <td>
                        {{Form::text('placement_by', $job_spec->placement_by,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('placement_by') ? ' is-invalid' : ''), 'placeholder'=>'Placement By'])}}
                        @foreach($errors->get('placement_by') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Job Origin:</th>
                    <td>
                        {{Form::select('origin_id',$job_origin_drop_down, $job_spec->origin_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('origin_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                        @foreach($errors->get('origin_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Client:</th>
                    <td>{{Form::text('client', $job_spec->client,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('client') ? ' is-invalid' : ''),'placeholder'=>'Client'])}}
                        @foreach($errors->get('client') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Position Role:</th>
                    <td>
                        {{Form::select('role_id',$role_drop_down, $job_spec->role_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Position Type:</th>
                    <td>{{Form::select('employment_type_id',$resource_type_drop_down, $job_spec->employment_type_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('employment_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                        @foreach($errors->get('employment_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                <tr>
                <tr>
                    <th>Profession:</th>
                    <td>{{Form::select('profession_id', $profession_drop_down, $job_spec->profession_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('profession_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select', 'id' => 'profession_id'])}}
                        @foreach($errors->get('profession_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Speciality:</th>
                    <td>{{Form::select('speciality_id',$speciality_drop_down, $job_spec->speciality_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select', 'id' => 'speciality_id'])}}
                        @foreach($errors->get('speciality_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>{{Form::select('customer_id', $customer_drop_down, $job_spec->customer_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('customer_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Location:</th>
                    <td>
                        {{Form::text('location', $job_spec->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('location') ? ' is-invalid' : ''), 'placeholder'=>'Location'])}}
                        @foreach($errors->get('location') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Salary text:</th>
                    <td>
                        {{Form::text('salary', $job_spec->salary,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('salary') ? ' is-invalid' : ''), 'placeholder'=>'Salary'])}}
                        @foreach($errors->get('salary') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Respond to:</th>
                    <td>
                        {{Form::text('respond_to', $job_spec->respond_to,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('respond_to') ? ' is-invalid' : ''), 'placeholder'=>'Respond To'])}}
                        @foreach($errors->get('respond_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Hourly Rate Minimum:</th>
                    <td>
                        {{Form::text('hourly_rate_minimum', $job_spec->hourly_rate_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum'])}}
                        @foreach($errors->get('hourly_rate_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Hourly Rate Maximum:</th>
                    <td>
                        {{Form::text('hourly_rate_maximum', $job_spec->hourly_rate_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum'])}}
                        @foreach($errors->get('hourly_rate_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Monthly Salary Minimum</th>
                    <td>
                        {{Form::text('monthly_salary_minimum', $job_spec->monthly_salary_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum'])}}
                        @foreach($errors->get('monthly_salary_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Monthly Salary Maximum:</th>
                    <td>
                        {{Form::text('monthly_salary_maximum', $job_spec->monthly_salary_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum'])}}
                        @foreach($errors->get('monthly_salary_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $job_spec->start_date,['class'=>'form-control form-control-sm col-sm-12 datepicker'. ($errors->has('start_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Applications Close Date:</th>
                    <td>
                        {{Form::text('applicaiton_closing_date', $job_spec->applicaiton_closing_date,['class'=>'form-control form-control-sm col-sm-12 datepicker'. ($errors->has('applicaiton_closing_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('applicaiton_closing_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Skills:</th>
                    <td>{{Form::select('skills_id[]', $skills_drop_down, explode('|', $job_spec->skills),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('skills_id') ? ' is-invalid' : ''), 'multiple'])}}
                        @foreach($errors->get('skills_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Priority:</th>
                    <td>{{Form::select('priority', $priority, $job_spec->priority,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('priority') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('priority') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>BEE:</th>
                    <td>{{Form::select('b_e_e', [1 => "Yes", 2 => "No"] ,$job_spec->b_e_e,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('b_e_e') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('b_e_e') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Recruiter:</th>
                    <td>{{Form::select('recruiter_id', $recruiters_drop_down, $job_spec->recruiter_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('recruiter_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Recruiter'])}}
                        @foreach($errors->get('recruiter_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Gender:</th>
                    <td>{{Form::select('gender', [1 => "Male", 2 => "Female"], $job_spec->gender,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('gender') ? ' is-invalid' : ''), 'placeholder' => 'Select Gender'])}}
                        @foreach($errors->get('gender') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Document:</th>
                    <td>
                        {{Form::file('document',  old('document'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('document') ? ' is-invalid' : ''), 'placeholder' => 'Select Recruiter']) }}
                        @foreach($documents as $document)
                            <br>
                            <a href="{{route('jobSpecDocuments', $document)}}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                        @endforeach    
                        @foreach($errors->get('document') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Work Type:</th>
                    <td>{{Form::select('work_type_id', $work_type_drop_down, $job_spec->work_type_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('work_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                        @foreach($errors->get('work_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Job Status:</th>
                    <td>{{Form::select('status_id', $job_spec_status_drop_down, $job_spec->status_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Duties and Responsibilities:</th>
                    <td>{{Form::textarea('duties_and_reponsibilities', $job_spec->duties_and_reponsibilities,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('duties_and_reponsibilities') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('duties_and_reponsibilities') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Desired Experience & Qualification:</th>
                    <th>
                        {{Form::textarea('desired_experience_and_qualification', $job_spec->desired_experience_and_qualification,['size' => '30x5','class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('desired_experience_and_qualification') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('desired_experience_and_qualification') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </th>
                <tr>
                <tr>
                    <th>Package & Remuneration:</th>
                    <td>
                        {{Form::textarea('package_and_renumeration', $job_spec->package_and_renumeration,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('package_and_renumeration') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('package_and_renumeration') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Special Note:</th>
                    <td>
                        {{Form::textarea('special_note', $job_spec->special_note,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12', 'id' => 'special_note'])}}
                        @foreach($errors->get('special_note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Position Description:</th>
                    <td colspan="3">
                        {{Form::textarea('position_description', $job_spec->position_description,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('position_description') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('position_description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {

            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            $('#profession_id').change(function () {

                var profession_id = $('#profession_id').val();

                axios.post('/cv/getspeciality/' + profession_id)
                .then(function (response) {
                    var options_html = '<option>Please select</option>';
                    $.each(response.data.speciality, function (key, value) {
                        options_html += '<option value=' + key + '>' + value + '</option>';
                    });

                    $('#speciality_id').html(options_html);
                    $('#speciality_id').trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });

            });

        });

    </script>
@endsection