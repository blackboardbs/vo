<table>
    <thead>
    <tr>
        <th>Reference Number</th>
        <th>Position Type</th>
        <th>Placement By</th>
        <th>Application Close Date</th>
        <th>Position Description</th>
        <th>Priority</th>
        <th>Customer</th>
        <th>Documents</th>
        <th>Job Status</th>
        <th>No of CV’s Submitted</th>
    </tr>
    </thead>
    <tbody>
    @foreach($job_specs as $job_spec)
        <tr>
            <td>{{$job_spec->reference_number}}</td>
            <td>{{$job_spec->positiontype?->description}}</td>
            <td>{{$job_spec->placement_by}}</td>
            <td>{{$job_spec->applicaiton_closing_date}}</td>
            <td>{{$job_spec->position_description}}</td>
            <td>
                @if($job_spec->priority < 3)
                    <span>{{$job_spec->priority}}</span>
                @elseif($job_spec->priority >= 3 && $job_spec->priority < 6)
                    <span>{{$job_spec->priority}}</span>
                @elseif($job_spec->priority >= 6)
                    <span>{{$job_spec->priority}}</span>
                @endif
            </td>
            <td>{{isset($job_spec->customer->customer_name)?$job_spec->customer->customer_name:null}}</td>
            <td>
                @foreach($documents[$job_spec->id] as $document)
                    <a href="{{route('jobSpecDocuments', $document)}}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                @endforeach
            </td>
            <td>{{isset($job_spec->status->name)?$job_spec->status->name:''}}</td>
            <td>{{isset($cvs_sumbitted)?$cvs_sumbitted:''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>