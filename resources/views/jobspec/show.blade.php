@extends('adminlte.default')
@section('title') View Job Spec @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
             &nbsp;&nbsp;&nbsp;<a href="{{route('cv.search')}}?j={{$job_spec->id}}" class="btn btn-success btn-sm" style="margin-right: 15px;" target="_blank"><i class="fa fa-paper-plane"></i> Submit a CV</a> 
             <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('jobspec.edit',$job_spec->id)}}" class="btn btn-success btn-sm float-right" style="margin-left:5px;"><i class="far fa-edit"></i> Edit Job Spec</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('jobspec.updatewishlist', $job_spec->id), 'method' => 'put','class'=>'mt-3','autocomplete'=>'off'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: left;">Wishlist</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Wishlist:</th>
                        <td>
                            {{Form::select('cv_wishlist_id',$wishlist_drop_down, $job_spec->cv_wishlist_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('cv_wishlist_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select'])}}
                            @foreach($errors->get('cv_wishlist_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td width="230" class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">Update</button>&nbsp;&nbsp;&nbsp;
                            @if($job_spec->cv_wishlist_id > 0)
                                <a href="{{route('cvwishlist.show', $job_spec->cv_wishlist_id > 0 ? $job_spec->cv_wishlist_id : 0)}}" class="btn btn-dark btn-sm float-right" style="margin-left: 10px;"><i class="fa fa-list"></i> WishList</a>
                            @else
                                <button id="wishlist_modal_btn" type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#wishlistModal">
                                    <i class="fa fa-plus"></i> Wishlist
                                </button>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: left;">Job Spec Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Reference Number:</th>
                        <td>
                            {{Form::text('reference_number', $job_spec->reference_number,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('reference_number') ? ' is-invalid' : ''), 'placeholder'=>'Reference Number', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('reference_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Position Role:</th>
                        <td>
                            {{Form::select('role_id',$role_drop_down, $job_spec->role_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('role_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Position Type:</th>
                        <td>{{Form::select('employment_type_id',$resource_type_drop_down, $job_spec->employment_type_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('employment_type_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('employment_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Profession:</th>
                        <td>{{Form::select('profession_id', $profession_drop_down, $job_spec->profession_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('profession_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('profession_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Work Type:</th>
                        <td>{{Form::select('work_type_id', $work_type_drop_down, $job_spec->work_type_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('work_type_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('work_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Speciality:</th>
                        <td>{{Form::select('speciality_id',$speciality_drop_down, $job_spec->speciality_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('speciality_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Start Date:</th>
                        <td>
                            {{Form::text('start_date', $job_spec->start_date,['class'=>'form-control form-control-sm col-sm-12 datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Applications Close Date:</th>
                        <td>
                            {{Form::text('applicaiton_closing_date', $job_spec->applicaiton_closing_date,['class'=>'form-control form-control-sm col-sm-12 datepicker'. ($errors->has('applicaiton_closing_date') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('applicaiton_closing_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Created Date:</th>
                        <td>
                            {{Form::text('created_at', date('Y-m-d', strtotime($job_spec->created_at)),['class'=>'form-control form-control-sm col-sm-12 datepicker'. ($errors->has('created_at') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('created_at') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th colspan="2">&nbsp;</th>
                    </tr>
                    <tr>
                        <th>Skills:</th>
                        <td>{{Form::select('skills_id[]', $skills_drop_down, explode('|', $job_spec->skills),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('skills_id') ? ' is-invalid' : ''), 'multiple', 'disabled'])}}
                            @foreach($errors->get('skills_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Priority:</th>
                        <td>{{Form::select('priority', $priority, $job_spec->priority,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('priority') ? ' is-invalid' : ''), 'disabled'])}}
                            @foreach($errors->get('priority') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>BEE:</th>
                        <td>{{Form::select('b_e_e', [1 => "Yes", 2 => "No"] ,$job_spec->b_e_e,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('b_e_e') ? ' is-invalid' : ''), 'disabled'])}}
                            @foreach($errors->get('b_e_e') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Recruiter:</th>
                        <td>{{Form::select('recruiter_id', $recruiters_drop_down, $job_spec->recruiter_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('recruiter_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Recruiter', 'disabled'])}}
                            @foreach($errors->get('recruiter_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Job Status:</th>
                        <td>{{Form::select('status_id', $job_spec_status_drop_down, $job_spec->status_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('status_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Job Origin:</th>
                        <td>
                            {{Form::select('origin_id',$job_origin_drop_down, $job_spec->origin_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('origin_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('origin_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Gender:</th>
                        <td>{{Form::select('gender', [1 => "Male", 2 => "Female"], $job_spec->gender,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('gender') ? ' is-invalid' : ''), 'disabled'])}}
                            @foreach($errors->get('gender') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Document:</th>
                        <td>
                            @foreach($documents as $document)
                                <a href="{{route('jobSpecDocuments', $document)}}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Position Description:</th>
                        <td colspan="3">
                            {{Form::textarea('position_description', $job_spec->position_description,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('position_description') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('position_description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Duties and Responsibilities:</th>
                        <td colspan="3">{{Form::textarea('duties_and_reponsibilities', $job_spec->duties_and_reponsibilities,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('duties_and_reponsibilities') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('duties_and_reponsibilities') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Package & Remuneration:</th>
                        <td colspan="3">
                            {{Form::textarea('package_and_renumeration', $job_spec->package_and_renumeration,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('package_and_renumeration') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('package_and_renumeration') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Desired Experience & Qualification:</th>
                        <th colspan="3">
                            {{Form::textarea('desired_experience_and_qualification', $job_spec->desired_experience_and_qualification,['size' => '30x5','class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('desired_experience_and_qualification') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('desired_experience_and_qualification') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </th>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: left;">Salary Information</th>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Salary text:</th>
                        <td colspan="3">
                            {{Form::text('salary', $job_spec->salary,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('salary') ? ' is-invalid' : ''), 'placeholder'=>'Salary', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('salary') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Monthly Salary Minimum</th>
                        <td>
                            {{Form::text('monthly_salary_minimum', number_format($job_spec->monthly_salary_minimum, 2, '.', ','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('monthly_salary_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Monthly Salary Maximum:</th>
                        <td>
                            {{Form::text('monthly_salary_maximum', number_format($job_spec->monthly_salary_maximum, 2, '.', ','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('monthly_salary_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Hourly Rate Minimum:</th>
                        <td>
                            {{Form::text('hourly_rate_minimum', number_format($job_spec->hourly_rate_minimum, 2, '.', ','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('hourly_rate_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Hourly Rate Maximum:</th>
                        <td>
                            {{Form::text('hourly_rate_maximum', number_format($job_spec->hourly_rate_maximum, 2, '.', ','),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('hourly_rate_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: left;">Client Information</th>
                    </tr>
                    <tr>
                        <th>Client:</th>
                        <td>{{Form::text('client', $job_spec->client,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('client') ? ' is-invalid' : ''),'placeholder'=>'Client', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('client') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th style='width: 20%'>Placement By:</th>
                        <td>
                            {{Form::text('placement_by', $job_spec->placement_by,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('placement_by') ? ' is-invalid' : ''), 'placeholder'=>'Placement By', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('placement_by') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Location:</th>
                        <td>
                            {{Form::text('location', $job_spec->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('location') ? ' is-invalid' : ''), 'placeholder'=>'Location', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('location') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Customer:</th>
                        <td>{{Form::select('customer_id', $customer_drop_down, $job_spec->customer_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('customer_id') ? ' is-invalid' : ''), 'placeholder' => 'Please Select', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('customer_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Respond to:</th>
                        <td>
                            {{Form::text('respond_to', $job_spec->respond_to,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('respond_to') ? ' is-invalid' : ''), 'placeholder'=>'Respond To', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('respond_to') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2"></td>
                    <tr>
                    <tr>
                        <th>Special Note:</th>
                        <td colspan="3">
                            {{Form::textarea('special_note', $job_spec->special_note,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12', 'id' => 'special_note', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('special_note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="5" class="btn-dark" style="text-align: left;">Activity Log</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Date/Time</th><th>Activity</th><th>Reference</th><th>By</th><th>Action</th>
                    </tr>
                    @foreach($actitvities_log as $activity)
                        <tr>
                            <td>{{$activity->date}}</td>
                            <td>{{$activity->activity}}</td>
                            <td>
                                @if(isset($activity->cv->user))
                                    {{$activity->cv->user->first_name.' '.$activity->cv->user->last_name.' ('.$activity->cv->id.')'}}
                                @else
                                    {{isset($activity->jobSpec->reference_number)?$activity->jobSpec->reference_number:null}}
                                @endif
                            </td>
                            <td>{{isset($activity->author)?$activity->author->email:null}}</td>
                            <td>
                                {{--<a href="" class="btn btn-sm btn-danger">View</a>--}}
                                @if(strpos(strtoupper($activity->activity), 'CV') == 0)
                                    <a href="{{route('jobspecactivitystatus.status', ['jobSpecActivityLog' => $activity, 'status_id' => 2])}}" class="btn btn-sm btn-primary">Resubmit</a>
                                    <a href="{{route('jobspecactivitystatus.status', ['jobSpecActivityLog' => $activity, 'status_id' => 3])}}" class="btn btn-sm btn-primary">Rejected</a>
                                    <a href="{{route('jobspecactivitystatus.status', ['jobSpecActivityLog' => $activity, 'status_id' => 4])}}" class="btn btn-sm btn-primary">Interview</a>
                                    <a href="{{route('jobspecactivitystatus.status', ['jobSpecActivityLog' => $activity, 'status_id' => 5])}}" class="btn btn-sm btn-primary">Offer</a>
                                    <a href="{{route('jobspecactivitystatus.status', ['jobSpecActivityLog' => $activity, 'status_id' => 6])}}" class="btn btn-sm btn-primary">Appointed</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="wishlistModal" tabindex="-1" role="dialog" aria-labelledby="wishlistModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="wishlistModalLabel">Add New Wishlist</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{Form::open(['url' => route('cvwishlist.store'), 'method' => 'post', 'class'=>'mt-3', 'autocomplete'=>'off'])}}
                <input type="hidden" name="jobspec_id" id="jobspec_id" value="{{$job_spec->id}}" />
                <div class="modal-body">
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                            <tr>
                                <td>WishList: </td>
                                <td>
                                    {{Form::text('name', old('name'), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'id' => 'name'])}}
                                    @foreach($errors->get('name') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" type="button" class="btn btn-success btn-sm">Save</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $('#consultants_chosen').css('width', '100%');
        $('.chosen-container').css('width', '100%');
    </script>
@endsection