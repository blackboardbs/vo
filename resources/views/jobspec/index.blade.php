@extends('adminlte.default')
@section('title') Job Spec @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('jobspec.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Job Spec</a>
            <x-export route="jobspec.index"></x-export>
        @endif
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                Customer<br/>
                {{Form::select('c',$customer_dropdown,old('c'),['class'=>' form-control search', 'id' => 'c','style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Reference Number<br/>
                <input type="text" name="rn" class="form-control w-100" value="{{isset($_GET['rn']) ? $_GET['rn'] : ''}}" onkeyup="handle(event)" />
            </div>
            <div class="col-sm-3 col-sm">
                Profession<br/>
                {{Form::select('p',$profession_dropdown,old('p'),['class'=>' form-control search', 'id' => 'p', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Speciality<br/>
                {{Form::select('sp',$speciality_dropdown,old('sp'),['class'=>' form-control search', 'id' => 'sp', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Position Type<br/>
                {{Form::select('e',$resource_type_dropdown,old('e'),['class'=>' form-control search', 'id' => 'e', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Job Origin<br/>
                {{Form::select('o',$job_origin_dropdown,old('o'),['class'=>' form-control search', 'id' => 'o', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Job Status<br/>
                {{Form::select('s',$job_spec_status_dropdown,old('s'),['class'=>' form-control search', 'id' => 's', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Rate Minimum<br/>
                {{Form::select('rm',$money_drop_down,old('rm'),['class'=>' form-control search', 'id' => 'rm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Skills<br/>
                {{Form::select('skill',$skill_dropdown,old('skill'),['class'=>' form-control search', 'id' => 'skill', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Priority<br/>
                {{Form::select('priority',$priority_drop_down,old('priority'),['class'=>' form-control search', 'id' => 'priority', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                BEE<br/>
                {{Form::select('bee',$yes_or_no_dropdown,old('bee'),['class'=>' form-control search', 'id' => 'bee', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Recruiter<br/>
                {{Form::select('recruiter', $recruiters_dropdown,old('recruiter'),['class'=>' form-control search', 'id' => 'recruiter', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Rate Maximum<br/>
                {{Form::select('rx',$money_drop_down,old('rx'),['class'=>' form-control search', 'id' => 'rx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Salary Minimum<br/>
                {{Form::select('sm',$money_drop_down,old('sm'),['class'=>' form-control search', 'id' => 'sm', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Salary Maximum<br/>
                {{Form::select('sx',$money_drop_down,old('sx'),['class'=>' form-control search', 'id' => 'sx', 'style'=>'width: 100%;', 'placeholder' => 'All'])}}
            </div>
            <div class="col-sm-3 col-sm">
                <a href="{{route('jobspec.index')}}" type="submit" class="btn btn-info w-100" style="margin-top: 23px;">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('reference_number', 'Reference Number')</th>
                    <th>@sortablelink('role_id', 'Position Type')</th>
                    <th>@sortablelink('placement_by', 'Placement By')</th>
                    <th>@sortablelink('applicaiton_closing_date', 'Application Close Date')</th>
                    <th>@sortablelink('position_description', 'Position Description')</th>
                    <th class="text-center">@sortablelink('priority', 'Priority')</th>
                    <th>@sortablelink('customer_id', 'Customer')</th>
                    <th>@sortablelink('document', 'Documents')</th>
                    <th>@sortablelink('status_id', 'Job Status')</th>
                    <th>@sortablelink('no_cvs','No of CV’s Submitted')</th>
                    @if($can_update)
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($job_specs as $job_spec)
                    <tr>
                        <td><a href="{{route('jobspec.show',$job_spec)}}">{{$job_spec->reference_number}}</a></td>
                        <td>{{$job_spec->positiontype?->description}}</td>
                        <td>{{$job_spec->placement_by}}</td>
                        <td>{{$job_spec->applicaiton_closing_date}}</td>
                        <td>{{$job_spec->position_description}}</td>
                        <td class="text-center">
                            @if($job_spec->priority < 3)
                                <span class="badge badge-success">{{$job_spec->priority}}</span>
                            @elseif($job_spec->priority >= 3 && $job_spec->priority < 6)
                                <span class="badge badge-warning">{{$job_spec->priority}}</span>
                            @elseif($job_spec->priority >= 6)
                                <span class="badge badge-danger">{{$job_spec->priority}}</span>
                            @endif
                        </td>
                        <td>{{$job_spec->customer?->customer_name}}</td>
                        <td>
                            @foreach($documents[$job_spec->id] as $document)
                                <a href="{{route('jobSpecDocuments', $document)}}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                            @endforeach
                        </td>
                        <td>{{$job_spec->status?->name}}</td>
                        <td>{{isset($cvs_sumbitted)?$cvs_sumbitted:''}}</td>
                        @if($can_update)
                            <td>

                                <div class="d-flex">
                                    <a href="{{route('jobspec.edit',$job_spec)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['jobspec.destroy', $job_spec],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $job_specs->firstItem() }} - {{ $job_specs->lastItem() }} of {{ $job_specs->total() }}
                        </td>
                        <td>
                            {{ $job_specs->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function (){
            const resources = @json($resource_dropdown);
            const recruiter_ids = @json($recruiter_ids);
            let options = `<option value="${null}" selected disabled>Select Recruiter</option>`;
            resources.forEach(resource => recruiter_ids.forEach(recruiter => {
                if (resource.id == recruiter){
                    options += `<option value="${resource.id}">${resource.full_name}</option>`;
                }
            }))

            $("#recruiter").html(options)
        })
    </script>
@endsection