@extends('adminlte.default')

@section('title') View Team @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            {{Form::open(['url' => route('team.store',['companyid' => $companyid]), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="6" class="btn-dark">Team Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Team Name:</th>
                    <td>{{Form::text('team_name',old('team_name'),['class'=>'form-control form-control-sm'. ($errors->has('team_name') ? ' is-invalid' : ''),'placeholder'=>'Team Name'])}}
                        @foreach($errors->get('team_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Team Manager:</th>
                    <td>{{Form::select('team_manager',$team_manager_dropdown,old('team_manager'),['class'=>'form-control form-control-sm'. ($errors->has('team_manager') ? ' is-invalid' : ''),'placeholder'=>'Team Manager'])}}
                        @foreach($errors->get('team_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status:</th>
                    <td>{{Form::select('status_id',$sidebar_process_statuses,'1',['class'=>'form-control form-control-sm','id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
