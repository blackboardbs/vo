@extends('adminlte.default')

@section('title') Edit Team @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('editTeam')" class="btn btn-sm btn-primary ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            {{Form::open(['url' => route('team.update',$team), 'method' => 'patch','class'=>'mt-3','id'=>'editTeam'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="6" class="btn-dark">Team Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Team Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('team_name',$team->team_name,['class'=>'form-control form-control-sm'. ($errors->has('team_name') ? ' is-invalid' : ''),'placeholder'=>'Team Name'])}}
                        @foreach($errors->get('team_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Team Manager: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('team_manager',$users_dropdown,$team->team_manager,['class'=>'form-control form-control-sm '. ($errors->has('team_manager') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('team_manager') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$team->status_id,['class'=>'form-control form-control-sm ','id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection