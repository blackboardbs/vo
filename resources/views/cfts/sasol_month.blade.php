@extends('adminlte.default')
@section('title') View Custom Combined Timesheet  @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
    <hr />
@endsection

@section('content')
    <div class="container-fluid">
        @php
            $date = new DateTime($timesheet->last_day_of_month);

            $total_time = 0;
            $total_mileage = 0;
            function minutesToTime($minutes){
                $hours = floor($minutes/60);
                $minutes = $minutes % 60;
                $negation_number = '';
                if($hours < 0){
                    $hours *= -1;
                    $negation_number = '-';
                }

                if($minutes < 0){
                    $minutes *= -1;
                    $negation_number = '-';
                }
                $hour_str = $hours < 10 ? '0'.$hours : $hours;
                $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                return $negation_number.$hour_str.':'.$minutes_str;
            }
        @endphp
        <table class="table table-sm mt-3" id="timesheets_div">
            <thead>
                <tr>
                    <th colspan="5">
                        <img src="{!! asset('assets/templates/eim_template_header.png') !!}" width="100%;" style="border: none;">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th colspan="5" class="eim-template-heading" style="text-align: center;">{{$cfts->additional_text}}</th>
                </tr>
                <tr>
                    <th colspan="5" style="color: #ff1320">Note: All overtime will require pre-approval by your Line Manager</th>
                </tr>
                <tr>
                    <th class="eim-template-heading" style="width: 20%;">Resource Name:</th><td style="width: 20%;">{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td><td style="width: 20%;">&nbsp;</td><th class="eim-template-heading" style="width: 20%;">Start Date:</th><td style="width: 20%;">{{ date('Y/m/d', strtotime($cfts->start_date))}}</td>
                </tr>
            </tbody>
        </table>
        <div style="width: 100%; height: 100%; white-space: nowrap; position: relative; overflow-x:scroll; overflow-y:hidden;">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th rowspan="2" class="eim-template-heading">No</th><th rowspan="2" class="eim-template-heading">Project</th><th rowspan="2" class="eim-template-heading">PO Number</th>
                        @foreach($timeline_mon_hour as $key => $value)
                        <th colspan="7" class="eim-template-heading" style="text-align: center;">{{$mon_date[$key]}}</th>
                        @endforeach
                        <td class="eim-template-heading"></td>
                    </tr>
                    <tr>
                        @foreach($timeline_mon_hour as $key => $value)
                        <th class="eim-template-heading">M</th><th class="eim-template-heading">Tu</th><th class="eim-template-heading">W</th><th class="eim-template-heading">th</th><th class="eim-template-heading">F</th><th class="eim-template-heading">Sa</th><th class="eim-template-heading">Su</th>
                        @endforeach
                        <td class="eim-template-heading">Tot</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td><td>{{isset($timesheet->project->name)? $timesheet->project->name : ''}}</td><td>{{isset($assignment->customer_po)?$assignment->customer_po:''}}</td>
                        @php
                            $total_billing_hours = 0;
                            $Weeks_counter = 1;
                        @endphp
                        @foreach($timeline_mon_hour as $key => $value)
                            <th class="eim-template-heading">{{ minutesToTime($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) }}</th><th class="eim-template-heading">{{ minutesToTime($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]) }}</th>
                            @php
                                $total_minutes = ($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) + ($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) + ($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) + ($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) + ($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) + ($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) + ($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]);
                                $total_time = minutesToTime($total_minutes);
                                $total_billing_hours += $total_minutes;
                                $Weeks_counter++;
                            @endphp
                        @endforeach
                        <td>{{minutesToTime($total_billing_hours)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br/>
        <div class="row">
        @foreach($digisign_users as $digisign_user)
            @php $type = ''; @endphp
            @php $user_full_name = ''; @endphp
            @if($digisign_user->user_number == 1)
                @php
                    $type = 'Resource';
                    if(isset($employee_ts->first_name)){
                        $user_full_name = $employee_ts->first_name.' '.$employee_ts->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 2)
                @php
                    $type = $assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner';
                    if(isset($project_manager_ts_user->first_name)){
                        $user_full_name = $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 3)
                @php
                    $type = $assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner';
                    if(isset($project_owner_ts_user->first_name)){
                        $user_full_name = $project_owner_ts_user->first_name.' '.$project_owner_ts_user->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 4)
                @php
                    $type = $assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager';
                    if(isset($project_manager_new_ts_user->first_name)){
                        $user_full_name = $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 5)
                @php
                    $type = $assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager';
                    if(isset($line_manager_ts_user->first_name)){
                        $user_full_name = $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 6)
                @php
                    $type = $assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver';
                    if(isset($claim_approver_ts_user->first_name)){
                        $user_full_name = $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name;
                    }
                @endphp
            @endif
            @if($digisign_user->user_number == 7)
                @php
                    $type = $assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager';
                    if(isset($resource_manager_ts_user->first_name)){
                        $user_full_name = $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name;
                    }
                @endphp
            @endif

            @if($digisign_user->user_number > 7)
                @php $type = 'Name'; @endphp
                $user_full_name = 'Name';
            @endif

            <div class="col-sm-4">
                <table class="table borderless table-sm mt-3" style="width: 100%;">
                    <thead>
                    <tr><th class="eim-template-heading">{{$type}}</th></tr>
                    <tr><td style="height: 70px; border: 1px #000000 solid;">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td></tr>
                    <tr><td class="signature">{{$user_full_name}}</td></tr>
                    </thead>
                </table>
            </div>

        @endforeach
        </div>
        {{--<table class="table table-sm mt-3">
            <tbody>
                <tr>
                    <th colspan="3">
                        <div class="row">
                            <div class="col-sm-4">
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                        <tr><th class="eim-template-heading">Resource</th></tr>
                                        <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                        <tr><td>{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td></tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                    <tr><th class="eim-template-heading">Project Manager</th></tr>
                                    <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                    <tr><td>{{$project_manager_name}}</td></tr>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                        <tr><th class="eim-template-heading">Line Manager</th></tr>
                                        <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                        <tr><td>{{$line_manager_name}}</td></tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </th>
                </tr>
            </tbody>
        </table>--}}
        {{--<table class="table table-borderless">
            <tbody>
                <tr>
                    <td colspan="4" class="text-center">
                        <a href="{{route('cfts.pdf', $cfts->id)}}" target="_blank" class="btn btn-dark btn-sm"><i class="fa fa-download"></i> PDF</a>
                        <a href="" onclick="printContents()" target="_blank" class="btn btn-dark btn-sm"><i class="fa fa-print"></i> Print</a>
                    </td>
                </tr>
            </tbody>
        </table>--}}
    </div>
@endsection
@section('extra-css')
    <style>
        .eim-template-heading{
            background-color: #161860;
            color: #ffffff;
            font-size: 21px;
        }

        .borderless td, .borderless th {
            border: none;
        }
    </style>
@endsection
@section('extra-js')
    <script>

        function printContents() {
            var printContents = document.getElementById('timesheets_div').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }

    </script>
@endsection
