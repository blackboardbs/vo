@extends('adminlte.default')
@section('title') Custom Formatted Timesheets @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('cfts.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Custom Formated Timesheets</a>
        @endif
    </div>
@endsection
@section('content')
    <div class="container-fluid">
            <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
                <div class="col-sm-3 col-sm">
                    Customer<br/>
                    {{Form::select('customer',$customer_drop_down,old('customer'),['class'=>' form-control form-control-sm search', 'id' => 'customer', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-sm-3 col-sm">
                    Status<br/>
                    {{Form::select('status',$status_drop_down,old('status'),['class'=>' form-control form-control-sm search', 'id' => 'status', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-sm-3 col-sm">
                    Clear Filters<br/>
                    <a href="{{route('cfts.index')}}" style="width: 100%; text-align: left; font-size: 13px; border: none !important; color: #357ca5 !important; border-radius: 4px !important; padding: 5px 7px 5px 7px !important;" class="btn btn-default" type="submit">Clear Filters</a>
                </div>
            </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                    <tr class="btn-dark">
                        <th>@sortablelink('id', 'CFTS')</th>
                        <th>@sortablelink('customer.customer_name', 'Customer Name')</th>
                        <th>@sortablelink('reference', 'Reference')</th>
                        <th>@sortablelink('template', 'Template')</th>
                        <th>@sortablelink('digisign','Signed')</th>
                        <th>@sortablelink('created_at', 'Created Date')</th>
                        <th>@sortablelink('status.description', 'Status')</th>
                        @if($can_update)
                            <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @forelse($cfts_collections as $_cfts)
                    <tr>
                        <td><a href="{{route('cfts.show',$_cfts['id'])}}">{{$_cfts['id']}}</a></td>
                        <td><a href="{{route('cfts.show',$_cfts['id'])}}">{{$_cfts['customer_name']}}</a></td>
                        <td>{{$_cfts['reference']}}</td>
                        <td>{{$_cfts['template']}}</td>
                        <td>{{($_cfts['digisign'] == 2)?'Digisign':'Exteranl'}}</td>
                        <td>{{$_cfts['created_at']}}</td>
                        <td>{{$_cfts['status']}}</td>
                        @if($can_update)
                        <td>
                            {{--Todo: fix the edit functionality you have time--}}
                            {{--<a href="{{route('cfts.edit',$_cfts['id'])}}" class="btn btn-success btn-sm">Edit</a>--}}
                            {{ Form::open(['method' => 'DELETE','route' => ['cfts.destroy', $_cfts['id']],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $cfts->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection