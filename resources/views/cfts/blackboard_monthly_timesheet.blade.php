@extends('adminlte.default')
@section('title') View Custom Combined Timesheet  @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('cfts.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
    <hr />
@endsection

@section('content')
    <div class="container-fluid">
        @php
            $date = new DateTime($timesheet->last_day_of_month);

            $total_time = 0;
            $total_mileage = 0;
            function minutesToTime($minutes){
                $hours = floor($minutes/60);
                $minutes = $minutes % 60;
                $negation_number = '';
                if($hours < 0){
                    $hours *= -1;
                    $negation_number = '-';
                }

                if($minutes < 0){
                    $minutes *= -1;
                    $negation_number = '-';
                }
                $hour_str = $hours < 10 ? '0'.$hours : $hours;
                $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                return $negation_number.$hour_str.':'.$minutes_str;
            }
        @endphp
        <div  id="timesheets_div">
            <table class="table table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="3">
                            <img src="{!! asset('assets/templates/bb_logo.jpg') !!}" width="30%;" style="border: none;">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th colspan="3" class="eim-template-heading" style="text-align: center;">{{$cfts->additional_text}}</th>
                    </tr>
                    <tr>
                        <th colspan="3" style="color: #ff1320">Note: All overtime will require pre-approval by your Line Manager</th>
                    </tr>
                    <tr>
                        <th class="eim-template-heading" style="width: 33%;">Resource Name:</th><td colspan="2">{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td>
                    </tr>
                    <tr>
                        <th class="eim-template-heading" style="width: 33%;">Project Name:</th><td colspan="2">{{isset($timesheet->project->name)? $timesheet->project->name : ''}}</td>
                    </tr>
                    <tr>
                        <th class="eim-template-heading" style="width: 33%;">PPO Number:</th><td colspan="2"></td>
                    </tr>
                    <tr>
                        <th class="eim-template-heading" style="width: 33%;">PO Number: </th><td colspan="2">{{isset($assignment->customer_po)?$assignment->customer_po:''}}</td>
                    </tr>
                    <tr>
                        <th colspan="3"></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-sm mt-3" id="timesheets_div">
                <tbody>
                    <tr>
                        <th colspan="3">
                            <div class="row">
                                @php
                                    $total_billing_hours = 0;
                                    $Weeks_counter = 1;
                                @endphp
                                @foreach($timeline_mon_hour as $key => $value)
                                @php
                                    $total_minutes = ($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) + ($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) + ($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) + ($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) + ($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) + ($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) + ($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]);
                                    $total_time = minutesToTime($total_minutes);
                                @endphp
                                <div class="col-sm-4">
                                    <table class="table borderless table-sm mt-3">
                                        <thead>
                                            <tr><th colspan="3" class="eim-template-heading" style="text-align: center;">Week {{$Weeks_counter}}</th></tr>
                                            <tr><td class="eim-template-heading">Date</td><td class="eim-template-heading">Day</td><td class="eim-template-heading">Hours</td></tr>
                                        </thead>
                                        <tbody>
                                            <tr><td>{{$mon_date[$key]}}</td><td>Monday</td><td style="text-align: right;">{{ minutesToTime($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) }}</td></tr>
                                            <tr><td>{{$tue_date[$key]}}</td><td>Tuesday</td><td style="text-align: right;">{{ minutesToTime($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) }}</td></tr>
                                            <tr><td>{{$wed_date[$key]}}</td><td>Wednesday</td><td style="text-align: right;">{{ minutesToTime($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) }}</td></tr>
                                            <tr><td>{{$thu_date[$key]}}</td><td>Thursday</td><td style="text-align: right;">{{ minutesToTime($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) }}</td></tr>
                                            <tr><td>{{$fri_date[$key]}}</td><td>Friday</td><td style="text-align: right;">{{ minutesToTime($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) }}</td></tr>
                                            <tr><td>{{$sat_date[$key]}}</td><td>Saturday</td><td style="text-align: right;">{{ minutesToTime($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) }}</td></tr>
                                            <tr><td>{{$sun_date[$key]}}</td><td>Sunday</td><td style="text-align: right;">{{ minutesToTime($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]) }}</td></tr>
                                            <tr><th colspan="2" class="eim-template-heading">Week Total</th><th class="eim-template-heading" style="text-align: right;">{{ $total_time }}</th></tr>
                                        </tbody>
                                    </table>
                                </div>
                                @php
                                    $total_billing_hours += $total_minutes;
                                    $Weeks_counter++;
                                @endphp
                                @endforeach
                                <div class="col-md-12 col-sm-12">
                                    <table class="table borderless table-sm mt-3">
                                        <thead>
                                            <tr><th colspan="2" class="eim-template-heading">Total Billing Hours</th><th class="eim-template-heading" style="text-align: right;">{{ minutesToTime($total_billing_hours) }}</th></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <div class="row">
                                @foreach($digisign_users as $digisign_user)
                                    @php $type = ''; @endphp
                                    @php $user_full_name = ''; @endphp
                                    @if($digisign_user->user_number == 1)
                                        @php
                                            $type = 'Resource';
                                            if(isset($employee_ts->first_name)){
                                                $user_full_name = $employee_ts->first_name.' '.$employee_ts->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 2)
                                        @php
                                            $type = $assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner';
                                            if(isset($project_manager_ts_user->first_name)){
                                                $user_full_name = $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 3)
                                        @php
                                            $type = $assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner';
                                            if(isset($project_owner_ts_user->first_name)){
                                                $user_full_name = $project_owner_ts_user->first_name.' '.$project_owner_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 4)
                                        @php
                                            $type = $assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager';
                                            if(isset($project_manager_new_ts_user->first_name)){
                                                $user_full_name = $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 5)
                                        @php
                                            $type = $assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager';
                                            if(isset($line_manager_ts_user->first_name)){
                                                $user_full_name = $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 6)
                                        @php
                                            $type = $assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver';
                                            if(isset($claim_approver_ts_user->first_name)){
                                                $user_full_name = $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif
                                    @if($digisign_user->user_number == 7)
                                        @php
                                            $type = $assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager';
                                            if(isset($resource_manager_ts_user->first_name)){
                                                $user_full_name = $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name;
                                            }
                                        @endphp
                                    @endif

                                    @if($digisign_user->user_number > 7)
                                        @php $type = 'Name'; @endphp
                                        $user_full_name = 'Name';
                                    @endif
                                    <div class="col-sm-4">
                                        <table class="table borderless table-sm mt-3" style="width: 100%;">
                                            <thead>
                                            <tr><th class="eim-template-heading">{{$type}}</th></tr>
                                            <tr><td style="height: 70px; border: 1px #000000 solid;">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td></tr>
                                            <tr><td class="signature">{{$user_full_name}}</td></tr>
                                            </thead>
                                        </table>
                                    </div>

                                @endforeach
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                        <tr>
                                            <td colspan="2" style="font-style: italic;">Template: Blackboard Monthly CFTS</td><td style="text-align: right; font-style: italic;">Produced by Blackboard Web Office Time Management</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <table class="table table-borderless" style="border-bottom: none;">
            <tbody>
                <tr>
                    <td colspan="4" class="text-center">
                        <a href="?print=1" target="_blank" class="btn btn-dark btn-sm"><i class="fa fa-print"></i> Print</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
@section('extra-css')
    <style>
        .eim-template-heading{
            background-color: grey;
            color: #ffffff;
            font-size: 21px;
        }

        .borderless td, .borderless th {
            border: none;
        }
    </style>
@endsection
@section('extra-js')
    <script>

        function printContents() {
            var printContents = document.getElementById('timesheets_div').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }

    </script>
@endsection
