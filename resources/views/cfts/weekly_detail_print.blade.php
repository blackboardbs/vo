<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>CF Template Weekly Detail</title>
    <style>
        body{margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}}
        .eim-template-heading{
            background-color: #161860;
            color: #ffffff;
            font-size: 18px;
        }

        .borderless td, .borderless th {
            border: none;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini" style="line-height: 1.24em;">
<div id="app" class="wrapper">
    <div class="container-fluid">
        <div id="timesheets_div">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="6">
                            <img src="{!! asset('assets/templates/eim_template_header.png') !!}" style="border: none; width: 250px;">
                        </th>
                        <td class="header-text">
                            Timesheet
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent; border-bottom: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Name</th><td colspan="6">{{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}} {{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}}</td>
                    </tr>
                    <tr>
                        <th>Week</th><td>{{$timesheet->week}}</td><td>From (Monday)</td><td colspan="4">{{$timesheet->first_day_of_week}}</td>
                    </tr>
                    <tr>
                        <th colspan="2">&nbsp;</th><td>To (Sunday)</td><td colspan="4">{{$timesheet->last_day_of_week}}</td>
                    </tr>
                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent; border-bottom: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Date</th><th>Project code</th><th>Project name</th><th>Description</th><th>Hours</th><th>Billable</th><th>Non Billable</th>
                    </tr>
                @php
                    $total_hours_billable = 0;
                    $total_minutes_billable = 0;
                    $total_hours_non_billable = 0;
                    $total_minutes_non_billable = 0;
                @endphp

                @foreach($timelines as $timeline)
                    @php
                        if($timeline->is_billable == 1){
                            $total_hours_billable += $timeline->total;
                            $total_minutes_billable += $timeline->total_m;
                        }
                        else{
                            $total_hours_non_billable += $timeline->total;
                            $total_minutes_non_billable += $timeline->total_m;
                        }

                        $description_of_work = isset($timeline->task->description)?$timeline->task->description:'';
                        if($description_of_work != ''){
                            if($timeline->description_of_work != '')
                                $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                        else{
                            $description_of_work = $timeline->description_of_work;
                        }
                    @endphp

                    @if($timeline->is_billable == 1)
                        <tr>
                            <td>{{date('Y-m-d', strtotime($timeline->created_at))}}</td><td>{{isset($timesheet->project->ref) ? $timesheet->project->name : ''}}</td><td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td><td>{{$description_of_work}}</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td><td>-</td>
                        </tr>
                    @else
                        <tr>
                            <td>{{date('Y-m-d', strtotime($timeline->created_at))}}</td><td>{{isset($timesheet->project->ref) ? $timesheet->project->name : ''}}</td><td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td><td>{{$description_of_work}}</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td><td>-</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td>
                        </tr>
                    @endif
                @endforeach
                    <tr>
                        <th colspan="4">TOTAL (weekly billable hours)</th><td></td><th>{{minutesToTime(($total_hours_billable * 60) + $total_minutes_billable)}}</th><td></td>
                    </tr>
                    <tr>
                        <th colspan="4">TOTAL (weekly non-billable hours)</th><td></td><td></td><th>{{minutesToTime(($total_hours_non_billable * 60) + $total_minutes_non_billable)}}</th>
                    </tr>
                </tbody>
            </table>
            <br/>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <th style="width: 170px; border: 1px solid transparent">Consultant</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <th style="width: 170px; border: 1px solid transparent">Client</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <th style="width: 170px; border: 1px solid transparent">PM</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;"></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>