@extends('adminlte.default')

@section('title') Active Timesheets @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div style="margin-top: 10px;" class="col-md-3 col-sm">
                Template<br/>
                {!! Form::select('template', $template_drop_down, isset($selected_template) ? $selected_template : old('template'), ['class' => 'form-control  form-control-sm search', 'id' => 'template']) !!}
            </div>
            <div style="margin-top: 10px;" class="col-md-3 col-sm">
                Week Start Date<br/>
                {!! Form::select('start_date', $week_drop_down, isset($selected_week) ? $selected_week : old('start_date'), ['class' => 'form-control  form-control-sm search', 'id' => 'start_date']) !!}
            </div>
            <div style="margin-top: 10px;" class="col-md-3 col-sm">
                Customer<br/>
                {!! Form::select('customer', $customer_drop_down, $selected_customer_id, ['class' => 'form-control  form-control-sm search', 'id' => 'customer']) !!}
            </div>
            <div class="col-md-3 col-sm">
                Project<br/>
                {!! Form::select('project', $project_drop_down, $selected_project != 0 ? $selected_project : old('project'), ['class' => 'form-control  form-control-sm search', 'id' => 'project']) !!}
            </div>
            <div class="col-md-3 col-sm">
                Resources<br/>
                {!! Form::select('employees', $resource_drop_down, $selected_resource != 0 ? $selected_resource : old('employees'), ['class' => 'form-control  form-control-sm search', 'id' => 'employees']) !!}
            </div>
            <div class="col-md-3 col-sm">
                Week From<br/>
                {!! Form::select('week_from', $year_weeks, old('week_from'), ['placeholder' => 'All', 'class' => 'form-control  form-control-sm search', 'id' => 'week_from']) !!}
            </div>
            <div class="col-md-3 col-sm">
                Week To<br/>
                {!! Form::select('week_to', $year_weeks, old('week_to'), ['placeholder' => 'All', 'class' => 'form-control  form-control-sm search', 'id' => 'week_to']) !!}
            </div>
            <div class="col-sm-3 col-sm">
                Clear Filters<br/>
                <a href="{{route('cfts.create')}}" style="" class="btn btn-sm btn-default col-md-12 col-sm-12" type="submit">Clear Filters</a>
            </div>
        </form>
        <div class="table-responsive">
            {!! Form::open(['url' => route('cfts.prepare'), 'id' => 'timesheet']) !!}
            <input type="hidden" name="start_date_h" id="start_date_h" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : $selected_week}}" />
            <input type="hidden" name="template" id="template" value="{{isset($_GET['template']) ? $_GET['template'] : $selected_template}}" />
            @php

                function minutesToTime($minutes){
                    $hours = floor($minutes/60);
                    $minutes = $minutes % 60;
                    $negation_number = '';
                    if($hours < 0){
                        $hours *= -1;
                        $negation_number = '-';
                    }

                    if($minutes < 0){
                        $minutes *= -1;
                        $negation_number = '-';
                    }
                    $hour_str = $hours < 10 ? '0'.$hours : $hours;
                    $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                    return $negation_number.$hour_str.':'.$minutes_str;
                }

            @endphp
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr class="bg-dark">
                    <th>@sortablelink('#')</th>
                    <th>@sortablelink('Customer Name')</th>
                    <th>@sortablelink('Resource')</th>
                    <th>@sortablelink('Project')</th>
                    <th>@sortablelink('Year Week')</th>
                    <th class="text-right">@sortablelink('Bill')</th>
                    <th class="text-right">@sortablelink('No Bill')</th>
                    <th>@sortablelink('Expenses')</th>
                    <th>@sortablelink('Status')</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $total_billable_hoursb_ = 0;
                    $total_non_billable_hours_ = 0;
                @endphp
                @forelse($timesheets as $key => $timesheet)
                    @php
                        /*$total_billable_hoursb_ += $billable_hours[$key] ;
                        $total_non_billable_hours_ += $non_billable_hours[$key] ;*/
                        $total_billable_hoursb_ += $timesheet->billable_minutes;
                        $total_non_billable_hours_ += $timesheet->non_billable_minutes;
                    @endphp
                    <tr>
                        <td>{!! Form::checkbox('timesheet[]', $timesheet->id) !!} {{ $timesheet->id }}</td>
                        <td><a href="{{ route('timesheet.show', $timesheet) }}">{{ isset($timesheet->customer) ? $timesheet->customer->customer_name : ''}}</a></td>
                        <td><a href="{{ route('timesheet.show', $timesheet) }}">{{ isset($timesheet->employee) ? $timesheet->employee->first_name.', '.$timesheet->employee->last_name : '' }}</a> </td>
                        <td>{{ isset($timesheet->project) ? $timesheet->project->name : '' }}</td>
                        <td>{{ $timesheet->year_week }}</td>
                        <td class="text-right">{{ /*$billable_hours[$key]*/ minutesToTime($timesheet->billable_minutes) }}</td>
                        <td class="text-right">{{ /*$non_billable_hours[$key]*/ minutesToTime($timesheet->non_billable_minutes) }}</td>
                        <td>@forelse($time_exp[$key] as $expense)
                                @if($expense != null && $expense->claim_auth_date != null)
                                    <span class="badge badge-success mr-1"> {{ substr(strtoupper($expense->description), 0, 1) }} | <i class="fa fa-check" aria-hidden="true"></i></span>
                                @else
                                    <span class="badge badge-danger mr-1"> {{ substr(strtoupper($expense->description), 0, 1) }} | <i class="fa fa-times" aria-hidden="true"></i></span>
                                @endif
                            @empty
                            @endforelse
                        </td>
                        <td>{{ isset($timesheet->status) ? $timesheet->status->description : '' }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="17" class="text-center">No open timesheet.</td>
                    </tr>
                @endforelse
                @if(count($timesheets) > 0)
                    <tr>
                        <td colspan="4"></td>
                        <th>TOTAL HOURS</th>
                        <th class="text-right">{{ minutesToTime($total_billable_hoursb_) }}</th>
                        <th class="text-right">{{ minutesToTime($total_non_billable_hours_) }}</th>
                        <th></th><th></th>
                    </tr>
                @endif

                </tbody>
            </table>
            <div class="text-center">{!! Form::submit('Create Custom Formated Timesheet', ['class' => 'btn btn-sm', 'disabled' => 'disabled', 'id' => 'prepare']) !!}</div>
            {!! Form::close() !!}
            @if(!request()->has('week'))
                {{ $timesheets->links() }}
            @endif
            <br/>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });

        $("#timesheet").on('click', function (event) {
            if ($(event.target).is('input')) {
                $("#prepare").prop('disabled', false);
            }
        })
    </script>
@endsection
