@extends('adminlte.default')
@section('title') View Custom Formated Timesheet  @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
    <hr />
@endsection

@section('content')
    <div class="container-fluid">
        @php
            /*$date = new DateTime($timesheet->last_day_of_month);

            $total_time = 0;
            $total_mileage = 0;*/
            function minutesToTime($minutes){
                $hours = floor($minutes/60);
                $minutes = $minutes % 60;
                $negation_number = '';
                if($hours < 0){
                    $hours *= -1;
                    $negation_number = '-';
                }

                if($minutes < 0){
                    $minutes *= -1;
                    $negation_number = '-';
                }
                $hour_str = $hours < 10 ? '0'.$hours : $hours;
                $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                return $negation_number.$hour_str.':'.$minutes_str;
            }
        @endphp
        <div id="timesheets_div">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="6">
                            <img src="{!! asset('assets/templates/eim_template_header.png') !!}" style="border: none; width: 250px;">
                        </th>
                        <td class="header-text">
                            Timesheet
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Name</th><td colspan="6">{{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}} {{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}}</td>
                    </tr>
                    <tr>
                        <th>Week</th><td>{{$timesheet->week}}</td><td>From (Monday)</td><td colspan="4">{{$timesheet->first_day_of_week}}</td>
                    </tr>
                    <tr>
                        <th colspan="2">&nbsp;</th><td>To (Sunday)</td><td colspan="4">{{$timesheet->last_day_of_week}}</td>
                    </tr>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Date</th><th>Project code</th><th>Project name</th><th>Description</th><th>Hours</th><th>Billable</th><th>Non Billable</th>
                        @php
                            $total_hours_billable = 0;
                            $total_minutes_billable = 0;
                            $total_hours_non_billable = 0;
                            $total_minutes_non_billable = 0;
                        @endphp

                        @foreach($timelines as $timeline)
                            @php
                                if($timeline->is_billable == 1){
                                    $total_hours_billable += $timeline->total;
                                    $total_minutes_billable += $timeline->total_m;
                                }
                                else{
                                    $total_hours_non_billable += $timeline->total;
                                    $total_minutes_non_billable += $timeline->total_m;
                                }

                                $description_of_work = isset($timeline->task->description)?$timeline->task->description:'';
                                if($description_of_work != ''){
                                    if($timeline->description_of_work != '')
                                        $description_of_work .= ' - '.$timeline->description_of_work;
                                }
                                else{
                                    $description_of_work = $timeline->description_of_work;
                                }
                            @endphp

                            @if($timeline->is_billable == 1)
                            <tr>
                                <td>{{date('Y-m-d', strtotime($timeline->created_at))}}</td><td>{{isset($timesheet->project->ref) ? $timesheet->project->name : ''}}</td><td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td><td>{{$description_of_work}}</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td><td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td><td>-</td>
                            </tr>
                            @else
                            <tr>
                                <td>{{date('Y-m-d', strtotime($timeline->created_at))}}</td><td>{{isset($timesheet->project->ref) ? $timesheet->project->name : ''}}</td><td>{{isset($timesheet->project->name) ? $timesheet->project->name : ''}}</td><td>{{$description_of_work}}</td><td></td><td>-</td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}<td>{{minutesToTime(($timeline->total * 60) + $timeline->total_m)}}</td>
                            </tr>
                            @endif
                        @endforeach
                        <tr>
                            <th colspan="4">TOTAL (weekly billable hours)</th><td></td><th>{{minutesToTime(($total_hours_billable * 60) + $total_minutes_billable)}}</th><td></td>
                        </tr>
                        <tr>
                            <th colspan="4">TOTAL (weekly non-billable hours)</th><td></td><td></td><th>{{minutesToTime(($total_hours_non_billable * 60) + $total_minutes_non_billable)}}</th>
                        </tr>
                        <tr>
                            <th colspan="7">&nbsp;</th>
                        </tr>
                        <tr>
                            <th>Consultant</th><td style="border-bottom: 1px solid #000000;"></td><td class="text-right">Date:</td><th style="border-bottom: 1px solid #000000"></th>
                        </tr>
                        <tr>
                            <th colspan="7">&nbsp;</th>
                        </tr>
                        <tr>
                            <th>Client</th><td style="border-bottom: 1px solid #000000;"></td><td class="text-right">Date:</td><th style="border-bottom: 1px solid #000000"></th>
                        </tr>
                        <tr>
                            <th colspan="7">&nbsp;</th>
                        </tr>
                        <tr>
                            <th>PM</th><td style="border-bottom: 1px solid #000000;"></td><td class="text-right">Date:</td><th style="border-bottom: 1px solid #000000"></th>
                        </tr>
                    </tr>
                </tbody>
            </table>
            {{--<table class="table table-sm mt-3" id="timesheets_div">
                <tbody>
                    <tr>
                        <th colspan="3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <table class="table borderless table-sm mt-3">
                                        <thead>
                                            <tr><th class="eim-template-heading">Resource</th></tr>
                                            <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                            <tr><td>{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td></tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col-sm-4">
                                    <table class="table borderless table-sm mt-3">
                                        <thead>
                                        <tr><th class="eim-template-heading">Project Manager</th></tr>
                                        <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                        <tr><td>{{$project_manager_name}}</td></tr>
                                    </table>
                                </div>
                                <div class="col-sm-4">
                                    <table class="table borderless table-sm mt-3">
                                        <thead>
                                        <tr><th class="eim-template-heading">Line Manager</th></tr>
                                        <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                        <tr><td>{{$line_manager_name}}</td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>--}}
        </div>
        {{--<table class="table table-borderless">
            <tbody>
                <tr>
                    <td colspan="4" class="text-center">
                        <a href="?print=1" target="_blank" class="btn btn-dark btn-sm"><i class="fa fa-print"></i> Print</a>
                    </td>
                </tr>
            </tbody>
        </table>--}}
    </div>
@endsection
@section('extra-css')
    <style>
        .eim-template-heading{
            background-color: #161860;
            color: #ffffff;
            font-size: 21px;
        }

        .borderless td, .borderless th {
            border: none;
        }

        .header-text{
            text-align: right;
            font-size: 30px;
        }
    </style>
@endsection
@section('extra-js')
    <script>

        function printContents() {
            var printContents = document.getElementById('timesheets_div').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }

    </script>
@endsection