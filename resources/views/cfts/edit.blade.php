@extends('adminlte.default')
@section('title') Scouting @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('scouting.update', $scouting), 'method' => 'put','class'=>'mt-3', 'id'=>'create_scouting_form','files' => true,'autocomplete'=>'on'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Create Scouting</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Resource First Name:</th>
                    <td>
                        {{Form::text('resource_first_name',$scouting->resource_first_name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_first_name') ? ' is-invalid' : ''), 'placeholder'=>'First Name'])}}
                        @foreach($errors->get('resource_first_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Resource Last Name</th>
                    <td>
                        {{Form::text('resource_last_name',$scouting->resource_last_name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_last_name') ? ' is-invalid' : ''), 'placeholder'=>'Last name'])}}
                        @foreach($errors->get('resource_last_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Role:</th>
                    <td>{{Form::select('role_id',$role_drop_down,$scouting->role_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Email:</th>
                    <td>{{Form::text('email',$scouting->email,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Process Status:</th>
                    <td>{{Form::select('process_status_id',$process_statuses_drop_down,$scouting->process_status_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('process_status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('process_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Process Interview:</th>
                    <td>{{Form::select('interview_status_id',$process_interview_drop_down,$scouting->interview_status_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('interview_status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('interview_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                <tr>
                    <th>Vendor:</th>
                    <td>{{Form::select('vendor_id',$vendor_drop_down, $scouting->vendor_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('vendor_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('vendor_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Technical Rating:</th>
                    <td>{{Form::select('technical_rating_id',$tecnical_rating_drop_down, $scouting->technical_rating_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('technical_rating_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('technical_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Business Rating:</th>
                    <td>{{Form::select('business_rating_id',$business_rating_drop_down, $scouting->business_rating_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('business_rating_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('business_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Resource Level:</th>
                    <td>{{Form::select('resource_level_id',$resource_level_drop_down, $scouting->resource_level_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('resource_level_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_level_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Estimated Hourly Rate</th>
                    <td>
                        {{Form::text('estimated_hourly_rate',$scouting->estimated_hourly_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('estimated_hourly_rate') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate'])}}
                        @foreach($errors->get('estimated_hourly_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Availability:</th>
                    <td>
                        {{Form::text('availability',$scouting->availability,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('availability') ? ' is-invalid' : ''), 'placeholder'=>'Availability'])}}
                        @foreach($errors->get('availability') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Referred By:</th>
                    <td>{{Form::select('referral',$users_drop_down, $scouting->referral,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('referral') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('referral') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assessed By:</th>
                    <td>
                        {{Form::select('assessed_by',$users_drop_down, $scouting->assessed_by,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('assessed_by') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('assessed_by') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Document Link:</th>
                    <td colspan="3">
                        {{Form::file('file',['class'=>'form-control'. ($errors->has('file') ? ' is-invalid' : ''),'placeholder'=>'File'])}}
                        @foreach($errors->get('file') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Update</button>&nbsp;<a type="reset" href="{{route('scouting.index')}}" class="btn btn-danger btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $('#consultants_chosen').css('width', '100%');
        $('.chosen-container').css('width', '100%');
    </script>
@endsection