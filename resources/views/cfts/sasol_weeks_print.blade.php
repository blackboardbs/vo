<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>CFTS Timesheet</title>
        <style>

            body{margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}}

            .eim-template-heading{
             background-color: #161860;
             color: #ffffff;
             font-size: 18px;
            }

            .borderless td, .borderless th {
                border: none;
            }

            .signature {
                font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
                color: #000000;
                text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
            }

        </style>
    </head>
    <body class="hold-transition sidebar-mini" style="line-height: 1.24em;">
        <div id="app" class="wrapper">
            <td class="container">
                @php
                    $date = new DateTime($timesheet->last_day_of_month);

                    $total_time = 0;
                    $total_mileage = 0;
                    function minutesToTime($minutes){
                        $hours = floor($minutes/60);
                        $minutes = $minutes % 60;
                        $negation_number = '';
                        if($hours < 0){
                            $hours *= -1;
                            $negation_number = '-';
                        }

                        if($minutes < 0){
                            $minutes *= -1;
                            $negation_number = '-';
                        }
                        $hour_str = $hours < 10 ? '0'.$hours : $hours;
                        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;
                        return $negation_number.$hour_str.':'.$minutes_str;
                    }
                @endphp
                <table class="table table-sm mt-3 borderless" id="timesheets_div">
                    <tbody>
                        <tr>
                            <th colspan="3">
                                <img style="width:100%; height:190px;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'eim_template_header.png')}}">
                            </th>
                        </tr>
                        <tr>
                            <th colspan="3" class="eim-template-heading" style="text-align: center;">{{$cfts->additional_text}}</th>
                        </tr>
                        <tr>
                            <th colspan="3" style="color: #ff1320">Note: All overtime will require pre-approval by your Line Manager</th>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">Resource Name:</th><td colspan="2">{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">Project Name:</th><td colspan="2">{{isset($timesheet->project->name)? $timesheet->project->name : ''}}</td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">PPO Number:</th><td colspan="2"></td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">PO Number: </th><td colspan="2">{{isset($assignment->customer_po)?$assignment->customer_po:''}}</td>
                        </tr>
                        <tr>
                            <th colspan="3"></td>
                        </tr>
                        <tr>
                            @php
                                $total_billing_hours = 0;
                                $Weeks_counter = 1;
                            @endphp
                            @foreach($timeline_mon_hour as $key => $value)
                                @php
                                    $total_minutes = ($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) + ($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) + ($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) + ($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) + ($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) + ($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) + ($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]);
                                    $total_time = minutesToTime($total_minutes);
                                @endphp
                                    @if(($Weeks_counter - 1) % 3 == 0)
                                        </tr>
                                        <tr>
                                    @endif
                                    <td style="width: 33%">
                                        <table class="table borderless table-sm mt-3">
                                            <thead>
                                                <tr><th colspan="3" class="eim-template-heading" style="text-align: center;">Week {{$Weeks_counter}}</th></tr>
                                                <tr><td class="eim-template-heading">Date</td><td class="eim-template-heading">Day</td><td class="eim-template-heading">Hours</td></tr>
                                            </thead>
                                            <tbody>
                                                <tr><td>{{$mon_date[$key]}}</td><td>Monday</td><td style="text-align: right;">{{ minutesToTime($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) }}</td></tr>
                                                <tr><td>{{$tue_date[$key]}}</td><td>Tuesday</td><td style="text-align: right;">{{ minutesToTime($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) }}</td></tr>
                                                <tr><td>{{$wed_date[$key]}}</td><td>Wednesday</td><td style="text-align: right;">{{ minutesToTime($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) }}</td></tr>
                                                <tr><td>{{$thu_date[$key]}}</td><td>Thursday</td><td style="text-align: right;">{{ minutesToTime($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) }}</td></tr>
                                                <tr><td>{{$fri_date[$key]}}</td><td>Friday</td><td style="text-align: right;">{{ minutesToTime($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) }}</td></tr>
                                                <tr><td>{{$sat_date[$key]}}</td><td>Saturday</td><td style="text-align: right;">{{ minutesToTime($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) }}</td></tr>
                                                <tr><td>{{$sun_date[$key]}}</td><td>Sunday</td><td style="text-align: right;">{{ minutesToTime($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]) }}</td></tr>
                                                <tr><th colspan="2" class="eim-template-heading">Week Total</th><th class="eim-template-heading" style="text-align: right;">{{ $total_time }}</th></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                @php
                                    $total_billing_hours += $total_minutes;
                                    $Weeks_counter++;
                                @endphp
                            @endforeach
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">Total Billing Hours </th>
                            <td class="eim-template-heading" style="width: 33%;">&nbsp;</td>
                            <td class="eim-template-heading" style="width: 33%; text-align: right; font-weight: bold;">{{ minutesToTime($total_billing_hours) }}</td>
                        </tr>

                        {{--<tr>
                            <td style="width: 33%;">
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                    <tr><th class="eim-template-heading">Resource</th></tr>
                                    <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                    <tr><td>{{isset($timesheet->employee->last_name)? $timesheet->employee->last_name : ''}} {{isset($timesheet->employee->first_name)? $timesheet->employee->first_name : ''}}</td></tr>
                                    </thead>
                                </table>
                            </td>
                            <td>
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                    <tr><th class="eim-template-heading">Project Manager</th></tr>
                                    <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                    <tr><td>{{$project_manager_name}}</td></tr>
                                </table>
                            </td>
                            <td>
                                <table class="table borderless table-sm mt-3">
                                    <thead>
                                    <tr><th class="eim-template-heading">Line Manager</th></tr>
                                    <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                    <tr><td>{{$line_manager_name}}</td></tr>
                                    </thead>
                                </table>
                            </td>
                        </tr>--}}
                    </tbody>
                </table>
                <table class="table table-bordered table-sm mt-3">
                    <tbody>
                        <tr>
                            @php
                                $signature_counter = 0;
                            @endphp
                            @foreach($digisign_users as $digisign_user)
                                @php $type = ''; @endphp
                                @php $user_full_name = ''; @endphp
                                @if($digisign_user->user_number == 1)
                                    @php
                                        $type = 'Resource';
                                        if(isset($employee_ts->first_name)){
                                            $user_full_name = $employee_ts->first_name.' '.$employee_ts->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 2)
                                    @php
                                        $type = $assignment->internal_owner_known_as != '' ? $assignment->internal_owner_known_as : 'Internal Owner';
                                        if(isset($project_manager_ts_user->first_name)){
                                            $user_full_name = $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 3)
                                    @php
                                        $type = $assignment->product_owner_known_as != '' ? $assignment->product_owner_known_as : 'Product Owner';
                                        if(isset($project_owner_ts_user->first_name)){
                                            $user_full_name = $project_owner_ts_user->first_name.' '.$project_owner_ts_user->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 4)
                                    @php
                                        $type = $assignment->project_manager_new_known_as != '' ? $assignment->project_manager_new_known_as : 'Project Manager';
                                        if(isset($project_manager_new_ts_user->first_name)){
                                            $user_full_name = $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 5)
                                    @php
                                        $type = $assignment->line_manager_known_as != '' ? $assignment->line_manager_known_as : 'Line Manager';
                                        if(isset($line_manager_ts_user->first_name)){
                                            $user_full_name = $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 6)
                                    @php
                                        $type = $assignment->claim_approver_known_as != '' ? $assignment->claim_approver_known_as : 'Claim Approver';
                                        if(isset($claim_approver_ts_user->first_name)){
                                            $user_full_name = $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name;
                                        }
                                    @endphp
                                @endif
                                @if($digisign_user->user_number == 7)
                                    @php
                                        $type = $assignment->resource_manager_known_as != '' ? $assignment->resource_manager_known_as : 'Resource Manager';
                                        if(isset($resource_manager_ts_user->first_name)){
                                            $user_full_name = $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name;
                                        }
                                    @endphp
                                @endif

                                @if($digisign_user->user_number > 7)
                                    @php $type = 'Name'; @endphp
                                    $user_full_name = 'Name';
                                @endif

                                {{--<table class="table table-bordered table-sm mt-3">
                                    <tbody>
                                    <tr>
                                        <th style="width: 170px; border: 1px solid transparent">{{$type}}</th><td style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;" class="signature">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td><td class="text-right" style="border: 1px solid transparent">Date:</td><th style="width: 250px; border-bottom: 1px solid #000000; border-top: 1px solid transparent; border-right: 1px solid transparent;">{{$digisign_user->signed == 1 ? $digisign_user->user_sign_date : ''}}</th>
                                    </tr>
                                    </tbody>
                                </table>--}}
                                <td style="border: 1px solid transparent;">
                                    <table class="table borderless table-sm mt-3" style="width: 100%;">
                                        <thead>
                                        <tr><th class="eim-template-heading">{{$type}}</th></tr>
                                        <tr><td style="height: 40px; border: 1px #000000 solid;" class="signature">{{$digisign_user->signed == 1 ? $digisign_user->user_name : ''}}</td></tr>
                                        <tr><td>{{$user_full_name}}</td></tr>
                                        </thead>
                                    </table>
                                </td>

                                @php
                                    $signature_counter += 1;
                                @endphp
                                @if(($signature_counter % 3) == 0)
                                    </tr>
                                    <tr>
                                @endif

                            @endforeach
                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </body>
</html>
