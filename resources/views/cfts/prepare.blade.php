@extends('adminlte.default')
@section('title') Prepare Customer Formatted Timesheet @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cfts.store'), 'method' => 'post' ,'class'=>'mt-3', 'id' => 'create_cfts_frm'])}}
            <input type="hidden" name="start_date_q" id="start_date_q" value="{{$start_date}}" />
            <input type="hidden" name="template_q" id="template_q" value="{{$template}}" />
            @if(count($timesheets))
                @foreach($timesheets as $timesheet)
                    {{ Form::hidden('timesheets[]', $timesheet) }}
                @endforeach
            @endif
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark">Generating CFTS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Timesheet Header Description:</th>
                        <td>
                            {{Form::text('additional_text', old('invoice_ref'),['class'=>'form-control form-control-sm'. ($errors->has('additional_text') ? ' is-invalid' : ''),'placeholder'=>'Timesheet Header Description'])}}
                            @foreach($errors->get('additional_text') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Start Date:</th>
                        <td>
                            {{Form::text('start_date',$start_date,['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Template:</th>
                        <td>{{Form::select('template_id', $cfts_template_drop_down, $template,['class'=>'form-control form-control-sm'. ($errors->has('template_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('template_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Signature:</th>
                        <td>
                            {{Form::select('signature', [0 => 'Please Select', 1 => 'External', 2 => 'Digisign'],old('signature'),['class'=>'form-control form-control-sm datepicker'. ($errors->has('signature') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('signature') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Generate CFTS</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-danger btn-sm">Cancel</a></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection