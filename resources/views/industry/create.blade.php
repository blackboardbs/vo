@extends('adminlte.default')

@section('title') Add Industry Experience @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('industry.store',['cvid' => $cv]), 'method' => 'post','class'=>'mt-3','files'=>true])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Industry:</th>
                    <td>{{Form::select('industry',$industry_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('industry') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('industry') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status:</th>
                    <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection