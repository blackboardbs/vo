@extends('adminlte.default')

@section('title') View Industry Experience @endsection

@section('header')
    <div class="container container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container">
        <hr />
        <div class="table-responsive">
            @foreach($iexperience as $result)
                <table class="table table-bordered table-sm">
                    <tbody>
                    <tr>
                        <th>Industry:</th>
                        <td>{{$result->industry->description}}</td>
                        <th>Status:</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@endsection
