@extends('adminlte.default')

@section('title') Edit Qualification @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            @foreach($qualification as $result)
                {{Form::open(['url' => route('qualification.update',['qualificationid' => $result,'cvid' => $cv, 'res_id' => $result->res_id]), 'method' => 'post','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'saveForm'])}}
                <table class="table table-bordered table-sm">
                    <tbody>
                    <tr>
                        <th>Year: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3"><select name="year_complete" class="form-control form-control-sm ">
                                <option value="{{$result->year_complete}}">{{$result->year_complete}}</option>
                                @for($i = date('Y'); $i>1970; --$i)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                            @foreach($errors->get('year_complete') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Qualification: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::text('qualification',$result->qualification,['class'=>'form-control form-control-sm'. ($errors->has('qualification') ? ' is-invalid' : ''),'placeholder'=>'Qualification'])}}
                            @foreach($errors->get('qualification') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Institution:</th>
                        <td>{{Form::text('institution',$result->institution,['class'=>'form-control form-control-sm'. ($errors->has('institution') ? ' is-invalid' : ''),'placeholder'=>'Institution'])}}
                            @foreach($errors->get('institution') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                    </tr>
                    <th>Subjects:</th>
                    <td colspan="3">{{ Form::textarea('subjects', $result->subjects, ['size' => '100x5', 'class'=>'form-control form-control-sm'. ($errors->has('subjects') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('subjects') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <tr>
                        <th>Row Order:</th>
                        <td>
                            {{Form::select('row_order',$row_order, $result->row_order, ['class'=>'form-control form-control-sm '. ($errors->has('row_order') ? ' is-invalid' : '')])}}
                            {{--<select name="row_order" class="form-control form-control-sm ">
                                <option value="{{$result->row_order}}">{{$result->row_order}}</option>
                                @for($i = 0; $i<30; ++$i)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>--}}
                            @foreach($errors->get('row_order') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status_id',$status_dropdown,$result->status_id,['class'=>'datepicker form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                {{Form::close()}}
            @endforeach()
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection