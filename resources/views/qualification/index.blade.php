@extends('adminlte.default')

@section('title') Customer @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('customer.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Customer</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('customer_name','Customer Name')</th>
                    <th>@sortablelink('email')</th>
                    <th>@sortablelink('phone')</th>
                    <th>@sortablelink('cell')</th>
                    <th>@sortablelink('status.description','status')</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customer as $result)
                    <tr>
                        <td><a href="{{route('customer.show',$result)}}">{{$result->customer_name}}</a></td>
                        <td>{{$result->email}}</td>
                        <td>{{$result->phone}}</td>
                        <td>{{$result->cell}}</td>
                        <td>{{$result->statusd->description}}</td>
                        <td>
                            <a href="{{route('customer.edit',$result)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['customer.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No companies match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $customer->links() }}
        </div>
    </div>
@endsection
