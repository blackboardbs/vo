@extends('adminlte.default')

@section('title') View Qualification @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('qualification.edit', ['qualificationid' => $qualification->id, 'cvid' => $cvid,'res_id' => $qualification->res_id])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Qualification</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Year:</th>
                    <td colspan="3">{{$qualification->year_complete}}</td>
                </tr>
                <tr>
                    <th>Qualification:</th>
                    <td>{{$qualification->qualification}}</td>
                    <th>Institution:</th>
                    <td>{{$qualification->institution}}</td>
                </tr>
                <tr>
                </tr>
                <th>Subjects:</th>
                <td colspan="3">{{$qualification->subjects}}</td>
                <tr>
                    <th>Row Order:</th>
                    <td>{{$qualification->row_order}}</td>
                    <th>Status:</th>
                    <td>{{$qualification->status->description}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
