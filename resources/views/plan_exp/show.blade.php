@extends('adminlte.default')

@section('title') View Planned Expense @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('plan_exp.edit', $plan_exp)}}" class="btn btn-success float-right ml-1">Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="table-responsive">
        <table class="table table-bordered table-sm mt-3">
            <tbody>
            <tr>
                <th>Account:</th>
                <td colspan="3">{{$plan_exp->account?->description}}</td>
            </tr>
            <tr>
                <th>Account Element:</th>
                <td colspan="3">{{$plan_exp->account_element?->description}}</td>
            </tr>
            <tr>
                <th>Description:</th>
                <td colspan="3">{{$plan_exp->description}}</td>
            </tr>
            <tr>
                <th>Company:</th>
                <td colspan="3">{{$plan_exp->company?->company_name}}</td>
            </tr>
            <tr>
                <th>Due Date:</th>
                <td>{{$plan_exp->plan_exp_date}}</td>
                <th>Amount:</th>
                <td>{{$plan_exp->plan_exp_value}}</td>
            </tr>
            <tr>
                <th>Status:</th>
                <td>{{$plan_exp->status_desc?->description}}</td>
                <th></th>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    </div>
@endsection
