@extends('adminlte.default')

@section('title') Edit Planned Expense @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('plan_exp.update',$plan_exp), 'method' => 'put','class'=>'mt-3','id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Account: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('account_id',$account_dropdown,$plan_exp->account_id,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_account'])}}
                        @foreach($errors->get('account_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Account Element: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::hidden('haccount_element_id',$plan_exp->account_element_id,array('id'=>'haccount_element_id'))}}
                        {{Form::select('account_element_id',$account_element_dropdown->keyBy('id')->map(fn($ae) => $ae->description),$plan_exp->account_element_id,['class'=>'form-control form-control-sm '.($errors->has('account_element_id') ? ' is-invalid' : ''),'id'=>'account_element_id'])}}

                        @foreach($errors->get('account_element_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Description: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('description',$plan_exp->description,['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Description'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Company: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::select('company_id',$company_dropdown,$plan_exp->company_id,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_company'])}}
                        @foreach($errors->get('company_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Due Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('plan_exp_date',$plan_exp->plan_exp_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('plan_exp_date') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('plan_exp_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Amount: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('plan_exp_value',$plan_exp->plan_exp_value,['class'=>'form-control form-control-sm'. ($errors->has('plan_exp_value') ? ' is-invalid' : ''),'placeholder'=>'Amount'])}}
                        @foreach($errors->get('plan_exp_value') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status',$status_dropdown,$plan_exp->status,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        $('#sidebar_process_account').on('change',function() {
            var account = $('#sidebar_process_account').val();
            if(account) {
                $.ajax({
                    url: '/api/plan_exp/account_element/'+account,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('#account_element_id').html('<option value="">Select Account Element</option>');


                        $.each(data, function(key, value){

                            $('#account_element_id').append('<option value="'+ key +'">' + value + '</option>');

                        });


                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                    }
                });
            } else {
                $('select[name="account_element_id"]').empty();
            }
        });
        });

        $('document').ready(function() {
            var account = $('#sidebar_process_account').val();
            if(account) {
                $.ajax({
                    url: '/api/plan_exp/account_element/'+account,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {

                        $('#account_element_id').empty();


                        $.each(data, function(key, value){

                            $('#account_element_id').append('<option value="'+ key +'">' + value + '</option>');

                        });


                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                        $('#account_element_id').val($('#haccount_element_id').val());

                    }
                });
            } else {
                $('select[name="account_element_id"]').empty();
            }
        });



    </script>
@endsection
