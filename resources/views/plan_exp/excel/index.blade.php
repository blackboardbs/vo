<table class="table table-bordered table-sm table-hover">
    <thead class="btn-dark">
    <tr>
        <th>#</th>
        <th>Date</th>
        <th>Account</th>
        <th>Account Element</th>
        <th>Description</th>
        <th>Value</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @forelse($expenses as $expense)
        <tr>
            <td>{{$expense->id}}</td>
            <td>{{$expense->plan_exp_date}}</td>
            <td>{{$expense->account?->description}}</td>
            <td>{{$expense->account_element?->description}}</td>
            <td>{{$expense->description}}</td>
            <td>{{number_format($expense->plan_exp_value,2,'.',',')}}</td>
            <td>{{$expense->status_desc?->description}}</td>
        </tr>
    @empty
        <tr>
            <td colspan="100%" class="text-center">No referrers match those criteria.</td>
        </tr>
    @endforelse
    <tr>
        <th colspan="4"></th>
        <th>TOTAL</th>
        <th>{{number_format($expenses->sum('plan_exp_value'),2,'.',',')}}</th>
        <th colspan="2"></th>
    </tr>
    </tbody>
</table>