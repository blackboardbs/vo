@extends('adminlte.default')

@section('title') Planned Expenses @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
            @if($can_create)
                <a href="{{route('plan_exp.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Expense</a>
                <button type="button" class="btn btn-success float-right mr-2" data-toggle="modal" data-target="#copyPlannedExpense" id="trigger-copy">
                    <i class="fa fa-copy"></i> Copy Expense
                </button>
            @endif
            <x-export route="plan_exp.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3" action="{{route('plan_exp.index')}}" id="searchform" name="searchform" method="get" autocomplete="off">
        <div class="col-sm-3 col-sm" style="max-width:14%;">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('company_id', $company_dropdown, request()->company_id, ['class' => 'form-control w-100 search', 'id' => 'company_id'])}}
                    <span>Company</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm" style="max-width:14%;">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('account_id',$account_dropdown,request()->account_id,['class'=>'form-control w-100 search', 'id' => 'account_id'])}}
                    <span>Account</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm" style="max-width:14%;">
            <div class="form-group input-group">
                <label class="has-float-label">
            {{Form::select('account_element_id',[],(isset(request()->account_element_id) ? request()->account_element_id : '0'),['class'=>'form-control w-100 search', 'id' => 'account_element_id'])}}
            <span>Account Element</span>
                </label>
            </div>
        </div>
        <div class="col-sm-3 col-sm" style="max-width:16%;">
            <div class="form-group input-group">
                <label class="has-float-label">
                    <input type="text" name="q" class="form-control w-100 search" value="{{request()->q}}">
                    <span>Matching</span>
                </label>
            </div>
        </div>
        <div class="col-sm-4 col-sm" style="max-width:14%;">
            <div class="form-group input-group">
                <label class="has-float-label">
                    <input type="text" id="from" name="from" class="datepicker form-control w-100" value="{{request()->from}}">

            <span>Date From</span>
                </label>
            </div>
        </div>
        <div class="col-sm-4 col-sm" style="max-width:14%;">
            <div class="form-group input-group">
                <label class="has-float-label">
                    <input type="text" id="to" name="to" class="datepicker form-control w-100 search" value="{{request()->to}}">
            <span>Date To</span>
                </label>
            </div>
        </div>
        <div class="col-sm-2 col-sm" style="max-width:13%;">
            <button type="reset" value="Reset" onclick="clearFilters()" class="btn btn-info w-100">Clear Filters</button>
        </div>
        <table class="table table-bordered table-striped table-sm table-hover" style="margin-top: 15px;">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('account','Account')</th>
                <th><span id="period1"></span></th>
                <th><span id="period2"></span></th>
                <th><span id="period3"></span></th>
                <th><span id="period4"></span></th>
                <th><span id="period5"></span></th>
                <th><span id="period6"></span></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th colspan="7">Income</th>
                </tr>
                @forelse($income_expenses as $plan_exp)
                    <tr>
                        <td>{{$plan_exp->description}}</td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 1)">{{number_format($plan_exp->period_01,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 2)">{{number_format($plan_exp->period_02,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 3)">{{number_format($plan_exp->period_03,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 4)">{{number_format($plan_exp->period_04,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 5)">{{number_format($plan_exp->period_05,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 6)">{{number_format($plan_exp->period_06,2,'.',',')}}</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No income match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th>TOTAL</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_01'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_02'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_03'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_04'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_05'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($income_expenses->sum('period_06'),2,'.',',')}}</th>
                </tr>
                <tr style="height: 10px;"></tr>
                <tr>
                    <th colspan="7">Expenses</th>
                </tr>
                @forelse($expense_expenses as $plan_exp)
                    <tr>
                        <td>{{$plan_exp->description}}</td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 1)">{{number_format($plan_exp->period_01,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 2)">{{number_format($plan_exp->period_02,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 3)">{{number_format($plan_exp->period_03,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 4)">{{number_format($plan_exp->period_04,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 5)">{{number_format($plan_exp->period_05,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 6)">{{number_format($plan_exp->period_06,2,'.',',')}}</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No expense match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th>TOTAL</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_01'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_02'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_03'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_04'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_05'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($expense_expenses->sum('period_06'),2,'.',',')}}</th>
                </tr>
                <tr style="height: 10px;"></tr>
                <tr>
                    <th colspan="7">Assets</th>
                </tr>
                @forelse($asset_expenses as $plan_exp)
                    <tr>
                        <td>{{$plan_exp->description}}</td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 1)">{{number_format($plan_exp->period_01,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 2)">{{number_format($plan_exp->period_02,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 3)">{{number_format($plan_exp->period_03,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 4)">{{number_format($plan_exp->period_04,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 5)">{{number_format($plan_exp->period_05,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 6)">{{number_format($plan_exp->period_06,2,'.',',')}}</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No asset match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th>TOTAL</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_01'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_02'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_03'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_04'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_05'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($asset_expenses->sum('period_06'),2,'.',',')}}</th>
                </tr>
                <tr style="height: 10px;"></tr>
                <tr>
                    <th colspan="7">Liabilities</th>
                </tr>
                @forelse($liability_expenses as $plan_exp)
                    <tr>
                        <td>{{$plan_exp->description}}</td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 1)">{{number_format($plan_exp->period_01,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 2)">{{number_format($plan_exp->period_02,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 3)">{{number_format($plan_exp->period_03,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 4)">{{number_format($plan_exp->period_04,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 5)">{{number_format($plan_exp->period_05,2,'.',',')}}</a></td>
                        <td class="text-right"><a type="button" href="#" onclick="expFilter({{ $plan_exp->company_id }}, {{ $plan_exp->account_id }}, 6)">{{number_format($plan_exp->period_06,2,'.',',')}}</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No liability match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th>TOTAL</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_01'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_02'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_03'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_04'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_05'),2,'.',',')}}</th>
                    <th class="text-right">{{number_format($liability_expenses->sum('period_06'),2,'.',',')}}</th>
                </tr>
            </tbody>
        </table>
</form>
<hr>

<div class="table-responsive">
    
    <table class="table table-bordered table-sm table-hover">
        <thead class="btn-dark">
        <tr>
            <th>@sortablelink('id','#')</th>
            <th>@sortablelink('plan_exp_date','Date')</th>
            <th>@sortablelink('account.description','Account')</th>
            <th>@sortablelink('account_element.description','Account Element')</th>
            <th>@sortablelink('description','Description')</th>
            <th class="text-right">@sortablelink('plan_exp_value','Value')</th>
            <th>@sortablelink('status_desc.description','Status')</th>
            @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                <th>Action</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @forelse($expenses as $expense)
            <tr>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->id}}</a></td>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->plan_exp_date}}</a></td>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->account->description}}</a></td>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->account_element->description}}</a></td>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->description}}</a></td>
                <td class="text-right"><a href="{{route('plan_exp.show',$expense)}}">{{number_format($expense->plan_exp_value,2,'.',',')}}</a></td>
                <td><a href="{{route('plan_exp.show',$expense)}}">{{$expense->status_desc->description}}</a></td>
                @if(Auth::user()->hasAnyRole(['admin', 'admin_manager']))
                <td class="last">
                    <a href="{{route('plan_exp.edit',$expense)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                    {{ Form::open(['method' => 'DELETE','route' => ['plan_exp.destroy', $expense->id],'style'=>'display:inline','class'=>'delete']) }}
                    <button type="submit" class="btn btn-danger btn-sm ml-1 mr-1"><i class="fas fa-trash"></i></button>
                    {{ Form::close() }}
                </td>
                @endif
            </tr>
        @empty
            <tr>
                <td colspan="100%" class="text-center">No planned expenses match those criteria.</td>
            </tr>
        @endforelse
        <tr>
            <th colspan="4"></th>
            <th>TOTAL</th>
            <th class="text-right">{{number_format($expenses->sum('plan_exp_value'),2,'.',',')}}</th>
            <th colspan="2"></th>
        </tr>
        </tbody>
    </table>
<div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
    <table class="tabel tabel-borderless" style="margin: 0 auto">
        <tr>
            <td style="vertical-align: center;">
                Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $expenses->firstItem() }} - {{ $expenses->lastItem() }} of {{ $expenses->total() }}
            </td>
            <td>
                {{ $expenses->appends(request()->except('page'))->links() }}
            </td>
        </tr>
    </table>
</div>
</div>
        <div class="modal fade" id="copyPlannedExpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <form action="{{route('plan_exp.copy')}}" method="POST" id="copy-planned">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Select Date Range To Copy</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group input-group">
                                        <label class="has-float-label">
                                            {{Form::select('start_date', $source_range, null, ['class' => 'form-control '. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder' => 'All'])}}
                                            @foreach($errors->get('start_date') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <span>Source Period</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group input-group">
                                        <label class="has-float-label">
                                            {{Form::select('end_date', $target_range, null, ['class' => 'form-control '. ($errors->has('end_date') ? ' is-invalid' : ''), 'placeholder' => 'All'])}}
                                            @foreach($errors->get('end_date') as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                            <span>Target Period</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-between">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-dark"><i class="fa fa-copy"></i> Copy</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
@endsection
@section('extra-js')
<script>
$(document).on('ready', function (){
    const url = new URL(window.location.toLocaleString());
    let account_element_dropdown = @json($account_element_dropdown);
    let dropdown = [];
    let options = `<option value="" selected="selected">All</option>`;

    account_element_dropdown.sort((a, b) => {
        let textA = a.description.toUpperCase();
        let textB = b.description.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });

    dropdown = url.searchParams.get('account_id') && (url.searchParams.get('account_id') != 0)
        ? account_element_dropdown.filter(item => item.account_id == url.searchParams.get('account_id'))
        : account_element_dropdown;

    dropdown.forEach(v => options += `<option value='${v.id}'>${v.description}</option>`)

    $("#account_element_id").html(options).val(url.searchParams.get('account_element_id'))
    $("#account_element_id").val(url.searchParams.get('account_element_id'))


    $(function (){
        @if($errors->count())
            $('#trigger-copy').click()
        @endif
    })

    var currentDate = new Date();
    var currentMonth = currentDate.getMonth();
    var monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    currentMonth--;
    var currentMonthElement = document.getElementById("period1");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;

    currentMonth++;
    var currentMonthElement = document.getElementById("period2");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;
    
    currentMonth++;
    var currentMonthElement = document.getElementById("period3");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;

    currentMonth++;
    var currentMonthElement = document.getElementById("period4");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;

    currentMonth++;
    var currentMonthElement = document.getElementById("period5");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;

    currentMonth++;
    var currentMonthElement = document.getElementById("period6");
    var currentMonthName = monthNames[currentMonth];
    currentMonthElement.textContent = currentMonthName;
})

function clearFilters(){
    window.location = "/plan_exp";
}

function expFilter(company_id, account_id, period) {
    $('#company_id').val(company_id);
    $('#account_id').val(account_id);
    if (period === 1) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth();
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth());
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth()+1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    if (period === 2) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth() + 1;
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth() + 1);
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth() + 1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    if (period === 3) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth() + 2;
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth() + 2);
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth() + 1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    if (period === 4) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth() + 3;
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth() + 3);
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth() + 1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    if (period === 5) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth() + 4;
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth() + 4);
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth() + 1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    if (period === 6) {
        let currentDateFrom = new Date();
        currentDateFrom.setDate(1);
        let yearFrom = currentDateFrom.getFullYear();
        let monthFrom = currentDateFrom.getMonth() + 5;
        let dayFrom = currentDateFrom.getDate();
        var currentFrom = yearFrom + '-' + (monthFrom < 10 ? '0' : '') + monthFrom + '-' + (dayFrom < 10 ? '0' : '') + dayFrom;

        let currentDateTo = new Date();
        currentDateTo.setMonth(currentDateTo.getMonth() + 5);
        currentDateTo.setDate(0);
        let yearTo = currentDateTo.getFullYear();
        let monthTo = currentDateTo.getMonth() + 1;
        let dayTo = currentDateTo.getDate();
        var currentTo = yearTo + '-' + (monthTo < 10 ? '0' : '') + monthTo + '-' + (dayTo < 10 ? '0' : '') + dayTo;
        
        $('#from').val(currentFrom);
        $('#to').val(currentTo);
    }
    $('#searchform').submit();
}
</script>
{{-- <script>
    document.addEventListener("DOMContentLoaded", function () {
        document.querySelectorAll('.filter-btn').forEach(item => {
            item.addEventListener('click', event => {
                event.preventDefault();
                let companyId = item.getAttribute('data-company');
                let period = item.getAttribute('data-period');
                console.log("Company ID:", companyId);
                console.log("Period:", period);
                
                // Construct the full URL manually
                let url = "{{ route('plan_exp.index') }}";
                url += '?plan_exp_company=' + companyId + '&period=' + period;
                console.log("URL:", url);
                
                // Redirect to the constructed URL
                window.location.href = url;
            });
        });
    });
</script> --}}



@endsection
@section('extra-css')
    <style>
        .modal.show .modal-dialog {
            max-width: 25%!important;
        }
    </style>
@endsection