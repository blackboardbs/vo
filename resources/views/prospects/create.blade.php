@extends('adminlte.default')

@section('title') Add Prospect @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createProspect')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('prospects.store'), 'method' => 'post','class'=>'mt-3','id'=>'createProspect'])}}
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Prospect <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('prospect_name',old('prospect_name'),['class'=>'form-control form-control-sm'. ($errors->has('prospect_name') ? ' is-invalid' : ''),'placeholder'=>'Prospect Name'])}}
                        @foreach($errors->get('prospect_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Solution</th>
                    <td>{{Form::text('solution',old('solution'),['class'=>'form-control form-control-sm'. ($errors->has('solution') ? ' is-invalid' : ''),'placeholder'=>'Solution'])}}
                        @foreach($errors->get('solution') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Scope</th>
                    <td colspan="3">{{Form::text('scope',old('scope'),['class'=>'form-control form-control-sm'. ($errors->has('scope') ? ' is-invalid' : ''),'placeholder'=>'Scope'])}}
                        @foreach($errors->get('scope') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Hours</th>
                    <td>{{Form::text('hours',old('hours'),['class'=>'form-control form-control-sm'. ($errors->has('hours') ? ' is-invalid' : ''),'placeholder'=>'Hours'])}}
                        @foreach($errors->get('hours') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Value <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('value',old('value'),['class'=>'form-control form-control-sm'. ($errors->has('value') ? ' is-invalid' : ''),'placeholder'=>'Value'])}}
                        @foreach($errors->get('value') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Resource</th>
                    <td>{{Form::text('resources',old('resources'),['class'=>'form-control form-control-sm'. ($errors->has('resources') ? ' is-invalid' : ''),'placeholder'=>'Resources'])}}
                        @foreach($errors->get('resources') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Chance <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('chance', [0 => 0, 10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 60 => 60, 70 => 70, 80 => 80, 90 => 90, 100 => 100], old('chance'),['class'=>'form-control form-control-sm'. ($errors->has('chance') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('chance') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Decision Date</th>
                    <td>{{Form::text('decision_date',old('decision_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('decision_date') ? ' is-invalid' : ''),'placeholder'=>'Decision Date'])}}
                        @foreach($errors->get('decision_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Est Start Date</th>
                    <td>{{Form::text('est_start_date',old('est_start_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('est_start_date') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('est_start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Partners</th>
                    <td>
                        {{Form::text('partners',old('partners'),['class'=>'form-control form-control-sm'. ($errors->has('partners') ? ' is-invalid' : ''),'placeholder'=>'Partners'])}}
                        @foreach($errors->get('partners') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Contact</th>
                    <td>
                        {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact'])}}
                        @foreach($errors->get('contact') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td colspan="3">
                        {{Form::textarea('note',old('note'),['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : ''),'placeholder'=>'Note'])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('status_id',$sidebar_process_statuses,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'sidebar_process_statuses'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Account Manager</th>
                    <td>
                        {{Form::select('account_manager_id', $account_manager_drop_down ,null ,['class'=>'form-control form-control-sm '. ($errors->has('account_manager_id') ? ' is-invalid' : ''),'id'=>'account_manager_id'])}}
                        @foreach($errors->get('account_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection