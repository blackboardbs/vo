@extends('adminlte.default')

@section('title') View Prospect @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('prospects.edit',$prospect[0]->id)}}" class="btn btn-success float-right" style="margin-left:5px;">Edit Prospects</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($prospect as $result)
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Prospect</th>
                    <td>{{$result->prospect_name}}</td>
                    <th>Solution</th>
                    <td>{{$result->solution}}</td>
                </tr>
                <tr>
                    <th>Scope</th>
                    <td colspan="3">{{$result->scope}}</td>
                </tr>
                <tr>
                    <th>Hours</th>
                    <td>{{$result->hours}}</td>
                    <th>Value</th>
                    <td>{{$result->value}}</td>
                </tr>
                <tr>
                    <th>Resource</th>
                    <td>{{$result->resources}}</td>
                    <th>Chance</th>
                    <td>{{($result->chance < 10) ? ($result->chance * 10).'%' : $result->chance.'%'}}</td>
                </tr>
                <tr>
                    <th>Decision Date</th>
                    <td>{{$result->decision_date}}</td>
                    <th>Est Start Date</th>
                    <td>{{$result->est_start_date}}</td>
                </tr>
                <tr>
                    <th>Partners</th>
                    <td>{{$result->partners}}</td>
                    <th>Contact</th>
                    <td>{{$result->contact}}</td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td colspan="3">{{$result->note}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$result->status->description}}</td>
                    <th>Account Manager</th>
                    <td>{{$result->account_manager?->name()}}</td>
                </tr>
            </table>
            @endforeach
            <blackboard-prospect-comments
                    {{--:users="{{$resource_dropdown}}"--}}
                    prospect-id="{{$prospect->first()?->id}}"
            ></blackboard-prospect-comments>
        </div>
    </div>
@endsection
