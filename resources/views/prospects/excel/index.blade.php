<table>
    <thead>
    <tr>
        <th>Prospect Name</th>
        <th>Account Manager</th>
        <th>Solution</th>
        <th>Decision Date</th>
        <th>Chance</th>
        <th>Value</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($prospects as $prospect)
        <tr>
            <td>{{$prospect->prospect_name}}</td>
            <td>{{$prospect->account_manager?->name()}}</td>
            <td>{{$prospect->solution}}</td>
            <td>{{$prospect->decision_date}}</td>
            <td>{{$prospect->chance}}%</td>
            <td>{{round($prospect->value, 2)}}</td>
            <td>{{$prospect->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>