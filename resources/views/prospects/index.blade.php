@extends('adminlte.default')


@section('content')
            <sales-pipeline :status_drop_down="{{$status_drop_down}}" :account_manager_drop_down="{{$account_manager_drop_down}}" :system_drop_down="{{ $systems }}" :customer_drop_down="{{$customers}}" :lead_drop_down="{{$leads}}" :industry_drop_down="{{$industries}}" :contact_drop_down="{{$contacts}}"></sales-pipeline>
@endsection