@extends('adminlte.default')
@section('title') Show Warning Maintenance @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Edit Warning Maintenance</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 25%'>Function Area:</th>
                    <td style='width: 25%'>
                        {{Form::text('function_area', $warning_maintenance->function_area,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('function_area') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('function_area') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 25%'>Type:</th>
                    <td style='width: 25%'>
                        {{Form::select('type', [1 => 'Cron', 2 => 'Hardcoded / Manual'], $warning_maintenance->type,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('type') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Event:</th>
                    <td>
                        {{Form::textarea('event', $warning_maintenance->event,['size' => '30x5', 'class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('event') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('event') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Trigger:</th>
                    <td>
                        @if($warning_maintenance->type == 1)
                            {{Form::select('trigger', $trigger_drop_down, explode('|', $warning_maintenance->trigger),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('trigger') ? ' is-invalid' : ''), 'multiple' => 'mutiple', 'disabled' => 'disabled'])}}
                        @else
                            {{Form::textarea('trigger', $warning_maintenance->trigger,['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('trigger') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @endif
                        @foreach($errors->get('trigger') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Time:</th>
                    <td>
                        {{Form::select('time', $time_drop_down, explode('|', $warning_maintenance->time),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('time') ? ' is-invalid' : ''), 'placeholder' => 'Please select...', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('time') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Recipient Role:</th>
                    <td>
                        {{Form::select('recipient_role', $recipient_role_drop_down, explode('|', $warning_maintenance->recipient_role),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('recipient_role') ? ' is-invalid' : ''), 'multiple' => 'mutiple', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('recipient_role') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Notification:</th>
                    <td>
                        {{Form::select('notifications', $notification_drop_down, explode('|', $warning_maintenance->notifications),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('notifications') ? ' is-invalid' : ''), 'multiple' => 'mutiple', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('notifications') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $status_drop_down, explode('|', $warning_maintenance->status_id),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('status_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('.chosen-container').css('width', '100%');
        });
    </script>
@endsection