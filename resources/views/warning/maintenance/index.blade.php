@extends('adminlte.default')
@section('title') Warnings Maintenance @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('type', [1 => 'Cron', 2 => 'Hardcoded / Manual'], null, ['class'=>'form-control search ', 'style'=>'width: 100%;', 'placeholder' => 'Please select...'])}}
                        <span>Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <a href="{{route('warning_maintenance.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('id', 'Ref')</th>
                    <th>@sortablelink('function_area', 'Function Area')</th>
                    <th>@sortablelink('type', 'Type')</th>
                    <th>@sortablelink('event', 'Event')</th>
                    <th>@sortablelink('trigger', 'Trigger')</th>
                    <th>@sortablelink('time', 'Time')</th>
                    <th>@sortablelink('recipient_role', 'Recipient Role')</th>
                    <th>@sortablelink('notifications', 'Notifications')</th>
                    <th>@sortablelink('status_id', 'Status')</th>
                    @if($can_update)
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($warning_maintenances as $warning_maintenance)
                    <tr>
                        <td><a href="{{route('warning_maintenance.show',$warning_maintenance)}}">{{$warning_maintenance->id}}</a></td>
                        <td><a href="{{route('warning_maintenance.show',$warning_maintenance)}}">{{$warning_maintenance->function_area}}</a></td>
                        <td>{{$warning_maintenance->type == 1 ? 'Cron' : 'Hardcoded / Manual'}}</td>
                        <td>{{$warning_maintenance->event}}</td>
                        <td style="width: 15%;">
                            @if($warning_maintenance->type == 1)

                                @php
                                    $triggers = [];
                                    if(isset($warning_maintenance->trigger)){
                                        $triggers = explode('|', $warning_maintenance->trigger);
                                    }
                                @endphp
                                @foreach($triggers as $trigger)
                                    {{$trigger_values[$trigger]}}
                                    <br/>
                                @endforeach

                            @else
                                {{$warning_maintenance->trigger}}
                            @endif
                        </td>
                        <td>{{ isset($warning_maintenance->time) ? ($warning_maintenance->time < 10 ? '0'.$warning_maintenance->time.':00' : $warning_maintenance->time.':00') : '-' }}</td>
                        <td>
                            @php
                                $recipient_roles = explode('|', $warning_maintenance->recipient_role);
                            @endphp
                            @foreach($recipient_roles as $recipient_role)
                                {{$warning_recipient_values_values[$recipient_role]}}
                                <br/>
                            @endforeach
                        </td>
                        <td>
                            @php
                                $notifications = explode('|', $warning_maintenance->notifications);
                            @endphp
                            @foreach($notifications as $notification)
                                {{$notification_values[$notification]}}
                                <br/>
                            @endforeach
                        </td>
                        <td>{{$warning_maintenance->status?->description}}</td>
                        @if($can_update)
                            <td>
                                <a href="{{route('warning_maintenance.edit',$warning_maintenance)}}" class="btn btn-success btn-sm">Edit</a>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $warning_maintenances->firstItem() }} - {{ $warning_maintenances->lastItem() }} of {{ $warning_maintenances->total() }}
                        </td>
                        <td>
                            {{ $warning_maintenances->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection