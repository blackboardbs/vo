  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="padding-bottom: 35px !important;">
    <!-- Brand Logo -->
    <div class="{{'text-'.($config->logo_placement??'left')}}">
      <a href="{{ url('/') }}" class="brand-link">
        <img src="{!! route('company_avatar', ['q' => $config->site_logo??null, 'site_logo' => true]) !!}" id="brand" alt="{{($config?->company->company_name ?? '')}}" style="opacity: .8; width: 100%;height: auto">
      </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar blackboard-scrollbar" style="overflow-x:hidden;">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @auth
          <img src="{{route('user_avatar',['q'=>Auth::user()->avatar])}}" class="img-circle elevation-2" alt="User Image">
          @endauth
        </div>
        <div class="info">
          @auth
          <a href="{{route('profile',Auth::user()->id)}}" class="d-block">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
          @endauth
        </div>
      </div>

      <!-- Sidebar Menu -->
      @auth
      <nav class="mt-2">
        <input type="text" id="search" class="form-control form-control-sm">
        <ul class="nav search-list nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="padding-bottom: 85px;">

          @if(can_view_sidebar("Dashboard"))
            <li class="nav-item has-treeview">
              <a href="/" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview" style="display: none;">
                @if(can_view_sidebar('ActiveDashboard'))
                  <li class="nav-item">
                    <a href="{{route('dashboard.index')}}" class="nav-link {{ request()->routeIs('dashboard.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Active Dashboard</p>
                    </a>
                  </li>
                @endif

                @if(can_view_sidebar('CustomDashboard'))

                  <li class="nav-item">
                    <a href="{{route('reports.custom_dashboard')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.custom_dashboard') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Custom Dashboard</p>
                    </a>
                  </li>
                @endif
              </ul>
            </li>
          @endif
          @if(can_view_sidebar("MyWorkWeek"))
          <li class="nav-item">
            <a href="{{ route('my-work-week.index') }}" class="nav-link   {{ (\Request::is('my-work-week.index') ? 'active' : '') }}">
              <i class="nav-icon far fa-id-card"></i>
              <p>
                My Work Week
              </p>
            </a>
          </li>
          @endif
          @if(can_view_sidebar("Recent"))
          <li class="nav-item">
            <a href="{{ route('recents') }}" class="nav-link   {{ (\Request::is('recents') ? 'active' : '') }}">
              <i class="nav-icon far fa-clock"></i>
              <p>
                Recents
              </p>
            </a>
          </li>
          @endif

          @if(can_view_sidebar("Calendar"))
          <li class="nav-item  {{ (\Request::route()->getName() == 'calendar.index') ? 'active' : '' }}">
            <a href="{{ route("calendar.index") }}" class="nav-link   {{ (\Request::route()->getName() == 'calendar.index') ? 'active' : '' }}">
              <i class="nav-icon far fa-calendar"></i>
              <p>
                Calendar
              </p>
            </a>
          </li>
          @endif

          @if(can_view_sidebar("Company"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-building"></i>
              <p>
                Company
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

              @if(can_view_sidebar("CompanyProfile"))
              <li class="nav-item">
                <a href="{{route('company.index')}}" class="nav-link {{ request()->routeIs('company.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("PlannedUtilization"))
              <li class="nav-item">
                <a href="{{route('utilization.index')}}" class="nav-link {{ request()->routeIs('utilization.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Planned Utilization</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("PlannedExpense"))
              <li class="nav-item">
                <a href="{{route('plan_exp.index')}}" class="nav-link {{ request()->routeIs('plan_exp.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Planned Expenses</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("ExpenseTracking"))
              <li class="nav-item">
                <a href="{{route('exptracking.index')}}" class="nav-link {{ request()->routeIs('exptracking.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Expense Tracking</p>
                </a>
              </li>
              @endif
              @if(can_view_sidebar("MessageBoard"))
                <li class="nav-item">
                  <a href="{{route('messages.index')}}" class="nav-link {{ request()->routeIs('messages.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Message Board</p>
                  </a>
                </li>
              @endif

              @if(can_view_sidebar("BillingCycle"))
                <li class="nav-item">
                  <a href="{{route('billing.index')}}" class="nav-link {{ request()->routeIs('billing.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Billing Cycle</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("InvoiceContact"))
                  <li class="nav-item">
                    <a href="{{route('contact.index')}}" class="nav-link {{ request()->routeIs('contact.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Contacts</p>
                    </a>
                  </li>
              @endif
              @if(can_view_sidebar("Equipment"))
                <li class="nav-item">
                  <a href="{{route('assetreg.index')}}" class="nav-link {{ request()->routeIs('assetreg.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Equipment</p>
                  </a>
                </li>
              @endif
            </ul>

          </li>
          @endif

          @if(can_view_sidebar("Project"))

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-archive"></i>
                <p>
                  Project
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

                @if(can_view_sidebar("Project"))
                  <li class="nav-item">
                    <a href="{{route('project.index')}}" class="nav-link {{ request()->routeIs('project.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Project <small>(New/Edit)</small></p>
                    </a>
                  </li>
                @endif

                @if(can_view_sidebar("Assignment"))
                  <li class="nav-item">
                    <a href="{{route('assignment.index')}}" class="nav-link {{ request()->routeIs('assignment.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Assignment</p>
                    </a>
                  </li>
                @endif
                @if(can_view_sidebar("Task") && \App\Models\Task::count())
                      <li class="nav-item">
                        <a href="{{url('/kanban/'.(\App\Models\Project::latest()->first()->id??\App\Models\Timesheet::latest()->first()?->project_id).'?project_id=0&on_kanban=1')}}" class="nav-link {{ (\Request::route()->getName() == 'task.kanban') ? 'active' : '' }}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Kanban Board</p>
                        </a>
                      </li>
                @endif
                @if(can_view_sidebar("Project"))
                  <li class="nav-item">
                    <a href="{{route('approvalroles.index')}}" class="nav-link {{ request()->routeIs('approvalroles.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Approval Rules</p>
                    </a>
                  </li>
                @endif
                @if(can_view_sidebar("Timesheet"))
                  <li class="nav-item  has-treeview">
                    <a href="{{ route('timesheet.index') }}" class="nav-link {{ request()->routeIs('timesheet.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Timesheet</p>
                    </a>
                  </li>
                @endif
                <!-- @if(can_view_sidebar("CustomFormattedTimesheet"))
                  <li class="nav-item  has-treeview">
                    <a href="{{ route('cfts.index') }}" class="nav-link {{ (\Request::route()->getName() == 'cfts.index') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Custom Formatted Timesheet</p>
                    </a>
                  </li>
                @endif -->
                @if(can_view_sidebar("CostCalculator"))
                  <li class="nav-item">
                    <a href="{{ route('cost_calculator.index') }}" class="nav-link {{ request()->routeIs('cost_calculator.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Cost Calculation</p>
                    </a>
                  </li>
                @endif

                  @if(can_view_sidebar("Sprint"))
                    <li class="nav-item">
                      <a href="{{ route('sprint.index', ['status_id' => 1]) }}" class="nav-link {{ request()->routeIs('sprint.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Sprints</p>
                      </a>
                    </li>
                  @endif
                  <li class="nav-item">
                    <a href="{{ route('risk.index') }}" class="nav-link {{ request()->routeIs('risk.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Risks</p>
                    </a>
                  </li>

                @if(can_view_sidebar("SiteReport"))
                  <li class="nav-item">
                    <a href="{{ route('site_report.index') }}" class="nav-link {{ request()->routeIs('site_report.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Site Report</p>
                    </a>
                  </li>
                @endif
              </ul>
            </li>
          @endif

          @if(can_view_sidebar("Resource"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user-circle"></i>
              <p>
                Resource
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              @if(can_view_sidebar("ResourceProfile"))
              <li class="nav-item">
                <a href="{{route('resource.index')}}" class="nav-link {{ request()->routeIs('resource.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              @endif
              @if(can_view_sidebar("Commission"))
                <li class="nav-item">
                  <a href="{{route('commission.index')}}" class="nav-link {{ request()->routeIs('commission.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Commission</p>
                  </a>
                </li>
              @endif

              @if(can_view_sidebar("Anniversary"))
                <li class="nav-item">
                  <a href="{{route('anniversary.index')}}" class="nav-link {{ request()->routeIs('anniversary.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Anniversary</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("Assessment"))
                <li class="nav-item">
                  <a href="{{ route('assessment.index') }}" class="nav-link {{ request()->routeIs('assessment.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Assessment</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("Leave"))
                <li class="nav-item">
                  <a href="{{route('leave.index')}}" class="nav-link {{ request()->routeIs('leave.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Leave</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("LeaveBalance"))
                <li class="nav-item">
                  <a href="{{route('leave_balance.index')}}" class="nav-link {{ request()->routeIs('leave_balance.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Leave Balance</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("LeaveStatement"))
                <li class="nav-item">
                  <a href="{{route('leave_statement.index')}}" class="nav-link {{ request()->routeIs('leave_statement.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Leave Statement</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("Action"))
                <li class="nav-item">
                  <a href="{{route('action.index')}}" class="nav-link {{ request()->routeIs('action.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Action</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("Board"))
                <li class="nav-item">
                  <a href="{{route('board.index')}}" class="nav-link {{ request()->routeIs('board.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Board</p>
                  </a>
                </li>
              @endif

              @if(can_view_sidebar("LogBook"))
              <li class="nav-item">
                <a href="{{route('logbook.index')}}" class="nav-link {{ request()->routeIs('logbook.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Logbook</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("ResourceTask"))
              <li class="nav-item">
                <a href="{{route('resource_task.index')}}" class="nav-link {{ request()->routeIs('resource_task.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Resource Tasks</p>
                </a>
              </li>
              @endif

            </ul>
          </li>
          @endif

            @if(can_view_sidebar("Recruitment"))
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-user-plus"></i>
                  <p>
                    Recruitment
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                    @if(can_view_sidebar("Scouting"))
                    <li class="nav-item">
                      <a href="{{route('scouting.index')}}" class="nav-link {{ request()->routeIs('scouting.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Scouting</p>
                      </a>
                    </li>
                  @endif
                    @if(can_view_sidebar("CV"))
                      <li class="nav-item">
                        <a href="{{route('cv.index')}}" class="nav-link {{ request()->routeIs('cv.*') ? 'active' : '' }}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>CV</p>
                        </a>
                      </li>
                    @endif
                    @if(can_view_sidebar("JobSpec"))
                      <li class="nav-item">
                        <a href="{{route('jobspec.index')}}" class="nav-link {{ request()->routeIs('jobspec.*') ? 'active' : '' }}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Job Spec</p>
                        </a>
                      </li>
                    @endif
                    @if(can_view_sidebar("DistributionList"))
                      <li class="nav-item">
                        <a href="{{ route('distributionlist.index') }}" class="nav-link {{ request()->routeIs('distributionlist.*') ? 'active' : '' }}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Distribution List</p>
                        </a>
                      </li>
                    @endif
                </ul>
              </li>
            @endif

          @if(can_view_sidebar("Customer"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Customer
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              @if(can_view_sidebar("CustomerProfile"))
              <li class="nav-item">
                <a href="{{route('customer.index')}}" class="nav-link {{ request()->routeIs('customer.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("CustomerQuotation"))
              <li class="nav-item">
                <a href="{{route('quotation.index')}}" class="nav-link {{ request()->routeIs('quotation.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Quotation</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("Prospects"))
              <li class="nav-item">
                <a href="{{route('prospects.index')}}" class="nav-link {{ request()->routeIs('prospects.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Prospects</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("CustomerInvoice"))
              <li class="nav-item">
                <a href="{{route('customer.invoice')}}" class="nav-link {{ request()->routeIs('customer.invoice*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer Invoice</p>
                </a>
              </li>
              @endif
              @if(can_view_sidebar("CustomerStatement"))
                <li class="nav-item">
                  <a href="{{route('customerstatements.index')}}" class="nav-link {{ request()->routeIs('customerstatements.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Customer Statement</p>
                  </a>
                </li>
              @endif

              @if(can_view_sidebar("CustomerProFormaInvoice"))
              <li class="nav-item">
                <a href="{{ route('customer.proforma') }}" class="nav-link {{ request()->routeIs('customer.proforma') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer Pro-forma Invoice</p>
                </a>
              </li>
              @endif
              @if(can_view_sidebar("CustomerPreference"))
              <li class="nav-item">
                <a href="{{ route('customerpreference.index') }}" class="nav-link {{ request()->routeIs('customerpreference.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer Preference</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @endif

          @if(can_view_sidebar("Vendor"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-industry"></i>
              <p>
                Vendor
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              @if(can_view_sidebar("VendorProfile"))
              <li class="nav-item">
                <a href="{{route('vendors.index')}}" class="nav-link {{ request()->routeIs('vendors.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("VendorInvoice"))
              <li class="nav-item">
                <a href="{{ route('vendor.invoice') }}" class="nav-link {{ request()->routeIs('vendor.invoice*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
              @endif
                @if(can_view_sidebar("VendorStatement"))
                  <li class="nav-item">
                    <a href="{{ route('vendorstatements.index') }}" class="nav-link {{ request()->routeIs('vendorstatements.*') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Vendor Statement</p>
                    </a>
                  </li>
                @endif

              @if(can_view_sidebar("VendorProFormaInvoice"))
              <li class="nav-item">
                <a href="{{ url('/proforma_invoice') }}" class="nav-link {{ (request()->is('proforma_invoice') ? 'active' : '') }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pro-forma Invoice</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @endif
          @if(can_view_sidebar("UserOnboarding") || auth()->user()->hasRole('user_onboarding'))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-th-list"></i>
              <p>
                Onboarding
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
                @if(can_view_sidebar("UserOnboarding") || auth()->user()->hasRole('user_onboarding'))
                <li class="nav-item">
                  <a href="{{route('useronboarding.index')}}" class="nav-link {{ request()->routeIs('useronboarding.*') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Users</p>
                  </a>
                </li>
                @endif
              </ul>
          </li>
          @endif
          @if(can_view_sidebar("Insights"))
            <li class="nav-item">
              <a href="" class="nav-link">
                <i class="nav-icon fab fa-superpowers"></i>
                <p>Insight<i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

                @if(can_view_sidebar("ConsultantReports"))
                  <li class="nav-item">
                    <a href="{{route('reports.index')}}" class="nav-link {{ request()->routeIs('reports.index') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Consultant Reports</p>
                    </a>
                  </li>
                @endif

                @if(can_view_sidebar("CustomerReports"))
                  <li class="nav-item">
                    <a href="{{ route('reports.customer') }}" class="nav-link {{ request()->routeIs('reports.customer') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Customer Reports</p>
                    </a>
                  </li>
                @endif
                @if(can_view_sidebar("FinancialReport"))
                  <li class="nav-item">
                    <a href="{{ route('reports.financial') }}" class="nav-link {{ request()->routeIs('reports.financial') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Financial Reports</p>
                    </a>
                  </li>
                @endif

                @if(can_view_sidebar("ManagementReports"))
                  <li class="nav-item">
                    <a href="{{ route('reports.manager') }}" class="nav-link {{ request()->routeIs('reports.manager') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Management Reports</p>
                    </a>
                  </li>
                @endif

                @if(can_view_sidebar("RecruitmentReports"))
                  <li class="nav-item">
                    <a href="{{ route('reports.recruitment') }}" class="nav-link {{ request()->routeIs('reports.recruitment') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Recruitment Reports</p>
                    </a>
                  </li>
                @endif
                  @if(can_view_sidebar("TaskReport"))
                    <li class="nav-item">
                      <a href="{{ route('task.reports') }}" class="nav-link {{ request()->routeIs('task.reports') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Task Reports</p>
                      </a>
                    </li>
                  @endif
              </ul>
            </li>
          @endif

          @if(can_view_sidebar("DocumentFiles") || can_view_sidebar("DocumentTemplates") || can_view_sidebar("DocumentUploadedTemplates") || can_view_sidebar("DocumentDigisign"))
          <li class="nav-item has-treeview" style="margin-bottom:10px;">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-file-signature"></i>
              <p>
                Documents
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if(can_view_sidebar("DocumentFiles"))
              <li class="nav-item">
                <a href="{{route('document.index')}}" class="nav-link {{ request()->routeIs('document.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Files</p>
                </a>
              </li>
              @endif

              {{--@if(can_view_sidebar("DocumentTemplates"))
              <li class="nav-item">
                <a href="{{ route('template.index') }}" class="nav-link {{ request()->routeIs('template.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Templates</p>
                </a>
              </li>
              @endif--}}

              @if(can_view_sidebar("DocumentUploadedTemplates"))
              <li class="nav-item">
                <a href="{{ route('advancedtemplate.index') }}" class="nav-link {{ request()->routeIs('advancedtemplate.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                    <p>Templates</p>
                </a>
              </li>
              @endif

              @if(can_view_sidebar("DocumentDigisign"))
              <li class="nav-item">
                <a href="{{ route('digisign.index') }}" class="nav-link {{ request()->routeIs('digisign.*') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Digi Sign</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @endif

          @if(can_view_sidebar("User"))
          <li class="header" style="margin-bottom:10px;">ADMIN</li>
          @endif
          @if(Auth::user()->hasRole('admin'))
          <li class="nav-item has-treeview">
            <a href="{{ route('myaccount.index')}}" class="nav-link {{ request()->routeIs('myaccount.*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-store"></i>
              <p>
                Account
              </p>
            </a>
          </li>
          @endif
          @if(can_view_sidebar("User") || can_view_sidebar("Roles") || can_view_sidebar("Api") || can_view_sidebar("ViewPermissions"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-lock"></i>
              <p>
                Security
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if(can_view_sidebar("User"))
                <li class="nav-item has-treeview">
                  <a href="{{ route('users.index')}}" class="nav-link {{ request()->routeIs('users.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-users"></i>
                    <p>
                      Users
                    </p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("Roles"))
                <li class="nav-item">
                  <a href="{{ route('module.index') }}" class="nav-link {{ request()->routeIs('module.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-cog"></i>
                    <p>Roles</p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("ViewPermissions"))

                <li class="nav-item">
                  <a href="{{ route('viewpermission.index') }}" class="nav-link {{ request()->routeIs('viewpermission.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-cogs"></i>
                    <p>View Permissions</p>
                  </a>
                </li>
              @endif
            </ul>
          </li>
          @endif

          @if(can_view_sidebar("MasterDataManagement") || can_view_sidebar("Api") || can_view_sidebar("Configs") || can_view_sidebar("WarningMaintenance"))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-wrench"></i>
              <p>
                Setup
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if(can_view_sidebar("MasterDataManagement"))
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fa fa-database"></i>
                    <p>
                      Master Data
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{route('account.index')}}" class="nav-link {{ request()->routeIs('account.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Account</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('account_element.index')}}" class="nav-link {{ request()->routeIs('account_element.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Account Element</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('anniversarytype.index')}}" class="nav-link {{ request()->routeIs('anniversarytype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Anniversary Type</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('assessment_master.index')}}" class="nav-link {{ request()->routeIs('assessment_master.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Assessment KPI</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('assessment_master_details.index')}}" class="nav-link {{ request()->routeIs('assessment_master_details.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Assessment Master Details</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('asset_class.index')}}" class="nav-link {{ request()->routeIs('asset_class.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Asset Class</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('assignment_standard_cost.index')}}" class="nav-link {{ request()->routeIs('assignment_standard_cost.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Assignment Standard Cost</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('master_bankaccounttype.index')}}" class="nav-link {{ request()->routeIs('master_bankaccounttype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Bank Account Type</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('business_function.index')}}" class="nav-link {{ request()->routeIs('business_function.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Business Function</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('master_calendar.index')}}" class="nav-link {{ request()->routeIs('master_calendar.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Calendar</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('master_cost.index')}}" class="nav-link {{ request()->routeIs('master_cost.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Cost Center</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('contracttype.index')}}" class="nav-link {{ request()->routeIs('contracttype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Contract Type</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('master_country.index')}}" class="nav-link {{ request()->routeIs('master_country.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Country</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('cv_company.index')}}" class="nav-link {{ request()->routeIs('cv_company.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>CV Company</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('customise_dashboard.index')}}" class="nav-link {{ request()->routeIs('customise_dashboard.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Dashboard Components</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('master_dashboard.index')}}" class="nav-link {{ request()->routeIs('master_dashboard.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Default Dashboard</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('default_favs.index')}}" class="nav-link {{ request()->routeIs('master_dashboard.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Default Favourites</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('expense_type.index')}}" class="nav-link {{ request()->routeIs('expense_type.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Expense Type</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('deliverytype.index')}}" class="nav-link {{ request()->routeIs('deliverytype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Task Delivery Type</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('favourites.index')}}" class="nav-link {{ request()->routeIs('favourites.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Favourites</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('industry.index')}}" class="nav-link {{ request()->routeIs('industry.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Industry</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('profession.index')}}" class="nav-link {{ request()->routeIs('profession.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Profession</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('speciality.index')}}" class="nav-link {{ request()->routeIs('speciality.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Speciality</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('sprintstatus.index')}}" class="nav-link {{ request()->routeIs('sprintstatus.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Sprint Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('leave_type.index')}}" class="nav-link {{ request()->routeIs('leave_type.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Leave Type</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('marital_status.index')}}" class="nav-link {{ request()->routeIs('marital_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Marital Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('medical_master.index')}}" class="nav-link {{ request()->routeIs('medical_master.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Medical Certificate Type</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('paymentbase.index')}}" class="nav-link {{ request()->routeIs('paymentbase.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Payment Base</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('paymenttype.index')}}" class="nav-link {{ request()->routeIs('paymenttype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Payment Type</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('process_interview.index')}}" class="nav-link {{ request()->routeIs('process_interview.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Process Interview</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('process_status.index')}}" class="nav-link {{ request()->routeIs('process_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Process Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('prospectstatus.index')}}" class="nav-link {{ request()->routeIs('prospectstatus.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Prospect Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('project_terms.index')}}" class="nav-link {{ request()->routeIs('project_terms.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Project Terms</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('quotation_terms.index')}}" class="nav-link {{ request()->routeIs('quotation_terms.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Quotation Terms</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('relationship.index')}}" class="nav-link {{ request()->routeIs('relationship.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Relationship</p>
                      </a>
                    </li>
                    <li class="nav-item ">
                      <a href="{{route('resource_level.index')}}" class="nav-link {{ request()->routeIs('resource_level.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Resource Level</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{route('resource_position.index')}}" class="nav-link {{ request()->routeIs('resource_position.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Resource Position</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('skill_level.index')}}" class="nav-link {{ request()->routeIs('skill_level.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Skill Level</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('solution.index')}}" class="nav-link {{ request()->routeIs('solution.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Solution</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('system.index')}}" class="nav-link {{ request()->routeIs('system.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Skill</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('onboardingstatus.index')}}" class="nav-link {{ request()->routeIs('onboardingstatus.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Onboarding Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('tasktags.index')}}" class="nav-link {{ request()->routeIs('tasktags.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Task Tags</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('title.index')}}" class="nav-link {{ request()->routeIs('title.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Title</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('templatestyles.index')}}" class="nav-link {{ request()->routeIs('templatestyles.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Template Styles</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('scoutingrole.index')}}" class="nav-link {{ request()->routeIs('scoutingrole.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Main Role</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('usertype.index')}}" class="nav-link {{ request()->routeIs('usertype.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>User Type</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('vat_rate.index')}}" class="nav-link {{ request()->routeIs('vat_rate.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Vat Rate</p>
                      </a>
                    </li>
                  </ul>
                </li>
              @endif

              @if(Auth::user()->email == 'support@consulteaze.com' || Auth::user()->email == 'stefan@blackboardbi.com')
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fa fa-database"></i>
                    <p>
                      Admin Master Data
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{route('account_type.index')}}" class="nav-link {{ request()->routeIs('account_type.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Account Type</p>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="{{route('master_country.index')}}" class="nav-link {{ request()->routeIs('master_country.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Country</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('invoice_status.index')}}" class="nav-link {{ request()->routeIs('invoice_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Invoice Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('leave_type.index')}}" class="nav-link {{ request()->routeIs('leave_type.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Leave Type</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('marital_status.index')}}" class="nav-link {{ request()->routeIs('marital_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Marital Status</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('payment_method.index')}}" class="nav-link {{ request()->routeIs('payment_method.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Payment Method</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('payment_terms.index')}}" class="nav-link {{ request()->routeIs('payment_terms.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Payment Terms</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('project_type.index')}}" class="nav-link {{ request()->routeIs('project_type.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Project Type</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('project_status.index')}}" class="nav-link {{ request()->routeIs('project_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Project Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('project_terms.index')}}" class="nav-link {{ request()->routeIs('project_terms.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Project Terms</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('status.index')}}" class="nav-link {{ request()->routeIs('status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Status</p>
                      </a>
                    </li>

                    <li class="nav-item searchable">
                      <a href="{{route('system_health_master.index')}}" class="nav-link {{ request()->routeIs('system_health_master.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>System Health</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('master_term.index')}}" class="nav-link {{ request()->routeIs('master_term.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Terms</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('vendor_invoice_status.index')}}" class="nav-link {{ request()->routeIs('vendor_invoice_status.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Vendor Invoice Status</p>
                      </a>
                    </li>
                    <li class="nav-item searchable">
                      <a href="{{route('master_template.index')}}" class="nav-link {{ request()->routeIs('master_template.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Template Type</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('variable.index') }}" class="nav-link {{ request()->routeIs('variable.*') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Template Variables</p>
                      </a>
                    </li>
                  </ul>
                </li>
              @endif

              @if(Auth::user()->api)
                <li class="nav-item">
                  <a href="{{route('api.index')}}" class="nav-link {{ request()->routeIs('api.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-network-wired"></i>
                    <p>
                      API
                    </p>
                  </a>
                </li>
              @endif  

              @if(can_view_sidebar("Configs"))
                <li class="nav-item">
                  <a href="{{route('configs.index')}}" class="nav-link {{ request()->routeIs('configs.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-cogs"></i>
                    <p>
                      Configs
                    </p>
                  </a>
                </li>
              @endif
              @if(can_view_sidebar("WarningMaintenance"))
                <li class="nav-item">
                  <a href="{{route('warning_maintenance.index')}}" class="nav-link {{ request()->routeIs('warning_maintenance.*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-bell"></i>
                    <p>
                      Warnings
                    </p>
                  </a>
                </li>
              @endif
            </ul>
          </li>
          @endif

          @if(can_view_sidebar("SystemHealth"))
            <li class="nav-item has-treeview">
              <a href="{{ route('system_health.index')}}" class="nav-link {{ request()->routeIs('system_health.*') ? 'active' : '' }}">
                <i class="nav-icon fa fa-users"></i>
                <p>
                  System Health
                </p>
              </a>
            </li>
          @endif

 </ul>

</nav>
@endauth
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
