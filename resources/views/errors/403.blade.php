@extends('adminlte.default')
@section('title') Unauthorized @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <hr>
    <div>
        <div class="align-content-center">
            <table class="table" align="center" style="width:50%;margin:0px auto;position:fixed;top:30%;left:30%;">
                <tr style="border-top:0px;">
                    <td style="border-top:0px;"><h3><i class="fa fa-exclamation-triangle"></i> </h3></td>
                    <td style="border-top:0px;"><p>It appears you don't have permission to access this page. Please make sure you're authorized to view this content.</p></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
