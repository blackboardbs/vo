@extends('adminlte.default')

@section('title') Edit Availability @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('availability.update',['availability' => $availability,'cv' => $cv, 'resource' => $availability->res_id]), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                <tr>
                    <th>Availability Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('avail_status',$availstatus_dropdown,$availability->avail_status,['class'=>'form-control form-control-sm '. ($errors->has('avail_status') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('avail_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Availability Date:</th>
                    <td>{{Form::text('avail_date',$availability->avail_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('avail_date') ? ' is-invalid' : ''),'placeholder'=>'Available Date'])}}
                        @foreach($errors->get('avail_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('status_date',$availability->status_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('status_date') ? ' is-invalid' : ''),'placeholder'=>'Status Date'])}}
                        @foreach($errors->get('status_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Availability Note:</th>
                    <td>{{Form::textarea('avail_note',$availability->avail_note,['class'=>'form-control form-control-sm'. ($errors->has('avail_note') ? ' is-invalid' : ''),'placeholder'=>'Available Note'])}}
                        @foreach($errors->get('avail_note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Rate Hour:</th>
                    <td>{{Form::text('rate_hour',$availability->rate_hour,['class'=>'form-control form-control-sm'. ($errors->has('rate_hour') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                        @foreach($errors->get('rate_hour') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Rate Month:</th>
                    <td>{{Form::text('rate_month',$availability->rate_month,['class'=>'form-control form-control-sm'. ($errors->has('rate_month') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                        @foreach($errors->get('rate_month') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$availability->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection