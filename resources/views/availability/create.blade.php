@extends('adminlte.default')
@section('title') Add Availability @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('availability.store',['cvid' => $cv,'res_id' => $res]), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Availability Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('avail_status',$availstatus_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('avail_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('avail_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Availability Date:</th>
                        <td>{{Form::text('avail_date',old('avail_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('avail_date') ? ' is-invalid' : ''),'placeholder'=>'Available Date'])}}
                            @foreach($errors->get('avail_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Status Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::text('status_date',$date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('status_date') ? ' is-invalid' : ''),'placeholder'=>'Status Date'])}}
                            @foreach($errors->get('status_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Availability Note:</th>
                        <td>{{Form::textarea('avail_note',old('avail_note'),['class'=>'form-control form-control-sm'. ($errors->has('avail_note') ? ' is-invalid' : ''),'placeholder'=>'Available Note'])}}
                            @foreach($errors->get('avail_note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Rate Hour:</th>
                        <td>{{Form::text('rate_hour',old('rate_hour'),['class'=>'form-control form-control-sm'. ($errors->has('rate_hour') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                            @foreach($errors->get('rate_hour') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Rate Month:</th>
                        <td>{{Form::text('rate_month',old('rate_month'),['class'=>'form-control form-control-sm'. ($errors->has('rate_month') ? ' is-invalid' : ''),'placeholder'=>'0.00'])}}
                            @foreach($errors->get('rate_month') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                        <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr class="btn-dark">
                        <th>Status Date</th>
                        <th>Availability Status</th>
                        <th>Availability Date</th>
                        <th>Note</th>
                        <th>Rate</th>
                        <th>Month Rate</th>
                        <th>Record Status</th>
                    </tr>
                    @forelse($availabilities as $availability)
                    <tr>
                        <td>{{$availability->status_date}}</td>
                        <td>{{$availability->avail_statusd->description}}</td>
                        <td>{{$availability->avail_date}}</td>
                        <td>{{$availability->avail_note}}</td>
                        <td>{{$availability->rate_hour}}</td>
                        <td>{{$availability->rate_month}}</td>
                        <td>{{$availability->status->description}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8">No records to show</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection