@extends('adminlte.default')

@section('title') View Availability @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('availability.edit', ['availability' => $availability,'cv' => $cv_id, 'resource' => $res_id])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Availability</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Availability Status:</th>
                        <td>{{$availability->avail_statusd->description}}</td>
                        <th>Availability Date:</th>
                        <td>{{$availability->avail_date}}</td>
                    </tr>
                    <tr>
                        <th>Status Date:</th>
                        <td>{{$availability->status_date}}</td>
                        <th>Availability Note:</th>
                        <td>{{$availability->avail_note}}</td>
                    </tr>
                    <tr>
                        <th>Rate Hour:</th>
                        <td>{{$availability->rate_hour}}</td>
                        <th>Rate Month:</th>
                        <td>{{$availability->rate_month}}</td>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>{{$availability->status->description}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
