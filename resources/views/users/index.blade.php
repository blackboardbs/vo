@extends('adminlte.default')

@section('title') Users @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('users.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> User</a>
            <x-export route="users.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::text('exp_date_from', request()->exp_date_from,['class' => 'form-control form-control-sm datepicker w-100'])}}
                <span>Expiry Date From</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::text('exp_date_to', request()->exp_date_to,['class' => 'form-control form-control-sm datepicker w-100'])}}
                <span>Expiry Date To</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{request()->q}}" onkeyup="handle(event)" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('customer', $customer_dropdown,  request()->customer,['class' => 'form-control w-100', 'placeholder' => 'All'])}}
                <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('vendor', $vendor_dropdown,  request()->vendor,['class' => 'form-control w-100', 'placeholder' => 'All'])}}
                <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('company', $company_dropdown, request()->company,['class' => 'form-control w-100', 'placeholder' => 'All'])}}
                <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 mt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('role', $roles,  request()->role,['class' => 'form-control w-100'])}}
                <span>Role</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3 mt-3">
                <a href="{{route('users.index')}}" class="btn btn-info w-100">Clear Filter</a>
            </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('first_name', 'Name')</th>
                <th>@sortablelink('email', 'Email')</th>
                <th>@sortablelink('phone', 'Phone')</th>
                <th><abbr title="This is the amount of roles a user is assigned to.">Roles</abbr></th>
                <th>@sortablelink('created_at', 'Added')</th>
                <th>Status</th>
                @if($can_update)
                <th class="last">Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td><a href="{{route('profile',$user)}}">{{$user->name()}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>
                        {{-- $user->roles->count() --}}
                        @foreach($user->roles as $roles)
                            {{$roles->display_name}} <br/>
                        @endforeach
                    </td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>{{$user->status?->description}}</td>
                    @if($can_update)
                    <td>
                        <div class="d-flex">
                        @if(!isset($user->api_token) && empty($user->api_token))
                            <a href="{{route('users.generateToken',$user)}}" class="btn btn-secondary btn-sm"  data-toggle="tooltip" data-placement="top" title="Give API Access"><i class="fas fa-barcode"></i></a>&nbsp;
                        @else
                            {{-- <a href="{{route('users.removeToken',$user->api?->id)}}" class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="top" title="Remove API Access"><i class="fas fa-barcode"></i></a>&nbsp; --}}
                            <a href="{{route('users.revokeToken',[$user,$user->api?->id])}}" class="btn btn-danger btn-sm"  data-toggle="tooltip" data-placement="top" title="Remove API Access"><i class="fas fa-barcode"></i></a>&nbsp;
                        @endif
                        <a href="{{route('users.edit',$user)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>&nbsp;
                        @if($user->id != auth()->id())
                            @if($user->status_id == '1')
                                <a href="{{route('users.destroy',$user)}}" class="btn btn-danger btn-sm"><i class="fas fa-user-alt-slash"></i></a>
                            @elseif($user->status_id == '2')
                                <a href="{{route('users.destroy',$user)}}" class="btn btn-danger btn-sm"><i class="fas fa-user-alt"></i></a>
                            @endif
                        @endif
                        </div>
                    </td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No clients match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $users->firstItem() }} - {{ $users->lastItem() }} of {{ $users->total() }}
                    </td>
                    <td>
                        {{ $users->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {
            $('select').on('change', function () {
                $('#searchform').submit();
            })
        })
    </script>
@endsection
