@extends('adminlte.default')

@section('title') Create User @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
    <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
    <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if($can_add_login_user == 0)
            <div class="alert alert-warning">
                You have reached the maximum number of users for your subscription. You can add more users, but these users will not have access to the system.<br /><br />
                Please upgrade your subscription to add additional login users.
            </div>
        @endif
    {{Form::open(['url' => route('users.store'), 'method' => 'post','files'=>true, 'autocomplete'=>'off','id'=>'saveForm'])}}
    <input type="hidden" name="cv" id="cv" value="{{$from_cv}}"/>
            <div class="form-row mt-3">
                <div class="form-group col-md-6">
                    {{Form::label('first_name', 'First Name', ['class' => 'col-auto mx-0 px-0'])}} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                    {{Form::text('first_name',$scouting->resource_first_name??old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                    @foreach($errors->get('first_name') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group col-md-6">
                    {{Form::label('last_name', 'Last Name', ['class' => 'col-auto mx-0 px-0'])}} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                    {{Form::text('last_name',$scouting->resource_last_name??old('last_name'),['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
                    @foreach($errors->get('last_name') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{Form::label('email', 'Email', ['class' => 'col-auto mx-0 px-0', 'title' => "The email must be unique and will be the user’s username"])}}
                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The email must be unique and will be the user’s username"><i class="far fa-question-circle"></i></span></sup>
                        &nbsp;
                    <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                    {{Form::text('email',$scouting->email??old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                    @foreach($errors->get('email') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group col-md-6">
                    {{Form::label('phone', 'Phone')}}
                    {{Form::text('phone',$scouting->phone??old('phone'),['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                    @foreach($errors->get('phone') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                <div class="form-check mb-3">

                    @if($can_add_login_user == 0)
                        {{Form::checkbox('login_user',0,false,['class'=>'form-check-input '. ($errors->has('login_user') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                    @else
                        {{Form::checkbox('login_user',1,true,['class'=>'form-check-input '. ($errors->has('login_user') ? ' is-invalid' : '')])}}
                    @endif
                    {{Form::label('login_user', 'Login User', ['class' => 'col-auto form-check-label, px-0'])}}
                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="A login user will sign in and interact in the application. A non-login user will be a named user, but will not sign in. A non-login user can be assigned to projects, timesheets, resumes, etc."><i class="far fa-question-circle"></i></span></sup>
                        &nbsp;
                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                    @foreach($errors->get('login_user') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                </div>
                <div class="form-group col-md-6">
                    {{Form::label('expiry_date', 'Expiration Date')}}
                    {{Form::text('expiry_date','2024-12-01',['class'=>'datepicker form-control form-control-sm'. ($errors->has('expiry_date') ? ' is-invalid' : '')])}}
                    @foreach($errors->get('customer') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                {{Form::label('landing_page', 'Landing Page', ['class' => 'col-auto form-check-label, px-0'])}}
                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title=" A Default landing page can be set per user to enhance the user experience and reduce the number of clicks. This will be the first page that a user sees after signing into Consulteaze."><i class="far fa-question-circle"></i></span></sup>
                &nbsp;
                @include("_landing")
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{Form::label('avatar', 'Display Picture')}}
                    {{Form::file('avatar',['class'=>'form-control  form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                    @foreach($errors->get('avatar') as $error)
                        <div class="invalid-feedback">
                            {{ $error}}
                        </div>
                    @endforeach
                    <small id="avatar" class="form-text text-muted">
                        Images must be a square and will be resized to 200x200
                    </small>
                    <br>
                    <img src="{{route('user_avatar',['q'=>'default.png'])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/>

                </div>
                <div class="form-group col-md-6">
                    {{Form::label('role', 'Role', ['class' => 'col-auto mx-0 px-0'])}}
                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="A user can be associated to one or more system roles. A Permissions are inherited from the highest to lowes role. Role permissions can be customised under the Admin / Securiry / Roles menu by the system administrator."><i class="far fa-question-circle"></i></span></sup>
                    &nbsp;
                    <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
                    {{Form::select('role[]',$roles,old('role[]'),['class'=>'form-control form-control-sm text-capitalize'. ($errors->has('role') ? ' is-invalid' : ''),'multiple', 'size' => '10'])}}
                    @foreach($errors->get('role') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                    <small class="form-text text-muted">
                        Hold <kbd>Ctrl</kbd> to select multiple entries
                    </small>
                </div>
            </div>

            <div class="form-row mt-2 border border-info rounded pt-2">
                <div class="form-group col-md-6">
                    <div class="form-check">
                        {{Form::checkbox('vendor_check',1,false,['class'=>'form-check-input '. ($errors->has('onboarding') ? ' is-invalid' : ''), 'id' => 'vendor_check'])}}
                        {{Form::label('vendor_check', 'Vendor', ['class' => 'form-check-label'])}}
                    </div>
                    <small class="text-muted">Only use this function if the user is invoicing through a vendor. The user will be linked to the specific vendor for invoicing and reporting. Only a user linked to the Vendor role would be able to create vendor invoices and view statements. A normal consultant does not need the Vendor role and would normally only have the Contractor role, and linked to the vendor in this section.</small>
                    {{Form::select('vendor',$vendor_dropdown,old('vendor'),['class'=>'form-control form-control-sm '. ($errors->has('vendor') ? ' is-invalid' : ''), 'id' => 'vendor', 'style' => 'display:none'])}}
                    @foreach($errors->get('vendor') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check">
                        {{Form::checkbox('customer_check',1,false,['class'=>'form-check-input text-bold '. ($errors->has('onboarding') ? ' is-invalid' : ''), 'id' => 'customer_check'])}}
                        {{Form::label('customer_check', 'Customer', ['class' => 'form-check-label'])}}
                        @foreach($errors->get('onboarding') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </div>
                    <small class="text-muted mb-2">A user with role Customer would be able to see reports and projects for the customer linked in this section.</small>

                    {{Form::select('customer',$customer_dropdown,old('customer'),['class'=>'form-control form-control-sm '. ($errors->has('customer') ? ' is-invalid' : ''), 'id' => 'customer', 'style' => 'display:none'])}}
                    @foreach($errors->get('customer') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="border border-info rounded p-1 mt-3" style="margin-right: -5px; margin-left: -5px">
                <div class="form-check">
                    {{Form::checkbox('onboarding',1,false,['class'=>'form-check-input '. ($errors->has('onboarding') ? ' is-invalid' : ''), 'id' => 'onboarding'])}}
                    {{Form::label('onboarding', 'Onboard', ['class' => 'form-check-label'])}}
                    @foreach($errors->get('onboarding') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>
                <small class="text-muted mb-3">
                    Only select this option if the User must be onboarded through the Consulteaze workflow. The user will receive an email that will lead him to a form where all required information can be captured for contracting and onboarding. The approval manager will review and approve information and the onboarding activities will be available to track under the Onboarding/User menu.
                </small>

                <div class="form-row" style="display: none" id="onboarding-details">
                    <div class="form-group col-md-6">
                        {{Form::label('usertype_id', 'User Type')}}
                        {{Form::select('usertype_id',$usertype_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('usertype_id') ? ' is-invalid' : ''), 'placeholder' => 'Select User Type'])}}
                        @foreach($errors->get('usertype_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-6">
                        {{Form::label('appointment_manager_id', 'Appointment Manager')}}
                        {{Form::select('appointment_manager_id',$appointment_manager_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('appointment_manager_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Appointment Manager'])}}
                        @foreach($errors->get('appointment_manager_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

    {{Form::close()}}
    </div>
@endsection

@section('extra-js')

    <script>

        $(function () {
            hideAndSeek("#vendor_check", "#vendor")
            hideAndSeek("#customer_check", "#customer")
            hideAndSeek("#onboarding", "#onboarding-details")
        });

        function hideAndSeek(checkBox, toReveal) {
            $(checkBox).on('click', function (){
                if ($(checkBox).is(':checked')) {
                    $(toReveal).show()
                } else {
                    $(toReveal).hide()
                }
            })
        }
    </script>
@endsection