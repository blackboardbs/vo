@extends('adminlte.default')

@section('title') Edit User @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
    <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
    <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    {{Form::open(['url' => route('users.updatesuper', $user->id), 'method' => 'put','files'=>true, 'autocomplete'=>'off','id'=>'saveForm'])}}

    <div class="form-group">
        {{Form::label('expiry_date', 'Expiration Date')}}
        {{Form::text('expiry_date',$user->expiry_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('expiry_date') ? ' is-invalid' : '')])}}
        @foreach($errors->get('customer') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('status', 'Status')}} <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span>
        {{Form::select('status',$status_drop_down,$user->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
        @foreach($errors->get('status') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>

    {{Form::close()}}
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection