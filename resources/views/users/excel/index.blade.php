<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Roles</th>
        <th>Added</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->name()}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>
                {{-- $user->roles->count() --}}
                @foreach($user->roles as $roles)
                    {{$roles->display_name}} <br/>
                @endforeach
            </td>
            <td>{{$user->created_at->diffForHumans()}}</td>
            <td>{{$user->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>