@extends('adminlte.default')

@section('title') Profile @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>{{$user->name()}}</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                @if($can_update)
                <a href="{{route('users.edit', $user)}}" class="btn btn-success float-right ml-1">Edit</a>
                @endif
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-12">
            <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="{{route('user_avatar',['q'=>$user->avatar])}}" class="blackboard-avatar blackboard-avatar-profile"/>
                        </div>
                        <div class="col-lg-9">
                            <ul>
                                <dt>
                                    First name:
                                </dt>
                                <dd>
                                    {{$user->first_name}}
                                </dd>
                                <dt>
                                    Last Name
                                </dt>
                                <dd>
                                    {{$user->last_name}}
                                </dd>
                                <dt>
                                    Email
                                </dt>
                                <dd>
                                    {{$user->email}}
                                </dd>
                                <dt>
                                    Phone
                                </dt>
                                <dd>
                                    {{$user->phone}}
                                </dd>
                                <dt>
                                    Company
                                </dt>
                                <dd>
                                    @if(isset($user->company->company_name))
                                        {{$user->company->company_name}}
                                    @else
                                        <small class="text-muted">This user is not assigned to any company</small>
                                    @endif
                                </dd>
                                <dt>
                                    Resource
                                </dt>
                                <dd>
                                    @isset($user->resource)
                                        <a href="{{route('resource.show',$user->resource->id)}}" class="btn btn-warning btn-sm">View Resource</a>

                                        <a href="{{route('resource.edit',$user->resource->id)}}" class="btn btn-success btn-sm ml-1">Edit Resource</a>
                                    @endisset
                                </dd>
                                <dt>
                                    Customer
                                </dt>
                                <dd>
                                    @if(isset($user->customer->customer_name))
                                        {{$user->customer->customer_name}}
                                    @else
                                        <small class="text-muted">This user is not assigned to any customer</small>
                                    @endif
                                </dd>
                                <dt>
                                    Vendor
                                </dt>
                                <dd>
                                    @if(isset($user->vendor->vendor_name))
                                        {{$user->vendor->vendor_name}}
                                    @else
                                        <small class="text-muted">This user is not assigned to any vendor</small>
                                    @endif
                                </dd>
                                <dt>
                                    Roles
                                </dt>
                                @forelse($user->roles as $role)
                                    <dd>{{$role->display_name}}</dd>
                                @empty
                                    <dd><small class="text-muted">This user is not assigned to any roles</small></dd>
                                @endforelse
                                <dt>
                                    Expiry Date
                                </dt>
                                <dd>
                                    @if(isset($user->expiry_date))
                                        {{$user->expiry_date}}
                                    @else
                                        <small class="text-muted">This user has no expiration date.</small>
                                    @endif
                                </dd>
                                <dt>
                                    Status
                                </dt>
                                <dd>
                                    @if(isset($user->status->description))
                                        {{$user->status->description}}
                                    @endif
                                </dd>
                                <dt>
                                    Onboard
                                </dt>
                                <dd>
                                    {{$user->onboarding_value}}
                                </dd>
                                <dt>
                                    User Type
                                </dt>
                                <dd>
                                    {{$user->userType->description??null}}
                                </dd>
                                <dt>
                                    Appointment Manager
                                </dt>
                                <dd>
                                    {{isset($user->appointmentManager)?$user->appointmentManager->name():null}}
                                </dd>
                                <dt>
                                    Login User
                                </dt>
                                <dd>
                                    {{($user->login_user == 1)?'Yes':'No'}}
                                </dd>
                                <dt>
                                    
                                    @if(Auth::user()->hasRole('admin') || auth()->user()->id == $user->id)
                                    {{ Form::open(['method' => 'POST','route' => ['resend.credentials', $user],'style'=>'display:inline']) }}
                                    {{ Form::submit('Resend Credentials to User', ['class' => 'btn btn-dark btn-sm']) }}
                                    {{ Form::close() }}
                                    @endif
                                    @if(Auth::user()->hasRole('admin'))
                                    {{ Form::open(['method' => 'POST','route' => ['resendadmin.credentials', $user],'style'=>'display:inline']) }}
                                    {{ Form::submit('Resend Credentials to Admin', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                    {{ Form::close() }}
                                    @endif
                                </dt>
                                <dd>

                                </dd>
                            </ul>
                        </div>
                    </div>

        </div>

    </div>
    </div>
@endsection
