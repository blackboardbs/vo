@extends('adminlte.default')

@section('title') Settings @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="submitProfilePage()" class="btn btn-primary ml-1 {{ $can_update ? '' : 'disabled' }}" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('extra-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.4.2/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/css/bootstrap-iconpicker.min.css">
@endsection

@section('content')
    <div class="container-fluid">
    <div class="mt-3">
            <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                    <li><a data-toggle="tab" href="#security">Security</a></li>
                    <li><a data-toggle="tab" href="#preferences">Preferences</a></li>
                    <li><a data-toggle="tab" href="#favourites3">Favourites</a></li>
                    <li><a data-toggle="tab" href="#landing-page">Landing Page</a></li>
                    <!--<li><a data-toggle="tab" href="#notification">Notification</a></li>-->
                </ul>

            <div style="min-height:45px;width:100%;height:100%;position: relative;" id="clock">
                <div class="clock" style="left:55%;margin-top:-30px;"></div>
            </div>

                    {{Form::open(['url' => route('settings.update'), 'method' => 'post','files'=>true])}}
            <div class="tab-content">
                <div id="profile" class="tab-pane active">
                    <div class="container-fluid container-title">
                        <div class="row col-md-12 w-100 pt-3">
                            <div class="col col-md-4 text-left">
                            <div class="form-group">
                                {{Form::label('avatar', 'Display Picture',['class'=>'text-left float-left d-block pr-3'])}}

                                <br>
                                <img src="{{route('user_avatar',['q'=>Auth::user()->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/>
                                {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                                @foreach($errors->get('avatar') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error}}
                                    </div>
                                @endforeach
                                <small id="avatar" class="form-text text-muted">
                                    Images will be cropped to 200x200
                                </small>
                            </div>
                            </div>
                            <div class="col">
                            <div class="form-group row col-md-12">
                                <div class="col-md-3">
                                    {{Form::label('first_name', 'First Name',['class'=>'d-block'])}}
                                </div>
                                <div class="col-md-9">
                                {{Form::text('first_name',auth()->user()->first_name,['class'=>'form-control form-control-sm w-50'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                                @foreach($errors->get('first_name') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error}}
                                    </div>
                                @endforeach
</div>
                            </div>

                                <div class="form-group row col-md-12">
                                <div class="col-md-3">
                                    {{Form::label('last_name', 'Last Name',['class'=>'d-block'])}}
                                </div>
                                <div class="col-md-9">
                                    {{Form::text('last_name',auth()->user()->last_name,['class'=>'form-control form-control-sm w-50'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
                                    @foreach($errors->get('last_name') as $error)
                                        <div class="invalid-feedback">
                                            {{ $error}}
                                        </div>
                                    @endforeach
                                </div>
                                </div>

                                <div class="form-group row col-md-12">
                                <div class="col-md-3">
                                    {{Form::label('email', 'Email',['class'=>'d-block'])}}
                                </div>
                                <div class="col-md-9">
                                    {{Form::text('email',auth()->user()->email,['class'=>'form-control form-control-sm w-50'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                                    @foreach($errors->get('email') as $error)
                                        <div class="invalid-feedback">
                                            {{ $error}}
                                        </div>
                                    @endforeach
                                </div>
                                </div>

                                <div class="form-group row col-md-12">
                                <div class="col-md-3">
                                    {{Form::label('phone', 'Phone',['class'=>'d-block'])}}
                                </div>
                                <div class="col-md-9">
                                    {{Form::text('phone',auth()->user()->phone,['class'=>'form-control form-control-sm w-50'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                                    @foreach($errors->get('phone') as $error)
                                        <div class="invalid-feedback">
                                            {{ $error}}
                                        </div>
                                    @endforeach
                                </div>
                                </div>
</div>
                            </div>
                    </div>
                </div>
                <div id="security" class="tab-pane fade">
                    <div class="container-fluid container-title">
                    <div class="row col-md-12 w-100 pt-3">
                        @if(auth()->user()->login_user == 1)

                            <div class="form-group row col-md-12">
                                <div class="col-md-2">
                                {{Form::label('old_password', 'Old Password')}}
                                </div>
                                <div class="col-md-4">
                                {{Form::password('old_password',['class'=>'form-control form-control-sm'. ($errors->has('old_password') ? ' is-invalid' : ''),'placeholder'=>'Old password'])}}
                                @foreach($errors->get('old_password') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error}}
                                    </div>
                                @endforeach
                                </div>
                            </div>

                            <div class="form-group row col-md-12">
                                <div class="col-md-2">
                                {{Form::label('password', 'New Password')}}
                                </div>
                                <div class="col-md-4">
                                {{Form::password('password',['class'=>'form-control form-control-sm'. ($errors->has('password') ? ' is-invalid' : ''),'placeholder'=>'New password'])}}
                                @foreach($errors->get('password') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error }}
                                    </div>
                                @endforeach
                                </div>
                            </div>

                            <div class="form-group row col-md-12">
                                <div class="col-md-2">
                                {{Form::label('password_confirmation', 'Confirm New Password')}}
                                </div>
                                <div class="col-md-4">
                                {{Form::password('password_confirmation',['class'=>'form-control form-control-sm'. ($errors->has('password_confirmation') ? ' is-invalid' : ''),'placeholder'=>'Confirm New Password'])}}
                                @foreach($errors->get('password_confirmation') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error}}
                                    </div>
                                @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Change password</button>
                            </div>

                        @endif
                    </div>
                    </div>
                </div>
                <div id="preferences" class="tab-pane fade">
                    <div class="container-fluid container-title">
                    <div class="row col-md-12 w-100 pt-3">
                        <div class="form-group">

                        <div class="row col-md-12">
                            <div>
                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="action" value="action" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'action') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="action">Action</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="task" value="task" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'task') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="task">Task</label>
                                </div>


                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="leave" value="leave" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'leave') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="leave">Leave</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="assignment" value="assignment" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'assignment') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="assignments">Assignments</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="assessment" value="assessment" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'assessment') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="assignments">Assessment</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="anniversary" value="anniversary" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'anniversary') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="anniversaries">Anniversaries</label>
                                </div>


                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="cale" value="cale" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'cale') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="cale">Events</label>
                                </div>


                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="user" value="user" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'user') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="user">User</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="cinvoice" value="cinvoice" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'cinvoice') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="cinvoice">Customer Invoice</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="vinvoice" value="vinvoice" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'vinvoice') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="vinvoice">Vendor Invoice</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="certificates" value="certificates" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'certificates') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="certificates">Medical Certificate</label>
                                </div>

                                <div class="form-check form-check-inline col-md-2">
                                    <input class="form-check-input calFilter" type="checkbox" id="holidays" value="holidays" name="calendar[]" {{(strpos(auth()->user()->calendar_view,'holidays') !== false ? 'checked="checked"' : '')}}>
                                    <label class="form-check-label" for="holidays">Public Holidays</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
                <div id="favourites2" class="tab-pane fade">
                    <div class="container-fluid container-title">
                        {{Form::hidden('role_id', $user_role)}}
                        <div class="form-group">

                            <div class="col-lg-12">
                                <div>
                                    @forelse($favourites as $favourite)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input calFilter" type="checkbox" value="{{$favourite->id}}" name="favourites_id[]" {{((in_array($favourite->id, $fav_ids)) ? 'checked="checked"' : '')}}>
                                            <label class="form-check-label" for="favourites_id">{{$favourite->module_name}}</label>
                                        </div>
                                    @empty
                                        <div>
                                            No Favourites Available
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="favourites3" class="tab-pane fade">
                    <div class="container-fluid container-title pt-3">
                        <div class="form-group mt">

                            <div class="col-lg-12">
                                <!-- <div style="margin-top:10px;">
                                    <form action="{{ route('favorite.store') }}" method="post">
                                        @csrf
                                        <strong>Add Favourite</strong>
                                        <input id="user_id" name="user_id" type="text" hidden value="{{ $user->id }}">
                                        <div style="width: 50%; margin-top:5px;">
                                            <label for="title" class="form-label">Title</label>
                                            <input type="text" class="form-control form-control-sm" id="title" name="title" placeholder="Title">
                                        </div>
                                        <div style="width: 50%; margin-top:5px;">
                                            <label for="url" class="form-label">URL</label>
                                            <small>Input the last part of the url after http://{{tenant()->domains()?->first()?->domain}}</small>
                                            <input type="text" class="form-control form-control-sm" id="url" name="url" placeholder="/url">
                                        </div>
                                        <div style="width: 50%; margin-top:5px;">
                                            <label for="icon" class="form-label">Icon</label>
                                            <input type="text" id="icon" name="icon" hidden>
                                            <button type="button" style="border: 1px solid rgba(95, 95, 95, 0.264);" class="btn btn-default btn-sm" role="iconpicker" data-iconset="fontawesome5" data-search="true" data-search-text="Search icons..." data-placement="bottom"></button>
                                        </div>
                                        <div style="width: 50%; margin-top:5px;">
                                            <label for="color" class="form-label">Color</label>
                                            <input type="color" class="form-control" id="color" name="color" value="#000000">
                                        </div>
                                        <div style="width: 50%; margin-top:5px;">
                                            <button type="submit" class="btn btn-success btn-sm">Add</button>
                                        </div>
                                    </form>
                                </div>
                                <br>
                                <strong>Manage Favourites</strong>
                                <br>
                                <br> -->
                                <div class="row">
                                    @forelse($pages as $page)
                                        <div class="col-md-4 mb-3">
                                            <div class="form-check form-check-inline" style="margin: 5px;">
                                                <input class="form-check-input calFilter mr-3" type="checkbox" id="favorites_id{{ $page->id }}" value="{{$page->id}}" name="favorites_id[]" onclick="toggleFavorite1({{ $page->id }})" {{ $user->favorites->contains($page->id) ? 'checked="checked"' : '' }}>
                                                <label class="form-check-label" for="favorites_id{{ $page->id }}"><span style="font-size:.75rem">{!! $page->icon !!}</span> {{ $page->title }}</label>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="col-12">
                                            No Favourites Available
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="landing-page" class="tab-pane fade">
                    <div class="container-fluid container-title">
                    <div class="row col-md-12 w-100 pt-3">
                        {{Form::hidden('user_id', auth()->id())}}
                        <div class="form-group">

                            @include("_landing")
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                        {{Form::close()}}
    </div>

    </div>
@endsection
@section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.10.0/js/bootstrap-iconpicker.bundle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });

            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#myTab li').removeClass('active');
                $('#myTab a[href="' + activeTab + '"]').tab('show');
                $('#myTab a[href="' + activeTab + '"]').parent('li').addClass('active');
            }

            $('#myTab li a').on('click',function(){
                $('#myTab li').removeClass('active');
                $(this).parent('li').addClass('active');
            });

            // Initialize icon picker
            $('[role="iconpicker"]').iconpicker();

            // Update icon input and preview when icon is selected
            $('[role="iconpicker"]').on('iconpickerSelected', function(event) {
                const selectedIcon = event.iconpickerValue;
                const color = $('#color').val();
                updateIconHtml(selectedIcon, color);
            });

            // Update icon preview when color changes
            $('#color').on('change', function() {
                const selectedIcon = $('[role="iconpicker"]').find('input').val();
                const color = $(this).val();
                updateIconHtml(selectedIcon, color);
            });

            function updateIconHtml(iconClass, color) {
                const iconHtml = `<i class="${iconClass} fa-lg" style="color: ${color};"></i>`;
                $('#icon').val(iconHtml);
            }

            $("#clock").hide();
        });

        function toggleFavorite1(page_id) {
            // $("#clock").show();
            // axios.post(`/favorites/toggle`, { page_id })
            //     .then(response => {
            //         console.log('Favorite status updated:', response.data.favorited);
            //         // window.location.reload();
            //     })
            //     .catch(error => {
            //         console.error('Error toggling favorite:', error);
            //     });
        }
        function submitProfilePage(){
            $('form').each(function() {
                // Check if the form is visible
                if ($(this).is(':visible')) {
                    // Submit the form
                    $(this).submit();
                }
            });
        }

    </script>
@endsection
