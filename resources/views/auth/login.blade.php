@extends('layouts.proud')

@section('proud-form')
    <form method="POST" action="{{ route('login') }}" name="g-v3-recaptcha-captcha" id="g-v3-recaptcha-captcha">
        {{ csrf_field() }}
        @if(session()->has('login_error'))
            <div class="alert alert-danger">
                {{ session()->get('login_error') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                {{Form::email('identity',old('identity'),['class'=>'form-control'. ($errors->has('identity') ? ' is-invalid' : ''),'placeholder'=>'Email','required','autofocus'])}}
            </div>
            @foreach($errors->get('identity') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                {{Form::password('password',['class'=>'form-control'. ($errors->has('password') ? ' is-invalid' : ''),'placeholder'=>'Password','required'])}}
            </div>
            @foreach($errors->get('password') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>
        {{--<div class="form-group">
            <div class="input-group">
                {!! app('captcha')->render() !!}
            </div>
        </div>--}}

        {{--<div class="form-group">
            <div class="input-group">
                {!! RecaptchaV3::initJs() !!}
                {!! RecaptchaV3::field('captcha') !!}
            </div>
        </div>--}}


        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" {{old('remember') ? 'checked' : ''}}> Remember Me
                </label>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-block" value="Log in">
        </div>

    </form>
@endsection

@section('proud-footer')
    <div class="card-footer row p-3" style="margin-left:0px !important;margin-right:0px !important;">
        <div class="col-md-6">
            <a class="password-request" href="{{route('password.request')}}">
                Forgot Your Password?
            </a>
<!--            First time here? <a href="{{route('register')}}">Register</a>-->
        </div>
        <div class="col-md-6 text-right">

        </div>

    </div>


@endsection

@section('proud-extra')

@endsection
