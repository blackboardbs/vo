<!-- Button trigger modal -->
<button style="display: none;" id="alertButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#alertModal">
    Launch modal
</button>
<!-- Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                <button id="closeAlertModal" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="alertModalBody" class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Confirm</button>
                <button type="button" class="btn btn-secondary" onclick="cancel()">Cancel</button>
            </div>
        </div>
    </div>
</div>
@section('extra-js')
    <script>
        let type = 0;
        $(function(){

            $('#start_date').change(function(){
                checkStartDate();
            });

            $('#end_date').change(function(){
                checkEndDate();
            });

        });

        function checkStartDate(){
            type = 1;
            let start_date = new Date($('#start_date').val());
            let parent_start_date = new Date($('#parent_start_date').val());
            let parent_name = $('#parent_name').val();
            let child_name = $('#child_name').val();

            if(start_date < parent_start_date) {
                $('#alertModalBody').html('The ' + child_name + ' start date is less than the ' + parent_name + ' start date. <br/><br/>Do you want to confirm this?');
                $('#alertButton').click();
            }
        }

        function checkEndDate(){
            type = 2;
            let end_date = new Date($('#end_date').val());
            let parent_end_date = new Date($('#parent_end_date').val());
            let parent_name = $('#parent_name').val();
            let child_name = $('#child_name').val();

            if(end_date > parent_end_date) {
                $('#alertModalBody').html('The ' + child_name + ' end date is greater than the ' + parent_name + ' end date. <br/><br/>Do you want to confirm this?');
                $('#alertButton').click();
            }
        }

        function cancel(){
            if(type == 1){
                $('#start_date').val($('#parent_start_date').val());
            }

            if(type == 2){
                $('#end_date').val($('#parent_end_date').val());
            }

            $('#closeAlertModal').click();
        }

        function getParentDates(parent_name, parent){
            console.log('parent', parent);
            parent_id = parent.value;
            axios.get('/api/getparent/'+parent_name+'/'+parent_id)
            .then(function (response) {
                console.log('response', response);
                $('#start_date').val(response.data.start_date);
                $('#end_date').val(response.data.end_date);

                $('#parent_start_date').val(response.data.start_date)
                $('#parent_end_date').val(response.data.end_date)
            });
        }
    </script>
    {{-- <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>
        tinymce.init({
            selector:'textarea.details',
            width: "100%",
            height: 300
        });
    </script> --}}
@endsection