@extends('adminlte.default')
@section('title') Edit Risk @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('editRisk')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive mb-3">
            {{Form::open(['url' => route('risk.update', $risk->id), 'method' => 'put', 'class'=>'mt-3','id'=>'editRisk'])}}
            <input type="hidden" id="parent_start_date" name="parent_start_date" value="{{isset($project->start_date) ? $project->start_date: ''}}" />
            <input type="hidden" id="parent_end_date" name="parent_end_date" value="{{isset($project->end_date) ? $project->end_date : ''}}" />
            <input type="hidden" id="parent_name" name="parent_name" value="Project" />
            <input type="hidden" id="child_name" name="child_name" value="Epic" />
            <table class="table table-bordered table-sm mb-0">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $risk->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'placeholder' => 'Name', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description:</th>
                    <td>
                        {{Form::text('description' , $risk->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'id'=>'description', 'placeholder' => 'Description', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $risk->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Risk Date:</th>
                    <td>
                        {{Form::text('risk_date', $risk->risk_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('risk_date') ? ' is-invalid' : ''), 'id'=>'risk_date', 'autocomplete' => 'off', 'placeholder' => 'Date'])}}
                        @foreach($errors->get('risk_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Risk Area:</th>
                    <td>
                        {{Form::select('risk_area', $riskAreaDropDown, $risk->risk_area_id, ['class'=>'form-control form-control-sm'. ($errors->has('risk_area') ? ' is-invalid' : ''), 'id' => 'risk_area', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('risk_area') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Risk Item:</th>
                    <td>
                        @if($risk->risk_area_id == 4)
                        {{Form::text('risk_item', $risk->risk_item, ['class'=>'form-control form-control-sm'. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_1' , 'placeholder' => 'Risk Item'])}}
                        {{Form::select('risk_item[]', [1, 2], old('risk_item'), ['class'=>'form-control form-control-sm '. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_2', 'placeholder' => 'Please select...', 'style' => 'display: none;', 'multiple ' => 'multiple ', 'style' => 'display: none;'])}}
                        @else
                        {{Form::text('risk_item1', $risk->risk_item, ['class'=>'form-control form-control-sm'. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_1', 'placeholder' => 'Risk Item', 'style' => 'display: none;'])}}
                        {{Form::select('risk_item[]', $riskItemDropDown, $risk_items, ['class'=>'form-control form-control-sm '. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_2', 'placeholder' => 'Please select...', 'multiple ' => 'multiple '])}}
                        @endif
                        @foreach($errors->get('risk_item') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Resposibility:</th>
                    <td>
                        {{Form::select('resposibility_user_id', $usersDropDown, $risk->resposibility_user_id, ['class'=>'form-control form-control-sm'. ($errors->has('resposibility_user_id') ? ' is-invalid' : ''), 'id' => 'resposibility_user_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('resposibility_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Accountability:</th>
                    <td>
                        {{Form::select('accountablity_user_id', $usersDropDown, $risk->accountablity_user_id, ['class'=>'form-control form-control-sm'. ($errors->has('accountablity_user_id') ? ' is-invalid' : ''), 'id' => 'accountablity_user_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('accountablity_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Likelihood:</th>
                    <td>
                        {{Form::select('likelihood_id', $likelyDropDown, $risk->likelihood_id, ['class'=>'form-control form-control-sm'. ($errors->has('likelihood_id') ? ' is-invalid' : ''), 'id' => 'likelihood_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('likelihood_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Impact:</th>
                    <td>
                        {{Form::select('impact_id', $impactDropDown, $risk->impact_id, ['class'=>'form-control form-control-sm '. ($errors->has('impact_id') ? ' is-invalid' : ''),'id'=>'impact_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('impact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Security:</th>
                    <td>
                        {{Form::select('security_id', $securityDropDown, $risk->security_id, ['class'=>'form-control form-control-sm '. ($errors->has('security_id') ? ' is-invalid' : ''),'id'=>'security_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('security_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $risk->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Risk Mitigation Strategy:</th>
                    <td>
                        {{Form::text('risk_mitigation_strategy',  $risk->risk_mitigation_strategy, ['class'=>'form-control form-control-sm'. ($errors->has('risk_mitigation_strategy') ? ' is-invalid' : ''), 'id' => 'risk_mitigation_strategy'])}}
                        @foreach($errors->get('risk_mitigation_strategy') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
        <div class="row">
            <div class="col-md-12 bg-dark text-center py-2 mb-3">
                <strong><i class="fas fa-comments"></i> FEEDBACK</strong>
            </div>
            <div class="col-md-6 mb-3">
                <div class="form-group">
                    <textarea class="form-control" id="feedback"></textarea>
                    <div id="empty-feedback" class="invalid-feedback" style="display: none">
                        <strong>Please provide valid feedback</strong>
                    </div>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="send_notification" id="send_no" checked value="0">
                    <label class="form-check-label" for="send_no">Don't Send Notification</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="send_notification" id="send_yes" value="1">
                    <label class="form-check-label" for="send_yes">Send Notification</label>
                </div>
                <div class="form-check form-check-inline float-right">
                    <button class="btn btn-sm btn-success float-right" id="send_feedback"><i class="far fa-comments"></i> Send Feedback</button>
                </div>
            </div>
            <div class="col-md-6 mt-3" id="all-feedback">

                @forelse($risk->feedback->sortByDesc('created_at') as $feedback)
                    <div class="row mb-2 p-r-2">
                       <div class="col-md-2 text-center">
                           <img src="{{route('risk.feedback.avatar', $feedback->user_id)}}" height="48" width="48"  style="border-radius: 100%">
                       </div>
                        <div class="col-md-10 bg-gray-light rounded-lg p-2">
                            {!! $feedback->feedback !!}
                            <hr class="my-0">
                            <span class="float-right mt-1"><p class="mb-0" style="font-size: 0.8rem"><i class="far fa-clock"></i> {{$feedback->created_at}}</p></span>
                        </div>
                    </div>
                    @empty
                @endforelse
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    {{-- <script>
        $(function () {

            $('#risk_area').change(function(){
                let risk_area_id = $("select[name=risk_area]").val();

                if(risk_area_id == ''){
                    
                    $('#risk_item_1').show();
                        // $('#risk_item_2').hide();
                        $('#risk_item_2').hide();
                } else {
                    axios.get('/risk/getriskitem/'+risk_area_id, {})
                        .then((response) => {
                            if(risk_area_id != 4){
                                // $('#risk_item_2').show();
                                $('#risk_item_2').show();
                                $('#risk_item_1').hide();

                                const entries = Object.entries(response.data);
                                let options = '';// '<option>Please select...</option>';
                                entries.forEach(value => {
                                    options += '<option value="'+value[0]+'">'+value[1]+'</option>';
                                });

                                $('#risk_item_2').html(options);
                            } else {
                                $('#risk_item_1').show();
                                // $('#risk_item_2').hide();
                                $('#risk_item_2').hide();
                            }
                            // console.log('Response', response);
                        })
                        .catch(function () {
                            toastr.error('<strong>Error!</strong> An Error occured!!!');

                            toastr.options.timeOut = 1000;
                            console.log("An Error occured!!!");
                        });
                    }
            });
        });
    </script> --}}
    <script src="{!! asset('tinymce2/js/tinymce/tinymce.min.js') !!}"></script>
    <script>
    </script>
    <script>
        $(function (){

            $('.chosen-container').css('width', '100%');

            $('#risk_area').change(function(){
                let risk_area_id = $("select[name=risk_area]").val();

                if(risk_area_id == ''){
                    
                    $('#risk_item_1').show();
                        // $('#risk_item_2').hide();
                        $('#risk_item_2').hide();
                } else {
                    axios.get('/risk/getriskitem/'+risk_area_id, {})
                    .then((response) => {
                        if(risk_area_id != 4){
                            // $('#risk_item_2').show();
                            $('#risk_item_2').show();
                            $('#risk_item_1').hide();

                            const entries = Object.entries(response.data);
                            let options = '';// '<option>Please select...</option>';
                            entries.forEach(value => {
                                options += '<option value="'+value[0]+'">'+value[1]+'</option>';
                            });

                            $('#risk_item_2').html(options);
                        } else {
                            $('#risk_item_1').show();
                            $('#risk_item_1').val('');
                            // $('#risk_item_2').hide();
                            $('#risk_item_2').hide();
                        }
                        // console.log('Response', response);
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });
                }
            });

        tinymce.init({
            selector:'textarea#feedback',
            width: '100%',
            height: 300
        });
            $("#send_feedback").on('click', function (){

                $(this).html(
                    '<div class="spinner-grow spinner-grow-sm" role="status">' +
                    '  <span class="sr-only">Loading...</span>' +
                    '</div> Loading...'
                );

                let feedback = tinymce.get('feedback').getContent();
                let notification = '';
                if ($("#send_no").is(":checked"))
                    notification = false;
                if($("#send_yes").is(":checked"))
                    notification = true;
                if (feedback =="") {
                    $("#empty-feedback").show()
                    setTimeout(() => $("#empty-feedback").hide(), 2000);
                    return;
                }

                axios.post('/risk/feedback/{{$risk->id}}', {
                    feedback: feedback,
                    notification: notification
                })
                    .then(function (response) {
                        tinymce.get('feedback').setContent('');
                        let res_feedback = response.data.feedback;
                        $("#all-feedback").prepend(
                            '<div class="row mb-2 p-r-2">'+
                                '<div class="col-md-2 text-center">' +
                                    '<img src="/risk/feedback/avatar/'+res_feedback.user_id+'" height="48" width="48"  style="border-radius: 100%">'+
                                '</div>'+
                                '<div class="col-md-10 bg-gray-light rounded-lg p-2">'+
                                    res_feedback.feedback +
                                    '<hr class="my-0">' +
                                    '<span class="float-right mt-1"><p class="mb-0" style="font-size: 0.8rem"><i class="far fa-clock"></i> '+res_feedback.created_at+'</p></span>' +
                                '</div>' +
                            '</div>'
                        );
                        $("#send_feedback").html(
                            '<i class="far fa-comments"></i> Send Feedback'
                        );
                        console.log(response.data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
        });
    </script>
@endsection