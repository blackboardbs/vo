@extends('adminlte.default')
@section('title') View Risk @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <span class="float-right">
            <a href="{{route('risk.edit',$risk)}}" class="btn btn-success float-right ml-1">Edit</a>
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        </span>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {!! $riskClassification !!}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $risk->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'placeholder' => 'Name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Description:</th>
                    <td>
                        {{Form::text('description' , $risk->description, ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'id'=>'description', 'placeholder' => 'Description', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('description') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $risk->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Risk Date:</th>
                    <td>
                        {{Form::text('risk_date', $risk->risk_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('risk_date') ? ' is-invalid' : ''), 'id'=>'risk_date', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('risk_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Risk Area:</th>
                    <td>
                        {{Form::select('risk_area', $riskAreaDropDown, $risk->risk_area_id, ['class'=>'form-control form-control-sm'. ($errors->has('risk_area') ? ' is-invalid' : ''), 'id' => 'risk_area', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('risk_area') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Risk Item:</th>
                    <td>
                        @if($risk->risk_area_id == 4)
                            {{Form::text('risk_item', $risk->risk_item, ['class'=>'form-control form-control-sm'. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_1', 'disabled' => 'disabled'])}}
                        @else
                            {{Form::select('risk_item[]', $riskItemDropDown, $risk_items, ['class'=>'form-control form-control-sm '. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_2', 'disabled' => 'disabled', 'multiple ' => 'multiple '])}}
                        @endif
                        @foreach($errors->get('risk_item') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Resposibility:</th>
                    <td>
                        {{Form::select('resposibility_user_id', $usersDropDown, $risk->resposibility_user_id, ['class'=>'form-control form-control-sm'. ($errors->has('resposibility_user_id') ? ' is-invalid' : ''), 'id' => 'resposibility_user_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('resposibility_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Accountability:</th>
                    <td>
                        {{Form::select('accountablity_user_id', $usersDropDown, $risk->accountablity_user_id, ['class'=>'form-control form-control-sm'. ($errors->has('accountablity_user_id') ? ' is-invalid' : ''), 'id' => 'accountablity_user_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('accountablity_user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Likelihood:</th>
                    <td>
                        {{Form::select('likelihood_id', $likelyDropDown, $risk->likelihood_id, ['class'=>'form-control form-control-sm'. ($errors->has('likelihood_id') ? ' is-invalid' : ''), 'id' => 'likelihood_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('likelihood_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Impact:</th>
                    <td>
                        {{Form::select('impact_id', $impactDropDown, $risk->impact_id, ['class'=>'form-control form-control-sm '. ($errors->has('impact_id') ? ' is-invalid' : ''),'id'=>'impact_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('impact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Security:</th>
                    <td>
                        {{Form::select('security_id', $securityDropDown, $risk->security_id, ['class'=>'form-control form-control-sm '. ($errors->has('security_id') ? ' is-invalid' : ''),'id'=>'security_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('security_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $risk->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Risk Mitigation Strategy:</th>
                    <td>
                        {{Form::text('risk_mitigation_strategy', $risk->risk_mitigation_strategy, ['class'=>'form-control form-control-sm'. ($errors->has('risk_mitigation_strategy') ? ' is-invalid' : ''), 'id' => 'risk_mitigation_strategy', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('risk_mitigation_strategy') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('.chosen-container').css('width', '100%');
            // $('#risk_item_2_chosen').hide();

            $('#risk_area').change(function(){
                let risk_area_id = $("select[name=risk_area]").val();

                axios.get('/risk/getriskitem/'+risk_area_id, {})
                    .then((response) => {
                        if(risk_area_id != 4){
                            // $('#risk_item_2').show();
                            $('#risk_item_2_chosen').show();
                            $('#risk_item_1').hide();

                            const entries = Object.entries(response.data);
                            let options = '';// '<option>Please select...</option>';
                            entries.forEach(value => {
                                options += '<option value="'+value[0]+'">'+value[1]+'</option>';
                            });

                            $('#risk_item_2').html(options);
                            $('#risk_item_2').trigger("chosen:updated");
                        } else {
                            $('#risk_item_1').show();
                            // $('#risk_item_2').hide();
                            $('#risk_item_2_chosen').hide();
                        }
                        // console.log('Response', response);
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });
            });
        });
    </script>
@endsection