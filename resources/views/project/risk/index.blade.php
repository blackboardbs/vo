@extends('adminlte.default')
@section('title') Risk @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('risk.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Risk</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name','Name')</th>
                    <th>@sortablelink('description','Description')</th>
                    <th>@sortablelink('project.name','Project')</th>
                    <th>@sortablelink('risk_date','Risk Date')</th>
                    <th>@sortablelink('riskarea.name','Risk Area')</th>
                    {{--<th>Risk Item</th>--}}
                    <th>@sortablelink('status.name','Status')</th>
                    <th class="text-right last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($risks as $risk)
                    <tr>
                        <td><a href="{{route('risk.show',$risk)}}">{{$risk->name}}</a></td>
                        <td>{{$risk->description}}</td>
                        <td>{{$risk->project?->name}}</td>
                        <td>{{$risk->risk_date}}</td>
                        <td>{{$risk->riskarea?->name}}</td>
                        <td>{{$risk->status?->description}}</td>
                        <td class="text-right">
                            <div class="d-flex">
                                <a href="{{route('risk.create')}}?epic_id={{$risk->id}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                <a href="{{route('risk.edit',$risk)}}" class="btn btn-success btn-sm ml-1 mr-1"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['risk.destroy', $risk],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" class="text-center">No risk entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr> 
                        <td style="vertical-align: center;">
                            {{ Form::open(['method' => 'GET','route' => ['risk.index'],'class'=>'item_filter']) }}
                                Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $risks->firstItem() }} - {{ $risks->lastItem() }} of {{ $risks->total() }}
                            {{ Form::close() }}
                        </td>
                        <td>
                            {{ $risks->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function(){
            $('#r').change(function(){
                $('.item_filter').submit();
            });
        });
    </script>
@endsection