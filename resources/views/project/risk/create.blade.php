@extends('adminlte.default')
@section('title') Add Risk @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('createRisk')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('risk.store'), 'method' => 'post', 'class'=>'mt-3','id'=>'createRisk'])}}
            <input type="hidden" id="parent_start_date" name="parent_start_date" value="{{isset($project->start_date) ? $project->start_date: ''}}" />
            <input type="hidden" id="parent_end_date" name="parent_end_date" value="{{isset($project->end_date) ? $project->end_date : ''}}" />
            <input type="hidden" id="parent_name" name="parent_name" value="Project" />
            <input type="hidden" id="child_name" name="child_name" value="Epic" />
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Name:</th>
                        <td>
                            {{Form::text('name', old('name'), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'placeholder' => 'Name', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Description:</th>
                        <td>
                            {{Form::text('description' , old('description'), ['class'=>'form-control form-control-sm'. ($errors->has('description') ? ' is-invalid' : ''),'id'=>'description', 'placeholder' => 'Description', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Project:</th>
                        <td>
                            {{Form::select('project_id', $projectDropDown, old('project_id'), ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('project_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Risk Date:</th>
                        <td>
                            {{Form::text('risk_date', old('risk_date'), ['class'=>'form-control form-control-sm datepicker'. ($errors->has('risk_date') ? ' is-invalid' : ''), 'id'=>'risk_date', 'autocomplete' => 'off', 'placeholder' => 'Date'])}}
                            @foreach($errors->get('risk_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Risk Area:</th>
                        <td>
                            {{Form::select('risk_area', $riskAreaDropDown, old('risk_area'), ['class'=>'form-control form-control-sm'. ($errors->has('risk_area') ? ' is-invalid' : ''), 'id' => 'risk_area', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('risk_area') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Risk Item:</th>
                        <td>
                            {{Form::text('risk_item1', (old('risk_area') == 4 ? old('risk_item1') : null), ['class'=>'form-control form-control-sm'. ($errors->has('risk_item1') ? ' is-invalid' : ''),'id'=>'risk_item_1', 'placeholder' => 'Risk Item'])}}
                            @foreach($errors->get('risk_item') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::select('risk_item[]', $riskItemDropDown, old('risk_item'), ['class'=>'form-control form-control-sm '. ($errors->has('risk_item') ? ' is-invalid' : ''),'id'=>'risk_item_2', 'placeholder' => 'Please select...', 'style' => 'display: none;', 'multiple ' => 'multiple '])}}
                            @foreach($errors->get('risk_item') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Resposibility:</th>
                        <td>
                            {{Form::select('resposibility_user_id', $usersDropDown, old('resposibility_user_id'), ['class'=>'form-control form-control-sm'. ($errors->has('resposibility_user_id') ? ' is-invalid' : ''), 'id' => 'resposibility_user_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('resposibility_user_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Accountability:</th>
                        <td>
                            {{Form::select('accountablity_user_id', $usersDropDown, old('accountablity_user_id'), ['class'=>'form-control form-control-sm'. ($errors->has('accountablity_user_id') ? ' is-invalid' : ''), 'id' => 'accountablity_user_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('accountablity_user_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Likelihood:</th>
                        <td>
                            {{Form::select('likelihood_id', $likelyDropDown, old('likelihood_id'), ['class'=>'form-control form-control-sm'. ($errors->has('likelihood_id') ? ' is-invalid' : ''), 'id' => 'likelihood_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('likelihood_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Impact:</th>
                        <td>
                            {{Form::select('impact_id', $impactDropDown, old('impact_id'), ['class'=>'form-control form-control-sm '. ($errors->has('impact_id') ? ' is-invalid' : ''),'id'=>'impact_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('impact_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Security:</th>
                        <td>
                            {{Form::select('security_id', $securityDropDown, old('security_id'), ['class'=>'form-control form-control-sm '. ($errors->has('security_id') ? ' is-invalid' : ''),'id'=>'security_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('security_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status:</th>
                        <td>
                            {{Form::select('status_id', $statusDropDown, old('status_id'), ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'placeholder' => 'Please select...'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Risk Mitigation Strategy:</th>
                        <td>
                            {{Form::text('risk_mitigation_strategy',  old('risk_mitigation_strategy'), ['class'=>'form-control form-control-sm'. ($errors->has('risk_mitigation_strategy') ? ' is-invalid' : ''), 'id' => 'risk_mitigation_strategy'])}}
                            @foreach($errors->get('risk_mitigation_strategy') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js') 

    <script>
        $(function () {
            $('.chosen-container').css('width', '100%');
            $('#risk_item_2').hide();

            $('#risk_area').change(function(){
                let risk_area_id = $("select[name=risk_area]").val();

                if(risk_area_id == ''){
                    
                    $('#risk_item_1').show();
                        // $('#risk_item_2').hide();
                        $('#risk_item_2').hide();
                } else {
                    axios.get('/risk/getriskitem/'+risk_area_id, {})
                    .then((response) => {
                        if(risk_area_id != 4){
                            // $('#risk_item_2').show();
                            $('#risk_item_2').show();
                            $('#risk_item_1').hide();

                            const entries = Object.entries(response.data);
                            let options = '';// '<option>Please select...</option>';
                            entries.forEach(value => {
                                options += '<option value="'+value[0]+'">'+value[1]+'</option>';
                            });

                            $('#risk_item_2').html(options);
                        } else {
                            $('#risk_item_1').show();
                            // $('#risk_item_2').hide();
                            $('#risk_item_2').hide();
                        }
                        // console.log('Response', response);
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });
                }
            });
        });
    </script>
@endsection