@extends('adminlte.default')
@section('title') Project @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{--@if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))--}}
        @if($can_create)
        <a href="{{route('project.create')}}" class="btn btn-dark float-right" ><i class="fa fa-plus"></i> Project</a>
            <x-export route="project.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) ? $_GET['r'] : '15')}}" />
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('company_id', $company_drop_down, old('company_id', 0), ['class' => 'form-control w-100 search', 'id' => 'company_id', 'placeholder' => 'All'])}}
                <span>Company</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('customer_id', $customer_drop_down, old('customer_id', 0), ['class' => 'form-control w-100 search', 'id' => 'customer_id', 'placeholder' => 'All'])}}
                <span>Customer</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        
                        {{Form::select('resource_id', $resource_drop_down, request()->resource_id, ['class'=>'form-control search w-100', 'placeholder' => 'All'])}}
                        
                <span>Resource</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('status_id', $project_status_dropdown, request()->status_id, ['class' => 'form-control w-100 search', 'id' => 'status_id', 'placeholder' => 'All'])}}
                <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="start_date" value="{{isset($_GET['start_date']) ? $_GET['start_date'] : ''}}" class="datepicker start_date form-control col-sm-12 search w-100">
                {{-- {{Form::text('start_date',null,['class'=>'datepicker start_date form-control col-sm-12 search'. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date','style'=>'width:100% !important;'])}} --}}
                <span>Start Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="end_date" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : ''}}" class="datepicker end_date form-control col-sm-12 search w-100">
                {{-- {{Form::text('start_date',null,['class'=>'datepicker start_date form-control col-sm-12 search'. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date','style'=>'width:100% !important;'])}} --}}
                <span>End Date</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('active_only', [0 => 'No', 1 => 'Yes'], request()->active_only, ['class' => 'form-control w-100 search', 'id' => 'active_only'])}}
                <span>Active Only</span>
                    </label>
                </div>
            </div>
            {{-- <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="checkbox" name="active_only[]" class="form-check-input search" value="1" {{ request()->has('active_only') ? 'checked' : '' }}  />
                        Active only
                    </label>
                </div>
            </div> --}}
            <div class="col-sm-3 col-sm pt-3">
                <a href="{{route('project.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('customer.customer_name', 'Customer')</th>
                    <th>@sortablelink('name','Project')</th>
                    <th>@sortablelink('ref', 'Project ref')</th>
                    <th>@sortablelink('start_date', 'Start')</th>
                    <th>@sortablelink('end_date', 'End')</th>
                    <th class="text-right">Hours Assigned</th>
                    <th class="text-right">Actual Bill Hours</th>
                    <th class="text-right">Actual Non-Bill Hours</th>
                    <th class="text-right">Hours Remaining</th>
                    <th class="text-right">Completion %</th>
                    <th>@sortablelink('status.description','Project Status')</th>
                    {{--@if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))--}}
                    @if($can_update)
                    <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($projects as $project)
                    <tr>
                        <td><a href="{{route('project.show',$project)}}">{{isset($project->customer->customer_name)? $project->customer->customer_name: 'Customer' }}</a></td>
                        <td>{{$project->name}}</td>
                        <td>{{$project->ref}}</td>
                        <td>{{$project->start_date}}</td>
                        <td>{{$project->end_date}}</td>
                        <td class="text-right">{{number_format($project->assignment->sum('hours'),2)}}</td>
                        <td class="text-right">{{isset($worked_bill_hours[$project->id]) ? number_format($worked_bill_hours[$project->id],2) : number_format(0,2)}}</td>
                        <td class="text-right">{{isset($worked_non_bill_hours[$project->id]) ? number_format($worked_non_bill_hours[$project->id],2) : number_format(0,2)}}</td>
                        <td class="text-right">{{number_format($project->project_type_id == 1 ? $project->assignment->sum('hours') - (($worked_bill_hours[$project->id] ?? 0)) : $project->assignment->sum('hours') - (($worked_bill_hours[$project->id] ?? 0) + ($worked_non_bill_hours[$project->id] ?? 0)),2)}}</td>
                        <td class="text-right">
                            {{number_format($percentage[$project->id]??0,2)}}%
                        </td>
                        <td>{{$project->status?->description }}</td>
                        @if($can_create)
                        <td>
                            <div class="d-flex">
                            <a href="{{route('task.index')}}?project_id={{$project->id}}" class="btn btn-info btn-sm"><i class="fa fa-id-card"></i></a>
                            <a href="{{route('project.edit',$project)}}" class="btn btn-success btn-sm ml-1 mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['project.destroy', $project],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm" {{($project->assignment_count > 0 || $project->tasks_count > 0 || $project->timesheets_count > 0 || $project->sprints_count > 0 || $project->risks_count > 0 || $project->epics_count > 0 || $project->invoices_count > 0 || $project->vinvoices_count > 0 ? 'disabled' : '')}}><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No projects match those criteria.</td>
                    </tr>
                @endforelse
                <tr>
                    <th colspan="5" class="text-right">Total</th>
                    <th class="text-right">{{number_format($total_po_hours,2)}}</th>
                    <th class="text-right">{{ number_format(array_sum($worked_bill_hours),2) }}</th>
                    <th class="text-right">{{ number_format(array_sum($worked_non_bill_hours),2) }}</th>
                    <th class="text-right">{{number_format($total_actual,2)}}</th>
                    <th class="text-right">{{number_format($total_percentage,2)}}%</th>
                    <th></th>
                    <th></th>
                </tr>
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('s',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['s']) ? $_GET['s'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'s'])}}  {{ $projects->firstItem() }} - {{ $projects->lastItem() }} of {{ $projects->total() }}
                        </td>
                        <td>
            {{ $projects->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script src="{{asset('js/filters.js')}}"></script>
    <script>
        $(document).on('ready', function (){
            // $('#status_id').on('blur',function(){
            //     alert('fff');
            // })
            // let resource_dropdown = @json(session('resource_dropdown'));

            // $("#resource").dropdownSearch({
            //     name: "resource",
            //     dropdown: resource_dropdown !== null ? resource_dropdown : []
            // });

            // const url = new URL(window.location.toLocaleString());

            // if (url.searchParams.get('resource_id')){
            //     let resource = `<input type="hidden" name="resource_id" value="${url.searchParams.get('resource_id')}" >`;
            //     $("#searchform").append(resource)
            // }


        })
        function clearFilters(){
            window.location = "/plan_exp";
        }
    </script>
@endsection
@section('extra-css')
    <style>
        li.page-item {

display: none;
}

.page-item:first-child,
.page-item:nth-child( 2 ),
.page-item:nth-last-child( 2 ),
.page-item:nth-child( 3 ),
.page-item:nth-last-child( 3 ),
.page-item:nth-child( 4 ),
.page-item:nth-last-child( 4 ),
.page-item:nth-child( 5 ),
.page-item:nth-last-child( 5 ),
.page-item:last-child,
.page-item.active,
.page-item.disabled {

display: block;
}
.pagination{
    padding-left: 10px;
    margin-bottom: 0px !important;
}

.pagination>li:first-child {
border-left:0px !important;
}
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection
