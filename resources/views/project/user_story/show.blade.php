@extends('adminlte.default')

@section('title') View User Story @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('task.create', $userStory->feature->epic->project_id ? $userStory->feature->epic->project_id : -1)}}?us_id={{$userStory->id}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Task</a>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        @if($userStory->status_id != 4)
            <a href="{{route('userstory.edit', $userStory->id)}}" class="btn btn-success float-right" style="margin-right: 10px;"><i class="fa fa-edit"></i> Edit</a>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('userstory.update', $userStory->id), 'method' => 'put','class'=>'mt-3','files'=>true])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $userStory->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Feature:</th>
                    <td>
                        {{Form::select('feature_id', $featureDropDown, $userStory->feature_id, ['class'=>'form-control form-control-sm '. ($errors->has('feature_id') ? ' is-invalid' : ''),'id'=>'feature_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('feature_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $userStory->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $userStory->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Hours Planned:</th>
                    <td>
                        {{Form::text('hours_planned', $userStory->hours_planned, ['class'=>'form-control form-control-sm'. ($errors->has('hours_planned') ? ' is-invalid' : ''), 'id' => 'hours_planned', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('hours_planned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Confidence %:</th>
                    <td>
                        {{Form::text('confidence_percentage', $userStory->confidence_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('confidence_percentage') ? ' is-invalid' : ''),'id'=>'confidence_percentage', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('confidence_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Cost:</th>
                    <td>
                        {{Form::text('estimated_cost', $userStory->estimated_cost, ['class'=>'form-control form-control-sm'. ($errors->has('estimated_cost') ? ' is-invalid' : ''), 'id' => 'estimated_cost', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('estimated_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Estimated Income:</th>
                    <td>
                        {{Form::text('estimated_income', $userStory->estimated_income, ['class'=>'form-control form-control-sm'. ($errors->has('estimated_income') ? ' is-invalid' : ''),'id'=>'estimated_income', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('estimated_income') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable:</th>
                    <td>
                        {{Form::select('billable', [1 => 'Yes', 0 => 'No'], $userStory->billable, ['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : ''), 'id' => 'billable', 'readonly' => 'readonly', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint:</th>
                    <td>
                        {{Form::select('sprint_id', $sprintDropDown, $userStory->sprint_id, ['class'=>'form-control form-control-sm '. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Dependency:</th>
                    <td>
                        {{Form::select('dependency_id', $userStoryDropDown, $userStory->dependency_id, ['class'=>'form-control form-control-sm'. ($errors->has('dependency_id') ? ' is-invalid' : ''), 'id' => 'dependency_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $userStory->status_id, ['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'id' => 'status_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Details:</th>
                    <td colspan="3">
                        {!! $userStory->details !!}
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>
        tinymce.init({
            selector:'textarea.details',
            width: "100%",
            height: 300
        });
    </script>
@endsection
