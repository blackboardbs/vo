@extends('adminlte.default')
@section('title') User Stories @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('userstory.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> User Story</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name','Name')</th>
                    <th>@sortablelink('feature.name','Feature')</th>
                    <th>@sortablelink('start_date','Start Date')</th>
                    <th>@sortablelink('end_date','End Date')</th>
                    <th>@sortablelink('hours_planned','Hours Planned')</th>
                    <th>@sortablelink('confindence_percentage','Confidence %')</th>
                    <th>@sortablelink('estimate_cost','Estimate Cost')</th>
                    <th>@sortablelink('estimate_income','Estimate Income')</th>
                    <th>@sortablelink('billable','Billable')</th>
                    <th>@sortablelink('sprint.name','Sprint')</th>
                    <th>@sortablelink('status.name','Status')</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($userStories as $userStory)
                    <tr>
                        <td><a href="{{route('userstory.show',$userStory->id)}}">{{$userStory->name}}</a></td>
                        <td>{{isset($userStory->feature->name)?$userStory->feature->name:''}}</td>
                        <td>{{isset($userStory->start_date)?$userStory->start_date:''}}</td>
                        <td>{{isset($userStory->end_date)?$userStory->end_date:''}}</td>
                        <td>{{isset($userStory->hours_planned)?$userStory->hours_planned:''}}</td>
                        <td>{{isset($userStory->confidence_percentage)?$userStory->confidence_percentage:''}}</td>
                        <td>{{isset($userStory->estimate_cost)?$userStory->estimate_cost:''}}</td>
                        <td>{{isset($userStory->estimate_income)?$userStory->estimate_income:''}}</td>
                        <td>{{($userStory->billable == 1)?'Yes':'No'}}</td>
                        <td>{{isset($userStory->sprint->name)?$userStory->sprint->name:''}}</td>
                        <td>{{isset($userStory->status->name)?$userStory->status->name:''}}</td>
                        <td class="text-right">
                            {{--<a href="{{route('task.create', -1)}}?user_story_id={{$userStory->id}}" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i> Task</a>--}}
                            @if($userStory->status_id != 4)
                                <a href="{{route('userstory.edit',$userStory->id)}}" class="btn btn-success btn-sm">Edit</a>
                            @endif
                            {{ Form::open(['method' => 'DELETE','route' => ['userstory.destroy', $userStory->id],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No feature entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
