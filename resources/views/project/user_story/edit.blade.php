@extends('adminlte.default')

@section('title') Edit User Story @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('userstory.update', $userStory->id), 'method' => 'put','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <input type="hidden" id="parent_start_date" name="parent_start_date" value="{{isset($feature->start_date) ? $feature->start_date: ''}}" />
            <input type="hidden" id="parent_end_date" name="parent_end_date" value="{{isset($feature->end_date) ? $feature->end_date : ''}}" />
            <input type="hidden" id="parent_name" name="parent_name" value="Feature" />
            <input type="hidden" id="child_name" name="child_name" value="User Story" />
            <input type="hidden" id="project_id" name="project_id" value="{{$project_id}}" />
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $userStory->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Feature:</th>
                    <td>
                        {{Form::select('feature_id', $featureDropDown, $userStory->feature_id, ['class'=>'form-control form-control-sm '. ($errors->has('feature_id') ? ' is-invalid' : ''),'id'=>'feature_id'])}}
                        @foreach($errors->get('feature_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $userStory->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $userStory->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Hours Planned:</th>
                    <td>
                        {{Form::text('hours_planned', $userStory->hours_planned, ['class'=>'form-control form-control-sm'. ($errors->has('hours_planned') ? ' is-invalid' : ''), 'id' => 'hours_planned'])}}
                        @foreach($errors->get('hours_planned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Confidence %:</th>
                    <td>
                        {{Form::text('confidence_percentage', $userStory->confidence_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('confidence_percentage') ? ' is-invalid' : ''),'id'=>'confidence_percentage'])}}
                        @foreach($errors->get('confidence_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Cost:</th>
                    <td>
                        {{Form::text('estimate_cost', $userStory->estimate_cost, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_cost') ? ' is-invalid' : ''), 'id' => 'estimate_cost', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('estimate_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Estimated Income:</th>
                    <td>
                        {{Form::text('estimate_income', $userStory->estimate_income, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_income') ? ' is-invalid' : ''),'id'=>'estimate_income'])}}
                        @foreach($errors->get('estimate_income') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable:</th>
                    <td>
                        {{Form::select('billable', [1 => 'Yes', 0 => 'No'], $userStory->billable, ['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : ''), 'id' => 'billable', 'autocomplete' => 'off', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint:</th>
                    <td>
                        {{Form::select('sprint_id', $sprintDropDown, $userStory->sprint_id, ['class'=>'form-control form-control-sm '. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Dependency:</th>
                    <td>
                        {{Form::select('dependency_id', $userStoryDropDown, $userStory->dependency_id, ['class'=>'form-control form-control-sm'. ($errors->has('dependency_id') ? ' is-invalid' : ''), 'id' => 'dependency_id', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $userStory->status_id, ['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''), 'id' => 'status_id', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Details:</th>
                    <td colspan="3">
                        {!! Form::textarea('details', $userStory->details, ['class'=>'form-control form-control-sm details '. ($errors->has('details') ? ' is-invalid' : ''), 'autocomplete' => 'off']) !!}
                        @foreach($errors->get('dependency_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
    @include('project.alert_modal')
@endsection
