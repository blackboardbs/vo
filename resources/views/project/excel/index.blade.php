<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead>
        <tr class="btn-dark">
            <th>Customer</th>
            <th>Project</th>
            <th>Project ref</th>
            <th>Start</th>
            <th>End</th>
            <th>PO Hours</th>
            <th>Actual Hours</th>
            <th>Completion %</th>
            <th>Project Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{$project->customer?->customer_name}}</td>
                <td>{{$project->name}}</td>
                <td>{{$project->ref}}</td>
                <td>{{$project->start_date}}</td>
                <td>{{$project->end_date}}</td>
                <td>{{$project->assignment?->sum('hours')}}</td>
                <td>{{$project->timesheets?->sum(function ($timesheet){
                            return round($timesheet->timeline?->sum('total') + ($timesheet->timeline?->sum('total_m')/60));
                        })}}</td>
                <td class="text-right">
                    {{$percentage[$project->id]??0}}%
                </td>
                <td>{{$project->status?->description}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>