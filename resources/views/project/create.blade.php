@extends('adminlte.default')
@section('title') Project @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
                <a href="javascript:void()" onclick="saveForm('create_project_frm')" class="btn btn-primary ml-1 mr-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
        <ul class="nav nav-tabs mt-3">
                <li class="">
                    <a href="#basics" class="active" data-toggle="tab">Basics</a>
                </li>
                <li class="">
                    <a href="#header" class="" data-toggle="tab">Header</a>
                </li>
                <li class="">
                    <a href="#customer" class="" data-toggle="tab">Customer</a>
                </li>
                <li class="">
                    <a href="#cost" class="" data-toggle="tab">Cost</a>
                </li>
                <li class="">
                    <a href="#resourcing" class="" data-toggle="tab">Assignments</a>
                </li>
                <li class="">
                    <a href="#expenses" class="" data-toggle="tab">Expenses</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Tasks</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Timesheets</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Epics</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Features</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">User Stories</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Sprints</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Risks</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Actions</a>
                </li>
                <li class="disabled">
                    <a href="javascript:void()" class="">Analysis</a>
                </li>
            </ul>
            <hr style="margin-top: -1px">
            {{Form::open(['url' => route('project.store'), 'method' => 'post','class'=>'mt-3', 'id'=>'create_project_frm','files' => true,'autocomplete'=>'off'])}}
            
            <div class="tab-content">
                <div class="tab-pane fade show active" id="basics">
                    <div class="row py-3">
                        <table class="table table-borderless">
                            <thead>   
                                <tr>
                                    <th colspan="4" class="btn-dark" style="text-align: center;">Basics</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    {{-- <th class="col-md-2 p-2" ><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Enter a short descriptive project name"></i>Project Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> </th> --}}
                                    <th class="col-md-2 p-2" >
                                        Project Name: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Enter a short descriptive project name"><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">
                                        <div style="display: inline-block;">
                                            {{Form::text('name',null,['class'=>'form-control form-control-sm  col-sm-11'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name'])}}
                                        </div>
                                        
                                        @foreach($errors->get('name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Enter a project reference. This code will display on the kanban board and is used as a quick internal reference. The shorter the better. E.g. AB123"></i>Project Ref. (Timesheet): <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Project Ref. (Timesheet): 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Enter a project reference. This code will display on the kanban board and is used as a quick internal reference. The shorter the better. E.g. AB123"><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">
                                        {{Form::text('ref',null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref'])}}
                                        @foreach($errors->get('ref') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select Income for customer billable projects. Select Cost for internal projects."></i>Project Type: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Project Type: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select Income for customer billable projects. Select Cost for internal projects."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">{{Form::select('project_type_id',$project_type_dropdown,1,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('project_type_id') ? ' is-invalid' : ''), 'placeholder'=>'Please Select'])}}
                                        @foreach($errors->get('project_type_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the internal owner for the project from a list of users. The internal owner will automatically be added as an optional approver of timesheets for the project and will have access to view the project record."></i>Internal Owner: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Internal Owner: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the internal owner for the project from a list of users. The internal owner will automatically be added as an optional approver of timesheets for the project and will have access to view the project record."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">{{Form::select('manager_id',$users_dropdown,null,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('manager_id') ? ' is-invalid' : ''), 'id' =>'internal_owner_id', 'placeholder' => 'Select Internal Owner'])}}
                                        @foreach($errors->get('manager_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The default customer billable flag. Set the flag for billable to Yes if a customer must be billed for tasks performed by default. Set the flag to No for non-billable or internal projects."></i>Billable: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Billable: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The default customer billable flag. Set the flag for billable to Yes if a customer must be billed for tasks performed by default. Set the flag to No for non-billable or internal projects."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">
                                        {{Form::select('billable',$yes_or_no_dropdown,old('billable', 1),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('billable') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="The status of a project determines if it needs to be available for planning and time capture. Only projects with status In Progress or Confirmed are available for time capturing."></i>Project Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Project Status: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="The status of a project determines if it needs to be available for planning and time capture. Only projects with status In Progress or Confirmed are available for time capturing."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">
                                        {{Form::select('status_id',$project_status_dropdown,old('status_id', 3),['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                                        @foreach($errors->get('status_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Project Start date"></i>Start Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Start Date: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Project Start date"><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">{{Form::text('start_date',old('start_date'),['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                                        @foreach($errors->get('start_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Project End date"></i>End Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        End Date: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Project End date"><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">{{Form::text('end_date',old('end_date'),['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                                        @foreach($errors->get('end_date') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>

                                <tr>
                                    {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the customer for the project. Create a customer for the company, if it is an internal project."></i>Customer: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                    <th class="col-md-2 p-2" >
                                        Customer: 
                                        <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the customer for the project. Create a customer for the company, if it is an internal project."><i class="far fa-question-circle"></i></span></sup>
                                        <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                    </th>
                                    <td class="col-md-4 p-2">{{Form::select('customer_id',$customer_dropdown,null,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('customer_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Customer'])}}
                                        @foreach($errors->get('customer_id') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="header">
                    <div class="row py-3">
                        <table class="table table-borderless">
                            <thead>   
                                <tr>
                                    <th colspan="4" class="btn-dark" style="text-align: center;">Header</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the company if the system is setup for multiple companies. The default company will be selected and can be setup on the Config page by the System Administrator."></i>Company: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th> --}}
                                <th class="col-md-2 p-2" >
                                    Company: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the company if the system is setup for multiple companies. The default company will be selected and can be setup on the Config page by the System Administrator."><i class="far fa-question-circle"></i></span></sup>
                                    <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span> 
                                </th>
                                <td class="col-md-4 p-2">{{Form::select('company_id',$company_dropdown,$config->company_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('company_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="This is a text field that will print on the project agreement. The default value is 'Max 8 hours per day'."></i>Hours of Work Per Day:</th> --}}
                                <th class="col-md-2 p-2" >
                                    Hours of Work Per Day: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="This is a text field that will print on the project agreement. The default value is 'Max 8 hours per day'."><i class="far fa-question-circle"></i></span></sup>
                                </th>
                                <td class="col-md-4 p-2">{{Form::text('hours_of_work',$terms->exp_hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''),'placeholder'=>'0'])}}
                                    @foreach($errors->get('hours_of_work') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                {{-- <th class="col-md-2 p-2">
                                    <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="This is the default standard weekly layout of timesheets when printed. Note that the layout can be selected at point of printing the timesheet."></i>Default Timesheet Template
                                    
                                </th> --}}
                                <th class="col-md-2 p-2" >
                                    Default Timesheet Template: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="This is the default standard weekly layout of timesheets when printed. Note that the layout can be selected at point of printing the timesheet."><i class="far fa-question-circle"></i></span></sup>
                                </th>
                                <td class="col-md-4 p-2">
                                    {{Form::select('timesheet_template_id', $timesheet_template_dropdown, old('timesheet_template_id'), ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_template_id') ? ' is-invalid' : ''), 'placeholder' => 'Select Template'])}}
                                    @foreach($errors->get('timesheet_template_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="This is the default custom timesheet layout for billing periods. Setup a custom timesheet layout under master data / Template Styles. This layout will combine multiple weeks that belongs to the same billing period."></i>Billing Timesheet</th> --}}
                                <th class="col-md-2 p-2" >
                                    Billing Timesheet: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="This is the default custom timesheet layout for billing periods. Setup a custom timesheet layout under master data / Template Styles. This layout will combine multiple weeks that belongs to the same billing period."><i class="far fa-question-circle"></i></span></sup>
                                </th>
                                <td class="col-md-4 p-2">{{Form::select('billing_timesheet_templete_id', $billing_period_template_dropdown, null, ['class'=>'form-control form-control-sm '. ($errors->has('billing_timesheet_templete_id') ? ' is-invalid' : ''), 'placeholder' => 'Billing Period Template'])}}
                                    @foreach($errors->get('billing_timesheet_templete_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>

                                {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Where will the project be executed from. E.g. Remote or Dubai office"></i>Location:</th> --}}
                                <th class="col-md-2 p-2" >
                                    Location: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Where will the project be executed from. E.g. Remote or Dubai office"><i class="far fa-question-circle"></i></span></sup>
                                </th>
                                <td class="col-md-4 p-2">{{Form::text('location',old('location'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('location') ? ' is-invalid' : ''),'placeholder'=>'Location'])}}
                                    @foreach($errors->get('location') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Text that explains the high level scope of the project."></i>Project Scope</th> --}}
                                <th class="col-md-2 p-2" >
                                    Project Scope: 
                                    <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Text that explains the high level scope of the project."><i class="far fa-question-circle"></i></span></sup>
                                </th>
                                <td class="col-md-4 p-2">{{Form::textarea('scope',old('scope'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('scope') ? ' is-invalid' : ''),'placeholder'=>'Project Scope', 'rows' => 3])}}
                                    @foreach($errors->get('scope') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="customer">
                    <div class="row py-3">
                    <table class="table table-borderless">
                        <thead>   
                            <tr>
                                <th colspan="4" class="btn-dark" style="text-align: center;">Header</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>

                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Text field that holds a general optional reference. E.g. RFQ reference, or PR reference"></i>Customer Ref.:</th> --}}
                            <th class="col-md-2 p-2" >
                                Customer Ref: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Text field that holds a general optional reference. E.g. RFQ reference, or PR reference"><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="col-md-4 p-2">{{Form::text('customer_ref',old('customer_ref'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Ref No'])}}
                                @foreach($errors->get('customer_ref') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2"></th>
                            <td class="col-md-4 p-2"></td>
                        </tr>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Customer purchase order reference"></i>Customer PO Number:</th> --}}
                            <th class="col-md-2 p-2" >
                                Customer PO Number: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Customer purchase order reference"><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="col-md-4 p-2">{{Form::text('cust_order_number',old('cust_order_number'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('cust_order_number') ? ' is-invalid' : ''),'placeholder'=>'Customer Order Number'])}}
                                @foreach($errors->get('cust_order_number') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2"></th>
                            <td class="col-md-4 p-2"></td>
                        </tr>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the customer invoice contact from the list of contacts. Create contacts under the Company menu."></i>Customer Invoice Contact: </th> --}}
                            <th class="col-md-2 p-2" >
                                Customer Invoice Contact: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the customer invoice contact from the list of contacts. Create contacts under the Company menu."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="col-md-4 p-2">{{Form::select('customer_invoice_contact_id',$invoice_contact_dropdown,null,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('customer_invoice_contact_id') ? ' is-invalid' : ''), 'placeholder' => "Select Customer Invoice Contact"])}}
                                @foreach($errors->get('customer_invoice_contact_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2"></th>
                            <td class="col-md-4 p-2"></td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="cost">
                    <div class="row py-3">
                    <table class="table table-borderless">
                        <thead>   
                            <tr>
                                <th colspan="4" class="btn-dark" style="text-align: center;">Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select a billing cycle that will govern timesheets and invoicing for this project. There are a number of standard monthly billing cycles available. Custom billing cycles can be created under the Company menu."></i>Billing Cycle</th> --}}
                            <th class="col-md-2 p-2" >
                                Billing Cycle: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select a billing cycle that will govern timesheets and invoicing for this project. There are a number of standard monthly billing cycles available. Custom billing cycles can be created under the Company menu."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="col-md-4 p-2">{{Form::select('billing_cycle_id', $billing_cycle_dropdown, null, ['class'=>'form-control form-control-sm '. ($errors->has('billing_cycle_id') ? ' is-invalid' : ''), 'placeholder' => 'Billing Cycle'])}}
                                @foreach($errors->get('billing_cycle_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="A text field for reporting."></i>Opex/Capex:</th> --}}
                            <th class="col-md-2 p-2" >
                                Opex/Capex: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="A text field for reporting."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="col-md-4 p-2">{{Form::text('opex_project',old('opex_project'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('opex_project') ? ' is-invalid' : ''),'placeholder'=>'Opex/Capex'])}}
                                @foreach($errors->get('opex_project') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the vendor invoice contact from the list of contacts. Create contacts under the Company menu. Optional."></i>Vendor Invoice Contact:</th> --}}
                            <th class="col-md-2 p-2" >
                                Vendor Invoice Contact: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the vendor invoice contact from the list of contacts. Create contacts under the Company menu. Optional."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="p-2" colspan="3">{{Form::select('vendor_invoice_contact_id',$invoice_contact_dropdown, old('vendor_invoice_contact_id'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('vendor_invoice_contact_id') ? ' is-invalid' : ''),'placeholder'=>'Select Vendor Invoice Contact'])}}
                                @foreach($errors->get('vendor_invoice_contact_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>

                        </tr>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Text note about billing arrangements. E.g. What needs to be included in the billing pack. Like timesheets and a task summary."></i>Billing Note:</th> --}}
                            <th class="col-md-2 p-2" >
                                Billing Note: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Text note about billing arrangements. E.g. What needs to be included in the billing pack. Like timesheets and a task summary."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="p-2" colspan="3">
                                {{Form::textarea('billing_note',null,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>''])}}
                            </td>
                        </tr>
                        <tr >
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Default is No  - when time and material is billed. Or Yes if there is a fixed income or cost."></i>Project Fixed Price:</th> --}}
                            <th class="col-md-2 p-2" >
                                Project Fixed Price: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Default is No  - when time and material is billed. Or Yes if there is a fixed income or cost."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="p-2">
                                {{Form::select('is_fixed_price', $yes_or_no_dropdown, 1, ['class'=>'form-control form-control-sm'. ($errors->has('is_fixed_price') ? ' is-invalid' : ''), 'onchange' => 'isFixedPrice()', 'id' => 'is_fixed_price', 'placeholder' => 'Select Project Fixed Price'])}}
                                @foreach($errors->get('is_fixed_price') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Total project hours for validation and recon. Project hours can exist on Assignment and Project level."></i>Total Project Hours:</th> --}}
                            <th class="col-md-2 p-2" >
                                Total Project Hours: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Total project hours for validation and recon. Project hours can exist on Assignment and Project level."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td class="p-2">
                                {{Form::number('total_project_hours', old('total_project_hours'), ['step'=>'.01','class'=>'form-control form-control-sm'. ($errors->has('total_project_hours') ? ' is-invalid' : ''), 'id' => 'total_project_hours'])}}
                                @foreach($errors->get('total_project_hours') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr class="projectCostPrices">
                            <th class="p-2" colspan="2">Customer</th>
                            <th class="p-2" colspan="2">Vendor</th>
                        </tr>
                        <tr class="projectCostPrices">
                            <th class="col-md-2 p-2">Project Fixed Price - Expenses</th>
                            <td class="col-md-4 p-2">{{Form::text('fixed_price_expenses', old('fixed_price_expenses'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_expenses') ? ' is-invalid' : ''),'placeholder'=>'Fixed Price Expenses'])}}
                                @foreach($errors->get('fixed_price_expenses') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">
                                Project Fixed Price  Cost - Expenses
                            </th>
                            <td class="col-md-4 p-2">{{Form::text('project_fixed_cost_expense', old('project_fixed_cost_expense'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_expense') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Cost Expense'])}}
                                @foreach($errors->get('project_fixed_cost_expense') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr class="projectCostPrices">
                            <th class="col-md-2 p-2">
                                Project Fixed Price - Labour
                            </th>
                            <td class="projectCostPrices col-md-4 p-2">{{Form::text('fixed_price_labour', old('fixed_price_labour'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_labour') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Price Labour'])}}
                                @foreach($errors->get('fixed_price_labour') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Project Fixed Price  Cost - Labour</th>
                            <td class="col-md-4 p-2">{{Form::text('project_fixed_cost_labour', old('project_fixed_cost_labour'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_labour') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Cost Labour'])}}
                                @foreach($errors->get('project_fixed_cost_labour') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr class="projectCostPrices">
                            <th class="col-md-2 p-2">
                                Project Fixed Price - Total
                            </th>
                            <td class="col-md-4 p-2">{{Form::text('total_fixed_price', old('total_fixed_price'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('total_fixed_price') ? ' is-invalid' : ''),'placeholder'=>'Project Total Fixed Price'])}}
                                @foreach($errors->get('total_fixed_price') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Project Fixed Price  Cost - Total</th>
                            <td class="col-md-4 p-2">{{Form::text('project_total_fixed_cost', old('project_total_fixed_cost'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_total_fixed_cost') ? ' is-invalid' : ''),'placeholder'=>'Project Total Fixed Cost'])}}
                                @foreach($errors->get('project_total_fixed_cost') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="resourcing">
                    <div class="row py-3" style="display: block">
                        <blackboard-assignment 
                        :project="{{ $project ?? 0 }}" 
                        :project-assignments="{{ $project->assignment ?? '{}' }}" 
                        :unassigned-consultants="{{ isset($project) ? $project->consultantsWithoutAssignment($project->assignment) : '{}' }}"
                        :users-dropdown="{{ $resource_dropdown }}"
                        :roles-dropdown="{{ $roles_drop_down }}"></blackboard-assignment>
                    </div>
                </div>
                <div class="tab-pane fade" id="expenses">
                    <div class="row py-3">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <th colspan="4" class="btn-dark" style="text-align: center;">Billable Expenses</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            {{-- <th class="col-md-2 p-2"><i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Select the claimable terms by consultants for this project. The Terms can be maintained under master data and will default on individual assignments linked to this project."></i>Terms:</th> --}}
                            <th class="col-md-2 p-2" >
                                Terms: 
                                <sup><span style="cursor: pointer" class="text-capitalize" data-toggle="tooltip" data-placement="right" title="Select the claimable terms by consultants for this project. The Terms can be maintained under master data and will default on individual assignments linked to this project."><i class="far fa-question-circle"></i></span></sup>
                            </th>
                            <td colspan="3" class="p-2">
                                {{Form::select('terms_version',$project_terms_dropdown,old('terms_version'),['id'=>'terms_version', 'class'=>'form-control ', 'onchange' => 'changeTerms()'])}}
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2 p-2">Travel:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('travel',$terms->exp_travel??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12', 'id' => 'travel'])}}
                                @foreach($errors->get('travel') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Parking:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('parking',$terms->exp_parking??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'parking'])}}
                                @foreach($errors->get('parking') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2 p-2">Car Rental:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('car_rental',$terms->exp_car_rental??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12', 'id' => 'car_rental'])}}
                                @foreach($errors->get('car_rental') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Flights:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('flights',$terms->exp_flights??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'flights'])}}
                                @foreach($errors->get('flights') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2 p-2">Toll:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('toll',$terms->exp_toll??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'toll'])}}
                                @foreach($errors->get('toll') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Accommodation:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('accommodation',$terms->exp_accommodation??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'accommodation'])}}
                                @foreach($errors->get('accommodation') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2 p-2">Per Diem:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('per_diem',$terms->exp_per_diem??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'per_diem'])}}
                                @foreach($errors->get('per_diem') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th class="col-md-2 p-2">Out of Town Allowance:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('out_of_town',$terms->exp_out_of_town??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'out_of_town'])}}
                                @foreach($errors->get('out_of_town') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="col-md-2 p-2">Data:</th>
                            <td class="col-md-4 p-2">{{Form::textarea('data',$terms->exp_data??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'data'])}}
                                @foreach($errors->get('data') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th>Other:</th>
                            <td class="p-2" colspan="3">{{Form::textarea('other',$terms->exp_other??'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'other'])}}
                                @foreach($errors->get('other') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>

                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(document).ready(function (){
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $(function () {
                $('#is_fixed_price').on('change',function(){
                    let is_fixed_price = $("select[name=is_fixed_price]").val();
                    if(is_fixed_price == 0){
                        $('.projectCostPrices').hide();
                    } else {
                        $('.projectCostPrices').show();
                    }
                });

                let consultants = $("#consultants");
                const tfoot = $("#consultant_roles");

                consultants.on('change', () => {
                    let trow = "<tr>"
                    consultants.val()?.forEach(consultant => {
                        trow += `<th>Consultant</th>
                                 <th><input type="text" class="form-control-sm form-control form-control-plaintext" value="${getFullName(consultant)}"></th>
                                 <th>Role</th>
                                 <td>${buildRolesSelect(consultant)}</td>
                                 </tr>`;
                    })
                    tfoot.html(trow)

                    if (consultants.val().length){
                        tfoot.show()
                    }else {
                        tfoot.hide()
                    }

                })

                function getFullName(user_id) {
                    const resources = @json($resource_dropdown);
                    let selected_user = [];
                    resources?.forEach(resource => {
                        if (resource.id == user_id){
                            selected_user = resource.full_name
                        }
                    })
                    return selected_user;
                }

                function buildRolesSelect(user_id){
                    const roles = Object.entries(@json($roles_dropdown))?.map(v => {
                        return {
                            id: v[0],
                            name: v[1]
                        }
                    });

                    let select = `<select class="form-control form-control-sm" name="consultant_role_${user_id}" placeholder="Select Role">`;

                    roles.forEach(role => {
                        select += `<option value="${role.id}" ${role.id == 2 ? 'selected': ''}>${role.name}</option>`
                    })

                    return select += "</select>"
                }
            })
        })
    </script>
@endsection
