@extends('adminlte.default')

@section('title') Show Sprint @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                @if($sprint->status_id != 2)
                    <blackboard-sprint-complete sprint-id="{{$sprint->id}}"></blackboard-sprint-complete>
                    <a href="{{route('sprint.edit', $sprint->id)}}" class="btn btn-success float-right ml-1">Edit</a>
                @endif
                <a href="{{url('/kanban/'.$sprint->project_id)}}?sprint={{$sprint->id}}" class="btn btn-warning float-right ml-1">Kanban Board</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('sprint.store'), 'method' => 'post','class'=>'mt-3','files'=>true])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $sprint->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $sprint->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $sprint->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $sprint->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Sprint Health:</th>
                    <td>
                        <div class="progress">
                            <div class="progress-bar {{$sprint->sprintHealth($summaries_hours, $sprint->goals)?->color}}" role="progressbar" style="width: {{number_format($sprint->sprintHealth($summaries_hours, $sprint->goals)?->progress)}}%" aria-valuenow="{{number_format($sprint->sprintHealth($summaries_hours, $sprint->goals)?->progress)}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </td>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $sprint->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>

        <ul class="nav nav-tabs mt-3">
            <li class="">
                <a href="#goals" class="active" data-toggle="tab">Goals</a>
            </li>
            <li class="">
                <a href="#resources" class="" data-toggle="tab">Resources</a>
            </li>
            <li class="">
                <a href="#analysis" class="" data-toggle="tab">Analysis</a>
            </li>
            <li class="">
                <a href="#sprint-review" class="" data-toggle="tab">Sprint Review</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="goals">
                <hr style="margin-top:-1px;"/>
                @if($notice)
                    <strong class="bg-danger">{{$notice}}</strong>
                @endif
                <table class="table table-sm">
                    <thead class="bg-dark">
                        <tr>
                            <th class="w-25">Goals</th>
                            <th class="text-right">No Of Tasks</th>
                            <th class="text-right">Completed</th>
                            <th class="text-right">Tasks Outstanding</th>
                            <th class="text-right">Overdue Tasks</th>
                            <th class="text-right">Impediments</th>
                            <th class="text-right">Hours Planned</th>
                            <th class="text-right">Actual Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($goals_overview as $goal)
                            <tr>
                                <td>{{$goal->name}}</td>
                                <td class="text-right">{{$goal->no_of_tasks}}</td>
                                <td class="text-right">{{$goal->no_of_completed_tasks}}</td>
                                <td class="text-right {{$goal->color_of_outstatbing}}">{{$goal->no_of_outstanding_tasks}}</td>
                                <td class="text-right {{$goal->color_of_overdue}}">{{$goal->no_of_overdue_tasks}}</td>
                                <td class="text-right">{{$goal->no_of_impendiments}}</td>
                                <td class="text-right">{{$goal->hours_planned}}</td>
                                <td class="text-right {{$goal->color_of_actual_hours}}">{{number_format($goal->actual_hours,2)}}</td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center">This sprint has no goals.</td>
                                </tr>
                        @endforelse
                        <tr>
                            <th>Total</th>
                            <th class="text-right">{{$goals_overview->sum('no_of_tasks')}}</th>
                            <th class="text-right">{{$goals_overview->sum('no_of_completed_tasks')}}</th>
                            <th class="text-right">{{$goals_overview->sum('no_of_outstanding_tasks')}}</th>
                            <th class="text-right">{{$goals_overview->sum('no_of_overdue_tasks')}}</th>
                            <th class="text-right">{{$goals_overview->sum('no_of_impendiments')}}</th>
                            <th class="text-right">{{$goals_overview->sum('hours_planned')}} </th>
                            <th class="text-right">{{number_format($goals_overview->sum('actual_hours'),2)}}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="resources">
                <hr style="margin-top:-1px;"/>
                <table class="table table-sm">
                    <thead class="bg-dark">
                    <tr>
                        <th>Resource</th>
                        <th>Function</th>
                        <th>Capacity (Hours per day)</th>
                        <th class="last">Leave Days</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sprint->resources as $resource)
                        <tr>
                            <td>{{$resource->resource?->name()}}</td>
                            <td>{{$resource->function?->description}}</td>
                            <td>{{$resource->capacity}}</td>
                            <td class="text-right">{{number_format($resource->leave_days, 2)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="sprint-review">
                <hr style="margin-top:-1px;"/>
                <table class="table table-sm">
                    <tbody> 
                        <tr class="bg-dark"><th>What went Well?</th></tr>
                        @forelse($went_well as $key => $review)
                            <tr>
                                <td>{{$key+1}}. {{ $review->body}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td>Nothing to show.</td>
                            </tr>
                        @endforelse
                        <tr class="bg-dark"><th>What went Wrong?</th></tr>
                        @forelse($went_wrong as $key => $review)
                            <tr>
                                <td>{{$key+1}}. {{$review->body}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td>Nothing to show.</td>
                            </tr>
                        @endforelse
                        <tr class="bg-dark"><th>What can we improve?</th></tr>
                        @forelse($needs_improvement as $key => $review)
                            <tr>
                                <td>{{$key+1}}. {{$review->body}}</td>
                            </tr>
                        @empty
                            <tr>
                                <td>Nothing to show.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="analysis">
                <hr style="margin-top:-1px;"/>
                <div class="w-100 bg-dark  mt-3 px-3 py-1">
                    <h5>Burn down chart - Based on the number of Tasks</h5>
                </div>
                <div style="width: 100%; height: 350px">
                    <canvas id="chart1" width="450" height="250"></canvas>
                </div>
                <div class="w-100 bg-dark mt-3 px-3 py-1">
                    <h5>Burn down chart - Based on the hours</h5>
                </div>
                <div style="width: 100%; height: 350px">
                    <canvas id="chart2" width="450" height="250"></canvas>
                </div>

                <div class="w-100 bg-dark mt-3 px-3 py-1">
                    <h5>Sprint Forecast</h5>
                </div>
                <div  style="width: 100%; height: 350px">
                    <canvas id="chart3" width="450" height="250"></canvas>
                </div>

                <table class="table table-sm mt-3">
                    <thead class="bg-dark">
                    <tr><th colspan="100%">Task Summary</th></tr>
                    <tr>
                        <th>Resource</th>
                        <th>No of Tasks</th>
                        <th>Planned Hours</th>
                        <th>Actual Hours</th>
                        <th>Over Due Tasks</th>
                        <th>Backlog</th>
                        <th>Planned</th>
                        <th>In Progress</th>
                        <th>Done</th>
                        <th>Completed</th>
                        <th>Impediment</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($summaries as $summary)
                            <tr>
                                <td>{{$summary->ResourceName}}</td>
                                <td>{{number_format($summary->No_of_Task)}}</td>
                                <td>{{number_format($summary->HoursPlanned, 2)}}</td>
                                <td>{{number_format($summary->ActualHours,2)}}</td>
                                <td>{{number_format($summary->OverdueTasks)}}</td>
                                <td>{{number_format($summary->Backlog)}}</td>
                                <td>{{number_format($summary->Planned)}}</td>
                                <td>{{number_format($summary->InProgress)}}</td>
                                <td>{{number_format($summary->Done)}}</td>
                                <td>{{number_format($summary->Completed)}}</td>
                                <td>{{number_format($summary->Impediments)}}</td>
                            </tr>
                            @empty

                        @endforelse
                        <tr>
                            <th>Total</th>
                            <th>{{number_format($summaries->sum('No_of_Task'))}}</th>
                            <th>{{number_format($summaries->sum('HoursPlanned'), 2)}}</th>
                            <th>{{number_format($summaries->sum('ActualHours'),2)}}</th>
                            <th>{{number_format($summaries->sum('OverdueTasks'),2)}}</th>
                            <th>{{number_format($summaries->sum('Backlog'))}}</th>
                            <th>{{number_format($summaries->sum('Planned'))}}</th>
                            <th>{{number_format($summaries->sum('InProgress'))}}</th>
                            <th>{{number_format($summaries->sum('Done'))}}</th>
                            <th>{{number_format($summaries->sum('Completed'))}}</th>
                            <th>{{number_format($summaries->sum('Impediments'))}}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-sm mt-3">
                    <thead class="bg-dark">
                        <tr><th colspan="100%">Hours Summary</th></tr>
                    <tr>
                        <th>Resource</th>
                        <th>Planned Hours</th>
                        <th>Actual Hours</th>
                        <th>Hours Outstanding</th>
                        <th>Hours Available</th>
                        <th>Potential Outcome</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($summaries_hours as $summary)
                            <tr>
                                <td>{{$summary->ResourceName}}</td>
                                <td>{{number_format($summary->HoursPlanned,2)}}</td>
                                <td>{{number_format($summary->ActualHours,2)}}</td>
                                <td>{{number_format($summary->HoursOutstanding,2)}}</td>
                                <td>{{number_format($summary->HoursAvailable,2)}}</td>
                                <td>{{str_replace('Inufficient hours', 'Insufficient hours',$summary->PotentialOutcomeText)}}</td>
                                <td></td>
                            </tr>
                        @empty

                        @endforelse
                    <tr>
                        <th>Total</th>
                        <th>{{number_format($summaries_hours->sum('HoursPlanned'),2)}}</th>
                        <th>{{number_format($summaries_hours->sum('ActualHours'),2)}}</th>
                        <th>{{number_format(($summaries_hours->sum('HoursOutstanding')),2)}}</th>
                        <th>{{number_format(($summaries_hours->sum('HoursAvailable')),2)}}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
@section('extra-js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        $(document).ready(function (){
            const ctx = document.getElementById('chart1');
            new Chart(ctx, {
                type: 'line',
                data: {
                    labels: @json($chart1['labels']),
                    datasets: [{
                        label: 'Goal',
                        data: @json($chart1['total_hours'])
                    },{
                        label: 'Burn Down',
                        data: @json($chart1['burn_down'])
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });

            const ctx2 = document.getElementById('chart2');
            new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: @json($chart2['labels']),
                    datasets: [{
                        label: 'Goal',
                        data: @json($chart2['total_hours'])
                    },{
                        label: 'Burn Down',
                        data: @json($chart2['burn_down'])
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });

            const ctx3 = document.getElementById('chart3');
            new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: @json($chart3['labels']),
                    datasets: [{
                        label: 'Goal',
                        data: @json($chart3['total_hours'])
                    },{
                        label: 'Actual',
                        data: @json($chart3['task_burn_down'])
                    },{
                        label: 'Capacity',
                        data: @json($chart3['actual_burn_down'])
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                autoSkip: false
                            }
                        }]
                    },
                    legend: {
                        display: false,
                    }
                }
            });
        })
    </script>
@endsection
