@extends('adminlte.default')
@section('title') Sprint @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('sprint.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Sprint</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('project_id', $projectDropdown, request()->project_id,['class'=>'form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Project</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('status_id', $statusDropdown, request()->status_id??1, ['class' => 'form-control w-100 search', 'id' => 'status_id', 'placeholder' => 'All'])}}
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100 search" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('sprint.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>Name</th>
                    <th>Goal</th>
                    <th>Project</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th class="text-right">Planned Hours</th>
                    <th class="text-right">Booked Hours</th>
                    <th class="text-right">Hours%</th>
                    <th class="text-right">Burn%</th>
                    <th>Status</th>
                    <th class="text-right last
                    ">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sprints as $sprint)
                    <tr>
                        <td><a href="{{route('sprint.show',$sprint)}}">{{$sprint->name}}</a></td>
                        <td>{{$sprint->goals?->first()?->name}}</td>
                        <td>{{$sprint->project?->name}}</td>
                        <td>{{$sprint->start_date}}</td>
                        <td>{{$sprint->end_date}}</td>
                        <td class="text-right">{{number_format($sprint->plannedHours(),2)}}</td>
                        <td class="text-right">{{_minutes_to_time($sprint->bookedHours() * 60)}}</td>
                        <td class="text-right">{{number_format($sprint->hoursPerc(), 2)}}%</td>
                        <td class="text-right">{{number_format($sprint->burnedHours(), 2)}}%</td>
                        <td>{{$sprint->status?->description}}</td>
                        <td class="text-right">
                            <div class="d-flex">
                                @if($sprint->status_id == 2)
                                    <a href="javascript:void(0)" class="btn btn-warning btn-sm mr-1"><i class="fa fa-lock" aria-hidden="true"></i></a>
                                @else
                                    <a href="{{route('sprint.edit',$sprint)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['sprint.destroy', $sprint],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                @endif
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No sprints entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $sprints->firstItem() }} - {{ $sprints->lastItem() }} of {{ $sprints->total() }}
                        </td>
                        <td>
                            {{ $sprints->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
@section('extra-js')
    <script src="{{asset('js/filters.js')}}"></script>
    <script>
        $(document).on('ready', function (){
            // $('#status_id').on('blur',function(){
            //     alert('fff');
            // })
            // let resource_dropdown = @json(session('resource_dropdown'));

            // $("#resource").dropdownSearch({
            //     name: "resource",
            //     dropdown: resource_dropdown !== null ? resource_dropdown : []
            // });

            // const url = new URL(window.location.toLocaleString());

            // if (url.searchParams.get('resource_id')){
            //     let resource = `<input type="hidden" name="resource_id" value="${url.searchParams.get('resource_id')}" >`;
            //     $("#searchform").append(resource)
            // }


        })
        function clearFilters(){
            window.location = "/plan_exp";
        }
    </script>
@endsection