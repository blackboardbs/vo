@extends('adminlte.default')

@section('title') Add Sprint @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('createSprint')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('sprint.store'), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'createSprint'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Name:</th>
                        <td>
                            {{Form::text('name', old('name'), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Project:</th>
                        <td>
                            {{Form::select('project_id', $projectDropDown, old('project_id'), ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id'])}}
                            @foreach($errors->get('project_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Start Date:</th>
                        <td>
                            {{Form::text('start_date', old('start_date'), ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>End Date:</th>
                        <td>
                            {{Form::text('end_date', old('end_date'), ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('end_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Goal:</th>
                        <td>
                            {{Form::text('goal', old('goal'), ['class'=>'form-control form-control-sm'. ($errors->has('goal') ? ' is-invalid' : ''), 'id' => 'billable', 'autocomplete' => 'off'])}}
                            @foreach($errors->get('goal') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status:</th>
                        <td>
                            {{Form::select('status_id', $statusDropDown, 1, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
