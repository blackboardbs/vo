@extends('adminlte.default')

@section('title') Edit Sprint @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('editSprint')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('sprint.update', $sprint), 'method' => 'put','class'=>'mt-3','files'=>true,'id'=>'editSprint'])}}
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $sprint->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $sprint->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $sprint->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $sprint->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td>
                        {{Form::select('status_id', $statusDropDown, $sprint->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}

            <ul class="nav nav-tabs mt-3">
                <li class="">
                    <a href="#goals" class="active" data-toggle="tab">Goals</a>
                </li>
                <li class="">
                    <a href="#resources" class="" data-toggle="tab">Resources</a>
                </li>
                <li class="">
                    <a href="#sprint-review" class="" data-toggle="tab">Sprint Review</a>

            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="goals">
                    <hr style="margin-top:-1px;"/>
                    <blackboard-goals :sprint-id="{{(int) $sprint->id}}"></blackboard-goals>
                </div>
                <div class="tab-pane fade" id="resources">
                    <hr style="margin-top:-1px;"/>
                    <blackboard-sprint-resource
                            :sprint-id="{{(int) $sprint->id}}"
                            :project-id="{{(int) $sprint->project_id}}"
                    ></blackboard-sprint-resource>
                </div>
                <div class="tab-pane fade" id="sprint-review">
                    <hr style="margin-top:-1px;"/>
                    <blackboard-sprint-reviews :sprint-id="{{(int) $sprint->id}}"></blackboard-sprint-reviews>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
