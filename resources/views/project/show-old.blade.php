@extends('adminlte.default')
@section('title') Work Breakdown Structure @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="goBack()" class="btn btn-dark btn-sm"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('project.edit', $project->id)}}" class="btn btn-sm btn-success ml-2"><i class="far fa-edit"></i> Edit Project</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
<table class="table table-borderless">
    <tr class="pb-4">
        <td class="font-weight-bold pr-3 pb-3">Terms</td>
        <td class="pb-3"></td>
    </tr>
    <tr class="pb-4">
        <td class="font-weight-bold pb-3">Travel</td>
        <td class="pb-3">{!! $project->expense?->travel !!}</td>
        <td class="font-weight-bold pb-3">Parking</td>
        <td class="pb-3">{!! $project->expense?->parking !!}</td>
    </tr>
    <tr class="pb-4">
        <td class="font-weight-bold pb-3">Car Rental</td>
        <td class="pb-3">{!! $project->expense?->car_rental !!}</td>
        <td class="font-weight-bold pb-3">Flights</td>
        <td class="pb-3">{!! $project->expense?->flights !!}</td>
    </tr>
    <tr>
        <td class="font-weight-bold pb-3">Toll</td>
        <td class="pb-3">{!! $project->expense?->toll !!}</td>
        <td class="font-weight-bold pb-3">Accomodation</td>
        <td class="pb-3">{!! $project->expense?->accommodation !!}</td>
    </tr>
    <tr>
        <td nowrap class="font-weight-bold pb-3 pr-3">Out of Office Allowance</td>
        <td class="pb-3">{!! $project->expense?->out_of_town !!}</td>
        <td class="font-weight-bold pb-3">Per Diem</td>
        <td class="pb-3">{!! $project->expense?->per_diem !!}</td>
    </tr>
    <tr>
        <td class="font-weight-bold pb-3">Data</td>
        <td class="pb-3">{!! $project->expense?->data !!}</td>
        <td class="font-weight-bold pb-3">Other</td>
        <td class="pb-3">{!! $project->expense?->other !!}</td>
    </tr>
</table>
    <div class="container-fluid">
        @include('task.task-menu')
        <hr />
        <div class="table-responsive">
            @if(($permissions[1]['permission'] == 'View') || ($permissions[1]['permission'] == 'Create') || ($permissions[1]['permission'] == 'Update') || ($permissions[1]['permission'] == 'Full'))
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Project</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project No:</th>
                    <td>
                        {{Form::text('name',$wbs_nr,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>Created Date</th>
                    <td>
                        {{Form::text('ref',$project->created_at,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                <tr>
                    <th>Customer:</th>
                    <td>{{Form::select('customer_id',$customer_drop_down,$project->customer_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                    <th>Customer Ref.:</th>
                    <td>{{Form::text('customer_ref',$project->customer_ref,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled','placeholder'=>'Customer Ref No'])}}
                    </td>
                </tr>
                <tr>
                    <th>Customer Invoice Contact:</th>
                    <td>{{Form::text('customer_id',$customer_invoice_contact,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                    <th>Vendor Invoice Contact:</th>
                    <td>{{Form::text('customer_ref',$vendor_invoice_contact,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Project Name:</th>
                    <td>
                        {{Form::text('name',$project->name,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet)</th>
                    <td>
                        {{Form::text('ref',$project->ref,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Project Type:</th>
                    <td>
                        {{Form::text('name',isset($project->project_type)?$project->project_type->description:'',['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th>Location:</th>
                    <td>{{Form::text('location',$project->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_ref') ? ' is-invalid' : ''), 'disabled'=>'disabled', 'placeholder'=>'Location'])}}
                        @foreach($errors->get('location') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Own Project Lead:</th>
                    <td>{{Form::select('manager_id',$consultant_drop_down,$project->manager_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                    <th>Company:</th>
                    <td>{{Form::select('company_id',$company_drop_down,$project->company_id,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled'])}}
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Start Date:</th>
                    <td>
                        {{Form::text('name',$project->start_date,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project Name'])}}
                    </td>
                    <th style='width: 20%'>End Date</th>
                    <td>
                        {{Form::text('ref',$project->end_date,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Project ref'])}}
                    </td>
                </tr>
                <tr>
                    <th>Customer Order Number</th>
                    <td>{{Form::text('cust_order_number',$project->customer_po,['class'=>'form-control form-control-sm  col-sm-12', 'disabled'=>'disabled', 'placeholder'=>'Customer Order Number'])}}</td>
                    <th>Hours of Work Per Day:</th>
                    <td>{{Form::text('hours_of_work',$project->hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''), 'disabled'=>'disabled','placeholder'=>'0'])}}
                        @foreach($errors->get('hours_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Opex/Capex:</th>
                    <td>{{Form::text('opex_project',$project->opex_project,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('opex_project') ? ' is-invalid' : ''),'disabled'=>'disabled','placeholder'=>'Opex/Capex'])}}
                        @foreach($errors->get('opex_project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Default Timesheet Template
                    </th>
                    <td>
                        {{Form::select('timesheet_template_id', [1 => 'Standard Template', 2 => 'Weekly Template', 3 => 'Arbour Template'], $project->timesheet_template_id, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_template_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('timesheet_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>


                <tr>
                    <th>Project Fixed Price:</th>
                    <td>
                        {{Form::select('is_fixed_price', ['' => 'Please select ...', 1 => 'Yes', 2 => 'No'], $project->is_fixed_price, ['class'=>'form-control form-control-sm'. ($errors->has('is_fixed_price') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('is_fixed_price') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Project Fixed Price Labour
                    </th>
                    <td>{{Form::text('fixed_price_labour', $project->fixed_price_labour,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_labour') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('fixed_price_labour') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Fixed Price Expenses:</th>
                    <td>{{Form::text('fixed_price_expenses', $project->fixed_price_expenses,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_expenses') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('fixed_price_expenses') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Project Total Fixed Price
                    </th>
                    <td>{{Form::text('total_fixed_price', $project->total_fixed_price,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('total_fixed_price') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('total_fixed_price') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Fixed Cost Labour:</th>
                    <td>{{Form::text('project_fixed_cost_labour', $project->project_fixed_cost_labour,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_labour') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_fixed_cost_labour') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Project Fixed Cost Expense
                    </th>
                    <td>{{Form::text('project_fixed_cost_expense', $project->project_fixed_cost_expense,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_expense') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_fixed_cost_expense') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Total Fixed Cost:</th>
                    <td>{{Form::text('project_total_fixed_cost', $project->project_total_fixed_cost,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_total_fixed_cost') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                        @foreach($errors->get('project_total_fixed_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Billing Cycle
                    </th>
                    <td>
                        {{Form::select('billing_cycle_id', $billing_cycle_dropdown, $project->billing_cycle_id, ['class' => 'form-control form-control-sm', 'disabled', 'placeholder' => 'Billing Cycle'])}}
                    </td>
                </tr>


                </tbody>
            </table>
            <br/>
            @endif
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <td style='width: 33%'><a class="btn btn-dark btn-sm" href="{{route('project.copy', $project->id)}}">[Copy Project]</a></td>
                        <td><a class="btn btn-dark btn-sm" href="{{route('project.edit', $project)}}">[Edit Project]</a></td>
                        <td style='width: 33%'><a class="btn btn-dark btn-sm">[Print Project]</a></td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <!-- Consultants Assigned Block-->
            @if(($permissions[2]['permission'] == 'View') || ($permissions[2]['permission'] == 'Create') || ($permissions[2]['permission'] == 'Update') || ($permissions[2]['permission'] == 'Full'))
            {{--<table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Consultants Assigned</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="row">
                                @forelse($consultants_selected as $key => $value)
                                    @if(!in_array($key, $consultant_have_assignments))
                                        <div class="col-sm-3"><a class="btn btn-dark btn-sm" href="{{route('assignment.add', Array($project->id, $key))}}"><i class="fa fa-plus"></i> {{$value}}</a></div>


                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Please assign the resources to assignments</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Please note that consultant below have not been assigned to the project.</p>
                                                        <hr>
                                                        <div>
                                                            @forelse($consultants_selected as $key => $value)
                                                                @if(!in_array($key, $consultant_have_assignments))
                                                                    <a class="btn btn-dark btn-sm mr-3" href="{{route('assignment.add', Array($project->id, $key))}}"><i class="fa fa-plus"></i> {{$value}}</a>
                                                                @endif
                                                            @empty
                                                            @endforelse
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @foreach($consultant_have_assignments as $assignment_key)
                                            @if($key == $assignment_key)
                                                <div class="col-sm-3"><a href="{{route('assignment.show', $consultant_assignment_ids[$key])}}"> {{$value}}</a></div>
                                            @endif
                                        @endforeach
                                    @endif
                                @empty
                                    No Consultants selected
                                @endforelse
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>--}}

                    <table class="table table-bordered table-sm mt-3">
                        <thead>
                        <tr>
                            <th colspan="4" class="btn-dark" style="text-align: center;">Consultants assigned</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <div class="row">
                                    @forelse($consultants_selected as $key => $value)
                                        @if(!in_array($key, $consultant_have_assignments))
                                            <div class="col-sm-3"><a class="btn btn-dark btn-sm" href="{{route('assignment.add', Array($project->id, $key))}}"><i class="fa fa-plus"></i> {{$value}}</a></div>


                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Please assign the resources to assignments</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Please note that consultant below have not been assigned to the project.</p>
                                                            <hr>
                                                            <div>
                                                                @forelse($consultants_selected as $key => $value)
                                                                    @if(!in_array($key, $consultant_have_assignments))
                                                                        <a class="btn btn-dark btn-sm mr-3" href="{{route('assignment.add', Array($project->id, $key))}}"><i class="fa fa-plus"></i> {{$value}}</a>
                                                                    @endif
                                                                @empty
                                                                @endforelse
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            @foreach($consultant_have_assignments as $assignment_key)
                                                @if($key == $assignment_key)
                                                    <div class="col-sm-3"><a href="{{route('assignment.show', $consultant_assignment_ids[$key])}}"> {{$value}}</a></div>
                                                @endif
                                            @endforeach
                                        @endif
                                    @empty
                                        No Consultants selected
                                    @endforelse
                                </div>
                            </td>
                        </tr>

                        </tbody>
                        <tfoot id="consultant_roles" @if(isset($consultant_role_users) && !empty($consultant_role_users)) style="" @else style="display: none;" @endif>
                        @if(isset($consultant_role_users) && !empty($consultant_role_users))
                            @foreach($consultant_role_users as $key => $consultant_role_user)
                                <tr>
                                    <th>Consultant:</th>
                                    <td>
                                        {{Form::select('consultant_role_user_'.$consultant_role_user->id,$consultant_role_user_drop_down,$consultants[$key],['id'=>'consultant_role_user', 'class'=>'form-control '. ($errors->has('consultant_role_user') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                                        @foreach($errors->get('consultant_role_user') as $error)
                                            <div class="invalid-feedback">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    </th>
                                    <th>Role:</th>
                                    <td>
                                        {{Form::select('consultant_role_'.$consultant_role_user->id, $roles_drop_down, $consultant_role_user->getViewRole(30, $consultant_role_user->id),['id'=>'consultant_role', 'class'=>'form-control '. ($errors->has('consultant_role') ? ' is-invalid' : ''), 'disabled' => "disabled"])}}
                                        @foreach($errors->get('consultant_role') as $error)
                                            <div class="invalid-feedback">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan="4"><!-- // Display all roles here --></td></tr>
                        @endif
                        </tfoot>
                    </table>
                    <hr/>
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <th>Start Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                            <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date', 'disabled' => 'disabled'])}}
                                @foreach($errors->get('start_date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th>End Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                            <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date', 'disabled' => 'disabled'])}}
                                @foreach($errors->get('end_date') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Billable: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                            <td>
                                {{Form::select('billable',[''=>'Please Select',0=>'No',1=>'Yes'],$project->billable,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                                @foreach($errors->get('billable') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th>Project Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                            <td>
                                {{Form::select('status_id',$project_status_drop_down,$project->status_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('status_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                                @foreach($errors->get('status_id') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>



            @endif
            <br/>
            <!-- Treeview Block-->
            @if(($permissions[8]['permission'] == 'View') || ($permissions[8]['permission'] == 'Create') || ($permissions[8]['permission'] == 'Update') || ($permissions[8]['permission'] == 'Full'))
            @include('project.treeview')
            <table class="table table-sm table-borderless">
                <tbody>
                <tr>
                    <td><a class="btn btn-dark btn-sm" href="{{route('epic.create')}}?project_id={{$project->id}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Epic</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('feature.create')}}?project_id={{$project->id}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Feature</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('userstory.create')}}?project_id={{$project->id}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create User Story</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('task.create', $project)}}?project_id={{$project->id}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Task</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('action.create')}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Action</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('sprint.create')}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Sprint</a></td>
                    <td><a class="btn btn-dark btn-sm" href="{{route('task.index')}}?project_id={{$project->id}}"><i class="fa fa-id-card"></i>&nbsp;&nbsp;View Tasks</a></td>
                </tr>
                </tbody>
            </table>
            <br/>
            @endif
            <!-- Sprints Block-->
            @if(($permissions[9]['permission'] == 'View') || ($permissions[9]['permission'] == 'Create') || ($permissions[9]['permission'] == 'Update') || ($permissions[9]['permission'] == 'Full'))
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="10" class="btn-dark" style="text-align: center;">Sprints</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Goal</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($sprints as $sprint)
                            <tr>
                                <td><a href="{{route('sprint.show',$sprint)}}">{{$sprint->name}}</a></td>
                                <td>{{isset($sprint->goal)?$sprint->goal:''}}</td>
                                <td>{{isset($sprint->start_date)?$sprint->start_date:''}}</td>
                                <td>{{isset($sprint->end_date)?$sprint->end_date:''}}</td>
                                <td>{{isset($sprint->status->name)?$sprint->status->name:''}}</td>
                                <td class="text-right">
                                    <a href="{{route('sprint.edit',$sprint)}}" class="btn btn-success btn-sm">Edit</a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['sprint.destroy', $sprint],'style'=>'display:inline','class'=>'delete']) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No sprints entries for this project.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
            <br/>
            @endif
            <!-- Billable Expenses Block-->
            @if(($permissions[4]['permission'] == 'View') || ($permissions[4]['permission'] == 'Create') || ($permissions[4]['permission'] == 'Update') || ($permissions[4]['permission'] == 'Full'))
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Billable Expenses</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Travel</th>
                        <td>{{Form::textarea('travel',isset($expense->travel)?$expense->travel:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                        <th>Parking</th>
                        <td>{{Form::textarea('parking',isset($expense->parking)?$expense->parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                    </tr>
                    <tr>
                        <th>Accommodation</th>
                        <td>{{Form::textarea('accommodation',isset($expense->accommodation)?$expense->accommodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                        <th>Per Diem</th>
                        <td>{{Form::textarea('per_diem',isset($expense->per_diem)?$expense->per_diem:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                    </tr>
                    <tr>
                        <th>Out of Town Allowance</th>
                        <td>{{Form::textarea('out_of_town',isset($expense->out_of_town)?$expense->out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                        <th>Data</th>
                        <td>{{Form::textarea('data',isset($expense->data)?$expense->data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                    </tr>
                    <tr>
                        <th>Other</th>
                        <td colspan="3">{{Form::textarea('other',isset($expense->other)?$expense->other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'N/A', 'disabled'=>'disabled'])}}
                        </td>
                    </tr>
                    <tr>
                        <th>Billing Note</th>
                        <td colspan="3">
                            {{Form::textarea('billing_note',isset($project->billing_note)?$project->billing_note:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Reimbursable as incurred.', 'disabled'=>'disabled'])}}
                        </td>
                    </tr>
                </tbody>
            </table>
            <br/>
            @endif
            <!-- Project Analysis Block-->
            @if(($permissions[5]['permission'] == 'View') || ($permissions[5]['permission'] == 'Create') || ($permissions[5]['permission'] == 'Update') || ($permissions[5]['permission'] == 'Full'))
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Project Analysis</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="width: 25%;">Project PO Value</th>
                        <td style="width: 25%;">{{$projectPOValue}}</td>
                        <th style="width: 25%;">Actual Invoiced</th>
                        <td style="width: 25%;">{{$actualInvoiced}}</td>
                    </tr>
                    <tr>
                        <th>Planned Project Cost - Labour</th>
                        <td>{{$plannedProjectCostLabour}}</td>
                        <th>Actual Labour Invoiced</th>
                        <td>{{$actualLabourCost}}</td>
                    </tr>
                    <tr>
                        <th>Planned Project Cost - Expenses</th>
                        <td>{{$plannedProjectCostExpenses}}</td>
                        <th>Actual Expenses Invoiced</th>
                        <td>{{$actualExpensesInvoiced}}</td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td></td>
                        <th>Actual Labour Cost</th>
                        <td>{{$actualLabourCost}}</td>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <td></td>
                        <th>Actual Expenses</th>
                        <td>{{$actualExpenses}}</td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Planned Hours</th>
                        <td>{{$plannedHours}}</td>
                        <th>Profit / (Loss) Value</th>
                        <td>{{$profitOrLossValue}}</td>
                    </tr>
                    <tr>
                        <th>Actual Hours</th>
                        <td>{{$actualHours}}</td>
                        <th>Profit / (Loss) %</th>
                        <td>{{$profitOrLossPercentage}} %</td>
                    </tr>
                    <tr>
                        <th>Difference</th>
                        <td>{{$difference}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Completed %</th>
                        <td>{{$complete}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            @endif
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection