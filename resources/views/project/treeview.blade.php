
<div class="w-100 p-0 m-0">
    <treeview :project="{{$project}}"></treeview>
</div>
<div id="list" class="row w-100 p-0 m-0" style="display:none;">
    <task-list :project="{{$project}}"></task-list>
</div>

<!-- Task Modal -->
<div class="modal fade" id="task-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div style="min-width: 1200px !important;height:93vh" class="modal-content">
            <div class="modal-header" style="height: 3.5rem;">
                <h5 class="modal-title" id="add_task_modal_label" style="width: 100%;">
                    Task #<span class="task-id"></span><br/>
                </h5>
                <button id="closeModalBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td>
                            <div class="form-group input-group mb-0">
                                <label class="has-float-label mb-0">
                                    <input style="font-size: 17px;" type="text" id="task-description" class="form-control col-md-12 mb-0" />
                                    <span>Description</span>
                                </label>
                            </div>
                        </td>
                        <td class="last">
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(1)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-frown fa-lg" style="color: red;"></i></button>
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(2)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-exclamation fa-lg" style="color: orange;"></i></button>
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(3)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-smile fa-lg" style="color: green;"></i></button>
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(4)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-user-check fa-lg" style="color: purple;"></i></button>
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(5)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-upload fa-lg" style="color: blue;"></i></button>
                            <button class="btn btn-default bg-transparent no-border" onclick="setEmote(0)" style="border-radius: 50%; margin-right:1%;"><i class="fa fa-times fa-lg"></i></button>
                        </td>
                        <td class="last pl-4">
                            <button class="btn btn-sm btn-success" onclick="updateTask()" id="save-task">Save & Close</button>
                        </td>
                    </tr>
                </table>
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width:50%">
                                    <div class="d-flex">
                                    <div class="form-group input-group mb-0 col-md-9">
                                        <label class="has-float-label mb-0">
                                            <select id="employee-id" class="form-control col-md-12" disabled></select>
                                            <span>Assigned User</span>
                                        </label>
                                    </div>
                                    <a onclick="commentsFocus()" style="cursor: pointer;"><i class="fa fa-comment"></i> <span id="comment-no"></span> comments</a>
                                    </div>
                                </td>
                                <td style="width: 50%">
                                    {{-- <div class="form-group input-group mb-0 col-md-9">
                                        <label class="has-float-label mb-0">
                                            <select id="reassign-id" class="form-control col-md-12"></select>
                                            <span>Reassign User</span>
                                        </label>
                                    </div> --}}
                                </td>
                            </tr>
                        </table>       

                
                        <div class="mb-3 pb-0" style="background-color: #F8F8F8; padding: 15px; border-radius: 7px;">
                            <table class="table table-sm table-borderless mb-0">
                                <tr>
                                    <td class="col-md-4">
                                    <div class="form-group input-group">
                                        <label class="has-float-label mb-0">
                                            <select id="project-id" class="form-control col-md-12" disabled></select>
                                            <span style="background-color: rgb(248, 248, 248);">Project</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="customer-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Customer</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="sprint-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Sprint</span>
                                        </label>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="user-story-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">User Story</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="dependency" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Dependency</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="goal-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Goal</span>
                                        </label>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="delivery-type-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Delivery Type</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="permition-id" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Permission</span>
                                        </label>
                                    </div>
                                    </td>
                                    <td class="col-md-4">
                                        <div class="form-group input-group">
                                            <label class="has-float-label mb-0">
                                            <select id="status" class="form-control col-md-12">
                                            </select>
                                            <span style="background-color: rgb(248, 248, 248);">Status</span>
                                        </label>
                                    </div>
                                    </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="min-height:50%">
                            <ul class="nav nav-tabs">
                                <li class="">
                                    <a href="#details" class="active" data-toggle="tab">Details</a>
                                </li>
                                <li class="">
                                    <a href="#timesheets" class="" data-toggle="tab">Timesheets</a>
                                </li>
                            </ul>
                            <div class="tab-content" style="margin-top:-18px;">
                                <div class="tab-pane fade show active" id="details">
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-sm table-borderless">
                                                <thead>
                                                <tr class="planning-btn">
                                                    <th style="border-bottom: 1px solid grey !important;" id="toggle-arrow">
                                                        Description <span><i class="float-right fa fa-chevron-down"></i></span>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody id="toggle-description">
                                                <tr>
                                                    <td>
                                                        <textarea id="note-1" rows="5" cols="50" class="form-control form-control-sm col-md-12">
                                                        </textarea>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-sm table-borderless">
                                                <thead>
                                                <tr class="planning-btn">
                                                    <th id="toggle-discussion-arrow" style="border-bottom: 1px solid grey !important;">
                                                        Discussion<i class="float-right fa fa-chevron-down"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody id="toggle-discussion">
                                                <tr>
                                                    <td id="message_td">
                                                        <textarea id="message" rows="5" cols="50"  class="form-control form-control-sm col-md-12"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <button id="save-note" style="margin-bottom: 5px; margin-top: 5px; color: #ffffff;" class="btn btn-sm btn-warning">Save Note</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="table table-sm table-borderless">
                                                            <tbody id="discussion-chat">
                                                            <tr>
                                                                <td style="width: 5%;"><span class="chat-initials" style="background-color: #00feff" title="John Doe">JD</span></td>
                                                                <td style="width: 95%;">
                                                                    <div class="form-control form-control-sm col-md-12 discussion-messages">
                                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-3">
                                            <table class="table table-sm table-borderless">
                                                <thead>
                                                    <tr class="planning-btn">
                                                        <th style="border-bottom: 1px solid grey !important;" id="toggle-planning">
                                                            Planning <span><i class="float-right fa fa-chevron-down"></i></span>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="planned-body">
                                                    <tr><td>
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label">
                                                                <input type="date" id="start-date" class="form-control form-control-sm datepicker col-md-12" />
                                                                <span>Start Date</span>
                                                            </label>
                                                        </div>
                                                        </td></tr>
                                                    <tr><td>
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label">
                                                                <input type="date" id="end-date" class="form-control form-control-sm datepicker col-md-12 datepicker" />
                                                            <span>End Date</span>
                                                            </label>
                                                        </div>
                                                    </td></tr>
                                                    <tr>
                                                        <td>                                                            
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label">
                                                                <select id="billable" class="form-control col-md-12">
                                                                </select>
                                                                <span>Billable</span>
                                                            </label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="form-group input-group mt-3">
                                                                <label class="has-float-label">
                                                                    <select id="on_kanban" class="form-control col-md-12">
                                                                </select>
                                                                <span>Display on Kanban</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr><td>
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label">
                                                                <input type="text" id="planned-hours" class="form-control form-control-sm col-md-12" />
                                                            <span>Hours Planned</span>
                                                            </label>
                                                        </div>
                                                    </td></tr>
                                                    <tr><td>
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label"><input type="text" id="weight" class="form-control form-control-sm col-md-12" />
                                                            <span>Weight</span>
                                                            </label>
                                                        </div></td></tr>
                                                    <tr><td>
                                                        <div class="form-group input-group mt-3">
                                                            <label class="has-float-label">
                                                                <input type="text" id="remaining-time" class="form-control form-control-sm col-md-12" />
                                                            <span>Remaining</span>
                                                            </label>
                                                        </div>
                                                    </td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- Milestone -->
                                        <div class="col-md-3">
                                            <table class="table table-sm table-borderless">
                                                <thead>
                                                <tr class="planning-btn">
                                                    <th colspan="2" style="border-bottom: 1px solid grey !important;">
                                                        Attachments <i class="float-right fa fa-chevron-down"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody id="documents-list">
                                                <tr>
                                                    <td colspan="2">
                                                        <div>
                                                            {{-- <label><button style="color: #ffffff;" onclick="clickFile('file')" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i> Add File</button></label> --}}
                                                            {{-- <input style="display: none;" id="file" type="file" class="form-control form-control-sm col-md-12" value="Add File"/> --}}
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="timesheets">
                                    <hr/>
                                    <table class="table table-sm table-striped table-hover">
                                        <tbody>
                                        <tr>
                                            <th style="width: 20%;">Select Year Week</th>
                                            <td>
                                                <select id="year-week" class="form-control form-control-sm col-md-12">
                                                    <option value="year_week.id">weeks</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-sm table-striped table-hover" style="display: none" id="split-week-notification">
                                        <tbody>
                                        <tr>
                                            <th style="color: red;">Please note that this is split week and a timesheet for the remainder of the week should be created if not already created.</th>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-sm table-condensed table-bordered">
                                        <thead>
                                        <tr style="background-color: #F8F8F8;" id="week-days">

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="time-dropdown">
                                            <td>Hours</td>
                                            <td>
                                                <select id="mon-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="tue-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="wed-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="thu-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="fri-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="sat-hours" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td><select id="sun-hours" class="form-control form-control-sm col-md-12"></select></td>
                                            <td rowspan="2">
                                                <input type="number" id="mileage" class="form-control form-control-sm col-md-12" />
                                            </td>
                                            <td rowspan="2">
                                                <select id="timesheet-billable" class="form-control form-control-sm col-md-12">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="time-dropdown">
                                            <td>Minutes</td>
                                            <td>
                                                <select id="mon-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="tue-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="wed-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="thu-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="fri-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="sat-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                            <td>
                                                <select id="sun-minutes" class="form-control form-control-sm col-md-12"></select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <button onclick="addTimeLine()" class="btn btn-sm btn-warning" style="color: #ffffff;">Save Timeline</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-sm table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th colspan="11">Task Timesheet</th>
                                        </tr>
                                        <tr style="background-color: rgb(248, 248, 248);">
                                            <th>Timesheet ID</th><th>Week</th><th>Bill</th><th>Travel</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th><th>Total</th><td>&nbsp;</td>
                                        </tr>
                                        </thead>
                                        <tbody id="timeline-rows">

                                        <tr>
                                            <th colspan="11">
                                                TOTAL
                                            </th>
                                            <th colspan="2" id="total">
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- User Story Modal -->
<div class="modal fade" id="user-story-modal" tabindex="-1" role="dialog" aria-labelledby="user-story-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_task_modal_label" style="width: 100%;">
                    User Story <span class="user-story-id"></span><br/>
                    <table class="table table-sm table-borderless" style="margin-bottom: 0px !important;">
                        <tr>
                            <td style="width: 20px;"><span class="user-story-id"></span></td><td><input style="font-size: 17px;" type="text" id="user-story-description" class="form-control form-control-sm col-md-12" /></td>
                        </tr>
                    </table>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td style="width: 25%">
                            <a onclick="commentsFocus()" style="cursor: pointer;"><i class="fa fa-comment"></i> <span id="discussion-comment-no">0</span> comments</a>
                        </td>
                        <td style="width: 25%">&nbsp;&nbsp; </td>
                        <td style="width: 30%">
                        </td>
                        <td style="width: 20%" class="text-right">
                            <button class="btn btn-sm btn-success" onclick="updateUserStory()" id="save-userstory">Save & Close</button>
                        </td>
                    </tr>
                </table>

                <div style="background-color: #F8F8F8; padding: 15px; border-radius: 4px;">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <th style="width: 10%;">Sprint</th>
                            <td style="width: 23%;">
                                <select id="userstory-sprint-id" class="form-control form-control-sm col-md-12">
                                </select>
                            </td>
                            <th style="width: 10%;">Dependency</th>
                            <td style="width: 23%;">
                                <select id="userstory-dependency" class="form-control form-control-sm col-md-12">
                                </select>
                            </td>
                            <th style="width: 10%;">Status</th>
                            <td style="width: 23%;">
                                <select id="userstory-status" class="form-control form-control-sm col-md-12">
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="details">
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-sm table-borderless">
                                <thead>
                                <tr class="planning-btn">
                                    <th style="border-bottom: 1px solid grey !important;" id="toggle-arrow">
                                        Description <span><i class="float-right fa fa-chevron-down"></i></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="toggle-description">
                                <tr>
                                    <td>
                                            <textarea id="user-story-details" rows="5" cols="50" class="form-control form-control-sm col-md-12">
                                            </textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-sm table-borderless">
                                <thead>
                                <tr class="planning-btn">
                                    <th id="toggle-discussion-arrow" style="border-bottom: 1px solid grey !important;">
                                        Discussion<i class="float-right fa fa-chevron-down"></i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="toggle-discussion">
                                <tr>
                                    <td id="message_td">
                                        <textarea id="user-story-discussion" rows="5" cols="50"  class="form-control form-control-sm col-md-12"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <button id="save-user-story-discussion" style="margin-bottom: 5px; margin-top: 5px; color: #ffffff;" class="btn btn-sm btn-warning">Save Note</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table table-sm table-borderless">
                                            <tbody id="userstory-discussion-chat">
                                            <tr>
                                                <td style="width: 5%;"><span class="chat-initials" style="background-color: #00feff" title="John Doe">JD</span></td>
                                                <td style="width: 95%;">
                                                    <div class="form-control form-control-sm col-md-12 discussion-messages">
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table class="table table-sm table-borderless">
                                <thead>
                                <tr class="planning-btn">
                                    <th style="border-bottom: 1px solid grey !important;" id="toggle-planning">
                                        Planning <span><i class="float-right fa fa-chevron-down"></i></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="planned-body">
                                <tr><td>Start Date</td></tr>
                                <tr><td><input type="date" id="userstory-start-date" class="form-control form-control-sm datepicker col-md-12" /></td></tr>
                                <tr><td>End Date</td></tr>
                                <tr><td><input type="date" id="userstory-end-date" class="form-control form-control-sm datepicker col-md-12 datepicker" /></td></tr>
                                <tr><td>Billable</td></tr>
                                <tr>
                                    <td>
                                        <select id="userstory-billable" class="form-control form-control-sm col-md-12">
                                        </select>
                                    </td>
                                </tr>
                                <tr><td>Hours Planned</td></tr>
                                <tr><td><input type="text" id="userstory-planned-hours" class="form-control form-control-sm col-md-12" /></td></tr>
                                <tr><td>Weight</td></tr>
                                <tr><td><input type="text" id="userstory-weight" class="form-control form-control-sm col-md-12" /></td></tr>
                                <tr><td>Remaining</td></tr>
                                <tr><td><input type="text" id="userstory-remaining-time" class="form-control form-control-sm col-md-12" /></td></tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- Milestone -->
                        <div class="col-md-3">
                            <table class="table table-sm table-borderless">
                                <thead>
                                <tr class="planning-btn">
                                    <th colspan="2" style="border-bottom: 1px solid grey !important;">
                                        Attachments <i class="float-right fa fa-chevron-down"></i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="userstory-documents-list">
                                <tr>
                                    <td colspan="2">
                                        <div>
                                            {{-- <label><button style="color: #ffffff;" onclick="clickFile('userstory-file')" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i> Add File</button></label> --}}
                                            {{-- <input style="display: none;" id="userstory-file" type="file" class="form-control form-control-sm col-md-12" value="Add File"/> --}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha256-fzFFyH01cBVPYzl16KT40wqjhgPtq6FFUB6ckN2+GGw=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#exampleModal').modal('show');
            $('#assignConsultants').modal('show')
        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    function switchTaskView(val){
        if(val == 'list'){
            document.getElementById('list').style.display = 'table'
            document.getElementById('tree-view-table').style.display = 'none'
            document.getElementById('list-btn').classList.add('btn-info')
            document.getElementById('tree-btn').classList.remove('btn-info')
        }
        
        if(val == 'tree'){
            document.getElementById('list').style.display = 'none'
            document.getElementById('tree-view-table').style.display = 'table'
            document.getElementById('list-btn').classList.remove('btn-info')
            document.getElementById('tree-btn').classList.add('btn-info')
        }
    }
    </script>
    <script>
        let serverUrl = window.location.origin;
        let autoCompleteUsers = [];
        let users = Object.entries({!! json_encode($resource_drop_down??[]) !!});
        let initial_colors = [];
        let task_id = "";
        let actualTask = {};
        let hours_drop_down = {{json_encode($hours_drop_down??[])}};
        let minutes_drop_down = {{json_encode($minutes_drop_down??[])}};
        let time_options = "";
        let selected_year_week = "{!! $selected_year_week??0 !!}";
        let timesheet = {};
        let billable = "";
        let user_story_id = 0;
        let user_story_old = "";

        users.forEach((user, index) =>{
            autoCompleteUsers.push({
                text: user[1].toLowerCase(),
                value: "@"+user[1]
            })
        });

        $("#mon-hours, #tue-hours, #wed-hours, #thu-hours, #fri-hours, #sat-hours, #sun-hours").html(timeOptions(hours_drop_down));
        $("#mon-minutes, #tue-minutes, #wed-minutes, #thu-minutes, #fri-minutes, #sat-minutes, #sun-minutes").html(timeOptions(minutes_drop_down));

        function toggleChildre(childClass, type){
            $('.'+childClass).toggle('slow');

            if($('#'+childClass).hasClass('fa-plus')){
                $('#'+childClass).removeClass('fa-plus');
                $('#'+childClass).addClass('fa-minus');
            } else {
                $('#'+childClass).addClass('fa-plus');
                $('#'+childClass).removeClass('fa-minus');
                $('.'+type).hide();
                $('.'+type).find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
                // Close all the children as well
                // $('.'+childClass).hide();
            }
        }

        function collapseExpandAll(){
            if($('#collapseExpandBtnText').html() == 'Expand All'){
                $('.child-first-level').show();
                $('.child-second-level').show();
                $('.child-third-level').show();
                $('#tree-view-table').find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
                $('#collapseExpandBtnText').html('Collapse All');
            } else {
                $('.child-first-level').hide();
                $('.child-second-level').hide();
                $('.child-third-level').hide();
                $('.empty').hide();
                $('#tree-view-table').find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
                $('#collapseExpandBtnText').html('Expand All');
            }
        }

        function populateSelect(arr,selected_option) {
            let options = '';
                    options = options +
                    '<option value="">-</option>';
            for (var i = 0; i < arr.length; i++) {
                // POPULATE SELECT ELEMENT WITH JSON.
                // add the value attribute in option as the id of bird
                if(arr[i].name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].name + '</option>';
                }
                if(arr[i].company_name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].company_name + '</option>';
                }
                if(arr[i].customer_name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].customer_name + '</option>';
                }
                if(arr[i].team_name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].team_name + '</option>';
                }
                if(arr[i].full_name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].full_name + '</option>';
                }
                if(arr[i].description){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].description + '</option>';
                }
            }
            return options
        }

        function commentsFocus(){
            const element = document.getElementById('message_td');

            if (element) {
                element.scrollIntoView();
            }
        }

        $(function(){
            $("#save-note").on('click', () => {
                if (actualTask.status == 5){
                    return false;
                }
                $("#save-note").text("sending...");
                axios.post('/api/task/message/'+task_id, {
                    message: tinyMCE.get('message').getContent(),
                    user_id: "{{auth()->id()}}"
                })
                    .then(response => {
                        tinyMCE.get('message').setContent("");
                        getMessages(task_id);
                        $("#save-note").text("Save Note");
                    })
                    .catch(error => {
                        alert('An error occured, could not save the task.');
                        tinyMCE.get('message').setContent("");
                        $("#save-note").text("Save Note");

                    });
            })
        })

        function getMessages(task_id){
            axios.get('/api/task/getmessages/'+task_id, {})
                .then(response => {
                    let messages = response.data;
                    let discussion = "";
                    messages.forEach((message, index) =>{
                        let style = "background-color: " + randomColor(getInitials(message.user.first_name, message.user.last_name));
                        discussion += "<tr>" +
                                "<td style=\"width: 5%;\">"+
                                    '<span class="chat-initials" title="'+getInitials(message.user.first_name, message.user.last_name)+'" style="'+style+'">'+getInitials(message.user.first_name, message.user.last_name) + '</span>' +

                                "</td>"+
                                '<td style="width: 95%;"><div class="form-control form-control-sm col-md-12">'+ message.note +'</div></td>' +
                            "</tr>";
                    });

                    let messages_count = (messages != null) ? messages.length : 0;

                    $("#comment-no").text(messages_count);
                    $("#discussion-chat").html(discussion);
                }).catch(e =>{
                    console.log(e);
                });
        }

        function getInitials(first_name, last_name){
            return first_name.charAt(0) + last_name.charAt(0);
        }

        function randomColor(initials){
            const r = () => Math.floor(256 * Math.random());

            if(typeof(initial_colors[initials]) == 'undefined'){
                initial_colors[initials] = (`rgb(${r()}, ${r()}, ${r()})`);
            }

            return initial_colors[initials];
        }

        function selectOptions(select, active_option){
            let options = "<option value='0'>Select Option</option>";

            select.forEach((v, i) =>{
                options += "<option value='"+v[0]+"'"+((v[0] == active_option)?" selected":"")+">"+v[1]+"</option>";
            })

            return options;
        }

        function clickFile(field){
            document.getElementById(field).click();
        }

        function getDocuments(url){
            let selector = (url.indexOf("task") >= 0)?"#documents-list":"#userstory-documents-list";
            let delete_url = (url.indexOf("task") >= 0)?"/api/task/deletedocument/":"/api/userstory/deletedocument/";
            let files_l = (url.indexOf("task") >= 0)?"/files/tasks/":"/files/userstory/";
            axios.get(url, {})
                .then(response => {
                    let tr = '';
                    response.data.forEach((v, i) => {
                        tr += '<tr>' +
                            '<td><a href="'+files_l+v.file+'" target="_blank">'+ v.name +'</a></td>' +
                            '<td class="text-right"><a onclick="deleteDocument(\''+delete_url+v.id+'\')"><i style="color: red;" class="fa fa-trash" title="Delete attachment"></i></a></td>' +
                        '</tr>'
                    })
                    $(selector+" tr:not(:first)").remove();
                    $(selector).append(tr);
                }).catch(error =>{
                console.log(error.response)
            });
        }

        function deleteDocument(url){
            if (actualTask.status == 5){
                return false;
            }
            const confirmFlag = confirm('Are you sure you want to delete this attachment?')

            if(!confirmFlag){
                return false;
            }

            axios.post(url, {})
                .then(response => {
                    let get_type = response.data.uri.split("/")[4];

                    getDocuments("/api/"+get_type+"/getdocuments/"+user_story_id);
                    alert('Attachment deleted successfully.')
                });
        }

        $("#file, #userstory-file").on('change', (e) =>{
            if (actualTask.status == 5){
                return false;
            }
            let url, get_url;
            if (e.target.id === $("#userstory-file").attr("id")){
                url = '/api/userstory/uploadfile/'+user_story_id;
                get_url = '/api/userstory/getdocuments/'+user_story_id;
            }else{
                url = '/api/task/uploadfile/'+task_id;
                get_url = '/api/task/getdocuments/'+task_id;
            }

            let file = e.target.files[0];

            e.preventDefault();

            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }

            let formData = new FormData();
            formData.append('file', file);
            formData.append('user_id', {{ auth()->id() }});

            axios.post(url, formData, config)
                .then(response => {
                    getDocuments(get_url);
                    file = null;
                    document.getElementById(e.target.id).value= null;
                    alert('File uploaded successfully');
                })
                .catch(function (error) {
                    alert("File upload failed")
                    console.log(error.response.data);
                });
        });

        $("#year-week").on('change', ()=>{
            selected_year_week = $("#year-week").val().prop('disabled', (i, v) => {
                if (task.status == 5) return true;
                return false;
            });
            weekChange()
        });

        function weekChange(){
            if (actualTask.status == 5){
                return false;
            }

            let year_weeks = {!! json_encode($year_week_drop_down??[]) !!};
            let week_option = "";
            let week_days  = "";

            year_weeks.forEach((value, index) =>{
                let year_week = verifyYearWeek(value.id, value.name)
                let selected = (year_week == selected_year_week)?"selected":"";
                week_option += '<option value="'+year_week+'" '+ selected +'>'+value.name+'</option>';

                if (year_week == selected_year_week) {
                    week_days = '<th>Client Allocation</th><th>Mon ' + value.days[0] + '</th><th>Tue ' + value.days[1] + '</th><th>Wed ' + value.days[2] + '</th><th>Thu ' + value.days[3] + '</th><th>Fri ' + value.days[4] + '</th><th>Sat ' + value.days[5] + '</th><th>Sun ' + value.days[6] + '</th><th>Travel(Mileage)</th><th>Billable</th>';
                    if (year_week.length !== 6){
                        $("#split-week-notification").css('display', 'table');
                    }else {
                        $("#split-week-notification").css('display', 'none');
                    }

                    if(value.days[6] < 7) {
                        let split_week_number = selected_year_week.split("_");

                        if(split_week_number[1] == 1){
                            $("#mon-hours, #mon-minutes").attr("disabled", (value.days[0] < 7));
                            $("#tue-hours, #tue-minutes").attr("disabled", (value.days[1] < 7));
                            $("#wed-hours, #wed-minutes").attr("disabled", (value.days[2] < 7));
                            $("#thu-hours, #thu-minutes").attr("disabled", (value.days[3] < 7));
                            $("#fri-hours, #fri-minutes").attr("disabled", (value.days[4] < 7));
                            $("#sat-hours, #sat-minutes").attr("disabled", (value.days[5] < 7));
                            $("#sun-hours, #sun-minutes").attr("disabled", (value.days[6] < 7));
                        }

                        if(split_week_number[1] == 2){
                            $("#mon-hours, #mon-minutes").attr("disabled", (value.days[0] > 7));
                            $("#tue-hours, #tue-minutes").attr("disabled", (value.days[1] > 7));
                            $("#wed-hours, #wed-minutes").attr("disabled", (value.days[2] > 7));
                            $("#thu-hours, #thu-minutes").attr("disabled", (value.days[3] > 7));
                            $("#fri-hours, #fri-minutes").attr("disabled", (value.days[4] > 7));
                            $("#sat-hours, #sat-minutes").attr("disabled", (value.days[5] > 7));
                            $("#sun-hours, #sun-minutes").attr("disabled", (value.days[6] > 7));
                        }
                    }else {
                        $("#mon-hours, #mon-minutes, #tue-hours, #tue-minutes, #wed-hours, #wed-minutes, #thu-hours, #thu-minutes, #fri-hours, #fri-minutes, #sat-hours, #sat-minutes, #sun-hours, #sun-minutes").attr('disabled', false);
                    }
                }

            })
            $("#week-days").html(week_days).prop('disabled', (i, v) => {
                if (actualTask.status == 5) return true;
                return false;
            });
            $("#year-week").html(week_option).prop('disabled', (i, v) => {
                if (actualTask.status == 5) return true;
                return false;
            });
        }

        function verifyYearWeek(id, name) {

            if (name.length === 18){
                return id.substring(0, 6)
            }

            return id;
        }

        function timeOptions(time) {
            time.forEach((t, index) =>{
                time_options += '<option value="'+ t +'">' +t +'</option>';
            })

            return time_options;
        }

        $("#timesheet-billable").on('change', () => billable = $("#timesheet-billable").val())

        function addTimeLine(){
            if (actualTask.status == 5){
                alert("You can't capture time against a completed task");
                return false;
            }
            if(actualTask.customer_id > 0){
                // Continue
            } else {
                alert('Alert please first save customer');
                return 0;
            }

            if(actualTask.project_id > 0){
                // Continue
            } else {
                alert('Alert please first save project');
                return 0;
            }

            if(actualTask.employee_id > 0){
                // Continue
            } else {
                alert('Alert please first save consultant');
                return 0;
            }

            timesheet["mon_hour"] = $("#mon-hours").val();
            timesheet["mon_minute"] = $("#mon-minutes").val();
            timesheet["tue_hour"] = $("#tue-hours").val();
            timesheet["tue_minute"] = $("#tue-minutes").val();
            timesheet["wed_hour"] = $("#wed-hours").val();
            timesheet["wed_minute"] = $("#wed-minutes").val();
            timesheet["thu_hour"] = $("#thu-hours").val();
            timesheet["thu_minute"] = $("#thu-minutes").val();
            timesheet["fri_hour"] = $("#fri-hours").val();
            timesheet["fri_minute"] = $("#fri-minutes").val();
            timesheet["sat_hour"] = $("#sat-hours").val();
            timesheet["sat_minute"] = $("#sat-minutes").val();
            timesheet["sun_hour"] = $("#sun-hours").val();
            timesheet["sun_minute"] = $("#sun-minutes").val();
            timesheet["mileage"] = $("#mileage").val();
            timesheet["billable"] = billable;

            let request = {
                task: actualTask,
                year_week: selected_year_week,
                timesheet: timesheet,
                user_id: actualTask.employee_id
            };

            axios.post('/api/task/addtimesheettimeline', {
                request
            })
                .then(response => {
                    getTimelines();
                    timesheet["mon_hour"] = $("#mon-hours").val(0);
                    timesheet["mon_minute"] = $("#mon-minutes").val(0);
                    timesheet["tue_hour"] = $("#tue-hours").val(0);
                    timesheet["tue_minute"] = $("#tue-minutes").val(0);
                    timesheet["wed_hour"] = $("#wed-hours").val(0);
                    timesheet["wed_minute"] = $("#wed-minutes").val(0);
                    timesheet["thu_hour"] = $("#thu-hours").val(0);
                    timesheet["thu_minute"] = $("#thu-minutes").val(0);
                    timesheet["fri_hour"] = $("#fri-hours").val(0);
                    timesheet["fri_minute"] = $("#fri-minutes").val(0);
                    timesheet["sat_hour"] = $("#sat-hours").val(0);
                    timesheet["sat_minute"] = $("#sat-minutes").val(0);
                    timesheet["sun_hour"] = $("#sun-hours").val(0);
                    timesheet["sun_minute"] = $("#sun-minutes").val(0);
                    timesheet["mileage"] = $("#mileage").val(0);
                    timesheet["billable"] = $("#timesheet-billable").val(0);
                    alert('Timeline successfully saved');
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        function getTimelines(){
            axios.get('/api/task/gettimesheettimelines/'+task_id, {})
                .then(response => {
                    let table_rows = "";
                    response.data.timeline_minutes.forEach((timeline, index) =>{
                        table_rows += '<tr>' +
                            '<td>'+ timeline.id +'</td><td>'+ timeline.from_to +'</td><td>'+ ((timeline.billable == 1)?"Yes":"No") +'</td><td>'+ timeline.mileage +'</td><td>'+ timeline.mon +'</td><td>'+ timeline.tue +'</td><td>'+ timeline.wed +'</td><td>'+ timeline.thu +'</td><td>'+ timeline.fri +'</td><td>'+ timeline.sat +'</td><td>'+ timeline.sun +'</td><td>'+ timeline.total +'</td>' +
                            '<td class="text-right">' +
                                '<a style="cursor: pointer;" onclick="deleteTimeline('+timeline.id+')" title="Delete timeline"><i class="fa fa-trash" style="color: red;"></i></a>' +
                            '</td></tr>'
                    });
                    $("#timeline-rows").html(table_rows);
                    $("#total").text(response.data.total);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        function setEmote(emote_id){
            if (actualTask.status == 5){
                alert("You can't edit a completed task")
                return false;
            }
            axios.post('/task/setemote/'+task_id, {
                emote_id: emote_id
            })
                .then(response => {
                    alert('Emote set successfully')
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        function updateTask() {
            $("#save-task").text("Sending...")
            let request = {
                tree_view: true,
                description: $("#task-description").val(),
                employee_id: $("#employee-id").val(),
                project_id: $("#project-id").val(),
                customer_id: $("#customer-id").val(),
                sprint_id: $("#sprint-id").val(),
                user_story_id: $("#user-story-id").val(),
                dependency: $("#dependency").val(),
                status: $("#status").val(),
                task_delivery_type_id: $("#delivery-type-id").val(),
                task_permission_id: $("#task_permission_id").val(),
                note_1: tinyMCE.get('note-1').getContent(),
                start_date: $("#start-date").val(),
                end_date: $("#end-date").val(),
                billable: $("#billable").val(),
                hours_planned: $("#planned-hours").val(),
                weight: $("#weight").val(),
                remaining_time: $("#remaining-time").val()
            };

            //request = mergeTask(actualTask, request);
            axios.post('/api/task/updatetask/'+task_id, request)
                .then(response => {
                    $("#save-task").text("Save & Close");
                    $("#task-details").modal("hide");
                    window.location.reload()
                }).catch(error => {
                alert('An error occured, could not save the task.');
                $("#save-task").text("Save & Close");
                console.log(error.response);
            });
        }

        function mergeTask(old_task,new_task){
            let submit_task = {}
            for (task_p in old_task){
                if (!new_task.hasOwnProperty(task_p)){
                    submit_task[task_p] = old_task[task_p];
                }else {
                    submit_task[task_p] = new_task[task_p];
                }

            }
            return submit_task;
        }

        function deleteTimeline(timeline_id){
            if (actualTask.status == 5){
                alert("You can't edit a completed task");
            }
            const confirmFlag = confirm('Are you sure you want to delete the timeline?')

            if(!confirmFlag){
                return false;
            }

            axios.post('/api/task/deletetimeline/'+timeline_id, {})
                .then(response => {
                    this.getTimelines(task_id);
                    alert('Timeline deleted successfully.')
                });
        }

        // function userStory(user_story){
        //     user_story_id = user_story.id;
        //     user_story_old = user_story;
        //     let total_time_captured = 0;

        //     user_story.tasks.forEach((task, index) => {
        //         task.timelines.forEach((line, key) => total_time_captured += (parseInt(line.total) + (parseInt(line.total_m)/60)))
        //     })
        //     tinymce.get('user-story-details').setContent(user_story.details??"");

        //     if (user_story.status_id == 4){
        //         $(".user-story-id").text(user_story.id).prop('disabled', true);
        //         $("#user-story-description").val(user_story.name).prop('disabled', true);
        //         tinymce.get('user-story-details').getBody().setAttribute('contenteditable', 'false');
        //         tinymce.get('user-story-discussion').getBody().setAttribute('contenteditable', 'false');
        //         $("#userstory-start-date").val(user_story.start_date).prop('disabled', true);
        //         $("#userstory-end-date").val(user_story.end_date).prop('disabled', true);
        //         $("#userstory-billable").html(selectOptions([[0, "No"], [1, "Yes"]], user_story.billable)).prop('disabled', true)
        //         $("#userstory-planned-hours").val(user_story.hours_planned).prop('disabled', true);
        //         $("#userstory-sprint-id").html(selectOptions(Object.entries({!! json_encode($sprints_drop_down??[]) !!}), user_story.sprint_id)).prop('disabled', true);
        //         $("#userstory-dependency").html(selectOptions(Object.entries({!! json_encode($user_story_drop_down??[]) !!}), user_story.dependency_id)).prop('disabled', true);
        //         $("#userstory-remaining-time").val(user_story.hours_planned - total_time_captured).prop('disabled', true);
        //         $("#userstory-file").prop('disabled', true);
        //     }else{
        //         $(".user-story-id").text(user_story.id);
        //         $("#user-story-description").val(user_story.name);
        //         $("#userstory-start-date").val(user_story.start_date);
        //         $("#userstory-end-date").val(user_story.end_date);
        //         $("#userstory-billable").html(selectOptions([[0, "No"], [1, "Yes"]], user_story.billable));
        //         $("#userstory-planned-hours").val(user_story.hours_planned);
        //         $("#userstory-sprint-id").html(selectOptions(Object.entries({!! json_encode($sprints_drop_down??[]) !!}), user_story.sprint_id));
        //         $("#userstory-dependency").html(selectOptions(Object.entries({!! json_encode($user_story_drop_down??[]) !!}), user_story.dependency_id));
        //         $("#userstory-remaining-time").val(user_story.hours_planned - total_time_captured);
        //     }

        //     $("#userstory-status").html(selectOptions(Object.entries({!! json_encode($user_story_status_drop_down??[]) !!}), user_story.status_id));

        //     getDiscussion(user_story_id);
        //     getDocuments("/api/userstory/getdocuments/"+user_story_id);

        //     $("#user-story-modal").modal("show");
        // }

        $("#save-user-story-discussion").on('click', function (){
            $("#user-story-discussion").text("sending...");
            axios.post('/userstory/'+user_story_id+'/discussion', {
                discussion: tinyMCE.get('user-story-discussion').getContent()
            })
                .then(response => {
                    alert(response.data.message)
                    tinyMCE.get('user-story-discussion').setContent("");
                    getDiscussion(user_story_id);
                    $("#save-user-story-discussion").text("Save Note");
                })
                .catch(error => {
                    alert('An error occured, could not save the task.');
                    tinyMCE.get('user-story-discussion').setContent("");
                    $("#save-user-story-discussion").text("Save Note");

                });
        });

        function getDiscussion(user_story_id){
            axios.get('/userstorydiscussion/'+user_story_id, {})
                .then(response => {
                    let messages = response.data.discussion;
                    let discussion = "";
                    messages.forEach((message, index) =>{
                        let style = "background-color: " + randomColor(getInitials(message.user.first_name, message.user.last_name));
                        discussion += "<tr>" +
                            "<td style=\"width: 5%;\">"+
                            '<span class="chat-initials" title="'+getInitials(message.user.first_name, message.user.last_name)+'" style="'+style+'">'+getInitials(message.user.first_name, message.user.last_name) + '</span>' +

                            "</td>"+
                            '<td style="width: 95%;"><div class="form-control form-control-sm col-md-12">'+ message.discussion +'</div></td>' +
                            "</tr>";
                    });

                    let messages_count = (messages != null) ? messages.length : 0;

                    $("#discussion-comment-no").text(messages_count);
                    $("#userstory-discussion-chat").html(discussion);
                }).catch(e =>{
                console.log(e);
            });
        }

        function updateUserStory() {
            $("#save-userstory").text("Sending...")
            let request = {
                name: $("#user-story-description").val(),
                sprint_id: $("#userstory-sprint-id").val(),
                dependency_id: $("#userstory-dependency").val(),
                status_id: $("#userstory-status").val(),
                details: tinyMCE.get('user-story-details').getContent(),
                start_date: $("#userstory-start-date").val(),
                end_date: $("#userstory-end-date").val(),
                billable: $("#userstory-billable").val(),
                hours_planned: $("#userstory-planned-hours").val()
            };

            request = mergeTask(user_story_old, request);

            axios.post('/api/userstory/'+user_story_id+"/update", request)
                .then(response => {
                    $("#save-userstory").text("Save & Close");
                    $("#user-story-modal").modal("hide");
                    window.location.reload()
                })
                .catch(error => {
                    alert('An error occured, could not save the task.');
                    $("#save-userstory").text("Save & Close");
                    console.log(error.response);
                });
        }

    </script>
@endsection
@section('extra-css')
    <style>
        .task-tree-view td:last-child,#list td:last-child{
            white-space: nowrap;
        }
        
        .task-tree-view {
            cursor: pointer;
            background-color: #F4F4F4;
        }

        .task-tree-view:hover {
            background-color: #f0f0f0;
        }

        #btnCE{
            cursor: pointer !important;
        }

        .fa-plus, .fa-minus {
            font-size: 12px;
            font-weight: bold;
        }
        .modal-lg{
            max-width: 1200px!important;
        }
        #toggle-arrow:hover, #toggle-discussion-arrow:hover,
        #toggle-planning:hover{
            color: #357ca5!important;
            cursor: pointer;
        }
        .fullscreen-position{
            left: -295px!important;
            top: -30px!important;
            padding: 40px 100px!important;
            background-color: rgba(0,0,0, .7)!important;
            height: 100vh!important;
        }
        .modal{
            z-index: 1150;
        }
        .tox .tox-collection__item-checkmark, .tox .tox-collection__item-icon {
            align-items: center;
            display: flex;
            height: 24px;
            justify-content: left !important;
            width: 260px !important;
        }

        .tox .tox-collection--grid .tox-collection__group {
            display: block !important;
            flex-wrap: wrap;
            max-height: 208px;
            overflow-x: hidden;
            overflow-y: auto;
            padding: 0;
        }
        .chat-initials {
            padding: 5px;
            border-radius: 25px;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
@endsection