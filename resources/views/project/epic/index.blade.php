@extends('adminlte.default')
@section('title') Epic @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('epic.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Epic</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name','Name')</th>
                    <th>@sortablelink('project.name','Project')</th>
                    <th>@sortablelink('start_date','Start Date')</th>
                    <th>@sortablelink('end_date','End Date')</th>
                    <th>@sortablelink('hours_planned','Hours Planned')</th>
                    <th>@sortablelink('confindence_percentage','Confidence %')</th>
                    <th>@sortablelink('estimate_cost','Estimate Cost')</th>
                    <th>@sortablelink('estimate_income','Estimate Income')</th>
                    <th>@sortablelink('billable','Billable')</th>
                    <th>@sortablelink('sprint.name','Sprint')</th>
                    <th>@sortablelink('status.name','Status')</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($epics as $epic)
                    <tr>
                        <td><a href="{{route('epic.show',$epic)}}">{{$epic->name}}</a></td>
                        <td>{{isset($epic->project->name)?$epic->project->name:''}}</td>
                        <td>{{isset($epic->start_date)?$epic->start_date:''}}</td>
                        <td>{{isset($epic->end_date)?$epic->end_date:''}}</td>
                        <td>{{isset($epic->hours_planned)?$epic->hours_planned:''}}</td>
                        <td>{{isset($epic->confidence_percentage)?$epic->confidence_percentage:''}}</td>
                        <td>{{isset($epic->estimate_cost)?$epic->estimate_cost:''}}</td>
                        <td>{{isset($epic->estimate_income)?$epic->estimate_income:''}}</td>
                        <td>{{($epic->billable == 1)?'Yes':'No'}}</td>
                        <td>{{isset($epic->sprint->name)?$epic->sprint->name:''}}</td>
                        <td>{{isset($epic->status->name)?$epic->status->name:''}}</td>
                        <td class="text-right">
                            @if($epic->status_id != 4)
                                <a href="{{route('feature.create')}}?epic_id={{$epic->id}}" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i>  Feature</a>
                                <a href="{{route('epic.edit',$epic)}}" class="btn btn-success btn-sm">Edit</a>
                            @endif
                            {{ Form::open(['method' => 'DELETE','route' => ['epic.destroy', $epic],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" class="text-center">No epic entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
