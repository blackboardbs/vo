@extends('adminlte.default')

@section('title') Edit Epic @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('epic.update', $epic), 'method' => 'put','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <input type="hidden" id="parent_start_date" name="parent_start_date" value="{{isset($project->start_date) ? $project->start_date: ''}}" />
            <input type="hidden" id="parent_end_date" name="parent_end_date" value="{{isset($project->end_date) ? $project->end_date : ''}}" />
            <input type="hidden" id="parent_name" name="parent_name" value="Project" />
            <input type="hidden" id="child_name" name="child_name" value="Epic" />
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $epic->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'name', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $epic->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null),'id'=>'project_id'/*, 'onclick' => 'getParentDates("project", this)'*/])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $epic->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id'=>'start_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $epic->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'end_date', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Hours Planned:</th>
                    <td>
                        {{Form::text('hours_planned', $epic->hours_planned, ['class'=>'form-control form-control-sm'. ($errors->has('hours_planned') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'hours_planned'])}}
                        @foreach($errors->get('hours_planned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Confidence %:</th>
                    <td>
                        {{Form::text('confidence_percentage', $epic->confidence_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('confidence_percentage') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null),'id'=>'confidence_percentage'])}}
                        @foreach($errors->get('confidence_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Cost:</th>
                    <td>
                        {{Form::text('estimate_cost', $epic->estimate_cost, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_cost') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'estimate_cost', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('estimate_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Estimated Income:</th>
                    <td>
                        {{Form::text('estimate_income', $epic->estimate_income, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_income') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null),'id'=>'estimate_income'])}}
                        @foreach($errors->get('estimate_income') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable:</th>
                    <td>
                        {{Form::select('billable', [1 => 'Yes', 0 => 'No'], $epic->billable, ['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'billable', 'autocomplete' => 'off', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint:</th>
                    <td>
                        {{Form::select('sprint_id', $sprintDropDown, $epic->sprint_id, ['class'=>'form-control form-control-sm '. ($errors->has('sprint_id') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null),'id'=>'sprint_id'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td colspan="3">
                        {{Form::select('status_id', $statusDropDown, $epic->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''), 'id'=>'status_id'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note', $epic->note, ['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : ''), ($epic->status_id == 4 ? "disabled":null), 'id' => 'note', 'autocomplete' => 'off'])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
    @include('project.alert_modal')
@endsection
