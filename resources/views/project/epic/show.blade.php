@extends('adminlte.default')

@section('title') View Epic @endsection

@section('header')
    <div class="container-fluid container-title">
        <div class="container-fluid container-title">
            <h3>@yield('title')</h3>
            <a href="{{route('feature.create')}}?epic_id={{$epic->id}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Feature</a>
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="{{route('epic.edit', $epic->id)}}" class="btn btn-success float-right" style="margin-right: 10px;"><i class="fa fa-edit"></i> Edit</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $epic->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project:</th>
                    <td>
                        {{Form::select('project_id', $projectDropDown, $epic->project_id, ['class'=>'form-control form-control-sm '. ($errors->has('project_id') ? ' is-invalid' : ''),'id'=>'project_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('project_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>
                        {{Form::text('start_date', $epic->start_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id'=>'start_date', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date:</th>
                    <td>
                        {{Form::text('end_date', $epic->end_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'end_date', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Hours Planned:</th>
                    <td>
                        {{Form::text('hours_planned', $epic->hours_planned, ['class'=>'form-control form-control-sm'. ($errors->has('hours_planned') ? ' is-invalid' : ''), 'id' => 'hours_planned', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('hours_planned') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Confidence %:</th>
                    <td>
                        {{Form::text('confidence_percentage', $epic->confidence_percentage, ['class'=>'form-control form-control-sm'. ($errors->has('confidence_percentage') ? ' is-invalid' : ''),'id'=>'confidence_percentage', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('confidence_percentage') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Estimated Cost:</th>
                    <td>
                        {{Form::text('estimate_cost', $epic->estimate_cost, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_cost') ? ' is-invalid' : ''), 'id' => 'estimate_cost', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('estimate_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Estimated Income:</th>
                    <td>
                        {{Form::text('estimate_income', $epic->estimate_income, ['class'=>'form-control form-control-sm'. ($errors->has('estimate_income') ? ' is-invalid' : ''),'id'=>'estimate_income', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('estimate_income') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable:</th>
                    <td>
                        {{Form::select('billable', [1 => 'Yes', 0 => 'No'], $epic->billable, ['class'=>'form-control form-control-sm'. ($errors->has('billable') ? ' is-invalid' : ''), 'id' => 'billable', 'readonly' => 'readonly', 'placeholder' => 'Please select...'])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Sprint:</th>
                    <td>
                        {{Form::select('sprint_id', $sprintDropDown, $epic->sprint_id, ['class'=>'form-control form-control-sm '. ($errors->has('sprint_id') ? ' is-invalid' : ''),'id'=>'sprint_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('sprint_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Status:</th>
                    <td colspan="3">
                        {{Form::select('status_id', $statusDropDown, $epic->status_id, ['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note', $epic->note, ['size' => '30x5', 'class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : ''), 'id' => 'note', 'readonly' => 'readonly'])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
