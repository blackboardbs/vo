@extends('adminlte.default')
@section('title') Feature @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('feature.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Feature</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('name','Name')</th>
                    <th>@sortablelink('epic.name','Epic')</th>
                    <th>@sortablelink('start_date','Start Date')</th>
                    <th>@sortablelink('end_date','End Date')</th>
                    <th>@sortablelink('hours_planned','Hours Planned')</th>
                    <th>@sortablelink('confindence_percentage','Confidence %')</th>
                    <th>@sortablelink('estimate_cost','Estimate Cost')</th>
                    <th>@sortablelink('estimate_income','Estimate Income')</th>
                    <th>@sortablelink('billable','Billable')</th>
                    <th>@sortablelink('sprint.name','Sprint')</th>
                    <th>@sortablelink('status.name','Status')</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($features as $feature)
                    <tr>
                        <td><a href="{{route('feature.show',$feature)}}">{{$feature->name}}</a></td>
                        <td>{{isset($feature->epic->name)?$feature->epic->name:''}}</td>
                        <td>{{isset($feature->start_date)?$feature->start_date:''}}</td>
                        <td>{{isset($feature->end_date)?$feature->end_date:''}}</td>
                        <td>{{isset($feature->hours_planned)?$feature->hours_planned:''}}</td>
                        <td>{{isset($feature->confidence_percentage)?$feature->confidence_percentage:''}}</td>
                        <td>{{isset($feature->estimate_cost)?$feature->estimate_cost:''}}</td>
                        <td>{{isset($feature->estimate_income)?$feature->estimate_income:''}}</td>
                        <td>{{($feature->billable == 1)?'Yes':'No'}}</td>
                        <td>{{isset($feature->sprint->name)?$feature->sprint->name:''}}</td>
                        <td>{{isset($feature->status->name)?$feature->status->name:''}}</td>
                        <td class="text-right">
                            @if($feature->status_id != 4)
                                <a href="{{route('userstory.create')}}?feature_id={{$feature->id}}" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i> User Story</a>
                                <a href="{{route('feature.edit',$feature)}}" class="btn btn-success btn-sm">Edit</a>
                            @endif
                            {{ Form::open(['method' => 'DELETE','route' => ['feature.destroy', $feature],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No feature entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
