@extends('adminlte.default')
@section('title') Project @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <a href="{{route('project.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-caret-left"></i> Back</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('project.update', $project->id), 'method' => 'put','class'=>'mt-3', 'files' => true,'autocomplete'=>'off'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Edit work breakdown structure No.{{$wbs_nr}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Project Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::text('name',$project->name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('name') ? ' is-invalid' : ''), 'placeholder'=>'Project Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Project Ref. (Timesheet): <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::text('ref',$project->ref,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref') ? ' is-invalid' : ''), 'placeholder'=>'Project ref'])}}
                        @foreach($errors->get('ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Type: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('project_type_id',$project_type_drop_down,$project->project_type_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('project_type_id') ? ' is-invalid' : ''), 'placeholder'=>'Please Select'])}}
                        @foreach($errors->get('project_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Internal Owner: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('manager_id',$consultant_drop_down,$project->manager_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('manager_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('manager_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Customer: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('customer_id',$customer_drop_down,$project->customer_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('customer_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer Ref.:</th>
                    <td>{{Form::text('customer_ref',$project->customer_ref,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('customer_ref') ? ' is-invalid' : ''),'placeholder'=>'Customer Ref No'])}}
                        @foreach($errors->get('customer_ref') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>

                <tr>
                    <th>Customer PO Number:</th>
                    <td>{{Form::text('cust_order_number',$project->customer_po,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('cust_order_number') ? ' is-invalid' : ''),'placeholder'=>'Customer Order Number'])}}
                        @foreach($errors->get('cust_order_number') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Customer Invoice Contact: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('customer_invoice_contact_id',$invoice_contact_dropdown,$project->customer_invoice_contact_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('customer_invoice_contact_id') ? ' is-invalid' : ''), 'placeholder' => "Select Customer Invoice Contact"])}}
                        @foreach($errors->get('customer_invoice_contact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Vendor Invoice Contact:</th>
                    <td>{{Form::select('vendor_invoice_contact_id',$invoice_contact_dropdown, $project->vendor_invoice_contact_id,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('vendor_invoice_contact_id') ? ' is-invalid' : ''),'placeholder'=>'Select Vendor Invoice Contact'])}}
                        @foreach($errors->get('vendor_invoice_contact_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Company: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('company_id',$company_drop_down,$project->company_id,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('company_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('company_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Hours of Work Per Day:</th>
                    <td>{{Form::text('hours_of_work',$project->hours_of_work,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hours_of_work') ? ' is-invalid' : ''),'placeholder'=>'0'])}}
                        @foreach($errors->get('hours_of_work') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Location:</th>
                    <td>{{Form::text('location',$project->location,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('location') ? ' is-invalid' : ''),'placeholder'=>'Location'])}}
                        @foreach($errors->get('location') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Opex/Capex:</th>
                    <td>{{Form::text('opex_project',$project->opex_project,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('opex_project') ? ' is-invalid' : ''),'placeholder'=>'Opex/Capex'])}}
                        @foreach($errors->get('opex_project') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Default Timesheet Template
                    </th>
                    <td>
                        {{Form::select('timesheet_template_id', [1 => 'Standard Template', 2 => 'Weekly Template', 3 => 'Arbour Template'], $project->timesheet_template_id, ['class'=>'form-control form-control-sm'. ($errors->has('timesheet_template_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('timesheet_template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>


                <tr>
                    <th>Project Fixed Price:</th>
                    <td>
                        {{Form::select('is_fixed_price', ['' => 'Please select ...', 1 => 'Yes', 2 => 'No'], $project->is_fixed_price, ['class'=>'form-control form-control-sm'. ($errors->has('is_fixed_price') ? ' is-invalid' : ''), 'id' => 'is_fixed_price'])}}
                        @foreach($errors->get('is_fixed_price') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td colspan="2" class="placeholder_fixed_price_fileds" style="display: none">&nbsp;</td>
                    <th class="fixed_price_fileds">
                        Project Fixed Price Labour
                    </th>
                    <td class="fixed_price_fileds">{{Form::text('fixed_price_labour', $project->fixed_price_labour,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_labour') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Price Labour'])}}
                        @foreach($errors->get('fixed_price_labour') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="fixed_price_fileds">
                    <th>Fixed Price Expenses:</th>
                    <td>{{Form::text('fixed_price_expenses', $project->fixed_price_expenses,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('fixed_price_expenses') ? ' is-invalid' : ''),'placeholder'=>'Fixed Price Expenses'])}}
                        @foreach($errors->get('fixed_price_expenses') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Project Total Fixed Price
                    </th>
                    <td>{{Form::text('total_fixed_price', $project->total_fixed_price,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('total_fixed_price') ? ' is-invalid' : ''),'placeholder'=>'Project Total Fixed Price'])}}
                        @foreach($errors->get('total_fixed_price') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="fixed_price_fileds">
                    <th>Project Fixed Cost Labour:</th>
                    <td>{{Form::text('project_fixed_cost_labour', $project->project_fixed_cost_labour,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_labour') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Cost Labour'])}}
                        @foreach($errors->get('project_fixed_cost_labour') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>
                        Project Fixed Cost Expense
                    </th>
                    <td>{{Form::text('project_fixed_cost_expense', $project->project_fixed_cost_expense,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_fixed_cost_expense') ? ' is-invalid' : ''),'placeholder'=>'Project Fixed Cost Expense'])}}
                        @foreach($errors->get('project_fixed_cost_expense') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th  class="fixed_price_fileds">Project Total Fixed Cost:</th>
                    <td  class="fixed_price_fileds">{{Form::text('project_total_fixed_cost', $project->project_total_fixed_cost,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('project_total_fixed_cost') ? ' is-invalid' : ''),'placeholder'=>'Project Total Fixed Cost'])}}
                        @foreach($errors->get('project_total_fixed_cost') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Billing Cycle</th>
                    <td>{{Form::select('billing_cycle_id', $billing_cycle_dropdown, $project->billing_cycle_id, ['class'=>'form-control form-control-sm '. ($errors->has('billing_cycle_id') ? ' is-invalid' : ''), 'placeholder' => 'Billing Cycle'])}}
                        @foreach($errors->get('billing_cycle_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <td colspan="2" class="placeholder_fixed_price_fileds" style="display: none">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Resourcing</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Consultants: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">
                        {{Form::select('consultants[]',$consultant_drop_down,$consultants,['id'=>'consultants', 'class'=>'form-control '. ($errors->has('consultants') ? ' is-invalid' : ''),'multiple'])}}
                        @foreach($errors->get('consultants') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>

                </tbody>
                <tfoot id="consultant_roles" @if(isset($consultant_role_users) && !empty($consultant_role_users)) style="" @else style="display: none;" @endif>
                @if(isset($consultant_role_users) && !empty($consultant_role_users))
                    @foreach($consultant_role_users as $key => $consultant_role_user)
                        <tr>
                            <th>Consultant:</th>
                            <td>
                                {{Form::select('consultant_role_user_'.$consultant_role_user->id,$consultant_role_user_drop_down,$consultants[$key],['id'=>'consultant_role_user', 'class'=>'form-control '. ($errors->has('consultant_role_user') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                                @foreach($errors->get('consultant_role_user') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error }}
                                    </div>
                            @endforeach
                            </th>
                            <th>Role:</th>
                            <td>
                                {{Form::select('consultant_role_'.$consultant_role_user->id, $roles_drop_down, $consultant_role_user->getViewRole(30, $consultant_role_user->id),['id'=>'consultant_role', 'class'=>'form-control '. ($errors->has('consultant_role') ? ' is-invalid' : ''), 'onchange' => "changeRole(".$consultant_role_user->id.", this)"])}}
                                @foreach($errors->get('consultant_role') as $error)
                                    <div class="invalid-feedback">
                                        {{ $error }}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="4"><!-- // Display all roles here --></td></tr>
                @endif
                </tfoot>
            </table>
            <hr/>
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                <tr>
                    <th>Start Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date'])}}
                        @foreach($errors->get('start_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>End Date: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date'])}}
                        @foreach($errors->get('end_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billable: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('billable',[''=>'Please Select',0=>'No',1=>'Yes'],$project->billable,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('billable') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('billable') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Project Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('status_id',$project_status_drop_down,$project->status_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Billable Expenses</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Select Terms: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">
                        {{Form::select('terms_version',$terms_drop_down,$project->terms_version,['id'=>'terms_version', 'class'=>'form-control ', 'onchange' => 'changeTerms()'])}}
                    </td>
                </tr>
                <tr>
                    <th>Travel:</th>
                    <td>{{Form::textarea('travel',isset($expense->travel)? $expense->travel: '',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'travel'])}}
                        @foreach($errors->get('travel') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Parking:</th>
                    <td>{{Form::textarea('parking',isset($expense->parking)?$expense->parking:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'parking'])}}
                        @foreach($errors->get('parking') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Accommodation:</th>
                    <td>{{Form::textarea('accommodation',isset($expense->accommodation)?$expense->accommodation:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'accommodation'])}}
                        @foreach($errors->get('accommodation') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Per Diem:</th>
                    <td>{{Form::textarea('per_diem',isset($expense->per_diem)?$expense->per_diem:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'per_diem'])}}
                        @foreach($errors->get('per_diem') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Out of Town Allowance:</th>
                    <td>{{Form::textarea('out_of_town',isset($expense->out_of_town)?$expense->out_of_town:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'out_of_town'])}}
                        @foreach($errors->get('out_of_town') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Data:</th>
                    <td>{{Form::textarea('data',isset($expense->data)?$expense->data:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'data'])}}
                        @foreach($errors->get('data') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Other:</th>
                    <td colspan="3">{{Form::textarea('other',isset($expense->other)?$expense->other:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id' => 'other'])}}
                        @foreach($errors->get('other') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Billing Note:</th>
                    <td colspan="3">
                        {{Form::textarea('billing_note',isset($project->billing_note)?$project->billing_note:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>''])}}
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>

        let consultant_roles = [];
        // Set default javascript values, vuejs would have done this so easy
        @if(isset($consultants) && !empty($consultants))
            consultant_roles = [
                @foreach($consultants as $consultant_id)
                    {
                        consultant: '{!! $consultant_id !!}',
                        role: '{!! $consultant_role_user->getViewRole(30, $consultant_id) !!}'
                    },
                @endforeach
            ];
        @endif

        let consultant_drop_down = {
            @foreach($consultant_drop_down as $key => $consultant)
            {!! "'".$key."':'".$consultant."'," !!}
            @endforeach
        };
        let roles_drop_down = [@foreach($roles_drop_down as $role) {!! "'".$role."'," !!} @endforeach];
        let temp_constultant_role = [];

        function changeRole(consultant, element){
            for(let i = 0; i < consultant_roles.length; i++){
                if(consultant_roles[i].consultant == consultant){
                    consultant_roles[i].role = element.value;
                }
            }
        }

        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            $('#consultants').change(function () {
                let selected_consultants = $(this).val();
                let html_data = '';
                let options = '';
                temp_constultant_role = consultant_roles;
                consultant_roles = [];
                if(selected_consultants != null) {
                    $('#consultant_roles').show();
                    for (let i = 0; i < selected_consultants.length; i++) {

                        if (selected_consultants[i] != 0) {
                            let object = {
                                'consultant': selected_consultants[i],
                                'role': 2
                            };
                            for(let k = 0; k < temp_constultant_role.length; k++){
                                if(temp_constultant_role[k].consultant == selected_consultants[i]){
                                    object = {
                                        'consultant': selected_consultants[i],
                                        'role': temp_constultant_role[k].role
                                    };
                                    console.log('Consultant', temp_constultant_role[k]);
                                }
                            }
                            consultant_roles.push(object);

                            options = '';
                            for(j = 0; j < roles_drop_down.length; j++){
                                let selected = '';
                                for(let k = 0; k < consultant_roles.length; k++){
                                    if(consultant_roles[k].consultant == selected_consultants[i]) {
                                        if (j == consultant_roles[k].role) {
                                            selected = 'selected';
                                        }
                                    }
                                }
                                options += '<option value="'+j+'" '+selected+'>'+roles_drop_down[j]+'</option>';
                            }

                            html_data += '<tr>' +
                                '<th>Consultant:</th>' +
                                '<td>' +
                                '<select id="consultant_role_user_'+selected_consultants[i]+'" class="form-control form-control-sm col-sm-12" disabled="disabled">' +
                                '<option>' + consultant_drop_down[selected_consultants[i]] + '</option>' +
                                '</select>' +
                                '</th>' +
                                '<th>Role:</th>' +
                                '<td>' +
                                '<select id="consultant_role_'+selected_consultants[i]+'" name="consultant_role_'+selected_consultants[i]+'" onchange="changeRole('+selected_consultants[i]+', this)" class="form-control form-control-sm col-sm-12">' +
                                options +
                                '</select>' +
                                '</td>' +
                                '</tr>';

                        } else {
                            // alert('test');
                        }
                    }
                    $('#consultant_roles').html(html_data);

                    console.log($(this).val());
                } else {
                    $('#consultant_roles').hide();
                }
            });

            @if($project->is_fixed_price == 1)
                $('.fixed_price_fileds').show();
                $('.placeholder_fixed_price_fileds').hide();
            @else
                $('.fixed_price_fileds').hide();
                $('.placeholder_fixed_price_fileds').show();
            @endif

            $('#is_fixed_price').change(function(){
                let is_fixed_price = $("select[name=is_fixed_price]").val();
                if(is_fixed_price == 2){
                    $('.fixed_price_fileds').hide();
                    $('.placeholder_fixed_price_fileds').show();
                } else {
                    $('.fixed_price_fileds').show();
                    $('.placeholder_fixed_price_fileds').hide();
                }
            });
        });

        function changeTerms(){
            var data = {
                terms_id: $("select[name=terms_version]").val()
            };
            axios.post('{{route('project.getterms')}}', data)
                .then(function (data) {
                    $('#travel').html(data['data'].terms.exp_travel);
                    $('#parking').html(data['data'].terms.exp_parking);
                    $('#accommodation').html(data['data'].terms.exp_accommodation);
                    $('#per_diem').html(data['data'].terms.exp_per_diem);
                    $('#out_of_town').html(data['data'].terms.exp_out_of_town);
                    $('#data').html(data['data'].terms.exp_data);
                    $('#other').html(data['data'].terms.exp_other);
                })
                .catch(function () {
                    console.log("An Error occured!!!");
                });
        }
    </script>
@endsection
