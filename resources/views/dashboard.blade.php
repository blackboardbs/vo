@extends('adminlte.default')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v3</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        @if(Auth::user()->is('admin'))
            @include('dashboard.system-admin')
        @elseif(Auth::user()->is('admin_manager'))
            @include('dashboard.admin-manager')
        @elseif(Auth::user()->is('consultant') || Auth::user()->is('contractor'))
            @include('dashboard.consultant')
        @elseif(Auth::user()->is('manager'))
            @include('dashboard.manager')
        @elseif(Auth::user()->is('vendor') || Auth::user()->is('customer_client'))
            @include('dashboard.vendor')
        @endif
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('extra-js')
<script src="{!! global_asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
<script src="{!! global_asset('adminlte/dist/js/pages/dashboard3.js') !!}"></script>
@endsection