@extends('adminlte.default')
@section('title') Module View Permissions @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('module',$modules_drop_down,old('module'),['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                <span>Module View Permission|</span>
                    </label>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>#</th>
                    <th>@sortablelink('name', 'Name')</th>
                    <th>@sortablelink('status.description','Status')</th>
                    <th class="last">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($modules_view as $module)
                    <tr>
                        <td>{{$module->id}}</td>
                        <td><a href="{{route('viewpermission.show', $module)}}">{{$module->display_name}}</a></td>
                        <td>{{isset($module->status->description) ? $module->status->description : '' }}</td>
                        <td><a href="{{route('viewpermission.edit',$module)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No modules match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $modules_view->firstItem() }} - {{ $modules_view->lastItem() }} of {{ $modules_view->total() }}
                        </td>
                        <td>
                            {{ $modules_view->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{--{{ $module->appends(request()->except('page'))->links() }}--}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection