@extends('adminlte.default')
@section('title') {{$module->display_name}} View Permissions @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="table-responsive">
            {{Form::open(['url' => route('viewpermission.update',$module->id), 'method' => 'patch','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm table-hover">
                <thead>
                    <tr class="btn-dark">
                        <th>Role</th>
                        @foreach($module_sections as $module_section)
                        <th>{{$module_section->name}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                @foreach($module_roles as $module_role)
                    <tr>
                        <td>{{$module_role->name}}</td>
                        @foreach($module_sections as $module_section)
                        <td>
                            {{Form::select(str_replace(' ', '-', strtolower($module_section->name)).'_'.$module_role->id.'_'.$module_section->id, $permissions_drop_down, $module_role->getPermission($module->id, $module_role->id, $module_section->id),['class'=>'form-control form-control-sm '. ($errors->has($module_section->name.'_'.$module_role->id) ? ' is-invalid' : ''), 'placeholder' => 'None'])}}
                        </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
            <table class="table table-borderless">
                <tr>
                    <td colspan="4" class="text-center">
                        <button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a>
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection