@extends('adminlte.default')

@section('title') View Industry Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('industry_experience.edit', $industry_experience[0])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Industry Experience</a>
            </div>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($industry_experience as $result)
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Resource</th>
                        <td> {{$result->resource['emp_firstname']. ' '.$result->resource['emp_lastname']}} </td>
                        <th>Industry</th>
                        <td>{{$result->industry->description}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>{{$result->statusd->description}}</td>
                    </tr>
                </table>
            @endforeach
        </div>
    </div>
@endsection