@extends('adminlte.default')

@section('title') Industry Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('industry_experience.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Industry Experience</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('res_id','Resource')</th>
                    <th>@sortablelink('ind_id','Industry')</th>
                    <th>@sortablelink('statusd.description','status')</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($industry_experience as $result)
                    <tr>
                        <td><a href="{{route('industry_experience.show',$result)}}">{{$result->resource->first_name. ' '.$result->resource->last_name}}</a></td>
                        <td>{{$result->industry->description}}</td>
                        <td>{{$result->statusd->description}}</td>
                        <td>
                            <a href="{{route('industry_experience.edit',$result)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['industry_experience.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data industry experience entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $industry_experience->links() }}
        </div>
    </div>
@endsection
