@extends('adminlte.default')

@section('title') Add Industry Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1 float-right">Save</a>
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('industry_experience.store'), 'method' => 'post','class'=>'mt-3','id'=>'saveForm'])}}
            <input type="hidden" id="cv_id" name="cv_id" value="{{$cv_id}}" />
            <input type="hidden" id="res_id" name="res_id" value="{{$res_id}}" />
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Industry <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('ind_id[]',$industry_dropdown,$industries,['class'=>'form-control form-control-sm '. ($errors->has('ind_id') ? ' is-invalid' : ''),'id'=>'ind_id'])}}
                        @foreach($errors->get('ind_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Resource <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        <select name="res_id" class="form-control form-control-sm" id="resource_dropdown">
                            @foreach($resource_dropdown as $rs)
                                <option value="{{ $rs->id }}">{{ $rs->full_name }}</option>
                            @endforeach
                        </select>
                        @foreach($errors->get('res_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::select('status',$status_dropdown,1,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                        @foreach($errors->get('status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection