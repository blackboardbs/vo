@extends('adminlte.default')

@section('title') Edit Industry Experience @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            @foreach($industry_experience as $result)
                {{Form::open(['url' => route('industry_experience.update',['costid' => $result]), 'method' => 'post','class'=>'mt-3'])}}
                <input type="hidden" id="cv_id" name="cv_id" value="{{$cv_id}}" />
                <input type="hidden" id="res_id" name="res_id" value="{{$res_id}}" />
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Industry <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('ind_id',$industry_dropdown,$result->ind_id,['class'=>'form-control form-control-sm ','id'=>'industry_dropdown'])}}
                            @foreach($errors->get('ind_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Resource <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('res_id',$resource_dropdown,$result->res_id,['class'=>'form-control form-control-sm ','id'=>'resource_dropdown'])}}
                            @foreach($errors->get('res_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td colspan="3">{{Form::select('status',$status_dropdown,$result->status,['class'=>'form-control form-control-sm ','id'=>'status_dropdown'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                <table class="table table-borderless">
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </table>
                {{Form::close()}}
            @endforeach
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection