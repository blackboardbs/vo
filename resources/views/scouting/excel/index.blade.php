<table>
    <thead>
    <tr>
        <th>Resource Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Process Status</th>
        <th>Process Interview</th>
        <th>Role</th>
        <th>Vendor</th>
        <th>Technical Rating</th>
        <th>Business Rating</th>
        <th>Resource Level</th>
    </tr>
    </thead>
    <tbody>
    @foreach($scoutings as $scouting)
        <tr>
            <td>{{$scouting->resource_first_name}} {{$scouting->resource_last_name}}</td>
            <td>{{$scouting->phone}}</td>
            <td>{{$scouting->email}}</td>
            <td>{{$scouting->processstatus?->name}}</td>
            <td>{{$scouting->interviewstatus?->name}}</td>
            <td>{{$scouting->role?->name}}</td>
            <td>{{$scouting->vendor?->vendor_name}}</td>
            <td>{{$scouting->businessrating?->name}}</td>
            <td>{{$scouting->technicalrating?->name}}</td>
            <td>{{$scouting->resourcelevel?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>