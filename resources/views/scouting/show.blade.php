@extends('adminlte.default')
@section('title') Scouting @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('scouting.edit',$scouting->id)}}" class="btn btn-success float-right" style="margin-left:5px;">Edit</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark" style="text-align: center;">Create Scouting</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Resource First Name:</th>
                    <td>
                        {{Form::text('resource_first_name',$scouting->resource_first_name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_first_name') ? ' is-invalid' : ''), 'placeholder'=>'First Name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('resource_first_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Resource Last Name</th>
                    <td>
                        {{Form::text('resource_last_name',$scouting->resource_last_name,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_last_name') ? ' is-invalid' : ''), 'placeholder'=>'Last name', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('resource_last_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Role:</th>
                    <td>{{Form::select('role_id',$role_drop_down,$scouting->role_id,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Email:</th>
                    <td>{{Form::text('email',$scouting->email,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Process Status:</th>
                    <td>{{Form::select('process_status_id',$process_statuses_drop_down,$scouting->process_status_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('process_status_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('process_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Process Interview:</th>
                    <td>{{Form::select('interview_status_id',$process_interview_drop_down,$scouting->interview_status_id,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('interview_status_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('interview_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                <tr>
                    <th>Vendor:</th>
                    <td>{{Form::select('vendor_id',$vendor_drop_down, $scouting->vendor_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('vendor_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('vendor_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Technical Rating:</th>
                    <td>{{Form::select('technical_rating_id',$tecnical_rating_drop_down, $scouting->technical_rating_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('technical_rating_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('technical_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Business Rating:</th>
                    <td>{{Form::select('business_rating_id',$business_rating_drop_down, $scouting->business_rating_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('business_rating_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('business_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Resource Level:</th>
                    <td>{{Form::select('resource_level_id',$resource_level_drop_down, $scouting->resource_level_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('resource_level_id') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('resource_level_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Estimated Hourly Rate</th>
                    <td>
                        {{Form::text('estimated_hourly_rate',$scouting->estimated_hourly_rate,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('estimated_hourly_rate') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('estimated_hourly_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Availability:</th>
                    <td>
                        {{Form::text('availability',$scouting->availability,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('availability') ? ' is-invalid' : ''), 'placeholder'=>'Availability', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('availability') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Hourly Rate Minimum:</th>
                    <td>
                        {{Form::text('hourly_rate_minimum',$scouting->hourly_rate_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('hourly_rate_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Hourly Rate Maximum:</th>
                    <td>
                        {{Form::text('hourly_rate_maximum',$scouting->hourly_rate_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('hourly_rate_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Monthly Salary Minimum</th>
                    <td>
                        {{Form::text('monthly_salary_minimum',$scouting->monthly_salary_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('monthly_salary_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Monthly Salary Maximum:</th>
                    <td>
                        {{Form::text('monthly_salary_maximum',$scouting->monthly_salary_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum', 'disabled'=>'disabled'])}}
                        @foreach($errors->get('monthly_salary_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Referred By:</th>
                    <td>{{Form::select('referral',$users_drop_down, $scouting->referral,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('referral') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('referral') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assessed By:</th>
                    <td>
                        {{Form::select('assessed_by',$users_drop_down, $scouting->assessed_by,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('assessed_by') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                        @foreach($errors->get('assessed_by') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                {{--<tr>
                    <th>Document Link:</th>
                    <td colspan="3">
                        @forelse($documents as $document)
                            <a href="{{ route('getfile', $document) }}" target="_blank" download="{{$document->name}}">{{$document->name}}</a><br>
                        @empty
                            No documents uploaded for this resource
                        @endforelse
                    </td>
                </tr>--}}
                </tbody>
            </table>

            <table class="table table-borderless table-sm">
                <tbody>
                <tr>
                    <th colspan="5" class="btn-dark">Documents</th>
                </tr>
                <tr>
                    <th>Document</th>
                    <th>File Name</th>
                    <th>Upload Date</th>
                    <th>Uploaded by</th>
                    <th>Action</th>
                </tr>
                @forelse($documents as $document)
                    <tr>
                        <td>{{ Html::link($document->file, $document->name) }}</td>
                        <td>{{$document->file}}</td>
                        <td>{{$document->created_at}}</td>
                        <td>{{isset($document->user->first_name)?$document->user->first_name:''}} {{isset($document->user->last_name)?$document->user->last_name:''}}</td>
                        <td>
                            <a href="{{route('document.edit', $document->id)}}?reference_id={{$document->reference_id}}&document_type_id={{$document->document_type_id}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE', 'route' => ['document.destroy', $document->id],'style'=>'display:inline','class'=>'delete']) }}
                            <input type="hidden" id="reference_id" name="reference_id" value="{{$document->reference_id}}"/>
                            <input type="hidden" id="document_type_id" name="document_type_id" value="{{$document->document_type_id}}"/>
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No documents found.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $('#consultants_chosen').css('width', '100%');
        $('.chosen-container').css('width', '100%');
    </script>
@endsection
