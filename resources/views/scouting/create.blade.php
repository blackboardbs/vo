@extends('adminlte.default')
@section('title') Scouting @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('create_scouting_form')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('scouting.store'), 'method' => 'post','class'=>'mt-3', 'id'=>'create_scouting_form','files' => true,'autocomplete'=>'on'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Create Scouting</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th style='width: 20%'>Resource First Name:</th>
                    <td>
                        {{Form::text('resource_first_name',old('resource_first_name'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_first_name') ? ' is-invalid' : ''), 'placeholder'=>'First Name'])}}
                        @foreach($errors->get('resource_first_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th style='width: 20%'>Resource Last Name</th>
                    <td>
                        {{Form::text('resource_last_name',old('resource_last_name'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('resource_last_name') ? ' is-invalid' : ''), 'placeholder'=>'Last name'])}}
                        @foreach($errors->get('resource_last_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>
                        {{Form::text('phone',old('phone'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Email:</th>
                    <td>{{Form::text('email',old('email'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Role:</th>
                    <td>
                        {{Form::select('role_id',$role_drop_down,null,['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('role_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Process Interview:</th>
                    <td>{{Form::select('interview_status_id',$process_interview_drop_down,null,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('interview_status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('interview_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                <tr>
                    <th>Vendor:</th>
                    <td>{{Form::select('vendor_id',$vendor_drop_down, old('vendor_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('vendor_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('vendor_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Technical Rating:</th>
                    <td>{{Form::select('technical_rating_id',$tecnical_rating_drop_down, old('technical_rating_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('technical_rating_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('technical_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Business Rating:</th>
                    <td>{{Form::select('business_rating_id',$business_rating_drop_down, old('business_rating_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('business_rating_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('business_rating_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Resource Level:</th>
                    <td>{{Form::select('resource_level_id',$resource_level_drop_down, old('resource_level_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('resource_level_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('resource_level_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Estimated Hourly Rate:</th>
                    <td>
                        {{Form::text('estimated_hourly_rate',old('estimated_hourly_rate'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('estimated_hourly_rate') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate'])}}
                        @foreach($errors->get('estimated_hourly_rate') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Availability:</th>
                    <td>
                        {{Form::text('availability',old('availability'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('availability') ? ' is-invalid' : ''), 'placeholder'=>'Availability'])}}
                        @foreach($errors->get('availability') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Hourly Rate Minimum:</th>
                    <td>
                        {{Form::text('hourly_rate_minimum',old('hourly_rate_minimum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum'])}}
                        @foreach($errors->get('hourly_rate_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Hourly Rate Maximum:</th>
                    <td>
                        {{Form::text('hourly_rate_maximum',old('hourly_rate_maximum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum'])}}
                        @foreach($errors->get('hourly_rate_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th style='width: 20%'>Monthly Salary Minimum</th>
                    <td>
                        {{Form::text('monthly_salary_minimum',old('monthly_salary_minimum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum'])}}
                        @foreach($errors->get('monthly_salary_minimum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Monthly Salary Maximum:</th>
                    <td>
                        {{Form::text('monthly_salary_maximum',old('monthly_salary_maximum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum'])}}
                        @foreach($errors->get('monthly_salary_maximum') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Referred By:</th>
                    <td>{{Form::select('referral',$users_drop_down, old('referral'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('referral') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('referral') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Assessed By:</th>
                    <td>
                        {{Form::select('assessed_by',$users_drop_down, old('assessed_by'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('assessed_by') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('assessed_by') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Process Status:</th>
                    <td>{{Form::select('process_status_id',$process_statuses_drop_down,null,['class'=>'form-control form-control-sm   col-sm-12 '. ($errors->has('process_status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('process_status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Reference Number:</th>
                    <th>
                        {{Form::text('reference_code', old('reference_code'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('reference_code') ? ' is-invalid' : ''), 'placeholder'=>'Reference Number'])}}
                        @foreach($errors->get('reference_code') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </th>
                <tr>
                <tr>
                    <th>Profession:</th>
                    <td>{{Form::select('profession_id',$profession_drop_down, old('profession_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('profession_id') ? ' is-invalid' : ''), 'id' => 'profession_id'])}}
                        @foreach($errors->get('profession_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Speciality:</th>
                    <td>{{Form::select('speciality_id',$speciality_drop_down, old('speciality_id'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'id' => 'speciality_id'])}}
                        @foreach($errors->get('speciality_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Skill:</th>
                    <td>
                        {{Form::select('skills[]',$skills_drop_down, old('skills'),['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('skills') ? ' is-invalid' : ''), 'multiple'])}}
                        @foreach($errors->get('skills') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Looking for:</th>
                    <td>
                        <input type="checkbox" name="permanent" id="permanent" value="1"/> Permanent <input type="checkbox" name="contracting" id="contracting" value="1"/> Contracting <input type="checkbox" name="temporary" id="temporary" value="1"/> Temporary
                    </td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td colspan="3">
                        {{Form::textarea('note', old('note'),['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Documents:</th>
                    <td colspan="3">
                        {{Form::file('file[]',['class'=>'form-control'. ($errors->has('file') ? ' is-invalid' : ''),'placeholder'=>'File', 'multiple' => 'true'])}}
                        @foreach($errors->get('file') as $error)
                            <div class="invalid-feedback">
                                {{ $error }}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function(){
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            $('#profession_id').change(function(){

                var profession_id = $('#profession_id').val();

                axios.post('/cv/getspeciality/'+profession_id)
                .then(function (response) {
                    var options_html = '<option>Please Select</option>';
                    $.each(response.data.speciality, function (key, value) {
                        options_html += '<option value='+key+'>'+value+'</option>';
                    })

                    $('#speciality_id').html(options_html);
                    $('#speciality_id').trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });

            });
        });
    </script>
@endsection
