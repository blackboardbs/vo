@extends('adminlte.default')
@section('title') Scouting @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
            <a href="{{route('scouting.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Scouting</a>
            <x-export route="scouting.index"></x-export>
        @endif
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('process_status_id',$process_status_dropdown,old('process_status_id'),['class'=>' form-control search w-100', 'id' => 'process_status_id', 'placeholder' => 'Select Process Status'])}}
                        <span>Process Status</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('process_interview_id',$process_interview_dropdown,old('process_interview_id'),['class'=>' form-control search w-100', 'id' => 'process_interview_id', 'placeholder' => 'Select Process Interview'])}}
                        <span>Process Interview</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('scouting_role_id',$scouting_role_dropdown,old('scouting_role_id'),['class'=>' form-control search w-100', 'id' => 'scouting_role_id', 'placeholder'=>'Select Scouting Role'])}}
                        <span>Role</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('vendor_id',$vendor_dropdown,old('vendor_id'),['class'=>' form-control search w-100', 'id' => 'vendor_id', 'placeholder'=>'Select Vendor'])}}
                        <span>Vendor</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('resource_level_id',$resource_level_dropdown,old('resource_level_id'),['class'=>' form-control search w-100', 'id' => 'resource_level_id', 'placeholder'=>'Select Resource Level'])}}
                        <span>Resource Level</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('business_rating_id',$business_rating_dropdown,old('business_rating_id'),['class'=>' form-control search w-100', 'id' => 'business_rating_id', 'placeholder'=>'Select Business Rating'])}}
                        <span>Business Rating</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('technical_rating_id',$technical_rating_dropdown,old('technical_rating_id'),['class'=>' form-control search w-100', 'id' => 'technical_rating_id', 'placeholder'=>'Select Technical Rating'])}}
                        <span>Technical Rating</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('profession_id',$profession_dropdown,old('profession_id'),['class'=>' form-control search w-100', 'id' => 'profession_id', 'placeholder'=>'Select Profession'])}}
                        <span>Profession</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('speciality_id',$speciality_dropdown,old('speciality_id'),['class'=>' form-control search w-100', 'id' => 'speciality_id', 'placeholder'=>'Select Speciality'])}}
                        <span>Speciality</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('skill_id',$skill_dropdown,old('skill_id'),['class'=>' form-control search w-100', 'id' => 'skill_id', 'placeholder'=>'Select Skill'])}}
                        <span>Skill</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('reference_code',$reference_code_dropdown,old('reference_code'),['class'=>' form-control search w-100', 'id' => 'reference_code', 'placeholder'=>'Select Reference Code'])}}
                        <span>Reference Code</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('permanent',$yes_or_no_dropdown,old('permanent'),['class'=>' form-control search w-100', 'id' => 'permanent', 'placeholder'=>'Select Permanent'])}}
                        <span>Permanent</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('contracting',$yes_or_no_dropdown,old('contracting'),['class'=>' form-control search w-100', 'id' => 'contracting', 'placeholder'=>'Select Contracting'])}}
                        <span>Contracting</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('temporary',$yes_or_no_dropdown,old('temporary'),['class'=>' form-control search w-100', 'id' => 'temporary', 'placeholder'=>'Select Temporary'])}}
                        <span>Temporary</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3" style="max-width: 20% !important;">
                <a href="{{route('scouting.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('first_name', 'Resource Name')</th>
                    <th>@sortablelink('phone', 'Phone')</th>
                    <th>@sortablelink('email', 'Email')</th>
                    <th>@sortablelink('processstatus.name', 'Process Status')</th>
                    <th>@sortablelink('proceinterview.name', 'Process Interview')</th>
                    <th>@sortablelink('role.name', 'Role')</th>
                    <th>@sortablelink('vendor.vendor_name','Vendor')</th>
                    <th>@sortablelink('technicalrating.name', 'Technical Rating')</th>
                    <th>@sortablelink('businessrating.name', 'Business Rating')</th>
                    <th>@sortablelink('resourcelevel.description', 'Resource Level')</th>
                    @if($can_update)
                        <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($scoutings as $scouting)
                    <tr>
                        <td><a href="{{route('scouting.show',$scouting)}}">{{$scouting->resource_first_name}} {{$scouting->resource_last_name}}</a></td>
                        <td>{{$scouting->phone}}</td>
                        <td>{{$scouting->email}}</td>
                        <td>{{$scouting->processstatus?->name}}</td>
                        <td>{{$scouting->interviewstatus?->name}}</td>
                        <td>{{$scouting->role?->name}}</td>
                        <td>{{$scouting->vendor?->vendor_name}}</td>
                        <td>{{$scouting->businessrating?->name}}</td>
                        <td>{{$scouting->technicalrating?->name}}</td>
                        <td>{{$scouting->resourcelevel?->description}}</td>
                        @if($can_update)
                            <td>
                                <div class="d-flex">
                                    <a href="{{route('scouting.edit',$scouting)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                                    {{--<a href="{{route('scouting.convert',$scouting)}}" class="btn btn-warning btn-sm">Convert Resource</a>--}}
                                    @if(!isset($scouting->convertion_date))
                                        <a href="{{route('scouting.converttocv',$scouting)}}" class="btn btn-warning btn-sm mr-1"><i class="fas fa-exchange-alt"></i></a>
                                    @endif
                                    {{ Form::open(['method' => 'DELETE','route' => ['scouting.destroy', $scouting],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                </div>
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No records match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $scoutings->firstItem() }} - {{ $scoutings->lastItem() }} of {{ $scoutings->total() }}
                        </td>
                        <td>
                            {{ $scoutings->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection
