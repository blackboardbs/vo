@extends('adminlte.default')

@section('title') User Logins @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{--<a href="{{route('account.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Account</a>--}}
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            Show &nbsp;
            {{Form::select('s',['0'=>'All','5'=>'5','10'=>'10','15'=>'15','20'=>'20'],old('s'),['class'=>'form-control form-control-sm search'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>User Name</th>
                    <th>User IP</th>
                    <th>Password</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($user_logs as $log)
                    <tr>
                        <td>{{ $log->user_name }}</td>
                        <td>{{$log->user_ip}}</td>
                        <td>{{$log->password}}</td>
                        <td>{{$log->date}}</td>
                        <td>{{($log->login_status == 1) ? 'Successful' : 'Failed'}}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE','route' => ['logins.destroy', $log],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No user logs available</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $user_logs->links() }}
        </div>
    </div>
@endsection
