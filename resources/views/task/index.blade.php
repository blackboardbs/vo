<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
    @include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
        @include('task.task-menu')
        
                <task-list :project="{{$project}}" :project-view="1"></task-list>

    </div>
@endsection
@section('extra-css')
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }

        .box-title{
            font-size: 19px;
        }
    </style>
@endsection