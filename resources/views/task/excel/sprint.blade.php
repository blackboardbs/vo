<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Goal</th>
        <th>Project</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sprints as $sprint)
        <tr>
            <td>{{$sprint->name}}</td>
            <td>{{isset($sprint->goal)?$sprint->goal:''}}</td>
            <td>{{isset($sprint->project->name)?$sprint->project->name:''}}</td>
            <td>{{isset($sprint->start_date)?$sprint->start_date:''}}</td>
            <td>{{isset($sprint->end_date)?$sprint->end_date:''}}</td>
            <td>{{isset($sprint->status->name)?$sprint->status->name:''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>