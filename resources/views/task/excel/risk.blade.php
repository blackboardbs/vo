<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Project</th>
        <th>Risk Date</th>
        <th>Risk Area</th>
        {{--<th>Risk Item</th>--}}
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($risks as $risk)
        <tr>
            <td>{{$risk->name}}</td>
            <td>{{isset($risk->description)?$risk->description:''}}</td>
            <td>{{isset($risk->project->name)?$risk->project->name:''}}</td>
            <td>{{isset($risk->risk_date)?$risk->risk_date:''}}</td>
            <td>{{isset($risk->riskarea->name)?$risk->riskarea->name:''}}</td>
            {{--<td>{{$risk->risk_area_id == 4 ? $risk->risk_item : $risk->risk_area_id }}</td>--}}
            <td>{{isset($risk->status->description)?$risk->status->description:''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>