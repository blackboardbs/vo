<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/16
 * Time: 17:28
 */
?>

@extends('adminlte.default')
@section('title')Kanban Board @endsection

@section('content')
    <div class="container-fluid">
        <kanban-board 
            :show-timeframe={{$show_timeframe}} 
            :show-hours={{$show_hours}} 
            :show-actuals={{$show_actuals}} 
            :project-id="{{$project_id}}" 
            :auth-user="{{Auth::user()->id}}" 
            :permissions="{{json_encode($permissions)}}"
            :viewr="{{json_encode($view->kanban_view)}}"
            :kanban-filters="{{json_encode(array_filter(request()->all(), static function($var){return $var !== null;} ))}}">
        </kanban-board>
    </div>
@endsection
