<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
    @include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
        @include('task.task-menu')
        @include('task.task-filters')
        <hr class="mt-0">

        <div class="box">
            <div class="box-header pb-2" style="display: flex;
        align-items: center; /* Vertically center the items */
        justify-content: space-between; /* Adjust spacing between items */
        padding-bottom: 0.5rem; /* Example padding, adjust as needed */">
                <h3 class="box-title m-0">Project: {{$project->name}}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                @if(!empty($gantt_chart->toArray()))
                    <div style="overflow-x:auto;">
                    <table class="table table-sm" id="gantt-table">
                        <thead class="bg-dark">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @php $beggining_of_month = $gantt_start_date->copy(); @endphp
                            @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                @if($i == $i->endOfMonth())
                                    <th class="text-center border-right border-left" colspan="{{$beggining_of_month->diffInDays($i->endOfMonth()) + 1}}">{{$i->format("F Y")}}</th>
                                    @php $beggining_of_month = $i->copy()->addDay() @endphp
                                @endif

                            @endfor
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @php $beggining_of_week = $gantt_start_date->copy(); @endphp
                            @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                @if($i == $i->endOfWeek())
                                    <th class="text-center  border-right border-left" colspan="{{$beggining_of_week->diffInDays($i->endOfWeek()) + 1}}">Week {{$i->weekOfYear}}</th>
                                    @php $beggining_of_week = $i->copy()->addDay() @endphp
                                @endif

                            @endfor
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                <th class="{{($i->isWeekend() ? 'bg-gray':null )}}  border-right border-left">{{$i->format('d D')}}</th>
                            @endfor
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($gantt_chart as $key => $gantt)
                                <tr class="epic" style="cursor: pointer" onclick="expandItems('.epic-{{$gantt["epic_id"]}}', '.feature')">
                                    <th class="text-primary">{{$gantt["epic_id"]??null}} | {{$gantt["epic_name"]??null}}</th>
                                    <td></td>
                                    <td></td>
                                    <td>{{$gantt["epic_start_date"]->toDateString()??null}}</td>
                                    <td>{{$gantt["epic_end_date"]->toDateString()??null}}</td>
                                    @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                        @if((!empty($gantt["epic_start_date"]) && $i >= $gantt["epic_start_date"] && !empty($gantt["epic_end_date"]) && $i <= $gantt["epic_end_date"]))
                                            <td class="{{($i->isWeekend() ? 'bg-gray':'bg-primary' )}}" style="width: 80px"></td>
                                        @else
                                            <td class="{{($i->isWeekend() ? 'bg-gray':null )}}"></td>
                                        @endif
                                    @endfor
                                </tr>
                                    @foreach($gantt["features"] as $feature)
                                        <tr class="feature epic-{{$gantt["epic_id"]}}" style="cursor: pointer" onclick="expandItems('.feature-{{$feature["feature_id"]}}', '.task')">
                                            <td></td>
                                            <th class="text-info">{{$feature["feature_id"]}} | {{$feature["feature_name"]}}</th>
                                            <td></td>
                                            <td>{{$feature["feature_start_date"]->toDateString()}}</td>
                                            <td>{{$feature["feature_end_date"]->toDateString()}}</td>
                                            @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                                @if(($i >= $feature["feature_start_date"] && $i <= $feature["feature_end_date"]))
                                                    <td class="{{($i->isWeekend() ? 'bg-gray':'bg-info' )}}" ></td>
                                                @else
                                                    <td class="{{($i->isWeekend() == "Sun" ? 'bg-gray-light':null )}}"></td>
                                                @endif
                                            @endfor
                                        </tr>
                                        @foreach($feature['user_stories'] as $story)
                                            @foreach($story as $task)
                                                <tr class="task feature-{{$feature["feature_id"]}}">
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <div class="tool-tip text-white " style="text-align: center!important;"> <strong class="text-warning">{{$task["task_id"]}} | {{$task["task_name"]}}</strong>
                                                            <span class="tool-tiptext">
                                                                        <table class="table table-sm mb-0" style="background-color: transparent!important;">
                                                                            <tbody>
                                                                                <tr class="text-left">
                                                                                    <td style="border-top: none!important;">Consultant:</td>
                                                                                    <td style="border-top: none!important;">{{$task["consultant"]}}</td>
                                                                                </tr>
                                                                                <tr class="text-left">
                                                                                    <td>Planned Hours:</td>
                                                                                    <td>{{$task["planned_hours"]}} hours</td>
                                                                                </tr>
                                                                                <tr class="text-left">
                                                                                    <td>Task Status:</td>
                                                                                    <td>{{$task["status"]}}</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td>{{$task["task_start_date"]->toDateString()}}</td>
                                                    <td>{{$task["task_end_date"]->toDateString()}}</td>
                                                    @for($i = $gantt_start_date->copy(); $i <= $gantt_last_date; $i->addDay())
                                                        @if(($i >= $task["task_start_date"] && $i <= $task["task_end_date"]))
                                                            <td class="{{($i->isWeekend() ? 'bg-gray':'bg-warning' )}} text-center" >
                                                                {{----}}
                                                            </td>
                                                        @else
                                                            <td class="{{($i->isWeekend() ? 'bg-gray':null )}}"></td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        @endforeach
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                @else
                    <h5>No Tasks</h5>
                @endif
            </div>
            <!-- /.box-body -->
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }

        .box-title{
            font-size: 19px;
        }
        .tool-tip {
            position: relative;
            display: inline-block;
        }

        .tool-tip .tool-tiptext {
            visibility: hidden;
            width: 320px;
            background-color: #555;
            color: #fff;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -160px;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .tool-tip .tool-tiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        .tool-tip:hover .tool-tiptext {
            visibility: visible;
            opacity: 1;
        }
        .table thead tr th,.table thead tr td, .table tbody tr th, .table tbody tr td{
            white-space: nowrap!important;
            padding: 0 15px;
        }
        .table{
            width: auto!important;
        }
        ::-webkit-scrollbar {
            width: 10px;
        }

        ::-webkit-scrollbar-track {
           /* box-shadow: inset 0 0 5px grey;*/
            /*border-radius: 5px;*/
            border: 1px solid #343a40;
        }
        ::-webkit-scrollbar-thumb {
            background: #343a40;
            /*border-radius: 5px;*/
        }
        ::-webkit-scrollbar-thumb:hover {
            background: #23272b;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {
            $(".feature, .task").hide();

        });

        function expandItems(selector, child) {
            if ($(selector).css('display') == 'table-row'){
                if ($(selector).prev().hasClass("epic"))
                    $('.'+$(selector).next().attr('class').split(' ')[0]).fadeOut();
                $(selector).fadeOut();
                $(child).css("display", "none");
            }else{
                $(selector).fadeIn();
            }
        }
    </script>
@endsection

