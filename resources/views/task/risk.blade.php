<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
    @include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
        @include('task.task-menu')

        @include('task.task-filters')
        <hr class="mt-0">

        <div class="box">
            <div class="box-header pb-2" style="display: flex;
        align-items: center; /* Vertically center the items */
        justify-content: space-between; /* Adjust spacing between items */
        padding-bottom: 0.5rem; /* Example padding, adjust as needed */">
                <h3 class="box-title m-0">Project: {{$project->name}}</h3>
                <a href="{{route('task.risk', [...request()->except('sort', 'direction'), ...['export' => 1, 'project_id' => $project->id]])}}" class="btn btn-info float-right mr-2"><i class="fa fa-file-excel"></i> Export In Excel</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>@sortablelink('name','Name')</th>
                            <th>@sortablelink('description','Description')</th>
                            <th>@sortablelink('project.name','Project')</th>
                            <th>@sortablelink('risk_date','Risk Date')</th>
                            <th>@sortablelink('riskarea.name','Risk Area')</th>
                            {{--<th>Risk Item</th>--}}
                            <th>@sortablelink('status.name','Status')</th>
                            <th class="text-right last">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($risks as $risk)
                            <tr>
                                <td><a href="{{route('risk.show',$risk)}}">{{$risk->name}}</a></td>
                                <td>{{isset($risk->description)?$risk->description:''}}</td>
                                <td>{{isset($risk->project->name)?$risk->project->name:''}}</td>
                                <td>{{isset($risk->risk_date)?$risk->risk_date:''}}</td>
                                <td>{{isset($risk->riskarea->name)?$risk->riskarea->name:''}}</td>
                                {{--<td>{{$risk->risk_area_id == 4 ? $risk->risk_item : $risk->risk_area_id }}</td>--}}
                                <td>{{isset($risk->status->description)?$risk->status->description:''}}</td>
                                <td class="text-right">
                                    <div class="d-flex">
                                    <!-- <a href="{{route('risk.create')}}?epic_id={{$risk->id}}" class="btn btn-warning btn-sm"><i class="fa fa-plus"></i>  Risk</a> -->
                                    <a href="{{route('risk.edit',$risk)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['risk.destroy', $risk],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="12" class="text-center">No risk entries match those criteria.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $risks->appends(request()->except('page'))->links() }}
                </div>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }

        .box-title{
            font-size: 19px;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection

