<ul class="nav nav-tabs">
    @isset($permissions)
        @if((isset($permissions[6]['permission']) && $permissions[6]['permission'] == 'View') || ($permissions[6]['permission'] == 'Create') || ($permissions[6]['permission'] == 'Update') || ($permissions[6]['permission'] == 'Full'))
            <li class="{{ (\Request::route()->getName() == 'task.index') ? 'active' : '' }}"><a href="{{route('task.index')}}?project_id={{$project->id}}">Tasks - List</a></li>
        @endif
        @if(isset($permissions[7]['permission']) && ($permissions[7]['permission'] == 'View') || ($permissions[7]['permission'] == 'Create') || ($permissions[7]['permission'] == 'Update') || ($permissions[7]['permission'] == 'Full'))
            <li class="{{ (\Request::route()->getName() == 'task.kanban') ? 'active' : '' }}"><a href="{{route('task.kanban', $project->id)}}">Tasks - Kanban Board</a></li>
        @endif
        @if(isset($permissions[8]) && (($permissions[8]['permission'] == 'View') || ($permissions[8]['permission'] == 'Create') || ($permissions[8]['permission'] == 'Update') || ($permissions[8]['permission'] == 'Full')))
            <li class="{{ (\Request::route()->getName() == 'task.treeviewnew') ? 'active' : '' }}"><a href="{{route('task.treeviewnew', $project->id)}}">Tree View</a></li>
        @endif
        @if(isset($permissions[11]['permission']) && (($permissions[11]['permission'] == 'View') || ($permissions[11]['permission'] == 'Create') || ($permissions[11]['permission'] == 'Update') || ($permissions[11]['permission'] == 'Full')))
            <li class="{{ (\Request::route()->getName() == 'task.gantt') ? 'active' : '' }}"><a href="{{route('task.gantt', $project->id)}}">Gantt</a></li>
        @endif
        @if(isset($permissions[9]['permission']) && (($permissions[9]['permission'] == 'View') || ($permissions[9]['permission'] == 'Create') || ($permissions[9]['permission'] == 'Update') || ($permissions[9]['permission'] == 'Full')))
            <li class="{{ (\Request::route()->getName() == 'task.sprint') ? 'active' : '' }}"><a href="{{route('task.sprint', $project->id)}}">Sprint</a></li>
        @endif
        @if(isset($permissions[10]['permission']) && (($permissions[10]['permission'] == 'View') || ($permissions[10]['permission'] == 'Create') || ($permissions[10]['permission'] == 'Update') || ($permissions[10]['permission'] == 'Full')))
            <li class="{{ (\Request::route()->getName() == 'task.risk') ? 'active' : '' }}"><a href="{{route('task.risk', $project->id)}}">Risks</a></li>
        @endif
    @endisset
</ul>