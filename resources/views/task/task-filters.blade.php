  <form class="" id="searchforms" autocomplete="off">
    <div class="row pt-3 mt-3 pb-1">
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('project_id', $projects_drop_down, isset($_GET['project_id'])?$_GET['project_id']:$project->id, ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                  <span>Projects</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('resource', $resource_drop_down, (isset($_GET['resource']) ? $_GET['resource'] : ''), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Resource</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('customer', $customer_drop_down, (isset($_GET['customer']) ? $_GET['customer'] : ''), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Customer</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('team', $team_drop_down, (isset($_GET['team']) ? $_GET['team'] : ''), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Team</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::text('start_date', null, ['class'=>'datepicker start_date form-control search col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'style'=>'width:100% !important;'])}}
                    <span>Start Date</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::text('end_date', null, ['class'=>'datepicker end_date form-control search form-control-sm  col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'style'=>'width:100% !important;'])}}
                    <span>End Date</span>
                </label>
            </div>
        </div>
    </div>
    <div class="row pt-0 mt-0 pb-1">
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('sprint', $sprints_drop_down, (isset($_GET['sprint']) ? $_GET['sprint'] : ''), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Sprint</span>
                </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                <label class="has-float-label">
                    {{Form::select('status', $task_status_drop_down, (isset($_GET['status']) ? $_GET['status'] : ''), ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                    <span>Status</span>
                </label>
            </div>
        </div>
        @if((\Request::route()->getName() == 'task.kanban'))
            <div class="col-md-2 col-sm-12">
                <div class="form-group input-group">
                        <label class="has-float-label">
                        {{Form::select('on_kanban', $display_on_kanban, 1, ['class'=>' form-control search', 'style'=>'width: 100%;'])}}
                        <span>Display On Kanban</span>
                    </label>
                </div>
            </div>
        @endif
        <div class="col-md-2 col-sm-12">
            <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::text('task_id', (isset($_GET['task_id']) ? $_GET['task_id'] : ''), ['class'=>'form-control search', 'style'=>'width: 100%;'])}}
                        <span>ID</span>
                    </label>
            </div>
        </div>
        <div class="col-md-2 col-sm-12">
            <a href="?project_id={{$project->id}}" style="border: none !important; width: 100%;" class="btn btn-info" type="submit">Clear Filters</a>
        </div>
    </div>
</form>
