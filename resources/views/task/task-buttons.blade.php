<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <a href="{{route('bug.create', $project->id)}}" class="btn btn-danger float-right" style="margin-right: 10px;"><i class="fa fa-bug" aria-hidden="true"></i> Bug</a>
    <a href="{{route('task.create', $project->id)}}?type=task" class="btn btn-dark float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> Task</a>
    <a href="{{route('risk.create')}}" class="btn btn-danger float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> Risk</a>
    <button onclick="history.back()" class="btn btn-dark float-right mr-2"><i class="fa fa-caret-left"></i> Back</button>
    <!-- <a href="{{route('userstory.create')}}?project_id={{$project->id}}&type=task" class="btn btn-success float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> User Story</a>
    <a href="{{route('feature.create')}}?project_id={{$project->id}}&type=task" class="btn btn-success float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> Feature</a>
    <a href="{{route('epic.create')}}?project_id={{$project->id}}&type=task" class="btn btn-success float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> Epic</a> -->
</div>