<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
@include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
    @include('task.task-menu')
        
        <treeview :project="{{$project}}" :project-view="1"></treeview>

    </div>
@endsection
@section('extra-css')
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }

        .box-title{
            ;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {


            $('.status_update').on('change', function(){
                var id = (this).id;
                var value = (this).value;
                var data = {
                    'id': id,
                    'value': value
                };

                axios.post('{{route('task.changestatus')}}', data)
                    .then(function (data) {
                        alert(data['data'].message);
                        location.reload();
                    })
                    .catch(function () {
                        console.log("An Error occured!!!");
                    });

            });

        });
    </script>
@endsection
