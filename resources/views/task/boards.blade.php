<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/16
 * Time: 17:28
 */
?>

@extends('adminlte.default')

@section('title')Kanban Board @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button><br>
        <a href="{{route('task.create', $project_id)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Task</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li><a href="{{route('task.index')}}?project_id={{$project_id}}">Tasks - List</a></li>
            {{--<li><a href="{{route('task.treeview', $project_id)}}">Tasks - Tree View</a></li>--}}
            <li class="active"><a href="#">Tasks - Kanban Board</a></li>
        </ul>
        <br>
        <boards :boards="boards"></boards>
    </div>
    <div class="modal fade" id="addBoard" role="dialog" aria-labelledby="addBoardLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="#" @submit.prevent="addNewBoard()">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new board</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Board name..." name="board" v-model="newBoardName">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-dark" type="submit" :disabled="isCreatingBoard || !newBoardName">Create Board</button>
                        <button type="button" class="pull-left btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        #boards .board {
            width: 304px;
            padding-left: 5px;
            padding-right: 5px;
            float: left;
        }

        #boards .board .kanban-wrapper {
            background-color: #E2E4E6;
            border-radius: 5px;
            overflow: hidden;
        }

        #boards .board .kanban-wrapper .board-title {
            padding: 10px;
        }

        #boards .board .kanban-wrapper .board-title h2 {
            margin: 0;
            font-size: 14px;
            font-weight: 600;
            color: #272727;
        }

        #boards .board .kanban-wrapper .cards {
            list-style: none;
            margin: 0;
            padding: 0 10px;
        }

        #boards .board .kanban-wrapper .cards > div {
            min-height: 5px;
            padding: 5px 0;
        }

        #boards .board .kanban-wrapper .cards .card {
            overflow: hidden;
            padding: 8px;
            background-color: #fff;
            border-bottom: 1px solid #ccc;
            border-radius: 3px;
            cursor: pointer;
            margin-bottom: 6px;
            max-width: 300px;
            min-height: 20px;
        }

        #boards .board .kanban-wrapper .cards .card:hover {
            background-color: #edeff0;
        }

        #boards .board .kanban-wrapper .add-card {
            color: #838c91;
            display: block;
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            padding: 8px 10px;
            position: relative;
        }

        #boards .board .kanban-wrapper .add-card[disabled],
        #boards .board .kanban-wrapper .add-card[disabled]:hover {
            cursor: not-allowed;
            text-decoration: none;
        }

        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        li.active {
            border-left: 1px solid #ddd !important;
            border-top: 1px solid #ddd !important;
            border-right: 1px solid #ddd !important;
        }
    </style>
@endsection