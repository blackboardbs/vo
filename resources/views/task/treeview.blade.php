@extends('adminlte.default')
@section('title') View Tasks @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button><br>
        <a href="{{route('task.create', $project_id)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Task</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li><a href="{{route('task.index')}}?project_id={{$project_id}}">Tasks - List</a></li>
            {{--<li class="active"><a href="{{route('task.treeview', $project_id)}}">Tasks - Tree View</a></li>--}}
            <li><a href="{{route('task.kanban', $project_id)}}">Tasks - Kanban Board</a></li>
        </ul>

        <br>
        {!! $tree !!}
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://glimberger.github.io/bstree/assets/css/bstree.css">
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        li.active {
            border-left: 1px solid #ddd !important;
            border-top: 1px solid #ddd !important;
            border-right: 1px solid #ddd !important;
        }
    </style>
@endsection
@section('extra-js')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script src="https://glimberger.github.io/bstree/assets/js/jquery.bstree.js"></script>
    <script>
        $('document').ready(function () {
            $('#mytree').bstree({
                updateNodeTitle: function (node, title) {
                    return '<span class="label label-default">' + node.attr('data-id') + '</span> ' + title
                }
            })
        })
    </script>
@endsection