<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
    @include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
        @include('task.task-menu')
        @include('task.task-filters')
        <hr class="mt-0">

        <div class="box">
            <div class="box-header pb-2" style="display: flex;
        align-items: center; /* Vertically center the items */
        justify-content: space-between; /* Adjust spacing between items */
        padding-bottom: 0.5rem; /* Example padding, adjust as needed */">
                <h3 class="box-title m-0">Project: {{$project->name}}</h3>
                <a href="{{route('task.sprint', [...request()->except('sort', 'direction'), ...['export' => 1, 'project_id' => $project->id]])}}" class="btn btn-info float-right mr-2"><i class="fa fa-file-excel"></i> Export In Excel</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr class="btn-dark">
                            <th>Name</th>
                            <th>Goal</th>
                            <th>Project</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th class="text-right last">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($sprints as $sprint)
                            <tr>
                                <td><a href="{{route('sprint.show',$sprint)}}">{{$sprint->name}}</a></td>
                                <td>{{isset($sprint->goal)?$sprint->goal:''}}</td>
                                <td>{{isset($sprint->project->name)?$sprint->project->name:''}}</td>
                                <td>{{isset($sprint->start_date)?$sprint->start_date:''}}</td>
                                <td>{{isset($sprint->end_date)?$sprint->end_date:''}}</td>
                                <td>{{isset($sprint->status->name)?$sprint->status->name:''}}</td>
                                <td class="text-right">
                                    <div class="d-flex">
                                    <a href="{{route('sprint.edit',$sprint)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    {{ Form::open(['method' => 'DELETE','route' => ['sprint.destroy', $sprint],'style'=>'display:inline','class'=>'delete']) }}
                                    <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No sprints entries match those criteria.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {{--{{ $professions->links() }}--}}
                </div>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }

        .box-title{
            font-size: 19px;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection

