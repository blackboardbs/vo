@extends('adminlte.default')
@section('title') Edit @isset($bug) Bug @endisset Task @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('create_task_frm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="box">
            <div class="box-header">
                <table class="table table-sm table-borderless">
                    <tbody>
                    <tr>
                        <td>
                            <h5 class="box-title">
                                @if(isset($project->id))<a href="{{route('project.show', isset($project->id) ? $project->id : '')}}">{{$project->name}}</a> / @endif
                                @if(isset($task->user_story->feature->epic->id))<a href="{{route('feature.show', isset($task->user_story->feature->epic->id) ? $task->user_story->feature->epic->id : '')}}">{{$task->user_story->feature->epic->name}}</a> / @endif
                                @if(isset($task->user_story->feature->id))<a href="{{route('feature.show', isset($task->user_story->feature->id) ? $task->user_story->feature->id : '')}}">{{$task->user_story->feature->name}}</a> / @endif
                                @if(isset($task->user_story->id))<a href="{{route('userstory.show', isset($task->user_story->id) ? $task->user_story->id : '')}}">{{$task->user_story->name}}</a> @endif
                            </h5>
                        </td>
                        <td>
                            <a href="{{route('task.create', $task->project_id)}}" class="btn btn-primary float-right">Add another task to project no: {{$task->project_id}}</a>
                            <a href="{{route('project.show', $task->project_id)}}" class="btn btn-primary float-right" style="margin-right: 10px;">Go to project no: {{$task->project_id}}</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
            {{Form::open(['url' => route('task.update', $task->id), 'method' => 'put','class'=>'mt-3', 'id'=>'create_task_frm','files' => true])}}
            <input type="hidden" id="project_id" name="project_id" value="{{$project_id}}" />
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Edit Task (Task No. {{$task->id}})</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style="width: 20%;">Description:</th>
                        <td style="width: 30%;">
                            {{Form::text('description',$task->description,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'placeholder'=>'Description'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Consultant</th>
                        <td>
                            {{Form::select('consultant', $consultant_drop_down,$task->employee_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('consultant') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('consultant') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Project</th>
                        <td>
                            {{Form::select('project', $project_drop_down,$task->project_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('project') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('project') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Customer</th>
                        <td>
                            {{Form::select('customer', $customer_drop_down, $task->customer_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('customer') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('customer') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Sprint</th>
                        <td>
                            {{Form::select('sprint_id', $sprintDropDown, $task->sprint_id,['id'=>'sprint','class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('sprint_id') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('sprint_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>User Story</th>
                        <td>
                            {{Form::select('user_story_id', $userStories, $task->user_story_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('user_story_id') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('user_story_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 20%;">Dependency</th>
                        <td style="width: 30%;">
                            {{Form::select('dependency', $dependency_drop_down, $task->dependency,['class'=>'form-control form-control-sm  col-sm-12 '. ($errors->has('dependency') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('dependency') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Goal</th>
                        <td>
                            {{Form::select('goal_id', [], $task->goal_id,['id'=>'goal','class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('goal_id') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('goal_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Delivery Type</th>
                        <td>
                            {{Form::select('task_delivery_type_id', $task_delivery_type_drop_down, $task->task_delivery_type_id,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('task_delivery_type_id') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('task_delivery_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status</th>
                        <td>
                            {{Form::select('status', $status_drop_down,$task->status,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('status') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Start Date</th>
                        <td>
                            {{Form::text('start_date',$task->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''),'placeholder'=>'Start Date', 'id' => 'start_date'])}}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>End Date</th>
                        <td>
                            {{Form::text('end_date',$task->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''),'placeholder'=>'End Date', 'id' => 'end_date'])}}
                            @foreach($errors->get('end_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Billable</th>
                        <td>
                            {{Form::select('billable', [0 => 'No', 1 => 'Yes'],$task->billable,['class'=>'form-control form-control-sm col-sm-12 ', 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('billable') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Display on kanban</th>
                        <td>
                            {{Form::select('on_kanban', $show_on_kanban_dropdown, $task->on_kanban,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('on_kanban') ? ' is-invalid' : ''), 'placeholder' => 'Please select'])}}
                            @foreach($errors->get('on_kanban') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Hours Planned</th>
                        <td>
                            {{Form::text('hours_planned',$task->hours_planned,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('hours_planned') ? ' is-invalid' : ''),'placeholder'=>'Hours'])}}
                            @foreach($errors->get('hours_planned') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Weight</th>
                        <td>
                            {{Form::text('weight', $task->weight,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('weight') ? ' is-invalid' : ''), 'placeholder' => 'Weight'])}}
                            @foreach($errors->get('weight') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Note</th>
                        <td colspan="3">
                            {{Form::textarea('note',$task->note_1,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'note'])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    @isset($bug)
                        <tr>
                            <th style='width: 20%'>Severity Level:</th>
                            <td>
                                {{Form::text('severity_level', $task->bugTask->severity_level??"",['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('severity_level') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('severity_level') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            <th>Reference Number</th>
                            <td>
                                {{Form::text('ref_number', $task->bugTask->ref_number??"",['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('ref_number') ? ' is-invalid' : '')])}}
                                @foreach($errors->get('ref_number') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                            {{Form::hidden('bug', $bug)}}
                        </tr>
                    @endisset
                </tbody>
            </table>
            {{Form::close()}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('description', 'Description')</th>
                    <th>@sortablelink('resource.name', 'Resource')</th>
                    <th>@sortablelink('start_date', 'Start')</th>
                    <th>@sortablelink('end_date', 'End')</th>
                    <th>@sortablelink('status.description', 'Status')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task2)
                    <tr>
                        <td><a href="{{route('task.view',$task2->id)}}">{{isset($task2->description)?$task2->description:''}}</a></td>
                        <td>{{isset($task2->consultant->last_name)?$task2->consultant->last_name:''}}, {{isset($task2->consultant->first_name)?$task2->consultant->first_name:''}}</td>
                        <td>{{isset($task2->start_date)?$task2->start_date:''}}</td>
                        <td>{{isset($task2->end_date)?$task2->end_date:''}}</td>
                        <td>
                            {{Form::select('status_'.$task->id, $status_drop_down, $task->status, ['id' => $task->id, 'class'=>'form-control form-control-sm status_update', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks added for this project.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        </div>
    </div>
    </div>
@endsection
@section('extra-js')

    <script>
        $(function () {
            
            let sprint = $('#sprint').val()
                if(sprint && sprint > 0){
                    axios.get('/api/goal/'+sprint, {})
                        .then(response => {
                            $("#goal").html(populateSelect(JSON.parse(JSON.stringify(response.data.goals))??[], {{$task->goal_id}}));
                        });
                    }

            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');
            $('#form-submit').on('click', e => {
                let startdate = new Date($("#start_date").val()),
                    endDate = new Date($("#end_date").val());
                if (startdate.valueOf() > endDate.valueOf()){
                    alert("start date cannot be greater than end date")
                    return false;
                }

            })
            
            $('#sprint').on('click',function(){
                let sprint = $('#sprint').val()
                if(sprint && sprint > 0){
                    axios.get('/api/goal/'+sprint, {})
                        .then(response => {
                            $("#goal").html(populateSelect(JSON.parse(JSON.stringify(response.data.goals))??[], ''));
                        });
                    }
            })
        });

        function populateSelect(arr,selected_option) {
            let options = '';
                    options = options +
                    '<option value="">-</option>';
            for (var i = 0; i < arr.length; i++) {
                if(arr[i].name){
                    options = options +
                    '<option value=' + arr[i].id + ''+((arr[i].id == selected_option)?" selected":"")+ '>' + arr[i].name + '</option>';
                }
            }
            return options
        }
    </script>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .box-title{
            font-size: 19px;
        }
    </style>
@endsection
