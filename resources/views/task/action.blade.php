<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/14
 * Time: 17:28
 */
?>
@extends('adminlte.default')
@section('title')Project Tasks @endsection
@section('header')
    @include('task.task-buttons')
@endsection
@section('content')
    <div class="container-fluid">
        @include('task.task-menu')

        <hr>
        @include('task.task-filters')
        <hr>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Project: {{$project->name}}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">

                </div>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        .nav-tabs {
            border-bottom: 1px solid #ddd !important;
        }
        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        li.active {
            border-left: 1px solid #ddd !important;
            border-top: 1px solid #ddd !important;
            border-right: 1px solid #ddd !important;
        }

        .box-title{
            font-size: 19px;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection

