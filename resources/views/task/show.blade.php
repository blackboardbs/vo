@extends('adminlte.default')
@section('title') View Task @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="box">
            <div class="box-header">
                <tablec class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td><h3 class="box-title"><a href="{{route('project.show', $project->id)}}">{{$project->name}}</a> / <a href="">Epic</a> / <a href="{{route('userstory.show', $task->user_story_id)}}">User Story</a></h3></td>
                            <td>
                                <a href="{{route('task.create', $task->project_id)}}" class="btn btn-dark btn-sm float-right">Add another task to project no: {{$task->project_id}}</a>
                                <a href="{{route('project.show', $task->project_id)}}" class="btn btn-dark btn-sm float-right" style="margin-right: 10px;">Go to project no: {{$task->project_id}}</a>
                            </td>
                        </tr>
                    </tbody>
                </tablec>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm mt-3">
                        <thead>
                            <tr>
                                <th colspan="4" class="btn-dark" style="text-align: center;">Task No. {{$task->id}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th style='width: 20%'>Task No:</th>
                                <td>
                                    {{Form::text('task_number',$task->id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('task_number') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('task_number') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Description:</th>
                                <td>
                                    {{Form::text('description',$task->description,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('description') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th style='width: 20%'>Dependency</th>
                                <td>
                                    {{Form::text('dependency', isset($task->taskdependency->description) ? $task->taskdependency->description:'',['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('dependency') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('dependency') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Consultant</th>
                                <td>
                                    {{Form::text('consultant',isset($task->consultant->first_name)?$task->consultant->first_name:''."".isset($task->consultant->last_name)?$task->consultant->last_name:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('consultant') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('consultant') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Customer</th>
                                <td>
                                    {{Form::text('customer', isset($task->customer->customer_name)?$task->customer->customer_name:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('customer') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('customer') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Project</th>
                                <td>
                                    {{Form::text('project' ,isset($task->project->name)?$task->project->name:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('project') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('project') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>
                                    {{Form::text('start_date',$task->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('start_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>End Date</th>
                                <td>
                                    {{Form::text('end_date',$task->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('end_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Hours Planned</th>
                                <td>
                                    {{Form::text('hours_planned',$task->hours_planned,['class'=>'form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('hours_planned') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Billable</th>
                                <td>
                                    {{Form::text('billable',$task->billable == 1 ? 'Yes' : 'No',['class'=>'form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('billable') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>User Story</th>
                                <td>
                                    {{Form::select('user_story_id', $userStoryDropDown, $task->user_story_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('user_story_id') ? ' is-invalid' : ''), 'readonly' => 'readonly'])}}
                                    @foreach($errors->get('user_story_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Status</th>
                                <td>
                                    {{Form::text('status',isset($task->taskstatus->description)?$task->taskstatus->description:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('status') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('status') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td colspan="3">
                                    {{Form::textarea('note',$task->note_1,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'note', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('note') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Risks</th>
                                <td>
                                    {{Form::textarea('risks',isset($task->risks) ? $task->risks : '',['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12','id'=>'risks'])}}
                                    @foreach($errors->get('risks') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Issues</th>
                                <td>
                                    {{Form::textarea('issues',isset($task->issues) ? $task->issues : '',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'issues'])}}
                                    @foreach($errors->get('issues') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"><a class="btn btn-default" oncick="saveRisksAndIssues()" class="btn btn-sm">Save</a></td>
                            </tr>
                            @if($task->milistone == 1)
                            <tr>
                                <th colspan="4" class="btn-dark">Task is a Milestone</th>
                            </tr>
                            <tr>
                                <th>Invoice Date</td>
                                <td>
                                    {{Form::text('invoice_date',$task->invoice_date,['class'=>'datepicker form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('invoice_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Milestone</th>
                                <td>
                                    {{Form::text('milestone',$task->milistone == 1 ? 'Yes' : 'No',['class'=>'form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('milestone') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Invoice Ref</th>
                                <td>
                                    {{Form::text('invoice_ref',$task->invoice_ref,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('invoice_ref') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('invoice_ref') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Invoice Status</th>
                                <td>
                                    {{Form::text('invoice_status', isset($task->invoicestatus->description)?$task->invoicestatus->description:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('invoice_status') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                                    @foreach($errors->get('invoice_status') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            @endif
                        </tbody>
                </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-hover">
                        <thead class="btn-dark">
                        <tr>
                            <th>@sortablelink('description', 'Description')</th>
                            <th>@sortablelink('resource.name', 'Resource')</th>
                            <th>@sortablelink('start_date', 'Start')</th>
                            <th>@sortablelink('end_date', 'End')</th>
                            <th>@sortablelink('status.description', 'Status')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tasks as $task)
                            <tr>
                                <td><a href="{{route('task.view',$task->id)}}">{{isset($task->description)?$task->description:''}}</a></td>
                                <td>{{isset($task->consultant->last_name)?$task->consultant->last_name:''}}, {{isset($task->consultant->first_name)?$task->consultant->first_name:''}}</td>
                                <td>{{isset($task->start_date)?$task->start_date:''}}</td>
                                <td>{{isset($task->end_date)?$task->end_date:''}}</td>
                                <td>
                                    {{Form::select('status_'.$task->id, $status_drop_down, $task->status, ['id' => $task->id, 'class'=>' form-control form-control-sm status_update', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No tasks added for this project.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .box-title{
            font-size: 19px;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        function saveRisksAndIssues(){
            alert("What");
        }
    </script>
@endsection