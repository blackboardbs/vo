@extends('adminlte.default')
@section('title') View Task @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <button type="submit" class="btn btn-primary ml-1">Copy Task</button>
                @if($task->status != 5)
                    <a href="{{route('task.edit',$task->id)}}" class="btn btn-success ml-1">Edit</a>
                @endif
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <div class="box-header">
                <table class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td>
                                <h5 class="box-title">
                                    @if(isset($project->id))<a href="{{route('project.show', isset($project->id) ? $project->id : '')}}">{{$project->name}}</a> / @endif
                                    @if(isset($task->user_story->feature->epic->id))<a href="{{route('feature.show', isset($task->user_story->feature->epic->id) ? $task->user_story->feature->epic->id : '')}}">{{$task->user_story->feature->epic->name}}</a> / @endif
                                    @if(isset($task->user_story->feature->id))<a href="{{route('feature.show', isset($task->user_story->feature->id) ? $task->user_story->feature->id : '')}}">{{$task->user_story->feature->name}}</a> / @endif
                                    @if(isset($task->user_story->id))<a href="{{route('userstory.show', isset($task->user_story->id) ? $task->user_story->id : '')}}">{{$task->user_story->name}}</a> @endif
                                </h5>
                            </td>
                            <td>
                                <a href="{{route('task.create', $task->project_id)}}" class="btn btn-primary float-right">Add another task to project no: {{$task->project_id}}</a>
                                <a href="{{route('project.show', $task->project_id)}}" class="btn btn-primary float-right" style="margin-right: 10px;">Go to project no: {{$task->project_id}}</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
            {{Form::open(['url' => route('task.copy', $task->id), 'method' => 'post','class'=>'mt-3', 'id'=>'copy_task_frm','files' => true])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: center;">Task No: {{$task->id}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style='width: 20%'>Description:</th>
                        <td>
                            {{Form::text('description', $task->description,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('description') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Consultant</th>
                        <td>
                            {{Form::text('consultant', $consultant,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('consultant') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('consultant') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Project</th>
                        <td>
                            {{Form::text('project', isset($task->project->name)?$task->project->name:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('project') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('project') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Customer</th>
                        <td>
                            {{Form::text('customer', isset($task->customer->customer_name)?$task->customer->customer_name:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('customer') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('customer') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Sprint</th>
                        <td>
                            {{Form::select('sprint_id', $sprintDropdown, $task->sprint_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('sprint_id') ? ' is-invalid' : ''), 'placeholder' => '-', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('sprint_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>User Story</th>
                        <td>
                            {{Form::select('user_story_id', $userStories, $task->user_story_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('user_story_id') ? ' is-invalid' : ''), 'placeholder' => '-', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('user_story_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Dependency</th>
                        <td>
                            {{Form::text('dependency', isset($task->taskdependency->description)?$task->taskdependency->description:'',['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('dependency') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('dependency') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Goal</th>
                        <td>
                            {{Form::select('goal_id', $userStories, $task->goal_id,['id'=>'goal','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('goal_id') ? ' is-invalid' : ''), 'placeholder' => '-', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('goal_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Delivery Type</th>
                        <td>
                            {{Form::select('task_delivery_type_id', $task_delivery_type_drop_down, $task->task_delivery_type_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('task_delivery_type_id') ? ' is-invalid' : ''), 'placeholder' => '', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('task_delivery_type_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Status</th>
                        <td>
                            {{Form::text('status', isset($task->taskstatus->description)?$task->taskstatus->description:'',['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('status') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Start Date</th>
                        <td>
                            {{Form::text('start_date',$task->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('start_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>End Date</th>
                        <td>
                            {{Form::text('end_date',$task->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('end_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Billable</th>
                        <td>
                            {{Form::select('billable',['0'=>'No','1'=>'Yes'], $task->billable,['class'=>'form-control form-control-sm col-sm-12', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('billable') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Display On Kanban</th>
                        <td>
                            {{Form::text('user_story_id',  $task->display_on_kanban,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('user_story_id') ? ' is-invalid' : ''), 'placeholder' => '-', 'disabled' => 'disabled'])}}
                            @foreach($errors->get('user_story_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Hours Planned</th>
                        <td>
                            {{Form::text('hours_planned', $task->hours_planned,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('hours_planned') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('hours_planned') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Weight</th>
                        <td>
                            {{Form::text('weight', $task->weight,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('weight') ? ' is-invalid' : ''), 'disabled'=>'disabled'])}}
                            @foreach($errors->get('weight') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Note</th>
                        <td colspan="3">
                           {!! $task->note_1 !!}
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('description', 'Description')</th>
                    <th>@sortablelink('resource.name', 'Resource')</th>
                    <th>@sortablelink('start_date', 'Start')</th>
                    <th>@sortablelink('end_date', 'End')</th>
                    <th>@sortablelink('status.description', 'Status')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr>
                        <td><a href="{{route('task.view',$task->id)}}">{{isset($task->description)?$task->description:''}}</a></td>
                        <td>{{isset($task->consultant->last_name)?$task->consultant->last_name:''}}, {{isset($task->consultant->first_name)?$task->consultant->first_name:''}}</td>
                        <td>{{isset($task->start_date)?$task->start_date:''}}</td>
                        <td>{{isset($task->end_date)?$task->end_date:''}}</td>
                        <td>
                            {{Form::select('status_'.$task->id, $status_drop_down, $task->status, ['id' => $task->id, 'class'=>' form-control form-control-sm status_update', 'style'=>'width: 100%;', 'disabled' => 'disabled'])}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No tasks added for this project.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>


    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');
        });
    </script>
@endsection