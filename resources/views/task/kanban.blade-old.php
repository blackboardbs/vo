<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 2018/10/16
 * Time: 17:28
 */
?>

@extends('adminlte.default')
@section('title')Kanban Board @endsection
@section('header')
    @include('task.task-buttons')
@endsection

@section('content')
    <div class="container-fluid">
        @include('task.task-menu')
        <hr style="margin-top: -1px">
        @include('task.task-filters')
        <hr class="mt-0">
        {{-- <task-component parameters="{{json_encode($vueParameters)}}" project-id="{{$project->id}}"></task-component> --}}
        <task-cards kanban="{{json_encode($kanban)}}" user-id="{{auth()->id()}}" filters="{{json_encode(array_filter(request()->all(), static function($var){return $var !== null;} ))}}" project-id="{{$project_id}}"></task-cards>
        {{--<div class="row">
            @foreach($task_statuses as $task_status)
                <div class="col-lg-3 task-board">
                    <p class="board-heading">{{$task_status->description}}</p>
                    <ul id="ul_list_{{$task_status->id}}" board-id="{{$task_status->id}}" class="sortable connectedSortable">
                        @foreach($tasks as $task)
                            @if($task->status == $task_status->id)
                                <li task-id="{{$task->id}}" project-id="{{$project_id}}" class="card card_{{$task->task_type_id}}"><span class="task-type">{{isset($task->tasktype->name)?$task->tasktype->name:''}}</span>{{$task->description}}</li>
                            @endif
                        @endforeach
                    </ul>
                    <P class="add-card">
                        <a class="add-card-link" onclick="addTask({{$project_id}}, {{$task_status->id}})">Add Task ...</a>
                    </P>
                </div>
            @endforeach
        </div>--}}
    </div>

    <button id="launch_modal" style="display: none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_task_modal">
        Launch modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="add_task_modal" tabindex="-1" role="dialog" aria-labelledby="add_task_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div style="width: 800px;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_task_modal_label">Add Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-sm mt-3">
                        <input type="hidden" id="project_id" name="project_id">
                        <input type="hidden" id="task_status_id" name="task_status_id">
                        <thead>
                            <tr>
                                <th colspan="4" class="btn-dark" style="text-align: center;">Create New Task</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th style='width: 50%'>Description:</th>
                                <td>
                                    {{Form::text('description', old('description'), ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('description') ? ' is-invalid' : ''), 'id' => 'description', 'placeholder'=>'Description'])}}
                                    @foreach($errors->get('description') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <td>
                                    {{Form::select('task_type_id', $task_type_drop_down, null,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('task_type_id') ? ' is-invalid' : ''), 'id' => 'task_type_id', 'placeholder' => 'Select Type'])}}
                                    @foreach($errors->get('task_type_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Hours</th>
                                <td>
                                    {{Form::text('hours_planned',old('hours_planned'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('hours_planned') ? ' is-invalid' : ''), 'id' => 'hours_planned','placeholder'=>'Hours'])}}
                                    @foreach($errors->get('hours_planned') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Consultant</th>
                                <td>
                                    {{Form::select('consultant', $task_constultants_drop_down,null,['class'=>'form-control form-control-sm col-sm-12 '. ($errors->has('consultant') ? ' is-invalid' : ''), 'id' => 'consultant', 'placeholder' => 'Select Consultant'])}}
                                    @foreach($errors->get('consultant') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>
                                    {{Form::text('new_start_date',$project->start_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('start_date') ? ' is-invalid' : ''), 'id' => 'new_start_date','placeholder'=>'Start Date'])}}
                                    @foreach($errors->get('start_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>End Date</th>
                                <td>
                                    {{Form::text('new_end_date',$project->end_date,['class'=>'datepicker form-control form-control-sm col-sm-12'. ($errors->has('end_date') ? ' is-invalid' : ''), 'id' => 'new_end_date','placeholder'=>'End Date'])}}
                                    @foreach($errors->get('end_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Billable</th>
                                <td>
                                    {{Form::select('billable', [0 => 'No', 1 => 'Yes'],$project->billable,['class'=>'form-control form-control-sm col-sm-12', 'id' => 'billable', 'placeholder' => 'Select'])}}
                                    @foreach($errors->get('billable') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td colspan="3">
                                    {{Form::textarea('note',null,['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','id'=>'note', 'placeholder' => 'note'])}}
                                    @foreach($errors->get('note') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="justify-content: center;">
                    <button id="btn_clode_modal" type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-dark btn-sm" onclick="saveTask()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('extra-js')

    <script>

        $(function() {



            $( ".sortable" ).sortable({
                connectWith: ".connectedSortable",
                receive: function( event, ui ) {
                    //$(this).css({"background-color":"blue"});
                    var board_id = $(this).attr("board-id");
                    var task_id = ui.item.attr('task-id');
                    var project_id = ui.item.attr('project-id');

                    var template_id = $("select[name=template_type_id]").val();
                    axios.post('/task/move/'+task_id, {
                        'status': board_id,
                        'project_id': project_id
                    })
                    .then(function (data) {
                        // console.log(data);
                        // location.reload();
                    })
                    .catch(function (error) {
                        alert("Your session has expired please login and try again");
                        window.location.reload();
                    });
                }
            }).disableSelection();

            $('.add-button').click(function() {
                var txtNewItem = $('#new_text').val();
                $(this).closest('div.task-board').find('ul').append('<li class="card">'+txtNewItem+'</li>');
            });

            $('select').on('change', () => $("select").trigger("chosen:updated"))
        });


    </script>
@endsection
