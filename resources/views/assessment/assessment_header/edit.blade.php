@extends('adminlte.default')

@section('title') Edit Assessment Header @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('assess')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('assessment.update',$assessment_header), 'method' => 'patch','class'=>'mt-3', 'autocomplete' => 'off','id'=>'assess'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>Name</th>
                        <td colspan="3">{{ Form::select('user_id', $user_dropdown, $assessment_header->resource_id, ['class'=>'form-control form-control-sm'. ($errors->has('user_id') ? ' is-invalid' : '') , 'id' => 'resource_id']) }}
                            @foreach($errors->get('user_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Customer</th>
                        <td>{{Form::select('customer_id',$customer_dropdown,$assessment_header->customer_id,['class'=>'form-control form-control-sm '.($errors->has('customer_id') ? ' is-invalid' : ''),'id'=>'customer'])}}
                            @foreach($errors->get('customer_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th  colspan="3">Assessed By</th>
                        <td >{{ Form::select('assessor_id', $assessor_dropdown, $assessment_header->assessed_by, ['class'=>'form-control form-control-sm '. ($errors->has('assessor_id') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('assessor_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th style="width: 25%">Template</th>
                        <td>{{ Form::select('template_id', $templates_dropdown, $assessment_header->template_id, ['class'=>'form-control'. ($errors->has('template_id') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('template_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Assessement Date</th>
                        <td colspan="3">{{ Form::text('assessment_date', $assessment_header->assessment_date, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('assessment_date') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('assessment_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Next Assessment</th>
                        <td>{{ Form::text('next_assessment', $assessment_header->next_assessment, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('next_assessment') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('next_assessment') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Assessement Start</th>
                        <td colspan="3">{{ Form::text('assessment_start', $assessment_header->assessment_period_start, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('assessment_start') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('assessment_start') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Assessment End</th>
                        <td>{{ Form::text('assessment_end', $assessment_header->assessment_period_end, ['class'=>'form-control form-control-sm datepicker'. ($errors->has('assessment_end') ? ' is-invalid' : '')]) }}
                            @foreach($errors->get('assessment_end') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection