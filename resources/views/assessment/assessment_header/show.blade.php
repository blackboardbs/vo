@extends('adminlte.default')

@section('title') Resource Assessment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('assessment.edit',$assessment_header->id)}}" class="btn btn-success float-right mx-1">Edit</a>
            <div class="dropdown">
                <button class="btn btn-dark dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-file" aria-hidden="true"></i> Documents
                </button>

                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('assessment.print', $assessment_header)}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                    <a class="dropdown-item" href="{{route('assessment.send', $assessment_header)}}"><i class="fa fa-envelope" aria-hidden="true"></i> Send</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th colspan="4" class="btn-dark">Assessment Header</th>
                </tr>
                </thead>
                <tr>
                    <th>Name</th>
                    <td>{{(isset($assessment_header->user->first_name) ? $assessment_header->user->first_name : '')}} {{(isset($assessment_header->user->last_name) ? $assessment_header->user->last_name : '' )}}</td>
                    <th>Resource Number</th>
                    <td>{{ (isset($assessment_header->user->resource->resource_no) && $assessment_header->user->resource->resource_no != null) ? $assessment_header->user->resource->resource_no : '' }}</td>
                </tr>

                <tr>
                    <th>Position</th>
                    <td>{{( ($assessment_header->user->resource != null) && ($assessment_header->user->resource->employee_position != null)) ? $assessment_header->user->resource->employee_position->description : '' }}</td>
                    <th>Template</th>
                    <td>{{$assessment_header->template?->name}}</td>
                </tr>

                <tr>
                    <th>Assessment Date</th>
                    <td>{{ $assessment_header->assessment_date }}</td>
                    <th>Next Assessment</th>
                    <td>{{ $assessment_header->next_assessment }}</td>
                </tr>
                <tr>
                    <th>Assessment Period</th>
                    <td>{{ $assessment_header->assessment_period_start }}</td>
                    <th>To</th>
                    <td>{{ $assessment_header->assessment_period_end }}</td>
                </tr>
                <tr>
                    <th>Client/Project</th>
                    <td>{{ isset($assessment_header->customer)?$assessment_header->customer->customer_name:'' }}</td>
                    <th>Assessed By</th>
                    <td>{{ isset($assessment_header->assessor)?$assessment_header->assessor->first_name.' '.$assessment_header->assessor->last_name:'' }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center"></td>
                </tr>
            </table>
            <span>{{Form::hidden('assessment_id', $assessment_header->id)}}</span>
            <table  class="table table-bordered table-sm" id="activity-table">
                <thead>
                <tr class="btn-dark">
                    <th colspan="6">This Assessment Period
                         <button class="btn btn-outline-light btn-sm float-right" id="add-line"><i class="fa fa-plus" aria-hidden="true"></i> Add Activity</button>
                        </th>
                </tr>
                <tr class="btn-light">
                    <th width="30">#</th>
                    <th>Task</th>
                    <th>Competency Level <span style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="The competency level of the task where 10 is expert and 0 is not competent."><i class="far fa-question-circle"></i></span></th>
                    <th>Comfort Level <span style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="The level of comfort that the resource has with this task. This is not how well the task is completed, but how much the resource enjoy doing this task. 10 is comfortable, 0 is hating it."><i class="far fa-question-circle"></i></span></th>
                    <th>Notes</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_activities as $assessment_activity)
                    <tr>
                        <td>{{$assessment_activity->id}}</td>
                        <td>{{$assessment_activity->assessment_task_description}}</td>
                        <td>{{$assessment_activity->competency_level}}</td>
                        <td>{{$assessment_activity->comfort_level}}</td>
                        <td>{{$assessment_activity->notes}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assessment_activity.edit',$assessment_activity)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assessment_activity.destroy', $assessment_activity],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr id="activity-addition">
                        <td>&nbsp;</td>
                        <td>
                            {{Form::textarea('assessment_task_description', old('assessment_task_description'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Task description'])}}
                        </td>
                        <td>
                            {{Form::select('competency_level', $competency_level_dropdown, 1, ['class' => 'form-control form-control-sm'])}}
                        </td>
                        </td>
                        <td>
                            {{Form::select('comfort_level', $competency_level_dropdown, 1, ['class' => 'form-control form-control-sm'])}}
                        </td>
                        </td>
                        <td>
                            {{Form::textarea('notes', old('notes'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Notes'])}}

                        </td>

                        </td>
                        <td><a href="#" class="btn btn-primary" id="save-activity"><i class="fas fa-save"></i></td>
                    </tr>
                @endforelse

                </tbody>
            </table>

            <table  class="table table-bordered table-sm" id="plan-table">
                <thead>
                <tr class="btn-dark">
                    <th colspan="6">Next Assessment Period 
                            <button class="btn btn-outline-light btn-sm float-right" id="add-plan"><i class="fa fa-plus" aria-hidden="true"></i> Add Plan</button>
                    </th>
                </tr>
                <tr class="btn-light">
                    <th width="30">#</th>
                    <th>Task</th>
                    <th>Notes</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_plan as $plan)
                    <tr>
                        <td>{{$plan->id}}</td>
                        <td>{{$plan->assessment_task_description}}</td>
                        <td>{{$plan->notes}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assessment_plan.edit',$plan)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assessment_plan.destroy', $plan],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr id="plan-addition">
                        <td>&nbsp;</td>
                        <td>
                            {{Form::textarea('assessment_task_description', old('assessment_task_description'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Task description'])}}
                        </td>
                        <td>
                            {{Form::textarea('notes', old('notes'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Notes'])}}
                        </td>

                        </td>
                        <td><a href="#" class="btn btn-primary" id="save-plan"><i class="fas fa-save"></i></a></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <table  class="table table-bordered table-sm" id="concerns-table">
                <thead>
                <tr class="btn-dark">
                    <th colspan="6">Assessment Concerns 
                        <button class="btn btn-outline-light btn-sm float-right" id="concerns-plan"><i class="fa fa-plus" aria-hidden="true"></i> Add Concerns</button>
                    </th>
                </tr>
                <tr class="btn-light">
                    <th width="30">#</th>
                    <th>Concerns / Areas of improvements</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_concerns as $concern)
                    <tr>
                        <td>{{$concern->id}}</td>
                        <td>{{$concern->notes}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assessment_concern.edit',$concern)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assessment_concern.destroy', $concern],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr id="concerns-addition">
                        <td>&nbsp;</td>
                        <td>
                            {{Form::textarea('notes', old('notes'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Notes'])}}

                        </td>

                        </td>
                        <td><a href="#" class="btn btn-primary" id="save-concerns"><i class="fas fa-save"></i></a></td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <table  class="table table-bordered table-sm" id="notes-table">
                <thead>
                <tr class="btn-dark">
                    <th colspan="6">Assessment Notes 
                        <button class="btn btn-outline-light btn-sm float-right" id="add-note"><i class="fa fa-plus" aria-hidden="true"></i> Add Notes</button>
                </th>
                </tr>
                <tr class="btn-light">
                    <th width="30">#</th>
                    <th>General Notes</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_notes as $note)
                    <tr>
                        <td>{{$note->id}}</td>
                        <td>{{$note->notes}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assessment_notes.edit',$note)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assessment_notes.destroy', $note],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr id="notes-addition">
                        <td>&nbsp;</td>
                        <td>
                            {{Form::textarea('notes', old('notes'), ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Notes'])}}
                        </td>

                        </td>
                        <td><a href="#" class="btn btn-primary" id="save-note"><i class="fas fa-save"></i></a></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{Form::open(['url' => route('assessment_measure'), 'method' => 'post','class'=>'my-3'])}}
            <table  class="table table-bordered table-sm mb-0" id="assessment_measure_show">
                <thead>
                <tr class="btn-dark">
                    <th colspan="2">Assessment Measures <button class="btn btn-outline-light btn-sm float-right" id="edit-assessment"><i class="fas fa-edit"></i> Edit Assessment Measure</button></th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_master as $index => $master)
                    <tr>
                        <th class="last pr-4" nowrap>{{isset($master->description)?$master->description:''}}</th>

                        <td class="pl-2">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Assessment Measure</th>
                                    <th>Score <span style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="5 is good and 0 is bad."><i class="far fa-question-circle"></i></span></th>
                                    <th>Note</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($assessment_measure as $measure)
                                    @if($assessment_header->id == $measure->assessment_id && $master->id == $measure->assessment_master_id)
                                        <tr>
                                            <td>{{$measure->assessment_master_details->assessment_measure}}</td>
                                            <td>{{$measure->score}}</td>
                                            <td>{{$measure->notes}}</td>
                                        </tr>
                                    @endif
                                @empty
                                    @forelse($assessment_master_details[$index] as $details)
                                        <tr>
                                            {!! Form::hidden('assessment_id', $assessment_header->id) !!}
                                            {!! Form::hidden('assessment_master_id[]', $master->id) !!}
                                            {!! Form::hidden('assessment_measure_detail_id[]', $details->id) !!}
                                            <td>
                                                {!! Form::text('assessment_measure_details[]', $details->assessment_measure, ['class'=>'form-control form-control-sm measure-details'. ($errors->has('assessment_measure_details[]') ? ' is-invalid' : ''), 'disabled']) !!}
                                                @foreach($errors->get('assessment_measure_details[]') as $error)
                                                    <div class="invalid-feedback">
                                                        {{ $error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td>
                                                {!! Form::select('assessment_measure_score[]', [0=>0, 1=>1,2=>2,3=>3,4=>4,5=>5], null,['class'=>'form-control form-control-sm'. ($errors->has('assessment_measure_score[]') ? ' is-invalid' : '')]) !!}
                                                @foreach($errors->get('assessment_measure_score[]') as $error)
                                                    <div class="invalid-feedback">
                                                        {{ $error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td>
                                                {!! Form::textarea('assessment_note[]', old('assessment_note[]'), ['class'=> 'form-control form-control-sm'. ($errors->has('assessment_note[]') ? ' is-invalid' : ''), 'rows'=>2]) !!}
                                                @foreach($errors->get('assessment_note[]') as $error)
                                                    <div class="invalid-feedback">
                                                        {{ $error}}
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @empty
                                    @endforelse
                                @endforelse
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @empty


                @endforelse
                @if(count($assessment_master_details) )
                    <tr>
                        <td colspan="2" class="text-center">
                            {!! Form::submit('Save Assessment', ['class' => 'btn btn-sm btn-dark']) !!}
                            <a href="{{route('assessment.show', ['assessment'=>$assessment_header, 'print'=>1])}}" id="exampleModalLabel" class="btn btn-sm btn-dark ml-2"><i class="fas fa-print"></i> Print Assessment</a>
                            <a href="#" class="btn btn-sm bg-dark d-print-none ml-2" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Assessment</a>
                            <a href="#" class="btn btn-sm btn-success d-print-none ml-2" data-toggle="modal" data-target="#assessmentDigisignModal"><i class="fas fa-paper-plane"></i> Send Digisign</a>
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>
            {!! Form::close() !!}
            {{Form::open(['url' => route('assessment_measure'), 'method' => 'post','class'=>'my-3', 'id' => 'editing-assessment'])}}
            <table  class="table table-bordered table-sm mb-0">
                <thead>
                <tr class="btn-dark">
                    <th colspan="2">Assessment Measures </th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_master as $index => $master)
                    <tr>
                        <th>{{isset($master->description)?$master->description:''}}</th>

                        <td>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Assessment Measure</th>
                                    <th>Score <span style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="5 is good and 0 is bad."><i class="far fa-question-circle"></i></span></th>
                                    <th>Note</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($assessment_master_details[$index] as $details)
                                    <tr>
                                        {!! Form::hidden('assessment_id', $assessment_header->id) !!}
                                        {!! Form::hidden('assessment_master_id[]', $master->id) !!}
                                        {!! Form::hidden('assessment_measure_detail_id[]', $details->id) !!}
                                        <td>
                                            {!! Form::text('assessment_measure_details[]', $details->assessment_measure, ['class'=>'form-control form-control-sm measure-details'. ($errors->has('assessment_measure_details[]') ? ' is-invalid' : ''), 'disabled']) !!}
                                            @foreach($errors->get('assessment_measure_details[]') as $error)
                                                <div class="invalid-feedback">
                                                    {{ $error}}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            {!! Form::select('assessment_measure_score[]', [0=>0, 1=>1,2=>2,3=>3,4=>4,5=>5], isset($assessment_measure_scores[$details->id]) ? $assessment_measure_scores[$details->id]['score'] : null,['class'=>'form-control form-control-sm'. ($errors->has('assessment_measure_score[]') ? ' is-invalid' : '')]) !!}
                                            @foreach($errors->get('assessment_measure_score[]') as $error)
                                                <div class="invalid-feedback">
                                                    {{ $error}}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td>
                                            {!! Form::textarea('assessment_note[]', isset($assessment_measure_scores[$details->id]) ? $assessment_measure_scores[$details->id]['note'] : null, ['class'=> 'form-control form-control-sm'. ($errors->has('assessment_note[]') ? ' is-invalid' : ''), 'rows'=>2]) !!}
                                            @foreach($errors->get('assessment_note[]') as $error)
                                                <div class="invalid-feedback">
                                                    {{ $error}}
                                                </div>
                                            @endforeach
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @empty
                @endforelse
                <tr>
                    <td colspan="2" class="text-center">
                        {!! Form::submit('Save Assessment', ['class' => 'btn btn-sm btn-dark']) !!}
                        <a href="{{route('assessment.show', ['assessment'=>$assessment_header, 'print'=>1])}}" id="exampleModalLabel" class="btn btn-sm btn-dark ml-2"><i class="fas fa-print"></i> Print Assessment</a>
                        <a href="#" class="btn btn-sm bg-dark ml-2" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-paper-plane"></i> Send Assessment</a>
                    </td>
                </tr>
                </tbody>
            </table>
            {!! Form::close() !!}
        </div>
        <!-- Modal -->
        {!! Form::open(['url' => route('assessment.email', ['id'=>$assessment_header, 'email'=>1]), 'method' => 'post']) !!}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title default" id="exampleModalLabel">Select Emails or Enter An Email</h5>
                        <h5 class="modal-title sending text-center" id="exampleModalLabel" style="display: none">Sending Your Assessment...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="email-form">
                            <div class="form-check">
                                <input type="checkbox" name="assessor" value="{{ isset($assessment_header->assessor)?$assessment_header->assessor->email:''}}" checked class="form-check-input" id="assessor">
                                <label class="form-check-label" for="customer_email">{{ isset($assessment_header->assessor)?$assessment_header->assessor->email:''}}</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" name="resource" value="{{(isset($assessment_header->user->first_name) ? $assessment_header->user->email : '')}}" checked class="form-check-input" id="resource">
                                <label class="form-check-label" for="assessor">{{(isset($assessment_header->user->first_name) ? $assessment_header->user->email : '')}}</label>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="user_email" class="form-control form-control-sm" id="user_email" placeholder="Email">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group text-center">
                                <button class="btn btn-dark btn-sm" id="send-btn"><i class="fas fa-paper-plane"></i> Send Assessment</button>
                            </div>
                        </div>
                        <div id="loader-wrapper">
                            <div class="rounded-circle border-top border-bottom border-dark" id="loader" style="width: 6rem;height: 6rem;margin: 0 auto; border-width: 10px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    </div>
    <!-- Start Block - Candidate email CV Modal -->
    @include('assessment.assessment_header.send_digising_modal')
    <!-- End Block - Candidate email CV Modal -->
@endsection

@section("extra-css")
    <link rel="stylesheet" href="{{asset('css/multiple-emails.css')}}">
    <style>
        ul.multiple_emails-ul{
            margin-bottom: 0!important;
        }
         #loader-wrapper{
             display: none;
         }
        #loader {
            -webkit-animation-name: spin;
            -webkit-animation-duration: 1500ms;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-name: spin;
            -moz-animation-duration: 1500ms;
            -moz-animation-iteration-count: infinite;
            -moz-animation-timing-function: ease-in-out;
            -ms-animation-name: spin;
            -ms-animation-duration: 1500ms;
            -ms-animation-iteration-count: infinite;
            -ms-animation-timing-function: ease-in-out;

            animation-name: spin;
            animation-duration: 1500ms;
            animation-iteration-count: infinite;
            animation-timing-function: ease-in-out;
        }
        @-ms-keyframes spin {
            from { -ms-transform: rotate(0deg); }
            to { -ms-transform: rotate(360deg); }
        }
        @-moz-keyframes spin {
            from { -moz-transform: rotate(0deg); }
            to { -moz-transform: rotate(360deg); }
        }
        @-webkit-keyframes spin {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endsection

@section('extra-js')
    <script src="{{ global_asset('js/assessments.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{global_asset('js/multiple-emails.js')}}"></script>
    <script>
        $(function () {
        $("#editing-assessment").hide();
        $("#edit-assessment").on('click', function (e) {
            e.preventDefault();
            $("#assessment_measure_show").hide();
            $("#editing-assessment").show();
        })
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })
        $(function () {
            $('#user_email').multiple_emails({
                position: 'top', // Display the added emails above the input
                theme: 'bootstrap', // Bootstrap is the default theme
                checkDupEmail: true // Should check for duplicate emails added
            });

            $('#current_emails').text($('#user_email').val());

            $('#user_email').change( function(){
                $('#current_emails').text($(this).val());
            });
        })
        $("#send-btn").on('click', function () {
            $("#email-form").hide();
            $("#loader-wrapper").fadeIn();
            $(".default").hide();
            $(".sending").fadeIn();
        });
    </script>
@endsection