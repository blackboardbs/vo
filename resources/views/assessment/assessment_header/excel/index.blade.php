<table>
    <thead>
    <tr>
        <th>Resource</th>
        <th>Customer</th>
        <th>Assessment Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($assessment_headers as $result)
        <tr>
            <td>{{$result->user?->name()}}</td>
            <td>{{$result->customer?->customer_name}}</td>
            <td>{{$result->assessment_date}}</td>
        </tr>
    @endforeach
    </tbody>
</table>