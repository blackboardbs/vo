@extends('adminlte.default')

@section('title') Assessment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('assessment.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Assessment</a>
        <x-export route="assessment.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{route('assessment.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('user.first_name','Resource')</th>
                    <th>@sortablelink('customer.customer_name','Customer')</th>
                    <th>@sortablelink('assessment_date','Assessment Date')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assessment_headers as $result)
                    <tr>
                        <td><a href="{{route('assessment.show',$result)}}">{{$result->user->first_name.' '.$result->user->last_name}}</a></td>
                        <td><a href="{{route('assessment.show',$result)}}">{{isset($result->customer) ? $result->customer->customer_name : '<CustomerName>'}}</a></td>
                        <td class="last">{{$result->assessment_date}}
                            @if(isset($data[$result->id]) && $data[$result->id] == 1)
                            <a href="{{route('assessment.copy', $result)}}" class="btn btn-sm btn-dark ml-3 mr-2"><i class="fas fa-plus"></i> New Assessment</a>
                            @endif
                        </td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assessment.edit',$result)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assessment.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No assessments are available.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $assessment_headers->firstItem() }} - {{ $assessment_headers->lastItem() }} of {{ $assessment_headers->total() }}
                        </td>
                        <td>
                            {{ $assessment_headers->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $assessment_headers->links() }} --}}
        </div>
    </div>
@endsection
