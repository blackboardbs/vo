@extends('adminlte.default')

@section('title') Add Assessment Header @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('assess')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('assessment.store'), 'method' => 'post','class'=>'mt-3', 'autocomplete' => 'off','id'=>'assess'])}}
                        <div class="mt-1 d-block w-100">
                            <span class="alert alert-danger" id="no-company" style="display: none;"></span>
                        </div>
            <table class="table table-bordered table-sm">
                <tr>
                    <th style="width: 15%">Name</th>
                    {{-- <td>{{ Form::select('resource_id', $resource_dropdown, !Auth::user()->hasRole('admin') ? Auth::user()->id : null, ['class'=>'form-control form-control-sm'. ($errors->has('resource_id') ? ' is-invalid' : ''), 'id' => 'resource_id']) }} --}}
                        <td><select name="resource_id" class="form-control {{($errors->has('resource_id') ? ' is-invalid' : '')}}" id="resource_id">
                        @foreach($resource_dropdown as $key => $value)
                            <option value="{{$key}}" {{!Auth::user()->hasRole('admin') && Auth::user()->id == $key ? 'selected' : ''}}>{{$value}}</option>
                        @endforeach
                        </select>
                        @foreach($errors->get('resource_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th style="width: 25%">Customer</th>
                    <td>{{ Form::select('customer_id', $customer_dropdown, null, ['class'=>'form-control '. ($errors->has('customer_id') ? ' is-invalid' : ''), 'id' => 'customer']) }}
                        
                        @foreach($errors->get('customer_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th style="width: 15%">Assessed By</th>
                    <td>{{ Form::select('assessed_by', $assesor_dropdown, $manager_id, ['class'=>'form-control'. ($errors->has('assessed_by') ? ' is-invalid' : ''), 'id'=>'assessor']) }}
                        @foreach($errors->get('assessed_by') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th style="width: 25%">Template</th>
                    <td>{{ Form::select('template_id', $templates_dropdown, null, ['class'=>'form-control'. ($errors->has('template_id') ? ' is-invalid' : '')]) }}
                        @foreach($errors->get('template_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
@endsection
@section('extra-js')
    <script src="{{asset('chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
    {{--<script src="{{ asset('js/assessments.js') }}"  type="text/javascript" ></script>--}}
    <script>
        $(function () {
            $('.chosen-select').css('width', '100%');
            $('.chosen-container').css('width', '100%');
        });
        $(function () {
            let resource_id = $("#resource_id");
            resource_id.on('change', function () {
                axios.get('/assessment_customers/' + resource_id.val())
                    .then(function (response) {
                        if(response.data.length > 0){
                            for (i = 0; i < response.data.length; i++) {
                                $("#customer").append($('<option>', {
                                    value: response.data[i].id,
                                    text : response.data[i].customer_name
                                }));
                            };
                            $("#no-company").removeClass('d-block');
                            $("#no-company").hide();
                        }else {
                            $("#no-company").show().text("Selected consultant has no assignment");
                            $("#no-company").addClass('d-block');
                        }

                    }).catch(function (error) {
                        console.log(error);
                });

                axios.get('/assessment_approver/' + resource_id.val())
                    .then(function (response) {
                        console.log()
                        $("#assessor").val(response.data.manager_id);
                    }).catch(function (error) {
                    console.log(error);
                });
            })
        })
    </script>
@endsection