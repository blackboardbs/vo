<!-- Modal -->
<div class="modal fade" id="assessmentDigisignModal" tabindex="-1" role="dialog" aria-labelledby="assetDigisignModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assessment Digisign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => route('digisign.assessment', $assessment_header->id), 'method' => 'post']) !!}
                <div class="form-group row">
                    <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                    </div>
                </div>
                <hr>
                <div class="form-check">
                    <input type="checkbox" name="resource_email" value="{{$resource->email}}" checked class="form-check-input" id="resource_email" />
                    <label class="form-check-label" for="resource_email">{{$resource->first_name.' '.$resource->last_name}} ({{$resource->email}})</label>
                </div>
                <hr>
                <div class="form-group text-center">
                    {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>