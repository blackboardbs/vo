@extends('adminlte.default')

@section('title') Edit Assessment Notes @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
                {{Form::open(['url' => route('assessment_notes.update',$assessment_notes), 'method' => 'patch','class'=>'mt-3'])}}
                <table class="table table-bordered table-sm">
                    <tr>
                        <th>General Notes</th>
                        <td colspan="3">{{Form::textarea('notes', $assessment_notes->notes, ['class' => 'form-control form-control-sm', 'rows' => 2, 'placeholder' => 'Task description'])}}
                            @foreach($errors->get('notes') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </table>
                <table class="table table-borderless">
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </table>
                {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
@endsection