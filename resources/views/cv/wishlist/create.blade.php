@extends('adminlte.default')
@section('title') Add Wishlist @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('cvwishlist.store'), 'method' => 'post','class'=>'mt-3'])}}
            {{--<input type="hidden" id="job_spec_id" name="cv_id" value="{{$job_spec_id}}" />
            <input type="hidden" id="cv_id" name="cv_id" value="{{$cv_id}}" />--}}
            <input type="hidden" name="o" id="o" value="{{old('o')}}" />
            <table class="table table-bordered table-sm">
                <tr>
                    <th>Name <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('name', old('name'), ['class'=>'form-control form-control-sm','id'=>'name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    {{--<th>Job Reference<small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        {{Form::select('job_spec_id', $job_spec_drop_down, old('job_spec_id'),['class'=>'form-control form-control-sm '. ($errors->has('job_spec_id') ? ' is-invalid' : ''),'id' => 'job_spec_id'])}}
                        @foreach($errors->get('ind_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>--}}
                </tr>
            </table>
            <table class="table table-borderless">
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection