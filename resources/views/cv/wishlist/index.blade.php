@extends('adminlte.default')
@section('title') CV WishList @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
        <a href="{{route('cvwishlist.create')}}?o=1" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Wishlist</a>
        @endif
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))
                        <th class="text-right">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($cvWishLists as $cvWishList)
                    <tr>
                        <td><a href="{{route('cvwishlist.show',$cvWishList->id)}}">{{$cvWishList->name}}</a></td>
                        <td>{{isset($cvWishList->status->description) ? $cvWishList->status->description : ''}}</td>
                        @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager'))
                            <td class="text-right">
                                @if($can_update)
                                <a href="{{route('cvwishlist.edit',$cvWishList->id)}}" class="btn btn-success btn-sm">Edit</a>
                                @endif
                                {{ Form::open(['method' => 'DELETE','route' => ['cvwishlist.destroy', $cvWishList->id],'style'=>'display:inline','class'=>'delete']) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No Wishlists match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
