@extends('adminlte.default')
@section('title') View Wishlist @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th colspan="4" class="btn-dark" style="text-align: left;">Wishlist Information</th>
                    </tr>
                </thead>
                <tr>
                    <th>Name:<span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('name', $cvWishList->name, ['class'=>'form-control form-control-sm','id'=>'name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Job References:<span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>
                        @php
                            $jobSpecs
                        @endphp
                        @foreach($jobSpecs as $key => $jobSpec)
                            {{$jobSpec->reference_number}}
                            @if($key != (sizeof($jobSpecs) - 1))
                                ,
                            @endif
                        @endforeach
                    </td>
                </tr>
            <tbody>
        </table>
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th colspan="7" class="btn-dark" style="text-align: left;">CVs</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Name</th><th>Email</th><th>Role</th><th>Phone</th><th>Sent</th><th>Job Spec Reference</th><th class="text-right">Actions</th>
                </tr>
                @forelse($cvs as $cv)
                    <tr>
                        <td><a href="{{route('cv.show',['cvid' => $cv->id,'resid' => $cv->user_id])}}">{{$cv->user->first_name}} {{$cv->user->last_name}}</a></td>
                        <td>{{$cv->user->email}}</td>
                        <td>{{isset($cv->role->name)?$cv->role->name:''}}</td>
                        <td>{{isset($cv->role->phone)?$cv->role->phone:''}}</td>
                        <td>{{isset($cv_sent_at[$cv->id])?$cv_sent_at[$cv->id]->toDateString():null}}</td>
                        <td>
                            @if(isset($job_spec_ref[$cv->id]))
                                @foreach($job_spec_ref[$cv->id] as $key => $ref)
                                    {{isset($ref->jobSpec->reference_number)?(($key < count($job_spec_ref[$cv->id]) - 1 )?$ref->jobSpec->reference_number.' | ' : $ref->jobSpec->reference_number):null}}
                                @endforeach
                            @endif
                        </td>
                        <td class="text-right">
                            <a class="btn btn-sm btn-success" href="{{route('cvsubmit.show', ['cv' => $cv->id, 'w'=>isset($wishlist_id[$cv->id])?$wishlist_id[$cv->id]:null])}}" target="_blank"><i class="fa fa-paper-plane"></i>Submit a CV</a>&nbsp;
                            {{ Form::open(['method' => 'DELETE','route' => ['wishlist.destroy', $cv->id],'style'=>'display:inline','class'=>'delete']) }}
                            <input type="hidden" name="cv_wishlist_id" id="cv_wishlist_id" value="{{$cvWishList->id}}" />
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <td colspan="5">No CVs were added to this wishlist</td>
                @endforelse
            </table>
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {

        });
    </script>
@endsection