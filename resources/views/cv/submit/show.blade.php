@extends('adminlte.default')
@section('title') Selected CV @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <tbody>
                    <tr>
                        <td colspan="2">Name</td>
                        <td colspan="4">{{isset($cv->user->first_name)? $cv->user->first_name : ''}} {{isset($cv->user->last_name)? $cv->user->last_name : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Estimated Hourly Rate</td>
                        <td colspan="4">{{isset($estimated_hourly_rate) && $estimated_hourly_rate > 0? number_format($estimated_hourly_rate, 2, '.', ',') : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Hourly Rate Minimum</td>
                        <td colspan="4">{{isset($cv->hourly_rate_minimum) && $cv->hourly_rate_minimum > 0 ? number_format($cv->hourly_rate_minimum, 2, '.', ',') : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Houly Rate Maximum</td>
                        <td colspan="4">{{isset($cv->hourly_rate_maximum) && $cv->hourly_rate_maximum > 0 ? number_format($cv->hourly_rate_maximum, 2, '.', ',') : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Monthly Salary Minimum</td>
                        <td colspan="4">{{isset($cv->monthly_salary_minimum) && $cv->monthly_salary_minimum > 0 ? number_format($cv->monthly_salary_minimum, 2, '.', ',') : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Monthly Salary Maximum</td>
                        <td colspan="4">{{isset($cv->monthly_salary_maximum) && $cv->monthly_salary_maximum > 0 ? number_format($cv->monthly_salary_maximum, 2, '.', ',') : ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Last Availability Note</td>
                        <td colspan="4">{{$cv->note}}</td>
                    </tr>
                </tbody>
            </table>

            
            <table class="table table-bordered table-sm table-hover mt-2">
                <tbody>
                    <tr class="btn-dark">
                        <th>Status Date</th>
                        <th>Avail Status</th>
                        <th>Avail Date</th>
                        <th>Note</th>
                        <th>Rate</th>
                        <th>Month Rate</th>
                    </tr>
                    @if(isset($latest_availability))
                    <tr>
                        <td>{{isset($latest_availability->status_date) ? $latest_availability->status_date : ''}}</td>
                        <td>{{isset($availability_status->description) ? $availability_status->description : ''}}</td>
                        <td>{{isset($latest_availability->avail_date) ? $latest_availability->avail_date : ''}}</td>
                        <td>{{isset($latest_availability->avail_note) ? $latest_availability->avail_note : ''}}</td>
                        <td>{{isset($latest_availability->rate_hour) ? $latest_availability->rate_hour : ''}}</td>
                        <td>{{isset($latest_availability->rate_month) ? $latest_availability->rate_month : ''}}</td>
                    </tr>
                    @else
                    <tr>
                        <td class="text-center" colspan="6">No records to show</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td class="text-center">
                            {{--<button type="submit" class="btn btn-sm btn-success">Confirm</button>--}}
                            <a id="modal_btn" class="btn btn-sm btn-success" data-toggle="modal" data-target="#configuration_modal"><i class="fas fa-paper-plane"></i> Confirm</a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

        <!-- Start Block - Candidate email CV Modal -->
        @include('cv.submit.submit_cv_modal')
        <!-- End Block - Candidate email CV Modal -->

    </div>
@endsection
@section('extra-css')
<style>
    .loader {
        position: absolute;
        left: 40%;
        top: 30%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid rgba(137, 126, 122, 0.91);
        width: 140px;
        height: 140px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
@endsection
@section('extra-js')
    <script src="{!! global_asset('tinymce2/js/tinymce/tinymce.min.js') !!}"></script>
    <script>
        $(function () {

            let editor = tinymce.init({selector:'textarea#message'});

            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            });
        });

        function submitCV(){
            var job_spec_email = $('#job_spec_email').val();
            var job_spec_email_cc = $('#job_spec_email_cc').val();
            var job_spec_email_bcc = $('#job_spec_email_bcc').val();
            var cv_id = $('#cv_id').val();
            var job_spec_id = $('#job_spec_id').val();
            var rate = $('#customer_quote_hourly_rate').val();
            var ctc = $('#customer_quote_expected_salary').val();
            var template = $('#template').val();
            var format = $('#attachment').val();
            var text_message = $('#message').val();
            var w = $('#w').val();
            var cv_type = $('#cv_type').val();
            //var checkbox = $('#checkbox').val();

            var documents_array = [];
            $.each($("input[name='checkbox']:checked"), function(){
                documents_array.push($(this).val());
            });
            documents = documents_array.join("|");

            var checkbox = $( "input[name='checkbox']" ).val();
            var generate_system_cv = $('#generate_system_cv').val();

            let attachment_cv = '';

            if(job_spec_id == -1){
                alert("Please select the job spec.");
                return false;
            }

            if(job_spec_email == ""){
                alert("Please complete email field.");
                return false;
            }

            if((generate_system_cv == 0) && (documents == "")){
                alert("Please tick documents you would like to send or set Generate system CV to Yes.");
                return false;
            }

            var confirm_submit = confirm("Are you sure you want to submit this CV?");

            if(confirm_submit){

                $('#loader_div').show();
                $('#submit_cv_btn').attr("disabled", true);


                var data = {
                    cv_type: $('#template').val(),
                    cv_format: $('#attachment').val(),
                    named_cv: $('#job_spec_print_name').val()
                };

                axios.post('../../cv/generate/'+cv_id, { data })
                .then(response => {
                    attachment_cv = response.data.generated_document_name;

                    if(attachment_cv == ''){
                        alert('Error: CV could not be generated');
                        return false;
                    }

                    axios.post('/cvsubmit/submit', {
                        job_spec_email: job_spec_email,
                        job_spec_email_cc: job_spec_email_cc,
                        job_spec_email_bcc:job_spec_email_bcc,
                        cv_id: cv_id,
                        job_spec_id: job_spec_id,
                        rate: rate,
                        ctc: ctc,
                        template: template,
                        format: format,
                        text_message: text_message,
                        w: w,
                        checkbox: checkbox,
                        generate_system_cv: generate_system_cv,
                        'cv_type': cv_type,
                        'attachment_cv' : attachment_cv,
                        'attachments' : documents
                    })
                    .then(function (response) {

                        $('#loader_div').hide();
                        $('#submit_cv_btn').attr("disabled", false);
                        console.log(response);

                        alert(response.data.message);
                        $('#closeModalBtn').click();
                    })
                    .catch(function (error) {
                        $('#loader_div').hide();
                        $('#submit_cv_btn').attr("disabled", false);
                        alert("Error: There was a problem submiting this CV.");
                        console.log(error);
                    });











                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                    alert('Error: CV could not be sent.');
                });
















            }

        }

        $('#job_spec_id').change(function(){
            getCustomerEmail($('#job_spec_id').val());
        });

        $('#generate_system_cv').change(function(){
            let generate_system_cv = $('#generate_system_cv').val();
            if(generate_system_cv == 1){
                $('.generate_system_cv_class').show();
            }
            else{
                $('.generate_system_cv_class').hide();
            }
        });

        function getCustomerEmail(job_id){

            axios.get('/cvsubmit/getcustomeremail/'+job_id, {
                job_id: job_id
            })
            .then(function (response) {
                $('#job_spec_email').val(response.data.customer_email);
                $('#commission').val(response.data.commission);
            })
            .catch(function (error) {
                console.log(error.response);
            });

        }

    </script>
@endsection
