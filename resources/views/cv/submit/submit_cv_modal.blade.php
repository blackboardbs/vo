<!-- Modal -->
<div class="modal fade" id="configuration_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="configuration_label">Submit CV</h5>
                <button id="closeModalBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="cv_id" name="cv_id" value="{{$cv->id}}" />
                <input type="hidden" id="cv_type" name="cv_type" value="{{$cv_type}}" />
                {{--<input type="hidden" id="job_spec_id" name="job_spec_id" value="{{$_GET['j']}}" />--}}
                <div class='row'>
                    <div class="col-md-12">
                        <table class="table table-bordered table-sm table-hover">
                            <tbody>
                            <tr>
                                <td>Job Spec</td>
                                <td style="width: 50%;">
                                    {{Form::select('job_spec_id', $job_spec_drop_down, isset($_GET['j'])?$_GET['j']:-1,['class'=>'form-control form-control-sm'. ($errors->has('job_spec_id') ? ' is-invalid' : ''), 'id' => 'job_spec_id'])}}
                                    @foreach($errors->get('job_spec_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td style="width: 50%;">
                                    {{--<input id="job_spec_email" name="job_spec_email" type="email" style="width: 100%;" required />--}}
                                    {{Form::text('job_spec_email', null,['class'=>'form-control form-control-sm'. ($errors->has('job_spec_email') ? ' is-invalid' : ''), 'id' => 'job_spec_email'])}}
                                    @foreach($errors->get('job_spec_email') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Email cc: </td>
                                <td style="width: 50%;">
                                    <div class="input-group" style="display:flex !important;">
                                        {{Form::text('job_spec_email_cc', null,['class'=>'form-control form-control-sm'. ($errors->has('job_spec_email_cc') ? ' is-invalid' : ''), 'id' => 'job_spec_email_cc'])}}
                                        <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Separate emails by commas">
                                            <span class="input-group-text"><i class="fas fa-info"></i></span>
                                        </div>
                                    </div>
                                    {{--<input id="job_spec_email" name="job_spec_email" type="email" style="width: 100%;" required />--}}

                                    @foreach($errors->get('job_spec_email_cc') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Email bcc: </td>
                                <td style="width: 50%;">
                                    {{--<input id="job_spec_email" name="job_spec_email" type="email" style="width: 100%;" required />--}}
                                    <div class="input-group" style="display:flex !important;">
                                        {{Form::text('job_spec_email_bcc', null,['class'=>'form-control form-control-sm'. ($errors->has('job_spec_email_bcc') ? ' is-invalid' : ''), 'id' => 'job_spec_email_bcc'])}}
                                        <div class="input-group-append" data-toggle="tooltip" data-placement="top" title="Separate emails by commas">
                                            <span class="input-group-text"><i class="fas fa-info"></i></span>
                                        </div>
                                    </div>
                                    @foreach($errors->get('job_spec_email_bcc') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Customer quote hourly rate:</td>
                                <td style="width: 50%;">
                                    {{Form::text('customer_quote_hourly_rate', isset($cv->charge_out_rate) ?$cv->charge_out_rate : old('customer_quote_hourly_rate'),['class'=>'form-control form-control-sm'. ($errors->has('customer_quote_hourly_rate') ? ' is-invalid' : ''), 'id' => 'customer_quote_hourly_rate'])}}
                                    @foreach($errors->get('customer_quote_hourly_rate') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Placement Agreement %:</td>
                                <td style="width: 50%;">
                                    {{Form::text('commission', isset($latest_availability->commission) ? $latest_availability->rate_hour : '',['class'=>'form-control form-control-sm'. ($errors->has('commission') ? ' is-invalid' : ''), 'id' => 'commission'])}}
                                    @foreach($errors->get('commission') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Customer quote expected salary CTC:</td>
                                <td>
                                    {{Form::text('customer_quote_expected_salary', isset($latest_availability->rate_month) ? $latest_availability->rate_month : '',['class'=>'form-control form-control-sm'. ($errors->has('customer_quote_expected_salary') ? ' is-invalid' : ''), 'id' => 'customer_quote_expected_salary'])}}
                                    @foreach($errors->get('customer_quote_expected_salary') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Generate system CV:</td>
                                <td>
                                    {{Form::select('generate_system_cv', [0 => 'No', 1 => 'Yes'], 1, ['class'=>'form-control form-control-sm '. ($errors->has('generate_system_cv') ? ' is-invalid' : ''), 'id' => 'generate_system_cv'])}}
                                    @foreach($errors->get('generate_system_cv') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr class="generate_system_cv_class">
                                <td>CV Template:</td>
                                <td>
                                    {{Form::select('template', $cvTemplatesDropDown, isset($_GET['cv_type']) ? $_GET['cv_type'] : 2,['class'=>'form-control form-control-sm '. ($errors->has('template') ? ' is-invalid' : ''), 'id' => 'template'])}}
                                    @foreach($errors->get('template') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr class="generate_system_cv_class">
                                <td>Attachment format:</td>
                                <td>
                                    {{Form::select('attachment', [1 => 'PDF', 2 => 'MS Word'], isset($_GET['cv_format']) ? $_GET['cv_format'] : 2, ['class'=>'form-control form-control-sm '. ($errors->has('attachment') ? ' is-invalid' : ''), 'id' => 'attachment'])}}
                                    @foreach($errors->get('attachment') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr class="generate_system_cv_class">
                                <td>Print name:</td>
                                <td>
                                    {{Form::select('job_spec_print_name', [ 1 => 'Print Name', 2 => 'Do not Print Name'], isset($_GET['named_cv']) ? $_GET['named_cv'] : 1, ['class'=>'form-control form-control-sm '. ($errors->has('job_spec_print_name') ? ' is-invalid' : ''), 'id' => 'job_spec_print_name'])}}
                                    @foreach($errors->get('job_spec_print_name') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Select Documents:</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="table table-sm table-borderless">
                                        <tr>
                                            <td>Document Name</td><td>File Name</td><td>Upload Date</td><td>Uploaded By</td><td>Select</td>
                                        </tr>
                                        @forelse($documents as $document)
                                        <tr>
                                            <td>{{$document->name}}</td><td>{{$document->file}}</td><td>{{$document->created_at}}</td><td>{{$document->user->first_name}} {{$document->user->last_name}}</td><td><input type="checkbox" id="checkbox_{{$document->id}}" name="checkbox" value="{{$document->id}}"/></td>
                                        </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6">No documents uploaded</td>
                                            </tr>
                                        @endforelse
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Subject:</td>
                                <td>
                                    {{Form::text('subject', 'CV - '.$cv->user->first_name.' '.$cv->user->last_name, ['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''), 'id' => 'subject'])}}
                                    @foreach($errors->get('subject') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                    {{Form::hidden('w', (isset($_GET['w'])?$_GET['w']:null), ['id' => 'w'])}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea rows="7" cols="50" style="width: 100%;" id="message" name="message">
                                        <p>
                                            Please find CV of {{isset($cv->user->first_name)? $cv->user->first_name : ''}} {{isset($cv->user->last_name)? $cv->user->last_name : ''}} attached.
                                        </p>
                                        <p>
                                            {{isset($cv->user->first_name)? $cv->user->first_name : ''}} {{isset($cv->user->last_name)? $cv->user->last_name : ''}} will be available from {{isset($latest_availability->avail_date) ? $latest_availability->avail_date : ''}}.
                                        </p>
                                        <p>
                                            @if(isset($latest_availability->rate_hour))
                                            Rate is {{isset($latest_availability->rate_hour) ? 'R'.$latest_availability->rate_hour : ''}} per hour excluding TAX and expenses.
                                            @elseif(isset($latest_availability->rate_month))
                                            Expected Salary is {{isset($latest_availability->rate_month) ? 'R'.$latest_availability->rate_month : ''}} per month Cost to Company.
                                            @endif
                                        </p>
                                    </textarea>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button id="submit_cv_btn" onclick="submitCV()" class="btn btn-sm bg-dark d-print-none">Submit <i class="fas fa-paper-plane"></i></button>
                    </div>
                </div>
                <div id="loader_div" class="loader" style="display: none;"></div>
            </div>
        </div>
    </div>
</div>
