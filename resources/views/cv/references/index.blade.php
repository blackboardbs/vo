@extends('adminlte.default')

@section('title') Master Data - Profession @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('profession.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Profession</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>@sortablelink('description','Description')</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($professions as $profession)
                    <tr>
                        <td><a href="{{route('profession.show',$profession)}}">{{$profession->name}}</a></td>
                        <td class="text-right">
                            <a href="{{route('profession.edit',$profession)}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['method' => 'DELETE','route' => ['profession.destroy', $profession],'style'=>'display:inline','class'=>'delete']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No master data profession entries match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $professions->links() }}--}}
        </div>
    </div>
@endsection
