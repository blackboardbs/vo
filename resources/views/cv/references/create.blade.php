@extends('adminlte.default')
@section('title') Add CV Reference @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cvreference.store'), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'saveForm'])}}
            <input type="hidden" name="cv_id" id="cv_id" value="{{$cv_id}}" />
            <input type="hidden" name="cv_res_id" id="cv_res_id" value="{{$cv_res_id}}" />
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th>Name:</th>
                        <td>
                            {{Form::text('name', old('name'), ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name'])}}
                            @foreach($errors->get('name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Last Name:</th>
                        <td>
                            {{Form::text('last_name', old('last_name'), ['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''), 'id' => 'last_name'])}}
                            @foreach($errors->get('last_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>
                            {{Form::text('email', old('email'), ['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''), 'id' => 'email'])}}
                            @foreach($errors->get('email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Position:</th>
                        <td>
                            {{Form::text('position', old('position'), ['class'=>'form-control form-control-sm'. ($errors->has('position') ? ' is-invalid' : ''), 'id' => 'position'])}}
                            @foreach($errors->get('position') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                            {{Form::text('phone', old('phone'), ['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''), 'id' => 'phone'])}}
                            @foreach($errors->get('phone') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Company:</th>
                        <td>
                            {{Form::text('company', old('company'), ['class'=>'form-control form-control-sm'. ($errors->has('company') ? ' is-invalid' : ''), 'id' => 'company'])}}
                            @foreach($errors->get('company') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Validate Date:</th>
                        <td>
                            {{Form::text('validate_date', old('validate_date'), ['class'=>'datepicker form-control form-control-sm'. ($errors->has('validate_date') ? ' is-invalid' : ''), 'id' => 'validate_date'])}}
                            @foreach($errors->get('validate_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Note:</th>
                        <td>
                            {{Form::text('note', old('note'), ['class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : ''), 'id' => 'note'])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection