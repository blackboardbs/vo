@extends('adminlte.default')
@section('title') View CV Reference @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('cvreference.edit', $cv_reference)}}?cvid={{$cv_id}}&res_id={{$cv_res_id}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Reference</a>
            </div>
        </div>
        </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cvreference.update', $cv_reference->id), 'method' => 'put','class'=>'mt-3'])}}
            <input type="hidden" name="" id="{{$cv_id}}" value="" />
            <input type="hidden" name="" id="" value="{{$cv_res_id}}" />
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Name:</th>
                    <td>
                        {{Form::text('name', $cv_reference->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''), 'id' => 'name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Last Name:</th>
                    <td>
                        {{Form::text('last_name', $cv_reference->last_name, ['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''), 'id' => 'last_name', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('last_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>
                        {{Form::text('email', $cv_reference->email, ['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''), 'id' => 'email', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Position:</th>
                    <td>
                        {{Form::text('position', $cv_reference->position, ['class'=>'form-control form-control-sm'. ($errors->has('position') ? ' is-invalid' : ''), 'id' => 'position', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('position') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>
                        {{Form::text('phone', $cv_reference->phone, ['class'=>'form-control form-control-sm'. ($errors->has('phone') ? ' is-invalid' : ''), 'id' => 'phone', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Company:</th>
                    <td>
                        {{Form::text('company', $cv_reference->company, ['class'=>'form-control form-control-sm'. ($errors->has('company') ? ' is-invalid' : ''), 'id' => 'company', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('company') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Validate Date:</th>
                    <td>
                        {{Form::text('validate_date', $cv_reference->validate_date, ['class'=>'datepicker form-control form-control-sm'. ($errors->has('validate_date') ? ' is-invalid' : ''), 'id' => 'validate_date', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('validate_date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Note:</th>
                    <td>
                        {{Form::text('note', $cv_reference->note, ['class'=>'form-control form-control-sm'. ($errors->has('note') ? ' is-invalid' : ''), 'id' => 'note', 'disabled' => 'disabled'])}}
                        @foreach($errors->get('note') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection