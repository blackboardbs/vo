@extends('adminlte.default')

@section('title') CV @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
        <a href="{{route('cv.user')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> CV</a>
            <x-export route="cv.index"></x-export>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" action="{{route('cv.index')}}">
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('skill_level_id',$skill_level_dropdown,old('skill_level_id'),['class'=>'form-control search w-100', 'placeholder'=>'Select Skill Level', 'id' => 'skill_level_id'])}}
                        <span>Skill</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('avail_status_id',$availability_status_dropdown,old('avail_status_id'),['class'=>'form-control search w-100', 'placeholder'=>'Select Availability', 'id' => 'avail_status_id'])}}
                <span>Availability</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('rt',$resource_type_dropdown,old('rt'),['class'=>'form-control search w-100', 'placeholder'=>'Select Resource Type', 'id' => 'rt'])}}
                <span>Resource Type</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="qf" class="form-control w-100" value="{{isset($_GET['qf']) ? $_GET['qf'] : ''}}" onkeyup="handle(event)" />
                {{-- {{Form::text('qf',old('qf'),['class'=>'form-control search', 'style'=>'width: 100%;', 'id' => 'qf'])}} --}}
                <span>Qualification</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="e" class="form-control w-100" value="{{isset($_GET['e']) ? $_GET['e'] : ''}}" onkeyup="handle(event)" />
                {{-- {{Form::text('e',old('e'),['class'=>'form-control search', 'style'=>'width: 100%;', 'id' => 'e'])}} --}}
                <span>Experience</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                    <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm pt-3 row pr-0">
                <div class="col-md-12 pr-0">
                    <a href="{{route('cv.index')}}" class="btn btn-info w-100" type="submit">Clear Filters</a>
                </div>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('user.first_name','Name')</th>
                    <th>@sortablelink('user.email','Email')</th>
                    <th>@sortablelink('role','Role')</th>
                    <th>@sortablelink('quality','Quality')</th>
                    <th>@sortablelink('availability','Availability')</th>
                    <th>@sortablelink('user.phone','Phone')</th>
                    {{--<th>@sortablelink('res_position','Position')</th>
                    <th>@sortablelink('type.description','Type')</th>--}}
                    <th>@sortablelink('status.description','Status')</th>
                    @if($can_update)
                    <th class="last">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($cv as $consultant)
                    <tr>
                        <td><a href="{{route('cv.show',$consultant)}}">{{$consultant->user?->name()}}</a></td>
                        <td>{{$consultant->user?->email}}</td>
                        <td>{{$consultant->role?->name}}</td>
                        <td>{{$consultant->quality->count() != null && $consultant->quality->count() > 0?$consultant->quality[$consultant->quality->count() - 1]->quality_percentage.'%':''}}</td>
                        @php
                            $latest_date = null;
                            $date = date("Y-m-d");
                            foreach($consultant->availability as $availability):
                                if($latest_date == null){
                                    $latest_date = $availability->avail_date;
                                }
                                else{
                                    if(date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))){
                                        $latest_date = $availability->avail_date;
                                    }
                                }
                            endforeach;
                            $color = '#fff';

                            if(($latest_date != null) && date('Y-m-d') >= date('Y-m-d', strtotime($latest_date))){
                                $color = '#01FF70';
                            }

                            if(($latest_date != null) && date('Y-m-d', strtotime($date.'- 30 days')) >= date('Y-m-d', strtotime($latest_date))){
                                $color = 'orange';
                            }

                        @endphp
                        <td style="background-color: {{ $color }};">
                            @if($latest_date != null)
                                {{$latest_date}}
                            @endif
                        </td>
                        <td>{{isset($consultant->user->phone)?$consultant->user->phone:''}}</td>
                        {{--<td>{{$consultant->res_position}}</td>
                        <td>{{isset($consultant->type->description) ? $consultant->type->description : ''}}</td>--}}
                        <td>{{isset($consultant->status->description) ? $consultant->status->description : ''}}</td>
                        @if($can_update)
                        <td>
                            <div class="d-flex">
                            <a href="{{route('cv.edit',['cvid' => $consultant,'res_id' => $consultant->user_id])}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['cv.destroy', $consultant->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>

                        </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No cv's match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $cv->firstItem() }} - {{ $cv->lastItem() }} of {{ $cv->total() }}
                        </td>
                        <td>
                            {{ $cv->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
