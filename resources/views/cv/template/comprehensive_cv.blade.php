<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>CURRICULUM VITAE</title>
    <!-- <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'> -->
    <style>
        body{
            margin:0;font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.table,hr{margin-bottom:1rem}.container{width:98%;padding-right:7.5px;padding-left:7.5px;margin-right:auto;margin-left:auto}hr{margin-top:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.table-responsive{display:block;width:100%;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}.bg-dark,.bg-dark a{color:#fff!important}.p-2{padding:.5rem!important}.bg-dark{background-color:#343a40!important}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:7.5px;padding-left:7.5px}.row{display:flex;flex-wrap:wrap;margin-right:-7.5px;margin-left:-7.5px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}*,::after,::before{box-sizing:border-box}.text-right{text-align:right!important}.table{width:100%;max-width:100%;background-color:transparent}table{border-collapse:collapse}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}th{text-align:inherit}.table-responsive>.table-bordered{border:0}.table-bordered,.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}
        }

        .signature {
            font: 400 30px/0.8 'Great Vibes', Helvetica, sans-serif;
            color: #000000;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }

        .keep-together {
            page-break-inside: avoid;
        }

        .break-before {
            page-break-before: always;
        }

        .break-after {
            page-break-after: always;
        }

    </style>
</head>
<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        <div class="container">
            <table width="100%;">
                <tbody>
                    <tr>
                        <td style="text-align: center;">
                            <img style="width: 40%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; font-size: 27px; color: grey;">
                            Linking Your Career With Your Future!
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp</p>
            <h1 style="text-align: center; margin-top: 270px;">CURRICULUM VITAE</h1>
            <h2 style="text-align: center;">{{$preferred_name}} {{$last_name}}</h2>
            <h3 style="text-align: center;">{{$main_skill}}</h3>
            <h3 style="text-align: center;">{{$main_qualification}}</h3>
            <p>&nbsp</p>
            <p>&nbsp</p>
            <p>&nbsp</p>
            <p style="text-align: center; font-size: 20px;">Presented by {{$defaultCompanyName}}</p>
            <p>&nbsp</p>
            <p style="text-align: center; color: grey; margin-top: 350px;">
                This Curriculum Vitae has been prepared in accordance with the Labour Relations Act. Interview confirmation of a candidate from {{$defaultCompanyName}} by the client constitutes acceptance of {{$defaultCompanyName}} Terms and Conditions of business, full details available from {{$defaultCompanyName}}.
            </p>
            <p class="break-after">&nbsp;</p>
            <table>
                <tbody>
                <tr>
                    <td style="width: 70%;">&nbsp;</td>
                    <td>
                        <img style="width:100%;" src="{{public_path('assets'.DIRECTORY_SEPARATOR.'bb_logo.jpg')}}">
                    </td>
                </tr>
                </tbody>
            </table>
            <p style="text-align: left; font-size: 18px; background-color: gray;">Personal Details</p>
            <table style="width: 100%;" cellpadding="7">
                <tbody>
                    <tr>
                        <td style="width: 33%;">Surname</td>
                        <td style="width: 33%;">
                            {{$last_name}}
                        </td>
                        <td style="width: 34%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Full Name</td>
                        <td>
                            {{$preferred_name}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>
                            {{$gender}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Race</td>
                        <td>
                            {{$race}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>
                            {{$nationality}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td>
                            {{$DOB}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Identity Number</td>
                        <td>
                            {{$ID}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Driver's Licence</td>
                        <td>
                            {{$driversLicence}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Own Transport</td>
                        <td>
                            {{$ownTransport}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Disability</td>
                        <td>
                            {{$disability}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Languages</td>
                        <td>
                            {{$homeLanguage}}}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Current Location</td>
                        <td>
                            {{$currentLocation}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Marital Status</td>
                        <td>
                            {{$maritalStatus}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Dependants</td>
                        <td>
                            {{$dependants}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Notice Period</td>
                        <td>
                            {{$availableStatus.' '.$availableDate}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Current Salary CTC</td>
                        <td>

                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Current Salary (Nett)</td>
                        <td>
                            {{$currentSalaryNett}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Expected Salary</td>
                        <td>
                            {{$expectedSalaryNett}}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p style="font-size: 18px; background-color: gray;">About Yourself</p>
            <hr>
            <p>{{$overview}}</p>
            <p>&nbsp;</p>
            <p style="font-size: 18px; background-color: gray;">Scholastic Education</p>
            <hr>
            <table style="width: 100%;" cellpadding="7">
                <tbody>
                    <tr>
                        <td style="width: 40%;">School Matriculate at</td>
                        <td style="width: 5%;">
                            :
                        </td>
                        <td style="width: 55%;">{{$schoolMatriculated}}</td>
                    </tr>
                    <tr>
                        <td style="width: 40%;">Highest Grade / Standard</td>
                        <td style="width: 5%;">
                            :
                        </td>
                        <td style="width: 55%;">{{$highestSchoolGrade}}</td>
                    </tr>
                    <tr>
                        <td style="width: 40%;">Date</td>
                        <td style="width: 5%;">
                            :
                        </td>
                        <td style="width: 55%;">{{$schoolCompletionYear}}</td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p style="font-size: 18px; background-color: gray;">Tertiary Education</p>
            <hr>
            <table style="width: 100%;" cellpadding="7">
                <tbody>
                    @foreach($cv->qualification as $qualification)
                    <tr>
                        <td style="width: 30%;">
                            Institude
                        </td>
                        <td style="width: 60%;">
                            {{$qualification->institution}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Date
                        </td>
                        <td style="width: 60%;">
                            {{$qualification->year_complete}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Qualification
                        </td>
                        <td style="width: 60%;">
                            {{$qualification->qualification}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p style="font-size: 18px; background-color: gray;">Computer Literacy and Skills Matrix</p>
            <hr>
            <table style="width: 100%;" cellpadding="7">
                <tbody>
                <tr style="background-color: gray;">
                    <th style="width: 50%;">
                        Skill
                    </th>
                    <th style="width: 15%;">
                        Level
                    </th>
                    <th style="width: 15%;">
                        Experience
                    </th>
                </tr>
                @foreach($cv->skill as $skill)
                    <tr>
                        <td>
                            {{$skill->res_skill}}
                        </td>
                        <td>
                            {{isset($skill_level[$skill->skill_level])?$skill_level[$skill->skill_level]:''}}
                        </td>
                        <td>
                            {{$skill->years_experience}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <p>&nbsp;</p>

            <p style="font-size: 18px; background-color: gray;">Work Summary</p>
            <hr>
            @php
                $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            @endphp
            @foreach($cv->experience as $experience)
            @php
                            $tmpPeriodEnd = ($months[substr($experience->period_end, 4, 6)]??0).' '.substr($experience->period_end, 0, 4);
                            if($tmpPeriodEnd == '0 0'){
                                $tmpPeriodEnd = 'Current';
                            }
                        @endphp
            <table style="width: 100%;" cellpadding="7">
                <tbody>
                    <tr>
                        <td style="width: 30%;">
                            Company
                        </td>
                        <td style="width: 70%;">
                            {{$experience->company}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Period
                        </td>
                        <td style="width: 70%;">
                            {{$months[substr($experience->period_start, 4, 6)].' '.substr($experience->period_start, 0, 4)}} - {{$tmpPeriodEnd}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Role
                        </td>
                        <td style="width: 70%;">
                            {{$experience->role}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Responsibilities
                        </td>
                        <td style="width: 70%;">
                            {!! $experience->responsibility !!}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">
                            Tools
                        </td>
                        <td style="width: 70%;">
                            {{$experience->tools}}
                        </td>
                    </tr>
                    </tbody>
                </table>
            <p>&nbsp;</p>
            @endforeach
            <p style="font-size: 18px; background-color: gray;">Current / Previous Company</p>
            <hr>
            @foreach($cv->experience as $experience)
                <table style="width: 100%;" cellpadding="7">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">
                                Company
                            </td>
                            <td style="width: 70%;">
                                {{$experience->company}}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                Position
                            </td>
                            <td style="width: 70%;">
                                {{$experience->role}}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                Date
                            </td>
                            <td style="width: 70%;">
                                {{$experience->period_start}} - {{$experience->period_end}}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                Reason for Leaving
                            </td>
                            <td style="width: 70%;">
                                {{$experience->reason_for_leaving}}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">Duties and Responsibilities</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {{$experience->responsibility}}
                            </td>
                            <td colspan="2">
                                {{$experience->tools}}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
            @endforeach
            </div>
        </div>
    </body>
</html>
