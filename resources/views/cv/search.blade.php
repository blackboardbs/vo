@extends('adminlte.default')

@section('title') CV Search @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_view_wish_list)
        <a href="{{route('cvwishlist.index')}}" class="btn btn-dark btn-sm float-right" style="margin-left: 10px;"><i class="fa fa-list"></i> WishList</a>
        @endif
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="row">
            <div class="col-md-3">
                <form id="cv_search_form" name="cv_search_form" method="get" action="{{route('cv.search')}}">
                    <div class="accordion" id="accordionExample">
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading24">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse24" aria-expanded="true" aria-controls="collapse24">
                                        <span style="float: left;">Skills</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse24" class="collapse {{$skills_flag == true? 'show':''}}" aria-labelledby="heading24" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{--{{Form::text('ss', old('ss'),['class'=>'form-control form-control-sm chosen', 'style'=>'width: 100%;'])}}--}}
                                    {{Form::select('ss[]',$skills_drop_down, old('ss'),['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('ss') ? ' is-invalid' : ''), 'multiple'])}}
                                    @foreach($errors->get('ss') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingEight">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                        <span style="float: left;">Main Role</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseEight" class="collapse {{$main_role_flag == true ? 'show':''}}" aria-labelledby="headingEight" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('role[]',$role_drop_down,old('role_id'),['class'=>'form-control form-control-sm   col-sm-12'. ($errors->has('role_id') ? ' is-invalid' : ''), 'multiple'])}}
                                    @foreach($errors->get('role_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div><div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading11">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                        <span style="float: left;">Profession</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse11" class="collapse {{$profession_flag == true? 'show':''}}" aria-labelledby="heading11" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('p[]',$profession_drop_down, old('p'),['class'=>' form-control form-control-sm', 'id' => 'p', 'style'=>'width: 100%;', 'multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading12">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                        <span style="float: left;">Speciality</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse12" class="collapse {{$speciality_flag == true? 'show':''}}" aria-labelledby="heading12" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('sp[]',$speciality_drop_down, old('sp'),['class'=>' form-control form-control-sm', 'id' => 'sp', 'style'=>'width: 100%;', 'multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        <span style="float: left;">Resource Type</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseSeven" class="collapse {{$resource_type_flag == true? 'show':''}}" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('rt[]',$resource_type_drop_down,old('rt'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading25">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse25" aria-expanded="false" aria-controls="collapse25">
                                        <span style="float: left;">Placement Options</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse25" class="collapse {{$placement_options_flag == true? 'show':''}}" aria-labelledby="heading25" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('po[]',[-1 => 'All', 1 => 'Permanent', 2 => 'Temporary', 3 => 'Contracting'], old('po'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span style="float: left;">Availability</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse {{$availability_flag == true? 'show':''}}" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('a[]',$availability_drop_down,old('a'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <span style="float: left;">BBBEE Race</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse {{$bbbe_flag == true? 'show':''}}" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('bbe[]',$bbbee_race_drop_down,old('bbe'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <span style="float: left;">Experience Months</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse {{$experience_months_flag == true? 'show':''}}" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('em', $months_experince_drop_down, old('em'), ['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <span style="float: left;">Industry</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse {{$industry_flag == true? 'show':''}}" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('i[]',$industry_drop_down,old('i'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <span style="float: left;">Nationality</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse {{$nationality_flag == true? 'show':''}}" aria-labelledby="headingFive" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::text('n',null,old('n'),['class'=>'form-control form-control-sm', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <span style="float: left;">Qualification</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse {{$qualification_flag == true? 'show':''}}" aria-labelledby="headingSix" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::text('qf',null,old('qf'),['class'=>'form-control form-control-sm', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingNine">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                        <span style="float: left;">Skills Level</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseNine" class="collapse {{$skill_level_flag == true? 'show':''}}" aria-labelledby="headingNine" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('skl[]',$skill_drop_down,old('skl'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="headingTen">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                        <span style="float: left;">Disability</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTen" class="collapse {{$disability_flag == true? 'show':''}}" aria-labelledby="headingTen" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('d[]',[-1 => 'Select Disability', 1 => 'No', 2 => 'Yes'],old('d'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;','multiple'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading17">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                        <span style="float: left;">Hourly Rate Minimum</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse17" class="collapse {{$hourly_rate_minimum_flag == true? 'show':''}}" aria-labelledby="heading17" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('rm',$hourly_rate_drop_down, old('rm'),['class'=>' form-control form-control-sm', 'id' => 'rm', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading13">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                        <span style="float: left;">Hourly Rate Maximum</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse13" class="collapse {{$hourly_rate_maximum_flag == true? 'show':''}}" aria-labelledby="heading13" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('rx',$hourly_rate_drop_down, old('rx'),['class'=>' form-control form-control-sm', 'id' => 'rx', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading14">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                        <span style="float: left;">Monthly Salary Minimum</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse14" class="collapse {{$monthly_salary_minimum_flag == true? 'show':''}}" aria-labelledby="heading14" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('sm',$money_drop_down, old('sm'),['class'=>' form-control form-control-sm', 'id' => 'sm', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 15px !important;" class="card">
                            <div style="padding: 0px !important;" class="card-header" id="heading16">
                                <h5 class="mb-0">
                                    <button style="width: 100%; background-color: #cccc; color: #000;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                        <span style="float: left;">Monthly Salary Maximum</span> <i style="float: right;" class="fa fa-chevron-down"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse16" class="collapse {{$monthly_salary_maximum_flag == true? 'show':''}}" aria-labelledby="heading16" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{Form::select('sx',$money_drop_down, old('sx'),['class'=>' form-control form-control-sm', 'id' => 'sx', 'style'=>'width: 100%;'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search_button">
                        <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-search"></i> Search</button>&nbsp;&nbsp;
                        <a href="{{route('cv.search')}}" class="btn btn-danger btn-sm"><i class="fa fa-redo-alt"></i> Clear Filters</a>
                    </div>
                </form>
            </div>
            <div class="col-md-9">

                @if($cv->count() > 0)
                    <table id="advance_cv_table" class="table borderless table-sm">
                        <thead style="display: none;">
                            <tr>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($cv as $first_key => $consultant)
                            <tr>
                                <td>
                                    <table class="table borderless table-sm">
                                        <tbody>
                                            <tr>
                                                <th style="width: 20%; border: none;">Name</th>
                                                <th style="width: 40%; border: none;">Skill</th>
                                                <th style="width: 40%; border: none;">Main Qualification</th>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; border: none;">{{$consultant->user->first_name}} {{$consultant->user->last_name}}</td>
                                                <td style="width: 40%; border: none;">
                                                    @php
                                                        $is_content = false;
                                                        $skill_counter = 0;
                                                        $skills_lenth = sizeof($consultant->skill);
                                                    @endphp
                                                    @if(isset($consultant->skill))
                                                        @foreach($consultant->skill as $key => $value)
                                                            @if($skill_counter < 4)
                                                                @if(trim($value->res_skill) != '')
                                                                    @if($is_content)
                                                                        , {{ $value->res_skill }}
                                                                    @else
                                                                        {{ $value->res_skill }}
                                                                    @endif
                                                                    @php
                                                                        $is_content = true;
                                                                    @endphp
                                                                @endif
                                                            @else
                                                                @if($key == 4)
                                                                    <span id="read_more_dots_{{$first_key}}" style="display: block;">...</span><span style="display: none" id="read_more_{{$first_key}}">
                                                                @endif
                                                                {{--Start read more section--}}
                                                                @if(trim($value->res_skill) != '')
                                                                    @if($is_content)
                                                                        , {{ $value->res_skill }}
                                                                    @else
                                                                        {{ $value->res_skill }}
                                                                    @endif
                                                                    @php
                                                                        $is_content = true;
                                                                    @endphp
                                                                @endif
                                                                {{--End read more section--}}
                                                                @if($key == ($skills_lenth - 1))
                                                                    </span>
                                                                    <p></p><a id="read_more_btn_{{$first_key}}" style="cursor: pointer;" onclick="readMore({{$first_key}})">Read more</a></p>
                                                                @endif
                                                            @endif
                                                            @php
                                                                $skill_counter += 1;
                                                            @endphp
                                                        @endforeach
                                                        @php
                                                            $is_content = false;
                                                        @endphp
                                                    @endif
                                                </td>
                                                <td style="width: 40%; border: none;">
                                                    {{isset($consultant->main_qualification) ? $consultant->main_qualification : ''}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%; border: none;">Position</th>
                                                <th style="width: 40%; border: none;">Experience - Tool</th>
                                                <th style="width: 40%; border: none;">Availability</th>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; border: none;"></td>
                                                <td style="width: 40%; border: none;"></td>
                                                <td style="width: 40%; border: none;">
                                                    @if(isset($consultant->availability))
                                                        @foreach($consultant->availability as $key => $value)
                                                            {{ isset($value->avail_statusd->description) ? $value->avail_statusd->description : '' }} &nbsp;
                                                        @endforeach
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%; border: none;">Main Role</th>
                                                <th style="width: 40%; border: none;">Experience - Resposibility</th>
                                                <th style="width: 40%; border: none;">
                                                    &nbsp;
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; border: none;">
                                                    {{isset($consultant->role->name)?$consultant->role->name:''}}
                                                </td>
                                                <td style="width: 40%; border: none;"></td>
                                                <td style="width: 40%; border: none;">
                                                    <a href="{{route('cv.show',['cv' => $consultant->id,'resource' => $consultant->user_id])}}" class="btn btn-danger btn-sm"><i class="fa fa-id-card"></i> View Profile</a> <button type="submit" class="btn btn-success btn-sm" onclick="addToWishList({{$consultant->id}}, '{{$consultant->user->first_name.' '.$consultant->user->last_name}}')"><i class="fa fa-plus"></i> Wishlist</button>
                                                    @if(isset($_GET['j']) && $_GET['j'] > 0)
                                                        <a href="{{route('cvsubmit.show', $consultant->id)}}?j={{$_GET['j']}}" class="btn btn-sm btn-dark">Select CV</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>&nbsp;</p>
                                </td>
                            </tr>
                            {{--<hr>
                            <br/>--}}
                        @endforeach
                    @else
                        <div>No cv's match those criteria.</div>
                    @endif
                    </tbody>
                </table>
                {{-- Please do not enable the links as they break when filtering, till further notice --}}
                {{--{{ ( !isset($_GET['a']) && !isset($_GET['sk']) && !isset($_GET['bbe']) && !isset($_GET['n']) ) ? $cv->links() : '' }}--}}
            </div>
        </div>
    </div>

    <!-- Button trigger modal -->
    <button id="wishlist_modal_btn" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#wishlistModal" value="12" style="display: none;">
        <i class="fa fa-plus"></i> Wishlist
    </button>
    <!-- Modal -->
    <div class="modal fade" id="wishlistModal" tabindex="-1" role="dialog" aria-labelledby="wishlistModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="wishlistModalLabel">Add to Wishlist</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modal_message" style="color: green;">

                    </div>
                    <input type="hidden" name="cv_name_id" id="cv_name_id" value="" />
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                            <tr>
                                <td>Name:</td>
                                <td id="cv_wishlist_name"></td>
                            </tr>
                            <tr>
                                <td>WishList: </td>
                                <td>
                                    {{Form::select('cv_wishlist_id', $cvWishListDropDown, null,['class'=>'form-control form-control-sm '. ($errors->has('cv_wishlist_id') ? ' is-invalid' : ''),'id' => 'cv_wishlist_id'])}}
                                    @foreach($errors->get('cv_wishlist_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <span id="action_buttons">
                        <button onclick="saveToWishList()" type="button" class="btn btn-success btn-sm">Save</button>
                        <a href="{{route('cvwishlist.create')}}" type="button" class="btn btn-dark btn-sm">Add New WishList</a>
                    </span>
                    <button id="closeModalBtn" type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{!! asset('adminlte/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('adminlte/datatables/datatables.net-bs/css/datatables.mark.min.css') !!}">
    <style>
        .card-body {
            padding: 0rem;
        }

        .dataTables_info{
            width: 750px;
        }

        .search_button{
            position: fixed;
            bottom: 60px;
            left: 257px;
            color: #FFF;
            text-align: center;
        }

        .pagination{
            float: right !important;
        }

        .active{
            background-color: #0e90d2 !important;
        }

        .row{
            width: 100%;
        }

        .dataTables_length{
            float: left !important;
        }

        .dataTables_filter{
            float: right !important;
        }

        .content-wrapper .pagination>li.active>a {
            color: #fff !important;
            text-decoration: none;
        }

        .pagination>li.active>a {
            text-decoration: none;
            z-index: 1;
            color: #fff;
            background-color: #000000 !important;
            border-color: #000000;
        }

    </style>
@endsection
@section('extra-js')

    <!-- DataTables -->
    <script src="{!! asset('adminlte/datatables/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('adminlte/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('adminlte/datatables/datatables.net-bs/js/mark.js(jquery.mark.min.js)') !!}"></script>
    <script src="{!! asset('adminlte/datatables/datatables.net-bs/js/datatables.mark.js') !!}"></script>
    <script>
        $(function () {

            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');
            $('input').css('width', '100%');

            $('#advance_cv_table').DataTable({
                'paging'      : true,
                 'mark': true
                /*'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false*/
            });

            $('#p').change(function(){

                var profession_id = 0;

                var profession_ids = $('#p').val();

                axios.post('/cv/getspeciality/'+profession_id, {
                    profession_ids: profession_ids
                })
                .then(function (response) {

                var options_html = '<option>Please select</option>';
                $.each(response.data.speciality, function (key, value) {
                    options_html += '<option value='+key+'>'+value+'</option>';
                });

                    $('#sp').html(options_html);
                    $('#sp').trigger("chosen:updated");
                })
                .catch(function (error) {
                    console.log(error);
                });

            });

        });

        function addToWishList(cv_id, cv_name){

            $('#wishlist_modal_btn').click();
            $('#cv_wishlist_name').html(cv_name);
            $('#cv_name_id').val(cv_id);

        }

        function saveToWishList(){

            var cv_wishlist_id = $('#cv_wishlist_id').val();
            if(cv_wishlist_id == -1){
                alert("Please select or add a new wishlist.");
                return false;
            }

            var cv_name_id = $('#cv_name_id').val();

            axios.post('/wishlist/add', {
                cv_name_id: cv_name_id,
                cv_wishlist_id: cv_wishlist_id,
            })
            .then(function (response) {
                //console.log(response);
                //$('#action_buttons').html('');
                //$('#modal_message').html(response.data.message);
                alert(response.data.message);
                $('#closeModalBtn').click();

            })
            .catch(function (error) {
                console.log(error);
            });

        }

        function readMore(id){
            $('#read_more_'+id).toggle('slow');
            $('#read_more_dots_'+id).toggle('slow');

            if($('#read_more_btn_'+id).html() == 'Read more'){
                $('#read_more_btn_'+id).html('Read less');
            }
            else{
                $('#read_more_btn_'+id).html('Read more')
            }
        }

    </script>
@endsection