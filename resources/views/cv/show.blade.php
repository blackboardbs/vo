@extends('adminlte.default')
@section('title') View CV @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <table class="table table-borderless table-sm">
            <tr>
                <td style="width: 20%;">
                    {{Form::select('cv_template_id', $cvTemplatesDropDown, /*[2 => 'Standard Template', 3 => 'New Template', 4 => 'Comprehensive Template'],*/ $configDefaultCV, ['class'=>'form-control'. ($errors->has('cv_template_id') ? ' is-invalid' : ''), 'id' => 'cv_template_id'])}}
                </td>
                <td style="width: 15%;">
                    {{Form::select('cv_format_id', [1 => 'PDF', 2 => 'Microsoft Word'], 2, ['class'=>'form-control'. ($errors->has('cv_format_id') ? ' is-invalid' : ''), 'id' => 'cv_format_id'])}}
                </td>
                <td style="width: 15%;">
                    {{Form::select('named_cv_id', [1 => 'Print Name', 2 => 'Do not Print Name'], 1, ['class'=>'form-control'. ($errors->has('named_cv_id') ? ' is-invalid' : ''), 'id' => 'named_cv_id'])}}
                </td>
                <td>
                    {{ Form::open(['method' => 'GET','route' => ['cv.generate',  $cv->id],'style'=>'display:inline','class'=>'generate']) }}
                        <input type="hidden" value="{{$cv->id}}" name="cv_id" id="cv_id" />
                        <input type="hidden" value="{{$configDefaultCV}}" name="cv_type" id="cv_type" />
                        <input type="hidden" value="1" name="cv_format" id="cv_format" />
                        <input type="hidden" value="1" name="named_cv" id="named_cv" />
                        {{--<button type="submit" class="btn btn-success btn-sm ml-2 text-center" target="_blank"> Generate CV</button>--}}
                        <button id="generateCvButton" onclick="generate()" {{--href="{{route('cv.generate',  $cv->id)}}?cv_type=2"--}} class="btn btn-success ml-2 text-center" target="_blank"> Generate CV </button>
                    {{ Form::close() }}
                    {{ Form::open(['method' => 'GET','route' => ['cvsubmit.show', $cv->id],'style'=>'display:inline','class'=>'send']) }}
                        <input type="hidden" value="{{$configDefaultCV}}" name="cv_type" id="cv_type_new" />
                        <input type="hidden" value="1" name="cv_format" id="cv_format" />
                        <input type="hidden" value="1" name="named_cv" id="named_cv" />
                        <button class="btn btn-dark ml-1" target="_blank"> Send CV</button>
                    {{ Form::close() }}
                    <button class="btn btn-warning ml-1" target="_blank" style="color: #ffffff;" data-toggle="modal" data-target="#emailCVModal"> Email CV</button>
                    @if($scouting_id)
                        <a href="{{route('scouting.show', $scouting_id)}}" class="btn btn-dark ml-1" target="_blank"> Scouting</a>
                    @endif
                </td>
                <td>

                    @if($can_update)
                        <a href="{{route('cv.edit',['cvid' => $cv,'res_id' => $cv->user_id])}}" class="btn btn-success ml-2 float-right">Edit</a>
                    @endif
                    <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                </td>
            </tr>
        </table>
        {{--<button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#myModal"> Send CV</button>--}}
        <div id="download_link_div" class="col-md-12" style="border: 1px solid lightgrey; padding: 10px; border-radius: 4px; display: none;">
            <a id="download_link" href="" target="_blank">Download CV</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">

            @if($cvPersonalInformationModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">Personal Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowspan="7">Photo:</th>
                        <td rowspan="7">
                            <img src="{{route('user_avatar',['q'=>$cv->user->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/><br />

                            @foreach($errors->get('avatar') as $error)
                                <div class="invalid-feedback">
                                    {{ $error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Firstname:</th>
                        <td><span id="first_name">{{ $cv->user->first_name }}</span></td>
                    </tr>
                    <tr>
                        <th>Lastname:</th>
                        <td><span id="last_name">{{ $cv->user->last_name }}</span></td>
                    </tr>
                    <tr>
                        <th>Middle Name:</th>
                        <td>{{$cv->resource_middle_name}}</td>
                    </tr>
                    <tr>
                        <th>Prefered Name:</th>
                        <td>{{$cv->resource_pref_name}}</td>
                    </tr>
                    <tr>
                        <th>DOB:</th>
                        <td>{{$cv->date_of_birth}}</td>
                    </tr>
                    <tr>
                        <th>Place of birth:</th>
                        <td>{{$cv->place_of_birth}}</td>
                    </tr>
                    <tr>
                        <th>ID No:</th>
                        <td>{{$cv->id_no}}</td>
                    </tr>
                    <tr>
                        <th>Passport:</th>
                        <td>{{$cv->passport_no}}</td>
                        <th>Nationality:</th>
                        <td>{{$cv->nationality}}</td>
                    </tr>
                    <tr>
                        <th>Marital Status:</th>
                        <td>{{($cv->marital_status != null) ? $cv->marital_status->description : ''}}</td>
                        <th>Health:</th>
                        <td>{{$cv->health}}</td>
                    </tr>
                    <tr>
                        <th>Home Language:</th>
                        <td>{{$cv->home_language}}</td>
                        <th>Other Language:</th>
                        <td>{{$cv->other_language}}</td>
                    </tr>
                    <tr>
                        <th>Criminal Record:</th>
                        <td>{{$cv->criminal_offence}}</td>
                        <th>Drivers License:</th>
                        <td>{{$cv->drivers_license}}</td>
                    </tr>
                    <tr>
                        <th>Current Location:</th>
                        <td>{{Form::text('current_location', $cv->current_location, ['class'=>'form-control form-control-sm'. ($errors->has('current_location') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('current_location') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Own Transport:</th>
                        <td>
                            {{Form::select('own_transport', [0 => 'No', 1 => 'Yes'], $cv->own_transport,['class'=>'form-control form-control-sm'. ($errors->has('own_transport') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('own_transport') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Dependants:</th>
                        <td>{{Form::select('dependants', $number_drop_down, $cv->dependants,['class'=>'form-control form-control-sm'. ($errors->has('dependants') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('dependants') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Disability:</th>
                        <td>
                            @if($cv->disabilityd != null){{$cv->disabilityd->description}}@endif
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Scholastic Education</th>
                    </tr>
                    <tr>
                        <th>School Matriculated:</th>
                        <td>{{Form::text('school_matriculated',$cv->school_matriculated,['class'=>'form-control form-control-sm'. ($errors->has('school_matriculated') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('school_matriculated') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Higher Grade:</th>
                        <td>
                            {{Form::text('highest_grade',$cv->highest_grade,['class'=>'form-control form-control-sm'. ($errors->has('highest_grade') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('highest_grade') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>School Completion Year:</th>
                        <td>{{Form::text('school_completion_year',$cv->school_completion_year,['class'=>'form-control form-control-sm'. ($errors->has('school_completion_year') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('school_completion_year') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Position Information</th>
                    </tr>
                    <tr>
                        <th>Main Qualification:</th>
                        <td>{{$cv->main_qualification}}</td>
                        <th>Main Role:</th>
                        <td>
                            {{Form::select('role_id',$role_dropdown, $cv->role_id, ['class'=>'form-control form-control-sm'. ($errors->has('role_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('role_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contract House</th>
                        <td>
                            {{Form::select('vendor_id',$vendor_drop_down, $cv->vendor_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('vendor_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('vendor_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Placement Options</th>
                        <td>
                            <input type="checkbox" name="permanent" id="permanent" value="1" {{$cv->is_permanent == 1 ? 'checked':''}} disabled="disabled" /> Permanent <input type="checkbox" name="contracting" id="contracting" value="1" {{$cv->is_contract == 1 ? 'checked':''}} disabled="disabled" /> Contracting <input type="checkbox" name="temporary" id="temporary" value="1" {{$cv->is_temporary == 1 ? 'checked':''}} disabled="disabled" /> Temporary
                        </td>
                    </tr>
                    <tr>
                        <th>Profession</th>
                        <td>
                            {{Form::select('profession_id',$profession_drop_down, $cv->profession_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('profession_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('profession_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Speciality</th>
                        <td>
                            {{Form::select('speciality_id',$speciality_drop_down, $cv->speciality_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'disabled' => 'disabled'])}}
                            @foreach($errors->get('speciality_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Hourly Rate Minimum:</th>
                        <td>
                            {{Form::text('hourly_rate_minimum',$cv->hourly_rate_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('hourly_rate_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Hourly Rate Maximum:</th>
                        <td>
                            {{Form::text('hourly_rate_maximum',$cv->hourly_rate_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('hourly_rate_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Monthly Salary Minimum</th>
                        <td>
                            {{Form::text('monthly_salary_minimum',$cv->monthly_salary_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('monthly_salary_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Monthly Salary Maximum:</th>
                        <td>
                            {{Form::text('monthly_salary_maximum',$cv->monthly_salary_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum', 'disabled'=>'disabled'])}}
                            @foreach($errors->get('monthly_salary_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Current Salary (Nett):</th>
                        <td>{{Form::text('current_salary_nett',$cv->current_salary_nett,['class'=>'form-control form-control-sm'. ($errors->has('current_salary_nett') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('current_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Expected Salary (Nett):</th>
                        <td>{{Form::text('expected_salary_nett',$cv->expected_salary_nett,['class'=>'form-control form-control-sm'. ($errors->has('expected_salary_nett') ? ' is-invalid' : ''),'disabled'=>'disabled'])}}
                            @foreach($errors->get('expected_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Contact Information</th>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>{{isset($cv->user->phone)?$cv->user->phone:''}}</td>
                        <th>Cell:</th>
                        <td>{{$cv->cell}}</td>
                    </tr>
                    <tr>
                        <th>Cell 2:</th>
                        <td>{{$cv->cell2}}</td>
                        <th>Fax:</th>
                        <td>{{$cv->fax}}</td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>{{isset($cv->user->email)?$cv->user->email:''}}</td>
                        <th>Personal Email:</th>
                        <td>{{$cv->personal_email}}</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">BBBEE Information</th>
                    </tr>
                    <tr>
                        <th>Naturalization</th>
                        <td>@if($cv->naturalizationd != null){{$cv->naturalizationd->description}}@endif</td>
                        <th>Race</th>
                        <td>@if($cv->raced != null){{$cv->raced->description}}@endif</td>
                    </tr>
                    <tr>
                        <th>BBBEE Race</th>
                        <td>@if($cv->bbbee_raced != null){{$cv->bbbee_raced->description}}@endif</td>
                        <th>Gender</th>
                        <td>@if($cv->genderd != null){{$cv->genderd->description}}@endif</td>
                    </tr>
                    <tr>
                        <th>Disability</th>
                        <td>@if($cv->disabilityd != null){{$cv->disabilityd->description}}@endif</td>
                        <th>Charge Out Rate</th>
                        <td>{{$cv->charge_out_rate}}</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Overview</th>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>{{$cv->status->description}}</td>
                        <th>
                            @if(isset($cv->scouting_id) && $cv->scouting_id > 0)
                            <a href="{{route('scouting.show', $cv->scouting_id)}}" >View Scouting Record</a>
                            @endif
                        </th>
                        <td>
                            @if(isset($cv->scouting_id) && $cv->scouting_id > 0)
                            <a href="{{route('scouting.show', $cv->scouting_id)}}" >View Scouting Record</a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Overview Text:</th>
                        <th colspan="3">
                            {{Form::textarea('note',isset($cv->note)?$cv->note:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Note',  'disabled' => 'disabled'])}}
                        </th>
                    </tr>
                </tbody>
            </table>
            @endif

            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="5" class="btn-dark">Documents</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Document</th>
                        <th>File Name</th>
                        <th>Upload Date</th>
                        <th>Uploaded by</th>
                        <th class="last">Action</th>
                    </tr>
                    @forelse($documents as $document)
                        <tr>
                            <td>{{ Html::link($document->file, $document->name) }}</td>
                            <td>{{ $document->file }}</td>
                            <td>{{$document->created_at}}</td>
                            <td>{{isset($document->user->first_name)?$document->user->first_name:''}} {{isset($document->user->last_name)?$document->user->last_name:''}}</td>
                            <td>
                                <div class="d-flex">
                                <a href="{{route('editdocument', $document->id)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE', 'route' => ['deletedocument', $document->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">No documents found.</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="8" style="text-align: center">
                            <a href="{{route('uploaddocument')}}?cvid={{$cv->id}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Document</a>
                        </td>
                    </tr>
                </tbody>
            </table>

            @if($cvAvalabilityModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="8">
                            Availability
                            @if($canCreateCvAvalability)
                            <a href="{{route('availability.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Availability</a>
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Status Date</th>
                        <th>Avail Status</th>
                        <th>Avail Date</th>
                        <th>Note</th>
                        <th>Rate</th>
                        <th>Month Rate</th>
                        <th>Rec Status</th>
                       <th class="last">Action</th>
                    </tr>
                    @forelse($availability as $results)
                        <tr>
                            <td><a href="{{route('availability.show',$results)}}?cvid={{$cv->id}}&res_id={{$results->res_id}}">{{$results->status_date}}</a></td>
                            <td>{{$results->avail_statusd->description}}</td>
                            <td>{{$results->avail_date}}</td>
                            <td>{{$results->avail_note}}</td>
                            <td>{{$results->rate_hour}}</td>
                            <td>{{$results->rate_month}}</td>
                            <td>{{$results->status->description}}</td>
                            <td>
                                <div class="d-flex">
                                @if($canUpdateCvAvalability)
                                <a href="{{route('availability.edit',['availability' => $results,'cv' => $cv,'resource' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['availability.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                @endif
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8">No records to show</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="8" style="text-align: center">
                            @if($canCreateCvAvalability)
                            <a href="{{route('availability.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Availability</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="8">
                            Activity
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Activity</th>
                        <th>Action Date</th>
                        <th>Actioned By</th>
                    </tr>
                    @forelse($activities as $activity)
                        <tr>
                            <td><a href="">{{$activity->description}}</a></td>
                            <td>{{$activity->created_at}}</td>
                            <td>{{isset($activity->causer->first_name)?$activity->causer->first_name:''}} {{isset($activity->causer->last_name)?$activity->causer->last_name:''}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8">No activities to show</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            @if($cvSkillModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="6">
                            Skill
                            @if($canCreateCvSkill)
                            <a href="{{route('skill.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Skill</a>
                            @endif
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Skill</th>
                        <th>Tool</th>
                        <th>Level</th>
                        <th>Years</th>
                        <th>Status</th>
                        <th class="last">Action</th>
                    </tr>
                    @forelse($skill as $results)
                        <tr>
                            <td><a href="{{route('skill.show',['skill' => $results, 'cv' => $cv->id])}}"> {{ $results->res_skill }} </a> </td>
                            <td>{{($results->tool_id > 0 ? $results->tool_idd->description : '')}}</td>
                            <td>{{($results->skill_level > 0 ? $results->skill_leveld->description : '')}}</td>
                            <td>{{$results->years_experience}}</td>
                            <td>{{$results->status->description}}</td>
                            <td>
                                <div class="d-flex">
                                @if($canUpdateCvSkill)
                                <a href="{{route('skill.edit',['skill' => $results, 'cv' => $cv,'resource' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['skill.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                @endif
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">No records to show</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="6" style="text-align: center">
                            @if($canCreateCvSkill)
                            <a href="{{route('skill.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Skill</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvQualificationModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="5">
                            Qualification
                            @if($canCreateCvQualification)
                            <a href="{{route('qualification.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Qualification</a>
                            @endif
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Qualification</th>
                        <th>Institution</th>
                        <th>Year Complete</th>
                        <th>Status</th>
                        <th class="last">Action</th>
                    </tr>
                    @forelse($qualification as $results)
                        <tr>
                            <td><a href="{{route('qualification.show',['qualification' => $results])}}?cvid={{$cv->id}}">{{$results->qualification}}</td>
                            <td>{{$results->institution}}</td>
                            <td>{{$results->year_complete}}</td>
                            <td>{{$results->status->description}}</td>
                            <td>
                                <div class="d-flex">
                                @if($canUpdateCvQualification)
                                <a href="{{route('qualification.edit',['qualificationid' => $results, 'cvid' => $cv,'res_id' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['qualification.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                @endif
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" style="text-align: center;">No records to show</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="5" style="text-align: center">
                            @if($canCreateCvQualification)
                            <a href="{{route('qualification.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Qualification</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvExpenseModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">Experience
                            <a href="{{route('experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Experience</a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    @endphp
                    @forelse($experience as $results)
                        @php
                            $tmpPeriodEnd = ($months[substr($results->period_end, 4, 6)]??0).' '.substr($results->period_end, 0, 4);
                            if($tmpPeriodEnd == '0 0'){
                                $tmpPeriodEnd = 'Current';
                            }
                        @endphp
                        <tr>
                            <th>Period</th>
                            <td><a href="{{route('experience.show',$results)}}?cvid={{$cv->id}}">{{$months[substr($results->period_start, 4, 6)].' '.substr($results->period_start, 0, 4)}} - {{$tmpPeriodEnd}}</a></td>
                            <td rowspan="7">@if(isset($results->company_id) && $results->company_id > 0) <img src="{{route('cv_company_logo',['q'=>$results->cv_companyd->logo])}}" id="company-preview-large" class="company-avatar company-avatar-profile" /> @else <img src="{{route('company_avatar',['q'=>'default.jpg'])}}" id="company-preview-large" class="company-avatar company-avatar-profile" /> @endif</td>
                            <td rowspan="7" class="last">
                                <div class="d-flex">
                                <a href="{{route('experience.edit',['experienceid' =>$results,'cvid' => $cv,'res_id' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => ['experience.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Company</th>
                            <td>{{($results->company != null ? $results->company : $results->cv_companyd->description)}}</td>
                        </tr>
                        <tr>
                            <th>Project</th>
                            <td>{{$results->project}}</td>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <td>{{$results->role}}</td>
                        </tr>
                        <tr>
                            <th>Responsibility</th>
                            <td>{{$results->responsibility}}</td>
                        </tr>
                        <tr>
                            <th>Tools</th>
                            <td>{{$results->tools}}</td>
                        </tr>
                        <tr>
                            <th>Reason for leaving</th>
                            <td>{{$results->reason_for_leaving}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" style="text-align: center;">No records to show</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="5" style="text-align: center">
                            <a href="{{route('experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Experience</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvIndustryExperienceModuleCanView)
            {{Form::open(['url' => route('industry_experience.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off'])}}
            <input type="hidden" name="cv_id" id="cv_id" value="{{$cv->id}}" />
            <input type="hidden" name="res_id" id="res_id" value="{{$cv->user_id}}" />
            <input type="hidden" name="status" id="status" value="1" />
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="2">
                            Industry Experience
                            @if($canCreateCvIndustryExperience)
                            <a href="{{route('industry_experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa {{empty($industry_experience_ids)?'fa-plus':'fa-pen'}}"></i> Industry Experience</a>
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {{Form::select('ind_id[]',$industry_experience_dropdown, $selected_industry_eperiencies,['class'=>'form-control form-control-sm '. ($errors->has('ind_id') ? ' is-invalid' : ''),'id'=>'ind_id', 'multiple'])}}
                            @foreach($errors->get('ind_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td width="130" class="text-right">
                            @if($canUpdateCvIndustryExperience)
                            <button type="submit" class="btn btn-success btn-sm">Update</button>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
            @endif

            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="8">
                            References
                            <a href="{{route('cvreference.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> References</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Position</td>
                        <td>Phone</td>
                        <td>Company</td>
                        <td>Date</td>
                        <td>Note</td>
                        <td class="last">Action</td>
                    </tr>
                    @forelse($cv_references as $cv_reference)
                    <tr>
                        <td><a href="{{route('cvreference.show', $cv_reference)}}?cvid={{$cv->id}}&res_id={{$cv->user_id}}">{{$cv_reference->name}}</a></td>
                        <td>{{$cv_reference->email}}</td>
                        <td>{{$cv_reference->position}}</td>
                        <td>{{$cv_reference->phone}}</td>
                        <td>{{$cv_reference->company}}</td>
                        <td>{{$cv_reference->validate_date}}</td>
                        <td>{{$cv_reference->note}}</td>
                        <td class="text-right">
                            <div class="d-flex">
                            <a href="{{route('cvreference.edit',['cvreference' => $cv_reference,'cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['cvreference.destroy', $cv_reference->id],'style'=>'display:inline','class'=>'delete']) }}
                            <input type="hidden" id="cv_id" name="cv_id" value="{{$cv->id}}" />
                            <input type="hidden" id="res_id" name="res_id" value="{{$cv->user_id}}" />
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="8" style="text-align: center;">No records to show</td>
                        </tr>
                    @endforelse
                    <tr>
                        <td colspan="8" style="text-align: center">
                            <a href="{{route('cvreference.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> References</a>
                        </td>
                    </tr>
                </tbody>
            </table>

            @if($cvQualificationModuleCanView)
            {{Form::open(['url' => route('cvquality.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off'])}}
            <input type="hidden" name="cvq_id" id="cvq_id" value="{{$cv->id}}"/>
            <input type="hidden" name="resq_id" id="resq_id" value="{{$cv->user_id}}"/>
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="6">
                        CV Quality
                    </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 20%;%">Intorduction Letter Submitted</td>
                        <td style="width: 10%;%"><input type="checkbox" name="introduction_letter" id="introduction_letter" value="1"  {{isset($cv_quality->introduction_letter) && $cv_quality->introduction_letter == 1 ? 'checked':''}}/></td>
                        <td style="width: 20%;%">{{isset($cv_quality->introduction_letter_date)?$cv_quality->introduction_letter_date:''}}</td>
                        <td style="width: 20%;%"><a class="btn btn-dark btn-sm" onclick="sendIntroductionLetter()">Send</a></td>
                        <td style="width: 20%;%">CV Quality</td>
                        <td style="width: 10%;%"><span style="border-color: red; color: {{$quality_percentage < 50 ? 'red' : ''}} {{$quality_percentage >= 50 && $quality_percentage < 80 ? 'orange' : ''}} {{$quality_percentage >= 80 ? 'green' : ''}}; font-weight: bold;">{{isset($quality_percentage)?$quality_percentage:'0'}}%</span></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;%">Terms and Conditions Accepted</td>
                        <td style="width: 10%;%"><input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" value="1"  {{isset($cv_quality->terms_and_conditions) && $cv_quality->terms_and_conditions == 1 ? 'checked':''}}/></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 10%;%"></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;%">Criminal Record Check(if required)</td>
                        <td style="width: 10%;%"><input type="checkbox" name="criminal_record" id="criminal_record" value="1" {{isset($cv_quality->criminal_record) && $cv_quality->criminal_record == 1 ? 'checked':''}}/></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 20%;%"></td>
                        <td style="width: 10%;%"></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;%">Credit Check(if required)</td>
                        <td style="width: 10%;%"><input type="checkbox" name="credit_check" id="credit_check" value="1"  {{isset($cv_quality->credit_check) && $cv_quality->credit_check == 1 ? 'checked':''}}/></td>
                        <td colspan="4">
                            {{Form::text('result_text',isset($cv_quality->result_text)?$cv_quality->result_text:'',['class'=>'form-control form-control-sm'. ($errors->has('result_text') ? ' is-invalid' : ''),'placeholder' => 'Result text'])}}
                            @foreach($errors->get('result_text') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="text-center">
                            <button type="submit" class="btn btn-dark btn-sm">Save</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
            @endif

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                {{Form::open(['url' => route('cv.send', $cv->id), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off'])}}
                <div class="modal-header">
                    <h4 class="modal-title">Send CV</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-sm mt-3">
                        <tbody>
                        <tr>
                            <td>Subject:</td>
                            <td>
                                {{Form::text('subject',$subject,['class'=>'datepicker form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''),'placeholder' => 'Subject'])}}
                                @foreach($errors->get('subject') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Meassage:</td>
                            <td>
                                {{Form::textarea('message',old('message'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12', 'id' => 'message'])}}
                                @foreach($errors->get('message') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                {{Form::text('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder' => 'Email'])}}
                                @foreach($errors->get('email') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <!-- Start Block - Candidate email CV Modal -->
    @include('cv.email.email_modal')
    <!-- End Block - Candidate email CV Modal -->

@endsection

@section('extra-js')
    <script src="{!! global_asset('tinymce2/js/tinymce/tinymce.min.js') !!}"></script>

    <script>
        $(function(){
            let editor = tinymce.init({selector:'textarea#email_body_md'});
        })

        $('#cv_template_id').on('change', function(){
           $('#cv_type').val($(this).val());
           $('#cv_type_new').val($(this).val());
           $('#candidate_cv_template').val($(this).val());
        });

        $('#cv_format_id').on('change', function(){
           $('#cv_format').val($(this).val());
            $('#email_cv_format').val($(this).val());
        });

        $('#named_cv_id').on('change', function(){
           $('#named_cv').val($(this).val());
            $('#candidate_cv_print_name').val($(this).val());
        });

        $(function () {

            axios.post('/get_user', {
                user_id: '{!! $cv->user_id !!}'
            })
            .then(function (response) {
                $('#first_name').text(response.data.first_name);
                $('#last_name').text(response.data.last_name);
                $('#email').text(response.data.email);
                $('#phone').text(response.data.phone);
            })
            .catch(function (error) {
                console.log(error);
            });
        });

        function generateCV(cvid,userid){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let templateid = $('#cv_template_id').val();
            $.ajax({
                url: '/cv/' + cvid + '/' + userid + '/download/' + templateid,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var a = document.createElement('a');
                    var url = '/storage/template/' + '?q=' + data.file;
                    a.href = url;
                    a.download = data.file.replace(/^.*[\\\/]/, '');
                    a.click();
                    window.URL.revokeObjectURL(url);
                }
            });
        }

        function submitTemplateCV(template_type, cv_id, name_type){
            var template_id = $('#cv_template_id').val();
            window.open("../../../template/"+template_type+"/previewtemplate/"+template_id+"/"+cv_id+"/"+name_type, "_blank");
        }


        function goBack() {
            window.history.back();
        }

        function sendIntroductionLetter() {
            console.log("Sending Introduction letter");
        }

        function generate(){

            $('#generateCvButton').html('Generating CV ...');
            $('#generateCvButton').attr('disabled', true);
            $('#download_link_div').hide();
            let cv_id = {!! $cv->id !!};
            let cv_type = $('#cv_template_id').val();
            let cv_format = $('#cv_format_id').val();
            let named_cv = $('#named_cv_id').val();

            var data = {
                cv_type: $('#cv_template_id').val(),
                cv_format: $('#cv_format_id').val(),
                named_cv: $('#named_cv_id').val()
            };

            console.log(cv_id);
            console.log(data);

            axios.post('../../cv/generate/'+cv_id, { data })
            .then(function (response) {
                $('#generateCvButton').html('Generate CV');
                $('#generateCvButton').attr('disabled', false);
                console.log('Success');
                console.log(response);
                $('#download_link').attr('href', response.data.document_name);
                $('#download_link_div').show();
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        function emailCV(){
            let editor = tinymce.get('email_body_md');

            $('#candidate_loader_div').show();
            $('#email_cv_btn').attr('disabled', true);
            let cv_id = {!! $cv->id !!};

            var data = {
                cv_type: $('#candidate_cv_template').val(),
                cv_format: $('#email_cv_format').val(),
                named_cv: $('#candidate_cv_print_name').val()
            };

            axios.post('../../cv/generate/'+cv_id, { data })
            .then(function (response) {
                $('#email_cv_btn').attr('disabled', false);
                console.log('Success');
                console.log(response);

                let email_data = {
                  name: $('#candidate_prefered_name').val(),
                  to: $('#candidate_email').val(),
                  cc: $('#candidate_email_cc').val(),
                  bcc: $('#candidate_email_bcc').val(),
                  subject: $('#subject').val(),
                  mail_body: editor.getContent(),
                  attachment: response.data.generated_document_name,
                };
                axios.post('../../cv/emailcandidate', { email_data })
                .then(response => {
                    $('#candidate_loader_div').hide();
                    $('#closeCandidateModalBtn').click();
                    console.log(response);
                    alert(response.data.message);
                })
                .catch(function(){
                    alert('Error: CV could not be sent.');
                });
            })
            .catch(function (error) {
                console.log(error);
                alert('Error: CV could not be sent.');
            });

            //$('#email_body').val(editor.getContent());
        }

    </script>
@endsection
