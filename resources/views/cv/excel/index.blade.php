<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Quality</th>
        <th>Availability</th>
        <th>Phone</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cv as $consultant)
        <tr>
            <td>{{$consultant->user?->name()}}</td>
            <td>{{$consultant->user?->email}}</td>
            <td>{{$consultant->role?->name}}</td>
            <td>{{$consultant->quality->count() != null && $consultant->quality->count() > 0?$consultant->quality[$consultant->quality->count() - 1]->quality_percentage.'%':''}}</td>
            @php
                $latest_date = null;
                $date = date("Y-m-d");
                foreach($consultant->availability as $availability):
                    if($latest_date == null){
                        $latest_date = $availability->avail_date;
                    }
                    else{
                        if(date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))){
                            $latest_date = $availability->avail_date;
                        }
                    }
                endforeach;
            @endphp
            <td>
                @if($latest_date != null)
                    {{$latest_date}}
                @endif
            </td>
            <td>{{$consultant->user?->phone}}</td>
            <td>{{$consultant->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>