@extends('adminlte.default')
@section('title') Add CV @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createCv')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cv.store'), 'method' => 'post','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'createCv'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">Personal Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowspan="5">Photo:</th>
                        <td rowspan="5">
                            <img src="{{route('user_avatar',['q'=>$user->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/><br />
                        </td>
                        <th>First Name:</th>
                        <td>
                            <span id="first_name"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>Last Name:</th>
                        <td>
                            <span id="last_name"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>Preferred Name:</th>
                        <td>{{Form::text('resource_pref_name',old('resource_pref_name'),['class'=>'form-control form-control-sm'. ($errors->has('resource_pref_name') ? ' is-invalid' : ''),'placeholder' => 'Preferred Name'])}}
                            @foreach($errors->get('resource_pref_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Middle Name:</th>
                        <td>{{Form::text('resource_middle_name',old('resource_middle_name'),['class'=>'form-control form-control-sm'. ($errors->has('resource_middle_name') ? ' is-invalid' : ''),'placeholder'=>'Middle Name'])}}
                            @foreach($errors->get('resource_middle_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>ID No:</th>
                        <td>{{Form::text('id_no',old('id_no'),['class'=>'form-control form-control-sm'. ($errors->has('id_no') ? ' is-invalid' : ''),'placeholder'=>'ID No'])}}
                            @foreach($errors->get('id_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Date Of Birth:</th>
                        <td>{{Form::text('date_of_birth',old('date_of_birth'),['class'=>'form-control form-control-sm datepicker'. ($errors->has('date_of_birth') ? ' is-invalid' : ''),'placeholder'=>'Date Of Birth'])}}
                            @foreach($errors->get('date_of_birth') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Place Of Birth</th>
                        <td>{{Form::text('place_of_birth',old('place_of_birth'),['class'=>'form-control form-control-sm'. ($errors->has('place_of_birth') ? ' is-invalid' : ''),'placeholder'=>'Place Of Birth'])}}
                            @foreach($errors->get('place_of_birth') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Nationality</th>
                        <td>{{Form::text('nationality',old('nationality'),['class'=>'form-control form-control-sm'. ($errors->has('nationality') ? ' is-invalid' : ''),'placeholder'=>'Nationality'])}}
                            @foreach($errors->get('nationality') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Passport:</th>
                        <td>{{Form::text('passport_no',old('passport_no'),['class'=>'form-control form-control-sm'. ($errors->has('passport_no') ? ' is-invalid' : ''),'placeholder'=>'Passport'])}}
                            @foreach($errors->get('passport_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Driver's License:</th>
                        <td>{{Form::text('drivers_license',old('drivers_license'),['class'=>'form-control form-control-sm'. ($errors->has('drivers_license') ? ' is-invalid' : ''),'placeholder'=>'Driver\'s License'])}}
                            @foreach($errors->get('drivers_license') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Criminal Record:</th>
                        <td>
                            {{Form::text('criminal_offence',old('criminal_offence'),['class'=>'form-control form-control-sm'. ($errors->has('criminal_offence') ? ' is-invalid' : ''),'placeholder'=>'Criminal Record'])}}
                            @foreach($errors->get('criminal_offence') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Current Location:</th>
                        <td>{{Form::text('current_location',old('current_location'),['class'=>'form-control form-control-sm'. ($errors->has('current_location') ? ' is-invalid' : ''),'placeholder'=>'Current Location'])}}
                            @foreach($errors->get('current_location') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Own Transport:</th>
                        <td>
                            {{Form::select('own_transport', [0 => 'No', 1 => 'Yes'], old('own_transport'),['class'=>'form-control form-control-sm'. ($errors->has('own_transport') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('own_transport') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Dependants:</th>
                        <td>{{Form::select('dependants', $number_drop_down, old('dependants'),['class'=>'form-control form-control-sm'. ($errors->has('dependants') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('dependants') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Disability:</th>
                        <td>
                            {{Form::select('disability', [0 => 'No', 1 => 'Yes'], old('disability'),['class'=>'form-control form-control-sm'. ($errors->has('disability') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('disability') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Health:</th>
                        <td>{{Form::text('health',old('health'),['class'=>'form-control form-control-sm'. ($errors->has('health') ? ' is-invalid' : ''),'placeholder'=>'Health'])}}
                            @foreach($errors->get('health') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Marital Status:</th>
                        <td>
                            {{Form::select('marital_status',$marital_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('marital_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('marital_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Home Language:</th>
                        <td>{{Form::text('home_language',old('home_language'),['class'=>'form-control form-control-sm'. ($errors->has('home_language') ? ' is-invalid' : ''),'placeholder'=>'Home Language'])}}
                            @foreach($errors->get('home_language') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Other Language:</th>
                        <td>{{Form::text('other_language',old('other_language'),['class'=>'form-control form-control-sm'. ($errors->has('other_language') ? ' is-invalid' : ''),'placeholder'=>'Other Language'])}}
                            @foreach($errors->get('other_language') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Scholastic Education</th>
                    </tr>
                    <tr>
                        <th>School Matriculated:</th>
                        <td>{{Form::text('school_matriculated',old('school_matriculated'),['class'=>'form-control form-control-sm'. ($errors->has('school_matriculated') ? ' is-invalid' : ''),'placeholder'=>'School Matriculated'])}}
                            @foreach($errors->get('school_matriculated') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Higher Grade:</th>
                        <td>
                            {{Form::text('highest_grade',old('highest_grade'),['class'=>'form-control form-control-sm'. ($errors->has('highest_grade') ? ' is-invalid' : ''),'placeholder'=>'Highest Grade'])}}
                            @foreach($errors->get('highest_grade') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>School Completion Year:</th>
                        <td>{{Form::text('school_completion_year',old('school_completion_year'),['class'=>'form-control form-control-sm'. ($errors->has('school_completion_year') ? ' is-invalid' : ''),'placeholder'=>'School Completion Year'])}}
                            @foreach($errors->get('school_completion_year') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Position Information</th>
                    </tr>
                    <tr>
                        <th>Main Qualification:</th>
                        <td>
                            {{Form::text('main_qualification',old('main_qualification'),['class'=>'form-control form-control-sm'. ($errors->has('main_qualification') ? ' is-invalid' : ''),'placeholder'=>'Main Qualification'])}}
                            @foreach($errors->get('main_qualification') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Main Role:</th>
                        <td>
                            {{Form::select('role_id',$role_dropdown, old('role_id'), ['class'=>'form-control form-control-sm'. ($errors->has('role_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('role_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contract House</th>
                        <td>
                            {{Form::select('vendor_id',$vendor_drop_down, old('vendor_id'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('vendor_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Placement Options</th>
                        <td>
                            <input type="checkbox" name="permanent" id="permanent" value="1" /> Permanent <input type="checkbox" name="contracting" id="contracting" value="1" /> Contracting <input type="checkbox" name="temporary" id="temporary" value="1" /> Temporary
                        </td>
                    </tr>
                    <tr>
                        <th>Profession</th>
                        <td>
                            {{Form::select('profession_id',$profession_drop_down, old('profession_id'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('profession_id') ? ' is-invalid' : ''), 'id' => 'profession_id'])}}
                            @foreach($errors->get('profession_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Speciality</th>
                        <td>
                            {{Form::select('speciality_id',$speciality_drop_down, old('speciality_id'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'id' => 'speciality_id'])}}
                            @foreach($errors->get('speciality_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Hourly Rate Minimum:</th>
                        <td>
                            {{Form::text('hourly_rate_minimum',old('hourly_rate_minimum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum'])}}
                            @foreach($errors->get('hourly_rate_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Hourly Rate Maximum:</th>
                        <td>
                            {{Form::text('hourly_rate_maximum',old('hourly_rate_maximum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum'])}}
                            @foreach($errors->get('hourly_rate_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Monthly Salary Minimum:</th>
                        <td>
                            {{Form::text('monthly_salary_minimum',old('monthly_salary_minimum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum'])}}
                            @foreach($errors->get('monthly_salary_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Monthly Salary Maximum:</th>
                        <td>
                            {{Form::text('monthly_salary_maximum',old('monthly_salary_maximum'),['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum'])}}
                            @foreach($errors->get('monthly_salary_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Current Salary (Nett):</th>
                        <td>{{Form::text('current_salary_nett',old('current_salary_nett'),['class'=>'form-control form-control-sm'. ($errors->has('current_salary_nett') ? ' is-invalid' : ''),'placeholder'=>'Current Salary'])}}
                            @foreach($errors->get('current_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Expected Salary (Nett):</th>
                        <td>{{Form::text('expected_salary_nett',old('expected_salary_nett'),['class'=>'form-control form-control-sm'. ($errors->has('expected_salary_nett') ? ' is-invalid' : ''),'placeholder'=>'Expected Salary'])}}
                            @foreach($errors->get('expected_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Contact Information</th>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                            <span id="phone"></span>
                        </td>
                        <th>Cell:</th>
                        <td>{{Form::text('cell',old('cell'),['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                            @foreach($errors->get('cell') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Cell 2:</th>
                        <td>{{Form::text('cell2',old('cell2'),['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                            @foreach($errors->get('cell2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Fax:</th>
                        <td>{{Form::text('fax',old('fax'),['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                            @foreach($errors->get('fax') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>
                            <span id="email"></span>
                        </td>
                        <th>Personal Email:</th>
                        <td>{{Form::text('personal_email',old('personal_email'),['class'=>'form-control form-control-sm'. ($errors->has('personal_email') ? ' is-invalid' : ''),'placeholder'=>'Personal Email'])}}
                            @foreach($errors->get('personal_email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    {{--<tr>
                        <th colspan="4" class="btn-dark">Qualification Information</th>
                    </tr>
                    <tr>
                        <th>Main Qualification</th>
                        <td>{{Form::text('main_qualification',old('main_qualification'),['class'=>'form-control form-control-sm'. ($errors->has('main_qualification') ? ' is-invalid' : ''),'placeholder'=>'Main Qualification'])}}
                            @foreach($errors->get('main_qualification') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Main Role:</th>
                        <td>
                            {{Form::select('role_id',$role_dropdown,old('role_id'),['class'=>'form-control form-control-sm '. ($errors->has('role_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('role_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contract House</th>
                        <td>{{Form::text('contract_house',old('contract_house'),['class'=>'form-control form-control-sm'. ($errors->has('contract_house') ? ' is-invalid' : ''),'placeholder'=>'Contract House'])}}
                            @foreach($errors->get('contract_house') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th></th>
                        <td></td>
                    </tr>--}}

                    <tr>
                        <th colspan="4" class="btn-dark">BBBEE Information</th>
                    </tr>
                    <tr>
                        <th>Naturalization</th>
                        <td>{{Form::select('naturalization_id',$naturalization,null,['class'=>'form-control form-control-sm '. ($errors->has('naturalization_id') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('naturalization_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Race</th>
                        <td>{{Form::select('race',$race,null,['class'=>'form-control form-control-sm '. ($errors->has('race') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('race') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>BBBEE Race</th>
                        <td>{{Form::select('bbbee_race',$bbbee_race,null,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_race') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('bbbee_race') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Gender</th>
                        <td>{{Form::select('gender',$gender,null,['class'=>'form-control form-control-sm '. ($errors->has('gender') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('gender') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        {{--<th>Disability</th>
                        <td>{{Form::select('disability',$disability,null,['class'=>'form-control form-control-sm '. ($errors->has('disability') ? ' is-invalid' : ''),'id'=>'disability'])}}
                            @foreach($errors->get('disability') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>--}}
                        <th>Charge Out Rate</th>
                        <td>
                            {{Form::text('charge_out_rate',old('charge_out_rate'),['class'=>'form-control form-control-sm '. ($errors->has('charge_out_rate') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('charge_out_rate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Overview</th>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>
                            @if(isset($result->scouting_id) && $result->scouting_id > 0))
                                <a href="{{route('scouting.show', $result->scouting_id)}}" >View Scouting Record</a>
                            @endif
                        </th>
                        <td>
                            @if(isset($result->scouting_id) && $result->scouting_id > 0))
                                <a href="{{route('scouting.show', $result->scouting_id)}}" >View uploaded documents</a>
                            @endif
                        </td>
                        {{Form::hidden('user_id', $user_id)}}
                    </tr>
                    <tr>
                        <th>Overview Text:</th>
                        <th colspan="3">
                            {{Form::textarea('note',old('note'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Note'])}}
                        </th>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {


            axios.post('/get_user', {
                user_id: '{!! $user_id !!}'
            })
            .then(function (response) {
                $('#first_name').text(response.data.first_name);
                $('#last_name').text(response.data.last_name);
                $('#email').text(response.data.email);
                $('#phone').text(response.data.phone);
            })
            .catch(function (error) {
                console.log(error);
            });
        });

        $('#profession_id').change(function(){

            var profession_id = $('#profession_id').val();

            axios.post('/cv/getspeciality/'+profession_id)
            .then(function (response) {
                var options_html = '<option>Please select</option>';
                $.each(response.data.speciality, function (key, value) {
                    options_html += '<option value='+key+'>'+value+'</option>';
                })

                $('#speciality_id').html(options_html);
            })
            .catch(function (error) {
                console.log(error);
            });

        });

    </script>
@endsection