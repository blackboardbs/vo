 <!-- Modal -->
 <div class="modal fade" id="emailCVModal" tabindex="-1" role="dialog" aria-labelledby="emailCVModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="configuration_label">Email CV</h5>
                <button id="closeCandidateModalBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class="col-md-12">
                        <table class="table table-bordered table-sm table-hover">
                            <tbody>
                                <tr>
                                    <td>Name: </td>
                                    <td style="width: 50%;">
                                        {{Form::text('candidate_prefered_name', (isset($cv->user->first_name)? $cv->user->first_name : '').' '.(isset($cv->user->last_name)? $cv->user->last_name : ''),['class'=>'form-control form-control-sm'. ($errors->has('candidate_prefered_name') ? ' is-invalid' : ''), 'id' => 'candidate_prefered_name'])}}
                                        @foreach($errors->get('candidate_prefered_name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email: </td>
                                    <td style="width: 50%;">
                                        {{Form::text('candidate_email', isset($cv->user->email)?$cv->user->email:'',['class'=>'form-control form-control-sm'. ($errors->has('candidate_email') ? ' is-invalid' : ''), 'id' => 'candidate_email'])}}
                                        @foreach($errors->get('candidate_email') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email cc: </td>
                                    <td style="width: 50%;">
                                            {{Form::text('candidate_email_cc', null,['class'=>'form-control form-control-sm'. ($errors->has('candidate_email_cc') ? ' is-invalid' : ''), 'id' => 'candidate_email_cc'])}}
                                        @foreach($errors->get('candidate_email_cc') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email bcc: </td>
                                    <td style="width: 50%;">
                                            {{Form::text('candidate_email_bcc', null,['class'=>'form-control form-control-sm'. ($errors->has('candidate_email_bcc') ? ' is-invalid' : ''), 'id' => 'candidate_email_bcc'])}}
                                        @foreach($errors->get('job_spec_email_bcc') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>CV Template:</td>
                                    <td>
                                        {{Form::select('candidate_cv_template', $cvTemplatesDropDown, $configDefaultCV,['class'=>'form-control form-control-sm'. ($errors->has('candidate_cv_template') ? ' is-invalid' : ''), 'id' => 'candidate_cv_template'])}}
                                        @foreach($errors->get('candidate_cv_template') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Format:</td>
                                    <td>
                                        {{Form::select('email_cv_format', [1 => 'PDF', 2 => 'MS Word'], 2, ['class'=>'form-control form-control-sm'. ($errors->has('attachment') ? ' is-invalid' : ''), 'id' => 'email_cv_format'])}}
                                        @foreach($errors->get('email_cv_format') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Print Name:</td>
                                    <td style="width: 50%;">
                                        {{Form::select('candidate_cv_print_name', [ 1 => 'Print Name', 2 => 'Do not Print Name'], 1,['class'=>'form-control form-control-sm'. ($errors->has('candidate_cv_print_name') ? ' is-invalid' : ''), 'id' => 'candidate_cv_print_name'])}}
                                        @foreach($errors->get('candidate_cv_print_name') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject:</td>
                                    <td>
                                        {{Form::text('subject', 'CV - '.$cv->user->first_name.' '.$cv->user->last_name, ['class'=>'form-control form-control-sm'. ($errors->has('subject') ? ' is-invalid' : ''), 'id' => 'subject'])}}
                                        @foreach($errors->get('subject') as $error)
                                            <div class="invalid-feedback">
                                                {{$error}}
                                            </div>
                                        @endforeach
                                        {{Form::hidden('w', (isset($_GET['w'])?$_GET['w']:null), ['id' => 'w'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea rows="7" cols="50" style="width: 100%;" id="email_body_md" name="email_body_md">
                                            Dear {{isset($cv->user->first_name)? $cv->user->first_name : ''}} {{isset($cv->user->last_name)? $cv->user->last_name : ''}},
                                            <br/>
                                        </textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button id="email_cv_btn" onclick="emailCV()" class="btn btn-sm bg-dark d-print-none">Submit <i class="fas fa-paper-plane"></i></button>
                    </div>
                </div>
                <div id="loader_div" class="loader" style="display: none;"></div>
            </div>
        </div>
    </div>
</div>