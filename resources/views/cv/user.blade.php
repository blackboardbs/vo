@extends('adminlte.default')

@section('title') CV User @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{-- <a href="{{route('cv.create')}}" class="btn btn-dark float-right ml-1"><i class="fa fa-plus"></i> CV</a> --}}
        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <form id="user_form" method="get" action="{{route('cv.create')}}">
            <fieldset class="group">
                <legend class="heading">Existing User</legend>
                <div class="col-sm-4 col-sm">
                    User: <br/>
                    {{Form::select('r',$resource,old('r'),['class'=>'form-control form-control-sm ', 'style'=>'width: 100%;'])}}
                </div>
                <div class="col-sm-8 col-sm">
                </div>
                <div class="col-sm-4 col-sm">
                    <button style="margin-top: 23px;" class="btn btn-dark" type="submit">Continue</button>
                </div>
            </fieldset>
        </form>
        <form id="user_form" method="get" action="{{route('users.create')}}">
            <fieldset class="group">
                <legend class="heading">New User</legend>
                <div class="col-sm-4 col-sm">
                    <input type="hidden" name="cv" id="cv" value="1"/>
                    <button style="margin-top: 23px;" class="btn btn-primary" type="submit">Create a New User</button>
                </div>
            </fieldset>
        </form>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        fieldset.group {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
            border-radius: 4px;
        }

        legend.heading {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

        });
@endsection
