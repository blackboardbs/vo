@extends('adminlte.default')
@section('title') Edit CV @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('editCv')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('cv.update',['cvid' => $cv,'res_id' => $cv->user_id]), 'method' => 'post','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'editCv'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">Personal Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowspan="5">Photo:</th>
                        <td rowspan="5">
                            <img src="{{route('user_avatar',['q'=>$cv->user->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/><br />
                        </td>
                        <th>First Name:</th>
                        <td>
                            <span id="first_name">{{ $cv->user->first_name }}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>Last Name:</th>
                        <td>
                            <span id="last_name">{{ $cv->user->last_name }}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>Preferred Name:</th>
                        <td>{{Form::text('resource_pref_name',$cv->resource_pref_name,['class'=>'form-control form-control-sm'. ($errors->has('resource_pref_name') ? ' is-invalid' : ''),'placeholder' => 'Preferred Name'])}}
                            @foreach($errors->get('resource_pref_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Middle Name:</th>
                        <td>{{Form::text('resource_middle_name',$cv->resource_middle_name,['class'=>'form-control form-control-sm'. ($errors->has('resource_middle_name') ? ' is-invalid' : ''),'placeholder'=>'Middle Name'])}}
                            @foreach($errors->get('resource_middle_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>ID No:</th>
                        <td>{{Form::text('id_no',$cv->id_no,['class'=>'form-control form-control-sm'. ($errors->has('id_no') ? ' is-invalid' : ''),'placeholder'=>'ID No'])}}
                            @foreach($errors->get('id_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Date Of Birth:</th>
                        <td>{{Form::text('date_of_birth',$cv->date_of_birth,['class'=>'form-control form-control-sm datepicker'. ($errors->has('date_of_birth') ? ' is-invalid' : ''),'placeholder'=>'Date Of Birth'])}}
                            @foreach($errors->get('date_of_birth') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Place Of Birth</th>
                        <td>{{Form::text('place_of_birth',$cv->place_of_birth,['class'=>'form-control form-control-sm'. ($errors->has('place_of_birth') ? ' is-invalid' : ''),'placeholder'=>'Place Of Birth'])}}
                            @foreach($errors->get('place_of_birth') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Nationality</th>
                        <td>{{Form::text('nationality',$cv->nationality,['class'=>'form-control form-control-sm'. ($errors->has('nationality') ? ' is-invalid' : ''),'placeholder'=>'Nationality'])}}
                            @foreach($errors->get('nationality') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Passport:</th>
                        <td>{{Form::text('passport_no',$cv->passport_no,['class'=>'form-control form-control-sm'. ($errors->has('passport_no') ? ' is-invalid' : ''),'placeholder'=>'Passport'])}}
                            @foreach($errors->get('passport_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>

                        <th>Driver's License:</th>
                        <td>{{Form::text('drivers_license',$cv->drivers_license,['class'=>'form-control form-control-sm'. ($errors->has('drivers_license') ? ' is-invalid' : ''),'placeholder'=>'Driver\'s License'])}}
                            @foreach($errors->get('drivers_license') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Criminal Record:</th>
                        <td>{{Form::text('criminal_offence',$cv->criminal_offence,['class'=>'form-control form-control-sm'. ($errors->has('criminal_offence') ? ' is-invalid' : ''),'placeholder'=>'Criminal Record'])}}
                            @foreach($errors->get('criminal_offence') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Current Location:</th>
                        <td>{{Form::text('current_location', $cv->current_location, ['class'=>'form-control form-control-sm'. ($errors->has('current_location') ? ' is-invalid' : ''),'placeholder'=>'Current Location'])}}
                            @foreach($errors->get('current_location') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Own Transport:</th>
                        <td>
                            {{Form::select('own_transport', [0 => 'No', 1 => 'Yes'], $cv->own_transport,['class'=>'form-control form-control-sm'. ($errors->has('own_transport') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('own_transport') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Dependants:</th>
                        <td>{{Form::select('dependants', $number_drop_down, $cv->dependants,['class'=>'form-control form-control-sm'. ($errors->has('dependants') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('dependants') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2"></td>
                        {{--<th>Disability:</th>
                        <td>
                            {{Form::select('disability', [0 => 'No', 1 => 'Yes'], $cv->disability,['class'=>'form-control form-control-sm'. ($errors->has('disability') ? ' is-invalid' : ''),'placeholder'=>'Please select...'])}}
                            @foreach($errors->get('disability') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>--}}
                    </tr>
                    <tr>
                        <th>Health:</th>
                        <td>{{Form::text('health',$cv->health,['class'=>'form-control form-control-sm'. ($errors->has('health') ? ' is-invalid' : ''),'placeholder'=>'Health'])}}
                            @foreach($errors->get('health') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Marital Status:</th>
                        <td>{{Form::select('marital_status',$marital_dropdown,$cv->marital_status_id,['class'=>'form-control form-control-sm '. ($errors->has('marital_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('marital_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Home Language:</th>
                        <td>{{Form::text('home_language',$cv->home_language,['class'=>'form-control form-control-sm'. ($errors->has('home_language') ? ' is-invalid' : ''),'placeholder'=>'Home Language'])}}
                            @foreach($errors->get('home_language') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Other Language:</th>
                        <td>{{Form::text('other_language',$cv->other_language,['class'=>'form-control form-control-sm'. ($errors->has('other_language') ? ' is-invalid' : ''),'placeholder'=>'Other Language'])}}
                            @foreach($errors->get('other_language') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Scholastic Education</th>
                    </tr>
                    <tr>
                        <th>School Matriculated:</th>
                        <td>{{Form::text('school_matriculated',$cv->school_matriculated,['class'=>'form-control form-control-sm'. ($errors->has('school_matriculated') ? ' is-invalid' : ''),'placeholder'=>'School Matriculated'])}}
                            @foreach($errors->get('school_matriculated') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Higher Grade:</th>
                        <td>
                            {{Form::text('highest_grade',$cv->highest_grade,['class'=>'form-control form-control-sm'. ($errors->has('highest_grade') ? ' is-invalid' : ''),'placeholder'=>'Highest Grade'])}}
                            @foreach($errors->get('highest_grade') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>School Completion Year:</th>
                        <td>{{Form::text('school_completion_year',$cv->school_completion_year,['class'=>'form-control form-control-sm'. ($errors->has('school_completion_year') ? ' is-invalid' : ''),'placeholder'=>'School Completion Year'])}}
                            @foreach($errors->get('school_completion_year') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Position Information</th>
                    </tr>
                    <tr>
                        <th>Main Qualification:</th>
                        <td>
                            {{Form::text('main_qualification',$cv->main_qualification,['class'=>'form-control form-control-sm'. ($errors->has('main_qualification') ? ' is-invalid' : ''),'placeholder'=>'Main Qualification'])}}
                            @foreach($errors->get('main_qualification') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Main Role:</th>
                        <td>
                            {{Form::select('role_id',$role_dropdown, $cv->role_id, ['class'=>'form-control form-control-sm'. ($errors->has('role_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('role_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contract House</th>
                        <td>
                            {{Form::select('vendor_id',$vendor_drop_down, $cv->vendor_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('vendor_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('vendor_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Placement Options</th>
                        <td>
                            <input type="checkbox" name="permanent" id="permanent" value="1" {{$cv->is_permanent == 1 ? 'checked':''}}/> Permanent <input type="checkbox" name="contracting" id="contracting" value="1" {{$cv->is_contract == 1 ? 'checked':''}}/> Contracting <input type="checkbox" name="temporary" id="temporary" value="1" {{$cv->is_temporary == 1 ? 'checked':''}}/> Temporary
                        </td>
                    </tr>
                    <tr>
                        <th>Profession</th>
                        <td>
                            {{Form::select('profession_id',$profession_drop_down, $cv->profession_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('profession_id') ? ' is-invalid' : ''), 'id' => 'profession_id'])}}
                            @foreach($errors->get('profession_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Speciality</th>
                        <td>
                            {{Form::select('speciality_id',$speciality_drop_down, $cv->speciality_id,['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('speciality_id') ? ' is-invalid' : ''), 'id' => 'speciality_id'])}}
                            @foreach($errors->get('speciality_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Hourly Rate Minimum:</th>
                        <td>
                            {{Form::text('hourly_rate_minimum',$cv->hourly_rate_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Minimum'])}}
                            @foreach($errors->get('hourly_rate_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Hourly Rate Maximum:</th>
                        <td>
                            {{Form::text('hourly_rate_maximum',$cv->hourly_rate_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('hourly_rate_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Hourly Rate Maximum'])}}
                            @foreach($errors->get('hourly_rate_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th style='width: 20%'>Monthly Salary Minimum</th>
                        <td>
                            {{Form::text('monthly_salary_minimum',$cv->monthly_salary_minimum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_minimum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Minimum'])}}
                            @foreach($errors->get('monthly_salary_minimum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Monthly Salary Maximum:</th>
                        <td>
                            {{Form::text('monthly_salary_maximum',$cv->monthly_salary_maximum,['class'=>'form-control form-control-sm  col-sm-12'. ($errors->has('monthly_salary_maximum') ? ' is-invalid' : ''), 'placeholder'=>'Monthly Salary Maximum'])}}
                            @foreach($errors->get('monthly_salary_maximum') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Current Salary (Nett):</th>
                        <td>{{Form::text('current_salary_nett',$cv->current_salary_nett,['class'=>'form-control form-control-sm'. ($errors->has('current_salary_nett') ? ' is-invalid' : ''),'placeholder'=>'Current Salary'])}}
                            @foreach($errors->get('current_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Expected Salary (Nett):</th>
                        <td>{{Form::text('expected_salary_nett',$cv->expected_salary_nett,['class'=>'form-control form-control-sm'. ($errors->has('expected_salary_nett') ? ' is-invalid' : ''),'placeholder'=>'Expected Salary'])}}
                            @foreach($errors->get('expected_salary_nett') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Contact Information</th>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                            <span id="phone"></span>
                        </td>
                        <th>Cell:</th>
                        <td>
                            {{Form::text('cell',$cv->cell,['class'=>'form-control form-control-sm'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                            @foreach($errors->get('cell') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Cell 2:</th>
                        <td>{{Form::text('cell2',$cv->cell2,['class'=>'form-control form-control-sm'. ($errors->has('cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell 2'])}}
                            @foreach($errors->get('cell2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Fax:</th>
                        <td>{{Form::text('fax',$cv->fax,['class'=>'form-control form-control-sm'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                            @foreach($errors->get('fax') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>
                            <span id="email"></span>
                        </td>
                        <th>Personal Email:</th>
                        <td>{{Form::text('personal_email',$cv->personal_email,['class'=>'form-control form-control-sm'. ($errors->has('personal_email') ? ' is-invalid' : ''),'placeholder'=>'Personal Email'])}}
                            @foreach($errors->get('personal_email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Contract House</th>
                        <td>{{Form::text('contract_house',$cv->contract_house,['class'=>'form-control form-control-sm'. ($errors->has('contract_house') ? ' is-invalid' : ''),'placeholder'=>'Contract House'])}}
                            @foreach($errors->get('contract_house') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">BBBEE Information</th>
                    </tr>
                    <tr>
                        <th>Naturalization</th>
                        <td>{{Form::select('naturalization_id',$naturalization,$cv->naturalization_id,['class'=>'form-control form-control-sm '. ($errors->has('naturalization_id') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('naturalization_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Race</th>
                        <td>{{Form::select('race',$race,$cv->race,['class'=>'form-control form-control-sm '. ($errors->has('race') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('race') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>BBBEE Race</th>
                        <td>{{Form::select('bbbee_race',$bbbee_race,$cv->bbbee_race,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_race') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('bbbee_race') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Gender</th>
                        <td>{{Form::select('gender',$gender,$cv->gender,['class'=>'form-control form-control-sm '. ($errors->has('gender') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('gender') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Disability</th>
                        <td>{{Form::select('disability',$disability,$cv->disability,['class'=>'form-control form-control-sm '. ($errors->has('disability') ? ' is-invalid' : ''),'id'=>'disability'])}}
                            @foreach($errors->get('disability') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>Charge Out Rate</th>
                        <td>
                            {{Form::text('charge_out_rate',$cv->charge_out_rate,['class'=>'form-control form-control-sm '. ($errors->has('charge_out_rate') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('charge_out_rate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th colspan="4" class="btn-dark">Overview</th>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>
                            {{Form::select('status_id',$status_dropdown,$cv->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                            @foreach($errors->get('status_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <th>
                            @if(isset($cv->scouting_id) && $cv->scouting_id > 0)
                            <a href="{{route('scouting.show', $cv->scouting_id)}}" >View Scouting Record</a>
                            @endif
                        </th>
                        <td>
                            @if(isset($cv->scouting_id) && $cv->scouting_id > 0)
                            <a href="{{route('scouting.show', $cv->scouting_id)}}" >View uploaded documents</a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Overview Text:</th>
                        <th colspan="3">
                            {{Form::textarea('note',isset($cv->note)?$cv->note:'',['size' => '30x5','class'=>'form-control form-control-sm col-sm-12','value'=>'Note'])}}
                        </th>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}

            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="8">Availability
                        @if($canCreateCvAvalability)
                        <a href="{{route('availability.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Availability</a>
                        @endif
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Status Date</th>
                    <th>Avail Status</th>
                    <th>Avail Date</th>
                    <th>Note</th>
                    <th>Rate</th>
                    <th>Month Rate</th>
                    <th>Rec Status</th>
                    <th class="last">Action</th>
                </tr>
                @forelse($availability as $results)
                <tr>
                    <td><a href="{{route('availability.show', ['availability' => $results, 'cv' => $cv->id, 'resource' => $results->res_id])}}">{{$results->status_date}}</a></td>
                    <td>{{$results->avail_statusd->description}}</td>
                    <td>{{$results->avail_date}}</td>
                    <td>{{$results->note}}</td>
                    <td>{{$results->rate_hour}}</td>
                    <td>{{$results->rate_month}}</td>
                    <td>{{$results->status->description}}</td>
                    <td>
                        <div class="d-flex">
                        @if($canUpdateCvAvalability)
                        <a href="{{route('availability.edit',['availability' => $results,'cv' => $cv->id,'resource' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                        {{ Form::open(['method' => 'DELETE','route' => ['availability.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                        <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                        {{ Form::close() }}
                        @endif
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="8">No records to show</td>
                </tr>
                @endforelse
                    <tr>
                        <td colspan="8" style="text-align: center">
                            @if($canCreateCvAvalability)
                            <a href="{{route('availability.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Availability</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            @if($cvSkillModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="6">
                            Skill
                            @if($canCreateCvSkill)
                            <a href="{{route('skill.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Skill</a>
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Skill</th>
                        <th>Tool</th>
                        <th>Level</th>
                        <th>Years</th>
                        <th>Status</th>
                        <th class="last">Action</th>
                    </tr>
                @forelse($skill as $results)
                    <tr>
                        <td><a href="{{route('skill.show',['skill' => $results, 'cv' => $cv])}}"> {{ $results->res_skill }} </td>
                        <td>{{($results->tool_id > 0 ? $results->tool_idd->description : '')}}</td>
                        <td>{{($results->skill_level > 0 ? $results->skill_leveld->description : '')}}</td>
                        <td>{{$results->years_experience}}</td>
                        <td>{{$results->status->description}}</td>
                        <td>
                            <div class="d-flex">
                            @if($canUpdateCvSkill)
                            <a href="{{route('skill.edit',['skill' => $results, 'cv' => $cv,'resource' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['skill.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            @endif
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No records to show</td>
                    </tr>
                @endforelse
                    <tr>
                        <td colspan="6" style="text-align: center">
                            @if($canCreateCvSkill)
                            <a href="{{route('skill.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Skill</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvQualificationModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="5">
                            Qualification
                            @if($canCreateCvQualification)
                            <a href="{{route('qualification.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Qualification</a>
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Qualification</th>
                        <th>Institution</th>
                        <th>Year Complete</th>
                        <th>Status</th>
                        <th class="last">Action</th>
                    </tr>
                    @forelse($qualification as $results)
                    <tr>
                        <td><a href="{{route('qualification.show',['qualification' => $results, 'cvid' => $cv->id])}}">{{$results->qualification}}</td>
                        <td>{{$results->institution}}</td>
                        <td>{{$results->year_complete}}</td>
                        <td>{{$results->status->description}}</td>
                        <td>
                            <div class="d-flex">
                            @if($canUpdateCvQualification)
                            <a href="{{route('qualification.edit',['qualificationid' => $results, 'cvid' => $cv,'res_id' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['qualification.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            @endif
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" style="text-align: center;">No records to show</td>
                    </tr>
                    @endforelse
                    <tr>
                        <td colspan="5" style="text-align: center">
                            @if($canCreateCvQualification)
                            <a href="{{route('qualification.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Qualification</a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvExpenseModuleCanView)
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">
                            Experience
                            <a href="{{route('experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa fa-plus"></i> Experience</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($experience as $results)
                    <tr>
                        <th>Period</th>
                        <td><a href="{{route('experience.show',$results)}}">{{$results->period_start}} - {{$results->period_end}}</a></td>
                        <td rowspan="6">@if(isset($results->company_id) && $results->company_id > 0) <img src="{{route('cv_company_logo',['q'=>$results->cv_companyd->logo])}}" id="company-preview-large" class="company-avatar company-avatar-profile" /> @else <img src="{{route('company_avatar',['q'=>'default.jpg'])}}" id="company-preview-large" class="company-avatar company-avatar-profile" /> @endif</td>
                        <td rowspan="6" class="last">
                            <div class="d-flex">
                            <a href="{{route('experience.edit',['experienceid' =>$results,'cvid' => $cv,'res_id' => $results->res_id])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['experience.destroy', $results->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td>{{($results->company != null ? $results->company : $results->cv_companyd->description)}}</td>
                    </tr>
                    <tr>
                        <th>Project</th>
                        <td>{{$results->project}}</td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td>{{$results->role}}</td>
                    </tr>
                    <tr>
                        <th>Responsibility</th>
                        <td>{{$results->responsibility}}</td>
                    </tr>
                    <tr>
                        <th>Tools</th>
                        <td>{{$results->tools}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" style="text-align: center;">No records to show</td>
                    </tr>
                    @endforelse
                    <tr>
                        <td colspan="5" style="text-align: center">
                            <a href="{{route('experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Experience</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif

            @if($cvIndustryExperienceModuleCanView)
                {{Form::open(['url' => route('industry_experience.store'), 'method' => 'post','class'=>'mt-3','autocomplete'=>'off'])}}
                <input type="hidden" name="cv_id" id="cv_id" value="{{$cv->id}}" />
                <input type="hidden" name="res_id" id="res_id" value="{{$cv->user_id}}" />
                <input type="hidden" name="status" id="status" value="1" />
                <table class="table table-bordered table-sm">
                    <thead class="btn-dark">
                    <tr>
                        <th colspan="2">
                            Industry Experience
                            @if($canCreateCvIndustryExperience)
                                <a href="{{route('industry_experience.create',['cvid' => $cv, 'res_id' => $cv->user_id])}}" class="btn btn-outline-light btn-sm float-right"><i class="fa {{empty($industry_experience_ids)?'fa-plus':'fa-pen'}}"></i> Industry Experience</a>
                            @endif
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            {{Form::select('ind_id[]',$industry_experience_dropdown, $selected_industry_eperiencies,['class'=>'form-control form-control-sm '. ($errors->has('ind_id') ? ' is-invalid' : ''),'id'=>'ind_id', 'multiple'])}}
                            @foreach($errors->get('ind_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td width="130" class="text-right">
                            @if($canUpdateCvIndustryExperience)
                                <button type="submit" class="btn btn-success btn-sm">Update</button>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                {{Form::close()}}
            @endif
        </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {


            $('#profession_id').change(function(){

                var profession_id = $('#profession_id').val();

                axios.post('/cv/getspeciality/'+profession_id)
                .then(function (response) {
                    var options_html = '<option>Please select</option>';
                    $.each(response.data.speciality, function (key, value) {
                        options_html += '<option value='+key+'>'+value+'</option>';
                    })

                    $('#speciality_id').html(options_html);
                })
                .catch(function (error) {
                    console.log(error);
                });

            });

            axios.post('/get_user', {
                user_id: '{!! $cv->user_id !!}'
            })
            .then(function (response) {
                $('#first_name').text(response.data.first_name);
                $('#last_name').text(response.data.last_name);
                $('#email').text(response.data.email);
                $('#phone').text(response.data.phone);
            })
            .catch(function (error) {
                console.log(error);
            });
        });

        function generateCV(cvid,userid){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let templateid = $('#cv_template_id').val();
            $.ajax({
                url: '/cv/' + cvid + '/' + userid + '/download/' + templateid,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var a = document.createElement('a');
                    var url = '/storage/template/' + '?q=' + data.file;
                    a.href = url;
                    a.download = data.file.replace(/^.*[\\\/]/, '');
                    a.click();
                    window.URL.revokeObjectURL(url);
                    //window.location.href = '/storage/template/' + '?q=' + data.file;
                }
            });
        }

        function submitTemplateCV(template_type, cv_id, name_type){
            var template_id = $('#cv_template_id').val();
            window.open("../../../template/"+template_type+"/previewtemplate/"+template_id+"/"+cv_id+"/"+name_type, "_blank");
        }

    </script>
@endsection