@extends('adminlte.default')
@section('title') Upload CV Document @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('updatedocument', $document->id), 'method' => 'put', 'class'=>'mt-3', 'files' => true, 'autocomplete' => 'off','id'=>'saveForm'])}}
            <input type="hidden" id="cvid" name="cvid" value="{{$cvid}}"/>
            <table class="table table-borderless table-sm" style="width: 70%;">
                <tr>
                    <td>Name</td>
                    <td>
                        {{Form::text('name',$document->name,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Document</td>
                    <td>
                        <input type="file" id="file" name="file" class="{{$errors->has('file') ? ' is-invalid' : ''}}" />
                        @foreach($errors->get('file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
