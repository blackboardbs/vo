@extends('adminlte.default')

@section('title') Successful Logins @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('user_name', 'User Name')</th>
                    {{--<th>@sortablelink('password', 'Password')</th>--}}
                    <th>@sortablelink('user_ip', 'IP Address')</th>
                    <th>@sortablelink('date', 'Date')</th>
                    {{--@if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                        <th>Action</th>
                    @endif--}}
                </tr>
                </thead>
                <tbody>
                @forelse($logins as $login)
                    <tr>
                        <td>{{$login->user_name}}</td>
                        {{--<td>{{$attempts->password}}</td>--}}
                        <td>{{$login->user_ip}}</td>
                        <td>{{$login->date}}</td>
                        {{--@if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                            <td>
                                {{ Form::open(['method' => 'DELETE','route' => ['logins.destroy', $login->id],'style'=>'display:inline','class'=>'delete']) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}
                            </td>
                        @endif--}}
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">There is No Expired Users.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $logins->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection