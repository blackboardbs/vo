@extends('adminlte.default')

@section('title') Failed Login Attempts @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('user_name', 'User Name')</th>
                    <th>@sortablelink('password', 'Password')</th>
                    <th>@sortablelink('user_ip', 'IP Address')</th>
                    <th>@sortablelink('date', 'Date')</th>
                    {{-- @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager') || Auth::user()->isAn('manager') || Auth::user()->isAn('consultant') || Auth::user()->isAn('contructor')) --}}
                    @if (\auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($failed_attempts as $attempts)
                    <tr>
                        <td>{{$attempts->user_name}}</td>
                        <td>{{$attempts->password}}</td>
                        <td>{{$attempts->user_ip}}</td>
                        <td>{{$attempts->date}}</td>
                        {{-- @if(Auth::user()->isAn('admin') || Auth::user()->isAn('admin_manager') || Auth::user()->isAn('manager') || Auth::user()->isAn('consultant') || Auth::user()->isAn('contructor')) --}}
                        @if (\auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                            <td>
                                {{ Form::open(['method' => 'DELETE','route' => ['logins.destroy', $attempts],'style'=>'display:inline','class'=>'delete']) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">There is No Expired Users.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $failed_attempts->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection