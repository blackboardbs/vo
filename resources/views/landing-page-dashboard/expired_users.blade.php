@extends('adminlte.default')

@section('title') Expired Users @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('first_name', 'Resource')</th>
                    <th>@sortablelink('expiry_date', 'Expiry Date')</th>
                    @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse($expired_users as $referrer)
                    <tr>
                        <td><a href="{{route('profile',$referrer->id)}}">{{ $referrer->first_name.' '.$referrer->last_name }}</a></td>
                        <td>{{$referrer->expiry_date}}</td>

                        @if(Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager', 'consultant', 'contructor']))
                            <td>
                                <a href="{{route('users.edit',$referrer)}}" class="btn btn-success btn-sm">Edit</a>
                                {{ Form::open(['method' => 'DELETE','route' => ['users.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) }}
                                {{ Form::close() }}
                            </td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">There is No Expired Users.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $expired_users->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection