@extends('adminlte.default')

@section('title') {{--Welcome {{auth()->user()->first_name.' '.auth()->user()->last_name}}--}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if(isset($setup_steps) && (count($setup_steps) > 0))
        <div id="bubbles-div" class="row" style="overflow-x: auto; height: 150px;">
            <table>
                <tbody>
                    <tr>
                        @foreach($setup_steps as $setup_step)

                            @if((isset($setup_step->modulelinks->view) && ($setup_step->modulelinks->view != '')) || (isset($setup_step->modulelinks->add) && ($setup_step->modulelinks->add != '')))
                            <td style="width: 8.3%;">
                                <div class="col-sm-12" style="width: 120px; border: 1px solid black; border-radius: 4px; margin: 4px;">
                                    <div style="min-height: 70px;">{{preg_replace('/(?<!\ )[A-Z]/', ' $0', $setup_step->module->display_name)}}</div>
                                    <div><a href="" data-toggle="tooltip" data-placement="bottom" title="Help"><i class="fa fa-question-circle"></i></a><span style="float: right; color: {{$setup_step->status_id == 0? 'red' : 'orange'}}"><i class="far fa-circle"></i></span></div>
                                    <div>
                                        <span>
                                            <a {!! isset($setup_step->modulelinks->view) && $setup_step->modulelinks->view != '' ? 'href="'.$setup_step->modulelinks->view.'"' : 'style="display: none;"' !!} data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-folder-open"></i></a>
                                        </span>
                                        <span style="padding-left: 24px;">
                                            <a {!! isset($setup_step->modulelinks->add) && $setup_step->modulelinks->add != '' ? 'href="'.$setup_step->modulelinks->add.'"' : 'style="display: none;"' !!} data-toggle="tooltip" data-placement="bottom" title="Add"><i class="fa fa-plus"></i></a>
                                        </span>
                                        <span style="float: right;">
                                            <a href="{{route('setup.skipstep', $setup_step->module->id)}}" data-toggle="tooltip" data-placement="bottom" title="Skip"><i class="fa fa-window-close"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            @endif
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
        @endif
        <div class="row">
            @if(in_array(str_replace(' ', '_', strtoupper('Number Of Open Job Specs').'_1'), $user_prefs))
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box" style="{{$configs_color($configs->ojs_minimum_qty, $configs->ojs_minimum_color, $configs->ojs_between_color, $configs->ojs_target_qty, $configs->ojs_target_color, $number_of_open_job_spec)}}">
                        <div class="inner">
                            <h3>{{$number_of_open_job_spec}}</h3>

                            <p class="text-bold">Number of Open Job Specs</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-suitcase"></i>
                        </div>
                        <a href="{{route('reports.jobspecsammary')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endif
                @if(in_array(str_replace(' ', '_', strtoupper('number of new job specs').'_1'), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{$number_of_new_job_specs}}</h3>

                                <p class="text-bold">Number of New Job Specs</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-suitcase"></i>
                            </div>
                            <a href="{{route('reports.jobspecsammary')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('Jobs Specs To Close_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger-gradient">
                            <div class="inner">
                                <h3>{{$job_specs_to_close}}</h3>

                                <p class="text-bold">Jobs Specs to Close</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-suitcase"></i>
                            </div>
                            <a href="{{route('reports.jobspecsammary')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('jobs submitted').'_1'), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success-gradient">
                            <div class="inner">
                                <h3>{{$jobs_submitted}}</h3>

                                <p class="text-bold">Jobs Submitted</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-suitcase"></i>
                            </div>
                            <a href="{{route('reports.jobspecsammary')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('number of active candidates_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{$number_of_active_candidates}}</h3>

                                <p class="text-bold">Number of Active Candidates</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('number of candidates submitted_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success-gradient">
                            <div class="inner">
                                <h3>{{$number_of_candidates_submitted}}</h3>

                                <p class="text-bold">Number of Candidates Submitted</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('number of new candidates').'_1'), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{$number_of_new_candidates}}</h3>

                                <p class="text-bold">Number of New Candidates</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('number of candidates updated_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning-gradient">
                            <div class="inner">
                                <h3>{{$number_of_candidates_updated}}</h3>

                                <p class="text-bold">Number of Candidates Updated</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('CVs Submitted This Month_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box"  style="{{$configs_color($configs->cvsm_minimum_qty, $configs->cvsm_minimum_color, $configs->cvsm_between_color, $configs->cvsm_target_qty, $configs->cvsm_target_color, $cv_submitted_this_month)}}">
                            <div class="inner">
                                <h3>{{$cv_submitted_this_month}}</h3>

                                <p class="text-bold">CV's Submitted This Month</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-invoice"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('new scouts added this week').'_1'), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box"  style="{{$configs_color($configs->nsaw_minimum_qty, $configs->nsaw_minimum_color, $configs->nsaw_between_color, $configs->nsaw_target_qty, $configs->nsaw_target_color, $new_scouts_added_this_week)}}">
                            <div class="inner">
                                <h3>{{$new_scouts_added_this_week}}</h3>

                                <p class="text-bold">New Scouts Added This Week</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('scouting.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('new scouts added this month_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box" style="{{$configs_color($configs->nsam_minimum_qty, $configs->nsam_minimum_color, $configs->nsam_between_color, $configs->nsam_target_qty, $configs->nsam_target_color, $new_scouts_added_this_months)}}">
                            <div class="inner">
                                <h3>{{$new_scouts_added_this_months}}</h3>

                                <p class="text-bold">New Scouts Added This Month</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('scouting.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('new cvs added this week_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box" style="{{$configs_color($configs->nsaw_minimum_qty, $configs->nsaw_minimum_color, $configs->nsaw_between_color, $configs->nsaw_target_qty, $configs->nsaw_target_color, $new_cvs_added_this_week)}}">
                            <div class="inner">
                                <h3>{{$new_cvs_added_this_week}}</h3>

                                <p class="text-bold">New CVs Added This Week</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-invoice"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('new cvs added this month_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box"  style="{{$configs_color($configs->nsam_minimum_qty, $configs->nsam_minimum_color, $configs->nsam_between_color, $configs->nsam_target_qty, $configs->nsam_target_color, $new_cvs_added_this_months)}}">
                            <div class="inner">
                                <h3>{{$new_cvs_added_this_months}}</h3>

                                <p class="text-bold">New CVs Added This Month</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-file-invoice"></i>
                            </div>
                            <a href="{{route('cv.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(in_array(str_replace(' ', '_', strtoupper('Potential Commission Earned').'_1'), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{$potential_commission_earned}}</h3>

                                <p class="text-bold">Potential Commission Earned</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{route('commission.index')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
            @if(in_array(str_replace(' ', '_', strtoupper('Expired Users_1')), $user_prefs))
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box {{($expired_users > 0) ? 'bg-danger-gradient' : 'bg-info-gradient'}}">
                        <div class="inner">
                            <h3>{{$expired_users}}</h3>

                            <p class="text-bold">Expired Users</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user-times"></i>
                        </div>
                        <a href="{{route('dashboard.expired_users')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endif

            @if(in_array(str_replace(' ', '_', strtoupper('Failed Login Attempts_1')), $user_prefs))
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box {{($failed_login_attempts > 0) ? 'bg-warning-gradient' : 'bg-info-gradient'}}">
                        <div class="inner">
                            <h3>{{$failed_login_attempts}}</h3>

                            <p class="text-bold">Failed Login Attempts</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user-secret"></i>
                        </div>
                        <a href="{{ route('logins.failed') }}" class="small-box-footer"  style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endif

                @if(in_array(str_replace(' ', '_', strtoupper('number of logins_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{$logins}}</h3>

                                <p class="text-bold">Number of Logins</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-sign-in-alt"></i>
                            </div>
                            <a href="{{route('logins.success')}}" class="small-box-footer" style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif

                @if(in_array(str_replace(' ', '_', strtoupper('assignment outstanding hours_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success-gradient">
                            <div class="inner">
                                <h3>{{number_format($assignment_out_hours,0,'.',',')}}</h3>

                                <p class="text-bold">Assignment Outstanding Hours</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-business-time"></i>
                            </div>
                            <a href="{{route('assignment.index')}}" class="small-box-footer"  style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif

                @if(in_array(str_replace(' ', '_', strtoupper('Usage: Timesheets Count_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info-gradient">
                            <div class="inner">
                                <h3>{{ number_format($timesheets_count) }}</h3>

                                <p class="text-bold">Usage: Timesheets count</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-calendar-check"></i>
                            </div>
                            <a href="{{route('timesheet.index')}}" class="small-box-footer"  style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif

                @if(in_array(str_replace(' ', '_', strtoupper('Users To Expire In 30 Days_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning-gradient">
                            <div class="inner">
                                <h3>{{$users_to_expire}}</h3>

                                <p class="text-bold">Users to Expire in 30 Days</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-clock"></i>
                            </div>
                            <a href="{{route('dashboard.to_expire')}}" class="small-box-footer"  style="color: #000000!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif

                @if(in_array(str_replace(' ', '_', strtoupper('utilization current week_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        @php $week_perc = isset($total_income["percentage_week"])?$total_income["percentage_week"]:0 @endphp
                        <div class="small-box {{($week_perc < 50) ? 'bg-danger-gradient' : (($week_perc >= 50 && $week_perc < 65) ? 'bg-warning-gradient' : (($week_perc >= 65) ? 'bg-success-gradient' : 'bg-info-gradient'))}}">
                            <div class="inner">
                                <h3>{{number_format($week_perc,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                                <p class="text-bold">Utilization Current Week</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-cog"></i>
                            </div>
                            <a href="reports/dashboard#utilization" class="small-box-footer"  style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif

                @if(in_array(str_replace(' ', '_', strtoupper('utilization past 3 weeks_1')), $user_prefs))
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        @php $util_perc_3 = isset($total_income["total_percentage"])?$total_income["total_percentage"]:0 @endphp
                        <div class="small-box {{($util_perc_3 < 50) ? 'bg-danger-gradient' : ((($util_perc_3 >= 50) && ($util_perc_3 < 65)) ? 'bg-warning-gradient' : (($util_perc_3 >= 65) ? 'bg-success-gradient' : 'bg-info-gradient'))}}">
                            <div class="inner">
                                <h3>{{number_format($util_perc_3,0,'.',',')}}<sup style="font-size: 20px">%</sup></h3>

                                <p class="text-bold">Utilization Past 3 Weeks</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users-cog"></i>
                            </div>
                            <a href="reports/dashboard#utilization" class="small-box-footer"  style="color: #ffffff!important;">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
        </div>
        <div class="row">

            <!-- Latest 10 Candidates -->

            @if(in_array(str_replace(' ', '_', strtoupper('Latest 10 Candidates')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Latest 10 Candidates</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table-striped table table-sm">
                                <thead>
                                    <tr>
                                        <th>CV ID</th>
                                        <th>Full Names</th>
                                        <th>Main Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($latest_10_candidates as $candidate)
                                        <tr>
                                            <td>{{$candidate->id}}</td>
                                            <td>{{isset($candidate->user)?$candidate->user->first_name.' '.$candidate->user->last_name:null}}</td>
                                            <td>{{isset($candidate->role)?$candidate->role->name:null}}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3" class="text-center">No recents candidates</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

        <!-- Latest 10 Job Specs -->

            @if(in_array(str_replace(' ', '_', strtoupper('Latest 10 Job Specs')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Latest 10 Job Specs</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table-striped table table-sm">
                                <thead>
                                <tr>
                                    <th>Priority</th>
                                    <th>Job Reference</th>
                                    <th>Position Role</th>
                                    <th>Customer</th>
                                    <th>Application Close Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($latest_10_job_specs as $job)
                                    <tr>
                                        <td>{{$job->priority}}</td>
                                        <td>{{$job->reference_number}}</td>
                                        <td>{{isset($job->positionrole)?$job->positionrole->name:null}}</td>
                                        <td>{{isset($job->customer)?$job->customer->customer_name:null}}</td>
                                        <td>{{$job->applicaiton_closing_date}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">No recents job specs</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

            <!-- Failed login attempts over 30 days trend -->

            @if(in_array(str_replace(' ', '_', strtoupper('number of failed logins')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Number of failed logins</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="loginsChart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

        <!-- Failed login attempts over 30 days trend -->

            @if(in_array(str_replace(' ', '_', strtoupper('Recruitment Priorities')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Recruitment Priorities</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-sm">
                                <thead>
                                    <th>Priority</th>
                                    <th>Job Reference</th>
                                    <th>Position Role</th>
                                    <th>Customer</th>
                                    <th>Application Close Date</th>
                                </thead>
                                <tbody>
                                    @forelse($recruitment_priorities as $priority)
                                        <tr>
                                            <td>{{$priority->priority}}</td>
                                            <td>{{$priority->reference_number}}</td>
                                            <td>{{isset($priority->positionrole)?$priority->positionrole->name:null}}</td>
                                            <td>{{isset($priority->customer)?$priority->customer->customer_name:null}}</td>
                                            <td>{{$priority->applicaiton_closing_date}}</td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No jobs priority available</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

            <!-- Number of logins -->

            @if(in_array(str_replace(' ', '_', strtoupper('Number Of Logins')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Number of logins</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="loginsSuccChart" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

            @if(in_array(str_replace(' ', '_', strtoupper('hours outstanding on assignments')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Hours Outstanding on Assignments bar chart</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="assignmentHours" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

            @if(in_array(str_replace(' ', '_', strtoupper('Usage: Timesheet Count')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Usage: Timesheet count</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="timesheet" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endif

            @if(in_array(str_replace(' ', '_', strtoupper('Failed Login Attempts Over 30 Days Trend')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                Failed login attempts over 30 days trend
                            </h3>

                            <div class="card-tools">
                                {{$failed_30_login->links()}}
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">User Name</th>
                                    <th scope="col">Password</th>
                                    <th scope="col">IP Address</th>
                                    <th scope="col">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($failed_30_login as $login)
                                    <tr>
                                        <td scope="row">{{$login->user_name}}</td>
                                        <td>{{$login->password}}</td>
                                        <td>{{$login->user_ip}}</td>
                                        <td>{{$login->date}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td scope="row" colspan="4" class="text-center">No Failed Login</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

            {{--EXPENSES--}}

            @if(in_array(str_replace(' ', '_', strtoupper('consultant expenses')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                Expenses to be Approved / Paid
                            </h3>

                            <div class="card-tools">
                                {{$expenses->links()}}
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-sm table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Approver</th>
                                    <th scope="col">Consultant</th>
                                    <th scope="col">Project</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">YearWk</th>
                                    <th scope="col">Amount</th>
                                    @if(auth()->user()->hasAnyRole(['super_admin', 'admin', 'admin_manager', 'manager']))
                                          <th scope="col">Approve/Pay</th>
                                    @endif
</tr>
</thead>
<tbody>
@forelse($expenses as $expense)
   <tr>
       <td scope="row"><a href="{{isset($expense["timesheet_maintenance"])?$expense["timesheet_maintenance"]:$expense["url"]}}">{{$expense["approver"]}}</a></td>
       <td><a href="{{isset($expense["timesheet_maintenance"])?$expense["timesheet_maintenance"]:$expense["url"]}}">{{$expense["consultant"]}}</a></td>
       <td><a href="{{isset($expense["timesheet_maintenance"])?$expense["timesheet_maintenance"]:$expense["url"]}}">{{substr($expense["project"],0,14)}}</a></td>
       <td><span style="cursor: pointer" class="expense-description"  data-toggle="tooltip" data-placement="top" title="{{$expense["description"]}}"><i class="fas fa-info-circle"></i></span></td>
       <td>{{substr($expense["year_week"], 0, 6)}}</td>
       <td>{{number_format((is_float($expense["amount"]) ? $expense["amount"] : 0.00 ),2,'.',',')}}</td>
       @if(auth()->user()->hasAnyRole(['super_admin', 'admin', 'admin_manager', 'manager']))
           <td scope="col">
               @if(isset($expense['approve_date']))
                   <a href="{{$expense['pay_url']}}" class="btn btn-sm btn-success">Pay</a>
               @else
                   <a href="{{$expense['url']}}" class="btn btn-sm btn-success">Approve</a>
               @endif
           </td>
       @endif
   </tr>
@empty
   <tr>
       <td scope="row" colspan="6" class="text-center">No Expenses are available</td>
       @if(auth()->user()->hasAnyRole(['super_admin', 'admin', 'admin_manager', 'manager']))
           <td scope="col"></td>
       @endif
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--IMPORTANT DATES--}}
@if(in_array(str_replace(' ', '_', strtoupper('important dates')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Important Dates
</h3>

<div class="card-tools">

</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Day</th>
   <th scope="col">Name</th>
   <th scope="col">Relationship</th>
</tr>
</thead>
<tbody>
@forelse($anniversaries as $anniversary)
   <tr>
       <td scope="row"><a href="{{route('anniversary.show',$anniversary)}}">{{$anniversary->month_name.' '.$anniversary->day_of_month}}</a></td>
       <td><a href="{{route('anniversary.show',$anniversary)}}">{{$anniversary->name.'\'s '.(isset($anniversary->anniversary_type)?$anniversary->anniversary_type->description:'')}}</a></td>
       <td><a href="{{route('anniversary.show',$anniversary)}}">{{(($anniversary->relationship != 1)?(isset($anniversary->relationshipd)?$anniversary->relationshipd->description:''). ' of ':'').(isset($anniversary->resource)?$anniversary->resource->first_name.' '.$anniversary->resource->last_name:'')}}</a></td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">No Important dates are available</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--NOTICE BOARD--}}
@if(in_array(str_replace(' ', '_', strtoupper('notice board')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Notice Board
</h3>

<div class="card-tools">

</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Notice Date</th>
   <th scope="col">Notice</th>
</tr>
</thead>
<tbody>
@forelse($notices as $notice)
   <tr>
       <td scope="row">{{$notice->message_date}}</td>
       <td style="word-break:break-word;white-space:break-spaces;">{{$notice->message}}</td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">No Notices available</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--INCOME PROJECTS WEEKLY UTILIZATION--}}
@if(in_array(str_replace(' ', '_', strtoupper('Income Projects Weekly Utilization')), $user_prefs))
<div class="col-lg-6">
<div class="card card-primary">
<div class="card-header">
<h3 class="card-title">Income Projects Weekly Utilization</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="weekly_income_utilization" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

{{--COST PROJECTS WEEKLY UTILIZATION--}}
@if(in_array(str_replace(' ', '_', strtoupper('Cost Projects Weekly Utilization')), $user_prefs))
<div class="col-lg-6">
<div class="card card-warning">
<div class="card-header">
<h3 class="card-title">Cost Projects Weekly Utilization</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="weekly_cost_utilization" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

{{--CASHFLOW--}}
@if(in_array(str_replace(' ', '_', strtoupper('cashflow')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header">
<h3 class="card-title">Cashflow</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<table class="table table-striped">
<thead>
<tr>
   <th>Cashflow</th>
   <th class="text-right">Total</th>
   <th class="text-right">5</th>
   <th class="text-right">30</th>
   <th class="text-right">30+</th>
</tr>
</thead>
<tbody>
<tr>
    <th colspan="5">Account Receivable</th>
</tr>
<tr>
   <td>Invoiced</td>
   <td class="text-right">{{number_format(
        ($invoiced["five_days"] + $invoiced['thirty_days'] + $invoiced["thirty_plus_days"])
        ,0,'.',',')}}</td>
   <td class="text-right">{{number_format($invoiced["five_days"],0,'.',',')}}</td>
   <td class="text-right">{{number_format($invoiced['thirty_days'],0,'.',',')}}</td>
   <td class="text-right">{{number_format($invoiced["thirty_plus_days"],0,'.',',')}}</td>
</tr>
<tr>
   <td>Not Invoice</td>
   <td class="text-right">{{number_format($not_invoiced,0,'.',',')}}</td>
   <td class="text-right">{{number_format(0,0,'.',',')}}</td>
   <td class="text-right">{{number_format(0,0,'.',',')}}</td>
   <td class="text-right">{{number_format($not_invoiced,0,'.',',')}}</td>
</tr>
<tr>
    <th colspan="5">Accounts Payable</th>
</tr>
<tr>
    <td>Invoiced</td>
    <td class="text-right">{{number_format(
        ($vendor_invoiced["vendor_five_days"] + $vendor_invoiced["vendor_thirty_days"] + $vendor_invoiced["vendor_thirty_plus_days"])
        ,0,'.',',')}}</td>
    <td class="text-right">{{number_format($vendor_invoiced["vendor_five_days"],0,'.',',')}}</td>
    <td class="text-right">{{number_format(($vendor_invoiced["vendor_thirty_days"]),0,'.',',')}}</td>
    <td class="text-right">{{number_format($vendor_invoiced["vendor_thirty_plus_days"],0,'.',',')}}</td>
</tr>
<tr>
    <td>Not Invoice</td>
    <td class="text-right">{{number_format($Vendor_not_invoiced,0,'.',',')}}</td>
    <td class="text-right">{{number_format(0,0,'.',',')}}</td>
    <td class="text-right">{{number_format(0,0,'.',',')}}</td>
    <td class="text-right">{{number_format($Vendor_not_invoiced,0,'.',',')}}</td>
</tr>
<tr>
   <td>Expense Claims</td>
   <td class="text-right">{{number_format($expense_claims,0,'.',',')}}</td>
   <td class="text-right">{{number_format($expense_claims,0,'.',',')}}</td>
   <td class="text-right">0</td>
   <td class="text-right">0</td>
</tr>
<tr>
    <td>Planned Expenses</td>
    <td class="text-right">{{number_format(($planned_expenses['five_days'] + $planned_expenses['thirty_days'] + $planned_expenses['thirty_days_plus']),0,'.',',')}}</td>
    <td class="text-right">{{number_format($planned_expenses['five_days'],0,'.',',')}}</td>
    <td class="text-right">{{number_format($planned_expenses['thirty_days'],0,'.',',')}}</td>
    <td class="text-right">{{number_format($planned_expenses['thirty_days_plus'],0,'.',',')}}</td>
</tr>
@php
   $nett = ((($invoiced["five_days"] + $invoiced['thirty_days'] + $invoiced["thirty_plus_days"]) + $not_invoiced) - (($vendor_invoiced["vendor_five_days"] + $vendor_invoiced["vendor_thirty_days"] + $vendor_invoiced["vendor_thirty_plus_days"]) + $Vendor_not_invoiced + $expense_claims + ($planned_expenses['five_days'] + $planned_expenses['thirty_days'] + $planned_expenses['thirty_days_plus'])));
   $nett5 = ($invoiced["five_days"] - ($vendor_invoiced["vendor_five_days"] + $expense_claims + $planned_expenses['five_days']));
   $nett30 = ($invoiced['thirty_days'] - ($vendor_invoiced["vendor_thirty_days"] + $planned_expenses['thirty_days']));
   $nett30p = (($invoiced["thirty_plus_days"] + $not_invoiced) - ($vendor_invoiced["vendor_thirty_plus_days"] + $Vendor_not_invoiced + $planned_expenses['thirty_days_plus']));
@endphp
<tr>
   <td>Nett Cashflow</td>
   <td class="text-right {{($nett < 0)?'bg-danger':'bg-success'}}">{{number_format($nett ,0,'.',',')}}</td>
   <td class="text-right {{($nett5 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett5 ,0,'.',',')}}</td>
   <td class="text-right {{($nett30 < 0)?'bg-danger':'bg-success'}}">{{number_format($nett30,0,'.',',')}}</td>
   <td class="text-right {{($nett30p < 0)?'bg-danger':'bg-success'}}">{{number_format($nett30p,0,'.',',')}}</td>
</tr>
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

{{--OPEN INCOME ASSIGNMENT--}}
@if(in_array(str_replace(' ', '_', strtoupper('Open Assignment Income')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">Open Assignment – Income Bottom 10 hours outstanding</h3>

<div class="card-tools">
{{--{{$open_income_assignments[0]->links()}}--}}
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Project Name</th>
   <th scope="col">Consultant</th>
   <th scope="col">Start</th>
   <th scope="col">End</th>
   <th scope="col">Hours Out</th>
</tr>
</thead>
<tbody>
@forelse($open_income_assignments as $assignment)
   <tr>
       <td scope="row"><a href="{{route('assignment.show', $assignment["id"])}}">{{$assignment["project"]}}</a></td>
       <td><a href="{{route('assignment.show', $assignment["id"])}}">{{$assignment["consultant"]}}</a></td>
       <td>{{$assignment["start_date"]}}</td>
       <td class="{{(($assignment["end_date"] < now()->toDateString())?'bg-danger':(($assignment["end_date"] == now()->toDateString())?'bg-warning':''))}}">{{$assignment["end_date"]}}</td>
       <td class="text-right {{(($assignment["complete_percentage"] <= 30)?'bg-success':(($assignment["complete_percentage"] > 30 && $assignment["complete_percentage"] <= 65)?'bg-warning':'bg-danger'))}}">{{number_format($assignment["remaining_hours"])}}</td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">No Open income Assignments are available</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--OPEN COST ASSIGNMENT--}}
@if(in_array(str_replace(' ', '_', strtoupper('Open Assignment - Cost')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">Open Assignment – Cost Bottom 10 hours outstanding</h3>

<div class="card-tools">
{{--{{$open_cost_assignments[0]->links()}}--}}
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Project Name</th>
   <th scope="col">Consultant</th>
   <th scope="col">Start</th>
   <th scope="col">End</th>
   <th scope="col">Hours Out</th>
</tr>
</thead>
<tbody>
@forelse($open_cost_assignments as $assignment)
   <tr>
       <td scope="row"><a href="{{route('assignment.show', $assignment["id"])}}">{{$assignment["project"]}}</a></td>
       <td><a href="{{route('assignment.show', $assignment["id"])}}">{{$assignment["consultant"]}}</a></td>
       <td>{{$assignment["start_date"]}}</td>
       <td class="{{(($assignment["end_date"] < now()->toDateString())?'bg-danger':(($assignment["end_date"] == now()->toDateString())?'bg-warning':''))}}">{{$assignment["end_date"]}}</td>
       <td class="text-right {{(($assignment["complete_percentage"] <= 30)?'bg-success':(($assignment["complete_percentage"] > 30 && $assignment["complete_percentage"] <= 65)?'bg-warning':'bg-danger'))}}">{{$assignment["remaining_hours"]}}</td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">No Open cost Assignments are available</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--UTILIZATION - INCOME--}}
@if(in_array(str_replace(' ', '_', strtoupper('utilization income')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
    Utilization – Income Exception Bottom 10
</h3>

<div class="card-tools">

</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped" id="utilization">
<thead>
<tr>
   <th>Consultant</th>
   <th class="text-center">Week-3</th>
   <th class="text-center">Week-2</th>
   <th class="text-center">Week-1</th>
   <th class="text-center">Ave 3 Week<br> Util %</th>
   <th class="text-center">This Week</th>
</tr>
</thead>
<tbody>
<tr>
   <th></th>
   <th class="text-center {{(isset($weeks['planned3']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week3']:'000000'}}</th>
   <th class="text-center {{(isset($weeks['planned2']))?'':'bg-danger'}}">{{isset($weeks['week2'])?$weeks['week2']:'000000'}}</th>
   <th class="text-center {{(isset($weeks['planned1']))?'':'bg-danger'}}">{{isset($weeks['week1'])?$weeks['week1']:'000000'}}</th>
   <th></th>
   <th class="text-center {{(isset($weeks['planned']))?'':'bg-danger'}}">{{isset($weeks['week'])?$weeks['week']:'000000'}}</th>
</tr>
@forelse($income_utilization as $utilization)
   <tr>
       <td>{{isset($utilization["resource"])?$utilization["resource"]:''}}</td>
       <td class="text-center">{{number_format((isset($utilization["week3"])?$utilization["week3"]:0),0,'.',',')}}</td>
       <td class="text-center">{{number_format((isset($utilization["week2"])?$utilization["week2"]:0),0,'.',',')}}</td>
       <td class="text-center">{{number_format((isset($utilization["week1"])?$utilization["week1"]:0),0,'.',',')}}</td>
       <td class="text-center {{(($utilization["util_percentage"] * 100) < 50)?'bg-danger':((($utilization["util_percentage"] * 100) >= 50 && ($utilization["util_percentage"] * 100) < 65)?'bg-warning':((($utilization["util_percentage"] * 100) >= 65 && ($utilization["util_percentage"] * 100) < 80)?'bg-yellow':((($utilization["util_percentage"] * 100) >= 80)?'bg-success':'')))}}">{{number_format(($utilization["util_percentage"] * 100),0,'.',',')}}%</td>
       <td class="text-center">{{number_format($utilization["week"],0,'.',',')}}</td>
   </tr>
@empty
   <tr>
       <td colspan="6" class="text-center">No Results</td>
   </tr>
@endforelse
<tr>
   <th>Total Hours</th>
   <th class="text-center">{{$total_income["total_week_3"]}}</th>
   <th class="text-center">{{$total_income["total_week_2"]}}</th>
   <th class="text-center">{{$total_income["total_week_1"]}}</th>
   <th class="text-center">{{ $total_income["total_week_3"] + $total_income["total_week_2"] + $total_income["total_week_1"] }}</th>
   <th class="text-center">{{$total_income["total_week"]}}</th>
</tr>
<tr>
<th>Total Util %</th>
   <th class="text-center {{(($total_income["percentage_week3"] * 100) < 50)?'bg-danger':((($total_income["percentage_week3"] * 100) >= 50 && ($total_income["percentage_week3"] * 100) < 65)?'bg-warning':((($total_income["percentage_week3"] * 100) >= 65 && ($total_income["percentage_week3"] * 100) < 80)?'bg-yellow':((($total_income["percentage_week3"] * 100) >= 80)?'bg-success':'')))}}">{{number_format(($total_income["percentage_week3"] * 100),0,'.',',')}}%</th>
   <th class="text-center {{(($total_income["percentage_week2"] * 100) < 50)?'bg-danger':((($total_income["percentage_week2"] * 100) >= 50 && ($total_income["percentage_week2"] * 100) < 65)?'bg-warning':((($total_income["percentage_week2"] * 100) >= 65 && ($total_income["percentage_week2"] * 100) < 80)?'bg-yellow':((($total_income["percentage_week2"] * 100) >= 80)?'bg-success':'')))}}">{{number_format(($total_income["percentage_week2"] * 100),0,'.',',')}}%</th>
   <th class="text-center {{(($total_income["percentage_week1"] * 100) < 50)?'bg-danger':((($total_income["percentage_week1"] * 100) >= 50 && ($total_income["percentage_week1"] * 100) < 65)?'bg-warning':((($total_income["percentage_week1"] * 100) >= 65 && ($total_income["percentage_week1"] * 100) < 80)?'bg-yellow':((($total_income["percentage_week1"] * 100) >= 80)?'bg-success':'')))}}">{{number_format(($total_income["percentage_week1"] * 100),0,'.',',')}}%</th>
   <th class="text-center {{($total_income["total_percentage"] < 50)?'bg-danger':(($total_income["total_percentage"] >= 50 && $total_income["total_percentage"] < 65)?'bg-warning':(($total_income["total_percentage"] >= 65 && $total_income["total_percentage"] < 80)?'bg-yellow':(($total_income["total_percentage"] >= 80)?'bg-success':'')))}}">{{number_format($total_income["total_percentage"],0,'.',',')}}%</th>
   <th class="text-center {{(((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 50)?'bg-danger':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) >= 50 && ((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 65)?'bg-warning':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) >= 65 && ((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100) < 80)?'bg-yellow':((((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0)* 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_income["percentage_week"])?$total_income["percentage_week"]:0) * 100),0,'.',',')}}%</th>
</tr>
<tr>
   <td colspan="6" class="text-danger">{{($weeks["planned3"] && $weeks["planned2"] && $weeks["planned1"] && $weeks["planned"])?'':'Weeks in red are not planned for'}}</td>
</tr>
<tr>
   <td class="text-center bg-danger"><50</td>
   <td class="text-center bg-warning"> >50 65< </td>
   <td class="text-center bg-yellow"> >65 80< </td>
   <td class="text-center bg-success"> >80 </td>
   <td class="text-right" colspan="2">Utilization is calculated at {{isset($weeks["week_hours"])?$weeks["week_hours"]->wk_hours:40}} hours per resource per week</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
@endif

{{--UTILIZATION – COST--}}
@if(in_array(str_replace(' ', '_', strtoupper('utilization Cost')), $user_prefs))
                <div class="col-lg-6">
                    <div class="card card-dark">
                        <div class="card-header ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                Utilization – Cost Exception Bottom 10
                            </h3>

                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-sm table-striped" id="utilization">
                                <thead>
                                <tr>
                                    <th>Consultant</th>
                                    <th class="text-center">Week-3</th>
                                    <th class="text-center">Week-2</th>
                                    <th class="text-center">Week-1</th>
                                    <th class="text-center">Ave 3 Week<br> Util %</th>
                                    <th class="text-center">This Week</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th></th>
                                    <th class="text-center {{(isset($weeks['planned3']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week3']:'000000'}}</th>
                                    <th class="text-center {{(isset($weeks['planned2']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week2']:'000000'}}</th>
                                    <th class="text-center {{(isset($weeks['planned1']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week1']:'000000'}}</th>
                                    <th></th>
                                    <th class="text-center {{(isset($weeks['planned']))?'':'bg-danger'}}">{{isset($weeks['week3'])?$weeks['week']:'000000'}}</th>
                                </tr>
                                @forelse($cost_utilization as $utilization)
                                    <tr>
                                        <td>{{isset($utilization["resource"])?$utilization["resource"]:0}}</td>
                                        <td class="text-center">{{number_format(isset($utilization["week3"])?$utilization["week3"]:0)}}</td>
                                        <td class="text-center">{{number_format(isset($utilization["week2"])?$utilization["week2"]:0)}}</td>
                                        <td class="text-center">{{number_format(isset($utilization["week1"])?$utilization["week1"]:0)}}</td>
                                        <td class="text-center {{(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 50)?'bg-danger':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 50 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 65)?'bg-warning':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 65 && ((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) < 80)?'bg-yellow':((((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($utilization["util_percentage"])?$utilization["util_percentage"]:0) * 100))}}%</td>
                                        <td class="text-center">{{number_format(isset($utilization["week"])?$utilization["week"]:0)}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">No Results</td>
                                    </tr>
                                @endforelse
                                <tr>
                                    <th>Total Hours</th>
                                    <th class="text-center">{{isset($total_cost["total_week_3"])?$total_cost["total_week_3"]:0}}</th>
                                    <th class="text-center">{{isset($total_cost["total_week_2"])?$total_cost["total_week_2"]:0}}</th>
                                    <th class="text-center">{{isset($total_cost["total_week_1"])?$total_cost["total_week_1"]:0}}</th>
                                    <th class="text-center">{{ (isset($total_cost["total_week_3"])?$total_cost["total_week_3"]:0) + (isset($total_cost["total_week_2"])?$total_cost["total_week_2"]:0) + (isset($total_cost["total_week_1"])?$total_cost["total_week_1"]:0) }}</th>
                                    <th class="text-center">{{isset($total_cost["total_week"])?$total_cost["total_week"]:0}}</th>
                                </tr>
                                <tr>
                                    <th>Total Util %</th>
                                    <th class="text-center {{(((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week3"])?$total_cost["percentage_week3"]:0) * 100),0,'.',',')}}%</th>
                                    <th class="text-center {{(((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week2"])?$total_cost["percentage_week2"]:0) * 100),0,'.',',')}}%</th>
                                    <th class="text-center {{(((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week1"])?$total_cost["percentage_week1"]:0) * 100),0,'.',',')}}%</th>
                                    <th class="text-center {{((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 50)?'bg-danger':(((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) >= 50 && (isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 65)?'bg-warning':(((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) >= 65 && (isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) < 80)?'bg-yellow':(((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0) >= 80)?'bg-success':'')))}}">{{number_format((isset($total_cost["total_percentage"])?$total_cost["total_percentage"]:0),0,'.',',')}}%</th>
                                    <th class="text-center {{(((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 50)?'bg-danger':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 50 && ((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 65)?'bg-warning':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 65 && ((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) < 80)?'bg-yellow':((((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100) >= 80)?'bg-success':'')))}}">{{number_format(((isset($total_cost["percentage_week"])?$total_cost["percentage_week"]:0) * 100),0,'.',',')}}%</th>
                                </tr>
                                <tr>
                                    <td colspan="6" class="text-danger">{{(isset($weeks["planned3"]) && isset($weeks["planned2"]) && isset($weeks["planned1"]) && isset($weeks["planned"]))?'':'Weeks in red are not planned for'}}</td>
                                </tr>
                                <tr>
                                    <td class="text-center bg-danger"><50</td>
                                    <td class="text-center bg-warning"> >50 65< </td>
                                    <td class="text-center bg-yellow"> >65 80< </td>
                                    <td class="text-center bg-success"> >80 </td>
                                    <td class="text-right" colspan="2">Utilization is calculated at {{isset($weeks["week_hours"])?$weeks["week_hours"]->cost_wk_hours:40}} hours per resource per week</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('Users To Expire In 30 Days')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Users to Expire in 30 days
</h3>

<div class="card-tools">
{{$to_expire_30->links()}}
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Resource</th>
   <th scope="col">Email</th>
   <th scope="col">Expiry Date</th>
</tr>
</thead>
<tbody>
@forelse($to_expire_30 as $user)
   <tr>
       <td scope="row"><a href="{{route('profile', $user)}}">{{$user->first_name.' '.$user->last_name}}</a></td>
       <td><a href="{{route('profile', $user)}}">{{$user->email}}</a></td>
       <td><a href="{{route('profile', $user)}}">{{$user->expiry_date}}</a></td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">No Users Who are Going to expire</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('leave not approved')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Leave not Approved
</h3>

<div class="card-tools">
{{$leave_not_approved->links()}}
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th scope="col">Resource</th>
   <th scope="col">From</th>
   <th scope="col">End Date</th>
</tr>
</thead>
<tbody>
@forelse($leave_not_approved as $leave)
   <tr>
       <td scope="row"><a href="{{route('leave.edit', $leave)}}">{{($leave->resource)?$leave->resource->first_name.' '.$leave->resource->last_name :''}}</a></td>
       <td><a href="{{route('leave.edit', $leave)}}">{{$leave->date_from}}</a></td>
       <td><a href="{{route('leave.edit', $leave)}}">{{$leave->date_to}}</a></td>
   </tr>
@empty
   <tr>
       <td scope="row" colspan="4" class="text-center">All Leave Approved</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('debtor ageing')), $user_prefs))
    <div class="col-lg-6">
        <div class="card card-dark">
            <div class="card-header ui-sortable-handle" style="cursor: move;">
                <h3 class="card-title">Debtor Ageing</h3>
                <div class="card-tools">
                {{--{{$leave_not_approved->links()}}--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                        <tr>
                           <th>Customer Name</th>
                           <th class="text-right">Total</th>
                           <th class="text-right">30</th>
                           <th class="text-right">30+</th>
                        </tr>
                    </thead>
                <tbody>
                    @forelse($debtor_ageing as $debtor)
                       <tr>
                           <td><a href="{{route('customer.invoice', ['customer' => $debtor["customer_id"], 'invoice_status' => 1])}}">{{isset($debtor["customer"])?$debtor["customer"]:''}}</a></td>
                           <td class="text-right">{{number_format((
                                                    (isset($debtor["total_due_30"])?$debtor["total_due_30"]:0) +
                                                    (isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
                                                    (isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
                                                    (isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
                                                    ),2,'.',',')}}</td>
                           <td class="text-right">{{number_format((isset($debtor["total_due_30"])?$debtor["total_due_30"]:0),2,'.',',')}}</td>
                           <td class="text-right">{{number_format((
                                (isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
                                (isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
                                (isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
                           ),2,'.',',')}}</td>
                       </tr>
                       @php
                           $grand_total_debtor += (
                               (isset($debtor["total_due_30"])?$debtor["total_due_30"]:0) +
                               (isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
                               (isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
                               (isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
                               );
                           $grand_total_30_debtor += (isset($debtor["total_due_30"])?$debtor["total_due_30"]:0);
                           $grand_total_60_debtor += (
                                (isset($debtor["total_due_60"])?$debtor["total_due_60"]:0) +
                                (isset($debtor["total_due_90"])?$debtor["total_due_90"]:0) +
                                (isset($debtor["total_due_90_plus"])?$debtor["total_due_90_plus"]:0)
                           );
                       @endphp
                    @empty
                    @endforelse
                    <tr class="bg-gray-light">
                       <th>TOTAL</th>
                       <th class="text-right">{{number_format($grand_total_debtor, 2,'.',',')}}</th>
                       <th class="text-right">{{number_format($grand_total_30_debtor ,2,'.',',')}}</th>
                       <th class="text-right">{{number_format($grand_total_60_debtor, 2,'.',',')}}</th>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('planning and pipelines')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Planning & Pipeline
</h3>

<div class="card-tools">
{{--{{$leave_not_approved->links()}}--}}
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th>Prospect Name</th>
   <th>Solution</th>
   <th class="text-right">Value</th>
   <th class="text-right">Start</th>
   <th class="text-center">% Chance</th>
</tr>
</thead>
<tbody>
@forelse($pipelines as $pipeline)
   <tr>
       <td><a href="{{route('prospects.show', $pipeline)}}">{{$pipeline->prospect_name}}</a></td>
       <td><a href="{{route('prospects.show', $pipeline)}}">{{$pipeline->solution}}</a></td>
       <td class="text-right">{{number_format($pipeline->value,2,'.',',')}}</td>
       <td class="text-right">{{$pipeline->est_start_date}}</td>
       <td class="text-center {{($pipeline->chance >= 8)?'bg-success':(($pipeline->chance > 5 && $pipeline->chance < 8)?'bg-warning':(($pipeline->chance < 5)?'bg-danger':''))}}">{{$pipeline->chance * 10}}</td>
   </tr>
@empty
   <tr>
       <td colspan="7" class="text-center">No projects in the pipeline</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 customers chart')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Customers</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="top_10_customers" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 customers table')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Customers</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th>Top 10 Customers (12 months)</th>
   <th class="text-right">Hours</th>
   <th class="text-right">Value</th>
   <th class="text-center">Ave Rate</th>
</tr>
</thead>
<tbody>
@forelse($top_10_customers as $customer)
   <tr>
       <td><a href="{{route('customer.show', $customer->customer_id)}}">{{isset($customer->customer)?$customer->customer?->customer_name:''}}</a></td>
       <td class="text-right">{{number_format($customer->hours,0,'.',',')}}</td>
       <td class="text-right">{{number_format($customer->value,2,'.',',')}}</td>
       <td class="text-center">{{($customer->hours != 0 && $customer->value != 0)?number_format($customer->value/$customer->hours,0,'.',','):0}}</td>
   </tr>
@empty
   <tr>
       <td colspan="4" class="text-center">No Customers</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 solutions table')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Solutions</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th>Top 10 Solutions (12 months)</th>
   <th class="text-right">Hours</th>
   <th class="text-right">Value</th>
   <th class="text-center">Ave Rate</th>
</tr>
</thead>
<tbody>
@forelse($top_10_solutions as $solution)
   <tr>
       <td>{{isset($solution->system_desc)?$solution->system_desc->description:''}}</td>
       <td class="text-right">{{number_format($solution->hours,0,'.',',')}}</td>
       <td class="text-right">{{number_format($solution->value,2,'.',',')}}</td>
       <td class="text-center">{{($solution->hours != 0 && $solution->value != 0)?number_format($solution->value/$solution->hours,0,'.',','):0}}</td>
   </tr>
@empty
   <tr>
       <td colspan="4" class="text-center">No Solutions</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 solutions chart')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Solutions</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="top_10_solutions" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 locations chart')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Locations</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="top_10_locationss" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 locations table')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Top 10 Locations</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th>Top 10 Locations (12 months)</th>
   <th class="text-right">Hours</th>
   <th class="text-right">Value</th>
   <th class="text-center">Ave Rate</th>
</tr>
</thead>
<tbody>
@forelse($top_10_locations as $location)
   <tr>
       <td>{{isset($location->location)?$location->location->name:''}}</td>
       <td class="text-right">{{number_format($location->hours,0,'.',',')}}</td>
       <td class="text-right">{{number_format($location->value,2,'.',',')}}</td>
       <td class="text-center">{{($location->hours != 0 && $location->value != 0)?number_format($location->value/$location->hours,0,'.',','):0}}</td>
   </tr>
@empty
   <tr>
       <td colspan="4" class="text-center">No Locations</td>
   </tr>
@endforelse
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('monthly consulting')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Monthly Consulting</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="consulting_canvas" style="height: 450px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('year to date chart')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Year-to-Date Consulting</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<div class="chart">
<canvas id="year_to_date" style="height: 286px; width: 572px;" width="715" height="357"></canvas>
</div>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('year to date table')), $user_prefs))
<div class="col-md-6">
<div class="card card-secondary">
<div class="card-header">
<h3 class="card-title">Year-to-Date Consulting</h3>

<div class="card-tools">
<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
</button>

</div>
</div>
<div class="card-body">
<table class="table table-sm table-striped">
<thead>
<tr>
   <th>Year To Date</th>
   <th class="text-right">Hours</th>
   <th class="text-right">Value</th>
   <th class="text-right">% Change from Last Year</th>
   <th class="text-center">Ave Rate</th>
</tr>
</thead>
<tbody>
<tr>
   <td>{{$year_to_date_1->year??'0000'}}</td>
   <td class="text-right">{{number_format(($year_to_date_1->hours??0),0,'.',',')}}</td>
   <td class="text-right">{{number_format(($year_to_date_1->value??0),2,'.',',')}}</td>
   <td class="text-right">{{isset($year_to_date_2->value) && (int)$year_to_date_2->value?number_format(((($year_to_date_1->value??0)/$year_to_date_2->value)*100),0,'.',','):0}}%</td>
   <td class="text-center">{{(isset($year_to_date_1->hours) && (int)$year_to_date_1->hours? number_format((($year_to_date_1->value??0)/$year_to_date_1->hours),0,'.',','):0)}}</td>
</tr>
<tr>
   <td>{{isset($year_to_date_2->year)?$year_to_date_2->year:'0000'}}</td>
   <td class="text-right">{{number_format((isset($year_to_date_2->hours)?$year_to_date_2->hours:0),0,'.',',')}}</td>
   <td class="text-right">{{number_format((isset($year_to_date_2->value)?$year_to_date_2->value:0),2,'.',',')}}</td>
   <td class="text-right">{{isset($year_to_date_3->value) && (int)$year_to_date_3->value?number_format((($year_to_date_2->value/$year_to_date_3->value)*100),0,'.',','):0}}%</td>
   <td class="text-center">{{(isset($year_to_date_2->value) && (int)$year_to_date_2->hours?number_format(($year_to_date_2->value/$year_to_date_2->hours),0,'.',','):0)}}</td>
</tr>
<tr>
   <td>{{isset($year_to_date_3->year)?$year_to_date_3->year:'0000'}}</td>
   <td class="text-right">{{number_format((isset($year_to_date_3->hours)?$year_to_date_3->hours:0),0,'.',',')}}</td>
   <td class="text-right">{{number_format((isset($year_to_date_3->value)?$year_to_date_3->value:0),2,'.',',')}}</td>
   <td class="text-right">{{isset($year_to_date_4->value) && (int)$year_to_date_4->value?number_format((($year_to_date_3->value/$year_to_date_4->value)*100),0,'.',','):0}}%</td>
   <td class="text-center">{{(isset($year_to_date_3->hours) && (int)$year_to_date_3->hours?number_format(($year_to_date_3->value/$year_to_date_3->hours),0,'.',','):0)}}</td>
</tr>
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
</div>
@endif

@if(in_array(str_replace(' ', '_', strtoupper('creditors ageing')), $user_prefs))
    <div class="col-lg-6">
        <div class="card card-dark">
            <div class="card-header ui-sortable-handle" style="cursor: move;">
                <h3 class="card-title">Creditors Ageing</h3>
                <div class="card-tools">
                    {{--{{$leave_not_approved->links()}}--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-sm mt-3">
                    <thead>
                        <tr>
                            <th>Vendor Name</th>
                            <th class="text-right">Total</th>
                            <th class="text-right">30</th>
                            <th class="text-right">30+</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($creditor_ageing as $creditor)
                            <tr>
                                <td><a href="{{route('vendor.invoice', ['vendor' => $debtor["customer_id"], 'invoice_status' => 1])}}">{{isset($creditor["customer"])?$creditor["customer"]:''}}</a></td>
                                <td class="text-right">{{number_format((
                                    (isset($creditor["total_due_30"])?$creditor["total_due_30"]:0) +
                                    (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
                                    (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
                                    (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
                                ),2,'.',',')}}</td>
                                <td class="text-right">{{number_format((isset($creditor["total_due_30"])?$creditor["total_due_30"]:0),2,'.',',')}}</td>
                                <td class="text-right">{{number_format((
                                    (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
                                    (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
                                    (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
                                ),2,'.',',')}}</td>
                            </tr>
                                @php
                                        $grand_total_creditor += (
                                            (isset($creditor["total_due_30"])?$creditor["total_due_30"]:0) +
                                            (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
                                            (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
                                            (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
                                            );
                                        $grand_total_30_creditor += (isset($creditor["total_due_30"])?$creditor["total_due_30"]:0);
                                        $grand_total_60_creditor += (
                                             (isset($creditor["total_due_60"])?$creditor["total_due_60"]:0) +
                                             (isset($creditor["total_due_90"])?$creditor["total_due_90"]:0) +
                                             (isset($creditor["total_due_90_plus"])?$creditor["total_due_90_plus"]:0)
                                        );
                                    @endphp
                                @empty
                                @endforelse
                                <tr class="bg-gray-light">
                                    <th>TOTAL</th>
                                    <th class="text-right">{{number_format($grand_total_creditor, 2,'.',',')}}</th>
                                    <th class="text-right">{{number_format($grand_total_30_creditor ,2,'.',',')}}</th>
                                    <th class="text-right">{{number_format($grand_total_60_creditor, 2,'.',',')}}</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

@if(in_array(str_replace(' ', '_', strtoupper('outstanding timesheets')), $user_prefs))
<div class="col-lg-6">
<div class="card card-dark">
<div class="card-header ui-sortable-handle" style="cursor: move;">
<h3 class="card-title">
Outstanding Timesheets
</h3>

<div class="card-tools">
@if(count($outstanding_timesheets) > 5)
   <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#oustanding_timesheets">
       More
   </button>
@endif
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
<table class="table table-striped">
{{--<thead>
<tr>
   <th scope="col">Resource</th>
   <th scope="col">From</th>
   <th scope="col">End Date</th>
</tr>
</thead>--}}
<tbody>
@foreach(array_slice($outstanding_timesheets, 0, 5) as $key => $timesheet)
   <tr>
       <td scope="row"><a href="{{route('timesheet.index')}}">{{isset($timesheet)?$timesheet:''}}</a></td>
   </tr>
   {{--@empty
       <tr>
           <td scope="row" colspan="4" class="text-center">No outstanding timesheets</td>
       </tr>--}}
@endforeach
</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="oustanding_timesheets" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
   <div class="modal-content">
       <div class="modal-header">
           <h5 class="modal-title" id="exampleModalLabel">Outstanding Timesheets</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
       </div>
       <div class="modal-body">
           <ul class="list-group">
               @forelse($outstanding_timesheets as $timesheet)
                   <li class="list-group-item">{{$timesheet}}</li>
               @empty
               @endforelse
           </ul>
       </div>
       <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       </div>
   </div>
</div>
</div>
</div>
</div>
</div>
@endif
</div>
</div>
@endsection

@section('extra-js')
<script src="{!! global_asset('adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
@if(isset($setup_steps) && (count($setup_steps) > 0))
<script src="{!! global_asset('nicescroll/jquery.nicescroll.min.js') !!}"></script>
<script src="{!! global_asset('dist/bootstrap/js/tooltip.js') !!}"></script>
<script>
$(function() {
$("#bubbles-div").niceScroll({cursorborder:"",cursorcolor:"#C1C1C1",boxzoom:false});
$('[data-toggle="tooltip"]').tooltip();
});
</script>
@endif
<script>
@if(in_array(str_replace(' ', '_', strtoupper('Number Of Logins')), $user_prefs))
$(function () {
var loginsSuccCanvasChart = document.getElementById("loginsSuccChart");
if (loginsSuccCanvasChart) {
new Chart(loginsSuccCanvasChart, {
type: 'bar',
data: {
labels: [@forelse($logs as $log) @if($log->login_status == 1) @php $date = date_create($log->date) @endphp "{{date_format($date, 'd F Y')}}", @endif @empty @endforelse],
datasets: [{
label: '',
data: [@forelse($login_success as $success_count) "{{ $success_count }}", @empty @endforelse],
backgroundColor: 'rgba(40, 167, 69,.8)',
borderColor: 'rgba(40, 167, 69,.8)',
}]
},
options: {
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            },
            gridLines: {
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                display: false
            },
            ticks: {
                autoSkip: false
            }
        }]
    },
    legend: {
        display: false,
    }
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('number of failed logins')), $user_prefs))
$(function () {
var loginsCanvasChart = document.getElementById("loginsChart");
if (loginsCanvasChart) {
new Chart(loginsCanvasChart, {
type: 'bar',
data: {
labels: [@forelse($logs as $log) @if($log->login_status == 0) @php $date = date_create($log->date) @endphp "{{date_format($date, 'd F Y')}}", @endif @empty @endforelse],
datasets: [{
label: '',
data: [@forelse($login_fail as $fail_count) "{{ $fail_count }}", @empty @endforelse],
backgroundColor: 'rgba(220, 53, 69,.8)',
borderColor: 'rgba(220, 53, 69,.8)',
}]
},
options: {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
   beginAtZero: true
},
gridLines: {
   display: false
}
}],
xAxes: [{
gridLines: {
   display: false
},
ticks: {
   autoSkip: false
}
}]
},
legend: {
display: false,
}
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('hours outstanding on assignments')), $user_prefs))
$(function () {
var assCanvasChart = document.getElementById("assignmentHours");
if (assCanvasChart) {
new Chart(assCanvasChart, {
type: 'bar',
data: {
labels: [@forelse($assignment_project as $key => $assignment) "{{isset($assignment->project) ? '('.$hours_outstanding_ass[$key]['resource'].') '.substr($assignment->project->name,0,20) : '<AssignmentName>'}}", @empty @endforelse],
datasets: [{
label: '',
data: [@foreach($hours_outstanding_ass as $hours) "{{ round($hours["hours"]) }}", @endforeach],
backgroundColor: 'rgba(23, 162, 184,.8)',
borderColor: 'rgba(23, 162, 184,.8)',
}]
},
options: {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
   beginAtZero: true
},
gridLines: {
   display: false
}
}],
xAxes: [{
gridLines: {
   display: false
},
ticks: {
   autoSkip: false
}
}]
},
legend: {
display: false,
}
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('Usage: Timesheet Count')), $user_prefs))
$(function () {
if (document.getElementById("timesheet")) {
new Chart(document.getElementById("timesheet"), {
type: 'line',
data: {
labels: [@foreach($months as $month) '{{$month}}', @endforeach],
datasets: [{
data: [@foreach($timesheets_past_year as $past_year) '{{$past_year}}', @endforeach],
label: "{{$last_year}}",
borderColor: "#1fc8e3",
fill: true
}, {
data: [@foreach($timesheets_this_year as $current_year) '{{$current_year}}', @endforeach],
label: "{{$this_year}}",
borderColor: "#1694a9",
fill: true
}
]
},
options: {
maintainAspectRatio: false,
title: {
display: true,
}
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('Income Projects Weekly Utilization')), $user_prefs))
$(function () {
var WeeklyIncomeUtilization = document.getElementById("weekly_income_utilization");
if (WeeklyIncomeUtilization) {
new Chart(WeeklyIncomeUtilization, {
type: 'bar',
data: {
labels: [@forelse($weekly_income_utilization as $income_uti) "{{$income_uti->week}}",@empty @endforelse],
datasets: [{
label: '',
data: [@forelse($weekly_income_utilization as $income_uti_hours) "{{ $income_uti_hours->hours }}", @empty @endforelse],
backgroundColor: 'rgba(0, 123, 255,.8)',
borderColor: 'rgba(0, 123, 255,.8)',
}]
},
options: {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
   beginAtZero:true
},
gridLines: {
   display:false
}
}],
xAxes: [{
gridLines: {
   display:false
},
ticks: {
   autoSkip: false
}
}]
},
legend: {
display: false,
}
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('Cost Projects Weekly Utilization')), $user_prefs))
$(function () {
var weeklyCostUtilization = document.getElementById("weekly_cost_utilization");
if (weeklyCostUtilization){
new Chart(weeklyCostUtilization, {
type: 'bar',
data: {
labels: [@forelse($weekly_cost_utilization as $cost_uti) "{{$cost_uti->week}}",@empty @endforelse],
datasets: [{
label: '',
data: [@forelse($weekly_cost_utilization as $cost_uti_hours) "{{ $cost_uti_hours->hours }}", @empty @endforelse],
backgroundColor: 'rgba(255, 193, 7,.8)',
borderColor: 'rgba(255, 193, 7,.8)',
}]
},
options: {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
   beginAtZero:true
},
gridLines: {
   display:false
}
}],
xAxes: [{
gridLines: {
   display:false
},
ticks: {
   autoSkip: false
}
}]
},
legend: {
display: false,
}
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 customers chart')), $user_prefs))
$(function () {
var top10CustomersCanvasChart = document.getElementById("top_10_customers");
if (top10CustomersCanvasChart){
new Chart(top10CustomersCanvasChart, {
type: 'doughnut',
data: {
labels: [@forelse($top_10_customers as $customer) "{{isset($customer->customer) ? $customer->customer->customer_name : ''}}",@empty @endforelse],
datasets: [{
label: '',
data: [@forelse($top_10_customers as $top_customers) "{{ $top_customers->value }}", @empty @endforelse],
backgroundColor: ["#8000ff","#bfff00", "#ff00ff", "#00ff80", "#ffbf00","#ffff00","#80ff00", "#00ffbf", "#bf00ff"],
}]
},
options: {
maintainAspectRatio: false,
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 solutions chart')), $user_prefs))
$(function () {
var top10SolutionsCanvasChart = document.getElementById("top_10_solutions");
if (top10SolutionsCanvasChart){
new Chart(top10SolutionsCanvasChart, {
type: 'doughnut',
data: {
labels: [@forelse($top_10_solutions as $solution) "{{(isset($solution->system_desc->description) ? $solution->system_desc->description : '')}}",@empty @endforelse],
datasets: [{
label: '',
data: [@forelse($top_10_solutions as $top_solution) "{{ $top_solution->value }}", @empty @endforelse],
backgroundColor: ["#e83e8c","#dc3545", "#fd7e14", "#ffc107", "#28a745","#20c997","#17a2b8", "#007bff", "#6610f2"],
}]
},
options: {
maintainAspectRatio: false,
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('top 10 locations chart')), $user_prefs))
$(function () {
var top10LocationsCanvasChart = document.getElementById("top_10_locationss");
if (top10LocationsCanvasChart){
new Chart(top10LocationsCanvasChart, {
type: 'doughnut',
data: {
labels: [@forelse($top_10_locations as $location) "{{(isset($location->location->name) ? $location->location->name : '')}}",@empty @endforelse],
datasets: [{
label: '',
data: [@forelse($top_10_locations as $top_location) "{{ $top_location->value }}", @empty @endforelse],
backgroundColor: ["#0abda0", "#d4dca9","#ebf2ea", "#bf9d7a", "#80add7","#20c997","#17a2b8", "#007bff", "#6610f2"],
}]
},
options: {
maintainAspectRatio: false,
}
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('monthly consulting')), $user_prefs))
$(function () {
var consultingCanvasChart = document.getElementById("consulting_canvas");
var thisYear = {
label: '{{isset($monthly_consulting_1[0]->year)?$monthly_consulting_1[0]->year:''}}',
data: [@forelse($monthly_value_1 as $value) "{{$value}}", @empty @endforelse],
backgroundColor: 'rgb(0,142,204)',
borderColor: 'rgb(0,142,204)',
};

var lastYear = {
label: '{{isset($monthly_consulting_2[0]->year)?$monthly_consulting_2[0]->year:''}}',
data: [@forelse($monthly_value_2 as $value) "{{$value}}", @empty @endforelse],
backgroundColor: '#513cdc',
borderColor: '#513cdc',
};

var lastOfLastYear = {
label: '{{isset($monthly_consulting_3[0]->year)?$monthly_consulting_3[0]->year:''}}',
data: [@forelse($monthly_value_3 as $value) "{{$value}}", @empty @endforelse],
backgroundColor: '#d525c9',
borderColor: '#d525c9',
};

var consultingData = {
labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
datasets: [lastOfLastYear, lastYear, thisYear]
};

var monthlyConsulting = {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
beginAtZero:true,
callback: function (data) {
return numeral(data).format('0,0')
}
},
gridLines: {
display:false
},
}],
xAxes: [{
gridLines: {
display:false
},
ticks: {
autoSkip: false
},
stacked: true
}]
},
};
if (consultingCanvasChart){
new Chart(consultingCanvasChart, {
type: 'bar',
data: consultingData,
options: monthlyConsulting
});
}
});
@endif

@if(in_array(str_replace(' ', '_', strtoupper('year to date chart')), $user_prefs))
$(function () {
var yearToDateCanvasChart = document.getElementById("year_to_date");
if (yearToDateCanvasChart) {
new Chart(yearToDateCanvasChart, {
type: 'horizontalBar',
data: {
labels: ["{{isset($year_to_date_3->year)?$year_to_date_3->year:''}}", "{{isset($year_to_date_2->year)?$year_to_date_2->year:''}}", "{{isset($year_to_date_1->year)?$year_to_date_1->year:''}}"],
datasets: [{
label: 'Value',
data: ["{{isset($year_to_date_3->value)?round($year_to_date_3->value,2):0}}", "{{isset($year_to_date_2->value)?round($year_to_date_2->value,2):0}}", "{{isset($year_to_date_1->value)?round($year_to_date_1->value,2):0}}",],
backgroundColor: 'rgba(255, 193, 7,.8)',
borderColor: 'rgba(255, 193, 7,.8)',
},
{
label: 'Hours',
data: ["{{isset($year_to_date_3->hours)?round($year_to_date_3->hours,2):0}}","{{isset($year_to_date_2->hours)?round($year_to_date_2->hours,2):0}}", "{{isset($year_to_date_1->hours)?round($year_to_date_1->hours,2):0}}"],
backgroundColor: 'rgba(255, 193, 7,.0)',
borderColor: 'rgba(255, 193, 7,.0)',
}]
},
options: {
maintainAspectRatio: false,
scales: {
yAxes: [{
ticks: {
   beginAtZero:true
},
gridLines: {
   display:false
}
}],
xAxes: [{
gridLines: {
   display:false
},
ticks: {
   stepSize: 100000,
   callback: function(value) {
       var ranges = [
           { divider: 1e6, suffix: 'M' },
           { divider: 1e3, suffix: 'k' }
       ];
       function formatNumber(n) {
           for (var i = 0; i < ranges.length; i++) {
               if (n >= ranges[i].divider) {
                   return (n / ranges[i].divider).toString() + ranges[i].suffix;
               }
           }
           return n;
       }
       return formatNumber(value);
   }
}
}]
},
legend: {
display: false,
}
}
});
}

});
@endif
$(function () {
    $('.expense-description').tooltip({
        position: {
            my: "center bottom-10", // the "anchor point" in the tooltip element
            at: "center top", // the position of that anchor point relative to selected element
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
            }
        },
    })
})
</script>
@endsection
@section('extra-css')
    <style>
        .bg-yellow{
            background-color: #ffff00;
        }
    </style>
@endsection