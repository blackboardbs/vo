<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$user->name()}}</title>
    <style>
        @font-face {
            font-family: 'alexbrush';
            src: url("{{public_path('fonts/alexbrush.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @font-face {
            font-family: 'openSans';
            src: url("{{public_path('fonts/OpenSans-Regular-webfont.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;
        }
    </style>
    <style>
        body{
            margin:20px 10px;
            font-family:'openSans',"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:0.75rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff;
        }
        .heading-color{
            background-color: {{$custom_style->header_background??'#161860'}};
            color: {{$custom_style->header_color??'#FFFFFF'}};
        }
        .detail-color{
            background-color: {{$custom_style->detail_background??'#FFFFFF'}};
            color: {{$custom_style->detail_color??'#000000'}};
        }
        .footer-color{
            background-color: {{$custom_style->footer_background??'#161860'}};
            color: {{$custom_style->footer_color??'#FFFFFF'}};
        }
        .heading{
            font-size: 14px;
            width: 28%;
        }
        .p{
            padding: 3px 10px;
        }
        .row{
            display: inline-block;
            width: 33%;
            margin-top: 5px;
        }
        .logos{
            display: inline-block;
            width: {{((100-1)/(!count($logos)?1:count($logos)))}}%;
            margin-top: 5px;
        }
        .table{
            width: 100%;
            border-collapse: collapse;
        }
        .text-right{
            text-align: right;
        }
        table { page-break-inside:avoid }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        .responsive {
            max-width: 100%;
            height: auto;
            vertical-align: {{$custom_style->logos_vertical_alignment??'top'}};
        }
        .resource-signature{
            font-size: 26px;
            font-family: 'alexbrush', serif;
            line-height: 1.5;
        }
    </style>
</head>
<body>
    @forelse($logos as $logo)
        @if(is_file($logo))
            <div class="logos">
                <img src="{{$logo}}" alt="" class="responsive">
            </div>
        @endif
    @empty
        <p>There's no logo selected</p>
    @endforelse

    {!! $custom_style->comment??null !!}
    <table class="table" style="margin-top: 15px;">
        @if(isset($custom_style->is_billing_period_included))
            <tr>
                <th class="heading-color heading p">Timesheet for:</th>
                <th class="p" style="white-space: nowrap;">{{$billing_period->period_name. (!Str::contains($billing_period->period_name, $billing_period->year)?' '.$billing_period->year:null)}}</th>
                <th class="heading-color heading p">Billing Period:</th>
                <th class="p"  style="white-space: nowrap;">{{$billing_period->start_date}} - {{$billing_period->end_date}}</th>
            </tr>
        @endif
        <tr>
            <th class="heading-color heading p">{{$custom_style->resource_label??"Resource Name"}}:</th>
            <th class="p" style="white-space: nowrap;">{{$user->name()}}</th>
            <th class="heading-color heading p">{{$custom_style->project_label??"Project Name"}}:</th>
            <th class="p" style="white-space: nowrap;">{{$project->project->name??null}}</th>
        </tr>
        <tr>
            <th class="heading-color heading p">{{$custom_style->start_date_label??"Date From"}}:</th>
            <th class="p">{{$date_from}}</th>
            <th class="heading-color heading p">{{$custom_style->po_number_label??"PO Number"}}:</th>
            <th class="p">{{$project->customer_po}}</th>
        </tr>
        <tr>
            <th class="heading-color heading p">{{$custom_style->end_date_label??"Date To"}}:</th>
            <th class="p">{{$date_to}}</th>
        </tr>
        <tr>
            <th class="heading-color heading p"></th>
            <th class="p"></th>
        </tr>
    </table>
    @foreach($week_days as $index => $week)
        <table class="table" style="margin-top: 20px">
            <thead class="heading-color">
            <tr>
                <th class="p" style="width: 80px">Date</th>
                <th class="p">Description</th>
                <th class="p text-right">Hours</th>
                <th class="p text-right">Billable</th>
                <th class="p text-right">Non Billable</th>
            </tr>
            </thead>
            <tbody class="detail-color">
                <tr>
                    <td class="p" style="width: 80px">{{$week["monday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["monday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["monday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["monday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["monday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["tuesday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["tuesday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["tuesday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["tuesday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["tuesday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["wednesday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["wednesday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["wednesday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["wednesday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["wednesday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["thursday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["thursday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["thursday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["thursday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["thursday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["friday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["friday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["friday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["friday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["friday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["saturday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["saturday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["saturday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["saturday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["saturday"]["non_billable"]??null}}</td>
                </tr>
                <tr>
                    <td class="p" style="width: 80px">{{$week["sunday"]["date"]??null}}</td>
                    <td class="p" style="text-transform: capitalize">{{$week["sunday"]["description"]??null}}</td>
                    <td class="p text-right">{{$week["sunday"]["hours"]??null}}</td>
                    <td class="p text-right">{{$week["sunday"]["billable"]??null}}</td>
                    <td class="p text-right">{{$week["sunday"]["non_billable"]??null}}</td>
                </tr>
                <tr class="heading-color">
                    <th class="p" colspan="3">TOTAL HOURS</th>
                    <th class="p text-right">{{$week["total"]["bill_total"]}}</th>
                    <th class="p text-right">{{$week["total"]["non_bill_total"]}}</th>
                </tr>
                @if($index === (count($week_days) - 1))
                    <tr>
                        <th colspan="5">&nbsp;</th>
                    </tr>
                    <tr class="heading-color">
                        <th class="p" colspan="3">GRAND TOTAL HOURS</th>
                        <th class="p text-right">{{_minutes_to_time($bill_total)}}</th>
                        <th class="p text-right">{{_minutes_to_time($non_bill_total)}}</th>
                    </tr>
                    <tr class="heading-color">
                        <th class="p" colspan="3">GRAND TOTAL HOURS IN DECIMAL</th>
                        <th class="p text-right">{{number_format(($bill_total/60),2,',','.')}}</th>
                        <th class="p text-right">{{number_format(($non_bill_total/60),2,',','.')}}</th>
                    </tr>
                @endif
            </tbody>
        </table>
    @endforeach

    @foreach($digisign as $sign)
        @if(count($digisign) > 1)
            <div>
                <div class="row" style="width: 48%; margin-right: 15px">
                    <div class="footer-color p" style="margin-top: 20px">
                        <strong>{{$sign->type}}</strong>
                    </div>
                    <div class="resource-signature" style="border: 1px solid black; height: 40px; margin-bottom: 10px; padding: 5px 0 0 10px;">

                        @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                            {{$sign->user}}
                        @endif
                    </div>
                    <strong>{{$sign->user??null}}</strong>
                </div>
                <div class="row" style="width: 48%; margin-right: 15px; vertical-align: top;">
                    <div class="footer-color p" style="margin-top: 20px">
                        Date:
                    </div>
                    <div style="border: 1px solid black; height: 40px; margin-bottom: 10px; padding: 5px 0 0 10px; line-height: 1.5">
                        <strong>@if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif</strong>
                    </div>
                </div>
            </div>
        @else
            <div>
                <div class="row" style="width: 100%">
                    <div class="footer-color p" style="margin-top: 20px">
                        <strong>{{$sign->type}}</strong>
                    </div>
                    <div class="resource-signature" style="border: 1px solid black; height: 40px; margin-bottom: 10px; padding: 5px 0 0 10px;">

                        @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                            {{$sign->user}}
                        @endif
                    </div>
                    <strong>{{$sign->user??null}}</strong>
                </div>
                <div class="row" style="width: 100%; vertical-align: top;">
                    <div class="footer-color p" style="margin-top: 20px">
                        Date:
                    </div>
                    <div style="border: 1px solid black; height: 40px; margin-bottom: 10px; padding: 5px 0 0 10px; line-height: 1.5">
                        <strong>@if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif</strong>
                    </div>
                </div>
            </div>
        @endif

    @endforeach
</body>
</html>
