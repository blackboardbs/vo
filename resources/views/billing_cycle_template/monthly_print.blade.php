<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>CF Template Weekly Detail</title>
    <style>
        @font-face {
            font-family: 'alexbrush';
            src: url("{{public_path('fonts/alexbrush.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @font-face {
            font-family: 'openSans';
            src: url("{{public_path('fonts/OpenSans-Regular-webfont.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
    </style>
    <style>
        body{
            margin: 20px 10px;
            font-family:'openSans',"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:0.75rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff
        }
        .table,hr{
            margin-bottom:1rem
        }
        hr{
            margin-top:1rem;
            border:0;
            border-top:1px solid rgba(0,0,0,.1)
        }
        .table-responsive{
            display:block;
            width:100%;
            overflow-x:auto;
            -webkit-overflow-scrolling:touch;
            -ms-overflow-style:-ms-autohiding-scrollbar
        }
        .bg-dark,.bg-dark a{
            color:#fff!important
        }
        .p-2{
            padding:.5rem!important
        }
        .bg-dark{
            background-color:#343a40!important
        }
        .col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{
            position:relative;
            width:100%;
            min-height:1px;
            padding-right:7.5px;
            padding-left:7.5px
        }
        .row{
            display:flex;
            flex-wrap:wrap;
            margin-right:-7.5px;
            margin-left:-7.5px
        }
        .no-gutters{
            margin-right:0;
            margin-left:0
        }
        .no-gutters>.col,.no-gutters>[class*=col-]{
            padding-right:0;
            padding-left:0
        }
        *,::after,::before{
            box-sizing:border-box
        }
        .text-right{
            text-align:right!important
        }
        .table{
            width:100%;
            max-width:100%;
            background-color:transparent
        }
        table{
            border-collapse:collapse
        }
        .table td,.table th{
            padding:.75rem;
            vertical-align:top;
            border-top:1px solid #dee2e6
        }
        th{
            text-align:inherit
        }
        .table-responsive>.table-bordered{
            border:0
        }.table-bordered,.table-bordered td,.table-bordered th{border:1px solid {{$custom_style->header_background??'#161860'}} }.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}}
        .eim-template-heading{
            background-color: #161860;
            color: #ffffff;
        }

        .heading-color{
            background-color: {{$custom_style->header_background??'#161860'}};
            color: {{$custom_style->header_color??'#FFFFFF'}};
        }
        .detail-color{
            background-color: {{$custom_style->detail_background??'#FFFFFF'}};
            color: {{$custom_style->detail_color??'#000000'}};
        }
        .footer-color{
            background-color: {{$custom_style->footer_background??'#161860'}};
            color: {{$custom_style->footer_color??'#FFFFFF'}};
        }

        .borderless td, .borderless th {
            border: none;
        }
        .border{
            border: 1px solid {{$custom_style->header_background??'#161860'}} ;
        }
        .border-bottom-0{
            border-bottom: 0!important;
        }
        .resource-col{
            display: inline-block;
            margin-top: 5px;
        }
        .resource-col-3{
            width: 33%;
        }
        .resource-col-2{
            width: 48%;
            margin-right: 10px
        }
        .resource-p-m-3{
            padding: 3px 10px;
        }
        .resource-signature{
            border: 1px solid {{$custom_style->header_background??'#161860'}} ;
            padding: 10px 0 0 40px;
            height: 50px;
            font-size: 26px;
            font-family: 'alexbrush', Arial, serif;
            line-height: 1.2;
        }
        .responsive {
            max-width: 100%;
            height: auto;
            vertical-align: {{$custom_style->logos_vertical_alignment??'top'}};
        }
        .logos{
            display: inline-block;
            width: {{((100-1)/(!count($logos)?1:count($logos)))}}%;
            margin-top: 5px;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini" style="line-height: 1.24em;">
<div id="app" class="wrapper">
    <div class="container-fluid">
        <div id="timesheets_div">
            @forelse($logos as $logo)
                <div class="logos">
                    @if(is_file($logo))
                        <img src="{{$logo}}" alt="" class="responsive">
                    @endif
                </div>
            @empty
                <p>There's no logo selected</p>
            @endforelse
            {!! $custom_style->comment??null !!}
            <table class="table table-bordered table-sm mt-3">
                <tbody>
                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr>
                        <th class="heading-color">Name</th><td colspan="6">{{$monthly_data->name}}</td>
                    </tr>
                    <tr>
                        <th class="heading-color">Billing Period</th><td>{{$monthly_data->billing_period}}</td><td class="heading-color">From </td><td colspan="4">{{$monthly_data->from}}</td>
                    </tr>
                    <tr>
                        <th class="heading-color">PO Number </th><td>{{$monthly_data->po_number}}</td><td class="heading-color">To </td><td colspan="4">{{$monthly_data->to}}</td>
                    </tr>

                    <tr>
                        <td colspan="7" style="border-right: 1px solid transparent; border-left: 1px solid transparent;">&nbsp;</td>
                    </tr>
                    <tr class="heading-color">
                        <th style="width: 15%; vertical-align: middle;">Date</th><th style="white-space: nowrap; vertical-align: middle;">Project code</th><th style="vertical-align: middle;">Project name</th><th style="vertical-align: middle;">Description</th><th style="vertical-align: middle;">Hours</th><th style="vertical-align: middle;">Billable</th><th style="vertical-align: middle;">Non Billable</th>
                    </tr>
                    @foreach($monthly_data->monthly_data as $data)
                        <tr class="detail-color">
                            <td>{{$data->date}}</td>
                            <td style="white-space: nowrap">{{$data->project_ref}}</td>
                            <td>{{$data->project_name}}</td>
                            <td>{{substr($data->description,0,100)}}{{(mb_strlen($data->description) > 100)?'...':null}}</td>
                            <td>{{_minutes_to_time($data->hours)}}</td>
                            <td>{{_minutes_to_time($data->billable_hours)}}</td>
                            <td>{{_minutes_to_time($data->non_billable)}}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <th colspan="4">TOTAL (hours)</th><th>{{_minutes_to_time(collect($monthly_data->monthly_data)->sum('hours'))}}</th><th></th><td></td>
                        </tr>
                        <tr>
                            <th colspan="4">TOTAL (billable hours)</th><td></td><th>{{_minutes_to_time(collect($monthly_data->monthly_data)->sum('billable_hours'))}}</th><td></td>
                        </tr>
                        <tr>
                            <th colspan="4">TOTAL (non billable hours)</th><td></td><td></td><th>{{_minutes_to_time(collect($monthly_data->monthly_data)->sum('non_billable'))}}</th>
                        </tr>
                        <tr class="footer-color">
                            <th colspan="4">Time In Decimal</th>
                            <th>{{number_format((collect($monthly_data->monthly_data)->sum('hours')/60),2,',','.')}}</th>
                            <th>{{number_format((collect($monthly_data->monthly_data)->sum('billable_hours')/60),2,',','.')}}</th>
                            <th>{{number_format((collect($monthly_data->monthly_data)->sum('non_billable')/60),2,',','.')}}</th>
                        </tr>
                </tbody>
            </table>

            @foreach($digisign as $digi_sign)
                @if(sizeof($digisign) >= 3)
                    <div class="resource-col resource-col-3 fo">
                        <div class="resource-p-m-3 border border-bottom-0 footer-color">
                            <strong>{{$digi_sign->type}}</strong>
                        </div>
                        <div class="resource-signature">
                            @if($digi_sign->type == "Resource"  && (isset($custom_style->is_signed) && $custom_style->is_signed))
                                {{$digi_sign->user}}
                            @endif
                        </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date: @if($digi_sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))<span style="margin-left: 50px">{{now()->toDateString()}}</span> @endif</div>
                        <strong>{{$digi_sign->user??null}}</strong>
                    </div>
                @elseif(sizeof($digisign) == 2)
                    <div class="resource-col resource-col-2">
                        <div class="resource-p-m-3 border border-bottom-0 footer-color">
                            <strong>{{$digi_sign->type}}</strong>
                        </div>
                        <div class="resource-signature">
                            @if($digi_sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                                {{$digi_sign->user}}
                            @endif
                        </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date: @if($digi_sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))<span style="margin-left: 50px">{{now()->toDateString()}}</span> @endif</div>
                        <strong>{{$digi_sign->user??null}}</strong>
                    </div>
                @else
                    <div class="resource-p-m-3 border border-bottom-0 footer-color">
                        <strong>{{$digi_sign->type}}</strong>
                    </div>
                    <div class="resource-signature">

                        @if($digi_sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                            {{$digi_sign->user}}
                        @endif
                    </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date: @if($digi_sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))<span style="margin-left: 50px">{{now()->toDateString()}}</span> @endif</div>
                    <strong>{{$digi_sign->user??null}}</strong>
                @endif
            @endforeach
        </div>
    </div>
</div>
</body>
</html>
