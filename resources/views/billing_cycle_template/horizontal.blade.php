<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$user->name()}}</title>
    <style>
        @font-face {
            font-family: 'alexbrush';
            src: url("{{public_path('fonts/alexbrush.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @font-face {
            font-family: 'openSans';
            src: url("{{public_path('fonts/OpenSans-Regular-webfont.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
    </style>
    <style>
        body{
            margin:20px 10px;
            font-family:'openSans',"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:0.75rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff;
        }
        .heading-color{
            background-color: {{$custom_style->header_background??'#161860'}};
            color: {{$custom_style->header_color??'#FFFFFF'}};
        }
        .detail-color{
            background-color: {{$custom_style->detail_background??'#FFFFFF'}};
            color: {{$custom_style->detail_color??'#000000'}};
        }
        .footer-color{
            background-color: {{$custom_style->footer_background??'#161860'}};
            color: {{$custom_style->footer_color??'#FFFFFF'}};
        }
        .heading{
            font-size: 14px;
            width: 33%;
        }
        .p{
            padding: 3px 10px;
        }
        .d-inline-block{
            display: inline-block;
        }
        .strong{
            font-weight: bold;
        }
        .table{
            width: 100%;
            border-collapse: collapse;
        }
        .border-right{
            border-right: 1px solid gray
        }
        .row{
            display: inline-block;
            width: 33%;
            margin-top: 5px;
        }
        .logos{
            display: inline-block;
            width: {{((100-1)/(!count($logos)?1:count($logos)))}}%;
            margin-top: 5px;
        }
        .responsive {
            max-width: 100%;
            height: auto;
            vertical-align: {{$custom_style->logos_vertical_alignment??'top'}};
        }
        .resource-signature{
            font-size: 26px;
            font-family: 'alexbrush', serif;
            line-height: 1.2;
        }
    </style>
</head>
<body>

    @forelse($logos as $logo)
        @if(is_file($logo))
            <div class="logos">
                <img src="{{$logo}}" alt="" class="responsive">
            </div>
        @endif
        @empty
        <p>There's no logo selected</p>
    @endforelse

    {!! $custom_style->comment??null !!}
    <div style="margin-top: 10px">
        @if(isset($custom_style->is_billing_period_included))
            <div class="d-inline-block heading-color p strong" style="width: 18%">Timesheet for:</div>
            <div class="d-inline-block p strong" style="width: 38%">{{Str::contains($billing_period->period_name, $billing_period->year)?$billing_period->period_name:($billing_period->period_name." ".$billing_period->year)}}</div>
            <div class="d-inline-block heading-color p strong" style="width: 18%">Billing Period:</div>
            <div class="d-inline-block p strong" style="width: 18%">{{$billing_period->start_date}} - {{$billing_period->end_date}}</div>
        @endif
        <div class="d-inline-block heading-color p strong" style="width: 18%">{{$custom_style->resource_label??"Resource Name"}}:</div>
        <div class="d-inline-block p strong" style="width: 37%">{{$user->name()}}</div>
        <div class="d-inline-block heading-color p strong" style="width: 18%">{{$custom_style->start_date_label??"Date From"}}:</div>
        <div class="d-inline-block p strong" style="width: 18%">{{$start_date}}</div>
        <div class="d-inline-block heading-color p strong" style="width: 18%">{{$custom_style->project_label??"Project Name"}}:</div>
        <div class="d-inline-block p strong" style="width: 37%">{{$project->project->name??null}}</div>
        <div class="d-inline-block heading-color p strong" style="width: 18%">{{$custom_style->po_number_label??"PO Number"}}:</div>
        <div class="d-inline-block p strong" style="width: 18%">{{$project->customer_po}}</div>
    </div>
    <table class="table" style="margin-top: 20px">
        <thead class="heading-color">
            <tr>
                <td class="p strong" rowspan="2">Task Description</td>
                @foreach($start_of_weeks as $week_start)
                    <td class="p strong border-right" style="text-align: center;" colspan="7">{{\Carbon\Carbon::parse($week_start)->format('d-M-y')}}</td>
                @endforeach
                <td class="p border-right" rowspan="2">Tot</td>
                <td class="p border-right" rowspan="2">Tot In Dec</td>
            </tr>
            <tr>
                @foreach($start_of_weeks as $week_start)
                    <td class="p strong ">M</td>
                    <td class="p strong">Tu</td>
                    <td class="p strong">W</td>
                    <td class="p strong">th</td>
                    <td class="p strong">F</td>
                    <td class="p strong">Sa</td>
                    <td class="p strong border-right">Su</td>
                @endforeach
            </tr>
        </thead>
        <tbody class="detail-color">
            @foreach($week_days as $key => $week)
                <tr>
                    <td rowspan="2">{{$key}}</td>
                    @foreach($start_of_weeks as $week_start)
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })/60) < 10
                                ?'0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })/60) < 10
                                ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })/60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })/60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })/60) < 10
                                ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })/60) < 10
                                ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })/60) < 10
                                ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })/60)}}</td>
                        <td class="p border-right" style="vertical-align: bottom">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })/60) < 10
                                ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })/60)
                                :floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })/60)}}
                        </td>
                    @endforeach
                    <td class="strong p border-right" style="text-align: right">
                        {{floor($week->sum("total")/60) < 10
                            ? '0'.floor($week->sum("total")/60)
                            :floor($week->sum("total")/60)}}
                    </td>
                    <td rowspan="2" class="strong p border-right" style="text-align: right; border-bottom: 1px solid gray">
                        {{number_format(($week->sum("total")/60), 2, ',', '.')}}
                    </td>
                </tr>
                <tr>
                    @foreach($start_of_weeks as $week_start)
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->monday;
                                    }
                                    return 0;
                                })%60)}}</td>
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->tuesday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->wednesday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->thursday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->friday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                        <td class="p" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->saturday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                        <td class="p border-right" style="vertical-align: bottom; border-bottom: 1px solid black">
                            {{floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })%60) < 10 ? '0'.floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })%60):floor($week->sum(function ($line) use($week_start){
                                    if ($line->first_day_of_week == $week_start){
                                        return $line->sunday;
                                    }
                                    return 0;
                                })%60)}}
                        </td>
                    @endforeach
                    <td class="strong p border-right" style="text-align: right; border-bottom: 1px solid black">
                        {{floor($week->sum("total")%60) < 10 ? '0'.floor($week->sum("total")%60):floor($week->sum("total")%60)}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @foreach($digisign as $sign)
        @if(sizeof($digisign) >= 3)
            <div class="row" style="page-break-inside:avoid">
                <div class="footer-color p" style="margin-top: 20px">
                    <strong>{{$sign->type}}</strong>
                </div>
                <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 10px 0 0 7px;">
                    @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                        {{$sign->user}}
                    @endif
                </div>
                <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px; padding: 2px">
                    Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                </div>
                <strong>{{$sign->user??null}}</strong>
            </div>
        @elseif(sizeof($digisign) == 2)
            <div class="row" style="width: 48%; margin-right: 15px;page-break-inside:avoid">
                <div class="footer-color p" style="margin-top: 20px">
                    <strong>{{$sign->type}}</strong>
                </div>
                <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 10px 0 0 7px;">
                    @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                        {{$sign->user}}
                    @endif
                </div>
                <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px; padding: 2px">
                    Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                </div>
            </div>
        @else
            <div style="page-break-inside:avoid">
                <div class="footer-color p" style="margin-top: 20px;">
                    <strong>{{$sign->type}}</strong>
                </div>
                <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 10px 0 0 7px;">
                    @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                        {{$sign->user}}
                    @endif
                </div>
                <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px; padding: 2px">
                    Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                </div>
            </div>
        @endif
    @endforeach
</body>
</html>
