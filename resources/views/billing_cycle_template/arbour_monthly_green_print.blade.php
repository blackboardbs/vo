<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>CF Template Weekly Detail</title>
    <style>
        body{
            margin:20px 10px;
            font-family:"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:0.75rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff
        }
        .table,hr{
            margin-bottom:1rem
        }
        .container{
            width:98%;
            padding-right:7.5px;
            padding-left:7.5px;
            margin-right:auto;
            margin-left:auto
        }
        hr{
            margin-top:1rem;
            border:0;
            border-top:1px solid rgba(0,0,0,.1)
        }
        .table-responsive{
            display:block;
            width:100%;
            overflow-x:auto;
            -webkit-overflow-scrolling:touch;
            -ms-overflow-style:-ms-autohiding-scrollbar
        }
        .bg-dark,.bg-dark a{
            color:#fff!important
        }
        .p-2{
            padding:.5rem!important
        }
        .bg-dark{
            background-color:#343a40!important
        }
        .col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{
            position:relative;
            width:100%;
            min-height:1px;
            padding-right:7.5px;
            padding-left:7.5px
        }
        .row{
            display:flex;
            flex-wrap:wrap;
            margin-right:-7.5px;
            margin-left:-7.5px
        }
        .no-gutters{
            margin-right:0;
            margin-left:0
        }
        .no-gutters>.col,.no-gutters>[class*=col-]{
            padding-right:0;
            padding-left:0
        }
        *,::after,::before{
            box-sizing:border-box
        }
        .text-right{
            text-align:right!important
        }
        .table{
            width:100%;
            max-width:100%;
            background-color:transparent
        }
        table{
            border-collapse:collapse
        }
        .table td,.table th{
            padding:.75rem;
            vertical-align:top;
            border-top:1px solid #dee2e6
        }
        th{
            text-align:inherit
        }
        .table-responsive>.table-bordered{
            border:0
        }.table-bordered,.table-bordered td,.table-bordered th{border:1px solid {{$custom_style->header_background??'#161860'}} }.table-sm td,.table-sm th{padding:.3rem}.table-borderless>tbody>tr>td,.table-borderless>tbody>tr>th,.table-borderless>tfoot>tr>td,.table-borderless>tfoot>tr>th,.table-borderless>thead>tr>td,.table-borderless>thead>tr>th{border:none}.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th{border:0}.text-center{text-align:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.text-uppercase{text-transform:uppercase!important}@media (min-width:768px){.table-responsive{overflow-x:inherit!important}.col-md-4{flex:0 0 33.333333%;max-width:33.333333%}.col-md-2{flex:0 0 16.666667%;max-width:16.666667%}.col-md-5{flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-1{flex:0 0 8.333333%;max-width:8.333333%}}
        .eim-template-heading{
            background-color: #161860;
            color: #ffffff;
        }

        .heading-color{
            background-color: {{$custom_style->header_background??'#4e5e35'}};
            color: {{$custom_style->header_color??'#fff'}};
            border: #0e0e0e solid 1px;
        }
        .detail-color{
            color: #000000;
            border: #0e0e0e solid 1px;
        }
        .bg-weekend{
            background-color:#9b9f5a!important;
        }
        .bg-project-name{
            background-color: #e8e5ca;
        }
        .r-border{
            border-right: #0e0e0e solid 1px!important;
        }
        .l-border{
            border-left: #0e0e0e solid 1px!important;
        }
        .footer-color{
            background-color: {{$custom_style->footer_background??'#161860'}};
            color: {{$custom_style->footer_color??'#FFFFFF'}};
        }

        .borderless td, .borderless th {
            border: none;
        }
        .border{
            border: 1px solid {{$custom_style->header_background??'#161860'}} ;
        }
        .border-bottom-0{
            border-bottom: 0!important;
        }
        .resource-col{
            display: inline-block;
            margin-top: 5px;
        }
        .resource-col-3{
            width: 33%;
        }
        .resource-col-2{
            width: 48%;
            margin-right: 15px
        }
        .resource-p-m-3{
            padding: 3px 10px;
        }
        .resource-signature{
            border: 1px solid {{$custom_style->header_background??'#161860'}} ;
            height: 30px;
        }
        .responsive {
            max-width: 100%;
            height: auto;
            vertical-align: {{$custom_style->logos_vertical_alignment??'top'}};
        }
        .logos{
            display: inline-block;
            width: {{((100-1)/(!count($logos)?1:count($logos)))}}%;
            margin-top: 5px;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini" style="line-height: 1.24em;">
<div id="app" class="wrapper">
    <div class="container-fluid">
        <div id="timesheets_div">
            <div style="align-items: center">
                <div style="display: inline-block; width: 49%">
                    <img src="{{public_path("/assets/arbour_logo.png")}}" alt="" style="height: 80px; width: auto">
                </div>
                <div style="display: inline-block; width: 49%; padding-left: 64px">
                    <strong style="font-size: 1.25rem; color: #808080;padding-bottom: 0.5rem;">Timesheet</strong>
                </div>
            </div>
            {{--@forelse($logos as $logo)
                <div class="logos">
                    @if(is_file($logo))
                        <img src="{{$logo}}" alt="" class="responsive">
                    @endif
                </div>
            @empty
                <p>There's no logo selected</p>
            @endforelse--}}
            {!! $custom_style->comment??null !!}
                @php
                    $to_array = explode("-", $monthly_data->to);
                    $from_array = explode("-", $monthly_data->from);
                @endphp
            <table style="font-size: 1rem; margin: 1rem 0">
                <tr>
                    <td style="padding-right: 3rem;padding-bottom: 0.5rem">Name: </td><th style="padding-bottom: 0.5rem">{{$monthly_data->name}}</th><th style="padding-bottom: 0.5rem"></th>
                </tr>
                <tr>

                    <td style="padding-right: 1rem; padding-bottom: 0.5rem">Billing Period: </td>
                    <th style="padding-bottom: 0.5rem">{{$monthly_data->billing_period}}</th>
                    <td style="padding-left: 4rem; padding-bottom: 0.5rem">From: </td><th style="padding-bottom: 0.5rem">{{date('d-M-y', mktime(0, 0,0,$from_array[1], $from_array[2], $from_array[0]))}}</th>
                </tr>
                <tr>
                    <td style="padding-right: 3rem; padding-bottom: 0.5rem">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><th style="padding-bottom: 0.5rem">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th><td style="padding-left: 4rem; padding-bottom: 0.5rem">To:</td><th style="padding-bottom: 0.5rem">{{date('d-M-y', mktime(0, 0,0,$to_array[1], $to_array[2], $to_array[0]))}}</th>
                </tr>
            </table>
            <table style="font-size: 13px" class="table table-borderless table-sm mt-3">
                <tbody>
                    <tr class="heading-color">
                        <th style="width: 15%; vertical-align: middle; padding: 8px;border-right: #0e0e0e solid 1px;">Date</th><th style="white-space: nowrap; vertical-align: middle;border-right: #0e0e0e solid 1px;">PO nr/Call number</th><th style="vertical-align: middle;border-right: #0e0e0e solid 1px;">Project name</th><th style="vertical-align: middle;border-right: #0e0e0e solid 1px;">Description</th><th style="vertical-align: middle;border-right: #0e0e0e solid 1px;" class="text-right">Hours</th><th style="vertical-align: middle;border-right: #0e0e0e solid 1px;" class="text-right">Billable</th><th style="vertical-align: middle;border-right: #0e0e0e solid 1px;" class="text-right">Non Billable</th>
                    </tr>
                    @foreach($monthly_data->monthly_data as $data)
                        <tr class="detail-color {{Carbon\Carbon::parse($data->date)->isWeekend()?'bg-weekend':''}}">
                            <td class="r-border">{{$data->date}}</td>
                            <td class="r-border" style="white-space: nowrap">{{$data->project_ref}}</td>
                            <td class="r-border {{Carbon\Carbon::parse($data->date)->isWeekday()?'bg-project-name':''}}">{{$data->project_name}}</td>
                            <td class="r-border">{{substr($data->description,0,100)}}{{(mb_strlen($data->description) > 100)?'...':null}}</td>
                            <td class="r-border text-right">{{number_format($data->hours/60,2,'.',',')}}</td>
                            <td class="r-border text-right">{{number_format($data->billable_hours/60,2,'.',',')}}</td>
                            <td class="r-border text-right">{{number_format($data->non_billable/60,2,'.',',')}}</td>
                        </tr>
                    @endforeach
                        <tr class="r-border l-border">
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr style="background-color: #4e5e35; border: #0e0e0e solid 1px; color: #fff;">
                            <th colspan="2" class="r-border">TOTAL (billable hours)</th><th class="r-border"></th><th class="r-border"></th><th class="text-right r-border">{{number_format((collect($monthly_data->monthly_data)->sum('hours')/60),2,'.',',')}}</th><th class="text-right r-border">{{number_format((collect($monthly_data->monthly_data)->sum('billable_hours')/60),2,'.',',')}}</th><td></td>
                        </tr>
                        <tr style="background-color: #4e5e35; border: #0e0e0e solid 1px; color: #fff;">
                            <th colspan="2" class="r-border">TOTAL (non billable hours)</th><th class="r-border"></th><th class="r-border"></th><td class="r-border"></td><td class="r-border"></td><th class="text-right r-border">{{number_format((collect($monthly_data->monthly_data)->sum('non_billable')/60),2,'.',',')}}</th>
                        </tr>
                </tbody>
            </table>

            @foreach($digisign as $digi_sign)
                <table class="table table-borderless" style="font-size: 1rem; margin-top: 60px">
                    <tr>
                        <td style="padding: 10px 5px 5px 5px; width: 10%">{{$digi_sign->type}}:</td>
                        <td style="padding: 10px 5px 5px 5px;border-bottom: 1px solid #000000; width: 30%"></td>
                        <td style="padding: 10px 5px 5px 5px;width: 20%"></td>
                        <td style="padding: 10px 5px 5px 5px; width: 10%">Date:</td>
                        <td style="width: 30%; border-bottom: 1px solid #000000;padding: 10px 5px 5px 5px"></td>
                    </tr>
                </table>
                {{-- @if(sizeof($digisign) >= 3)
                    <div class="resource-col resource-col-3 fo">
                        <div class="resource-p-m-3 border border-bottom-0 footer-color">
                            <strong>{{$digi_sign->type}}</strong>
                        </div>
                        <div class="resource-signature">

                        </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date:</div>
                        <strong>{{$digi_sign->user??null}}</strong>
                    </div>
                @elseif(sizeof($digisign) == 2)
                    <div class="resource-col resource-col-2">
                        <div class="resource-p-m-3 border border-bottom-0 footer-color">
                            <strong>{{$digi_sign->type}}</strong>
                        </div>
                        <div class="resource-signature">

                        </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date:</div>
                        <strong>{{$digi_sign->user??null}}</strong>
                    </div>
                @else
                    <div class="resource-p-m-3 border border-bottom-0 footer-color">
                        <strong>{{$digi_sign->type}}</strong>
                    </div>
                    <div class="resource-signature">

                    </div>
                        <div style="border-left: 1px solid {{$custom_style->header_background??'#161860'}};border-bottom: 1px solid {{$custom_style->header_background??'#161860'}};border-right: 1px solid {{$custom_style->header_background??'#161860'}};margin-bottom: 10px;padding: 2px; height: 20px;">Date:</div>
                    <strong>{{$digi_sign->user??null}}</strong>
                @endif--}}
            @endforeach
        </div>
    </div>
</div>
</body>
</html>
