<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$user->name()}}</title>
    <style>
        @font-face {
            font-family: 'alexbrush';
            src: url("{{public_path('fonts/alexbrush.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @font-face {
            font-family: 'openSans';
            src: url("{{public_path('fonts/OpenSans-Regular-webfont.woff')}}") format('woff');
            font-weight: normal;
            font-style: normal;

        }
    </style>
    <style>
        body{
            margin:20px 10px;
            font-family:'openSans',"Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            font-size:0.75rem;
            font-weight:400;
            line-height:1.5;
            color:#212529;
            text-align:left;
            background-color:#fff;
        }
        .heading-color{
            background-color: {{$custom_style->header_background??'#161860'}};
            color: {{$custom_style->header_color??'#FFFFFF'}};
        }
        .detail-color{
            background-color: {{$custom_style->detail_background??'#FFFFFF'}};
            color: {{$custom_style->detail_color??'#000000'}};
        }
        .footer-color{
            background-color: {{$custom_style->footer_background??'#161860'}};
            color: {{$custom_style->footer_color??'#FFFFFF'}};
        }
        .heading{
            font-size: 14px;
            width: 33%;
        }
        .p{
            padding: 3px 10px;
        }
        .row{
            display: inline-block;
            width: 33%;
            margin-top: 5px;
        }
        .logos{
            display: inline-block;
            width: {{((100-1)/(!count($logos)?1:count($logos)))}}%;
            margin-top: 5px;
        }
        .table{
            width: 100%;
            border-collapse: collapse;
        }
        .responsive {
            max-width: 100%;
            height: auto;
            vertical-align: {{$custom_style->logos_vertical_alignment??'top'}};
        }
        .resource-signature{
            font-size: 26px;
            font-family: 'alexbrush', Arial, sans-serif;
            line-height: 1.5;
        }
    </style>
</head>
<body>
    @forelse($logos as $logo)
        <div class="logos">
            @if(is_file($logo))
                <img src="{{$logo}}" alt="" class="responsive">
            @endif
        </div>
    @empty
        <p>There's no logo selected</p>
    @endforelse


    {!! $custom_style->comment??null !!}
    <table class="table" style="margin-top: 15px;">
        @if(isset($custom_style->is_billing_period_included))
            <tr>
                <th class="heading-color heading p">Timesheet for:</th>
                <th class="p">{{Str::contains($billing_period->period_name, $billing_period->year)?$billing_period->period_name:($billing_period->period_name." ".$billing_period->year)}}</th>
            </tr>
            <tr>
                <th class="heading-color heading p">Billing Period:</th>
                <th class="p">{{$billing_period->start_date}} - {{$billing_period->end_date}}</th>
            </tr>
        @endif
        <tr>
            <th class="heading-color heading p">{{$custom_style->resource_label??"Resource Name"}}:</th>
            <th class="p">{{$user->name()}}</th>
        </tr>
        <tr>
            <th class="heading-color heading p">{{$custom_style->project_label??"Project Name"}}:</th>
            <th class="p">{{$project->project->name??null}}</th>
        </tr>
        <tr>
            <th class="heading-color heading p">{{$custom_style->po_number_label??"PO Number"}}:</th>
            <th class="p">{{$project->customer_po}}</th>
        </tr>
    </table>

    <div style="margin-top: 15px; width: 100%;">
        @foreach($week_days as $key => $day)
            <div class="row">
                <table class="table">
                    <thead class="heading-color">
                        <tr>
                            <th class="p" colspan="3" style="text-align: center">Week {{$key + 1}}</th>
                        </tr>
                        <tr>
                            <td class="p">Date</td>
                            <td class="p">Day</td>
                            <td class="p">Hours</td>
                        </tr>
                    </thead>
                    <tbody class="detail-color">
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["monday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDay()->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDay()->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["tuesday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDays(2)->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDays(2)->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["wednesday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDays(3)->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDays(3)->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["thursday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDays(4)->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDays(4)->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["friday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDays(5)->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDays(5)->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["saturday"])}}</td>
                        </tr>
                        <tr>
                            <td class="p">{{$day["start_date"]->copy()->addDays(6)->format("d-M-y")}}</td>
                            <td class="p">{{$day["start_date"]->copy()->addDays(6)->format("l")}}</td>
                            <td class="p">{{_minutes_to_time($day["sunday"])}}</td>
                        </tr>
                        <tr class="heading-color">
                            <th class="p" colspan="2">Week Total</th>
                            <th class="p">{{_minutes_to_time($day["total"])}}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
        <div class="heading-color p" style="margin-top: 20px">
            <div><strong>Total Billing Hours <span style="float: right">{{_minutes_to_time($week_days->sum('total'))}}</span></strong></div>
            <div><strong>Total Billing Hours In Decimal <span style="float: right">{{number_format(($week_days->sum('total')/60),2,',','.')}}</span></strong></div>
        </div>

        @foreach($digisign as $sign)
                @if(sizeof($digisign) >= 3)
                    <div class="row">
                        <div class="footer-color p" style="margin-top: 20px">
                            <strong>{{$sign->type}}</strong>
                        </div>
                        <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 15px 0 0 7px;">
                            @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                                {{$sign->user}}
                            @endif
                        </div>
                        <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px">
                            Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                        </div>
                    </div>
                @elseif(sizeof($digisign) == 2)
                    <div class="row" style="width: 48%; margin-right: 10px">
                        <div class="footer-color p" style="margin-top: 20px">
                            <strong>{{$sign->type}}</strong>
                        </div>
                        <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 15px 0 0 7px;">
                            @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                                {{$sign->user}}
                            @endif
                        </div>
                        <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px; padding: 2px">
                            Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                        </div>
                    </div>
                @else
                    <div class="footer-color p" style="margin-top: 20px;">
                        <strong>{{$sign->type}}</strong>
                    </div>
                    <div class="resource-signature" style="border: 1px solid black; height: 40px; padding: 10px 0 0 7px;">
                        @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed))
                            {{$sign->user}}
                        @endif
                    </div>
                    <div style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; height: 20px; margin-bottom: 10px; padding: 2px">
                        Date: @if($sign->type == "Resource" && (isset($custom_style->is_signed) && $custom_style->is_signed)){{now()->toDateString()}} @endif
                    </div>
                @endif
        @endforeach

    </div>
</body>
</html>
