<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Status</th>
        <th>Added</th>
    </tr>
    </thead>
    <tbody>
    @foreach($resource as $referrer)
        <tr>
            <td>{{ $referrer->user?->name() }}</td>
            <td>{{$referrer->user?->email}}</td>
            <td>{{$referrer->user?->phone}}</td>
            <td>{{$referrer->status?->description}}</td>
            <td>{{$referrer->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>