@extends('adminlte.default')

@section('title') Edit Resource @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
    <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
    <a href="javascript:void(0)" onclick="saveForm('editResource')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('resource.update',$resource->id), 'method' => 'patch','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'editResource'])}}
            <table class="table table-bordered table-sm">
                <thead class="thead-light">
                </thead>
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Personal Detail</th>
                    </tr>
                    <tr>
                        <th rowspan="10" class="col-md-3">Photo:</th>
                        <td rowspan="10" class="col-md-3">
                            <img src="{{route('user_avatar',['q'=>$resource->user?->avatar])}}" class="blackboard-avatar blackboard-avatar-profile"/>
                        </td>
                        <th class="col-md-3">Resource:</th>
                        <td class="col-md-3">{{ $resource->user?->name()}}</td>
                    </tr>
                    <tr>
                        <th>Prefered Name:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->resource_pref_name }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>ID:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->id_no }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif </td>
                    </tr>
                    <tr>
                        <th>Middle Name</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->resource_middle_name }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $resource->user?->email }}</td>
                    </tr>
                    <tr>
                        <th>Cell:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->cell }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Cell 2:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->cell2 }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>{{ $resource->user?->phone }}</td>
                    </tr>
                    <tr>
                        <th>Home Phone:</th>
                        <td>{{Form::text('home_phone',old('home_phone'),['class'=>'form-control form-control-sm'. ($errors->has('home_phone') ? ' is-invalid' : ''),'placeholder'=>'Home Phone'])}}
                            @foreach($errors->get('home_phone') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Date Of Birth:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->date_of_birth }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Status <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('status',$status_dropdown, $resource->status_id,['class'=>'form-control form-control-sm '. ($errors->has('status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Emergency Contact:</th>
                        <td>{{ $resource->user->userOnboarding?->contactDetails?->next_of_kin_name }}</td>
                    </tr>
                    <tr>
                        <th>NDA Template</th>
                        <td>{{Form::select('nda_template_id',$nda_template_dropdown, $resource->nda_template_id,['class'=>'form-control form-control-sm '. ($errors->has('nda_template_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('nda_template_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Emergency Contact Phone:</th>
                        <td>{{ $resource->user->userOnboarding?->contactDetails?->next_of_kin_number }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Position Information</th>
                    </tr>
                    <tr>
                        <th class="col-md-3">Position:</th>
                        <td class="col-md-3">{{Form::select('position',$resource_position_dropdown, $resource->resource_position ,['class'=>'form-control form-control-sm '. ($errors->has('position') ? ' is-invalid' : ''), 'placeholder' => "Select Position"])}}
                            @foreach($errors->get('position') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th class="col-md-3">Resource Number:</th>
                        <td class="col-md-3">{{Form::text('resource_number',$resource->resource_no,['class'=>'form-control form-control-sm'. ($errors->has('resource_number') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('resource_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Resource Level:</th>
                        <td>{{Form::select('resource_level',$resource_level_dropdown,$resource->resource_level,['class'=>'form-control form-control-sm '. ($errors->has('resource_level') ? ' is-invalid' : ''), 'placeholder' => 'Select Resource Level'])}}
                            @foreach($errors->get('resource_level') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Resource Type:</th>
                        <td>{{Form::select('resource_type',$resource_type_dropdown, $resource->resource_type, ['class'=>'form-control form-control-sm '. ($errors->has('resource_type') ? ' is-invalid' : ''), 'placeholder' => 'Select Resource Type'])}}
                            @foreach($errors->get('resource_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Standard Cost Bracket:</th>
                        <td>{{Form::select('standard_cost_bracket',$assignment_standard_cost_dropdown,$resource->standard_cost_bracket,['class'=>'form-control form-control-sm '. ($errors->has('standard_cost_bracket') ? ' is-invalid' : ''), 'placeholder' => 'Select Standard Cost Center'])}}
                            @foreach($errors->get('standard_cost_bracket') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Commission:</th>
                        <td>{{Form::select('commission',$dropdown_process_commission,$resource->resource_level,['class'=>'form-control form-control-sm '. ($errors->has('commission') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('commission') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Join Date:</th>
                        <td>{{Form::text('join_date',$resource->join_date,['class'=>'form-control form-control-sm datepicker '. ($errors->has('join_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('join_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Termination Date:</th>
                        <td>{{Form::text('termination_date',$resource->termination_date ? \Carbon\Carbon::parse($resource->termination_date)->format('Y-m-d') :  '',['class'=>'form-control form-control-sm datepicker '. ($errors->has('termination_date') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('termination_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Technical Rating: </th>
                        <td>{{Form::select('technical_rating_id',$technical_rating_dropdown,$resource->technical_rating_id ,['class'=>'form-control form-control-sm '. ($errors->has('technical_rating_id') ? ' is-invalid' : ''), 'placeholder' => 'Technical Rating'])}}
                            @foreach($errors->get('technical_rating_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Cost Center: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                        <td>{{Form::select('cost_center',$cost_center_dropdown,$resource->cost_center_id,['class'=>'form-control form-control-sm '. ($errors->has('cost_center') ? ' is-invalid' : ''), 'placeholder' => 'Select Cost Center'])}}
                            @foreach($errors->get('cost_center') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Business Skills:</th>
                        <td>{{Form::select('business_skill_id',$business_rating_dropdown, $resource->business_skill_id,['class'=>'form-control form-control-sm '. ($errors->has('business_skill_id') ? ' is-invalid' : ''), 'placeholder' => 'Business Skill'])}}
                            @foreach($errors->get('business_skill_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Report to:</th>
                        <td>{{Form::select('report_to',$users_dropdown, $resource->manager_id,['class'=>'form-control form-control-sm '. ($errors->has('report_to') ? ' is-invalid' : ''), 'placeholder' => 'Select Manager'])}}
                            @foreach($errors->get('report_to') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Team:</th>
                        <td>{{Form::select('team',$team_dropdown, $resource->team_id,['class'=>'form-control form-control-sm '. ($errors->has('team') ? ' is-invalid' : ''), 'placeholder' => 'Select Team'])}}
                            @foreach($errors->get('team') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Job Information</th>
                    </tr>
                    <tr>
                        <th>Utilization:</th>
                        <td><input type="checkbox" name="util" {{($resource->util == '1' ? 'checked' : '')}} /></td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Utilization From:</th>
                        <td>{{Form::text('util_date_from',$resource->util_date_from,['class'=>'datepicker form-control form-control-sm'. ($errors->has('util_date_from') ? ' is-invalid' : ''),'placeholder'=>'Utilization From'])}}</td>
                        <th>Utilization To:</th>
                        <td>{{Form::text('util_date_to',$resource->util_date_to,['class'=>'datepicker form-control form-control-sm'. ($errors->has('util_date_to') ? ' is-invalid' : ''),'placeholder'=>'Utilization To'])}}</td>
                    </tr>
                    <tr>
                        <th>Project Type:</th>
                        <td>{{Form::select('project_type',$project_type_dropdown,$resource->project_type,['class'=>'form-control form-control-sm '. ($errors->has('project_type') ? ' is-invalid' : ''), 'placeholder' => 'Select Project Type'])}}
                            @foreach($errors->get('project_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th>Medical Certificate Type:</th>
                        <td>{{Form::select('medical_certificate_type',$dropdown_process_medical_certificate_type,$resource->medical_certificate_type,['class'=>'form-control form-control-sm '. ($errors->has('medical_certificate_type') ? ' is-invalid' : ''),'id'=>'dropdown_process_marriage'])}}
                            @foreach($errors->get('medical_certificate_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                        <th>Medical Certificate Expiry:</th>
                        <td>{{Form::text('medical_certificate_expiry',$resource->medical_certificate_expiry,['class'=>'datepicker form-control form-control-sm'. ($errors->has('medical_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'Medical Certificate Expiry'])}}
                            @foreach($errors->get('medical_certificate_expiry') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach</td>
                    </tr>


                        {{Form::hidden('leave_bal', $leave_bal)}}
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="6">Payment Details</th>
                    </tr>
                    <tr>
                        <th class="col-md-2">Type Of Account</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->bankAccountType?->description }}</td>
                        <th class="col-md-2">Account Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->account_name }}</td>
                        <th class="col-md-2">Account Holder Relation</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->relationshipType?->description }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Bank Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->bank_name }}</td>
                        <th class="col-md-2">Branch Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->branch_number }}</td>
                        <th class="col-md-2">Branch Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->branch_name }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Account Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->account_number }}</td>
                        <th class="col-md-2"></th>
                        <td class="col-md-2"></td>
                        <th class="col-md-2"></th>
                        <td class="col-md-2"></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="6">SARS Details</th>
                    </tr>
                    <tr>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[0]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[1]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[2]??null) }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[3]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[4]??null) }}</td>
                        <th class="col-md-2">Income Tax Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->income_tax_number }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="2">Documentation</th>
                    </tr>
                    <tr>
                        <th>ID/Passport:</th>
                        <td>
                            @if($resource->user->userOnboarding?->id_passport)
                                <a href="{{route('id_passport', ['q' => $resource->user->userOnboarding?->id_passport])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>

                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Proof of Address:</th>
                        <td>
                            @if($resource->user->userOnboarding?->proof_of_address)
                                <a href="{{route('proof_of_address', ['q' => $resource->user->userOnboarding?->proof_of_address])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Proof of Bank Account:</th>
                        <td>
                            @if($resource->user->userOnboarding?->paymentDetails?->proof_of_bank_account)
                                <a href="{{route('proof_of_bank_account', ['q' => $resource->user->userOnboarding?->paymentDetails?->proof_of_bank_account])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
        </div>
@endsection