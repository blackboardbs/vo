@extends('adminlte.default')

@section('title') Resource @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="resource.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if (\auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']))
            <form class="form-inline mt-3 searchform" id="searchform">
                <div class="col-sm-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('resource_type_id', $resource_type_dropdown, old('resource_type_id', 0), ['class' => 'form-control w-100 search', 'id' => 'resource_type_id', 'placeholder' => 'All'])}}
                            <span>Resource Type</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('position_id', $resource_position_dropdown, old('position_id', 0), ['class' => 'form-control w-100 search', 'id' => 'position_id', 'placeholder' => 'All'])}}
                            <span>Position</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('level_id', $resource_level_dropdown, old('level_id', 0), ['class' => 'form-control w-100 search', 'id' => 'level_id', 'placeholder' => 'All'])}}
                            <span>Level</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('standard_cost_bracket_id', $assignment_standard_cost_dropdown, old('standard_cost_bracket_id', 0), ['class' => 'form-control w-100 search', 'id' => 'standard_cost_bracket_id', 'placeholder' => 'All'])}}
                            <span>Standard Cost Bracket</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="max-width:20% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            {{Form::select('team_id', $team_dropdown, old('team_id', 0), ['class' => 'form-control w-100 search', 'id' => 'team_id'])}}
                            <span>Team</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 pt-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('status_id', $status_dropdown, old('status_id', 0), ['class' => 'form-control w-100 search', 'id' => 'status_id', 'placeholder' => 'All'])}}
                    <span>Status</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 pt-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('util', $yes_or_no_dropdown, request()->util, ['class' => 'form-control w-100 search', 'placeholder' => "All", 'id' => 'util'])}}
                    <span>Utilization</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 pt-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('cost_center_id', $cost_center_dropdown, old('cost_center_id'), ['class' => 'form-control w-100 search', 'placeholder' => "All", 'id' => 'cost_center_id'])}}
                    <span>Cost Center</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 pt-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                    {{Form::select('report_to_id', $managers, old('report_to_id'), ['class' => 'form-control w-100 search', 'placeholder' => 'All'])}}
                    <span>Report To</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 pt-3" style="max-width:16% !important;">
                    <div class="form-group input-group">
                        <label class="has-float-label">
                            <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" />
                            <span>Matching</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-3 pt-3" style="max-width:20% !important;">
                    <a href="{{route('resource.index')}}" class="btn btn-info w-100">Clear Filters</a>
                </div>
        </form>
        @endif

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('user.first_name', 'Name')</th>
                <th>@sortablelink('user.email', 'Email')</th>
                <th>@sortablelink('user.phone', 'Phone')</th>
                <th>@sortablelink('status.description', 'Status')</th>
                <th>@sortablelink('created_at', 'Added')</th>
                @if($can_update)
                    <th class="last">Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @forelse($resource as $referrer)
                <tr>
                    <td><a href="{{route('resource.show',$referrer->id)}}">{{ $referrer->user != null ?$referrer->user->first_name : 'FirstName'}} {{$referrer->user != null ? $referrer->user->last_name : 'LastName'}}</a></td>
                    <td><a href="mailto:{{isset($referrer->user->email)?$referrer->user->email:'Email'}}">{{isset($referrer->user->email)?$referrer->user->email:'Email'}}</a></td>
                    <td>{{$referrer->user != null ? $referrer->user->phone : ''}}</td>
                    <td>{{isset($referrer->status->description)?$referrer->status->description:'Status'}}</td>
                    <td>{{$referrer->created_at}}</td>
                    @if($can_update)
                        <td>
                            <div class="d-flex">
                            <a href="{{route('resource.edit',$referrer->id)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['resource.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm" {{$referrer->team_count > 0 || count($referrer->user->timesheets) > 0 || count($referrer->user->tasks) > 0 || count($referrer->user->expense_tracking) > 0 || count($referrer->user->assignments) > 0 || count($referrer->user->invoice) > 0 || count($referrer->user->vinvoice) > 0 || $referrer->cv ? 'disabled' : ''}}><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No referrers match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $resource->firstItem() }} - {{ $resource->lastItem() }} of {{ $resource->total() }}
                    </td>
                    <td>
                        {{ $resource->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
@endsection
@section('extra-css')
    <style>
        .list{
            display: none;
            z-index: 1000;
        }
        .data-list:hover{
            background-color: #0e90d2;
            color: #f9f9f9!important;
        }
    </style>
@endsection
