@extends('adminlte.default')

@section('title') View Resource @endsection

@section('header')
    <div class="container-fluid container-title">
    <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            @if($can_update)
                <a href="{{route('resource.edit',$resource->id)}}" class="btn btn-success float-right mr-1"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
            @endif
            <div class="dropdown mr-1">
                <button class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-file" aria-hidden="true"></i> Documents
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" target="_blank" href="{{route('resource.print', $resource)}}"><i class="fa fa-print" aria-hidden="true"></i> Print NDA</a>
                    <a class="dropdown-item" href="{{route('resource.send', $resource)}}"><i class="fa fa-envelope" aria-hidden="true"></i> Send NDA</a>
                </div>
            </div>
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive mt-3">
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Personal Detail</th>
                    </tr>
                    <tr>
                        <th rowspan="10" class="col-md-3">Photo:</th>
                        <td rowspan="10" class="col-md-3 text-center"><img src="{{route('user_avatar',['q'=>$resource->user?->avatar])}}" class="blackboard-avatar blackboard-avatar-profile"/><br /></td>
                        <th class="col-md-3">Resource:</th>
                        <td class="col-md-3">{{$resource->user?->first_name}} {{$resource->user?->last_name}}</td>
                    </tr>
                    <tr>
                        <th>Prefered Name:</th>
                        <td>{{$resource->cv?->resource_pref_name}}</td>
                    </tr>
                    <tr>
                        <th>ID:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->id_no }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif </td>
                    </tr>
                    <tr>
                        <th>Middle Name</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->resource_middle_name }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $resource->user?->email }}</td>
                    </tr>
                    <tr>
                        <th>Cell:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->cell }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Cell 2:</th>
                        <td>@if($resource->cv != null)  {{ $resource->cv->cell2 }} @else <a href="{{ route("cv.user") }}">{{ $msg }}</a> @endif</td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>{{ $resource->user?->phone }}</td>
                    </tr>
                    <tr>
                        <th>Home Phone:</th>
                        <td>{{$resource->home_phone}}</td>
                    </tr>
                    <tr>
                        <th>Date Of Birth:</th>
                        <td>{{ $resource->cv?->date_of_birth }}</td>
                    </tr>
                    <tr>
                        <th>Status:</th>
                        <td>{{$resource->status?->description}}</td>
                        <th>Emergency Contact:</th>
                        <td>{{ $resource->user->userOnboarding?->contactDetails?->next_of_kin_name }}</td>
                    </tr>
                    <tr>
                        <th>NDA Template</th>
                        <td>{{$resource->NDATemplate?->name}}</td>
                        <th>Emergency Contact Phone:</th>
                        <td>{{ $resource->user->userOnboarding?->contactDetails?->next_of_kin_number }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Position Information</th>
                    </tr>
                    <tr>
                        <th class="col-md-3">Position:</th>
                        <td class="col-md-3">{{$resource->resource_position_desc?->description}}</td>
                        <th class="col-md-3">Resource Number:</th>
                        <td class="col-md-3">{{ $resource->resource_no }}</td>
                    </tr>
                    <tr>
                        <th>Resource Level:</th>
                        <td>{{$resource->resource_level_desc?->description}}</td>
                        <th>Resource Type:</th>
                        <td>{{ $resource->resource_type_desc?->description}}</td>
                    </tr>
                    <tr>
                        <th>Standard Cost Bracket</th>
                        <td>{{ $resource->standard_cost?->description }}</td>
                        <th>Commission</th>
                        <td>{{ $resource->commission?->description }}</td>
                    </tr>
                    <tr>
                        <th>Join Date:</th>
                        <td>{{ $resource->join_date }}</td>
                        <th>Termination Date:</th>
                        <td>{{ $resource->termination_date ? \Carbon\Carbon::parse($resource->termination_date)->format('Y-m-d') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Technical Rating:</th>
                        <td>{{ $resource->technical_rating?->name }}</td>
                        <th>Cost Centre:</th>
                        <td>{{ $resource->cost_center?->description}}</td>
                    </tr>
                    <tr>
                        <th>Business Skills:</th>
                        <td>{{ $resource->business_skill?->name }}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Report to:</th>
                        <td>{{$resource->manager?->name()}}</td>
                        <th>Team:</th>
                        <td>{{ $resource->team?->team_name }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="4">Job Information</th>
                    </tr>
                    <tr>
                        <th class="col-md-3">Utilization:</th>
                        <td class="col-md-3">{{Form::checkbox('', $resource->util, $resource->util == 0 ? false : true)}}</td>
                        <th class="col-md-3"></th>
                        <td class="col-md-3"></td>
                    </tr>
                    <tr>
                        <th>Utilization From:</th>
                        <td>{{$resource->util_date_from}}</td>
                        <th>Utilization To:</th>
                        <td>{{$resource->util_date_to}}</td>
                    </tr>
                    <tr>
                        <th>Project Type:</th>
                        <td>{{ $resource->projecttype?->description }}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Medical Certificate Type:</th>
                        <td>{{$resource->medical_cert?->description}}</td>
                        <th>Medical Certificate Expiry:</th>
                        <td>{{$resource->medical_certificate_expiry}}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="6">Payment Details</th>
                    </tr>
                    <tr>
                        <th class="col-md-2">Type Of Account</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->bankAccountType?->description }}</td>
                        <th class="col-md-2">Account Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->account_name }}</td>
                        <th class="col-md-2">Account Holder Relation</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->relationshipType?->description }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Bank Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->bank_name }}</td>
                        <th class="col-md-2">Branch Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->branch_number }}</td>
                        <th class="col-md-2">Branch Name</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->branch_name }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Account Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->paymentDetails?->account_number }}</td>
                        <th class="col-md-2"></th>
                        <td class="col-md-2"></td>
                        <th class="col-md-2"></th>
                        <td class="col-md-2"></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="6">SARS Details</th>
                    </tr>
                    <tr>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[0]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[1]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[2]??null) }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[3]??null) }}</td>
                        <th class="col-md-2">Directive Number</th>
                        <td class="col-md-2">{{ (explode(',',$resource->user->userOnboarding?->directive_number)[4]??null) }}</td>
                        <th class="col-md-2">Income Tax Number</th>
                        <td class="col-md-2">{{ $resource->user->userOnboarding?->income_tax_number }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-sm mt-2">
                <tbody>
                    <tr>
                        <th class="bg-dark" colspan="2">Documentation</th>
                    </tr>
                    <tr>
                        <th>ID/Passport:</th>
                        <td>
                            @if($resource->user->userOnboarding?->id_passport)
                                <a href="{{route('id_passport', ['q' => $resource->user->userOnboarding?->id_passport])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>

                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Proof of Address:</th>
                        <td>
                            @if($resource->user->userOnboarding?->proof_of_address)
                                <a href="{{route('proof_of_address', ['q' => $resource->user->userOnboarding?->proof_of_address])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Proof of Bank Account:</th>
                        <td>
                            @if($resource->user->userOnboarding?->paymentDetails?->proof_of_bank_account)
                                <a href="{{route('proof_of_bank_account', ['q' => $resource->user->userOnboarding?->paymentDetails?->proof_of_bank_account])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                            @else
                                No document was uploaded.
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
