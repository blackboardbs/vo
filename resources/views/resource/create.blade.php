@extends('adminlte.default')

@section('title') Add Resource @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('resource')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('resource.store'), 'method' => 'post','class'=>'mt-3','files'=>true, 'id' => 'resource','autocomplete'=>'off'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                <tr>
                    <th colspan="4">Complete the form below.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="7">Photo:</th>
                    <td rowspan="7">
                        <img src="{{route('resource_avatar',['q'=>Auth::user()->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/><br />
                        {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                        @foreach($errors->get('avatar') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach
                        <small id="avatar" class="form-text text-muted">
                            Images will be cropped to 200x200
                        </small>
                    </td>
                    <th>Resource No:</th>
                    <td>{{Form::text('emp_no',old('emp_no'),['class'=>'form-control form-control-sm'. ($errors->has('emp_no') ? ' is-invalid' : ''),'placeholder'=>'Employee No'])}}
                        @foreach($errors->get('emp_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>ID:</th>
                    <td>{{Form::text('emp_id_no',old('emp_id_no'),['class'=>'form-control form-control-sm'. ($errors->has('emp_id_no') ? ' is-invalid' : ''),'placeholder'=>'ID Number'])}}
                        @foreach($errors->get('emp_id_no') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Employee Name:</th>
                    <td>{{Form::select('user_id', $resource,old('user_id'),['class'=>'employee_id form-control form-control-sm '. ($errors->has('user_id') ? ' is-invalid' : ''),'placeholder'=>'Select Employee'])}}
                        @foreach($errors->get('user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Vendor Name</th>
                    <td>{{Form::select('vendor_id', $vendor,old('vendor_id'),['class'=>'vendor form-control form-control-sm '. ($errors->has('vendor_id') ? ' is-invalid' : ''),'placeholder'=>'Select Vendor','disabled'=>'disabled'])}}
                        @foreach($errors->get('user_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <!--
                <tr>
                    <th>Firstname:</th>
                    <td>{{Form::text('emp_firstname',old('emp_firstname'),['class'=>'form-control form-control-sm'. ($errors->has('emp_firstname') ? ' is-invalid' : ''),'placeholder'=>'Employee Firstname'])}}
                        @foreach($errors->get('emp_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Lastname:</th>
                    <td>{{Form::text('emp_lastname',old('emp_lastname'),['class'=>'form-control form-control-sm'. ($errors->has('emp_lastname') ? ' is-invalid' : ''),'placeholder'=>'Employee Lastname'])}}
                        @foreach($errors->get('emp_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr> -->
                <tr>
                    <th>Preferred Name:</th>
                    <td>{{Form::text('emp_prefname',old('emp_prefname'),['class'=>'form-control form-control-sm'. ($errors->has('emp_prefname') ? ' is-invalid' : ''),'placeholder'=>'Employee Prefered Name'])}}
                        @foreach($errors->get('emp_prefname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Join Date:</th>
                    <td>{{Form::text('join_date',old('join_date'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('join_date') ? ' is-invalid' : ''),'placeholder'=>'Join Date'])}}
                        @foreach($errors->get('join-date') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Employee Type:</th>
                    <td>{{Form::select('emp_type',$dropdown_process_type,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_type') ? ' is-invalid' : ''),'id'=>'dropdown_process_type'])}}
                        @foreach($errors->get('emp_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Position:</th>
                    <td>{{Form::select('emp_position',$dropdown_process_position,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_position') ? ' is-invalid' : ''),'id'=>'dropdown_process_position'])}}
                        @foreach($errors->get('emp_position') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Employee Level:</th>
                    <td>{{Form::select('emp_level',$dropdown_process_level,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_level') ? ' is-invalid' : ''),'id'=>'dropdown_process_level'])}}
                        @foreach($errors->get('emp_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Commission:</th>
                    <td>{{Form::select('emp_commision',$dropdown_process_commission,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_commision') ? ' is-invalid' : ''),'id'=>'dropdown_process_commission'])}}
                        @foreach($errors->get('emp_commission') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cost Centre:</th>
                    <td>{{Form::select('cost_centre',$dropdown_process_cost,null,['class'=>'form-control form-control-sm '. ($errors->has('cost_centre') ? ' is-invalid' : ''),'id'=>'dropdown_process_cost'])}}
                        @foreach($errors->get('cost_centre') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Report to:</th>
                    <td>{{Form::select('emp_reportto',$dropdown_process_reportto,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_reportto') ? ' is-invalid' : ''),'id'=>'dropdown_process_reportto', 'placeholder'=>'Report to'])}}
                        @foreach($errors->get('emp_reportto') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Status:</th>
                    <td>{{Form::select('status_id',$dropdown_process_statuses,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'dropdown_process_statuses'])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Project Type:</th>
                    <td>{{Form::select('project_type',$dropdown_process_project_type,null,['class'=>'form-control form-control-sm '. ($errors->has('project_type') ? ' is-invalid' : ''),'id'=>'dropdown_process_project_type'])}}
                        @foreach($errors->get('project_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td></td>
                    <th>Utilization:</th>
                    <td>{{Form::checkbox('emp_utilization')}}
                        @foreach($errors->get('emp_utilization') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td></td>
                </tr>
                <tr>
                    <th>Utilization From:</th>
                    <td>{{Form::text('utilization_from',old('utilization_from'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('utilization_from') ? ' is-invalid' : ''),'placeholder'=>'Utilization From'])}}
                        @foreach($errors->get('utilization_from') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Utilization To:</th>
                    <td>{{Form::text('utilization_to',old('utilization_to'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('utilization_to') ? ' is-invalid' : ''),'placeholder'=>'Utilization To'])}}
                        @foreach($errors->get('utilization_to') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Medical Certificate Type:</th>
                    <td>{{Form::select('medical_certificate_type',$dropdown_process_medical_certificate_type,null,['class'=>'form-control form-control-sm '. ($errors->has('medical_certificate_type') ? ' is-invalid' : ''),'id'=>'dropdown_process_marriage'])}}
                        @foreach($errors->get('medical_certificate_type') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Medical Certificate Expiry:</th>
                    <td>{{Form::text('medical_certificate_expiry',old('medical_certificate_expiry'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('medical_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'Medical Certificate Expiry'])}}
                        @foreach($errors->get('medical_certificate_expiry') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>

                <tr>
                    <th>Passport:</th>
                    <td>{{Form::text('emp_passport',old('emp_passport'),['class'=>'form-control form-control-sm'. ($errors->has('emp_passport') ? ' is-invalid' : ''),'placeholder'=>'Passport'])}}
                        @foreach($errors->get('emp_passport') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Nationality:</th>
                    <td>{{Form::text('emp_nationality',old('emp_nationality'),['class'=>'form-control form-control-sm'. ($errors->has('emp_nationality') ? ' is-invalid' : ''),'placeholder'=>'Nationality'])}}
                        @foreach($errors->get('emp_nationality') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Race:</th>
                    <td>{{Form::select('race',$dropdown_process_race,null,['class'=>'form-control form-control-sm '. ($errors->has('race') ? ' is-invalid' : ''),'id'=>'dropdown_process_race'])}}
                        @foreach($errors->get('race') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>BEE Race:</th>
                    <td>{{Form::select('bee_race',$dropdown_process_bee_race,null,['class'=>'form-control form-control-sm '. ($errors->has('bee_race') ? ' is-invalid' : ''),'id'=>'dropdown_process_bee_race'])}}
                        @foreach($errors->get('bee_race') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Gender:</th>
                    <td>{{Form::select('gender',$dropdown_process_gender,null,['class'=>'form-control form-control-sm '. ($errors->has('gender') ? ' is-invalid' : ''),'id'=>'dropdown_process_gender'])}}
                        @foreach($errors->get('gender') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Disability:</th>
                    <td>{{Form::select('disability',$dropdown_process_disability,null,['class'=>'form-control form-control-sm '. ($errors->has('disability') ? ' is-invalid' : ''),'id'=>'dropdown_process_disability'])}}
                        @foreach($errors->get('disability') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Marital Status:</th>
                    <td>{{Form::select('emp_marital_status',$dropdown_process_marriage,null,['class'=>'form-control form-control-sm '. ($errors->has('emp_marital_status') ? ' is-invalid' : ''),'id'=>'dropdown_process_marriage'])}}
                        @foreach($errors->get('emp_marital_status') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Health:</th>
                    <td>{{Form::text('emp_health',old('emp_health'),['class'=>'form-control form-control-sm'. ($errors->has('emp_health') ? ' is-invalid' : ''),'placeholder'=>'Health'])}}
                        @foreach($errors->get('emp_health') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Home Language:</th>
                    <td>{{Form::text('home_language',old('home_language'),['class'=>'form-control form-control-sm'. ($errors->has('home_language') ? ' is-invalid' : ''),'placeholder'=>'Home Language'])}}
                        @foreach($errors->get('home_language') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Other Language:</th>
                    <td>{{Form::text('other_language',old('other_language'),['class'=>'form-control form-control-sm'. ($errors->has('other_language') ? ' is-invalid' : ''),'placeholder'=>'Other Language'])}}
                        @foreach($errors->get('other_language') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>DOB:</th>
                    <td>{{Form::text('dob',old('dob'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('dob') ? ' is-invalid' : ''),'placeholder' => 'Date of Birth'])}}
                        @foreach($errors->get('dob') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Place of birth:</th>
                    <td>{{Form::text('place_of_birth',old('place_of_birth'),['class'=>'form-control form-control-sm'. ($errors->has('place_of_birth') ? ' is-invalid' : ''),'placeholder'=>'Place of birth'])}}
                        @foreach($errors->get('place_of_birth') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Criminal Record:</th>
                    <td>{{Form::select('criminal_rec',[1 => "YES", 0 => "NO"], 0,['class'=>'form-control form-control-sm '. ($errors->has('criminal_rec') ? ' is-invalid' : ''),'placeholder'=>'Criminal Record'])}}
                        @foreach($errors->get('criminal_rec') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Drivers License:</th>
                    <td>{{Form::text('drivers_lic',old('drivers_lic'),['class'=>'form-control form-control-sm'. ($errors->has('drivers_lic') ? ' is-invalid' : ''),'placeholder'=>'Drivers License'])}}
                        @foreach($errors->get('drivers_lic') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Main Qualification:</th>
                    <td>{{Form::text('emp_main_qualification',old('emp_main_qualification'),['class'=>'form-control form-control-sm'. ($errors->has('emp_main_qualification') ? ' is-invalid' : ''),'placeholder'=>'Main Qualification'])}}
                        @foreach($errors->get('emp_main_qualification') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Main Skill:</th>
                    <td>{{Form::text('emp_main_skill',old('emp_main_skill'),['class'=>'form-control form-control-sm'. ($errors->has('emp_main_skill') ? ' is-invalid' : ''),'placeholder'=>'Main Skill'])}}
                        @foreach($errors->get('emp_main_skill') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('emp_phone',old('emp_phone'),['class'=>'form-control form-control-sm'. ($errors->has('emp_phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('emp_phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Cell:</th>
                    <td>{{Form::text('emp_cell',old('emp_cell'),['class'=>'form-control form-control-sm'. ($errors->has('emp_cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('emp_cell') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Cell 2:</th>
                    <td>{{Form::text('emp_cell2',old('emp_cell2'),['class'=>'form-control form-control-sm'. ($errors->has('emp_cell2') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                        @foreach($errors->get('emp_cell2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Fax:</th>
                    <td>{{Form::text('emp_fax',old('emp_fax'),['class'=>'form-control form-control-sm'. ($errors->has('emp_fax') ? ' is-invalid' : ''),'placeholder'=>'fax'])}}
                        @foreach($errors->get('emp_fax') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{Form::text('emp_email',old('emp_email'),['class'=>'form-control form-control-sm'. ($errors->has('emp_email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('emp_email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Home Phone:</th>
                    <td>{{Form::text('emp_home_phone',old('emp_home_phone'),['class'=>'form-control form-control-sm'. ($errors->has('emp_home_phone') ? ' is-invalid' : ''),'placeholder'=>'Home Phone'])}}
                        @foreach($errors->get('emp_home_phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection


@section('extra-js')

    <script>
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            /*$('#terms_version').change(function () {
               $('#create_project_frm').submit();
            });*/
            checkVendor($('.employee_id').val());
        });

        $('#resource').on('submit', () => {

            if($('input[name="emp_prefname"]').val().length == 0){
                $('input[name="emp_prefname"]').val($('input[name="emp_firstname"]').val());
            }
        });

        $('.employee_id').on('change',function () {
            checkVendor($(this).val());
        });


        function checkVendor(id){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = id;
            $.ajax({
                url: '/getvendor/'+id,
                type:"POST",
                data:{id:id},
                success:function(data){
                    console.log(data);
                    if(data == 1) {
                        $('.vendor').removeAttr('disabled');
                        $('.vendor').trigger('chosen:updated');
                    } else {
                        $('[name=vendor_id]').val( '' );
                        $('.vendor').attr('disabled',true);
                        $('.vendor').trigger('chosen:updated');
                    }
                }
            });
        }
    </script>
@endsection