@extends('adminlte.default')
@section('header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            Edit {{$knownAs}}
        </h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
        <hr style="background-color: #4b646f;">
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box box-primary">
                        <!-- form start -->
                        {{Form::open(['url' => route(strtolower($modelName).'.update', $model), 'method' => 'put','class'=>'mt-3'])}}
                        <div class="box-body">
                            <div class="row">
                                @foreach($form_fields as $key => $form_field)
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">{{$form_field['label']}}</label>
                                            @if($form_field['type'] == 'text')
                                                {{Form::text($form_field['field'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => '', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'text_area')
                                                {{Form::textarea($form_field['field'], $model[$form_field['field']], ['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => '', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'look_up')
                                                {{Form::select($form_field['field'], $form_field['look_up_values'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => 'Please select', 'autocomplete' => 'off'])}}
                                            @endif
                                            @foreach($errors->get($form_field['field']) as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-dark btn-sm">Update</button>
                        </div>
                        {{Form::close()}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
@section('extra-css')
    <style>
        .ui-menu .ui-menu-item {
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
            font-size: 13px;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $(function () {
            let autocomplete_elements = [@foreach($autocomplete_elements as $autocomplete_element) {!! '"'.$autocomplete_element->name.'",' !!} @endforeach];

            $( "#name" ).autocomplete({
                source: autocomplete_elements
            });
        });
    </script>
@endsection
