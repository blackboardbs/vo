@extends('adminlte.default')
@section('header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View {{$look_up->name}}
            <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
        </h1>
        <hr style="background-color: #4b646f;">
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <!-- /.box-header -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$look_up->name}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @foreach($form_fields as $form_field)
                                <div class="form-group">
                                    <label for="name">{{$form_field['label']}}</label>
                                    @if($form_field['type'] == 'text')
                                        {{Form::text($form_field['field'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12', 'id' => $form_field['field'], 'placeholder' => $form_field['label'], 'autocomplete' => 'off'])}}
                                    @endif
                                    @if($form_field['type'] == 'text_area')
                                        {{Form::textarea($form_field['field'], $model[$form_field['field']], ['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12', 'id' => $form_field['field'], 'placeholder' => $form_field['label'], 'autocomplete' => 'off'])}}
                                    @endif
                                    @if($form_field['type'] == 'look_up')
                                        {{Form::select($form_field['field'], $form_field['look_up_values'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12', 'id' => $form_field['field'], 'placeholder' => 'Please select', 'autocomplete' => 'off'])}}
                                    @endif
                                    @foreach($errors->get($form_field['field']) as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
