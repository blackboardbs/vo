@extends('adminlte.default')
@section('header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            Show {{$knownAs}}
        </h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route(strtolower($modelName).'.edit', $model)}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit {{$knownAs}}</a>
            </div>
        </div>
        <hr style="background-color: #4b646f;">
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                @foreach($form_fields as $key => $form_field)
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">{{$form_field['label']}}</label>
                                            @if($form_field['type'] == 'text')
                                                {{Form::text($form_field['field'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'disabled' => 'disabled', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'text_area')
                                                {{Form::textarea($form_field['field'], $model[$form_field['field']], ['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'disabled' => 'disabled', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'look_up')
                                                {{Form::select($form_field['field'], $form_field['look_up_values'], $model[$form_field['field']], ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'disabled' => 'disabled', 'autocomplete' => 'off'])}}
                                            @endif
                                            @foreach($errors->get($form_field['field']) as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    {{--@if(($key % 2) == 0)
                                        </div> <!-- Close Row -->
                                        <div class="row"> <!-- Open Row -->
                                    @endif--}}
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection