@extends('adminlte.default')
@section('header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add {{$modelName}}
            <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
        </h1>
        <hr style="background-color: #4b646f;">
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <!-- /.box-header -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$modelName}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {{Form::open(['url' => route(strtolower($modelName).'.store'), 'method' => 'post','class'=>'mt-3'])}}
                            <div class="box-body">
                                <div class="row">
                                    @foreach($form_fields as $key => $form_field)
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">{{$form_field['label']}}</label>
                                            @if($form_field['type'] == 'text')
                                            {{Form::text($form_field['field'], old($form_field['field']), ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => '', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'text_area')
                                            {{Form::textarea($form_field['field'], old($form_field['field']), ['size' => '30x5', 'class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => '', 'autocomplete' => 'off'])}}
                                            @endif
                                            @if($form_field['type'] == 'look_up')
                                                @if($form_field['field'] == 'status_id')
                                                    {{Form::select($form_field['field'], $form_field['look_up_values'], 1, ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => 'Please select', 'autocomplete' => 'off'])}}
                                                @else
                                                    {{Form::select($form_field['field'], $form_field['look_up_values'], old($form_field['field']), ['class'=>'form-control form-control-sm col-sm-12'. ($errors->has($form_field['field']) ? ' is-invalid' : ''), 'id' => $form_field['field'], 'placeholder' => 'Please select', 'autocomplete' => 'off'])}}
                                                @endif
                                            @endif
                                            @foreach($errors->get($form_field['field']) as $error)
                                                <div class="invalid-feedback">
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @if(($key % 2) == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn bg-purple">Save</button>
                            </div>
                        {{Form::close()}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
