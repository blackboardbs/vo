@extends('adminlte.default')
@section('title') {{$knownAs}} @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route(strtolower($modelName).'.create')}}" class="btn btn-dark float-right" style="margin-left: 10px;"><i class="fa fa-plus"></i> Create</a>
    </div>
@endsection
    <!-- Main content -->
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="row">
            <div class="col-md-12">
                @if(sizeof($lookUpValues) > 0)
                <table id="look_up_table" class="table_paginate table table-bordered table-sm table-hover">
                    <thead>
                        <tr class="btn-dark">
                            @foreach($formFields as $formField)
                                <th>{{$formField['label']}}</th>
                            @endforeach
                            <th class="text-right last">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($lookUpValues as $lookUpValue)
                        <tr>
                            @foreach($formFields as $key => $formField)
                            <td>
                                @if(isset($relations[$formField['field']]))
                                    @forelse($relations[$formField['field']] as $key => $value)
                                        @if($lookUpValue[$formField['field']] == $key)
                                            {{$value}}
                                        @endif
                                    @empty
                                    @endforelse
                                @else
                                    @if($formField['field'] == 'name')
                                        <a href="{{route(strtolower($modelName).'.show',$lookUpValue)}}">{{$lookUpValue[$formField['field']]}}</a>
                                    @else
                                    {{$lookUpValue[$formField['field']]}}
                                    @endif
                                @endif
                            </td>
                            @endforeach
                            <td class="text-right">
                                <div class="d-flex">
                                <a href="{{route(strtolower($modelName).'.edit',$lookUpValue)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                {{ Form::open(['method' => 'DELETE','route' => [strtolower($modelName).'.destroy', $lookUpValue],'style'=>'display:inline','class'=>'delete']) }}
                                <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <p>No results match your search criteria.</p>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{!! asset('adminlte/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
    <style>
        .row{
            width: 100%;
        }
        .form-inline label {
            justify-content: left;
        }

        .dataTables_info{
            width: 750px;
        }

        .search_button{
            position: fixed;
            bottom: 60px;
            left: 257px;
            color: #FFF;
            text-align: center;
        }

        .pagination{
            float: right !important;
        }

        .active{
            background-color: #0e90d2 !important;
        }

        .row{
            width: 100%;
        }

        .dataTables_length{
            float: left !important;
        }

        .dataTables_filter{
            float: right !important;
        }

        .content-wrapper .pagination>li.active>a {
            color: #fff !important;
            text-decoration: none;
        }

        .pagination>li.active>a {
            text-decoration: none;
            z-index: 1;
            color: #fff;
            background-color: #000000 !important;
            border-color: #000000;
        }

    </style>
@endsection
