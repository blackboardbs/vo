<table>
    <thead>
    <tr>
        <th>Asset Nr</th>
        <th>Make</th>
        <th>Model</th>
        <th>Issued To</th>
        <th>Aquire Date</th>
        <th>Retire Date</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($assetreg as $referrer)
        <tr>
            <td>{{$referrer->asset_nr}}</td>
            <td>{{$referrer->make}}</td>
            <td>{{$referrer->model}}</td>
            <td>{{isset($referrer->resource)?$referrer->resource->name():$referrer->assetHistoryActive?->user?->name()}}</td>
            <td>{{$referrer->aquire_date}}</td>
            <td>{{$referrer->retire_date}}</td>
            <td>{{$referrer->status?->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>