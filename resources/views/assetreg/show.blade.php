@extends('adminlte.default')

@section('title') View Equipment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('assetreg.print', $assetreg->id)}}" target="_blank" id="print_asset_btn" class="btn btn-dark ml-1"><i class="fa fa-print"></i> Print</a>&nbsp;
                {{--<a href="#" class="btn ml-1 bg-dark d-print-none" data-toggle="modal" data-target="#assetDigisignModal"><i class="fas fa-paper-plane"></i> Digisign</a>--}}
                <a href="{{route('assetreg.edit', $assetreg)}}" class="btn btn-success float-right ml-1">Edit</a>
                <button type="button" class="btn btn-primary ml-1" data-toggle="modal" data-target="#writeOff" id="write-off-launcher">
                    Write Off
                </button>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            <ul class="nav nav-tabs mt-3">
                <li class="">
                    <a href="#asset" class="active" data-toggle="tab">Asset</a>
                </li>
                <li class="">
                    <a href="#finance" class="" data-toggle="tab">Finance</a>
                </li>
                <li class="">
                    <a href="#warranty" class="" data-toggle="tab">Warranty</a>
                </li>
                <li class="">
                    <a href="#support" class="" data-toggle="tab">Support</a>
                </li>
                <li class="">
                    <a href="#insurance" class="" data-toggle="tab">Insurance</a>
                </li>
                <li class="">
                    <a href="#history" class="" data-toggle="tab">History</a>
                </li>
                <li class="">
                    <a href="#documents" class="" data-toggle="tab">Documents</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="asset">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Asset Class:</th>
                                <td>{{$assetreg->assetClass?->description}}</td>
                                <th>Asset Number:</th>
                                <td>{{$assetreg->asset_nr}}</td>
                            </tr>
                            <tr>
                                <th>Make:</th>
                                <td>{{$assetreg->make}}</td>
                                <th>Model:</th>
                                <td>{{$assetreg->model}}</td>
                            </tr>
                            <tr>
                                <th>Serial Nr:</th>
                                <td>{{$assetreg->serial_nr}}</td>
                                <th>Issued to:</th>
                                <td>{{$assetreg->assetHistoryActive?->user?->name()}}</td>
                            </tr>
                            <tr>
                                <th rowspan="5">Picture</th>
                                <td rowspan="5">
                                    <a href="{{route('asset_register',['q'=>$assetreg->picture])}}" class="open-lightbox">
                                        <img src="{{route('asset_register',['q'=>$assetreg->picture])}}" class="img-thumbnail" style="height: 150px"/>
                                    </a>
                                </td>
                                <th>Issued Date:</th>
                                <td>{{$assetreg->assetHistoryActive?->issued_at}}</td>
                            </tr>
                            <tr>
                                <th>Template:</th>
                                <td>{{$assetreg->template?->name}}</td>
                            </tr>
                            <tr>
                                <th>Status:</th>
                                <td>{{$assetreg->status?->description}}</td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <td></td>
                            </tr>
                            <tr class="bg-dark">
                                <th colspan="4">For Computers</th>
                            </tr>
                            <tr>
                                <th>Processor</th>
                                <td>{{$assetreg->processor}}</td>
                                <th>RAM</th>
                                <td>{{$assetreg->ram}}</td>
                            </tr>
                            <tr>
                                <th>HDD TB</th>
                                <td>{{$assetreg->hdd}}</td>
                                <th>Screen Size Inch</th>
                                <td>{{$assetreg->screen_size}}</td>
                            </tr>

                            <tr>
                                <th>Supplier</th>
                                <td>{{$assetreg->supplier}}</td>
                                <th>Additional Information</th>
                                <td>{!! $assetreg->additional_info !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="finance">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Original Value:</th>
                                <td class="text-right pr-2">{{number_format($assetreg->original_value, 2)}}</td>
                                <th>Acquired Date:</th>
                                <td>{{$assetreg->aquire_date}}</td>
                            </tr>
                            <tr>
                                <th>Est. Life Months</th>
                                <td class="text-right pr-2">{{$assetreg->assetClass?->est_life_month}}</td>
                                <th>Retire Date:</th>
                                <td>{{$assetreg->retire_date}}</td>
                            </tr>
                            <tr>
                                <th>Remaining Months</th>
                                <td class="text-right pr-2">{{$remaining_months}}</td>
                                <th>Written-off Date</th>
                                <td>{{isset($assetreg->written_off_at)?\Carbon\Carbon::parse($assetreg->written_off_at)->toDateString():null}}</td>
                            </tr>
                            <tr>
                                <th>Additional Cost</th>
                                <td class="text-right pr-2">{{$additional_cost}}</td>
                                <th>Written-off Value</th>
                                <td class="text-right pr-2">{{number_format($assetreg->written_off_value, 2)}}</td>
                            </tr>
                            <tr>
                                <th>Current Book Value</th>
                                <td class="text-right pr-2">{{$current_book_value}}</td>
                                <th>Sold Date</th>
                                <td>{{isset($assetreg->sold_at)?\Carbon\Carbon::parse($assetreg->sold_at)->toDateString():null}}</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td></td>
                                <th>Sold Value</th>
                                <td class="text-right pr-2">{{number_format($assetreg->sold_value,2)}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="warranty">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th style="width: 25%;">Warranty Months</th>
                                    <td style="width: 25%;">{{$assetreg->support_months}}</td>
                                    <th style="width: 25%;">Warranty Expiry Date</th>
                                    <td style="width: 25%;">{{$assetreg->support_expiry_date}}</td>
                                </tr>
                                <tr>
                                    <th>Warranty Note</th>
                                    <td colspan="3">{{$assetreg->warranty_note}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="support">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th>Support Months</th>
                                    <td>{{$assetreg->support_months}}</td>
                                    <th>Support Expiry Date</th>
                                    <td>{{$assetreg->support_expiry_date}}</td>
                                </tr>
                                <tr>
                                    <th>Support Provided By</th>
                                    <td>{{$assetreg->support_provided_by}}</td>
                                    <th>Support Note</th>
                                    <td>{{$assetreg->support_note}}</td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td>{{$assetreg->contact_number}}</td>
                                    <th></th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Contact Email</th>
                                    <td>{{$assetreg->contact_email}}</td>
                                    <th></th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Reference Number</th>
                                    <td colspan="3">{{$assetreg->reference_number}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="insurance">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th>Insurer</th>
                                    <td>{{$assetreg->insurance?->insurer}}</td>
                                    <th>Excess Amount</th>
                                    <td class="text-right pr-2">{{$assetreg->insurance?->excess_amount}}</td>
                                </tr>
                                <tr>
                                    <th>Insured Value</th>
                                    <td class="text-right pr-2">{{$assetreg->insurance?->insured_value}}</td>
                                    <th>Policy Ref</th>
                                    <td>{{$assetreg->insurance?->policy_ref}}</td>
                                </tr>
                                <tr>
                                    <th>Cover Date</th>
                                    <td>{{$assetreg->insurance?->covered_at}}</td>
                                    <th>Insurance Note</th>
                                    <td>{{$assetreg->insurance?->note}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="history">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <equipment-history asset-id="{{$assetreg->id}}"></equipment-history>
                        <equipment-additional-cost asset-id="{{$assetreg->id}}"></equipment-additional-cost>
                    </div>
                </div>
                <div class="tab-pane fade" id="documents">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <h4>Documents</h4>

                    </div>
                </div>
            </div>
        </div>

        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="writeOff" tabindex="-1" role="dialog" aria-labelledby="writeOffTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="writeOffTitle">Write Off Equipment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {!! Form::open(['url' => route('assetreg.writeoff', $assetreg->id), 'method' => 'post']) !!}
                    <div class="modal-body">

                        <div class="form-group">
                            {!! Form::label('type', "Type") !!}
                            {!! Form::select('type', ['' => 'Select option', 'written_off' => 'Written Off', 'sold' => 'Sold'], '', ['class' => 'form-control form-control-sm '. ($errors->has('type') ? ' is-invalid' : '')]) !!}
                            @foreach($errors->get('type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            {!! Form::label('date', "Date") !!}
                            {!! Form::text('date', old('date'), ['class' => 'form-control form-control-sm datepicker '. ($errors->has('date') ? ' is-invalid' : '')]) !!}
                            @foreach($errors->get('date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            {!! Form::label('value', "Value") !!}
                            {!! Form::text('value', old('value'), ['class' => 'form-control form-control-sm '. ($errors->has('value') ? ' is-invalid' : '')]) !!}
                            @foreach($errors->get('date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="assetDigisignModal" tabindex="-1" role="dialog" aria-labelledby="assetDigisignModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Select Emails</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['url' => route('assetreg.senddigisign', $assetreg->id), 'method' => 'post']) !!}
                        <div class="form-group row">
                            <label for="subject" class="col-sm-2 col-form-label">Subject:</label>
                            <div class="col-sm-10">
                                <input type="text" name="subject" class="form-control form-control-sm" id="subject" value="{{$subject}}" placeholder="Email Subject">
                            </div>
                        </div>
                        <hr>
                        <div class="form-check">
                            <input type="checkbox" name="resource_email" value="{{$assetreg->issuedTo?->email}}" checked class="form-check-input" id="resource_email">
                            <label class="form-check-label" for="resource_email">{{$assetreg->issuedTo?->name()}} ({{$assetreg->issuedTo?->email}})</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" name="report_to_email" value="{{$claim_approver->email}}" checked class="form-check-input" id="report_to_email">
                            <label class="form-check-label" for="report_to_email">{{$claim_approver->first_name.' '.$claim_approver->last_name}} ({{$claim_approver->email}})</label>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            {!! Form::submit('Send for Digisign', ['class' => 'btn btn-sm btn-dark']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@section('extra-css')
    <style>
        img {
            max-width: 100%;
        }
        .modal.show .modal-dialog {
            width: 35%;
        }

        .lightbox-opened {
            background-color: #333;
            background-color: rgba(51, 51, 51, 0.9);
            cursor: pointer;
            height: 100%;
            left: 0;
            overflow-y: scroll;
            padding: 24px;
            position: fixed;
            text-align: center;
            top: 0;
            width: 100%;
            z-index: 1001;
        }
        .lightbox-opened:before {
            background-color: #333;
            background-color: rgba(51, 51, 51, 0.9);
            color: #eee;
            content: "x";
            font-family: sans-serif;
            padding: 6px 12px;
            position: fixed;
            text-transform: uppercase;
            margin-top: 3rem;
        }
        .lightbox-opened img {
            margin-top: 3rem;
            box-shadow: 0 0 6px 3px #333;
        }

        .no-scroll {
            overflow: hidden;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                },
            })
        })

        $(function () {
            $(document).on('ready', function () {
                @if(count($errors->all()))
                    $('#write-off-launcher').click()
                @endif
            })
        })

        $("#print_asset_btn").click(function(){

            //console.log('log');
            axios.post('/assetreg/print', {
                asset_id: $("#employee_id").val(),
                customer_id: $("#client_id").val()
            })
            .then(function (response) {
                $('#project_id').empty();
                console.log(response)
                option_change += "<option value=''>Project</option>";
                for(var i = 0; i < response.data.length; i++) option_change += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                $('#project_id').html(option_change).trigger("chosen:updated");
            })
            .catch(function (error) {
                console.log(error);
            });

        });

        $("#clients_td").on('change', '#client_id', function () {
            let option_change = '';
            axios.post('/getclientprojects', {
                resource_id: $("#employee_id").val(),
                customer_id: $("#client_id").val()
            })
            .then(function (response) {
                $('#project_id').empty();
                console.log(response)
                option_change += "<option value=''>Project</option>";
                for(var i = 0; i < response.data.length; i++) option_change += "<option value='"+ response.data[i].id +"'>" + response.data[i].name + " (" + response.data[i].id + ")</option>";
                $('#project_id').html(option_change).trigger("chosen:updated");
            })
            .catch(function (error) {
                console.log(error);
            });
        });

        (function($) {

            // Open Lightbox
            $('.open-lightbox').on('click', function(e) {
                e.preventDefault();
                var image = $(this).attr('href');
                $('html').addClass('no-scroll');
                $('body').append('<div class="lightbox-opened"><img src="' + image + '"></div>');
            });

            // Close Lightbox
            $('body').on('click', '.lightbox-opened', function() {
                $('html').removeClass('no-scroll');
                $('.lightbox-opened').remove();
            });

        })(jQuery);
    </script>
@endsection
