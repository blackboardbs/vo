@extends('adminlte.default')
@section('title') Add Asset Condition @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('assetconditionnote.store'), 'method' => 'post','class'=>'mt-3'])}}
            <table class="table table-bordered table-sm">
                <thead class="btn-dark">
                    <tr>
                        <th colspan="4">Asset Condition</th>
                    </tr>
                </thead>
                <tbody>
                    <input type="hidden" id="asset_id" name="asset_id" value="{{$asset_id}}" />
                    <tr>
                        <th>Note</th>
                        <td>
                            {{Form::textarea('note', old('note'),['size' => '30x5','class'=>'form-control form-control-sm col-sm-12'. ($errors->has('note') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('note') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Include in Acceptance Letter</th>
                        <td>
                            {{Form::select('include_in_acceptance_letter', [1 => 'Yes', 0 => 'No'],old('include_in_acceptance_letter'),['class'=>'form-control form-control-sm col-sm-12'. ($errors->has('include_in_acceptance_letter') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('include_in_acceptance_letter') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                    </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection