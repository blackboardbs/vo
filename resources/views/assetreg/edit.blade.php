@extends('adminlte.default')

@section('title') Edit Equipment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <button id="save" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

        <hr>

        <div class="table-responsive">
            {{Form::open(['url' => route('assetreg.update', $assetreg->id), 'method' => 'PATCH','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'saveForm'])}}
            <ul class="nav nav-tabs mt-3">
                <li class="">
                    <a href="#asset" class="active" data-toggle="tab">Asset</a>
                </li>
                <li class="">
                    <a href="#finance" class="" data-toggle="tab">Finance</a>
                </li>
                <li class="">
                    <a href="#warranty" class="" data-toggle="tab">Warranty</a>
                </li>
                <li class="">
                    <a href="#support" class="" data-toggle="tab">Support</a>
                </li>
                <li class="">
                    <a href="#insurance" class="" data-toggle="tab">Insurance</a>
                </li>
                <li class="">
                    <a href="#history" class="" data-toggle="tab">History</a>
                </li>
                <li class="">
                    <a href="#documents" class="" data-toggle="tab">Documents</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="asset">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Asset Class:</th>
                                <td>
                                    {{Form::select('assetclass',$assetclass,$assetreg->asset_class,['class'=>'form-control form-control-sm '. ($errors->has('assetclass') ? ' is-invalid' : ''), 'id' => 'asset-class', 'placeholder'=>'Asset Class'])}}
                                    @foreach($errors->get('assetclass') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Asset Number:</th>
                                <td>
                                    {{Form::text('asset_nr',$assetreg->asset_nr,['class'=>'form-control form-control-sm'. ($errors->has('asset_nr') ? ' is-invalid' : ''),'placeholder'=>'Asset Number'])}}
                                    @foreach($errors->get('asset_nr') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Make:</th>
                                <td>
                                    {{Form::text('make',$assetreg->make,['class'=>'form-control form-control-sm'. ($errors->has('make') ? ' is-invalid' : ''),'placeholder'=>'Make'])}}
                                    @foreach($errors->get('make') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Model:</th>
                                <td>
                                    {{Form::text('model',$assetreg->model,['class'=>'form-control form-control-sm'. ($errors->has('make') ? ' is-invalid' : ''),'placeholder'=>'Model'])}}
                                    @foreach($errors->get('make') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Serial Nr:</th>
                                <td>
                                    {{Form::text('serial_nr',$assetreg->serial_nr,['class'=>'form-control form-control-sm'. ($errors->has('serial_nr') ? ' is-invalid' : ''),'placeholder'=>'Serial Nr'])}}
                                    @foreach($errors->get('serial_nr') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Issued to:</th>
                                <td>{{$assetreg->assetHistoryActive?->user?->name()}}</td>
                            </tr>
                            <tr>
                                <th rowspan="8">Picture:</th>
                                <td rowspan="8">
                                    {{Form::file('picture',['class'=>'form-control form-control-sm'. ($errors->has('picture') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                                    @foreach($errors->get('picture') as $error)
                                        <div class="invalid-feedback">
                                            {{ $error}}
                                        </div>
                                    @endforeach
                                    <img src="{{route('asset_register',['q'=>$assetreg->picture])}}" id="blackboard-preview-large" style="height: 150px"/>
                                </td>
                                <th>Template</th>
                                <td>
                                    {{Form::select('template_id', $templates, $assetreg->template_id, ['class'=>'form-control form-control-sm'. ($errors->has('template_id') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('template_id') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>

                            </tr>
                            <tr>
                                <th>Status:</th>
                                <td>
                                    {{Form::select('status',$sidebar_process_statuses,$assetreg->status,['class'=>'form-control form-control-sm ','id'=>'sidebar_process_statuses'])}}
                                    @foreach($errors->get('status') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr class="bg-dark">
                                <th colspan="2">For Computers:</th>
                            </tr>
                            <tr>
                                <th>Processor:</th>
                                <td>
                                    {{Form::select('processor',array('','i3','i5','i7','Other'),$assetreg->processor,['class'=>'form-control form-control-sm ',])}}
                                    @foreach($errors->get('processor') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>RAM:</th>
                                <td>
                                    {{Form::text('ram',$assetreg->ram,['class'=>'form-control form-control-sm'. ($errors->has('ram') ? ' is-invalid' : ''),'placeholder'=>'RAM'])}}
                                    @foreach($errors->get('ram') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>HDD TB:</th>
                                <td>
                                    {{Form::text('hdd',$assetreg->hdd,['class'=>'form-control form-control-sm'. ($errors->has('hdd') ? ' is-invalid' : ''),'placeholder'=>'HDD'])}}
                                    @foreach($errors->get('hdd') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Screen Size Inch:</th>
                                <td>
                                    {{Form::text('screen_size',$assetreg->screen_size,['class'=>'form-control form-control-sm'. ($errors->has('screen_size') ? ' is-invalid' : ''),'placeholder'=>'Screen Size'])}}
                                    @foreach($errors->get('screen_size') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Supplier:</th>
                                <td>
                                    {{Form::text('supplier',$assetreg->supplier,['class'=>'form-control form-control-sm'. ($errors->has('supplier') ? ' is-invalid' : ''),'placeholder'=>'Supplier'])}}
                                    @foreach($errors->get('supplier') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Additional Information:</th>
                                <td colspan="3">
                                    <div id="additional"></div>
                                    @foreach($errors->get('additional_info') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="finance">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Original Value:</th>
                                <td class="text-right pr-2">
                                    {{Form::text('original_value',$assetreg->original_value,['class'=>'form-control form-control-sm'. ($errors->has('original_value') ? ' is-invalid' : ''),'placeholder'=>'Original Value'])}}
                                    @foreach($errors->get('original_value') as $error)
                                        <div class="invalid-feedback">
                                            This field must contain value only without currency
                                        </div>
                                    @endforeach
                                </td>
                                <th>Acquired Date:</th>
                                <td>
                                    {{Form::text('aquire_date',$assetreg->aquire_date,['class'=>'datepicker form-control form-control-sm'. ($errors->has('aquire_date') ? ' is-invalid' : ''),'placeholder' => 'Aquire Date'])}}
                                    @foreach($errors->get('aquire_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Est. Life Months:</th>
                                <td class="text-right pr-2">
                                    {{Form::text('est_life',$assetreg->est_life,['class'=>'form-control form-control-sm'. ($errors->has('est_life') ? ' is-invalid' : ''), 'id' => 'est-life', 'placeholder'=>'Est Life Months'])}}
                                    @foreach($errors->get('est_life') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Retire Date:</th>
                                <td>
                                    {{Form::text('retire_date',$assetreg->retire_date,['class'=>'datepicker2 form-control form-control-sm'. ($errors->has('retire_date') ? ' is-invalid' : ''),'placeholder' => 'Retire Date'])}}
                                    @foreach($errors->get('retire_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Remaining Months:</th>
                                <td class="text-right pr-2">{{$remaining_months}}</td>
                                <th>Written-off Date:</th>
                                <td>
                                    {{Form::text('write_off_at',$assetreg->write_off_at,['class'=>'datepicker2 form-control form-control-sm'. ($errors->has('write_off_at') ? ' is-invalid' : ''),'placeholder' => 'Written-off Date'])}}
                                    @foreach($errors->get('write_off_at') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Additional Cost:</th>
                                <td class="text-right pr-2">{{$additional_cost}}</td>
                                <th>Written-off Value:</th>
                                <td>
                                    {{Form::text('written_off_value',$assetreg->written_off_value,['class'=>'form-control form-control-sm'. ($errors->has('written_off_value') ? ' is-invalid' : ''),'placeholder' => 'Written-off Value'])}}
                                    @foreach($errors->get('written_off_value') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Current Book Value:</th>
                                <td class="text-right pr-2">{{$current_book_value}}</td>
                                <th>Sold Date:</th>
                                <td>
                                    {{Form::text('sold_at',$assetreg->sold_at,['class'=>'datepicker2 form-control form-control-sm'. ($errors->has('sold_at') ? ' is-invalid' : ''),'placeholder' => 'Sold Date'])}}
                                    @foreach($errors->get('sold_at') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td></td>
                                <th>Sold Value:</th>
                                <td>
                                    {{Form::text('sold_value',$assetreg->sold_value,['class'=>'form-control form-control-sm'. ($errors->has('sold_value') ? ' is-invalid' : ''),'placeholder' => 'Sold Value'])}}
                                    @foreach($errors->get('sold_value') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="warranty">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th style="width: 25%;">Warranty Months:</th>
                                <td style="width: 25%;">
                                    {{Form::text('warranty_months',$assetreg->warranty_months,['class'=>'form-control form-control-sm'. ($errors->has('warranty_months') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('warranty_months') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th style="width: 25%;">Warranty Expiry Date:</th>
                                <td style="width: 25%;">
                                    {{Form::text('warranty_expiry_date',$assetreg->warranty_expiry_date,['class'=>'datepicker2 form-control form-control-sm'. ($errors->has('warranty_expiry_date') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('warranty_expiry_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Warranty Note:</th>
                                <td colspan="3">
                                    <div id="warranty-note"></div>
                                    @foreach($errors->get('warranty_note') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="support">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Support Months:</th>
                                <td style="width: 35%">
                                    {{Form::text('support_months',$assetreg->support_months,['class'=>'form-control form-control-sm'. ($errors->has('support_months') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('support_months') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Support Expiry Date:</th>
                                <td>
                                    {{Form::text('support_expiry_date',$assetreg->support_expiry_date,['class'=>'datepicker2 form-control form-control-sm '. ($errors->has('support_expiry_date') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('support_expiry_date') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Support Provided By:</th>
                                <td>
                                    {{Form::text('support_provided_by',$assetreg->support_provided_by,['class'=>'form-control form-control-sm'. ($errors->has('support_provided_by') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('support_provided_by') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th rowspan="4">Support Note:</th>
                                <td rowspan="4">
                                    <div id="support-note"></div>
                                    @foreach($errors->get('support_note') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Contact Number:</th>
                                <td>
                                    {{Form::text('contact_number',$assetreg->contact_number,['class'=>'form-control form-control-sm'. ($errors->has('contact_number') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('contact_number') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Contact Email:</th>
                                <td>
                                    {{Form::text('contact_email',$assetreg->contact_email,['class'=>'form-control form-control-sm'. ($errors->has('contact_email') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('contact_email') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Reference Number:</th>
                                <td>
                                    {{Form::text('reference_number',$assetreg->reference_number,['class'=>'form-control form-control-sm'. ($errors->has('reference_number') ? ' is-invalid' : '')])}}
                                    @foreach($errors->get('reference_number') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="insurance">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <table class="table table-bordered table-sm">
                            <tbody>
                            <tr>
                                <th>Insurer:</th>
                                <td style="width: 35%">
                                    {{Form::text('insurer',$assetreg->insurance?->insurer,['class'=>'form-control form-control-sm '. ($errors->has('insurer') ? ' is-invalid' : ''), 'placeholder' => 'Insurer'])}}
                                    @foreach($errors->get('insurer') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Excess Amount:</th>
                                <td class="text-right">
                                    {{Form::text('excess_amount',$assetreg->insurance?->excess_amount,['class'=>'form-control form-control-sm'. ($errors->has('excess_amount') ? ' is-invalid' : ''),'placeholder'=>'Excess Amount'])}}
                                    @foreach($errors->get('excess_amount') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Insured Value:</th>
                                <td class="text-right">
                                    {{Form::text('insured_value',$assetreg->insurance?->insured_value,['class'=>'form-control form-control-sm'. ($errors->has('insured_value') ? ' is-invalid' : ''),'placeholder'=>'Insured Value'])}}
                                    @foreach($errors->get('insured_value') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Policy Ref:</th>
                                <td>
                                    {{Form::text('policy_ref',$assetreg->insurance?->policy_ref,['class'=>'form-control form-control-sm'. ($errors->has('policy_ref') ? ' is-invalid' : ''),'placeholder'=>'Insured Value'])}}
                                    @foreach($errors->get('policy_ref') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Cover Date:</th>
                                <td>
                                    {{Form::text('covered_at',$assetreg->insurance?->covered_at,['class'=>'form-control form-control-sm datepicker'. ($errors->has('covered_at') ? ' is-invalid' : ''),'placeholder'=>'Cover Date'])}}
                                    @foreach($errors->get('covered_at') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                                <th>Insurance Note:</th>
                                <td>
                                    <div id="note"></div>
                                    @foreach($errors->get('note') as $error)
                                        <div class="invalid-feedback">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="history">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <equipment-history asset-id="{{$assetreg->id}}"></equipment-history>
                        <equipment-additional-cost asset-id="{{$assetreg->id}}"></equipment-additional-cost>
                    </div>
                </div>
                <div class="tab-pane fade" id="documents">
                    <hr style="margin-top:-1px;"/>
                    <div class="row py-3">
                        <h3>Documents</h3>
                    </div>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('extra-js')

    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
    <script>
        $(function () {
            $(function () {
                $(function (){
                    const additionalQuill = new Quill('#additional', quillOptions());
                    const warrantyQuill = new Quill('#warranty-note', quillOptions());
                    const supportQuill = new Quill('#support-note', quillOptions());
                    const noteQuill = new Quill('#note', quillOptions());

                    additionalQuill.root.innerHTML = "{!! $assetreg->additional_info !!}";
                    warrantyQuill.root.innerHTML = "{!! $assetreg->warranty_note !!}";
                    supportQuill.root.innerHTML = "{!! $assetreg->support_note !!}";
                    noteQuill.root.innerHTML = "{!! $assetreg->insurance?->note !!}";

                    $("#save").on('click', function (){
                        const additional = '<textarea name="additional_info" style="display: none">'+(additionalQuill.root.innerHTML !== "<p><br></p>" ? additionalQuill.root.innerHTML : '')+'</textarea>';
                        const warranty = '<textarea name="warranty_note" style="display: none" >'+(warrantyQuill.root.innerHTML !== "<p><br></p>" ? warrantyQuill.root.innerHTML : '')+'</textarea>';
                        const support = '<textarea name="support_note" style="display: none" >'+(supportQuill.root.innerHTML !== "<p><br></p>" ? supportQuill.root.innerHTML : '')+'</textarea>';
                        const note = '<textarea name="note" style="display: none" >'+(noteQuill.root.innerHTML !== "<p><br></p>" ? noteQuill.root.innerHTML : '')+'</textarea>';

                        $("#saveForm").append(additional, warranty, support, note).submit();
                    });
                });
            })
            $(function () {
                $('[data-toggle="tooltip"]').tooltip({
                    position: {
                        my: "center bottom-10", // the "anchor point" in the tooltip element
                        at: "center top", // the position of that anchor point relative to selected element
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                                .addClass( "arrow" )
                                .addClass( feedback.vertical )
                                .addClass( feedback.horizontal )
                                .appendTo( this );
                        }
                    },
                })
            })
        });
        $(function () {
            $( ".datepicker2" ).datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                showWeek:true,
                yearRange: "2017:+10"
            });
        })
        function quillOptions(){
            return {
                theme: 'snow',
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                        [{ 'list': 'ordered'}, { 'list': 'bullet' }, { 'list': 'check' }],
                        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                        [{ 'align': [] }],
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                        ['blockquote'],
                        ['link'],

                        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme

                        ['clean']                                         // remove formatting button
                    ]
                }
            }
        }
    </script>
@endsection
@section('extra-css')
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
@endsection