@extends('adminlte.default')

@section('title') Equipment @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('assetreg.create')}}" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Equipment</a>
        <x-export route="assetreg.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform">
            <div class="col-md-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                        {{-- {{Form::text('q',old('query'),['class'=>'form-control form-control-sm w-100','placeholder'=>'Search...'])}} --}}
                        <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('insurer', $yes_or_no_dropdown, request()->insurer, ['class' => 'form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Equipment With Insurance</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('asset_class', $assetclass_dropdown, request()->asset_class, ['class' => 'form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Asset Class</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width: 20%;">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        {{Form::select('status_id', $status_dropdown, request()->status_id, ['class' => 'form-control w-100 search', 'placeholder' => 'All'])}}
                        <span>Status</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3" style="max-width: 20%;">
                <a href="{{route('assetreg.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('asset_nr','Asset Nr')</th>
                    <th>Asset Class</th>
                    <th>@sortablelink('make','Make')</th>
                    <th>@sortablelink('model','Model')</th>
                    <th>@sortablelink('issued_to','Issued To')</th>
                    <th>@sortablelink('aquire_date','Aquire Date')</th>
                    <th>@sortablelink('retire_date','Retire Date')</th>
                    <th>Insurer</th>
                    <th>@sortablelink('status_id','Status')</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($assetreg as $referrer)
                    <tr>
                        <td><a href="{{route('assetreg.show',$referrer)}}">{{$referrer->asset_nr}}</a></td>
                        <td>{{$referrer->assetClass?->description}}</td>
                        <td>{{$referrer->make}}</td>
                        <td>{{$referrer->model}}</td>
                        <td>{{isset($referrer->resource)?$referrer->resource->name():$referrer->assetHistoryActive?->user?->name()}}</td>
                        <td>{{$referrer->aquire_date}}</td>
                        <td>{{$referrer->retire_date}}</td>
                        <td>{{$referrer->insurance?->insurer}}</td>
                        <td>{{$referrer->status->description}}</td>
                        <td>
                            <div class="d-flex">
                            <a href="{{route('assetreg.edit',$referrer)}}" class="btn btn-success btn-sm mr-1"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['assetreg.destroy', $referrer->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No equipment match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
                <table class="tabel tabel-borderless" style="margin: 0 auto">
                    <tr>
                        <td style="vertical-align: center;">
                            Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $assetreg->firstItem() }} - {{ $assetreg->lastItem() }} of {{ $assetreg->total() }}
                        </td>
                        <td>
                            {{ $assetreg->appends(request()->except('page'))->links() }}
                        </td>
                    </tr>
                </table>
            </div>
            {{-- {{ $assetreg->appends(['s' => $assetreg->perPage(),'q' => \Illuminate\Support\Facades\Request::get('q')])->links() }} --}}
        </div>
    </div>
@endsection
