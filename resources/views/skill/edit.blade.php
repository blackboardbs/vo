@extends('adminlte.default')

@section('title') Edit Skill @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('frm_skills')" class="btn btn-primary ml-1">Save</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('skill.update',['skillid' => $skill,'cvid' => $cv, 'res_id' => $skill->res_id]), 'method' => 'post','class'=>'mt-3','files'=>true,'id'=>'frm_skills'])}}
            @if(isset($_GET['fc']) && $_GET['fc'] == 1)
                <input type="hidden" id="from_create" name="from_create" value="1" />
                <input type="hidden" id="cvid" name="cvid" value="{{$cv}}" />
                <input type="hidden" id="res_id" name="res_id" value="{{$skill->res_id}}" />
            @endif
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Skill: <small class="text-muted d-none d-sm-inline">(required without tool)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('res_skill',$skill->res_skill,['class'=>'form-control form-control-sm'. ($errors->has('res_skill') ? ' is-invalid' : ''),'placeholder'=>'Skill'])}}
                        @foreach($errors->get('res_skill') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Select a Skill or Tool: <small class="text-muted d-none d-sm-inline">(required without skill)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::select('tool_id',$system_dropdown,$skill->tool_id,['id' => 'select-skill','class'=>'form-control form-control-sm '. ($errors->has('tool_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('tool_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr id="other-skill">
                    <th>or type(if not in list): <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('tool',$skill->tool,['class'=>'form-control form-control-sm'. ($errors->has('tool') ? ' is-invalid' : ''),'placeholder'=>'Tool'])}}
                        @foreach($errors->get('tool') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Years Experience: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('years_experience',$skill->years_experience,['class'=>'form-control form-control-sm'. ($errors->has('years_experience') ? ' is-invalid' : ''),'placeholder'=>'Years Experience'])}}
                        @foreach($errors->get('years_experience') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Skill Level: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('skill_level',$skill_level_dropdown,$skill->skill_level,['class'=>'form-control form-control-sm '. ($errors->has('skill_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('skill_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Row Order:</th>
                    <td>
                        {{Form::select('row_order',$row_order, $skill->row_order, ['class'=>'form-control form-control-sm '. ($errors->has('row_order') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('row_order') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                        @endforeach</td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,$skill->status_id,['class'=>'datepicker form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        #other-skill{
            display: none;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {

            if($('#select-skill').val() == 16){
                $('#other-skill').show();
            }
            $('#select-skill').on('change', function () {
                if($('#select-skill').val() == 16){
                    $('#other-skill').show();
                }else{
                    $('#other-skill').hide();
                }
            })
            })
    </script>
@endsection