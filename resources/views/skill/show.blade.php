@extends('adminlte.default')

@section('title') View Skill @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                <a href="{{route('skill.edit', ['skill' => $skill,'cv' => $cvid, 'resource' => $skill->res_id])}}" class="btn btn-success btn-sm float-right" style="margin-left: 5px;"><i class="far fa-edit"></i> Edit Skill</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Skill:</th>
                    <td colspan="3">{{$skill->res_skill}}</td>
                </tr>
                <tr>
                    <th>Skill or Tool:</th>
                    <td colspan="3">{{$skill->tool_idd->description}}</td>
                </tr>
                <tr>
                    <th>or type(if not in list:</th>
                    <td colspan="3">{{$skill->tool}}</td>
                </tr>
                <tr>
                    <th>Years Experience:</th>
                    <td>{{$skill->years_experience}}</td>
                    <th>Skill Level:</th>
                    <td>{{$skill->skill_leveld->description}}</td>
                </tr>
                <tr>
                    <th>Row Order:</th>
                    <td>{{$skill->row_order}}</td>
                    <th>Status:</th>
                    <td>{{$skill->status->description}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
