@extends('adminlte.default')

@section('title') Add Skill @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('frm_skills')" class="btn btn-primary ml-1">Save</a>
            <a onclick="saveAndAddMore()" class="btn btn-primary ml-1">Save and Add more</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('skill.store',['cvid' => $cv,'res_id' => $res]), 'method' => 'post','class'=>'mt-3', 'id' => 'frm_skills', 'files'=>true])}}
            <input type="hidden" id="add_more" name="add_more" value="0">
            <table class="table table-bordered table-sm">
                <tbody>
                <tr>
                    <th>Skill: <small class="text-muted d-none d-sm-inline">(required without tool)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('res_skill',old('res_skill'),['class'=>'form-control form-control-sm'. ($errors->has('res_skill') ? ' is-invalid' : ''),'placeholder'=>'Skill'])}}
                        @foreach($errors->get('res_skill') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Select a Skill or Tool: <small class="text-muted d-none d-sm-inline">(required without skill)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::select('tool_id',$system_dropdown,old('tool_id'),['id' => 'select-skill','class'=>'form-control form-control-sm '. ($errors->has('tool_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('tool_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr id="other-skill">
                    <th>or type(if not in list): <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td colspan="3">{{Form::text('tool',old('tool'),['class'=>'form-control form-control-sm'. ($errors->has('tool') ? ' is-invalid' : ''),'placeholder'=>'Tool'])}}
                        @foreach($errors->get('tool') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Years Experience: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('years_experience',old('years_experience'),['class'=>'form-control form-control-sm'. ($errors->has('years_experience') ? ' is-invalid' : ''),'placeholder'=>'Years Experience'])}}
                        @foreach($errors->get('years_experience') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Skill Level: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('skill_level',$skill_level_dropdown,old('skill_level'),['class'=>'form-control form-control-sm '. ($errors->has('skill_level') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('skill_level') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Row Order:</th>
                    <td>{{Form::select('row_order',$row_order, $row_order[1], ['class'=>'form-control form-control-sm '. ($errors->has('row_order') ? ' is-invalid' : '')])}}

                        {{--<select name="row_order" class="form-control form-control-sm  {{($errors->has('row_order') ? ' is-invalid' : '')}}">
                            <option value="{{(old('row_order') > 0 ? old('row_order') : 0)}}" disabled>{{(old('row_order') > 0 ? old('row_order') : 0)}}</option>
                            @for($i = 0; $i<30; ++$i)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor--}}
                        @foreach($errors->get('row_order') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                    <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::select('status_id',$status_dropdown,1,['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : '')])}}
                        @foreach($errors->get('status_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr class="btn-dark">
                        <th colspan="6">
                            Skill
                        </th>
                    </tr>
                    <tr>
                        <th>Skill</th>
                        <th>Tool</th>
                        <th>Level</th>
                        <th>Years</th>
                        <th>Status</th>
                        <th class="text-right last">Action</th>
                    </tr>
                </thead>
                <tbody id="skill_items">
                @forelse($skills as $skill)
                    <tr id="{{$skill->id}}">
                        <td>{{ $skill->res_skill }} </td>
                        <td>{{($skill->tool_id > 0 ? $skill->tool_idd->description : '')}}</td>
                        <td>{{($skill->skill_level > 0 ? $skill->skill_leveld->description : '')}}</td>
                        <td>{{$skill->years_experience}}</td>
                        <td>{{$skill->status->description}}</td>
                        <td class="text-right">
                            <div class="d-flex">
                            <a href="{{route('skill.edit',['skill' => $skill, 'cv' => $cv,'resource' => $skill->res_id])}}?fc=1" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['skill.destroy', $skill->id],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></button>
                            <input type="hidden" id="from_create" name="from_create" value="1" />
                            <input type="hidden" id="cvid" name="cvid" value="{{$cv}}" />
                            <input type="hidden" id="res_id" name="res_id" value="{{$skill->res_id}}" />
                            {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No sill added yet</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        #other-skill{
            display: none;
        }
    </style>
@endsection
@section('extra-js')

    <script>
        $(function () {


            $('#select-skill').on('change', function () {
                if($('#select-skill').val() == 16){
                    $('#other-skill').show();
                }
            })

            $('tbody').sortable({
                change: function(event, ui) {
                    /*console.log(ui.item.context.id);
                    console.log(ui.item.context.rowIndex);
                    console.log(ui.item);*/
                }
            });

        });

        function saveAndAddMore(){
            $('#add_more').val(1);
            $('#frm_skills').submit();
        }

    </script>
@endsection