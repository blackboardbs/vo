@extends('adminlte.default')

@section('title') Company @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($can_create)
        <a href="{{route('company.create')}}" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-plus"></i> Company</a>
        @endif
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <form class="form-inline mt-3 searchform" id="searchform">
        <div class="col-md-3">
            <div class="form-group input-group">
                <label class="has-float-label">
                    <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                    <span>Matching</span>
                </label>
            </div>
        </div>
        <div class="col-md-2">
            <a href="{{route('company.index')}}" class="btn btn-info w-100">Clear Filters</a>
        </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead>
            <tr class="btn-dark">
                <th>@sortablelink('company_name', 'Company Name')</th>
                <th>@sortablelink('email','Email')</th>
                <th>@sortablelink('phone', 'Phone')</th>
                <th>@sortablelink('cell', 'Cell')</th>
                <th>@sortablelink('created_at', 'Added')</th>
                <th>@sortablelink('status.description','Status')</th>
                @if($can_update)
                    <th class="last">Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @forelse($company as $result)
                <tr>
                    <td><a href="{{route('company.show',$result)}}">{{$result->company_name}}</a></td>
                    <td>{{$result->email}}</td>
                    <td>{{$result->phone}}</td>
                    <td>{{$result->cell}}</td>
                    <td>{{$result->created_at}}</td>
                    <td>{{$result->status?->description}}</td>
                    @if($can_update)
                    <td>
                        <div class="d-flex">
                        <a href="{{route('company.edit',$result)}}" class="btn btn-success btn-sm toolt"><i class="fas fa-pencil-alt"></i><span class="tooltiptext">Edit</span></a>

                            {{ Form::open(['method' => 'DELETE','route' => ['company.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1 mr-1 toolt" {{($result->projects_count > 0 || $result->users_count > 0 ? 'disabled' : '')}}><i class="fas fa-trash"></i><span class="tooltiptext">Delete</span></button>
                            {{ Form::close() }}

                        <a href="{{route('team.create',['company_id' => $result])}}" class="btn btn-primary btn-sm toolt"><i class="fa fa-plus"></i><span class="tooltiptext">Add Team</span></a>
                        </div>
                    </td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No companies match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="w-100 d-block col-md-12 text-center" style="align-items: center;">      
            <table class="tabel tabel-borderless" style="margin: 0 auto">
                <tr>
                    <td style="vertical-align: center;">
                        Items per page {{Form::select('r',['15'=>'15','30'=>'30','45'=>'45','60'=>'60','75'=>'75','90'=>'90'], (isset($_GET['r']) ? $_GET['r'] : '15'),['class'=>'form-control form-control-sm d-inline ml-2 mr-2','style'=>'width:60px;','id'=>'r'])}}  {{ $company->firstItem() }} - {{ $company->lastItem() }} of {{ $company->total() }}
                    </td>
                    <td>
                        {{ $company->appends(request()->except('page'))->links() }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
