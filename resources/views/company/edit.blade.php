@extends('adminlte.default')

@section('title')
Edit Company
@endsection

@section('header')
<div class="container-fluid container-title">
    <h3>@yield('title')</h3>
    <div class="btn-toolbar float-right">
        <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
        <a href="javascript:void(0)" onclick="saveForm('editCompany')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
    </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
    {{Form::open(['url' => route('company.update',$company->id), 'method' => 'patch','class'=>'mt-3','files'=>true, 'autocomplete' => 'off','id'=>'editCompany'])}}
    <table class="table table-bordered table-sm mt-3">
        <thead>
        <tr>
            <th colspan="4" class="btn-dark">General Information</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th rowspan="5">Company Logo:</th>
            <td rowspan="5"><div class="img-wrapper"><img src="{{route('company_avatar',['q'=>$company->company_logo])}}" id="blackboard-preview-large" /></div>
                {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Company Logo','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                @foreach($errors->get('avatar') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            <th>Company Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
            <td>{{Form::text('company_name',$company->company_name,['class'=>'form-control form-control-sm'. ($errors->has('company_name') ? ' is-invalid' : ''),'placeholder'=>'Company Name'])}}
                @foreach($errors->get('company_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>
        </tr>
        <tr>
            <th>Registration Number:</th>
            <td>{{Form::text('registration_number',$company->business_reg_no,['class'=>'form-control form-control-sm'. ($errors->has('registration_number') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}</td>
        </tr>
        <tr>
            <th>VAT Number:</th>
            <td>{{Form::text('vat_number',$company->vat_no,['class'=>'form-control form-control-sm'. ($errors->has('vat_number') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}</td>
        </tr>
        <tr>
            <th>Web:</th>
            <td>{{Form::text('web',$company->web,['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}</td>
        </tr>
        <tr>
            <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
            <td>{{Form::select('status_id',$status_dropdown,$company->status_id,['class'=>'form-control form-control-sm'. ($errors->has('status_id') ? ' is-invalid' : ''),'placeholder'=>'N/A'])}}
                @foreach($errors->get('status_id') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>

        <tr>
            <th>Contact Firstname:</th>
            <td>{{Form::text('contact_firstname',$company->contact_firstname,['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}</td>
            <th rowspan="6">Postal Address:</th>
            <td rowspan="6">
                {{Form::text('address_ln1',$company->postal_address_line1,['class'=>'form-control form-control-sm'. ($errors->has('address_ln1') ? ' is-invalid' : ''),'placeholder'=>'Address Line 1'])}}
                {{Form::text('address_ln2',$company->postal_address_line2,['class'=>'form-control form-control-sm input-pad'. ($errors->has('address_ln2') ? ' is-invalid' : ''),'placeholder'=>'Address Line 2'])}}
                {{Form::text('address_ln3',$company->postal_address_line3,['class'=>'form-control form-control-sm input-pad'. ($errors->has('address_ln3') ? ' is-invalid' : ''),'placeholder'=>'Address Line 3'])}}
                {{Form::text('city_suburb',$company->city_suburb,['class'=>'form-control form-control-sm input-pad'. ($errors->has('city_suburb') ? ' is-invalid' : ''),'placeholder'=>'City/Suburb'])}}
                {{Form::text('state_province',$company->state_province,['class'=>'form-control form-control-sm input-pad'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                {{Form::text('postal_zip',$company->postal_zipcode,['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zip') ? ' is-invalid' : ''),'placeholder'=>'Postal Zip Code'])}}
                {{Form::select('country_id',$country,$company->country_id,['class'=>'form-control form-control-sm input-pad '. ($errors->has('country_id') ? ' is-invalid' : ''),'placeholder'=>'N/A'])}}
            </td>
        </tr>
        <tr>
            <th>Contact Lastname:</th>
            <td>{{Form::text('contact_lastname',$company->contact_lastname,['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}</td>
        </tr>
        <tr>
            <th>Email: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
            <td>{{Form::text('email',$company->email,['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                @foreach($errors->get('email') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>{{Form::text('phone',$company->phone,['class'=>'form-control form-control-sm phone_number'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}</td>
        </tr>
        <tr>
            <th>Cell:</th>
            <td>{{Form::text('cell',$company->cell,['class'=>'form-control form-control-sm phone_number'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}</td>
        </tr>
        <tr>
            <th>Fax:</th>
            <td>{{Form::text('fax',$company->fax,['class'=>'form-control form-control-sm phone_number'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}</td>
        </tr>
        </tbody>
    </table>
    <table class="table table-bordered table-sm mt-3">
        <thead>
        <tr>
            <th colspan="4" class="btn-dark">BBBEE Information</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>BBBEE Level:</th>
            <td>{{Form::select('bbbee_level_id',$sidebar_process_bbbee_level,$company->bbbee_level_id,['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level_id') ? ' is-invalid' : ''),'id'=>'bbbee_level_id'])}}
                @foreach($errors->get('bbbee_level_id') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            <th>BBBEE Certificate Expiry Date:</th>
            <td>{{Form::text('bbbee_certificate_expiry',$company->bbbee_certificate_expiry,['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry Date'])}}
                @foreach($errors->get('bbbee_certificate_expiry') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>
        </tr>
        <tr>
            <th>Black Ownership:</th>
            <td>{{Form::text('black_ownership',$company->black_ownership,['class'=>'form-control form-control-sm'. ($errors->has('black_ownership') ? ' is-invalid' : ''),'placeholder'=>'Black Ownership'])}}
                @foreach($errors->get('black_ownership') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            <th>Black Female Ownership:</th>
            <td>{{Form::text('black_female_ownership',$company->black_female_ownership,['class'=>'form-control form-control-sm'. ($errors->has('black_female_ownership') ? ' is-invalid' : ''),'placeholder'=>'Black Female Ownership'])}}
                @foreach($errors->get('black_female_ownership') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>
        </tr>
        <tr>
            <th>Black Voting Rights:</th>
            <td>{{Form::text('black_voting_rights',$company->black_voting_rights,['class'=>'form-control form-control-sm'. ($errors->has('black_voting_rights') ? ' is-invalid' : ''),'placeholder'=>'Black Voting Rights'])}}
                @foreach($errors->get('black_voting_rights') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            <th>Black Female Voting Rights:</th>
            <td>{{Form::text('black_female_voting_rights',$company->black_female_voting_rights,['class'=>'form-control form-control-sm'. ($errors->has('black_female_voting_rights') ? ' is-invalid' : ''),'placeholder'=>'Black Female Voting Rights'])}}
                @foreach($errors->get('black_female_voting_rights') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>
        </tr>
        <tr>
            <th>Black Recognition Level:</th>
            <td>{{Form::text('bbbee_recognition_level',$company->bbbee_recognition_level,['class'=>'form-control form-control-sm'. ($errors->has('bbbee_recognition_level') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Recognition Level'])}}
                @foreach($errors->get('bbbee_recognition_level') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            <th></th>
            <td></td>
        </tr>
        </tbody>
    </table>
    <table class="table table-bordered table-sm mt-3">
        <thead>
        <tr>
            <th colspan="4" class="btn-dark">Banking Details</th>
        </tr>
        </thead>
        <tbody>


        <tr>
            <th>Bank Name:</th>
            <td>
                {{Form::text('bank_name',$company->bank_name,['class'=>'form-control form-control-sm'. ($errors->has('bank_name') ? ' is-invalid' : ''),'placeholder'=>'Bank Name'])}}
                @foreach($errors->get('bank_name') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach</td>
            </td>
            <th>Branch Code:</th>
            <td>
                {{Form::text('branch_code',$company->branch_code,['class'=>'form-control form-control-sm'. ($errors->has('branch_code') ? ' is-invalid' : ''),'placeholder'=>'Branch Code'])}}
                @foreach($errors->get('branch_code') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Account Number:</th>
            <td>
                {{Form::text('bank_acc_no',$company->bank_acc_no,['class'=>'form-control form-control-sm'. ($errors->has('bank_acc_no') ? ' is-invalid' : ''),'placeholder'=>'Account Number'])}}
                @foreach($errors->get('bank_acc_no') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
            <th>Account Name:</th>
            <td>
                {{Form::text('account_name',(isset($company->account_name)?$company->account_name:$company_name),['class'=>'form-control form-control-sm'. ($errors->has('account_name') ? ' is-invalid' : ''),'placeholder'=>'Account Name'])}}
                @foreach($errors->get('account_name') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Branch Name:</th>
            <td>
                {{Form::text('branch_name',$company->branch_name,['class'=>'form-control form-control-sm'. ($errors->has('branch_name') ? ' is-invalid' : ''),'placeholder'=>'Branch Name'])}}
                @foreach($errors->get('branch_name') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
            <th>Bank Account Type:</th>
            <td>
                {{Form::select('bank_account_type_id',$bank_account_type_dropdown,$company->bank_account_type_id,['class'=>'form-control form-control-sm'. ($errors->has('bank_account_type_id') ? ' is-invalid' : ''),'placeholder'=>'Select Bank Account Type'])}}
                @foreach($errors->get('bank_account_type_id') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Swift Code:</th>
            <td>
                {{Form::text('swift_code',$company->swift_code,['class'=>'form-control form-control-sm'. ($errors->has('swift_code') ? ' is-invalid' : ''),'placeholder'=>'Swift Code'])}}
                @foreach($errors->get('swift_code') as $error)
                    <div class="invalid-feedback">
                        {{ $error}}
                    </div>
                @endforeach
            </td>
            <th></th>
            <td></td>
        </tr>
        </tbody>
    </table>
    <table class="table table-bordered table-sm mt-3">
        <thead>
        <tr>
            <th colspan="4" class="btn-dark">Invoice Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>Invoice Text:</th>
            <td>{{Form::text('invoice_text',$company->invoice_text,['class'=>'form-control form-control-sm'. ($errors->has('invoice_text') ? ' is-invalid' : ''),'placeholder'=>'Invoice Text'])}}
                @foreach($errors->get('invoice_text') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach</td>
        </tr>
        </tbody>
    </table>
    {{Form::close()}}
    <table class="table table-bordered table-sm mt-3">
        <thead>
        <tr>
            <th colspan="5" class="btn-dark">Teams</th>
        </tr>
        <tr>
            <th>Team Name</th>
            <th>Team Manager</th>
            <th>Status</th>
            <th>Created By</th>
            <th class="last">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($team as $result)
            <tr>
                <td>{{$result->team_name}}</td>
                <td>{{$result->manager?->name()}}</td>
                <td>{{$result->status?->description}}</td>
                <td>{{$result->creator?->name()}}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{route('team.edit', $result)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                        {{ Form::open(['method' => 'DELETE','route' => ['team.destroy', $result],'style'=>'display:inline','class'=>'delete']) }}
                    <button type="submit" class="btn btn-danger btn-sm ml-1 mr-1"><i class="fas fa-trash"></i></button>
                    {{ Form::close() }}
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">There are no teams for this company</td>
            </tr>
        @endforelse
        <tr>
            <td colspan="5" class="text-center"><a href="{{route('team.create',['companyid' => $company])}}" class="btn btn-dark btn-sm">Add Team</a></td>
        </tr>
        </tbody>
    </table>
</div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('.chosen-container').css({width: "100%"});
            var max_chars = 15;

            $('.phone_number').keyup( function(e){
                if ($(this).val().length >= max_chars) {
                    $(this).val($(this).val().substr(0, max_chars));
                }
            });
        });
    </script>
@endsection
