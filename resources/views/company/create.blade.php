@extends('adminlte.default')

@section('title') Add Company @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('createCompany')" class="btn btn-primary ml-1" style="margin-top: -7px;">Save</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <hr />
    <div class="table-responsive">
    {{Form::open(['url' => route('company.store'), 'method' => 'post','class'=>'mt-3', 'files' => true, 'autocomplete' => 'off','id'=>'createCompany'])}}
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">General Information</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                   <th rowspan="5">Company Logo:</th>
                    <td rowspan="5"><div class="img-wrapper"><img src="{{route('company_avatar',['q'=>Auth::user()->avatar])}}" id="blackboard-preview-large" /></div>
                        {{Form::file('avatar',['class'=>'form-control form-control-sm'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Company Logo','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
                        @foreach($errors->get('avatar') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th>Company Name: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('company_name',old('company_name'),['class'=>'form-control form-control-sm'. ($errors->has('company_name') ? ' is-invalid' : ''),'placeholder'=>'Company Name'])}}
                        @foreach($errors->get('company_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
            <tr>
                <th>Registration Number:</th>
                <td>{{Form::text('registration_number',old('registration_number'),['class'=>'form-control form-control-sm'. ($errors->has('registration_number') ? ' is-invalid' : ''),'placeholder'=>'Registration Number'])}}
                    @foreach($errors->get('registration_number') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>VAT Number:</th>
                <td>{{Form::text('vat_number',old('vat_number'),['class'=>'form-control form-control-sm'. ($errors->has('vat_number') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                    @foreach($errors->get('vat_number') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Web:</th>
                <td>{{Form::text('web',old('web'),['class'=>'form-control form-control-sm'. ($errors->has('web') ? ' is-invalid' : ''),'placeholder'=>'Web'])}}
                    @foreach($errors->get('web') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            <tr>
                <th>Status: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                <td>{{Form::select('status_id',$status_dropdown,old('status_id', \App\Enum\Status::ACTIVE->value),['class'=>'form-control form-control-sm '. ($errors->has('status_id') ? ' is-invalid' : ''),'id'=>'status_id', 'placeholder' => 'Select Status'])}}
                    @foreach($errors->get('status_id') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
                <tr>
                    <th>Contact Firstname:</th>
                    <td>{{Form::text('contact_firstname',old('contact_firstname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_firstname') ? ' is-invalid' : ''),'placeholder'=>'Contact Firstname'])}}
                        @foreach($errors->get('contact_firstname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th rowspan="6">Postal Address:</th>
                    <td rowspan="6">{{Form::text('address_ln1',old('address_ln1'),['class'=>'form-control form-control-sm'. ($errors->has('address_ln1') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Ln1'])}}
                        @foreach($errors->get('address_ln1') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('address_ln2',old('address_ln2'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('address_ln2') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Ln2'])}}
                        @foreach($errors->get('address_ln2') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('address_ln3',old('address_ln3'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('address_ln3') ? ' is-invalid' : ''),'placeholder'=>'Postal Address Ln3'])}}
                        @foreach($errors->get('address_ln3') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('suburb',old('suburb'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('suburb') ? ' is-invalid' : ''),'placeholder'=>'City Suburb'])}}
                        @foreach($errors->get('suburb') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('state_province',old('state_province'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('state_province') ? ' is-invalid' : ''),'placeholder'=>'State/Province'])}}
                        @foreach($errors->get('state_province') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::text('postal_zip',old('postal_zip'),['class'=>'form-control form-control-sm input-pad'. ($errors->has('postal_zip') ? ' is-invalid' : ''),'placeholder'=>'Postal/Zip Code'])}}
                        @foreach($errors->get('postal_zip') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                        {{Form::select('country_id',$sidebar_process_countries,'1',['class'=>'form-control form-control-sm input-pad '. ($errors->has('country') ? ' is-invalid' : ''),'id'=>'sidebar_process_countries'])}}
                        @foreach($errors->get('country_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach

                    </td>
                </tr>
                <tr>
                    <th>Contact Lastname:</th>
                    <td>{{Form::text('contact_lastname',old('contact_lastname'),['class'=>'form-control form-control-sm'. ($errors->has('contact_lastname') ? ' is-invalid' : ''),'placeholder'=>'Contact Lastname'])}}
                        @foreach($errors->get('contact_lastname') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Email: <small class="text-muted d-none d-sm-inline">(required)</small><span class="text-danger d-inline d-sm-none">*</span></th>
                    <td>{{Form::text('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                        @foreach($errors->get('email') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>

                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>{{Form::text('phone',old('phone'),['class'=>'form-control form-control-sm phone_number'. ($errors->has('phone') ? ' is-invalid' : ''),'placeholder'=>'Phone'])}}
                        @foreach($errors->get('phone') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Cell:</th>
                    <td>{{Form::text('cell',old('cell'),['class'=>'form-control form-control-sm phone_number'. ($errors->has('cell') ? ' is-invalid' : ''),'placeholder'=>'Cell'])}}
                    @foreach($errors->get('cell') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>

                </tr>
                <tr>
                    <th>Fax:</th>
                    <td>{{Form::text('fax',old('fax'),['class'=>'form-control form-control-sm phone_number'. ($errors->has('fax') ? ' is-invalid' : ''),'placeholder'=>'Fax'])}}
                    @foreach($errors->get('fax') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">BBBEE Information</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th>BBBEE Level:</th>
                    <td>{{Form::select('bbbee_level_id',$sidebar_process_bbbee_level,old('bbbee_level_id'),['class'=>'form-control form-control-sm '. ($errors->has('bbbee_level_id') ? ' is-invalid' : ''),'id'=>'bbbee_level_id'])}}
                        @foreach($errors->get('bbbee_level_id') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th>BBBEE Certificate Expiry Date:</th>
                    <td>{{Form::text('bbbee_certificate_expiry',old('bbbee_certificate_expiry'),['class'=>'datepicker form-control form-control-sm'. ($errors->has('bbbee_certificate_expiry') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Certificate Expiry Date'])}}
                        @foreach($errors->get('bbbee_certificate_expiry') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Black Ownership:</th>
                    <td>{{Form::text('black_ownership', null,['class'=>'form-control form-control-sm'. ($errors->has('black_ownership') ? ' is-invalid' : ''),'placeholder'=>'Black Ownership'])}}
                        @foreach($errors->get('black_ownership') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th>Black Female Ownership:</th>
                    <td>{{Form::text('black_female_ownership', null,['class'=>'form-control form-control-sm'. ($errors->has('black_female_ownership') ? ' is-invalid' : ''),'placeholder'=>'Black Female Ownership'])}}
                        @foreach($errors->get('black_female_ownership') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Black Voting Rights:</th>
                    <td>{{Form::text('black_voting_rights', null,['class'=>'form-control form-control-sm'. ($errors->has('black_voting_rights') ? ' is-invalid' : ''),'placeholder'=>'Black Voting Rights'])}}
                        @foreach($errors->get('black_voting_rights') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th>Black Female Voting Rights:</th>
                    <td>{{Form::text('black_female_voting_rights', null,['class'=>'form-control form-control-sm'. ($errors->has('black_female_voting_rights') ? ' is-invalid' : ''),'placeholder'=>'Black Female Voting Rights'])}}
                        @foreach($errors->get('black_female_voting_rights') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Black Recognition Level:</th>
                    <td>{{Form::text('bbbee_recognition_level',old('bbbee_recognition_level'),['class'=>'form-control form-control-sm'. ($errors->has('bbbee_recognition_level') ? ' is-invalid' : ''),'placeholder'=>'BBBEE Recognition Level'])}}
                        @foreach($errors->get('bbbee_recognition_level') as $error)
                            <div class="invalid-feedback">
                                {{ $error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">Banking Details</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Bank Name:</th>
                    <td>{{Form::text('bank_name',old('bank_name'),['class'=>'form-control form-control-sm'. ($errors->has('bank_name') ? ' is-invalid' : ''),'placeholder'=>'Bank Name'])}}
                    @foreach($errors->get('bank_name') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                    <th>Branch Code:</th>
                    <td>{{Form::text('branch_code',old('branch_code'),['class'=>'form-control form-control-sm'. ($errors->has('branch_code') ? ' is-invalid' : ''),'placeholder'=>'Branch Code'])}}
                    @foreach($errors->get('branch_code') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                </tr>

                <tr>
                    <th>Account Number:</th>
                    <td>{{Form::text('bank_acc_no',old('bank_acc_no'),['class'=>'form-control form-control-sm'. ($errors->has('bank_acc_no') ? ' is-invalid' : ''),'placeholder'=>'Account Number'])}}
                    @foreach($errors->get('bank_acc_no') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
                    <th>Account Name:</th>
                    <td>{{Form::text('account_name',$company_name,['class'=>'form-control form-control-sm'. ($errors->has('account_name') ? ' is-invalid' : ''),'placeholder'=>'Account Name'])}}
                        @foreach($errors->get('account_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Branch Name:</th>
                    <td>{{Form::text('branch_name',old('branch_name'),['class'=>'form-control form-control-sm'. ($errors->has('branch_name') ? ' is-invalid' : ''),'placeholder'=>'Branch Name'])}}
                        @foreach($errors->get('branch_name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th>Bank Account Type:</th>
                    <td>{{Form::select('bank_account_type_id',$bank_account_type_dropdown,null,['class'=>'form-control form-control-sm '. ($errors->has('bank_account_type_id') ? ' is-invalid' : ''),'placeholder'=>'Select Bank Account Type'])}}
                        @foreach($errors->get('bank_account_type_id') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                </tr>
                <tr>
                    <th>Swift Code:</th>
                    <td>{{Form::text('swift_code',old('swift_code'),['class'=>'form-control form-control-sm'. ($errors->has('swift_code') ? ' is-invalid' : ''),'placeholder'=>'Swift Code'])}}
                        @foreach($errors->get('swift_code') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach</td>
                    <th></th>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">Invoice Details</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Invoice Text:</th>
                <td>{{Form::text('invoice_text',old('invoice_text'),['class'=>'form-control form-control-sm'. ($errors->has('invoice_text') ? ' is-invalid' : ''),'placeholder'=>'Invoice Text'])}}
                    @foreach($errors->get('invoice_text') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach</td>
            </tr>
            </tbody>
        </table>
        {{Form::close()}}
    </div>
    </div>
@endsection

@section('extra-js')

    <script>
        $(function () {
            $('#consultants_chosen').css('width', '100%');
            $('.chosen-container').css('width', '100%');

            var max_chars = 15;

            $('.phone_number').keyup( function(e){
                if ($(this).val().length >= max_chars) {
                    $(this).val($(this).val().substr(0, max_chars));
                }
            });

        });
    </script>
@endsection
