@extends('adminlte.default')

@section('title')
Company
@endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
                <button onclick="history.back()" class="btn btn-dark float-right" style="margin-top: -7px;"><i class="fa fa-caret-left"></i> Back</button>
                @if($can_update)
                <a href="{{route('company.edit',$company)}}" class="btn btn-success float-right ml-1" style="margin-top: -7px;">Edit</a>
                @endif
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">General Information</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <tr>
                <th rowspan="5">Company Logo:</th>
                <td rowspan="5"><div class="img-wrapper"><img src="{{route('company_avatar',['q'=>$company->company_logo])}}" id="company-preview-large" /></div></td>
                <th>Company Name:</th>
                <td>{{$company->company_name}}</td>
            </tr>
            <tr>
                <th>Registration Number:</th>
                <td>{{$company->business_reg_no}}</td>
            </tr>
            <tr>
                <th>VAT Number:</th>
                <td>{{$company->vat_no}}</td>
            </tr>
            <tr>
                <th>Web:</th>
                <td>{{$company->web}}</td>
            </tr>
            <tr>
                <th>Status:</th>
                <td>{{$company->status?->description}}</td>
            </tr>

            <tr>
                <th>Contact Person:</th>
                <td>{{$company->contact_firstname}} {{$company->contact_lastname}}</td>
                <th rowspan="5">Postal Address:</th>
                <td rowspan="5">
                    {{$company->postal_address_line1}}<br />
                    {{$company->postal_address_line2}}<br />
                    {{$company->postal_address_line2}}<br />
                    {{$company->city_suburb}}
                    {{$company->state_province}}
                    {{$company->postal_zipcode}}
                    {{$company->country->description}}
                </td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{{$company->email}}</td>

            </tr>
            <tr>
                <th>Phone:</th>
                <td>{{$company->phone}}</td>
            </tr>
            <tr>
                <th>Cell:</th>
                <td>{{$company->cell}}</td>
            </tr>
            <tr>
                <th>Fax:</th>
                <td>{{$company->fax}}</td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">BBBEE Information</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>BBBEE Level</th>
                <td>{{$company->bbbee_level?->description}}</td>
                <th>BBBEE Certificate Expiry Date</th>
                <td>{{$company->bbbee_certificate_expiry}}</td>
            </tr>
            <tr>
                <th>Black Ownership</th>
                <td>{{$company->black_ownership}}</td>
                <th>Black Female Ownership</th>
                <td>{{$company->black_female_ownership}}</td>
            </tr>
            <tr>
                <th>Black Voting Rights</th>
                <td>{{$company->black_voting_rights}}</td>
                <th>Black Female Voting Rights</th>
                <td>{{$company->black_female_voting_rights}}</td>
            </tr>
            <tr>
                <th>BBBEE Recognition Level</th>
                <td>{{$company->bbbee_recognition_level}}</td>
                <th></th>
                <td></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="4" class="btn-dark">Banking Details</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Bank Name:</th>
                <td>{{$company->bank_name}}</td>
                <th>Branch Code:</th>
                <td>{{$company->branch_code}}</td>
            </tr>
            <tr>
                <th>Account Number:</th>
                <td>{{$company->bank_acc_no}}</td>
                <th>Account Name:</th>
                <td>{{$company->account_name}}</td>
            </tr>
            <tr>
                <th>Branch Name:</th>
                <td>{{$company->branch_name}}</td>
                <th>Bank Account Type:</th>
                <td>{{$company->bankAccountType?->description}}</td>
            </tr>
            <tr>
                <th>Swift Code:</th>
                <td>{{$company->swift_code}}</td>
                <th></th>
                <td></td>
            </tr>
            </tbody>
        </table>

        <table class="table table-bordered table-sm mt-3">
            <thead>
            <tr>
                <th colspan="5" class="btn-dark">Teams</th>
            </tr>
            <tr>
                <th>Team Name</th>
                <th>Team Manager</th>
                <th>Status</th>
                <th>Created By</th>
                <th class="last">Actions</th>
            </tr>
            </thead>
            <tbody>
            @forelse($teams as $team)
                <tr>
                    <td>{{$team->team_name}}</td>
                    <td>{{$team->manager?->name()}}</td>
                    <td>{{$team->status?->description}}</td>
                    <td>{{$team->creator?->name()}}</td>
                    <td>
                        <div class="d-flex">
                            <a href="{{route('team.edit', $team)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            {{ Form::open(['method' => 'DELETE','route' => ['team.destroy', $team],'style'=>'display:inline','class'=>'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm ml-1 mr-1"><i class="fas fa-trash"></i></button>
                            {{ Form::close() }}
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">There are no teams for this company</td>
                </tr>
            @endforelse
            <tr>
                <td colspan="5" class="text-center"><a href="{{route('team.create',['company_id' => $company])}}" class="btn btn-dark btn-sm">Add Team</a></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
