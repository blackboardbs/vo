<div class="dropdown">
    <button class="btn btn-dark dropdown-toggle mr-1" type="button" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-file"></i> Documents
    </button>
    <div class="dropdown-menu">
        @if($vendor)
            <a class="dropdown-item" target="_blank" href="{{route("assignment.supplier.print", $id)}}"><i class="fa fa-print"></i> Print Supplier Agreement</a>
            <a class="dropdown-item" href="{{route("assignment.supplier.send", $id)}}"><i class="fa fa-envelope"></i> Send Supplier Agreement</a>
        @endif
        <a class="dropdown-item" target="_blank" href="{{route("assignment.resource.print", $id)}}"><i class="fa fa-print"></i> Print Resource Agreement</a>
        <a class="dropdown-item" href="{{route('assignment.resource.send', $id)}}"><i class="fa fa-envelope"></i> Send Resource Agreement</a>
    </div>
</div>
