@extends('adminlte.default')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Create Role</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
                    </li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                {{Form::open(['url' => route('roles.store'), 'method' => 'post'])}}
                <div class="form-group mt-3">
                    {{Form::label('name', 'Name')}}
                    {{Form::text('name',old('name'),['class'=>'form-control'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                    @foreach($errors->get('name') as $error)
                    <div class="invalid-feedback">
                        {{ $error }}
                    </div>
                    @endforeach
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-hover">
                        @foreach($permissions as $permission)
                            <tr>

                                @if($permission["parent_id"] == '0')
                                    <td class="last"><strong>{{$permission["display_name"]}}</strong></td>

                                        @if($permission["parent_id"] == '0' && $permission["subparent_id"] != null)
                                            <td><select name="permissions[{{$permission["id"]}}]" class="form-control form-control-sm ">
                                                    <option value="">Select</option>
                                                    <option value="{{$permission["id"]+1}}">5 - Full</option>
                                                    <option value="{{$permission["id"]+2}}">4 - Delete</option>
                                                    <option value="{{$permission["id"]+3}}">3 - Update</option>
                                                    <option value="{{$permission["id"]+4}}">2 - Create</option>
                                                    <option value="{{$permission["id"]+5}}">1 - View</option>
                                                    <option value="{{$permission["id"]+6}}">0 - None</option>
                                                </select></td>
                                        @endif
                                @endif

                                @if($permission["parent_id"] > '0' && $permission["level"] == null)
                                    <td class="last" style="padding-left:15px">{{$permission["display_name"]}}</td>

                                        @if($permission["parent_id"] > '0' && $permission["subparent_id"] != null)
                                            <td><select name="permissions[{{$permission["id"]}}]" class="form-control form-control-sm ">
                                                    <option value="">Select</option>
                                                    <option value="{{$permission["id"]+1}}">5 - Full</option>
                                                    <option value="{{$permission["id"]+2}}">4 - Delete</option>
                                                    <option value="{{$permission["id"]+3}}">3 - Update</option>
                                                    <option value="{{$permission["id"]+4}}">2 - Create</option>
                                                    <option value="{{$permission["id"]+5}}">1 - View</option>
                                                    <option value="{{$permission["id"]+6}}">0 - None</option>
                                                </select></td>
                                        @endif
                                @endif







                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn">Save</button>
                </div>
                {{Form::close()}}
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection