@extends('adminlte.default')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Roles</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            @permission(['roles_2','roles_3','roles_4','roles_5'])
                            <a href="{{route('roles.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Role</a>
                            @endpermission
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            {{--<form class="form-inline mt-3">
                Show &nbsp;
                {{Form::select('s',['all'=>'All','mine'=>'My','company'=>'Branch'],old('selection'),['class'=>'form-control form-control-sm'])}}
                &nbsp; matching &nbsp;
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
                </div>
            </form>

            <hr>--}}
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    {{Form::open(['url' => route('roles.update_vendor'), 'method' => 'post','class'=>'mt-3 mb-3'])}}
                    <div class="table-responsive" id="roles">
                        {{--<table id="tbl-roles" class="table table-bordered table-sm table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    @foreach($permissions as $permission)
                                        <th>{{$permission->display_name}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($roles as $role)
                                    <tr>
                                        <td>{{$role->display_name}}</td>
                                        @foreach($permissions as $permission)
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input name="permission[{{$role->id.']['.$permission->id}}]" {{($role->permissions->where('id',$permission->id)->count() > 0) ? 'checked' : ''}} type="checkbox" class="custom-control-input" id="permission[{{$role->id.']['.$permission->id}}]" value="{{$permission->id}}">
                                                    <label class="custom-control-label" for="permission[{{$role->id.']['.$permission->id}}]">&nbsp;</label>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100%" class="text-center">No roles match those criteria.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>--}}
                        <table id="tbl-roles" class="table table-bordered table-sm table-hover fixed_headers table-fixed" style="height:100%;">
                            <thead class="thead-light">
                            <tr>
                                <th></th>
                                @foreach($roles as $role)
                                    <th class="text-center">{{$role["display_name"]}}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody id="role">
                            {{--@php
                            echo $html;
                            @endphp--}}
                            @foreach($permissions as $permission)


                                    @if($permission["parent_id"] == '0' && $permission["is_sub_level"] == '0')
                                    <tr>     <td><strong>{{$permission["display_name"]}}</strong></td>

                                        @foreach($roles as $role)
                                            @if($permission["parent_id"] == '0' && $permission["subparent_id"] != null)
                                                <td><select name="permissions[{{$role->id.']['.$permission["id"]}}]" class="form-control form-control-sm">
                                                        <option value="">Select</option>
                                                        <option value="{{$permission["id"]+1}}" {{($role->permissions->where('id',$permission["id"]+1)->count() > 0) ? 'selected' : ''}}>5 - Full</option>
                                                        <option value="{{$permission["id"]+2}}" {{($role->permissions->where('id',$permission["id"]+2)->count() > 0) ? 'selected' : ''}}>4 - Delete</option>
                                                        <option value="{{$permission["id"]+3}}" {{($role->permissions->where('id',$permission["id"]+3)->count() > 0) ? 'selected' : ''}}>3 - Update</option>
                                                        <option value="{{$permission["id"]+4}}" {{($role->permissions->where('id',$permission["id"]+4)->count() > 0) ? 'selected' : ''}}>2 - Create</option>
                                                        <option value="{{$permission["id"]+5}}" {{($role->permissions->where('id',$permission["id"]+5)->count() > 0) ? 'selected' : ''}}>1 - View</option>
                                                        <option value="{{$permission["id"]+6}}" {{($role->permissions->where('id',$permission["id"]+6)->count() > 0) ? 'selected' : ''}}>0 - None</option>
                                                    </select></td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    @endif

                                        @if($permission["parent_id"] > '0' && $permission["level"] == null && $permission["is_sub_level"] == '0')
                                  <tr>
                                      <td style="padding-left:15px">{{$permission["display_name"]}}</td>

                                            @foreach($roles as $role)
                                                @if($permission["parent_id"] > '0' && $permission["subparent_id"] != null)
                                                    <td><select name="permissions[{{$role->id.']['.$permission["id"]}}]" class="form-control form-control-sm">
                                                            <option value="">Select</option>
                                                            <option value="{{$permission["id"]+1}}" {{($role->permissions->where('id',$permission["id"]+1)->count() > 0) ? 'selected' : ''}}>5 - Full</option>
                                                            <option value="{{$permission["id"]+2}}" {{($role->permissions->where('id',$permission["id"]+2)->count() > 0) ? 'selected' : ''}}>4 - Delete</option>
                                                            <option value="{{$permission["id"]+3}}" {{($role->permissions->where('id',$permission["id"]+3)->count() > 0) ? 'selected' : ''}}>3 - Update</option>
                                                            <option value="{{$permission["id"]+4}}" {{($role->permissions->where('id',$permission["id"]+4)->count() > 0) ? 'selected' : ''}}>2 - Create</option>
                                                            <option value="{{$permission["id"]+5}}" {{($role->permissions->where('id',$permission["id"]+5)->count() > 0) ? 'selected' : ''}}>1 - View</option>
                                                            <option value="{{$permission["id"]+6}}" {{($role->permissions->where('id',$permission["id"]+6)->count() > 0) ? 'selected' : ''}}>0 - None</option>
                                                        </select></td>
                                                @endif
                                            @endforeach
                                </tr>
                                    @endif

                                        @if($permission["parent_id"] > '0' && $permission["level"] == "0" && $permission["is_sub_level"] == '1' and $permission["sub_level"] == '0')
                                     <tr>
                                         <td style="padding-left:30px">{{$permission["display_name"]}}</td>

                                            @foreach($roles as $role)
                                                @if($permission["parent_id"] > '0' && $permission["subparent_id"] != null)
                                                    <td><select name="permissions[{{$role->id.']['.$permission["id"]}}]" class="form-control form-control-sm">
                                                            <option value="">--</option>
                                                            <option value="{{$permission["id"]+1}}" {{($role->permissions->where('id',$permission["id"]+1)->count() > 0) ? 'selected' : ''}}>5 - Full</option>
                                                            <option value="{{$permission["id"]+2}}" {{($role->permissions->where('id',$permission["id"]+2)->count() > 0) ? 'selected' : ''}}>4 - Delete</option>
                                                            <option value="{{$permission["id"]+3}}" {{($role->permissions->where('id',$permission["id"]+3)->count() > 0) ? 'selected' : ''}}>3 - Update</option>
                                                            <option value="{{$permission["id"]+4}}" {{($role->permissions->where('id',$permission["id"]+4)->count() > 0) ? 'selected' : ''}}>2 - Create</option>
                                                            <option value="{{$permission["id"]+5}}" {{($role->permissions->where('id',$permission["id"]+5)->count() > 0) ? 'selected' : ''}}>1 - View</option>
                                                            <option value="{{$permission["id"]+6}}" {{($role->permissions->where('id',$permission["id"]+6)->count() > 0) ? 'selected' : ''}}>0 - None</option>
                                                        </select></td>
                                                @endif
                                            @endforeach
                                </tr>
                                        @endif
                            @endforeach

                            </tbody>
                        </table>
                        <br />
                        <br />
                    </div>
                    <div class="blackboard-fab mr-3 mb-3">
                        @permission(['roles_3','roles_4','roles_5'])
                            </tbody>
                        </table>
                        <br />
                        <br />
                    </div>
                    <button type="submit" class="btn btn-primary" style="float: right;position:fixed;right:10px;bottom:10px"><i class="fa fa-save"></i> Save</button>
                    @endpermission
                </div>
                {{Form::close()}}
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@section('extra-css')
    <style>
        .table-fixed {
            width: 100%;
        }

        /*This will work on every browser but Chrome Browser*/


        /*This will work on every browser*/
        .table-fixed thead th {
            position: sticky;
            position: -webkit-sticky;
            top: 0;
            z-index: 999;
            background-color: #000;
            color: #fff;
        }
    </style>
@endsection
@section('extra-js')
    <script src="adminlte/dist/js/jquery.stickytableheaders.js"></script>
    <script>
        /*$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "/get_roles/vendor",
            data: {   },
            success: function(data)
            {
                $('#role').html(data);
                $( ".accordian select" ).css('visibilty','hidden');
                $( ".accordian" ).css('visibilty','collapse');
            }
        });*/

        var offset = 0;
        $("html:not(.legacy) table").stickyTableHeaders({fixedOffset: offset});

        $( function() {
            $( ".accordion" ).accordion({
                collapsible: true
            });
        } );

    </script>
@endsection