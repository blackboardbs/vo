@extends('adminlte.default')
@section('title') User Onboarding @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @if($access_appointing_manager)
            {{ Form::open(['method' => 'POST','route' => ['useronboarding.enable', $onboarding],'style'=>'display:inline', 'class' => 'float-right ml-2']) }}
            {{ Form::submit('Enable Onboarding Edit', ['class' => 'btn btn-success']) }}
            {{ Form::close() }}
        @endif
        @if($onboarding->commited_at != '' && !isset($onboarding->appointingManager) && !auth()->user()->hasRole('user_onboarding'))
            <a href="{{route('useronboarding.appontmentmanager.create', $onboarding)}}" class="btn btn-dark float-right ml-2"><i class="fa fa-plus"></i> Appointing Manager Form</a>
        @endif
        @if(!$onboarding->commited_at && auth()->user()->id == $onboarding->user_id && !isset($onboarding->appointingManager))
            <a href="{{route('useronboarding.edit',$onboarding)}}" class="btn btn-success float-right ml-2">Edit</a>
        @endif
        @if($onboarding->appointingManager?->contract_type_id == 1)
            <a href="{{route('appontmentmanager.print.permanent',$onboarding->user)}}" target="_blank" class="btn btn-dark float-right ml-2"><i class="fa fa-print"></i> Print Contract</a>
            <a href="{{route('appontmentmanager.send',['user' => $onboarding->user, 'type' => 'fixed'])}}" class="btn btn-dark float-right ml-2"><i class="fa fa-envelope"></i> Send Contract</a>
        @endif
        @if($onboarding->appointingManager?->contract_type_id == 2)
            <a href="{{route('appontmentmanager.print.fixed',$onboarding->user)}}" target="_blank" class="btn btn-dark float-right ml-2"><i class="fa fa-print"></i> Print Contract</a>
            <a href="{{route('appontmentmanager.send',['user' => $onboarding->user, 'type' => 'fixed'])}}" class="btn btn-dark float-right ml-2"><i class="fa fa-envelope"></i> Send Contract</a>
        @endif

        <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="6" class="btn-dark" style="text-align: center;">User Header</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>First Name:</th>
                    <td>
                        {{ Form::text('name', ($onboarding->user->first_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                    </td>
                    <th>Last Name:</th>
                    <td>
                        {{ Form::text('lname', ($onboarding->user->last_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                    </td>
                    <th>Email:</th>
                    <td>
                        {{ Form::text('email', ($onboarding->user->email??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                    </td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td>
                        {{ Form::text('phone', ($onboarding->user->phone??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                    </td>
                    <th>Vendor:</th>
                    <td>
                        {{ Form::text('vendor', ($onboarding->user->vendor->vendor_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th class="text-center" colspan="6">Personal Details</th>
                </tr>
                <tr>
                    <th>Title:</th>
                    <td>{{ Form::select('title_id', $title_dropdown, $onboarding->title_id, ['class' => 'form-control form-control-sm chosen-select', 'disabled', 'placeholder' => "Select Title"]) }}</td>
                    <th>Known As Name:</th>
                    <td>{{ Form::text('Known_as', $onboarding->Known_as, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>ID Number:</th>
                    <td>{{ Form::text('id_number', $onboarding->id_number, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Date Of Birth:</th>
                    <td>{{ Form::text('date_of_birth', $onboarding->date_of_birth, ['class' => 'form-control form-control-sm datepicker', 'disabled']) }}</td>
                    <th>Passport Number:</th>
                    <td>{{ Form::text('passport_number', $onboarding->passport_number, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Passport Country:</th>
                    <td>{{ Form::select('country_id', $country_dropdown, $onboarding->country_id, ['class' => 'form-control form-control-sm chosen-select', 'disabled', 'placeholder' => 'Select Country']) }}</td>
                </tr>
                <tr class="bg-dark">
                    <th class="text-center" colspan="6">Contact Details</th>
                </tr>
                <tr>
                    <th>Physical Address:</th>
                    <td colspan="5">{{ Form::text('physical_address', $onboarding->contactDetails->physical_address??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Postal Address:</th>
                    <td colspan="5">{{ Form::text('postal_address', $onboarding->contactDetails->postal_address??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Email Address:</th>
                    <td colspan="2">{{ Form::text('email', $onboarding->contactDetails->email??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Home Number:</th>
                    <td colspan="2">{{ Form::text('home_number', $onboarding->contactDetails->home_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Work Number:</th>
                    <td colspan="2">{{ Form::text('work_number', $onboarding->contactDetails->work_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Cell Number:</th>
                    <td colspan="2">{{ Form::text('cell_number', $onboarding->contactDetails->cell_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Next Of Kin Name:</th>
                    <td colspan="2">{{ Form::text('next_of_kin_name', $onboarding->contactDetails->next_of_kin_name??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Next Kin Number:</th>
                    <td colspan="2">{{ Form::text('next_of_kin_number', $onboarding->contactDetails->next_of_kin_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr class="bg-dark">
                    <th class="text-center" colspan="6">Payment Details</th>
                </tr>
                <tr>
                    <th>Type Of Account:</th>
                    <td>{{ Form::select('bank_account_type_id', $account_type_dropdown, $onboarding->paymentDetails->bank_account_type_id??null, ['class' => 'form-control form-control-sm chosen-select', 'disabled', 'placeholder' => 'Select Bank Account Type']) }}</td>
                    <th>Account Name:</th>
                    <td>{{ Form::text('account_name', $onboarding->paymentDetails->account_name??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Account Holder Relation:</th>
                    <td>{{ Form::select('relationship_id', $relationship_dropdown, $onboarding->paymentDetails->relationship_id??null, ['class' => 'form-control form-control-sm chosen-select', 'disabled', 'placeholder' => 'Select A Relationship']) }}</td>
                </tr>
                <tr>
                    <th>Bank Name:</th>
                    <td>{{ Form::text('bank_name', $onboarding->paymentDetails->bank_name??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Branch Number:</th>
                    <td>{{ Form::text('branch_number', $onboarding->paymentDetails->branch_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Branch Name:</th>
                    <td>{{ Form::text('branch_name', $onboarding->paymentDetails->branch_name??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Account Number:</th>
                    <td>{{ Form::text('account_number', $onboarding->paymentDetails->account_number??null, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th></th>
                    <td></td>
                    <th></th>
                    <td></td>
                </tr>
                <tr class="bg-dark">
                    <th class="text-center" colspan="6">SARS Details</th>
                </tr>
                <tr>
                    <th>Directive Number:</th>
                    <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[0]??null), ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Directive Number:</th>
                    <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[1]??null), ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Directive Number:</th>
                    <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[2]??null), ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr>
                    <th>Directive Number:</th>
                    <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[3]??null), ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Directive Number:</th>
                    <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[4]??null), ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                    <th>Income Tax Number:</th>
                    <td>{{ Form::text('income_tax_no', $onboarding->income_tax_number, ['class' => 'form-control form-control-sm', 'disabled']) }}</td>
                </tr>
                <tr class="bg-dark">
                    <th class="text-center" colspan="6">Upload Documents</th>
                </tr>
                <tr>
                    <th>ID/Passport:</th>
                    <td>
                        @if($onboarding->id_passport)
                            <a href="{{route('id_passport', ['q' => $onboarding->id_passport])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>

                        @else
                            No document was uploaded.
                        @endif
                    </td>
                    <th>Proof of Address:</th>
                    <td>
                        @if($onboarding->proof_of_address)
                            <a href="{{route('proof_of_address', ['q' => $onboarding->proof_of_address])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                        @else
                            No document was uploaded.
                        @endif
                    </td>
                    <th>Proof of Bank Account:</th>
                    <td>
                        @if(isset($onboarding->paymentDetails->proof_of_bank_account))
                            <a href="{{route('proof_of_bank_account', ['q' => $onboarding->paymentDetails->proof_of_bank_account])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                        @else
                            No document was uploaded.
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                {{--<tr>
                    <th colspan="2">POIA Declaration:</th>
                    <td colspan="4">
                        <h4>POPI ACT<br>
                            AGREEMENT AND CONSENT DECLARATION
                        </h4>
                        <p>
                            CONSENT TO PROCESS PERSONAL INFORMATION IN TERMS OF THE PROTECTION OF<br>
                            P E T R S O N A L INFORMATION ACT, N O . 4 OF 2013 (POPIA)<br>
                            The purpose of the POPIA is to protect personal information of individuals and businesses and to<br>
                            give effect to their right of privacy as provided for in the Constitution. By signing this form, you<br>
                            consent to your personal information to be processed by {{ App\Company::first(['company_name'])->company_name??null }}<br>
                            and consent is effective immediately and will remain effective until such consent is<br>
                            withdrawn.

                        </p>
                    </td>
                </tr>
                <tr>
                    <th>Tick to accept:</th>
                    <td colspan="5">{{ Form::checkbox('id_passport', $onboarding->id_passport, ['class' => 'form-control form-control-sm', 'disabled' => true]) }}</td>
                </tr>--}}
                </tbody>
            </table>
            @if(!auth()->user()->hasRole('user_onboarding'))
            @isset($onboarding->appointingManager)
                <table class="table table-bordered table-sm">
                    <thead>
                    <tr class="bg-dark">
                        <th colspan="2" style="vertical-align:middle" class="pl-1">Appointing Manager</th>
                        <th colspan="2" class="text-right py-1">
                            @if(!$onboarding->appointment_confirmed_at)
                            <a href="{{route('appontmentmanager.edit', $onboarding->appointingManager->id)}}" class="btn btn-sm btn-success text-uppercase" style="color: #fff !important;background-color: #28a745 !important;border-color: #28a745 !important;">Edit Appointing Manager</a>
                            @endif
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Contract Type:</th>
                        <td>{{Form::select('contract_type_id', $contract_type_dropdown, $onboarding->appointingManager->contract_type_id, ['class' => 'form-control form-control-sm chosen-select', 'placeholder' => 'Select Contract Type', 'disabled'])}}</td>
                        <th>Payment Base:</th>
                        <td>{{Form::select('payment_base_id', $payment_base_dropdown, $onboarding->appointingManager->payment_base_id, ['class' => 'form-control form-control-sm chosen-select', 'placeholder' => 'Select Payment Base', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Payment Type</th>
                        <td>{{Form::select('payment_type_id', $payment_type_dropdown, $onboarding->appointingManager->payment_type_id, ['class' => 'form-control form-control-sm chosen-select', 'placeholder' => 'Select Payment Type', 'disabled'])}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Start Date:</th>
                        <td>{{Form::text('start_date', $onboarding->appointingManager->start_date, ['class' => 'form-control form-control-sm datepicker', 'placeholder' => 'YYYY-MM-DD', 'disabled'])}}</td>
                        <th>End Date: <small class="text-muted">(If not permanent)</small></th>
                        <td>{{Form::text('end_date', $onboarding->appointingManager->end_date??null, ['class' => 'form-control form-control-sm datepicker', 'placeholder' => 'YYYY-MM-DD', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Position:</th>
                        <td>{{Form::text('position', $onboarding->appointingManager->position??null, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                        <th>Annual Salary:</th>
                        <td>{{Form::text('annual_salary', $onboarding->appointingManager->annual_salary??null, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Leave Days:</th>
                        <td>{{Form::text('leave_days', $onboarding->appointingManager->leave_days??null, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                        <th>Notice Period: <small class="text-muted">(If vendor type)</small></th>
                        <td>{{Form::text('notice_period', $onboarding->appointingManager->notice_period??null, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Reporting Manager:</th>
                        <td>{{Form::select('reporting_manager', $reporting_manager_dropdown, $onboarding->appointingManager->reporting_manager??null, ['class' => 'form-control form-control-sm chosen-select', 'placeholder' => 'Select Reporting Manager', 'disabled'])}}</td>
                        <th>Other Income:</th>
                        <td>{{Form::text('other_income', $onboarding->appointingManager->other_income??null, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Other Conditions:</th>
                        <td>{{Form::textarea('other_conditions', $onboarding->appointingManager->other_conditions??null, ['class' => 'form-control form-control-sm', 'rows' => 4, 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Does the employee require a laptop?</th>
                        <td>{{Form::text('require_laptop', $onboarding->appointingManager->require_laptop == 1 ? 'Yes' : 'No', ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                        <th>Does the user require a Mobile phone?</th>
                        <td>{{Form::text('require_phone', $onboarding->appointingManager->require_phone?->name, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Does the user require Mobile Internet?</th>
                        <td>{{Form::text('require_mobile_internet', $onboarding->appointingManager->require_mobile_internet?->name, ['class' => 'form-control form-control-sm', 'disabled'])}}</td>
                    </tr>
                    <tr>
                        <th>Notes on onboarding:</th>
                        <td>{{Form::textarea('notes', $onboarding->appointingManager->notes??null, ['class' => 'form-control form-control-sm', 'rows' => 4, 'disabled'])}}</td>
                        <th>Responsibilities</th>
                        <td>{{Form::textarea('responsibilities', $onboarding->appointingManager->responsibilities??null, ['class' => 'form-control form-control-sm', 'rows' => 4, 'disabled'])}}</td>
                    </tr>
                    <tr class="bg-dark">
                        <th colspan="4" class="text-center">Templates</th>
                    </tr>
                    <tr>
                        <th>Permanent Contract Template </th>
                        <td>{{Form::select('permenant_contract_template_id', $permanent_term_dropdown, $onboarding->appointingManager->permenant_contract_template_id, ['class' => 'form-control form-control-sm', 'placeholder' => "Select Template", 'disabled'])}}</td>
                        <th>Fixed Term Contract Template</th>
                        <td>{{Form::select('fixed_contract_template_id', $fixed_term_dropdown, $onboarding->appointingManager->fixed_contract_template_id, ['class' => 'form-control form-control-sm', 'placeholder' => "Select Template"])}}</td>
                    </tr>
                    </tbody>
                </table>
                @if(!$onboarding->appointment_confirmed_at && $access_appointing_manager)
                {{ Form::open(['method' => 'POST', 'route' => ['useronboarding.appontment.confirm', $onboarding],'style'=>'display:inline']) }}
                <center><button type="submit" class="btn btn-dark">Confirm Appointment</button></center>
                {{ Form::close() }}
                @endif
                <br/>
                <br/>
            @endisset
            @endif
        </div>
    </div>
@endsection
