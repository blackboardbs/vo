@extends('adminlte.default')
@section('title') User Onboarding @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <x-export route="useronboarding.index"></x-export>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                        <input type="text" name="q" class="form-control w-100" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" onkeyup="handle(event)" />
                <span>Matching</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('appointment_manager_id',$appointment_manager, null, ['class'=>'form-control search chosen-select', 'style'=>'width: 100%;', 'placeholder' => 'Select Appointment Manager'])}}
                <span>Appointment Manager</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-3 col-sm">
                <div class="form-group input-group">
                    <label class="has-float-label">
                {{Form::select('status_id',$appointment_status,null,['class'=>'form-control search chosen-select', 'style'=>'width: 100%;', 'placeholder' => "Select Appointment Status"])}}
                <span>Onboarding Status</span>
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <a href="{{route('useronboarding.index')}}" class="btn btn-info w-100">Clear Filters</a>
            </div>
        </form>
        {{--<form class="form-inline mt-3 searchform" id="searchform" autocomplete="off">
            <div class="col-sm-3 col-sm">
                Search<br/>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm search', 'style'=>'width: 100%;','placeholder'=>'Search...'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Company<br/>
                {{Form::select('company',$company_drop_down, null, ['class'=>'form-control form-control-sm search chosen-select', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Customer<br/>
                {{Form::select('customer',$customer_drop_down,null,['class'=>'form-control form-control-sm search chosen-select', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Status<br/>
                {{Form::select('project_status',$project_status_drop_down,null,['class'=>'form-control form-control-sm search chosen-select', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Resource<br/>
                {{Form::select('resource',$resource_drop_down,old('resource'),['class'=>'chosen-select form-control form-control-sm search', 'style'=>'width: 100%;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                Start Date<br/>
                {{Form::text('start_date',null,['class'=>'datepicker start_date form-control form-control-sm  col-sm-12 search'. ($errors->has('start_date') ? ' is-invalid' : ''), 'placeholder'=>'Start Date','style'=>'width:100% !important;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                End Date<br/>
                {{Form::text('end_date',null,['class'=>'datepicker end_date form-control form-control-sm  col-sm-12 search'. ($errors->has('end_date') ? ' is-invalid' : ''), 'placeholder'=>'End Date','style'=>'width:100% !important;'])}}
            </div>
            <div class="col-sm-3 col-sm">
                <a href="{{route('project.index')}}" style="margin-top: 23px;" class="btn btn-default btn-sm" type="submit">Clear Filters</a>
            </div>
        </form>--}}
        <hr>
        <div style="display: block;overflow-x: auto;white-space: nowrap;">
            <table class="table table-bordered table-sm table-hover">
                <thead>
                <tr class="btn-dark">
                    <th>User Name</th>
                    <th>User Surname</th>
                    <th>Appointment Manager</th>
                    <th>Approver</th>
                    <th>Onboarding Status</th>
                    <th>User Type</th>
                    <th>Contract</th>
                    <th>Payment Type</th>
                    <th>Payment Base</th>
                    <th>User Created</th>
                    <th>Information Updated</th>
                    <th>Contract Details </th>
                    <th>Approved</th>
                    <th>Contract Issued</th>
                    <th>Contract Received</th>
                    <th>Contract Signed</th>
                    <th>Contract filed</th>
                    <th>Notes</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($onboardings as $onboarding)
                        <tr>
                            <td>
                                <a href="{{route('useronboarding.show',$onboarding)}}">{{$onboarding->user->first_name??null}}</a>
                            </td>
                            <td>
                                <a href="{{route('useronboarding.show',$onboarding)}}">{{$onboarding->user->last_name??null}}</a>
                            </td>
                            <td>{{$onboarding->user->appointmentManager->first_name??null}}</td>
                            <td>{{$approver}}</td>
                            <td>{{$onboarding->status->description??null}}</td>
                            <td>{{$onboarding->user->userType->description??null}}</td>
                            <td>{{$onboarding->appointingManager->contractType->description??null}}</td>
                            <td>{{$onboarding->appointingManager->paymentType->description??null}}</td>
                            <td>{{$onboarding->appointingManager->paymentBase->description??null}}</td>
                            <td>{{Carbon\Carbon::parse($onboarding->created_at)->toDateString()}}</td>
                            <td>{{$onboarding->commited_at != '' ? Carbon\Carbon::parse($onboarding->commited_at)->toDateString() : ''}}</td>
                            <td>{{isset($onboarding->appointingManager->created_at)?Carbon\Carbon::parse($onboarding->appointingManager->created_at)->toDateString():null}}</td>
                            <td>{{isset($onboarding->appointingManager->approved_at)?Carbon\Carbon::parse($onboarding->appointingManager->approved_at)->toDateString():null}}</td>
                            <td>{{isset($onboarding->appointingManager->issued_at)?Carbon\Carbon::parse($onboarding->appointingManager->issued_at)->toDateString():null}}</td>
                            <td>{{isset($onboarding->appointingManager->recieved_at)?Carbon\Carbon::parse($onboarding->appointingManager->recieved_at)->toDateString():null}}</td>
                            <td>{{isset($onboarding->appointingManager->signed_at)?Carbon\Carbon::parse($onboarding->appointingManager->signed_at)->toDateString():null}}</td>
                            <td>{{isset($onboarding->appointingManager->filed_at)?Carbon\Carbon::parse($onboarding->appointingManager->filed_at)->toDateString():null}}</td>
                            <td>{{$onboarding->appointingManager->notes??null}}</td>
                            <td>

                                <a href="{{route('useronboarding.show',$onboarding)}}" class="btn btn-sm btn-info"><i class="fas fa-eye" aria-hidden="true"></i></a>
                                @if(!isset($onboarding->appointingManager) && !auth()->user()->hasRole('user_onboarding') && !isset($onboarding->commited_at))
                                    <a href="{{route('useronboarding.edit',$onboarding)}}" class="btn btn-success btn-sm ml-1"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
                                @endif
                                @if(auth()->user()->hasRole('user_onboarding') && !isset($onboarding->commited_at))
                                    <a href="{{route('useronboarding.edit',$onboarding)}}" class="btn btn-success btn-sm ml-1"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
                                @endif
                                @if($onboarding->status_id == 3 && ((auth()->id() == $approval_manager_id) || auth()->user()->hasAnyRole(['admin','admin_manager'])))
                                    {{ Form::open(['method' => 'POST', 'route' => ['contract.approve', $onboarding],'style'=>'display:inline']) }}
                                    {{ Form::submit('Approve', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                    {{ Form::close() }}
                                @elseif(auth()->id() == $hr_id)
                                    @switch($onboarding->status_id)
                                        @case(4)
                                            {{ Form::open(['method' => 'POST', 'route' => ['contract.issued', $onboarding],'style'=>'display:inline']) }}
                                            {{ Form::submit('Issue Contract', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                            {{ Form::close() }}
                                            @break
                                        @case(5)
                                            {{ Form::open(['method' => 'POST','route' => ['contract.received', $onboarding], 'style'=>'display:inline']) }}
                                            {{ Form::submit('Contract Received', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                            {{ Form::close() }}
                                            @break
                                        @case(6)
                                            {{ Form::open(['method' => 'POST','route' => ['contract.signed', $onboarding],'style'=>'display:inline']) }}
                                            {{ Form::submit('Contract Signed', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                            {{ Form::close() }}
                                            @break
                                        @case(7)
                                            {{ Form::open(['method' => 'POST','route' => ['contract.filed', $onboarding],'style'=>'display:inline']) }}
                                            {{ Form::submit('Contract Filed', ['class' => 'btn btn-dark btn-sm ml-1']) }}
                                            {{ Form::close() }}
                                            @break
                                    @endswitch
                                @endif
                                @if(!auth()->user()->hasRole('user_onboarding'))
                                {{ Form::open(['method' => 'DELETE','route' => ['useronboarding.destroy', $onboarding],'style'=>'display:inline','class'=>'delete']) }}
                                
                                <button type="submit" class="btn btn-sm btn-danger ml-1"><i class="fas fa-trash"></i></button>
                                {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="13" class="text-center">There is currently no onboarding user</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
