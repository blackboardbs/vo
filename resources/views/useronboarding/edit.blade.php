@extends('adminlte.default')
@section('title') User Onboarding @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <div class="btn-group mr-2">
                @if(auth()->user()->id != $onboarding->user->id)
                <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr />
        <div class="table-responsive">
            {{Form::open(['url' => route('useronboarding.update', $onboarding), 'method' => 'put','class'=>'mt-3', 'files' => true,'autocomplete'=>'off', 'id' => 'onboarding-form'])}}
            <table class="table table-bordered table-sm mt-3">
                <thead>
                <tr>
                    <th colspan="6" class="btn-dark" style="text-align: center;">User Header</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>First Name:</th>
                        <td>
                            {{ Form::text('name', ($onboarding->user->first_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                        </td>
                        <th>Last Name:</th>
                        <td>
                            {{ Form::text('lname', ($onboarding->user->last_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                        </td>
                        <th>Email:</th>
                        <td>
                            {{ Form::text('email', ($onboarding->user->email??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                        </td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                            {{ Form::text('phone', ($onboarding->user->phone??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                        </td>
                        <th>Vendor:</th>
                        <td>
                            {{ Form::text('vendor', ($onboarding->user->vendor->vendor_name??null), ['class' => 'form-control form-control-sm', 'disabled']) }}
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr class="bg-dark">
                        <th class="text-center" colspan="6">Personal Details</th>
                    </tr>
                    <tr>
                        <th>Title:</th>
                        <td>{{ Form::select('title_id', $title_dropdown, $onboarding->title_id, ['class' => 'form-control form-control-sm ', 'placeholder' => "Select Title"]) }}</td>
                        <th>Known As Name:</th>
                        <td>{{ Form::text('Known_as', $onboarding->Known_as, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>ID Number:</th>
                        <td>{{ Form::text('id_number', $onboarding->id_number, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Date Of Birth:</th>
                        <td>{{ Form::text('date_of_birth', $onboarding->date_of_birth, ['class' => 'form-control form-control-sm datepicker']) }}</td>
                        <th>Passport Number:</th>
                        <td>{{ Form::text('passport_number', $onboarding->passport_number, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Passport Country:</th>
                        <td>{{ Form::select('country_id', $country_dropdown, $onboarding->country_id, ['class' => 'form-control form-control-sm ', 'placeholder' => 'Select Country']) }}</td>
                    </tr>
                    <tr class="bg-dark">
                        <th class="text-center" colspan="6">Contact Details</th>
                    </tr>
                    <tr>
                        <th>Physical Address:</th>
                        <td colspan="5">{{ Form::text('physical_address', $onboarding->contactDetails->physical_address??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Postal Address:</th>
                        <td colspan="5">{{ Form::text('postal_address', $onboarding->contactDetails->postal_address??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Email Address:</th>
                        <td colspan="2">{{ Form::text('email', $onboarding->contactDetails->email??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Home Number:</th>
                        <td colspan="2">{{ Form::text('home_number', $onboarding->contactDetails->home_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Work Number:</th>
                        <td colspan="2">{{ Form::text('work_number', $onboarding->contactDetails->work_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Cell Number:</th>
                        <td colspan="2">{{ Form::text('cell_number', $onboarding->contactDetails->cell_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Next Of Kin Name:</th>
                        <td colspan="2">{{ Form::text('next_of_kin_name', $onboarding->contactDetails->next_of_kin_name??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Next Kin Number:</th>
                        <td colspan="2">{{ Form::text('next_of_kin_number', $onboarding->contactDetails->next_of_kin_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr class="bg-dark">
                        <th class="text-center" colspan="6">Payment Details</th>
                    </tr>
                    <tr>
                        <th>Type Of Account:</th>
                        <td>{{ Form::select('bank_account_type_id', $account_type_dropdown, $onboarding->paymentDetails->bank_account_type_id??null, ['class' => 'form-control form-control-sm ', 'placeholder' => 'Select Bank Account Type']) }}</td>
                        <th>Account Name:</th>
                        <td>{{ Form::text('account_name', $onboarding->paymentDetails->account_name??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Account Holder Relation:</th>
                        <td>{{ Form::select('relationship_id', $relationship_dropdown, $onboarding->paymentDetails->relationship_id??null, ['class' => 'form-control form-control-sm ', 'placeholder' => 'Select A Relationship']) }}</td>
                    </tr>
                    <tr>
                        <th>Bank Name:</th>
                        <td>{{ Form::text('bank_name', $onboarding->paymentDetails->bank_name??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Branch Number:</th>
                        <td>{{ Form::text('branch_number', $onboarding->paymentDetails->branch_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Branch Name:</th>
                        <td>{{ Form::text('branch_name', $onboarding->paymentDetails->branch_name??null, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Account Number:</th>
                        <td>{{ Form::text('account_number', $onboarding->paymentDetails->account_number??null, ['class' => 'form-control form-control-sm']) }}</td>
                        <th></th>
                        <td></td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr class="bg-dark">
                        <th class="text-center" colspan="6">SARS Details</th>
                    </tr>
                    <tr>
                        <th>Directive Number:</th>
                        <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[0]??null), ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Directive Number:</th>
                        <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[1]??null), ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Directive Number:</th>
                        <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[2]??null), ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr>
                        <th>Directive Number:</th>
                        <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[3]??null), ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Directive Number:</th>
                        <td>{{ Form::text('directive_number[]', (explode(',',$onboarding->directive_number)[4]??null), ['class' => 'form-control form-control-sm']) }}</td>
                        <th>Income Tax Number:</th>
                        <td>{{ Form::text('income_tax_number', $onboarding->income_tax_number, ['class' => 'form-control form-control-sm']) }}</td>
                    </tr>
                    <tr class="bg-dark">
                        <th class="text-center" colspan="6">Upload Documents</th>
                    </tr>
                    <tr>
                        <th>ID/Passport:</th>
                        <td>
                            <div class="row">
                                <div class="col-md-12 pb-2">
                                    @if($onboarding->id_passport)
                                        <a href="{{route('id_passport', ['q' => $onboarding->id_passport])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    {{ Form::file('id_passport', old('id_passport'), ['class' => 'form-control form-control-sm']) }}
                                </div>
                            </div>
                        </td>
                        <th>Proof of Address:</th>
                        <td>
                            <div class="row">
                                <div class="col-md-12 pb-2">
                                    @if($onboarding->proof_of_address)
                                        <a href="{{route('proof_of_address', ['q' => $onboarding->proof_of_address])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    {{ Form::file('proof_of_address', old('proof_of_address'), ['class' => 'form-control form-control-sm']) }}
                                </div>
                            </div>
                        </td>
                        <th>Proof of Bank Account:</th>
                        <td>
                            <div class="row">
                                <div class="col-md-12 pb-2">
                                    @if(isset($onboarding->paymentDetails->proof_of_bank_account))
                                        <a href="{{route('proof_of_bank_account', ['q' => $onboarding->paymentDetails->proof_of_bank_account])}}" target="_blank" class="btn btn-sm btn-outline-success" style="color: #28a745!important;">View Document</a>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    {{ Form::file('proof_of_bank_account', old('proof_of_bank_account'), ['class' => 'form-control form-control-sm']) }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <th colspan="2">POIA Declaration:</th>
                        <td colspan="4">
                            <h4>POPI ACT<br>
                                AGREEMENT AND CONSENT DECLARATION
                            </h4>
                            <p>
                                CONSENT TO PROCESS PERSONAL INFORMATION IN TERMS OF THE PROTECTION OF<br>
                                P E R S O N A L INFORMATION ACT, N O . 4 OF 2013 (POPIA)<br>
                                The purpose of the POPIA is to protect personal information of individuals and businesses and to<br>
                                give effect to their right of privacy as provided for in the Constitution. By signing this form, you<br>
                                consent to your personal information to be processed {{ ('by '.App\Models\Company::first(['company_name'])->company_name)??null }}<br>
                                and consent is effective immediately and will remain effective until such consent is<br>
                                withdrawn.

                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th>Tick to accept:</th>
                        <td colspan="5">{{ Form::checkbox('popia_accept',1, $onboarding->popia_accept, ['class' => 'form-check-input ml-0', 'id' => 'popi']) }}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="4" class="text-center">
                        <button type="submit" class="btn btn-sm{{isset($onboarding->confirmed_at)?' disabled': null}}" id="save">Save</button>&nbsp;
                        <button class="mr-1 btn btn-sm btn-success{{isset($onboarding->confirmed_at)?' disabled': null}}" id="confirm">Commit</button>
                        <a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a>
                    </td>
                </tr>
                </tbody>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('extra-js')

    <script>
        $( document ).ready(function() {
    console.log( "ready!" );
        $("#confirm").on('click', e => {
            e.preventDefault()
            
            if(!$("#popi").is(":checked")){
                // alert("You cannot confirm unless you accept the popi act")
                let YOUR_MESSAGE_STRING_CONST = "You cannot confirm unless you accept the popi act";

                confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                    // return true;
                    // e.currentTarget.submit();
                    // $(this).parents('form:first').submit();
                },true);
            }else {
                let YOUR_MESSAGE_STRING_CONST = "You cannot edit your information after you commit";

                confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {
                    // return true;
                    // e.currentTarget.submit();
                    let hiddenInput = $('<input>').attr({
                        type: 'hidden',
                        name: 'confirm',
                        value: '1'
                    });

                    $('#onboarding-form').append(hiddenInput).submit();
                    // $(this).parents('form:first').submit();
                },true);
            }

        })
    })
    </script>
@endsection
