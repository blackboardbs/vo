@extends('adminlte.default')
@section('title') Appointing Manager @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="btn-toolbar float-right">
            <button onclick="history.back()" class="btn btn-dark float-right"><i class="fa fa-caret-left"></i> Back</button>
            <a href="javascript:void(0)" onclick="saveForm('saveForm')" class="btn btn-primary ml-1">Save</a>
        </div>
    </div>
@endsection
@section('content')
<div class="container-fluid">
        <hr />
        <div class="table-responsive">
    {{Form::open(['url' => route('useronboarding.appontmentmanager.store', $useronboarding), 'method' => 'post','class'=>'mt-3', 'files' => true,'autocomplete'=>'off','id'=>'saveForm'])}}
        <table class="table table-bordered table-sm mt-3">
            <thead>
                <tr class="bg-dark">
                    <th colspan="4" class="text-center">Appointing Manager</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Contract Type:</th>
                    <td>{{Form::select('contract_type_id', $contract_type_dropdown, null, ['class' => 'form-control form-control-sm ', 'id' => 'contract_type', 'placeholder' => 'Select Contract Type'])}}</td>
                    <th>Payment Base:</th>
                    <td>{{Form::select('payment_base_id', $payment_base_dropdown, null, ['class' => 'form-control form-control-sm ', 'placeholder' => 'Select Payment Base'])}}</td>
                </tr>
                <tr>
                    <th>Payment Type</th>
                    <td>{{Form::select('payment_type_id', $payment_type_dropdown, null, ['class' => 'form-control form-control-sm chosen-select', 'placeholder' => 'Select Payment Type'])}}</td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Start Date:</th>
                    <td>{{Form::text('start_date', old('start_date'), ['class' => 'form-control form-control-sm datepicker', 'placeholder' => 'YYYY-MM-DD'])}}</td>
                    <th>End Date: <small class="text-muted">(If not permanent)</small></th>
                    <td>{{Form::text('end_date', old('end_date'), ['class' => 'form-control form-control-sm datepicker', 'placeholder' => 'YYYY-MM-DD'])}}</td>
                </tr>
                <tr>
                    <th>Position:</th>
                    <td>{{Form::text('position', old('position'), ['class' => 'form-control form-control-sm'])}}</td>
                    <th>Annual Salary:</th>
                    <td>{{Form::text('annual_salary', old('annual_salary'), ['class' => 'form-control form-control-sm'])}}</td>
                </tr>
                <tr>
                    <th>Leave Days:</th>
                    <td>{{Form::text('leave_days', $leave_days, ['class' => 'form-control form-control-sm'])}}</td>
                    <th>Notice Period: <small class="text-muted">(If vendor type)</small></th>
                    <td>{{Form::text('notice_period', old('notice_period'), ['class' => 'form-control form-control-sm'])}}</td>
                </tr>
                <tr>
                    <th>Reporting Manager:</th>
                    <td>{{Form::select('reporting_manager', $reporting_manager_dropdown, null, ['class' => 'form-control form-control-sm ', 'placeholder' => 'Select Reporting Manager'])}}</td>
                    <th>Other Income:</th>
                    <td>{{Form::text('other_income', old('other_income'), ['class' => 'form-control form-control-sm'])}}</td>
                </tr>
                <tr>
                    <th>Other Conditions:</th>
                    <td>{{Form::textarea('other_conditions', old('other_conditions'), ['class' => 'form-control form-control-sm', 'rows' => 4])}}</td>
                </tr>
                <tr>
                    <th>Does the employee require a laptop?</th>
                    <td>{{Form::select('require_laptop', $yes_or_no_dropdown, 1, ['class' => 'form-control form-control-sm'])}}</td>
                    <th>Does the user require a Mobile phone?</th>
                    <td>{{Form::select('require_phone', $yes_or_no_dropdown, 0, ['class' => 'form-control form-control-sm'])}}</td>
                </tr>
                <tr>
                    <th>Does the user require Mobile Internet?</th>
                    <td>{{Form::select('require_mobile_internet', $yes_or_no_dropdown, 0, ['class' => 'form-control form-control-sm'])}}</td>
                </tr>
                <tr>
                    <th>Notes on onboarding:</th>
                    <td>{{Form::textarea('notes', old('notes'), ['class' => 'form-control form-control-sm', 'rows' => 4])}}</td>
                    <th>Responsibilities</th>
                    <td>{{Form::textarea('responsibilities', old('responsibilities'), ['class' => 'form-control form-control-sm', 'rows' => 4])}}</td>
                </tr>
                <tr class="bg-dark">
                    <th colspan="4" class="text-center">Templates</th>
                </tr>
                <tr>
                    <th>Permanent Contract Template</th>
                    <td>{{Form::select('permenant_contract_template_id', $permanent_term_dropdown, null, ['class' => 'form-control form-control-sm', 'id' => 'permanent-contract', 'placeholder' => "Select Template"])}}</td>
                    <th>Fixed Term Contract Template</th>
                    <td>{{Form::select('fixed_contract_template_id', $fixed_term_dropdown, null, ['class' => 'form-control form-control-sm', 'id' => 'fixed-contract', 'placeholder' => "Select Template"])}}</td>
                </tr>
            </tbody>
        </table>
    {{Form::close()}}
</div>
</div>
@endsection

@section('extra-js')
    <script>
        $(document).on('ready',function (){
            $(function (){
                $("#contract_type").on('change', function (){
                    if ($("#contract_type").val() == 1)
                    {
                        $("#fixed-contract").attr('disabled', true)
                    }else {
                        $("#fixed-contract").attr('disabled', false)
                    }

                    if ($("#contract_type").val() == 2)
                    {
                        $("#permanent-contract").attr('disabled', true)
                    }else {
                        $("#permanent-contract").attr('disabled', false)
                    }
                })
            })
        })
    </script>
@endsection
