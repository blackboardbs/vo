<table>
    <thead>
    <tr>
        <th>User Name</th>
        <th>User Surname</th>
        <th>Appointment Manager</th>
        <th>Approver</th>
        <th>Onboarding Status</th>
        <th>User Type</th>
        <th>Contract</th>
        <th>Payment Type</th>
        <th>Payment Base</th>
        <th>User Created</th>
        <th>Information Updated</th>
        <th>Contract Details </th>
        <th>Approved</th>
        <th>Contract Issued</th>
        <th>Contract Received</th>
        <th>Contract Signed</th>
        <th>Contract filed</th>
        <th>Notes</th>
    </tr>
    </thead>
    <tbody>
    @foreach($onboardings as $onboarding)
        <tr>
            <td>{{$onboarding->user?->first_name}}</td>
            <td>{{$onboarding->user?->last_name}}</td>
            <td>{{$onboarding->user?->appointmentManager?->first_name}}</td>
            <td>{{$approver}}</td>
            <td>{{$onboarding->status?->description}}</td>
            <td>{{$onboarding->user?->userType?->description}}</td>
            <td>{{$onboarding->appointingManager?->contractType?->description}}</td>
            <td>{{$onboarding->appointingManager?->paymentType?->description}}</td>
            <td>{{$onboarding->appointingManager?->paymentBase?->description}}</td>
            <td>{{Carbon\Carbon::parse($onboarding->created_at)->toDateString()}}</td>
            <td>{{Carbon\Carbon::parse($onboarding->updated_at)->toDateString()}}</td>
            <td>{{isset($onboarding->appointingManager->created_at)?Carbon\Carbon::parse($onboarding->appointingManager->created_at)->toDateString():null}}</td>
            <td>{{isset($onboarding->appointingManager->approved_at)?Carbon\Carbon::parse($onboarding->appointingManager->approved_at)->toDateString():null}}</td>
            <td>{{isset($onboarding->appointingManager->issued_at)?Carbon\Carbon::parse($onboarding->appointingManager->issued_at)->toDateString():null}}</td>
            <td>{{isset($onboarding->appointingManager->recieved_at)?Carbon\Carbon::parse($onboarding->appointingManager->recieved_at)->toDateString():null}}</td>
            <td>{{isset($onboarding->appointingManager->signed_at)?Carbon\Carbon::parse($onboarding->appointingManager->signed_at)->toDateString():null}}</td>
            <td>{{isset($onboarding->appointingManager->filed_at)?Carbon\Carbon::parse($onboarding->appointingManager->filed_at)->toDateString():null}}</td>
            <td>{{$onboarding->appointingManager?->notes}}</td>
        </tr>
    @endforeach
    </tbody>
</table>