@extends('adminlte.default')
@section('title') Add Document @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <button onclick="history.back()" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</button>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <hr>
        <div class="table-responsive">
            {{Form::open(['url' => route('document.update', $document), 'method' => 'put', 'class'=>'mt-3', 'files' => true, 'autocomplete' => 'off'])}}
            <input type="hidden" id="reference_id" name="reference_id" value="{{$reference_id}}"/>
            <input type="hidden" id="document_type_id" name="document_type_id" value="{{$document_type_id}}"/>
            <table class="table table-borderless table-sm" style="width: 70%;">
                <tr>
                    <td>Name</td>
                    <td>
                        {{Form::text('name', $document->name, ['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                        @foreach($errors->get('name') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Document</td>
                    <td>
                        <input type="file" id="file" name="file" class="{{$errors->has('file') ? ' is-invalid' : ''}}" />
                        @foreach($errors->get('file') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
            <table class="table table-borderless">
                <tr>
                    <td colspan="4" class="text-center"><button type="submit" class="btn btn-sm">Save</button>&nbsp;<a href="{{url()->previous()}}" class="btn btn-default btn-sm">Cancel</a></td>
                </tr>
            </table>
            {{Form::close()}}
        </div>
    </div>
@endsection
