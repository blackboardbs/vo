
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from "@vue/compat";
import * as bootstrap from 'bootstrap';
import 'popper.js';
import {createApp} from 'vue/dist/vue.esm-bundler.js';
import VueFullscreen from 'vue-fullscreen';
import _ from 'lodash';
import axios from 'axios';

import VueSimpleAlert from "vue3-simple-alert-next";
import { QuillEditor, Quill } from '@vueup/vue-quill'
import '@vueup/vue-quill/dist/vue-quill.snow.css';


Vue.use(QuillEditor);

window._ = _;
window.bootstrap = bootstrap;
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*import Echo from 'laravel-echo'
import Pusher from 'pusher-js';
window.Pusher = Pusher;
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'ed4bcd1555412589ae52',
    cluster: 'ap2',
    forceTLS: true
});*/


import Header from "./layout/header.vue";
import Notification from "./components/Notification.vue";
import InvoiceItemComponent from "./components/InvoiceItemComponent.vue";
import InvoiceHeaderComponent from "./components/InvoiceHeaderComponent.vue";
import ActionComponent from "./components/ActionComponent.vue";
//import TaskComponent from "./components/TaskComponent.vue";
import MyWorkWeek from "./components/my-work-week/MyWorkWeekComponent.vue";
import Kanban from "./components/kanban/KanbanComponent.vue";
import Tasks from "./components/kanban/TasksComponent.vue";
// import TaskCards from "./components/TaskCards.vue";
import CreateTimesheet from "./components/CreateTimesheet.vue";
import AssignmentComponent from "./components/project/AssignmentComponent.vue";
import TimesheetListComponent from "./components/project/TimesheetListComponent.vue";
// import TaskComponent from "./components/project/TaskComponent.vue";
import BillingPeriodComponent from "./components/BillingPeriodComponent.vue";
import CombinedTimesheetsComponent from "./components/CombinedTimesheetsComponent.vue";
import InvoiceExpenses from "./components/InvoiceExpenses.vue";
import Timesheet from "./components/timesheets/TimesheetComponent.vue";
import Choices from "choices.js";
import GoalComponent from "./components/project/sprints/GoalComponent.vue";
import SprintReviewComponent from "./components/project/sprints/SprintReviewComponent.vue";
import SprintResourcesComponent from "./components/project/sprints/SprintResourcesComponent.vue";
import ProspectComments from "./components/ProspectComments.vue";
import SystemHealth from "./components/system-health/SystemHealthComponent.vue";
import SalesPipeline from "./components/sales/Index.vue";
import SetupComponent from "./components/setup/SetupComponent.vue";
import SprintCompleteModal from "@/components/project/sprints/SprintCompleteModal.vue";
import ProFormaInvoice from "@/components/ProFormaInvoice.vue";
import SiteReportHeader from "./components/site-report/SiteReportHeader.vue";
import TaskList from "./components/project/TaskListComponent.vue";
import Treeview from "./components/project/TreeviewComponent.vue";
import CreateTemplate from "@/components/Template/CreateTemplate.vue";
import EquipmentHistory from "@/components/Equipment/EquipmentHistory.vue";
import AdditionalCosts from "@/components/Equipment/AdditionalCosts.vue";

const app = createApp({});
app.component('setup-component', SetupComponent);
app.component('top-nav', Header);
app.component('blackboard-notifications', Notification);
app.component('invoice-items', InvoiceItemComponent);
app.component('invoice-header-component', InvoiceHeaderComponent);
// app.component('action-component', ActionComponent);
// app.component('task-component', TaskComponent);
app.component('my-work-week', MyWorkWeek);
app.component('kanbanBoard', Kanban);
app.component('task-filters', Tasks);
// app.component('task-cards', TaskCards);
app.component('create-timesheet', CreateTimesheet);
app.component('blackboard-billing-period', BillingPeriodComponent);
app.component('blackboard-assignment', AssignmentComponent);
app.component('blackboard-timesheet-list', TimesheetListComponent);
// app.component('blackboard-task', TaskComponent);
app.component('blackboard-combined-timesheets', CombinedTimesheetsComponent);
app.component('blackboard-invoice-expenses', InvoiceExpenses);
app.component('blackboard-timesheet', Timesheet);
app.component('blackboard-goals', GoalComponent);
app.component('blackboard-sprint-reviews', SprintReviewComponent);
app.component('blackboard-sprint-resource', SprintResourcesComponent);
app.component('blackboard-prospect-comments', ProspectComments);
app.component('system-health', SystemHealth);
app.component('sales-pipeline', SalesPipeline);
app.component('blackboard-sprint-complete', SprintCompleteModal);
app.component('create-template', CreateTemplate);
app.component('pro-forma-invoice', ProFormaInvoice);
app.component('site-report-header', SiteReportHeader);
app.component('equipment-history', EquipmentHistory);
app.component('equipment-additional-cost', AdditionalCosts)
app.component('task-list', TaskList);
app.component('treeview', Treeview);

app.use(VueFullscreen);

app.use(VueSimpleAlert,{text:"Are you sure?",
    title:"Warning",
    showCancelButton:false,
    confirmButtonColor:'#dc3545',
    backdrop:true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No'});
app.mount("#app");

$( ".datepicker" ).datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    showWeek:true,
    yearRange: "1920:2030"
});
