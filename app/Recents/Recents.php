<?php

namespace App\Recents;

use App\Models\Module;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class Recents
{
    public function recent_activities($subject_type, $link, $optional = 0)
    {
        $recentActivities = Activity::with(['causer', 'subject'])->orderBy('id', 'desc')
            ->where('subject_type', $subject_type);

        $module = Module::where('name', '=', 'App\Models\Recent')->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $recentActivities->whereIn('causer_id', Auth::user()->team());
            }
        } else {
            abort(403);
        }

        $recentActivities = $recentActivities->take(10)->get();
        $activities = [];
        foreach ($recentActivities as $activity) {
            array_push($activities, [
                'properties' => $activity->properties,
                'consultant' => ($activity->causer) ? $activity->causer->first_name.' '.$activity->causer->last_name : '',
                'subject' => $activity->subject,
                'description' => $activity->description,
                'link' => ($activity->description == 'deleted') ? null : route($link, ['id' => (($activity->subject_type == \App\Models\TimeExp::class) ? $activity->subject->timesheet_id : $activity->subject_id), 'res_id' => (($optional) ? $activity->subject->user_id : null)]),
                'time' => $activity->created_at->diffForHumans(),
                'created_at' => substr($activity->created_at, 0, 10),
            ]);
        }

        return $activities;
    }
}
