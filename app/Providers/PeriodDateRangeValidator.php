<?php
namespace App\Providers;

use Illuminate\Validation\Validator;

class PeriodDateRangeValidator extends Validator{
    public function validateEmptyWith($attribute, $value, $parameters){
        return (($value != 0) || ($this->getValue($parameters[0]) != '' && $this->getValue($parameters[1]) != '')) ? false : true;
    }
}