<?php

namespace App\Jobs;

use App\Config;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendExpenseTrackingEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $expenseTracking;
    protected $siteName;
    protected $helpPortal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $expenseTracking, $siteName, $helpPortal)
    {
        $this->email = $email;
        $this->expenseTracking = $expenseTracking;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new \App\Mail\ExpenseTracking($this->expenseTracking, $this->siteName, $this->helpPortal));
    }
}
