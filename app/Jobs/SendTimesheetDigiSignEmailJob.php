<?php

namespace App\Jobs;

use App\Mail\TimesheetDigiSignMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendTimesheetDigiSignEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $documentId;
    protected $userNumber;
    protected $userName;
    protected $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $documentId, $userNumber, $userName, $subject)
    {
        $this->email = $email;
        $this->documentId = $documentId;
        $this->userNumber = $userNumber;
        $this->userName = $userName;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new TimesheetDigiSignMail($this->documentId, $this->userNumber, $this->userName, $this->subject));
    }
}
