<?php

namespace App\Jobs;

use App\Mail\UpgradePremiumMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendUpgradePremiumEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(protected readonly array $premium)
    {}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        
        Mail::to('support@consulteaze.com')->send(new UpgradePremiumMail($this->premium));
    }
}
