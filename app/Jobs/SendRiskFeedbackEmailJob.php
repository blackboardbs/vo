<?php

namespace App\Jobs;

use App\Mail\RiskFeedbackMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendRiskFeedbackEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emails;
    protected $risk;
    protected $feedback;
    protected $feedbackOwner;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emails, $risk, $feedback, $feedbackOwner)
    {
        $this->emails = $emails;
        $this->risk = $risk;
        $this->feedback = $feedback;
        $this->feedbackOwner = $feedbackOwner;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->emails)
            ->send(new RiskFeedbackMail($this->risk, $this->feedback, $this->feedbackOwner));
    }
}
