<?php

namespace App\Jobs;

use App\Mail\MasterServiceAgreementMail;
use App\Models\Config;
use App\Models\Vendor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class MasterServiceAgreementJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private readonly string $file, private readonly string $email){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $company = Config::first(['company_id'])?->company;
        Mail::to(array_filter([$company->email, $this->email]))->send(new MasterServiceAgreementMail($this->file));
    }
}
