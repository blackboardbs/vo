<?php

namespace App\Jobs;

use App\Mail\AssetAknowledgementDigiSignMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssertAknowledgementDigiSignEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $assertId;
    protected $digiSignUserId;
    protected $resource;
    protected $subject;
    protected $digiSignUsersSigned;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $assertId, $digiSignUserId, $resource, $subject, $digiSignUsersSigned)
    {
        $this->email = $email;
        $this->assertId = $assertId;
        $this->digiSignUserId = $digiSignUserId;
        $this->resource = $resource;
        $this->subject = $subject;
        $this->digiSignUsersSigned = $digiSignUsersSigned;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new AssetAknowledgementDigiSignMail($this->assertId, $this->digiSignUserId, $this->resource, $this->subject, $this->digiSignUsersSigned));
    }
}
