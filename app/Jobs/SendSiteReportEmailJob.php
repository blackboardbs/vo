<?php

namespace App\Jobs;

use App\Models\Company;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Http\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendSiteReportEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        protected  array $emails,
        protected Customer $customer,
        protected Company $company,
        protected string $subject,
        protected string $body,
        protected string $file
    ){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->emails)
            ->send(new \App\Mail\SiteReport($this->customer, $this->company, $this->subject, $this->body, $this->file));
    }
}
