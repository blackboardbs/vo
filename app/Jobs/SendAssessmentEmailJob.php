<?php

namespace App\Jobs;

use App\Mail\AssessmentMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssessmentEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $customerName;
    protected $resource;
    protected $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $customerName, $resource, $file)
    {
        $this->email = $email;
        $this->customerName = $customerName;
        $this->resource = $resource;
        $this->file = $file;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new AssessmentMail($this->customerName, $this->resource, $this->file));
    }
}
