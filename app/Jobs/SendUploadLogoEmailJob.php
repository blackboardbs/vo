<?php

namespace App\Jobs;

use App\Mail\UploadLogoMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendUploadLogoEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $emails;
    protected $body;
    protected $url;
    protected $buttonText;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emails, $body, $url, $buttonText)
    {
        $this->emails = $emails;
        $this->body = $body;
        $this->url = $url;
        $this->buttonText = $buttonText;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->emails)->send(new UploadLogoMail($this->body, $this->url, $this->buttonText));
    }
}
