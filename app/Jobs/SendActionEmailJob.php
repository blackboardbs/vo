<?php

namespace App\Jobs;

use App\Mail\ActionMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendActionEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $subject;
    protected $full_name;
    protected $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $subject, $full_name, $action)
    {
        $this->email = $email;
        $this->subject = $subject;
        $this->full_name = $full_name;
        $this->action = $action;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to(trim($this->email))->send(new ActionMail($this->subject, $this->full_name, $this->action));
    }
}
