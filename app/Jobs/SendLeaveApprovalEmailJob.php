<?php

namespace App\Jobs;

use App\Mail\LeaveApprovedMail;
use App\Mail\LeaveDeniedMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendLeaveApprovalEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $leave;
    protected $leaveType;
    protected $projects;
    protected $resource;
    protected $user;
    protected $leaveStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $leave, $leaveType, $projects, $resource, $user, $leaveStatus)
    {
        $this->email = $email;
        $this->leave = $leave;
        $this->leaveType = $leaveType;
        $this->projects = $projects;
        $this->resource = $resource;
        $this->user = $user;
        $this->leaveStatus = $leaveStatus;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if($this->leaveStatus == '1') {
            Mail::to($this->email)->send(new LeaveApprovedMail($this->leave, $this->leaveType, $this->projects, $this->resource, $this->user));

        }
        if($this->leaveStatus == '2') {
            Mail::to($this->email)->send(new LeaveDeniedMail($this->leave, $this->leaveType, $this->projects, $this->resource, $this->user));
        }
    }
}
