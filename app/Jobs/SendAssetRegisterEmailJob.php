<?php

namespace App\Jobs;

use App\Mail\AssetCreateMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssetRegisterEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $assert;
    protected $resource;
    protected $company;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $assert, $resource, $company)
    {
        $this->email = $email;
        $this->assert = $assert;
        $this->resource = $resource;
        $this->company = $company;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new AssetCreateMail($this->assert, $this->resource,$this->company));
    }
}
