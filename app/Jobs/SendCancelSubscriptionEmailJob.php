<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Mail\CancelSubscriptionMail;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendCancelSubscriptionEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $user;
    protected $details;

    /**
     * Create a new job instance.
     */
    public function __construct($email, $user, $details)
    {
        $this->email = $email;
        $this->user = $user;
        $this->details = $details;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $user = User::first();
        Mail::to(trim($this->email))->send(new CancelSubscriptionMail($user,$details));
    }
}
