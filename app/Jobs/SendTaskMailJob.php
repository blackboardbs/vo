<?php

namespace App\Jobs;

use App\Mail\TaskCreated;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendTaskMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $emails;
    private $task;
    private $helpportal;

    public function __construct($emails, $task, $helpportal)
    {
        $this->emails = $emails;
        $this->task = $task;
        $this->helpportal = $helpportal;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->emails)->send(new TaskCreated($this->task, $this->helpportal));
    }
}
