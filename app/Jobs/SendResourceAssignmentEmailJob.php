<?php

namespace App\Jobs;

use App\Mail\ResourceAssignmentMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendResourceAssignmentEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $resource;
    protected $document;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $resource, $document)
    {
        $this->email = $email;
        $this->resource = $resource;
        $this->document = $document;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new ResourceAssignmentMail($this->resource, $this->document));
    }
}
