<?php

namespace App\Jobs;

use App\API\Clockify\ClockifyAPI;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ClockifyTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $task;
    private $projectId;

    public function __construct(string $task, string $projectId)
    {
        $this->task = $task;
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     */
    public function handle(ClockifyAPI $clockify): void
    {
        $clockify->post("/projects/".$this->projectId."/tasks", ["name" => $this->task]);
    }
}
