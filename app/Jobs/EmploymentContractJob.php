<?php

namespace App\Jobs;

use App\Mail\EmploymentContractMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EmploymentContractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private readonly array $data){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->data['emails'])->send(new EmploymentContractMail($this->data));
    }
}
