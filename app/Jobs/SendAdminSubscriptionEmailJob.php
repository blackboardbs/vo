<?php

namespace App\Jobs;

use App\Model\User;
use App\Mail\AdminSubscriptionMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendAdminSubscriptionEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $details;

    /**
     * Create a new job instance.
     */
    public function __construct($email,$details)
    {
        $this->email = $email;
        $this->details = $details;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $user = User::first();
        Mail::to(trim($this->email))->send(new AdminSubscriptionMail($user,$details));
    }
}
