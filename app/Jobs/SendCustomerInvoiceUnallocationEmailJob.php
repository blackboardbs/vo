<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendCustomerInvoiceUnallocationEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $customerInvoice;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $customerInvoice)
    {
        $this->email = $email;
        $this->customerInvoice = $customerInvoice;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new \App\Mail\InvoiceUnallocation($this->customerInvoice));
    }
}
