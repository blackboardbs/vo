<?php

namespace App\Jobs;

use App\Mail\ResourceTaskMail;
use App\Mail\ResourceTaskUpdateMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendResourceTaskEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $task;
    protected $user;
    protected $resource;
    protected $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $task, $user, $resource, $action = 1)
    {
        $this->email = $email;
        $this->task = $task;
        $this->user = $user;
        $this->resource = $resource;
        $this->action = $action;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->action == 1)
            Mail::to($this->email)->send(new ResourceTaskMail($this->task,$this->user,$this->resource));
        else
            Mail::to($this->email)->send(new ResourceTaskUpdateMail($this->task,$this->user,$this->resource));
    }
}
