<?php

namespace App\Jobs;

use App\Mail\TaskNoteEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $full_name;
    protected $subject;
    protected $note;
    protected $description;
    protected $project_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $full_name, $subject, $note, $description, $project_id)
    {
        $this->email = $email;
        $this->full_name = $full_name;
        $this->subject = $subject;
        $this->note = $note;
        $this->description = $description;
        $this->project_id = $project_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to(trim($this->email))->send(new TaskNoteEmail($this->full_name, $this->subject, $this->note, $this->description, $this->project_id));
    }
}
