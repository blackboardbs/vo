<?php

namespace App\Jobs;

use App\Mail\SupplierAssignmentMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendSupplierAssignmentEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $claimApprover;
    protected $digiSignUserId;
    protected $assignmentId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $claimApprover, $digiSignUserId, $assignmentId)
    {
        $this->email = $email;
        $this->claimApprover = $claimApprover;
        $this->digiSignUserId = $digiSignUserId;
        $this->assignmentId = $assignmentId;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new SupplierAssignmentMail($this->claimApprover, $this->digiSignUserId, $this->assignmentId));
    }
}
