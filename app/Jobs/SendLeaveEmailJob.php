<?php

namespace App\Jobs;

use App\Mail\LeaveRequestMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendLeaveEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $leave;
    protected $leaveType;
    protected $projects;
    protected $resource;
    protected $annualLeave;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $leave, $leaveType, $projects, $resource, $annualLeave)
    {
        $this->email = $email;
        $this->leave = $leave;
        $this->leaveType = $leaveType;
        $this->projects = $projects;
        $this->resource = $resource;
        $this->annualLeave = $annualLeave;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        logger("it worked");
        //Mail::to($this->email)->send(new LeaveRequestMail($this->leave,$this->leaveType, $this->projects,$this->resource,$this->annualLeave));
    }
}
