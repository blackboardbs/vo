<?php

namespace App\Jobs;

use App\Mail\AssignmentCreated;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssignmentEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $assignment;
    protected $helpPortal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $assignment, $helpPortal)
    {
        $this->email = $email;
        $this->assignment = $assignment;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to(array_filter($this->email))->send(new AssignmentCreated($this->assignment, $this->helpPortal));
    }
}
