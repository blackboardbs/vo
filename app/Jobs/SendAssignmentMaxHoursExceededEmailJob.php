<?php

namespace App\Jobs;

use App\Mail\AssignmentMaxHoursExceeded;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssignmentMaxHoursExceededEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $approver;
    protected $assignment;
    protected $timesheet;
    protected $hours;
    protected $helpPortal;
    protected $assignmentDailyHours;
    protected $capacityAllocatedHours;
    protected $hoursToDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($approver, $assignment, $timesheet, $hours, $helpPortal, $assignmentDailyHours, $capacityAllocatedHours, $hoursToDate)
    {
        $this->approver = $approver;
        $this->assignment = $assignment;
        $this->timesheet = $timesheet;
        $this->hours = $hours;
        $this->helpPortal = $helpPortal;
        $this->assignmentDailyHours = $assignmentDailyHours;
        $this->capacityAllocatedHours = $capacityAllocatedHours;
        $this->hoursToDate = $hoursToDate;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->approver)->send(
            new AssignmentMaxHoursExceeded(
                $this->assignment,
                $this->timesheet,
                $this->hours,
                $this->helpPortal,
                $this->assignmentDailyHours,
                $this->capacityAllocatedHours,
                $this->hoursToDate
            )
        );
    }
}
