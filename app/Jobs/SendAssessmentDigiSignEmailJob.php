<?php

namespace App\Jobs;

use App\Mail\AssessmentDigisignEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAssessmentDigiSignEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $subject;
    protected $customerName;
    protected $resource;
    protected $userId;
    protected $assessmentId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $subject, $customerName, $resource, $userId, $assessmentId)
    {
        $this->email = $email;
        $this->subject = $subject;
        $this->customerName = $customerName;
        $this->resource = $resource;
        $this->userId = $userId;
        $this->assessmentId = $assessmentId;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->send(new AssessmentDigisignEmail($this->subject, $this->customerName, $this->resource,
        $this->userId, $this->assessmentId));
    }
}
