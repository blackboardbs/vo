<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendVendorInvoiceEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $ccEmails;
    protected $vendor;
    protected $company;
    protected $subject;
    protected $body;
    protected $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $ccEmails, $vendor, $company, $subject, $body, $file)
    {
        $this->email = $email;
        $this->ccEmails = $ccEmails;
        $this->vendor = $vendor;
        $this->company = $company;
        $this->subject = $subject;
        $this->body = $body;
        $this->file = $file;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->email)->cc($this->ccEmails)->send(new \App\Mail\VendorInvoiceMail($this->vendor, $this->company, $this->subject, $this->body, $this->file));
    }
}
