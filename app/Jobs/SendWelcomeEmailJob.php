<?php

namespace App\Jobs;

use App\Mail\WelcomeMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        protected readonly User $user,
        protected readonly string $password,
        protected readonly ?User $admin
    ){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if(!$this->admin){
            Mail::to($this->user->email)->send(new WelcomeMail($this->user, $this->password));
        } else {
            Mail::to($this->admin->email)->send(new WelcomeMail($this->user, $this->password));
        }
    }
}
