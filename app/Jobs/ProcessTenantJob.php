<?php

namespace App\Jobs;

use App\Enum\Status;
use App\Mail\NewTenantMail;
use App\Mail\TenantDetailsMail;
use App\Mail\TenantInstructionReceivedMail;
use App\Models\Company;
use App\Models\Config;
use App\Models\Tenant;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\WebhookClient\Jobs\ProcessWebhookJob;
use Spatie\WebhookClient\Models\WebhookCall;
use Stancl\Tenancy\Database\Models\Domain;

class ProcessTenantJob extends ProcessWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public WebhookCall $webhookCall)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $payload = $this->webhookCall->payload;

        $mailPayload = (object) [
            'full_name' => ($payload['user']['first_name']??null)." ".($payload['user']['last_name']??null),
            'email' => ($payload['user']['email']??null),
            'phone' => ($payload['user']['phone']??null),
            'company_name' => ($payload['company']['name']??null),
        ];

        Mail::to("support@consulteaze.com")->send(new TenantInstructionReceivedMail($mailPayload));

        $tenant = Tenant::create();

        $subdomain = $this->generateUniqueSubdomain($this->tenantName($payload['company']['name']));

        $tenant->domains()->create([
            'domain' => $subdomain
        ]);

        $tenant->run(function($tenant) use($payload, $subdomain)
        {

            $company = Company::create([
                'company_name' => $payload['company']['name']??'',
                'postal_address_line1' => $payload['company']['address_line_1']??'',
                'postal_address_line2' => $payload['company']['address_line_2']??'',
                'postal_address_line3' => $payload['company']['address_line_3']??'',
                'email' => $payload['user']['email']??'',
                'state_province' => $payload['company']['province']??'',
                'city_suburb' => $payload['company']['suburb']??'',
                'postal_zipcode' => $payload['company']['postal_code']??'',
                'vat_no' => $payload['company']['tax']??'',
                'status_id' => Status::ACTIVE->value,
                'country_id' => $payload['company']['country']??1,
                'bbbee_level_id' => 1,
                'creator_id' => 1,
            ]);

            $this->defaultCompanyLogo($company);

            $service = new UserService();

            $password = Str::password(12);

            $user = User::create([
                'first_name' => $payload['user']['first_name']??'',
                'last_name' => $payload['user']['last_name']??'',
                'phone' => $payload['user']['phone']??'',
                'email' => $payload['user']['email']??'',
                'password' => Hash::make($password),
                'company_id' => $company->id,
                'login_user' => 1,
                'new_setup' => 1,
                'status_id' => Status::ACTIVE->value,
                'resource_id' => $service->resourceId(),
                'expiry_date' => now()->addYears(5)->toDateString(),
                'onboarding' => 0,
                'calendar_view' => 'action,task,leave,assignment,assessment,anniversary,cale'
            ]);

            $user->resource()?->update(['user_id' => $user->id]);

            $this->defaultUserAvatar($user);


            $service->createUserLandingPage($user->id, 'https://'.$tenant->domains()->first()?->domain.'/dashboard');

            $config = $this->configureTenant($payload);

            if (App::isProduction())
            {
                Mail::to($user->email)->send(new TenantDetailsMail('https://'.$tenant->domains()->first()?->domain, $password));
                Mail::to("support@consulteaze.com")
                    ->send(
                        new NewTenantMail($user, "https://".$subdomain, $company, $config)
                    );
            }
        });
    }

    public function failed(\Throwable $exception)
    {
        logger($exception->getMessage());
    }

    private function tenantName(string $name): string
    {
        $local_name = explode(" ", $name);

        if (count($local_name) > 1){
            $tmp_name = $local_name[0];

            Arr::forget($local_name, 0);

            $new_name = collect($local_name)->map(fn($tmp) => $tmp[0]);

            $tmp_name .= preg_replace('/[^a-zA-Z0-9_ -]/s', '', implode("", $new_name->toArray()));
            if (App::isProduction())
            {
                return \str($tmp_name)->lower()->trim().'.consulteaze.com';
            }else{
                //return \str($tmp_name)->lower(); //To be used when using initialize by subdomain middleware
                return \str($tmp_name)->lower()->trim().'.localhost'; // To be used when using initialize by domain
            }

        }

        $sub = \str(preg_replace('/[^a-zA-Z0-9_ -]/s', '',$name))->lower();

        return App::isProduction() ? $sub.'.consulteaze.com' : $sub.'.localhost';

    }

    private function configureTenant(array $webhook_payload): Config
    {
        $config = new Config();
        $config->days_to_approve_leave = 2;
        $config->auditing_records = 3;
        $config->site_title = $webhook_payload['company']['name']??null;
        $config->admin_email = $webhook_payload['user']['email']??null;
        $config->home_page_message = "Welcome to Consulteaze";
        $config->assessment_approver_user_id = 1;
        $config->company_id = Company::first()?->id;
        $config->cost_center_id = 1;
        $config->utilization_method = 2;
        $config->utilization_hours_per_week = 40;
        $config->utilization_cost_hours_per_week = 40;
        $config->project_terms_id = 1;
        $config->claim_approver_id = 1;
        $config->vat_rate_id = 1;
        $config->calendar_action_colour = "1014FF";
        $config->calendar_task_colour = "9AADFF";
        $config->calendar_leave_colour = "8A7EFF";
        $config->calendar_assignment_colour = "FF712F";
        $config->calendar_assessment_colour = "A4FF4B";
        $config->calendar_anniversary_colour = "4DFFBD";
        $config->calendar_cinvoice_colour = "C4FF9C";
        $config->calendar_vinvoice_colour = "FF9CF7";
        $config->calendar_user_colour = "FFE324";
        $config->calendar_medcert_colour = "64C0FF";
        $config->calendar_events_colour = "FF4848";
        $config->cost_min_percent = 10;
        $config->cost_min_colour = "DC3545";
        $config->cost_mid_colour = "FFC107";
        $config->cost_max_percent = 20;
        $config->cost_max_colour = "28A745";
        $config->assessment_frequency = 14;
        $config->assessment_master = "3,4";
        $config->timesheet_weeks = 12;
        $config->timesheet_weeks_future = 0;
        $config->annual_leave_days = 17;
        $config->background_color = "1460C9";
        $config->font_color = "D8D8D8";
        $config->active_link = "020A59";
        $config->first_warning_days = 30;
        $config->last_warning_days = 1;
        $config->first_warning_percentage = 70;
        $config->second_warning_percentage = 80;
        $config->last_warning_percentage = 90;
        $config->ojs_minimum_qty = 10;
        $config->ojs_minimum_color = "FF0000";
        $config->ojs_between_color = "FFFF00";
        $config->ojs_target_qty = 20;
        $config->ojs_target_color = "00FF00";
        $config->cvsm_minimum_color = "FF0000";
        $config->cvsm_between_color = "FFFF00";
        $config->cvsm_target_color = "00FF00";
        $config->cvsw_minimum_qty = 10;
        $config->cvsw_minimum_color = "FF0000";
        $config->cvsw_between_color = "FFFF00";
        $config->cvsw_target_qty = 20;
        $config->cvsw_target_color = "00FF00";
        $config->nsam_minimum_color = "FF0000";
        $config->nsam_between_color = "FFFF00";
        $config->nsam_target_qty = 20;
        $config->nsam_target_color = "00FF00";
        $config->nsaw_minimum_color = "FF0000";
        $config->nsaw_between_color = "FFFF00";
        $config->nsaw_target_qty = 10;
        $config->nsaw_target_color = "00FF00";
        $config->timesheet_template_id = 1;
        $config->default_invoice_template = "standardinvoice";
        $config->billing_cycle_id = 1;
        $config->default_custom_template = 1;
        $config->customer_invoice_prefix = "SI";
        $config->customer_invoice_format = "SI000000";
        $config->customer_invoice_start_number = "10000";
        $config->customer_pro_forma_invoice_prefix = "PI";
        $config->customer_pro_forma_invoice_format = "PI000000";
        $config->customer_pro_forma_invoice_start_number = "50000";
        $config->vendor_invoice_prefix = "VI";
        $config->vendor_invoice_format = "VI000000";
        $config->vendor_invoice_start_number = "30000";
        $config->hr_user_id = 1;
        $config->hr_approval_manager_id = 1;
        $config->supply_chain_email = $webhook_payload['user']['email'];
        $config->supply_chain_approval_manager_id = 1;
        $config->customer_email = $webhook_payload['user']['email'];
        $config->customer_approval_manager_id = 1;
        $config->kanban_nr_of_tasks = 7;
        $config->kanban_show_impediment = 1;
        $config->kanban_show_hours = 1;
        $config->kanban_show_actuals = 1;
        $config->kanban_show_timeframe = 1;
        $config->kanban_show_completed = 1;
        $config->sh_good_min = 0;
        $config->sh_good_max = 0;
        $config->sh_fair_min = 1;
        $config->sh_fair_max = 3;
        $config->sh_bad_min = 4;
        $config->sh_bad_max = 999;
        $config->save();
        

        return $config;
    }

    private function defaultUserAvatar(User $user): void
    {
        $fileAddress = public_path('assets/users/default.png');
        $file = new UploadedFile($fileAddress, 'file.jpg');
        $request = new Request();
        $request->files->set('avatar', $file);
        $user->thumbnail(request: $request, field: 'avatar', type: 'user', height: 200, width: 200);
        $user->update(['avatar' => $request->file('avatar')->hashName()]);
    }

    private function defaultCompanyLogo(Company $company): void
    {
        $fileAddress = public_path('assets/default.png');
        $file = new UploadedFile($fileAddress, 'file.jpg');
        $request = new Request();
        $request->files->set('avatar', $file);
        $company->logo(
            request: $request,
            field: 'avatar',
            type: 'company',
            height: 200
        );

        $company->update(['company_logo' => $request->file('avatar')->hashName()]);
    }

    private function generateUniqueSubdomain(string $subdomain): string
    {
        $domains = Domain::where('domain', $subdomain)->get();

        if($domains->isNotEmpty()){
            $splitdomain = explode('.', $domains->sortByDesc('id')->first()?->domain);
            $sub = $splitdomain[0]??'';
            $matches = [];
            preg_match('/(.*?)(\d*)$/', $sub, $matches);
            $base = $matches[1];
            $num = $matches[2];

            $domain_array = explode('.', $subdomain);
            $update_subdomain = $base . ((int) $num + 1);
            $domain_array[0] = $update_subdomain;

            $subdomain = implode('.', $domain_array);

            return $this->generateUniqueSubdomain($subdomain);
        }
        return $subdomain;
    }
}
