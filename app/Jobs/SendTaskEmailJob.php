<?php

namespace App\Jobs;

use App\Mail\TaskCreated;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendTaskEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emails;
    protected $task;
    protected $help_portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emails, $task, $help_portal)
    {
        $this->emails = $emails;
        $this->task = $task;
        $this->help_portal = $help_portal;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to($this->emails)->send(new TaskCreated($this->task, $this->help_portal));
    }
}
