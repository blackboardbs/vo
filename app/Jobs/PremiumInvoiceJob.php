<?php

namespace App\Jobs;

use App\API\Currencies\Currency;
use App\Enum\FrequencyEnum;
use App\Enum\InvoiceType;
use App\Enum\Status;
use App\Models\Config;
use App\Models\Customer;
use App\Models\CustomerInvoice;
use App\Models\InvoiceContact;
use App\Models\InvoiceItems;
use App\Models\Recurring;
use App\Models\Tenant;
use App\Models\User;
use App\Models\VatRate;
use Carbon\Carbon;
use Composer\Semver\Interval;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Jobs\ProcessWebhookJob;
use Spatie\WebhookClient\Models\WebhookCall;

class PremiumInvoiceJob extends ProcessWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public WebhookCall $webhookCall)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(Currency $currency): void
    {
        $tenantId = app()->isProduction() ? "a177e389-8d01-437e-8fe0-1691313a6b69" : "a10762a2-7eec-4f15-a2db-73823d9febe6";

        Tenant::find($tenantId)?->run(function () use ($currency) {

                $payload = $this->webhookCall->payload;

                $company = $payload['company'];
                $user = $payload['user'];
                $lineItem = $payload['account_details'];

                $customer = Customer::where('customer_name', $company['name'])->first();

                if (!isset($customer)) {
                    $customer = new Customer();
                    $customer->customer_name = $company['name'];
                    $customer->vat_no = $company['tax'];
                    $customer->contact_firstname = $user['first_name'];
                    $customer->contact_lastname = $user['last_name'];
                    $customer->cell = $user['phone'];
                    $customer->phone = $user['phone'];
                    $customer->email = $user['email'];
                    $customer->street_address_line1 = $company['address_line_1'];
                    $customer->street_address_line2 = $company['address_line_2'];
                    $customer->street_address_line3 = $company['address_line_3'];
                    $customer->state_province = $company['province'];
                    $customer->city_suburb = $company['suburb'];
                    $customer->postal_zipcode = $company['postal_code'];
                    $customer->country_id = $company['country'];
                    $customer->status = Status::ACTIVE->value;
                    $customer->save();

                    $contact = new InvoiceContact();
                    $contact->first_name = $user['first_name'];
                    $contact->last_name = $user['last_name'];
                    $contact->email = $user['email'];
                    $contact->contact_number = $user['phone'];
                    $contact->status_id = Status::ACTIVE->value;
                    $contact->save();

                    $customer->invoice_contact_id = $contact->id;
                    $customer->save();
                }

                $customer_invoice = CustomerInvoice::latest()->first();

                $invoiceNumber = $customer_invoice ? $customer_invoice->invoiceNumberString():0;

                $customer_invoice = new CustomerInvoice();
                $customer_invoice->customer_id = $customer->id;
                $customer_invoice->customer_invoice_number = $invoiceNumber;
                $customer_invoice->invoice_date = now()->toDateString();
                $customer_invoice->company_id = Config::first(['company_id'])?->company_id;
                $customer_invoice->bill_status = 1;
                $customer_invoice->currency = $lineItem['currency'];
                $customer_invoice->conversion_rate = $currency->conversionRate($lineItem['currency'])['rate']??0;
                $customer_invoice->save();

                $emails = User::whereHas('roles', fn($role) => $role->where('name', 'admin'))->pluck('email')->toArray();

                $customer_invoice->recurring()->save(new Recurring([
                    'first_invoice_at' => $lineItem['starts_at'],
                    'due_days_from_invoice_date' => 30,
                    'frequency' => FrequencyEnum::Month->value,
                    'interval' => 1,
                    'expires_at' => now()->addDecade()->toDateString(),
                    'email_subject' => "Consulteaze subscription invoice",
                    'emails' => implode(",", [$user['email'], ...$emails]),
                ]));

                if (Carbon::parse($lineItem['starts_at'])->toDateString() === now()->toDateString()){
                    $invoiceCopy = $customer_invoice->replicate();
                    $invoiceCopy->invoice_date = now()->toDateString();
                    $invoiceCopy->due_date = now()->addDays(30)->toDateString();
                    $invoiceCopy->save();

                    $customer_invoice->recurring()->update([
                        'last_invoiced_at' => now()->toDateTimeString()
                    ]);

                    $customer_invoice->histories()->attach($invoiceCopy->id, ['type' => InvoiceType::Customer->value]);
                }

                $vat_rate = VatRate::where('vat_code', 1)->where('end_date', '>=', now()->toDateString())->first();

                $invoiceItem = new InvoiceItems();
                $invoiceItem->invoice_id = $customer_invoice->id;
                $invoiceItem->invoice_type_id = InvoiceType::Customer->value;
                $invoiceItem->description = "Consulteaze monthly subscription";
                $invoiceItem->quantity = 1;
                $invoiceItem->price_excl = $lineItem['price'] - ($lineItem['price'] * ($vat_rate->vat_rate/100));
                $invoiceItem->vat_code = $vat_rate->vat_code;
                $invoiceItem->vat_percentage = $vat_rate->vat_rate;
                $invoiceItem->discount_amount = 0;
                $invoiceItem->total = $lineItem['price'];
                $invoiceItem->creator_id = auth()->id();
                $invoiceItem->status_id = Status::ACTIVE->value;
                $invoiceItem->save();

                if($invoiceItem->invoice_type_id == 1){
                    $customerInvoice = CustomerInvoice::find($invoiceItem->invoice_id);
                    $customerInvoice->invoice_value = (string) $this->lineItemsTotal($invoiceItem);
                    $customerInvoice->save();

                    if($customerInvoice->histories()->count()){
                        $itemCopy = $invoiceItem->replicate();
                        $itemCopy->invoice_id = $customerInvoice->histories()->first()?->id;
                        $itemCopy->save();

                        $generatedInvoice = $customerInvoice->histories()->first();
                        $generatedInvoice->invoice_value = (string) $this->lineItemsTotal($itemCopy);
                        $generatedInvoice->save();
                    }
                }
            });
    }

    private function lineItemsTotal(InvoiceItems $invoiceItem): float|int
    {
        $invoiceItems = InvoiceItems::selectRaw('*,0 as canEdit')->where('status_id', '=', 1)->where('invoice_type_id', '=', $invoiceItem->invoice_type_id)->where('invoice_id', '=', $invoiceItem->invoice_id)->get();

        return $invoiceItems->sum('total');
    }
}
