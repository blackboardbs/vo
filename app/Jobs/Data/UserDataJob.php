<?php

namespace App\Jobs\Data;

use App\Models\User;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class UserDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        User::where('id', '<>', 1)->get()->each(fn($user) => $user->delete());

        collect(Arr::except($this->readCsv("users.csv"), [0]))
            // ->map(fn($user) => Arr::except($user, ['id']))
            ->chunk(1000)
            ->each(fn($user) => User::insert($user->toArray()));
    }
}
