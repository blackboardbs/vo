<?php

namespace App\Jobs\Data;

use App\Models\CustomerInvoiceLineExpense;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomerInvoiceLineExpenseDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        CustomerInvoiceLineExpense::truncate();

        collect($this->readCsv("customer_invoice_line_expenses.csv"))
            ->chunk(1000)
            ->each(fn($invoice_expense) => CustomerInvoiceLineExpense::insert($invoice_expense->toArray()));
    }
}
