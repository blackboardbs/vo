<?php

namespace App\Jobs\Data;

use App\Models\Timeline;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class TimelineDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Timeline::truncate();

        collect($this->readCsv("timeline.csv"))
            ->chunk(1000)
            ->each(function($timeline){

                $timeline = $timeline->map(fn($line) => Arr::except($line, ['time_id', 'wbs_id']))->toArray();

                Timeline::insert($timeline);
            });
    }
}
