<?php

namespace App\Jobs\Data;

use App\Models\CustomerInvoiceLine;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomerInvoiceLineDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        CustomerInvoiceLine::truncate();

        collect($this->readCsv("customer_invoice_lines.csv"))
            ->chunk(1000)
            ->each(fn($invoice_line) => CustomerInvoiceLine::insert($invoice_line->toArray()));
    }
}
