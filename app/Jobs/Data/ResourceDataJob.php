<?php

namespace App\Jobs\Data;

use App\Models\Resource;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class ResourceDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Resource::whereNot('id', 1)->get()
            ->each(fn($resource) => $resource->delete());

        collect(Arr::except($this->readCsv("resource.csv"), [0]))
            // ->map(fn($resource) => Arr::except($resource, ['id']))
            ->chunk(1000)
            ->each(fn($resource) => Resource::insert($resource->toArray()));

    }
}
