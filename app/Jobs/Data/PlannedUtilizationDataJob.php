<?php

namespace App\Jobs\Data;

use App\Models\Utilization;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;

class PlannedUtilizationDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Utilization::truncate();

        $utilization = collect($this->readCsv('plan_utilization.csv'))
            ->map(fn($util) => Arr::except($util, ['id', 'created_at', 'updated_at', 'deleted_at']))
            ->toArray();

        Utilization::insert($utilization);
    }
}
