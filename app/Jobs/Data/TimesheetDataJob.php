<?php

namespace App\Jobs\Data;

use App\Models\Timesheet;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class TimesheetDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Timesheet::truncate();

        collect($this->readCsv('timesheet.csv'))
            ->chunk(1000)
            ->each(function($timesheet){

                $timesheet = $timesheet->map(function($time){

                    if (isset($time['billable'])){
                        $time['is_billable'] = $time['billable'];
                    }

                    return Arr::except($time, ['billable', 'wbs_id']);
                });

                Timesheet::insert($timesheet->toArray());
        });
    }
}
