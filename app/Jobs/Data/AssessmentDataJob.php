<?php

namespace App\Jobs\Data;

use App\Models\AssessmentHeader;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AssessmentDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        AssessmentHeader::truncate();

        collect($this->readCsv("assessment_headers.csv"))
            ->chunk(1000)
            ->each(fn($assesmnent) => AssessmentHeader::insert($assesmnent->toArray()));
    }
}
