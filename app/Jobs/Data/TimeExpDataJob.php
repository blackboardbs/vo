<?php

namespace App\Jobs\Data;

use App\Models\TimeExp;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class TimeExpDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        TimeExp::truncate();

        collect($this->readCsv("time_exp.csv"))
            ->chunk(1000)
            ->each(function($expense){
                $expense = $expense->map(fn($time_exp) => Arr::except($time_exp, [
                    'billable',
                    'declining_reason',
                    'time_id',
                    'wbs_id'
                ]))->toArray();
                TimeExp::insert($expense);
            });
    }
}
