<?php

namespace App\Jobs\Data;

use App\Models\ExpenseTracking;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class ExpenseTrackingDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $expenses = collect($this->readCsv("exp_tracking.csv"))->chunk(1000);

        ExpenseTracking::truncate();

        foreach ($expenses as $expense){
            ExpenseTracking::insert($expense->toArray());
        }

    }
}
