<?php

namespace App\Jobs\Data;

use App\Models\PlannedExpense;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PlannedExpensesDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $expenses = collect($this->readCsv('plan_exp.csv'))->chunk(1000);

        PlannedExpense::truncate();

        $expenses->each(fn($expense) => PlannedExpense::insert($expense->toArray()));
    }
}
