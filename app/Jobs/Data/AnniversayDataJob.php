<?php

namespace App\Jobs\Data;

use App\Dashboard\Anniversaries;
use App\Models\Anniversary;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AnniversayDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Anniversary::truncate();

        collect($this->readCsv("anniversary.csv"))
            ->chunk(1000)
            ->each(fn($anniversary) => Anniversary::insert($anniversary->toArray()));
    }
}
