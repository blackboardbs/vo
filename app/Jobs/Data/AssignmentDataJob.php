<?php

namespace App\Jobs\Data;

use App\Models\Assignment;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AssignmentDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Assignment::truncate();

        collect($this->readCsv('assignment.csv'))
            ->chunk(1000)
            ->each(fn($assignment) => Assignment::insert($assignment->toArray()));
    }
}
