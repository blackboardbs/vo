<?php

namespace App\Jobs\Data;

use App\Models\VendorInvoice;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class VendorInvoiceDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        VendorInvoice::truncate();

        collect($this->readCsv("vendor_invoice.csv"))
            ->chunk(1000)
            ->each(fn($vendor_invoice) => VendorInvoice::insert($vendor_invoice->toArray()));
    }
}
