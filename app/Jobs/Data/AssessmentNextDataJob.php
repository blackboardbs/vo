<?php

namespace App\Jobs\Data;

use App\Models\AssessmentPlannedNext;
use App\Traits\HasCsv;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AssessmentNextDataJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HasCsv;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        AssessmentPlannedNext::truncate();

        collect($this->readCsv("assessment_planned_nexts.csv"))
            ->chunk(1000)
            ->each(fn($next) => AssessmentPlannedNext::insert($next->toArray()));
    }
}
