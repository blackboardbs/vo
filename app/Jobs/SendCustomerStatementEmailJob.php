<?php

namespace App\Jobs;

use App\Mail\CustomerStatementEmail;
use App\Mail\VendorStatementMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendCustomerStatementEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $ccEmails;
    protected $customerInvoice;
    protected $file;
    protected $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $ccEmails, $customerInvoice, $file, $action = 1)
    {
        $this->email = $email;
        $this->ccEmails = $ccEmails;
        $this->customerInvoice = $customerInvoice;
        $this->file = $file;
        $this->action = $action;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if ($this->action == 1)
            Mail::to($this->email)->cc(array_filter($this->ccEmails))->send(new CustomerStatementEmail($this->customerInvoice, $this->file));
        else
            Mail::to($this->email)->cc(array_filter($this->ccEmails))->send(new VendorStatementMail($this->customerInvoice, $this->file));
    }
}
