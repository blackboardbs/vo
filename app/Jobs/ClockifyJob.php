<?php

namespace App\Jobs;

use App\API\Clockify\ClockifyAPI;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClockifyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $client;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $client)
    {

        $this->client = $client;
    }

    /**
     * Execute the job.
     */
    public function handle(ClockifyAPI $clockify): void
    {
        $clockify->addClient(["name" => $this->client["name"], "note" => $this->client["id"]]);
    }
}
