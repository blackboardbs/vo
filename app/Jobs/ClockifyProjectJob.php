<?php

namespace App\Jobs;

use App\API\Clockify\ClockifyAPI;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClockifyProjectJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $project;
    public function __construct(array $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     */
    public function handle(ClockifyAPI $clockifyAPI): void
    {
        $clockifyAPI->postEndPoint(
            config('app.clockify.base_endpoint')."/workspaces/".config('app.clockify.workspace')."/projects",
            $this->project
        );
    }
}
