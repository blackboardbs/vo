<?php

namespace App\Actions\Config;

use App\Models\Config;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class StoreOrCreate
{
    public function action(Request $request, Config $config): Config
    {
        $assessment_master = '';
        if ($request->input('assessment_master')) {
            foreach ($request->input('assessment_master') as $key => $assessment) {
                $assessment_master .= ($key != (count($request->input('assessment_master')) - 1)) ? $assessment.',' : $assessment;
            }
        }

        $config->days_to_approve_leave = $request->input('days_to_approve_leave');
        $config->calendar_content = $request->input('calendar_content');
        $config->home_page_message = $request->input('home_page_message');
        $config->auditing_records = $request->input('auditing_records');
        $config->site_title = $request->input('site_title');
        $config->site_name = $request->input('site_name');
        $config->admin_email = $request->input('admin_email');
        $config->assessment_approver_user_id = $request->input('assessment_approver_user_id');
        $config->utilization_hours_per_week = $request->input('utilization_hours_per_week');
        $config->utilization_method = $request->input('utilization_method');
        $config->utilization_cost_hours_per_week = $request->input('utilization_cost_hours_per_week');
        $config->calendar_action_colour = $request->input('action_colour');
        $config->calendar_task_colour = $request->input('task_colour');
        $config->calendar_leave_colour = $request->input('leave_colour');
        $config->calendar_assignment_colour = $request->input('assignment_colour');
        $config->calendar_assessment_colour = $request->input('assessment_colour');
        $config->calendar_anniversary_colour = $request->input('anniversary_colour');
        $config->calendar_events_colour = $request->input('events_colour');
        $config->calendar_user_colour = $request->input('user_colour');
        $config->calendar_cinvoice_colour = $request->input('cinvoice_colour');
        $config->calendar_vinvoice_colour = $request->input('vinvoice_colour');
        $config->calendar_medcert_colour = $request->input('mcertificate_colour');
        $config->calendar_public_holidays_colour = $request->input('public_holidays_colour');
        $config->cost_min_percent = $request->input('cost_min_percent');
        $config->cost_min_colour = $request->input('cost_min_colour');
        $config->cost_mid_colour = $request->input('cost_mid_colour');
        $config->cost_max_percent = $request->input('cost_max_percent');
        $config->cost_max_colour = $request->input('cost_max_colour');
        $config->project_terms_id = $request->input('project_terms_id');
        $config->claim_approver_id = $request->input('claim_approver_id');
        $config->vendor_template_id = $request->input('vendor_template_id') != '' ? $request->input('vendor_template_id') : 0;
        $config->resource_template_id = $request->input('resource_template_id') != '' ? $request->input('resource_template_id') : 0;
        $config->assessment_frequency = $request->input('assessment_frequency');
        $config->assessment_master = $assessment_master;
        $config->company_id = $request->input('company_id');
        $config->first_warning_days = $request->input('first_warning_days');
        $config->second_warning_days = $request->input('second_warning_days');
        $config->last_warning_days = $request->input('last_warning_days');
        $config->first_warning_percentage = $request->input('first_warning_percentage');
        $config->second_warning_percentage = $request->input('second_warning_percentage');
        $config->last_warning_percentage = $request->input('last_warning_percentage');
        $config->cost_center_id = $request->input('cost_center_id');
        $config->timesheet_weeks = $request->input('timesheet_weeks');
        $config->timesheet_weeks_future = $request->input('timesheet_weeks_future');
        $config->annual_leave_days = $request->input('annual_leave_days');
        $config->sick_leave_cycle_months = $request->input('sick_leave_cycle_months');
        $config->sick_leave_days_per_cycle = $request->input('sick_leave_days_per_cycle');
        $config->vat_rate_id = $request->input('vat_rate_id');
        $config->invoice_body_msg = $request->input('invoice_body_msg');
        $config->default_invoice_template = $request->input('default_invoice_template');
        $config->default_pf_invoice_template = $request->input('default_pf_invoice_template');
        $config->logo_placement = $request->input('logo_placement');
        if ($request->background_color != $request->font_color) {
            $config->background_color = $request->background_color;
            $config->font_color = $request->font_color;
            $config->active_link = $request->active_link;
        }
        $exception_message = '';
        if ($request->hasFile('site_logo')) {
            try {
                $request->file('site_logo')->store('public/assets');
                $file = $request->file('site_logo')->hashName();
                $config->site_logo = $file;

                $image = Image::make(base_path().'/public/assets/'.$file);
                $image->resize(null, 120, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }

        $config->ojs_minimum_qty = $request->input('ojs_minimum_qty');
        $config->ojs_minimum_color = $request->input('ojs_minimum_color');
        $config->ojs_between_color = $request->input('ojs_between_color');
        $config->ojs_target_qty = $request->input('ojs_target_qty');
        $config->ojs_target_color = $request->input('ojs_target_color');
        $config->cvsm_minimum_qty = $request->input('cvsm_minimum_qty');
        $config->cvsm_minimum_color = $request->input('cvsm_minimum_color');
        $config->cvsm_between_color = $request->input('cvsm_between_color');
        $config->cvsm_target_qty = $request->input('cvsm_target_qty');
        $config->cvsm_target_color = $request->input('cvsm_target_color');
        $config->cvsw_minimum_qty = $request->input('cvsw_minimum_qty');
        $config->cvsw_minimum_color = $request->input('cvsw_minimum_color');
        $config->cvsw_between_color = $request->input('cvsw_between_color');
        $config->cvsw_target_qty = $request->input('cvsw_target_qty');
        $config->cvsw_target_color = $request->input('cvsw_target_color');
        $config->nsam_minimum_qty = $request->input('nsam_minimum_qty');
        $config->nsam_minimum_color = $request->input('nsam_minimum_color');
        $config->nsam_between_color = $request->input('nsam_between_color');
        $config->nsam_target_qty = $request->input('nsam_target_qty');
        $config->nsam_target_color = $request->input('nsam_target_color');
        $config->nsaw_minimum_qty = $request->input('nsaw_minimum_qty');
        $config->nsaw_minimum_color = $request->input('nsaw_minimum_color');
        $config->nsaw_between_color = $request->input('nsaw_between_color');
        $config->nsaw_target_qty = $request->input('nsaw_target_qty');
        $config->nsaw_target_color = $request->input('nsaw_target_color');
        $config->default_cv_template = $request->input('default_cv_template');
        $config->cv_submission_text = $request->input('cv_submission_text');
        $config->timesheet_template_id = $request->input('timesheet_template_id');
        $config->default_custom_template = $request->input('default_custom_template');
        $config->recruitment_default_email = $request->input('recruitment_default_email');
        $config->recruitment_default_contact_number = $request->input('recruitment_default_contact_number');
        $config->recruitment_default_cv_id = $request->input('recruitment_default_cv_id');
        $config->recruitment_default_company_id = $request->input('recruitment_default_company_id');
        $config->billing_cycle_id = $request->input('billing_cycle_id');
        $config->customer_invoice_prefix = $request->input('customer_invoice_prefix');
        $config->customer_invoice_format = $request->input('customer_invoice_format');
        $config->customer_invoice_start_number = $request->input('customer_invoice_start_number');
        $config->customer_pro_forma_invoice_prefix = $request->input('customer_pro_forma_invoice_prefix');
        $config->customer_pro_forma_invoice_format = $request->input('customer_pro_forma_invoice_format');
        $config->customer_pro_forma_invoice_start_number = $request->input('customer_pro_forma_invoice_start_number');
        $config->vendor_invoice_prefix = $request->input('vendor_invoice_prefix');
        $config->vendor_invoice_format = $request->input('vendor_invoice_format');
        $config->vendor_invoice_start_number = $request->input('vendor_invoice_start_number');
        $config->hr_user_id = $request->input('hr_user_id');
        $config->hr_approval_manager_id = $request->input('hr_approval_manager_id');
        $config->supply_chain_email = $request->input('supply_chain_email');
        $config->supply_chain_approval_manager_id = $request->input('supply_chain_approval_manager_id');
        $config->customer_email = $request->input('customer_email');
        $config->customer_approval_manager_id = $request->input('customer_approval_manager_id');
        // $config->kanban_show_extra_columns = $request->input('kanban_show_extra_columns');
        $config->kanban_show_backlog = ($request->input('showBacklog') == 'on' ? 1 : 0);
        $config->kanban_show_planned = ($request->input('showPlanned') == 'on' ? 1 : 0);
        $config->kanban_show_in_progress = ($request->input('showInProgress') == 'on' ? 1 : 0);
        $config->kanban_show_done = ($request->input('showDone') == 'on' ? 1 : 0);
        $config->kanban_show_impediment = ($request->input('showImpediment') == 'on' ? 1 : 0);
        $config->kanban_show_completed = ($request->input('showCompleted') == 'on' ? 1 : 0);
        $config->kanban_show_hours = ($request->input('showHours') == 'on' ? 1 : 0);
        $config->kanban_show_actuals = ($request->input('showActuals') == 'on' ? 1 : 0);
        $config->kanban_show_timeframe = ($request->input('showTimeframe') == 'on' ? 1 : 0);
        $config->kanban_nr_of_tasks = $request->input('kanban_nr_of_tasks');
        $config->kanban_view = $request->input('kanban_view');
        $config->sh_good_min = $request->input('sh_good_min');
        $config->sh_good_max = $request->input('sh_good_max');
        $config->sh_fair_min = $request->input('sh_fair_min');
        $config->sh_fair_max = $request->input('sh_fair_max');
        $config->sh_bad_min = $request->input('sh_bad_min');
        $config->sh_bad_max = $request->input('sh_bad_max');
        $config->prospect_columns = implode(',',$request->showprospect);
        $config->site_report_title = $request->input('site_report_title');
        $config->include_consultants = ($request->input('incl_consultants') == 'on' ? 1 : 0);
        $config->include_task_hours = ($request->input('incl_task_hours') == 'on' ? 1 : 0);
        $config->include_hours_summary = ($request->input('incl_hours_summary') == 'on' ? 1 : 0);
        $config->include_consultant_name = ($request->input('incl_consultant_name') == 'on' ? 1 : 0);
        $config->include_report_to = ($request->input('incl_report_to') == 'on' ? 1 : 0);
        $config->include_risks = ($request->input('incl_risks') == 'on' ? 1 : 0);
        $config->include_customer_logo = ($request->input('incl_customer_logo') == 'on' ? 1 : 0);
        $config->include_company_logo = ($request->input('incl_company_logo') == 'on' ? 1 : 0);
        $config->site_report_footer = $request->input('site_report_footer');
        $config->show_delivery_type = $request->input('show_delivery_type');
        $config->client_service_agreement_id = $request->input('client_service_agreement_id');
        $config->provider_service_agreement_id = $request->input('provider_service_agreement_id');
        $config->save();

        return $config;
    }
}