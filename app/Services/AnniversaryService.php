<?php

namespace App\Services;

use App\Http\Requests\AnniversaryRequest;
use App\Models\Anniversary;
use App\Models\Module;
use App\Services\HasPermissions;

class AnniversaryService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\Anniversary")->first();
    }

    public function createAnniversary(AnniversaryRequest $request, Anniversary $anniversary): Anniversary
    {
        $anniversary->anniversary = $request->input('anniversary');
        $anniversary->emp_id = $request->input('emp_id');
        $anniversary->name = $request->input('name');
        $anniversary->email = $request->input('email');
        $anniversary->phone = $request->input('phone');
        $anniversary->contact_email = $request->input('contact_email');
        $anniversary->contact_phone = $request->input('contact_phone');
        $anniversary->relationship = $request->input('relationship');
        $anniversary->anniversary_date = $request->input('anniversary_date');
        $anniversary->status_id = $request->input('status_id');
        $anniversary->save();

        return $anniversary;
    }

    public function updateAnniversary(AnniversaryRequest $request, Anniversary $anniversary): Anniversary
    {
        return $this->createAnniversary($request, $anniversary);
    }
}