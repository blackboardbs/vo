<?php

namespace App\Services;

use App\Enum\Status;
use App\Models\Config;
use App\Models\VatRate;
use Illuminate\Database\Eloquent\Collection;

class ProFormaInvoiceService
{
    public function lineItemsGrouped(Collection $timesheets, string $rate): \Illuminate\Support\Collection
    {
        return  $timesheets->map(function ($timesheet) use ($rate) {
            $items = $timesheet->timeline?->map(fn ($line) => (object)[
                'description' => $line->task->description ?? $line->description_of_work,
                'time' => $line->total + ($line->total_m/60)
            ]);

            $price = $items->sum('time') * $timesheet->$rate;

            return (object) [
                'rate' => $timesheet->$rate??0,
                'currency' => $timesheet->currency??'R',
                'vat' => $this->vatRate() * 100,
                'description' => $items->first()?->description,
                'time' => $items->sum('time'),
                'exc_price' => $price,
                'inc_price' => $price * (1 + $this->vatRate())
            ];
        })->groupBy('description');
    }

    public function vatRate(): float|int
    {
        $vatRate = VatRate::query();

        if (request()->has('vat_rate_id')) {
            $vatRate = $vatRate->where('id', request()->vat_rate_id)->first();
        }else{
            $config = Config::first(['vat_rate_id']);
            $vatRate = $vatRate->select('vat_rate')
                ->where('status', Status::ACTIVE->value)
                ->where('vat_code', $config->vat_rate_id)->where('start_date', '<=', now()->toDateString())->where('end_date', '>=', now()->toDateString())
                ->latest()
                ->first();
        }

        return $vatRate->vat_rate/100;
    }
}