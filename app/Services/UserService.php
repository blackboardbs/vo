<?php

namespace App\Services;

use App\Enum\Status;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\SendWelcomeEmailJob;
use App\Jobs\SendAdminSubscriptionEmailJob;
use App\Jobs\SendCancelSubscriptionEmailJob;
use App\Mail\WelcomeOnboardingMail;
use App\Mail\WelcomeOnboardingManagerNotificationMail;
use App\Models\Company;
use App\Models\Config;
use App\Models\LandingPage;
use App\Models\Module;
use App\Models\Resource;
use App\Models\User;
use Carbon\Carbon;
use App\Models\LeaveBalance;
use App\Models\UserOnboarding;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use SebastianBergmann\Diff\Exception;

class UserService extends HasPermissions
{
    public function module(): Module|null
    {
        return Module::where('name', '=', "App\Models\User")->first();
    }

    public function createUser(UpdateUserRequest|StoreUserRequest $request, User $user): User
    {
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        if ($user->email != $request->input('email')) {
            $user->email = $request->input('email');
        }
        $user->phone = $request->input('phone');
        $user->company_id = $request->input('company')??Company::first()?->id;
        $user->status_id = $request->input('status')??Status::ACTIVE->value;
        $user->expiry_date = $request->input('expiry_date');
        $user->onboarding = $request->input('onboarding', 0);
        $user->usertype_id = $request->input('usertype_id');
        $user->appointment_manager_id = $request->input('appointment_manager_id');
        $user->vendor_id = $request->input('vendor');
        $user->customer_id = $request->input('customer');

        if ($request->hasFile('avatar')) {
            $user->thumbnail(request: $request, field: 'avatar', type: 'user', height: 200, width: 200);
            $user->avatar = $request->file('avatar')->hashName();
        }

        return $user;
    }

    public function updateUser(UpdateUserRequest $request, User $user): User
    {
        $user = $this->createUser($request, $user);
        if (!$user->resource_id) {
            $user->resource_id = $this->resourceId();
        }
        $user = $this->loginType($request, $user);
        $user->save();

        return $user;
    }

    public function loginType(UpdateUserRequest $request, User $user): User
    {
        $password = Str::random(8);

        if ($user->login_user) {
            if ($request->login_user != 1) {
                $user->login_user = 0;
                $user->password = Hash::make($password);
            }
        } else {
            if ($request->login_user == 1) {
                $user->login_user = 1;
                $user->password = Hash::make($password);
                $this->sendUserEmail($user, $password);
            }
        }

        return $user;
    }

    public function userDoesNotHaveResource(User $user): User
    {

        $resource = new Resource;
        $resource->user_id = $user->id;
        $resource->save();

        $user->resource_id = $resource->id;
        $user->save();

        return $user;
    }

    public function managers(): Collection
    {
        return User::selectRaw("id, CONCAT(first_name,' ', last_name) AS full_name")->where('status_id', 1)->where('expiry_date', '>=', now()->toDateString())->whereHas('roles', function ($role) {
            return $role->whereIn('id', [1, 2, 3]);
        })->pluck('full_name', 'id');
    }

    public function resourceId(): int
    {
        $resource = new Resource;
        $resource->status_id = Status::ACTIVE->value;
        $resource->save();

        return $resource->id;
    }

    public function createUserOnboarding(int $user_id): void
    {
        $onboarding = new UserOnboarding();
        $onboarding->user_id = $user_id;
        $onboarding->status_id = Status::ACTIVE->value;
        $onboarding->save();
    }

    public function createUserLandingPage(int $user_id, string $landing_page = null): void
    {
        LandingPage::updateOrCreate(
            ['user_id' => $user_id],
            ['page' => $landing_page??(request('page') ?? url('/dashboard'))]
        );
    }

    public function assignUserRoles(Request $request, User $user): void
    {
        if ($request->input('onboarding')) {
            $user->assignRole('user_onboarding');
        } else {
            $user->assignRole($request->input('role'));
        }
    }

    public function sendOnboardingEmails(User $user, string $password): string
    {
        $user_fields = ['id', 'first_name', 'last_name', 'email', 'phone'];
        $config = Config::with([
            'company' => function ($company) {
                $company->select(['id', 'company_name']);
            }, 'hrUser' => function ($hr) use ($user_fields) {
                $hr->select($user_fields);
            }, 'approvalManager' => function ($approvingManager) use ($user_fields) {
                $approvingManager->select($user_fields);
            }])->select(['company_id', 'admin_email', 'hr_user_id', 'hr_approval_manager_id'])->first();
        $company = $config->company->company_name ?? (Company::first(['company_name'])->company_name ?? 'Blackboard');
        $appointing_manager = $user->appointmentManager;
        $onboarding = $user->userOnboarding;
        $hr = $config->hrUser;
        $user_typer = $user->userType->description ?? 'No user type is selected';
        try {
            Mail::to($user->email)->send(new WelcomeOnboardingMail($user, $company, $password, $onboarding, $appointing_manager, $config->admin_email));
            Mail::to([$hr->email, $appointing_manager->email])
                ->send(new WelcomeOnboardingManagerNotificationMail($user, $hr, $appointing_manager, $config->approvalManager, $company, $user_typer, $onboarding));
            $message = 'User captured successfully';
        }catch (\Exception $exception){
            $message = $exception->getMessage();
        }

        return $message;
    }

    public function sendUserEmail(User $user, $password): string
    {
        try {
            SendWelcomeEmailJob::dispatch($user, $password,null)->delay(now()->addMinutes(5));
            $message = 'User captured successfully';
        }catch (\Exception $exception){
            $message = $exception->getMessage();
        }

        return $message;
    }

    public function sendAdminSubscriptionEmail($details): string
    {
        $email = User::first()->email;
        try {
            SendAdminSubscriptionEmailJob::dispatch($email,$details)->delay(now()->addMinutes(5));
            $message = 'User captured successfully';
        }catch (\Exception $exception){
            $message = $exception->getMessage();
        }

        return $message;
    }

    public function sendCancelSubscriptionEmail($details): string
    {
        $user = User::first();
        try {
            SendCancelSubscriptionEmailJob::dispatch('support@consulteaze.com',$user,$details)->delay(now()->addMinutes(5));
            $message = 'User captured successfully';
        }catch (\Exception $exception){
            $message = $exception->getMessage();
        }

        return $message;
    }

    public function createLeaveBalance($user_id){
        $config = Config::first();
        $leave = LeaveBalance::where('resource_id', $user_id)->where('leave_type_id', 1)->get();

        if (count($leave) == 0) {
            $leavebalance = new LeaveBalance();
            $leavebalance->resource_id = $user_id;
            $leavebalance->leave_type_id = 1;
            $leavebalance->balance_date = Carbon::parse(now())->format('Y-m-d');
            $leavebalance->leave_per_cycle = $config->annual_leave_days;
            $leavebalance->opening_balance = 0;
            $leavebalance->status = 1;
            $leavebalance->created_by = auth()->id();
            $leavebalance->save();
        } else {
            $leavebalance = LeaveBalance::find($leave[0]->id);
            $leavebalance->resource_id = $user_id;
            $leavebalance->leave_type_id = 1;
            $leavebalance->balance_date = Carbon::parse(now())->format('Y-m-d');
            $leavebalance->leave_per_cycle = $config->annual_leave_days;
            $leavebalance->opening_balance = 0;
            $leavebalance->status = 1;
            $leavebalance->created_by = auth()->id();
            $leavebalance->save();
        }
    }
}