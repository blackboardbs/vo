<?php

namespace App\Services;

use App\Http\Requests\BillingCycleRequest;
use App\Models\BillingCycle;
use App\Models\Module;

class BillingCycleService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where(['name' => "App\\Models\\BillingCycle", 'status_id' => 1])->first();
    }

    public function createBillingPeriod(BillingCycleRequest $request, BillingCycle $billing_cycle): BillingCycle
    {
        $billing_cycle->name = $request->name;
        $billing_cycle->description = $request->description;
        $billing_cycle->max_weeks = $request->max_weeks;
        $billing_cycle->status_id = $request->status_id;
        $billing_cycle->save();

        return $billing_cycle;
    }

    public function updateBillingPeriod(BillingCycleRequest $request, BillingCycle $billing_cycle): BillingCycle
    {
        return $this->createBillingPeriod($request, $billing_cycle);
    }

    public function yearWeekNumbers(): array
    {
        $yearly_weeks = now()->lastOfYear()->weekOfYear;
        $max_weeks_dropdown = [];
        for ($i = 1; $i <= $yearly_weeks; $i++) {
            $max_weeks_dropdown[$i] = $i;
        }

        return $max_weeks_dropdown;
    }
}