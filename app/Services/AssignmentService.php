<?php

namespace App\Services;

use App\Models\Module;
use App\Services\HasPermissions;

class AssignmentService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\Assignment")->first();
    }
}