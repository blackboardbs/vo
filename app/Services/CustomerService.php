<?php

namespace App\Services;

use App\API\Clockify\ClockifyAPI;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use App\Models\Document;
use App\Models\Module;
use App\Models\User;
use Intervention\Image\Facades\Image;

class CustomerService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', '=', 'App\Models\CustomerProfile')->first();
    }

    public function createCustomer(StoreCustomerRequest|UpdateCustomerRequest $request, Customer $customer): Customer
    {
        $customer->customer_name = $request->input('customer_name');
        $customer->business_reg_no = $request->input('business_reg_no');
        $customer->vat_no = $request->input('vat_no');
        $customer->account_manager = $request->input('account_manager');
        $customer->contact_firstname = $request->input('contact_firstname');
        $customer->contact_lastname = $request->input('contact_lastname');
        $customer->contact_birthday = $request->input('contact_birthday');
        $customer->phone = $request->input('phone');
        $customer->cell = $request->input('cell');
        $customer->cell2 = $request->input('cell2');
        $customer->fax = $request->input('fax');
        $customer->email = $request->input('email');
        $customer->web = $request->input('web');
        $customer->postal_address_line1 = $request->input('postal_address_line1');
        $customer->postal_address_line2 = $request->input('postal_address_line2');
        $customer->postal_address_line3 = $request->input('postal_address_line3');
        $customer->city_suburb = $request->input('city_suburb');
        $customer->postal_zipcode = $request->input('postal_zipcode');
        $customer->street_address_line1 = $request->input('street_address_line1');
        $customer->street_address_line2 = $request->input('street_address_line2');
        $customer->street_address_line3 = $request->input('street_address_line3');
        $customer->street_city_suburb = $request->input('street_city_suburb');
        $customer->street_zipcode = $request->input('street_zipcode');
        $customer->state_province = $request->input('state_province');
        $customer->country_id = $request->input('country_id');
        $customer->payment_terms = $request->input('payment_terms_days');
        $customer->billing_note = $request->input('billing_note');
        $customer->medical_certificate_type = $request->input('medical_certificate_type');
        $customer->medical_certificate_text = $request->input('medical_certificate_text');
        $customer->bbbee_level = $request->input('bbbee_level');
        $customer->bbbee_certificate_expiry = $request->input('bbbee_certificate_expiry');
        $customer->bbbee_level_preference = $request->input('bbbee_level_pref');
        $customer->status = $request->input('status');
        $customer->commission = $request->input('commission');
        $customer->creator_id = auth()->id();
        $customer->billing_cycle_id = $request->input('billing_cycle_id');
        $customer->invoice_contact_id = $request->input('invoice_contact_id');
        $customer->onboarding = $request->input('onboarding');
        $customer->appointment_manager_id = $request->input('appointment_manager_id');
        $customer->service_agreement_id = $request->input('service_agreement_id');
        $customer->nda_template_id = $request->input('nda_template_id');

        if ($request->hasFile('logo')) {
            try {
                $path = $request->file('logo')->store('avatars/customer');
                $file = $request->file('logo')->hashName();

                $image = Image::make(storage_path('app'.DIRECTORY_SEPARATOR.$path));

                if ($image->height() > 300) {
                    $image->resize(null, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                $image->save();

                $customer->logo = $file;
            } catch (\Exception $e) {
                $exception_message = $e;
            }
        }
        $customer->save();

        return $customer;
    }

    public function updateCustomer(UpdateCustomerRequest $request, $customer): Customer
    {
        return $this->createCustomer($request, $customer);
    }

    public function updateClockifyWithCustomer(ClockifyAPI $api, Customer $customer): void
    {
        if (config('app.clockify.is_enabled')) {
            try {
                $customer->refresh();
                $api->addClient(['name' => $customer->customer_name, 'note' => $customer->id]);
            } catch (\Exception $exception) {
                logger($exception->getMessage());
            }
        }
    }

    public function storeDocuments(StoreCustomerRequest $request, Customer $customer): void
    {
        if ($request->hasfile('documents')) {
            $files = $request->file('documents');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $file->move(public_path().'/files/customers/', $filename);

                $document = new Document();
                $document->type_id = 10;
                $document->name = $filename;
                $document->file = 'files/customers/'.$filename;
                $document->document_type_id = 10;
                $document->reference = 'Customer';
                $document->reference_id = $customer->id;
                $document->creator_id = auth()->id();
                $document->owner_id = auth()->id();
                $document->status_id = 1;
                $document->digisign_status_id = null;
                $document->digisign_approver_user_id = null;
                $document->save();
            }
        }
    }

    public function relationships(): array
    {
        return [
            'statusd:id,description',
            'accountm:id,first_name,last_name',
            'country:id,name',
            'billingCycle:id,name',
            'invoiceContact:id,first_name,last_name'
        ];
    }

    public function managers()
    {
        return User::selectRaw("id, CONCAT(first_name,' ', last_name) AS full_name")->where('status_id', 1)->where('expiry_date', '>=', now()->toDateString())->whereHas('roles', function ($role) {
            return $role->whereIn('id', [1, 2, 3]);
        })->pluck('full_name', 'id');
    }
}