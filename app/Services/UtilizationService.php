<?php

namespace App\Services;

use App\Http\Requests\StoreUtilizationRequest;
use App\Http\Requests\UpdateUtilizationRequest;
use App\Models\Config;
use App\Models\Module;
use App\Models\Utilization;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class UtilizationService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', 'App\Models\PlannedUtilization')->first();
    }

    public function createUtilization(StoreUtilizationRequest $request): void
    {
        $this->validateYearWkIsUnique($request);

         foreach ($this->generateYearWeekFromDateRange($request) as $year_week){
             $utilization = new Utilization();

             $this->assignValues($request, $utilization, $year_week);
         }
    }

    public function updateUtilization(UpdateUtilizationRequest $request, Utilization $utilization): Utilization
    {
        return $this->assignValues($request, $utilization, $request->yearwk);
    }

    private function assignValues(StoreUtilizationRequest|UpdateUtilizationRequest $request, Utilization $utilization, string $week): Utilization
    {
        $utilization->yearwk = $week;
        $utilization->res_no = $request->input('res_no');
        $utilization->wk_hours = $request->input('wk_hours');
        $utilization->cost_res_no = $request->input('cost_res_no');
        $utilization->cost_wk_hours = $request->input('cost_wk_hours');
        $utilization->status_id = $request->input('status_id');
        $utilization->save();

        return $utilization;
    }

    public function config(): Config|null
    {
        return Config::first([
            'utilization_method',
            'utilization_hours_per_week',
            'utilization_cost_hours_per_week'
        ]);
    }

    public function validateYearWkIsUnique(StoreUtilizationRequest $request): void
    {
        $year_weeks = $this->generateYearWeekFromDateRange($request);

        $existing_util = Utilization::whereBetween('yearwk', [Arr::first($year_weeks), Arr::last($year_weeks)])->count();

        if ($existing_util) {
            $error_messages = 'One or more of the weeks in your date range already have a utilization plan';
            throw ValidationException::withMessages([
                'start_date' => $error_messages,
                'end_date' => $error_messages
            ]);
        }
    }

    private function generateYearWeekFromDateRange(StoreUtilizationRequest $request): array
    {
        if ($request->exists(['start_date', 'end_date'])){
            $plan_range = CarbonPeriod::create($request->start_date, $request->end_date);
            return collect($plan_range)
                ->map(fn($date) => $date->format("YW"))
                ->unique()
                ->values()
                ->toArray();
        }

        return [$request->yearwk];
    }

    public function weeksDropdown(Utilization $last_wk_entry): array
    {
        if ($last_wk_entry != null) {
            $last_wk = $last_wk_entry->yearwk;
            $year = $this->year($last_wk);
        } else {
            $last_wk = now()->subWeek()->weekOfYear;
            $year = now()->year;
        }

        $week = substr($last_wk, -2);
        $date = now()->setISODate($year, $week)->addWeek()->weekOfYear;

        $next_week = $date;
        $week_drop_down = [];
        for ($i = $next_week; $i > 0; $i--) {
            $week = str_pad($i, 2, "0", STR_PAD_LEFT);
            $week_drop_down[$year.$week] = $year.$week;
        }

        $year -= 1;
        for ($i = 52; $i > 0; $i--) {
            $week = str_pad($i, 2, "0", STR_PAD_LEFT);
            $week_drop_down[$year.$week] = $year.$week;
        }

        return $week_drop_down;
    }

    private function year(string $last_year_week)
    {
        $prev_record_year = substr($last_year_week, 0, 4);

        if (Carbon::create($prev_record_year)->lastOfYear()->weekOfYear == substr($last_year_week, -2))
        {
            return now()->year;
        }

        return $prev_record_year;
    }
}