<?php

namespace App\Services;

use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\Module;
use App\Models\Team;

class TeamService extends HasPermissions
{

    function module(): Module
    {
        return Module::where('name', '=', 'App\Models\Teams')->first();
    }

    public function createTeam(StoreTeamRequest|UpdateTeamRequest $request, Team $team): Team
    {
        $team->team_name = $request->input('team_name');
        $team->team_manager = $request->input('team_manager');
        $team->status_id = $request->input('status_id');

        return $team;
    }

    public function updateTeam(UpdateTeamRequest $request, Team $team): Team
    {
        $team = $this->createTeam($request, $team);
        $team->save();

        return $team;
    }
}