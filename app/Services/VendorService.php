<?php

namespace App\Services;

use App\Http\Requests\StoreVendorRequest;
use App\Http\Requests\UpdateVendorRequest;
use App\Models\Module;
use App\Models\User;
use App\Models\Vendor;

class VendorService extends HasPermissions
{
    public function module(): Module|null
    {
        return Module::where('name', '=', 'App\Models\VendorProfile')->first();
    }

    public function createVendor(StoreVendorRequest|UpdateVendorRequest $request, Vendor $vendor): Vendor
    {
        $vendor->vendor_name = $request->input('vendor_name');
        $vendor->business_reg_no = $request->input('business_reg_no');
        $vendor->vat_no = $request->input('vat_no');
        $vendor->account_manager = $request->input('account_manager');
        $vendor->assignment_approver = $request->input('assignment_approver');
        $vendor->payment_terms = $request->input('payment_terms');
        $vendor->payment_terms_days = $request->input('payment_terms_days');
        $vendor->billing_period = $request->input('billing_period');
        $vendor->contact_firstname = $request->input('contact_firstname');
        $vendor->contact_lastname = $request->input('contact_lastname');
        $vendor->contact_birthday = $request->input('contact_birthday');
        $vendor->phone = $request->input('phone');
        $vendor->cell = $request->input('cell');
        $vendor->cell2 = $request->input('cell2');
        $vendor->fax = $request->input('fax');
        $vendor->email = $request->input('email');
        $vendor->web = $request->input('web');
        $vendor->postal_address_line1 = $request->input('postal_address_line1');
        $vendor->postal_address_line2 = $request->input('postal_address_line2');
        $vendor->postal_address_line3 = $request->input('postal_address_line3');
        $vendor->city_suburb = $request->input('city_suburb');
        $vendor->postal_zipcode = $request->input('postal_zipcode');
        $vendor->street_address_line1 = $request->input('street_address_line1');
        $vendor->street_address_line2 = $request->input('street_address_line2');
        $vendor->street_address_line3 = $request->input('street_address_line3');
        $vendor->street_city_suburb = $request->input('street_city_suburb');
        $vendor->street_zipcode = $request->input('street_zipcode');
        $vendor->state_province = $request->input('state_province');
        $vendor->country_id = $request->input('country_id');
        $vendor->bbbee_level = $request->input('bbbee_level');
        $vendor->bbbee_certificate_expiry = $request->input('bbbee_certificate_expiry');
        $vendor->bank_name = $request->input('bank_name');
        $vendor->branch_code = $request->input('branch_code');
        $vendor->bank_acc_no = $request->input('bank_acc_no');
        $vendor->status_id = $request->input('status_id');
        $vendor->account_name = $request->input('account_name');
        $vendor->branch_name = $request->input('branch_name');
        $vendor->bank_account_type_id = $request->input('bank_account_type_id');
        $vendor->swift_code = $request->input('swift_code');
        $vendor->invoice_contact_id = $request->input('invoice_contact_id');
        $vendor->creator_id = auth()->id();
        $vendor->onboarding = $request->input('onboarding');
        $vendor->supply_chain_appointment_manager_id = $request->input('supply_chain_appointment_manager_id');
        $vendor->service_agreement_id = $request->input('service_agreement_id');
        $vendor->nda_template_id = $request->input('nda_template_id');

        if ($request->hasFile('avatar')) {
            try {
                $vendor->logo(
                    request: $request,
                    field: 'avatar',
                    type: 'vendor',
                    height: 200
                );

                $vendor->vendor_logo = $request->file('avatar')->hashName();
            } catch (\Exception $e) {
                logger($e->getMessage());
            }
        }
        $vendor->save();

        return $vendor;
    }

    public function updateVendor(UpdateVendorRequest $request, Vendor $vendor): Vendor
    {
        return $this->createVendor($request, $vendor);
    }

    public function assignVendorToUser(int $user_id, int $vendor_id): void
    {
        $user = User::find($user_id);
        $user->vendor_id = $vendor_id;
        $user->save();
    }

    public function managers()
    {
        return User::selectRaw("id, CONCAT(first_name,' ', last_name) AS full_name")->where('status_id', 1)->where('expiry_date', '>=', now()->toDateString())->whereHas('roles', function ($role) {
            return $role->whereIn('id', [1, 2, 3]);
        })->pluck('full_name', 'id');
    }
}