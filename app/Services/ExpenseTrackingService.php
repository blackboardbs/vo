<?php

namespace App\Services;

use App\Models\Module;

class ExpenseTrackingService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', '=', 'App\Models\ExpenseTracking')->first();
    }
}