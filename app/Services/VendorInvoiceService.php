<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Config;
use App\Models\Module;
use App\Models\VatRate;
use App\Models\Vendor;
use App\Services\HasPermissions;

class VendorInvoiceService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\VendorInvoice")->first();
    }

    public function vatRate(Vendor $vendor): int
    {
        $config = Config::with('company:id,vat_no')->first(['company_id', 'vat_rate_id']);
        $vat_rate = VatRate::query();

        if ($vendor->vat_no && isset($config->vat_rate_id)) {
            return $vat_rate->where('vat_code', $config->vat_rate_id)
                ->where('end_date', '>=', now()->toDateString())
                ->first(['id'])?->id;
        }elseif ($vendor->vat_no && !isset($config->vat_rate_id)) {
            return $vat_rate->where('description', 'Standard')
                ->where('end_date', '>=', now()->toDateString())
                ->first(['id'])?->id;
        }
        return $vat_rate->where('description', 'Zero Rate')->first(['id'])?->id;
    }
}