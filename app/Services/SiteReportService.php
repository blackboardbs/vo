<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\CustomerSiteReport;
use App\Models\CustomerSiteReportActual;
use App\Models\CustomerSiteReportPlanned;
use App\Models\CustomerSiteReportHoursSummary;
use App\Models\CustomerSiteReportRisks;
use App\Models\Module;
use App\Services\HasPermissions;

class SiteReportService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\SiteReport")->first();
    }

    public function createSiteReport(Request $request, CustomerSiteReport $site_report)
    {
        $site_report->template_id = $request->input('template_id')??null;
        $site_report->ref = $this->refNumberString()??null;
        $site_report->customer_id = $request->input('customer_id')??null;
        $site_report->project_id = $request->input('project_id')??null;
        $site_report->project_name = $request->input('project_name')??null;
        $site_report->employee_id = $request->input('employee_id')??null;
        $site_report->consultant_name = $request->input('consultant_name')??null;
        $site_report->review_week_from = substr($request->input('review_date_from'),0,6)??null;
        $site_report->review_week_to = substr($request->input('review_date_to'),0,6)??null;
        $site_report->review_date_from = substr($request->input('review_date_from'),8,18)??null;
        $site_report->review_date_to = substr($request->input('review_date_to'),8,18)??null;
        $site_report->plan_week_from = substr($request->input('plan_date_from'),0,6)??null;
        $site_report->plan_week_to = substr($request->input('plan_date_to'),0,6)??null;
        $site_report->plan_date_from = substr($request->input('plan_date_from'),8,18)??null;
        $site_report->plan_date_to = substr($request->input('plan_date_to'),8,18)??null;
        $site_report->include_consultant = $request->input('include_consultants')??0;
        $site_report->include_task_hours = $request->input('include_task_hours')??0;
        $site_report->include_summary_hours = $request->input('include_hours_summary')??0;
        $site_report->include_consultant_name = $request->input('include_consultant_name')??0;
        $site_report->include_report_to = $request->input('include_report_to')??0;
        $site_report->report_to = $request->input('report_to')??null;
        $site_report->include_risks = $request->input('include_risks')??0;
        $site_report->include_company_logo = $request->input('include_company_logo')??0;
        $site_report->include_customer_logo = $request->input('include_customer_logo')??0;

        $report_to = Assignment::with('resource.resource.manager')->where('project_id',$request->input('project_id'))->first();
        $site_report->assigned_to_name = $report_to->report_to && $report_to->report_to != '' ? $report_to->report_to : ($report_to->resource->resource->manager ? $report_to->resource->resource->manager->first_name.' '.$report_to->resource->resource->manager->last_name : '');

        $site_report->site_report_title = $request->input('site_report_title')??'';
        $site_report->site_report_footer = $request->input('site_report_footer')??'';
        $site_report->save();

        $old_activities = CustomerSiteReportActual::where('site_report_id',$site_report->id)->delete();

        if($request->has('activities') && count($request->activities) > 0){
            foreach($request->activities as $activity){
                $actual = new CustomerSiteReportActual();
                $actual->site_report_id = $site_report->id;
                $actual->employee_name = $activity['employee_name']??null;
                $actual->task_status = $activity['task_status']??null;
                $actual->task = $activity['task'];
                $actual->total_hours = $activity['total_hours'];
                $actual->hours_billable = $activity['hours_billable'];
                $actual->hours_non_billable = $activity['hours_non_billable'];
                $actual->notes = $activity['notes']??null;
                $actual->save();
            }
        }
        
        $old_planned = CustomerSiteReportPlanned::where('site_report_id',$site_report->id)->delete();

        if($request->has('planned_activities') && count($request->planned_activities) > 0){
            foreach($request->planned_activities as $plan){
                $planned = new CustomerSiteReportPlanned();
                $planned->site_report_id = $site_report->id;
                $planned->employee_name = $plan['employee_name']??null;
                $planned->task_status = $plan['task_status']??null;
                $planned->task = $plan['task'];
                $planned->actual_hours = $plan['actual_hours'];
                $planned->hours_available = $plan['hours_available'];
                $planned->hours_planned = $plan['hours_planned'];
                $planned->notes = $plan['notes']??null;
                $planned->save();
            }
        }

        $old_hours = CustomerSiteReportHoursSummary::where('site_report_id',$site_report->id)->delete();

        if($request->has('hours_summary') && count($request->hours_summary) > 0){
            foreach($request->hours_summary as $sum){
                $summary = new CustomerSiteReportHoursSummary();
                $summary->site_report_id = $site_report->id;
                $summary->as_at = $sum['as_at'];
                $summary->po_billed_hours = $sum['po_billed_hours'];
                $summary->po_hours_budgeted  = $sum['po_hours_budgeted'];
                $summary->po_hours_remaining = $sum['po_hours_remaining'];
                $summary->po_hours_unbilled = $sum['po_hours_unbilled'];
                $summary->po_hours_unbilled_last = $sum['po_hours_unbilled_last'];
                $summary->po_hours_unbilled_previous = $sum['po_hours_unbilled_previous'];
                $summary->save();
            }
        }

        $old_risks = CustomerSiteReportRisks::where('site_report_id',$site_report->id)->delete();

        if($request->has('risks') && count($request->risks) > 0){
            foreach($request->risks as $ris){
                $risk = new CustomerSiteReportRisks();
                $risk->site_report_id = $site_report->id;
                $risk->name = $ris['name'];
                $risk->likelihood = $ris['likelihood'];
                $risk->impact = $ris['impact'];
                $risk->save();
            }
        }

        return $site_report;
    }

    public function updateSiteReport(Request $request, CustomerSiteReport $site_report)
    {
        return $this->createSiteReport($request, $site_report);
    }
    
    public function getRef(){
        $count = CustomerSiteReport::count();
        $cnt = $count + 1;
        $ref = '';
        if($cnt <= 10){
            $ref = 'SR0000'.$cnt;
        }
        if($cnt >= 10){
            $ref = 'SR000'.$cnt;
        }
        if($cnt >= 100){
            $ref = 'SR00'.$cnt;
        }
        if($cnt >= 1000){
            $ref = 'SR0'.$cnt;
        }
        if($cnt >= 10000){
            $ref = 'SR'.$cnt;
        }

        return $ref;
    }

    public function extractIntFromString(string|int $prefix, int|string $invoice_number): int
    {
        $remove_prefix = substr($invoice_number, strlen($prefix));
        return intval(ltrim($remove_prefix, '0'));
    }

    private function newRefNumberInt(): int
    {
            $last_site_report = CustomerSiteReport::select(['ref'])->whereNotNull('ref')->latest()->first();

            if (isset($last_site_report->ref)) {
                return  $this->extractIntFromString('SR', $last_site_report->ref) + 1;
            }

            return 10000;
    }

    public function refNumberString(): string
    {
        $startNumber = 10000;
        return $this->formatInvoiceNumber('SR', $this->newRefNumberInt())
            ?? ('SR'.$startNumber??10000);
    }

    public function formatInvoiceNumber(string $prefix, int $num): string|int
    {
        if(!isset($prefix)){
            return $num;
        }

        $formattedNumber = str_pad($num, 6, '0', STR_PAD_LEFT);

        return $prefix.$formattedNumber;
    }
}