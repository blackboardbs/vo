<?php

namespace App\Services;

use App\Http\Requests\ContactRequest;
use App\Models\InvoiceContact;
use App\Models\Module;

class ContactService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\InvoiceContact")->first();
    }

    public function createContact(ContactRequest $request, InvoiceContact $contact): InvoiceContact
    {
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->contact_number = $request->contact_number;
        $contact->birthday = $request->birthday;
        $contact->status_id = $request->status_id;
        $contact->save();

        return $contact;
    }

    public function updateContact(ContactRequest $request, InvoiceContact $contact): InvoiceContact
    {
        return $this->createContact($request, $contact);
    }
}