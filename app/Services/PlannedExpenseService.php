<?php

namespace App\Services;

use App\Http\Requests\PlannedExpenseCopyRequest;
use App\Http\Requests\StorePlannedExpenseRequest;
use App\Http\Requests\UpdatePlannedExpenseRequest;
use App\Models\Module;
use App\Models\PlannedExpense;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;

class PlannedExpenseService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', '=', "App\\Models\\PlannedExpense")->first();
    }

    public function createPlannedExpense(StorePlannedExpenseRequest|UpdatePlannedExpenseRequest $request, PlannedExpense $expense): PlannedExpense
    {
        $expense->description = $request->input('description');
        $expense->plan_exp_value = $request->input('plan_exp_value');
        $expense->plan_exp_date = $request->input('plan_exp_date');
        $expense->status = $request->input('status');
        $expense->account_id = $request->input('account_id');
        $expense->account_element_id = $request->input('account_element_id');
        $expense->company_id = $request->input('company_id');
        $expense->save();

        return $expense;
    }

    public function updatePlannedExpense(UpdatePlannedExpenseRequest $request, PlannedExpense $expense): PlannedExpense
    {
        return $this->createPlannedExpense($request, $expense);
    }

    public function copyPlannedExpenses(PlannedExpenseCopyRequest $request): void
    {
        PlannedExpense::whereYear('plan_exp_date', '=', (explode('-',$request->start_date)[0]??null))
            ->whereMonth('plan_exp_date', '=', (explode('-',$request->start_date)[1]??null))
            ->get()->each(function($expense) use($request){
                $new_expense = $expense->replicate();
                $new_expense->plan_exp_date = $this->generateNewTargetDate($request->end_date, $expense->plan_exp_date);
                $new_expense->save();
            });
    }

    private function generateNewTargetDate(string $target_period, string $old_date): string
    {
        $period = explode('-',$target_period);
        $create_date = Carbon::parse(($period[0]??now()->year).'-'.($period[1]??now()->month).'-01');
        $prev_date = Carbon::parse($old_date);

        if ($prev_date->day > $create_date->copy()->lastOfMonth()->day){
            $transaction_date = $create_date->lastOfMonth()->toDateString();
        }else{
            $create_date->day = $prev_date->day;
            $transaction_date = $create_date->toDateString();
        }

        return $transaction_date;
    }

    public function yearMonthDropdown(int $sub, int $add = 0): Collection
    {
        $dates_range = CarbonPeriod::create(now()->subYears($sub)->toDateString(), now()->addYears($add)->toDateString());
        $range = [];

        foreach ($dates_range as $date){
            $range[] = [
                'id' => $date->format('Y-m'),
                'name' => $date->format('M Y')
            ];
        }
        return collect($range)->reverse()->pluck('name', 'id');
    }
}