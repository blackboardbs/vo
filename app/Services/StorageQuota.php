<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class StorageQuota
{
    public function isStorageQuotaReached(float|int $file_size = 0): bool
    {
        return ($this->tenantStorageSize() + $file_size) > $this->addStorageBuffer();
    }

    public function storageUsage(): float
    {
        $usage = 0;

        try {
            $usage = ($this->tenantStorageSize() / $this->packageStorageSize()) * 100;
        }catch (\DivisionByZeroError $error){
            logger($error->getMessage());
        }

        return round($usage, 2);
    }

    public function isQuotaPercentage(float|int $percentage = 0.80): bool
    {
        $storage_percent = $this->packageStorageSize() * $percentage;

        return $this->tenantStorageSize() >= $storage_percent;
    }

    private function tenantStorageSize(): int|float
    {
        $files_collection = collect(Storage::allFiles())->chunk(100);

        $file_sizes = $files_collection->map(fn($chunk) => $chunk->map(fn($file) => Storage::size($file)));

        return $file_sizes->flatten()->sum();
    }

    private function packageStorageSize(): int|float
    {
            $package = [];

            $package['TRIAL'] = ['users'=>10,'non-users'=>250,'amount'=>0, 'storage' => 5];
            $package['CLASSIC'] = ['users'=>10,'non-users'=>250,'amount'=>1900, 'storage' => 5];
            $package['STANDARD'] = ['users'=>30,'non-users'=>750,'amount'=>4500, 'storage' => 20];
            $package['PREMIUM'] = ['users'=>9999,'non-users'=>9999,'amount'=>9999, 'storage' => 100];
            
            $owner = User::first();
                    try {
                        $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);
                        if ($response->status() != 404){
                            $license = $response->json();
                        }

                    } catch (\Exception $e) {
                        logger($e);
                    }

            $license = $response->json()??[];

            // Get the package storage size which is in GB and convert it to bytes
            if (isset($license["subscription"]["subscription_tier"])){
                return ($package[$license["subscription"]["subscription_tier"]]['storage']??0) * 1073741824;
            }
        return 0;
    }

    private function addStorageBuffer(float $buffer = 1.05): float|int
    {
        return $this->packageStorageSize() * $buffer;
    }
}