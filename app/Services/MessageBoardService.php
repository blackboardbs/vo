<?php

namespace App\Services;

use App\Http\Requests\MessageBoardRequest;
use App\Models\MessageBoard;
use App\Models\Module;

class MessageBoardService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', 'App\Models\MessageBoard')->first();
    }

    public function createMessage(MessageBoardRequest $request, MessageBoard $message): MessageBoard
    {
        $message->message = $request->message;
        $message->message_date = $request->message_date;
        $message->status_id = $request->status_id;
        $message->save();

        return $message;
    }

    public function updateMessage(MessageBoardRequest $request, MessageBoard $messgae): MessageBoard
    {
        return $this->createMessage($request, $messgae);
    }
}