<?php

namespace App\Services;

use App\Http\Requests\UpdateResourceRequest;
use App\Models\Module;
use App\Models\Resource;
use App\Services\HasPermissions;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;

class ResourceService extends HasPermissions
{
    function module(): Module
    {
        return Module::where('name', '=', \App\Models\Resource::class)->first();
    }

    public function createResource(UpdateResourceRequest $request, Resource $resource): Resource
    {
        $resource->resource_no = $request->input('resource_number');
        $resource->resource_position = $request->input('position');
        $resource->resource_level = $request->input('resource_level');
        $resource->commission_id = $request->input('commission');
        $resource->cost_center_id = $request->input('cost_center');
        $resource->technical_rating_id = $request->input('technical_rating_id');
        $resource->business_skill_id = $request->input('business_skill_id');
        $resource->manager_id = $request->input('report_to');
        $resource->home_phone = $request->input('home_phone');
        $resource->status_id = $request->input('status');
        $resource->nda_template_id = $request->input('nda_template_id');
        $resource->resource_type = $request->input('resource_type');
        $resource->join_date = $request->input('join_date');
        $resource->termination_date = $request->input('termination_date');
        $resource->project_type = $request->input('project_type');
        if ($request->has('util')) {
            $resource->util = '1';
        } else {
            $resource->util = '0';
        }
        $resource->util_date_from = $request->input('util_date_from');
        $resource->util_date_to = $request->input('util_date_to');
        $resource->medical_certificate_expiry = $request->input('medical_certificate_expiry');
        $resource->medical_certificate_type = $request->input('medical_certificate_type');
        $resource->creator_id = auth()->user()->getAuthIdentifier('id');
        $resource->team_id = $request->input('team');
        $resource->standard_cost_bracket = $request->input('standard_cost_bracket');
        $resource->save();

        return $resource;
    }

    public function updateResource(UpdateResourceRequest $request, Resource $resource): Resource
    {
        return $this->createResource($request, $resource);
    }

    public function response(int|null $leave_balance): RedirectResponse
    {
        if ($leave_balance) {
            return redirect()->route('leave_balance.create')->with('flash_success', 'Resource updated successfully.');
        }

        return redirect()->route('resource.index')->with('flash_success', 'Resource updated successfully.');
    }

    public function managersDropdown(): Collection
    {
        return Resource::with(['manager:id,first_name,last_name'])
            ->selectRaw('DISTINCT manager_id')
            ->where('manager_id', '>', 0)
            ->whereNotNull('manager_id')
            ->get()
            ->map(fn($resource) => [
                'id' => $resource->manager?->id,
                'full_name' => $resource->manager?->first_name." ".$resource->manager?->last_name
            ])->filter()
            ->values()
            ->pluck('full_name', 'id');
    }

    public function relationships(): array
    {
        return [
            'user:id,first_name,last_name,avatar,email,phone',
            'cv:id,resource_pref_name,resource_middle_name,cell,cell2,date_of_birth',
            'resource_position_desc:id,description',
            'resource_type_desc:id,description',
            'resource_level_desc:id,description',
            'commission:id,description',
            'cost_center:id,description',
            'standard_cost:id,description',
            'technical_rating:id,name',
            'business_skill:id,name',
            'medical_cert:id,description',
            'projecttype:id,description',
            'manager:id,first_name,last_name',
            'status:id,description',
            'team:id,team_name'
        ];
    }
}