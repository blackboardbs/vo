<?php

namespace App\Services;

use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Company;
use App\Models\Config;
use App\Models\Module;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class CompanyService extends HasPermissions
{
    public function __construct(private string $feedback_message = ""){}

    function module(): Module
    {
        return Module::where('name', '=', 'App\Models\CompanyProfile')->first();
    }

    public function createCompany(StoreCompanyRequest|UpdateCompanyRequest $request, Company $company, StorageQuota $quota): Company
    {
        $company->company_name = $request->input('company_name');
        $company->business_reg_no = $request->input('registration_number');
        $company->vat_no = $request->input('vat_number');
        $company->contact_firstname = $request->input('contact_firstname');
        $company->contact_lastname = $request->input('contact_lastname');
        $company->phone = $request->input('phone');
        $company->cell = $request->input('cell');
        $company->fax = $request->input('fax');
        $company->email = $request->input('email');
        $company->web = $request->input('web');
        $company->postal_address_line1 = $request->input('address_ln1');
        $company->postal_address_line2 = $request->input('address_ln2');
        $company->postal_address_line3 = $request->input('address_ln3');
        $company->city_suburb = $request->input('suburb');
        $company->state_province = $request->input('state_province');
        $company->postal_zipcode = $request->input('postal_zip');
        $company->country_id = $request->input('country_id');
        $company->bank_name = $request->input('bank_name');
        $company->branch_code = $request->input('branch_code');
        $company->bank_acc_no = $request->input('bank_acc_no');
        $company->bbbee_level_id = $request->input('bbbee_level_id');
        $company->bbbee_certificate_expiry = $request->input('bbbee_certificate_expiry');
        $company->black_ownership = $request->input('black_ownership');
        $company->black_female_ownership = $request->input('black_female_ownership');
        $company->black_voting_rights = $request->input('black_voting_rights');
        $company->black_female_voting_rights = $request->input('black_female_voting_rights');
        $company->bbbee_recognition_level = $request->input('bbbee_recognition_level');
        $company->status_id = $request->input('status_id');
        $company->creator_id = auth()->id();
        $company->account_name = $request->input('account_name');
        $company->branch_name = $request->input('branch_name');
        $company->bank_account_type_id = $request->input('bank_account_type_id');
        $company->swift_code = $request->input('swift_code');
        $company->invoice_text = $request->input('invoice_text');
        if ($request->hasFile('avatar') && !$quota->isStorageQuotaReached($request->file('avatar')->getSize())) {
            $company->company_logo = $this->uploadCompanyLogo($request, $company) ? $request->file('avatar')->hashName() : null;
        }
        $company->save();

        return $company;
    }

    public function updateCompany(UpdateCompanyRequest $request, Company $company, StorageQuota $quota): Company
    {
        if ($request->hasFile('avatar')){
            $this->deleteLogo($company->company_logo);
        }
        return $this->createCompany($request, $company, $quota);
    }

    public function companyResponse(string $action): RedirectResponse
    {
        $response = redirect(route('company.index'))->with('flash_success', $action);

        if ($this->feedback_message){
            return $response->with('flash_warning', $this->feedback_message);
        }

        return $response;
    }

    public function uploadCompanyLogo(StoreCompanyRequest|UpdateCompanyRequest $request, Company $company): bool
    {
        try {
            $company->logo(
                request: $request,
                field: 'avatar',
                type: 'company',
                height: 200
            );
            $success = true;
        } catch (\Exception $e) {
            logger($e->getMessage());
            $this->feedback_message = "Logo was not successfully uploaded.";
            $success = false;
        }

        return $success;
    }

    public function deleteLogo(string|null $logo): void
    {
        Storage::delete('avatars/company/'.$logo);
    }

    public function email(): string
    {
        $config = Config::with(['company:id,email'])->first();

        return $config->company->email??Company::first()?->email;
    }
}