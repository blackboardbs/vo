<?php

namespace App\Services;

use App\Http\Requests\StoreAssetRegisterRequest;
use App\Jobs\SendAssetRegisterEmailJob;
use App\Models\AssetRegister;
use App\Models\Company;
use App\Models\Config;
use App\Models\Module;
use App\Models\User;
use App\Services\HasPermissions;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class EquipmentService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', '=', 'App\Models\Equipment')->first();
    }

    public function storeInsurance(StoreAssetRegisterRequest $request, AssetRegister $assetRegister): void
    {
        if ($request->insurer){
            $assetRegister->insurance()
                ->create($request->only(['insurer', 'insured_value', 'covered_at', 'excess_amount', 'note', 'policy_ref']));
        }
    }

    public function storeAssetHistory(StoreAssetRegisterRequest $request, AssetRegister $referrer): void
    {
        $referrer->assetHistory()->create([
            'user_id' => $request->user_id,
            'issued_at' => $request->issued_at,
            'is_included_in_letter' => $request->is_included_in_letter,
            'note' => $request->history_note,
            'is_active' => 1
        ]);
    }

    public function storeAdditionalCost(StoreAssetRegisterRequest $request, AssetRegister $referrer): void
    {
        if($request->cost_issued_at){
            $referrer->additionalCost()->create([
                'date' => $request->cost_issued_at,
                'cost' => ($request->cost * 100),
                'warranty' => $request->cost_warranty,
                'insurance' => $request->cost_insurance,
                'notes' => $request->cost_note,
                'support_ref' => $request->support_ref
            ]);
        }
    }

    public function notifyUser(AssetRegister $referrer): void
    {
        $resource = $referrer->assetHistoryActive?->user;

        if (isset($resource)){
            $company = Company::find(Config::first(['company_id'])?->company_id);
            SendAssetRegisterEmailJob::dispatch($resource->email, $referrer, $resource, $company);
        }
    }

    public function claimApprover(): User
    {
        $config = Config::first(['claim_approver_id']);

        return User::findOrNew($config->claim_approver_id);
    }

    public function remainingMonth(array $dates): int
    {
        if (isset($dates['written_off_at']) || isset($dates['sold_at'])){
            return 0;
        }elseif (isset($dates['retire_date'])){
            return now()->diffInMonths($dates['retire_date']);
        }

        return 0;
    }

    public function currentBookValue(array $equipment): float
    {
        if (isset($equipment['written_off_at']) || isset($equipment['sold_at']))
        {
            return 0.0;
        }

       //dd("original value: ".$equipment['original_value'].", remaining months: ".$this->remainingMonth($equipment).", estimated months: ".$equipment['est_life']);

        try {
            $book_value = (($equipment['original_value'] * $this->remainingMonth($equipment))/$equipment['est_life']) + $equipment['cost_of_repairs'];
            return $book_value;
        }catch (\DivisionByZeroError  $exception){
            return 0.0;
        }
    }

    public function validating(Request $request): Validator
    {
        return \Illuminate\Support\Facades\Validator::make($request->all(), [
            'date' => 'required|date',
            'cost' => 'required|numeric',
            'warranty' => 'nullable|string:max:190',
            'insurance' => 'nullable|string:max:190',
            'support_ref' => 'nullable|string:max:190',
            'notes' => 'nullable|string',
        ]);
    }
}