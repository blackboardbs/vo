<?php

namespace App\Services;

use App\Models\Module;
use App\Services\HasPermissions;
use Illuminate\Http\Request;
use App\Models\Config;
use App\Models\Leave;
use App\Models\LeaveBalance;
use App\Models\Resource;
use Carbon\Carbon;

class LeaveService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\Leave")->first();
    }

    public function calculateAnnualLeave(Request $request,Config $config,Resource $resource, $leave_balance,$all = false){
        $accrue = true;
        $pay_out = false;
        if ($request->has('date') && $request->date) {
            $date_to = Carbon::parse($request->date)->lastOfMonth()->toDateString();
        } else if ($request->has('leave') && $request->leave && !$request->has('date')) {
            $date_to = Carbon::createFromDate(substr($request->leave, 0, 4), substr($request->leave, 4))->lastOfMonth()->toDateString();
            // If termination date exists and is earlier than the calculated date_to, use it
            if ($resource->termination_date && $resource->termination_date != '' && Carbon::parse($date_to)->gte(Carbon::parse($resource->termination_date))) {
                if (Carbon::parse($resource->termination_date)->lastOfMonth()->isSameDay($date_to)) {
                    $accrue = true;
                } else {
                $accrue = false;
                }
                $termination_date = Carbon::parse($resource->termination_date)->lastOfMonth()->toDateString();
                if (Carbon::parse($termination_date)->lessThan($date_to)) {
                    $date_to = $termination_date;
                }
                if($leave_balance->pay_out && $leave_balance->pay_out == 1){
                    $pay_out = true;
                }
            }
        } else {
            $date_to = now()->lastOfMonth()->toDateString();
        }
        

        $employee_dates = $this->calculateEmployeeDates($resource->join_date??$resource->created_at,$date_to);
        
        $total_leave_applied_for = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 1)->whereBetween('date_from', [$employee_dates['start_date'], $date_to])
        ->whereNull('leave_status')->sum('no_of_days');
        
        $leave_taken = Leave::where('emp_id', $resource->user_id)
                            ->where('leave_type_id', 1)
                            ->when(!$all, function ($query) {
                                return $query->where('leave_status', 1);
                            });
        
        $previous_month = Carbon::parse($date_to)->subMonthsNoOverflow(1);

        if ($leave_balance) {

            $leave = $this->getLeaveTaken($request, $resource);
            
            $lt = Leave::where('emp_id', $resource->user_id)
                        ->where('leave_type_id', 1)
                        ->whereBetween('date_from', [$leave_balance->balance_date, $previous_month])
                        ->when(!$all, function ($query) {
                            return $query->where('leave_status', 1);
                        })
                        ->latest()
                        ->sum('no_of_days');
                
            $base_date = Carbon::parse($leave_balance->balance_date);

            $diff_months = $base_date->diffInMonths($date_to);
            
            $opening_balance = ($leave_balance->opening_balance - $lt) + ($diff_months * ($leave_balance->leave_per_cycle / 12));
            // dd($accrue);
            $days_accrued = !$accrue ? 0 : $leave_balance->leave_per_cycle / 12;
            
            $closing_balance = ($opening_balance + $days_accrued) - $leave;
            
        } else {

            $opening_date = Carbon::parse($resource->join_date)->diffInYears($date_to) > 1 ? now()->subYear() : Carbon::parse($resource->join_date);

            $leave = $leave_taken->whereBetween('date_from', [$opening_date->toDateString(), $date_to])
                ->latest() 
                ->sum('no_of_days');

            $diff_months = $opening_date->addMonth()->diffInMonths($date_to);

            $opening_balance = ($diff_months * ($config->annual_leave_days / 12)) - (($leave) ? $leave : 0);

            $days_accrued = Carbon::parse($date_to)->gte($resource->termination_date) ? 0 : ($config->annual_leave_days / 12);

            $closing_balance = $opening_balance + $days_accrued;
            
            if (! $config->annual_leave_days) {

                $opening_balance = 0;

                $closing_balance = 0;

                $days_accrued = 0;

            }
        }

        if ($request->has('leave') && $request->leave) {
            $leave_taken = $leave_taken->whereYear('date_from', substr($request->leave, 0, 4))
                ->whereMonth('date_from', substr($request->leave, 4));
        } else {
            $leave_taken = $leave_taken->whereYear('date_from', now()->year)
                ->whereMonth('date_from', (now()->month < 10) ? '0'.now()->month : now()->month);
        }

        $leave_taken = $leave_taken->get();

        return [
            'total_leave_applied_for'=>$total_leave_applied_for,
            'leave_taken' => $leave_taken->sum('no_of_days')??0,
            'opening_balance'=>$opening_balance,
            'closing_balance'=>$closing_balance,
            'days_accrued'=>$days_accrued,
            'pay_out' => $pay_out ? ['pay_out_date'=>$leave_balance->pay_out_date,'pay_out_leave'=>$leave_balance->pay_out_balance] : []
        ];
    }

    public function calculateSickLeave(Request $request,Config $config,Resource $resource){
        if ($request->has('leave') && $request->leave) {
            $date_to = Carbon::createFromDate(substr($request->leave, 0, 4), substr($request->leave, 4))->lastOfMonth()->toDateString();
            // dd($date_to);
        } else {
            $date_to = now()->lastOfMonth()->toDateString();
        }
        $leave_taken = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 2)->where('leave_status',1);


            $opening_date = Carbon::parse($resource->join_date == '' ? substr($resource->created_at,0,10) : $resource->join_date);

            $year = $opening_date->year;
            $month = $opening_date->month;
            $new_year = $year;
            
            $new_cycle = true;
            $new_opening_date = $year;
            $year_array = [];

            $cycle = $config->sick_leave_cycle_months/12;
// dd($cycle);
            if($opening_date){
                for($i = $year;$i<=now()->year;$i+=$cycle){
                    array_push($year_array,$i);
                }
            }

            $cycle_start = $year_array[array_key_last($year_array)] == now()->year && $opening_date->month > now()->month ? Carbon::parse($year_array[array_key_last($year_array)-1].'-'.$opening_date->month.'-'.$opening_date->day)->format("Y-m-d") : Carbon::parse($year_array[array_key_last($year_array)].'-'.$opening_date->month.'-'.$opening_date->day)->format("Y-m-d");
            $cycle_end = Carbon::parse($cycle_start)->addMonths($config->sick_leave_cycle_months)->format("Y-m-d");

            // dd(["start"=>$cycle_start,"end"=>$cycle_end ]);

            $leave = $leave_taken->whereBetween('date_from', [Carbon::parse($date_to)->startOfMonth(), $date_to])
                ->get()
                ->sum('no_of_days');

                 $total_leave = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 2)->whereBetween('date_from', [$cycle_start, $cycle_end])
                 ->get()
                 ->sum('no_of_days');

                //  dd($total_leave);

        // if ($request->has('leave') && $request->leave) {
        //     $leave_taken = $leave_taken->whereYear('date_from', substr($request->leave, 0, 4))
        //         ->whereMonth('date_from', substr($request->leave, 4));
        // } else {
        //     $leave_taken = $leave_taken->whereYear('date_from', now()->year)
        //         ->whereMonth('date_from', (now()->month < 10) ? '0'.now()->month : now()->month);
        // }
        $leave_taken = $leave_taken->first();

        $cycle_days = $date_to < $cycle_start ? 0 : $config->sick_leave_days_per_cycle;

        return ['cycle_days' => $cycle_days ,'leave_taken' => $leave??0,'total_leave'=>$total_leave,'sick_cycle_start'=>$cycle_start,'sick_cycle_end'=>$cycle_end];
    }

    public function calculateUnpaidLeave(Request $request,Config $config,Resource $resource){
        if ($request->has('leave') && $request->leave) {
            $date_to = Carbon::createFromDate(substr($request->leave, 0, 4), substr($request->leave, 4))->lastOfMonth()->toDateString();
        } else {
            $date_to = now()->lastOfMonth()->toDateString();
        }
        $leave_taken = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 4)->where('leave_status',1);


            $opening_date = Carbon::parse($resource->join_date == '' ? substr($resource->created_at,0,10) : $resource->join_date);

            $year = $opening_date->year;
            $month = $opening_date->month;
            $new_year = $year;
            
            $new_cycle = true;
            $new_opening_date = $year;

            $cycle_start = Carbon::parse((now()->month < $month ? now()->year-1 : now()->year).'-'.$opening_date->month.'-'.$opening_date->day)->format("Y-m-d");
            $cycle_end = Carbon::parse($cycle_start)->addYears(1)->format("Y-m-d");

            $leave = $leave_taken->whereBetween('date_from', [Carbon::parse($date_to)->firstOfMonth(), $date_to])
                ->get()
                ->sum('no_of_days');

        $leave_taken = $leave_taken->first();

        return ['leave_taken' => $leave??0,'sick_cycle_start'=>$cycle_start,'sick_cycle_end'=>$cycle_end];
    }

    public function calculateOtherLeave(Request $request,Config $config,Resource $resource){
        if ($request->has('leave') && $request->leave) {
            $date_to = Carbon::createFromDate(substr($request->leave, 0, 4), substr($request->leave, 4))->lastOfMonth()->toDateString();
        } else {
            $date_to = now()->lastOfMonth()->toDateString();
        }
        $leave_taken = Leave::where('emp_id', $resource->user_id)->whereNotIn('leave_type_id', [1,2,4])->where('leave_status',1);


            $opening_date = Carbon::parse($resource->join_date == '' ? substr($resource->created_at,0,10) : $resource->join_date);

            $year = $opening_date->year;
            $month = $opening_date->month;
            $new_year = $year;
            
            $new_cycle = true;
            $new_opening_date = $year;

            $cycle_start = Carbon::parse((now()->month < $month ? now()->year-1 : now()->year).'-'.$opening_date->month.'-'.$opening_date->day)->format("Y-m-d");
            $cycle_end = Carbon::parse($cycle_start)->addYears(1)->format("Y-m-d");

            $leave = $leave_taken->whereBetween('date_from', [Carbon::parse($date_to)->firstOfMonth(), $date_to])
                ->get()
                ->sum('no_of_days');

        $leave_taken = $leave_taken->first();

        return ['leave_taken' => $leave??0,'sick_cycle_start'=>$cycle_start,'sick_cycle_end'=>$cycle_end];
    }

    public function getLeaveTaken(Request $request,$resource){
        $leave = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 1)->where('leave_status',1);

        if ($request->has('leave') && $request->leave) {
            $leave = $leave->whereYear('date_from', substr($request->leave, 0, 4))
                ->whereMonth('date_from', substr($request->leave, 4));
        } else {
            $leave = $leave->whereYear('date_from', now()->year)
                ->whereMonth('date_from', (now()->month < 10) ? '0'.now()->month : now()->month);
        }

        return $leave->sum('no_of_days');
    }

    public function calculateEmployeeDates(String $startDate1,String $currentDate):array {
        
        // Set the employee's start date
        // dd($startDate1);
        $startDate = Carbon::createFromFormat('Y-m-d', Carbon::parse($startDate1)->format('Y-m-d'));

        // Get the current year
        $currentYear = Carbon::parse($currentDate)->year;

        // Set the start date for the current year
        $startDateCurrentYear = Carbon::create($currentYear, 1, 1, 0, 0, 0);

        // If the start date is in a later year, adjust to the current year
        if ($startDate->year > $currentYear) {
            $startDate = $startDateCurrentYear;
        } elseif ($startDate->year < $currentYear) {
            $startDate = Carbon::create($currentYear, $startDate->month, $startDate->day, 0, 0, 0);
        }

        // Set the end date for the current year
        $endDateCurrentYear = Carbon::create($currentYear, 12, 31, 23, 59, 59);

        return array(
            'start_date' => $startDate->gt($currentDate) ? $startDate->copy()->subYear(1) : $startDate,
            'end_date' => $startDate->gt($currentDate) ? $startDate->subDay(1) : $startDate->addYear(1)->subDay(1)
        );
    }
}