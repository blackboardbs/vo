<?php

namespace App\Services;

use App\Models\Module;
use Illuminate\Support\Facades\Auth;

abstract class HasPermissions
{
    abstract function module(): Module|null;

    public function hasListAllPermissions(): bool
    {
        if (!$this->module()){
            return false;
        }

        return (Auth::user()->canAccess($this->module()?->id, 'view_all')
            || Auth::user()->canAccess($this->module()?->id, 'view_team'));
    }

    public function hasViewTeamPermission(): bool
    {
        if (!$this->module()){
            return false;
        }

        return (!Auth::user()->canAccess($this->module()?->id, 'view_all') && Auth::user()->canAccess($this->module()?->id, 'view_team'));
    }

    public function hasCreatePermissions(): bool
    {
        if (!$this->module()){
            return false;
        }

        return (Auth::user()->canAccess($this->module()?->id, 'create_all')
            || Auth::user()->canAccess($this->module()?->id, 'create_team'));
    }

    public function hasViewPermission(): bool
    {
        if (!$this->module()){
            return false;
        }

        return Auth::user()->canAccess($this->module()?->id, 'show_all')
            || Auth::user()->canAccess($this->module()?->id, 'show_team');
    }

    public function hasUpdatePermissions(): bool
    {
        if (!$this->module()){
            return false;
        }

        return (Auth::user()->canAccess($this->module()?->id, 'update_all')
            || Auth::user()->canAccess($this->module()?->id, 'update_team'));
    }

    public function hasShowPermissions(): bool
    {
        if (!$this->module()){
            return false;
        }

        return (Auth::user()->canAccess($this->module()?->id, 'show_all')
            || Auth::user()->canAccess($this->module()?->id, 'show_team'));
    }

}