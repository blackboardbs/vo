<?php

namespace App\Services;

use App\Models\Assignment;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class PackageDowngrade
{
    public function downgrade(array $package): void
    {
        $license = session('subscriptionDetail');

        $package_login_users = 1;
        $package_non_login_users = 0;
        $package_name = $license['subscription']['subscription_tier']??null;

        if (isset($package_name))
        {
            $package_login_users = $package[$package_name]['users']??1;
            $package_non_login_users = $package[$package_name]['non-users']??0;
        }

        $system_login_users = User::select('id')->where('login_user', 1)->where('status_id', 1)->count();
        $system_non_login_users = User::select('id')->where('login_user', '<>', 1)->where('status_id', 1)->count();

        $this->downgradeUsers($system_login_users, $package_login_users, 1);

        $this->downgradeUsers($system_non_login_users, $package_non_login_users, 0);
    }

    private function downgradeUsers(int $system_users, int $package_users, int $login): void
    {
        if ($system_users > $package_users){
            $assignment_user_ids = $this->userIdsFromAssignments();

            $assignment_users_no_admins = $this->removeAdminsFromAssignmentUsers($assignment_user_ids);

            if ($this->sumAdminAndAssignmentUsers($assignment_users_no_admins) > $package_users){
                $length = $package_users - count($this->adminIds());
                $allowed_login_users = array_slice($assignment_users_no_admins, 0, $length);
                $merge_allowed_with_admins = $this->uniqueArrayValues($allowed_login_users, $this->adminIds());
            }else{
                $limit = $package_users - $this->sumAdminAndAssignmentUsers($assignment_users_no_admins);
                $assignment_admin_users = $this->uniqueArrayValues($assignment_users_no_admins, $this->adminIds());
                $users_without_assignments = User::select('id')
                    ->where('login_user', $login)
                    ->whereNotIn('id', $assignment_admin_users)
                    ->latest()
                    ->limit($limit)
                    ->pluck('id')
                    ->toArray();

                $merge_allowed_with_admins = array_merge($assignment_admin_users, $users_without_assignments);
            }

            User::whereNotIn('id', $merge_allowed_with_admins)
                ->get()
                ->each(function ($user){
                    $user->login_user = 0;
                    $user->status_id = 2;
                    $user->expiry_date = now()->subDays(5)->toDateString();
                    $user->save();
                });
        }
    }

    private function adminIds(): array
    {
        return User::select('id')->whereHas('roles', fn($role) => $role->where('id', 1))->pluck('id')->toArray();
    }

    private function uniqueArrayValues(array $first_array, array $second_array): array
    {
        return array_values(array_unique(array_merge($first_array, $second_array)));
    }

    private function removeAdminsFromAssignmentUsers(array $assignment_user_ids): array
    {
        foreach ($this->adminIds() as $admin){
            if (in_array($admin, $assignment_user_ids))
                unset($assignment_user_ids[array_search($admin, $assignment_user_ids)]);
        }

        return array_values($assignment_user_ids);
    }

    private function userIdsFromAssignments(): array
    {
        return Assignment::selectRaw('DISTINCT employee_id')
            ->whereNotIn('assignment_status', [4,5])
            ->whereHas('consultant')
            ->latest()->pluck('employee_id')->toArray();
    }

    private function sumAdminAndAssignmentUsers(array $assignment_users_no_admins): int
    {
        return count($assignment_users_no_admins) + count($this->adminIds());
    }
}