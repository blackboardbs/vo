<?php

namespace App\Services;

use App\API\Currencies\Currency;
use App\Http\Requests\InvoiceRequest;
use App\Models\Company;
use App\Models\Config;
use App\Models\CustomerInvoice;
use App\Models\Invoice;
use App\Models\Module;
use App\Models\VatRate;
use App\Services\HasPermissions;

class CustomerInvoiceService extends HasPermissions
{

    function module(): Module|null
    {
        return Module::where('name', "App\Models\CustomerInvoice")->first();
    }

    public function saveInvoice(InvoiceRequest $request, Currency $currency): CustomerInvoice
    {
        $customer_invoice = CustomerInvoice::latest()->first();

        $invoiceNumber = $customer_invoice ? $customer_invoice->invoiceNumberString():0;

        $customer_invoice = new CustomerInvoice();
        $customer_invoice->customer_id = $request->input('customer_id');
        $customer_invoice->customer_invoice_number = $invoiceNumber;
        $customer_invoice->invoice_date = $request->input('invoice_date');
        $customer_invoice->due_date = $request->input('due_date');
        $customer_invoice->company_id = $request->input('company_id');
        $customer_invoice->paid_date = null;
        $customer_invoice->customer_unallocate_date = null;
        $customer_invoice->invoice_value = null;
        $customer_invoice->bill_status = 1;
        $customer_invoice->invoice_notes = $request->input('invoice_notes');
        $customer_invoice->inv_ref = $request->input('inv_ref');
        $customer_invoice->cust_inv_ref = $request->input('cust_inv_ref');
        $customer_invoice->project_id = $request->input('project_id');
        $customer_invoice->resource_id = $request->input('resource_id');
        $customer_invoice->currency = $request->input('currency');
        $customer_invoice->conversion_rate = $currency->conversionRate($request->input('currency'))['rate']??0;
        $customer_invoice->save();

        return $customer_invoice;
    }

    public function vatRate(): int
    {
        $config = Config::with('company:id,vat_no')->first(['company_id', 'vat_rate_id']);
        $vat_rate = VatRate::query();

        if ($this->doesCompanyVatExists($config) && isset($config->vat_rate_id)) {
            return $vat_rate->where('vat_code', $config->vat_rate_id)
                ->where('end_date', '>=', now()->toDateString())
                ->first(['id'])?->id;
        }elseif ($this->doesCompanyVatExists($config) && !isset($config->vat_rate_id)) {
            return $vat_rate->where('description', 'Standard')
                ->where('end_date', '>=', now()->toDateString())
                ->first(['id'])?->id;
        }
        return $vat_rate->where('description', 'Zero Rate')->first(['id'])?->id;
    }

    public function currencies():array
    {
        return (new Currency())->currencies()['currencies'] ?? [];
    }

    private function doesCompanyVatExists(Config $config): bool
    {
        $company = isset($config->company) ? $config->company : Company::first(['vat_no']);

        return isset($company?->vat_no);
    }

}