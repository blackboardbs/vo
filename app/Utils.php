<?php

namespace App;

use App\Enum\Status;
use App\Enum\YesOrNoEnum;
use App\Mail\AssignmentHoursOverspent;
use App\Models\Assignment;
use App\Models\Config;
use App\Models\Notification;
use App\Models\PlanUtilization;
use App\Models\RoleUser;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class Utils
{
    public string $helpPortal;
    public string $date;

    public function __construct()
    {
        $this->helpPortal = 'https://bbshelpportal.blackboardbs.com/';

        // $this->userRoles = RoleUser::where('role_id', '=', 1)->get();
        // $emails = $this->userRoles;
        // $adminEmails = [];
        // foreach ($emails as $admin) {
        //     array_push($adminEmails, ($admin->user?->login_user == '1' ? $admin->user->email : null));
        // }

        // $this->admins = $adminEmails;
        $this->date = Carbon::now()->toDateString();
    }

    public function differenceInDays($moduleDate)
    {
        return Carbon::parse($moduleDate)->diffInDays(Carbon::now());
    }

    public function hoursOverspent($employee_id, $project_id)
    {
        $assignment = Assignment::where('employee_id', '=', $employee_id)
            ->where('project_id', '=', $project_id)
            ->whereIn('assignment_status', [2, 3])
            ->first();
        $timesheet_hours = 1;
        if ($assignment) {
            $timesheet_hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable->value.' THEN timeline.total + ROUND((total_m/60),2) ELSE 0 END) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.employee_id', '=', $employee_id)
                ->where('timesheet.project_id', '=', $project_id)
                ->first();
        }

        $timesheet = Timesheet::where('employee_id', '=', $employee_id)->where('project_id', '=', $project_id)->latest()->first();

        if (isset($assignment->hours) && $assignment->hours < $timesheet_hours->actual_hours) {
            $emails = [];
            array_push($emails, ($timesheet->user->email ?? null));
            if($timesheet->resource->manager && $timesheet->resource->manager->login_user == '1'){
            array_push($emails, ($timesheet->resource->manager->email ?? null));
            }
            $emails = array_filter(array_unique(array_merge($emails, $this->adminEmail())));

            if (count($emails)) {
                Mail::to($emails)->send(new AssignmentHoursOverspent($assignment, $this->helpPortal, $timesheet_hours));
            }
        }
    }

    public function outstandingTimesheets($numberOfWeeks, $user_id = null)
    {
        if ($numberOfWeeks != 1) {
            $resources = Assignment::with(['resource', 'resource_user'])->select('employee_id')->whereIn('assignment_status', [2, 3])
               ->whereHas('resource_user', function ($query) {
                   $query->where('util', '=', 1);
                   $query->where('join_date', '<', now()->toDateString());
               });

            if (auth()->user()->hasRole('manager')) {
                $resources = $resources->whereHas('resource_user', function ($query) {
                    $query->where('manager_id', '=', auth()->id());
                });
            } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
                $resources = $resources->where('employee_id', '=', auth()->user()->id);
            }
            $resources = $resources->groupBy('employee_id')->get();
        } else {
            $resources = Assignment::with('resource')->selectRaw('employee_id, id, project_id')->whereIn('assignment_status', [2, 3])
               ->whereHas('resource_user', function ($query) {
                   $query->where('util', '=', 1);
                   $query->where('join_date', '<', now()->toDateString());
               })
               ->groupBy('id')
               ->groupBy('employee_id')
               ->groupBy('project_id')
               ->get();
        }

        $weeks = PlanUtilization::select('yearwk')->whereBetween('yearwk',
            [
                now()->subWeeks($numberOfWeeks)->year.((now()->subWeeks($numberOfWeeks)->weekOfYear < 10) ? '0'.now()->subWeeks($numberOfWeeks)->weekOfYear : now()->subWeeks($numberOfWeeks)->weekOfYear),
                now()->subWeeks(1)->year.((now()->subWeeks(1)->weekOfYear < 10) ? '0'.now()->subWeeks(1)->weekOfYear : now()->subWeeks(1)->weekOfYear),
            ])->orderBy('yearwk', 'desc')->get();

        $outstanding_timesheet = [];

        $current_week = Carbon::parse(now())->year.(Carbon::parse(now())->weekOfYear < 10 ? '0'.Carbon::parse(now())->weekOfYear : Carbon::parse(now())->weekOfYear);
        //dd($current_week);
        foreach ($weeks as $week) {
            foreach ($resources as $resource) {
                $time = Timesheet::select('year_week', 'employee_id')->whereHas('resource')->where('year_week', '=', $week->yearwk)->where('employee_id', '=', $resource->employee_id)->groupBy('year_week')->groupBy('employee_id')->first();
                if ($week->yearwk <= $current_week && $numberOfWeeks != 1) {
                    $outstanding_timesheet[] = (isset($time) ? null : $resource->resource?->first_name.' '.$resource->resource?->last_name.' - '.$week->yearwk);
                } else {
                    $outstanding_timesheet[] = (! isset($time)) ? $resource : null;
                }
            }
        }

        return array_values(array_filter($outstanding_timesheet));
    }

    public function notifications($name, $route)
    {
        //insert notification message for manager
        $notification = new Notification();
        $notification->name = $name;
        $notification->link = $route;
        $notification->save();

        return $notification;
    }

    public function notification_id($user_id, $notification)
    {
        $user_notifications = [];
        $user_ids = [];

        foreach ($this->adminEmail() as $user) {
            array_push($user_ids, $user->user_id);
        }
        array_push($user_ids, $user_id);

        foreach (array_filter(array_unique($user_ids)) as $id) {
            if (! is_null($id)) {
                array_push($user_notifications, [
                    'user_id' => $id,
                    'notification_id' => $notification->id,
                ]);
            }
        }

        return $user_notifications;
    }

    public function siteName(): string
    {
        return Config::first()->site_name??"Consulteaze";
    }

    public function adminEmail(): array
    {
        return User::select(['email'])
            ->where('status_id', Status::ACTIVE->value)
            ->where('login_user', YesOrNoEnum::Yes->value)
            ->whereHas('roles', fn($role) => $role->where('name', 'admin'))
            ->get()
            ->map(fn($user) => $user->email)
            ->toArray();
    }

    public function claimApprover()
    {
        return Config::first()->claim_approver_id??(User::first()->id??1);
    }
}
