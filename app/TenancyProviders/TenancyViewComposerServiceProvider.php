<?php

namespace App\TenancyProviders;

use App\Enum\TimesheetTemplatesEnum;
use App\Enum\YesOrNoEnum;
use App\Models\Account;
use App\Models\AccountElement;
use App\Models\AdvancedTemplates;
use App\Models\AssignmentStandardCost;
use App\Models\AvailStatus;
use App\Models\BillingCycle;
use App\Models\BillingPeriodStyle;
use App\Models\Board;
use App\Models\BusinessFunction;
use App\Models\BusinessRating;
use App\Models\Company;
use App\Models\Config;
use App\Models\CostCenter;
use App\Models\Country;
use App\Models\Customer;
use App\Models\ExpenseType;
use App\Models\InterviewStatus;
use App\Models\InvoiceContact;
use App\Models\JobOrigin;
use App\Models\JobSpecStatus;
use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\Page;
use App\Models\ProcessStatus;
use App\Models\Profession;
use App\Models\ProjectStatus;
use App\Models\ProjectTerms;
use App\Models\ProjectType;
use App\Models\ResourceLevel;
use App\Models\ResourcePosition;
use App\Models\ResourceType;
use App\Models\Scouting;
use App\Models\ScoutingRole;
use App\Models\SkillLevel;
use App\Models\Speciality;
use App\Models\Sprint;
use App\Models\Status;
use App\Models\System;
use App\Models\Team;
use App\Models\TechnicalRating;
use App\Models\Template;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Stancl\Tenancy\Contracts\TenancyBootstrapper;
use Stancl\Tenancy\Contracts\Tenant;

class TenancyViewComposerServiceProvider implements TenancyBootstrapper
{

    public function bootstrap(Tenant $tenant)
    {
        try
        {
            View::share($this->viewVariables());
        }catch (\Exception $exception){
            //logger($exception->getMessage());
        }
    }

    public function revert()
    {
        // TODO: Implement revert() method.
    }


    private function viewVariables(): array
    {
        //try {
            $licenses = [];
            $licenses['Trial']  = ['users'=>'10','non-users' => '20'];
            $licenses['Classic'] = ['users'=>'10','non-users' => '20'];
            $licenses['Standard'] = ['users'=>'10','non-users' => '20'];
            $licenses['Premium'] = ['users'=>'10','non-users' => '20'];
            
            $cache_time = now()->addMonths(3);

            $config = cache()->remember('config', $cache_time, fn() => Config::select('site_logo', 'company_id', 'logo_placement', 'background_color', 'font_color', 'active_link', 'vendor_template_id', 'resource_template_id', 'calendar_action_colour', 'calendar_task_colour', 'calendar_leave_colour', 'calendar_assignment_colour', 'calendar_assessment_colour', 'calendar_anniversary_colour', 'calendar_events_colour', 'calendar_user_colour', 'calendar_cinvoice_colour', 'calendar_vinvoice_colour', 'calendar_medcert_colour', 'calendar_public_holidays_colour', 'timesheet_template_id', 'billing_cycle_id', 'default_custom_template', 'sh_good_min', 'sh_good_max', 'sh_fair_min', 'sh_fair_max', 'sh_bad_min', 'sh_bad_max')->first());

            if (isset($config->site_logo) && file_exists(storage_path("app/public/assets/".$config->site_logo))){
                $logo = storage_path("app/public/assets/".$config->site_logo);
            }elseif (isset($config->company->company_logo) && file_exists(storage_path('app/avatars/company/'.$config->company->company_logo))) {
                $logo = storage_path('app/avatars/company/'.$config->company->company_logo);
            } else {
                $logo = global_asset('assets/default.png');
            }

            $company_dropdown = cache()->remember('company_dropdown', $cache_time, fn() => Company::where('status_id', 1)->orderBy('company_name')
                ->pluck('company_name', 'id')->prepend('All', null));

            $customer_dropdown = cache()->remember('customer_dropdown', $cache_time, fn() => Customer::where('status', 1)->orderBy('customer_name')->pluck('customer_name', 'id')->prepend('All', null));

            $account_dropdown = cache()->remember('account_dropdown', $cache_time, fn() => Account::orderBy('description')->pluck('description', 'id')->prepend('All', null));

            $account_element_dropdown = cache()->remember('account_element_dropdown', $cache_time->addWeek(), fn() => AccountElement::select(['id', 'account_id', 'description'])->get());

            $resource_dropdown = cache()->remember('resource_dropdown', $cache_time, fn() => User::selectRaw('CONCAT(`first_name`, " ",`last_name`) AS full_name, id')->where('expiry_date', '>=', now()->toDateString())->orderByRaw('CONCAT(`first_name`, " ",`last_name`)')->whereHas('roles', fn($roles) => $roles->whereNotIn('id', [7, 8]))->get());

            $resource_managers_dropdown = cache()->remember('resource_managers_dropdown', $cache_time, fn() => User::selectRaw('CONCAT(`first_name`, " ",`last_name`) AS full_name, id')->where('expiry_date', '>=', now()->toDateString())->orderByRaw('CONCAT(`first_name`, " ",`last_name`)')->whereHas('roles', fn($roles) => $roles->whereNotIn('id', [7, 9, 11, 12]))->get());

            $expense_approval_status = cache()->remember('expense_approval_status', $cache_time, fn() => [1 => 'New', 2 => 'Approved', 3 => 'Rejected', 4 => 'Updated']);

            $expense_payment_status = cache()->remember('expense_payment_status', $cache_time, fn() => [1 => 'New', 2 => 'Paid']);

            $expense_type_dropdown = cache()->remember('expense_type_dropdown', $cache_time, fn() => ExpenseType::where('status_id', 1)->pluck('description', 'id'));

            $yes_or_no_dropdown = cache()->remember('yes_or_no_dropdown', $cache_time, fn() => collect(YesOrNoEnum::cases())->pluck('name', 'value'));

            $status_dropdown = cache()->remember('status_dropdown', $cache_time, fn() => Status::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $project_status_dropdown = cache()->remember('project_status_dropdown', $cache_time, fn() => ProjectStatus::where('status_id', 1)->orderBy('description')->pluck('description', 'id'));

            $business_function_dropdown = cache()->remember('business_function_dropdown', $cache_time->addWeek(), fn() => BusinessFunction::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $resource_type_dropdown = cache()->remember('resource_type_dropdown', $cache_time, fn() => ResourceType::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $resource_position_dropdown = cache()->remember('resource_position_dropdown', $cache_time, fn() => ResourcePosition::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $resource_level_dropdown = cache()->remember('resource_level_dropdown', $cache_time, fn() => ResourceLevel::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $assignment_standard_cost_dropdown = cache()->remember('assignment_standard_cost_dropdown', $cache_time, fn() => AssignmentStandardCost::where('status_id', 1)->orderBy('description')->pluck('description', 'id'));

            $team_dropdown = cache()->remember('team_dropdown', $cache_time, fn() => Team::where('status_id', 1)->orderBy('team_name')->pluck('team_name', 'id')->prepend('All', null));

            $cost_center_dropdown = cache()->remember('cost_center_dropdown', $cache_time, fn() => CostCenter::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $process_status_dropdown = cache()->remember('process_status_dropdown', $cache_time, fn() => ProcessStatus::where('status_id', 1)->orderBy('name')->pluck('name', 'id'));

            $process_interview_dropdown = cache()->remember('process_interview_dropdown', $cache_time->addWeek(), fn() => InterviewStatus::where('status_id', 1)->orderBy('name')->pluck('name', 'id'));

            $scouting_role_dropdown = cache()->remember('scouting_role_dropdown', $cache_time, fn() => ScoutingRole::where('status_id', 1)->orderBy('name')->pluck('name', 'id'));

            $business_rating_dropdown = cache()->remember('business_rating_dropdown', $cache_time, fn() => BusinessRating::where('status_id', 1)->orderBy('name')->pluck('name', 'id'));

            $technical_rating_dropdown = cache()->remember('technical_rating_dropdown', $cache_time, fn() => TechnicalRating::where('status_id', 1)->orderBy('name')->pluck('name', 'id'));

            $profession_dropdown = cache()->remember('profession_dropdown', $cache_time, fn() => Profession::orderBy('name')->pluck('name', 'id'));

            $speciality_dropdown = cache()->remember('speciality_dropdown', $cache_time, fn() => Speciality::orderBy('name')->pluck('name', 'id'));

            $skill_dropdown = cache()->remember('skill_dropdown', $cache_time->addWeek(), fn() => System::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $reference_code_dropdown = cache()->remember('reference_code_dropdown', $cache_time, fn() => Scouting::orderBy('reference_code')->whereNotNull('reference_code')->where('reference_code', '!=', '')->pluck('reference_code', 'id'));

            $skill_level_dropdown = cache()->remember('skill_level_dropdown', $cache_time, fn() => SkillLevel::where('status', 1)->orderBy('description')->pluck('description', 'id'));

            $availability_status_dropdown = cache()->remember('availability_status_dropdown', $cache_time, fn() => AvailStatus::where('status_id', 1)->orderBy('description')->pluck('description', 'id'));

            $job_origin_dropdown = cache()->remember('job_origin_dropdown', $cache_time, fn() => JobOrigin::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'));

            $job_spec_status_dropdown = cache()->remember('job_spec_status_dropdown', $cache_time, fn() => JobSpecStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'));

            $sprint_dropdown = cache()->remember('sprint_dropdown', $cache_time->addWeek(), fn() => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', 0));

            $board_dropdown = cache()->remember('board_dropdown', $cache_time, fn() => Board::orderBy('name')->pluck('name', 'id')->prepend('Board', 0));

            $vendor_dropdown = cache()->remember('vendor_dropdown', $cache_time, fn() => Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please Select', null));

            $project_type_dropdown = cache()->remember('project_type_dropdown', $cache_time, fn() => ProjectType::orderBy('description')->pluck('description', 'id'));

            $invoice_contact_dropdown = cache()->remember('invoice_contact_dropdown', $cache_time, fn() => InvoiceContact::select(['id', 'first_name', 'last_name'])->where('status_id', 1)->orderBy('first_name')->orderBy('last_name')->get()->pluck('full_name', 'id'));

            $timesheet_template_dropdown = cache()->remember('timesheet_template_dropdown', $cache_time, fn() => collect(TimesheetTemplatesEnum::cases())->pluck('name', 'value'));

            $billing_cycle_dropdown = cache()->remember('billing_cycle_dropdown', $cache_time, fn() => BillingCycle::orderBy('name')->where('status_id', 1)->pluck('name', 'id'));

            $roles_dropdown = cache()->remember('roles_dropdown', $cache_time, fn() => ModuleRole::orderBy('name')->pluck('name', 'id'));

            $project_terms_dropdown = cache()->remember('project_terms_dropdown', $cache_time, fn() => ProjectTerms::orderBy('terms_version')->pluck('terms_version', 'id'));

            $billing_period_template_dropdown = cache()->remember('billing_period_template', $cache_time, fn() => BillingPeriodStyle::orderBy('name')->where('status_id', 1)->pluck('name', 'id'));

            $templates_dropdown = cache()->remember('template_dropdown', $cache_time->addWeek(), fn() => AdvancedTemplates::where('template_type_id', '=', 1)->orderBy('id')->pluck('name', 'id')->prepend('Template', '0'));

            $vendor_template_dropdown = cache()->remember('vendor_template_dropdown', $cache_time, fn() => AdvancedTemplates::where('template_type_id', '=', 2)->orderBy('id')->pluck('name', 'id')->prepend('Template', '0'));

            $countries_dropdown = cache()->remember('countries_dropdown', $cache_time, fn() => Country::orderBy('id')->pluck('name', 'id')->prepend('Country', '0'));

            $modules = cache()->remember('modules', $cache_time, fn() => Module::where('status_id', '=', 1)->get());

            $module_dropdowns = cache()->remember('module_dropdown', $cache_time, fn() => Module::where('status_id', '=', 1)->orderBy('display_name')->pluck('display_name', 'id')->prepend('Modules', '0')->toArray());

            return [
                'logo' => $logo,
                'modules' => $modules,
                'module_dropdowns' => $module_dropdowns,
                'config' => $config,
                'company_dropdown' => $company_dropdown,
                'account_dropdown' => $account_dropdown,
                'account_element_dropdown' => $account_element_dropdown,
                'resource_dropdown' => $resource_dropdown,
                'resource_managers_dropdown' => $resource_managers_dropdown->pluck('full_name', 'id'),
                'expense_approval_status' => $expense_approval_status,
                'expense_payment_status' => $expense_payment_status,
                'expense_type_dropdown' => $expense_type_dropdown,
                'yes_or_no_dropdown' => $yes_or_no_dropdown,
                'customer_dropdown' => $customer_dropdown,
                'status_dropdown' => $status_dropdown,
                'project_status_dropdown' => $project_status_dropdown,
                'business_function_dropdown' => $business_function_dropdown,
                'resource_type_dropdown' => $resource_type_dropdown,
                'resource_position_dropdown' => $resource_position_dropdown,
                'resource_level_dropdown' => $resource_level_dropdown,
                'assignment_standard_cost_dropdown' => $assignment_standard_cost_dropdown,
                'team_dropdown' => $team_dropdown,
                'cost_center_dropdown' => $cost_center_dropdown,
                'process_status_dropdown' => $process_status_dropdown,
                'process_interview_dropdown' => $process_interview_dropdown,
                'scouting_role_dropdown' => $scouting_role_dropdown,
                'business_rating_dropdown' => $business_rating_dropdown,
                'technical_rating_dropdown' => $technical_rating_dropdown,
                'profession_dropdown' => $profession_dropdown,
                'speciality_dropdown' => $speciality_dropdown,
                'skill_dropdown' => $skill_dropdown,
                'reference_code_dropdown' => $reference_code_dropdown,
                'skill_level_dropdown' => $skill_level_dropdown,
                'availability_status_dropdown' => $availability_status_dropdown,
                'job_origin_dropdown' => $job_origin_dropdown,
                'job_spec_status_dropdown' => $job_spec_status_dropdown,
                'recruiters_dropdown' => $this->usersByRole('recruitment', $resource_dropdown),
                'sprint_dropdown' => $sprint_dropdown,
                'board_dropdown' => $board_dropdown,
                'vendor_dropdown' => $vendor_dropdown,
                'project_type_dropdown' => $project_type_dropdown,
                'users_dropdown' => $resource_dropdown->pluck('full_name', 'id')->prepend('All', null),
                'invoice_contact_dropdown' => $invoice_contact_dropdown,
                'timesheet_template_dropdown' => $timesheet_template_dropdown,
                'billing_cycle_dropdown' => $billing_cycle_dropdown,
                'roles_dropdown' => $roles_dropdown,
                'project_terms_dropdown' => $project_terms_dropdown,
                'billing_period_template_dropdown' => $billing_period_template_dropdown,
                'templates_dropdown' => $templates_dropdown,
                'vendor_template_dropdown' => $vendor_template_dropdown,
                'countries_dropdown' => $countries_dropdown,
                'pages', Page::all(),
                'page_title' => isset($config->company_id) ? Company::find($config->company_id, ['company_name'])->company_name:config('app.name')
            ];
        /*}catch (\Exception $exception){

        }

        return [];*/
    }

    public function usersByRole(string $role, \Illuminate\Database\Eloquent\Collection $users): Collection
    {
        return $users->filter(fn($user) => $user->hasRole($role))->values();
    }
}