<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class GeneralExport implements FromView
{
    public function __construct(private readonly string $view, public array $data){}

    public function view(): View
    {
        return view($this->view)->with($this->data);
    }
}
