<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ScoutingExport implements FromCollection, WithHeadings
{
    public function __construct(Collection $resources)
    {
        $this->resources = $resources;
    }

    public function headings(): array
    {
        return [
            'Resource Name',
            'Email',
            'Resource Status',
            'Role',
            'Vendor',
            'Resource Level',
            'Business Rating',
            'Technical Rating',
        ];
    }

    public function collection(): Collection
    {
        return $this->resources;
    }
}
