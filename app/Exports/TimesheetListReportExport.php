<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TimesheetListReportExport implements FromView
{
    public $data;
    public $totals;
    private $view;

    public function __construct($data,$totals, $view)
    {
        $this->data = $data;
        $this->totals = $totals;
        $this->view = $view;
    }

    public function view(): View
    {
        return view($this->view)->with(['timelines' => $this->data,'totals'=>$this->totals]);
    }
}
