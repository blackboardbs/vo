<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

class TimesheetReportExport implements FromView
{
    protected $timesheetSummary;
    protected $weeks;
    protected $total;

    public function __construct(array $reports, array $weeks, array $total)
    {
        $this->timesheetSummary = $reports;
        $this->weeks = $weeks;
        $this->total = $total;
    }

    public function view(): View
    {
        return view('exports.timesummary', [
            'reports' => $this->timesheetSummary,
            'weeks' => $this->weeks,
            'grand_total' => $this->total
        ]);
    }

}
