<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TimesheetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $document_id;
    public $user_number;
    public $user_name;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document_id, $user_number, $user_name, $subject)
    {
        $this->document_id = $document_id;
        $this->user_number = $user_number;
        $this->user_name = $user_name;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.timesheet_email')->with(['document_id'=>$this->document_id, 'user_number'=>$this->user_number, 'user_name'=>$this->user_name, 'subject' => $this->subject]);
    }
}
