<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerStatementEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer_invoice;
    public $storage;
    public function __construct($customer_invoice, $storage)
    {
        $this->customer_invoice = $customer_invoice;
        $this->storage = $storage;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.customer_statement')->attach($this->storage);
    }
}
