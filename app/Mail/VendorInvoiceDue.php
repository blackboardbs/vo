<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VendorInvoiceDue extends Mailable
{
    use Queueable, SerializesModels;

    public $vendorInvoice;
    public $helpPortal;
    public function __construct($vendorInvoice, $helpPortal)
    {
        $this->vendorInvoice = $vendorInvoice;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.vendor_invoice_due');
    }
}
