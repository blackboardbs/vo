<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlannedUtilization extends Mailable
{
    use Queueable, SerializesModels;

    public $siteName;
    public $helpPortal;


    public function __construct($siteName, $helpPortal)
    {
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.planned_utilization');
    }
}
