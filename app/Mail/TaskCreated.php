<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $task;
    public $helpPortal;
    public function __construct($task, $helpPortal)
    {
        $this->task = $task;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.task_created');
    }
}
