<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $siteName;
    public $helpPortal;

    public function __construct($user, $siteName, $helpPortal)
    {
        $this->user = $user;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.user-account');
    }
}
