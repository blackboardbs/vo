<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignmentCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $assignment;
    public $helpPortal;
    public function __construct($assignment, $helpPortal)
    {
        $this->assignment = $assignment;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.assignment_created');
    }
}
