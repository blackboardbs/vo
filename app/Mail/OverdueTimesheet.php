<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OverdueTimesheet extends Mailable
{
    use Queueable, SerializesModels;

    public $assignments;
    public $resource;
    public $start_of_week;
    public $year_week;
    public $helpPortal;
    public function __construct($assignments, $resource, $year_week, $start_of_week, $helpPortal)
    {
        $this->assignments = $assignments;
        $this->resource = $resource;
        $this->year_week = $year_week;
        $this->start_of_week = $start_of_week;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.timesheet_overdue');
    }
}
