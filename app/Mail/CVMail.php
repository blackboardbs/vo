<?php

namespace App\Mail;

use App\Models\Document;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CVMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_name;
    public $subject;
    public $message;
    public $documentIDS;
    public $generate_cv_file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $subject, $message, $file, $generate_cv_file, $documentIDS)
    {
        $this->user_name = $user_name;
        $this->subject = $subject;
        $this->mail_body = '';
        $this->message = $message;
        $this->file = $file;
        $this->generate_cv_file = $generate_cv_file;
        $this->documentIDS = $documentIDS;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {

        $parameters = [
            'user_name'=>$this->user_name,
            'subject'=>$this->subject,
            'mail_body'=> $this->message,
            'message'=>$this->message,
            'file'=>$this->file??'',
        ];

        $email = $this->view('emails.send_cv')->with($parameters);

        if($this->generate_cv_file == 1 && $this->file != ''){
            $email->attach($this->file);
        }

        $_documentIDS = explode('|', $this->documentIDS);

        foreach ($_documentIDS as $documentID){
            $document = Document::find($documentID);
            if($document != null) {
                $email->attach($document->file);
            }
        }

        return $email;

        //return $this->view('emails.send_cv')->with($parameters)->attach($this->file);
    }
}
