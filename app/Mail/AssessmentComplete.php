<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssessmentComplete extends Mailable
{
    use Queueable, SerializesModels;

    public $manager;
    public $assessment;
    public $siteName;
    public $helpPortal;
    public function __construct($manager, $assessment, $siteName, $helpPortal)
    {
        $this->manager = $manager;
        $this->assessment = $assessment;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.assessment_completed');
    }
}
