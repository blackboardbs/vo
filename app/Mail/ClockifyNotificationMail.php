<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClockifyNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailBody;
    public function __construct($mailBody)
    {
        $this->mailBody = $mailBody;
        $this->subject("Clockify Notification");
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.clockify')->with(['mailBody' => $this->mailBody]);
    }
}
