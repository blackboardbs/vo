<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerInvoiceDue extends Mailable
{
    use Queueable, SerializesModels;

    public $customerInvoice;
    public $helpPortal;
    public function __construct($customerInvoice, $helpPortal)
    {
        $this->customerInvoice = $customerInvoice;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.customer_invoice_due');
    }
}
