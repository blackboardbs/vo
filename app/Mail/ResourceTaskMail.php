<?php

namespace App\Mail;

use App\Models\ResourceTask;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResourceTaskMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $resource;

    public $task;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ResourceTask $task, $user, $resource)
    {
        $this->user = $user;
        $this->resource = $resource;
        $this->task = $task;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.resource_task')->with(['user' => $this->user, 'resource' => $this->resource, 'task' => $this->task]);
    }
}
