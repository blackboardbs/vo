<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class VendorInvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $vendor;
    public $company;
    public $subject;
    public $body;
    protected $storage;
    public function __construct($vendor, $company, $subject, $body, $storage)
    {
        $this->vendor = $vendor;
        $this->company = $company;
        $this->subject = $subject;
        $this->body = $body;
        $this->storage = $storage;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.vendor_invoice')->attach($this->storage);
    }
}
