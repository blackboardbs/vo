<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RiskFeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    public $risk;
    public $feedback;
    public $feedbackOwner;
    public function __construct($risk, $feedback, $feedbackOwner)
    {
        $this->risk = $risk;
        $this->feedback = $feedback;
        $this->feedbackOwner = $feedbackOwner;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.riskfeedback')
            ->with([
                'risk' => $this->risk,
                'feedback' => $this->feedback,
                'feedbackOwner' => $this->feedbackOwner
            ]);
    }
}
