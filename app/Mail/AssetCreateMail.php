<?php

namespace App\Mail;

use App\Models\AssetRegister;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssetCreateMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public $resource;

    public $company;

    public function __construct(AssetRegister $data, $resource, $company)
    {
        $this->data = $data;
        $this->resource = $resource;
        $this->company = $company;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.acceptanceofequipment')->with(['logo' => isset($this->company->company_logo) ? $this->company->company_logo : '', 'company_name' => isset($this->company->company_name) ? $this->company->company_name : '', 'postal1' => isset($this->company->postal_address_line1) ? $this->company->postal_address_line1 : '', 'postal2' => isset($this->company->postal_address_line2) ? $this->company->postal_address_line2 : '', 'postal3' => isset($this->company->postal_address_line3) ? $this->company->postal_address_line3 : '', 'postal4' => isset($this->company->city_suburb) ? $this->company->city_suburb : '', 'postal5' => isset($this->company->state_province) ? $this->company->state_province : '', 'postal6' => isset($this->company->postal_zipcode) ? $this->company->postal_zipcode : '', 'name' => isset($this->resource->first_name) ? $this->resource->first_name : '', 'surname' => isset($this->resource->last_name) ? $this->resource->last_name : '', 'asset_nr' => isset($this->data->asset_nr) ? $this->data->asset_nr : '', 'asset_class' => isset($this->data->asset_class) ? $this->data->asset_class : '', 'model' => isset($this->data->model) ? $this->data->model : '', 'make' => isset($this->data->make) ? $this->data->make : '', 'make' => isset($this->data->make) ? $this->data->make : '', 'make' => isset($this->data->make) ? $this->data->make : '', 'serial_nr' => isset($this->data->serial_nr) ? $this->data->serial_nr : '', 'aquire_date' => isset($this->data->aquire_date) ? $this->data->aquire_date : '', 'processor' => isset($this->data->processor) ? $this->data->processor : '', 'ram' => isset($this->data->ram) ? $this->data->ram : '', 'hdd' => isset($this->data->hdd) ? $this->data->hdd : '', 'screen_size' => isset($this->data->screen_size) ? $this->data->screen_size : '']);
    }
}
