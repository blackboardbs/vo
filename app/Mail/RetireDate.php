<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RetireDate extends Mailable
{
    use Queueable, SerializesModels;

    public $asset;
    public $helpPortal;
    public function __construct($asset, $helpPortal)
    {
        $this->asset = $asset;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.retire_date');
    }
}
