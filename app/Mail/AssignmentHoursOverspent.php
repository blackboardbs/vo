<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignmentHoursOverspent extends Mailable
{
    use Queueable, SerializesModels;

    public $assignment;
    public $helpPortal;
    public $timesheet;
    public $completion_percentage;
    public function __construct($assignment, $helpPortal, $timesheet)
    {
        $this->assignment = $assignment;
        $this->helpPortal = $helpPortal;
        $this->timesheet = $timesheet;
        $this->completion_percentage = $this->percentage();
    }

    public function build()
    {
        return $this->view('emails.assignment_hours_overspent');
    }

    private function percentage()
    {
        if ($this->assignment->hours > 0){
            $percentage = ($this->timesheet->actual_hours/$this->assignment->hours) * 100;
            return round($percentage);
        }

        return 0;
    }
}
