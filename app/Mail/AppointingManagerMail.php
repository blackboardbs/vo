<?php

namespace App\Mail;

use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointingManagerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $candidate;
    public $appointing_manager;
    public $company;
    public $onboarding;

    public function __construct($candidate, $appointing_manager, $company,$onboarding)
    {
        $this->candidate = $candidate;
        $this->appointing_manager = $appointing_manager;
        $this->company = ($company??(Company::first(['company_name'])->company_name??"Blackboard"));
        $this->onboarding = $onboarding;
    }

    public function build()
    {
        return $this->view('emails.onboarding.appointing-manager');
    }
}
