<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResourceAssignmentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_name;
    public $digisign_user_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $digisign_user_id)
    {
        $this->user_name = $user_name;
        $this->digisign_user_id = $digisign_user_id;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.supplier_assignment')->with(['user'=>$this->user_name, 'digisign_user_id' => $this->digisign_user_id]);
    }
}
