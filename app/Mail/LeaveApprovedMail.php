<?php

namespace App\Mail;

use App\Models\Leave;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeaveApprovedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resource;

    public $user;

    public $leave;

    public $leave_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Leave $leave, $leave_type, $projects, $resource, $user)
    {
        $this->leave = $leave;
        $this->leave_type = $leave_type;
        $this->projects = $projects;
        $this->resource = $resource;
        $this->user = $user;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.leaveapproved')->with(['resource' => $this->resource, 'leave' => $this->leave, 'leave_type' => $this->leave_type, 'project' => $this->projects, 'user' => $this->user]);
    }
}
