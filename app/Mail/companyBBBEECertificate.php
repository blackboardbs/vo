<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class companyBBBEECertificate extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $helpPortal;
    public $siteName;
    public function __construct($company, $helpPortal, $siteName)
    {
        $this->company = $company;
        $this->helpPortal = $helpPortal;
        $this->siteName = $siteName;
    }

    public function build()
    {
        return $this->view('emails.company_bbbee_certificate');
    }
}
