<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceUnallocation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $customer_invoice;
    public function __construct($customer_invoice)
    {
        $this->customer_invoice = $customer_invoice;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.customer_invoice_unallocation');
    }
}
