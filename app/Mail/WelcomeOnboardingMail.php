<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeOnboardingMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $company;
    public $password;
    public $onboarding;
    public $appointing_manager;
    public $system_admin;
    public function __construct($user, $company, $password, $onboarding, $appointing_manager, $system_admin)
    {
        $this->user = $user;
        $this->company = $company;
        $this->password = $password;
        $this->onboarding = $onboarding;
        $this->appointing_manager = $appointing_manager;
        $this->system_admin = $system_admin;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.onboarding.welcome');
    }
}
