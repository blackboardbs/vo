<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VendorBBBEECertificateExpiry extends Mailable
{
    use Queueable, SerializesModels;

    public $vendor;
    public $siteName;
    public $helpPortal;
    public function __construct($vendor, $siteName, $helpPortal)
    {
        $this->vendor = $vendor;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.vendor_bbbee_certificate_expiry');
    }
}
