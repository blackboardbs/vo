<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Leave;

class LeaveRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resource;
    public $leave;
    public $leave_type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($leave, $leave_type, $projects, $resource)
    {
        $this->leave = $leave;
        $this->leave_type = $leave_type;
        $this->projects = (object) $projects;
        $this->resource = $resource;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.leave')->with(['resource'=>$this->resource,'leave'=>$this->leave,'leave_type'=>$this->leave_type,'project'=>$this->projects]);
    }
}
