<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadLogoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $body;
    public $url;
    public $button_text;
    public function __construct(string $body, array $url, string $button_text)
    {
        $this->body = $body;
        $this->url = $url;
        $this->button_text = $button_text;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->markdown('emails.upload-logo');
    }
}
