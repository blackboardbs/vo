<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssetAknowledgementDigiSignMail extends Mailable
{
    use Queueable, SerializesModels;

    public $document_id;
    public $user_number;
    public $user_name;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($asset_id, $digisign_user_id, $user_name, $subject, $signed)
    {
        $this->signed = $signed;
        $this->asset_id = $asset_id;
        $this->digisign_user_id = $digisign_user_id;
        $this->user_name = $user_name;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {

        $parameters = [
            'asset_id'=>$this->asset_id,
            'digisign_user_id'=>$this->digisign_user_id,
            'user_name'=>$this->user_name,
            'subject' => $this->subject,
            'signed' => $this->signed
        ];

        return $this->view('emails.asset_aknowledgement_digisign')->with($parameters);

    }
}
