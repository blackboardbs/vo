<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DistributionListEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_name;
    public $subject;
    public $mail_body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $subject, $message)
    {
        $this->user_name = $user_name;
        $this->subject = $subject;
        $this->mail_body = $message;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $parameters = [
            'user_name'=>$this->user_name,
            'subject'=>$this->subject,
            'mail_body'=> $this->mail_body
        ];

        return $this->view('emails.distribution_list')->with($parameters);
    }
}
