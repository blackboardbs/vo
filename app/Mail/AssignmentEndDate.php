<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignmentEndDate extends Mailable
{
    use Queueable, SerializesModels;

    public $assignment;
    public $actualHours;
    public $helpPortal;
    public function __construct($assignment, $actualHours, $helpPortal)
    {
        $this->assignment = $assignment;
        $this->actualHours = $actualHours;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.assignment_end_date');
    }
}
