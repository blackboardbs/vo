<?php

namespace App\Mail;

use App\Models\UserOnboarding;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserOnboardingConfirmMail extends Mailable
{
    use Queueable, SerializesModels;

    public $onboarding;
    public $hrUser;
    public $company;
    public $dear;
    public function __construct(UserOnboarding $onboarding, $hrUser, $company)
    {
        $this->onboarding = $onboarding;
        $this->hrUser = $hrUser;
        $this->company = $company;
        $this->dear = array_filter(
            array_values(
                array_unique([($this->hrUser->first_name??null), ($this->onboarding->user->appointmentManager->first_name??null)])
            )
        );
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.onboarding.user-commit');
    }
}
