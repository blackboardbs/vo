<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExpenseTracking extends Mailable
{
    use Queueable, SerializesModels;

    public $expense;
    public $siteName;
    public $helpPortal;
    public function __construct($expense, $siteName, $helpPortal)
    {
        $this->expense = $expense;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.expense_tracking');
    }
}
