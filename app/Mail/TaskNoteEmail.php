<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskNoteEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $description;
    public $user_name;
    public $_message;
    public $project_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $subject, $_message, $description, $project_id)
    {
        $this->user_name = $user_name;
        $this->description = $description;
        $this->subject = $subject;
        $this->_message = $_message;
        $this->project_id = $project_id;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $parameters = [
            'subject' => $this->subject,
            'user_name' => $this->user_name,
            '_message' => $this->_message,
            'project_id' => $this->project_id
        ];

        return $this->view('emails.task_note')->with($parameters);
    }
}
