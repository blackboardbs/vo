<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CVSubmitMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_name;
    public $subject;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $subject, $message, $file)
    {
        $this->user_name = $user_name;
        $this->subject = $subject;
        $this->message = $message;
        $this->file = $file;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $parameters = [
            'user_name'=> $this->user_name,
            'subject'=> $this->subject,
            'message'=> $this->message,
            'file'=> $this->file,
        ];

        return $this->view('emails.submit_cv')->with($parameters)->attach($this->file);
    }
}
