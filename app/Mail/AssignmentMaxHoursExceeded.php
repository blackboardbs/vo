<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignmentMaxHoursExceeded extends Mailable
{
    use Queueable, SerializesModels;

    public $assignment;
    public $timesheet;
    public $helpPortal;
    public $maxHoursPerDay;
    public $capacityPerDay;
    public $timelineHours;
    public $actualHours;

    public function __construct($assignment, $timesheet, $timelineHours, $helpPortal, $maxHoursPerDay, $capacityPerDay, $actualHours)
    {
        $this->assignment = $assignment;
        $this->timesheet = $timesheet;
        $this->helpPortal = $helpPortal;
        $this->maxHoursPerDay = $maxHoursPerDay;
        $this->capacityPerDay = $capacityPerDay;
        $this->timelineHours = $timelineHours;
        $this->actualHours = $actualHours;
    }

    public function build()
    {
        return $this->view('emails.assignment_max_hours_exceeded');
    }
}
