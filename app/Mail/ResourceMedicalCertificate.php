<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResourceMedicalCertificate extends Mailable
{
    use Queueable, SerializesModels;

    public $resource;
    public $siteName;
    public $helpPortal;
    public function __construct($resource, $siteName, $helpPortal)
    {
        $this->resource = $resource;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.medical_certificate');
    }
}
