<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class AdminSubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $details;
    /**
     * Create a new message instance.
     */
    public function __construct($user,$details)
    {
        $this->user = $user;
        $this->details = $details;
    }

    public function build()
    {
        return $this->view('emails.admin_subscription');
    }
}
