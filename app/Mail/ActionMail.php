<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user_name;
    public $subject;
    public $action;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $user_name, $action)
    {
        $this->subject = $subject;
        $this->user_name = $user_name;
        $this->action = $action;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $parameters = [
            'user_name'=>$this->user_name,
            'subject'=>$this->subject,
            'mail_body'=> $this->action
        ];

        return $this->view('emails.action_mail')->with($parameters);
    }
}
