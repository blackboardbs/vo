<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssessmentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $resource;
    protected $storage;
    public function __construct($customer, $resource, $storage)
    {
        $this->customer = $customer;
        $this->resource = $resource;
        $this->storage = $storage;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->subject("Assessment of ".$this->resource)->view('emails.assessment')->attach($this->storage);
    }
}
