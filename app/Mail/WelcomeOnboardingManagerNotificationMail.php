<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeOnboardingManagerNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $hr;
    public $appointing_manager;
    public $approving_manager;
    public $company;
    public $dear;
    public $user_typer;
    public $onboarding;
    public function __construct($user, $hr, $appointing_manager, $approving_manager, $company, $user_typer, $onboarding)
    {
        $this->user = $user;
        $this->hr = $hr;
        $this->appointing_manager = $appointing_manager;
        $this->approving_manager = $approving_manager;
        $this->company = $company;
        $this->user_typer = $user_typer;
        $this->onboarding =  $onboarding;

        $user_names = array_filter(
            array_values(
                array_unique([($this->hr->first_name??null), ($this->appointing_manager->first_name??null)])
            )
        );

        $this->dear = implode(", ",$user_names);
    }

    public function build()
    {
        return $this->view('emails.onboarding.welcome-managers');
    }
}
