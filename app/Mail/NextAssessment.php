<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NextAssessment extends Mailable
{
    use Queueable, SerializesModels;

    public $assessment;
    public $siteName;
    public $helpPortal;
    public function __construct($assessment, $siteName, $helpPortal)
    {
        $this->assessment = $assessment;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.next_assessment');
    }
}
