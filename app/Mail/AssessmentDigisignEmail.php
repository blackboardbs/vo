<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssessmentDigisignEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $resource;
    // protected $storage;
    protected $digisign_user_id;
    protected $_subject;

    public function __construct($_subject, $customer, $resource, /*$storage, */$digisign_user_id, $assessment_id)
    {
        $this->customer = $customer;
        $this->resource = $resource;
        // $this->storage = $storage;
        $this->digisign_user_id = $digisign_user_id;
        $this->assessment_id = $assessment_id;
        $this->_subject = $_subject;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $parameters = [
            'digisign_user_id' => $this->digisign_user_id,
            'assessment_id' => $this->assessment_id
        ];

        return $this->subject($this->_subject)->view('emails.assessment_digisign')->with($parameters);
    }
}
