<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnniversaryDate extends Mailable
{
    use Queueable, SerializesModels;

    public $anniversary;
    public $date;
    public function __construct($anniversary, $date)
    {
        $this->anniversary = $anniversary;
        $this->date = $date;
    }

    public function build()
    {
        return $this->view('emails.anniversary_date');
    }
}
