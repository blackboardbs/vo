<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class CustomerInvoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $customer;
    public $company;
    public $subject;
    public $body;
    protected $storage;
    public function __construct($customer, $company, $subject, $body, $storage)
    {
        $this->customer = $customer;
        $this->company = $company;
        $this->subject = $subject;
        $this->body = $body;
        $this->storage = $storage;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('emails.customer_invoice')->attach($this->storage);
    }
}
