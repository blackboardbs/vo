<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeaveApplication extends Mailable
{
    use Queueable, SerializesModels;

    public $teamManager;
    public $leave;
    public $projects;
    public $helpPortal;

    public function __construct($teamManager, $leave, $projects, $helpPortal)
    {
        $this->teamManager = $teamManager;
        $this->leave = $leave;
        $this->projects = $projects;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.leave_');
    }
}
