<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerBBBEECertificateExpiry extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $siteName;
    public $helpPortal;
    public function __construct($customer, $siteName, $helpPortal)
    {
        $this->customer = $customer;
        $this->siteName = $siteName;
        $this->helpPortal = $helpPortal;
    }

    public function build()
    {
        return $this->view('emails.customer_bbbee_certificate_expiry');
    }
}
