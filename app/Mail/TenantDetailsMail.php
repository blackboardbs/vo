<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\Config;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TenantDetailsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public User $user;
    public Company $company;
    //public Config $config;
    public function __construct(public string $tenant_url, public string $password)
    {
        $this->subject = "Welcome to Consulteaze";
    }

    /**
     * Get the message envelope.
     */
    public function build()
    {

        $this->user = User::first();
        $this->company = Company::first();
        return $this->view('emails.tenant')->with([
            'user' => $this->user,
            'company' => $this->company
        ]);
    }
}
