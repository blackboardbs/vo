<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VendorStatementMail extends Mailable
{
    use Queueable, SerializesModels;

    public $vendor_invoice;
    private $storage;
    public function __construct($vendor_invoice, $storage)
    {
        $this->vendor_invoice = $vendor_invoice;
        $this->storage = $storage;
    }

    public function build()
    {
        return $this->view('emails.vendor_statement')->attach($this->storage);
    }
}
