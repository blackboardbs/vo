<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnableEditAppointingManagerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $appointing_manager;
    public $onboarding;
    public $company;
    public function __construct($user, $appointing_manager, $onboarding)
    {
        $this->user = $user;
        $this->appointing_manager = $appointing_manager;
        $this->onboarding = $onboarding;
        $this->company = Config::with(['company' => function($company){
            $company->select(['id', 'company_name']);
        }])->select(['company_id'])->first()->company_name??(Company::first(['company_name'])->company_name??'Unknown Company');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.onboarding.enable-editing');
    }
}
