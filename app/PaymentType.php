<?php

namespace App;

use App\Models\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PaymentType extends Model
{
    use Sortable;

    public $sortable = ['id', 'description', 'status_id'];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
