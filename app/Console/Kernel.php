<?php

namespace App\Console;

use App\Console\Commands\ClockifyClient;
use App\Console\Commands\ClockifyProject;
use App\Console\Commands\ClockifyTask;
use App\Console\Commands\PublicHolidayCommand;
use App\Console\Commands\RecurringInvoiceCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UtilizationCron::class,
        Commands\UserAccounts::class,
        Commands\CompanyBBBEECertificateExpiry::class,
        Commands\ResourceMedicalCertificateExpiry::class,
        Commands\AnniversaryDate::class,
        Commands\EquipmentRetireDate::class,
        Commands\NextAssessment::class,
        Commands\TaskOverdue::class,
        Commands\CustomerBBBEECertificateExpiryDate::class,
        Commands\CustomerInvoiceDue::class,
        Commands\VendorBBBEECertificateExpiry::class,
        Commands\VendorInvoiceDue::class,
        Commands\ResourceUtilization::class,
        Commands\OutstandingTimesheets::class,
        Commands\LeaveOverdue::class,
        Commands\PlannedUtilization::class,
        Commands\assignmentCompletionWarning::class,
        Commands\DistributionListCommand::class,
        Commands\BillingPeriod::class,
        Commands\NextBillingPeriodCommand::class,
        ClockifyClient::class,
        ClockifyProject::class,
        ClockifyTask::class,
        PublicHolidayCommand::class,
        RecurringInvoiceCommand::class
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('command:distributionlist')
            ->everyFiveMinutes();
        $schedule->command('utilization:cron')->sundays()->at('4:00');
        $schedule->command('email:user_account')
            ->daily()->at('2:00');
        $schedule->command('email:companybbbeecertificateexpiry')
            ->daily()->at('2:00');
        $schedule->command('email:resource_medical_certificate_expiry')
            ->daily()->at('2:00');
        $schedule->command('email:anniversary')
            ->daily()->at('2:00');
        $schedule->command('email:equipment_retire')
            ->daily()->at('3:00');
        $schedule->command('email:next_assessment')
            ->daily()->at('3:00');
        $schedule->command('email:task_overdue')
            ->daily()->at('3:00');
        $schedule->command('email:customer_bbbee_certificate_expiry_date')
            ->daily()->at('3:00');
        $schedule->command('email:customer_invoice_due')
            ->daily()->at('3:00');
        $schedule->command('email:vendor_bbbee_certificate_expiry')
            ->daily()->at('3:00');
        $schedule->command('email:resource_utilization')
            ->daily()->at('4:00');
        $schedule->command('email:planned_utilization')
            ->daily()->at('4:00');
        $schedule->command('email:leave_overdue')
            ->daily()->at('4:00');
        $schedule->command('warning:assignment_completion_warning')
            ->daily()->at('4:00');
        $schedule->command('backup:run')->at('3:00');
        $schedule->command('send:outstanding_timesheet')
            ->weekly()->mondays()->at('5:00');
        $schedule->command('billing:period')
            ->daily()->at('5:00');
        $schedule->command('billing:nextperiod')
            ->weekly()->tuesdays()->at('5:00');
        $schedule->command('public:holidays')->quarterly()->at('6:00');
        $schedule->command('app:recurring-invoice')->daily()->at('8:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        //load cron

        //--/usr/local/bin/ea-php71 /home/blackwrd/wo_src/artisan utilization:cron >/dev/null 2>&1
        $this->load(base_path('/app/Console/Commands'));

        require base_path('routes/console.php');
    }
}
