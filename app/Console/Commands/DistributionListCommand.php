<?php

namespace App\Console\Commands;

use App\Models\DistributionList;
use App\Http\Controllers\DistributionListController;
use App\Mail\DistributionListEmail;
use App\Models\ScheduleRunFrequency;
use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class DistributionListCommand extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:distributionlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The Distribution List Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $distributionLists = DistributionList::orderBy('id')->where('status_id', '=', 1)->get();

        foreach ($distributionLists as $distributionList){
            $dateTimeNow = date('Y-m-d H:i');
            $next_run_date = date('Y-m-d H:i', strtotime($distributionList->next_run_date));

            if($next_run_date == $dateTimeNow){

                $from_list = [];
                $to_list = [];
                $from_emails = '';
                $from_html = '';
                $to_emails = '';
                $query_from_string = json_decode($distributionList->from_query, true);
                $query_to_string = json_decode($distributionList->to_query, true);

                $distributionListController = new DistributionListController();

                switch ($distributionList->from_query_type_id){
                    case 1:
                        $candidates = $distributionListController->getCandidatesData($query_from_string);
                        $candidates_counter = 0;

                        $from_html .= '<table width="100%" border="1"><tbody>';
                        $from_html .= '<tr><th>Name</th><th>Email</th></tr>';
                        $is_candidates = false;
                        foreach($candidates['cv'] as $candidate){
                            //$tmp_candidate = json_encode($candidate);
                            //$decode_candidate = json_decode($candidate, true);
                            $is_candidates = true;
                            $from_list[] = $candidate;
                            if($candidates_counter == 0){
                                $from_emails .= json_encode($candidate);
                            }
                            else
                            {
                                $from_emails .= ', '.(json_encode($candidate));
                            }

                            $from_html .= '<tr><td>'.(isset($candidate->user->first_name)?$candidate->user->first_name:'').' '.(isset($candidate->user->last_name)?$candidate->user->last_name:'').'</td><td>'.(isset($candidate->user->email)?$candidate->user->email:'').'</td></tr>';

                        }
                        $from_html .= '</tbody></table>';

                        if($is_candidates == false){
                            $from_html = '';
                        }

                    break;

                    case 2:
                        $job_specs = $distributionListController->getJobSpecData($query_from_string);
                        $job_specs_counter = 0;
                        foreach($job_specs as $job_spec){
                            $from_list[] = $job_spec;
                            if($job_specs_counter == 0){
                                $from_emails .= isset($job_spec->position_description) ? $job_spec->position_description : '';
                            }
                            else
                            {
                                $from_emails .= isset($job_spec->position_description) ? $job_spec->position_description : '';
                            }
                        }
                    break;

                    case 4:
                        // Uploaded excel list
                        // Todo: read this file, name and surname, first column must be the field names
                        $excelFile = storage_path('app'.DIRECTORY_SEPARATOR.'distributionlist'.DIRECTORY_SEPARATOR.$distributionList->from_query);
                        // Read excel file first correct format is column name, second column email
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                        $spreadsheet = $reader->load($excelFile);

                        // (3) READ CELLS
                        $sheet = $spreadsheet->getSheet(0);

                        $from_html .= '<table width="100%" border="1"><tbody>';
                        $from_html .= '<tr><th>Name</th><th>Email</th></tr>';

                        $counter = 1;
                        $exitFileFlagCounter = 0;
                        $isList = false;
                        while($exitFileFlagCounter < 5){
                            $cell = $sheet->getCell('A'.$counter);
                            $name = $cell->getValue();
                            $cell = $sheet->getCell('B'.$counter);
                            $email = $cell->getValue();

                            if((trim($email) === '') && (trim($name) === '')){
                                $exitFileFlagCounter++;;
                            } else {
                                $isList = true;
                                $to_list[$counter - 1]['name'] = $name;
                                $to_list[$counter - 1]['email'] = $email;

                                $from_html .= '<tr><td>'.$to_list[$counter - 1]['name'].'</td><td>'.$to_list[$counter - 1]['email'].'</td></tr>';
                            }

                            $counter++;
                        }
                        $from_html .= '</tbody></table>';

                    break;

                    case 5:
                        // From html upload
                        // No need to do anything here, the list will be uploaded automatically and saved to distribution config to be selected as a template
                        break;
                    break;
                }

                $scheduleRunFrequecy = ScheduleRunFrequency::find($distributionList->run_frequency_id);

                if($scheduleRunFrequecy->id != 41){

                    if($scheduleRunFrequecy->id == 1){
                        $next_run_date = null;
                    }

                    if($scheduleRunFrequecy->type == 'week'){
                        $next_run_date = date('Y-m-d H:i:s', strtotime(' + 1 week'));
                    }

                    if($scheduleRunFrequecy->type == 'month'){
                        $next_run_date = date('Y-m-d H:i:s', strtotime(' + 1 month'));
                    }

                    if($scheduleRunFrequecy->type == 'day'){
                        $next_run_date = date('Y-m-d H:i:s', strtotime(' + 1 day'));
                    }

                    if($scheduleRunFrequecy->day == 32){
                        $next_run_date = date('Y-m-t H:i:s', strtotime(' + 1 month'));
                    }

                }

                $distributionListNew = DistributionList::find($distributionList->id);
                $distributionListNew->next_run_date = $next_run_date;
                $distributionListNew->last_run_date = date('Y-m-d H:i:s');
                $distributionListNew->save();

                switch ($distributionList->to_query_type_id){
                    case 1:

                        $candidates = $distributionListController->getCandidatesData($query_to_string);
                        $candidates_counter = 0;
                        foreach($candidates['cv'] as $candidate){
                            $to_list[] = $candidate;
                            if($candidates_counter == 0){
                                //$to_emails .= isset($candidate->user->email) ? $candidate->user->email : '';
                                $to_emails .= json_encode($candidate);
                            }
                            else
                            {
                                //$to_emails .= ', '.(isset($candidate->user->email) ? $candidate->user->email : '');
                                $to_emails .= ', '.json_encode($candidate);
                            }
                        }

                        $from_html_with_params = str_replace('<EMAILLIST>', $from_html, $distributionList->email_body);
                        $from_html_with_params = str_replace('&lt;EMAILLIST&gt;', $from_html, $distributionList->email_body);

                        foreach ($to_list as $to_email){

                            $unsubscribe_link = env('APP_URL').'/unsubscribe?email='.$to_email->user->email;
                            $fullName = $to_email->user->first_name.' '.$to_email->user->last_name;

                            $from_html_with_params = str_replace('<USERFULLNAME>', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('<UNSUBSCRIBEEMAIL>', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $from_html_with_params = str_replace('&lt;USERFULLNAME&gt;', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('&lt;UNSUBSCRIBEEMAIL&gt;', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $html_email_body = $from_html_with_params;
                            $mail = Mail::to(trim($to_email->user->email));
                            $mail->send(new DistributionListEmail($fullName, 'Distribution List', $html_email_body));
                            //exit; //To - test in mailtrap, other you get a too many emails per second exception
                        }

                        break;

                    case 3:

                        $customer_preferences = $distributionListController->getCustomerPreferanceData($query_to_string);

                        $customer_preferences_counter = 0;
                        foreach ($customer_preferences['customer_preferences'] as $customer_preferences){
                            $to_list[] = $customer_preferences;
                            if($customer_preferences_counter == 0){
                                $to_emails .= isset($customer_preferences->customer->email) ? $customer_preferences->customer->email : '';
                            }
                            else
                            {
                                $to_emails .= ', '.(isset($customer_preferences->customer->email) ? $customer_preferences->customer->email : '');
                            }
                        }

                        $from_html_with_params = str_replace('<EMAILLIST>', $from_html, $distributionList->email_body);
                        $from_html_with_params = str_replace('&lt;EMAILLIST&gt;', $from_html, $distributionList->email_body);

                        foreach ($to_list as $to_email){

                            $fullName = $to_email->customer->customer_name;
                            $unsubscribe_link = env('APP_URL').'/unsubscribe?email='.$to_email->customer->email;

                            $from_html_with_params = str_replace('<USERFULLNAME>', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('<UNSUBSCRIBEEMAIL>', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $from_html_with_params = str_replace('&lt;USERFULLNAME&gt;', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('&lt;UNSUBSCRIBEEMAIL&gt;', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $html_email_body = $from_html_with_params;
                            $mail = Mail::to(trim($to_email->customer->email));
                            $mail->send(new DistributionListEmail($fullName, 'Distribution List', $html_email_body));
                        }

                        break;

                    case 4:
                        $excelFile = storage_path('app'.DIRECTORY_SEPARATOR.'distributionlist'.DIRECTORY_SEPARATOR.$distributionList->to_query);
                        // Read excel file first correct format is column name, second column email
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                        $spreadsheet = $reader->load($excelFile);

                        // (3) READ CELLS
                        $sheet = $spreadsheet->getSheet(0);

                        $counter = 1;
                        $exitFileFlagCounter = 0;
                        while($exitFileFlagCounter < 5){
                            $cell = $sheet->getCell('A'.$counter);
                            $name = $cell->getValue();
                            $cell = $sheet->getCell('B'.$counter);
                            $email = $cell->getValue();

                            if((trim($email) === '') && (trim($name) === '')){
                                $exitFileFlagCounter++;;
                            } else {
                                $to_list[$counter - 1]['name'] = $name;
                                $to_list[$counter - 1]['email'] = $email;
                            }

                            $counter++;
                        }

                        $from_html_with_params = str_replace('<EMAILLIST>', $from_html, $distributionList->email_body);
                        $from_html_with_params = str_replace('&lt;EMAILLIST&gt;', $from_html, $distributionList->email_body);

                        foreach($to_list as $to_contact_detail){
                            $fullName = $to_contact_detail['name'];
                            $unsubscribe_link = env('APP_URL').'/unsubscribe?email='.$to_contact_detail['email'];

                            $from_html_with_params = str_replace('<USERFULLNAME>', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('<UNSUBSCRIBEEMAIL>', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $from_html_with_params = str_replace('&lt;USERFULLNAME&gt;', $fullName, $from_html_with_params);
                            $from_html_with_params = str_replace('&lt;UNSUBSCRIBEEMAIL&gt;', '<a href="'.$unsubscribe_link.'" target="_blank">Click here to opt out</a>', $from_html_with_params);

                            $html_email_body = $from_html_with_params;
                            $mail = Mail::to(trim($to_contact_detail['email']));
                            $mail->send(new DistributionListEmail($fullName, $distributionList->subject, $html_email_body));
                        }

                        break;

                    case 5:

                        break;
                }

            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }

}
