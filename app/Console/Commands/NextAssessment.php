<?php

namespace App\Console\Commands;

use App\Models\AssessmentHeader;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class NextAssessment extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:next_assessment';

    protected $description = 'This command sends an email to the team manager and user whose assessment it\'s in 7 days';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $assessments = AssessmentHeader::where('next_assessment', '=', Carbon::now()->addDays(7)->toDateString())->get();
        foreach ($assessments as $assessment){
            $notification_name = 'Your next Assessment is due on '.$assessment->next_assessment;
            $notification = $utils->notifications($notification_name, route('assessment.create'));
            UserNotification::insert($utils->notification_id($assessment->resource_id, $notification));
            Mail::to(isset($assessment->user)?$assessment->user->email:null)->cc(isset($assessment->assessor)?$assessment->assessor->email:null)->send(new \App\Mail\NextAssessment($assessment, $utils->siteName(), $utils->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
