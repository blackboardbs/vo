<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class WarningsNotficationsCommand extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:warningsnotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is responsible for all system warnings and most system notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
