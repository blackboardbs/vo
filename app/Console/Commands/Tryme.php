<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\WebhookServer\WebhookCall;

class Tryme extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:tryme';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (!is_file(database_path('old_db_clone.sh'))){
            die("Database clone file not found");
        }

        shell_exec(database_path('db_clone.sh'));

        $password = Str::password(12);

        $user = User::where('email', 'stefan@menloweb.co.za')->first();
        $user->email = 'tryme@consulteaze.com';
        $user->password = Hash::make($password);
        $user->save();

        DB::statement("UPDATE `landing_pages` SET `page` = REPLACE(page, 'https://demo.consulteaze.com/', 'https://tryme.consulteaze.com/')");

        DB::statement("UPDATE `notifications` SET `link` = REPLACE(link, 'https://demo.consulteaze.com/', 'https://tryme.consulteaze.com/')");

        WebhookCall::create()
            ->url('https://subscription.consulteaze.com/api/tryme-key')
            ->payload(['key' => $password])
            ->useSecret('$2y$10$dfImg21eRHBCS3V./Dq76.l0HwW5vvgr6Krz.EvV19F1xmrncYaWq')
            ->dispatch();
    }
}
