<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class VendorBBBEECertificateExpiry extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:vendor_bbbee_certificate_expiry';

    protected $description = 'This Command notifies the account manager if bbbee certificate is about to expire';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $vendors = Vendor::where('bbbee_certificate_expiry', '=', Carbon::now()->addDays(7)->toDateString())->where('status_id', '=', 1)->get();
        foreach ($vendors as $vendor) {
            $notification_name = 'Your BBBEE Certificate of ' . (isset($vendor) ? $vendor->vendor_name : '') . ' on ' . $utils->siteName() . ' Web Office will expire on ' . $vendor->bbbee_certificate_expiry;
            $notification = $utils->notifications($notification_name, route('customer.edit', $vendor));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($vendor->email)->send(new \App\Mail\VendorBBBEECertificateExpiry($vendor, $utils->siteName(), $utils->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
