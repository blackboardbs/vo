<?php

namespace App\Console\Commands;

use App\Mail\ResourceMedicalCertificate;
use App\Models\Resource;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class ResourceMedicalCertificateExpiry extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:resource_medical_certificate_expiry';

    protected $description = 'This command notifies the system admin, manager and resource if the resource medical certificate is about to expire';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $adminEmails1 = $utils->adminEmail();
        $adminEmails = $adminEmails1;
        $siteName = $utils->siteName();
        $helpPortal = $utils->helpPortal;
        $resources = Resource::where('status_id', '=', 1)->whereNotNull('medical_certificate_expiry')->whereHas('user')->get();
        foreach ($resources as $resource){
            $manager_email = (isset($resource->manager)?$resource->manager->email:null);
            array_push($adminEmails, (!in_array($manager_email, $adminEmails)?$manager_email:null));
            $cleanEmails = array_filter(array_unique($adminEmails));
            $expiry_date = $utils->differenceInDays($resource->medical_certificate_expiry);
            $notification_name = 'Your Medical Certificate on '.$siteName.' Web Office will expire on '.$resource->medical_certificate_expiry;
            $notification = $utils->notifications($notification_name, route('resource.show', $resource));
            switch ($expiry_date)
            {
                case 29:
                    UserNotification::insert($utils->notification_id($resource->user_id, $notification));
                    Mail::to($resource->user->email)->cc($cleanEmails)->send(new ResourceMedicalCertificate($resource, $siteName, $helpPortal));
                    break;
                case 6:
                    UserNotification::insert($utils->notification_id($resource->user_id, $notification));
                    Mail::to($resource->user->email)->cc($cleanEmails)->send(new ResourceMedicalCertificate($resource, $siteName, $helpPortal));
                    break;
                case 1:
                    UserNotification::insert($utils->notification_id($resource->user_id, $notification));
                    Mail::to($resource->user->email)->cc($cleanEmails)->send(new ResourceMedicalCertificate($resource, $siteName, $helpPortal));
                    break;
            }
            if (count($adminEmails1) < count($cleanEmails))
                array_pop($adminEmails);
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
