<?php

namespace App\Console\Commands;

use App\Models\CustomerInvoice;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class CustomerInvoiceDue extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:customer_invoice_due';

    protected $description = 'This command will send an email to system admin when an invoice is due today';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $util = new Utils();
        $customer_invoices = CustomerInvoice::where('due_date', '=', $util->date)->where('bill_status', '=', 1)->get();
        foreach ($customer_invoices as $invoice){
            $notification_name = 'Note that a Customer Invoice is due for payment today.';
            $notification = $util->notifications($notification_name, route('customer.invoice_show', $invoice));
            UserNotification::insert($util->notification_id(null, $notification));
            Mail::to($util->adminEmail())->send(new \App\Mail\CustomerInvoiceDue($invoice,$util->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
