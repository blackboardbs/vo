<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class UpdateDatabase extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:update {--table=} {--column=} {--type=} {--default=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a column to a table on all databases';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Schema::table($this->option('table'), function($table)
        {
            $type = $this->option('type');
            if($this->option('default')){
                $table->$type($this->option('column'))->default($this->option('default'));
            } else {
                $table->$type($this->option('column'))->nullable();
            }
        });
    }

    public function getTenants()
    {
        return Tenant::all();
    }
}
