<?php

namespace App\Console\Commands;

use App\Enum\Status;
use App\Models\Assignment;
use App\Models\BillingPeriod as Period;
use App\Models\Config;
use App\Mail\EndOfBillingPeriod;
use App\Models\Tenant;
use App\Models\WarningMaintenance;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class BillingPeriod extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:period';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a notification to all resources linked to an Active Assignment on the Billing Period End Date of the current period based on the Billing Cycle linked to the project.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        $warningMaintanance = WarningMaintenance::find(24);

        if ($warningMaintanance->status_id == Status::ACTIVE->value)
        {
            $config = Config::select('billing_cycle_id')->first();

            $active_assignments = Assignment::with(['consultant' => function ($user) {
                $user->select('id', 'first_name', 'last_name', 'email');
            }, 'project' => function ($project) {
                $project->select('id', 'billing_cycle_id', 'customer_id');
            }, 'project.customer' => function ($customer) {
                $customer->select('id', 'billing_cycle_id', 'customer_name');
            }])
                ->select('employee_id', 'project_id')
                ->where('assignment_status', 3)
                ->whereHas('consultant')
                ->latest()->get()->unique('employee_id')->values();

            foreach ($active_assignments as $assignment) {
                $billing_period = Period::query();
                if ($assignment->project?->billing_cycle_id) {
                    $billing_cycle_id = $assignment->project->billing_cycle_id;
                } elseif ($assignment->project?->customer?->billing_cycle_id) {
                    $billing_cycle_id = $assignment->project->customer->billing_cycle_id;
                } else {
                    $billing_cycle_id = $config->billing_cycle_id;
                }

                $billing_period_timesheet = Period::with('billingCycle')->select('end_date')
                    ->where('billing_cycle_id', $billing_cycle_id)
                    ->whereMonth('end_date', now()->month)
                    ->whereYear('end_date', now()->year)
                    ->first();

                if (isset($billing_period_timesheet->end_date)) {
                    $data = [
                        'user' => $assignment->consultant->name(),
                        'end_date' => $billing_period_timesheet->end_date,
                        'customer' => $assignment->project->customer->customer_name,
                    ];

                    if (now()->addDay()->isWeekend() && ($billing_period_timesheet->end_date == now()->addDay()->toDateString())) {
                        Mail::to($assignment->consultant->email)->send(new EndOfBillingPeriod($data));
                    }

                    if (now()->addDay()->isWeekend() && ($billing_period_timesheet->end_date == now()->addDays(2)->toDateString())) {
                        Mail::to($assignment->consultant->email)->send(new EndOfBillingPeriod($data));
                    }

                    if ($billing_period_timesheet->end_date == now()->toDateString()) {
                        Mail::to($assignment->consultant->email)->send(new EndOfBillingPeriod($data));
                    }
                }
            }
        }
    }
    public function getTenants(): Collection
    {
        return Tenant::where('id', '!=', '94c9fbc8-4027-49d2-a1d6-9c560e375e5b')->get();
    }

}
