<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Mail\OverdueTimesheet;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class OutstandingTimesheets extends Command
{
    use TenantAwareCommand;
    
    protected $signature = 'send:outstanding_timesheet';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $utils = new Utils();
        $carbon = Carbon::now();
        $outstanding_timesheets = $utils->outstandingTimesheets(1);
        $employee_id = collect($outstanding_timesheets)->map(function ($employee) {
            return $employee->employee_id;
        })->unique()->values();
        $assignments = [];
        $user_email = '';
        $manager_email = '';
        $year_week = $carbon->now()->subWeek()->year.(($carbon->now()->subWeek()->weekOfYear < 10) ? '0'.$carbon->now()->subWeek()->weekOfYear : $carbon->now()->subWeek()->weekOfYear);
        $start_of_week = $carbon->now()->subWeek()->startOfWeek()->toDateString();

        foreach ($employee_id as $id) {
            foreach ($outstanding_timesheets as $timesheet) {
                if ($timesheet->employee_id == $id) {
                    $user_email = $timesheet->resource->email ?? null;
                    $manager_email = $timesheet->resource_user->manager->email ?? null;
                    $resource = ($timesheet->resource) ? $timesheet->resource : null;
                    array_push($assignments, Assignment::find($timesheet->id));
                }
            }

            $notification_name = 'Outstanding Timesheets.';
            $notification = $utils->notifications($notification_name, route('timesheet.create'));
            UserNotification::insert($utils->notification_id($id, $notification));

            Mail::to(array_filter(array_unique([$user_email, $manager_email])))->send(new OverdueTimesheet(array_unique($assignments), $resource, $year_week, $start_of_week, $utils->helpPortal));
            $assignments = [];
        }
    }
    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
