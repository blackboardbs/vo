<?php

namespace App\Console\Commands;

use App\Models\Config;
use App\Models\Resource;
use App\Models\Tenant;
use App\Models\Utilization;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class UtilizationCron extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'utilization:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A command to calculate Utilization';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $conf = Config::orderBy('id')->first(['utilization_method']);

        if ($conf?->utilization_method == '2') {
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);

            $datediff = now()->addDays(6)->diffInDays(now());

            $weekarr = [];

            for ($i = 0; $i <= $datediff; $i++) {
                $date = now()->addDay()->addDays($i)->toDateString();
                $year = Carbon::parse($date)->year;
                $week = (Carbon::parse($date)->weekOfYear < 10 ? '0' . Carbon::parse($date)->weekOfYear : Carbon::parse($date)->weekOfYear);
                $yrwk = $year . $week;

                $check = Utilization::where('yearwk', '=', $yrwk)->get();

                if (empty($check) || count($check) == '0') {
                    //if (is_array($weekarr) && !in_array($week, $weekarr)) {
                    $resources = Resource::where('util', '1')->where('project_type', '1')->whereRaw('? between util_date_from and util_date_to', $date)->get();
                    $cost_resources = Resource::where('util', '1')->where('project_type', '2')->whereRaw('? between util_date_from and util_date_to', $date)->get();

                    $weekarr[$i] = $week;

                    $utilization = new Utilization();
                    $utilization->yearwk = $yrwk;
                    $utilization->res_no = count($resources);
                    $utilization->cost_res_no = count($cost_resources);
                    $utilization->wk_hours = '40';
                    $utilization->cost_wk_hours = '40';
                    $utilization->status_id = '1';
                    $utilization->save();
                            //}
                }
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
