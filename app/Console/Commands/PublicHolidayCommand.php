<?php

namespace App\Console\Commands;

use App\Models\PublicHoliday;
use App\Models\Tenant;
use App\Traits\Holiday\HolidaysTrait;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class PublicHolidayCommand extends Command
{
    use HolidaysTrait, TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'public:holidays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save public holidays of the year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $holidays = PublicHoliday::where('year', now()->year)->get();

        if ($holidays->isEmpty()){
            logger($this->southAfricanPublicHolidays());

            foreach ($this->southAfricanPublicHolidays() as $day){
                $holiday = new PublicHoliday();
                $holiday->date = $day->date;
                $holiday->holiday_name = $day->holiday_name;
                $holiday->year = now()->year;
                $holiday->type = $day->type;
                $holiday->save();
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
