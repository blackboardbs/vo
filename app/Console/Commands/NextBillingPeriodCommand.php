<?php

namespace App\Console\Commands;

use App\Models\BillingCycle;
use App\Models\BillingPeriod as Period;
use App\Models\Config;
use App\Mail\NextBillingPeriod;
use App\Models\Project;
use App\Models\RoleUser;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class NextBillingPeriodCommand extends Command
{
    use TenantAwareCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:nextperiod';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warn if no billing period exist for an active cycle for the next week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $config = Config::select('billing_cycle_id')->first();

        /*$administrators = RoleUser::with(['user' => function ($user) {
            $user->select('id', 'first_name', 'last_name', 'email');
        }])->where('role_id', 1)->get()->map(function ($role) {
            return $role->user->email;
        })->filter()->unique()->values();*/

        $administrators = User::whereHas('roles', fn($role) => $role->where('id', 1))->pluck('email')->toArray();

        $projects = Project::with(['customer' => function ($customer) {
            $customer->select('id', 'billing_cycle_id');
        }])->select('id', 'customer_id', 'billing_cycle_id')
            ->where('status_id', 3)
            ->get()->unique('id');

        $billing_cycles = [];

        foreach ($projects as $project) {
            if ($project->billing_cycle_id) {
                $billing_cycle_id = $project->billing_cycle_id;
            } elseif ($project->customer->billing_cycle_id) {
                $billing_cycle_id = $project->customer->billing_cycle_id;
            } else {
                $billing_cycle_id = $config->billing_cycle_id;
            }

            array_push($billing_cycles, $billing_cycle_id);
        }

        $billing_cycles = BillingCycle::with('billingPeriods')->where('status_id', 1)->find(array_unique($billing_cycles));

        foreach ($billing_cycles as $cycle) {
            $periods = $cycle->billingPeriods->map(
                function ($period) {
                    return $period->id;
                }
            )->toArray();

            $current_period = Period::select('id', 'end_date')
                ->where('billing_cycle_id', $cycle->id)
                ->whereMonth('end_date', now()->month)
                ->whereYear('end_date', now()->year)
                ->first();

            $position = array_search($current_period->id, $periods);

            if ($position !== false) {
                if ($position === (count($periods) - 1) && $current_period->end_date > now()->addWeek()->toDateString()) {
                    Mail::to($administrators)->send(new NextBillingPeriod([
                        'billing_cycle_name' => $cycle->name,
                        'billing_period_end_date' => $current_period->end_date,
                    ]));
                }
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
