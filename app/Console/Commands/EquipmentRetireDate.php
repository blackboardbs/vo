<?php

namespace App\Console\Commands;

use App\Models\AssetRegister;
use App\Mail\RetireDate;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class EquipmentRetireDate extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:equipment_retire';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $adminEmails = $utils->adminEmail();

        $assets = AssetRegister::where('status_id', '=', 1)->where('retire_date', '=', Carbon::now()->addDays(30)->toDateString())->get();
        foreach ($assets as $asset){
            $notification_name = 'Equipment retire warning.';
            $notification = $utils->notifications($notification_name, route('assetreg.show', $asset));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($adminEmails)->send(new RetireDate($asset, $utils->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
