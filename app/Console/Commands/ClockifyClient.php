<?php

namespace App\Console\Commands;

use App\API\Clockify\ClockifyAPI;
use App\API\Clockify\ClockifyTrait;
use App\Models\Customer;
use App\Jobs\ClockifyJob;
use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class ClockifyClient extends Command
{
    use ClockifyTrait, TenantAwareCommand;

    protected $signature = 'clockify:clients';

    protected $description = 'Add new clients to clockify';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ClockifyAPI $clockify): void
    {
        abort_if(!config('app.clockify.is_enabled'), ClockifyAPi::STATUS_FORBIDDEN);

        $clients = Customer::select(['id','customer_name'])->where('status', 1)->get()->map(function($client){
            return [
                "id" => $client->id,
                "name" => $client->customer_name
            ];
        })->toArray();

        $diff_clients = $clockify->arrayMismatch($clients, $clockify->get('/clients?archived=false'));

        foreach ($diff_clients as $client){
            ClockifyJob::dispatch($client)->delay(1);
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
