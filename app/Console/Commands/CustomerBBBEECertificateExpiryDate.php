<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Mail\CustomerBBBEECertificateExpiry;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class CustomerBBBEECertificateExpiryDate extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:customer_bbbee_certificate_expiry_date';

    protected $description = 'This command notifies the account manager when a BBBEE certificate is about to expire';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $customers = Customer::where('bbbee_certificate_expiry', '=', Carbon::now()->addDays(7)->toDateString())->where('status', '=', 1)->get();
        foreach ($customers as $customer) {
            $notification_name = 'Your BBBEE Certificate of ' . (isset($customer) ? $customer->customer_name : '') . ' on ' . $utils->siteName() . ' Web Office will expire on ' . $customer->bbbee_certificate_expiry;
            $notification = $utils->notifications($notification_name, route('customer.edit', $customer));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($customer->email)->send(new CustomerBBBEECertificateExpiry($customer, $utils->siteName(), $utils->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
