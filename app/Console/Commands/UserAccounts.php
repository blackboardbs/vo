<?php

namespace App\Console\Commands;

use App\Mail\UserAccount;
use App\Models\Tenant;
use App\Models\User;
use App\Models\UserNotification;
use App\Utils;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class UserAccounts extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:user_account';

    protected $description = 'Command that sends an email to user and system admin within a given days';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $users = User::where('status_id', '=', 1)->where('login_user', '=', 1)->get();
        $utils = new Utils();
        $siteName = $utils->siteName();
        $helpPortal = $utils->helpPortal;
        $adminEmails = $utils->adminEmail();

        foreach ($users as $user) {
            $expiry_date = $utils->differenceInDays($user->expiry_date);
            $notification_name = 'Your Access to '.$siteName.' Web Office will expire on '.$user->expiry_date.' Please contact your System Administrator to renew your access.';
            $notification = $utils->notifications($notification_name, route('profile', $user));
            switch ($expiry_date) {
                case 29:
                    UserNotification::insert($utils->notification_id($user->id, $notification));
                    Mail::to($user->email)->cc($adminEmails)->send(new UserAccount($user, $siteName, $helpPortal));
                    break;
                case 6:
                    UserNotification::insert($utils->notification_id($user->id, $notification));
                    Mail::to($user->email)->cc($adminEmails)->send(new UserAccount($user, $siteName, $helpPortal));
                    break;
                case 1:
                    UserNotification::insert($utils->notification_id($user->id, $notification));
                    Mail::to($user->email)->cc($adminEmails)->send(new UserAccount($user, $siteName, $helpPortal));
                    break;
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
