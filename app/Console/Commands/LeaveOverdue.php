<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Models\Config;
use App\Models\Leave;
use App\Mail\LeaveApplication;
use App\Models\Tenant;
use App\Models\User;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class LeaveOverdue extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:leave_overdue';

    protected $description = 'This command notifies approver if he should approve leave';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $configs = Config::first()->days_to_approve_leave;
        $utils = new Utils();
        $leaves = Leave::whereNull('leave_status')->where('status', '=', 1)->get();
        foreach ($leaves as $leave){
            $date_cr = Carbon::parse($leave->created_at)->addDays($configs)->toDateString();
            $manager = isset(User::find($leave->emp_id)->resource->manager)?User::find($leave->emp_id)->resource->manager:null;
            $projects = Assignment::where('employee_id', '=', $leave->emp_id)->whereIn('assignment_status', [2,3])->get();
            $notification_name = 'Leave needs approval.';
            $notification = $utils->notifications($notification_name, route('leave.edit', $leave));
            if ($date_cr < $utils->date){
                UserNotification::insert($utils->notification_id($leave->emp_id, $notification));
                Mail::to($manager->email)->send(new LeaveApplication($manager, $leave, $projects, $utils->helpPortal));
            }

        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
