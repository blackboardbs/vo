<?php

namespace App\Console\Commands;

use App\Models\Task;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class TaskOverdue extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:task_overdue';

    protected $description = 'This command notifies user when the task is overdue';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $tasks = Task::where('end_date', '=', $utils->date)->whereIn('status', [2,3])->get();
        foreach ($tasks as $task){
            $task_user_email = isset($task->consultant)?$task->consultant->email:null;
            $creator_email = isset($task->creator)?$task->creator->email:null;
            //$notification_name = 'Task #'.$task->id.' is overdue or will become overdue.';
            //$notification = $utils->notifications($notification_name, route('utilization.create','week'));
            //UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($task_user_email)->cc($creator_email)->send(new \App\Mail\TaskOverdue($task, $utils->helpPortal));

        }


    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
