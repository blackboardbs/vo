<?php

namespace App\Console\Commands;

use App\Models\Resource;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class ResourceUtilization extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:resource_utilization';

    protected $description = 'This command notifies system admin when resource utilization is about to end';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $resources = Resource::where('status_id', '=', 1)->where('util_date_to', '=', Carbon::now()->addDays(7)->toDateString())->get();

        foreach ($resources as $resource){
            $notification_name = 'Utilization for Resource '.(isset($resource->user)?$resource->user->first_name:'').' is about to expire';
            $notification = $utils->notifications($notification_name, route('resource.show', $resource));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($utils->adminEmail())->send(new \App\Mail\ResourceUtilization($resource, $utils->siteName(), $utils->helpPortal));
        }

    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
