<?php

namespace App\Console\Commands;

use App\Models\Anniversary;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class AnniversaryDate extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:anniversary';

    protected $description = 'This Command sends an email to System admin and user about any anniversary';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $adminEmails = $utils->adminEmail();
        $anniversaries = Anniversary::get();

        foreach ($anniversaries as $anniversary) {
            if (Carbon::parse($anniversary->anniversary_date)->isBirthday()) {
                $notification_name = 'Congratulations on the '.(isset($anniversary->anniversary_type) ? $anniversary->anniversary_type->description : '').' of '.$anniversary->name.' on '.$utils->date;
                $notification = $utils->notifications($notification_name, route('anniversary.show', $anniversary));
                UserNotification::insert($utils->notification_id($anniversary->emp_id, $notification));
                Mail::to(isset($anniversary->resource) ? $anniversary->resource->email : '')->cc($adminEmails)->send(new \App\Mail\AnniversaryDate($anniversary, $utils->date));
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
