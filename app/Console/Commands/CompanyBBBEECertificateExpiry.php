<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Mail\companyBBBEECertificate;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class CompanyBBBEECertificateExpiry extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:companybbbeecertificateexpiry';

    protected $description = 'Command that sends an email when company bbbee certificate is about to expire';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $companies = Company::where('status_id', '=', 1)->get();
        $utils = new Utils();
        $helpPortal = $utils->helpPortal;
        $siteName = $utils->siteName();
        foreach ($companies as $company){
            $expiry_date = $utils->differenceInDays($company->bbbee_certificate_expiry);
            $notification_name = 'Your BBBEE Certificate of '.(isset($company)?$company->company_name:'').' on '.$siteName.' Web Office will expire on '.$company->bbbee_certificate_expiry;
            $notification = $utils->notifications($notification_name, route('company.edit', $company));
            if ($expiry_date == 6 && isset($company->bbbee_certificate_expiry)){
                UserNotification::insert($utils->notification_id(null, $notification));
                Mail::to($company->email)->send(new companyBBBEECertificate($company,$helpPortal, $siteName));
            }
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
