<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use App\Models\VendorInvoice;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class VendorInvoiceDue extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:vendor_invoice_due';

    protected $description = 'This command notifies the system admin about any vendor invoices that are due';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $vendorInvoices = VendorInvoice::where('vendor_invoice_status', '=', 1)->where('vendor_due_date', '=', $utils->date)->get();
        $adminEmails = $utils->adminEmail();
        foreach ($vendorInvoices as $invoice){
            $notification_name = 'Note that a Vendor Invoice is due for payment today.';
            $notification = $utils->notifications($notification_name, route('view.invoice', $invoice));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($adminEmails)->send(new \App\Mail\VendorInvoiceDue($invoice,$utils->helpPortal));
        }

    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
