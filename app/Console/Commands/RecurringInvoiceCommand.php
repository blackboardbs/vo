<?php

namespace App\Console\Commands;

use App\API\Currencies\Currency;
use App\Enum\InvoiceType;
use App\Jobs\SendCustomerInvoiceEmailJob;
use App\Models\CustomerInvoice;
use App\Models\Document;
use App\Models\InvoiceItems;
use App\Models\Project;
use App\Models\Tenant;
use App\Models\User;
use App\Models\VendorInvoice;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Browsershot\Exceptions\FileDoesNotExistException;
use Spatie\LaravelPdf\Facades\Pdf;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class RecurringInvoiceCommand extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:recurring-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate recurring non timesheet invoices based on frequencies until the end date or occurrences reach zero';

    /**
     * Execute the console command.
     */
    public function handle(Currency $currency)
    {
        CustomerInvoice::with(['nonTimesheetCustomerInvoices', 'recurring'])
            ->where('bill_status', 1)
            ->whereHas('recurring', fn($recurring) => $recurring->where('interval', '>', 0)->whereNotNull('scheduled_at'))
            ->get()
            ->each(function($customerInvoice) use ($currency) {

                $recurring = $customerInvoice->recurring;
                $invoiceDate = $customerInvoice->frequencyDate($recurring);
                $remainingInvoices = $customerInvoice->invoicesRemaining($recurring, $customerInvoice->histories()->count());

                $shouldCreateInvoice = is_null($recurring->last_invoiced_at) ? $recurring->first_invoice_at <= now()->toDateString() : $invoiceDate <= now()->toDateString();

                if ($shouldCreateInvoice && ($remainingInvoices > 0)) {
                     $customer_invoice = CustomerInvoice::latest()->first();

                     $invoiceNumber = $customer_invoice->invoiceNumberString();

                     $customer_invoice = new CustomerInvoice();
                     $customer_invoice->customer_id = $customerInvoice->customer_id;
                     $customer_invoice->customer_invoice_number = $invoiceNumber;
                     $customer_invoice->invoice_date = now()->toDateString();
                     $customer_invoice->due_date = now()->addDays($recurring?->due_days_from_invoice_date)->toDateString();
                     $customer_invoice->company_id = $customerInvoice->company_id;
                     $customer_invoice->paid_date = null;
                     $customer_invoice->customer_unallocate_date = null;
                     $customer_invoice->invoice_value = $customerInvoice->nonTimesheetCustomerInvoices?->sum('total');
                     $customer_invoice->bill_status = 1;
                     $customer_invoice->invoice_notes = $customerInvoice->invoice_notes;
                     $customer_invoice->inv_ref = $customerInvoice->inv_ref;
                     $customer_invoice->cust_inv_ref = $customerInvoice->cust_inv_ref;
                     $customer_invoice->project_id = $customerInvoice->project_id;
                     $customer_invoice->resource_id = $customerInvoice->resource_id;
                     $customer_invoice->currency = $customerInvoice->currency;
                     $customer_invoice->conversion_rate = $currency->conversionRate($customerInvoice->currency)['rate']??0;
                     $customer_invoice->save();

                     $customerInvoice->nonTimesheetCustomerInvoices->each(function($nonTimesheetCustomerInvoice) use ($customer_invoice) {
                         $this->invoiceItems($customer_invoice->id, $nonTimesheetCustomerInvoice);
                     });

                     $this->updateRecurring($customerInvoice);

                     $customerInvoice->histories()->attach($customer_invoice->id, ['type' => InvoiceType::Customer->value]);

                     $customerInvoice = $customer_invoice;

                     $parameters = [
                         'customerInvoice' => $customerInvoice,
                         'customerInvoiceItems' => $customerInvoice->nonTimesheetCustomerInvoices,
                         'contact_person' => $customerInvoice->contact(($customerInvoice->project ?? new Project()), $customerInvoice->customer),
                     ];

                     try {
                         $emails = isset($recurring->emails) ? explode(",", $recurring->emails) : [];

                         if (isset($customerInvoice->sysConfig()->invoice_body_msg)) {
                             $body = $customerInvoice->sysConfig()->invoice_body_msg;
                         } else {
                             $body = 'Please find Accounts Receivable invoice attached for your attention';
                         }

                         $storage = $customer_invoice->invoiceStoragePath('customer')?->storage;

                         Pdf::view('customer.invoice.standardinvoice.non_timesheet_pdf', $parameters)
                             ->format('a4')
                             ->save($storage);

                         $document = new Document();
                         $document->document_type_id = 5;
                         $document->name = 'Customer Tax Invoice';
                         $document->file = $customer_invoice->invoiceStoragePath('customer')?->file_name;
                         $document->creator_id = User::first()?->id;
                         $document->owner_id = isset($customerInvoice->resource->employee_id) ? $customerInvoice->resource->employee_id : User::first()?->id;
                         $document->reference_id = 5;
                         $document->save();
                         $subject = $recurring->email_subject ?? ($customerInvoice->customer?->customer_name.' Accounts Recievable Invoice');

                         activity()->on($customerInvoice)->withProperties(['customer_name' => ($customerInvoice->customer) ? $customerInvoice->customer->customer_name : ''])->log('emailed');

                         SendCustomerInvoiceEmailJob::dispatch(array_filter($emails), $customerInvoice->customer, $customerInvoice->company, $subject, $body, $storage);

                     } catch (FileDoesNotExistException $e) {
                         report($e);
                     }
                }
            });

        VendorInvoice::with(['nonTimesheetVendorInvoices', 'recurring'])->where('vendor_invoice_status', 1)->whereHas('recurring',
            fn($recurring) => $recurring->where('interval', '>', 0)->whereNotNull('scheduled_at'))
            ->get()
            ->each(function($vendorInvoice) use ($currency) {
                $recurring = $vendorInvoice->recurring;
                $invoiceDate = $vendorInvoice->frequencyDate($recurring);
                $remainingInvoices = $vendorInvoice->invoicesRemaining($recurring, $vendorInvoice->histories()->count());

                if ((is_null($recurring->last_invoiced_at) || $invoiceDate <= now()->toDateString()) && ($remainingInvoices > 0)) {

                    $vendor_invoice = new VendorInvoice();
                    $vendor_invoice->vendor_id = $vendorInvoice->vendor_id;
                    $vendor_invoice->vendor_invoice_date = now()->toDateTimeString();
                    $vendor_invoice->vendor_due_date = now()->addDays($recurring->due_days_from_invoice_date)->toDateTimeString();
                    $vendor_invoice->company_id = $vendorInvoice->company_id;
                    $vendor_invoice->vendor_paid_date = null;
                    $vendor_invoice->vendor_unallocate_date = null;
                    $vendor_invoice->vendor_invoice_value = $vendorInvoice->nonTimesheetVendorInvoices?->sum('total');
                    $vendor_invoice->vendor_invoice_status = 1;
                    $vendor_invoice->vendor_invoice_note = $vendorInvoice->vendor_invoice_note;
                    $vendor_invoice->vendor_invoice_ref = $vendorInvoice->vendor_invoice_ref;
                    $vendor_invoice->vendor_reference = $vendorInvoice->vendor_reference;
                    $vendor_invoice->project_id = $vendorInvoice->project_id;
                    $vendor_invoice->resource_id = $vendorInvoice->resource_id;
                    $vendor_invoice->currency = $vendorInvoice->currency;
                    $vendor_invoice->conversion_rate = $currency->conversionRate($vendorInvoice->currency)['rate']??0;
                    if (! isset($vendor_invoice->sysConfig()->vendor_invoice_start_number)) {
                        $vendor_invoice->save();
                    }else{
                        $vendor_invoice->saveInvoiceNumber();
                    }

                    $vendorInvoice->nonTimesheetVendorInvoices->each(function($nonTimesheetVendorInvoice) use ($vendor_invoice) {
                        $this->invoiceItems($vendor_invoice->id, $nonTimesheetVendorInvoice);
                    });

                    $this->updateRecurring($vendorInvoice);

                    $vendorInvoice->histories()->attach($vendor_invoice->id, ['type' => InvoiceType::Vendor->value]);

                    $vendorInvoice = $vendor_invoice;

                    $parameters = [
                        'vendorInvoice' => $vendorInvoice,
                        'vendorInvoiceItems' => $vendorInvoice->nonTimesheetVendorInvoices,
                    ];

                    try {
                        $emails = explode(',', $recurring->emails) ?? [];

                        if ($vendorInvoice->sysConfig()?->invoice_body_msg) {
                            $body = $vendorInvoice->sysConfig()?->invoice_body_msg;
                        } else {
                            $body = 'Please find Accounts Receivable invoice attached for your attention';
                        }

                        $storage = $vendor_invoice->invoiceStoragePath('vendor')?->storage;

                        Pdf::view('vendor.vendor_invoice_function.non_timesheet_pdf', $parameters)
                            ->format('a4')
                            ->save($storage);

                        $document = new Document();
                        $document->document_type_id = 5;
                        $document->name = 'Vendor Tax Invoice';
                        $document->file = $vendor_invoice->invoiceStoragePath('vendor')?->file_name;
                        $document->creator_id = User::first()->id??1;
                        $document->owner_id = $vendorInvoice->resource?->employee_id;
                        $document->reference_id = 5;
                        $document->save();
                        $subject = $recurring->email_subject ?? ($vendorInvoice->vendor?->vendor_name.' Accounts Recievable Invoice');

                        activity()->on($vendorInvoice)->withProperties(['customer_name' => $vendorInvoice->vendor?->vendor_name])->log('emailed');

                        SendCustomerInvoiceEmailJob::dispatch(array_filter($emails), $vendorInvoice->vendor, $vendorInvoice->company, $subject, $body, $storage);
                    } catch (FileDoesNotExistException $e) {
                        report($e);
                    }
                }
            });
    }

    private function invoiceItems(int $invoiceId, InvoiceItems $invoiceItems): void
    {
        $invoiceItem = new InvoiceItems();
        $invoiceItem->invoice_id = $invoiceId;
        $invoiceItem->invoice_type_id = $invoiceItems->invoice_type_id;
        $invoiceItem->description = $invoiceItems->description;
        $invoiceItem->quantity = $invoiceItems->quantity;
        $invoiceItem->price_excl = $invoiceItems->price_excl;
        $invoiceItem->vat_code = $invoiceItems->vat_code;
        $invoiceItem->vat_percentage = $invoiceItems->vat_percentage;
        $invoiceItem->discount_amount = $invoiceItems->discount_amount;
        $invoiceItem->total = $invoiceItems->total;
        $invoiceItem->creator_id = $invoiceItems->creator_id;
        $invoiceItem->status_id = 1;
        $invoiceItem->created_at = now();
        $invoiceItem->save();
    }

    public function updateRecurring(VendorInvoice|CustomerInvoice $invoice): void
    {
        $invoice->recurring()->update([
            'last_invoiced_at' => now()->toDateTimeString()
        ]);
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
