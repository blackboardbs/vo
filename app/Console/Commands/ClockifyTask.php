<?php

namespace App\Console\Commands;

use App\API\Clockify\ClockifyAPI;
use App\Jobs\ClockifyTaskJob;
use App\Models\Project;
use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class ClockifyTask extends Command
{
    use TenantAwareCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clockify:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push the tasks from web office and link them to correct project in clockify';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(ClockifyAPI $clockify): void
    {
        abort_if(!config('app.clockify.is_enabled'), ClockifyAPi::STATUS_FORBIDDEN);

        $projects = $clockify->formatProject(collect($clockify->projects()), false);

        foreach ($projects as $project){
            $local_project = Project::with(['tasks' => function($task){
                return $task->select(['id', 'project_id', 'description']);
            }])->find($project["note"]);

            $tasks = $local_project->tasks??[];

            foreach ($tasks as $task){
                ClockifyTaskJob::dispatch(($task->id." - ".$task->description), $project["id"])->delay(1);
            }
        }

    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
