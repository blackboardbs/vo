<?php

namespace App\Console\Commands;

use App\API\Clockify\ClockifyAPI;
use App\Jobs\ClockifyProjectJob;
use App\Models\Project;
use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class ClockifyProject extends Command
{
    use TenantAwareCommand;

    protected $signature = 'clockify:projects';


    protected $description = 'Get the list of projects that are in progress from web office and another list from clockify and compare them. If web office has projects that are not in clockify push them to clockify.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ClockifyAPI $clockify): void
    {
        abort_if(!config('app.clockify.is_enabled'), ClockifyAPi::STATUS_FORBIDDEN);

        $projects = Project::with(['customer' => function($customer){
            return $customer->select(['id', 'customer_name']);
        }])->select(['id', 'consultants', 'name', 'billable', 'customer_id'])->whereIn('status_id', [1,2,3])->get();

        //$user_ids = $clockify->consultantIds($projects);

        $projects = $clockify->formatProject($projects, true);

        $clockify_projects = $clockify->projects();

        $clockify_projects = $clockify->formatProject(collect($clockify_projects), false);

        $projects_diff = $clockify->arrayMismatch($projects, $clockify_projects);

        //$users = User::whereIn('id', $user_ids)->select(['email'])->get();

        foreach ($projects_diff as $project){
            ClockifyProjectJob::dispatch($project)->delay(1);
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
