<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RunCrons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:run-crons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tenants = Tenant::where('id', '<>', '7d51380f-64a0-4b1c-a1e9-5ad6b9259afb')->pluck('id');

        foreach ($tenants as $tenant){
            Artisan::call("tenants:run schedule:run --tenants=$tenant >> /var/www/consulteaze.com/app/storage/logs/cron.log 2>&1");
        }
    }
}
