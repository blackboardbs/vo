<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Models\AssignmentWarningCheck;
use App\Models\Config;
use App\Mail\AssignmentEndDate;
use App\Models\Tenant;
use App\Models\Timesheet;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class assignmentCompletionWarning extends Command
{
    use TenantAwareCommand;

    protected $signature = 'warning:assignment_completion_warning';

    protected $description = 'This command warns the administrator about any assignment that is about to be completed';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $assignments = Assignment::whereIn('assignment_status', [2,3])->get();
        $utils = new Utils();
        $day = Carbon::now();
        $configs = Config::first();

        foreach ($assignments as $assignment){
            $warning_checks = AssignmentWarningCheck::where('assignment_id', '=', $assignment->id)->latest()->first();
            $actual_hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable->value.' THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();

            if (($assignment->end_date == $day->now()->addDays($configs->first_warning_days)->toDateString()) && !isset($warning_checks)){
                $this->notifies($utils, 'email', $assignment, $actual_hours, 1);
            }elseif ((round(($actual_hours->actual_hours/$assignment->hours) * 100) >= $configs->first_warning_percentage) && (round(($actual_hours->actual_hours/$assignment->hours) * 100) < $configs->second_warning_percentage) && (!isset($warning_checks) || $warning_checks->warning_no != 1 )){
                $this->notifies($utils, 'percentage', $assignment, $actual_hours, 1);
            }elseif (($assignment->end_date == $day->now()->addDays($configs->second_warning_days)->toDateString()) && (!isset($warning_checks) || $warning_checks->warning_no == 1)){
                $this->notifies($utils, 'email', $assignment, $actual_hours, 2);
            }elseif ((round(($actual_hours->actual_hours/$assignment->hours) * 100) >= $configs->second_warning_percentage) && (round(($actual_hours->actual_hours/$assignment->hours) * 100) < $configs->last_warning_percentage) && (!isset($warning_checks) || $warning_checks->warning_no == 1 )){
                $this->notifies($utils, 'percentage', $assignment, $actual_hours, 2);
            }elseif (($assignment->end_date == $day->now()->addDays($configs->last_warning_days)->toDateString()) && (!isset($warning_checks) || $warning_checks->warning_no == 2)){
                $this->notifies($utils, 'email', $assignment, $actual_hours, 3);
            }elseif ((round(($actual_hours->actual_hours/$assignment->hours) * 100) >= $configs->last_warning_percentage) && (round(($actual_hours->actual_hours/$assignment->hours) * 100) < 100) && (!isset($warning_checks) || $warning_checks->warning_no == 2 )){
                $this->notifies($utils, 'percentage', $assignment, $actual_hours, 3);
            }
        }
    }

    private function notifies($util, $warningType, $assignment, $actual_hours, $warning_number){
        foreach ($util->userRoles as $user_id){
            $name = 'Assignment number ('.$assignment->id.') is about to expire.';
            $notification = $util->notifications($name, route('assignment.show',$assignment));
            $util->notification_id($user_id->user_id, $notification);
        }
        Mail::to($util->admins)->send(new AssignmentEndDate($assignment, $actual_hours, $util->helpPortal));

        $new_warning_check = new AssignmentWarningCheck();
        $new_warning_check->assignment_id = $assignment->id;
        $new_warning_check->warning_type = $warningType;
        $new_warning_check->warning_no = $warning_number;
        $new_warning_check->save();
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
