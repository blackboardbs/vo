<?php

namespace App\Console\Commands;

use App\Models\PlanUtilization;
use App\Models\Tenant;
use App\Models\UserNotification;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use Stancl\Tenancy\Concerns\TenantAwareCommand;

class PlannedUtilization extends Command
{
    use TenantAwareCommand;

    protected $signature = 'email:planned_utilization';

    protected $description = 'This command notifies system admin if there\' no planned utilization for this week';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $utils = new Utils();
        $carbon = Carbon::now();
        $plan_util = PlanUtilization::where('yearwk', '=', $carbon->now()->year.(($carbon->now()->weekOfYear < 10)?'0'.$carbon->now()->weekOfYear:$carbon->now()->weekOfYear))->first();

        if (!isset($plan_util)){
            $notification_name = 'Your Planned Utilization for '.$utils->siteName().' Web Office does not have a week planned for this week.';
            $notification = $utils->notifications($notification_name, route('utilization.create','week'));
            UserNotification::insert($utils->notification_id(null, $notification));
            Mail::to($utils->adminEmail())->send(new \App\Mail\PlannedUtilization($utils->siteName(), $utils->helpPortal));
        }
    }

    public function getTenants(): Collection
    {
        return Tenant::all();
    }
}
