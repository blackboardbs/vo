<?php

namespace App\Traits;

use App\Models\Assignment;
use App\Models\Sprint;
use App\Models\SprintResource;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Collection;

trait SprintTrait
{
    private function backgroundColor(Collection $summaries_hours, Collection $tasks): string
    {
        $potential_outcome = $summaries_hours->sum('HoursOutstanding') - $summaries_hours->sum('HoursAvailable');
        $impendiments = $tasks->map(fn($goal) => $goal->tasks?->filter(fn($task) => $task->status == 6)->values()->count())->sum();
        $not_complete = $tasks->map(fn($goal) => $goal->tasks?->filter(fn($task) => $task->status != 5)->values()->count())->sum();

        if ($this->status_id == 2 && $not_complete){
            return 'bg-danger';
        }elseif ($potential_outcome <= 0 && $impendiments == 0){
            return 'bg-success';
        }elseif ($potential_outcome > 0 && $impendiments > 0){
            return 'bg-warning';
        }

        return '';
    }

    public function sprintHealth(Collection $summaries_hours, Collection $tasks): object
    {
        $sprint_days = CarbonPeriod::create($this->start_date, $this->end_date)->count();
        $no_of_days_from_sprint_start_and_now = CarbonPeriod::create($this->start_date, now()->toDateString())->count();

        try {
            $progress = ($no_of_days_from_sprint_start_and_now/$sprint_days)*100;
        }catch (\DivisionByZeroError $e){
            $progress = 0;
        }

        return (object)[
            'color' => $this->backgroundColor($summaries_hours, $tasks),
            'progress' => $progress
        ];
    }

    public function createSprintResources(): void
    {
        $this->load('resources:sprint_id,employee_id');

        $sprint_resources_ids = $this->resources?->map(fn($resource) => $resource->employee_id)->toArray();

        Assignment::select(['employee_id', 'function', 'capacity_allocation_hours', 'capacity_allocation_min'])->where('project_id', $this->project_id)->get()
            ->each(function ($assignment) use ($sprint_resources_ids){
                if (!in_array($assignment->employee_id, $sprint_resources_ids)){
                    SprintResource::insert([
                        'sprint_id' => $this->id,
                        'employee_id' => $assignment->employee_id,
                        'function_id' => $assignment->function,
                        'capacity' => $assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)
                    ]);
                }
            });
    }

    public function plannedHours():float
    {
        return $this->sprintHoursSummary?->sum('HoursPlanned');
    }

    public function bookedHours(): float
    {
        return $this->sprintHoursSummary?->sum('ActualHours');
    }

    public function hoursPerc(): float
    {
        try {
            return ($this->bookedHours()/$this->plannedHours())*100;
        }catch (\DivisionByZeroError $e){
            return 0.00;
        }
    }

    public function burnedHours(): float
    {
        try {
            $plannedCompleted = $this->tasks?->sum(fn($task) => $task->status == 5 ? $task->hours_planned : 0);
            $planned = $this->tasks?->sum('hours_planned');
            return ($plannedCompleted/$planned)*100;
        }catch (\DivisionByZeroError $e){
            return 0.00;
        }
    }
}