<?php

namespace App\Traits;

use App\Enum\FrequencyEnum;
use App\Http\Requests\InvoiceRequest;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Recurring;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;

trait InvoiceTrait
{
    public function formatInvoiceNumber(string $prefix, int $num): string|int
    {
        if(!isset($prefix)){
            return $num;
        }

        $formattedNumber = str_pad($num, 6, '0', STR_PAD_LEFT);

        return $prefix.$formattedNumber;
    }

    public function extractIntFromString(string|int $prefix, int|string $invoice_number): int
    {
        $remove_prefix = substr($invoice_number, strlen($prefix));
        return intval(ltrim($remove_prefix, '0'));
    }

    public function saveRecurringInvoice(InvoiceRequest $request): void
    {
        $this->recurring()->save(new Recurring([
            'full_name' => $request->full_name, 'first_invoice_at' => $request->first_invoice_date,
            'due_days_from_invoice_date' => $request->due_days_from_invoice_date, 'frequency' => $request->frequency,
            'interval' => $request->interval, 'occurrences' => $request->occurrences, 'expires_at' => $request->expiry_date,
            'email_subject' => $request->email_subject, 'emails' => implode(",", $request->emails)
        ]));
    }

    private function newInvoiceNumberInt(): int
    {
        $prefixColumn = $this->whoCalledMe()."_invoice_prefix";
        $startNumber = $this->whoCalledMe()."_invoice_start_number";
        $invoiceNumber = $this->whoCalledMe()."_invoice_number";

        if (isset($this->sysConfig()->$startNumber)) {
            $last_invoice = \get_called_class()::select([$invoiceNumber])->whereNotNull($invoiceNumber)->latest()->first();

            if (isset($last_invoice->$invoiceNumber)) {
                return  $this->extractIntFromString($this->sysConfig()?->$prefixColumn, $last_invoice->$invoiceNumber) + 1;
            }

            return $this->sysConfig()?->customer_invoice_start_number;
        }

        return $this->id;
    }

    public function invoiceNumberString(): string
    {
        $prefixColumn = $this->whoCalledMe()."_invoice_prefix";
        $startNumber = last(explode("\\",$this->whoCalledMe()."_invoice_start_number"));
        return $this->formatInvoiceNumber($this->sysConfig()?->$prefixColumn, $this->newInvoiceNumberInt())
            ?? (($this->sysConfig()?->$prefixColumn).''.$this->sysConfig()->$startNumber??10000);
    }

    public function saveInvoiceNumber(): void
    {
        $invoice = $this->refresh();
        try {
            $column = $this->whoCalledMe()."_invoice_number";
            $invoice->$column = $this->invoiceNumberString();
            $invoice->save();
        } catch (QueryException $e) {
            echo $e->getMessage();
            $this->saveInvoiceNumber();
        }
    }

    public function whoCalledMe(): string
    {
        $className = last(explode("\\", get_called_class()));

        return head(explode("_",$this->pascalToSnake($className)));
    }

    public function sysConfig(): Config
    {
        return Config::select([
            'customer_invoice_prefix',
            'customer_invoice_start_number',
            'vendor_invoice_start_number',
            'vendor_invoice_prefix',
            'invoice_body_msg'
        ])->first();
    }

    public function contact(Project $project, Customer $customer)
    {
        $project = $project->load('customerInvoiceContact');
        $customer = $customer->load('invoiceContact');

        if (isset($project->customerInvoiceContact)) {
            return (object) [
                'name' => $project->customerInvoiceContact?->full_name,
                'number' => $project->customerInvoiceContact?->contact_number,
                'email' => $project->customerInvoiceContact?->email,
            ];
        }

        if (isset($customer->invoiceContact)) {
            return (object) [
                'name' => $customer->invoiceContact?->full_name,
                'number' => $customer->invoiceContact?->contact_number,
                'email' => $customer->invoiceContact?->email,
            ];
        }

        return (object) [
            'name' => $customer->contact_firstname.' '.$customer->contact_lastname,
            'number' => $customer->phone ?? $customer->cell,
            'email' => $customer->email,
        ];
    }

    private function pascalToSnake(string $input) : string
    {
        if (preg_match('/[A-Z]/', $input) === 0) {
            return $input;
        }
        $pattern = '/([a-z])([A-Z])/';
        $r = strtolower(preg_replace_callback($pattern, function ($a) {
            return $a[1] . "_" . strtolower($a[2]);
        }, $input));
        return $r;
    }

    public function invoiceType(): string|null
    {
        $this->loadMissing(['recurring']);
        if ($this->relationshipExist('invoiceLines')?->isNotEmpty() || $this->relationshipExist('vendorInvoiceLine')?->isNotEmpty())
        {
            return 'TI';
        }elseif ($this->recurring && ($this->relationshipExist('nonTimesheetCustomerInvoices')?->isNotEmpty() || $this->relationshipExist('nonTimesheetVendorInvoices')?->isNotEmpty()))
        {
            return 'RI';
        }elseif (!$this->recurring && ($this->relationshipExist('nonTimesheetCustomerInvoices')?->isNotEmpty() || $this->relationshipExist('nonTimesheetVendorInvoices')?->isNotEmpty()))
        {
            return 'NTI';
        }else{
            return null;
        }
    }

    public function relationshipExist($relation)
    {
        if (! method_exists(get_class(), $relation)) {
            return null;
        }

        $this->load($relation);

        return $this->$relation;
    }

    public function invoicesRemaining(Recurring $recurring, int $generate_invoices_no): int
    {
        if ($recurring->occurrences)
        {
            $occurance_end_date = Carbon::parse($recurring->first_invoice_at)
                ->{'add'.FrequencyEnum::from($recurring->frequency->value)->name.'s'}($recurring->occurrences * $recurring->interval)->toDateString();

            if ($occurance_end_date <= $recurring->expires_at){
                return (int) ($recurring->occurrences - $generate_invoices_no);
            }
        }

        return (int) $this->safeDevision($this->diffInFrequency($recurring), $recurring->interval);
    }

    public function frequencyDate(Recurring $recurring): string
    {
        try {
            $last_invoiced_at = Carbon::parse($recurring->last_invoiced_at);
            $addFrequency = FrequencyEnum::Month->value === $recurring->frequency->value
                ? 'add'.FrequencyEnum::from($recurring->frequency->value)->name.'sNoOverflow'
                : 'add'.FrequencyEnum::from($recurring->frequency->value)->name.'s';
            if ($last_invoiced_at->isLastOfMonth()){
                return $last_invoiced_at->$addFrequency($recurring->interval)->endOfMonth()->toDateString();
            }
            return $last_invoiced_at->$addFrequency($recurring->interval)->toDateString();
        }catch (\ValueError $e){
            return now()->toDateString();
        }
    }

    private function diffInFrequency(Recurring $recurring): int|float
    {
        $last_invoice = $recurring->last_invoiced_at??$recurring->first_invoice_at;

        return Carbon::parse($last_invoice)
            ->{'diffIn'.FrequencyEnum::from($recurring->frequency->value)->name.'s'}($recurring->expires_at);
    }

    private function safeDevision(int|float $dividend, int|float $divisor): int|float
    {
        try {
            return $dividend / $divisor;
        }catch (\DivisionByZeroError  $exception){
            return 0;
        }
    }

    public function invoiceStoragePath(string $type): object
    {
        $storage_dir = strtolower(str_replace(" ", "_", ('invoices'.DIRECTORY_SEPARATOR.$this->$type?->{$type."_name"})));
        if (! Storage::exists($storage_dir)) {
            Storage::makeDirectory($storage_dir);
        }

        $file_name = strtolower(str_replace(" ", "_", ($this->$type?->{$type."_name"}.'_'.$this->project?->ref.'_'.date('Y-m-d_H_i_s').'.pdf')));

        return (object) [
            'storage' => storage_path('app'.DIRECTORY_SEPARATOR.strtolower(str_replace(" ", "_", ($storage_dir.DIRECTORY_SEPARATOR.$file_name)))),
            'file_name' => $file_name,
        ];
    }
}