<?php

namespace App\Traits\Holiday;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

include('simple_html_dom.php');

trait HolidaysTrait
{
    protected function southAfricanPublicHolidays()
    {
        try {
            $office_holidays = file_get_html("https://www.officeholidays.com/countries/south-africa");
            $holidays = [];
            foreach ($office_holidays->find('table[class=country-table]', 0)->find('tr') as $tr){
                if (count($tr->find('td'))){
                    $row = $tr->find('td');
                    array_push($holidays, (object) [
                        'date' => Carbon::parse($row[1]->plaintext),
                        'holiday_name' => str_replace("&#039;", "'", $row[2]->plaintext),
                        'type' => $row[3]->plaintext == "National Holiday" ? "Public Holiday" : "Not a Public Holiday"
                    ]);
                }
            }

            return $holidays;
        }catch (\ErrorException $exception){
            Log::info($exception->getMessage());
            return [];
        }

    }
}