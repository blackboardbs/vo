<?php

namespace App\Traits;

use App\Models\ModuleRole;
use App\Models\ResourceViewRole;
use App\Models\StorePermission;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

trait ProjectTrait
{
    public function consultants(): Collection
    {
        return User::find(explode(',', $this->consultants), ['id','first_name', 'last_name']);
    }

    public function formatConsultants(Collection $assignments): array
    {
        $consultants_as_array = [];

        foreach ($this->consultants() as $consultant){
            foreach ($assignments as $assignment){
                if ($assignment->employee_id == $consultant->id){
                    $cons = '<a href="'.route('assignment.show', $assignment->id).'">'.$consultant->name().'</a>';
                    array_push($consultants_as_array,$cons);
                }
            }
        }

        return [...$consultants_as_array, ...$this->addAssignmentConsultant($assignments)];
    }

    public function addAssignmentConsultant(Collection $assignments): array
    {
        $assignment_consultants = $assignments->map(fn($assignment) => $assignment->employee_id)->toArray();
        $consultants = [];
        foreach ($this->consultants() as $consultant){
            if (!in_array($consultant->id, $assignment_consultants)){
                $cons = '<a href="'.route('assignment.add', ['project' => $this->id, 'resource' => $consultant->id]).'" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> '.$consultant->name().'</a>';
                $consultants[] = $cons;
            }
        }
        return $consultants;
    }

    public function consultantsWithoutAssignment(Collection $assignments): \Illuminate\Support\Collection
    {
        $consultants_with_assignment = $assignments->map(fn($assignment) => $assignment->employee_id)->toArray();
        $consultants_without_assignment = $this->consultants()->filter(fn($consultant) => !in_array($consultant->id, $consultants_with_assignment))->values()/*->map(fn($consultant) => $consultant->id)->toArray()*/;

        return $consultants_without_assignment->map(fn($consultant) => (object) [
            'emp_id' => $consultant->id,
            'consultant' => $consultant->name(),
            'role' => $this->assignmentRole($consultant)?->name,
            'role_id' => $this->assignmentRole($consultant)?->id
        ]);
    }

    public function assignmentRole(User $user)
    {
        return ModuleRole::find($user->getViewRole(30, $user->id, $this->id), ['id','name']);
    }

    public function percentage(int|float $hours, int|float $billable_hours, int|float $non_billable_hours): int
    {
        try {
            $percentage = round(($this->actualHours($billable_hours, $non_billable_hours)/$hours)*100);

        }catch (\DivisionByZeroError $e){
            $percentage = 0;
        }
        return $percentage;
    }

    public function outstandingHours(int|float $hours, int|float $billable_hours, int|float $non_billable_hours): string
    {
        return number_format($hours - $this->actualHours($billable_hours, $non_billable_hours),2,'.',',');
    }

    public function actualHours(int|float $billable_hours, int|float $non_billable_hours): int|float
    {
        if ($this->project_type_id == 1){
            return $billable_hours;
        }
        return $billable_hours + $non_billable_hours;
    }

    public function projectPermissions(): array|null
    {
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $this->id);

        if (! $permissions) {
            $resource_view_role = new ResourceViewRole();
            $resource_view_role->module_id = 30;
            $resource_view_role->resource_id = $this->manager_id;
            $resource_view_role->module_item_id = $this->id; // ProjectID, CustomerID, etc
            $resource_view_role->role_id = 1;
            $resource_view_role->save();

            $permissions = $storePermission->getPermissions(30, $this->id);
        }

        return $permissions;
    }

    public function canView(int $key): string|null
    {
        $permissions = $this->projectPermissions();

        return $permissions[$key]['permission']??null;
    }
}