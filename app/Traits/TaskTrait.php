<?php

namespace App\Traits;

trait TaskTrait
{
    public function description(): string
    {
        return $this?->description_of_work?$this?->task.' - '.$this?->description_of_work:$this?->task ?? '';
    }
}