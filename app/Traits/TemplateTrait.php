<?php

namespace App\Traits;

use App\Models\AdvancedTemplates;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\TemplateType;
use App\Models\Vendor;
use App\Services\EquipmentService;
use Illuminate\Support\Number;

trait TemplateTrait
{
    public function mapVariables(AdvancedTemplates $template)
    {
        $service = new EquipmentService();
        $company = Config::first()?->company;
        $company_contact_person = $company->contact_firstname." ".$company->contact_lastname;

        $variables = TemplateType::find($template->template_type_id)?->variables;

        //dd($variables);

        $templateBody = str_replace('class="mention"', '', $template->template_body);

        $asset_issued = $this->assetHistoryActive;
        $issue_date = $asset_issued->issued_at??$this->aquire_date;
        $issued_to = ($asset_issued?->user?->name()??($this->resource?->first_name." ".$this->resource?->last_name))??null;
        $asset_class = $this->assetClass?->description;

        foreach ($variables as $variable) {
            switch ($variable->variable) {
                case "company.company_logo":
                case "vendor.vendor_logo":
                    $logo = $this->templateLogo($variable->variable);
                    $templateBody = str_replace("[{$variable->variable}]", "<img src='".$logo."' style='max-height: 80px;width: auto' alt='$variable->display_name' />", $templateBody);
                    break;
                case "company.company_name":
                    $templateBody = str_replace("[{$variable->variable}]", $company->company_name, $templateBody);
                    break;
                case "company.company_contact_person":
                    $templateBody = str_replace("[{$variable->variable}]", "{$company_contact_person}", $templateBody);
                    break;
                case "company.phone":
                    $templateBody = str_replace("[{$variable->variable}]", $company->phone, $templateBody);
                    break;
                case "company.email":
                    $templateBody = str_replace("[{$variable->variable}]", $company->email, $templateBody);
                    break;
                case "company.business_reg_no":
                    $templateBody = str_replace("[{$variable->variable}]", $company->business_reg_no, $templateBody);
                    break;
                case "company.company_address":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->address($company)}", $templateBody);
                    break;
                case "asset.note":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->note}", $templateBody);
                    break;
                case "asset.current_book_value":
                   $equipment = $this->only(['written_off_at', 'sold_at', 'original_value', 'retire_date', 'est_life']);
                   $equipment['cost_of_repairs'] = $this->additionalCost?->sum('cost');
                   $current_book_value = "R".Number::format($service->currentBookValue($equipment), 2);
                   $templateBody = str_replace("[{$variable->variable}]", "{$current_book_value}", $templateBody);
                   break;
                case "asset.issued_date":
                    $templateBody = str_replace("[{$variable->variable}]", "{$issue_date}", $templateBody);
                    break;
                case "asset.issued_to":
                    $templateBody = str_replace("[{$variable->variable}]", "{$issued_to}", $templateBody);
                    break;
                case "asset.serial_number":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->serial_nr}", $templateBody);
                    break;
                case "asset.model":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->model}", $templateBody);
                    break;
                case "asset.make":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->make}", $templateBody);
                    break;
                case "asset.number":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->asset_nr}", $templateBody);
                    break;
                case "asset.class":
                    $templateBody = str_replace("[{$variable->variable}]", "{$asset_class}", $templateBody);
                    break;
                case "date":
                    $templateBody = str_replace("[{$variable->variable}]", now()->toDateString(), $templateBody);
                    break;
                case "vendor.phone":
                case "customer.phone":
                    $templateBody = str_replace("[{$variable->variable}]", $this->phone??$this->cell, $templateBody);
                    break;
                case "vendor.email":
                case "users.email":
                case "customer.email":
                    $templateBody = str_replace("[{$variable->variable}]", $this->email, $templateBody);
                    break;
                case "vendor.contact_person":
                case "customer.customer_contact_person":
                    $templateBody = str_replace("[{$variable->variable}]", "{$this->contact_firstname} {$this->contact_lastname}", $templateBody);
                    break;
                case "vendor.business_reg_no":
                case "customer.business_reg_no":
                    $templateBody = str_replace("[{$variable->variable}]", $this->business_reg_no, $templateBody);
                    break;
                case "vendor.vendor_address":
                case "customer.customer_address":
                    if (is_a($this, Customer::class) || is_a($this, Vendor::class)) {
                        $templateBody = str_replace("[{$variable->variable}]", $this->address($this), $templateBody);
                    }
                    break;
                case "vendor.vendor_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->vendorName(), $templateBody);
                    break;
                case "customer.customer_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->customer_name, $templateBody);
                    break;
                case "assignment.billable":
                    $templateBody = str_replace("[{$variable->variable}]", $this->billable->value, $templateBody);
                    break;
                case "expense.other":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->other, $templateBody);
                    break;
                case "expense.data":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->data, $templateBody);
                    break;
                case "expense.out_of_town":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->out_of_town, $templateBody);
                    break;
                case "expense.per_diem":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->per_diem, $templateBody);
                    break;
                case "expense.parking":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->parking, $templateBody);
                    break;
                case "expense.accommodation":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->accommodation, $templateBody);
                    break;
                case "expense.travel":
                    $templateBody = str_replace("[{$variable->variable}]", $this->expense?->travel, $templateBody);
                    break;
                case "assignment.claim_approver_id":
                    $templateBody = str_replace("[{$variable->variable}]", $this->claimApprover?->name(), $templateBody);
                    break;
                case "assignment.report_phone":
                    $templateBody = str_replace("[{$variable->variable}]", $this->report_phone, $templateBody);
                    break;
                case "assignment.report_to_email":
                    $templateBody = str_replace("[{$variable->variable}]", $this->report_to_email, $templateBody);
                    break;
                case "assignment.report_to":
                    $templateBody = str_replace("[{$variable->variable}]", $this->report_to, $templateBody);
                    break;
                case "assignment.location":
                    $templateBody = str_replace("[{$variable->variable}]", $this->location, $templateBody);
                    break;
                case "assignment.bonus_rate":
                    $templateBody = str_replace("[{$variable->variable}]", $this->bonus_rate, $templateBody);
                    break;
                case "assignment.hours_of_work":
                    $templateBody = str_replace("[{$variable->variable}]", $this->hours_of_work, $templateBody);
                    break;
                case "assignment.invoice_rate":
                    $templateBody = str_replace("[{$variable->variable}]", $this->invoice_rate, $templateBody);
                    break;
                case "assignment.hours":
                    $templateBody = str_replace("[{$variable->variable}]", $this->hours, $templateBody);
                    break;
                case "assignment.end_date":
                    $templateBody = str_replace("[{$variable->variable}]", $this->end_date, $templateBody);
                    break;
                case "assignment.start_date":
                    $templateBody = str_replace("[{$variable->variable}]", $this->start_date, $templateBody);
                    break;
                case "assignment.notes":
                    $templateBody = str_replace("[{$variable->variable}]", $this->note_1, $templateBody);
                    break;
                case "assignment.scope":
                    $templateBody = str_replace("[{$variable->variable}]", $this->scope, $templateBody);
                    break;
                case "assignment.project_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->project?->name, $templateBody);
                    break;
                case "assignment.consultant":
                    $templateBody = str_replace("[{$variable->variable}]", $this->consultant?->name(), $templateBody);
                    break;
                case "assignment.rate":
                    $templateBody = str_replace("[{$variable->variable}]", $this->rate, $templateBody);
                    break;
                case "vendor.account_manager":
                    $templateBody = str_replace("[{$variable->variable}]", $this->consultant?->vendor?->account_managerd?->name(), $templateBody);
                    break;
                case "vendor.payment_terms":
                    $templateBody = str_replace("[{$variable->variable}]", $this->consultant?->vendor?->payment_terms, $templateBody);
                    break;
                case "users.first_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->first_name, $templateBody);
                    break;
                case "users.last_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->last_name, $templateBody);
                    break;
                case "users.phone":
                    $templateBody = str_replace("[{$variable->variable}]", $this->phone, $templateBody);
                    break;
                case "users.title":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->title?->description, $templateBody);
                    break;
                case "users.id_number":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->id_number, $templateBody);
                    break;
                case "users.date_of_birth":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->date_of_birth, $templateBody);
                    break;
                case "users.passport_number":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->passport_number, $templateBody);
                    break;
                case "users.passport_country":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->country?->name, $templateBody);
                    break;
                case "users.address":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->contactDetails?->physical_address, $templateBody);
                    break;
                case "users.bank_account_type":
                    $templateBody = str_replace(
                        "[{$variable->variable}]",
                        $this->userOnboarding?->paymentDetails?->bankAccountType?->description,
                        $templateBody
                    );
                    break;
                case "users.bank_account_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->paymentDetails?->account_name, $templateBody);
                    break;
                case "users.bank_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->paymentDetails?->bank_name, $templateBody);
                    break;
                case "users.branch_number":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->paymentDetails?->branch_number, $templateBody);
                    break;
                case "users.account_number":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->paymentDetails?->account_number, $templateBody);
                    break;
                case "users.appointing_manager_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->appointmentManager?->name(), $templateBody);
                    break;
                case "users.start_date":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->start_date, $templateBody);
                    break;
                case "users.end_date":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->end_date, $templateBody);
                    break;
                case "users.contract_type":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->contractType?->description, $templateBody);
                    break;
                case "users.payment_base":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->paymentBase?->description, $templateBody);
                    break;
                case "users.position":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->position, $templateBody);
                    break;
                case "users.annual_salary":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->annual_salary, $templateBody);
                    break;
                case "users.leave_days":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->leave_days, $templateBody);
                    break;
                case "users.notice_period":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->notice_period, $templateBody);
                    break;
                case "users.reporting_manager":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->reportingManager?->name(), $templateBody);
                    break;
                case "users.other_income":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->other_income, $templateBody);
                    break;
                case "users.other_conditions":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->other_conditions, $templateBody);
                    break;
                case "users.responsibilities":
                    $templateBody = str_replace("[{$variable->variable}]", $this->userOnboarding?->appointingManager?->responsibilities, $templateBody);
                    break;
                case "assessment.user_fullname":
                    $templateBody = str_replace("[{$variable->variable}]", $this->user?->name(), $templateBody);
                    break;
                case "resource.resource_number":
                    $templateBody = str_replace("[{$variable->variable}]", $this->user?->resource?->resource_no, $templateBody);
                    break;
                case "resource.resource_position":
                    $templateBody = str_replace("[{$variable->variable}]", $this->user?->resource?->resource_position_desc?->description, $templateBody);
                    break;
                case "assessments.assessment_date":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessment_date, $templateBody);
                    break;
                case "assessments.assessment_period_start":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessment_period_start, $templateBody);
                    break;
                case "assessment.assessment_period_end":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessment_period_end, $templateBody);
                    break;
                case "assessment.next_assessment":
                    $templateBody = str_replace("[{$variable->variable}]", $this->next_assessment, $templateBody);
                    break;
                case "assessment.customer_name":
                    $templateBody = str_replace("[{$variable->variable}]", $this->customer->customer_name, $templateBody);
                    break;
                case "assessment.assessor":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessor->name(), $templateBody);
                    break;
                case "assessment.activities":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessmentActivities(), $templateBody);
                    break;
                case "next_assessment.activities":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessmentActivitiesNext(), $templateBody);
                    break;
                case "assessment_notes.notes":
                    $templateBody = str_replace("[{$variable->variable}]", $this->assessmentNote(), $templateBody);
                    break;
            }
        }

        return $templateBody;
    }

    private function address(Company|Vendor|Customer $company): string
    {
        $company_address = "";

        if (isset($company->postal_address_line_1)) {
            $company_address .= $company->postal_address_line_1."<br>";
        }

        if (isset($company->postal_address_line_2)) {
            $company_address .= $company->postal_address_line_2."<br>";
        }

        if (isset($company->postal_address_line_3)) {
            $company_address .= $company->postal_address_line_3."<br>";
        }

        if (isset($company->city_suburb)) {
            $company_address .= $company->city_suburb."<br>";
        }

        if (isset($company->state_province)) {
            $company_address .= $company->state_province."<br>";
        }

        if (isset($company->country)) {
            $company_address .= $company->country->name."<br>";
        }

        if (isset($company->postal_zipcode)) {
            $company_address .= $company->postal_zipcode."<br>";
        }

        return $company_address;
    }

    private function templateLogo(string $variable): string
    {
        $config = Config::select(['site_logo', 'company_id'])->first();
        if ($variable === "company.company_logo"){

            if (isset($config->site_logo) && file_exists(storage_path("app/public/assets/".$config->site_logo))){
                return storage_path("app/public/assets/".$config->site_logo);
            }elseif (isset($config->company->company_logo) && file_exists(storage_path('app/avatars/company/'.$config->company->company_logo))) {
                return storage_path("app/public/assets/".$config->site_logo);
            }
        }elseif ($variable === "vendor.vendor_logo"){
            if (file_exists(storage_path('app/avatars/vendor/'.$this->vendor_logo)))
            {
                return storage_path('app/avatars/vendor/'.$this->vendor_logo);
            }elseif (file_exists(storage_path('app/avatars/vendor/'.$this->consultant?->vendor?->vendor_logo)))
            {
                return storage_path('app/avatars/vendor/'.$this->consultant?->vendor?->vendor_logo);
            }
        }

        return public_path('assets/default.png');
    }

    public function vendorName(): string|null
    {
        if (isset($this->consultant->vendor)){
            return $this->consultant->vendor->vendor_name;
        }
        return $this->vendor_name;
    }

    private function assessmentActivities(): string
    {
        $table_row = "";

        foreach ($this->activities as $activity) {
            $table_row .= "<tr>
                                <td>{$activity->assessment_task_description}</td>
                                <td>{$activity->competency_level}</td>
                                <td>{$activity->comfort_level}</td>
                                <td>{$activity->notes}</td>
                          </tr>";
        }
        return "<table>
                    <thead>
                        <tr>
                            <th>Task</th>
                            <th>Competency Level</th>
                            <th>Comfort Level </th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>{$table_row}</tbody>
                </table>";
    }

    private function assessmentActivitiesNext(): string
    {
        $table_row = "";

        foreach ($this->nextAssessmentActivities as $activity) {
            $table_row .= "<tr>
                                <td>{$activity->assessment_task_description}</td>
                                <td>{$activity->notes}</td>
                          </tr>";
        }
        return "<table style='border:none'>
                    <thead>
                        <tr>
                            <th>Task</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>{$table_row}</tbody>
                </table>";
    }

    private function assessmentNote(): string
    {
        $list_items = "";

        foreach ($this->assessmentNotes as $note) {
            $list_items .= "<li>{{$note->notes}}</li>";
        }
        return "<ul>{$list_items}</ul>";
    }
}