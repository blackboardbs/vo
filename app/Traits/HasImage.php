<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait HasImage
{
    public function thumbnail(Request $request, string $field, string $type, int $height, int $width): void
    {
        $request->file($field)->store('avatars'.DIRECTORY_SEPARATOR.$type);
        $file_path = storage_path("app".DIRECTORY_SEPARATOR."avatars".DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$request->file($field)->hashName());
        $image = Image::make($file_path);
        if ($image->height() == $image->width()) {
            $image->resize($height, $height);
            $image->save();
        } else {
            $blur = Image::make($file_path)->fit($width, $height)->blur();

            $image->resize($width, $height, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });

            $blur->insert($image, 'center');
            $blur->save();
        }
    }

    public function logo(Request $request, string $field, string $type, int $height): void
    {
        $request->file($field)->store('avatars'.DIRECTORY_SEPARATOR.$type);
        $file = $request->file($field)->hashName();

        $file_path = storage_path("app".DIRECTORY_SEPARATOR."avatars".DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$file);
        $image = Image::make($file_path);
        $image->resize(null, $height, fn($constraint) => $constraint->aspectRatio());
        $image->save();
    }

    public function general(Request $request, string $field, string $type, int $size): void
    {
        $file_formats = ['jpg', 'png', 'gif', 'bmp', 'webp'];
        $request->file($field)->store($type);
        if (in_array($request->file($field)->extension(), $file_formats)){
            $file = $request->file($field)->hashName();
            $file_path = storage_path("app".DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$file);
            $image = Image::make($file_path);
            if (($image->height() > $image->width())) {
                if ($image->height() > $size) $image->resize(null, $size, fn($constraint) => $constraint->aspectRatio());
            }else{
                if ($image->width() > $size) $image->resize($size, null, fn($constraint) => $constraint->aspectRatio());
            }
            $image->save();
        }
    }

}