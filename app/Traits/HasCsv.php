<?php

namespace App\Traits;

trait HasCsv
{
    public function readCsv(string $fileName): array
    {
        $filePath = database_path('/data/'.$fileName);
        $file = fopen($filePath, 'r');

        $header = fgetcsv($file);

        $data = array();
        while ($row = fgetcsv($file)) {
            $data[] = array_combine($header, $this->sanitize($row));
        }

        fclose($file);

        return $data;
    }

    private function sanitize(array $row): array
    {
        return array_map(fn($column) => $column != "NULL" ? $column : null,$row);
    }
}