<?php

namespace App\Traits;

use App\Enum\YesOrNoEnum;
use App\Models\TimeExp;
use Illuminate\Support\Collection;

trait AssignmentTrait
{
    public function claimOrBill(Collection $expenses, string $claim_or_bill): int|float
    {
        return $expenses->filter(fn($exp) => $exp->$claim_or_bill == YesOrNoEnum::Yes)->values()->sum('amount');
    }

    public function expenseType(TimeExp $expense, string $type): int|float
    {
        if ($expense->$type == YesOrNoEnum::Yes)
            return $expense->amount;

        return 0;
    }

    public function profitPercentage(int|float $invoiced, int|float $labour): int|float
    {
        try {
            $percentage = ($labour/$invoiced)*100;
        }catch (\DivisionByZeroError $error){
            $percentage = 0;
        }

        return $percentage;
    }

    public function customerInvoice(Collection $timelines): Collection
    {
        return $timelines->map(
            fn($timeline) => (object) [
                'inv_number' => $timeline->timesheet?->invoice_number,
                'invoice_date' => $timeline->timesheet?->invoice_date,
                'invoice_due_date' => $timeline->timesheet?->invoice_due_date,
                'year_week' => $timeline->timesheet?->year_week,
                'inv_ref' => $timeline->timesheet?->inv_ref,
                'status' => $timeline->timesheet?->bill_statusd?->description,
                'bill_hours' => $timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0,
                'non_billable' => !$timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0,
                'invoice_amount' => ($timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0) * $this->invoice_rate,
                'exp_claim' => $timeline->timesheet?->time_exp()?->where('claim', 1)->sum('amount'),
                'exp_billable' => $timeline->timesheet?->time_exp()?->where('is_billable', 1)->sum('amount'),
            ])->whereNotNull('inv_number');
    }

    public function vendorInvoice(Collection $timelines): Collection
    {
        return $timelines->map(
            fn($timeline) => (object) [
                'inv_number' => $timeline->timesheet?->vendor_invoice_number,
                'invoice_date' => $timeline->timesheet?->vendor_invoice_date,
                'invoice_due_date' => $timeline->timesheet?->vendor_due_date,
                'year_week' => $timeline->timesheet?->year_week,
                'inv_ref' => $timeline->timesheet?->vendor_invoice_ref,
                'status' => $timeline->timesheet?->vendor_inv_status?->description,
                'bill_hours' => $timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0,
                'non_billable' => !$timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0,
                'invoice_amount' => ($timeline->is_billable ? ($timeline->total + ($timeline->total_m/60)):0) * $this->external_cost_rate,
                'exp_claim' => $timeline->timesheet?->time_exp()?->where('claim', 1)->sum('amount'),
                'exp_billable' => $timeline->timesheet?->time_exp()?->where('is_billable', 1)->sum('amount'),
            ])->whereNotNull('vendor_invoice_number');
    }
}