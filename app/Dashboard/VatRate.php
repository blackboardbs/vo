<?php

namespace App\Dashboard;

use App\Models\Company;
use App\Models\Config;
use App\Models\VatRate as Vat;

class VatRate
{
    public function tax_rates($customer, $timesheet, $is_vendor)
    {
        $config = Config::first();
        $company = ($config->company_id) ? Company::find($config->company_id) : Company::first();
        $vat_no = ($is_vendor) ? $customer->vat_no : $company->vat_no;
        if (($company->country_id == $customer->country_id) && ($vat_no != null)) {
            return  Vat::where('vat_code', '=', $config->vat_rate_id)->where('end_date', '>=', $timesheet->first_day_of_week)->first()->vat_rate;
        }

        return 0;
    }
}
