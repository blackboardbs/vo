<?php

namespace App\Dashboard;

use App\Models\Assignment;
use App\Models\PlanUtilization;
use App\Models\Resource;
use App\Models\Timesheet;

class Utilization
{
    public function weekly_utilization($bill_status, $project_type, $request)
    {
        $timesheet = Timesheet::selectRaw('timesheet.week, SUM(CASE WHEN timeline.is_billable = '.(($request->has('billable') && $request->billable != '') ? $request->billable : $bill_status).' THEN ROUND(timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereHas('project', function ($query) use ($project_type) {
                $query->where('type', '=', $project_type);
            })
            ->where('timesheet.year_week', '>=', now()->subWeeks(25)->year.((now()->subWeeks(25)->weekOfYear < 10) ? '0'.now()->subWeeks(25)->weekOfYear : now()->subWeeks(25)->weekOfYear))
            ->groupBy('timesheet.year_week')
            ->groupBy('timesheet.week')
            ->orderBy('timesheet.year_week');
        if ($request->has('company') && $request->company != '') {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }
        if (auth()->user()->hasRole('manager')) {
            $timesheet = $timesheet->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $timesheet = $timesheet->get();

        return $timesheet;
    }

    public function resource_utilization($request)
    {
        $resource_income = $this->get_utilized_resources(now(), 1, 1, $request);
        $resource_cost = $this->get_utilized_resources(now(), 0, 2, $request);

        $income_utilization = [];
        $cost_utilization = [];

        $planned_util_3 = $this->planned_util(now(), 3);
        $planned_util_2 = $this->planned_util(now(), 2);
        $planned_util_1 = $this->planned_util(now(), 1);
        $planned_util = $this->planned_util(now(), 0);
        if (! ($this->planned_util(now(), 3)) || ! ($this->planned_util(now(), 2)) || ! ($this->planned_util(now(), 1)) || ! ($this->planned_util(now(), 0))) {
            if (PlanUtilization::all()->count() < 4) {
                return redirect()->route('utilization.index')->with('flash_danger', 'Please check the missing weeks and add them');
            }
        }
        $year_week3 = now()->subDays(21)->year.((now()->subWeeks(3)->weekOfYear < 10) ? '0'.now()->subWeeks(3)->weekOfYear : now()->subWeeks(3)->weekOfYear);
        $year_week2 = now()->subDays(14)->year.((now()->subWeeks(2)->weekOfYear < 10) ? '0'.now()->subWeeks(2)->weekOfYear : now()->subWeeks(2)->weekOfYear);
        $year_week1 = now()->subDays(7)->year.((now()->subWeeks(1)->weekOfYear < 10) ? '0'.now()->subWeeks(1)->weekOfYear : now()->subWeeks(1)->weekOfYear);
        $year_week = now()->year.((now()->weekOfYear < 10) ? '0'.now()->weekOfYear : now()->weekOfYear);
        $weeks = [
            'week3' => $year_week3,
            'week2' => $year_week2,
            'week1' => $year_week1,
            'week' => $year_week,
            'planned3' => isset($planned_util_3) ? 1 : 0,
            'planned2' => isset($planned_util_2) ? 1 : 0,
            'planned1' => isset($planned_util_1) ? 1 : 0,
            'planned' => isset($planned_util) ? 1 : 0,
            'week_hours' => $planned_util_3,
        ];

        $planned = (isset($planned_util_3->wk_hours) ? $planned_util_3->wk_hours : 0) + (isset($planned_util_2->wk_hours) ? $planned_util_2->wk_hours : 0) + (isset($planned_util_1->wk_hours) ? $planned_util_1->wk_hours : 0);
        foreach ($resource_income as $res) {
            $week3 = $this->util_hours(now(), $res, 3, 1, 1, $request)->hours;
            $week2 = $this->util_hours(now(), $res, 2, 1, 1, $request)->hours;
            $week1 = $this->util_hours(now(), $res, 1, 1, 1, $request)->hours;
            $week = $this->util_hours(now(), $res, 0, 1, 1, $request)->hours;

            array_push($income_utilization, [
                'resource' => substr($res->user?->first_name, 0, 1).'. '.$res->user?->last_name,
                'week3' => $week3,
                'week2' => $week2,
                'week1' => $week1,
                'week' => $week,
                'util_percentage' => ($planned) ? ($week3 + $week2 + $week1) / $planned : 0,
                'three_weeks_total' => $week3 + $week2 + $week1,
            ]);
        }

        $planned = (isset($planned_util_3->cost_wk_hours) ? $planned_util_3->cost_wk_hours : 0) + (isset($planned_util_2->cost_wk_hours) ? $planned_util_2->cost_wk_hours : 0) + (isset($planned_util_1->cost_wk_hours) ? $planned_util_1->cost_wk_hours : 0);

        foreach ($resource_cost as $res_cost) {
            $week3 = $this->util_hours(now(), $res_cost, 3, 2, 0, $request)->hours;
            $week2 = $this->util_hours(now(), $res_cost, 2, 2, 0, $request)->hours;
            $week1 = $this->util_hours(now(), $res_cost, 1, 2, 0, $request)->hours;
            $week = $this->util_hours(now(), $res_cost, 0, 2, 0, $request)->hours;
            array_push($cost_utilization, [
                'resource' => substr($res_cost->user->first_name, 0, 1).'. '.$res_cost->user->last_name,
                'week3' => $week3,
                'week2' => $week2,
                'week1' => $week1,
                'week' => $week,
                'util_percentage' => ($planned) ? ($week3 + $week2 + $week1) / $planned : 0,
                'three_weeks_total' => $week3 + $week2 + $week1,
            ]);
        }

        $totals_income = [];
        $total_week3_income = $this->get_week_total($income_utilization)[0];
        $total_week2_income = $this->get_week_total($income_utilization)[1];
        $total_week1_income = $this->get_week_total($income_utilization)[2];
        $total_week_income = $this->get_week_total($income_utilization)[3];
        //$total_percent_coat = ($planned_util_3->res_no)?$this->get_week_total($income_utilization)[4]/$planned_util_3->res_no:0;
        $planned_income3 = ((isset($planned_util_3->wk_hours) ? $planned_util_3->wk_hours : 0) * (isset($planned_util_3->res_no) ? $planned_util_3->res_no : 0));
        $planned_income2 = ((isset($planned_util_2->wk_hours) ? $planned_util_2->wk_hours : 0) * (isset($planned_util_2->res_no) ? $planned_util_2->res_no : 0));
        $planned_income1 = ((isset($planned_util_1->wk_hours) ? $planned_util_1->wk_hours : 0) * (isset($planned_util_1->res_no) ? $planned_util_1->res_no : 0));
        $planned_income = ((isset($planned_util->wk_hours) ? $planned_util->wk_hours : 0) * (isset($planned_util->res_no) ? $planned_util->res_no : 0));
        $percentage_week3 = ($planned_income3) ? $total_week3_income / $planned_income3 : 0;
        $percentage_week2 = ($planned_income2) ? $total_week2_income / $planned_income2 : 0;
        $percentage_week1 = ($planned_income1) ? $total_week1_income / $planned_income1 : 0;
        array_push($totals_income, [
            'total_week_3' => $total_week3_income,
            'total_week_2' => $total_week2_income,
            'total_week_1' => $total_week1_income,
            'total_week' => $total_week_income,
            'percentage_week3' => $percentage_week3,
            'percentage_week2' => $percentage_week2,
            'percentage_week1' => $percentage_week1,
            'percentage_week' => ($planned_income) ? $total_week_income / $planned_income : 0,
            'total_percentage' => (($percentage_week3 * 100) + ($percentage_week2 * 100) + ($percentage_week1 * 100)) / 3,
        ]);

        $totals_cost = [];
        $total_week3_income = $this->get_week_total($cost_utilization)[0];
        $total_week2_income = $this->get_week_total($cost_utilization)[1];
        $total_week1_income = $this->get_week_total($cost_utilization)[2];
        $total_week_income = $this->get_week_total($cost_utilization)[3];
        //$total_percent_coat = (isset($planned_util->cost_res_no) && $planned_util->cost_res_no > 0)?$this->get_week_total($cost_utilization)[4]/$planned_util->cost_res_no:0;
        $planned_cost3 = ((isset($planned_util_3->cost_wk_hours) ? $planned_util_3->cost_wk_hours : 0) * (isset($planned_util_3->cost_res_no) ? $planned_util_3->cost_res_no : 0));
        $planned_cost2 = ((isset($planned_util_2->cost_wk_hours) ? $planned_util_2->cost_wk_hours : 0) * (isset($planned_util_2->cost_res_no) ? $planned_util_2->cost_res_no : 0));
        $planned_cost1 = ((isset($planned_util_1->cost_wk_hours) ? $planned_util_1->cost_wk_hours : 0) * (isset($planned_util_1->cost_res_no) ? $planned_util_1->cost_res_no : 0));
        $planned_cost = ((isset($planned_util->cost_wk_hours) ? $planned_util->cost_wk_hours : 0) * (isset($planned_util->cost_res_no) ? $planned_util->cost_res_no : 0));
        $percentage_week3 = ($planned_cost3) ? $total_week3_income / $planned_cost3 : 0;
        $percentage_week2 = ($planned_cost2) ? $total_week2_income / $planned_cost2 : 0;
        $percentage_week1 = ($planned_cost1) ? $total_week1_income / $planned_cost1 : 0;
        array_push($totals_cost, [
            'total_week_3' => $total_week3_income,
            'total_week_2' => $total_week2_income,
            'total_week_1' => $total_week1_income,
            'total_week' => $total_week_income,
            'percentage_week3' => $percentage_week3,
            'percentage_week2' => $percentage_week2,
            'percentage_week1' => $percentage_week1,
            'percentage_week' => ($planned_cost) ? $total_week_income / $planned_cost : 0,
            'total_percentage' => (($percentage_week3 * 100) + ($percentage_week2 * 100) + ($percentage_week1 * 100)) / 3,
        ]);

        return [
            'income_utilization' => collect(array_slice($income_utilization, 0, 10))->sortBy('three_weeks_total')->values()->all(),
            'cost_utilization' => collect(array_slice($cost_utilization, 0, 10))->sortBy('three_weeks_total')->values()->all(),
            'total_income' => $totals_income,
            'total_cost' => $totals_cost,
            'weeks' => $weeks,
        ];
    }

    public function get_utilized_resources($date, $assignments_status, $project_type, $request)
    {
        $resource_income = Resource::with(['user'])->select('user_id')
            ->where('util', '=', 1)
            ->where('project_type', '=', $project_type)
            ->where('util_date_from', '<=', $date->toDateString())->where('util_date_to', '>=', $date->now()->subWeeks(3)->startOfWeek()->toDateString())
            ->whereHas('user');
        if (auth()->user()->hasRole('manager')) {
            $resource_income = $resource_income->where('manager_id', '=', auth()->id());
        }
        if ($request->has('company') && $request->company != '') {
            $resource_income = $resource_income->whereHas('user', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }
        $resource_income = $resource_income->get();
        $assignment_income = Assignment::select('employee_id')->whereIn('assignment_status', [2, 3])->where('billable', '=', $assignments_status)->where('status', '=', 1)->groupBy('employee_id');
        if ($request->has('company') && $request->company != '') {
            $assignment_income = $assignment_income->whereHas('resource', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }
        if (auth()->user()->hasRole('manager')) {
            $assignment_income = $assignment_income->whereHas('resource_user', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $assignment_income = $assignment_income->get();
        $income_resources = [];
        foreach ($resource_income as $resource) {
            array_push($income_resources, $resource->user_id);
        }

        foreach ($assignment_income as $assignment) {
            array_push($income_resources, $assignment->employee_id);
        }

        $res_users = [];

        foreach (array_unique($income_resources) as $res) {
            array_push($res_users, Resource::select('user_id')->where('user_id', '=', $res)->first());
        }

        return array_filter($res_users);
    }

    private function planned_util($date, $weeks)
    {
        $planned_util = PlanUtilization::select('wk_hours', 'res_no', 'cost_res_no', 'cost_wk_hours')
            ->where('yearwk', '=', ($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear)
            ->first();

        return $planned_util;
    }

    private function util_hours($date, $res, $weeks, $type, $bill, $request)
    {
        $timeline = Timesheet::selectRaw('IFNULL(SUM(CASE WHEN timeline.is_billable = '.(($request->has('billable') && $request->billable != '') ? $request->billable : $bill).' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END),0) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.employee_id', '=', $res->user_id)
            ->whereHas('project', function ($query) use ($type) {
                $query->where('type', '=', $type);
            })
            ->where('timesheet.year_week', '=', (($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear));

        if ($request->has('company') && $request->company != '') {
            $timeline->where('timesheet.company_id', '=', $request->company);
        }
        $timeline = $timeline->first();

        return $timeline;
    }

    private function get_week_total($util_type)
    {
        $total_week3_income = 0;
        $total_week2_income = 0;
        $total_week1_income = 0;
        $total_week_income = 0;
        $total_percent = 0;
        foreach ($util_type as $util) {
            $total_week3_income += $util['week3'];
            $total_week2_income += $util['week2'];
            $total_week1_income += $util['week1'];
            $total_week_income += $util['week'];
            $total_percent += $util['util_percentage'];
        }

        return [$total_week3_income, $total_week2_income, $total_week1_income, $total_week_income, $total_percent];
    }
}
