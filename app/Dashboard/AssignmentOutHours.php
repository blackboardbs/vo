<?php

namespace App\Dashboard;

use App\Models\Assignment;
use App\Models\Timeline;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class AssignmentOutHours
{
    public function totalAssignmentHours(): int
    {
        return $this->assignments()->sum('hours');
    }

    public function assignments(): Collection
    {
        $ass = Assignment::with(['resource' => function($resource){
            return $resource->select(['id', 'first_name', 'last_name']);
        }, 'project:id,name'])->select('project_id', 'employee_id', 'hours')->whereIn('assignment_status', [2, 3])->groupBy('project_id', 'employee_id', 'hours');
        if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $ass = $ass->where('employee_id', '=', auth()->user()->id);
        } elseif (auth()->user()->hasRole('manager')) {
            $ass = $ass->whereHas('resource_user', function ($query) {
                $query->where('manager_id', '=', auth()->user()->id);
            });
        }

        return $ass->get();
    }

    public function totalTime(): int
    {
        return $this->assignmentOutHoursChart()['total_time']??0;
    }

    public function timeHours()
    {
        return $this->assignmentOutHoursChart()['time_hours']??0;
    }

    private function assignmentOutHoursChart(): array
    {
        $time_hours = [];
        $total_time = 0;
        foreach ($this->assignments() as $a) {
            $timeline = Timeline::selectRaw('IFNULL(SUM(total + (total_m/60)),0) AS hours')->whereHas('timesheet', function ($query) use ($a) {
                if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
                    $query->where('project_id', '=', $a->project_id);
                    $query->where('employee_id', '=', auth()->id());
                } else {
                    $query->where('project_id', '=', $a->project_id);
                    $query->where('employee_id', '=', $a->employee_id);
                }
            });

            if (auth()->user()->hasRole('manager')) {
                $timeline = $timeline->whereHas('timesheet.resource', function ($query) {
                    $query->where('manager_id', '=', auth()->user()->id);
                })->first();
            } else {
                $timeline = $timeline->first();
            }
            $total_time += $timeline->hours;

            array_push($time_hours, [
                'hours' => $a->hours - $timeline->hours,
                'resource' => isset($a->resource) ? $this->resourceInitial($a->resource) : '',
            ]);
        }

        return ['total_time' => $total_time, 'time_hours' => $time_hours];
    }

    private function resourceInitial(User $resource): string
    {
        return substr($resource->first_name, 0, 1).substr($resource->last_name, 0, 1);
    }
}
