<?php

namespace App\Dashboard;

use App\Models\ExpenseTracking;
use App\Models\PlannedExpense;
use App\Models\Timesheet;

class Cashflow
{
    public function invoiced($request, $items, $is_vendor = false, $page = 'invoiced')
    {
        $timesheets = Timesheet::selectRaw('timesheet.id, timesheet.company_id, timesheet.employee_id, timesheet.project_id, timesheet.customer_id, timesheet.year_week, timesheet.first_day_of_week, '.(($is_vendor) ? 'assignment.external_cost_rate AS rate' : 'assignment.invoice_rate AS rate').',
            SUM(IF(DATEDIFF(timesheet.invoice_due_date, CURDATE()) <= 5,timeline.total + (timeline.total_m/60),0)) AS five_days,
            SUM(IF(DATEDIFF(timesheet.invoice_due_date, CURDATE()) > 5 AND DATEDIFF(timesheet.invoice_due_date, CURDATE()) <= 10,timeline.total + (timeline.total_m/60),0)) AS ten_days,
            SUM(IF(DATEDIFF(timesheet.invoice_due_date, CURDATE()) > 10 AND DATEDIFF(timesheet.invoice_due_date, CURDATE()) <= 20,timeline.total + (timeline.total_m/60),0)) AS twenty_days,
            SUM(IF(DATEDIFF(timesheet.invoice_due_date, CURDATE()) > 20 AND DATEDIFF(timesheet.invoice_due_date, CURDATE()) <= 30,timeline.total + (timeline.total_m/60),0)) AS thirty_days,
            SUM(IF(DATEDIFF(timesheet.invoice_due_date, CURDATE()) > 30,timeline.total + (timeline.total_m/60),0)) AS thirty_days_plus
        ')
        ->with([($is_vendor) ? 'user.vendor' : 'user', 'customer', 'company', 'project', 'time_exp' => function ($query) use ($is_vendor) {
            if ($is_vendor) {
                $query->with('account')->selectRaw('timesheet_id, SUM(CASE WHEN claim = 1 THEN amount ELSE 0 END) AS amount, account_id');
                $query->groupBy('timesheet_id', 'account_id');
            } else {
                $query->with('account')->selectRaw('timesheet_id, SUM(CASE WHEN is_billable = 1 THEN amount ELSE 0 END) AS amount, account_id');
                $query->groupBy('timesheet_id', 'account_id');
            }
        }])->leftJoin('assignment', function ($join) {
            $join->on('timesheet.employee_id', 'assignment.employee_id');
            $join->on('timesheet.project_id', 'assignment.project_id');
        })
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.bill_status', 2)
            ->where('timeline.is_billable', 1)
            ->whereHas(($is_vendor) ? 'user.vendor' : 'user');

        if ($request->company) {
            $timesheets = $timesheets->where('timesheet.company_id', $request->company);
        }

        if ($request->team) {
            $timesheets = $timesheets->whereHas('resource', function ($query) use ($request) {
                $query->where('team_id', $request->team);
            });
        }

        if ($request->employee) {
            $timesheets = $timesheets->where('timesheet.employee_id', $request->employee);
        }

        if ($request->yearmonth) {
            $timesheets = $timesheets->where('timesheet.year', substr($request->yearmonth, 0, 4))
                ->where('timesheet.month', substr($request->yearmonth, 4));
        }

        if ($request->customer) {
            $timesheets = $timesheets->where('timesheet.customer_id', $request->customer);
        }

        if ($request->project) {
            $timesheets = $timesheets->where('timesheet.project_id', $request->project);
        }

        $timesheets = $timesheets->groupBy('timesheet.employee_id', 'timesheet.project_id', 'timesheet.customer_id', 'timesheet.year_week')
            ->groupBy(($is_vendor) ? 'assignment.external_cost_rate' : 'assignment.invoice_rate', 'timesheet.id', 'timesheet.company_id')
            ->groupBy('timesheet.first_day_of_week')
            ->orderBy('timesheet.year_week', 'desc');
        $timesheets = ($items == '*') ? $timesheets->get() : $timesheets->paginate($items, ['*'], $page);

        return $timesheets;
    }

    public function not_invoiced($request, $items, $is_vendor = false, $page = 'not_invoiced')
    {
        $not_invoiced = Timesheet::with([(($is_vendor) ? 'user.vendor' : 'user'), 'project', 'company', 'customer', 'time_exp' => function ($query) use ($is_vendor) {
            if ($is_vendor) {
                $query->with('account')->selectRaw('timesheet_id, SUM(CASE WHEN claim = 1 THEN amount ELSE 0 END) AS amount, account_id');
                $query->groupBy('timesheet_id', 'account_id');
            } else {
                $query->with('account')->selectRaw('timesheet_id, SUM(CASE WHEN is_billable = 1 THEN amount ELSE 0 END) AS amount, account_id');
                $query->groupBy('timesheet_id', 'account_id');
            }
        }])
            ->selectRaw('timesheet.id, timesheet.company_id, timesheet.employee_id, timesheet.project_id, timesheet.customer_id, timesheet.year_week, timesheet.first_day_of_week,
            SUM(IF(timeline.is_billable = 1, (timeline.total + (timeline.total_m/60)), 0)) AS hours,
            '.(($is_vendor) ? 'assignment.external_cost_rate' : 'assignment.invoice_rate').' AS rate')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            })
            ->where('timesheet.bill_status', 1)
            ->whereHas(($is_vendor) ? 'user.vendor' : 'user')
            ->groupBy('timesheet.id', 'timesheet.employee_id', 'timesheet.project_id', 'timesheet.customer_id')
            ->groupBy('timesheet.year_week', 'assignment.invoice_rate', 'assignment.external_cost_rate', 'timesheet.company_id')
            ->groupBy('timesheet.first_day_of_week')
            ->orderBy('timesheet.year_week', 'desc');
        $not_invoiced = ($items == '*') ? $not_invoiced->get() : $not_invoiced->paginate($items, ['*'], $page);

        return $not_invoiced;
    }

    public function expense_tracking($request, $items)
    {
        $expense_tracking = ExpenseTracking::with(['account', 'account_element', 'resource'])
            ->selectRaw('id, account_id, account_element_id, employee_id, yearwk,
            SUM(CASE WHEN claimable = 1 THEN amount ELSE 0 END) AS claim_expense,
            SUM(CASE WHEN billable = 1 THEN amount ELSE 0 END) AS billable_expense')
            ->groupBy('id', 'account_id', 'account_element_id', 'employee_id', 'yearwk');
        $expense_tracking = ($items == '*') ? $expense_tracking->get() : $expense_tracking->paginate($items, ['*'], 'expense_tracking');

        return $expense_tracking;
    }

    public function planned_expenses($request, $items, $is_management = false)
    {
        $planned_expenses = PlannedExpense::with(['account'])->selectRaw('account_id,
            SUM(IF(DATEDIFF(plan_exp_date, CURDATE()) <= 5,plan_exp_value,0)) AS five_days,
            SUM(IF(DATEDIFF(plan_exp_date, CURDATE()) > 5 AND DATEDIFF(plan_exp_date, CURDATE()) <= 10,plan_exp_value,0)) AS ten_days,
            SUM(IF(DATEDIFF(plan_exp_date, CURDATE()) > 10 AND DATEDIFF(plan_exp_date, CURDATE()) <= 20,plan_exp_value,0)) AS twenty_days,
            SUM(IF(DATEDIFF(plan_exp_date, CURDATE()) > 20 AND DATEDIFF(plan_exp_date, CURDATE()) <= 30,plan_exp_value,0)) AS thirty_days,
            SUM(IF(DATEDIFF(plan_exp_date, CURDATE()) > 30,plan_exp_value,0)) AS thirty_days_plus')
                ->whereBetween('plan_exp_date', [now()->toDateString(), now()->addDays(60)->toDateString()])
                ->groupBy('account_id');
        $planned_expenses = ($items == '*') ? $planned_expenses->get() : $planned_expenses->paginate($items, ['*'], 'planned_expense');

        return $planned_expenses;
    }
}
