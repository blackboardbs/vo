<?php

namespace App\Dashboard;

use App\Models\Timesheet;

class TimesheetCountChart
{
    public function timesheet_count_charts()
    {
        $timeYear = new HelperFunctions();

        $year_weeks = [
            now()->year.now()->subWeeks(4)->weekOfYear,
            now()->year.now()->subWeeks(3)->weekOfYear,
            now()->year.now()->subWeeks(2)->weekOfYear,
            now()->year.now()->subWeeks(1)->weekOfYear,
        ];
        $timesheet_usage_count = [];

        foreach ($year_weeks as $week) {
            $timesheet_temp = Timesheet::where('year_week', '=', $week);

            if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
                $timesheet_temp = $timesheet_temp->where('employee_id', auth()->user()->id)->get()->count();
            } elseif (auth()->user()->hasRole('manager')) {
                $timesheet_temp = $timesheet_temp->whereHas('resource', function ($query) {
                    $query->where('manager_id', '=', auth()->user()->id);
                })->get()->count();
            } else {
                $timesheet_temp = $timesheet_temp->get()->count();
            }

            array_push($timesheet_usage_count, $timesheet_temp);
        }

        $timesheets_past_year = $timeYear->time_year(now()->subYear()->year);
        $timesheets_this_year = $timeYear->time_year(now()->year);

        $months = [];
        $timesheet_past = [];
        $timesheet_this = [];
        $timesheet_this_val = [];
        $timesheet_last_val = [];

        if (count($timesheets_past_year) > 0) {
            foreach ($timesheets_past_year as $last) {
                $timesheet_past[$last->month] = $last->hours;
            }
        }

        if (count($timesheets_this_year) > 0) {
            foreach ($timesheets_this_year as $current) {
                $timesheet_this[$current->month] = $current->hours;
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $dateObj = \DateTime::createFromFormat('!m', $i);
            $monthName = $dateObj->format('F');
            array_push($months, strtoupper(substr($monthName, 0, 3)));

            if (isset($timesheet_past[$i])) {
                array_push($timesheet_last_val, $timesheet_past[$i]);
            } else {
                array_push($timesheet_last_val, 0);
            }
        }

        for ($i = 1; $i <= now()->month; $i++) {
            if (isset($timesheet_this[$i])) {
                array_push($timesheet_this_val, $timesheet_this[$i]);
            } else {
                array_push($timesheet_this_val, 0);
            }
        }

        $last_year = now()->subYear()->year;
        $this_year = now()->year;

        return [$last_year, $this_year, $year_weeks, $timesheet_usage_count, $timesheet_last_val, $timesheet_this_val, $months];
    }
}
