<?php

namespace App\Dashboard;

use App\Models\ExpenseTracking;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;

class Expenses
{
    public function dash_expenses()
    {
        $exp_tracking = ExpenseTracking::with(['approver', 'resource', 'assignment.project'])
            ->whereNull('payment_date')
            ->where('claimable', '=', 1);
        /*$time_exp = TimeExp::with(['timesheet.user', 'timesheet.project', 'timesheet'])
            ->whereHas('timesheet', function ($query){
                $query->
                $query->join('assignment', function ($join){
                    $join->on('timesheet.employee_id', 'assignment.employee_id');
                    $join->on('timesheet.project_id', 'assignment.project_id');
                });
            })
            ->whereNull('paid_date')
            ->where('claim', '=', 1);*/
        $time_exp = Timesheet::with(['user:id,first_name,last_name', 'project:id,name'])
            ->selectRaw('timesheet.id, time_exp.id AS exp_id, timesheet.employee_id, timesheet.project_id, timesheet.year_week,
            time_exp.claim_auth_date, time_exp.description, time_exp.amount, assignment.assignment_approver')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', 'assignment.employee_id');
                $join->on('timesheet.project_id', 'assignment.project_id');
            })
            ->leftJoin('time_exp', 'timesheet.id', 'time_exp.timesheet_id')
            ->whereNull('time_exp.paid_date')
            ->where('time_exp.claim', 1);

        if (! auth()->user()->hasAnyRole(['manager', 'admin_manager', 'admin'])) {
            $exp_tracking = $exp_tracking->where('employee_id', '=', auth()->id());
            $time_exp = $time_exp->where('timesheet.employee_id', '=', auth()->id());
        }
        if (auth()->user()->hasRole('manager')) {
            $exp_tracking = $exp_tracking->whereHas('actual_resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });

            $time_exp = $time_exp->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $time_exp = $time_exp->orderBy('timesheet.id', 'desc')->get();
        $exp_tracking = $exp_tracking->orderBy('id', 'desc')->get();
        $expenses = [];

        foreach ($exp_tracking as $expense) {
            array_push($expenses, [
                'approver' => isset($expense->approver) ? substr($expense->approver->first_name, 0, 1).'. '.$expense->approver->last_name : '',
                'consultant' => isset($expense->resource) ? substr($expense->resource->first_name, 0, 1).'. '.$expense->resource->last_name : '',
                'approve_date' => $expense->approved_on,
                'project' => isset($expense->assignment->project) ? $expense->assignment->project->name : '',
                'year_week' => $expense->yearwk,
                'amount' => $expense->amount,
                'description' => $expense->description,
                'url' => route('exptracking.edit', $expense),
                'pay_url' => route('exptracking.edit', $expense),
                'timesheet_maintenance' => null,
            ]);
        }
        foreach ($time_exp as $expense) {
            $user = User::find($expense->assignment_approver);
            array_push($expenses, [
                'approver' => isset($user) ? substr($user->first_name, 0, 1).'. '.$user->last_name : '',
                'consultant' => isset($expense->user) ? substr($expense->user->first_name, 0, 1).'. '.$expense->user->last_name : '',
                'approve_date' => $expense->claim_auth_date,
                'project' => isset($expense->project) ? $expense->project->name : '',
                'year_week' => $expense->year_week,
                'amount' => $expense->amount,
                'description' => $expense->description,
                'url' => route('expense.approve', $expense->exp_id),
                'pay_url' => route('expense.paid', $expense->exp_id),
                'timesheet_maintenance' => route('timesheet.maintain', $expense),
            ]);
        }

        return $expenses;
    }
}
