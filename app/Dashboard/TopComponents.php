<?php

namespace App\Dashboard;

use App\Models\Log;
use App\Models\PlanUtilization;
use App\Models\Resource;
use App\Models\Timesheet;

class TopComponents
{
    public function logins_top($status)
    {
        return Log::where('login_status', '=', $status)->whereBetween('date', [now()->subDays(30)->toDateString(), now()->toDateString()]);
    }

    public function timesheets_count(): int
    {
        $timesheets_count = Timesheet::where('status_id', '=', 1);

        if (auth()->user()->hasRole('manager')) {
            $timesheets_count = $timesheets_count->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->user()->id);
            });
        }
        if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $timesheets_count->where('employee_id', '=', auth()->user()->id);
        }

        return $timesheets_count->get()->count();
    }

    public function three_weeks_utilization(): float|int
    {
        $planned = PlanUtilization::whereBetween('yearwk', [$this->date->now()->year.$this->date->now()->subWeeks(3)->weekOfYear, $this->date->now()->year.$this->date->now()->subWeeks(1)->weekOfYear])->get();
        $actual_3 = 0;
        $planned_3 = 0;
        $res_no = 0;
        foreach ($planned as $plan) {
            $actual = Timesheet::selectRaw('IFNULL(SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END), 0) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.year_week', '=', $plan->yearwk);

            if (auth()->user()->hasAnyRole(['super_admin', 'admin', 'admin_manager'])) {
                $actual_3 += $actual->first()->actual_hours;
                $planned_3 += $plan->wk_hours * $plan->res_no;
            }

            if (auth()->user()->hasRole('manager')) {
                $actual_3 += $actual->whereHas('resource', function ($query) {
                    $query->where('manager_id', '=', auth()->user()->id);
                })->first()->actual_hours;
                $res_no += Resource::where('manager_id', '=', auth()->user()->id)->get()->count();
                $planned_3 += $plan->wk_hours * $res_no;
            }

            if (auth()->user()->hasAnyRole(['consultant', 'contractor', 'vendor'])) {
                $actual_3 = $actual->where('timesheet.employee_id', '=', auth()->id())->first()->actual_hours;
                $planned_3 += $plan->wk_hours;
            }
        }
        $util_percentage_3 = ($actual_3 == 0 || $planned_3 == 0) ? 0 : ($actual_3 / $planned_3) * 100;

        return $util_percentage_3;
    }
}
