<?php

namespace App\Dashboard;

use App\Models\Assignment;
use App\Models\Module;
use App\Models\Timesheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Commission
{
    public function commission(Request $request, $item, $year_month = '201911', $is_cashflow = false)
    {
        $consultants = Assignment::with('consultant')->selectRaw("assignment.employee_id as 'employee_id',CONCAT(users.first_name,' ',users.last_name) as 'full_name'") 
            ->where('billable', '=', 1)
            ->whereNotNull('rate')
            ->whereIn('status', [1, 2, 3])
            ->leftJoin('users','assignment.employee_id','=','users.id')
            ->groupBy('assignment.employee_id','users.first_name','users.last_name');

        if ($request->has('company') && $request->company != '') {
            $consultants = $consultants->whereHas('consultant', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }

        if ($request->has('team') && $request->team != '') {
            $consultants = $consultants->whereHas('resource_user', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }

        if ($request->has('employee') && $request->employee != '') {
            $consultants = $consultants->where('employee_id', '=', $request->employee);
        }
        if ($request->has('billable') && $request->billable != '') {
            $consultants = $consultants->where('billable', '=', $request->billable);
        }

        $module = Module::where('name', '=', 'App\Models\ReportConsultantBonus')->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $consultants = $consultants->whereIn('employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $consultants = $consultants->get();

        // dd($consultants->pluck('full_name','employee_id'));

        $ass_details = Timesheet::with(['user', 'project'])->selectRaw('assignment.id, timesheet.employee_id, timesheet.year, timesheet.month, timesheet.project_id, timesheet.customer_id,
                        SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS hours,
                        assignment.rate')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            })
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'assignment.employee_id');
            })
            ->whereIn('timesheet.employee_id', $consultants->pluck('employee_id'))
            ->whereNotNull('assignment.rate');
        if ($is_cashflow) {
            $ass_details = $ass_details->where('timesheet.month', '=', substr($year_month, 4))
                ->where('timesheet.year', '=', substr($year_month, 0, 4));
        } else {
            $ass_details = $ass_details->where('timesheet.month', '=', (($request->has('yearmonth') && $request->yearmonth != '') ? substr($request->yearmonth, 4) : now()->subMonth()->month))
                ->where('timesheet.year', '=', (($request->has('yearmonth') && $request->yearmonth != '') ? substr($request->yearmonth, 0, 4) : now()->year));
        }
        $ass_details = $ass_details->groupBy('timesheet.year', 'timesheet.month', 'timesheet.project_id', 'timesheet.customer_id', 'rate', 'assignment.id')
            ->groupBy('timesheet.employee_id')
            ->orderBy('users.last_name')
            ->orderBy('users.first_name')
            ->get();

        $commission1 = [];
        $commission2 = [];
        $commission3 = [];
        $commission4 = [];
        $commission5 = [];
        $commission6 = [];
        $commission7 = [];
        $commission8 = [];
        $commission9 = [];
        $commission10 = [];
        $commission11 = [];
        $commission12 = [];
        $employee_dropdown = [];

        $hours = 0;
        $value = 0;
        foreach ($ass_details as  $key => $consultant) {
            if ($key == 0 || $consultant->employee_id != $ass_details[$key - 1]->employee_id) {
                $hours = 0;
                $rate = 0;
                $value = 0;
                $hours += $consultant->hours;
                $value += ($consultant->hours * $consultant->rate);
            } else {
                $hours += $consultant->hours;
                $value += ($consultant->hours * $consultant->rate);
            }

            if (isset($ass_details[$key + 1]->employee_id) ? ($consultant->employee_id != $ass_details[$key + 1]->employee_id) : '' || $key == (count($ass_details) - 1)) {
                $rate = ($hours > 0) ? ($value / $hours) : 0;
                $comm1 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour01) && ($hours < $consultant->resource->commission->hour02) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm01 / 100) : 0;
                $comm2 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour02) && ($hours < $consultant->resource->commission->hour03) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm02 / 100) : 0;
                $comm3 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour03) && ($hours < $consultant->resource->commission->hour04) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm03 / 100) : 0;
                $comm4 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour04) && ($hours < $consultant->resource->commission->hour05) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm04 / 100) : 0;
                $comm5 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour05) && ($hours < $consultant->resource->commission->hour06) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm05 / 100) : 0;
                $comm6 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour06) && ($hours < $consultant->resource->commission->hour07) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm06 / 100) : 0;
                $comm7 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour07) && ($hours < $consultant->resource->commission->hour08) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm07 / 100) : 0;
                $comm8 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour08) && ($hours < $consultant->resource->commission->hour09) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm08 / 100) : 0;
                $comm9 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour09) && ($hours < $consultant->resource->commission->hour10) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm09 / 100) : 0;
                $comm10 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour10) && ($hours < $consultant->resource->commission->hour11) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm10 / 100) : 0;
                $comm11 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour11) && ($hours < $consultant->resource->commission->hour12) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm11 / 100) : 0;
                $comm12 = (isset($consultant->resource->commission) && ($hours >= $consultant->resource->commission->hour12) && ($rate > $consultant->resource->commission->min_rate)) ? $value * ($consultant->resource->commission->comm12 / 100) : 0;

                $commission1[$consultant->employee_id] = $comm1;
                $commission2[$consultant->employee_id] = $comm2;
                $commission3[$consultant->employee_id] = $comm3;
                $commission4[$consultant->employee_id] = $comm4;
                $commission5[$consultant->employee_id] = $comm5;
                $commission6[$consultant->employee_id] = $comm6;
                $commission7[$consultant->employee_id] = $comm7;
                $commission8[$consultant->employee_id] = $comm8;
                $commission9[$consultant->employee_id] = $comm9;
                $commission10[$consultant->employee_id] = $comm10;
                $commission11[$consultant->employee_id] = $comm11;
                $commission12[$consultant->employee_id] = $comm12;
            }
            $employee_dropdown[$consultant->employee_id] = $consultant->user?->name();
        }

        $parameters = [
            //'consultants' => $consultants,
            'timesheets' => $ass_details,
            //'total_hours' => 0,
            //'total_rate' => 0,
            //'total_value' => 0,
            'commission1' => $commission1,
            'commission2' => $commission2,
            'commission3' => $commission3,
            'commission4' => $commission4,
            'commission5' => $commission5,
            'commission6' => $commission6,
            'commission7' => $commission7,
            'commission8' => $commission8,
            'commission9' => $commission9,
            'commission10' => $commission10,
            'commission11' => $commission11,
            'commission12' => $commission12,
            'employee_dropdown' => $employee_dropdown
        ];

        return $parameters;
    }
}
