<?php

namespace App\Dashboard;

use App\Models\Anniversary;

class Anniversaries
{
    public function anniversary()
    {
        $anniversary = Anniversary::with(['anniversary_type:id,description', 'relationshipd:id,description', 'resource:id,first_name,last_name'])
            ->selectRaw('id, MONTHNAME(anniversary_date) AS month_name, DAYOFMONTH(anniversary_date) AS day_of_month, name, relationship, anniversary, emp_id')
            ->whereMonth('anniversary_date', '>=', now()->month)
            ->whereMonth('anniversary_date', '<=', now()->addDays(30)->month)
            ->orderByRaw('MONTH(anniversary_date)')
            ->orderByRaw('DAY(anniversary_date)')
            ->groupBy('id', 'month_name', 'day_of_month', 'name', 'relationship', 'anniversary', 'emp_id')
            ->get();

        return $anniversary;
    }
}
