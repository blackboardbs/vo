<?php


namespace App\Dashboard;


use App\Timesheet;

class MonthlyConsulting
{

    public function monthly_consulting(){

        $helper = new HelperFunctions();

        $years_value_3 = [];
        $years_value_2 = [];
        $years_value_1 = [];

        foreach ($helper->get_monthly_consulting(now(), 2) as $monthly_value) array_push($years_value_3, $monthly_value->value);
        if (count($helper->get_monthly_consulting(now(), 2)) > 0)  for ($i = 1; $i < $helper->get_monthly_consulting(now(), 2)[0]->month; $i++) array_unshift($years_value_3, 0);


        foreach ($helper->get_monthly_consulting(now(), 1) as $monthly_value) array_push($years_value_2, $monthly_value->value);
        if (count($helper->get_monthly_consulting(now(), 1)) > 0) for ($i = 1; $i < $helper->get_monthly_consulting(now(), 1)[0]->month; $i++) array_unshift($years_value_2, 0);

        foreach ($helper->get_monthly_consulting(now(), 0) as $monthly_value) array_push($years_value_1, $monthly_value->value);
        if (count($helper->get_monthly_consulting(now(), 0)) > 0) for ($i = 1; $i < $helper->get_monthly_consulting(now(), 0)[0]->month; $i++) array_unshift($years_value_1, 0);

        $total_year_1_value = 0;
        $total_year_1_hours = 0;
        $total_year_2_value = 0;
        $total_year_2_hours = 0;
        $total_year_3_value = 0;
        $total_year_3_hours = 0;
        $total_year_4_value = 0;
        $total_year_4_hours = 0;

        foreach ($helper->get_monthly_consulting(now(), 0) as $year_1_value):
            $total_year_1_value += $year_1_value->value;
            $total_year_1_hours += $year_1_value->hours;
        endforeach;

        foreach ($helper->get_monthly_consulting(now(), 1) as $year_2_value):
            $total_year_2_value += $year_2_value->value;
            $total_year_2_hours += $year_2_value->hours;
        endforeach;

        foreach ($helper->get_monthly_consulting(now(), 2) as $year_3_value):
            $total_year_3_value += $year_3_value->value;
            $total_year_3_hours += $year_3_value->hours;
        endforeach;

        foreach ($helper->get_monthly_consulting(now(), 3) as $year_4_value):
            $total_year_4_value += $year_4_value->value;
            $total_year_4_hours += $year_4_value->hours;
        endforeach;

        return [
            $years_value_3,
            $years_value_2,
            $years_value_1,
            $total_year_1_value,
            $total_year_1_hours,
            $total_year_2_value,
            $total_year_2_hours,
            $total_year_3_value,
            $total_year_3_hours,
            $total_year_4_value,
            $total_year_4_hours,
        ];
    }
}