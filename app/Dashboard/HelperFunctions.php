<?php

namespace App\Dashboard;

use App\Models\Assignment;
use App\Models\DefaultDashboard;
use App\Models\LandingPageDashboard;
use App\Models\Leave;
use App\Models\Log;
use App\Models\MessageBoard;
use App\Models\PlanUtilization;
use App\Models\Prospect;
use App\Models\Resource;
use App\Models\RoleUser;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HelperFunctions
{

    public $notices;

    public $pipelines;
    

    public function get_utilized_resources($date, $assignments_status, $project_type, $request)
    {
        $resource_income = Resource::select('user_id')
            ->where('util', '=', $project_type)
            ->where('project_type', '=', 1)
            ->where('util_date_from', '<=', $date->toDateString())->where('util_date_to', '>=', $date->now()->subWeeks(3)->startOfWeek()->toDateString());
        if (auth()->user()->isAn('manager')) {
            $resource_income = $resource_income->where('manager_id', '=', auth()->id());
        }
        if ($request->has('company') && $request->company != '') {
            $resource_income = $resource_income->whereHas('user', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }
        $resource_income = $resource_income->get();
        $assignment_income = Assignment::select('employee_id')->whereIn('assignment_status', [2, 3])->where('billable', '=', $assignments_status)->where('status', '=', 1)->groupBy('employee_id');
        if ($request->has('company') && $request->company != '') {
            $assignment_income = $assignment_income->whereHas('resource', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }
        if (auth()->user()->isAn('manager')) {
            $assignment_income = $assignment_income->whereHas('resource_user', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $assignment_income = $assignment_income->get();
        $income_resources = [];
        foreach ($resource_income as $resource) {
            array_push($income_resources, $resource->user_id);
        }

        foreach ($assignment_income as $assignment) {
            array_push($income_resources, $assignment->employee_id);
        }

        $res_users = [];

        foreach (array_unique($income_resources) as $res) {
            array_push($res_users, Resource::select('user_id')->where('user_id', '=', $res)->first());
        }

        return $res_users;
    }

    public function get_ageing_days_value($date, $request)
    {
        $timesheet = Timesheet::selectRaw('timesheet.customer_id, 
                SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))*assignment.invoice_rate), 2) ELSE 0 END) AS invoice_value')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->whereBetween('timesheet.invoice_date', $date)
            ->where('timesheet.bill_status', '=', 2)
            ->groupBy('timesheet.customer_id');
        if ($request->has('company') && $request->company != '') {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('billable') && $request->billable != '') {
            $timesheet = $timesheet->where('timeline.is_billable', '=', $request->billable);
        }
        $timesheet = $timesheet->get();

        return $timesheet;
    }

    public function planned_util($date, $weeks)
    {
        $planned_util = PlanUtilization::select('wk_hours', 'res_no', 'cost_res_no', 'cost_wk_hours')
            ->where('yearwk', '=', ($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear)
            ->first();

        return $planned_util;
    }

    public function get_week_total($week_values)
    {
        $total_week = 0;
        foreach ($week_values as $week) {
            $total_week += isset($week->hours) ? $week->hours : 0;
        }

        return $total_week;
    }

    public function util_hours($date, $res, $weeks, $type, $bill, $request)
    {
        $timeline = Timesheet::selectRaw('IFNULL(SUM(CASE WHEN timeline.is_billable = '.(($request->has('billable') && $request->billable != '') ? $request->billable : $bill).' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END),0) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.employee_id', '=', $res->user_id)
            ->whereHas('project', function ($query) use ($type) {
                $query->where('type', '=', $type);
            })
            ->where('timesheet.year_week', '=', (($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear));

        if ($request->has('company') && $request->company != '') {
            $timeline->where('timesheet.company_id', '=', $request->company);
        }
        $timeline = $timeline->first();

        return $timeline;
    }

    public function get_planned_util($date, $weeks)
    {
        $planned_util = PlanUtilization::select('yearwk', 'res_no', 'wk_hours')->where('yearwk', '=', (($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear))->first();

        return $planned_util;
    }

    public function get_actual_hours($date, $weeks)
    {
        $actual_hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.year_week', '=', (($date->now()->subWeeks($weeks)->weekOfYear < 10) ? $date->now()->subWeeks($weeks)->year.'0'.$date->now()->subWeeks($weeks)->weekOfYear : $date->now()->subWeeks($weeks)->year.$date->now()->subWeeks($weeks)->weekOfYear));

        if (auth()->user()->isAn('consultant') || auth()->user()->isAn('contractor')) {
            $actual_hours = $actual_hours->where('timesheet.employee_id', '=', auth()->id());
        } elseif (auth()->user()->isAn('manager')) {
            $actual_hours = $actual_hours->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $actual_hours = $actual_hours->first();

        return $actual_hours;
    }

    public function time_year($date)
    {
        return Timesheet::selectRaw('timesheet.month, IFNULL(SUM(timeline.total + (timeline.total_m/60)),0) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.year', '=', $date)
            ->groupBy('timesheet.month')
            ->get();
    }

    public function get_top_customers($date, $request)
    {
        $base_date = $date->now()->subMonths(12)->toDateString();
        $now = $date->now()->toDateString();
        $customer = Timesheet::with('customer')->selectRaw('timesheet.customer_id, SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours, 
                            SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))*assignment.invoice_rate),2) ELSE 0 END) AS value')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            })
            ->whereBetween('timesheet.year_week', [substr($base_date, 0, 4).(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear), substr($now, 0, 4).(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear)])
            ->groupBy('timesheet.customer_id')
            ->orderBy('value', 'desc')
            ->limit(10);
        if ($request->has('company') && $request->company != '') {
            $customer = $customer->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('billable') && $request->billable != '') {
            $customer = $customer->where('timeline.is_billable', '=', $request->billable);
        }
        if (auth()->user()->hasRole('manager')) {
            $customer = $customer->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $customer = $customer->get();

        return $customer;
    }

    public function get_top_solutions($date, $column, $request)
    {
        $base_date = $date->now()->subMonths(12)->toDateString();
        $now = $date->now()->toDateString();

        $solutions = Assignment::with(['system_desc', 'location'])->selectRaw('assignment.'.$column.', SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))*assignment.invoice_rate),2) ELSE 0 END) AS value,
            SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND((timeline.total + (timeline.total_m/60))) ELSE 0 END) AS hours')
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
            })
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('assignment.status', '=', 1)
            ->where('assignment.billable', '=', 1)
            ->whereBetween('timesheet.year_week', [substr($base_date, 0, 4).(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear), substr($now, 0, 4).(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear)]);

        if (auth()->user()->hasRole('manager')) {
            $solutions = $solutions->whereHas('resource_user', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }

        $solutions = $solutions->groupBy('assignment.'.$column)
            ->orderBy('value', 'desc')
            ->limit(10);
        if ($request->has('company') && $request->company != '') {
            $solutions = $solutions->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('billable') && $request->billable != '') {
            $solutions = $solutions->where('timeline.is_billable', '=', $request->billable);
        }
        $solutions = $solutions->get();

        return $solutions;
    }

    public function get_monthly_consulting($date, $years)
    {
        $timesheet = Timesheet::selectRaw('timesheet.month, timesheet.year, SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))*assignment.invoice_rate),2) ELSE 0 END) AS value,
        SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND((timeline.total + (timeline.total_m/60))) ELSE 0 END) AS hours')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.year', '=', $date->now()->subYears($years)->year);
        if (auth()->user()->hasRole('manager')) {
            $timesheet = $timesheet->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $timesheet = $timesheet->groupBy('timesheet.month', 'timesheet.year')
            ->get();

        return $timesheet;
    }

    public function get_year_to_date($year)
    {
        $timesheet = Timesheet::selectRaw('timesheet.year, SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours, SUM(CASE WHEN timeline.is_billable = 1 THEN ((timeline.total + (timeline.total_m/60))*assignment.invoice_rate) ELSE 0 END) AS value')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->where('timesheet.year', '=', now()->subYear($year)->year)
            ->whereBetween('timesheet.month', [1, now()->month]);
        if (auth()->user()->hasRole('manager')) {
            $timesheet = $timesheet->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }

        $timesheet = $timesheet->groupBy('timesheet.year')
            ->orderBy('timesheet.year', 'desc')
            ->first();

        return $timesheet;
    }

    public function user_dashboard($request)
    {
        $top_components = LandingPageDashboard::where('user_id', '=', auth()->user()->id);
        if (isset($request->status) && isset($request->name)) {
            $top_components = $top_components->where('dashboard_name', '=', $request->name);
        }
        $top_components = $top_components->where('status_id', '=', isset($request->status) ? $request->status : 1)->get();

        if (count($top_components) < 1) {

            $user_role = auth()->user()?->roles?->map(fn($role) => $role->id)->toArray();
            $top_components = DefaultDashboard::whereIn('role_id',$user_role)->where('status_id', '=', 1)->first();
            $comp_1 = explode(',', isset($top_components->top_component) ? $top_components->top_component : '');
            $comp_2 = explode(',', isset($top_components->charts_tables) ? $top_components->charts_tables : '');

            $components = array_merge($comp_1, $comp_2);
        } else {
            $components = [];
            foreach ($top_components as $dashboard_comp) {
                array_push($components, $dashboard_comp->component);
            }
        }

        return $components;
    }

    /**
     * @return mixed
     */
    public function create_paginate($array_items, $request, $items_in_number, $page_name = 'page')
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($array_items);
        $perPage = $items_in_number;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $items = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $items->setPath($request->url());
        //$items = $items->setPageName($page_name);

        return $items;
    }

    public function chartLogins(): Collection
    {
        return Log::select('date', 'login_status')->whereBetween('date', [now()->subDays(14)->toDateString(), now()->subDay()->toDateString()])->orderBy('date', 'desc')->groupBy('date')->groupBy('login_status')->get();
    }
}
