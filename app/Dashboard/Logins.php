<?php

namespace App\Dashboard;

use App\Models\Log;

class Logins
{
    public function logins($status)
    {
        $logs = new HelperFunctions();
        $login = [];

        foreach ($logs->chartLogins() as $log) {
            if ($log->login_status == $status) {
                array_push($login, Log::where('date', '=', $log->date)->where('login_status', '=', $status)->get()->count());
            }
        }

        return $login;
    }
}
