<?php

namespace App\Dashboard;

use App\Models\Timesheet;

class WeeklyUtilization
{
    public function weekly_utilization($bill_status, $project_type, $request)
    {
        $timesheet = Timesheet::selectRaw('timesheet.week, SUM(CASE WHEN timeline.is_billable = '.(($request->has('billable') && $request->billable != '') ? $request->billable : $bill_status).' THEN ROUND(timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereHas('project', function ($query) use ($project_type) {
                $query->where('type', '=', $project_type);
            })
            ->where('timesheet.year_week', '>=', now()->subWeeks(25)->year.((now()->subWeeks(25)->weekOfYear < 10) ? '0'.now()->subWeeks(25)->weekOfYear : now()->subWeeks(25)->weekOfYear))
            ->groupBy('timesheet.year_week')
            ->groupBy('timesheet.week')
            ->orderBy('timesheet.year_week');
        if ($request->has('company') && $request->company != '') {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }
        if (auth()->user()->hasRole('manager')) {
            $timesheet = $timesheet->whereHas('resource', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }
        $timesheet = $timesheet->get();

        return $timesheet;
    }
}
