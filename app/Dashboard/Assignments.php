<?php

namespace App\Dashboard;

use App\Models\Assignment;
use App\Models\Config;
use App\Models\Module;
use Illuminate\Support\Facades\Auth;

class Assignments
{
    public function open_assignments($request, $items, $bill)
    {
        $open_assignments = Assignment::with(['project', 'consultant'])->selectRaw('assignment.id, assignment.project_id, assignment.employee_id, assignment.start_date, assignment.end_date, assignment.hours,
                            SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS billable_hours,
                            SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS non_billable_hours, IFNULL(assignment.invoice_rate, 0) AS invoice_rate,
                            IFNULL(assignment.internal_cost_rate, 0) AS internal_cost_rate, IFNULL(assignment.external_cost_rate, 0) AS external_cost_rate, assignment.billable')
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
            })
            ->whereHas('project')
            ->whereHas('consultant')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereIn('assignment.assignment_status', [2, 3])
            ->where('assignment.billable', '=', (($request->has('billable') > 0 && $request->billable != '') ? $request->billable : $bill));

        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $open_assignments = $open_assignments->whereIn('assignment.employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if ($request->company > 0 && $request->company != null) {
            $open_assignments = $open_assignments->whereHas('project', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }

        if (auth()->user()->hasRole('manager')) {
            $open_assignments = $open_assignments->whereHas('resource_user', function ($query) {
                $query->where('manager_id', '=', auth()->id());
            });
        }

        $open_assignments = $open_assignments->groupBy('assignment.project_id')
            ->groupBy('assignment.employee_id')
            ->groupBy('assignment.start_date')
            ->groupBy('assignment.end_date')
            ->groupBy('assignment.hours')
            ->groupBy('assignment.internal_cost_rate')
            ->groupBy('assignment.external_cost_rate')
            ->groupBy('assignment.invoice_rate')
            ->groupBy('assignment.billable')
            ->groupBy('assignment.id')
            ->sortable(['end_date' => 'desc'])
            ->paginate($items, ['*'], 'open_assignments');

        $total_outstanding_amount = 0;
        $completion_perc = [];
        $outstanding_amount = [];

        foreach ($open_assignments as $assignment) {
            try {
                if ($assignment->billable == 1) {
                    $local_completion_percentage = ($assignment->billable_hours / $assignment->hours) * 100;
                } elseif ($assignment->non_billable_hours != 0) {
                    $local_completion_percentage = ($assignment->non_billable_hours / $assignment->hours) * 100;
                } else {
                    $local_completion_percentage = 0;
                }
            } catch (\DivisionByZeroError $e) {
                $local_completion_percentage = 0;
            }

            if ($assignment->billable == 1) {
                $r_out = ($assignment->hours - $assignment->billable_hours) * $assignment->invoice_rate;
            } else {
                $r_out = ($assignment->hours - $assignment->non_billable_hours) * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            }
            array_push($completion_perc, $local_completion_percentage);
            array_push($outstanding_amount, $r_out);

            $total_outstanding_amount += $r_out;
        }

        $configs = Config::first();
        $company = ($configs?->company_id > 0 || $configs?->company_id != null) ? $configs?->company_id : 1;

        return [$open_assignments, $total_outstanding_amount, $completion_perc, $outstanding_amount, $configs, $company];
    }

    public function sort_by_hours($request, $bill, $sortBy = 'remaining_hours')
    {
        $open_assignments = [];
        foreach ($this->open_assignments($request, 100, $bill)[0] as $assignment) {
            array_push($open_assignments, [
                'id' => $assignment->id,
                'project' => $assignment->project->name,
                'consultant' => substr($assignment->consultant->first_name, 0, 1).'. '.$assignment->consultant->last_name,
                'start_date' => $assignment->start_date,
                'end_date' => $assignment->end_date,
                'complete_percentage' => number_format((($assignment->billable_hours > 0) && ($assignment->hours > 0)) ? ($assignment->billable_hours / $assignment->hours) * 100 : 0),
                'remaining_hours' => $assignment->hours - $assignment->billable_hours,
            ]);
        }

        return collect(array_slice($open_assignments, 0, 10))->sortBy($sortBy)->values()->all();
    }
}
