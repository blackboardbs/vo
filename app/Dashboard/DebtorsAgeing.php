<?php

namespace App\Dashboard;

use App\Models\CustomerInvoice;
use App\Models\VendorInvoice;
use Carbon\Carbon;

class DebtorsAgeing
{
    public function debtor_ageing($request, $is_vendor = false)
    {
        if (! $is_vendor) {
            $customer_invoices = CustomerInvoice::with('customer')
                ->where('bill_status', 1)
                ->whereHas('customer')
                ->orderBy('customer_id')->get();
        } else {
            $vendor_invoices = VendorInvoice::with('vendor')
                ->where('vendor_invoice_status', 1)
                ->whereHas('vendor')
                ->orderBy('vendor_id')->get();
        }

        $invoice_type = ($is_vendor) ? $vendor_invoices : $customer_invoices;

        $debtors_ageing = [];
        $total_due_30 = 0;
        $total_due_60 = 0;
        $total_due_90 = 0;
        $total_due_90_plus = 0;
        $customer = '';

        foreach ($invoice_type as $key => $invoice) {
            if ($key != 0 && isset($invoice->customer->customer_name) && ($customer != $invoice->customer->customer_name)) {
                array_push($debtors_ageing, [
                    'customer' => ($is_vendor) ? $invoice_type[$key - 1]->vendor->vendor_name : $invoice_type[$key - 1]->customer->customer_name,
                    'customer_id' => ($is_vendor) ? $invoice_type[$key - 1]->vendor_id : $invoice_type[$key - 1]->customer_id,
                    'total_due_30' => $total_due_30,
                    'total_due_60' => $total_due_60,
                    'total_due_90' => $total_due_90,
                    'total_due_90_plus' => $total_due_90_plus,
                ]);
                $total_due_30 = 0;
                $total_due_60 = 0;
                $total_due_90 = 0;
                $total_due_90_plus = 0;
                $value = ($is_vendor) ? $invoice->vendor_invoice_value : $invoice->invoice_value;
                if ($this->diffInDays($invoice) <= 30) {
                    $total_due_30 = $value;
                } elseif (($this->diffInDays($invoice) > 30) && ($this->diffInDays($invoice) <= 60)) {
                    $total_due_60 = $value;
                } elseif (($this->diffInDays($invoice) > 60) && ($this->diffInDays($invoice) <= 90)) {
                    $total_due_90 = $value;
                } else {
                    $total_due_90_plus = $value;
                }
                $name = ($is_vendor) ? $invoice->vendor->vendor_name : $invoice->customer->customer_name;
                if (($key == (count($invoice_type) - 1)) && ($customer != $name)) {
                    $total_due_30 = 0;
                    $total_due_60 = 0;
                    $total_due_90 = 0;
                    $total_due_90_plus = 0;
                    if ($this->diffInDays($invoice) <= 30) {
                        $total_due_30 = $value;
                    } elseif (($this->diffInDays($invoice) > 30) && ($this->diffInDays($invoice) <= 60)) {
                        $total_due_60 = $value;
                    } elseif (($this->diffInDays($invoice) > 60) && ($this->diffInDays($invoice) <= 90)) {
                        $total_due_90 = $value;
                    } else {
                        $total_due_90_plus = $invoice->invoice_value;
                    }
                    array_push($debtors_ageing, [
                        'customer' => ($is_vendor) ? $invoice->vendor->vendor_name : $invoice->customer->customer_name,
                        'customer_id' => ($is_vendor) ? $invoice->vendor_id : $invoice->customer_id,
                        'total_due_30' => $total_due_30,
                        'total_due_60' => $total_due_60,
                        'total_due_90' => $total_due_90,
                        'total_due_90_plus' => $total_due_90_plus,
                    ]);
                }
            } else {
                $value = ($is_vendor) ? $invoice->vendor_invoice_value : $invoice->invoice_value;
                if ($this->diffInDays($invoice) <= 30) {
                    $total_due_30 += $value;
                } elseif ($this->diffInDays($invoice) > 30 && $this->diffInDays($invoice) <= 60) {
                    $total_due_60 += $value;
                } elseif (($this->diffInDays($invoice) > 60) && ($this->diffInDays($invoice) <= 90)) {
                    $total_due_90 += $value;
                } else {
                    $total_due_90_plus += $value;
                }
            }
            $customer = ($is_vendor) ? $invoice->vendor->vendor_name : $invoice->customer->customer_name;
        }

        return $debtors_ageing;
    }

    private function diffInDays($invoice): int
    {
        $date = Carbon::parse($invoice->invoice_date);
        $diff_in_days = $date->diffInDays(now());

        return $diff_in_days;
    }
}
