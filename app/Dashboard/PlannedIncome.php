<?php

namespace App\Dashboard;

use App\Models\ExpenseTracking;
use App\Models\PlannedExpense;

class PlannedIncome
{
    public function planned_expenses()
    {
        $planned = PlannedExpense::with('account')
            ->select('account_id', 'plan_exp_value', 'plan_exp_date')
            ->whereHas('account')
            ->where('plan_exp_date', '>=', now()->startOfMonth()->toDateString())
            ->groupBy('account_id', 'plan_exp_value', 'plan_exp_date')
            ->orderBy('account_id')
            ->get();
        $planned_expenses = [];

        $this_month = 0;
        $month_plus_1 = 0;
        $month_plus_2 = 0;
        $month_plus_3 = 0;
        $account = '';
        foreach ($planned as $key => $expense) {
            if (($account != $expense->account_id) && ($account != '')) {
                $current_exp = $planned[$key - 1];
                array_push($planned_expenses, [
                    'account' => ($current_exp->account) ? $current_exp->account->description : '',
                    'this_month' => $this_month,
                    'month_plus_1' => $month_plus_1,
                    'month_plus_2' => $month_plus_2,
                    'month_plus_3' => $month_plus_3,
                ]);

                $this_month = 0;
                $month_plus_1 = 0;
                $month_plus_2 = 0;
                $month_plus_3 = 0;
                if (isset($expense) && ($expense->plan_exp_date <= now()->endOfMonth()->toDateString())) {
                    $this_month += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonth()->endOfMonth()->toDateString())) {
                    $month_plus_1 += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->addMonth()->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonths(2)->endOfMonth()->toDateString())) {
                    $month_plus_2 += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->addMonths(2)->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonths(3)->endOfMonth()->toDateString())) {
                    $month_plus_3 += $expense->plan_exp_value;
                }
            } else {
                if (isset($expense) && ($expense->plan_exp_date <= now()->endOfMonth()->toDateString())) {
                    $this_month += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonth()->endOfMonth()->toDateString())) {
                    $month_plus_1 += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->addMonth()->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonths(2)->endOfMonth()->toDateString())) {
                    $month_plus_2 += $expense->plan_exp_value;
                } elseif (isset($expense) && ($expense->plan_exp_date > now()->addMonths(2)->endOfMonth()->toDateString()) && ($expense->plan_exp_date <= now()->addMonths(3)->endOfMonth()->toDateString())) {
                    $month_plus_3 += $expense->plan_exp_value;
                }
            }
            $account = $expense->account_id;
        }

        return $planned_expenses;
    }

    public function expense_tracking()
    {
        $exp_tracking = ExpenseTracking::with('account')
            ->selectRaw('SUM(amount) AS amount, account_id')
            ->whereHas('account')
            ->where('claimable', 1)
            ->where('payment_status', 1)
            ->groupBy('account_id')
            ->orderBy('account_id')
            ->get();

        return $exp_tracking;
    }
}
