<?php

namespace App\Timesheet;

use App\Models\Timeline;
use App\Models\Timesheet;

class PreviousTimesheet
{
    public function previousTimesheets($user_id)
    {
        $timesheets = Timesheet::with(['project'])->select('id', 'year_week', 'first_day_of_week', 'employee_id', 'project_id')
            ->where('employee_id', $user_id)
            ->groupBy('id', 'year_week', 'project_id', 'employee_id', 'first_day_of_week')
            ->orderBy('year_week', 'desc')
            ->limit(10)
            ->get();
        $previous_timesheets = [];
        foreach ($timesheets as $timesheet) {
            $hours = Timeline::selectRaw('SUM((total*60) + total_m) AS minutes')
                ->whereHas('timesheet', function ($query) use ($timesheet) {
                    $query->where('year_week', $timesheet->year_week);
                    $query->where('employee_id', $timesheet->employee_id);
                    $query->where('project_id', $timesheet->project_id);
                })->first();
            array_push($previous_timesheets, [
                'minutes' => isset($hours->minutes) ? $hours->minutes : 0,
                'year_week' => $timesheet->year_week.'('.$timesheet->first_day_of_week.')',
                'assignment' => isset($timesheet->project) ? $timesheet->project->name : '',
                'id' => $timesheet->id,
            ]);
        }

        return $previous_timesheets;
    }
}
