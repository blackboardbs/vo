<?php

namespace App\Timesheet;

use App\Models\Config;
use App\Models\Timesheet;
use App\Models\VatRate;
use Illuminate\Support\Facades\DB;

class InvoiceCalculations
{
    private $config;

    public function __construct()
    {
        $this->config = Config::first();
    }

    public function calculateInvoice($timesheets, $currency)
    {
        $conf = isset($this->config->default_invoice_template) ? $this->config->default_invoice_template : 'standardinvoice';
        switch ($conf) {
            case 'standardinvoice':
                $timesheets_compact = Timesheet::with(['timeline' => function ($query) {
                    $query->addSelect(['timesheet_id', DB::raw(' SUM(CASE WHEN is_billable = 1 THEN total ELSE 0 END) AS billable_hours,
                SUM(CASE WHEN is_billable = 1 THEN total_m ELSE 0 END) AS billable_minutes')])->groupBy('timesheet_id');
                }, 'time_exp' => function ($query) {
                    $query->where('is_billable', '=', 1);
                }, 'customer', 'employee', 'project', 'company'])
                    ->select('timesheet.id', 'timesheet.company_id', 'timesheet.employee_id', 'timesheet.year_week',
                        'timesheet.customer_id', 'timesheet.project_id', 'timesheet.first_day_of_week', DB::raw('IF('.$currency.' = 1, assignment.invoice_rate_sec, assignment.invoice_rate) AS invoice_rate, IF('.$currency.' = 1, assignment.currency_sec, assignment.currency) AS currency')
                    )
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->whereIn('timesheet.id', $timesheets)
                    ->groupBy('timesheet.company_id', 'timesheet.employee_id', 'timesheet.year_week', 'timesheet.customer_id')
                    ->groupBy('timesheet.project_id', 'timesheet.id', 'timesheet.first_day_of_week', 'assignment.invoice_rate')
                    ->groupBy('assignment.invoice_rate_sec', 'assignment.currency_sec', 'assignment.currency')
                    ->get();

                return $timesheets_compact;
            case 'invoicebytask':
                $timesheets_compact = Timesheet::with(['timeline' => function ($query) {
                    $query->with('task')->addSelect(['timesheet_id', 'task_id', 'description_of_work',
                        DB::raw('SUM(CASE WHEN is_billable = 1 THEN total ELSE 0 END) AS billable_hours,
                                 SUM(CASE WHEN is_billable = 1 THEN total_m ELSE 0 END) AS billable_minutes'), ])
                        ->groupBy('timesheet_id', 'task_id', 'description_of_work');
                }, 'time_exp' => function ($query) {
                    $query->where('is_billable', '=', 1);
                }, 'customer', 'employee', 'project', 'company'])
                    ->select(['timesheet.id', 'timesheet.company_id', 'timesheet.employee_id', 'timesheet.year_week',
                        'timesheet.customer_id', 'timesheet.project_id', 'timesheet.first_day_of_week', DB::raw('IF('.$currency.' = 1, assignment.invoice_rate_sec, assignment.invoice_rate) AS invoice_rate, IF('.$currency.' = 1, assignment.currency_sec, assignment.currency) AS currency'),
                    ])
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->whereIn('timesheet.id', $timesheets)
                    ->groupBy('timesheet.company_id', 'timesheet.employee_id', 'timesheet.year_week', 'timesheet.customer_id')
                    ->groupBy('timesheet.project_id', 'timesheet.id', 'timesheet.first_day_of_week', 'assignment.invoice_rate')
                    ->groupBy('assignment.invoice_rate_sec', 'assignment.currency_sec', 'assignment.currency')
                    ->get();

                return $timesheets_compact;
            case 'invoicebyweek':
                $timesheets_compact = Timesheet::with(['timeline' => function ($query) {
                    $query->addSelect(['timesheet_id',
                        DB::raw('SUM(CASE WHEN is_billable = 1 THEN total ELSE 0 END) AS billable_hours,
                                 SUM(CASE WHEN is_billable = 1 THEN total_m ELSE 0 END) AS billable_minutes'), ])
                        ->groupBy('timesheet_id');
                }, 'time_exp' => function ($query) {
                    $query->where('is_billable', '=', 1);
                }, 'customer', 'employee', 'project', 'company'])
                    ->select('timesheet.id', 'timesheet.company_id', 'timesheet.year_week',
                        'timesheet.customer_id', 'timesheet.project_id', 'timesheet.first_day_of_week', 'timesheet.last_day_of_week', DB::raw('IF('.$currency.' = 1, assignment.invoice_rate_sec, assignment.invoice_rate) AS invoice_rate, IF('.$currency.' = 1, assignment.currency_sec, assignment.currency) AS currency')
                    )
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->whereIn('timesheet.id', $timesheets)
                    ->groupBy('timesheet.company_id', 'timesheet.year_week', 'timesheet.customer_id')
                    ->groupBy('timesheet.project_id', 'timesheet.id', 'timesheet.first_day_of_week', 'assignment.invoice_rate')
                    ->groupBy('timesheet.last_day_of_week')
                    ->groupBy('assignment.invoice_rate_sec', 'assignment.currency_sec', 'assignment.currency')
                    ->get();

                return $timesheets_compact;
            default:
                $timesheets_compact = Timesheet::with(['timeline' => function ($query) {
                    $query->with('task')->addSelect(['timesheet_id', 'description_of_work', 'task_id',
                        DB::raw('SUM(CASE WHEN is_billable = 1 THEN total ELSE 0 END) AS billable_hours,
                                 SUM(CASE WHEN is_billable = 1 THEN total_m ELSE 0 END) AS billable_minutes'), ])
                        ->groupBy('timesheet_id', 'description_of_work', 'task_id');
                }, 'time_exp' => function ($query) {
                    $query->where('is_billable', '=', 1);
                }, 'customer', 'employee', 'project', 'company'])
                    ->select('timesheet.id', 'timesheet.employee_id', 'timesheet.company_id', 'timesheet.year_week',
                        'timesheet.customer_id', 'timesheet.project_id', 'timesheet.first_day_of_week', 'timesheet.last_day_of_week', DB::raw('IF('.$currency.' = 1, assignment.invoice_rate_sec, assignment.invoice_rate) AS invoice_rate, IF('.$currency.' = 1, assignment.currency_sec, assignment.currency) AS currency')
                    )
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->whereIn('timesheet.id', $timesheets)
                    ->groupBy('timesheet.company_id', 'timesheet.year_week', 'timesheet.customer_id', 'timesheet.employee_id')
                    ->groupBy('timesheet.project_id', 'timesheet.id', 'timesheet.first_day_of_week', 'assignment.invoice_rate')
                    ->groupBy('timesheet.last_day_of_week')
                    ->groupBy('assignment.invoice_rate_sec', 'assignment.currency_sec', 'assignment.currency')
                    ->get();

                return $timesheets_compact;
        }
    }

    public function dependencies($timesheets, $currency, $vat_rate_id)
    {
        $vat_rate = [];
        $total_hours = 0;
        $total_exclusive = 0;
        $total_vat = 0;
        $total_amount = 0;
        $total_amount_out_standing = 0;
        $customer_ids = [];
        foreach ($this->calculateInvoice($timesheets, $currency) as $timesheet) {
            $local_vat = VatRate::find($vat_rate_id)?->vat_rate;
            array_push($vat_rate, $local_vat);

            $inv_rate = (float) $timesheet->invoice_rate ?? 0;
            foreach ($timesheet->timeline as $timeline) {
                try {
                    $total_exclusive += round(($timeline->billable_hours + ($timeline->billable_minutes / 60)), 2) * $inv_rate;
                    $total_vat += (round(($timeline->billable_hours + ($timeline->billable_minutes / 60)), 2) * $inv_rate) * ($local_vat / 100);
                    $total_hours += round(($timeline->billable_hours + ($timeline->billable_minutes / 60)), 2);
                } catch (\ErrorException $exception) {
                }
            }

            foreach ($timesheet->time_exp as $time_exp) {
                $total_amount += $time_exp->amount;
                $total_amount_out_standing += ($time_exp->paid_date != null) ? 0 : $time_exp->amount;
            }

            array_push($customer_ids, $timesheet->customer_id);
        }

        if (count(array_unique($customer_ids)) > 1) {
            return 0;
        }

        return [
            'vat_rate' => $vat_rate,
            'total_hours' => $total_hours,
            'total_exclusive' => $total_exclusive,
            'total_vat' => $total_vat,
            'total_amount' => $total_amount,
            'total_amount_out_standing' => $total_amount_out_standing,
        ];
    }

    public function checkCurrencies($timesheets)
    {
        return $timesheets->map(function ($currency) {
            return $currency->currency;
        })->unique()->values()->toArray();
    }
}
