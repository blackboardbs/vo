<?php

namespace App\Timesheet;

use App\Models\BillingPeriod;
use App\Models\Config;
use App\Models\Task;
use App\Models\Timeline;
use App\Models\Timesheet;
use Carbon\Carbon;

trait TimesheetHelperTrait
{
    public function getYearWeek()
    {
        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $date_today = date('Y-m-d');
        $year_2020_week_1_flag = false;
        if (($year == 2019) && ($date_today == '2019-12-31')) {
            $year += 1;
            $year_2020_week_1_flag == true;
        }

        $config = Config::find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $number_of_weeks_future = isset($config->timesheet_weeks_future) && (int) $config->timesheet_weeks_future > 0 ? $config->timesheet_weeks_future : 0;

        if (($week + $number_of_weeks_future) <= 52) {
            $week += $number_of_weeks_future;
        }

        $week_drop_down = [];
        $week_drop_down_object = [];

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week <= 6) {
                    if (($i == 1) && ($year == 2020)) {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$year.'-'.$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'(2019-12-31)-1';

                        //New dropdown object
                        $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_2'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_2', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.$year.'-'.$week_two_month.'-01)-2', 'first_date' => '01', 'days' => $this->returnWeek($year, 12, 31)];
                        $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_1'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_2', 'name' => $year.($i < 10 ? '0'.$i : $i).'(2019-12-31)-1', 'first_date' => '31', 'days' => $this->returnWeek($year, 12, 31)];
                    } else {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';

                        //New dropdown object
                        $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_2'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_2', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2', 'first_date' => '01', 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                        $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_1'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_1', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1', 'first_date' => $date2->format('d'), 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                    }

                    continue;
                }

                if ($i == 1) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.date('Y-m').'-01)-2';
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';

                    //New dropdown object
                    $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_2'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_2', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.date('Y-m').'-01)-2', 'first_date' => '01', 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                    $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i).'_1'] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_1', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1', 'first_date' => $date2->format('d'), 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                } else {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';

                    //New dropdown object
                    $week_drop_down_object[$year.($i < 10 ? '0'.$i : $i)] = ['id' => $year.($i < 10 ? '0'.$i : $i).'_2', 'name' => $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')', 'first_date' => $date2->format('d'), 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                }
            }

            $number_of_weeks -= $week;
            $week = 52;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week <= 6) {
                $week_drop_down[$year.$i.'_2'] = $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2';
                $week_drop_down[$year.$i.'_1'] = $year.$i.'('.$date2->format('Y-m-d').')-1';

                //New dropdown object
                $week_drop_down_object[$year.$i.'_2'] = ['id' => $year.$i.'_2', 'name' => $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2', 'first_date' => '01', 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
                $week_drop_down_object[$year.$i.'_1'] = ['id' => $year.$i.'_1', 'name' => $year.$i.'('.$date2->format('Y-m-d').')-1', 'first_date' => $date2->format('d'), 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];

                continue;
            }
            $week_drop_down[$year.$i] = $year.$i.'('.$date2->format('Y-m-d').')';
            //New dropdown object
            $week_drop_down_object[$year.$i] = ['id' => $year.$i, 'name' => $year.$i.'('.$date2->format('Y-m-d').')', 'first_date' => $date2->format('d'), 'days' => $this->returnWeek($year, $date2->format('m'), $date2->format('d'))];
        }

        // return $week_drop_down;
        return $week_drop_down_object;
    }

    private function yearWeekDropDown()
    {
        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $date_today = date('Y-m-d');
        $year_2020_week_1_flag = false;
        if (($year == 2019) && ($date_today == '2019-12-31')) {
            $year += 1;
            $year_2020_week_1_flag == true;
        }

        $config = Config::select(['timesheet_weeks', 'timesheet_weeks_future'])->find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $number_of_weeks_future = isset($config->timesheet_weeks_future) && (int) $config->timesheet_weeks_future > 0 ? $config->timesheet_weeks_future : 0;

        if (($week + $number_of_weeks_future) <= 53) {
            $week += $number_of_weeks_future;
        }

        //$week_drop_down[0] = "Select Week";

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week <= 6) {
                    if (($i == 1) && ($year == 2020)) {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$year.'-'.$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'(2019-12-31)-1';
                    } else {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';
                    }

                    continue;
                }

                // Disable first week for 2021, this worked for 2020
                /*if($i == 1){
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_2'] = $year.($i<10?'0'.$i:$i)."(".date('Y-m')."-01)-2";
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_1'] = $year.($i<10?'0'.$i:$i)."(".$date2->format('Y-m-d').")-1";
                }
                else{*/
                $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                /*}*/
            }

            $number_of_weeks -= $week;
            $week = 53;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week <= 6) {
                // Weird week 53 for 2021
                if ($i == 53) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = ($year + 1).($i < 10 ? '0'.$i : $i).'(2021-'.$week_two_month.'-01)-2';
                } else {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                }
                $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';

                continue;
            }
            $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
        }

        return $week_drop_down;
    }

    private function weeks(Config $config, int $billingCycle = 0)
    {
        $weeks = [];
        $date = Carbon::now();
        $config_weeks = (int) ($config->timesheet_weeks ?? 12);
        $ceil_week = $date->copy()->addWeeks((int) $config->timesheet_weeks_future)->endOfWeek();
        $floor_week = $date->copy()->subWeeks($config_weeks)->startOfWeek();
        $billing_periods = BillingPeriod::query()
            ->where('billing_cycle_id', $billingCycle)
            ->whereBetween('start_date', [$floor_week->copy()->toDateString(), $ceil_week->copy()->toDateString()])
            ->orderBy('start_date', 'ASC')
            ->get();

        if ($billing_periods->isEmpty() || ! $billingCycle) {
            $weeks = collect($this->yearWeekDropDown())->map(function ($week, $key) {
                return [
                    'year_week' => (string) $key,
                    'date' => $week,
                ];
            })->sortByDesc('year_week')->values();
        } else {
            foreach ($billing_periods as $period) {
                for ($i = Carbon::parse($period->start_date)->startOfWeek(); $i <= Carbon::parse($period->end_date)->endOfWeek(); $i->addWeeks(1)) {
                    $week = str_pad($i->copy()->weekOfYear, 2, '0', STR_PAD_LEFT);
                    $year_week = $i->copy()->year.$week;

                    if ($i->copy()->toDateString() > now()->addWeeks(((int) $config->timesheet_weeks_future) ?? 0)->endOfWeek()->toDateString()) {
                        break;
                    }

                    $split_2_start = Carbon::parse($period->end_date);
                    $short_week = $i->copy()->startOfWeek()->diffInDays($split_2_start);

                    if ($short_week < 6) {
                        array_push($weeks, [
                            'year_week' => ($year_week == 202401 ? '202453_1' : $year_week).'_1',
                            'date' => ($year_week == 202401 ? 202453 : $year_week).'('.$i->copy()->toDateString().')-1',
                            'period' => $period->id,
                        ]);
                    } elseif ((Carbon::parse($period->start_date)->diffInDays($i->copy()->endOfWeek()) < 6) && Carbon::parse($period->start_date) <= now()) {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? '202501_2' : $year_week.'_2',
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.Carbon::parse($period->start_date)->toDateString().')-2',
                            'period' => $year_week == 202401 ? $period->id+1 : $period->id,
                        ]);
                    } else {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? 202501 : $year_week,
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.$i->copy()->toDateString().')',
                            'period' => $period->id,
                        ]);
                    }
                }
            }
        }

        return collect($weeks)->sortByDesc('year_week')->values();
    }

    public function returnWeek($year, $month, $firstDay)
    {
        $feb_last_day = 28;
        if ($year % 4 == 0) {
            $feb_last_day = 29;
        }
        $month_days = [1 => 31, 2 => $feb_last_day, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];

        $week_days = [];
        for ($i = 0; $i <= 6; $i++) {
            $week_days[$i] = (int) $firstDay < 10 ? '0'.(int) $firstDay : ''.$firstDay;
            if ($firstDay == $month_days[(int) $month]) {
                $firstDay = 1;
            } else {
                $firstDay++;
            }
        }

        return $week_days;
    }

    public function combinedTimesheet($timesheet, $config, $project)
    {
        $billing_cycle = $this->billingCycle($project, $config);

        return (object) [
            'use_custom_template' => $config->default_custom_template,
            'current_billing_period_id' => $this->getBillingPeriod($billing_cycle, Carbon::parse($timesheet->first_day_of_week)->startOfWeek(), 0)->id ?? 0,
            'project_timesheets' => $this->projectTimesheets($timesheet),
            'billing_cycle_id' => $billing_cycle,
        ];
    }

    private function projectTimesheets($timesheet)
    {
        return Timesheet::selectRaw('GROUP_CONCAT(DISTINCT timeline.id SEPARATOR ",") AS timeline_id,timesheet.id, timesheet.timesheet_lock, timesheet.first_day_of_week, 
        timesheet.year_week, timeline.is_billable, timeline.vend_is_billable, SEC_TO_TIME(SUM(((timeline.mon*60) + timeline.mon_m)*60)) AS monday, 
        SEC_TO_TIME(SUM(((timeline.tue*60) + timeline.tue_m)*60)) AS tuesday, SEC_TO_TIME(SUM(((timeline.wed*60) + timeline.wed_m)*60)) AS wednesday, 
        SEC_TO_TIME(SUM(((timeline.thu*60) + timeline.thu_m)*60)) AS thursday, SEC_TO_TIME(SUM(((timeline.fri*60) + timeline.fri_m)*60)) AS friday, 
        SEC_TO_TIME(SUM(((timeline.sat*60) + timeline.sat_m)*60)) AS saturday, SEC_TO_TIME(SUM(((timeline.sun*60) + timeline.sun_m)*60)) AS sunday, 
        SEC_TO_TIME(SUM(((timeline.total*60) + timeline.total_m)*60)) AS total, SUM(timeline.mileage) AS travel')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.employee_id', $timesheet->employee_id)
            ->where('timesheet.project_id', $timesheet->project_id)
            ->groupBy(['timesheet.timesheet_lock', 'timesheet.year_week', 'timesheet.first_day_of_week', 'timeline.is_billable', 'timeline.vend_is_billable', 'timesheet.id'])
            ->orderBy('timesheet.year_week', 'DESC')
            ->orderBy('timesheet.first_day_of_week', 'DESC')
            ->limit(6)->get();
    }

    public function billingCycle($project, $config)
    {
        if ($project->billing_cycle_id) {
            return $project->billing_cycle_id;
        } elseif ($project->customer->billing_cycle_id) {
            return $project->customer->billing_cycle_id;
        } else {
            return $config->billing_cycle_id;
        }
    }

    private function getBillingPeriod(int $billing_cycle, Carbon $date, int $split_week)
    {
        if (isset($billing_cycle)) {
            $billing_period = BillingPeriod::with('billingCycle')
                ->where('billing_cycle_id', $billing_cycle);

            // Calculate start of week once
            $start_of_week = $date->copy()->startOfWeek();
            $end_of_week = $date->copy()->endOfWeek();

            // if($split_week === 1){
            //     \Illuminate\Support\Facades\Log::info($start_of_week);
            // }

            if ($split_week === 1) {
                $billing_period = $billing_period->where('start_date', '<=', $start_of_week)
                    ->orderBy('end_date','desc');
            } elseif ($split_week === 2) {
                $billing_period = $billing_period->where('start_date', '>', $start_of_week)
                    ->where('end_date', '>', $end_of_week)
                    ->orderBy('start_date');
            } else {
                $billing_period = $billing_period->where('start_date', '<=', $start_of_week)
                    ->where('end_date', '>=', $start_of_week);
            }

            return $billing_period->first();
        }

        return null;
    }


    public function taskDetailsReportTimesheet($request)
    {
        $timesheets = Timesheet::orderBy('id');

        if ($request->has('mf') && ($request->input('mf') != -1)) {
            $year_month_array = explode('-', $request->input('mf'));
            $timesheets = $timesheets->where('year', '>=', $year_month_array[0])->where('month', '>=', $year_month_array[1]);
        }

        if ($request->has('mt') && ($request->input('mt') != -1)) {
            $year_month_array = explode('-', $request->input('mt'));
            $timesheets = $timesheets->where('year', '<=', $year_month_array[0])->where('month', '<=', $year_month_array[1]);
        }

        if ($request->has('cc') && ($request->input('cc') != -1)) {
            $timesheets = $timesheets->where('customer_id', '=', $request->input('cc'));
        }

        if ($request->has('p') && ($request->input('p') != -1)) {
            $timesheets = $timesheets->where('project_id', '=', $request->input('p'));
        }

        if ($request->has('c') && ($request->input('c') != -1)) {
            $timesheets = $timesheets->where('company_id', '=', $request->input('c'));
        }

        if ($request->has('r') && ($request->input('r') != -1)) {
            $timesheets = $timesheets->where('employee_id', '=', $request->input('r'));
        }

        if ($request->has('pf') && $request->input('pf') != -1) {
            $timesheets = $timesheets->where('year_week', '>=', $request->input('pf'));
        }

        if ($request->has('pt') && $request->input('pt') != -1) {
            $timesheets = $timesheets->where('year_week', '<=', $request->input('pt'));
        }

        if ($request->has('is') && $request->input('is') != -1) {
            $timesheets = $timesheets->where('bill_status', '=', $request->input('is'));
        }

        $is_billable = null;
        if ($request->has('b') && $request->input('b') != -1) {
            $is_billable = $request->input('b');
        }

        if ($request->has('t') && $request->input('t') != -1) {
            $task = Task::find($request->input('t')); //Get the specific task
            $timesheet_ids = Timeline::where('task_id', '=', $task->id)->pluck('timesheet_id')->toArray(); //Get the timeline
            $timesheets = $timesheets->whereIn('id', $timesheet_ids); //Now filter timesheets
        }

        $timesheets = $timesheets->get();

        return (object) [
            'is_billable' => $is_billable,
            'timesheets' => $timesheets,
        ];
    }
}
