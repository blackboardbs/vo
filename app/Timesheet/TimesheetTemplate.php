<?php

namespace App\Timesheet;

use App\Models\Assignment;
use App\Models\BillingPeriod;
use App\Models\Project;
use App\Models\Timeline;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TimesheetTemplate
{
    /**
     * @param object | Request $request
     * @return mixed
     */
    public function assignment($request)
    {
        return Assignment::select(
            [
                'project_id', 'customer_po', 'project_manager_ts_approver', 'product_owner_ts_approver', 'product_owner_id',
                'project_manager_new_ts_approver','project_manager_new_known_as', 'project_manager_new_id', 'line_manager_ts_approver',
                'line_manager_known_as', 'line_manager_id', 'claim_approver_ts_approver', 'claim_approver_known_as',
                'assignment_approver', 'resource_manager_ts_approver', 'resource_manager_id', 'internal_owner_known_as',
                'id', 'product_owner_known_as', 'resource_manager_known_as'
            ]
        )->with(['project' => function($project){
            $project->select(['id', 'name', 'manager_id', 'customer_id', 'company_id'])
                ->with(['customer' => function($customer){
                    $customer->select('id', 'customer_name', 'logo');
                }, 'company'=>function($company){
                    $company->select('id', 'company_name', 'company_logo');
                }]);
        }])->where(['employee_id' => $request->employee_id, 'project_id' => $request->project_id])
            ->first();
    }

    public function horizontalTemplateTimesheets(Request $request){
        return Timesheet::join('timeline', 'timesheet.id', 'timeline.timesheet_id')
            ->selectRaw('timesheet.id, timesheet.first_day_of_week, timesheet.first_day_of_month, 
                            IF(timeline.task_id > 0,(SELECT tasks.description FROM tasks WHERE tasks.id = timeline.task_id), timeline.description_of_work) AS description,  
                            SUM((timeline.mon*60)+timeline.mon_m) AS monday, SUM((timeline.tue*60)+timeline.tue_m) AS tuesday,
                            SUM((timeline.wed*60)+timeline.wed_m) AS wednesday, SUM((timeline.thu*60)+timeline.thu_m) AS thursday,
                            SUM((timeline.fri*60)+timeline.fri_m) AS friday, SUM((timeline.sat*60)+timeline.sat_m) AS saturday,
                            SUM((timeline.sun*60)+timeline.sun_m) AS sunday, SUM((timeline.total*60)+timeline.total_m) AS total
                        ')->where('timeline.is_billable', 1)
            ->groupBy(['timesheet.id', 'timeline.description_of_work', 'timeline.task_id', 'timesheet.first_day_of_week', 'timesheet.first_day_of_month'])
            ->orderBy('timesheet.first_day_of_week')
            ->find($request->timesheets_id);
    }

    public function gridTemplateTimesheets(Request $request)
    {
        return Timesheet::join('timeline', 'timesheet.id', 'timeline.timesheet_id')
            ->selectRaw('timesheet.first_day_of_week, SUM((timeline.mon*60)+timeline.mon_m) AS monday,
                    SUM((timeline.tue*60)+timeline.tue_m) AS tuesday, SUM((timeline.wed*60)+timeline.wed_m) AS wednesday,
                    SUM((timeline.thu*60)+timeline.thu_m) AS thursday, SUM((timeline.fri*60)+timeline.fri_m) AS friday,
                    SUM((timeline.sat*60)+timeline.sat_m) AS saturday, SUM((timeline.sun*60)+timeline.sun_m) AS sunday,
                    SUM((timeline.total*60)+timeline.total_m) AS total
                ')->where('timeline.is_billable', 1)
            ->groupBy('first_day_of_week')
            ->orderBy('first_day_of_week')
            ->find($request->timesheets_id);
    }

    public function verticalTemplateTimesheets(Request $request)
    {
        $timesheet = Timesheet::with('timeline.task')
            ->findOrFail($request->timesheets_id, ['id', 'first_day_of_week', 'first_day_of_month', 'last_day_of_month']);


        return $timesheet->map(function($line){
            $date = Carbon::parse($line->first_day_of_week)->startOfWeek();
            $week = [
                'begining_of_month' => $line->first_day_of_month,
                'end_of_month' => $line->last_day_of_month,
                'monday' => [
                    'date' => $date->copy()->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'tuesday' => [
                    'date' => $date->copy()->addDay()->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'wednesday' => [
                    'date' => $date->copy()->addDays(2)->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'thursday' => [
                    'date' => $date->copy()->addDays(3)->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'friday' => [
                    'date' => $date->copy()->addDays(4)->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'saturday' => [
                    'date' => $date->copy()->addDays(5)->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'sunday' => [
                    'date' => $date->copy()->addDays(6)->format('d-M-y'),
                    'description' => '',
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0
                ],
                'total' => [
                    'hours' => 0,
                    'bill_hour' => 0,
                    'non_bill_hour' => 0,
                ]
            ];

            foreach ($line->timeline as $timeline){
                $week['monday']['description'] .= (($timeline->mon*60) + $timeline->mon_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['monday']['hours'] += (($timeline->mon*60) + $timeline->mon_m);
                $week['monday']['bill_hour'] += $timeline->is_billable?(($timeline->mon*60) + $timeline->mon_m):0;
                $week['monday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->mon*60) + $timeline->mon_m):0;

                $week['tuesday']['description'] .= (($timeline->tue*60) + $timeline->tue_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['tuesday']['hours'] += (($timeline->tue*60) + $timeline->tue_m);
                $week['tuesday']['bill_hour'] += $timeline->is_billable?(($timeline->tue*60) + $timeline->tue_m):0;
                $week['tuesday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->tue*60) + $timeline->tue_m):0;

                $week['wednesday']['description'] .= (($timeline->wed*60) + $timeline->wed_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['wednesday']['hours'] += (($timeline->wed*60) + $timeline->wed_m);
                $week['wednesday']['bill_hour'] += $timeline->is_billable?(($timeline->wed*60) + $timeline->wed_m):0;
                $week['wednesday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->wed*60) + $timeline->wed_m):0;

                $week['thursday']['description'] .= (($timeline->thu*60) + $timeline->thu_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['thursday']['hours'] += (($timeline->thu*60) + $timeline->thu_m);
                $week['thursday']['bill_hour'] += $timeline->is_billable?(($timeline->thu*60) + $timeline->thu_m):0;
                $week['thursday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->thu*60) + $timeline->thu_m):0;

                $week['friday']['description'] .= (($timeline->fri*60) + $timeline->fri_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['friday']['hours'] += (($timeline->fri*60) + $timeline->fri_m);
                $week['friday']['bill_hour'] += $timeline->is_billable?(($timeline->fri*60) + $timeline->fri_m):0;
                $week['friday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->fri*60) + $timeline->fri_m):0;

                $week['saturday']['description'] .= (($timeline->sat*60) + $timeline->sat_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['saturday']['hours'] += (($timeline->sat*60) + $timeline->sat_m);
                $week['saturday']['bill_hour'] += $timeline->is_billable?(($timeline->sat*60) + $timeline->sat_m):0;
                $week['saturday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->sat*60) + $timeline->sat_m):0;

                $week['sunday']['description'] .= (($timeline->sun*60) + $timeline->sun_m)?($timeline->task->description??$timeline->description_of_work):null;
                $week['sunday']['hours'] += (($timeline->sun*60) + $timeline->sun_m);
                $week['sunday']['bill_hour'] += $timeline->is_billable?(($timeline->sun*60) + $timeline->sun_m):0;
                $week['sunday']['non_bill_hour'] += !$timeline->is_billable?(($timeline->sun*60) + $timeline->sun_m):0;

                $week['total']['hours'] += (($timeline->total*60) + $timeline->total_m);
                $week['total']['bill_hour'] += $timeline->is_billable?(($timeline->total*60) + $timeline->total_m):0;
                $week['total']['non_bill_hour'] += !$timeline->is_billable?(($timeline->total*60) + $timeline->total_m):0;
            }

            return $week;
        });
    }

    public function verticalTemplateData($timesheet)
    {
        return $timesheet->map(function($query){

            return [
                'monday' => [
                    'date' => $query['monday']['date'],
                    'description' => substr($query['monday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['monday']['hours']),
                    'billable' => _minutes_to_time($query['monday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['monday']['non_bill_hour']),
                ],
                'tuesday' => [
                    'date' => $query['tuesday']['date'],
                    'description' => substr($query['tuesday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['tuesday']['hours']),
                    'billable' => _minutes_to_time($query['tuesday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['tuesday']['non_bill_hour']),
                ],
                'wednesday' => [
                    'date' => $query['wednesday']['date'],
                    'description' => substr($query['wednesday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['wednesday']['hours']),
                    'billable' => _minutes_to_time($query['wednesday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['wednesday']['non_bill_hour']),
                ],
                'thursday' => [
                    'date' => $query['thursday']['date'],
                    'description' => substr($query['thursday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['thursday']['hours']),
                    'billable' => _minutes_to_time($query['thursday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['thursday']['non_bill_hour']),
                ],
                'friday' => [
                    'date' => $query['friday']['date'],
                    'description' => substr($query['friday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['friday']['hours']),
                    'billable' => _minutes_to_time($query['friday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['friday']['non_bill_hour']),
                ],
                'saturday' => [
                    'date' => $query['saturday']['date'],
                    'description' => substr($query['saturday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['saturday']['hours']),
                    'billable' => _minutes_to_time($query['saturday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['saturday']['non_bill_hour']),
                ],
                'sunday' => [
                    'date' => $query['sunday']['date'],
                    'description' => substr($query['sunday']['description'], 0, 200),
                    'hours' => _minutes_to_time($query['sunday']['hours']),
                    'billable' => _minutes_to_time($query['sunday']['bill_hour']),
                    'non_billable' => _minutes_to_time($query['sunday']['non_bill_hour']),
                ],
                'total' => [
                    'total' => _minutes_to_time($query['total']['hours']),
                    'bill_total' => _minutes_to_time($query['total']['bill_hour']),
                    'non_bill_total' => _minutes_to_time($query['total']['non_bill_hour'])
                ],
                'non_bill_total_in_min' => $query['total']['non_bill_hour'],
                '_total' => $query['total']['hours']
            ];
        });
    }

    public function digiSignUsers($billing_digi_sign, $assignment, $user)
    {
        return collect($billing_digi_sign["digisign_users"])
            ->map(function ($digi) use ($user, $assignment, $billing_digi_sign){
                $digi_sign = [];

                if ($digi->user_number == 1){
                    array_push($digi_sign, (object)[
                        'id' => $user->id,
                        'type' => "Resource",
                        'user' => $user->name(),
                        'user_number' => 1,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }


                if ($billing_digi_sign["project_manager_ts_user"] && $digi->user_number == 2){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->internal_owner_known_as??"Internal Owner",
                        'user' => $billing_digi_sign["project_manager_ts_user"],
                        'user_number' => 2,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                if ($billing_digi_sign["project_owner_ts_user"] && $digi->user_number == 3){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->product_owner_known_as??"Product Owner",
                        'user' => $billing_digi_sign["project_owner_ts_user"],
                        'user_number' => 3,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                if ($digi->user_number == 4 && $billing_digi_sign["project_manager_new_ts_user"]){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->project_manager_new_known_as??"Project Manager",
                        'user' => $billing_digi_sign["project_manager_new_ts_user"],
                        'user_number' => 4,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                if ($digi->user_number == 5 && $billing_digi_sign["line_manager_ts_user"]){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->line_manager_known_as??"Line Manager",
                        'user' => $billing_digi_sign["line_manager_ts_user"],
                        'user_number' => 5,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                if ($digi->user_number == 6 && $billing_digi_sign["claim_approver_ts_user"]){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->claim_approver_known_as??"Claim Approver",
                        'user' => $billing_digi_sign["claim_approver_ts_user"],
                        'user_number' => 6,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                if ($digi->user_number == 7 && $billing_digi_sign["resource_manager_ts_user"]){
                    array_push($digi_sign, (object)[
                        'type' => $assignment->resource_manager_known_as??"Resource Manager",
                        'user' => $billing_digi_sign["resource_manager_ts_user"],
                        'user_number' => 7,
                        'signed' => $digi->signed,
                        'user_name' => $digi->user_name,
                        'user_sign_date' => $digi->user_sign_date
                    ]);
                }

                return $digi_sign;
            });
    }

    public function weeklyTemplateData($timesheet)
    {
        $date = Carbon::parse($timesheet->first_day_of_week);
        $timeline_weekly_details = [];
        $timeline_counter = 0;

        foreach ($timesheet->timeline as $timeline){

            if((($timeline->mon * 60) + $timeline->mon_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 0, 'mon', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->tue * 60) + $timeline->tue_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 1, 'tue', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->wed * 60) + $timeline->wed_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 2, 'wed', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->thu * 60) + $timeline->thu_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 3, 'thu', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->fri * 60) + $timeline->fri_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 4, 'fri', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->sat * 60) + $timeline->sat_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 5, 'sat', $timesheet);
                $timeline_counter++;
            }

            if((($timeline->sun * 60) + $timeline->sun_m) > 0){
                $timeline_weekly_details[$timeline_counter] = $this->timesheetPrintFormatter($timeline, $date, 6, 'sun', $timesheet);
                $timeline_counter++;
            }

        }

        return $timeline_weekly_details;
    }

    public function monthlyTemplateData(BillingPeriod $billingperiod, Collection $timesheets)
    {
        $dates = [];
        $data = [];

        for ($i = Carbon::parse($billingperiod->start_date); $i <= Carbon::parse($billingperiod->end_date); $i->addDay()){
            array_push($dates, [
                'date' => $i->format("D d-M-y"),
            ]);
        }

        foreach ($dates as $value){
            foreach ($timesheets as $timesheet){
                $start_week = Carbon::parse($timesheet->first_day_of_week);
                //dd($timesheet->timeline->groupBy('description'));
                foreach ($timesheet->timeline->groupBy('description') as $key => $timeline){
                    if (($timeline->sum("monday") > 0) &&($start_week->copy()->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'monday', $timesheet->project, $key));
                    }elseif (($timeline->sum("tuesday") > 0) &&($start_week->copy()->addDay()->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'tuesday', $timesheet->project, $key));
                    }elseif (($timeline->sum("wednesday") > 0) &&($start_week->copy()->addDays(2)->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'wednesday', $timesheet->project, $key));
                    }elseif (($timeline->sum("thursday") > 0) &&($start_week->copy()->addDays(3)->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'thursday', $timesheet->project, $key));
                    }elseif (($timeline->sum("friday") > 0) &&($start_week->copy()->addDays(4)->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'friday', $timesheet->project, $key));
                    }elseif (($timeline->sum("saturday") > 0) &&($start_week->copy()->addDays(5)->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'saturday', $timesheet->project, $key));
                    }elseif (($timeline->sum("sunday") > 0) &&($start_week->copy()->addDays(6)->format("D d-M-y") == $value["date"])){
                        array_push($data, (object)$this->monthlyTemplateLine($value["date"], $timeline, 'sunday', $timesheet->project, $key));
                    }else{
                        array_push($data, (object)[
                            "date" => $value["date"],
                            "project_ref" => null,
                            "project_name" => null,
                            "description" => null,
                            "hours" => null,
                            "billable_hours" => null,
                            "non_billable" => null
                        ]);
                    }
                }
            }
        }

        $collected_data = collect($data)->unique()->values()->groupBy('date');
        $final_data = [];

        foreach ($dates as $date){
            foreach ($collected_data as $item){
                if (count($item) > 1){
                    foreach ($item as $data){
                        if ($data->project_ref != null)
                            array_push($final_data, $data);
                    }
                }else{
                    array_push($final_data, $item[0]);
                }
            }

        }

        return collect($final_data)->unique();

        /*return (object)[
            'dates' => $dates,
            'data' => collect($data)->unique()->values()->groupBy('date')
        ];*/

        //return ;
    }

    private function timesheetPrintFormatter(Timeline $timeline, Carbon $date, int $day, string $column, Timesheet $timesheet)
    {
        $timeline_weekly_details = [];

        $description_of_work = "";
        if (isset($timeline->task->description)){
            $description_of_work = $timeline->task->description.' - '.$timeline->description_of_work;
        }else{
            $description_of_work = $timeline->description_of_work;
        }

        $timeline_weekly_details['is_billable'] = $timeline->is_billable;
        $timeline_weekly_details['total_minutes'] = ($timeline[$column] * 60) + $timeline[$column.'_m'];
        $timeline_weekly_details['total_minutes_to_time'] = _minutes_to_time(($timeline[$column]*60) + $timeline[$column.'_m']);;
        $timeline_weekly_details['date'] = $date->copy()->addDays($day)->toDateString();
        $timeline_weekly_details['project_code'] = $timesheet->project->ref ?? '';
        $timeline_weekly_details['project_name'] = $timesheet->project->name ?? '';
        $timeline_weekly_details['description_of_work'] = $description_of_work;

        return $timeline_weekly_details;
    }

    private function monthlyTemplateLine(string $date, Collection $timeline, string $day,Project $project, $description = null): array
    {
        return [
            "date" => $date,
            "project_ref" => $project->ref??null,
            "project_name" => $project->name??null,
            "description" => $description,
            "hours" => $timeline->sum($day),
            "billable_hours" => $timeline->sum(function ($line) use($day){
                if($line->is_billable) return $line->$day;
            }),
            "non_billable" => $timeline->sum(function ($line) use($day){
                if(!$line->is_billable) return $line->$day;
            })
        ];
    }
}