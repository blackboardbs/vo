<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BoardUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $boards = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $boards)
    {
        $this->boards = $boards;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn(): array
    {
        //return new PrivateChannel('channel-name');
        return new Channel('boards');
    }

    public function broadcastAs()
    {
        return 'updated';
    }
}
