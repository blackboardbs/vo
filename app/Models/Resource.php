<?php

namespace App\Models;

use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Kyslik\ColumnSortable\Sortable;

class Resource extends Model
{
    use Sortable;
    use TemplateTrait;

    protected $table = 'resource';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'email', 'phone', 'user_id', 'created_at'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function cv()
    {
        return$this->belongsTo(\App\Models\Cv::class, 'cv_id');
    }

    public function resource_position_desc()
    {
        return $this->belongsTo(\App\Models\ResourcePosition::class, 'resource_position');
    }

    public function resource_level_desc()
    {
        return $this->belongsTo(\App\Models\ResourceLevel::class, 'resource_level');
    }

    public function resource_type_desc()
    {
        return $this->belongsTo(\App\Models\ResourceType::class, 'resource_type');
    }

    public function projecttype()
    {
        return $this->belongsTo(\App\Models\ProjectType::class, 'project_type');
    }

    public function cost_center()
    {
        return $this->belongsTo(\App\Models\CostCenter::class, 'cost_center_id');
    }

    public function commission()
    {
        return $this->belongsTo(\App\Models\Commission::class, 'commission_id');
    }

    public function marital_status()
    {
        return $this->belongsTo(\App\Models\MaritalStatus::class, 'marital_status_id');
    }

    public function manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'manager_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function vendor()
    {
        return $this->belongsTo(\App\Models\Vendor::class, 'vendor_id');
    }

    public function medical_cert()
    {
        return $this->belongsTo(\App\Models\MedicalCertificateType::class, 'medical_certificate_type');
    }

    public function raced()
    {
        return $this->belongsTo(\App\Models\Race::class, 'race');
    }

    public function bbeeraced()
    {
        return $this->belongsTo(\App\Models\Race::class, 'race');
    }

    public function genderd()
    {
        return $this->belongsTo(\App\Models\Gender::class, 'gender');
    }

    public function disabilityd()
    {
        return $this->belongsTo(\App\Models\Disability::class, 'disability');
    }

    public function employee_position()
    {
        return $this->belongsTo(\App\Models\ResourcePosition::class, 'emp_position');
    }

    public function team()
    {
        return $this->belongsTo(\App\Models\Team::class, 'team_id');
    }

    public function business_skill()
    {
        return $this->belongsTo(\App\Models\BusinessRating::class, 'business_skill_id');
    }

    public function technical_rating()
    {
        return $this->belongsTo(\App\Models\TechnicalRating::class, 'technical_rating_id');
    }

    public function standard_cost()
    {
        return $this->belongsTo(\App\Models\AssignmentStandardCost::class, 'standard_cost_bracket');
    }

    public function NDATemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'nda_template_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(\App\Models\Document::class, 'documentable');
    }
}
