<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $fillable = [
        'user_onboarding_id',
        'bank_account_type_id',
        'account_name',
        'relationship_id',
        'bank_name',
        'branch_number',
        'branch_name',
        'account_number',
        'proof_of_bank_account'
    ];

    public function bankAccountType()
    {
        return $this->hasOne(BankAccountType::class, 'id');
    }

    public function relationshipType()
    {
        return $this->hasOne(Relationship::class, 'id');
    }
}
