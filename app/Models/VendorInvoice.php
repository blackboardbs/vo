<?php

namespace App\Models;

use App\Traits\InvoiceTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Kyslik\ColumnSortable\Sortable;

class VendorInvoice extends Model
{
    use Sortable;
    use InvoiceTrait;

    protected $table = 'vendor_invoice';
    protected $primaryKey = 'id';
    public $sortable = ['id','vendor_invoice_number', 'vendor_id', 'vendor_invoice_date', 'vendor_invoice_ref','vendor_paid_date', 'vendor_due_date', 'vendor_invoice_value', 'vendor_invoice_status', 'created_at'];

    protected function conversionRate(): Attribute
    {
        return Attribute::make(
            get: fn (int|null $value) => isset($value)?($value/100):1.00,
            set: fn (string|null $value) => isset($value)?((1/$value)*100):0.01,
        );
    }
    public function invoice_status(){
        return $this->belongsTo(VendorInvoiceStatus::class, 'vendor_invoice_status');
    }

    public function vendorInvoiceLine(){
        return $this->hasMany(VendorInvoiceLine::class,  'vendor_invoice_number');
    }

    public function vendorInvoiceLineExpense()
    {
        return $this->hasMany(VendorInvoiceLineExpense::class, 'vendor_invoice_number');
    }

    public function resource(){
        return $this->belongsTo(User::class, 'resource_id');
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function vatRate(): BelongsTo
    {
        return $this->belongsTo(VatRate::class, 'vat_rate_id');
    }

    public function recurring(): MorphOne
    {
        return $this->morphOne(Recurring::class, 'recurrable');
    }

    public function nonTimesheetVendorInvoices()
    {
        return $this->hasMany(InvoiceItems::class, 'invoice_id')->where('invoice_type_id', 2);
    }

    public function histories()
    {
        return $this->belongsToMany(self::class, 'recurring_invoice_histories', 'recurring_invoice_id', 'non_timesheet_invoice_id')->withTimestamps();
    }

}
