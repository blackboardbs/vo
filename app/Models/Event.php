<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Event extends Model
{
    protected $table = 'event';

    use Sortable;

    public $sortable = ['id', 'event', 'status', 'row_order', 'event_date'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
