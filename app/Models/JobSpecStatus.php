<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSpecStatus extends Model
{
    use SoftDeletes;

    protected $table = 'job_spec_status';
}
