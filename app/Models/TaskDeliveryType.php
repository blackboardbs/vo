<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class TaskDeliveryType extends Model
{
    use SoftDeletes;
    use Sortable;

    public $sortable = ['id', 'name', 'status_id'];

    public function status() 
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function scopeFilters($query)
    {
    }
}
