<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarEvents extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'id';
    protected $fillable = ['title','event_date'];
}
