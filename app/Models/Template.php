<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Template extends Model
{
    use Sortable;

    protected $table = 'template';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name', 'content', 'template_type_id'];

    public function type()
    {
        return $this->belongsTo(\App\Models\TemplateType::class, 'template_type_id');
    }
}
