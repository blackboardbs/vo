<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Spatie\Permission\Models\Role;

class DefaultDashboard extends Model
{
    use Sortable;

    public $sortable = ['id', 'dashboard_name', 'status_id', 'role_id'];

    public function roles()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
