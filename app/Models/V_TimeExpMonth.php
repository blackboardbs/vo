<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_TimeExpMonth extends Model
{
    protected $table = 'v_time_exp_month';

    public function leaved()
    {
        return $this->belongsTo(\App\Models\V_Leave::class, 'employee_id', 'employee_id');
    }
}
