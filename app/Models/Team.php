<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';

    protected $primaryKey = 'id';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function company()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'company', 'id', 'parent_company_id');
    }

    public function creator()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id', 'id', 'creator_id');
    }

    public function employee()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'id', 'team_id');
    }

    public function manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'team_manager', 'id', 'team_manager');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($team) {
                $team->where('parent_company_id', request()->company);
            })->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '0' && request()->employee != '', function ($team) {
                $team->whereHas('employee', function($q){
                    $q->where('parent_company_id', request()->company);
                });
            })->orderBy('team_name');
    }
}
