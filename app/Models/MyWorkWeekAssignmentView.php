<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyWorkWeekAssignmentView extends Model
{
    use HasFactory;
    protected $table = 'my_work_week_assignment_view';
}
