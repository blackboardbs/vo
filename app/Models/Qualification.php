<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $table = 'qualification';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function suburb()
    {
        return $this->belongsTo(\App\Models\Suburb::class, 'city_suburb_id');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    public function consultant()
    {
        return $this->belongsTo(\App\Models\User::class, 'consultant_emp_id');
    }

    public function terms()
    {
        return $this->belongsTo(\App\Models\Terms::class, 'terms_id');
    }
}
