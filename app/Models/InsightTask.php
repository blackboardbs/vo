<?php

namespace App\Models;

use App\Traits\TaskTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsightTask extends Model
{
    use HasFactory;
    use TaskTrait;

    protected $table = 'timesheet_report_view';

}
