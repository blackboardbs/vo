<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetAdditionalCost extends Model
{
    use HasFactory;

    protected $fillable = ['asset_id', 'date', 'cost', 'warranty', 'insurance', 'support_ref', 'notes'];

    protected function cost(): Attribute
    {
        return Attribute::make(
            get: fn (int|float|null $value) => $value/100,
            //set: fn (int|float|null $value) => $value*100,
        );
    }
}
