<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ExpenseType extends Model
{
    use Sortable;
    use HasFactory;
    public $sortable = ['id', 'description', 'status'];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
