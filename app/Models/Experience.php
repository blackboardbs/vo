<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'cv';

    public function cv_companyd()
    {
        return $this->belongsTo(\App\Models\CvCompany::class, 'company_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
