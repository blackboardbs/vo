<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessRating extends Model
{
    use SoftDeletes;

    protected $table = 'business_rating';
}
