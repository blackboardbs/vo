<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintHoursSummaryHistory extends Model
{
    use HasFactory;

    protected $fillable = ['sprint_id', 'user_id', 'planned_hours', 'booked_hours', 'outstanding_hours', 'available_hours', 'potential_outcome'];
}
