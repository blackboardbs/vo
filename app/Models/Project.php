<?php

namespace App\Models;

use App\Enum\TimesheetTemplatesEnum;
use App\Enum\YesOrNoEnum;
use App\Traits\ProjectTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;
use Kyslik\ColumnSortable\Sortable;

class Project extends Model
{
    use Sortable;
    use ProjectTrait;

    protected $table = 'projects';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name', 'type', 'ref', 'status_id', 'start_date', 'end_date'];

    protected $casts = [
        'billable' => YesOrNoEnum::class,
        'timesheet_template_id' => TimesheetTemplatesEnum::class,
        'is_fixed_price' => YesOrNoEnum::class
    ];

    public function status()
    {
        return $this->belongsTo(\App\Models\ProjectStatus::class, 'status_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'prepared_by_emp_id');
    }

    public function manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'manager_id');
    }

    public function expense()
    {
        return $this->belongsTo(\App\Models\Expense::class, 'exp_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function consultant()
    {
        return $this->belongsTo(\App\Models\User::class, 'consultant_emp_id');
    }

    public function assignment()
    {
        return $this->hasMany(Assignment::class, 'project_id');
    }
    public function actions()
    {
        return $this->hasMany(Action::class, 'related_project_id');
    }

    public function project_type()
    {
        return $this->belongsTo(\App\Models\ProjectType::class, 'project_type_id');
    }

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class, 'project_id');
    }

    public function epics()
    {
        return $this->hasMany(Epic::class, 'project_id');
    }

    public function sprints()
    {
        return $this->hasMany(Sprint::class, 'project_id');
    }
    public function risks()
    {
        return $this->hasMany(Risk::class, 'project_id');
    }

    public function billingCycle(): BelongsTo
    {
        return $this->belongsTo(BillingCycle::class, 'billing_cycle_id');
    }

    public function timesheets()
    {
        return $this->hasMany(Timesheet::class, 'project_id');
    }

    public function viewPermission($section)
    {
        $hasPermission = [
            'view' => false,
            'create' => false,
            'update' => false,
            'full' => false,
        ];

        switch ($section) {
            case 'project_header':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead']);
                break;
            case 'consultants_assigned':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator']);
                break;
            case 'tasks':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer']);
                $hasPermission['create'] = Auth::user()->hasRole(['own_project_lead', 'project_manager', 'project_administrator', 'external_party']);
                $hasPermission['update'] = Auth::user()->hasRole(['own_project_lead', 'project_manager', 'project_administrator', 'external_party']);
                $hasPermission['full'] = Auth::user()->hasRole(['own_project_lead', 'project_manager', 'project_administrator', 'external_party']);
                break;
            case 'billable_expenses':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead']);
                break;
            case 'project_analysis':
                // No access
                break;
            case 'task_list':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
            case 'kanban':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
            case 'tree':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
            case 'sprint':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
            case 'risk':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                $hasPermission['create'] = Auth::user()->hasRole(['own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator']);
                $hasPermission['update'] = Auth::user()->hasRole(['own_project_lead', 'project_manager']);
                $hasPermission['full'] = Auth::user()->hasRole(['own_project_lead', 'project_manager']);
                break;
            case 'gantt':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
            case 'cost':
                $hasPermission['view'] = Auth::user()->hasRole(['own_project_lead']);
                break;
            case 'actions':
                $hasPermission['view'] = Auth::user()->hasRole(['admin', 'admin_manager', 'manager', 'consultant', 'contractor', 'vendor', 'cv_resource', 'customer_client', 'user', 'recruitment', 'own_project_lead', 'assigned_resources', 'project_manager', 'project_administrator', 'customer_resource', 'observer', 'external_party']);
                break;
        }

        return $hasPermission;
    }

    public function customerInvoiceContact()
    {
        return $this->belongsTo(InvoiceContact::class, 'customer_invoice_contact_id');
    }

    public function vendorInvoiceContact()
    {
        return $this->belongsTo(InvoiceContact::class, 'vendor_invoice_contact_id');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($project) {
            $project->where('company_id', request()->company);
        })->when(request()->customer, function ($task) {
            if(request()->customer != '-1' && request()->customer != '0' && request()->customer != ''){
                $task->where('customer_id', request()->customer);
            }
        })->when(request()->client, function ($task) {
            if(request()->client != '-1' && request()->client != '0' && request()->client != ''){
                $task->where('customer_id', request()->client);
            }
        })->when(isset(request()->employee) && request()->employee != '-1', function ($user) {
            $user->whereHas('assignment',function ($q){
                $q->where('employee_id',request()->employee);
            });
        })->when(isset(request()->resource) && request()->resource != '-1' && request()->resource != '0' && request()->resource != '', function ($user) {
            if(request()->resource != '-1' && request()->resource != '0' && request()->resource != ''){
                $user->whereHas('assignment',function ($q){
                    $q->where('employee_id',request()->resource);
                });
        }})->when(isset(request()->project_status) && request()->project_status != '' && request()->project_status != '0', function ($project) {
            if(request()->project_status == '1' ){
                $project->whereIn('status_id', [1, 2, 3]);
            }
            if(request()->project_status == '2' ){
                $project->whereIn('status_id', [4, 5]);
            }
        });
    }

    public function invoices()
    {
        return $this->hasMany(CustomerInvoice::class, 'project_id');
    }

    public function vinvoices()
    {
        return $this->hasMany(VendorInvoice::class, 'project_id');
    }

    public function defaultBillingTemplate()
    {
        return $this->belongsTo(BillingPeriodStyle::class, 'billing_timesheet_templete_id');
    }
}
