<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class BEEMasterData extends Model
{
    use Sortable;

    protected $table = 'beemasterdata';

    protected $timestamp = false;

    public function project_t()
    {
        return $this->belongsTo(\App\Models\ProjectType::class, 'project_type');
    }

    public function naturalization()
    {
        return $this->belongsTo(\App\Models\Naturalization::class, 'naturalization_id');
    }

    public function race()
    {
        return $this->belongsTo(\App\Models\Race::class, 'race_id');
    }

    public function bbbee_l()
    {
        return $this->belongsTo(\App\Models\BBBEELevel::class, 'bbbee_level');
    }

    public function bbbee_r()
    {
        return $this->belongsTo(\App\Models\BBBEERace::class, 'bbbee_race');
    }

    public function genderd()
    {
        return $this->belongsTo(\App\Models\Gender::class, 'gender');
    }

    public function disabilityd()
    {
        return $this->belongsTo(\App\Models\Disability::class, 'disability');
    }
}
