<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kyslik\ColumnSortable\Sortable;

class TemplateType extends Model
{
    use Sortable;

    protected $table = 'template_type';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function variables(): HasMany
    {
        return $this->hasMany(TemplateVariable::class, 'template_type_id');
    }
}
