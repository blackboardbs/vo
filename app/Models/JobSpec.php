<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class JobSpec extends Model
{
    use HasFactory;
    use SoftDeletes, Sortable;

    public $sortable = ['id', 'reference_number', 'role_id', 'applicaiton_closing_date', 'employment_type_id'];

    public function positiontype()
    {
        return $this->belongsTo(\App\Models\ResourceType::class, 'employment_type_id');
    }

    public function positionrole()
    {
        return $this->belongsTo(\App\Models\ScoutingRole::class, 'role_id');
    }

    //Todo: Add Master Data
    public function origin()
    {
        return $this->belongsTo(\App\Models\JobOrigin::class, 'origin_id');
    }

    public function speciality()
    {
        return $this->belongsTo(\App\Models\Speciality::class, 'speciality_id');
    }

    //Todo: Add Master Data
    public function worktype()
    {
        return $this->belongsTo(\App\Models\WorkType::class, 'work_type_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    //Todo: Add Master Data
    public function status()
    {
        return $this->belongsTo(\App\Models\JobSpecStatus::class, 'status_id');
    }

    public function recruiter()
    {
        return $this->belongsTo(\App\Models\User::class, 'recruiter_id');
    }
}
