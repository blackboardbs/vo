<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CVWishList extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'cv_wish_lists';

    public $sortable = ['id', 'cv_id', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function cretaor()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id');
    }
}
