<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BBBEELevel extends Model
{
    use HasFactory;

    protected $table = 'bbbee_level';

    protected $primaryKey = 'id';

    public function company()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'company', 'bbbee_level_id');
    }
}
