<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Epic extends Model
{
    use SoftDeletes;

    public function project(){
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function features(){
        return $this->hasMany(Feature::class, 'epic_id');
    }

    public function sprint(){
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function status(){
        return $this->belongsTo(TaskStatus::class, 'status_id');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($user) {
            $user->whereHas('project', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '0' && request()->customer != '', function ($task) {
                $task->whereHas('project', function ($q){
                    $q->where('customer_id', request()->customer);
                });
        })->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($user) {
                $user->where('project_id',request()->project);
            })->when(isset(request()->project_id) && request()->project_id != '-1' && request()->project_id != '0' && request()->project_id != '', function ($user) {
                $user->where('project_id',request()->project_id);
        });
    }
}
