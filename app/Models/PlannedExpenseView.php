<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlannedExpenseView extends Model
{
    use HasFactory;

    protected $table = 'planned_expenses_view';
}
