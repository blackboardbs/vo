<?php

namespace App\Models;

use App\Traits\SprintTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sprint extends Model
{
    use SoftDeletes;
    use SprintTrait;

    protected $fillable = ['creator_id', 'end_date', 'goal', 'name', 'project_id', 'start_date', 'status_id'];

    function status(){
        return $this->belongsTo(SprintStatus::class, 'status_id');
    }

    function project(){
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function userStories(): HasMany
    {
        return $this->hasMany(UserStory::class);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'sprint_id');
    }

    public function goals(): HasMany
    {
        return $this->hasMany(Goal::class, 'sprint_id');
    }

    public function sprintReviews(): HasMany
    {
        return $this->hasMany(SprintReview::class, 'sprint_id');
    }

    public function resources(): HasMany
    {
        return $this->hasMany(SprintResource::class, 'sprint_id');
    }

    public function sprintHoursSummary(): HasMany
    {
        return $this->hasMany(SprintHoursSummary::class, 'SprintId');
    }

    

    public function scopeFilters($query)
    {
        return $query->when(request()->project_id, function ($task) {
                if(isset(request()->project_id) && request()->project_id != '-1' && request()->project_id != '' && request()->project_id != 0){
                    $task->where('project_id', '=', request()->project_id);
                }
            });
    }
}
