<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Kyslik\ColumnSortable\Sortable;

class Timeline extends Model
{
    use Sortable;

    protected $table = 'timeline';

    public $sortable = ['id', 'status'];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function timesheet()
    {
        return $this->belongsTo(Timesheet::class, 'timesheet_id');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function taskDeliveryType()
    {
        return $this->belongsTo(TaskDeliveryType::class, 'task_delivery_type_id');
    }

    public function time($_minutes)
    {
        $hours = floor($_minutes / 60);
        $minutes = $_minutes % 60;
        $negation_number = '';

        if ($_minutes < 0) {
            $hours = floor(abs($_minutes) / 60);
            $minutes = abs($_minutes) % 60;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }
}
