<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Utilization extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'plan_utilization';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'yearwk', 'res_no', 'wk_hours', 'cost_res_no', 'cost_wk_hours', 'status_id', 'company_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
