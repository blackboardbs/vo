<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Site extends Model
{
    use Sortable;
    protected $table = 'site';
    protected $primaryKey = 'id';
    public $sortable = ['id', 'name', 'type', 'ref', 'status_id'];
}
