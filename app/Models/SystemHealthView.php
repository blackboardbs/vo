<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemHealthView extends Model
{
    use HasFactory;

    protected $table = 'sh_kpi_view';
}
