<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Risk extends Model
{
    use SoftDeletes, Sortable;

    public function project(){
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function riskarea(){
        return $this->belongsTo(RiskArea::class, 'risk_area_id');
    }

    public function riskitems(){
        return $this->hasMany(RiskItem::class, 'risk_id');
    }

    public function resposibility(){
        return $this->belongsTo(User::class, 'responsibility_user_id');
    }

    public function accountability(){
        return $this->belongsTo(User::class, 'accountability_user_id');
    }

    public function security(){
        return $this->belongsTo(Security::class, 'security_id');
    }

    public function likeylihood(){
        return $this->belongsTo(Likely::class, 'likelihood_id');
    }

    public function impact(){
        return $this->belongsTo(Impact::class, 'impact_id');
    }

    public function status(){
        return $this->belongsTo(TaskStatus::class, 'status_id');
    }

    public function feedback()
    {
        return $this->hasMany(RiskFeedback::class, 'risk_id');
    }
}
