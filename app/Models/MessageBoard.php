<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class MessageBoard extends Model
{
    use Sortable;
    use HasFactory;

    public $sortable = ['id', 'message', 'message_date', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
