<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class MCalendar extends Model
{
    use Sortable;

    protected $table = 'calendar';

    protected $primaryKey = 'calendar_id';

    public $sortable = ['calendar_id', 'date', 'year', 'month', 'week', 'day', 'day_name', 'status', 'public_holiday'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
