<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApprovalRules extends Model
{
    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id');
    }
    public function substituteApprover()
    {
        return $this->belongsTo(User::class, 'substitute_approver_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
