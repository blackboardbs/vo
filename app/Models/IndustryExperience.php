<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class IndustryExperience extends Model
{
    use Sortable, SoftDeletes;

    protected $table = 'industry_experience';

    protected $primaryKey = 'id';

    public $sortable = ['res_id', 'ind_id', 'id', 'status'];

    public function industry()
    {
        return $this->belongsTo(\App\Models\Industry::class, 'ind_id');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'res_id');
    }
}
