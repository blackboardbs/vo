<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Time extends Model
{
    use Sortable;

    protected $table = 'time';

    public $sortable = ['id', 'emp_id', 'status'];

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'emp_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'wbs_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function invoice_status()
    {
        return $this->belongsTo(\App\Models\InvoiceStatus::class, 'bill_status');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
