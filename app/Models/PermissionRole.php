<?php

namespace App\Models;

use App\Permission;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    public $timestamps = false;

    protected $table = 'permission_role';

    public function role()
    {
        return $this->belongsTo(\Spatie\Permission\Models\Role::class, 'role_id');
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }
}
