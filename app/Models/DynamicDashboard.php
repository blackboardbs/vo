<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class DynamicDashboard extends Model
{
    use HasFactory;
    use Sortable;

    public $sortable = ['id', 'description'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
