<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintBurnHourView extends Model
{
    use HasFactory;

    protected $table = 'sprint_burn_hour_view';
}
