<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ContractType extends Model
{
    use Sortable;

    public $sortable = ['id', 'description', 'status_id'];

    public function Status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
