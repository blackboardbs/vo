<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Document extends Model
{
    use Sortable;
    use SoftDeletes;

    protected $table = 'document';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name', 'content', 'document_type_id'];

    protected $guarded = [];

    protected $with = ['user:id,first_name,last_name'];

    public function documentable()
    {
        return $this->morphTo();
    }

    public function type()
    {
        return $this->belongsTo(\App\Models\DocumentType::class, 'document_type_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id');
    }

    public function owner()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner_id');
    }

    public function digisignstatus()
    {
        return $this->belongsTo(\App\Models\DigiSignStatus::class, 'digisign_status_id');
    }
}
