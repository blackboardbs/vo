<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Terms extends Model
{
    use Sortable;

    protected $table = 'terms';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'term', 'row_order', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
