<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public function permissions()
    {
        return $this->belongsTo(\App\Models\BouncerPermissions::class, 'id', 'entity_id');
    }

    public function simplepermissions()
    {
        return $this->belongsTo(\App\Models\SimplePermissions::class, 'id', 'role_id');
    }
}
