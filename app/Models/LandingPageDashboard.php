<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPageDashboard extends Model
{
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
