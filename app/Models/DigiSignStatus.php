<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class DigiSignStatus extends Model
{
    use Sortable;
    protected $table = 'digisign_status';
    protected $primaryKey = 'id';
    public $sortable = ['id', 'name'];
}
