<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Speciality extends Model
{
    use SoftDeletes;
    use Sortable;

    public $sortable = ['id', 'name', 'status_id'];

    public function profession()
    {
        return $this->belongsTo(\App\Models\Profession::class, 'profession_id');
    }

    public function creator()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
