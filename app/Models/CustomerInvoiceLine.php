<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerInvoiceLine extends Model
{
    public function assignment(){
        return $this->belongsTo(Assignment::class, 'assignment_id');
    }

    public function timesheet(){
        return $this->belongsTo(Timesheet::class, 'timesheet_id');
    }
}
