<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentExtension extends Model
{
    use SoftDeletes;

    protected $table = 'assignment_extension';
}
