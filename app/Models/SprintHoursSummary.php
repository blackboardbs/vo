<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintHoursSummary extends Model
{
    use HasFactory;

    protected $table = 'sprint_hours_summary_view';
}
