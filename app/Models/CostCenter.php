<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CostCenter extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'cost_center';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function appr_name()
    {
        return $this->belongsTo(\App\Models\User::class, 'approver');
    }

    public function scopeFilters($query)
    {
    }
}
