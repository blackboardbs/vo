<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CvCompany extends Model
{
    use Sortable;

    protected $table = 'cv_company';

    public $sortable = ['id', 'description', 'status_id'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
