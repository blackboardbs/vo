<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CVQuality extends Model
{
    protected $table = 'cv_qualities';
    use SoftDeletes;
}
