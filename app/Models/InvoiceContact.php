<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceContact extends Model
{
    use HasFactory;

    protected $appends = ['full_name'];

    protected $fillable = ['first_name', 'last_name', 'contact_number', 'email', 'birthday', 'status_id'];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name." ".$this->last_name;
    }
}
