<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Log extends Model
{
    use Sortable;

    public $sortable = ['id', 'user_ip', 'user_name', 'password', 'date'];
}
