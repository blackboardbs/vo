<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class AnniversaryType extends Model
{
    use Sortable;

    protected $table = 'anniversary_type';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
