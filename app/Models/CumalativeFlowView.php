<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CumalativeFlowView extends Model
{
    use HasFactory;
    protected $table = 'cashflow_cumalitive_flow_view';
}
