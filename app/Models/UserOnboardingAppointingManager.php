<?php

namespace App\Models;

use App\Enum\YesOrNoEnum;
use App\PaymentType;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class UserOnboardingAppointingManager extends Model
{
    protected $guarded = [];

    protected $dates = [
        'approved_at',
        'issued_at',
        'recieved_at',
        'signed_at',
        'filed_at',
    ];

    protected $casts = [
        'require_laptop' => YesOrNoEnum::class,
        'require_phone' => YesOrNoEnum::class,
        'require_mobile_internet' => YesOrNoEnum::class
    ];

    protected function annualSalary(): Attribute
    {
        return Attribute::make(
            get: fn (int|null $value) => $value/100,
            set: fn (int|float|null $value) => $value*100,
        );
    }

    public function userOnboarding()
    {
        return $this->belongsTo(UserOnboarding::class, 'user_onboarding_id');
    }

    public function contractType()
    {
        return $this->belongsTo(ContractType::class, 'contract_type_id');
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    public function paymentBase()
    {
        return $this->belongsTo(PaymentBase::class, 'payment_base_id');
    }

    public function reportingManager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'reporting_manager');
    }

    public function permanentContractTemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'permenant_contract_template_id');
    }

    public function fixedContractTemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'fixed_contract_template_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }
}
