<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TemplateVariable extends Model
{
    use HasFactory;

    protected $fillable = ['display_name', 'variable', 'template_type_id', 'status_id'];

    public function templateType(): BelongsTo
    {
        return $this->belongsTo(TemplateType::class);
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }
}
