<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectBillingView extends Model
{
    use HasFactory;

    protected $table = 'project_billing_view';
}
