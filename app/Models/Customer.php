<?php

namespace App\Models;

use App\Traits\HasImage;
use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Kyslik\ColumnSortable\Sortable;

class Customer extends Model
{
    use Sortable;
    use HasImage;
    use HasFactory;
    use TemplateTrait;

    const ONBOARDING = [
        1 => 'Yes',
        0 => 'No',
    ];

    protected $table = 'customer';

    protected $primaryKey = 'id';

    public $sortable = ['customer_name', 'cell', 'phone', 'email', 'created_at'];

    protected $appends = ['customer_onboarding'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function medical_certificate()
    {
        return $this->belongsTo(\App\Models\MedicalCertificateType::class, 'medical_certificate_type');
    }

    public function accountm()
    {
        return $this->belongsTo(\App\Models\User::class, 'account_manager');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    public function billingCycle()
    {
        return $this->belongsTo(BillingCycle::class, 'billing_cycle_id');
    }

    public function invoiceContact()
    {
        return $this->belongsTo(InvoiceContact::class, 'invoice_contact_id');
    }

    public function getCustomerOnboardingAttribute()
    {
        $onboarding = $this->onboarding ?? 0;

        return self::ONBOARDING[$onboarding];
    }

    public function appointmentManager()
    {
        return $this->belongsTo(User::class, 'appointment_manager_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'customer_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'customer_id', 'id');
    }
    
    public function quotations()
    {
        return $this->hasMany(Quotation::class, 'customer_id', 'id');
    }
    
    public function timesheets()
    {
        return $this->hasMany(Timesheet::class, 'customer_id', 'id');
    }
    
    public function invoices()
    {
        return $this->hasMany(CustomerInvoice::class, 'customer_id', 'id');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($proj) {
                    $proj->whereHas('projects', function ($q){
                        $q->where('company_id',request()->company);
                    });
            })
            ->when(isset(request()->company_id) && request()->company_id != '-1' && request()->company_id != '0' && request()->company_id != '', function ($proj) {
                $proj->whereHas('projects', function ($q){
                    $q->where('company_id',request()->company_id);
                });
        })
        ->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($proj) {
                $proj->whereHas('projects', function ($q){
                    $q->where('id',request()->project);
                });
        })
        ->when(isset(request()->project_status) && request()->project_status != '' && request()->project_status != '0', function ($project) {
            if(request()->project_status == '1' ){
                $project->whereHas('projects', function ($q){
                    $q->whereIn('status_id', [1, 2, 3]);
                });
            }
            if(request()->project_status == '2' ){
                $project->whereHas('projects', function ($q){
                    $q->whereIn('status_id', [4, 5]);
                });
            }
        })
        ->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '0' && request()->employee != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->employee);
                });
        })
        ->when(isset(request()->resource) && request()->resource != '-1' && request()->resource != '0' && request()->resource != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->resource);
                });
        })
        ->when(isset(request()->resource_id) && request()->resource_id != '-1' && request()->resource_id != '0' && request()->resource_id != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->resource_id);
                });
        });
        // ->when(request()->customer, function ($task) {
        //     $task->where('customer_id', request()->customer);
        // });
    }

    public function serviceAgreement(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'service_agreement_id');
    }

    public function NDATemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'nda_template_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }
}
