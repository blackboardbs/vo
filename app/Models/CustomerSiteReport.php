<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CustomerSiteReport extends Model
{
    use HasFactory;
    
    use sortable;

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function template()
    {
        return $this->belongsTo(\App\Models\CustomerSiteReportTemplate::class, 'template_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }
    
    public function actual()
    {
        return $this->hasMany(\App\Models\CustomerSiteReportActual::class, 'site_report_id');
    }
    
    public function planned()
    {
        return $this->hasMany(\App\Models\CustomerSiteReportPlanned::class, 'site_report_id');
    }
    
    public function hours_summary()
    {
        return $this->hasMany(\App\Models\CustomerSiteReportHoursSummary::class, 'site_report_id');
    }
    
    public function risks()
    {
        return $this->hasMany(\App\Models\CustomerSiteReportRisks::class, 'site_report_id');
    }
}
