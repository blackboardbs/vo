<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class System extends Model
{
    use Sortable;

    protected $table = 'system';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public $sortable = ['id', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
