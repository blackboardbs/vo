<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobSpecActivityLog extends Model
{
    use HasFactory;

    public function jobSpec()
    {
        return $this->belongsTo(\App\Models\JobSpec::class, 'job_spec_id');
    }

    public function author()
    {
        return $this->belongsTo(\App\Models\User::class, 'author_id');
    }

    public function status()
    {
        return $this->belongsTo('App\ActivityStatus', 'status_id');
    }

    public function cv()
    {
        return $this->belongsTo(\App\Models\Cv::class, 'cv_id');
    }
}
