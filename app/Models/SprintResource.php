<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SprintResource extends Model
{
    use HasFactory;
    protected $fillable = ['sprint_id', 'goal_id', 'employee_id', 'function_id', 'capacity'];

    public function sprint(): BelongsTo
    {
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function goal(): BelongsTo
    {
        return $this->belongsTo(Goal::class, 'goal_id');
    }

    public function resource(): BelongsTo
    {
        return $this->belongsTo(User::class, 'employee_id');
    }

    public function function(): BelongsTo
    {
        return $this->belongsTo(BusinessFunction::class, 'function_id');
    }
}
