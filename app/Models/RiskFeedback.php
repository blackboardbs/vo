<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskFeedback extends Model
{
    protected $table = 'risk_feedbacks';
    public function risk()
    {
        return $this->belongsTo(Risk::class, 'risk_id');
    }

    public function relatedUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
