<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ResourcePosition extends Model
{
    use Sortable;

    protected $table = 'resource_position';

    public $sortable = ['id', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
