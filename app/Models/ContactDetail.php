<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactDetail extends Model
{
    protected $fillable = [
        'physical_address',
        'postal_address',
        'email',
        'home_number',
        'work_number',
        'cell_number',
        'next_of_kin_name',
        'next_of_kin_number'
    ];
}
