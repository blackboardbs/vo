<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Timesheet extends Model
{
    use Sortable;

    protected $table = 'timesheet';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'employee_id', 'project_id', 'year_week', 'first_day_of_week', 'status_id', 'is_billable', 'system_id'];

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function employee()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'employee_id', 'user_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }/*

    public function project(){
        return $this->belongsTo('App\Models\Project', 'wbs_id');
    }*/

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function bill_statusd()
    {
        return $this->belongsTo(\App\Models\InvoiceStatus::class, 'bill_status');
    }

    public function timeline()
    {
        return $this->hasMany(\App\Models\Timeline::class, 'timesheet_id');
    }

    public function time_exp()
    {
        return $this->hasMany(\App\Models\TimeExp::class);
    }

    public function vendor_inv_status()
    {
        return $this->belongsTo(\App\Models\VendorInvoiceStatus::class, 'vendor_invoice_status');
    }

    public function customer_invoice()
    {
        return $this->belongsTo(CustomerInvoice::class, 'invoice_number');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($project) {
                $project->where('company_id', request()->company);
        })
        ->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
                $task->where('customer_id', request()->customer);
        })
        ->when(isset(request()->project) && request()->project != '-1' && request()->project != '' && request()->project != 0, function ($task) {
                $task->whereHas('project',function($q){
                    $q->where('id',request()->project);
                });
        })
        ->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '' && request()->employee != 0, function ($task) {
            $task->whereHas('employee',function($q){
                $q->where('id',request()->employee);
            });
        })
        ->when(isset(request()->employees) && request()->employees != '-1' && request()->employees != '' && request()->employees != 0, function ($task) {
            $task->whereHas('employee',function($q){
                $q->where('id',request()->employees);
            });
        })
        ->when(isset(request()->project_status) && request()->project_status != '' && request()->project_status != '0', function ($project) {
            if(request()->project_status == '1' ){
                $project->whereHas('project', function ($q){
                    $q->whereIn('status_id', [1, 2, 3]);
                });
            }
            if(request()->project_status == '2' ){
                $project->whereHas('project', function ($q){
                    $q->whereIn('status_id', [4, 5]);
                });
            }
        })
        ->when(request()->cis, function ($task) {
            if(request()->cis != '-1'){
                $task->where('bill_status', request()->cis);
            }
        })
        ->when(request()->vis, function ($task) {
            if(request()->vis != '-1'){
                if(request()->vis == '4'){
                    $task->whereNull('vendor_invoice_status');
                } else {
                $task->where('vendor_invoice_status', request()->vis);
                }
            }
        });
    }
}
