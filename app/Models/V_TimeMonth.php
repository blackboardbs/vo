<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_TimeMonth extends Model
{
    protected $table = 'v_time_month';

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function leave()
    {
        return $this->belongsTo(\App\Models\V_Leave::class, 'employee_id', 'employee_id');
    }
}
