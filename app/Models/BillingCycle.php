<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class BillingCycle extends Model
{
    use SoftDeletes;
    use Sortable;
    use HasFactory;

    public $sortable = ['name', 'updated_at'];

    public function billingPeriods(): HasMany
    {
        return $this->hasMany(BillingPeriod::class, 'billing_cycle_id');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function scopeFilters($query)
    {
    }
}
