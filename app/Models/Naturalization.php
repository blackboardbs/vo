<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Naturalization extends Model
{
    protected $table = 'naturalization';
    protected $primaryKey = 'id';
}