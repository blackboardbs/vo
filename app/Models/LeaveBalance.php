<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class LeaveBalance extends Model
{
    use Sortable;

    protected $table = 'leave_balance';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'emp_id', 'leave_type_id', 'opening_balance', 'leave_per_cycle', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }//

    public function leave_type()
    {
        return $this->belongsTo(\App\Models\LeaveType::class, 'leave_type_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'resource_id');
    }

    public function is_divisible_by_3($n)
    {
        $digits = str_split($n);
        $total = 0;
        foreach ($digits as $digit) {
            $total += $digit;
        }
        if ($total == 3 || ($total % 3 == 0)) {
            return true;
        }

        return false;
    }

    public function minus_year($n)
    {
        $total = $n;

        do {
            $total = $total - 3;
        } while ($total > 0);

        return $total;
    }
}
