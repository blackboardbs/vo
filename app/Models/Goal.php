<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Goal extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'sprint_id'];

    public function sprint(): BelongsTo
    {
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'goal_id');
    }

    public function resources(): HasMany
    {
        return $this->hasMany(SprintResource::class, 'goal_id');
    }
}
