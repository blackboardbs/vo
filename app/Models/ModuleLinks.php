<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleLinks extends Model
{
    use SoftDeletes;

    protected $table = 'module_links';

    public function module()
    {
        return $this->belongsTo(\App\Models\Module::class, 'module_id');
    }
}
