<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Kyslik\ColumnSortable\Sortable;

class Prospect extends Model
{
    use Sortable;

    protected $table = 'prospect';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'prospect_name', 'decision_date', 'value', 'status_id'];

    public function account_manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'account_manager_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\ProspectStatus::class, 'status_id');
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function discussion()
    {
        return $this->hasMany(ProspectNote::class, 'prospect_id');
    }
}
