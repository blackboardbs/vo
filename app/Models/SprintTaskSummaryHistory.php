<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintTaskSummaryHistory extends Model
{
    use HasFactory;

    protected $fillable = ['sprint_id', 'user_id', 'no_of_tasks', 'planned_hours', 'booked_hours', 'overdue_tasks', 'backlog', 'planned', 'in_progress', 'done', 'completed', 'impediments'];
}
