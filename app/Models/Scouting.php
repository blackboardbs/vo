<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Scouting extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'scouting';

    public $sortable = ['id', 'resource_first_name', 'resource_last_name', 'email', 'process_status_id', 'interview_status_id', 'user_id', 'vendor_id', 'status_id'];

    public function processstatus()
    {
        return $this->belongsTo(\App\Models\ProcessStatus::class, 'process_status_id');
    }

    public function interviewstatus()
    {
        return $this->belongsTo(\App\Models\ProcessInterview::class, 'interview_status_id');
    }

    public function role()
    {
        return $this->belongsTo(\App\Models\ScoutingRole::class, 'role_id');
    }

    public function vendor()
    {
        return $this->belongsTo(\App\Models\Vendor::class, 'vendor_id');
    }

    public function resourcelevel()
    {
        return $this->belongsTo(\App\Models\ResourceLevel::class, 'resource_level_id');
    }

    public function businessrating()
    {
        return $this->belongsTo(\App\Models\BusinessRating::class, 'business_rating_id');
    }

    public function technicalrating()
    {
        return $this->belongsTo(\App\Models\TechnicalRating::class, 'technical_rating_id');
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'reference_id', 'id');
    }
}
