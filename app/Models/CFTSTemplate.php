<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CFTSTemplate extends Model
{
    use SoftDeletes;

    protected $table = 'cfts_template';

    protected $primaryKey = 'id';
}
