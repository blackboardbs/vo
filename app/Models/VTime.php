<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VTime extends Model
{
    protected $table = 'v_time';

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function time()
    {
        return $this->belongsTo(\App\Models\Time::class, 'time_id');
    }

    public function invoice_status()
    {
        return $this->belongsTo(\App\Models\InvoiceStatus::class, 'bill_status');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Wbs::class, 'project_id');
    }

    public function vat_rate()
    {
        return $this->belongsTo(\App\Models\VatRate::class, 'vat_rate_id');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country');
    }
}
