<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Relationship extends Model
{
    use Sortable;

    protected $table = 'relationship';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
