<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemHealth extends Model
{
    use HasFactory;

    protected $table = 'sh_master_data';

    protected $primaryKey = 'id';

    public function category_name()
    {
        return $this->belongsTo(\App\Models\SystemHealthCategory::class, 'category');
    }
}
