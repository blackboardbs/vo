<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignmentCost extends Model
{
    protected $table = 'assignment_cost';

    public function account_element()
    {
        return $this->belongsTo(\App\Models\AccountElement::class, 'account_element_id');
    }

    public function payment_meth()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class, 'payment_method');
    }

    public function assignment()
    {
        return $this->belongsTo(\App\Models\Assignment::class, 'ass_id');
    }
}
