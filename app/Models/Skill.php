<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = 'skill';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function skill_leveld()
    {
        return $this->belongsTo(\App\Models\SkillLevel::class, 'skill_level');
    }

    public function tool_idd()
    {
        return $this->belongsTo(\App\Models\System::class, 'tool_id');
    }
}
