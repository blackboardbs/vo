<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ProcessInterview extends Model
{
    use SoftDeletes;
    use Sortable;

    public $sortable = ['id', 'name'];

    protected $table = 'interview_status';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
