<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentMaster extends Model
{
    use HasFactory;

    protected $table = 'assessment_master';

    public $sortable = ['id', 'description'];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
