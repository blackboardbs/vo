<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnniversaryType extends Model
{
    use HasFactory;

    protected $table = 'anniversary_type';
}
