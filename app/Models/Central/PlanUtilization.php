<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanUtilization extends Model
{
    use HasFactory;

    protected $table = 'plan_utilization';
}
