<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessInterview extends Model
{
    use HasFactory;

    protected $table = 'interview_status';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
