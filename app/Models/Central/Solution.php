<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    use HasFactory;

    protected $table = 'solution';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
