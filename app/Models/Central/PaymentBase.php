<?php

namespace App\Models\Central;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentBase extends Model
{
    use HasFactory;

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
