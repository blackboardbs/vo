<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateType extends Model
{
    use HasFactory;

    protected $table = 'template_type';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
