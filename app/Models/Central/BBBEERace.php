<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BBBEERace extends Model
{
    use HasFactory;

    protected $table = "bbbee_race";
}
