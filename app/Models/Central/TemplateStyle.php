<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateStyle extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "billing_period_styles";

    const TEMPLATES = [
        1 => "Grid Template",
        2 => "Horizontal Template",
        3 => "Vertical Template",
        4 => "Monthly Template"
    ];

    const LOGO = [
        null => "Select Logo",
        1 => "Customer",
        2 => "Company"
    ];

    public $sortable = ['name', 'template_id', 'status.description'];

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function template()
    {
        return self::TEMPLATES[$this->template_id]??0;
    }

    public function logo($index)
    {
        return self::LOGO[$index]??null;
    }
}
