<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountElement extends Model
{
    use HasFactory;

    protected $table = 'account_element';

    protected $primaryKey = 'id';

    protected $fillable = ['account_id', 'description', 'account_element', 'status', 'created_at', 'updated_at'];

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
