<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignmentStandardCost extends Model
{
    use HasFactory;

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class);
    }
}
