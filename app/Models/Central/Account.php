<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $table = 'account';

    protected $primaryKey = 'id';

    protected $fillable = ['account_type_id', 'description', 'account_group', 'status', 'creator_id', 'created_at', 'updated_at'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Central\Status::class, 'status');
    }

    public function account_type()
    {
        return $this->belongsTo(\App\Models\AccountType::class, 'account_type_id');
    }
}
