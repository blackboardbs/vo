<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResourcePosition extends Model
{
    use HasFactory;

    protected $table = 'resource_position';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
