<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailStatus extends Model
{
    use HasFactory;

    protected $table = 'avail_status';
}
