<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessRating extends Model
{
    use HasFactory;

    protected $table = 'business_rating';
}
