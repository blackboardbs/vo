<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CvCompany extends Model
{
    use HasFactory;

    protected $table = 'cv_company';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
