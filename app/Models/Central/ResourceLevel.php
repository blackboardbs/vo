<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResourceLevel extends Model
{
    use HasFactory;

    protected $table = 'resource_level';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
