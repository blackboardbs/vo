<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    use HasFactory;

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Central\Status::class, 'status');
    }
}
