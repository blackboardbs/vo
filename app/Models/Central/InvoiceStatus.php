<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
    use HasFactory;

    protected $table = 'invoice_status';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
