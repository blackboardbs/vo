<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrigin extends Model
{
    use HasFactory;

    protected $table = 'job_origin';
}
