<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentMasterDetail extends Model
{
    use HasFactory;

    public function assess_master()
    {
        return $this->belongsTo(AssessmentMaster::class, 'assessment_master_id');
    }
}
