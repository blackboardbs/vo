<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TechnicalRating extends Model
{
    use HasFactory;

    protected $table = 'technical_rating';
}
