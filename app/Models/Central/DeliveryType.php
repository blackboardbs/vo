<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryType extends Model
{
    use HasFactory;

    protected $table = "task_delivery_types";

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
