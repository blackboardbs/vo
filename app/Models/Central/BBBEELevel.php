<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BBBEELevel extends Model
{
    use HasFactory;

    protected $table = "bbbee_level";
}
