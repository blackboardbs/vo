<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorInvoiceStatus extends Model
{
    use HasFactory;

    protected $table = 'vendor_invoice_status';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
