<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessStatus extends Model
{
    use HasFactory;

    protected $table = 'process_status';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
