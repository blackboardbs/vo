<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardComponent extends Model
{
    use HasFactory;

    protected $table = "dynamic_dashboards";

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
