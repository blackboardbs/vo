<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetClass extends Model
{
    use HasFactory;

    protected $table = 'asset_class';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
