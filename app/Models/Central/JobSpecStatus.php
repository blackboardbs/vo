<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSpecStatus extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'job_spec_status';
}
