<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationTerm extends Model
{
    use HasFactory;

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }

    public function quotation()
    {
        return $this->belongsTo(\App\Models\Quotation::class, 'quotation_id');
    }

    public function terms()
    {
        return $this->belongsTo(\App\Models\Terms::class, 'terms_id');
    }
}
