<?php

namespace App\Models\Central;

use App\Models\Assignment;
use App\Models\Resource;
use App\Models\ResourceViewRole;
use App\Models\Team;
use App\Models\UserOnboarding;
use App\Models\UserType;
use App\Traits\HasImage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory;
    use HasRoles;
    use Sortable;
    use Notifiable;
    use HasImage;

    const ONBOARDING = [
        1 => 'Yes',
        0 => 'No',
    ];



    protected $table = 'users';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'first_name', 'last_name', 'email', 'phone', 'role_user', 'status_id', 'created_at', 'calendar_view'];

    protected $appends = ['onboarding_value'];



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'avatar', 'phone', 'company_id', 'login_user', 'status_id', 'resource_id', 'expiry_date', 'new_setup'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getOnboardingValueAttribute()
    {
        $onboarding = $this->onboarding ?? 0;

        return self::ONBOARDING[$onboarding];
    }

    public function name()
    {
        if ($this->last_name == '') {
            return $this->first_name;
        } else {
            return $this->first_name.' '.$this->last_name;
        }
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'resource_id');
    }

    public function timesheets()
    {
        return$this->hasMany(\App\Models\Timesheet::class, 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function vendor()
    {
        return $this->belongsTo(\App\Models\Vendor::class, 'vendor_id');
    }

    public function notifications()
    {
        return $this->hasManyThrough(\App\Models\Notification::class, \App\Models\UserNotification::class, 'user_id', 'id', 'id', 'notification_id')->orderBy('created_at', 'desc');
    }

    public function isUser()
    {
        return $this->hasRole('user');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function isConsultant()
    {
        return $this->hasRole('consultant');
    }

    public function is($roleName)
    {
        foreach ($this->roles()->get() as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }

        return false;
    }

    public function getPermissions($module_id)
    {
        $sql_query = 'SELECT sp.*
                        FROM simple_permissions AS sp
                        LEFT OUTER JOIN model_has_roles AS ru ON ru.role_id = sp.role_id
                        LEFT OUTER JOIN users AS u ON u.id = ru.model_id
                        LEFT OUTER JOIN modules as m on m.id = sp.module_id
                        WHERE u.id = '.$this->id.' AND m.id = '.$module_id??0;

        $permissions = DB::select($sql_query);

        return $permissions;
    }

    public function canAccess($module_id, $permission_type)
    {
        if ($this->roles()->whereId(1)->get()->isNotEmpty()){
            return true;
        }

        $permissions = $this->getPermissions($module_id);

        if (empty($permissions)) {
            return false;
        }

        $flag = false;

        //If user has multiple roles
        foreach ($permissions as $permission) {
            if ($permission_type == 'view_all') {
                if ($permission->list_all_records == 1 || $permission->list_all_records == 3 || $permission->list_all_records == 5 || $permission->list_all_records == 2) {
                    $flag = true;
                }
            }

            if ($permission_type == 'view_team') {
                if ($permission->list_own_and_team_records == 1 || $permission->list_own_and_team_records == 3 || $permission->list_own_and_team_records == 5 || $permission->list_own_and_team_records == 2) {
                    $flag = true;
                }
            }

            if ($permission_type == 'show_all') {
                if ($permission->show_all_records == 1 || $permission->show_all_records == 3 || $permission->show_all_records == 5 || $permission->show_all_records == 2) {
                    $flag = true;
                }
            }

            if ($permission_type == 'show_team') {
                if ($permission->show_own_and_team_records == 1 || $permission->show_own_and_team_records == 3 || $permission->show_own_and_team_records == 5 || $permission->show_own_and_team_records == 2) {
                    $flag = true;
                }
            }

            if ($permission_type == 'update_all') {
                if ($permission->show_all_records == 1 || $permission->show_all_records == 3 || $permission->show_all_records == 5) {
                    $flag = true;
                }
            }

            if ($permission_type == 'update_team') {
                if ($permission->show_own_and_team_records == 1 || $permission->show_own_and_team_records == 3 || $permission->show_own_and_team_records == 5) {
                    $flag = true;
                }
            }

            if ($permission_type == 'create_all') {
                if ($permission->show_all_records == 1 || $permission->show_all_records == 3 || $permission->show_all_records == 5) {
                    $flag = true;
                }
            }

            if ($permission_type == 'create_team') {
                if ($permission->show_own_and_team_records == 1 || $permission->show_own_and_team_records == 3 || $permission->show_own_and_team_records == 5) {
                    $flag = true;
                }
            }
        }

        return $flag;
    }


    /**
     * Get team if team manager else get user id
     */
    public function team(): Collection
    {
        $team_id = Team::where('team_manager', $this->id)->pluck('id')->toArray();
        if (empty($team_id)) {
            $team_ids = Resource::where('user_id', $this->id)->pluck('user_id');
        } else {
            $team_ids = Resource::where('team_id', $team_id)->orwhere('user_id', $this->id)->pluck('user_id');
        }

        return $team_ids;
    }

    public function teams()
    {
        $resource_manager_id = Resource::where('user_id', $this->id)->pluck('team_id');
        if ((int) $resource_manager_id[0] == 0) {
            $team_ids = Resource::where('user_id', $this->id)->pluck('user_id');
        } else {
            $team_ids = Resource::where('team_id', $resource_manager_id)->pluck('user_id');
        }

        return $team_ids;
    }

    public function scopeExcludes($query, array $roles)
    {
        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->whereNotIn('id', $roles);
        });
    }

    public function getViewRole($module_id, $resource_id, ?int $module_item_id = null)
    {
        $role = null;

        $resource_view_role = ResourceViewRole::where('resource_id', $resource_id)
            ->where('module_id', 30)
            ->when($module_item_id, fn($role) => $role->where('module_item_id', $module_item_id))
            ->first();

        if (isset($resource_view_role)) {
            $role = $resource_view_role->role_id;
        }

        return $role;
    }

    public function userType()
    {
        return $this->belongsTo(UserType::class, 'usertype_id');
    }

    public function appointmentManager()
    {
        return $this->belongsTo(self::class, 'appointment_manager_id');
    }

    public function userOnboarding()
    {
        return $this->hasOne(UserOnboarding::class, 'user_id');
    }

    public function assignments(){
        return $this->hasMany(Assignment::class, 'employee_id');
    }
    
    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($user) {
            $user->whereHas('assignments.project', function ($q){
                $q->where('company_id', request()->company);
            });
    
})->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '0' && request()->customer != '', function ($task) {
            $task->whereHas('assignments.project', function ($q){
                $q->where('customer_id', request()->customer);
            });
    })->when(isset(request()->company_id) && request()->company_id != '-1' && request()->company_id != '0' && request()->company_id != '', function ($user) {
        $user->whereHas('assignments.project', function ($q){
            $q->where('company_id', request()->company_id);
        })->orWhere('company_id',request()->company_id);
})->when(isset(request()->customer_id) && request()->customer_id != '-1' && request()->customer_id != '0' && request()->customer_id != '', function ($task) {
        $task->whereHas('assignments.project', function ($q){
            $q->where('customer_id', request()->customer_id);
        });
})->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($user) {
                $user->whereHas('assignments', function ($q){
                    $q->where('project_id',request()->project);
                });
            })->when(isset(request()->project_id) && request()->project_id != '-1' && request()->project_id != '0' && request()->project_id != '', function ($user) {
                $user->whereHas('assignments', function ($q){
                    $q->where('project_id',request()->project_id);
                });
    })->when(isset(request()->week) && request()->week != '-1' && request()->week != '0' && request()->week != '', function ($user) {
        $user->whereHas('timesheets', function ($q){
            $q->where('year_week',request()->week);
        });
})->orderBy('first_name')->orderBy('last_name');
    }
}
