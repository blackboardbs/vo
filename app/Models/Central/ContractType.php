<?php

namespace App\Models\Central;

use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    use HasFactory;

    public function Status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
