<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    use HasFactory;

    protected $table = 'cost_center';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }

    public function appr_name()
    {
        return $this->belongsTo(User::class, 'approver');
    }
}
