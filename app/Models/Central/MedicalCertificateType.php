<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalCertificateType extends Model
{
    use HasFactory;

    protected $table = 'medical_certificate_type';

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
