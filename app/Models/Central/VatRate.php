<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VatRate extends Model
{
    use HasFactory;

    protected $table = 'vat_rate';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
