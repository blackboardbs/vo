<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CFTSTemplate extends Model
{
    use HasFactory;

    protected $table = 'cfts_template';
}
