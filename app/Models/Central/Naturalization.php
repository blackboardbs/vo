<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Naturalization extends Model
{
    use HasFactory;

    protected $table = "naturalization";
}
