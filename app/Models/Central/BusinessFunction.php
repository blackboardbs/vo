<?php

namespace App\Models\Central;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessFunction extends Model
{
    use HasFactory;

    protected $table = 'business_function';

    public function statusd()
    {
        return $this->belongsTo(Status::class, 'status');
    }
}
