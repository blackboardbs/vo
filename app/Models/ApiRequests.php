<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiRequests extends Model
{
    use HasFactory;

    protected $table = 'api_requests';

    public function api_user()
    {
        return $this->belongsTo(User::class, 'user');
    }
}
