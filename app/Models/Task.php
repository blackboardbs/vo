<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kyslik\ColumnSortable\Sortable;

use Illuminate\Support\Facades\DB;
class Task extends Model
{
    use Sortable;

    protected $table = 'tasks';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'task_user', 'task_number', 'subject', 'description', 'due_date', 'status_id'];

    protected $appends = ['display_on_kanban'];

    const KANBAN_DISPLAY = [
        0 => 'No',
        1 => 'Yes',
    ];

    public function getDisplayOnKanbanAttribute()
    {
        return self::KANBAN_DISPLAY[$this->on_kanban] ?? null;
    }

    public function taskstatus()
    {
        return $this->belongsTo(\App\Models\TaskStatus::class, 'status');
    }
    
    public function tasknotes()
    {
        return $this->hasMany(\App\Models\TaskNote::class, 'task_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'task_user');
    }

    public function consultant()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function invoicestatus()
    {
        return $this->belongsTo(\App\Models\InvoiceStatus::class, 'invoice_status');
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\Task::class, 'parent_activity_id');
    }

    public function taskdependency()
    {
        return $this->belongsTo(\App\Models\Task::class, 'dependency');
    }

    public function tasktype()
    {
        return $this->belongsTo(\App\Models\TaskType::class, 'task_type_id');
    }

    public function childs()
    {
        return $this->hasMany(\App\Models\Task::class, 'parent_activity_id', 'id');
    }
    
    public function actions()
    {
        return $this->hasMany(\App\Models\Action::class, 'related_task_id', 'id');
    }

    public function user_story()
    {
        return $this->belongsTo(\App\Models\UserStory::class, 'user_story_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id');
    }

    public function timelines()
    {
        return $this->hasMany(Timeline::class, 'task_id');
    }

    public function bugTask()
    {
        return $this->hasOne(BugTask::class);
    }
    
    public function bug_task()
    {
        return $this->hasOne(BugTask::class);
    }
    

    public function getActualTime($task_id)
    {
        $timelines = \App\Models\Timeline::where('task_id', $task_id)->get();

        $totalHours = $timelines->sum('total');
        $totalMinutes = $timelines->sum('total_m');

        $minutes = ($totalHours * 60) + $totalMinutes;

        $actualTime = $minutes;

        return $actualTime;
    }

    public function minutesToTime($_minutes)
    {
        $hours = floor($_minutes / 60);
        $minutes = $_minutes % 60;
        $negation_number = '';
        /*if($hours < 0){
            $hours *= -1;
            $negation_number = '-';
        }

        if($minutes < 0){
            $minutes *= -1;
            $negation_number = '-';
        }*/

        if ($_minutes < 0) {
            $hours = floor(abs($_minutes) / 60);
            $minutes = abs($_minutes) % 60;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }

    public function scopeWhereProject($query, $project)
    {
        return $query->where(function ($query) use ($project) {
            if ($project == 0) {
                return $query->where('project_id', '>', 0);
            }

            return $query->where('project_id', $project);
        });
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->resource) && request()->resource != '-1' && request()->resource != '' && request()->resource != 0, function ($task) {
            return $task->where('employee_id', request()->resource);
        })->unless(isset(request()->resource) && request()->resource != '-1' && request()->resource != '' && request()->resource != 0, function ($task) {
            $roles = auth()->user()->roles->map(function ($role) {
                return $role->display_name;
            })->toArray();
            if (! in_array('System Administrator', $roles) && ! in_array('Manager', $roles)) {
                $task->where('employee_id', auth()->id());
            }
        })->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '' && request()->employee != 0, function ($task) {
            return $task->where('employee_id', request()->resource);
        })
            ->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
                $task->where('customer_id', request()->customer);
            })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
                $task->whereHas('project.company', function ($t) {
                    $t->where('id', '=', request()->company);
                });
            })->when(request()->start_date, function ($task) {
                $task->where('start_date', '>=', request()->start_date);
            })->when(request()->end_date, function ($task) {
                $task->where('end_date', '<=', request()->end_date);
            })->when(request()->task_id, function ($task) {
                $task->where('id', 'like', '%'.request()->task_id.'%');
            })->when(request()->sprint && request()->sprint != '-1', function ($task) {
                $task->where('sprint_id', request()->sprint)
                    ->orWhereHas('user_story', function ($story) {
                        $story->where('sprint_id', request()->sprint);
                    });
            })->when(request()->sprint && request()->sprint == '-1', function ($task) {
                $task->where(DB::raw('COALESCE(sprint_id,0)'), 0);
            })->when(request('status'), function ($task) {
                if (gettype(request('status')) == 'object') {
                    return $task->where('status', request('status')->id);
                }

                return $task->where('status', request()->status);
            })->when(request()->team, function ($task) {
                $task->whereHas('consultant.resource', function ($t) {
                    $t->where('team_id', '=', request()->team);
                });
            })->when(request()->project, function ($task) {
                // Don't uncomment, breaks kanban
                // if(isset(request()->project) && request()->project != '-1' && request()->project != '' && request()->project != 0){
                //     $task->where('project_id', '=', request()->project);
                // }
            })->when(request()->on_kanban, function ($task) {
                if(request()->on_kanban != '-1'){
                    $task->where('on_kanban', request()->on_kanban);
                }
            })->when(request()->task_tag, function ($task) {
                if(request()->task_tag != '-1'){
                    $task->where('emote_id', request()->task_tag);
                }
            })->when(request()->bugs !== null && request()->bugs !='-1', function ($task) {    
                if(request()->bugs =='1'){        
                    // If bugs is '1', return tasks that have a related bugTask
                    $task->whereHas('bugTask');
                } else {        
                    // If bugs is '0', return tasks that do not have a related bugTask
                    $task->doesntHave('bugTask');
                } 
            })->when(request()->priority !== null && request()->priority, function ($task) {
                if(request()->priority != '-1'){
                    $task->where('priority_level',request()->priority)->orWhereHas('bugTask', function ($t){
                        $t->where('priority_level',request()->priority);
                    });
                }
            });
    }

    public function scopeFilters2($query)
    {
        return $query->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '' && request()->employee != 0, function ($task) {
            return $task->where('employee_id', request()->resource);
        })
            ->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
                $task->where('customer_id', request()->customer);
            })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
                $task->whereHas('project.company', function ($t) {
                    $t->where('id', '=', request()->company);
                });
            })->when(request()->project, function ($task) {
                if(isset(request()->project) && request()->project != '-1' && request()->project != '' && request()->project != 0){
                    $task->where('project_id', '=', request()->project);
                }
            });
    }

    public function goal(): BelongsTo
    {
        return $this->belongsTo(Goal::class);
    }

    public function sprint(): BelongsTo
    {
        return $this->belongsTo(Sprint::class);
    }
}
