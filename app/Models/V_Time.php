<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class V_Time extends Model
{
    use Sortable;

    protected $table = 'v_time';

    public $sortable = ['id', 'customer_id', 'project_id', 'yearwk', 'employee_id'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function invoice_status()
    {
        return $this->belongsTo(\App\Models\InvoiceStatus::class, 'bill_status');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function time()
    {
        return $this->belongsTo(\App\Models\Time::class, 'time_id');
    }

    public function systemd()
    {
        return $this->belongsTo(\App\Models\System::class, 'system');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function countryd()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country');
    }
}
