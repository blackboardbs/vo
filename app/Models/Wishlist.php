<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
    use SoftDeletes;

    protected $casts = [
        'sent_at' => 'datetime',
    ];

    public function setSentAtAttribute($sent_at)
    {
        $this->attributes['sent_at'] = Carbon::parse($sent_at);
    }
}
