<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanUtilization extends Model
{
    protected $table = 'plan_utilization';

    protected $fillable = ['yearwk', 'res_no', 'wk_hours', 'cost_res_no', 'cost_wk_hours', 'status_id', 'company_id'];
}
