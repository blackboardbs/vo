<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Anniversary extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'anniversary';

    public $sortable = ['id', 'anniversary', 'emp_id', 'name', 'status_id', 'anniversary_date', 'relationship'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function anniversary_type()
    {
        return $this->belongsTo(\App\Models\AnniversaryType::class, 'anniversary');
    }

    public function relationshipd()
    {
        return $this->belongsTo(\App\Models\Relationship::class, 'relationship');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'emp_id');
    }
}
