<?php

namespace App\Models;

use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Kyslik\ColumnSortable\Sortable;

class AssetRegister extends Model
{
    use Sortable;
    use TemplateTrait;

    protected $table = 'asset_register';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'asset_nr', 'make', 'model', 'issued_to', 'aquire_date', 'retire_date', 'status_id'];

    protected function soldValue(): Attribute
    {
        return Attribute::make(
            get: fn (int|null $value) => $value/100,
            set: fn (int|float|null $value) => $value*100,
        );
    }

    protected function writtenOffValue(): Attribute
    {
        return Attribute::make(
            get: fn (int|null $value) => $value/100,
            set: fn (int|float|null $value) => $value*100,
        );
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'issued_to');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function assetClass()
    {
        return $this->belongsTo(\App\Models\AssetClass::class, 'asset_class');
    }

    public function insurance(): HasOne
    {
        return $this->hasOne(Insurance::class, 'asset_register_id');
    }

    public function assetConditionNotes(): HasMany
    {
        return $this->hasMany(AssetConditionNote::class, 'asset_id');
    }

    public function assetHistory(): HasMany
    {
        return $this->hasMany(AssetHistory::class, 'asset_id')->latest();
    }

    public function assetHistoryActive(): HasOne
    {
        return $this->hasOne(AssetHistory::class, 'asset_id')
            ->where('is_active', 1)->latest();
    }

    public function issuedTo(): BelongsTo
    {
        return $this->belongsTo(User::class, 'issued_to');
    }

    public function additionalCost(): HasMany
    {
        return $this->hasMany(AssetAdditionalCost::class, 'asset_id')->latest();
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'template_id');
    }
}
