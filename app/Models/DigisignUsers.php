<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DigisignUsers extends Model
{
    protected $table = 'digisign_users';
    protected $primaryKey = 'id';
}
