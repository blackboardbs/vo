<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CvSubmit extends Model
{
    use HasFactory;

    public function jobSpec()
    {
        return $this->belongsTo(\App\Models\JobSpec::class, 'job_spec_id');
    }
}
