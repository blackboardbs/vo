<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceViewRole extends Model
{
    use SoftDeletes;

    public function resource(): BelongsTo
    {
        return $this->belongsTo(User::class, 'resource_id');
    }

    public function moduleRole(): BelongsTo
    {
        return $this->belongsTo(ModuleRole::class, 'role_id');
    }
}
