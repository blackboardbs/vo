<?php

namespace App\Models;

use App\Traits\TaskTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsightTaskList extends Model
{
    use HasFactory;
    use TaskTrait;

    protected $table = 'task_view';
}
