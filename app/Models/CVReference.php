<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CVReference extends Model
{
    protected $table = 'cv_references';
    use SoftDeletes;
}
