<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackOwnership extends Model
{
    protected $table = 'black_ownership';
    protected $primaryKey = 'id';

}