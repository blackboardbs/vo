<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class BillingPeriodStyle extends Model
{
    use SoftDeletes;
    use Sortable;

    const TEMPLATES = [
        1 => "Grid Template",
        2 => "Horizontal Template",
        3 => "Vertical Template",
        4 => "Monthly Template"
    ];

    const LOGO = [
        null => "Select Logo",
        1 => "Customer",
        2 => "Company"
    ];

    public $sortable = ['name', 'template_id', 'status.description'];

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function template()
    {
        return self::TEMPLATES[$this->template_id]??0;
    }

    public function logo($index)
    {
        return self::LOGO[$index]??null;
    }
}
