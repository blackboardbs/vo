<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Commission extends Model
{
    //
    use Sortable;

    protected $table = 'commission';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'min_rate', 'status_id'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
