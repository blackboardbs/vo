<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Logbook extends Model
{
    use Sortable;

    protected $table = 'logbook';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'log_date', 'description', 'open_reading', 'km', 'fuel_cost', 'maint_cost', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'emp_id');
    }
}
