<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentMeasureScore extends Model
{
    public function assessment_master()
    {
        return $this->belongsTo(\App\Models\AssessmentMaster::class, 'assessment_master_id');
    }

    public function assessment_master_details()
    {
        return $this->belongsTo(\App\Models\AssessmentMasterDetails::class, 'assessment_master_detail_id');
    }

    public function assessment()
    {
        return $this->belongsTo(\App\Models\AssessmentHeader::class, 'assessment_id');
    }
}
