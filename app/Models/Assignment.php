<?php

namespace App\Models;

use App\Enum\YesOrNoEnum;
use App\Traits\AssignmentTrait;
use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Kyslik\ColumnSortable\Sortable;

class Assignment extends Model
{
    use Sortable;
    use AssignmentTrait;
    use TemplateTrait;
    use HasFactory;

    protected $table = 'assignment';

    public $sortable = ['id', 'project_id', 'hours', 'employee_id', 'start_date', 'end_date', 'status'];

    protected $casts = [
        'billable' => YesOrNoEnum::class,
        'project_manager_ts_approver' => YesOrNoEnum::class,
        'product_owner_ts_approver' => YesOrNoEnum::class,
        'project_manager_new_ts_approver' => YesOrNoEnum::class,
        'line_manager_ts_approver' => YesOrNoEnum::class,
        'claim_approver_ts_approver' => YesOrNoEnum::class,
        'resource_manager_ts_approver' => YesOrNoEnum::class
    ];

    public function resource(): BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function consultant(): BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function project(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function statusd(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function project_status(): BelongsTo
    {
        return $this->belongsTo(\App\Models\ProjectStatus::class, 'assignment_status');
    }

    public function function_desc(): BelongsTo
    {
        return $this->belongsTo(\App\Models\BusinessFunction::class, 'function');
    }

    public function system_desc(): BelongsTo
    {
        return $this->belongsTo(\App\Models\System::class, 'system');
    }

    public function timeline(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Timeline::class, 'project_id', 'wbs_id');
    }

    public function resource_user(): BelongsTo
    {
        return $this->belongsTo(Resource::class, 'employee_id', 'user_id');
    }

    public function location(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    public function product_owner(): BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'product_owner_id');
    }

    public function claim_approver(): BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'claim_approver_id');
    }

    public function internalOwner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function productOwner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'product_owner_id');
    }

    public function projectManager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'project_manager_new_id');
    }

    public function lineManager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'line_manager_id');
    }

    public function claimApprover(): BelongsTo
    {
        return $this->belongsTo(User::class, 'assignment_approver');
    }

    public function resourceManager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'resource_manager_id');
    }

    public function copyFromUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'copy_from_user');
    }

    public function vendorTemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'vendor_template_id');
    }

    public function resourceTemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'resource_template_id');
    }

    public function assignmentCosts(): HasMany
    {
        return $this->hasMany(AssignmentCost::class, 'ass_id');
    }

    public function expense(): BelongsTo
    {
        return $this->belongsTo(Expense::class, 'exp_id');
    }

    public function expense_tracking(): HasMany
    {
        return $this->hasMany(ExpenseTracking::class, 'assignment_id');
    }

    public function extensions(): HasMany
    {
        return $this->hasMany(AssignmentExtension::class, 'assignment_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }

    public function scopeFilters($query)
    {
        return $query->when(request()->project, function ($ass) {
            if(request()->project != '-1'){
                $ass->where('project_id', request()->project);
            }
        })->when(request()->customer, function ($ass) {
            if(request()->customer != '-1'){
                $ass->whereHas('project',function ($q){
                    $q->where('customer_id',request()->customer);
                });
            }
        })->when(request()->company, function ($ass) {
            if(request()->company != '-1'){
                $ass->whereHas('project',function ($q){
                    $q->where('company_id',request()->company);
                });
            }
        });
    }
}
