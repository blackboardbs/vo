<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CFTS extends Model
{
    use Sortable;
    use SoftDeletes;

    protected $table = 'cfts';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'text', 'start_date', 'customer_id', 'status_id', 'created_at'];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function template()
    {
        return $this->belongsTo(\App\Models\CFTSTemplate::class, 'template_id');
    }
}
