<?php

namespace App\Models;

use App\Traits\HasImage;
use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kyslik\ColumnSortable\Sortable;

class Vendor extends Model
{
    use Sortable;
    use HasImage;
    use HasFactory;
    use TemplateTrait;

    const ONBORDING = [
        1 => 'Yes',
        0 => 'No',
    ];

    protected $table = 'vendor';

    protected $primaryKey = 'id';

    public $sortable = ['vendor_name', 'contact_firstname', 'phone', 'cell', 'email', 'status_id', 'created_at'];

    protected $appends = ['onboarding_vendor'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function account_managerd()
    {
        return $this->belongsTo(\App\Models\User::class, 'account_manager');
    }

    
    public function invoice_contact()
    {
        return $this->belongsTo(\App\Models\InvoiceContact::class, 'invoice_contact_id');
    }

    public function assignment_approverd()
    {
        return $this->belongsTo(\App\Models\User::class, 'assignment_approver');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    public function bbbee_leveld()
    {
        return $this->belongsTo(\App\Models\BBBEELevel::class, 'bbbee_level');
    }

    public function bankAccountType()
    {
        return $this->belongsTo(\App\Models\BankAccountType::class, 'bank_account_type_id');
    }

    public function invoiceContact()
    {
        return $this->belongsTo(InvoiceContact::class, 'invoice_contact_id');
    }

    public function supplyChainAppointmentManager()
    {
        return $this->belongsTo(User::class, 'supply_chain_appointment_manager_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'vendor_id', 'id');
    }
    
    public function invoices()
    {
        return $this->hasMany(VendorInvoice::class, 'vendor_id', 'id');
    }

    public function getOnboardingVendorAttribute()
    {
        $onboarding = $this->onboarding ?? 0;

        return self::ONBORDING[$onboarding];
    }

    public function serviceAgreement(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'service_agreement_id');
    }

    public function NDATemplate(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'nda_template_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }
}
