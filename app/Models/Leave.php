<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Leave extends Model
{
    use Sortable;

    protected $table = 'leave';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'emp_id', 'leave_type_id', 'date_from', 'date_to', 'no_of_days', 'status_id'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }//

    public function leave_type()
    {
        return $this->belongsTo(\App\Models\LeaveType::class, 'leave_type_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'emp_id');
    }
}
