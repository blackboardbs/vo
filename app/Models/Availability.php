<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $table = 'availability';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function avail_statusd()
    {
        return $this->belongsTo(\App\Models\AvailStatus::class, 'avail_status');
    }
}
