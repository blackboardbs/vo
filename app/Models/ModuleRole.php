<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use \App\Models\StorePermission;

class ModuleRole extends Model
{
    use SoftDeletes, Sortable;

    public function getPermission($module_id, $module_role_id, $module_section_id)
    {
        $permission = null;

        $storePermission = StorePermission::where('module_id', $module_id)->where('module_role_id', $module_role_id)->where('module_section_id', $module_section_id)->first();

        if(isset($storePermission)){
            $permission = $storePermission->permission_id;
        }

        return $permission;
    }
}
