<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Spatie\Permission\Models\Role;

class UserFavourite extends Model
{
    use Sortable;

    public $sortable = ['module_name', 'button_functions', 'route'];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function favourites()
    {
        return $this->belongsTo(\App\Models\Favourite::class, 'favourite_id');
    }
}
