<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;

class DynamicForm extends Model
{

    /*
     * @param $look_up_id, the look up id
     * @return $parameters, the parameters to be passed to the view file
     * */
    public function list($look_up_id)
    {
        $look_up = LookUp::where('model', '=', 'LookUp')->first();
        $look_up_model_name = 'App\LookUp';

        if($look_up_id != null){
            $look_up = LookUp::find($look_up_id);
            $look_up_model_name = 'App\\'.$look_up->model;
        }

        $model = new $look_up_model_name;
        $table = $model->getTable();

        $columns = DB::select("describe " . $table);

        $exclude_columns = [];
        $exclude_default_columns = ['id', 'created_at', 'updated_at', 'deleted_at', 'creator_id'];
        $exclude_default_columns = array_merge($exclude_default_columns, $exclude_columns);

        $counter = 0;
        $relations = [];

        foreach ($columns as $column) {

            if (!in_array($column->Field, $exclude_default_columns)) {
                $label = '';
                $label_parts = explode('_', $column->Field);

                foreach ($label_parts as $label_part):
                    if ($label_part != 'id') {
                        $label .= ' ' . ucwords($label_part);
                    }
                endforeach;

                $type = explode('(', $column->Type);
                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($label_parts);
                        if ($label_parts[$parts_size - 1] == 'id') {
                            $form_fields[$counter]['type'] = 'look_up';
                            $Look_up_model_name = '';
                            foreach ($label_parts as $label_part):
                                if ($label_part != 'id') {
                                    $Look_up_model_name .= ucwords($label_part);
                                }
                            endforeach;
                            $Look_up_model = 'App\\' . $Look_up_model_name;
                            $relations[$column->Field] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->toArray();
                        } else {
                            $form_fields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $form_fields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $form_fields[$counter]['type'] = 'text';
                        break;
                }

                $form_fields[$counter]['label'] = $label;
                $form_fields[$counter]['field'] = $column->Field;

            }

            $counter++;

        }

        $look_up_values = $look_up_model_name::orderBy('name')->get();

        $parameters = [
            'look_up_values' => $look_up_values,
            'look_up' => $look_up,
            'form_fields' => $form_fields,
            'relations' => $relations
        ];

        return $parameters;
    }

    /*
     * @param $look_up_id, the look up table id
     * @param $model_id, if edit or show give the data model id
     * @param $exclude_columns, columns to be excluded by the form builder
     * @return $parameters, parameters to be passed to the view
     * */
    public function createDynamicForm($look_up_id, $model_id = null, $exclude_columns = [])
    {

        $look_up = LookUp::where('model', '=', 'LookUp')->first();

        if ($look_up_id > 0) {
            $look_up = LookUp::find($look_up_id);
        }

        $model_name = 'App\\' . $look_up->model;

        $model = new $model_name;
        $table = $model->getTable();

        $columns = DB::select("describe " . $table);

        $exclude_default_columns = ['id', 'created_at', 'updated_at', 'deleted_at', 'creator_id'];
        $exclude_default_columns = array_merge($exclude_default_columns, $exclude_columns);

        $counter = 0;
        $form_fields = [];

        foreach ($columns as $column) {

            if (!in_array($column->Field, $exclude_default_columns)) {
                $label = '';
                $label_parts = explode('_', $column->Field);

                foreach ($label_parts as $label_part):
                    if ($label_part != 'id') {
                        $label .= ' ' . ucwords($label_part);
                    }
                endforeach;

                $type = explode('(', $column->Type);
                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($label_parts);
                        if ($label_parts[$parts_size - 1] == 'id') {
                            $form_fields[$counter]['type'] = 'look_up';
                            $Look_up_model_name = '';
                            foreach ($label_parts as $label_part):
                                if ($label_part != 'id') {
                                    $Look_up_model_name .= ucwords($label_part);
                                }
                            endforeach;
                            $Look_up_model = 'App\\' . $Look_up_model_name;
                            $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
                        } else {
                            $form_fields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $form_fields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $form_fields[$counter]['type'] = 'text';
                        break;
                }

                $form_fields[$counter]['label'] = $label;
                $form_fields[$counter]['field'] = $column->Field;

            }

            $counter++;

        }

        $parameters = [
            'look_up' => $look_up,
            'form_fields' => $form_fields,
            'exclude_columns' => $exclude_columns
        ];

        if($model_id != null && $model_id > 0){
            $parameters['model_id'] = $model_id;
        }

        return $parameters;

    }

    /*
     * @param $look_up_id, the look up table id
     * @param $model_id, if edit or show give the data model id
     * @param $exclude_columns, columns to be excluded by the form builder
     * @return true if form successfully saved, else return false
     * */
    public function storeDynamicForm($look_up_id, Request $request, $validate = true){

        $look_up = LookUp::find($look_up_id);

        $look_up_class = 'App\\'.$look_up->model;
        $request_class_name = 'App\Http\Requests\\'.$look_up->request;

        $request_class = null;

        if (class_exists($request_class_name) && $validate) {

            $request_class = new $request_class_name;

            if(!$request_class->authorize()){
                return abort(403);
            }
            //Validate form if request class set
            $request->validate($request_class->rules());

        }

        $result['status'] = true;
        $result['message'] = $look_up->model.' successfully saved.';

        //The look up being store is the only class Look up, which holds all the look up tables
        if($look_up->model == 'LookUp' && !class_exists('App\\'.$request->input('model'))){

            $result['status'] = false;
            $result['message'] = 'Class App\\'.$request->input('name').' does not exist, please create the model first before creating this record.';

        }
        else{

            $model = new $look_up_class();

            foreach($request->request as $key => $value){
                if(!in_array($key, ['_method', '_token', 'model_id', 'look_up_id'])){
                    $model->$key = $value;
                }
            }

            $model->creator_id = Auth()->id();
            $model->save();

        }

        $result['look_up'] = $look_up;

        return $result;

    }

    /*
     * @param $look_up_id, the look up table id
     * @param $model_id, if edit or show give the data model id
     * @param $exclude_columns, columns to be excluded by the form builder
     * @return $parameters, parameters to be passed to the view
     * */
    public function editDynamicForm($look_up_id, $model_id = null, $exclude_columns = []){

        $look_up = LookUp::find($look_up_id);

        $model_name = 'App\\' . $look_up->model;

        $model = $model_name::find($model_id);

        $new_model = new $model_name;

        $table = $new_model->getTable();

        $columns = DB::select("describe " . $table);

        $exclude_default_columns = ['id', 'created_at', 'updated_at', 'deleted_at', 'creator_id'];
        $exclude_default_columns = array_merge($exclude_default_columns, $exclude_columns);

        $counter = 0;
        $form_fields = [];

        foreach ($columns as $column) {

            if (!in_array($column->Field, $exclude_default_columns)) {
                $label = '';
                $label_parts = explode('_', $column->Field);

                foreach ($label_parts as $label_part):
                    if ($label_part != 'id') {
                        $label .= ' ' . ucwords($label_part);
                    }
                endforeach;

                $type = explode('(', $column->Type);
                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($label_parts);
                        if ($label_parts[$parts_size - 1] == 'id') {
                            $form_fields[$counter]['type'] = 'look_up';
                            $Look_up_model_name = '';
                            foreach ($label_parts as $label_part):
                                if ($label_part != 'id') {
                                    $Look_up_model_name .= ucwords($label_part);
                                }
                            endforeach;
                            $Look_up_model = 'App\\' . $Look_up_model_name;
                            $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
                        } else {
                            $form_fields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $form_fields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $form_fields[$counter]['type'] = 'text';
                        break;
                }

                $form_fields[$counter]['label'] = $label;
                $form_fields[$counter]['field'] = $column->Field;

            }

            $counter++;

        }

        $parameters = [
            'model' => $model,
            'look_up' => $look_up,
            'form_fields' => $form_fields,
            'exclude_columns' => $exclude_columns
        ];

        return $parameters;

    }

    /*
     * @param $look_up_id, the look up table id
     * @param $model_id, if edit or show give the data model id
     * @param $exclude_columns, columns to be excluded by the form builder
     * @return true if form successfully saved, else return false
     * */
    public function updateDynamicForm($look_up_id, $model_id, Request $request, $validate = true){

        $look_up = LookUp::find($look_up_id);

        $look_up_class = 'App\\'.$look_up->model;
        $request_class_name = 'App\Http\Requests\\'.$look_up->request;

        $request_class = null;

        if (class_exists($request_class_name) && $validate) {

            $request_class = new $request_class_name;

            if(!$request_class->authorize()){
                return abort(403);
            }
            //Validate form if request class set
            $request->validate($request_class->rules());

        }

        $model = $look_up_class::find($model_id);

        foreach($request->request as $key => $value){
            if(!in_array($key, ['_method', '_token', 'model_id', 'look_up_id'])){
                $model->$key = $value;
            }
        }

        $model->save();

        return $look_up;

    }

}
