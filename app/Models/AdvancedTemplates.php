<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Kyslik\ColumnSortable\Sortable;

class AdvancedTemplates extends Model
{
    use Sortable;

    protected $table = 'advanced_templates';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name', 'file'];

    protected $fillable = ['template_type_id', 'name', 'template_body'];

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function type()
    {
        return pathinfo($this->file, PATHINFO_EXTENSION);
    }

    public function size()
    {
        try {
            $bytes = Storage::size('templates/'.$this->file);
            $i = floor(log($bytes, 1024));

            return round($bytes / pow(1024, $i), [0, 0, 2, 2, 3][$i]).['B', 'kB', 'MB', 'GB', 'TB'][$i];
        } catch (\Exception $e) {
            return 'LStat';
        }
    }

    public function temptype()
    {
        return $this->belongsTo(\App\Models\TemplateType::class, 'template_type_id');
    }
}
