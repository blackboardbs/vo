<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    use SoftDeletes;

    public function epic(){
        return $this->belongsTo(Epic::class, 'epic_id');
    }

    public function user_stories(){
        return $this->hasMany(UserStory::class, 'feature_id');
    }

    public function sprint(){
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function status(){
        return $this->belongsTo(TaskStatus::class, 'status_id');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($user) {
            $user->whereHas('epic.project', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '0' && request()->customer != '', function ($task) {
                $task->whereHas('epic.project', function ($q){
                    $q->where('customer_id', request()->customer);
                });
        })->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($user) {
                $user->whereHas('epic.project', function ($q){
                    $q->where('project_id',request()->project);
                });
            })->when(isset(request()->project_id) && request()->project_id != '-1' && request()->project_id != '0' && request()->project_id != '', function ($user) {
                $user->whereHas('epic.project', function ($q){
                    $q->where('project_id',request()->project_id);
                });
        })->when(isset(request()->epic) && request()->epic != '-1' && request()->epic != '0' && request()->epic != '', function ($user) {
                $user->where('epic_id',request()->epic);
        });
    }
}
