<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ProcessStatus extends Model
{
    use SoftDeletes;
    use Sortable;

    public $sortable = ['id', 'name'];

    protected $table = 'process_status';

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
