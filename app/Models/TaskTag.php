<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class TaskTag extends Model
{
    use HasFactory;

    use Sortable;

    protected $table = 'task_tags';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public $sortable = ['id', 'name', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
