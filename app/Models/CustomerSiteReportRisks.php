<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerSiteReportRisks extends Model
{
    use HasFactory;
    
    protected $table = 'customer_site_report_risks';
}
