<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleSettings extends Model
{
    public function module()
    {
        return $this->belongsTo(\App\Models\Module::class, 'module_id');
    }
}
