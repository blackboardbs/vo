<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Account extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'account';

    protected $primaryKey = 'id';

    protected $fillable = ['account_type_id', 'description', 'account_group', 'status', 'creator_id', 'created_at', 'updated_at'];

    public $sortable = ['id', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function account_type()
    {
        return $this->belongsTo(\App\Models\AccountType::class, 'account_type_id');
    }
}
