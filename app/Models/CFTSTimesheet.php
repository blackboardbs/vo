<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CFTSTimesheet extends Model
{
    use SoftDeletes;

    protected $table = 'cfts_timesheet';

    protected $primaryKey = 'id';
}
