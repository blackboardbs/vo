<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerSiteReportHoursSummary extends Model
{
    use HasFactory;
    
    protected $table = 'customer_site_report_hours_summary';
}
