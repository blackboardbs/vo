<?php

namespace App\Models;

use App\Enum\YesOrNoEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssetHistory extends Model
{
    use HasFactory;

    protected $fillable = ['asset_id', 'user_id', 'is_active', 'note', 'issued_at', 'is_included_in_letter'];

    protected $casts = ['is_included_in_letter' => YesOrNoEnum::class];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function asset(): BelongsTo
    {
        return $this->belongsTo(AssetRegister::class, 'asset_id');
    }
}
