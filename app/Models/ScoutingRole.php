<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ScoutingRole extends Model
{
    use SoftDeletes;
    use Sortable;

    public $sortable = ['id', 'name', 'status_id'];

    protected $table = 'scouting_role';

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
