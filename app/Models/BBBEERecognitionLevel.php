<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BBBEERecognitionLevel extends Model
{
    protected $table = 'bbbee_recognition_level';
    protected $primaryKey = 'id';

}