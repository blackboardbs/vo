<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $table = 'pages';

    protected $primaryKey = 'id';

    public $fillable = ['title', 'url', 'user_id', 'icon'];

    public function favoritedBy() {
        return $this->belongsToMany(User::class, 'favorites', 'page_id', 'user_id')->withTimestamps();
    }
    
}
