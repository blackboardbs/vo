<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackVotingRights extends Model
{
    protected $table = 'black_voting_rights';
    protected $primaryKey = 'id';

}