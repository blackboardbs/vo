<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class QuotationTerms extends Model
{
    use Sortable;

    protected $table = 'quotation_terms';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'quotation_id', 'terms_id', 'term', 'row_order', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function quotation()
    {
        return $this->belongsTo(\App\Models\Quotation::class, 'quotation_id');
    }

    public function terms()
    {
        return $this->belongsTo(\App\Models\Terms::class, 'terms_id');
    }
}
