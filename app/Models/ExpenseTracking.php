<?php

namespace App\Models;

use App\Enum\YesOrNoEnum;
use App\Traits\HasImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ExpenseTracking extends Model
{
    use HasFactory;
    use Sortable;
    use HasImage;

    protected $table = 'exp_tracking';

    protected $casts = [
        'claimable' => YesOrNoEnum::class,
        'billable' => YesOrNoEnum::class
    ];

    protected $guarded = [];

    protected $primaryKey = 'id';

    public $sortable = ['id', 'employee_id', 'transaction_date', 'account_id', 'account_element_id', 'status_id', 'description', 'assignment_id', 'yearwk', 'payment_reference', 'documentt', 'amount'];

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function actual_resource()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'employee_id', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id');
    }

    public function account_element()
    {
        return $this->belongsTo(\App\Models\AccountElement::class, 'account_element_id');
    }

    public function assignment()
    {
        return $this->belongsTo(\App\Models\Assignment::class, 'assignment_id');
    }

    public function approver()
    {
        return $this->belongsTo(\App\Models\User::class, 'approved_by');
    }

    public function cost_center_description()
    {
        return $this->belongsTo(\App\Models\CostCenter::class, 'cost_center');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function expense_type()
    {
        return $this->belongsTo(ExpenseType::class, 'expense_type_id');
    }
}
