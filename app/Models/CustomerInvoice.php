<?php

namespace App\Models;

use App\Enum\InvoiceType;
use App\Traits\InvoiceTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Kyslik\ColumnSortable\Sortable;

class CustomerInvoice extends Model
{
    use Sortable;
    use InvoiceTrait;

    protected $table = 'customer_invoices';

    public $sortable = ['id', 'ar_invoice_number', 'inv_ref', 'paid_date', 'due_date', 'invoice_value', 'bill_status', 'customer_id', 'invoice_date', 'created_at'];

    protected function conversionRate(): Attribute
    {
        return Attribute::make(
            get: fn (int|null $value) => isset($value)?($value/100):1.00,
            set: fn (string|null $value) => isset($value)?((1/$value)*100):0.01,
        );
    }

    public function invoice_status()
    {
        return $this->belongsTo(\App\Models\VendorInvoiceStatus::class, 'bill_status');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function timesheet()
    {
        return $this->belongsTo(\App\Models\Timesheet::class, 'timesheet_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function resource()
    {
        return $this->hasMany(\App\Models\CustomerInvoiceLine::class, 'customer_invoice_number');
    }

    public function nonTimesheetCustomerInvoices()
    {
        return $this->hasMany(InvoiceItems::class, 'invoice_id')->where('invoice_type_id', 1);
    }

    public function nonTimesheetVendorInvoices()
    {
        return $this->hasMany(InvoiceItems::class, 'invoice_id')->where('invoice_type_id', 2);
    }

    public function vatRate(): BelongsTo
    {
        return $this->belongsTo(VatRate::class, 'vat_rate_id');
    }

    public function recurring(): MorphOne
    {
        return $this->morphOne(Recurring::class, 'recurrable');
    }

    public function invoiceLines(): HasMany
    {
        return $this->hasMany(CustomerInvoiceLine::class, 'customer_invoice_number');
    }

    public function histories()
    {
        return $this->belongsToMany(self::class, 'recurring_invoice_histories', 'recurring_invoice_id', 'non_timesheet_invoice_id')
            ->where('type', InvoiceType::Customer->value)->withTimestamps();
    }
}
