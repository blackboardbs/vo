<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Module extends Model
{
    use Sortable;

    public $sortable = ['id', 'name', 'display_name', 'status_id'];

    protected $fillable = ['name', 'display_name', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

}
