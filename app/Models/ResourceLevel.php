<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ResourceLevel extends Model
{
    use Sortable;

    public $sortable = ['id', 'description', 'status'];

    protected $table = 'resource_level';

    protected $primaryKey = 'id';

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
