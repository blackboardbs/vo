<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ProjectTerms extends Model
{
    use Sortable;

    public $sortable = ['terms_ver', 'start_date','end_date'];
}
