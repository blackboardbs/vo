<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class VendorInvoiceStatus extends Model
{
    use Sortable;

    protected $table = 'vendor_invoice_status';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'status_id'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
