<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Cv extends Model
{
    use Sortable;

    protected $table = 'talent';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'user_id', 'res_position', 'cell', 'phone', 'personal_email'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function resource()
    {
        return $this->belongsTo(Resource::class, 'user_id', 'user_id');
    }

    public function availability()
    {
        return $this->hasMany(\App\Models\Availability::class, 'res_id', 'user_id');
    }

    public function qualification()
    {
        return $this->hasMany(\App\Models\Qualification::class, 'res_id', 'user_id');
    }

    public function experience()
    {
        return $this->hasMany(\App\Models\Experience::class, 'res_id', 'user_id');
    }

    public function iexperience()
    {
        return $this->hasMany(\App\Models\IndustryExperience::class, 'res_id', 'user_id');
    }

    public function skill()
    {
        return $this->hasMany(\App\Models\Skill::class, 'res_id', 'user_id');
    }

    public function type()
    {
        return $this->belongsTo(\App\Models\ResourceType::class, 'res_type');
    }

    public function resourcetype()
    {
        return $this->belongsTo(\App\Models\ResourceType::class, 'user_id');
    }

    public function level()
    {
        return $this->belongsTo(\App\Models\ResourceLevel::class, 'res_level');
    }

    public function marital_status()
    {
        return $this->belongsTo(\App\Models\MaritalStatus::class, 'marital_status_id');
    }

    public function disabilityd()
    {
        return $this->belongsTo(\App\Models\Disability::class, 'disability');
    }

    public function genderd()
    {
        return $this->belongsTo(\App\Models\Gender::class, 'gender');
    }

    public function bbbee_raced()
    {
        return $this->belongsTo(\App\Models\BBBEERace::class, 'bbbee_race');
    }

    public function raced()
    {
        return $this->belongsTo(\App\Models\Race::class, 'race');
    }

    public function naturalizationd()
    {
        return $this->belongsTo(\App\Models\Naturalization::class, 'naturalization_id');
    }

    public function cv()
    {
        return $this->hasMany(\App\Models\Experience::class, 'res_id');
    }

    public function role()
    {
        return $this->belongsTo(\App\Models\ScoutingRole::class, 'role_id');
    }

    public function quality()
    {
        return $this->hasMany(\App\Models\CVQuality::class, 'cv_id');
    }

    public function nationality()
    {
        return $this->hasMany(\App\Models\CVQuality::class, 'cv_id');
    }
}
