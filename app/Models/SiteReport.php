<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SiteReport extends Model
{
    use sortable;

    public $sortable = ['id', 'employee_id', 'customer_id', 'project_id', 'reporting_start_date', 'status'];

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'employee_id');
    }

    public function consultant()
    {
        return $this->belongsTo(\App\Models\User::class, 'consultant_emp_id');
    }

    public function internal_reviewer()
    {
        return $this->belongsTo(\App\Models\User::class, 'internal_review_emp_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
