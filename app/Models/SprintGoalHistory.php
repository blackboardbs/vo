<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SprintGoalHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'sprint_id', 'goal_id', 'no_of_tasks', 'task_completed', 'task_outstanding', 'overdue_tasks', 'impediments',
        'planned_hours', 'booked_hours'
    ];

    public function sprint(): BelongsTo
    {
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function goal(): BelongsTo
    {
        return $this->belongsTo(Goal::class, 'goal_id');
    }
}
