<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintForecastView extends Model
{
    use HasFactory;

    protected $table = 'sprint_forecast_view';
}
