<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kyslik\ColumnSortable\Sortable;

class ProspectStatus extends Model
{
    use HasFactory;
    use Sortable;

    public $sortable = ['id', 'description', 'status_id'];

    protected $table = 'prospect_statuses';
    protected $fillable = ['description', 'status_id'];

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
}
