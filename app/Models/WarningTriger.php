<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarningTriger extends Model
{
    use SoftDeletes;

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
