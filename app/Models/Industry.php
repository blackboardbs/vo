<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Industry extends Model
{
    use Sortable;

    protected $table = 'industry';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
