<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Api extends Model
{
    use HasFactory;

    use Sortable;

    protected $table = 'apis';
    protected $fillable = [
        'api_token',    ];
}
