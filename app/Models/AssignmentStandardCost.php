<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class AssignmentStandardCost extends Model
{
    use Sortable;

    public $sortable = ['description', 'standard_cost_rate', 'min_rate', 'max_rate'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class);
    }
}
