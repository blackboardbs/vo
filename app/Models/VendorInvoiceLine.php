<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class VendorInvoiceLine extends Model
{
    use Sortable;

    public $sortable = ['id', 'vendor_invoice_number'];

    protected $table = 'vendor_invoice_lines';

    public function time()
    {
        return $this->belongsTo(\App\Models\Time::class, 'time_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\Resource::class, 'emp_id');
    }

    public function vendor_invoice()
    {
        return $this->belongsTo(\App\Models\VendorInvoice::class, 'vendor_invoice_number');
    }

    public function assignment()
    {
        return $this->belongsTo(\App\Models\Assignment::class, 'ass_id');
    }
}
