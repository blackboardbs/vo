<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BBBEERace extends Model
{
    protected $table = 'bbbee_race';
    protected $primaryKey = 'id';

}