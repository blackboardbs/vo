<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Config extends Model
{
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function billingCycle(): BelongsTo
    {
        return $this->belongsTo(BillingCycle::class, 'billing_cycle_id');
    }

    public function approvalManager()
    {
        return $this->belongsTo(User::class, 'hr_approval_manager_id');
    }

    public function hrUser()
    {
        return $this->belongsTo(User::class, 'hr_user_id');
    }
}
