<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPreference extends Model
{
    use SoftDeletes;

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function profession()
    {
        return $this->belongsTo(\App\Models\Profession::class, 'profession_id');
    }

    public function speciality()
    {
        return $this->belongsTo(\App\Models\Speciality::class, 'speciality_id');
    }

    public function skill()
    {
        return $this->belongsTo(\App\Models\ScoutingRole::class, 'skill_id');
    }
}
