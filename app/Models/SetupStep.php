<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SetupStep extends Model
{
    use SoftDeletes;

    protected $table = 'setup_step';

    public function module()
    {
        return $this->belongsTo(\App\Models\Module::class, 'module_id');
    }

    public function modulelinks()
    {
        return $this->belongsTo(\App\Models\ModuleLinks::class, 'module_id', 'module_id');
    }
}
