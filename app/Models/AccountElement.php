<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class AccountElement extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'account_element';

    protected $primaryKey = 'id';

    protected $fillable = ['account_id', 'description', 'account_element', 'status', 'created_at', 'updated_at'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class);
    }
}
