<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PlannedExpense extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'plan_exp';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'account_element-id', 'plan_exp_value', 'plan_exp_date'];

    public function status_desc()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id');
    }

    public function account_element()
    {
        return $this->belongsTo(\App\Models\AccountElement::class, 'account_element_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }
}
