<?php

namespace App\Models;

use App\Traits\TemplateTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Kyslik\ColumnSortable\Sortable;

class AssessmentHeader extends Model
{
    use Sortable;
    use TemplateTrait;

    public $sortable = ['id', 'assessment_date', 'customer_id'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'resource_id');
    }

    public function assessor()
    {
        return $this->belongsTo(\App\Models\User::class, 'assessed_by');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function activities(): HasMany
    {
        return $this->hasMany(AssessmentActivities::class, 'assessment_id');
    }

    public function nextAssessmentActivities(): HasMany
    {
        return $this->hasMany(AssessmentPlannedNext::class, 'assessment_id');
    }

    public function assessmentNotes(): HasMany
    {
        return $this->hasMany(AssessmentNotes::class, 'assessment_id');
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(AdvancedTemplates::class, 'template_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }
}
