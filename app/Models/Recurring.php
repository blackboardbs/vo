<?php

namespace App\Models;

use App\Enum\FrequencyEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Recurring extends Model
{
    use HasFactory;

    protected $table = 'recurrings';

    protected $casts = ['frequency' => FrequencyEnum::class];
    protected $fillable = [
        'full_name', 'first_invoice_at',
        'due_days_from_invoice_date', 'frequency', 'interval',
        'occurrences', 'expires_at', 'email_subject', 'emails'
    ];

    public function recurrable(): MorphTo
    {
        return $this->morphTo();
    }
}
