<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LeaveStatement extends Model
{
    public function leave_type()
    {
        return $this->belongsTo(\App\Models\LeaveType::class, 'leave_type_id');
    }

    public function is_divisible_by_3($n)
    {
        $digits = str_split($n);
        $total = 0;
        foreach ($digits as $digit) {
            $total += $digit;
        }
        if ($total == 3 || ($total % 3 == 0)) {
            return true;
        }

        return false;
    }

    public function minus_year($n)
    {
        $total = $n;

        do {
            $total = $total - 3;
        } while ($total > 0);

        return $total;
    }

    public function get_opening_balance($emp, $start_date)
    {
        $resource = User::join('resource', 'users.id', '=', 'resource.user_id')->where('resource.user_id', $emp)->first();
        //dd($resource->join_date);
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $start_of_year = Carbon::parse(now())->startOfYear()->format('Y-m-d');
        //dd($start_date);
        if (Carbon::parse($resource->join_date)->year <= Carbon::parse($start_date)->year) {
            $join_year = Carbon::parse(now())->year;
            $join_year = $join_year - 1;
        } else {
            $join_year = Carbon::parse(now())->year;
        }

        $join_month = Carbon::parse($resource->join_date)->month;
        $join_day = Carbon::parse($resource->join_date)->startOfMonth()->day;

        $resource_start_date = $join_year.'-'.$join_month.'-'.$join_day;

        if (Carbon::parse($resource_start_date) > Carbon::parse($start_date)) {
            $leave_start = Carbon::parse($resource_start_date)->subYear(1)->format('Y-m-d');
        } else {
            $leave_start = Carbon::parse($resource_start_date)->format('Y-m-d');
        }
        //dd($leave_start);

        if (Carbon::parse($resource->join_date) < Carbon::parse($start_date)) {
            if (Carbon::parse($resource->join_date)->month == 12) {
                $ddiff1 = Carbon::parse(Carbon::parse($leave_start))->diffInMonths(Carbon::parse($start_date));
                //$ddiff = $ddiff1;
                if ($ddiff1 == 0) {
                    $ddiff = $ddiff1 + 1;
                } else {
                    $ddiff = $ddiff1 + 1;
                }
            //dd($leave_start);
            } else {
                $ddiff1 = Carbon::parse(Carbon::parse($leave_start)->endOfYear())->diffInMonths(Carbon::parse($start_date));
                if ($ddiff1 == 0) {
                    $ddiff2 = Carbon::parse(Carbon::parse($leave_start))->diffInMonths(Carbon::parse($start_date));
                    $ddiff = $ddiff2 + 1;

                //dd($ddiff2);
                } else {
                    $ddiff2 = Carbon::parse(Carbon::parse($leave_start))->diffInMonths(Carbon::parse($start_date));
                    $ddiff = $ddiff2;
                    //dd($ddiff);
                }
            }
        } else {
            $ddiff1 = Carbon::parse(Carbon::parse($leave_start))->diffInMonths($start_date);
            $ddiff = 12 - $ddiff1;
            //dd($ddiff);
        }

        //dd($ddiff);
        //dd($ddiff);

        $work_days = 5;

        if ($work_days == 5) {
            $annual_accrual = 1.25;
        }

        if ($work_days == 6) {
            $annual_accrual = 1.5;
        }

        $total_accrued = $ddiff * $annual_accrual;

        $leave_taken = Leave::where('emp_id', $emp)->where('leave_type_id', '1')->where('leave_status', '1')->get();
        //dd($leave_taken);
        $total_leave = 0;
        foreach ($leave_taken as $item) {
            if (Carbon::parse($item->date_from)->lt(Carbon::parse($start_date)->endOfMonth()) && Carbon::parse($item->date_from)->year == Carbon::parse($start_date)->year) {
                //if(Carbon::parse($item->date_from)->year == Carbon::parse($start_date)->year) {
                $total_leave = $total_leave + $item->no_of_days;
            }
        }
        //dd($total_accrued);
        if (Carbon::parse($resource->join_date) > Carbon::parse($start_date)) {
            $annual_leave_balance = 0;
        } else {
            $annual_leave_balance = $total_accrued - $total_leave;
        }

        //dd($ddiff);

        return $annual_leave_balance;
        /*$join_date = Carbon::parse($resource->join_date)->toDateString();

        $join_year = Carbon::parse(now())->year;
        $join_month = Carbon::parse($resource->join_date)->month;
        $join_day = Carbon::parse($resource->join_date)->startOfMonth()->day;

        $current_day = Carbon::parse($start_date)->startOfMonth()->day;
        $current_month = Carbon::parse($start_date)->month;
        $current_year = Carbon::parse($start_date)->year;


        if ($join_month == '12'){  //
            if($join_year < $current_year){
                $leave_start_year = $current_year - 1;
                $leave_start_month = $current_month;
                $leave_start_day = $current_day;
            } else {
                $leave_start_year = $current_year;
                $leave_start_month = $current_month;
                $leave_start_day = $current_day;
            }
        } elseif(($join_month <= $current_month && $join_day <= $current_day) || ($current_year == $join_year)) {
            //get resource join date month and day with current year
            $leave_start_year = $current_year;
            $leave_start_month = $join_month;
            $leave_start_day = $join_day;
        } else {
            $leave_start_year = $current_year - 1;
            $leave_start_month = $join_month;
            $leave_start_day = $join_day;
        }

        //resource join date in current year
        $resource_start_date = $leave_start_year . '-' . $leave_start_month . '-' . $leave_start_day;
        $leave_start = Carbon::parse($resource_start_date)->format('Y-m-d');

        if($join_month <= $current_month && $join_day <= $current_day) {
            if($current_year == $join_year){
                $date_difference = Carbon::parse($resource_start_date)->diffInMonths($start_date);
            } else {
                $date_difference = Carbon::parse(Carbon::parse($start_date)->startOfYear())->diffInMonths($start_date);
            }
        } else {

            //How long has the resource worked this cycle
            if(Carbon::parse($resource->join_date)->lt(Carbon::parse($start_date))) {
                $date_difference = Carbon::parse(Carbon::parse($start_date)->startOfYear())->diffInMonths($start_date)+1;
            } else {
                $date_difference = 0;
            }
            //$date_difference = Carbon::parse($resource_start_date)->diffInMonths($start_date);
        }

        $work_days = 5;


            if ($work_days == 5) {
                $annual_accrual = 1.25;
            }

            if ($work_days == 6) {
                $annual_accrual = 1.5;
            }

        $leave_taken = Leave::where('emp_id',$emp)->where('leave_type_id','1')->where('leave_status','1')->get();
        $total_leave = 0;
        foreach ($leave_taken as $item) {
            if(Carbon::parse($item->date_from)->lt(Carbon::parse($start_date)->endOfMonth()) && Carbon::parse($item->date_from)->year == $current_year) {
                $total_leave = $total_leave + $item->no_of_days;
            }
        }

        $opening_balance1 = $annual_accrual * $date_difference;
        $opening_balance2 = $opening_balance1 - $total_leave;

        return $opening_balance2;*/
    }
}
