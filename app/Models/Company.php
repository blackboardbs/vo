<?php

namespace App\Models;

use App\Traits\HasImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Company extends Model
{
    use Sortable;
    use HasImage;
    use HasFactory;

    protected $table = 'company';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'company_name', 'email', 'phone', 'cell', 'created_at', 'status_id', 'company_logo'];

    protected $fillable = [
        'company_name',
        'postal_address_line1',
        'postal_address_line2',
        'postal_address_line3',
        'state_province',
        'city_suburb',
        'postal_zipcode',
        'vat_no',
        'status_id',
        'country_id',
        'bbbee_level_id',
        'creator_id'
    ];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id');
    }

    public function manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'team_manager');
    }

    public function bbbee_level()
    {
        return $this->belongsTo(\App\Models\BBBEELevel::class, 'bbbee_level_id');
    }

    public function bankAccountType()
    {
        return $this->belongsTo(\App\Models\BankAccountType::class, 'bank_account_type_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'company_id', 'id');
    }
    public function users()
    {
        return $this->hasMany(User::class, 'company_id', 'id');
    }

    public function scopeFilters($query)
    {
        return $query->when(request()->project, function ($proj) {
            if(request()->project != '-1'){
                $proj->whereHas('projects', function ($q){
                    $q->where('id',request()->project);
                });
            }
        })
        ->when(isset(request()->project_status) && request()->project_status != '' && request()->project_status != '0', function ($project) {
            if(request()->project_status == '1' ){
                $project->whereHas('projects', function ($q){
                    $q->whereIn('status_id', [1, 2, 3]);
                });
            }
            if(request()->project_status == '2' ){
                $project->whereHas('projects', function ($q){
                    $q->whereIn('status_id', [4, 5]);
                });
            }
        })
        ->when(request()->customer, function ($proj) {
            if(request()->customer != '-1'){
                $proj->whereHas('projects', function ($q){
                    $q->where('customer_id',request()->customer);
                });
            }
        })  
        ->when(request()->customer_id, function ($proj) {
            if(request()->customer_id != '-1'){
                $proj->whereHas('projects', function ($q){
                    $q->where('customer_id',request()->customer_id);
                });
            }
        })        
        ->when(isset(request()->employee) && request()->employee != '-1' && request()->employee != '0' && request()->employee != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->employee);
                });
        })
        ->when(isset(request()->resource) && request()->resource != '-1' && request()->resource != '0' && request()->resource != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->resource);
                });
        })
        ->when(isset(request()->resource_id) && request()->resource_id != '-1' && request()->resource_id != '0' && request()->resource_id != '', function ($proj) {
                $proj->whereHas('projects.assignment', function ($q){
                    $q->where('employee_id',request()->resource_id);
                });
        });
        // ->when(request()->customer, function ($task) {
        //     $task->where('customer_id', request()->customer);
        // });
    }
}
