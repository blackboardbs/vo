<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackFemaleOwnership extends Model
{
    protected $table = 'black_female_ownership';
    protected $primaryKey = 'id';

}