<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackFemaleVotingRights extends Model
{
    protected $table = 'black_female_voting_rights';
    protected $primaryKey = 'id';

}