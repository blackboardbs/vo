<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistributionList extends Model
{
    use SoftDeletes;

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function frequency()
    {
        return $this->belongsTo(\App\Models\ScheduleRunFrequency::class, 'run_frequency_id');
    }
}
