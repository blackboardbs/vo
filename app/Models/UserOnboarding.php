<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserOnboarding extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function title(): BelongsTo
    {
        return $this->belongsTo(Title::class, 'title_id');
    }

    public function status()
    {
        return $this->belongsTo(OnboardingStatus::class, 'status_id');
    }

    public function paymentDetails()
    {
        return $this->hasOne(PaymentDetail::class, 'user_onboarding_id');
    }

    public function contactDetails()
    {
        return $this->hasOne(ContactDetail::class, 'user_onboarding_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function appointingManager()
    {
        return $this->hasOne(UserOnboardingAppointingManager::class, 'user_onboarding_id');
    }

    public function scopeFilter($query)
    {
        return $query->when(\request()->q, function($onboarding){
            $onboarding->whereHas('user', function ($user){
                $user->where('first_name', 'LIKE', '%'.\request()->q.'%')
                    ->orWhere('last_name', 'LIKE', '%'.\request()->q.'%')
                    ->orWhere('email', 'LIKE', '%'.\request()->q.'%');
            });
        })
            ->when(\request()->appointment_manager_id, function ($onboarding){
                $onboarding->whereHas('user', function ($user){
                    $user->where('appointment_manager_id', \request()->appointment_manager_id);
                });
            })
            ->when(\request()->status_id, function ($onboarding){
                $onboarding->where('status_id', \request()->status_id);
            });
    }
}
