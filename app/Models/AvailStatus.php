<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailStatus extends Model
{
    protected $table = 'avail_status';
}
