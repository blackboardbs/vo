<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ResourceTask extends Model
{
    use Sortable;

    protected $table = 'resource_task';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'task_number', 'subject', 'description', 'due_date', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\TaskStatus::class, 'status_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'task_user');
    }
}
