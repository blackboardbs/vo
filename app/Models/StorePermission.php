<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class StorePermission extends Model
{
    use SoftDeletes;

    public function module()
    {
        return $this->belongsTo(\App\Models\Module::class, 'module_id');
    }

    public function module_section()
    {
        return $this->belongsTo(\App\Models\ModuleSection::class, 'module_section_id');
    }

    public function permission()
    {
        return $this->belongsTo(\App\Models\PermissionType::class, 'permission_id');
    }

    public function role()
    {
        return $this->belongsTo(\App\Models\ModuleRole::class, 'module_role_id');
    }

    public function getPermissions($module_id, $module_item_id)
    {
        $resourceViewRole = ResourceViewRole::where('resource_id', auth()->id())
        ->where('module_id', 30)
        ->where('module_item_id', $module_item_id)
        ->first();

        $storePermissions = null;
        if (isset($resourceViewRole->role_id)) {
            $storePermissions = StorePermission::with(['module', 'module_section', 'role', 'permission'])
            ->where('module_id', $module_id)
            ->where('module_role_id', $resourceViewRole->role_id)
            ->get();
        }

        $permissions = null;

        if (Auth::user()->hasRole('admin')) {
            $module_sections = ModuleSection::get();

            foreach($module_sections as $ms){
                $permissions[$ms->id]['permission'] = 'Full';
            }
        }

        if (isset($storePermissions)) {
            foreach ($storePermissions as $storePermission) {
                $key = $storePermission->module_section->id ?? null;
                $permissions[$key]['id'] = $storePermission->id ?? null;
                $permissions[$key]['module'] = isset($storePermission->module->display_name) ? $storePermission->module->display_name : '';
                $permissions[$key]['section'] = isset($storePermission->module_section->name) ? $storePermission->module_section->name : '';
                $permissions[$key]['permission'] = isset($storePermission->permission->name) ? $storePermission->permission->name : '';

                // Todo: Find a way to allow for administrator to have access to project views
                if (Auth::user()->hasRole('admin')) {
                    $permissions[$key]['permission'] = 'Full';
                }
            }
        }

        return $permissions;
    }
}
