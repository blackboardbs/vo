<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SprintTaskSummary extends Model
{
    use HasFactory;

    protected $table = 'sprint_task_summary_view';

    public function resource(): BelongsTo
    {
        return $this->belongsTo(User::class, 'Resource_ID');
    }
}
