<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class DocumentType extends Model
{
    use Sortable;

    protected $table = 'document_type';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'name'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }
}
