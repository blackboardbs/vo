<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Board extends Model
{
    use SoftDeletes, Sortable;

    public function status(){
        return $this->belongsTo(Status::class, 'status_id');
    }
}
