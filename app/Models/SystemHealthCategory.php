<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemHealthCategory extends Model
{
    use HasFactory;

    protected $table = 'sh_categories';

    protected $primaryKey = 'id';
}
