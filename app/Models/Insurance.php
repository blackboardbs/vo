<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Insurance extends Model
{
    use HasFactory;

    protected $fillable = ['asset_register_id', 'insurer', 'insured_value', 'covered_at', 'excess_amount', 'note', 'policy_ref'];

    public function assertRegister(): BelongsTo
    {
        return $this->belongsTo(AssetRegister::class, 'asset_register_id');
    }
}
