<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class VatRate extends Model
{
    use Sortable;

    protected $table = 'vat_rate';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'vat_rate', 'vat_code', 'start_date', 'end_date', 'status'];

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
