<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClockifyTime extends Model
{
    public function timeline()
    {
        return $this->belongsTo(Timeline::class, 'timeline_id');
    }
}
