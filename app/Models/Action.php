<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes;

    public function assignedUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'assigned_user_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'updated_user_id');
    }

    public function originalUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'original_user_id');
    }

    public function createdUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'creator_id');
    }

    public function rejectionReason()
    {
        return $this->belongsTo(\App\Models\RejectionReason::class, 'rejection_reason_id');
    }

    public function relatedProject()
    {
        return $this->belongsTo(\App\Models\Project::class, 'related_project_id');
    }

    public function relatedTask()
    {
        return $this->belongsTo(\App\Models\Task::class, 'related_task_id');
    }

    public function relatedCustomer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'related_customer_id');
    }

    public function relatedJobSpec()
    {
        return $this->belongsTo(\App\Models\JobSpec::class, 'related_job_spec_id');
    }

    public function relatedCustomerInvoice()
    {
        return $this->belongsTo(\App\Models\CustomerInvoice::class, 'related_customer_invoice_id');
    }

    public function relatedVendorInvoice()
    {
        return $this->belongsTo(\App\Models\VendorInvoice::class, 'related_vendor_invoice_id');
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\ActionStatus::class, 'status_id');
    }
}
