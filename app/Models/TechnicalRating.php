<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalRating extends Model
{
    use SoftDeletes;

    protected $table = 'technical_rating';
}
