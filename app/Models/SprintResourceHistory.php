<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprintResourceHistory extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'sprint_id', 'function_id', 'capacity', 'leave_days'];
}
