<?php

namespace App\Models;

use App\Enum\YesOrNoEnum;
use Illuminate\Database\Eloquent\Model;

class TimeExp extends Model
{
    protected $table = 'time_exp';

    protected $casts = [
        'claim' => YesOrNoEnum::class,
        'is_billable' => YesOrNoEnum::class
    ];

    public function timesheet()
    {
        return $this->belongsTo(\App\Models\Timesheet::class, 'timesheet_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function project()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id');
    }

    public function account_element()
    {
        return $this->belongsTo(\App\Models\AccountElement::class, 'account_element_id');
    }

    public function claim_approver()
    {
        return $this->belongsTo(\App\Models\User::class, 'claim_auth_user');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
