<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Quotation extends Model
{
    use Sortable;

    protected $table = 'quotation';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'contact_firstname', 'contact_firstname', 'customer_ref', 'status_id'];

    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function resource()
    {
        return $this->belongsTo(\App\Models\User::class, 'prepared_by_emp_id');
    }

    public function expense()
    {
        return $this->belongsTo(\App\Models\Expense::class, 'exp_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function consultant()
    {
        return $this->belongsTo(\App\Models\User::class, 'consultant_emp_id');
    }
}
