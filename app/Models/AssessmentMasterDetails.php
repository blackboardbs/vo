<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class AssessmentMasterDetails extends Model
{
    use Sortable;

    public $sortable = ['assessment_master_id', 'assessment_group', 'assessment_measure'];

    protected $table = 'assessment_master_details';

    public function assess_master()
    {
        return $this->belongsTo(\App\Models\AssessmentMaster::class, 'assessment_master_id');
    }
}
