<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Status extends Model
{
    use Sortable;
    use HasFactory;

    protected $table = 'status';

    protected $primaryKey = 'id';

    public $sortable = ['id', 'description', 'order'];

    public function company()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'company', 'status');
    }

    public function statusd()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status');
    }
}
