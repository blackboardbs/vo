<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserStory extends Model
{
    use SoftDeletes;

    public function tasks(){
        return $this->hasMany(Task::class, 'user_story_id');
    }

    public function feature(){
        return $this->belongsTo(Feature::class, 'feature_id');
    }

    public function sprint(){
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }

    public function status(){
        return $this->belongsTo(TaskStatus::class, 'status_id');
    }

    public function userStoryDiscussions()
    {
        return $this->hasMany(UserStoryDiscussion::class, 'user_story_id');
    }

    public function scopeFilters($query)
    {
        return $query->when(isset(request()->company) && request()->company != '-1' && request()->company != '0' && request()->company != '', function ($user) {
            $user->whereHas('feature.epic.project', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '0' && request()->customer != '', function ($task) {
                $task->whereHas('feature.epic.project', function ($q){
                    $q->where('customer_id', request()->customer);
                });
        })->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($user) {
                $user->whereHas('feature.epic', function ($q){
                    $q->where('project_id',request()->project);
                });
        })->when(isset(request()->project_id) && request()->project_id != '-1' && request()->project_id != '0' && request()->project_id != '', function ($user) {
            $user->whereHas('feature.epic', function ($q){
                $q->where('project_id',request()->project_id);
            });
        })->when(
            !isset(request()->project) || request()->project == '-1' || request()->project == '0' || request()->project == '' &&
            !isset(request()->project_id) || request()->project_id == '-1' || request()->project_id == '0' || request()->project_id == '',
            function ($user) {
                $user->whereHas('feature.epic.project', function ($q) {
                    $q->whereIn('status_id', [1, 2, 3]);
                });
            }
        )->when(isset(request()->epic) && request()->epic != '-1' && request()->epic != '0' && request()->epic != '', function ($user) {
            $user->whereHas('feature.epic', function ($q){
                $q->where('id',request()->epic);
            });
        })->when(isset(request()->feature) && request()->feature != '-1' && request()->feature != '0' && request()->feature != '', function ($user) {
                $user->where('feature_id',request()->feature);
        });
    }
}
