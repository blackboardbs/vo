<?php

namespace App\Scouting;

use App\Models\Cv;
use App\Models\CvSubmit;
use App\Models\JobSpec;

class Dashboard
{
    public function openJobsHeaderComponent()
    {
        return JobSpec::where('status_id', 1)->where('applicaiton_closing_date', '>=', now()->toDateString())->get()->count();
    }

    public function daysDifferenceJobHeaderComponent($model, $column)
    {
        return app($model)->whereBetween($column, [now()->subDays(7)->toDateTimeString(), now()->toDateTimeString()])
            ->get()->count();
    }

    public function submitedCVHeaderComponent()
    {
        return CvSubmit::all()->count();
    }

    public function submittedCandidatesHeaderComponent()
    {
        return CvSubmit::selectRaw('job_spec_id')->groupBy('job_spec_id')->get()->count();
    }

    public function activeCandidatesHeaderComponent()
    {
        return Cv::all()->count();
    }

    public function addedThisMonth($model)
    {
        return app($model)->whereBetween('created_at', [now()->startOfMonth()->toDateString(), now()->endOfMonth()->toDateString()])->get()->count();
    }

    public function addedThisWeek($model)
    {
        return app($model)->whereBetween('created_at', [now()->startOfWeek()->toDateString(), now()->endOfWeek()->toDateString()])->get()->count();
    }

    public function potentialCommisionEarned()
    {
        return array_sum(CvSubmit::selectRaw('IFNULL(AVG(ctc * (commission/100)), 0) AS potential_commission')->groupBy('job_spec_id')->get()->toArray());
    }

    public function latestTen($model, $relationship = [])
    {
        $last_10 = app($model)->orderBy('created_at', 'desc')->limit(10);
        if (isset($relationship)){
            return $last_10->with($relationship)->get();
        }else{
            return $last_10->get();
        }
    }
}
