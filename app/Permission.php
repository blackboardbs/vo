<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function children(){
        return $this->belongsToMany('App\Permission','permissions','id','parent_id');
    }
}
