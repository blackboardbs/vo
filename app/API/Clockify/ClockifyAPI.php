<?php

namespace App\API\Clockify;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ClockifyAPI
{
    use ClockifyTrait;

    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_UNAUTHORIZED = 401;
    const STATUS_FORBIDDEN = 403;
    const STATUS_NOT_FOUND = 404;
    //const API_BASE_ENDPOINT = "https://api.clockify.me/api/v1";
    //const API_KEY = "ZWI4NmRkNzItZDJjZC00MGU5LTg0NDQtMDBiOGQ4Y2RlZmE0";
    //const API_WORKSPACE_ID = "63e49ad4bc8b683b15c9a150";

    public function getWorkspaceId(): string
    {
        try {
            $client = new Client();
            $request = $client->request(
                'GET',
                config('app.clockify.base_endpoint').'/workspaces/',
                [
                    'headers' => [
                        'Content-Types' => '*/*',
                        'Accept' => 'application/json',
                        'X-Api-Key' => config('app.clockify.api_key')
                    ]
                ]
            );
        }catch (GuzzleException $exception){
            logger($exception->getMessage());
        }

        if ($request->getStatusCode() == self::STATUS_OK){
            $response = json_decode($request->getBody()->getContents());
            return $response[0]->id??'';
        }

        return '';
    }

    public function get(string $endpoint): array
    {
        $request = $this->getEndPoint(
            config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').$endpoint
        );

        $response = json_decode($request->getBody()->getContents(), true);

        if ($request->getStatusCode() == self::STATUS_OK){
            return $this->dataformat($response);
        }

        return [];
    }

    public function post(string $endpoint, array $form)
    {
        $request = $this->postEndPoint(
            config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').$endpoint,
            $form
        );
    }

    public function addClient(array $form)
    {
        $request = $this->postEndPoint(
            config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').'/clients',
            $form
        );

        return $request->getStatusCode();
    }
}