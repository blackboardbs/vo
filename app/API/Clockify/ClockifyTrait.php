<?php

namespace App\API\Clockify;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;

trait ClockifyTrait
{
    public function arrayMismatch(array $local_array, array $clockify_array): array
    {
        if (count($clockify_array)){
            $not_matched = array_map(function ($item) use($clockify_array){
                if (!in_array($item, $clockify_array)){
                    return $item;
                }
                return null;
            }, $local_array);

            return array_filter($not_matched);
        }

        return $local_array;
    }

    public function dataformat(array $response): array
    {
        $clients = collect($response)->map(function ($client){
            return [
                'id' => (int) $client["note"],
                'name' => $client["name"]
            ];
        })->toArray();

        return $clients;
    }

    public function getEndPoint(string $url): Response
    {
        try {
            $request = new Client();
            $request = $request->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'Content-Types' => '*/*',
                        'Accept' => 'application/json',
                        'X-Api-Key' => config('app.clockify.api_key')
                    ]
                ]
            );
        }catch (GuzzleException $exception){
            logger($exception->getMessage());
            die($exception->getMessage());
        }

        return $request;
    }

    public function postEndPoint(string $url, array $form): Response
    {
        try {
            $request = new Client();
            $request = $request->request(
                "POST",
                $url,
                [
                    'headers' => [
                        'Content-Types' => 'application/json',
                        'Accept' => 'application/json',
                        'X-Api-Key' => config('app.clockify.api_key')
                    ],
                    'json' => $form
                ]
            );

        }catch (GuzzleException $exception){
            logger($exception->getMessage());
        }

        return $request;
    }

    public function destroy(string $url)
    {
        try {
            $request = new Client();
            $request->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'Content-Types' => '*/*',
                        'Accept' => 'application/json',
                        'X-Api-Key' => config('app.clockify.api_key')
                    ]
                ]
            );
        }catch (GuzzleException $exception){
            logger($exception->getMessage());
        }
    }

    public function consultantIds(Collection $projects)
    {
        $user_ids = [];
        foreach ($projects as $project){
            $consultants = explode(",", $project->consultants);
            foreach ($consultants as $consultant){
                if (!in_array($consultant, $user_ids)){
                    array_push($user_ids, $consultant);
                }
            }
        }

        return $user_ids;
    }

    public function singleClient(string $endpoint)
    {
        $request = $this->getEndPoint(
            config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').'/clients?archived=false&name='.$endpoint
        );

        if ($request->getStatusCode() == self::STATUS_OK){
            return $response = json_decode($request->getBody()->getContents());
        }

        return [];
    }

    public function projects()
    {
        return json_decode(
            $this->getEndPoint(
                config('app.clockify.base_endpoint')."/workspaces/".config('app.clockify.workspace').'/projects?archived=false'
            )->getBody()->getContents()
        );
    }

    public function formatProject(Collection $projects, bool $is_local)
    {
        return $projects->map(function ($project) use ($is_local){
            $project_formatted = [
                "name" => $project->name,
                "clientId" => $is_local ? ($this->singleClient($project->customer->customer_name??'')[0]->id??null) : $project->clientId,
                "note" => $is_local ? $project->id : (int) $project->note,
                "isPublic" => false
            ];

            if (!$is_local){
                $project_formatted["id"] = $project->id;
            }

            return $project_formatted;
        })->toArray();
    }

    public function projectId($project_id): string
    {
        foreach ($this->projects() as $project){
            if ((int) $project->note === (int) $project_id){
                return $project->id;
            }
        }

        return '';
    }
}