<?php

namespace App\API\Currencies;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Currency
{
    public function currencies(): array
    {
        $data = [];

        try {
            $response = $this->request("https://openexchangerates.org/api/currencies.json");

            if ($response->getStatusCode() === 200){
                $data["success"] = "API was called successful";
                $data["currencies"] = json_decode($response->getBody()->getContents(), true);
            }
        } catch (ClientException $client){
            $data["errors"] = $client->getMessage();
        }

        return $data;
    }

    public function conversionRate(string $currency): array
    {
        $data = [];
        try {
            $response = $this->request("https://v6.exchangerate-api.com/v6/44b4a91c1ad19a2c5820afb9/latest/ZAR");
            if ($response->getStatusCode() == 200){
                $data["success"] = "API was call successful";
                $response = json_decode($response->getBody()->getContents(), true);
                $data["rate"] = $response["conversion_rates"][strtoupper($currency)]??null;
            }
        }catch (ClientException $client){
            $data["errors"] = $client->getMessage();
        }

        return $data;
    }

    private function request(string $url)
    {
        $client = new Client();
        $response = $client->request("GET", $url);

        return $response;
    }
}