<?php

use App\Models\Module;
use Illuminate\Support\Facades\Auth;

if (!function_exists('_minutes_to_time')){

    function _minutes_to_time($_minutes): string
    {
        $hours = floor($_minutes/60);
        $minutes = $_minutes % 60;
        $negation_number = '';

        if ($_minutes < 0){
            $hours = floor(abs($_minutes)/60);
            $minutes = abs($_minutes) % 60;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }
}

if (!function_exists('can_view_sidebar')){
    function can_view_sidebar(string $module_name): bool
    {
        $module = Module::where('name', "App\Models\\$module_name")->latest()->first();

        if (!isset($module)){
            return false;
        }

        return (Auth::user()->canAccess($module?->id, 'view_all')
            || Auth::user()->canAccess($module?->id, 'view_team'));
    }
}
