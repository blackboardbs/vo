<?php

namespace App\Http;

use DB;
use Illuminate\Http\Request;

class GlobalForm
{
    /*
     * @param $look_up_id, the look up id
     * @return $parameters, the parameters to be passed to the view file
     * */
    public static function list($modelName, $includeColumns, $knownAs = null)
    {
        $lookUpModelName = 'App\\Models\\'.$modelName;

        $model = new $lookUpModelName;
        $table = $model->getTable();

        $columns = DB::select('describe '.$table);

        $includeDefaultColumns = [];
        $includeDefaultColumns = array_merge($includeDefaultColumns, $includeColumns);

        $counter = 0;
        $relations = [];

        foreach ($columns as $column) {
            if (in_array($column->Field, $includeDefaultColumns)) {
                $label = '';
                $labelParts = explode('_', $column->Field);

                foreach ($labelParts as $labelPart) {
                    if ($labelPart != 'id') {
                        $label .= ' '.ucwords($labelPart);
                    }
                }

                $type = explode('(', $column->Type);

                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($labelParts);
                        if ($labelParts[$parts_size - 1] == 'id') {
                            $formFields[$counter]['type'] = 'look_up';
                            $dropDownModelName = '';
                            foreach ($labelParts as $labelPart) {
                                if ($labelPart != 'id') {
                                    $dropDownModelName .= ucwords($labelPart);
                                }
                            }
                            $LookUpModel = 'App\\Models\\'.$dropDownModelName;
                            $relations[$column->Field] = $LookUpModel::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->toArray();
                        } else {
                            $formFields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $formFields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $formFields[$counter]['type'] = 'text';
                        break;
                }

                $formFields[$counter]['label'] = $label;
                $formFields[$counter]['field'] = $column->Field;
            }

            $counter++;
        }

        $lookUpValues = $lookUpModelName::orderBy('id')->get();

        $parameters = [
            'lookUpValues' => $lookUpValues,
            'formFields' => $formFields,
            'relations' => $relations,
            'modelName' => $modelName,
            'knownAs' => isset($knownAs) ? $knownAs : $modelName,
        ];

        return $parameters;
    }

    /*
     * @param $modelName, the model name
     * @param $includeColumns, columns to be excluded by the form builder
     * @return $parameters, parameters to be passed to the view
     * */
    public static function create($modelName, $includeColumns = [], $knownAs = null)
    {
        $model_name = 'App\\Models\\'.$modelName;

        $model = new $model_name;
        $table = $model->getTable();

        $columns = DB::select('describe '.$table);

        $includeDefaultColumns = [];
        $includeDefaultColumns = array_merge($includeDefaultColumns, $includeColumns);

        $counter = 0;
        $form_fields = [];

        foreach ($columns as $column) {
            if (in_array($column->Field, $includeDefaultColumns)) {
                $label = '';
                $label_parts = explode('_', $column->Field);

                foreach ($label_parts as $label_part) {
                    if ($label_part != 'id') {
                        $label .= ' '.ucwords($label_part);
                    }
                }

                $type = explode('(', $column->Type);
                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($label_parts);
                        if ($label_parts[$parts_size - 1] == 'id') {
                            $form_fields[$counter]['type'] = 'look_up';
                            $Look_up_model_name = '';
                            foreach ($label_parts as $label_part) {
                                if ($label_part != 'id') {
                                    $Look_up_model_name .= ucwords($label_part);
                                }
                            }
                            $Look_up_model = 'App\\Models\\'.$Look_up_model_name;
                            if ($Look_up_model_name == 'Status') {
                                $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('status', '=', 1)->pluck('name', 'id');
                            } else {
                                $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
                            }
                        } else {
                            $form_fields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $form_fields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $form_fields[$counter]['type'] = 'text';
                        break;
                }

                $form_fields[$counter]['label'] = $label;
                $form_fields[$counter]['field'] = $column->Field;
            }

            $counter++;
        }

        $parameters = [
            'form_fields' => $form_fields,
            'modelName' => $modelName,
            'knownAs' => $knownAs,
        ];

        return $parameters;
    }

    /*
     * @param $look_up_id, the look up table id
     * @param $model_id, if edit or show give the data model id
     * @param $exclude_columns, columns to be excluded by the form builder
     * @return true if form successfully saved, else return false
     * */
    public static function store($modelName, Request $request, $validate = true)
    {
        $lookUpClass = 'App\\Models\\'.$modelName;
        $requestClassName = 'App\Http\Requests\\'.$modelName.'Request';

        $requestClass = null;

        if (class_exists($requestClassName) && $validate) {
            $requestClass = new $requestClassName;

            if (! $requestClass->authorize()) {
                return abort(403);
            }
            //Validate form if request class set
            $request->validate($requestClass->rules());
        }

        $model = new $lookUpClass();

        foreach ($request->request as $key => $value) {
            if (! in_array($key, ['_method', '_token'])) {
                $model->$key = $value;
            }
        }

        $model->creator_id = Auth()->id();

        $model->save();

        return $model;
    }

    /*
     * @param $modelName, the model name
     * @param $includeColumns, columns to be excluded by the form builder
     * @return $parameters, parameters to be passed to the view
     * */
    public static function edit($id, $modelName, $includeColumns = [], $knownAs = null)
    {
        $model_name = 'App\\Models\\'.$modelName;

        $model = new $model_name;
        $table = $model->getTable();

        $model = $model_name::find($id);

        $columns = DB::select('describe '.$table);

        $includeDefaultColumns = [];
        $includeDefaultColumns = array_merge($includeDefaultColumns, $includeColumns);

        $counter = 0;
        $form_fields = [];

        foreach ($columns as $column) {
            if (in_array($column->Field, $includeDefaultColumns)) {
                $label = '';
                $label_parts = explode('_', $column->Field);

                foreach ($label_parts as $label_part) {
                    if ($label_part != 'id') {
                        $label .= ' '.ucwords($label_part);
                    }
                }

                $type = explode('(', $column->Type);
                switch ($type[0]) {
                    case 'int':
                        $parts_size = count($label_parts);
                        if ($label_parts[$parts_size - 1] == 'id') {
                            $form_fields[$counter]['type'] = 'look_up';
                            $Look_up_model_name = '';
                            foreach ($label_parts as $label_part) {
                                if ($label_part != 'id') {
                                    $Look_up_model_name .= ucwords($label_part);
                                }
                            }
                            $Look_up_model = 'App\\Models\\'.$Look_up_model_name;
                            if ($Look_up_model_name == 'Status') {
                                $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('status', '=', 1)->pluck('name', 'id');
                            } else {
                                $form_fields[$counter]['look_up_values'] = $Look_up_model::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
                            }
                        } else {
                            $form_fields[$counter]['type'] = 'text';
                        }
                        break;
                    case 'text':
                        $form_fields[$counter]['type'] = 'text_area';
                        break;
                    default:
                        $form_fields[$counter]['type'] = 'text';
                        break;
                }

                $form_fields[$counter]['label'] = $label;
                $form_fields[$counter]['field'] = $column->Field;
            }

            $counter++;
        }

        $parameters = [
            'form_fields' => $form_fields,
            'modelName' => $modelName,
            'model' => $model,
            'knownAs' => $knownAs,
        ];

        return $parameters;
    }

    /*
     * @return user if form successfully saved, else return false
     * */
    public static function update($id, $modelName, Request $request, $validate = true)
    {
        $lookUpClass = 'App\\Models\\'.$modelName;
        $requestClassName = 'App\Http\Requests\\'.$modelName.'Request';

        $requestClass = null;

        if (class_exists($requestClassName) && $validate) {
            $requestClass = new $requestClassName;

            if (! $requestClass->authorize()) {
                return abort(403);
            }
            //Validate form if request class set
            $request->validate($requestClass->rules());
        }

        $model = $lookUpClass::find($id);

        foreach ($request->request as $key => $value) {
            if (! in_array($key, ['_method', '_token'])) {
                $model->$key = $value;
            }
        }

        $model->save();

        return $model;
    }
}
