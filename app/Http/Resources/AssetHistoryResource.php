<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AssetHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "asset_id" => $this->asset_id,
            "user_id" => $this->user_id,
            "issued_at" => $this->issued_at,
            "is_active" => $this->is_active,
            "is_included_in_letter" => $this->is_included_in_letter,
            "include_in_letter" => $this->is_included_in_letter?->name,
            "note" => $this->note,
            "issued_to" => $this->user->name()
        ];
    }
}
