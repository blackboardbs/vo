<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'user_id' => auth()->id(),
            'count' => $this->count(),
            'notifications' => $this->take(5)->map(fn($user_notification) => [
                'id' => $user_notification->notification?->id,
                'name' => $user_notification->notification?->name,
                'link' => $user_notification->notification?->link,
                'created' => Carbon::parse($user_notification->notification?->created_at)->diffForHumans()
            ])
        ];
    }
}
