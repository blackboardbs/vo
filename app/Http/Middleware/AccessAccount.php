<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;

class AccessAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if(Auth::user() && Auth::user()->id > 0){
            $owner = User::first();
            try {
                $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);
                if ($response->status() != 404)
                    $request->session()->put('subscriptionDetail', $response->json());

                    $license = $response->json();

                    if (App::isProduction()){
                        $license_exp_date = Carbon::parse($license["subscription"]["billed_at"])->addMonth(1)->format('Y-m-d');
                        $exp_date = Carbon::parse($license["subscription"]["billed_at"])->addMonth(1)->addDays(7)->format('Y-m-d');
                        $today = Carbon::parse(now())->format('Y-m-d');
                    }
                        
                    // if($owner->hasAnyRole(['admin'])){
                    //     if(Carbon::parse($today)->diffInDays(Carbon::parse($exp_date)) > 7 && Carbon::parse($today)->addMonth(1) <= Carbon::parse($license_exp_date) ){
                    //         if($request->route()->getName() != 'ofsyaccount.index'){
                    //         return redirect()->route('ofsyaccount.index');
                    //         }
                    //     }
                    // }

            } catch (\Exception $e) {
                logger($e);
            }
        }

        return $next($request);
    }
}
