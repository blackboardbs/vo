<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Api;

class VerifyApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Access_Token');
        $user = $request->header('User');
        
        if (!$token) {
            return response()->json(['error' => 'Unauthorized no token'], 401);
        }

        if (!$user) {
            return response()->json(['error' => 'Unauthorized no user'], 401);
        }

        $api = Api::where('api_token', $token)->where('user', $user)->first();

        if (!$api) {
            return response()->json(['error' => 'Unauthorized no access'], 401);
        }

        return $next($request);
    }
}
