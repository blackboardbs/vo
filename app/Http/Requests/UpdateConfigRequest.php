<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'days_to_approve_leave' => 'required|integer',
            'utilization_hours_per_week' => 'required|integer',
            'home_page_message' => 'required|string',
            'auditing_records' => 'required|integer',
            'site_title' => 'required|string',
            'site_name' => 'required|string',
            'assessment_approver_user_id' => 'required|integer',
            'admin_email' => 'required|email',
            'cost_min_percent' => 'required|integer',
            'cost_min_colour' => 'required|string',
            'cost_mid_colour' => 'required|string',
            'cost_max_percent' => 'required|integer',
            'cost_max_colour' => 'required|string',
            'timesheet_weeks' => 'required|integer',
            'first_warning_days' => 'required|integer',
            'second_warning_days' => 'required|integer',
            'last_warning_days' => 'required|integer',
            'first_warning_percentage' => 'required|integer',
            'second_warning_percentage' => 'required|integer',
            'last_warning_percentage' => 'required|integer',
            'timesheet_weeks_future' => 'required|integer',
            'annual_leave_days' => 'required|integer',
        ];
    }
}
