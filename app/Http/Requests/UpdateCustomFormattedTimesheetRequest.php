<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomFormattedTimesheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'additional_text' => 'required|string',
            'start_date' => 'required|date',
            'template_id' => 'required|integer|not_in:0',
            'signature' => 'required|integer|not_in:0',
            'destination' => 'required|integer|not_in:0',
        ];
    }
}
