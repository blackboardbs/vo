<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSiteReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'consultant' => 'required|not_in:0',
            'customer' => 'required|not_in:0',
            'wbs' => 'required|not_in:0',
            'report_to_firstname' => 'required|string',
            'report_to_lastname' => 'required|string',
            'from_date' => 'required|date',
            'date_to' => 'required|date',
            'reporting_from_date' => 'required|date',
            'reporting_to_date' => 'required|date',
            'internal_review_emp_id' => 'required|not_in:0',
            'internal_review_date' => 'required|date',
            'status' => 'required|not_in:0',
            'task1' => 'nullable|string',
            'task2' => 'nullable|string',
            'task3' => 'nullable|string',
            'task4' => 'nullable|string',
            'task5' => 'nullable|string',
            'task6' => 'nullable|string',
            'task7' => 'nullable|string',
            'task8' => 'nullable|string',
            'task9' => 'nullable|string',
            'task10' => 'nullable|string',
            'issue1' => 'nullable|string',
            'issue2' => 'nullable|string',
            'issue3' => 'nullable|string',
            'issue4' => 'nullable|string',
            'issue5' => 'nullable|string',
            'issue6' => 'nullable|string',
            'issue7' => 'nullable|string',
            'issue8' => 'nullable|string',
            'issue9' => 'nullable|string',
            'issue10' => 'nullable|string',
            'risk1' => 'nullable|string',
            'risk2' => 'nullable|string',
            'risk3' => 'nullable|string',
            'risk4' => 'nullable|string',
            'risk5' => 'nullable|string',
            'risk6' => 'nullable|string',
            'risk7' => 'nullable|string',
            'risk8' => 'nullable|string',
            'risk9' => 'nullable|string',
            'risk10' => 'nullable|string',
            'escalation1' => 'nullable|string',
            'escalation2' => 'nullable|string',
            'escalation3' => 'nullable|string',
            'escalation4' => 'nullable|string',
            'escalation5' => 'nullable|string',
            'escalation6' => 'nullable|string',
            'escalation7' => 'nullable|string',
            'escalation8' => 'nullable|string',
            'escalation9' => 'nullable|string',
            'escalation10' => 'nullable|string',
            'suggestion1' => 'nullable|string',
            'suggestion2' => 'nullable|string',
            'suggestion3' => 'nullable|string',
            'suggestion4' => 'nullable|string',
            'suggestion5' => 'nullable|string',
            'suggestion6' => 'nullable|string',
            'suggestion7' => 'nullable|string',
            'suggestion8' => 'nullable|string',
            'suggestion9' => 'nullable|string',
            'suggestion10' => 'nullable|string',
            'next_step1' => 'nullable|string',
            'next_step2' => 'nullable|string',
            'next_step3' => 'nullable|string',
            'next_step4' => 'nullable|string',
            'next_step5' => 'nullable|string',
            'next_step6' => 'nullable|string',
            'next_step7' => 'nullable|string',
            'next_step8' => 'nullable|string',
            'next_step9' => 'nullable|string',
            'next_step10' => 'nullable|string',
        ];
    }
}
