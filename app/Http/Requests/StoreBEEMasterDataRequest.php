<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBEEMasterDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'project_type' => 'required|not_in:0',
            'naturalization_id' => 'required|not_in:0',
            'race_id' => 'required|not_in:0',
            'bbbee_level' => 'required|not_in:0',
            'bbbee_race' => 'required|not_in:0',
            'gender' => 'required|not_in:0',
            'disability' => 'required|not_in:0',
        ];
    }
}
