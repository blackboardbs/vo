<?php

namespace App\Http\Requests;

use App\Models\Module;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateTimeSheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();
        return (Auth::user()->canAccess($module[0]->id, 'update_all') || Auth::user()->canAccess($module[0]->id, 'update_team'));
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'employee_id' => 'required|integer',
            'year_week' => 'required|string',
            //'client_id' => 'required|integer',
            'project_id' => 'required|integer',
        ];
    }
}
