<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'customer_id' => 'required|not_in:0',
            /*'consultant_emp_id' => 'required|not_in:0',
            'hour_rate' => 'required|numeric',
            'duration' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',*/
            'scope_of_work' => 'string|required',
            'status_id' => 'required|not_in:0',
            //'terms_version' => 'required|not_in:0',
            /*'customer_ref' => 'string|required',*/
            'amount' => 'required|numeric'
        ];
    }
}
