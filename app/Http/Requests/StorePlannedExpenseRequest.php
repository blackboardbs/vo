<?php

namespace App\Http\Requests;

use App\Services\PlannedExpenseService;
use Illuminate\Foundation\Http\FormRequest;

class StorePlannedExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(PlannedExpenseService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'account_id' => 'required|not_in:0',
            'account_element_id' => 'required|not_in:0',
            'description' => 'required|string',
            'company_id' =>'required|not_in:0',
            'plan_exp_date' => 'required|date_format:Y-m-d',
            'plan_exp_value' => 'required|numeric',
            'status' => 'required|not_in:0',
        ];
    }
}
