<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'subject' => 'required|string',
            'description' => 'required|string',
            'due_date' => 'required|date',
            'assigned_user_id' => 'required|integer|not_in:0',
            'status_id' => 'required|integer|not_in:0',
            'estimated_effort' => 'numeric|nullable',
            'actual_hours' => 'numeric|nullable',
            /*'file' => 'required',
            'file.*' => 'mimes:doc,pdf,docx,txt,zip'*/
        ];
    }
}
