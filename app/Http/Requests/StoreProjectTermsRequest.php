<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectTermsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'terms_version' => 'string|required',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'exp_travel' => 'string|required',
            'exp_parking' => 'string|required',
            'car_rental' => 'string|required',
            'exp_flights' => 'string|required',
            'exp_other' => 'string|required',
            'exp_accommodation' => 'string|required',
            'exp_out_of_town' => 'string|required',
            'exp_toll' => 'string|required',
            'exp_data' => 'string|required',
            'exp_hours_of_work' => 'string|required',
        ];
    }
}
