<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobSpecRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'reference_number' => 'string',
            'role_id' => 'nullable|integer',
            'position_description' => 'string',
            'profession_id' => 'nullable|integer',
            'speciality_id' => 'nullable|integer',
            'client' => 'nullable|string',
            'placement_by' => 'string',
            'customer_id' => 'required|integer',
            'location' => 'nullable|string',
            'employment_type_id' => 'nullable|integer',
            'work_type_id' => 'nullable|integer',
            'salary' => 'nullable|string',
            'hourly_rate_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'hourly_rate_maximum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_maximum' => 'nullable|numeric|between:0,9999999999.99',
            'start_date' => 'date',
            'applicaiton_closing_date' => 'date',
            'skills_id' => 'array|nullable',
            'priority' => 'integer|nullable',
            'b_e_e' => 'integer|nullable',
            'recruiter_id' => 'integer|nullable',
            'gender' => 'integer|nullable',
            'duties_and_reponsibilities' => 'nullable|string',
            'desired_experience_and_qualification' => 'nullable|string',
            'package_and_renumeration' => 'nullable|string',
            'respond_to' => 'nullable|string',
            'special_note' => 'nullable|string',
            'status_id' => 'nullable|integer',
            'origin_id' => 'nullable|integer',
        ];
    }
}
