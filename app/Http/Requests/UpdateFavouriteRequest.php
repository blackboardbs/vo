<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFavouriteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'module_name' => 'required|string',
            'button_function' => 'required|array',
            'route' => 'required|string',
            'status_id' => 'required|not_in:0',
            'role_id' => 'required|array'
        ];
    }
}
