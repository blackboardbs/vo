<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'feature_id' => 'integer|required|not_in:0',
            'status_id' => 'integer|required|not_in:0',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'billable' => 'numeric|nullable',
            'hours_planned' => 'numeric|nullable',
            'estimate_cost' => 'numeric|nullable',
            'estimate_income' => 'numeric|nullable',
            'confidence_percentage' => 'numeric|nullable',
        ];
    }
}
