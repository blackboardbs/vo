<?php

namespace App\Http\Requests;

use App\Services\UtilizationService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUtilizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(UtilizationService $service): bool
    {
        return $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'yearwk' => ['required', 'string', Rule::unique('plan_utilization')->ignore($this->route("utilization")?->id)],
            'res_no' => 'required|integer',
            'wk_hours' => 'required|integer',
            'status_id' => 'required|integer',
        ];
    }
}
