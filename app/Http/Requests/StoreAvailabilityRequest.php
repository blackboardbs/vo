<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAvailabilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'avail_status' => 'required|not_in:0',
            'status_date' => 'required|date',
            'avail_note' => 'nullable|string',
            'avail_date' => 'nullable|date',
            'rate_hour' =>'nullable|string',
            'rate_month' =>'nullable|string',
            'status_id' =>'required|not_in:0',
        ];
    }
}
