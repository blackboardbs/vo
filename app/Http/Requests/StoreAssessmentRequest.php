<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAssessmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'assessment_id' => 'required|integer',
            'assessment_master_id.*' => 'required|integer',
            'assessment_measure_detail_id.*' => 'required|integer',
            'assessment_measure_details.*' => 'required|string',
            'assessment_measure_score.*' => 'required|integer',
            'assessment_note.*' => 'nullable|string'
        ];
    }
}
