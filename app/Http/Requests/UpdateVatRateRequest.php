<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVatRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'status' => 'required|not_in:0',
            'vat_rate' => 'required|regex:/^\d*(\.\d{2})?$/',
            'start_date' => 'required|string',
            'vat_code' => 'required|not_in:0',
            'end_date' => 'required|string',
        ];
    }
}
