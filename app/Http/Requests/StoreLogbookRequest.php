<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLogbookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            //
            'log_date' => 'required|date',
            'resource' => 'required|integer|exists:users,id',
            'description' => 'nullable|string',
            'opening_km' => 'nullable|numeric|min:0',
            'km' => 'nullable|numeric|min:0',
            'fuel_cost' => 'nullable|numeric|min:0',
            'maint_cost' => 'nullable|numeric|min:0',
            'status' => 'required|exists:status,id',
        ];
    }
}
