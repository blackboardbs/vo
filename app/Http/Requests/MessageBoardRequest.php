<?php

namespace App\Http\Requests;

use App\Services\MessageBoardService;
use Illuminate\Foundation\Http\FormRequest;

class MessageBoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(MessageBoardService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'message' => 'string|required',
            'message_date' => 'date|required',
            'status_id' => 'not_in:0|integer|required'
        ];
    }
}
