<?php

namespace App\Http\Requests;

use App\Models\Module;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();
        return Auth::user()->canAccess($module->id, 'create_all')
            || Auth::user()->canAccess($module->id, 'create_team');
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'ref' => 'required',
            'project_type_id' => 'required',
            'manager_id' => 'required|not_in:0',
            'company_id' => 'required|not_in:0',
            'customer_id' => 'required|not_in:0',
            'status_id' => 'required|not_in:0',
            'terms_version' => 'required|not_in:0',
            'billable' => 'required',
            //'hours_of_work' => 'string|required',
            'consultants' => 'nullable|array|min:1',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',

            'total_project_hours' => 'nullable|numeric|between:0,9999999999.99',
            'fixed_price_labour' => 'nullable|numeric|between:0,9999999999.99',
            'fixed_price_expenses' => 'nullable|numeric|between:0,9999999999.99',
            'total_fixed_price' => 'nullable|numeric|between:0,9999999999.99',
            'project_fixed_cost_labour' => 'nullable|numeric|between:0,9999999999.99',
            'project_fixed_cost_expense' => 'nullable|numeric|between:0,9999999999.99',
            'project_total_fixed_cost' => 'nullable|numeric|between:0,9999999999.99',
            'billing_cycle_id' => 'nullable|integer',
            'billing_timesheet_templete_id' => 'nullable|integer|exists:billing_period_styles,id',
            'customer_invoice_contact_id' => 'nullable|integer|exists:invoice_contacts,id'
        ];
    }
}
