<?php

namespace App\Http\Requests;

use App\Services\VendorService;
use Illuminate\Foundation\Http\FormRequest;

class StoreVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(VendorService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'vendor_name' => 'required|string',
            'business_reg_no' => 'nullable|string',
            'account_manager' => 'required|not_in:0',
            'assignment_approver' => 'required|not_in:0',
            'contact_firstname' => 'nullable|string',
            'contact_lastname' => 'nullable|string',
            'phone' => 'nullable|string',
            'cell' => 'nullable|string',
            'email' => 'required|email',
            'payment_terms_days' => 'required|integer',
            'status_id' => 'required|not_in:0',
            'bank_name' => 'nullable|string',
            'branch_code' => 'nullable|string',
            'bank_acc_no' => 'nullable|string',
            'billing_period' => 'nullable|string',
            'branch_name' => 'nullable|string',
            'account_name' => 'nullable|string',
            'swift_code' => 'nullable|string',
            'bank_account_type_id' => 'integer|nullable',
            'invoice_contact_id' => 'integer|nullable',
            'service_agreement_id' => 'nullable|exists:advanced_templates,id',
        ];
    }
}
