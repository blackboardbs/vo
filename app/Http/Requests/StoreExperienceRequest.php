<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'company' => 'required_if:company_id,0',
            'company_id' => 'required_without:company',
            'start_year' => 'required|not_in:0',
            'start_month' => 'required|not_in:0',
            'end_year' => 'required_if:current,0',
            'end_month' => 'required_if:current,0',
            'current' => 'required_if:end_year,0|required_if:end_month,0',
            'role' => 'nullable|string',
            'project' => 'nullable|string',
            'responsibility' => 'required|string',
            'tools' => 'nullable|string',
            //'row_order' => 'required|not_in:0',
            'status_id' => 'required|not_in:0',
        ];
    }
}
