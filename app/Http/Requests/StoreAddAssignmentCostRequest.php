<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddAssignmentCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'string|required',
            'account' => 'required',
            'account_element' => 'required|not_in:0',
            'cost' => 'string|required',
            'payment_freq' => 'string|nullable',
            'note' => 'string|nullable',
            //'payment_method' => 'required|not_in:0',
            'status' => 'required|not_in:0',
        ];
    }
}
