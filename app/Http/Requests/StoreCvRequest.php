<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            /*'res_firstname' => 'required|string',
            'res_middlename' => 'required|string',
            'res_lastname' => 'required|string',*/
            'resource_pref_name' => 'nullable|string',
            'resource_middle_name' => 'nullable|string',
            'id_no' => 'nullable|numeric',
            'date_of_birth' => 'nullable|date',
            'place_of_birth' => 'nullable|string',
            'nationality' => 'nullable|string',
            'passport_no' => 'nullable|string',
            'drivers_license' => 'nullable|string',
            'criminal_offence' => 'nullable|string',
            'health' => 'nullable|string',
            //'marital_status' => 'required|not_in:0',
            'home_language' => 'nullable|string',
            'other_language' => 'nullable|string',
            'cell' => 'nullable|string',
            'cell2' => 'nullable|string',
            'fax' => 'nullable|string',
            'personal_email' => 'nullable|string',
            'main_qualification' => 'nullable|string',
            'main_skill' => 'nullable|string',
            'contract_house' => 'nullable|string',
            'status_id' => 'required|not_in:0',
            'hourly_rate_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'hourly_rate_maximum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_maximum' => 'nullable|numeric|between:0,9999999999.99',

            'current_location' => 'nullable|string',
            'dependants' => 'nullable|numeric|between:0,9999999',
            'own_transport' => 'nullable|numeric|between:0,9',
            'current_salary_nett' => 'nullable|numeric|between:0,9999999999.99',
            'expected_salary_nett' => 'nullable|numeric|between:0,9999999999.99',
            'school_matriculated' => 'nullable|string',
            'highest_grade' => 'nullable|string',
            'school_completion_year' => 'nullable|numeric|between:0,9999999999',
        ];
    }
}
