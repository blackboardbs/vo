<?php

namespace App\Http\Requests;

use App\Models\Module;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        return (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'));
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'start_date' => 'required',
            'end_date' => 'required',
            'employee_id' => 'required|integer|exists:users,id',
            'assignment_approver' => 'required|not_in:0',
            'hours' => 'required|numeric|between:0,9999999999.99',
            'note_1' => 'required',
            'billable' => 'required',
            'status' => 'required|not_in:0',
            'rate' => 'nullable|numeric|between:0,9999999999.99',
            'fixed_labour_cost' => 'nullable|numeric|between:0,9999999999.99',
            'fixed_price' => 'nullable|numeric|between:0,9999999999.99',
            'capacity_allocation_percentage' => 'nullable|numeric|between:0,9999999999.99',
            'internal_cost_rate' => 'nullable|numeric|between:0,9999999999.99',
            'external_cost_rate' => 'nullable|numeric|between:0,9999999999.99',
            'invoice_rate' => 'nullable|numeric|between:0,9999999999.99',
            'internal_cost_rate_sec' => 'nullable|numeric|between:0,9999999999.99',
            'external_cost_rate_sec' => 'nullable|numeric|between:0,9999999999.99',
            'invoice_rate_sec' => 'nullable|numeric|between:0,9999999999.99',
            'rate_sec' => 'nullable|numeric|between:0,9999999999.99'
        ];
    }
}
