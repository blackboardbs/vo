<?php

namespace App\Http\Requests;

use App\Models\Module;
use App\Services\CompanyService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(CompanyService $service): bool
    {
        return $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'company_name' => 'required|string|max:255',
            'email' => 'required||email|max:255',
            'status_id' => 'required|not_in:0',
            'country_id' => 'exists:country,id',
            'registration_number' => 'nullable|string|max:255',
            'vat_number' => 'nullable|string|max:20',
            'contact_firstname' => 'nullable|string|max:255',
            'contact_lastname' => 'nullable|string|max:255',
            'cell' => 'nullable|string|max:15',
            'address_ln1' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'fax' => 'nullable|string',
            'suburb' => 'nullable|string|max:255',
            'web' => 'nullable|string|max:255',
            'state_province' => 'nullable|string|max:30',
            'postal_zip' => 'nullable|string',
            'bbbee_level_id' => 'nullable',
            'bbbee_certificate_expiry' => 'nullable|date',
            'black_ownership' => 'nullable|numeric',
            'black_female_ownership' => 'nullable|numeric',
            'black_voting_rights' => 'nullable|numeric',
            'black_female_voting_rights' => 'nullable|numeric',
            'bbbee_recognition_level' => 'nullable|numeric',
            'bank_name' => 'nullable|string',
            'branch_code' => 'nullable',
            'bank_acc_no' => 'nullable',
            'account_name' => 'string|nullable',
            'branch_name' => 'string|nullable',
            'bank_account_type_id' => 'integer|nullable',
            'swift_code' => 'string|nullable'
        ];
    }
}
