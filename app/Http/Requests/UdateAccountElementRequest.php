<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UdateAccountElementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'account_id' => 'required|integer',
            'description' => 'required|string',
            'account_element' => 'required|string',
            'status' => 'required|not_in:0'
        ];
    }
}
