<?php

namespace App\Http\Requests;

use App\Services\UserService;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(UserService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'phone' => 'nullable|string',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif',
            'role' => 'required|not_in:0',
            'usertype_id' => 'required_with:onboarding',
            'appointment_manager_id' => 'required_with:usertype_id',
            'expiry_date' => 'required|date'
        ];
    }
}
