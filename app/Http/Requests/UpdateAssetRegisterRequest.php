<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAssetRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'assetclass' => 'required|not_in:0',
            'asset_nr' => ['required', 'string',Rule::unique('asset_register')->ignore($this->route('assetreg')->id)],
            'status' => 'required|not_in:0',
            'make' => 'string|nullable',
            'original_value' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'model' => 'string|nullable',
            'aquire_date' => 'date|nullable',
            'retire_date' => 'date|nullable',
            'notes' => 'string|nullable',
            'est_life' => 'integer|nullable',
            'warranty_months' => 'integer|nullable',
            'warranty_expiry_date' => 'date|nullable',
            'warranty_note' => 'string|nullable',
            'support_months' => 'integer|nullable',
            'support_expiry_date' => 'date|nullable',
            'support_provided_by' => 'string|nullable',
            'support_note' => 'string|nullable',
            'contact_number' => 'string|nullable',
            'contact_email' => 'email|nullable',
            'reference_number' => 'string|nullable',
            'ram' => 'string|nullable',
            'hdd' => 'string|nullable',
            'screen_size' => 'string|nullable',
            'supplier' => 'string|nullable'
        ];
    }
}
