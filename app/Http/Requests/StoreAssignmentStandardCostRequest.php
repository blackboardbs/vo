<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAssignmentStandardCostRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'standard_cost_rate' => 'required|numeric',
            'min_rate' => 'required|numeric',
            'max_rate' => 'required|numeric',
            'status' => 'required|not_in:0',
        ];
    }
}
