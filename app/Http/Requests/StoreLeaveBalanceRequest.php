<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeaveBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'resource' => 'integer|required|not_in:0',
            'opening_balance' => 'required|numeric',
            'balance_date' => 'date|required',
            'leave_per_cycle' => 'integer|required|numeric|between:0,999.999',
            'status' => 'integer|required'
        ];
    }
}
