<?php

namespace App\Http\Requests;

use App\Services\CustomerService;
use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(CustomerService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'customer_name' => 'required|string',
            'business_reg_no' => 'nullable|string',
            'vat_no' => 'nullable|string',
            'account_manager' => 'required|not_in:0|exists:users,id',
            'contact_firstname' => 'required|string',
            'contact_lastname' => 'required|string',
            'contact_birthday' => 'nullable|date_format:Y-m-d',
            'phone' => 'nullable|string',
            'cell' => 'nullable|string',
            'fax' => 'nullable|string',
            'email' => 'nullable|email',
            'web' => 'nullable|url:http,https',
            'postal_address_line1' => 'nullable|string',
            'postal_address_line2' => 'nullable|string',
            'postal_address_line3' => 'nullable|string',
            'city_suburb' => 'nullable|string',
            'postal_zipcode' => 'nullable|string',
            'street_address_line1' => 'nullable|string',
            'street_address_line2' => 'nullable|string',
            'street_address_line3' => 'nullable|string',
            'street_city_suburb' => 'nullable|string',
            'street_zipcode' => 'nullable|string',
            'state_province' => 'nullable|string',
            'country_id' => 'required|exists:country,id',
            'payment_terms_days' => 'nullable|integer',
            'billing_note' => 'nullable|string',
            'bbbee_level' => 'nullable|integer',
            'bbbee_certificate_expiry' => 'nullable|date_format:Y-m-d',
            'bbbee_level_preference' => 'nullable|integer',
            'status' => 'required|not_in:0',
            'commission' => 'nullable',
            'billing_cycle_id' => 'nullable|integer',
            'logo' => 'nullable|image'
        ];
    }
}
