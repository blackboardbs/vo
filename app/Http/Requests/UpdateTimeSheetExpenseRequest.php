<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTimeSheetExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'date' => 'required|date',
            'is_billable' => 'required|integer',
            'claim' => 'required|integer',
            'description' => 'required|string',
            'amount' => 'required|numeric',
            'account_id' => 'required|integer',
        ];
    }
}
