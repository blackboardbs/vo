<?php

namespace App\Http\Requests;

use App\Services\AnniversaryService;
use Illuminate\Foundation\Http\FormRequest;

class AnniversaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(AnniversaryService $service): bool
    {
        return $service->hasCreatePermissions() || $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'anniversary_date' => 'required|date',
            //'relationship' => 'required',
            'name' => 'nullable|string',
            'emp_id' => 'required|not_in:0',
            'anniversary' => 'required|not_in:0',
            'email' => 'nullable|email',
            'phone' => 'nullable',
            'contact_email' => 'nullable',
            'contact_phone' => 'nullable',
            'status_id' => 'required|not_in:0',
        ];
    }
}
