<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateScoutingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'resource_first_name' => 'required|string',
            'resource_last_name' => 'required|string',
            'availability' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'role_id' => 'required|integer|not_in:0',
            'process_status_id' => 'required|integer|not_in:0',
            'interview_status_id' => 'required|integer|not_in:0',
            'hourly_rate_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'hourly_rate_maximum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_minimum' => 'nullable|numeric|between:0,9999999999.99',
            'monthly_salary_maximum' => 'nullable|numeric|between:0,9999999999.99',
            /*'user_id' => 'integer',*/
            /*'vendor_id' => 'integer',
            'technical_rating_id' => 'integer',
            'business_rating_id' => 'integer',*/
            'resource_level_id' => 'required|integer|not_in:0',
            /*'accessed_by' => 'integer',*/
            'estimated_hourly_rate' => 'nullable|string',
            /*'referral' => 'integer',*/
            /*'status_id' => 'required|integer|not_in:0'*/
        ];
    }
}
