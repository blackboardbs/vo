<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            //
            'resource' => 'required|not_in:0',
            'no_of_days' => 'required|numeric|gt:0',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            //'contact_details' => 'required|string',
            'leave_type' => 'required|not_in:0'
        ];
    }
}
