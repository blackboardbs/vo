<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateQualificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'year_complete' => 'required',
            'qualification' => 'required|string',
            'institution' => 'nullable|string',
            //'row_order' => 'required|not_in:0',
            'status_id' => 'required|not_in:0',
            'subjects' => 'nullable|string',
        ];
    }
}
