<?php

namespace App\Http\Requests;

use App\Services\UtilizationService;
use Illuminate\Foundation\Http\FormRequest;

class StoreUtilizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(UtilizationService $service): bool
    {
        return $service->hasCreatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            //
            'yearwk' => 'string|unique:plan_utilization|required_without_all:start_date,end_date',
            'start_date' => 'required_without:yearwk',
            'end_date' => 'required_without:yearwk',
            'res_no' => 'required|integer',
            'cost_res_no' => 'required|integer',
            'wk_hours' => 'required|integer',
            'cost_wk_hours' => 'required|integer',
            'status_id' => 'required|not_in:0',

        ];
    }
}
