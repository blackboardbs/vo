<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'consultant' => 'required|integer',
            'user_story_id' => 'required|integer|not_in:0',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'hours_planned' => 'required|integer',
            'status' => 'required|integer',
        ];
    }
}
