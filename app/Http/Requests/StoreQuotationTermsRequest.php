<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuotationTermsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'quotation' => 'required|not_in:0',
            'terms_id' => 'required|not_in:0',
            'term' => 'required|string',
            'freeze' => 'required|integer',
            'status' => 'required|not_in:0'
        ];
    }
}
