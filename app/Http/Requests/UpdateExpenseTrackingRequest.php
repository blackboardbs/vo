<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExpenseTrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            //
            'account' => 'required|not_in:0',
            'account_element_id' => 'required|not_in:0',
            'description' => 'required|string',
            'company' => 'required|not_in:0',
            'exp_date' => 'required|date',
            'amount' => 'required|numeric',
            'cost_center' => 'required|not_in:0',
            'payment' => 'nullable|string',
            'resource' => 'required_unless:assignment,0',
            'yearwk' => 'required_if:assignment,!=,0|not_in:0',
            'status' => 'required|not_in:0',
            //'approved_by' => 'required|not_in:0',
            //'approved_on' => 'date',
            //'approval_status' => 'integer|required',
            //'payment_date' => 'date|required',
            //'payment_status' => 'integer|required'

        ];
    }
}
