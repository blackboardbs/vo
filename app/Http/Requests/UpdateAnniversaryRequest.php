<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAnniversaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'anniversary_date' => 'required|date',
            //'relationship' => 'required',
            'aname' => 'nullable|string',
            'resource' => 'required|not_in:0',
            'anniversary' => 'required|not_in:0',
            'email' => 'nullable|email',
            'phone' => 'nullable',
            'contact_email' => 'nullable',
            'contact_phone' => 'nullable',
            'status' => 'required|not_in:0',
        ];
    }
}
