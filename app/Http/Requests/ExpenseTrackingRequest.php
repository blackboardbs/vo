<?php

namespace App\Http\Requests;

use App\Models\Account;
use Illuminate\Foundation\Http\FormRequest;

class ExpenseTrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'account' => 'required|integer|exists:App\Models\Account,id',
            'account_element' => 'required|integer|exists:App\Models\AccountElement,id',
            'description' => 'required|string',
            'company' => 'required|integer|exists:App\Models\Company,id',
            'exp_date' => 'required|date',
            'amount' => 'required|numeric|min:0',
            'yearwk' => 'required_with:assignment',
            'resource' => 'required|integer|exists:App\Models\User,id',
            'status' => 'required|integer|exists:App\Models\Status,id',
        ];
    }
}
