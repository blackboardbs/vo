<?php

namespace App\Http\Requests;

use App\Enum\YesOrNoEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreVendorInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'consultant' => 'required|not_in:0',
            'project' => 'required|not_in:0',
            'period' => 'required_without_all:date_from,date_to',
            'date_from' => 'required_if:period,==,0|date|required_with:date_to|nullable',
            'date_to' => 'required_with:date_from|after_or_equal:date_from|nullable',
            'invoice_number' => 'string|nullable',
            'invoice_message' => 'string|nullable',
            'vat_rate_id' => 'nullable|exists:vat_rate,id',
            'currency' => ['integer', Rule::in([0, 1])]
        ];
    }
}
