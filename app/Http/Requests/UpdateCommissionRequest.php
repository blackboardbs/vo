<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'min_rate' => 'nullable|numeric|between:0,9999999999.99',
            'hour01' => 'nullable|numeric|between:0,9999999999.99',
            'comm01' => 'nullable|numeric|between:0,9999999999.99',
            'hour02' => 'nullable|numeric|between:0,9999999999.99',
            'comm02' => 'nullable|numeric|between:0,9999999999.99',
            'hour03' => 'nullable|numeric|between:0,9999999999.99',
            'comm03' => 'nullable|numeric|between:0,9999999999.99',
            'hour04' => 'nullable|numeric|between:0,9999999999.99',
            'comm04' => 'nullable|numeric|between:0,9999999999.99',
            'hour05' => 'nullable|numeric|between:0,9999999999.99',
            'comm05' => 'nullable|numeric|between:0,9999999999.99',
            'hour06' => 'nullable|numeric|between:0,9999999999.99',
            'comm06' => 'nullable|numeric|between:0,9999999999.99',
            'hour07' => 'nullable|numeric|between:0,9999999999.99',
            'comm07' => 'nullable|numeric|between:0,9999999999.99',
            'hour08' => 'nullable|numeric|between:0,9999999999.99',
            'comm08' => 'nullable|numeric|between:0,9999999999.99',
            'hour09' => 'nullable|numeric|between:0,9999999999.99',
            'comm09' => 'nullable|numeric|between:0,9999999999.99',
            'hour10' => 'nullable|numeric|between:0,9999999999.99',
            'comm10' => 'nullable|numeric|between:0,9999999999.99',
            'hour11' => 'nullable|numeric|between:0,9999999999.99',
            'comm11' => 'nullable|numeric|between:0,9999999999.99',
            'hour12' => 'nullable|numeric|between:0,9999999999.99',
            'comm12' => 'nullable|numeric|between:0,9999999999.99',
            'status' => 'required|not_in:0'
        ];
    }
}
