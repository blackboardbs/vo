<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDefaultDashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'role_id' => "required|integer",
            'dashboard_name' => "required|string",
            'status_id' => "not_in:0|required",
            'top_components' => "required|array|max:4",
            'charts_tables' => "required|array"
        ];
    }
}
