<?php

namespace App\Http\Requests;

use App\Services\CustomerService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(CustomerService $service): bool
    {
        return $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'customer_name' => 'required|string',
            /*'business_reg_no' => 'required|string',*/
            'account_manager' => 'required|not_in:0',
            'contact_firstname' => 'required|string',
            'contact_lastname' => 'required|string',
            /*'payment_terms_days' => 'required',
            'phone' => 'required|string',
            'cell' => 'required|string',*/
            'email' => 'nullable|email',
            'country_id' => 'required|not_in:0',
            'status' => 'required|not_in:0',
            'billing_cycle_id' => 'nullable|integer'
        ];
    }
}
