<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistributionListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'from_query_type_id' => 'integer|required',
            //'from_query_filters' => 'string|required',
            //'from_query' => 'string|required',
            'to_query_type_id' => 'integer|required',
            //'to_query_filters' => 'string|required',
            //'to_query' => 'string|required',
            'next_run_date' => 'date|required',
            'hour' => 'required',
            'minute' => 'required',
            'run_frequency_id' => 'integer|required',
            'status_id' => 'integer|required'
        ];
    }
}
