<?php

namespace App\Http\Requests;

use App\Models\Module;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreTimeSheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();
        return (Auth::user()->canAccess($module[0]->id, 'create_all') || Auth::user()->canAccess($module[0]->id, 'create_team'));
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'employee_id' => 'required|integer|not_in:0',
            'year_week' => 'required|string|not_in:0',
            //'client_id' => 'required|integer|not_in:0',
            'project_id' => 'required|integer',
        ];
    }
}
