<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingPeriodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'billing_cycle_id' => 'required|integer',
            'year' => 'required|string|size:4',
            'period_name' => 'required|string',
            'period_number' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ];
    }
}
