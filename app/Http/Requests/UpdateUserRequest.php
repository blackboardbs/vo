<?php

namespace App\Http\Requests;

use App\Services\UserService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(UserService $service): bool
    {
        return $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$this->route('user')?->id,
            'phone' => 'nullable|string',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif',
            'role' => 'required|not_in:0',
            'status' => 'required|not_in:0'
            ];
    }
}
