<?php

namespace App\Http\Requests;

use App\Enum\FrequencyEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
       $rules = [
           'company_id' => 'required|exists:company,id',
           'customer_id' => 'required|exists:customer,id',
           'interval' => 'numeric|min:1|nullable',
           'project_id' => 'nullable|exists:projects,id',
           'resource_id' => 'nullable|exists:users,id',
           'inv_ref' => 'string|nullable',
           'cust_inv_ref' => 'string|nullable',
           'invoice_date' => 'date|nullable',
           'due_date' => 'date|nullable',
           'invoice_notes' => 'string|nullable',
           'occurrences' => 'nullable|integer|min:0',
           'expiry_date' => 'date|after:first_invoice_date',
           'currency' => 'string|required',
           'email_subject' => 'string|nullable',
           'emails' => 'array|nullable'
       ];

       if($this->exists('is_recurring')) {
           $rules['first_invoice_date'] = ['date', 'required'];
           $rules['due_days_from_invoice_date'] = 'required|integer|min:1';
           $rules['frequency'] = [Rule::enum(FrequencyEnum::class), 'required'];
           if ($this->isMethod('store')){
               $rules['first_invoice_date'][] = 'after_or_equal:today';
           }
       }

        return $rules;
    }
}
