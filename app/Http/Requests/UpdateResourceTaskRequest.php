<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateResourceTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'task_number' => 'nullable|string',
            'subject' => 'required|string',
            'description' => 'required|string',
            'due_date' => 'required|string',
            'task_user' => 'required|integer',
            'status' => 'required|integer',
        ];
    }
}
