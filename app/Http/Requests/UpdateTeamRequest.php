<?php

namespace App\Http\Requests;

use App\Services\TeamService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(TeamService $service): bool
    {
        return $service->hasUpdatePermissions();
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'team_name' => 'required|string',
            'team_manager' => 'required|integer|not_in:0',
            'status_id' => 'required|integer|not_in:0',
        ];
    }
}
