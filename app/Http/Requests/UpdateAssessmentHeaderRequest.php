<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAssessmentHeaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'customer_id' => 'nullable|exists:customer,id',
            'assessor_id' => 'required|exists:users,id',
            'assessment_date' => 'nullable|date',
            'next_assessment' => 'nullable|date|after_or_equal:assessment_date',
            'assessment_start' => 'nullable|date',
            'assessment_end' => 'nullable|date|after_or_equal:assessment_start',
        ];
    }
}
