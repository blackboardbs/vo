<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CVReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'last_name' => 'string|required',
            'email' => 'string|required',
            'position' => 'string',
            'phone' => 'string|required',
            'company' => 'string'
        ];
    }
}
