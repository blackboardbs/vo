<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAssignmentStandardCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'standard_cost_rate' => 'required|numeric',
            'min_rate' => 'required|numeric',
            'max_rate' => 'required|numeric',
            'status' => 'required|not_in:0',
        ];
    }
}
