<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssetConditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'note' => 'string|required',
            'include_in_acceptance_letter' => 'integer|required'
        ];
    }
}
