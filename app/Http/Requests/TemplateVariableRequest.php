<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplateVariableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'display_name' => ['required', 'string', 'max:191'],
            'variable' => ['required', 'string', 'max:191'],
            'template_type_id' => ['required', 'integer', 'exists:template_type,id'],
            'status_id' => ['required', 'integer', 'exists:status,id'],
        ];
    }
}
