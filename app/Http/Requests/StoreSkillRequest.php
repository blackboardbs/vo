<?php

namespace App\Http\Requests;

use App\Models\Module;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $module = Module::where('name', '=', 'App\Models\CVSkill')->first();
        return Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team');
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'res_skill' => 'required_if:tool_id, == , 0',
            //'tool_id' => 'required|not_in:0',
            'tool' => 'required_if:tool_id,16',
            'years_experience' => 'required|string',
            'skill_level' => 'required|not_in:0',
            //'row_order' => 'required|not_in:0',
            'status_id' => 'required|not_in:0'
        ];
    }
}
