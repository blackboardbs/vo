<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSiteReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            // 'consultant' => 'required|not_in:0',
            'customer_id' => 'required|not_in:0',
            'project_id' => 'required|not_in:0',
            // 'report_to_firstname' => 'nullable|string',
            // 'report_to_lastname' => 'nullable|string',
            // 'from_date' => 'nullable|date',
            // 'date_to' => 'nullable|date',
            // 'reporting_from_date' => 'nullable|date',
            // 'reporting_to_date' => 'nullable|date',
            //'internal_review_emp_id' => 'required|not_in:0',
            //'internal_review_date' => 'required|date',
            // 'status' => 'required|not_in:0',
        ];
    }
}
