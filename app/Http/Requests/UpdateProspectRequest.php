<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'prospect_name' => 'required|string',
            'solution' => 'nullable|string',
            'scope' => 'nullable|string',
            'hours' => 'nullable|integer',
            'value' => 'required|integer',
            'resources' => 'nullable|string',
            'decision_date' => 'nullable|date',
            'est_start_date' => 'nullable|date',
            'chance' => 'required',
            'contact' => 'nullable|string',
            'status_id' => 'required|not_in:0'
        ];
    }
}
