<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreResourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'emp_no' => 'nullable|string',
            'emp_id_no' => 'nullable|numeric',
            'user_id' => 'nullable|not_in:0',
            /*'emp_firstname' => 'required|string',
            'emp_lastname' => 'required|string',*/
            'emp_prefname' => 'nullable|string',
            'join_date' => 'nullable|date',
            'emp_nationality' => 'nullable|string',
            'emp_type' => 'nullable|not_in:0',
            'emp_position' => 'nullable|not_in:0',
            'emp_level' => 'nullable|not_in:0',
            'emp_commision' => 'nullable|not_in:0',
            'cost_centre' => 'required|not_in:0',
            'emp_reportto' => 'nullable|not_in:0',
            'status_id' => 'required|not_in:0',
            'project_type' => 'nullable|not_in:0',
            'utilization_from' => 'nullable|date',
            'utilization_to' => 'nullable|date',
            'emp_passport' => 'nullable|numeric',
            'emp_marital_status' => 'nullable|not_in:0',
            'emp_health' => 'nullable|string',
            'home_language' => 'nullable|string',
            'other_language' => 'nullable|string',
            'dob' => 'nullable|date',
            'place_of_birth' => 'nullable|string',
            'criminal_rec' => 'nullable|string',
            'drivers_lic' => 'nullable|string',
            'emp_main_qualification' => 'nullable|string',
            'emp_main_skill' => 'nullable|string',
            'emp_phone' => 'nullable|string',
            'emp_cell' => 'nullable|string',
            'emp_cell2' => 'nullable|string',
            'emp_email' => 'nullable|string',
            'emp_fax' => 'nullable|string',
            'emp_home_phone' => 'nullable|string',
        ];
    }
}
