<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUtilizationPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'no_resources' => 'required|integer',
            'no_cost_resources' => 'required|integer',
            'wk_bill_hours' => 'nullable|integer',
            'wk_cost_hours' => 'nullable|integer',
            'status' => 'required|not_in:0',
        ];
    }
}
