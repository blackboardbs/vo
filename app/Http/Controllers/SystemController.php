<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSystemRequest;
use App\Http\Requests\UpdateSystemRequest;
use App\Models\Status;
use App\Models\System;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SystemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $skills = System::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $skills = $skills->where('status', $request->input('status_filter'));
            }else{
                $skills = $skills->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $skills = $skills->where('description', 'like', '%'.$request->input('q').'%');
        }

        $skills = $skills->paginate($item);

        $parameters = [
            'skills' => $skills,
        ];

        return View('master_data.skill.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = System::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.skill.create')->with($parameters);
    }

    public function store(StoreSystemRequest $request): RedirectResponse
    {
        $skill = new System();
        $skill->description = $request->input('description');
        $skill->status = $request->input('status');
        $skill->save();

        return redirect(route('system.index'))->with('flash_success', 'Master Data Skill captured successfully');
    }

    public function show($skill_id): View
    {
        $parameters = [
            'system' => System::where('id', '=', $skill_id)->first(),
        ];

        return View('master_data.skill.show')->with($parameters);
    }

    public function edit($skill_id): View
    {
        $autocomplete_elements = System::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'skill' => System::where('id', '=', $skill_id)->first(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.skill.edit')->with($parameters);
    }

    public function update(UpdateSystemRequest $request, $skill_id): RedirectResponse
    {
        $skill = System::find($skill_id);
        $skill->description = $request->input('description');
        $skill->status = $request->input('status');
        $skill->save();

        return redirect(route('system.index'))->with('flash_success', 'Master Data Skill saved successfully');
    }

    public function destroy($skill_id): RedirectResponse
    {
        // System::destroy($skill_id);
        $item = System::find($skill_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('system.index')->with('flash_success', 'Master Data Skill suspended successfully');
    }
}
