<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUtilizationRequest;
use App\Http\Requests\UpdateUtilizationRequest;
use App\Models\Utilization;
use App\Services\UtilizationService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Carbon\Carbon;

class UtilizationController extends Controller
{
    public function index(Request $request, UtilizationService $service): View
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $utilizations = Utilization::with(['status:id,description'])->when($request->from,
            fn($utilization) => $utilization->where('yearwk', '>=', $request->date('from')->format("YW"))
        )->when($request->to,
            fn($utilization) => $utilization->where('yearwk', '<=', $request->date('to')->format("YW")))
            ->when($request->q, fn($utilization) => $utilization->where(fn($util) => $util->where('yearwk', 'LIKE', '%'.$request->q.'%')
                ->orWhere('res_no', 'like', '%'.$request->q.'%')
                ->orWhere('status_id', 'like', '%'.$request->q.'%')
                ->orWhere('company_id', 'like', '%'.$request->q.'%')
                ->orWhereHas('status', fn($status) => $status->where('description', 'LIKE', '%'.$request->q.'%'))))
            ->orderBy('yearwk','desc')
            ->paginate($request->r??15);

        return view('utilization.index')->with(['utilization' => $utilizations]);
    }

    public function create(Request $request, UtilizationService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $last_wk_entry = Utilization::select('id', 'yearwk', 'res_no', 'cost_res_no')->orderBy('yearwk', 'desc')->first();
        $parameters = [
            'year' => now()->year,
            'wk' => (now()->weekOfYear < 10 ? '0'.now()->weekOfYear : now()->weekOfYear),
            'week_drop_down' => $service->weeksDropdown($last_wk_entry),
            'utilization_hours' => $service->config()?->utilization_hours_per_week,
            'utilization_cost_hours' => $service->config()?->utilization_cost_hours_per_week,
            'no_resources' => ($last_wk_entry != null ? $last_wk_entry->res_no : 0),
            'no_cost_resources' => ($last_wk_entry != null ? $last_wk_entry->cost_res_no : 0),
        ];

        if ($request->type == 'week') {
            return view('utilization.create')->with($parameters);
        } else {
            return view('utilization.createplan')->with($parameters);
        }
    }

    public function store(StoreUtilizationRequest $request, UtilizationService $service): RedirectResponse
    {
        $service->createUtilization($request);

        return redirect(route('utilization.index'))->with('flash_success', 'Utilization captured successfully');
    }

    public function edit( Utilization $utilization, UtilizationService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $parameters = [
            'year' => now()->year,
            'wk' => now()->weekOfYear,
            'utilization' => $utilization,
            'utilization_dropdown' => Utilization::sortable('id', 'desc')->pluck('yearwk', 'yearwk')->prepend('Select Year Week', '0'),
        ];

        return view('utilization.edit')->with($parameters);
    }

    public function update(UpdateUtilizationRequest $request, Utilization $utilization, UtilizationService $service): RedirectResponse
    {
        $service->updateUtilization($request, $utilization);

        return redirect(route('utilization.index'))->with('flash_success', 'Utilization saved successfully');
    }

    public function show(Utilization $utilization, UtilizationService $service): View
    {
        abort_unless($service->hasShowPermissions(), 403);

        return view('utilization.show')->with(['utilization' => $utilization]);
    }

    public function destroy(Utilization $utilization, UtilizationService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $utilization->delete();

        return redirect()->route('utilization.index')->with('success', 'Utilization deleted successfully');
    }
}
