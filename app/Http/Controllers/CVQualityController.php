<?php

namespace App\Http\Controllers;

use App\Models\Availability;
use App\Models\CVQuality;
use App\Models\CVReference;
use App\Models\Experience;
use App\Models\IndustryExperience;
use App\Models\Qualification;
use App\Models\Resource;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CVQualityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('cv.reference.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $cv_quality = CVQuality::find($request->input('cvq_id'));
        if (! isset($cv_quality) || $cv_quality == null) {
            $cv_quality = new CVQuality();
        }

        $cv_quality->cv_id = $request->has('cvq_id') ? $request->input('cvq_id') : null;
        $cv_quality->introduction_letter = $request->has('introduction_letter') ? $request->input('introduction_letter') : 0;
        $cv_quality->introduction_letter_date = now();
        $cv_quality->terms_and_conditions = $request->has('terms_and_conditions') ? $request->input('terms_and_conditions') : 0;
        $cv_quality->criminal_record = $request->has('criminal_record') ? $request->input('criminal_record') : 0;
        $cv_quality->credit_check = $request->has('credit_check') ? $request->input('credit_check') : 0;
        $cv_quality->result_text = $request->has('result_text') ? $request->input('result_text') : 0;

        $user = User::find($request->input('resq_id'));

        $resource = Resource::find($user->resource_id);

        //Resource Compliance Tottal Percentage - Start
        $personal_info_perce = $this->complete_percentage('Cv', 'talent', $resource, 'user_id');
        $availability = Availability::where('res_id', '=', $resource->user_id)->count();
        $availability = ($availability > 0) ? 100 : 0;
        $skill = Skill::where('res_id', '=', $resource->user_id)->count();
        $skill = ($skill != 0) ? ((($skill > 5) ? 5 : $skill) / 5) * 100 : 0;
        $qualification = Qualification::where('res_id', '=', $resource->user_id)->count();
        $qualification = ($qualification != 0) ? ((($qualification > 2) ? 2 : $qualification) / 2) * 100 : 0;
        $experience = Experience::where('res_id', '=', $resource->user_id)->count();
        $experience = ($experience != 0) ? ((($experience > 5) ? 5 : $experience) / 5) * 100 : 0;
        $industry_experience = IndustryExperience::where('res_id', '=', $resource->user_id)->count();
        $industry_experience = ($industry_experience != 0) ? ((($industry_experience > 5) ? 5 : $industry_experience) / 5) * 100 : 0;

        $total_compliance = number_format(((($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience) > 0) ? ($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience) / 6 : 0), 0, '.', ',');
        //Resource Compliance Tottal Percentage - End

        $cv_checks = 0;
        if (isset($cv_quality)) {
            $cv_checks = (is_numeric($cv_quality->introduction_letter) ? $cv_quality->introduction_letter : 0) + (is_numeric($cv_quality->terms_and_conditions) ? $cv_quality->terms_and_conditions : 0) + (is_numeric($cv_quality->criminal_record) ? $cv_quality->criminal_record : 0) + (is_numeric($cv_quality->credit_check) ? $cv_quality->credit_check : 0);

            $cv_checks = $cv_checks / 4 * 100;
        }

        $cv_references = CVReference::where('cv_id', '=', $request->input('cvq_id'));

        $reference_checked = 0;
        if ($cv_references->count() >= 1) {
            $reference_checked = 100;
        }

        $quality_percentage = number_format(($total_compliance + $cv_checks + $reference_checked) / 3, 2, '.', ',');

        $cv_quality->quality_percentage = $quality_percentage;
        $cv_quality->save();

        return redirect(route('cv.show', ['cv' => $request->input('cvq_id'), 'resource' => $request->input('resq_id')]))->with('flash_success', 'CV Quality updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(CVQuality $cVQuality)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(CVQuality $cVQuality)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CVQuality $cVQuality)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(CVQuality $cVQuality)
    {
        //
    }

    private function complete_percentage($model, $table_name, $foreign_key, $own_col)
    {
        $pos_info = DB::select('SHOW COLUMNS FROM '.$table_name);
        $base_columns = count($pos_info);
        $not_null = 0;
        foreach ($pos_info as $col) {
            $not_null += app('App\Models\\'.$model)::selectRaw('SUM(CASE WHEN '.$col->Field.' IS NOT NULL THEN 1 ELSE 0 END) AS not_null')->where($own_col, '=', $foreign_key->user_id)->first()->not_null;
        }

        return ($not_null / $base_columns) * 100;
    }
}
