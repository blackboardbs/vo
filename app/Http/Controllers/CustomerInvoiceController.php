<?php

namespace App\Http\Controllers;

use App\API\Currencies\Currency;
use App\Enum\FrequencyEnum;
use App\Enum\InvoiceType;
use App\Models\BillingPeriod;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\CustomerInvoice;
use App\Models\CustomerInvoiceLine;
use App\Models\CustomerInvoiceLineExpense;
use App\Models\Document;
use App\Http\Requests\InvoiceRequest;
use App\Models\InvoiceItems;
use App\Jobs\SendCustomerInvoiceEmailJob;
use App\Jobs\SendCustomerInvoiceUnallocationEmailJob;
use App\Models\Module;
use App\Models\Project;
use App\Models\Recurring;
use App\Models\User;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\VatRate;
use App\Services\CustomerInvoiceService;
use App\Timesheet\InvoiceCalculations;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerInvoiceController extends Controller
{
    public function index(Request $request): View|BinaryFileResponse
    {
        if (! $request->input('r')) {
            $item = 15;
        } elseif ($request->input('r') == 0) {
            $item = CustomerInvoice::count();
        } else {
            $item = $request->input('r');
        }

        $customer_invoice = CustomerInvoice::with(['invoice_status:id,description', 'customer:id,customer_name'])->withCount(['recurring', 'nonTimesheetCustomerInvoices']);

        $module = Module::where('name', '=', \App\Models\CustomerInvoice::class)->get();

        abort_unless(Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
            $customer_invoice->where('customer_id', '=', Auth::user()->customer_id);
        }

        $can_create = Auth::user()->canAccess($module[0]->id, 'create_all') || Auth::user()->canAccess($module[0]->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module[0]->id) ? $module[0]->id : -1, 'update_all') || Auth::user()->canAccess(isset($module[0]->id) ? $module[0]->id : -1, 'update_team');

        if ($request->has('q') && $request->input('q') != '') {
            $customer_invoice->where('customer_invoice_number', 'like', '%'.$request->input('q').'%')
                ->orWhere('inv_ref', 'like', '%'.$request->input('q').'%')
                ->orWhere('invoice_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('invoice_value', 'like', '%'.$request->input('q').'%')
                ->orWhere('due_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('id', 'like', '%'.$request->input('q').'%')
                ->orWhere('paid_date', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('customer',function ($q) use ($request){
                    $q->where('customer_name','like','%'.$request->input('q').'%');
                });
        }

        $customer_invoice = $customer_invoice->when($request->customer_id,
            fn($invoice) => $invoice->where('customer_id', $request->customer_id)
        )->when($request->filter_from,
            fn($invoice) => $invoice->where('invoice_date', '>=', $request->filter_from)
        )->when($request->filter_to,
            fn($invoice) => $invoice->where('invoice_date', '<=', $request->filter_to)
        );
        /*if ($request->has('customer_id') && $request->input('customer') != '') {
            $customer_invoice = $customer_invoice->with('customer')
                ->where('customer_id', '=', $request->input('customer'));
        }*/


        if ($request->has('invoice_status') && $request->input('invoice_status') > 0) {
            $customer_invoice = $customer_invoice->with('invoice_status')
                ->where('bill_status', '=', $request->input('invoice_status'));
        }

        $customer_invoice = $customer_invoice->sortable(['id' => 'desc'])->paginate($item)->appends($request->query());

        
        $total = [];
        foreach($customer_invoice as $vi){
            if($vi->currency){
                if(isset($total[$vi->currency])){
                $total[$vi->currency] = $total[$vi->currency] + $vi->invoice_value;
                } else {
                    $total[$vi->currency] = $vi->invoice_value;
                }
            } else {
                if(isset($total['ZAR'])){
                $total['ZAR'] = $total['ZAR'] + $vi->invoice_value;
                } else {
                    $total['ZAR'] = $vi->invoice_value;
                }
            }
        }

        $parameters = [
            'customer_invoices' => $customer_invoice,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'total'=>$total
        ];

        if ($request->has('export')) return $this->export($parameters, 'customer.invoice');

        //return $parameters;
        return view('customer.invoice.index')->with($parameters);
    }

    public function create(Request $request): View
    {
        $item = $request->input('s') ?? 15;
        
        if (! $request->has('page')) {
            session()->forget(['customer_invoice_timesheets', 'invoice_timesheets']);
        }
        $timesheets = $this->timesheet($request);
        
        $total_billable_minutes = 0;
        $total_non_billable_minutes = 0;
        $time_exp = [];

        foreach ($timesheets as $timesheet) {
            $total_billable_minutes += ($timesheet->billable_minutes + ($timesheet->billable_hours * 60));
            $total_non_billable_minutes += ($timesheet->non_billable_minutes + ($timesheet->non_billable_hours * 60));
            array_push($time_exp, TimeExp::select('id', 'claim_auth_date', 'description')->where('timesheet_id', '=', $timesheet->id)->get());
        }
        
        $parameters = [
            'timesheets' => $timesheets,
            'total_billable_minutes' => $total_billable_minutes,
            'total_non_billable_minutes' => $total_non_billable_minutes,
            'year_weeks' => Timesheet::filters()->select('year_week')
            ->where('bill_status', '=', 1)
            ->groupBy(  'year_week')
            ->orderBy('year_week', 'desc')
            ->pluck('year_week','year_week')->prepend('All',''),
            'project' => Project::with('timesheets')->whereHas('timesheets',function ($q){
                $q->where('bill_status', '=', 1)
                ->whereNull('invoice_number')
                ->whereNull('inv_ref');
                })->when(isset(request()->employees) && request()->employees != '-1' && request()->employees != '0', function ($user) {
                    $user->whereHas('timesheets',function ($q){
                        $q->where('employee_id',request()->employees)->where('bill_status', '=', 1);
                    });
                })->filters()->where('project_type_id', 1)->orderBy('name')->pluck('name','id'),
            'employees' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->when(isset(request()->project) && request()->project != '-1' && request()->project != '0' && request()->project != '', function ($user) {
                $user->whereHas('timesheets', function ($q){
                    $q->where('project_id',request()->project)->where('bill_status', '=', 1)
                    ->whereNull('invoice_number')
                    ->whereNull('inv_ref');
                });
            })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '0' && request()->customer != '', function ($task) {
                $task->whereHas('timesheets', function ($q){
                    $q->where('customer_id', request()->customer)->where('bill_status', '=', 1)
                    ->whereNull('invoice_number')
                    ->whereNull('inv_ref');
                });
        })->whereHas('timesheets',function ($q){
                $q->where('bill_status', '=', 1);
                })->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'time_exp' => $time_exp,
            'customers' => Customer::filters()->when(isset(request()->employees) && request()->employees != '-1' && request()->employees != '0', function ($user) {
                $user->whereHas('timesheets',function ($q){
                    $q->where('employee_id',request()->employees)->where('bill_status', '=', 1);
                });
            })->orderBy('customer_name')->pluck('customer_name', 'id'),
            'selected_timesheets' => $request->session()->get('customer_invoice_timesheets'),
            'default_vat' => Config::first(['vat_rate_id'])
        ];

        return view('customer.invoice.create')->with($parameters);
    }

    public function prepare(CustomerInvoiceService $service)
    {
        $timesheets = \session('invoice_timesheets');
        if(isset($timesheets) && count($timesheets)){
            $first_timesheet = Timesheet::find($timesheets[0]);
            $customer = Customer::find($first_timesheet->customer_id);
        } else {
            session()->forget(['customer_invoice_timesheets', 'invoice_timesheets']);
            return redirect()->back()->with('flash_danger', 'Please ensure to select at least one timesheet');
        }

        //Default 30 days, calculate on customer selection
        $due_date = $customer->payment_terms ?? 30;


        $parameters = [
            'timesheet' => $timesheets,
            'invoice_date' => now()->toDateString(),
            'payment_due_date' => now()->addDays($due_date)->toDateString(),
            'currencies' => $service->currencies(),
            'vat_rate_id' => $service->vatRate(),
            'vat_rate_dropdown' => VatRate::selectRaw('id, CONCAT(description, " - ", vat_rate,"%") AS description')->where('end_date', '>=', now()->toDateString())->pluck('description', 'id'),
        ];

        return view('customer.invoice.prepare')->with($parameters);
    }

    public function generate(Request $request)
    {
        // dd($request->all());
        if ($request->has('save') || $request->has('send')) {
            $timesheets_id = $request->input('time_id');
        } else {
            $timesheets_id = $request->session()->get('invoice_timesheets');
        }

        if ($timesheets_id == null || !isset($timesheets_id) || count($timesheets_id) == 0) {
            $request->session()->forget('invoice_timesheets');

            return redirect()->route('customer.invoice_create')->with('flash_danger', 'Please Select timesheets.');
        }

        $timesheets = new InvoiceCalculations();
        $dependencies = $timesheets->dependencies($timesheets_id, $request->currency_type, $request->vat_rate_id);

        if ($dependencies == 0) {
            $request->session()->forget('invoice_timesheets');

            return redirect()->route('customer.invoice_create')->with('flash_danger', 'Please Select timesheets that belongs to the same customer.');
        }

        $timesheets_compact = $timesheets->calculateInvoice($timesheets_id, $request->currency_type);

        if (count($timesheets->checkCurrencies($timesheets_compact)) > 1) {
            $request->session()->forget('invoice_timesheets');

            return redirect()->route('customer.invoice_create')->with('flash_danger', 'Please Select assignments that uses the same currency.');
        }

        if (isset(CustomerInvoice::latest()->first()->id)) {
            $invoice_number = CustomerInvoice::latest()->first(['id', 'customer_invoice_number']);
        }

        $parameters = [
            'timesheets' => $timesheets_compact,
            'company' => $timesheets_compact->first()?->company,
            'total_hours' => $dependencies['total_hours'],
            'vat_rate' => $dependencies['vat_rate'],
            'vat_rate_id' => $request->vat_rate_id,
            'timesheets_id' => $timesheets_id,
            'invoice_notes' => $request->invoice_notes,
            'invoice_ref' => $request->invoice_ref,
            'customer_ref' => $request->customer_ref,
            'due_date' => $request->due_date,
            'invoice_date' => $request->invoice_date,
            'date' => Carbon::now()->toDateString(),
            'total' => $dependencies['total_amount'],
            'total_vat' => $dependencies['total_vat'],
            'total_exclusive' => $dependencies['total_exclusive'],
            'invoice_number' => isset($invoice_number) ? $invoice_number->invoiceNumberString($invoice_number) : 1, //isset($invoice_number)?$invoice_number + 1 : 1,
            'conf' => isset($this->sysConfig()->default_invoice_template) ? $this->sysConfig()->default_invoice_template : 'standardinvoice',
            'currency' => $this->sysConfig()->currency ?? 'ZAR',
            'currency_type' => $request->currency_type,
            'contact_person' => $this->contact($timesheets_compact[0]->project, $timesheets_compact[0]->customer),
        ];

        if ($request->has('save') || $request->has('send')) {
            $customer_invoice = new CustomerInvoice();
            $customer_invoice->inv_ref = $request->input('invoice_ref');
            $customer_invoice->invoice_date = $request->input('invoice_date');
            $customer_invoice->due_date = $request->input('due_date');
            $customer_invoice->customer_id = $timesheets_compact[0]->customer_id;
            $customer_invoice->company_id = $timesheets_compact[0]->company_id;
            $customer_invoice->project_id = $timesheets_compact[0]->project_id;
            $customer_invoice->paid_date = null;
            $customer_invoice->customer_unallocate_date = null;
            $customer_invoice->invoice_value = (string) ($dependencies['total_vat'] + $dependencies['total_exclusive'] + $dependencies['total_amount']);
            $customer_invoice->bill_status = 1;
            $customer_invoice->invoice_notes = $request->input('invoice_notes');
            $customer_invoice->cust_inv_ref = $request->input('customer_ref');
            $customer_invoice->currency = $request->currency;
            $customer_invoice->vat_rate_id = $request->vat_rate_id;
            if (! isset($this->sysConfig()->customer_invoice_start_number)) {
                $customer_invoice->save();
            }else{
                $customer_invoice->saveInvoiceNumber();
            }

            foreach ($timesheets_compact as $timesheet) {
                $time = Timesheet::find($timesheet->id);
                $time->timesheet_lock = 1;
                $time->invoice_number = $customer_invoice->id;
                $time->inv_ref = ($customer_invoice->inv_ref != null) ? $customer_invoice->inv_ref : $customer_invoice->id;
                $time->bill_status = 2;
                $time->invoice_date = $request->invoice_date;
                $time->invoice_due_date = $request->input('due_date');
                $time->invoice_paid_date = null;
                $time->cust_inv_ref = $request->input('customer_ref');
                $time->invoice_note = $request->input('invoice_note');
                $time->save();

                foreach ($timesheet->timeline as $timeline) {
                    switch ($parameters['conf']) {
                        case 'standardinvoice':
                            /*$text_line = (isset($timeline->description_of_work)?$timeline->description_of_work:(isset($timeline->task->description)?$timeline->task->description:null));*/
                            $text_line = 'Timesheet for '.(isset($timesheet->employee) ? $timesheet->employee->first_name.' '.$timesheet->employee->last_name : null).' for week '.$timesheet->year_week.' on assignment '.(isset($timesheet->project->name) ? $timesheet->project->name : null);
                            break;
                        case 'invoicebytask':
                            $text_line = ((isset($timeline->description_of_work) ? $timeline->description_of_work.' - '.(isset($timeline->task) ? $timeline->task->description : null) : (isset($timeline->task) ? $timeline->task->description : null)));
                            break;
                        case 'invoicebyweek':
                            $text_line = ('Wk'.$timesheet->year_week.' :'.$timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week);
                            break;
                        default:
                            $text_line = ($timesheet->year_week.' - '.(isset($timeline->description_of_work) ? $timeline->description_of_work : (isset($timeline->task->description) ? $timeline->task->description : null)).': '.(isset($timesheet->employee) ? $timesheet->employee->first_name.' '.$timesheet->employee->last_name : null));
                    }

                    $customer_invoice_lines = new CustomerInvoiceLine();
                    $customer_inv_line = CustomerInvoiceLine::select('customer_invoice_line')->orderBy('created_at', 'desc')->first();
                    $customer_invoice_lines->customer_invoice_line = $customer_invoice_lines->id;
                    $customer_invoice_lines->customer_invoice_number = $customer_invoice->id;
                    $customer_invoice_lines->timesheet_id = $timesheet->id;
                    $customer_invoice_lines->employee_id = $timesheet->employee->id;
                    //$customer_invoice_lines->assignment_id = $request->input('assignment_id')[$i];
                    $customer_invoice_lines->line_text = $text_line;
                    $customer_invoice_lines->quantity = (string) round(($timeline->billable_hours + ($timeline->billable_minutes / 60)), 2);
                    $customer_invoice_lines->price = (string) ($timesheet->invoice_rate ?? 0.00);
                    $customer_invoice_lines->invoice_line_value = ($customer_invoice_lines->price != null) ? (string) ($customer_invoice_lines->quantity * $customer_invoice_lines->price) : '0.00';
                    $customer_invoice_lines->save();
                }

                foreach ($timesheet->time_exp as $expense) {
                    $customer_inv_line_exp = new CustomerInvoiceLineExpense();
                    $customer_inv_line_exp->customer_invoice_line_expense = (empty($vile)) ? 1 : $vile->customer_invoice_line_expense + 1;
                    $customer_inv_line_exp->customer_invoice_number = $customer_invoice->id;
                    $customer_inv_line_exp->timesheet_id = $expense->timesheet_id;
                    $customer_inv_line_exp->time_exp_id = $expense->id;
                    $customer_inv_line_exp->description = $expense->description;
                    //$customer_inv_line_exp->claim = $expense->claim;
                    $customer_inv_line_exp->employee_id = $timesheet->employee_id;
                    //$customer_inv_line_exp->vendor_po = $customer_invoice_lines->vondor_po;
                    $customer_inv_line_exp->line_text = 'Expenses for '.$timesheet->employee->first_name.' '.$timesheet->employee->last_name.' for week '.$timesheet->year_week.' on assignment '.$timesheet->project->name.' for '.$expense->description;
                    $customer_inv_line_exp->amount = (string) $expense->amount;
                    //$customer_inv_line_exp->creator_id = auth()->id();
                    $customer_inv_line_exp->save();
                }
            }

            if ($request->has('send')) {

                $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
                $emails = [(isset($request->company_email) ? $request->company_email : null), (isset($request->customer_email) ? $request->customer_email : null)];
                if (isset($multiple_emails)) {
                    foreach ($multiple_emails as $user_email) {
                        array_push($emails, $user_email);
                    }
                }

                if (isset($request->subject)) {
                    $subject = $request->subject;
                } else {
                    $subject = ($timesheet->customer->customer_name ??'<Customer Name>').' Accounts Recievable Invoice';
                }

                if (isset($this->sysConfig()->invoice_body_msg)) {
                    $body = $this->sysConfig()->invoice_body_msg;
                } else {
                    $body = 'Please find Accounts Receivable invoice attached for your attention';
                }

                try {
                    $storage = $customer_invoice->invoiceStoragePath('customer')?->storage;

                    Pdf::view('customer.invoice.standardinvoice.show2', $parameters)
                        ->format('a4')
                        ->save($storage);

                    $document = new Document();
                    $document->document_type_id = 5;
                    $document->name = 'Customer Tax Invoice';
                    $document->file = $customer_invoice->invoiceStoragePath('customer')?->file_name;
                    $document->creator_id = auth()->id();
                    $document->owner_id = $timesheet->employee_id;
                    $document->save();
                    $subject = ($timesheet->customer->customer_name ?? '<Customer Name>').' Accounts Recievable Invoice';

                    activity()->on($customer_invoice)->withProperties(['customer_name' => ($customer_invoice->customer) ? $customer_invoice->customer->customer_name : ''])->log('emailed');
                    $emails[] = Auth::user()->email;
                    SendCustomerInvoiceEmailJob::dispatch(array_filter($emails), $timesheet->customer, $timesheet->company, $subject, $body, $storage)->delay(now()->addMinutes(5));

                    return redirect(route('customer.invoice'))->with('flash_success', 'Invoice sent successful');
                } catch (\Exception $e) {
                    report($e);

                    return redirect(route('customer.invoice_create'))->with('flash_danger', $e->getMessage());
                }
            }

            $request->session()->forget('invoice_timesheets');
            activity()->on($customer_invoice)->withProperties(['customer_name' => ($customer_invoice->customer) ? $customer_invoice->customer->customer_name : ''])->log('created');

            return redirect(route('customer.invoice'))->with('flash_success', 'Invoice saved successfully');
        }

        return view('customer.invoice.standardinvoice.show')->with($parameters);
    }

    public function generateNonTimesheetInvoice(Request $request, $invoice_id)
    {
        $customerInvoice = CustomerInvoice::find($invoice_id);
        $customerInvoiceItems = $customerInvoice->nonTimesheetCustomerInvoices;

        $discount = $customerInvoiceItems->sum('discount_amount');

        if ($customerInvoice->recurring && !$customerInvoice->recurring->scheduled_at){
            if (Carbon::parse($customerInvoice->recurring->first_invoice_at)->toDateString() === now()->toDateString()){
                $emails = $customerInvoice->recurring->emails ? explode(",", $customerInvoice->recurring->emails) : [];
                $request['user_email'] = json_encode($emails);
                $this->sendNonTimesheetInvoice($customerInvoice, $request);
            }
            $customerInvoice->recurring()->update([
                'scheduled_at' => now()->toDateString()
            ]);
        }

        $parameters = [
            'customerInvoice' => $customerInvoice,
            'customerInvoiceItems' => $customerInvoiceItems,
            'discount' => $discount,
            'contact_person' => $this->contact(($customerInvoice->project ?? new Project()), $customerInvoice->customer),
        ];

        if (\request()->has('print')){
            return Pdf::view('customer.invoice.standardinvoice.non_timesheet_pdf', $parameters)
                ->name($customerInvoice->customer?->customer_name."_".now()->format("H_i_s_m_Y").'.pdf');
        }

        return view('customer.invoice.standardinvoice.non_timesheet_show')->with($parameters);
    }

    public function sendNonTimesheetInvoice(CustomerInvoice $customerInvoice, Request $request)
    {
        $parameters = [
            'customerInvoice' => $customerInvoice,
            'customerInvoiceItems' => $customerInvoice->nonTimesheetCustomerInvoices,
            'contact_person' => $this->contact(($customerInvoice->project ?? new Project()), $customerInvoice->customer ?? new Customer()),
        ];

        try {
            $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
            $emails = [$request->company_email, $request->customer_email];
            $emails = [...$emails, ...$multiple_emails];

            if (isset($this->sysConfig()->invoice_body_msg)) {
                $body = $this->sysConfig()->invoice_body_msg;
            } else {
                $body = 'Please find Accounts Receivable invoice attached for your attention';
            }

            $storage = $customerInvoice->invoiceStoragePath("customer")?->storage;

            try {
                Pdf::view('customer.invoice.standardinvoice.non_timesheet_pdf', $parameters)
                    ->format('a4')
                    ->save($storage);
            }catch (\Exception $e){
                logger($e->getMessage());
            }

            $document = new Document();
            $document->document_type_id = 5;
            $document->name = 'Customer Tax Invoice';
            $document->file = $customerInvoice->invoiceStoragePath("customer")?->file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = isset($customerInvoice->resource->employee_id) ? $customerInvoice->resource->employee_id : null;
            $document->reference_id = 5;
            $document->save();
            $subject = (isset($customerInvoice->customer) ? $customerInvoice->customer->customer_name : '<Customer Name>').' Accounts Recievable Invoice';

            activity()->on($customerInvoice)->withProperties(['customer_name' => ($customerInvoice->customer) ? $customerInvoice->customer->customer_name : ''])->log('emailed');
            $emails[] = Auth::user()->email;
            SendCustomerInvoiceEmailJob::dispatch(array_filter($emails), $customerInvoice->customer, $customerInvoice->company, $subject, $body, $storage)->delay(now()->addMinutes(5));

            return redirect(route('customer.invoice'))->with('flash_success', 'Invoice sent successful');
        } catch (\Exception $e) {
            report($e);

            return redirect(route('nontimesheetinvoice.generate', $customerInvoice->id))->with('flash_danger', $e->getMessage());
        }
    }

    public function process_payment(Request $request): RedirectResponse
    {
        $customer_invoice = CustomerInvoice::find($request->input('invoice_id'));
        $customer_invoice->paid_date = $request->input('paid_date');
        $customer_invoice->bill_status = 2;
        $customer_invoice->save();

        $time = Timesheet::where('invoice_number', $customer_invoice->id)->get();

        foreach ($time as $result) {
            $result->timesheet_lock = 1;
            $result->bill_status = 3;
            $result->invoice_paid_date = $customer_invoice->paid_date;
            $result->save();
        }
        activity()->on($customer_invoice)->withProperties(['customer_name' => ($customer_invoice->customer) ? $customer_invoice->customer->customer_name : ''])->log('paid');

        return redirect(route('customer.invoice'))->with('flash_success', 'Invoice payment processed successfully');
    }

    public function show(Request $request, CustomerInvoice $customer_invoice)
    {
        $customer_invoice->load(['company', 'customer', 'project', 'vatRate:id,vat_rate']);

        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $invoice_lines = InvoiceItems::where('invoice_id', '=', $customer_invoice->id)->where('invoice_type_id', '=', 1)->get()->toArray();

        if (! empty($invoice_lines)) {
            return redirect(route('nontimesheetinvoice.generate', $customer_invoice->id));
        }

        if ($customer_invoice) {
            $customer_invoice_line = CustomerInvoiceLine::with('timesheet.company')->where('customer_invoice_number', $customer_invoice->id)
                ->get();

            $total_value_line = 0;
            $total_value_line_tax = 0;
            $vat_rate = $customer_invoice->vatRate?->vat_rate/100;
            $customer = $customer_invoice->customer;
            $total_hours = 0;

            $company = $customer_invoice->company;
            foreach ($customer_invoice_line as $key => $customer_invoice1) {
                $total_value_line += $customer_invoice1->invoice_line_value;
                $total_hours += $customer_invoice1->quantity;
                $total_value_line_tax = $total_value_line * $vat_rate;
            }

            $customer_invoice_line_exp = CustomerInvoiceLineExpense::where('customer_invoice_number', $customer_invoice->id)
                ->get();
            $total_expenses = 0;

            foreach ($customer_invoice_line_exp as $expense) {
                $total_expenses += $expense->amount;
            }

            $project = $customer_invoice->project;

            if (! isset($project)) {
                try {
                    $project = Project::find($customer_invoice_line[0]->timesheet->project_id);
                } catch (\Exception $e) {
                    $project = new Project();
                }
            }

            $parameter = [
                'customer_invoice' => $customer_invoice,
                'customer_invoice_lines' => $customer_invoice_line,
                'customer_invoice_line_exp' => $customer_invoice_line_exp,
                'total_hours' => $total_hours,
                'company' => $company ?? Company::find($this->sysConfig()->company_id),
                'vat_rate' => $vat_rate,
                'total_value_line' => $total_value_line,
                'total_expenses' => $total_expenses,
                'total_value_line_tax' => $total_value_line_tax,
                'path' => $path,
                'currency' => $this->sysConfig()->currency ?? 'ZAR',
                'contact_person' => $this->contact($project, $customer_invoice->customer),
            ];

            if (isset($request->customer_email) || isset($request->company_email) || isset($request->user_email) || $request->has('print')) {
                $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
                $emails = [(isset($request->company_email) ? $request->company_email : null), (isset($request->customer_email) ? $request->customer_email : null)];
                if (isset($multiple_emails)) {
                    foreach ($multiple_emails as $user_email) {
                        $emails[] = $user_email;
                    }
                }

                $emails[] = Auth::user()->email;

                if (isset($request->subject)) {
                    $subject = $request->subject;
                } else {
                    $subject = (isset($customer_invoice->customer) ? $customer_invoice->customer->customer_name : '<Customer Name>').' Accounts Recievable Invoice';
                }

                if (isset($this->sysConfig()->invoice_body_msg)) {
                    $body = $this->sysConfig()->invoice_body_msg;
                } else {
                    $body = 'Please find Accounts Receivable invoice attached for your attention';
                }

                try {
                    $filename = $customer_invoice->customer?->customer_name.'_'.$customer_invoice->project?->ref.'_'.substr($customer_invoice->user?->first_name, 0, 1).substr($customer_invoice->user?->last_name, 0, 1).'_'.date('Y-m-d_H_i_s').'.pdf';
                    $storage = $customer_invoice->invoiceStoragePath('customer')?->storage;
                    $parameter['logo'] = file_exists(storage_path('app/avatars/company/'.$company->company_logo))?storage_path('app/avatars/company/'.$company->company_logo):public_path('/assets/consulteaze_logo.png');
                    $pdf = Pdf::view('customer.invoice.standardinvoice.send', $parameter)
                        ->format('a4');
                    if ($request->has('print')){
                        return $pdf->name($filename);
                    }else{
                        $pdf->save($storage);
                    }

                    SendCustomerInvoiceEmailJob::dispatch(array_filter($emails), $customer_invoice->customer, $customer_invoice->company, $subject, $body, $storage)->delay(now()->addMinutes(5));

                    return redirect(route('customer.invoice_show', $customer_invoice))->with('flash_success', 'Invoice sent successful');
                } catch (\Exception $e) {
                    report($e);

                    return redirect(route('customer.invoice_show', $customer_invoice))->with('flash_danger', $e->getMessage());
                }
            } else {
                return view('customer.invoice.standardinvoice.view')->with($parameter);
            }
        } else {
            return redirect()->back()->with('flash_danger', 'The invoice you\'re looking for was not found');
        }
    }

    public function unallocate($customer_invoice_id): RedirectResponse
    {
        $customer_invoice = CustomerInvoice::find($customer_invoice_id);
        $times = Timesheet::where('invoice_number', '=', $customer_invoice->id)->get();

        $date = now()->toDateString();
        $customer_invoice->bill_status = 3;
        $customer_invoice->due_date = null;
        //$customer_invoice->invoice_date = null;
        $customer_invoice->customer_unallocate_date = $date;
        $customer_invoice->save();

        foreach ($times as $time) {
            //$time->customer_unallocate_date = $date;
            $time->bill_status = 1;
            $time->invoice_number = null;
            $time->inv_ref = null;
            $time->cust_inv_ref = null;
            $time->invoice_number = null;
            $time->invoice_date = null;
            $time->invoice_due_date = null;
            $time->timesheet_lock = null;
            $time->save();
        }

        activity()->on($customer_invoice)->withProperties(['customer_name' => ($customer_invoice->customer) ? $customer_invoice->customer->customer_name : ''])->log('cancelled');
        //Mail::to(Auth::user()->email)/*->cc(Config::first()->admin_email)*/->send(new \App\Mail\InvoiceUnallocation($customer_invoice));
        SendCustomerInvoiceUnallocationEmailJob::dispatch(Auth::user()->email, $customer_invoice)->delay(now()->addMinutes(5));

        return redirect(route('customer.invoice'))->with('flash_success', 'Invoice un-allocated successfully');
    }

    public function payment($id): View
    {
        $parameter = [
            'id' => $id,
            'date' => now()->toDateString(),
        ];

        return view('customer.invoice.payment')->with($parameter);
    }

    public function edit($customer_invoice_id)
    {
        $customer_invoice = CustomerInvoice::find($customer_invoice_id);

        $customerInvoiceItems = InvoiceItems::where('status_id', '=', 1)->where('invoice_id', '=', $customer_invoice_id)->get()->toArray();

        // Customer invoice in a non timesheet, redirect to the correct edit page
        if (isset($customerInvoiceItems) && ! empty($customerInvoiceItems)) {
            $params['invoice_id'] = $customer_invoice_id;
            if (request()->recurring)
            {
                $params['recurring'] = request()->recurring;
            }
            return redirect(route('nontimesheetinvoice.edit', $params));
        }

        $parameters = [
            'customer_invoice' => $customer_invoice,
        ];

        return view('customer.invoice.edit')->with($parameters);
    }

    public function update(Request $request, $customer_invoice_id)
    {
        $customer_invoice = CustomerInvoice::find($customer_invoice_id);
        $times = Timesheet::where('invoice_number', '=', $customer_invoice->id)->get();

        foreach ($times as $time) {
            $time->invoice_due_date = $request->input('due_date');
            $time->invoice_note = $request->input('invoice_notes');
            $time->inv_ref = $request->input('invoice_ref');
            $time->cust_inv_ref = $request->input('customer_ref');
            $time->invoice_date = $request->input('invoice_date');
            if($request->has('bill_status')){
                $time->bill_status = $request->input('bill_status');
            }
            $time->save();
        }

        if($request->has('bill_status')){
        $customer_invoice->bill_status = $request->input('bill_status');
        }
        $customer_invoice->invoice_notes = $request->input('invoice_notes');
        $customer_invoice->inv_ref = $request->input('invoice_ref');
        $customer_invoice->due_date = $request->input('due_date');
        $customer_invoice->invoice_date = $request->input('invoice_date');
        $customer_invoice->cust_inv_ref = $request->input('customer_ref');
        $customer_invoice->company_id = isset($time->company) ? $time->company->id : $this->sysConfig()->company_id;
        $customer_invoice->save();

        activity()->on($customer_invoice)->withProperties(['customer_name' => ($customer_invoice->customer) ? $customer_invoice->customer->customer_name : ''])->log('updated');

        if ($request->ajax()) {
            $data = [];
           return response()->json($data);
       }

        return redirect(route('customer.invoice'))->with('flash_success', 'Invoice updated successfully');
    }

    public function destroy($invoice_id)
    {
    }

    private function timesheet($request)
    {
        $item = $request->input('s') ?? 15;

        $timesheet = Timesheet::with([
            'customer:id,customer_name',
            'company:id,company_name',
            'employee:id,first_name,last_name',
            'project:id,name',
            'status:id,description'
        ])->select(DB::raw('timesheet.id, timesheet.employee_id,timesheet.company_id, timesheet.customer_id, timesheet.year_week, timesheet.project_id,timesheet.status_id,
                                                SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total ELSE 0 END) AS billable_hours,
                                                SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total ELSE 0 END) AS non_billable_hours,
                                                SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total_m ELSE 0 END) AS billable_minutes,
                                                SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total_m ELSE 0 END) AS non_billable_minutes,
                                                timesheet.first_day_of_week'))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereNull('timesheet.invoice_number')
            ->whereNull('timesheet.inv_ref')
            ->where('timesheet.bill_status', '=', 1)
            ->whereHas('project', function ($query) use ($request) {
                $query->where('project_type_id', 1);
                if ($request->has('project') && $request->input('project') != '' && $request->input('project') != '0') {
                    $query->where('id', $request->input('project'));
                    // dd($request->input('project'));
                }
            })
            ->groupBy('timesheet.id')
            ->groupBy('timesheet.employee_id', 'timesheet.first_day_of_week')
            ->groupBy('timesheet.customer_id')
            ->groupBy('timesheet.year_week')
            ->groupBy('timesheet.project_id')
            ->groupBy('timesheet.status_id')
            ->groupBy('timesheet.company_id')
            ->orderBy('timesheet.id', 'desc');

        if ($request->has('employees') && $request->input('employees') != '' && $request->input('employees') != '0') {
            $timesheet = $timesheet->where('timesheet.employee_id', '=', $request->employees);
        }

        if ($request->has('week') && $request->input('week') != '' && $request->input('week') != '0') {
            $timesheet = $timesheet->where('timesheet.year_week', '=', $request->input('week'));
        }

        if ($request->has('project') && $request->input('project') != '' && $request->input('project') != '0') {
            // $timesheet = $timesheet->where('timesheet.project_id', $request->input('project'));
        }

        if ($request->has('billing-period') && $request->input('billing-period') != '') {
            $billing_period = BillingPeriod::find($request->input('billing-period'));
            $timesheet = $timesheet->where('first_day_of_month', '>=', $billing_period->start_date)
                ->where('last_day_of_month', '<=', $billing_period->end_date);
        }

        if ($request->has('client') && $request->input('client') != '' && $request->input('client') != '0') {
            $timesheet = $timesheet->where('timesheet.customer_id', '=', $request->input('client'));
        }

        $module = Module::where('name', '=', \App\Models\CustomerInvoice::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $timesheet->whereIn('timesheet.employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $timesheet = $timesheet->paginate($item);

        return $timesheet;
    }

    public function storeTimesheetId(Request $request, $id): JsonResponse
    {
        if ($request->session()->has('customer_invoice_timesheets')) {
            $timesheets = $request->session()->get('customer_invoice_timesheets');

            if (in_array($id, $timesheets)) {
                $posted_timesheet[0] = $id;
                $timesheets = array_diff($timesheets, $posted_timesheet);
            } else {
                array_push($timesheets, $id);
            }
            $timesheets = array_values($timesheets);
            $request->session()->put('invoice_timesheets', $timesheets);
        } else {
            $timesheets = [];
            array_push($timesheets, $id);
            $request->session()->put('invoice_timesheets', $timesheets);
        }

        return response()->json('success');
    }

    public function createNonTimesheetInvoice(Currency $currency): View
    {
        $invoiceTypeID = 1; //Type 1 for customer invoice, type 2 for vendor invoice

        $parameters = [
            'invoiceTypeID' => $invoiceTypeID,
            'currency_drop_down' => $currency->currencies()['currencies'] ?? [],
        ];

        return view('customer.invoice.header_non_timesheet_create')->with($parameters);
    }

    public function storeNonTimesheetInvoice(InvoiceRequest $request, Currency $currency, CustomerInvoiceService $service): RedirectResponse
    {
        $customer_invoice = $service->saveInvoice($request, $currency);

        if ($request->is_recurring){
            $customer_invoice->saveRecurringInvoice($request);

            if (Carbon::parse($request->first_invoice_date)->toDateString() === now()->toDateString()){
                $invoiceCopy = $customer_invoice->replicate();
                $invoiceCopy->invoice_date = now()->toDateString();
                $invoiceCopy->due_date = now()->addDays($customer_invoice->recurring?->due_days_from_invoice_date)->toDateString();
                $invoiceCopy->save();

                $customer_invoice->recurring()->update([
                    'last_invoiced_at' => now()->toDateTimeString()
                ]);

                $customer_invoice->histories()->attach($invoiceCopy->id, ['type' => InvoiceType::Customer->value]);
            }
        }

        return redirect(route('nontimesheetinvoice.edit', ['invoice_id' => $customer_invoice->id, 'recurring' => $request->is_recurring]))->with('flash_success', 'Invoice added successfully');
    }

    public function editNonTimesheetInvoice($customerInvoice_id, Currency $currency): View
    {
        $customerInvoice = CustomerInvoice::find($customerInvoice_id);
        if (request()->recurring){
            $customerInvoice = $customerInvoice->load('recurring');
            $history_count = $customerInvoice->histories()->count();

            $customerInvoice = [
                'id' => $customerInvoice->id,
                'company_id' => $customerInvoice->company_id,
                'customer_id' => $customerInvoice->customer_id??0,
                'project_id' => !is_null($customerInvoice->project_id) ? $customerInvoice->project_id : 0,
                'resource_id' => $customerInvoice->resource_id,
                'invoice_date' => $customerInvoice->invoice_date,
                'due_date' => $customerInvoice->due_date,
                'currency' => $customerInvoice->currency,
                'first_invoice_date' => $customerInvoice->recurring?->first_invoice_at,
                'due_days_from_invoice_date' => $customerInvoice->recurring?->due_days_from_invoice_date,
                'frequency' => $customerInvoice->recurring?->frequency,
                'interval' => $customerInvoice->recurring?->interval,
                'occurrences' => $customerInvoice->recurring?->occurrences,
                'expiry_date' => $customerInvoice->recurring?->expires_at,
                'email_subject' => $customerInvoice->recurring?->email_subject,
                'emails' => explode(',', $customerInvoice->recurring?->emails),
                'full_name' => $customerInvoice->recurring?->full_name,
                'bill_status' => $customerInvoice->bill_status,
                'history_count' => $history_count,
                'remaining_invoices' => $customerInvoice->invoicesRemaining($customerInvoice->recurring, $history_count)
            ];
        }

        $invoiceNumber = $customerInvoice['id'];
        $invoiceTypeID = 1;

        $parameters = [
            'customerInvoice' => $customerInvoice,
            'invoiceNumber' => $invoiceNumber,
            'invoiceTypeID' => $invoiceTypeID,
            'currency_drop_down' => $currency->currencies()['currencies'] ?? [],
            'vat_rates' => VatRate::where('end_date','>',now()->format('Y-m-d'))->where('status','1')->orderBy('vat_code')->get(['vat_code','description']),
            'default_vat' => Config::first()
        ];

        return view('customer.invoice.add_lines_non_timesheet')->with($parameters);
    }

    public function updateNonTimesheetInvoice(CustomerInvoice $customerinvoice, InvoiceRequest $request): JsonResponse
    {
        $customerinvoice->company_id = $request->company_id;
        $customerinvoice->customer_id = $request->customer_id;
        $customerinvoice->project_id = $request->project_id;
        $customerinvoice->resource_id = $request->resource_id;
        $customerinvoice->invoice_date = $request->invoice_date;
        $customerinvoice->due_date = $request->due_date;
        $customerinvoice->currency = $request->currency;
        $customerinvoice->inv_ref = $request->inv_ref;
        $customerinvoice->cust_inv_ref = $request->cust_inv_ref;
        $customerinvoice->invoice_notes = $request->invoice_notes;
        if ($request->is_recurring){
            $customerinvoice->recurring->first_invoice_at = $request->first_invoice_date;
            $customerinvoice->recurring->due_days_from_invoice_date = $request->due_days_from_invoice_date;
            $customerinvoice->recurring->frequency = $request->frequency;
            $customerinvoice->recurring->interval = $request->interval;
            $customerinvoice->recurring->occurrences = $request->occurrences;
            $customerinvoice->recurring->expires_at = $request->expiry_date;
            $customerinvoice->recurring->email_subject = $request->email_subject;
            $customerinvoice->recurring->emails = implode(',', $request->emails);
            $customerinvoice->recurring->full_name = $request->full_name;
            $customerinvoice->push();
        }else{
            $customerinvoice->save();
        }

        return response()->json(['msg' => "success"]);
    }

    public function billingCycle(Project $project)
    {
        if ($project->billing_cycle_id) {
            $billing_cycle = $project->billing_cycle_id;
        } elseif ($project->customer->billing_cycle_id) {
            $billing_cycle = $project->customer->billing_cycle_id;
        } else {
            $billing_cycle = $this->sysConfig()->billing_cycle_id;
        }

        $billing_period = BillingPeriod::where('billing_cycle_id', $billing_cycle)->get()
            ->map(function ($period) {
                return [
                    'id' => $period->id,
                    'name' => $period->period_name,
                ];
            });

        return response()->json($billing_period);
    }

    private function contact(Project $project, Customer $customer)
    {
        $project = $project->load('customerInvoiceContact');
        $customer = $customer->load('invoiceContact');

        if (isset($project->customerInvoiceContact)) {
            return (object) [
                'name' => $project->customerInvoiceContact->full_name ?? null,
                'number' => $project->customerInvoiceContact->contact_number ?? null,
                'email' => $project->customerInvoiceContact->email ?? null,
            ];
        }

        if (isset($customer->invoiceContact)) {
            return (object) [
                'name' => $customer->invoiceContact->full_name ?? null,
                'number' => $customer->invoiceContact->contact_number ?? null,
                'email' => $customer->invoiceContact->email ?? null,
            ];
        }

        return (object) [
            'name' => $customer->contact_firstname.' '.$customer->contact_lastname,
            'number' => $customer->phone ?? $customer->cell,
            'email' => $customer->email,
        ];
    }

    private function sysConfig(): Config
    {
        return Config::first(
            [
                'default_invoice_template', 'currency',
                'customer_invoice_start_number',
                'customer_invoice_prefix',
                'customer_invoice_format',
                'invoice_body_msg', 'vat_rate_id',
                'company_id', 'billing_cycle_id',
            ]
        );
    }
}
