<?php

namespace App\Http\Controllers;

use App\Models\Favourite;
use App\Http\Requests\StoreFavouriteRequest;
use App\Http\Requests\UpdateFavouriteRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class FavouritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        // $favourites = Favourite::where('status_id', '=', 1)->sortable()->paginate(15);
        $item = $request->input('r') ?? 15;

        $favourites = Favourite::with(['status:id,description'])->sortable('module_name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $favourites = $favourites->where('status_id', $request->input('status_filter'));
            }else{
                $favourites = $favourites->sortable('module_name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $favourites = $favourites->where('module_name', 'like', '%'.$request->input('q').'%');
        }

        $favourites = $favourites->paginate($item);

        $parameters = [
            'favourites' => $favourites,
        ];

        return view('master_data.favourites.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Favourite::orderBy('module_name')->where('module_name', '!=', null)->where('module_name', '!=', '')->get();

        $parameters = [
            'roles_dropdown' => Role::pluck('display_name', 'id'),
            'status_dropdown' => Status::where('status', '=', 1)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.favourites.create')->with($parameters);
    }

    public function store(StoreFavouriteRequest $request): RedirectResponse
    {
        $button_function = '';
        $roles = '';
        foreach ($request->role_id as $key => $role) {
            $roles .= ($key != count($request->role_id) - 1) ? $role.',' : $role;
        }
        foreach ($request->button_function as $key => $button) {
            $button_function .= ($key != count($request->button_function) - 1) ? $button.',' : $button;
        }
        $favourite = new Favourite();
        $favourite->module_name = $request->module_name;
        $favourite->button_functions = $button_function;
        $favourite->route = $request->route;
        $favourite->role_id = $roles;
        $favourite->status_id = $request->status_id;
        $favourite->save();

        return redirect()->route('favourites.show', $favourite)->with('flash_success', 'Your favourite item has been added successfully');
    }

    public function show($id): View
    {
        $favourites = Favourite::find($id);
        $parameters = [
            'favourite' => $favourites,
        ];

        return view('master_data.favourites.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = Favourite::orderBy('module_name')->where('module_name', '!=', null)->where('module_name', '!=', '')->get();

        $favourites = Favourite::find($id);
        $parameters = [
            'favourite' => $favourites,
            'roles_dropdown' => Role::pluck('display_name', 'id'),
            'status_dropdown' => Status::where('status', '=', 1)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.favourites.edit')->with($parameters);
    }

    public function update(UpdateFavouriteRequest $request, $id): RedirectResponse
    {
        $button_function = '';
        $roles = '';
        if (isset($request->role_id)) {
            foreach ($request->role_id as $key => $role) {
                $roles .= ($key != count($request->role_id) - 1) ? $role.',' : $role;
            }
        }
        if (isset($request->button_function)) {
            foreach ($request->button_function as $key => $button) {
                $button_function .= ($key != count($request->button_function) - 1) ? $button.',' : $button;
            }
        }

        $favourite = Favourite::find($id);
        $favourite->module_name = $request->module_name;
        $favourite->button_functions = $button_function;
        $favourite->route = $request->route;
        $favourite->role_id = $roles;
        $favourite->status_id = $request->status_id;
        $favourite->save();

        return redirect()->route('favourites.show', $favourite)->with('flash_success', 'Your favourite item has been updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // Favourite::destroy($id);
        $item = Favourite::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('favourites.index')->with('flash_success', 'Your favourite item has been suspended successfully');
    }
}
