<?php

namespace App\Http\Controllers;

use App\Jobs\SendRiskFeedbackEmailJob;
use App\Mail\RiskFeedbackMail;
use App\Models\Risk;
use App\Models\RiskFeedback;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;

class RiskFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Risk $risk): JsonResponse
    {
        $feedback = new RiskFeedback();
        $feedback->risk_id = $risk->id;
        $feedback->user_id = auth()->id();
        $feedback->feedback = $request->feedback;
        $feedback->save();

        if ($request->notification) {
            //$risk = $risk->load(['resposibility', 'accountability']);
            //I need to find out why load method it's not working.

            $resposibility = User::find($risk->resposibility_user_id);
            $accountability = User::find($risk->accountablity_user_id);

            /*Mail::to([$resposibility->email, $accountability->email])
                ->send(new RiskFeedbackMail($risk, $feedback->feedback, $feedback->relatedUser->name()??''));*/
            SendRiskFeedbackEmailJob::dispatch([$resposibility->email, $accountability->email], $risk, $feedback->feedback, $feedback->relatedUser->name() ?? '');
        }

        return response()->json([
            'feedback' => $feedback,
        ], 201);
    }

    public function feedbackUserAvatar(User $user)
    {
        $avatar = new InitialAvatar();

        return $avatar->name($user->name())
                ->length(2)
                ->fontSize(0.5)
                ->size(96) // 48 * 2
                ->background('#0080ff')
                ->color('#fff')
                ->generate()
                ->stream('png', 100);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
