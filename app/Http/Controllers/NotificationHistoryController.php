<?php

namespace App\Http\Controllers;

use App\Models\UserNotification;
use Illuminate\Http\Request;
use Illuminate\View\View;

class NotificationHistoryController extends Controller
{
    //

    public function index(Request $request): View
    {
        $notifications = UserNotification::with('notification:id,name,link,created_at')
            ->select(['id', 'seen_at', 'notification_id', 'seen_at'])
            ->where('user_id', auth()->id())
            ->whereHas('notification', function ($notification) use ($request){
                return $notification->when($request->p, fn($notify) => $notify->where('name', 'like', '%'.$request->p.'%'))
                    ->when($request->f, fn($notify) => $notify->where('created_at', '>=', $request->f))
                    ->when($request->t, fn($notify) => $notify->where('created_at', '<=', $request->t));
            })->latest()
            ->paginate();

        $parameters = [
            'notifications' => $notifications,
        ];

        return view('notifications.index')->with($parameters);
    }
}
