<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIndustryRequest;
use App\Http\Requests\UpdateIndustryRequest;
use App\Models\Industry;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndustryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $industry = Industry::sortable('description', 'asc')->paginate($item);

        $industry = Industry::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $industry = $industry->where('status', $request->input('status_filter'));
            }else{
                $industry = $industry->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $industry = $industry->where('description', 'like', '%'.$request->input('q').'%');
        }

        $industry = $industry->paginate($item);

        $parameters = [
            'industry' => $industry,
        ];

        return View('master_data.industry.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Industry::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.industry.create')->with($parameters);
    }

    public function store(StoreIndustryRequest $request): RedirectResponse
    {
        $industry = new Industry();
        $industry->description = $request->input('description');
        $industry->status = $request->input('status');
        $industry->creator_id = auth()->id();
        $industry->save();

        return redirect(route('industry.index'))->with('flash_success', 'Master Data Industry captured successfully');
    }

    public function show($industry_id): View
    {
        $parameters = [
            'industry' => Industry::where('id', '=', $industry_id)->get(),
        ];

        return View('master_data.industry.show')->with($parameters);
    }

    public function edit($industry_id): View
    {
        $autocomplete_elements = Industry::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'industry' => Industry::where('id', '=', $industry_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.industry.edit')->with($parameters);
    }

    public function update(UpdateIndustryRequest $request, $industry_id): RedirectResponse
    {
        $industry = Industry::find($industry_id);
        $industry->description = $request->input('description');
        $industry->status = $request->input('status');
        $industry->creator_id = auth()->id();
        $industry->save();

        return redirect(route('industry.index'))->with('flash_success', 'Master Data Industry saved successfully');
    }

    public function destroy($industry_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product suspended successfully.", 'tr'=>'tr_'.$id]);
        // Industry::destroy($industry_id);
        $item = Industry::find($industry_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('industry.index')->with('success', 'Master Data Industry suspended successfully');
    }
}
