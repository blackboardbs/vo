<?php

namespace App\Http\Controllers;

use App\Enum\YesOrNoEnum;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Customer;
use App\Exports\TasksExport;
use App\Models\InsightTask;
use App\Models\InsightTaskList;
use App\Models\InvoiceStatus;
use App\Models\Project;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Timesheet\TimesheetHelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class TaskReportController extends Controller
{
    use TimesheetHelperTrait;

    public function detail(Request $request)
    {
        $tasks = InsightTask::select([
            'customer_name', 'customer_id',
            'project_name', 'project_id',
            'employee_id', DB::raw('CONCAT(`consultant_first_name`, " ", `consultant_last_name`) AS resource'),
            'task_id', 'task', 'description_of_work',
            'timesheet_date',
            DB::raw('SUM(Hours_Billable * 60) AS billable_minutes'), DB::raw('SUM(Hours_Non_Billable * 60) AS non_billable_minutes')
        ])->where(
            fn($task) => $task->where('Hours_Billable', '>', 0)->orWhere('Hours_Non_Billable', '>', 0)
        )->when($request->mf, fn($task) => $task->where('year_month', '>=',str_replace('-','', $request->mf)))
            ->when($request->mt, fn($task) => $task->where('year_month', '<=',str_replace('-','',$request->mt)))
            ->when($request->customer, fn($task) => $task->where('customer_id', $request->customer))
            ->when($request->project, fn($task) => $task->where('project_id', $request->project))
            ->when($request->company, fn($task) => $task->where('company_id', $request->company))
            ->when($request->employee, fn($task) => $task->where('employee_id', $request->employee))
            ->when($request->task, fn($task) => $task->where('task_id', $request->task))
            ->when($request->pf, fn($task) => $task->where('year_week', '>=', $request->pf))
            ->when($request->pt, fn($task) => $task->where('year_week', '<=', $request->pt))
            ->when($request->b, fn($task) => $task->where('is_billable', $request->b))
            ->unless($request->b == 0, fn($task) => $task->where('is_billable', $request->b))
            ->when($request->is, fn($task) => $task->where('invoice_status_id', $request->is))
            ->when($request->cct, fn($task) => $task->where('cost_center_id', $request->cct))
            ->groupBy('customer_id', 'project_id', 'task_id', 'description_of_work', 'timesheet_date', 'customer_name', 'project_name', 'employee_id', 'consultant_first_name', 'consultant_last_name', 'task')
            ->latest('timesheet_date')->paginate($request->r??15);

        if ($request->has('export')) {
            return Excel::download(new TasksExport($tasks, 'reports.task.exports.export-detail'), 'task-details.xlsx');
        }

        $invoice_status_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);
        $cost_centre_drop_down = CostCenter::orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);

        $parameters = [
            'tasks' => $tasks,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'task_drop_down' => $this->generateTaskDropdown($request),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'week_dropdown' => $this->generateYearWeek2($request),
            'year_month_drop_down' => $this->generateYearMonth2($request),
            'cost_centre_drop_down' => $cost_centre_drop_down
        ];

        return view('reports.task.detail')->with($parameters);
    }

    public function finance(Request $request)
    {
        $tasks = InsightTask::select([
            'customer_id', 'customer_name',
            'project_id', 'project_name',
            'employee_id', DB::raw('CONCAT(`consultant_first_name`, " ", `consultant_last_name`) AS resource'),
            'task_id', 'task',
            'timesheet_date',
            DB::raw('SUM(Hours_Billable * 60) AS billable_minutes'),
            'invoice_reference',
            'invoice_status'
        ])->where('Hours_Billable', '>', 0)
            ->when($request->mf, fn($task) => $task->where('year_month', '>=',str_replace('-','', $request->mf)))
            ->when($request->mt, fn($task) => $task->where('year_month', '<=',str_replace('-','',$request->mt)))
            ->when($request->customer, fn($task) => $task->where('customer_id', $request->customer))
            ->when($request->project, fn($task) => $task->where('project_id', $request->project))
            ->when($request->company, fn($task) => $task->where('company_id', $request->company))
            ->when($request->employee, fn($task) => $task->where('employee_id', $request->employee))
            ->when($request->task, fn($task) => $task->where('task_id', $request->task))
            ->when($request->pf, fn($task) => $task->where('year_week', '>=', $request->pf))
            ->when($request->pt, fn($task) => $task->where('year_week', '<=', $request->pt))
            ->when($request->b, fn($task) => $task->where('is_billable', $request->b))
            ->unless($request->b == 0, fn($task) => $task->where('is_billable', $request->b))
            ->when($request->is, fn($task) => $task->where('invoice_status_id', $request->is))
            ->when($request->cct, fn($task) => $task->where('cost_center_id', $request->cct))
            ->groupBy('customer_id', 'customer_name', 'project_id', 'project_name', 'employee_id', 'consultant_first_name', 'consultant_last_name', 'task_id', 'task', 'timesheet_date', 'invoice_reference', 'invoice_status')
            ->latest('timesheet_date')->paginate($request->r??15);

        if ($request->has('export')) {
            return Excel::download(new TasksExport($tasks, 'reports.task.exports.export-finance'), 'task-finance.xlsx');
        }

        $invoice_status_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);
        $cost_centre_drop_down = CostCenter::orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);


        $parameters = [
            'tasks' => $tasks,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'task_drop_down' => $this->generateTaskDropdown($request),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'weeks_dropdown' => $this->generateYearWeek2($request),
            'year_month_drop_down' => $this->generateYearMonth2($request),
            'cost_centre_drop_down' => $cost_centre_drop_down,
        ];

        return view('reports.task.finance')->with($parameters);
    }

    public function list(Request $request)
    {

        $data = InsightTaskList::selectRaw('customer_id, customer, project_id, project, employee_id, CONCAT(`first_name`, " ",`last_name`) AS resource, task_id, task_description, SUM(IF(is_billable = 1, actual_hours,0)) AS billable_hours, SUM(IF(is_billable = 0, actual_hours,0)) AS non_billable_hours, hours_planned')
            ->where('actual_hours', '>', 0)
            ->when($request->mf, fn($task) => $task->where('year_month', '>=',str_replace('-','', $request->mf)))
            ->when($request->mt, fn($task) => $task->where('year_month', '<=',str_replace('-','',$request->mt)))
            ->when($request->cc, fn($task) => $task->where('customer_id', $request->cc))
            ->when($request->p, fn($task) => $task->where('project_id', $request->p))
            ->when($request->c, fn($task) => $task->where('company_id', $request->c))
            ->when($request->r, fn($task) => $task->where('employee_id', $request->r))
            ->when($request->t, fn($task) => $task->where('task_id', $request->t))
            ->when($request->pf, fn($task) => $task->where('year_week', '>=', $request->pf))
            ->when($request->pt, fn($task) => $task->where('year_week', '<=', $request->pt))
            ->when($request->b, fn($task) => $task->where('is_billable', $request->b))
            ->unless($request->b == 0, fn($task) => $task->where('is_billable', $request->b))
            ->when($request->is, fn($task) => $task->where('invoice_status_id', $request->is))
            ->when($request->cct, fn($task) => $task->where('cost_center_id', $request->cct))
            ->groupBy('customer_id', 'project_id', 'employee_id', 'task_id', 'hours_planned')
            ->latest('year_month')
            ->get();

        $year_month_drop_down = $this->generateYearMonth();

        if ($request->has('export')) {
            return Excel::download(new TasksExport($data, 'reports.task.exports.export-list'), 'task-list.xlsx');
        }

        $project_drop_down = Project::orderBy('name')->orderBy('name')->pluck('name', 'id')->prepend('All', null);
        $invoice_status_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);
        $week_drop_down = Timesheet::orderBy('year_week', 'desc')->whereNotNull('year_week')->pluck('year_week', 'year_week')->prepend('All', null);
        $cost_centre_drop_down = CostCenter::orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);

        $parameters = [
            'tasks' => $data,
            'project_drop_down' => $project_drop_down,
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'weeks_dropdown' => $week_drop_down,
            'year_month_drop_down' => $year_month_drop_down,
            'cost_centre_drop_down' => $cost_centre_drop_down
        ];

        return view('reports.task.list')->with($parameters);
    }

    public function listDescription(Request $request)
    {
        $tasks = InsightTask::select([
            'customer_id', 'customer_name',
            'project_id', 'project_name',
            'employee_id', DB::raw('CONCAT(`consultant_first_name`," ",`consultant_last_name`) AS resource'),
            DB::raw('IFNULL(`description_of_work`, `task`) AS description'),
            DB::raw('SUM(`Hours_Billable`*60) AS billable_minutes'),
            DB::raw('SUM(`Hours_Non_Billable`*60) AS non_billable_minutes'),
            DB::raw('(SELECT DISTINCT `task_view`.`hours_planned` FROM `task_view` WHERE `task_view`.`task_id` = `timesheet_report_view`.`task_id`) AS planned_hours')
        ])->where(
            fn($task) => $task->where('Hours_Billable', '>', 0)->orWhere('Hours_Non_Billable', '>', 0)
        )->when($request->mf, fn($task) => $task->where('year_month', '>=',str_replace('-','', $request->mf)))
            ->when($request->mt, fn($task) => $task->where('year_month', '<=',str_replace('-','',$request->mt)))
            ->when($request->customer, fn($task) => $task->where('customer_id', $request->customer))
            ->when($request->project, fn($task) => $task->where('project_id', $request->project))
            ->when($request->company, fn($task) => $task->where('company_id', $request->company))
            ->when($request->employee, fn($task) => $task->where('employee_id', $request->employee))
            ->when($request->task, fn($task) => $task->where('task_id', $request->task))
            ->when($request->pf, fn($task) => $task->where('year_week', '>=', $request->pf))
            ->when($request->pt, fn($task) => $task->where('year_week', '<=', $request->pt))
            ->when($request->b, fn($task) => $task->where('is_billable', $request->b))
            ->unless($request->b == 0, fn($task) => $task->where('is_billable', $request->b))
            ->when($request->is, fn($task) => $task->where('invoice_status_id', $request->is))
            ->when($request->cct, fn($task) => $task->where('cost_center_id', $request->cct))
            ->groupBy('customer_id', 'project_id', 'employee_id', 'customer_name', 'project_name', 'consultant_first_name', 'consultant_last_name', 'description_of_work', 'task', 'task_id')
            ->paginate($request->r??15);

        $year_month_drop_down = $this->generateYearMonth();

        if ($request->has('export')) {
            return Excel::download(new TasksExport($tasks, 'reports.task.exports.export-list-description'), 'task-list-description.xlsx');
        }

        $invoice_status_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);
        $cost_centre_drop_down = CostCenter::orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id')->prepend('All', null);

        $parameters = [
            'tasks' => $tasks,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'task_drop_down' => $this->generateTaskDropdown($request),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'weeks_dropdown' => $this->generateYearWeek2($request),
            'year_month_drop_down' => $this->generateYearMonth2($request),
            'cost_centre_drop_down' => $cost_centre_drop_down,
        ];

        return view('reports.task.list_description')->with($parameters);
    }

    public function utilisation(Request $request)
    {
        $tasks = InsightTaskList::selectRaw('customer_id, customer, project_id, project, employee_id, CONCAT(first_name," ",last_name) AS resource, task_id, task_description, task_start_date, task_end_date, SUM(IF(is_billable = 1, (actual_hours * 60),0)) AS billable_minutes, SUM(IF(is_billable = 0, (actual_hours * 60),0)) AS non_billable_minutes, hours_planned, task_status')
            ->when($request->mf, fn($task) => $task->where('year_month', '>=',str_replace('-','', $request->mf)))
            ->when($request->mt, fn($task) => $task->where('year_month', '<=',str_replace('-','',$request->mt)))
            ->when($request->customer, fn($task) => $task->where('customer_id', $request->customer))
            ->when($request->project, fn($task) => $task->where('project_id', $request->project))
            ->when($request->company, fn($task) => $task->where('company_id', $request->company))
            ->when($request->employee, fn($task) => $task->where('employee_id', $request->employee))
            ->when($request->task, fn($task) => $task->where('task_id', $request->task))
            ->when($request->pf, fn($task) => $task->where('year_week', '>=', $request->pf))
            ->when($request->pt, fn($task) => $task->where('year_week', '<=', $request->pt))
            ->when($request->b, fn($task) => $task->where('is_billable', $request->b))
            ->unless($request->b == 0, fn($task) => $task->where('is_billable', $request->b))
            ->when($request->is, fn($task) => $task->where('invoice_status_id', $request->is))
            ->when($request->cct, fn($task) => $task->where('cost_center_id', $request->cct))
            ->when($request->ts, fn($task) => $task->where('task_status_id', $request->ts))
            ->groupBy('customer_id', 'project_id', 'employee_id', 'task_id')
            ->paginate($request->r??15);

        $year_month_drop_down = $this->generateYearMonth();

        if ($request->has('export')) {
            return Excel::download(new TasksExport($tasks, 'reports.task.exports.export-utilization-per-task'), 'task-utilization.xlsx');
        }

        $invoice_status_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id');
        $task_status_drop_down = TaskStatus::orderBy('description', 'asc')->whereNotNull('description')->pluck('description', 'id');
        $cost_centre_drop_down = CostCenter::orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id');

        $parameters = [
            'tasks' => $tasks,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'task_drop_down' => $this->generateTaskDropdown($request),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'weeks_dropdown' => $this->generateYearWeek2($request),
            'year_month_drop_down' => $this->generateYearMonth2($request),
            'task_status_drop_down' => $task_status_drop_down,
            'cost_centre_drop_down' => $cost_centre_drop_down
        ];

        return view('reports.task.utilisation')->with($parameters);
    }

    public function taskSummaryDescription(Request $request)
    {
        $timesheets = Timesheet::with(['project','customer','resource','resource.user'])->orderBy('id')
            ->when($request->has('mf') && ($request->input('mf') != '-1'),function($q) use ($request) {
                $year_month_array = explode('-', $request->input('mf'));
                $q->where('year', '>=', $year_month_array[0])->where('month', '>=', $year_month_array[1]);
            })
            ->when($request->has('mt') && ($request->input('mt') != '-1'),function($q) use ($request) {
                $year_month_array = explode('-', $request->input('mt'));
                $q->where('year', '<=', $year_month_array[0])->where('month', '<=', $year_month_array[1]);
            })
            ->when($request->has('customer') && ($request->input('customer') != '-1') && $request->input('customer')  != '', function ($q) use ($request) {
                $q->where('customer_id', '=', $request->input('customer'));
            })
            ->when($request->has('project') && ($request->input('project') != '-1') && $request->input('project') != '',function ($q) use ($request) {
                $q->where('project_id', '=', $request->input('project'));
            })
            ->when($request->has('company') && ($request->input('company') != '-1') && $request->input('company') != '',function ($q) use ($request) {
                $q->where('company_id', '=', $request->input('company'));
            })
            ->when($request->has('employee') && ($request->input('employee') != '-1') && $request->input('employee') != '',function ($q) use ($request) {
                $q->where('employee_id', '=', $request->input('employee'));
            })
            ->when($request->has('pf') && $request->input('pf') != null && $request->input('pf') != '-1' && $request->input('pf') != '',function($q) use ($request) {
                $q->where('year_week', '>=', $request->input('pf'));
            })
            ->when($request->has('pt') && $request->input('pt') != null && $request->input('pt') != '-1' && $request->input('pf') != '',function($q) use ($request) {
                $q->where('year_week', '<=', $request->input('pt'));
            })
            ->when($request->has('is') && $request->input('is') != '-1' && $request->input('is') != '',function($q) use ($request) {
                $q->where('bill_status', '=', $request->input('is'));
            })
            ->when($request->has('task') && $request->input('task') != '-1' && $request->input('task') != '',function($q) use ($request) {
                $task = Task::find($request->input('task')); //Get the specific task
                $timesheet_ids = Timeline::where('task_id', '=', $task->id)->pluck('timesheet_id')->toArray(); //Get the timeline
                $q->whereIn('id', $timesheet_ids); //Now filter timesheets
            })->paginate(15);

        $is_billable = null;
        if ($request->has('b') && $request->input('b') != -1 && $request->input('b') != '') {
            $is_billable = $request->input('b');
        }

        if ($request->has('cct') && $request->input('cct') != -1 && $request->input('cct') != '') {
            $cost_center_id = $request->input('cct');
            $timesheets = $timesheets->filter(function ($timesheet) use ($cost_center_id) {
                return $timesheet->resource->cost_center_id == $cost_center_id;
            });
        }

        $task_status_id = null;
        if ($request->has('ts') && $request->input('ts') != -1 && $request->input('ts') != '') {
            $task_status_id = $request->input('ts');
        }

        $tasks = $this->getTasksGroupByTask($timesheets, $is_billable, $task_status_id);

        if ($request->has('export')) {
            return Excel::download(new TasksExport($tasks, 'reports.task.exports.export-task-summary-description'), 'task-summary-description.xlsx');
        }

        $parameters = $tasks;

        $parameters['company_drop_down'] = Company::filters()->orderBy('company_name')->pluck('company_name', 'id');
        $parameters['customer_drop_down'] = Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id');
        $parameters['project_drop_down'] = Project::filters()->orderBy('name')->orderBy('name')->pluck('name', 'id');
        $parameters['task_drop_down'] = $this->generateTaskDropdown($request);
        $parameters['invoice_status_drop_down'] = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id');
        $parameters['period_from_drop_down'] = $this->generateYearWeek2($request);
        $parameters['period_to_drop_down'] = $this->generateYearWeek2($request);
        $parameters['task_status_drop_down'] = TaskStatus::orderBy('description', 'asc')->whereNotNull('description')->pluck('description', 'id');
        $parameters['year_month_drop_down'] = $this->generateYearMonth2($request);
        $parameters['cost_centre_drop_down'] = CostCenter::filters()->orderBy('description', 'desc')->whereNotNull('description')->pluck('description', 'id');
        $parameters['resource_drop_down'] = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        return view('reports.task.summary_description')->with($parameters);
    }

    public function getTasks($timesheets, $is_billable = null, $task_status_id = null)
    {
        $timeline_minutes_total_non_billable = 0;
        $timeline_minutes_total_billable = 0;
        $hours_planned_total = 0;
        $timeline_counter = 0;
        $tasks = [];

        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();

            if ($is_billable != null) {
                $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->where('is_billable', '=', $is_billable)->with('task')->get();
            }

            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                //Monday
                $mon_date[$timesheet->year_week] = $start_of_week_date;
                $tue_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                $sat_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                $sun_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));

                $description_of_work = isset($timeline->task->description) ? $timeline->task->description : '';
                $hours_planned = isset($timeline->task->hours_planned) ? $timeline->task->hours_planned : 0;
                $task_status = isset($timeline->task->taskstatus->description) ? $timeline->task->taskstatus->description : 0;
                $start_date = isset($timeline->task->start_date) ? $timeline->task->start_date : '';
                $end_date = isset($timeline->task->end_date) ? $timeline->task->end_date : '';

                if ((isset($task_status_id) && $task_status_id > 0) && (isset($timeline->task->taskstatus->id) && $timeline->task->taskstatus->id != $task_status_id)) {
                    continue; //This should work
                }

                if ($description_of_work != '') {
                    if ($timeline->description_of_work != '') {
                        $description_of_work .= ' - '.$timeline->description_of_work;
                    }
                } else {
                    $description_of_work = $timeline->description_of_work;
                }

                if ((($timeline->mon * 60) + $timeline->mon_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->mon * 60) + $timeline->mon_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->tue * 60) + $timeline->tue_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->tue * 60) + $timeline->tue_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $tue_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->wed * 60) + $timeline->wed_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->wed * 60) + $timeline->wed_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $wed_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->thu * 60) + $timeline->thu_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->thu * 60) + $timeline->thu_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $thu_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->fri * 60) + $timeline->fri_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->fri * 60) + $timeline->fri_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $fri_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->sat * 60) + $timeline->sat_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->sat * 60) + $timeline->sat_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $sat_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }

                if ((($timeline->sun * 60) + $timeline->sun_m) > 0) {
                    $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                    $tasks[$timeline_counter]['total_minutes'] = ($timeline->sun * 60) + $timeline->sun_m;
                    $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                    $tasks[$timeline_counter]['date'] = $sun_date[$timesheet->year_week];
                    $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                    $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                    $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                    $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                    $tasks[$timeline_counter]['task_status'] = $task_status;
                    $tasks[$timeline_counter]['start_date'] = $start_date;
                    $tasks[$timeline_counter]['end_date'] = $end_date;
                    $tasks[$timeline_counter]['timesheet'] = $timesheet;

                    if ($tasks[$timeline_counter]['is_billable'] == 1) {
                        $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                    } else {
                        $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                    }

                    $hours_planned_total += $hours_planned;

                    $timeline_counter += 1;
                }
            }
        }

        $parameters = [
            'timeline_minutes_total_billable' => $this->minutesToTime($timeline_minutes_total_billable),
            'timeline_minutes_total_non_billable' => $this->minutesToTime($timeline_minutes_total_non_billable),
            'hours_planned_total' => $hours_planned_total,
            'tasks' => $tasks,
        ];

        return $parameters;
    }

    public function getTasksGroupByTask($timesheets, $is_billable = null, $task_status_id = null)
    {
        $timeline_minutes_total_non_billable = 0;
        $timeline_minutes_total_billable = 0;
        $hours_planned_total = 0;
        $timeline_counter = 0;
        $tasks = [];

        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::with(['task','task.taskstatus'])->where('timesheet_id', '=', $timesheet->id)->with('task')->get();

            if ($is_billable != null) {
                $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->where('is_billable', '=', $is_billable)->with('task')->get();
            }

            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                //Monday
                $mon_date[$timesheet->year_week] = $start_of_week_date;

                $description_of_work = isset($timeline->task->description) ? $timeline->task->description : '';
                $hours_planned = isset($timeline->task->hours_planned) ? $timeline->task->hours_planned : 0;
                $task_status = isset($timeline->task->taskstatus->description) ? $timeline->task->taskstatus->description : 0;
                $start_date = isset($timeline->task->start_date) ? $timeline->task->start_date : '';
                $end_date = isset($timeline->task->end_date) ? $timeline->task->end_date : '';

                if ((isset($task_status_id) && $task_status_id > 0) && (isset($timeline->task->taskstatus->id) && $timeline->task->taskstatus->id != $task_status_id)) {
                    continue; //This should work
                }

                if ($description_of_work != '') {
                    if ($timeline->description_of_work != '') {
                        $description_of_work .= ' - '.$timeline->description_of_work;
                    }
                } else {
                    $description_of_work = $timeline->description_of_work;
                }

                $tasks[$timeline_counter]['is_billable'] = $timeline->is_billable;
                $tasks[$timeline_counter]['total_minutes'] = ($timeline->total * 60) + $timeline->total_m;
                $tasks[$timeline_counter]['total_minutes_to_time'] = $this->minutesToTime($tasks[$timeline_counter]['total_minutes']);
                $tasks[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                $tasks[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                $tasks[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                $tasks[$timeline_counter]['description_of_work'] = $description_of_work;
                $tasks[$timeline_counter]['hours_planned'] = $hours_planned;
                $tasks[$timeline_counter]['task_status'] = $task_status;
                $tasks[$timeline_counter]['start_date'] = $start_date;
                $tasks[$timeline_counter]['end_date'] = $end_date;
                $tasks[$timeline_counter]['timesheet'] = $timesheet;

                if ($tasks[$timeline_counter]['is_billable'] == 1) {
                    $timeline_minutes_total_billable += $tasks[$timeline_counter]['total_minutes'];
                } else {
                    $timeline_minutes_total_non_billable += $tasks[$timeline_counter]['total_minutes'];
                }

                $hours_planned_total += $hours_planned;

                $timeline_counter += 1;
            }
        }

        $parameters = [
            'timeline_minutes_total_billable' => $this->minutesToTime($timeline_minutes_total_billable),
            'timeline_minutes_total_non_billable' => $this->minutesToTime($timeline_minutes_total_non_billable),
            'hours_planned_total' => $hours_planned_total,
            'tasks' => $tasks,
        ];

        return $parameters;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }

    public function minutesToTime($minutes)
    {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;
        $negation_number = '';
        if ($hours < 0) {
            $hours *= -1;
            $negation_number = '-';
        }

        if ($minutes < 0) {
            $minutes *= -1;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }

    /**
     * @return array
     */
    public function generateYearMonth()
    {
        $years = TimeSheet::distinct('year')->orderBy('year', 'desc')->pluck('year');

        $year_month_drop_down = [];
        foreach ($years as $year) {
            for ($i = 12; $i >= 1; $i--) {
                if (($year == date('Y')) && ($i > date('m'))) {
                    continue;
                }
                if ($i < 10) {
                    $i = '0' . $i;
                }
                $year_month_drop_down[$year . '-' . $i] = $year . $i;
            }
        }
        return $year_month_drop_down;
    }

    public function generateYearMonth2(Request $request)
    {
        $years = TimeSheet::filters()->selectRaw("DISTINCT(CONCAT(`year`,'-',CASE WHEN `month` < 10 THEN CONCAT('0',`month`) ELSE `month` END)) as yearmonth")->pluck('yearmonth');

        if(count($years) == 0){
            $years2 = TimeSheet::distinct('year')->orderBy('year', 'desc')->pluck('year');

            $year_month_drop_down = [];
            foreach ($years2 as $year) {
                for ($i = 12; $i >= 1; $i--) {
                    if (($year == date('Y')) && ($i > date('m'))) {
                        continue;
                    }
                    if ($i < 10) {
                        $i = '0' . $i;
                    }
                    $year_month_drop_down[$year . '-' . $i] = $year . $i;
                }
            }
        } else {
            $year_month_drop_down = [];
            if(isset($request->mf)){ $year_month_drop_down[$request->mf] = str_replace('-','',$request->mf); }
            if(isset($request->mt) && $request->mt != $request->mf){ $year_month_drop_down[$request->mt] = str_replace('-','',$request->mt); }
            foreach ($years as $key => $year) {
                if(str_replace('-','',$year) != $request->mt && str_replace('-','',$year) != $request->mf){
                    $year_month_drop_down[$year] = str_replace('-','',$year);
                }
            }
        }
        arsort($year_month_drop_down);
        return $year_month_drop_down;
    }

    public function generateYearWeek2(Request $request)
    {
        $weeks = TimeSheet::filters()->orderBy('year_week', 'desc')->whereNotNull('year_week')->pluck('year_week', 'year_week');

        if(count($weeks) == 0){
            $year_week_drop_down = TimeSheet::distinct('year_week')->orderBy('year_week', 'desc')->pluck('year_week','year_week');
        } else {
            $year_week_drop_down = collect($weeks)->toArray();
            if(isset($request->pf)){ $year_week_drop_down[$request->pf] = $request->pf; }
            if(isset($request->pt) && $request->pt != $request->pf){ $year_week_drop_down[$request->pt] = $request->pt; }
            arsort($year_week_drop_down);
        }
        return $year_week_drop_down;
    }

    public function generateTaskDropdown(Request $request){
        $list = Task::when(request()->employee,function($q){
            $q->where('employee_id', request()->employee);
        })->when(request()->project,function($q){
            $q->where('project_id','=', request()->project);
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
            $task->where('customer_id', request()->customer);
        })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
            $task->whereHas('project.company', function ($t) {
                $t->where('id', '=', request()->company);
            });
        })->orderBy('description')->pluck('description', 'id');

        return $list;
    }
}
