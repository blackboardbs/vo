<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Commission;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Resource;
use App\Models\Team;
use App\Models\Time;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\V_Time;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function summary(Request $request): View
    {
        $timesheet41c = Timesheet::select('employee_id')->groupBy('employee_id')->get();

        if ($request->has('company') && $request->company != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('company_id', '=', $request->company)->groupBy('employee_id')->get();
        }

        if ($request->has('team') && $request->team != null) {
            $timesheet41c = Timesheet::select('employee_id')->whereHas('resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            })->groupBy('employee_id')->get();
        }

        if ($request->has('employee') && $request->employee != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('employee_id', '=', $request->employee)->groupBy('employee_id')->get();
        }

        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('year', '=', substr($request->yearmonth, 0, 4))->where('month', '=', substr($request->yearmonth, 4, 2))->groupBy('employee_id')->get();
        }

        /*if ($request->has('dates') && $request->dates != null){

            $timesheet41c = V_Time::select(DB::raw("distinct v_time.employee_id"))
                ->join('v_yearwk', 'v_time.yearwk', '=', 'v_yearwk.yearwk')
                ->with('user.resource.commission')
                ->whereBetween('invoice_date', [substr($request->dates,0, 10), substr($request->dates,13, 10)])
                ->groupBy('v_time.employee_id')
                ->groupBy('v_yearwk.yearmonth')
                ->orderBy('v_time.employee_id')
                ->orderBy('v_yearwk.yearmonth')
                ->get();
        }*/

        if ($request->has('customer') && $request->customer != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('customer_id', '=', $request->customer)->groupBy('employee_id')->get();
        }

        if ($request->has('project') && $request->project != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('project_id', '=', $request->project)->groupBy('employee_id')->get();
        }

        if ($request->has('billable') && $request->billable != null) {
            $timesheet41c = Timesheet::select('employee_id')->where('is_billable', '=', $request->billable)->groupBy('employee_id')->get();
        }

        $yearmonth_list = Calendar::select(DB::raw('DISTINCT(year) as year, month'))
            ->where('date', '<=', Carbon::now()->toDateString())
            ->where('status', '=', 1)
            ->orderBy('year', 'desc')
            ->orderBy('week', 'desc')
            ->paginate(15);

        //return $timesheet41c;

        $employee = [];
        $comm_hours1 = [];

        foreach ($timesheet41c as $timesheet) {
            foreach ($yearmonth_list as $list) {
                $year = $list->year;
                $month = ($list->month < 10) ? '0'.$list->month : $list->month;
                $cat[] = $list->month;

                $employee[$timesheet->employee_id] = Timesheet::select(DB::raw('timesheet.employee_id, timesheet.company_id, timesheet.year, timesheet.month,assignment.invoice_rate,
                    SUM(CASE WHEN (timeline.is_billable = 1) THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours_bill,
                    SUM(CASE WHEN (timeline.is_billable = 0) THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours_no_bill'))
                    ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->where('timesheet.year', '>=', 2014)
                    ->where('timesheet.employee_id', '=', $timesheet->employee_id)
                    ->groupBy('timesheet.employee_id')
                    ->groupBy('timesheet.company_id')
                    ->groupBy('timesheet.year')
                    ->groupBy('timesheet.month')
                    ->groupBy('assignment.invoice_rate')
                    ->orderBy('timesheet.year', 'desc')
                    ->orderBy('timesheet.month', 'desc')
                    ->take(12)
                    ->distinct()
                    ->get();

                $expenses[$timesheet->employee_id] = Timesheet::select(DB::raw('DISTINCT timesheet.employee_id, timesheet.company_id, timesheet.year, timesheet.month,
                    SUM(CASE WHEN (time_exp.claim = 1) THEN time_exp.amount ELSE 0 END) AS expense_claim,
                    SUM(CASE WHEN (time_exp.is_billable = 1) THEN time_exp.amount ELSE 0 END) AS expense_bill'))
                    ->leftJoin('time_exp', 'timesheet.id', '=', 'time_exp.timesheet_id')
                    ->where('timesheet.year', '>=', 2014)
                    ->where('timesheet.employee_id', '=', $timesheet->employee_id)
                    ->groupBy('timesheet.employee_id')
                    ->groupBy('timesheet.company_id')
                    ->groupBy('timesheet.year')
                    ->groupBy('timesheet.month')
                    ->orderBy('timesheet.year', 'desc')
                    ->orderBy('timesheet.month', 'desc')
                    ->take(12)
                    ->get();

                $comm_hours1[$timesheet->employee_id] = Timesheet::select(DB::raw('timesheet.year, timesheet.month, timesheet.employee_id, 
                    IFNULL(assignment.rate, 0) AS invoice_rate,
                    SUM(timeline.total + ROUND((timeline.total_m/60),2)) AS hours'))
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                        $join->on('timesheet.project_id', '=', 'assignment.project_id');
                    })
                    ->leftJoin('timeline', 'timesheet.id', 'timeline.timesheet_id')
                    ->where('timesheet.employee_id', '=', $timesheet->employee_id)
                    ->where('timesheet.year', '=', $list->year)
                    //->where('timeline.is_billable', '=', 1)
                    ->groupBy('timesheet.employee_id')
                    ->groupBy('timesheet.year')
                    ->groupBy('assignment.rate')
                    ->groupBy('timesheet.month')
                    ->orderBy('timesheet.year')
                    ->orderBy('timesheet.month')
                    ->take(12)
                    ->get();
            }
        }

        $calendar = Calendar::where('status', '=', 1)->whereDate('date', '<=', Carbon::now()->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get()->unique();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'employee' => $timesheet41c,
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'reports' => $employee,
            'expenses' => $expenses,
            'x01' => 0,
            'x02' => 0,
            'x03' => 0,
            'x04' => 0,
            'x05' => 0,
            'x06' => 0,
            'x07' => 0,
            'x08' => 0,
            'x09' => 0,
            'x10' => 0,
            'x11' => 0,
            'x12' => 0,
            'total_ldays' => 0,
            'total_tcomm' => 0,
            'total_expenses_claim' => 0,
        ];

        return view('reports.summary')->with($parameters);
    }
}
