<?php

namespace App\Http\Controllers;

use App\Models\CustomerInvoice;
use App\Models\InvoiceItems;
use App\Models\VendorInvoice;
use Illuminate\Http\Request;

class InvoiceItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $invoiceItems = InvoiceItems::where('status_id', '=', 1);

        if($request->has('invoice_id') && ($request->input('invoice_id') != 0)){
            $invoiceItems = $invoiceItems->where('invoice_id', '=', $request->input('invoice_id'));
        }

        $invoiceItems = $invoiceItems->get();

        return $invoiceItems;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceItems $invoiceItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceItems $invoiceItems)
    {
        //
    }

    public function getInvoiceItems($invoice_id, $invoice_type_id)
    {
        $invoiceItems = InvoiceItems::selectRaw('*,0 as canEdit')->where('status_id', '=', 1)->where('invoice_id', '=', $invoice_id)->where('invoice_type_id', '=', $invoice_type_id)->get();

        return $invoiceItems;
    }

    public function addInvoiceItem(Request $request)
    {
        $invoiceItem = new InvoiceItems();
        $invoiceItem->invoice_id = $request->input('invoice_id');
        $invoiceItem->invoice_type_id = $request->input('invoice_type_id');
        $invoiceItem->description = $request->input('description');
        $invoiceItem->quantity = $request->input('quantity');
        $invoiceItem->price_excl = $request->input('price_excl');
        $invoiceItem->vat_code = $request->input('vat_code');
        $invoiceItem->vat_percentage = $request->input('vat_percentage');
        $invoiceItem->discount_amount = $request->input('discount_amount');
        $invoiceItem->total = $request->input('total');
        $invoiceItem->creator_id = auth()->id();
        $invoiceItem->status_id = 1;
        $invoiceItem->created_at = now();
        $invoiceItem->save();

        $invoiceItems = InvoiceItems::selectRaw('*,0 as canEdit')->where('status_id', '=', 1)->where('invoice_type_id', '=', $invoiceItem->invoice_type_id)->where('invoice_id', '=', $invoiceItem->invoice_id)->get();

        if($invoiceItem->invoice_type_id == 1){
            $customerInvoice = CustomerInvoice::find($invoiceItem->invoice_id);
            $customerInvoice->invoice_value = (string) $this->lineItemsTotal($invoiceItem);
            $customerInvoice->save();

            if($customerInvoice->histories()->count()){
                $itemCopy = $invoiceItem->replicate();
                $itemCopy->invoice_id = $customerInvoice->histories()->first()?->id;
                $itemCopy->save();

                $generatedInvoice = $customerInvoice->histories()->first();
                $generatedInvoice->invoice_value = (string) $this->lineItemsTotal($itemCopy);
                $generatedInvoice->save();
            }
        }

        if($invoiceItem->invoice_type_id == 2){
            $vendorInvoice = VendorInvoice::find($invoiceItem->invoice_id);
            $vendorInvoice->vendor_invoice_value = (string) $this->lineItemsTotal($invoiceItem);
            $vendorInvoice->save();

            if($vendorInvoice->histories()->count()){
                $itemCopy = $invoiceItem->replicate();
                $itemCopy->invoice_id = $vendorInvoice->histories()->first()?->id;
                $itemCopy->save();

                $generatedInvoice = $vendorInvoice->histories()->first();
                $generatedInvoice->vendor_invoice_value = (string) $this->lineItemsTotal($itemCopy);
                $generatedInvoice->save();
            }
        }

        return $invoiceItems;
    }

    public function update(Request $request, $invoiceItems_ID)
    {
        $invoiceItem = InvoiceItems::find($invoiceItems_ID);
        $invoiceItem->invoice_id = $request->input('invoice_id');
        $invoiceItem->invoice_type_id = $request->input('invoice_type_id');
        $invoiceItem->description = $request->input('description');
        $invoiceItem->quantity = $request->input('quantity');
        $invoiceItem->price_excl = $request->input('price_excl');
        $invoiceItem->vat_code = $request->input('vat_code');
        $invoiceItem->vat_percentage = $request->input('vat_percentage');
        $invoiceItem->discount_amount = $request->input('discount_amount');
        $invoiceItem->total = $request->input('total');
        $invoiceItem->creator_id = auth()->id();
        $invoiceItem->status_id = 1;
        $invoiceItem->created_at = now();
        $invoiceItem->save();

        $invoiceItems = InvoiceItems::selectRaw('*,0 as canEdit')->where('status_id', '=', 1)->where('invoice_type_id', '=', $invoiceItem->invoice_type_id)->where('invoice_id', '=', $invoiceItem->invoice_id)->get();

        $totalInvoiceAmount = $invoiceItems->sum('total');

        if($invoiceItem->invoice_type_id == 1){
            $customerInvoice = CustomerInvoice::find($invoiceItem->invoice_id);
            $customerInvoice->invoice_value = (string) $totalInvoiceAmount;
            $customerInvoice->save();
        }

        if($invoiceItem->invoice_type_id == 2){
            $vendorInvoice = VendorInvoice::find($invoiceItem->invoice_id);
            $vendorInvoice->vendor_invoice_value = (string) $totalInvoiceAmount;
            $vendorInvoice->save();
        }

        return $invoiceItems;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceItems  $invoiceItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($invoiceItems_ID)
    {
        $invoiceItem = InvoiceItems::find($invoiceItems_ID);

        $invoice_id = $invoiceItem->invoice_id;
        $invoice_type_id = $invoiceItem->invoice_type_id;

        InvoiceItems::destroy($invoiceItems_ID);

        $invoiceItems = InvoiceItems::where('status_id', '=', 1)->where('invoice_type_id', '=', $invoice_type_id)->where('invoice_id', '=', $invoice_id)->get();

        $results  = [
            'message' => 'Invoice Item deleted successfully',
            'items' => $invoiceItems
        ];

        $totalInvoiceAmount = 0.00;
        $totalQuantity = 0.00;
        $totalExclusive = 0.00;
        $total_vat = 0.00;
        foreach ($invoiceItems as $invoiceItem){
            $totalQuantity += $invoiceItem->quantity;
            $totalExclusive += $invoiceItem->price_excl * $invoiceItem->quantity;
            $total_vat += ($invoiceItem->price_excl * $invoiceItem->quantity) * ($invoiceItem->vat_percentage / 100);
            $totalInvoiceAmount += ($invoiceItem->price_excl * $invoiceItem->quantity) + (($invoiceItem->price_excl * $invoiceItem->quantity) * ($invoiceItem->vat_percentage / 100));
        }

        if($invoiceItem->invoice_type_id == 1){
            $customerInvoice = CustomerInvoice::find($invoice_id);
            $customerInvoice->invoice_value = $totalInvoiceAmount;
            $customerInvoice->save();
        }

        if($invoiceItem->invoice_type_id == 2){
            $vendorInvoice = VendorInvoice::find($invoice_id);
            $vendorInvoice->vendor_invoice_value = $totalInvoiceAmount;
            $vendorInvoice->save();
        }

        return $results;
    }

    private function lineItemsTotal(InvoiceItems $invoiceItem): float|int
    {
        $invoiceItems = InvoiceItems::selectRaw('*,0 as canEdit')->where('status_id', '=', 1)->where('invoice_type_id', '=', $invoiceItem->invoice_type_id)->where('invoice_id', '=', $invoiceItem->invoice_id)->get();

        return $invoiceItems->sum('total');
    }
}
