<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ResourceDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $date = Carbon::now();
        $assignments = Assignment::with(['resource','resource.vendor','resource_user','resource_user.resource_level_desc','resource_user.technical_rating','resource_user.business_skill','project_status','project','project.manager','product_owner','project.customer'])
            ->whereIn('assignment_status', [2, 3])->sortable()->paginate(9);
        $working_days = [];
        $actual_hours = [];
        $hours_forecast = [];
        $assignment_months = [];
        $total_project_budget = [];
        $working_hours = [];
        $invoice_rate = [];
        $po_value = [];
        $planned_spend = [];
        foreach ($assignments as $assignment) {
            $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->start_date, $assignment->end_date])->get()->count();
            $forecast_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $assignment->end_date])->get()->count();
            $number_of_month = Calendar::select('month', 'year')->whereBetween('date', [$assignment->start_date, $assignment->end_date])->groupBy('month', 'year')->get()->count();
            $time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();
            $budget = Assignment::selectRaw('SUM(CASE WHEN billable = '.$assignment->billable?->value.' THEN hours * '.(($assignment->billable?->value == 1) ? 'invoice_rate' : 'internal_cost_rate + external_cost_rate').' ELSE 0 END) AS value')->where('customer_po', '=', $assignment->customer_po)->where('project_id', '=', $assignment->project_id)->first();
            $rate = ($assignment->billable == 1) ? $assignment->invoice_rate : ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            array_push($working_days, $calendar);
            array_push($actual_hours, $time);
            array_push($hours_forecast, $forecast_days * 8);
            array_push($assignment_months, $number_of_month);
            array_push($total_project_budget, $budget);
            array_push($working_hours, $calendar * 8);
            array_push($invoice_rate, $rate);
            array_push($po_value, $assignment->hours * $rate);
            array_push($planned_spend, ($time->hours * $rate) + (($forecast_days * 8) * $rate));

            //return $assignment->project;
        }

        $parameters = [
            'assignments' => $assignments,
            'working_days' => $working_days,
            'actual_hours' => $actual_hours,
            'hours_forecast' => $hours_forecast,
            'assignment_months' => $assignment_months,
            'total_project_budget' => $total_project_budget,
            'working_hours' => $working_hours,
            'invoice_rate' => $invoice_rate,
            'po_value' => $po_value,
            'planned_spend' => $planned_spend,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'resource-details');

        return view('reports.management.resource-details')->with($parameters);
    }
}
