<?php

namespace App\Http\Controllers;

use App\Exports\GeneralExport;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected function convertThousandsToK($value)
    {
        if ($value >= 1000 && $value < 1000000) {
            return number_format(($value / 1000), 1, '.', ',').'K';
        }

        if ($value >= 1000000) {
            return number_format(($value / 1000000), 1, '.', ',').'M';
        }

        return number_format($value, 2, '.', ',');
    }

    public function subscriptionPackage(): array
    {
        $package = [];

        $package['TRIAL'] = ['id'=>1,'users'=>10,'non-users'=>250,'amount'=>0, 'storage' => 5];
        $package['CLASSIC'] = ['id'=>2,'users'=>10,'non-users'=>250,'amount'=>1900, 'storage' => 5];
        $package['STANDARD'] = ['id'=>3,'users'=>30,'non-users'=>750,'amount'=>4500, 'storage' => 20];
        $package['PREMIUM'] = ['id'=>4,'users'=>9999,'non-users'=>9999,'amount'=>9999, 'storage' => 100];

        return $package;
    }

    public function sessionStore(): JsonResponse
    {
        if (session()->has('invoice_timesheets')) {
            $ids = array_unique(array_merge(session('invoice_timesheets'), \request()->ids));
            session(['invoice_timesheets' => $ids]);
        } else {
            session(['invoice_timesheets' => \request()->ids]);
        }

        return response()->json(['ids' => session('invoice_timesheets')]);
    }

    public function sessionPull(): JsonResponse
    {
        $ids = [];
        foreach (session('invoice_timesheets') as $id) {
            if (! in_array($id, \request()->ids)) {
                array_push($ids, $id);
            }
        }
        session(['invoice_timesheets' => $ids]);

        return response()->json(['ids' => session('invoice_timesheets')]);
    }

    protected function export(array $data, string $directory, string $filename = 'index'): BinaryFileResponse
    {
        $exported_file_name = $filename != 'index' ? $filename : $directory;

        return Excel::download(new GeneralExport( $directory.'.excel.'.$filename, $data), str_replace('.','_', $exported_file_name).'_'.now()->format('d_m_Y_H_i_s').'.xlsx');
    }
}
