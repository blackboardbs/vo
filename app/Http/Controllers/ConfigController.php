<?php

namespace App\Http\Controllers;

use App\Actions\Config\StoreOrCreate;
use App\API\Currencies\Currency;
use App\Models\AdvancedTemplates;
use App\Models\AssessmentMaster;
use App\Models\BillingCycle;
use App\Models\Company;
use App\Models\Config;
use App\Models\CostCenter;
use App\Models\CvTemplate;
use App\Http\Requests\UpdateConfigRequest;
use App\Models\ProjectTerms;
use App\Models\Site;
use App\Models\Template;
use App\Models\ProspectStatus;
use App\Models\User;
use App\Models\VatRate;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class ConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Currency $currency, Request $request)
    {

        $default_company = Company::orderBy('id')->first();
        $default_cv_template = CvTemplate::orderBy('id')->first();

        $cv_template_drop_down = CvTemplate::orderBy('id')->pluck('name', 'id');

        $prospect_status = ProspectStatus::orderBy('id')->get();

        $parameters = [
            'site_config' => Config::first(),
            'prospect_status' => $prospect_status,
            'sites_drop_down' => Site::select('id', 'name')->pluck('name', 'id'),
            'percentage' => ['0' => 'Select percentage', '10' => '10%', '20' => '20%', '30' => '30%', '40' => '40%', '50' => '50%', '60' => '60%', '70' => '70%', '80' => '80%', '90' => '90%', '100' => '100%'],
            'assessment_master_dropdown' => AssessmentMaster::select('description', 'id')->pluck('description', 'id'),
            'vat_rate_drop_down' => VatRate::select(DB::raw("CONCAT(description, ' ', vat_rate, '%') AS vat"), 'vat_code')->pluck('vat', 'vat_code')->prepend('Select Default Vat Rate'),
            'default_company' => $default_company,
            'default_cv_template' => $default_cv_template,
            'cv_template_drop_down' => $cv_template_drop_down,
            'currencies' => $currency->currencies()['currencies'] ?? [],
            // 'currencies' => [],
            'managers_drop_down' => User::selectRaw('CONCAT(first_name," ",last_name) AS full_name, id')->where('status_id', 1)->where('expiry_date', '>', now()->toDateString())->whereHas('roles', function ($role) {
                $role->whereIn('name', ['admin', 'admin_manager', 'manager']);
            })->pluck('full_name', 'id')->prepend('Select Approver', '0'),
            'user_onboarding_drop_down' => User::selectRaw('CONCAT(first_name," ",last_name) AS full_name, id')->where('status_id', 1)->where('expiry_date', '>', now()->toDateString())->whereHas('roles', function ($role) {
                $role->whereIn('name', ['admin', 'admin_manager', 'manager']);
            })->pluck('full_name', 'id')->prepend('Select Approver', '0'),
            'client_master_service_agreement' => AdvancedTemplates::where('template_type_id', 6)->pluck('name', 'id')->prepend('Select Service Agreement', '0'),
            'provider_master_service_agreement' => AdvancedTemplates::where('template_type_id', 7)->pluck('name', 'id')->prepend('Select Service Agreement', '0'),
        ];

        return view('configs.index')->with($parameters);
    }

    public function store(Request $request, Config $config, StoreOrCreate $storeOrCreate): RedirectResponse
    {
        $storeOrCreate->action($request, $config);

        return redirect()->back()->with('flash_success', 'Configs created successfully');
    }

    public function update(UpdateConfigRequest $request, Config $config, StoreOrCreate $storeOrCreate): RedirectResponse
    {
        $storeOrCreate->action($request, $config);

        return redirect()->back()->with('flash_success', 'Configs updated successfully');
    }

    public function default_style(): RedirectResponse
    {
        $config = Config::first();
        $config->background_color = null;
        $config->font_color = null;
        $config->active_link = null;
        $config->site_logo = null;
        $config->save();

        return redirect()->back()->with('flash_success', 'Stylesheet has been reset to defaults successfully');
    }
}
