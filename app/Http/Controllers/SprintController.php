<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Epic;
use App\Models\Feature;
use App\Http\Requests\SprintRequest;
use App\Models\Goal;
use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\Project;
use App\Models\Sprint;
use App\Models\SprintBurnHourView;
use App\Models\SprintBurnTaskView;
use App\Models\SprintForecastView;
use App\Models\SprintHoursSummary;
use App\Models\SprintResource;
use App\Models\SprintStatus;
use App\Models\SprintTaskSummary;
use App\Models\Status;
use App\Models\StorePermission;
use App\Models\Task;
use App\Models\Timesheet;
use App\Models\UserStory;
use Carbon\CarbonPeriod;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SprintController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $sprints = Sprint::with(['goals' => fn($goals) => $goals->select(['sprint_id', 'name']),
            'project' => fn($project) => $project->select(['id', 'name']),
            'sprintHoursSummary' => fn($sprintHoursSummary) => $sprintHoursSummary->select(['SprintId', 'HoursPlanned', 'ActualHours']),
            'status' => fn($status) => $status->select(['id', 'description']),
            'tasks' => fn ($tasks) => $tasks->select(['id', 'sprint_id', 'hours_planned', 'status'])
        ])->select(['id', 'project_id', 'status_id', 'name', 'start_date', 'end_date'])
            ->when($request->project_id, fn($sprint) => $sprint->where('project_id', $request->project_id))
            ->when($request->q, fn($sprint) => $sprint->where('name', 'LIKE', '%'.$request->q.'%'));

        if ($request->has('status_id') && $request->status_id != 0) {
            $sprints = $sprints->where('status_id', '=', $request->status_id);
        }
        if (!$request->has('status_id')) {
            $sprints = $sprints->where('status_id', 1);
        }
        $sprints = $sprints->latest()->paginate($request->input('r'));

        $project_ids = Sprint::selectRaw('DISTINCT project_id')->with('project:id,name')->get();

        $projectDropdown = $project_ids->map(
            fn($sprint) => ['id' => $sprint->project?->id, 'name' => $sprint->project?->name]
        )->pluck('name', 'id');

        $parameters = [
            'sprints' => $sprints,
            'projectDropdown' => $projectDropdown,
            'statusDropdown' => SprintStatus::orderBy('description')->pluck('description', 'id'),
        ];

        return view('project.sprint.index')->with($parameters);
    }

    public function create(): View
    {
        $module = Module::where('name', '=', \App\Models\Sprint::class)->first();

        abort_if(! Auth::user()->canAccess($module->id, 'create_all') || ! Auth::user()->canAccess($module->id, 'create_team'), 403);

        $projectDropDown = Project::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = SprintStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);

        $parameters = [
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
        ];

        return view('project.sprint.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SprintRequest $request): RedirectResponse
    {
        $sprint = new Sprint();
        $sprint->name = $request->input('name');
        $sprint->project_id = $request->input('project_id');
        $sprint->start_date = $request->input('start_date');
        $sprint->end_date = $request->input('end_date');
        $sprint->status_id = $request->input('status_id');
        $sprint->save();

        $goal = new Goal();
        $goal->sprint_id = $sprint->id;
        $goal->name = $request->input('goal')??$request->input('name');
        $goal->save();

        Assignment::select(['employee_id', 'function', 'capacity_allocation_hours', 'capacity_allocation_min'])
            ->where('project_id', $request->input('project_id'))->get()
            ->each(fn($assignment) => SprintResource::insert([
                'sprint_id' => $sprint->id,
                'goal_id' => $goal->id,
                'employee_id' => $assignment->employee_id,
                'function_id' => $assignment->function,
                'capacity' => $assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)
            ]));

        return redirect(route('sprint.index'))->with('flash_success', 'Sprint successfully added.');
    }

    public function show(Sprint $sprint): View
    {
        ini_set('max_execution_time', 300);
        $sprint->createSprintResources();

        $sprint = $sprint->load(['goals.tasks.timelines', 'sprintReviews', 'resources' => function($resource) use($sprint){
            $resource->selectRaw("sprint_resources.id, sprint_resources.employee_id, sprint_resources.sprint_id, sprint_resources.function_id, sprint_resources.capacity, (SELECT SUM(leave.no_of_days) FROM `leave` WHERE leave.emp_id = sprint_resources.employee_id AND leave.date_from >= ? AND leave.date_to <= ?) AS leave_days", [$sprint->start_date, $sprint->end_date])
                ->with(['resource:id,first_name,last_name', 'function:id,description']);
        }, 'tasks:sprint_id,goal_id']);

        $summaries = SprintTaskSummary::where('SprintId', $sprint->id)->get();

        $summaries_hours = SprintHoursSummary::where('SprintId', $sprint->id)->get();

        $goals_overview = $sprint->goals?->map(function ($goal) use ($sprint){
            $tasks = $goal->tasks->count();
            $completed = $goal->tasks()->where('status', 5)->count();
            $done = $goal->tasks()->where('status', 4)->count();
            $overdue = $goal->tasks()->where('status', '!=', 5)->where('end_date', '<', now()->toDateString())->count();
            $actual_hours = $goal->tasks->map(function ($task) {
                return $task->timelines->sum('total') + ($task->timelines->sum('total_m')/60);
            })->sum();
            if (($tasks - $completed)){
                $outstanding_colour = 'bg-danger';
            }elseif (($done > 0) && (($tasks == $done) || ($tasks == $completed))){
                $outstanding_colour = 'bg-warning';
            }elseif (!$goal->task?->filter(fn($task) => $task->status != 5)->values()->count()){
                $outstanding_colour = 'bg-success';
            }else{
                $outstanding_colour = '';
            }
            return (object)[
                'name' => $goal->name,
                'no_of_tasks' => $tasks,
                'no_of_completed_tasks' => $completed,
                'no_of_outstanding_tasks' => ($tasks - $completed),
                'color_of_outstatbing' => $outstanding_colour,
                'no_of_overdue_tasks' => $overdue,
                'color_of_overdue' => $overdue ? 'bg-danger' : '',
                'no_of_impendiments' => $goal->tasks?->filter(fn($task) => $task->status == 6)->values()->count(),
                'hours_planned' => $goal->tasks?->sum('hours_planned'),
                'actual_hours' => $actual_hours,
                'color_of_actual_hours' => $actual_hours > $goal->tasks?->sum('hours_planned') ? 'bg-danger' : ''
            ];
        });

        $projectDropDown = Project::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = SprintStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);

        $sprint_dates = SprintBurnTaskView::where('CalendarDate', '>=', $sprint->start_date)->where('CalendarDate', '<=', $sprint->end_date)->where('Sprintid', $sprint->id)->get();

        $sprint_dates2 = SprintBurnHourView::where('CalendarDate', '>=', $sprint->start_date)->where('CalendarDate', '<=', $sprint->end_date)->where('Sprintid', $sprint->id)->get();

        $sprint_dates3 = SprintForecastView::where('CalendarDate', '>=', $sprint->start_date)->where('CalendarDate', '<=', $sprint->end_date)->where('Sprintid', $sprint->id)->get();

        $chart1 = [
            'labels' => $sprint_dates->map(fn($sprint) => $sprint->CalendarDate),
            'total_hours' => $sprint_dates->map(fn($sprint) => (int) $sprint->TargetHours),
            'burn_down' => $sprint_dates->map(fn($sprint) => (int) $sprint->CummHours)
        ];

        $chart2 = [
            'labels' => $sprint_dates2->map(fn($sprint) => $sprint->CalendarDate),
            'total_hours' => $sprint_dates2->map(fn($sprint) => (int) $sprint->TargetHours),
            'burn_down' => $sprint_dates2->map(fn($sprint) => (int) $sprint->CummHours)
        ];

        $chart3 = [
            'labels' => $sprint_dates3->map(fn($sprint) => $sprint->CalendarDate),
            'total_hours' => $sprint_dates3->map(fn($sprint) => (int) $sprint->TargetHours),
            'task_burn_down' => $sprint_dates3->map(fn($sprint) => (int) $sprint->CummHours),
            'actual_burn_down' => $sprint_dates3->map(fn($sprint) => (int) $sprint->CummCapacity)
        ];

        $parameters = [
            'sprint' => $sprint,
            'goals_overview' => $goals_overview,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'went_well' => $sprint->sprintReviews?->filter(fn($review) => $review->review_type == 'went well')->values(),
            'went_wrong' => $sprint->sprintReviews?->filter(fn($review) => $review->review_type == 'went wrong')->values(),
            'needs_improvement' => $sprint->sprintReviews?->filter(fn($review) => $review->review_type == 'needs improvement')->values(),
            'chart1' => $chart1,
            'chart2' => $chart2,
            'chart3' => $chart3,
            'summaries' => $summaries,
            'summaries_hours' => $summaries_hours,
            'notice' => $sprint->tasks?->filter(fn($task) => !$task->goal_id)->values()->count()
                ?"There tasks assigned to this sprint that doesn't have a goal":null
        ];

        return view('project.sprint.show')->with($parameters);
    }

    public function edit(Sprint $sprint)
    {
        abort_if($sprint->status_id == 2, 403, 'You are not allowed to edit completed sprint');
        $sprint->createSprintResources();

        $sprint->load(['resources' => function($resource){
            $resource->select(['sprint_id','employee_id', 'function_id', 'capacity'])
                ->with([
                    'resource:id,first_name,last_name',
                    'function:id,description'
                ]);
        }]);

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $sprint->project_id) ?? [];

        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Sprint' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }

        $projectDropDown = Project::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = SprintStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $roles_drop_down = ModuleRole::orderBy('id')->where('module_id', 30)->get(['name', 'id']);

        $parameters = [
            'sprint' => $sprint,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'roles_drop_down' => $roles_drop_down
        ];

        return view('project.sprint.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Sprint  $sprint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sprint $sprint)
    {
        abort_if($sprint->status_id == 2, 403, 'You are not allowed to update completed sprint');
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $sprint->project_id) ?? [];

        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Sprint' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }

        $sprint->name = $request->input('name');
        $sprint->project_id = $request->input('project_id');
        $sprint->start_date = $request->input('start_date');
        $sprint->end_date = $request->input('end_date');
        $sprint->status_id = $request->input('status_id');
        $sprint->goal = $request->input('goal');
        $sprint->save();

        return redirect(route('sprint.index'))->with('flash_success', 'Sprint successfully updated.');
    }

    public function getParent($parent_name, $parent_id)
    {
        $parent = null;

        switch ($parent_name) {
            case 'project':
                $parent = Project::find($parent_id);
                break;
            case 'epic':
                $parent = Epic::find($parent_id);
                break;
            case 'feature':
                $parent = Feature::find($parent_id);
                break;
            case 'user_story':
                $parent = UserStory::find($parent_id);
                break;
        }

        return $parent;
    }

    public function minutesToTime($minutes)
    {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;
        $negation_number = '';
        if ($hours < 0) {
            $hours *= -1;
            $negation_number = '-';
        }

        if ($minutes < 0) {
            $minutes *= -1;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sprint $sprint)
    {
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $sprint->project_id) ?? [];

        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Sprint' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }

        Sprint::destroy($sprint->id);

        return redirect(route('sprint.index'))->with('flash_success', 'Sprint successfully deleted.');
    }

}
