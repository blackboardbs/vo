<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\AssignmentStandardCost;
use App\Models\BusinessFunction;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Customer;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\ResourceLevel;
use App\Models\ResourcePosition;
use App\Models\ResourceType;
use App\Models\Status;
use App\Models\Team;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Support\Collection;
use Illuminate\Http\JsonResponse;
class FiltersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function filters(): JsonResponse|array
    {
        if (request()->company){
            $company_dropdown = $this->company();
            session(['company_dropdown' => $company_dropdown]);
            return \response()->json($company_dropdown);
        }

        if (\request()->account){
            $account_dropdown = $this->account();
            session(['account_dropdown' => $account_dropdown]);
            return \response()->json();
        }

        if (\request()->account_element){
            $account_element_dropdown = $this->accountElement();
            session(['account_element_dropdown' => $account_element_dropdown]);
            return \response()->json($account_element_dropdown);
        }

        if (\request()->customer){
            $customer_dropdown = $this->customer();
            session(['customer_dropdown'=> $customer_dropdown]);
            return \response()->json($customer_dropdown);
        }

        if (\request()->resource){
            $resource_dropdown = $this->resources(request()->resource);
            session(['resource_dropdown' => $resource_dropdown]);
            return \response()->json($resource_dropdown);
        }

        if (\request()->project_status){
            session(['project_status_dropdown' => $this->projectStatus()]);
            return \response()->json($this->projectStatus());
        }

        if (\request()->project){
            session(['project_dropdown' => $this->project()]);
            return \response()->json($this->project());
        }

        if (\request()->function){
            session(['function_dropdown' => $this->businessFunction()]);
            return response()->json($this->businessFunction());
        }

        if (request()->resource_type){
            session(['resource_type_dropdown' => $this->resourceType()]);
            return response()->json($this->resourceType());
        }

        if (request()->position){
            session(['position_dropdown' => $this->position()]);
            return response()->json($this->position());
        }

        if (request()->level){
            session(['level_dropdown' => $this->level()]);
            return response()->json($this->level());
        }

        if (request()->standard_cost_bracket){
            session(['standard_cost_bracket_dropdown' => $this->standardCostBracket()]);
            return response()->json($this->standardCostBracket());
        }

        if (request()->team){
            session(['team_dropdown' => $this->team()]);
            return response()->json($this->team());
        }

        if (request()->status){
            session(['status_dropdown' => $this->status()]);
            return response()->json($this->status());
        }

        if (request()->cost_center){
            session(['cost_center_dropdown' => $this->costCenter()]);
            return response()->json($this->costCenter());
        }

        if (request()->report_to){
            session(['report_to_dropdown' => $this->resources(request()->report_to)]);
            return response()->json($this->resources(request()->report_to));
        }

        if (request()->vendor){
            session(['vendor_dropdown' => $this->vendor()]);
            return response()->json($this->vendor());
        }

        return [];
    }

    private function company() : Collection
    {
        return Company::select(['id', 'company_name'])
            ->where('company_name', 'like', '%'.\request()->company.'%')
            ->get()
            ->map(fn ($company) => ['name' => $company->company_name, 'id' => $company->id]);
    }

    private function account() : Collection
    {
        return Account::select(['id', 'description'])
            ->where('description', 'like', '%'.\request()->account.'%')
            ->get()
            ->map(fn ($account) => ['name' => $account->description, 'id' => $account->id]);
    }

    private function accountElement() : Collection
    {
        return AccountElement::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->account_element.'%')
            ->when(\request()->account_id, fn ($element) => $element->where('account_id', request()->account_id))
            ->get()
            ->map(fn($element) => ['name' => $element->description, 'id' => $element->id]);
    }

    private function customer(): Collection
    {
        return Customer::select(['id', 'customer_name'])
            ->where('customer_name', 'like', '%'.\request()->customer.'%')
            ->get()
            ->map(fn($customer) => ['id' => $customer->id, 'name' => $customer->customer_name]);
    }

    private function vendor(): Collection
    {
        return Vendor::select(['id', 'vendor_name'])
            ->where('vendor_name', 'like', '%'.\request()->vendor.'%')
            ->get()
            ->map(fn($vendor) => ['id' => $vendor->id, 'name' => $vendor->vendor_name]);
    }

    private function resources(string $field): Collection
    {
        return User::select(['id', 'first_name', 'last_name'])
            ->where(function ($user) use($field){
                $user->where('first_name', 'like', '%'.$field.'%')
                    ->orWhere('last_name', 'like', '%'.$field.'%');
            })->get()
            ->map(fn($user) => ['id' => $user->id, 'name' => $user->name()]);
    }

    private function projectStatus(): Collection
    {
        return ProjectStatus::select(['id', 'description'])
            ->where('description', 'LIKE', '%'.\request()->project_status.'%')
            ->get()
            ->map(fn($status) => ['id' => $status->id, 'name' => $status->description]);
    }

    private function project(): Collection
    {
        return Project::select(['id', 'name'])
            ->where('name', 'like', '%'.\request()->project.'%')
            ->when(\request()->customer_id, fn($project) => $project->where('customer_id', request()->customer_id))
            ->when(\request()->company_id, fn($project) => $project->where('company_id', request()->company_id))
            ->get();
    }

    private function businessFunction(): Collection
    {
        return BusinessFunction::select(['id', 'description'])
            ->where('description', 'like', '%'.\request()->function.'%')
            ->get()
            ->map(fn($function) => ['id' => $function->id, 'name' => $function->description]);
    }

    private function resourceType(): Collection
    {
        return ResourceType::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->resource_type.'%')
            ->get()
            ->map(fn($type) => ['id' => $type->id, 'name' => $type->description]);
    }

    private function position(): Collection
    {
        return ResourcePosition::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->position.'%')
            ->get()
            ->map(fn($resource) => ['id' => $resource->id, 'name' => $resource->description]);
    }

    private function level(): Collection
    {
        return ResourceLevel::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->level.'%')
            ->get()
            ->map(fn($level) => ['id' => $level->id, 'name' => $level->description]);
    }

    private function standardCostBracket(): Collection
    {
        return AssignmentStandardCost::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->standard_cost_bracket.'%')
            ->get()
            ->map(fn($cost) => ['id' => $cost->id, 'name' => $cost->description]);
    }

    public function team(): Collection
    {
        return Team::select(['id', 'team_name'])
            ->where('team_name', 'like', '%'.request()->team.'%')
            ->get()
            ->map(fn($team) => ['id' => $team->id, 'name' => $team->team_name]);
    }

    private function status(): Collection
    {
        return Status::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->status.'%')
            ->get()
            ->map(fn($status) => ['id' => $status->id, 'name' => $status->description]);
    }

    private function costCenter(): Collection
    {
        return CostCenter::select(['id', 'description'])
            ->where('description', 'like', '%'.request()->cost_center.'%')
            ->get()
            ->map(fn($cost) => ['id' => $cost->id, 'name' => $cost->description]);
    }
}
