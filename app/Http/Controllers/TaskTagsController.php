<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskTag;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Support\Collection;

class TaskTagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $tasktags = TaskTag::with(['statusd:id,description'])->sortable('name', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $tasktags = TaskTag::with(['statusd:id,description'])->sortable('name', 'asc')->where('status', $request->input('status_filter'));
            }else{
                $tasktags = TaskTag::with(['statusd:id,description'])->sortable('name', 'asc');
            }
        }

        // $tasktags = TaskTag::where('id', '>', 0);

        if ($request->has('q') && $request->input('q') != '') {
            $tasktags = $tasktags->where('name', 'like', '%'.$request->input('q').'%');
        }

        $tasktags = $tasktags->paginate($item);

        $parameters = [
            'tasktags' => $tasktags,
        ];

        return View('master_data.task_tags.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];
 
        return View('master_data.task_tags.create')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        // dd($request);
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'icon' => 'required|string|max:255',
            'description' => 'required|string',
            'color' => 'required|string'
        ]);

        $task_tags = new TaskTag();
        $task_tags->name = $validatedData['name'];
        $task_tags->description = $validatedData['description'] ?? null;
        $task_tags->icon = $validatedData['icon'];
        $task_tags->color = $validatedData['color'] ?? null;
        $task_tags->status = $request->input('status');
        $task_tags->save();

        return redirect(route('tasktags.index'))->with('flash_success', 'Master Data Task Tag captured successfully');
    }

    public function show($tag_id): View
    {
        $parameters = [
            'tasktag' => TaskTag::where('id', '=', $tag_id)->first()
        ];

        return View('master_data.task_tags.show')->with($parameters);
    }

    public function edit($tag_id): View
    {

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'tasktag' => TaskTag::where('id', '=', $tag_id)->first()
        ];

        return View('master_data.task_tags.edit')->with($parameters);
    }

    public function update(Request $request, $tag_id): RedirectResponse
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'icon' => 'required|string|max:255',
            'description' => 'required|string',
            'color' => 'required|string'
        ]);

        $task_tag = TaskTag::find($tag_id);
        $task_tag->name = $validatedData['name'];
        $task_tag->description = $validatedData['description'] ?? null;
        $task_tag->icon = $validatedData['icon'] != null ? $validatedData['icon'] : $task_tag->icon;
        $task_tag->color = $validatedData['color'] ?? null;
        $task_tag->status = $request->input('status');
        $task_tag->save();

        return redirect(route('tasktags.index'))->with('flash_success', 'Master Data Task Tag updated successfully');
    }

    public function destroy($tag_id): RedirectResponse
    {
        // TaskTag::destroy($tag_id);
        $task_tag = TaskTag::find($tag_id);
        $task_tag->status = 2;
        $task_tag->save();

        return redirect()->route('tasktags.index')->with('flash_success', 'Master Data Task Tag deleted successfully');
    }

    public function getTaskTags(Request $request): array
    {
        $tasktags = TaskTag::where('status', 1)->get();

        $noneOption = new TaskTag();
        $noneOption->id = 0;
        $noneOption->name = 'None';
        $noneOption->description = null;
        $noneOption->icon = '<i class="fa fa-times fa-lg"></i>';
        $noneOption->color = '#777';
        $noneOption->status = 1;
        $noneOption->created_at = null;
        $noneOption->updated_at = null;

        $tasktags = $tasktags->prepend($noneOption);
        // dd($tasktags);
        return $tasktags->toArray();
    }
}
