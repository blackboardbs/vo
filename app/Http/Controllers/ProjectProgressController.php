<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\BillingCycle;
use App\Models\BillingPeriod;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Customer;
use App\Models\Epic;
use App\Exports\ProjectExport;
use App\Models\Feature;
use App\Models\InvoiceStatus;
use App\Models\Module;
use App\Models\Project;
use App\Models\Task;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\TaskDeliveryType;
use App\Models\Timeline;
use App\Models\UserStory;
use App\Models\ProjectProgressReportView;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as Input;
use Maatwebsite\Excel\Facades\Excel;

class ProjectProgressController extends Controller
{
    public function projectProgress(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\TaskReport')->first();

        abort_if((! Auth::user()->canAccess($module->id, 'view_all') || ! Auth::user()->canAccess($module->id, 'view_team')), 403);

        ini_set('memory_limit', '-1');
        $project_prog = ProjectProgressReportView::query();

        if($request->has('company') && $request->input('company') != ''){
            $project_prog = $project_prog->where('company_id',$request->company);
        }
        if($request->has('customer') && $request->input('customer') != ''){
            $project_prog = $project_prog->where('customer_id',$request->customer);
        }
        if($request->has('project') && $request->input('project') != ''){
            $project_prog = $project_prog->where('project_id',$request->project);
        }
        if($request->has('resource') && $request->input('resource') != ''){
            $project_prog = $project_prog->where('employee_id',$request->resource);
        }
        if($request->has('billable') && $request->input('billable') != ''){
            $project_prog = $project_prog->where('is_billable',$request->billable);
        }
        if($request->has('epic') && $request->input('epic') != ''){
            $project_prog = $project_prog->where('epic_id',$request->epic);
        }
        if($request->has('feature') && $request->input('feature') != ''){
            $project_prog = $project_prog->where('feature_id',$request->feature);
        }
        if($request->has('user_story') && $request->input('user_story') != ''){
            $project_prog = $project_prog->where('user_story_id',$request->user_story);
        }
        if($request->has('task') && $request->input('task') != ''){
            $project_prog = $project_prog->where('task_id',$request->task);
        }
        if($request->has('invoice_status') && $request->input('invoice_status') != ''){
            $project_prog = $project_prog->where('invoice_status_id',$request->invoice_status);
        }
        if($request->has('from_week') && $request->input('from_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->from_week);
        }
        if($request->has('to_week') && $request->input('to_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->to_week);
        }
        if($request->has('from_month') && $request->input('from_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->from_month);
        }
        if($request->has('to_month') && $request->input('to_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->to_month);
        }
        if($request->has('cost_center') && $request->input('cost_center') != ''){
            $project_prog = $project_prog->where('cost_center_id',$request->cost_center);
        }
        if($request->has('task_delivery_type_id') && $request->input('task_delivery_type_id') != ''){
            $project_prog = $project_prog->where('task_delivery_type_id',$request->task_delivery_type_id);
        }

        $project_prog = $project_prog->orderByDesc('task_id')->get();
        // dd($project_prog);
        // dd($project_prog);
        $data = [];
        $task_variance = [];
        $total_planned = 0;
        $total = 0;
        foreach($project_prog as $proj){
            // if(isset($task_variance[$proj->task_id])){
            //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
            // }
            if(isset($data[$proj->task_id.'-'.$proj->is_billable])){
                $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] = $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] + $proj->total_hours??0;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                
            } else {
                $data[$proj->task_id.'-'.$proj->is_billable] = [
                    'task_id'=>$proj->task_id,
                    'task_description' => $proj->task,
                    'task_status' => $proj->task_status,
                'resource' => $proj->consultant_first_name.' '.$proj->consultant_last_name,
                'epic' => $proj->epic_id > 0 ? $proj->epic_name : '',
                'feature' => $proj->feature_id > 0 ? $proj->feature_name : '',
                'user_story' => $proj->user_story_id > 0 ? $proj->user_story_name : '',
                'billable' => $proj->is_billable,
                'planned_hours' => $proj->hours_planned,
                'total_hours' => $proj->total_hours,
                ];
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
            }
        }
        
        $data = $this->paginate2($data, request()->r??60);
        if (request()->has('export')) {
            $export['data'] = $data;
            return Excel::download(new ProjectExport(['export'=>$export], 'reports.exports.project-progress-report'), 'project-progress-report.xlsx');
        }

        return view('reports.project-progress')->with(['data'=>$data,'total_planned'=>$total_planned,'total'=>$total,
        'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
        'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
        'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
        'resource_drop_down2' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
        'task_drop_down' => $this->generateTaskDropdown($request),
        'user_stories_drop_down' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
        'feature_drop_down' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
        'epic_drop_down' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
        'invoice_status_drop_down' => InvoiceStatus::pluck('description', 'id'),
        'weeks_drop_down' => $this->generateYearWeek($request),
        'months_drop_down' => $this->generateYearMonth($request),
        'cost_center_drop_down' => CostCenter::filters()->orderBy('description')->pluck('description', 'id'),
        'actual_total' => 0/*$timeline->time($actual_total)*/,
        'planned_hours_total' => 0/*$timeline->time($total_planned)*/,
        'total_variance' => 0/*$timeline->time($total_planned - $actual_total)*/,
        'billing_cycle_drop_down' => BillingCycle::orderBy('name')->pluck('name', 'id'),
        'delivery_type' => TaskDeliveryType::filters()->orderBy('name')->pluck('name', 'id'),]);
    }

    public function projectProgressLevelTwo(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\TaskReport')->first();

        abort_if((! Auth::user()->canAccess($module->id, 'view_all') || ! Auth::user()->canAccess($module->id, 'view_team')), 403);

        ini_set('memory_limit', '-1');
        $project_prog = ProjectProgressReportView::query();

        if($request->has('company') && $request->input('company') != ''){
            $project_prog = $project_prog->where('company_id',$request->company);
        }
        if($request->has('customer') && $request->input('customer') != ''){
            $project_prog = $project_prog->where('customer_id',$request->customer);
        }
        if($request->has('project') && $request->input('project') != ''){
            $project_prog = $project_prog->where('project_id',$request->project);
        }
        if($request->has('resource') && $request->input('resource') != ''){
            $project_prog = $project_prog->where('employee_id',$request->resource);
        }
        if($request->has('billable') && $request->input('billable') != ''){
            $project_prog = $project_prog->where('is_billable',$request->billable);
        }
        if($request->has('epic') && $request->input('epic') != ''){
            $project_prog = $project_prog->where('epic_id',$request->epic);
        }
        if($request->has('feature') && $request->input('feature') != ''){
            $project_prog = $project_prog->where('feature_id',$request->feature);
        }
        if($request->has('user_story') && $request->input('user_story') != ''){
            $project_prog = $project_prog->where('user_story_id',$request->user_story);
        }
        if($request->has('task') && $request->input('task') != ''){
            $project_prog = $project_prog->where('task_id',$request->task);
        }
        if($request->has('invoice_status') && $request->input('invoice_status') != ''){
            $project_prog = $project_prog->where('invoice_status_id',$request->invoice_status);
        }
        if($request->has('from_week') && $request->input('from_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->from_week);
        }
        if($request->has('to_week') && $request->input('to_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->to_week);
        }
        if($request->has('from_month') && $request->input('from_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->from_month);
        }
        if($request->has('to_month') && $request->input('to_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->to_month);
        }
        if($request->has('cost_center') && $request->input('cost_center') != ''){
            $project_prog = $project_prog->where('cost_center_id',$request->cost_center);
        }
        if($request->has('task_delivery_type_id') && $request->input('task_delivery_type_id') != ''){
            $project_prog = $project_prog->where('task_delivery_type_id',$request->task_delivery_type_id);
        }

        $project_prog = $project_prog->orderByDesc('task_id')->get();
        // dd($project_prog);
        // dd($project_prog);
        $data = [];
        $task_variance = [];
        $total_planned = 0;
        $total = 0;
        foreach($project_prog as $proj){
            // if(isset($task_variance[$proj->task_id])){
            //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
            // }
            if(isset($data[$proj->task_id.'-'.$proj->is_billable])){
                $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] = $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] + $proj->total_hours??0;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                
            } else {
                $data[$proj->task_id.'-'.$proj->is_billable] = [
                    'task_id'=>$proj->task_id,
                'resource' => $proj->consultant_first_name.' '.$proj->consultant_last_name,
                'epic' => $proj->epic_id > 0 ? $proj->epic_name : '',
                'feature' => $proj->feature_id > 0 ? $proj->feature_name : '',
                'user_story' => $proj->user_story_id > 0 ? $proj->user_story_name : '',
                'billable' => $proj->is_billable,
                'planned_hours' => $proj->hours_planned,
                'total_hours' => $proj->total_hours,
                ];
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
            }
        }
        
        $data = $this->paginate2($data, request()->r??60);
        if (request()->has('export')) {
            $export['data'] = $data;
            return Excel::download(new ProjectExport(['export'=>$export], 'reports.exports.project-progress-report-2'), 'project-progress-report-2.xlsx');
        }

        return view('reports.project-progress-l2')->with(['data'=>$data,'total_planned'=>$total_planned,'total'=>$total,
        'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
        'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
        'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
        'resource_drop_down2' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
        'task_drop_down' => $this->generateTaskDropdown($request),
        'user_stories_drop_down' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
        'feature_drop_down' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
        'epic_drop_down' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
        'invoice_status_drop_down' => InvoiceStatus::pluck('description', 'id'),
        'weeks_drop_down' => $this->generateYearWeek($request),
        'months_drop_down' => $this->generateYearMonth($request),
        'cost_center_drop_down' => CostCenter::filters()->orderBy('description')->pluck('description', 'id'),
        'actual_total' => 0/*$timeline->time($actual_total)*/,
        'planned_hours_total' => 0/*$timeline->time($total_planned)*/,
        'total_variance' => 0/*$timeline->time($total_planned - $actual_total)*/,
        'billing_cycle_drop_down' => BillingCycle::orderBy('name')->pluck('name', 'id'),
        'delivery_type' => TaskDeliveryType::filters()->orderBy('name')->pluck('name', 'id'),]);
    }

    public function projectProgressLevelThree(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\TaskReport')->first();

        if (! Auth::user()->canAccess($module->id, 'view_all') || ! Auth::user()->canAccess($module->id, 'view_team')) {
            return abort(403);
        }
        ini_set('memory_limit', '-1');
        $project_prog = ProjectProgressReportView::query();

        if($request->has('company') && $request->input('company') != ''){
            $project_prog = $project_prog->where('company_id',$request->company);
        }
        if($request->has('customer') && $request->input('customer') != ''){
            $project_prog = $project_prog->where('customer_id',$request->customer);
        }
        if($request->has('project') && $request->input('project') != ''){
            $project_prog = $project_prog->where('project_id',$request->project);
        }
        if($request->has('resource') && $request->input('resource') != ''){
            $project_prog = $project_prog->where('employee_id',$request->resource);
        }
        if($request->has('billable') && $request->input('billable') != ''){
            $project_prog = $project_prog->where('is_billable',$request->billable);
        }
        if($request->has('epic') && $request->input('epic') != ''){
            $project_prog = $project_prog->where('epic_id',$request->epic);
        }
        if($request->has('feature') && $request->input('feature') != ''){
            $project_prog = $project_prog->where('feature_id',$request->feature);
        }
        if($request->has('user_story') && $request->input('user_story') != ''){
            $project_prog = $project_prog->where('user_story_id',$request->user_story);
        }
        if($request->has('task') && $request->input('task') != ''){
            $project_prog = $project_prog->where('task_id',$request->task);
        }
        if($request->has('invoice_status') && $request->input('invoice_status') != ''){
            $project_prog = $project_prog->where('invoice_status_id',$request->invoice_status);
        }
        if($request->has('from_week') && $request->input('from_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->from_week);
        }
        if($request->has('to_week') && $request->input('to_week') != ''){
            $project_prog = $project_prog->where('year_week','>=',$request->to_week);
        }
        if($request->has('from_month') && $request->input('from_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->from_month);
        }
        if($request->has('to_month') && $request->input('to_month') != ''){
            $project_prog = $project_prog->where('year_month','>=',$request->to_month);
        }
        if($request->has('cost_center') && $request->input('cost_center') != ''){
            $project_prog = $project_prog->where('cost_center_id',$request->cost_center);
        }
        if($request->has('task_delivery_type_id') && $request->input('task_delivery_type_id') != ''){
            $project_prog = $project_prog->where('task_delivery_type_id',$request->task_delivery_type_id);
        }

        $project_prog = $project_prog->orderByDesc('task_id')->get();
        // dd($project_prog);
        // dd($project_prog);
        $data = [];
        $task_variance = [];
        $total_planned = 0;
        $total = 0;
        foreach($project_prog as $proj){
            // if(isset($task_variance[$proj->task_id])){
            //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
            // }
            if(isset($data[$proj->task_id.'-'.$proj->is_billable])){
                $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] = $data[$proj->task_id.'-'.$proj->is_billable]['total_hours'] + $proj->total_hours??0;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                
            } else {
                $data[$proj->task_id.'-'.$proj->is_billable] = [
                    'task_id'=>$proj->task_id,
                'resource' => $proj->consultant_first_name.' '.$proj->consultant_last_name,
                'epic' => $proj->epic_id > 0 ? $proj->epic_name : '',
                'feature' => $proj->feature_id > 0 ? $proj->feature_name : '',
                'user_story' => $proj->user_story_id > 0 ? $proj->user_story_name : '',
                'billable' => $proj->is_billable,
                'planned_hours' => $proj->hours_planned,
                'total_hours' => $proj->total_hours,
                ];
                $total_planned += $proj->hours_planned;
                $total += $proj->total_hours;
                // if(isset($task_variance[$proj->task_id])){
                //     $task_variance[$proj->task_id] = $task_variance[$proj->task_id] - $proj->total_hours??0;
                // } else {
                    
                // $task_variance[$proj->task_id] = $proj->hours_planned - $proj->total_hours;
                // }
            }
        }
        
        $project_progress = $this->spreadObject(collect($data));
        $data = $this->paginate2($data, request()->r??60);
        if (request()->has('export')) {
            $export['data'] = $data;
            return Excel::download(new ProjectExport(['export'=>$export], 'reports.exports.project-progress-report-3'), 'project-progress-report-3.xlsx');
        }

        return view('reports.project-progress-l3')->with(['data'=>$data,'total_planned'=>$total_planned,'total'=>$total,
        'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
        'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
        'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
        'resource_drop_down2' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
        'task_drop_down' => $this->generateTaskDropdown($request),
        'user_stories_drop_down' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
        'feature_drop_down' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
        'epic_drop_down' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
        'invoice_status_drop_down' => InvoiceStatus::pluck('description', 'id'),
        'weeks_drop_down' => $this->generateYearWeek($request),
        'months_drop_down' => $this->generateYearMonth($request),
        'cost_center_drop_down' => CostCenter::filters()->orderBy('description')->pluck('description', 'id'),
        'actual_total' => 0/*$timeline->time($actual_total)*/,
        'planned_hours_total' => 0/*$timeline->time($total_planned)*/,
        'total_variance' => 0/*$timeline->time($total_planned - $actual_total)*/,
        'billing_cycle_drop_down' => BillingCycle::orderBy('name')->pluck('name', 'id'),
        'delivery_type' => TaskDeliveryType::filters()->orderBy('name')->pluck('name', 'id'),]);
    }

    public function paginate2($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, ['path' => url()->current()]);
    }

    public function projectDelivery(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\TaskReport')->first();

        abort_if((! Auth::user()->canAccess($module->id, 'view_all') || ! Auth::user()->canAccess($module->id, 'view_team')), 403);

        $project_prog = $this->projectProgressQ($request);

        if (request()->has('export')) {
            return Excel::download(new ProjectExport($project_prog, 'reports.exports.project-delivery-report'), 'project-delivery-report.xlsx');
        }

        return view('reports.project-delivery')->with($project_prog);
    }

    private function projectProgressQ(Request $request)
    {
        $items = \request()->r ?? 60;

        $tl = Timeline::with(['task' => function ($q) {
            $q->selectRaw('id, description, SUM(hours_planned * 60) AS planned_hours, user_story_id, status')
                ->with(['user_story' => function ($q) {
                    $q->select('id', 'name', 'feature_id')
                        ->with(['feature' => function ($q) {
                            $q->select('id', 'name', 'epic_id')
                                ->with(['epic' => function ($q) {
                                    $q->select('id', 'name');
                                }]);
                        }]);
                }, 'taskstatus' => function ($q) {
                    $q->select('id', 'description');
                }])->groupBy(['id', 'description', 'user_story_id', 'status']);
        },
            'timesheet' => function ($q) {
                $q->select('id', 'employee_id')->with(['user' => function ($q) {
                    $q->selectRaw('id, CONCAT(`first_name`," ", `last_name`) AS resource');
                }]);
            }, 'taskDeliveryType', ])
            ->selectRaw('task_id, SUM((total * 60) + total_m) AS total, is_billable, description_of_work, timesheet_id, task_delivery_type_id')
            ->whereHas('timesheet')
            ->when(request()->customer, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('customer_id', request()->customer);
                });
            })
            ->when(request()->project, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('project_id', request()->project);
                });
            })
            ->when(request()->company, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('company_id', request()->company);
                });
            })
            ->when(request()->resource, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('employee_id', request()->resource);
                });
            })
            ->when(request()->billable != null, function ($timeline) {
                $timeline->where('is_billable', request()->billable);
            })
            ->when(request()->task, function ($timeline) {
                $timeline->where('task_id', request()->task);
            })
            ->when(request()->user_story, function ($timeline) {
                $timeline->whereHas('task', function ($task) {
                    $task->where('user_story_id', request()->user_story);
                });
            })
            ->when(request()->feature, function ($timeline) {
                $timeline->whereHas('task.user_story', function ($q) {
                    $q->where('feature_id', request()->feature);
                });
            })
            ->when(request()->epic, function ($timeline) {
                $timeline->whereHas('task.user_story.feature', function ($q) {
                    $q->where('epic_id', request()->epic);
                });
            })
            ->when(request()->invoice_status, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('bill_status', request()->invoice_status);
                });
            })
            ->when(request()->from_week, function ($timeline) {
                $timeline->whereHas('timesheet', function ($timesheet) {
                    $timesheet->where('year_week', '>=', request()->from_week);
                });
            })
            ->when(request()->to_week, function ($timeline) {
                $timeline->whereHas('timesheet', function ($timesheet) {
                    $timesheet->where('year_week', '<=', request()->to_week);
                });
            })
            ->when(request()->from_month, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('month', '>=', substr((request()->from_month), 5))
                        ->where('year', substr(request()->from_month, 0, 4));
                });
            })
            ->when(request()->to_month, function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $q->where('month', '<=', substr((request()->to_month), 5))
                        ->where('year', substr(request()->to_month, 0, 4));
                });
            })
            ->when(request()->filled('billing_period_id'), function ($timeline) {
                $timeline->whereHas('timesheet', function ($q) {
                    $billing_period = BillingPeriod::find(\request()->billing_period_id);
                    if (isset($billing_period)) {
                        $q->where('first_day_of_month', '>=', $billing_period->start_date)
                            ->where('last_day_of_month', '<=', $billing_period->end_date);
                    }
                });
            })
            ->when(request()->cost_center, function ($timeline) {
                $timeline->whereHas('timesheet.project.assignment.resource_user', function ($q) {
                    $q->where('cost_center_id', request()->cost_center);
                });
            })
            ->when(request()->task_delivery_type_id, function ($timeline) {
                $timeline->where('task_delivery_type_id', request()->task_delivery_type_id);
            })
            ->groupBy(['task_id', 'is_billable', 'description_of_work', 'timesheet_id', 'task_delivery_type_id'])
            ->orderByDesc('task_id')
            ->get()->groupBy('task_id')->map(function ($timeline, $key) {
                if ($key == 0) {
                    return $timeline->map(function ($line) {
                        return [
                            'task_id' => $line->task_id,
                            'description' => null,
                            'resource' => $line->timesheet->user->resource ?? null,
                            'billable' => $line->is_billable,
                            'user_story' => 'No task',
                            'planned_hours' => _minutes_to_time(0),
                            'planned_hours_raw' => 0,
                            'actual_hours' => _minutes_to_time($line->total),
                            'actual_hours_raw' => $line->total,
                            'variance' => _minutes_to_time(0 - $line->total),
                            'variance_raw' => 0 - $line->total,
                            'delivery_type' => $line->taskDeliveryType->name ?? null,
                            'taskstatus' => null,
                        ];
                    });
                }

                return [
                    'task_id' => $timeline[0]->task_id,
                    'description' => $timeline[0]->task->description ?? 'No task',
                    'resource' => $timeline[0]->timesheet->user->resource ?? null,
                    'billable' => $timeline[0]->is_billable,
                    'user_story' => $timeline[0]->task->user_story ?? 'No user story',
                    'planned_hours' => _minutes_to_time($timeline[0]->task->planned_hours ?? 0),
                    'planned_hours_raw' => $timeline[0]->task->planned_hours ?? 0,
                    'actual_hours' => _minutes_to_time($timeline->sum('total')),
                    'actual_hours_raw' => $timeline->sum('total'),
                    'variance' => _minutes_to_time((($timeline[0]->task->planned_hours ?? 0) - $timeline->sum('total'))),
                    'variance_raw' => (($timeline[0]->task->planned_hours ?? 0) - $timeline->sum('total')),
                    'taskstatus' => $timeline[0]->task->taskstatus ?? null,
                    'delivery_type' => $timeline[0]->taskDeliveryType->name ?? null,
                ];
            })->filter()->values();

        $project_progress = $this->spreadObject($tl);

        return [
            'project_progress' => $this->pagination($project_progress, $items),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down2' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'task_drop_down' => $this->generateTaskDropdown($request),
            'user_stories_drop_down' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
            'feature_drop_down' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
            'epic_drop_down' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
            'invoice_status_drop_down' => InvoiceStatus::pluck('description', 'id'),
            'weeks_drop_down' => $this->generateYearWeek($request),
            'months_drop_down' => $this->generateYearMonth($request),
            'cost_center_drop_down' => CostCenter::filters()->orderBy('description')->pluck('description', 'id'),
            'actual_total' => 0/*$timeline->time($actual_total)*/,
            'planned_hours_total' => 0/*$timeline->time($total_planned)*/,
            'total_variance' => 0/*$timeline->time($total_planned - $actual_total)*/,
            'billing_cycle_drop_down' => BillingCycle::filters()->orderBy('name')->pluck('name', 'id'),
            'delivery_type' => TaskDeliveryType::filters()->orderBy('name')->pluck('name', 'id'),
        ];
    }

    private function spreadObject(object $timelines)
    {
        foreach ($timelines as $key => $line) {
            if (gettype($line) == 'object') {
                $temp_line = $timelines->pull($key);
                foreach ($temp_line as $tmp) {
                    $timelines->push($tmp);
                }
            }
        }

        return $timelines->values();
    }

    private function assignmentConsultants()
    {
        return Assignment::with(['consultant' => function ($query) {
            $query->selectRaw('id, CONCAT(`first_name`, " ", `last_name`) AS resource');
        }])->select('employee_id')->get()
            ->keyBy('employee_id')
            ->map(function ($assignment) {
                return $assignment->consultant->resource ?? '';
            });
    }

    public function generateYearMonth(Request $request)
    {
        $years = TimeSheet::filters()->selectRaw("DISTINCT(CONCAT(`year`,'-',CASE WHEN `month` < 10 THEN CONCAT('0',`month`) ELSE `month` END)) as yearmonth")->pluck('yearmonth');

        if(count($years) == 0){
            $years2 = TimeSheet::distinct('year')->orderBy('year', 'desc')->pluck('year');

            $year_month_drop_down = [];
            foreach ($years2 as $year) {
                for ($i = 12; $i >= 1; $i--) {
                    if (($year == date('Y')) && ($i > date('m'))) {
                        continue;
                    }
                    if ($i < 10) {
                        $i = '0' . $i;
                    }
                    $year_month_drop_down[$year . '-' . $i] = $year . $i;
                }
            }
        } else {
            $year_month_drop_down = [];
            if(isset($request->from_month)){ $year_month_drop_down[$request->from_month] = str_replace('-','',$request->from_month); }
            if(isset($request->to_month) && $request->to_month != $request->from_month){ $year_month_drop_down[$request->to_month] = str_replace('-','',$request->to_month); }
            foreach ($years as $key => $year) {
                if(str_replace('-','',$year) != $request->to_month && str_replace('-','',$year) != $request->from_month){
                    $year_month_drop_down[$year] = str_replace('-','',$year);
                }
            }
        }
        arsort($year_month_drop_down);
        return $year_month_drop_down;
    }

    public function generateYearWeek(Request $request)
    {
        $weeks = TimeSheet::filters()->orderBy('year_week', 'desc')->whereNotNull('year_week')->pluck('year_week', 'year_week');

        if(count($weeks) == 0){
            $year_week_drop_down = TimeSheet::distinct('year_week')->orderBy('year_week', 'desc')->pluck('year_week','year_week');
        } else {
            $year_week_drop_down = collect($weeks)->toArray();
            if(isset($request->from_week)){ $year_week_drop_down[$request->from_week] = $request->from_week; }
            if(isset($request->to_week) && $request->to_week != $request->from_week){ $year_week_drop_down[$request->to_week] = $request->to_week; }
        arsort($year_week_drop_down);
        }
        return $year_week_drop_down;
    }

    private function pagination(object $timelines, int $items = 15): LengthAwarePaginator
    {
        $page = Input::get('page', 1);
        $perPage = $items;

        return new LengthAwarePaginator(
            $timelines->forPage($page, $perPage), $timelines->count(), $perPage, $page, ['path' => url()->current()]
        );
    }

    public function generateTaskDropdown(Request $request){
        $list = Task::when(request()->employee,function($q){
            $q->where('employee_id', request()->employee);
        })->when(request()->resource,function($q){
            $q->where('employee_id', request()->resource);
        })->when(request()->project,function($q){
            $q->where('project_id','=', request()->project);
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
            $task->where('customer_id', request()->customer);
        })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
            $task->whereHas('project.company', function ($t) {
                $t->where('id', '=', request()->company);
            });
        })->orderBy('description')->pluck('description', 'id');

        return $list;
    }
}
