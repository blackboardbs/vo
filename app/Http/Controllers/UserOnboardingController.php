<?php

namespace App\Http\Controllers;

use App\Mail\ApprovedMail;
use App\Mail\EnableEditAppointingManagerMail;
use App\Mail\UserOnboardingConfirmMail;
use App\Models\AdvancedTemplates;
use App\Models\BankAccountType;
use App\Models\Config;
use App\Models\ContractType;
use App\Models\Country;
use App\Models\Relationship;
use App\Models\Status;
use App\Models\Title;
use App\Models\OnboardingStatus;
use App\Models\PaymentBase;
use App\PaymentType;
use App\Models\User;
use App\Models\UserOnboarding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class UserOnboardingController extends Controller
{
    public function index(Request $request)
    {
        $relationships = [...$this->userRelationship(), ...$this->statusRelationship()];

        if(auth()->user()->hasAnyRole(['user_onboarding'])){
            $onboardings = UserOnboarding::with($relationships)
            ->select(['id', 'user_id', 'status_id', 'created_at', 'updated_at','commited_at'])
            ->where('status_id','<>', 10)
            ->where('user_id', auth()->user()->id)
            ->filter()
            ->paginate();
        } else {
        $onboardings = UserOnboarding::with($relationships)
            ->select(['id', 'user_id', 'status_id', 'created_at', 'updated_at','commited_at'])
            ->where('status_id','<>', 10)
            ->filter()
            ->paginate();
        }

        $approver = Config::with(['approvalManager' => function($approver){
            $approver->select(['id', 'first_name']);
        }])->select(['hr_approval_manager_id'])->first()->approvalManager->first_name??null;

        $appointment_manager_dropdown = User::selectRaw('DISTINCT appointment_manager_id')->with(['appointmentManager' => function($manager){
            $manager->selectRaw('id, CONCAT(first_name, " ", last_name) AS full_name');
        }])->whereHas('appointmentManager')->get()->map(function($user){
            return [
                'id' => $user->appointment_manager_id,
                'full_name' => $user->appointmentManager->full_name
            ];
        })->pluck('full_name', 'id')->toArray();

        $onboarding_status_dropdown = OnboardingStatus::where('status_id', 1)->pluck('description', 'id');

        $data = [
            'onboardings' => $onboardings,
            'approver' => $approver,
            'appointment_manager' => $appointment_manager_dropdown,
            'appointment_status' => $onboarding_status_dropdown,
            'hr_id' => Config::first(['hr_user_id'])->hr_user_id??0,
            'approval_manager_id' => Config::first(['hr_approval_manager_id'])->hr_approval_manager_id??0
        ];

        if ($request->has('export')) return $this->export($data, 'useronboarding');

        return view('useronboarding.index')->with($data);

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(UserOnboarding $useronboarding)
    {

        $parameteres = array_merge(
            [
                'contract_type_dropdown' => ContractType::where('status_id', 1)->pluck('description', 'id'),
                'access_appointing_manager' => auth()->user()->id == ($useronboarding->user->appointment_manager_id??0),
                'payment_base_dropdown' => PaymentBase::where('status_id', 1)->pluck('description', 'id'),
                'payment_type_dropdown' => PaymentType::where('status_id', 1)->pluck('description', 'id'),
                'reporting_manager_dropdown' => User::selectRaw('id, CONCAT(first_name, " ", last_name) AS full_name')->where('expiry_date', '>', now()->toDateString())->where('status_id', 1)->pluck('full_name', 'id'),
                'permanent_term_dropdown' => AdvancedTemplates::where('template_type_id', 10)->pluck('name', 'id'),
                'fixed_term_dropdown' => AdvancedTemplates::where('template_type_id', 9)->pluck('name', 'id'),
            ], $this->editData($useronboarding));

        return view('useronboarding.show')->with($parameteres);
    }

    public function edit(UserOnboarding $useronboarding)
    {
        //$this->authorize('committed', $useronboarding);

        if (isset($useronboarding->commited_at)){
            return redirect()->route('useronboarding.show', $useronboarding->id)->with('flash_info', 'You cannot edit your information after you commit');
        }

        return view('useronboarding.edit')->with($this->editData($useronboarding));
    }

    public function update(Request $request, UserOnboarding $useronboarding)
    {

        $useronboarding->title_id = $request->title_id;
        $useronboarding->Known_as = $request->Known_as;
        $useronboarding->id_number = $request->id_number;
        $useronboarding->date_of_birth = $request->date_of_birth;
        $useronboarding->passport_number = $request->passport_number;
        $useronboarding->country_id = $request->country_id;
        $useronboarding->directive_number = implode(', ', $request->directive_number);
        $useronboarding->income_tax_number = $request->income_tax_number;
        $useronboarding->id_passport = $this->document('id_passport', $useronboarding);
        $useronboarding->proof_of_address = $this->document('proof_of_address', $useronboarding);
        $useronboarding->popia_accept = $request->popia_accept;
        if ($request->confirm){
            $useronboarding->commited_at = now()->toDateTimeString();
            $useronboarding->status_id = 2;
        }
        $useronboarding->save();

        $useronboarding_id = ['user_onboarding_id' => $useronboarding->id];

        $useronboarding->contactDetails()->updateOrCreate($useronboarding_id,[
            'physical_address' => $request->physical_address,
            'postal_address' => $request->postal_address,
            'email' => $request->email,
            'home_number' => $request->home_number,
            'work_number' => $request->work_number,
            'cell_number' => $request->cell_number,
            'next_of_kin_name' => $request->next_of_kin_name,
            'next_of_kin_number' => $request->next_of_kin_number
        ]);

        $useronboarding->paymentDetails()->updateOrCreate($useronboarding_id,[
            'bank_account_type_id' => $request->bank_account_type_id,
            'account_name' => $request->account_name,
            'relationship_id' => $request->relationship_id,
            'bank_name' => $request->bank_name,
            'branch_number' => $request->branch_number,
            'branch_name' => $request->branch_name,
            'account_number' => $request->account_number,
            'proof_of_bank_account' => $this->document('proof_of_bank_account', $useronboarding->paymentDetails)
        ]);

        if ($request->confirm){
            $config = Config::with(['hrUser' => function($hr){
                $hr->select(['id', 'first_name', 'last_name', 'email', 'phone']);
            }, 'company' => function($company){
                $company->select(['id', 'company_name']);
            }])->select(['hr_user_id', 'company_id'])->first();

            if (isset($useronboarding->user->appointmentManager)){
                $emails = array_filter(
                    array_values(
                        array_unique([($config->hrUser->email??null), ($useronboarding->user->appointmentManager->email??null)])
                    )
                );
                try {
                    $company = ($this->configData()->company->company_name??(Company::first(['company_name'])->company_name??'No company'));
                    Mail::to($emails)->send(new UserOnboardingConfirmMail($useronboarding, $config->hrUser, $company));
                }catch (\Exception $exception){
                    logger($exception);
                }
            }
        }

        return redirect()->route('useronboarding.show', $useronboarding->id)->with('flash_success', 'Useronboarding updated successfully');
    }

    public function destroy(UserOnboarding $userOnboarding)
    {
        //
    }

    public function enable(Request $request, UserOnboarding $useronboarding)
    {
        $config = Config::with(['hrUser' => function($hr){
            $hr->select(['id', 'first_name', 'last_name', 'email', 'phone']);
        }, 'company' => function($company){
            $company->select(['id', 'company_name']);
        }])->select(['hr_user_id', 'company_id'])->first();

        $useronboarding->commited_at = null;
        $useronboarding->popia_accept = null;
        $useronboarding->status_id = 1;
        $useronboarding->save();

        try {
            $appointing_manager = $useronboarding->user->appointmentManager??new User();
            Mail::to([($useronboarding->user->email??null), $appointing_manager->email])->send(new EnableEditAppointingManagerMail($useronboarding->user, $appointing_manager, $useronboarding));
        }catch (\Exception $exception){
            logger($exception->getMessage());
        }

        /*if (isset($useronboarding->user->appointmentManager)){
            $emails = array_filter(
                array_values(
                    array_unique([($config->hrUser->email??null), ($useronboarding->user->appointmentManager->email??null)])
                )
            );
            try {
                $company = ($this->configData()->company->company_name??(Company::first(['company_name'])->company_name??'No company'));
                Mail::to($emails)->send(new UserOnboardingConfirmMail($useronboarding, $config->hrUser, $company));
            }catch (\Exception $exception){
                logger($exception);
            }
        }*/

        return redirect()->route('useronboarding.edit',$useronboarding)->with('flash_success', 'User onboarding edit form is enabled');
    }

    public function approve(UserOnboarding $useronboarding)
    {
        $useronboarding->status_id = 4;
        $useronboarding->save();

        if (isset($useronboarding->appointingManager)){
            $useronboarding->appointingManager->update(
                ['approved_at' => now()->toDateTimeString()]
            );

            $mail_data = (object)[
                'user_name' => isset($useronboarding->user)?$useronboarding->user->name():null,
                'appointing_manager_user' => $useronboarding->user->appointmentManager?$useronboarding->user->appointmentManager:null,
                'contract_details' => $useronboarding->appointingManager,
                'hr' => $this->configData()->hrUser??null,
                'company' => ($this->configData()->company->company_name??(Company::first(['company_name'])->company_name??'No company'))
            ];

            try {
                $emails = array_filter(array_values(array_unique([($this->configData()->hrUser->email??null), ($useronboarding->user->appointmentManager->email??null)])));
                Mail::to($emails)->send(new ApprovedMail($mail_data));
            }catch (\Exception $exception){
                logger($exception->getMessage());
            }
        }

        return redirect()->route('useronboarding.index')->with('flash_success', 'Contract approved successfully');
    }

    public function issueContract(UserOnboarding $useronboarding)
    {
        $useronboarding->status_id = 5;
        $useronboarding->save();

        if (isset($useronboarding->appointingManager)){
            $useronboarding->appointingManager->update(
                ['issued_at' => now()->toDateTimeString()]
            );
        }

        return redirect()->route('useronboarding.index')->with('flash_success', 'Contract issued successfully');
    }

    public function contractReceived(UserOnboarding $useronboarding)
    {
        $useronboarding->status_id = 6;
        $useronboarding->save();

        if (isset($useronboarding->appointingManager)){
            $useronboarding->appointingManager->update(
                ['recieved_at' => now()->toDateTimeString()]
            );
        }

        return redirect()->route('useronboarding.index')->with('flash_success', 'Contract Received successfully');
    }

    public function contractSigned(UserOnboarding $useronboarding)
    {
        $useronboarding->status_id = 7;
        $useronboarding->save();

        if (isset($useronboarding->appointingManager)){
            $useronboarding->appointingManager->update(
                ['signed_at' => now()->toDateTimeString()]
            );
        }

        return redirect()->route('useronboarding.index')->with('flash_success', 'Contract Signed successfully');
    }

    public function contractFiled(UserOnboarding $useronboarding)
    {
        $useronboarding->status_id = 8;
        $useronboarding->save();

        if (isset($useronboarding->appointingManager)){
            $useronboarding->appointingManager->update(
                ['filed_at' => now()->toDateTimeString()]
            );
        }

        return redirect()->route('useronboarding.index')->with('flash_success', 'Contract Filed successfully');
    }

    private function userRelationship()
    {
        return [
            'user' => function($user){
                $user->select(
                    [
                        'id',
                        'first_name',
                        'last_name',
                        'phone',
                        'email',
                        'appointment_manager_id',
                        'usertype_id',
                        'vendor_id'
                    ])->with([
                    'appointmentManager' => function($appointer){
                        $appointer->select(['id', 'first_name']);
                    }, 'userType' => function($type){
                        $type->select(['id', 'description']);
                    }, 'vendor' => function($vendor){
                        $vendor->select(['id', 'vendor_name']);
                    }]);
            },'appointingManager' => function($appointing){
                $appointing->select(
                    [
                        'id',
                        'user_onboarding_id',
                        'user_onboarding_id',
                        'contract_type_id',
                        'payment_type_id',
                        'payment_base_id',
                        'start_date',
                        'end_date',
                        'position',
                        'annual_salary',
                        'leave_days',
                        'notice_period',
                        'reporting_manager',
                        'other_income',
                        'other_conditions',
                        'require_phone',
                        'require_mobile_internet',
                        'responsibilities',
                        'approved_at',
                        'issued_at',
                        'recieved_at',
                        'signed_at',
                        'filed_at',
                        'notes',
                        'created_at',
                        'permenant_contract_template_id',
                        'fixed_contract_template_id'
                    ])
                    ->with(['contractType' => function($contract){
                        $contract->select(['id', 'description']);
                    }, 'paymentType' => function($type){
                        $type->select(['id', 'description']);
                    }, 'paymentBase' => function($payment){
                        $payment->select(['id', 'description']);
                    }]);
            }
        ];
    }

    private function statusRelationship()
    {
        return [
            'status' => function($status){
                $status->select(['id', 'description']);
            }
        ];
    }

    private function document($fieldName, $useronboarding)
    {
        if (request()->hasFile($fieldName)){
            return request()->file($fieldName)->store('useronboarding');
        }elseif(isset($useronboarding->$fieldName)){
            return $useronboarding->$fieldName;
        }
        return null;
    }

    private function editData($useronboarding)
    {
        return [
            'onboarding' => $useronboarding->load($this->userRelationship()),
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            'title_dropdown' => Title::where('status_id', 1)->pluck('description', 'id'),
            'country_dropdown' => Country::where('status', 1)->pluck('name', 'id'),
            'account_type_dropdown' => BankAccountType::where('status_id', 1)->pluck('description', 'id'),
            'relationship_dropdown' => Relationship::where('status', 1)->pluck('description', 'id'),
        ];
    }

    private function configData($config = null)
    {
        $user_data = ['id', 'first_name', 'last_name', 'email'];

        return Config::with(['company' => function($company){
            $company->select(['id', 'company_name']);
        }, 'approvalManager' => function($manager) use($user_data){
            $manager->select($user_data);
        }, 'hrUser' => function($hr) use($user_data){
            $hr->select($user_data);
        }])->select(['company_id', 'hr_user_id', 'hr_approval_manager_id'])->first();
    }

}
