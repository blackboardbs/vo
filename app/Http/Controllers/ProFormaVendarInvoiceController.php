<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Enum\YesOrNoEnum;
use App\Models\Assignment;
use App\Models\Config;
use App\Http\Requests\StoreVendorInvoiceRequest;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\VatRate;
use App\Services\ProFormaInvoiceService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProFormaVendarInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(): View
    {
        $consultants = Timesheet::with(['employee:id,first_name,last_name'])
            ->whereHas('employee.vendor')
                ->select('employee_id')->get()->map(fn ($employee) => [
                    'id' => $employee->employee_id,
                    'full_name' => $employee->employee?->name(),
            ])->pluck('full_name', 'id');

        $parameters = [
            'consultant_dropdown' => $consultants,
            'invoice_type' => 'Pro-forma Invoice',

        ];
        //return $parameters;
        return view('vendor.pro-forma_vendar_invoice.index')->with($parameters);
    }

    public function calculate(StoreVendorInvoiceRequest $request, ProFormaInvoiceService $service)
    {
        $period = $request->input('period');
        $year = substr($period, 0, 4);
        $month = substr($period, 4);

        $timesheets = Timesheet::with([
            'employee' => fn($employee) => $employee->select(['id', 'first_name', 'last_name', 'vendor_id'])
                ->with(['vendor' => fn($vendor) => $vendor->select([
                    'id',
                    'vendor_name',
                    'postal_address_line1',
                    'postal_address_line2',
                    'postal_address_line3',
                    'city_suburb',
                    'state_province',
                    'postal_zipcode',
                    'contact_firstname',
                    'contact_lastname',
                    'vat_no',
                    'bank_name',
                    'branch_code',
                    'bank_acc_no',
                    ])
                ]),
            'time_exp' => fn($expenses) => $expenses->select(['timesheet_id', 'amount', 'description', 'date'])
                ->where('is_billable', YesOrNoEnum::Yes->value),
            'company' => fn($company) => $company->select([
                'id',
                'company_name',
                'company_logo',
                'postal_address_line1',
                'postal_address_line2',
                'postal_address_line3',
                'state_province',
                'postal_zipcode',
                'phone',
                'email',
                'vat_no'
            ]),
            'project:id,name,ref,end_date',
            'timeline' => fn($timesheets) => $timesheets->select(['timesheet_id', 'total', 'total_m', 'task_id', 'description_of_work'])
                ->with(['task:id,description'])->where('vend_is_billable', YesOrNoEnum::Yes->value),
        ])->leftJoin('assignment', function ($join){
            $join->on('assignment.employee_id', '=', 'timesheet.employee_id')
                ->on('assignment.project_id', '=', 'timesheet.project_id');
        })->select(['timesheet.id', 'timesheet.company_id', 'timesheet.employee_id', 'timesheet.project_id', 'assignment.external_cost_rate', 'assignment.currency'])
            ->where('timesheet.employee_id', $request->consultant)
            ->where('timesheet.project_id', '=', $request->project)
            ->whereHas('employee.vendor')
            ->when($year, fn($query) => $query->where("year", $year)->where("month", $month))
            ->unless($year, fn($query) => $query->where('first_day_of_week', '>=', $request->date_from)->where('last_day_of_week', '<=', $request->date_to))
            ->groupBy(['timesheet.id', 'timesheet.company_id', 'timesheet.employee_id', 'timesheet.project_id', 'assignment.external_cost_rate', 'assignment.currency'])
            ->get();

        if ($timesheets->isEmpty()) {
            return \redirect()->back()->with('flash_danger', 'There is no timesheets for the selected period.');
        }

        $lineItems = $service->lineItemsGrouped($timesheets, 'external_cost_rate');

        $expenses =  $timesheets->map(fn($time) => $time->time_exp)->flatten();

        $customer = $timesheets->first()?->employee?->vendor;
        $company = $timesheets->first()?->company;
        $project = $timesheets->first()?->project;

        $total_exclusive = $lineItems->sum(fn($item) => $item->sum('exc_price')) + $expenses->sum('amount');
        $total_inclusive = $lineItems->sum(fn($item) => $item->sum('inc_price')) + $expenses->sum('amount');
        $total_vat = $lineItems->sum(fn($item) => $item->sum('exc_price'))*$service->vatRate();

        $parameters = [
            'expenses' => $expenses,
            'lineItems' => $lineItems,
            'timesheet' => $timesheets,
            'invoice_no' => $request->input('invoice_number'),
            'company' => $company,
            'vendor' => $customer,
            'project' => $project,
            'period' => $request->period,
            'date_range' => $request->date_from.' - '.$request->date_to,
            'invoice_type' => 'View Pro-forma Invoice',
            'invoice_message' => $request->invoice_message,
            'total_exclusive' => $total_exclusive,
            'total_inclusive' => $total_inclusive,
            'total_vat' => $total_vat
        ];

        return view('vendor.pro-forma_vendar_invoice.show')->with($parameters);
    }

    private function vatRate(): float|int
    {
        $vatRate = VatRate::query();

        if (request()->has('vat_rate_id')) {
            $vatRate = $vatRate->where('id', request()->vat_rate_id)->first();
        }else{
            $config = Config::first(['vat_rate_id']);
            $vatRate = $vatRate->select('vat_rate')
                ->where('status', Status::ACTIVE->value)
                ->where('vat_code', $config->vat_rate_id)->where('start_date', '<=', now()->toDateString())->where('end_date', '>=', now()->toDateString())
                ->latest()
                ->first();
        }

        return $vatRate->vat_rate/100;
    }
}
