<?php

namespace App\Http\Controllers;

use App\API\Clockify\ClockifyAPI;
use App\Models\Customer;
use App\Jobs\SendTaskMailJob;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Assignment;
use App\Models\Sprint;
use App\Models\Goal;
use App\Models\StorePermission;
use App\Models\Task;
use App\Models\TaskDeliveryType;
use App\Models\TaskStatus;
use App\Models\UserStory;
use App\Models\TaskType;
use App\Models\TaskNote;
use App\Models\Team;
use App\Models\BugTask;
use App\Models\Timeline;
use App\Models\User;
use App\Models\Config;
use App\Models\Company;
use App\Models\BillingPeriod;
use App\Models\UserNotification;
use App\Timesheet\TimesheetHelperTrait;
use App\Utils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class KanbanBoardController extends Controller
{
    use TimesheetHelperTrait;

    public function kanban(Request $request, $projectid, StorePermission $storePermission)
    {
        $project = Project::find($projectid)
        ??Project::orderBy('id','desc')->whereIn('status_id',[1,2,3])->first();
        //get last project if no project id supplied

        if(!$project){
            return redirect()->route('project.create')->with(['flash_projecterror'=>'Please create a Project and Assignments to utilize the Kanban functionality.']);
        }
       
        $config = Config::first();

        $parameters = [
            'project' => $project,
            'project_id' => $request->project_id ?? $project->id,
            'permissions' => $storePermission->getPermissions(30, $project->id),
            'display_on_kanban' => Task::KANBAN_DISPLAY,
            'show_timeframe' => $config->kanban_show_timeframe,
            'show_hours' => $config->kanban_show_hours,
            'show_actuals' => $config->kanban_show_actuals,
            'view' => Config::first(['kanban_view'])
        ];

        return view('task.kanbanV2')->with($parameters);
    }

    public function getKanban(Request $request, $projectid){
        $project = Project::find($projectid)
        ??Project::orderBy('id','desc')->whereIn('status_id',[1,2,3])->first();

        $config = Config::first(['kanban_show_backlog','kanban_show_planned','kanban_show_in_progress','kanban_show_done','kanban_show_impediment','kanban_show_completed']);
            
            // Initialize the query for task statuses
            $query = TaskStatus::select(['id', 'description']);

            // Apply filters based on the configuration
            $descriptions = [];

            if ($config->kanban_show_backlog == 1) {
                $descriptions[] = 'Backlog';
            }
            
            if ($config->kanban_show_planned == 1) {
                $descriptions[] = 'Planned';
            }
            
            if ($config->kanban_show_in_progress == 1) {
                $descriptions[] = 'In Progress';
            }

            if ($config->kanban_show_done == 1) {
                $descriptions[] = 'Done';
            }

            if ($config->kanban_show_impediment == 1) {
                $descriptions[] = 'Impediment';
            }

            if ($config->kanban_show_completed == 1) {
                $descriptions[] = 'Complete';
            }

            if (count($descriptions) > 0) {
            $query->whereIn('description', $descriptions);
            }

            // Execute the query and get the results
            $task_status = $query->orderBy('order')->get();

            $view = $request->input('view');

            if($view == 'user_story'){
                $user_stories = UserStory::with(['feature','feature.epic'])->filters()->get();

                $kanban = $user_stories->map(function ($user_story) use ($task_status, $project, $request){
                    return [
                    'id' => $user_story->id,
                    'project_id' => $user_story->feature->epic->project_id,
                    'name' => $user_story->name,
                    'feature_id' => $user_story->feature_id,
                    'details' => $user_story->details,
                    'start_date' => $user_story->start_date,
                    'end_date' => $user_story->end_date,
                    'hours_planned' => $user_story->hours_planned,
                    'billable' => $user_story->billable,
                    'dependency_id' => $user_story->dependency_id,
                    'sprint_id' => $user_story->sprint_id,
                    'status_id' => $user_story->status_id,
                    'tasks' => $task_status->map(function ($status) use ($user_story,$project, $request) {
                                    return $this->kanbanData($project->id, $request, $status, 'user_story', $user_story->id);
                                }),
                    ];
                });
        
                $times = [];
        
                foreach($kanban as $kan){
                    foreach($kan['tasks'] as $k){
                        if(count($k['times'])){
                            foreach($k['times'] as $v=>$t){
                                $times[$v] = $t->hours;
                            }
                        }
                    }
                }
            } elseif($view == 'consultant'){
                $assignments = Assignment::whereIn('assignment_status',[1,2,3])->get(['employee_id']);
                $user_stories = User::whereIn('users.id', $assignments)->filters()->when(isset(request()->resource) && request()->resource != '-1' && request()->resource != '0' && request()->resource != '', function ($user) {
                    $user->where('id', request()->resource);
                })->orderBy('users.first_name')->orderBy('users.last_name')->get();

                $kanban = $user_stories->map(function ($user_story) use ($task_status, $project, $request){
                    return [
                    'id' => $user_story->id,
                    'first_name' => $user_story->first_name,
                    'last_name' => $user_story->last_name,
                    'tasks' => $task_status->map(function ($status) use ($user_story,$project, $request) {
                                    return $this->kanbanData($project->id, $request, $status, 'consultant', $user_story->id);
                                }),
                    ];
                });
        
                $times = [];
        
                foreach($kanban as $kan){
                    foreach($kan['tasks'] as $k){
                        if(count($k['times'])){
                            foreach($k['times'] as $v=>$t){
                                $times[$v] = $t->hours;
                            }
                        }
                    }
                }
            } elseif($view == 'priority') {

                $priorities = [5,4,3,2,1,0];

                $kanban = array_map(function ($priority) use ($task_status, $project, $request){
                    return [
                    'id' => $priority,
                    'name' => 'Priority '.$priority,
                    'tasks' => $task_status->map(function ($status) use ($priority,$project, $request) {
                                    return $this->kanbanData($project->id, $request, $status, 'priority', $priority);
                        }),
                    ];
                },$priorities);

                $times = [];
        
                foreach($kanban as $kan){
                    foreach($kan['tasks'] as $k){
                        if(count($k['times'])){
                            foreach($k['times'] as $v=>$t){
                                $times[$v] = $t->hours;
                            }
                        }
                    }
                }
            } else {

                $kanban = $task_status->map(function ($status) use ($project, $request) {
                        return $this->kanbanData($project->id, $request, $status);
                    });
        
                $times = [];
        
                foreach($kanban as $k){
                    if(count($k['times'])){
                        foreach($k['times'] as $v=>$t){
                            $times[$v] = $t->hours;
                        }
                    }
                }
            }

            return response()->json(['kanban'=>$kanban,'times'=>$times??[]]);
    }

    public function loadmore(Request $request, $project_id, TaskStatus $status): JsonResponse
    {
        return response()->json(['kanban' => $this->kanbanData($project_id, $request, $status)]);
    }

    public function loadmoreUserStory(Request $request, $project_id,$user_story_id, TaskStatus $status): JsonResponse
    {
        return response()->json(['kanban' => $this->kanbanData($project_id, $request, $status,'user_story',$user_story_id)]);
    }

    public function loadmoreConsultant(Request $request, $project_id,$consultant_id, TaskStatus $status): JsonResponse
    {
        return response()->json(['kanban' => $this->kanbanData($project_id, $request, $status,'consultant',$consultant_id)]);
    }

    public function loadmorePriority(Request $request, $project_id,$priority_id, TaskStatus $status): JsonResponse
    {
        return response()->json(['kanban' => $this->kanbanData($project_id, $request, $status,'priority',$priority_id)]);
    }

    private function getWeekDays($billing_cycle, $yearweek)
    {
        $year_week_arr = explode('_', $yearweek);

        $year = substr($year_week_arr[0], 0, 4);
        $week = substr($year_week_arr[0], -2);

        $year = (int) $year;
        $week = (int) $week;

        $date = now()->setISODate($year, $week);

        if (count($year_week_arr) == 2) {
            if ((int)$year_week_arr[1] === 1) {
                // \Illuminate\Support\Facades\Log::info($year_week_arr[0]);
                $billing_period = $this->getBillingPeriod($billing_cycle, $date, 1);
            }
            if ((int) $year_week_arr[1] === 2) {
                $billing_period = $this->getBillingPeriod($billing_cycle, $date, 2);
            }
        } else {
            $billing_period = $this->getBillingPeriod($billing_cycle, $date, 0);
        }

        // Generate the week days
        $week_days = [];
        $week_start = $date->copy()->startOfWeek();
        $week_end = $date->copy()->endOfWeek();

        for ($week_start; $week_start <= $week_end; $week_start->addDay()) {
            array_push($week_days, $week_start->format('D d'));
        }

        // Handle billing period adjustments
        $first_date_of_week = $date->copy()->startOfWeek();
        $last_date_of_week = $date->copy()->endOfWeek();

        if (count($year_week_arr) == 2) {
            if ((int)$year_week_arr[1] === 1) {
                $last_date_of_week = Carbon::parse($billing_period->end_date);
            }

            if ((int)$year_week_arr[1] === 2) {
                $first_date_of_week = Carbon::parse($billing_period->start_date);
            }
        }

        // Generate timesheet week days
        $timesheet_week_days = [];
        for ($first_date_of_week; $first_date_of_week <= $last_date_of_week; $first_date_of_week->addDay()) {
            array_push($timesheet_week_days, $first_date_of_week->format('D d'));
        }

        return ['timesheet_week' => $timesheet_week_days, 'week_days' => $week_days];
    }


    public function getDropDowns(Request $request, $project_id)
    {
        $config = Config::first();

        $projid = $request->project_id ?? $project_id;
        
        $year_week_drop_down = [];

        $week_days = [];

        $is_billable = 1;
        if($projid > 0){
            $project = Project::find($projid);
            $project = $project->load(['epics','epics.features','epics.features.user_stories']);


            $billing_cycle = $this->billingCycle($project, $config);

            $weeks = $this->weeks($config,$billing_cycle ?? 0);

            $is_billable = $project->billable;

            foreach($weeks as $w){
                $we = $this->getWeekDays($billing_cycle,$w["year_week"]);
                array_push($year_week_drop_down,[
                    'year_week' => $w["year_week"],
                    'date' => $w["date"],
                    'days' => $we['timesheet_week'],
                    'week' => $we['week_days']
                ]);
            }

            $user_story_dropdown = $project->epics->map(function ($epic) {
                return $epic->features->map(function ($feature) {
                    return $feature->user_stories->map(function ($story) {
                        return [
                            'id' => $story->id,
                            'name' => $story->name,
                        ];
                    });
                });
            });
            // return '';
            $assignments = Assignment::where('project_id',$project->id)->get(['employee_id']);
            $users = User::selectRaw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS 'full_name', users.id AS 'id',case when (select count(assignment.id) from assignment where assignment.employee_id = users.id and assignment.assignment_status in (1,2,3)) > 0 then 1 else 0 end as 'assignment_cnt'")->whereIn('users.id', $assignments)->filters()->orderBy('users.first_name')->orderBy('users.last_name')->get();
            $sprints = Sprint::where('project_id', $project->id)->orderBy('name')->get(['name', 'id','status_id']);
            $tasks = Task::where('project_id', $project->id)->orderBy('description')->get(['description', 'id']);
            $users_story_dropdown = $user_story_dropdown->flatten(2)->unique()->values();
        } else {

            $projects = Project::all();
            
            $users = User::selectRaw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS 'full_name', users.id AS 'id',case when (select count(assignment.id) from assignment where assignment.employee_id = users.id and assignment.assignment_status in (1,2,3)) > 0 then 1 else 0 end as 'assignment_cnt'")->filters()->orderBy('users.first_name')->orderBy('users.last_name')->get();
            $sprints = Sprint::orderBy('name')->get(['name', 'id','status_id']);
            $tasks = [];
            $users_story_dropdown = [];

        }

        $hours_drop_down = [];
        $minutes_drop_down = [];

            $customers = Customer::filters()->orderBy('customer_name')->get(['customer_name', 'id']);

            $company = Company::orderBy('company_name')->filters()->get(['company_name','id']);
        
            $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')
                ->where('team_name', '!=', '')->get(['team_name', 'id']);

            $task_status = TaskStatus::select(['id', 'description']);


            $task_status = $task_status->where('id','<','5');

            $task_status = $task_status->orWhere('order','5');
            
            $task_status = $task_status->orWhere('order','6');
            
            $task_status = $task_status->orderBy('order')->get();

            $parameters = [
                'customer' => $projid > 0 ? Customer::find($project->customer_id) : [],
                'users_drop_down' => $users,
                'all_users_drop_down' => User::selectRaw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS 'full_name', users.id AS 'id'")->orderBy('users.first_name')->orderBy('users.last_name')->get(),
                'goals_drop_down' => ($request->has('sprint') ? Goal::where('sprint_id',$request->sprint)->orderBy('name')->get(['id', 'name']) : []),
                'projects_drop_down' => Project::filters()->whereIn('status_id',[1,2,3])->orderBy('name')->get(['id', 'name']),
                'all_projects_drop_down' => Project::orderBy('name')->get(['id', 'name']),
                'company_drop_down' => $company,
                'customers_drop_down' => $customers,
                'team_drop_down' => $team_drop_down,
                'sprints_drop_down' => $sprints,
                'user_story_drop_down' => $users_story_dropdown,
                'tasks_drop_down' => $tasks,
                'statuses_drop_down' => $task_status,
                'task_delivery_type_drop_down' => TaskDeliveryType::orderBy('name')->get(),
                'permissions_drop_down' => [['id' => 1, 'name' => 'Public'], ['id' => 2, 'name' => 'Private']],
                'yes_no_drop_down' => [['id' => 0, 'name' => 'No'], ['id' => 1,  'name' => 'Yes']],
                'year_week_drop_down' => $year_week_drop_down,
                'hours_drop_down' => $hours_drop_down,
                'minutes_drop_down' => $minutes_drop_down,
                'is_billable' => $is_billable,
                'week_days' => $week_days
            ];

        return response()->json($parameters);
    }

    private function kanbanData($project, $request, $status, $type = null, $type_id = null)
    {
        $bugs = BugTask::get();
        foreach($bugs as $bug){
            Task::where('id',$bug->task_id)->update(['priority_level'=>$bug->priority_level]);
        }
        $config = Config::first();
        $page = $request->page ?? 0;
        $items_per_page = $config->kanban_nr_of_tasks ?? 7;
        $offset = $request->page * $items_per_page;
        $project_id = $request->project_id ?? $project;

        $tasks_count = Task::where('status', $status->id)->whereProject($project_id);
        if($type == 'user_story'){
            $tasks_count = $tasks_count->where('user_story_id',$type_id);
        }
        if($type == 'consultant'){
            $tasks_count = $tasks_count->where('employee_id',$type_id);
        }
        if($type == 'priority'){
            $tasks_count = $tasks_count->where('priority_level',$type_id);
        }
        $tasks_count = $tasks_count->filters()->count();

        $tasks = Task::select(['id', 'description', 'employee_id', 'emote_id', 'project_id', 'start_date', 'end_date', 'status', 'remaining_time','hours_planned','goal_id','sprint_id','user_story_id'])
        ->with(['consultant' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        },
            'bugTask' => function ($bug) {
                return $bug->select('task_id', 'severity_level', 'ref_number');
            }, 'project' => function ($project) {
                return $project->select(['id', 'name', 'ref']);
            },'tasknotes' => function ($notes){
                return $notes->select(['task_id','id'])->whereHas('user');
            } ])
            ->when(isset($type) && $type == 'user_story', function ($task) use ($type_id) {
                $task->where('user_story_id',$type_id);
            })
            ->when(isset($type) && $type == 'consultant', function ($task) use ($type_id) {
                $task->where('employee_id',$type_id);
            });
            

            if($type == 'priority'){
                $tasks = $tasks->where('priority_level',$type_id);
            }

            $tasks = $tasks->where('status', $status->id)->whereHas('consultant')->whereHas('project')->whereProject($project_id)->filters()->skip($items_per_page * $page)->take($items_per_page)->latest()->get();
            
            $times = [];
            foreach($tasks as $task){
                if($task->project?->project_type_id == 1){
                    $times[$task->id] = Timeline::selectRaw('IFNULL(SUM(total + (total_m/60)), 0) AS hours')->where('is_billable', '=', 1)->where('task_id', '=', $task->id)->first();
                } else {
                    $times[$task->id] = Timeline::selectRaw('IFNULL(SUM(total + (total_m/60)), 0) AS hours')->where('task_id', '=', $task->id)->first();
                }
            }
            // dd($times);

        return [
            'id' => $status->id,
            'description' => $status->description,
            'user_story_id' => $type == 'user_story' ? (int)$type_id : 0,
            'consultant_id' => $type == 'consultant' ? (int)$type_id : 0,
            'priority_id' => $type == 'priority' ? (int)$type_id : 0,
            'tasks' => $tasks,
            'times' => $times,
            'total_count' => $tasks_count,
            'load_more' => ($offset + $items_per_page) < $tasks_count,
            'page' => ++$page,
            'project_id' => $project_id,

        ];
    }

    private function yearWeekDropdowns()
    {
        $year_week_drop_down = $this->getYearWeek();

        $selected_year_week = '';
        $new_year_week_drop_down = [];
        foreach ($year_week_drop_down as $key => $value) {
            if ($selected_year_week == '') {
                $selected_year_week = $key;
            }

            $new_year_week_drop_down[] = [
                'id' => $key,
                'name' => $value,
            ];
        }

        return $new_year_week_drop_down;
    }

    private function resources()
    {
        $projects = Project::select(['consultants'])->whereIn('status_id', [1, 2, 3])->get();
        $user_ids = [];
        foreach ($projects as $project) {
            $user_ids = array_merge($user_ids, explode(',', $project->consultants));
        }

        return User::select(['id', 'first_name', 'last_name'])->whereIn('id', array_values(array_unique($user_ids)))
            ->get()->map(function ($user) {
                return (object) [
                    'id' => $user->id,
                    'full_name' => $user->name(),
                ];
            });
    }

    public function reassignTask(Request $request, Task $task, ClockifyAPI $clockify)
    {
        $new_task = new Task();
        $new_task->description = $task->description;
        $new_task->dependency = $task->dependency;
        $new_task->parent_activity_id = $task->parent_activity_id;
        $new_task->task_type_id = $task->task_type_id;
        $new_task->employee_id = $request->employee_id;
        $new_task->customer_id = $task->customer_id;
        $new_task->project_id = $task->project_id;
        $new_task->start_date = $task->start_date;
        $new_task->end_date = $task->end_date;
        $new_task->hours_planned = $task->hours_planned;
        $new_task->user_story_id = $task->user_story_id;
        $new_task->status = 3;
        $new_task->billable = $task->billable;
        $new_task->on_kanban = $task->on_kanban;
        $new_task->note_1 = $task->note_1;
        $new_task->creator_id = auth()->id();
        $new_task->save();

        $taskUserEmail = isset($task->consultant) ? $task->consultant->email : '';
        $notification = new Notification;
        $notification->name = 'New Task has been created';
        $notification->link = route('task.show', $task->id);
        $notification->save();

        $user_notifications = [];

        array_push($user_notifications, [
            'user_id' => $task->id,
            'notification_id' => $notification->id,
        ]);

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);
        $utils = new Utils();

        try {
            SendTaskMailJob::dispatch($taskUserEmail, $task, $utils->helpPortal)->delay(now()->addMinutes(5));
        } catch (\Exception $exception) {
            return redirect(route('task.index', ['project_id' => $task->project_id]))->with('flash_danger', 'Task '.$task->id.' captured successfully but failed to send an email with error: '.$exception->getMessage());
        }

        $clockify_project_id = $clockify->projectId($request->input('project'));

        try {
            if ($clockify_project_id && config('app.clockify.is_enabled')) {
                $clockify->post('/projects/'.$clockify_project_id.'/tasks', ['name' => $task->id.' - '.$task->description]);
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        }

        return response()->json(['task' => $new_task]);
    }
}
