<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProjectValueSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 15;
        $actual_value = [];
        $forecast_value = [];
        $po_value = [];
        $year_dropdown = [];
        $year_month_dropdown = [];
        $date = Carbon::now();
        $projects = Project::with(['customer'])->whereIn('status_id', [2, 3]);
        if ($request->has('company') && $request->company != null) {
            $projects = $projects->where('company_id', '=', $request->company);
        }
        if ($request->has('customer') && $request->customer != null) {
            $projects = $projects->where('customer_id', '=', $request->customer);
        }
        if ($request->has('billable') && $request->billable != null) {
            $projects = $projects->where('billable', '=', $request->billable);
        }
        $projects = $projects->sortable('name')->paginate($item);
        $years = Timesheet::select('year')->groupBy('year')->get();
        $year_mon = Timesheet::select('year', 'month')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        foreach ($years as $year) {
            $year_dropdown[$year->year] = $year->year;
        }
        foreach ($year_mon as $period) {
            $year_month_dropdown[$period->year.$period->month] = $period->year.(($period->month < 10) ? '0'.$period->month : $period->month);
        }
        foreach ($projects as $project) {
            $time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$project->billable?->value.' THEN '.(($project->billable?->value == 1) ? '(timeline.total + (timeline.total_m/60))*IFNULL(assignment.invoice_rate,0)' : '(timeline.total + (timeline.total_m/60))*(IFNULL(assignment.external_cost_rate,0) + IFNULL(assignment.internal_cost_rate,0))').' ELSE 0 END) AS value')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->leftJoin('assignment', function ($join) {
                    $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                    $join->on('timesheet.project_id', '=', 'assignment.project_id');
                })
                ->where('timesheet.project_id', '=', $project->id);
            if ($request->has('year') && $request->year != '') {
                $time = $time->where('timesheet.year', '=', $request->year);
            }
            if (($request->has('period_from') && $request->period_from != '') && ($request->has('period_to') && $request->period_to != '')) {
                $time = $time->whereBetween('timesheet.year', [substr($request->period_from, 0, 4), substr($request->period_to, 0, 4)])->whereBetween('timesheet.month', [substr($request->period_from, 4), substr($request->period_to, 4)]);
            }
            $time = $time->first();

            $ass = Assignment::where('project_id', '=', $project->id)->whereIn('assignment_status', [2, 3])->get();

            $ass_value = 0;
            $forecast_v = 0;
            foreach ($ass as $value) {
                $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $value->planned_end_date])->get()->count();
                $ass_value += $value->hours * (($project->billable == 1) ? $value->invoice_rate : ($value->external_cost_rate + $value->internal_cost_rate));
                $forecast_v += ($calendar * (($value->capacity_allocation_hours != null && $value->capacity_allocation_min != null) ? ($value->capacity_allocation_hours + ($value->capacity_allocation_min / 60)) : 8)) * (($project->billable == 1) ? $value->invoice_rate : ($value->external_cost_rate + $value->internal_cost_rate));
            }
            array_push($actual_value, $time);
            array_push($forecast_value, $forecast_v);
            array_push($po_value, $ass_value);
        }

        $total_actual_value = 0;
        $total_forecast_value = 0;
        $total_po_value = 0;
        foreach ($actual_value as $actual) {
            $total_actual_value += $actual->value;
        }
        foreach ($forecast_value as $actual) {
            $total_forecast_value += $actual;
        }
        foreach ($po_value as $po) {
            $total_po_value += $po;
        }

        $parameters = [
            'company_drop_down' => Company::filters()->select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'projects' => $projects,
            'timesheets' => $actual_value,
            'forecast_value' => $forecast_value,
            'po_value' => $po_value,
            'year_dropdown' => $year_dropdown,
            'year_month_dropdown' => $year_month_dropdown,
            'date' => $date,
            'total_actual_value' => $total_actual_value,
            'total_po_value' => $total_po_value,
            'total_forecast_value' => $total_forecast_value,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'projectvalue');

        return view('reports.management.projectvalue')->with($parameters);
    }
}
