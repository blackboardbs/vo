<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\ApprovalRules;
use App\Models\Assignment;
use App\Models\BillingCycle;
use App\Models\BillingPeriod;
use App\Models\BillingPeriodStyle;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\CustomerInvoice;
use App\Models\VendorInvoice;
use App\Models\DigisignUsers;
use App\Models\Document;
use App\Models\Epic;
use App\Http\Requests\StoreTimeLineRequest;
use App\Http\Requests\StoreTimeSheetExpenseRequest;
use App\Http\Requests\StoreTimeSheetRequest;
use App\Http\Requests\TimeSheetAttachmentRequest;
use App\Http\Requests\UpdateTimeLineRequest;
use App\Http\Requests\UpdateTimeSheetExpenseRequest;
use App\Http\Requests\UpdateTimeSheetRequest;
use App\Models\InvoiceStatus;
use App\Jobs\SendAssignmentMaxHoursExceededEmailJob;
use App\Jobs\SendTimesheetDigiSignEmailJob;
use App\Jobs\SendUploadLogoEmailJob;
use App\Models\Module;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Resource;
use App\Models\RoleUser;
use App\Models\Status;
use App\Models\Task;
use App\Models\TaskDeliveryType;
use App\Models\TaskStatus;
use App\Models\Time;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Timesheet\PreviousTimesheet;
use App\Timesheet\TimesheetHelperTrait;
use App\Models\Timesheet\TimesheetTemplate;
use App\Models\User;
use App\Models\UserNotification;
use App\Utils;
use App\Models\VendorInvoiceStatus;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Knp\Snappy\Exception\FileAlreadyExistsException;
use PDF;
use Spatie\Activitylog\Models\Activity;

class TimesheetNewController extends Controller
{
    use TimesheetHelperTrait;

    private $utils;

    public function __construct()
    {
        $this->middleware('auth');
        $this->utils = new Utils();
    }

    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        $flag = false;

        if (Auth::user()->canAccess($module[0]->id, 'create_all')) {
            $flag = true;
        }

        if (Auth::user()->canAccess($module[0]->id, 'create_team')) {
            $flag = true;
        }

        if ($flag == false) {
            return abort(403);
        }

        $employee_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'users.id')
            ->join('resource', 'users.id', '=', 'resource.user_id')->where('users.id', '=', Auth::id())
            ->where('resource.status_id', '=', 1)->orderBy('id', 'desc')
            ->pluck('full_name', 'id');

        $assignments = Assignment::with(['project' => function ($project) {
            $project->select('id', 'name', 'customer_id');
        }, 'project.customer' => function ($customer) {
            $customer->select('id', 'customer_name');
        }])->select('employee_id', 'project_id', 'end_date')
            ->where('assignment_status', 3)
            ->groupBy(['employee_id', 'project_id', 'end_date']);

        if (Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager'])) {
            $employees = $assignments->with(['resource' => function ($query) {
                $query->select('first_name', 'last_name', 'id');
            }])->get();
            foreach ($employees as $resource) {
                $employee_drop_down[$resource->employee_id] = (isset($resource->resource) ? $resource->resource->first_name.' '.$resource->resource->last_name : null);
            }
        }

        $parameters = [
            'employee' => isset($employee_drop_down) ? $employee_drop_down : [],
            'creator_id' => auth()->id(),
        ];

        return view('timesheet.create')->with($parameters);
    }

    public function newShow($timesheet_id){
        
        $timesheet = Timesheet::find($timesheet_id);

        $permissions = $this->timesheetTabPermissions();
// dd($permissions);
        $project = Project::find($timesheet->project_id);

        $config = Config::select(['timesheet_template_id', 'billing_cycle_id', 'default_custom_template'])->first();
        $config_temp = $config->timesheet_template_id ?? 1;
        $project_template_id = $project->timesheet_template_id ?? $config_temp;

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->get();

        $assignment_hours_left = $assignment[0]->hours;

        $empty = $assignment->isEmpty();

        $is_billable = 0;
        if (! $empty) {
            $assignment_hours_left = $assignment[0]->hours;

            if ($assignment[0]->billable == null || $assignment[0]->billable == '') {
                $is_billable = 0;
            } else {
                $is_billable = $assignment[0]->billable;
            }
        }

        $employee = User::find($timesheet->employee_id);

        $subject = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';
        $subject_send_email = 'Timesheet - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';

        $timelines = Timeline::with('task','timesheet.project','timesheet')->where('timesheet_id', '=', $timesheet->id)->get();

        $time_exp = TimeExp::where('timesheet_id', '=', $timesheet->id)->get();

        $task_drop_down = Task::where('project_id', '=', $timesheet->project_id)
            ->where('status', '!=', 5)
            ->where('employee_id', '=', $timesheet->employee_id)
            ->pluck('description', 'id');

        $total_non_bill = 0;
        $total_bill = 0;
        $timelines2 = Timesheet::selectRaw('timeline.is_billable as billable,SUM(timeline.total + (timeline.total_m/60)) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.employee_id', '=', $assignment[0]->employee_id)
                ->where('timesheet.project_id', '=', $assignment[0]->project_id)
                ->groupBy('timeline.is_billable')
                ->get();

        foreach ($timelines2 as $timeline) {

                if ($timeline->billable == 1) {
                    $total_bill += $timeline->actual_hours;
                }
                if ($timeline->billable == 0) {
                    $total_non_bill += $timeline->actual_hours;
                }
        }

        $remaining_assignment_hours = $project->project_type_id == 1 ? ($assignment_hours_left) - ($total_bill) : ($assignment_hours_left) - ($total_bill+$total_non_bill);
        // $assignment_hours = $total_time_in_minutes;
        // $remaining_assignment_hours = ($assignment_hours_left * 60);
        $combined_timesheets = $this->combinedTimesheet($timesheet, $config, $project);
        $billing_cycle = $combined_timesheets->billing_cycle_id;

        // $year_week_drop_down = $this->weeks($config, $billing_cycle ?? 0)->keyBy('year_week')->map(function ($week) {
        //     return $week['date'];
        // });

        $epics_drop_down = Epic::with(['features' => function ($feature) {
            return $feature->select(['id', 'name', 'epic_id'])->with(['user_stories' => function ($story) {
                return $story->select(['id', 'name', 'feature_id']);
            }]);
        }])->where('project_id', $timesheet->project_id)->get(['name', 'id']);

        $is_approver = ($assignment[0]->assignment_approver && $assignment[0]->assignment_approver == auth()->user()->id ? 1 : 0 );

        $parameters = [
            'is_approver' => $is_approver,
            'project_name' => $project->name,
            'project_ref' => $project->ref,
            'is_billable' => $is_billable,
            'assignment_hours_left' => $remaining_assignment_hours,
            // 'assignment_hours' => $assignment_hours,
            'time_exp' => $time_exp,
            'subject' => $subject,
            'subject_send_email' => $subject_send_email,
            'assignment' => $assignment,
            // 'total_exp_bill' => $total_exp_bill,
            // 'total_exp_claim' => $total_exp_claim,
            'timesheet' => $timesheet,
            // 'edit_timeline' => $edit_timeline,
            'use_custom_template' => (isset($project->billing_timesheet_templete_id) ? $project->billing_timesheet_templete_id : null),
            'timelines' => $timelines,
            'employee' => $employee,
            // 'hours' => $hours,
            // 'minutes' => $minutes,
            // 'task_drop_down' => $task_drop_down,
            'template_id' => $config->default_custom_template,
            // 'year_week_drop_down' => $year_week_drop_down,
            // 'week_days' => $week_days,
            'billing_cycle_id' => $billing_cycle,
            'current_billing_period_id' => $combined_timesheets->current_billing_period_id,
            'task_delivery_type_id' => TaskDeliveryType::orderBy('name')->pluck('name', 'id')->prepend('Select Task Delivery Type'),
            'epics_drop_down' => json_encode($epics_drop_down),
            'status_drop_down' => TaskStatus::pluck('description', 'id'),
            'yes_or_no_dropdown' => Task::KANBAN_DISPLAY,
            'permissions' => $permissions,
            'configs' => Config::first()
        ];

        // dd($parameters['template_id']);

        return view('timesheet.timeline.show')->with($parameters);
        // return view('timesheet.timeline.edit')->with($parameters);
    }

    public function toggleTimesheetLock(Request $request){
        $timesheet = Timesheet::find($request->timesheet);

        $timesheet->timesheet_lock = $request->lock_status;
        $timesheet->save();

        
        return response()->json(['message'=>'success']);
    }

    public function updateNote(Request $request){
        $timesheet = Timesheet::find($request->timesheet);

        $timesheet->invoice_note = $request->note;
        $timesheet->save();

        
        return response()->json(['message'=>'success']);
    }

    public function getTimesheetDropdowns($timesheet,$billing_cycle){
        $config = Config::first();
        $timesheet = Timesheet::find($timesheet);
        $task_drop_down = Task::where('project_id', '=', $timesheet->project_id)
            // ->where('status', '!=', 5)
            ->where('employee_id', '=', $timesheet->employee_id)
            ->get(['status','billable','description', 'id']);

        $accounts = Account::where('status','1')->orderBy('description')->get(['description','id']);

        $elements = AccountElement::where('status','1')->orderBy('description')->get(['description','id']);

        if(Carbon::parse($timesheet->first_day_of_week)->diffInDays(Carbon::parse($timesheet->last_day_of_week)) > 7){
            $date = now()->setISODate($timesheet->year, $timesheet->week);

            $parse_week_start_date = $date->copy()->startOfWeek();
            $parse_week_end_date = $date->copy()->endOfWeek();
        } else {
            $parse_week_start_date = Carbon::parse($timesheet->first_day_of_week);
            $parse_week_end_date = Carbon::parse($timesheet->last_day_of_week);
        }
        $week_days = [];

        for ($parse_week_start_date; $parse_week_start_date <= $parse_week_end_date; $parse_week_start_date->addDay()) {
            array_push($week_days, $parse_week_start_date->format('D d'));
        }

        $year_week_drop_down = $this->weeks($config, $billing_cycle ?? 0)->keyBy('year_week')->map(function ($week) {
            return $week['date'];
        });

        $epics_drop_down = Epic::with(['features' => function ($feature) {
            return $feature->select(['id', 'name', 'epic_id'])->with(['user_stories' => function ($story) {
                return $story->select(['id', 'name', 'feature_id']);
            }]);
        }])->where('project_id', $timesheet->project_id)->get(['name', 'id']);

        if($config->kanban_show_impediment != 0 || $config->kanban_show_completed != 0){
            $task_status = TaskStatus::select(['id', 'description']);

            $task_status = $task_status->where('id','<','5');

            if($config->kanban_show_impediment == 1){
                $task_status = $task_status->orWhere('order','5');
            }
            
            if($config->kanban_show_completed == 1){
                $task_status = $task_status->orWhere('order','6');
            }
            
            $task_status = $task_status->orderBy('order')->get();
        } else {
            $task_status = TaskStatus::select(['id', 'description'])->where('id','<','5')->orderBy('order')->get();
        }

        
        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();
        $hours = Timeline::selectRaw('IFNULL(SUM(total + (total_m/60)), 0) AS hours')->where('is_billable', '=', 1)->where('timesheet_id', '=', $timesheet->id)->first();

        $parameters = [
            'hours' => $hours->hours,
            'assignment' => $assignment,
            'year_week_drop_down' => $year_week_drop_down,
            'week_days' => $week_days, //202113
            'task_drop_down' => $task_drop_down,
            'account_drop_down' => $accounts,
            'elements_drop_down' => $elements,
            'epics_drop_down' => $epics_drop_down,
            'task_status' => $task_status,
            'task_delivery_types' => TaskDeliveryType::orderBy('name')->get(['name', 'id']),
            'attachments' => Document::where('reference_id', '=', $timesheet->id)->where('document_type_id', '=', 7)->get()
        ];

        return response()->json($parameters);
    }

    public function getProjectTimesheets($timesheet){
        $config = Config::select(['timesheet_template_id', 'billing_cycle_id', 'timesheet_weeks', 'timesheet_weeks_future', 'default_custom_template'])->first();

        $timesheet = Timesheet::find($timesheet);
        
        $project = Project::find($timesheet->project_id);
        
        $combined_timesheets = $this->combinedTimesheet($timesheet, $config, $project);
        $project_timesheets = $combined_timesheets->project_timesheets;

        $parameters = [
            'project_timesheets' => $project_timesheets
        ];

        return response()->json($project_timesheets);
    }

    private function weeks(Config $config, int $billingCycle = 0)
    {
        $weeks = [];
        $date = Carbon::now();
        $config_weeks = (int) ($config->timesheet_weeks ?? 12);
        $ceil_week = $date->copy()->addWeeks((int) $config->timesheet_weeks_future)->endOfWeek();
        $floor_week = $date->copy()->subWeeks($config_weeks)->startOfWeek();
        $billing_periods = BillingPeriod::query()
            ->where('billing_cycle_id', $billingCycle)
            ->whereBetween('start_date', [$floor_week->copy()->toDateString(), $ceil_week->copy()->toDateString()])
            ->orderBy('start_date', 'ASC')
            ->get();

        if ($billing_periods->isEmpty() || ! $billingCycle) {
            $weeks = collect($this->yearWeekDropDown())->map(function ($week, $key) {
                return [
                    'year_week' => (string) $key,
                    'date' => $week,
                ];
            })->sortByDesc('year_week')->values();
        } else {
            foreach ($billing_periods as $period) {
                for ($i = Carbon::parse($period->start_date)->startOfWeek(); $i <= Carbon::parse($period->end_date)->endOfWeek(); $i->addWeeks(1)) {
                    $week = str_pad($i->copy()->weekOfYear, 2, '0', STR_PAD_LEFT);
                    $year_week = $i->copy()->year.$week;

                    if ($i->copy()->toDateString() > now()->addWeeks(((int) $config->timesheet_weeks_future) ?? 0)->endOfWeek()->toDateString()) {
                        break;
                    }

                    $split_2_start = Carbon::parse($period->end_date);
                    $short_week = $i->copy()->startOfWeek()->diffInDays($split_2_start);

                    if ($short_week < 6) {
                        array_push($weeks, [
                            'year_week' => ($year_week == 202401 ? '202453_1' : $year_week).'_1',
                            'date' => ($year_week == 202401 ? 202453 : $year_week).'('.$i->copy()->toDateString().')-1',
                            'period' => $period->id,
                        ]);
                    } elseif ((Carbon::parse($period->start_date)->diffInDays($i->copy()->endOfWeek()) < 6) && Carbon::parse($period->start_date) <= now()) {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? '202501_2' : $year_week.'_2',
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.Carbon::parse($period->start_date)->toDateString().')-2',
                            'period' => $year_week == 202401 ? $period->id+1 : $period->id,
                        ]);
                    } else {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? 202501 : $year_week,
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.$i->copy()->toDateString().')',
                            'period' => $period->id,
                        ]);
                    }
                }
            }
        }

        return collect($weeks)->sortByDesc('year_week')->values();
    }

    private function yearWeekDropDown()
    {
        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $date_today = date('Y-m-d');
        $year_2020_week_1_flag = false;
        if (($year == 2019) && ($date_today == '2019-12-31')) {
            $year += 1;
            $year_2020_week_1_flag == true;
        }

        $config = Config::select(['timesheet_weeks', 'timesheet_weeks_future'])->find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $number_of_weeks_future = isset($config->timesheet_weeks_future) && (int) $config->timesheet_weeks_future > 0 ? $config->timesheet_weeks_future : 0;

        if (($week + $number_of_weeks_future) <= 53) {
            $week += $number_of_weeks_future;
        }

        //$week_drop_down[0] = "Select Week";

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week <= 6) {
                    if (($i == 1) && ($year == 2020)) {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$year.'-'.$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'(2019-12-31)-1';
                    } else {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';
                    }

                    continue;
                }

                // Disable first week for 2021, this worked for 2020
                /*if($i == 1){
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_2'] = $year.($i<10?'0'.$i:$i)."(".date('Y-m')."-01)-2";
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_1'] = $year.($i<10?'0'.$i:$i)."(".$date2->format('Y-m-d').")-1";
                }
                else{*/
                $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                /*}*/
            }

            $number_of_weeks -= $week;
            $week = 53;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week <= 6) {
                // Weird week 53 for 2021
                if ($i == 53) {
                    $week_drop_down[$year.$i.'_2'] = ($year + 1).$i.'(2021-'.$week_two_month.'-01)-2';
                } else {
                    $week_drop_down[$year.$i.'_2'] = $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2';
                }
                $week_drop_down[$year.$i.'_1'] = $year.$i.'('.$date2->format('Y-m-d').')-1';

                continue;
            }
            $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
        }

        return $week_drop_down;
    }

    public function getInvoiceDetails($timesheet_id){

        $timesheet = Timesheet::find($timesheet_id);

        $invoice = CustomerInvoice::find($timesheet->invoice_number);

        $status = InvoiceStatus::get();

        return response()->json(["invoice" => $invoice ?? [],"invoice_status"=>$status]);
    }

    public function getVendorInvoiceDetails($invoice_nr){

        $invoice = VendorInvoice::find($invoice_nr);

        $status = VendorInvoiceStatus::orderBy('description')->get();

        return response()->json(["invoice" => $invoice,"invoice_status"=>$status]);
    }

    public function timesheetTabPermissions(){
        $taskc = Module::where('name', '=', \App\Models\TimesheetTaskCreate::class)->first();

        $taskc_can_create = false;
        if (Auth::user()->canAccess($taskc?->id, 'create_task')) {
            $taskc_can_create = true;
        }
        
        $exp = Module::where('name', '=', \App\Models\TimesheetExpenses::class)->first();

        $exp_can_view = false;
        if (Auth::user()->canAccess($exp->id, 'show_all') || Auth::user()->canAccess($exp->id, 'show_team')) {
            $exp_can_view = true;
        }

        $exp_can_create = false;
        if (Auth::user()->canAccess($exp->id, 'create_all') || Auth::user()->canAccess($exp->id, 'create_team')) {
            $exp_can_create = true;
        }

        $exp_can_update = false;
        if (Auth::user()->canAccess($exp->id, 'update_all') || Auth::user()->canAccess($exp->id, 'update_team')) {
            $exp_can_update = true;
        }

        $timesheet = Module::where('name', '=', \App\Models\Timesheet::class)->first();

        $timesheet_can_view = false;
        if (Auth::user()->canAccess($timesheet->id, 'show_all') || Auth::user()->canAccess($timesheet->id, 'show_team')) {
            $timesheet_can_view = true;
        }

        $timesheet_can_create = false;
        if (Auth::user()->canAccess($timesheet->id, 'create_all') || Auth::user()->canAccess($timesheet->id, 'create_team')) {
            $timesheet_can_create = true;
        }

        $timesheet_can_update = false;
        if (Auth::user()->canAccess($timesheet->id, 'update_all') || Auth::user()->canAccess($timesheet->id, 'update_team')) {
            $timesheet_can_update = true;
        }

        $ci = Module::where('name', '=', \App\Models\CustomerInvoice::class)->first();

        $ci_can_view = false;
        if (Auth::user()->canAccess($ci->id, 'show_all') || Auth::user()->canAccess($ci->id, 'show_team')) {
            $ci_can_view = true;
        }

        $ci_can_create = false;
        if (Auth::user()->canAccess($ci->id, 'create_all') || Auth::user()->canAccess($ci->id, 'create_team')) {
            $ci_can_create = true;
        }

        $ci_can_update = false;
        if (Auth::user()->canAccess($ci->id, 'update_all') || Auth::user()->canAccess($ci->id, 'update_team')) {
            $ci_can_update = true;
        }

        $vi = Module::where('name', '=', \App\Models\VendorInvoice::class)->first();

        $vi_can_view = false;
        if (Auth::user()->canAccess($vi->id, 'show_all') || Auth::user()->canAccess($vi->id, 'show_team')) {
            $vi_can_view = true;
        }

        $vi_can_create = false;
        if (Auth::user()->canAccess($vi->id, 'create_all') || Auth::user()->canAccess($vi->id, 'create_team')) {
            $vi_can_create = true;
        }

        $vi_can_update = false;
        if (Auth::user()->canAccess($vi->id, 'update_all') || Auth::user()->canAccess($vi->id, 'update_team')) {
            $vi_can_update = true;
        }

        $permissions = [
            'taskc_can_create' => $taskc_can_create,
            'timesheet_can_view' => $timesheet_can_view,
            'timesheet_can_create' => $timesheet_can_create,
            'timesheet_can_update' => $timesheet_can_update,
            'exp_can_view' => $exp_can_view,
            'exp_can_create' => $exp_can_create,
            'exp_can_update' => $exp_can_update,
            'ci_can_view' => $ci_can_view,
            'ci_can_create' => $ci_can_create,
            'ci_can_update' => $ci_can_update,
            'vi_can_view' => $vi_can_view,
            'vi_can_create' => $vi_can_create,
            'vi_can_update' => $vi_can_update,
        ];

        return $permissions;
    }

}

abstract class PrintTemplate
{
    const StandardTemplate = 1;

    const WeeklyTemplate = 2;

    const ArbourTemplate = 3;

    const MonthlyTemplate = 4;
}
