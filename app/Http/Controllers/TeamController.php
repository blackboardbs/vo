<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\Team;
use App\Services\TeamService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TeamController extends Controller
{
    //
    public function create(Request $request, TeamService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        return view('team.create')->with(['companyid' => $request->company_id]);
    }

    public function store(StoreTeamRequest $request, Team $team, TeamService $service): RedirectResponse
    {
        $team = $service->createTeam($request, $team);
        $team->parent_company_id = $request->company_id;
        $team->creator_id = auth()->id();
        $team->save();

        return redirect('company')->with(['flash_success' => 'Team added successfully.']);
    }

    public function edit(Team $team, TeamService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        return view('team.edit')->with(['team' => $team]);
    }

    public function update(UpdateTeamRequest $request, Team $team, TeamService $service): RedirectResponse
    {
        $service->updateTeam($request, $team);

        return redirect()->route('company.index')->with('success', 'Team updated successfully');
    }

    public function destroy(Team $team, TeamService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $team->delete();

        return redirect()->route('company.index')->with('success', 'Team deleted successfully');
    }
}
