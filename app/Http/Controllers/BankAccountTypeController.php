<?php

namespace App\Http\Controllers;

use App\Models\BankAccountType;
use App\Http\Requests\BankAccountTypeRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BankAccountTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;
        $items = ($request->has('r') && $request->r > 0) ? $request->r : 300;
        // $account_type = BankAccountType::where('status_id', 1);
        // $account_type = BankAccountType::sortable('description', 'asc')->where('status_id', 1)->paginate($item);
        $account_type = BankAccountType::with(['status'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $account_type = $account_type->where('status_id', $request->input('status_filter'));
            }else{
                $account_type = $account_type->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $account_type = $account_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $account_type = $account_type->paginate($item);
        // $account_type = $account_type->paginate($items);
        $parameters = [
            'account_types' => $account_type,
        ];

        return view('master_data.bank_account_type.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = BankAccountType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();
        $parameters = [
            'status_drop_down' => Status::where('status', 1)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.bank_account_type.create')->with($parameters);
    }

    public function store(BankAccountTypeRequest $request): RedirectResponse
    {
        $account_type = new BankAccountType();
        $account_type->description = $request->description;
        $account_type->status_id = $request->status;
        $account_type->save();

        return redirect()->route('master_bankaccounttype.index')->with('flash_success', 'Bank BankAccountType Type was created successfully');
    }

    public function show($id): View
    {
        $parameters = [
            'account_type' => BankAccountType::find($id),
        ];

        return view('master_data.bank_account_type.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = BankAccountType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_drop_down' => Status::where('status', 1)->pluck('description', 'id'),
            'account_type' => BankAccountType::find($id),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.bank_account_type.edit')->with($parameters);
    }

    public function update(BankAccountTypeRequest $request, $id): RedirectResponse
    {
        $account_type = BankAccountType::find($id);
        $account_type->description = $request->description;
        $account_type->status_id = $request->status;
        $account_type->save();

        return redirect()->route('master_bankaccounttype.index')->with('flash_success', 'Bank BankAccountType Type was created successfully');
    }

    public function destroy($id)
    {
        $item = BankAccountType::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('master_bankaccounttype.index')->with('flash_warning', 'Bank BankAccountType Type suspended successfully');
    }
}
