<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMedicalCertificateTypeRequest;
use App\Http\Requests\UpdateMedicalCertificateTypeRequest;
use App\Models\MedicalCertificateType;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MedicalCertificateTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $medical_master = MedicalCertificateType::sortable('description', 'asc')->paginate($item);

        $medical_master = MedicalCertificateType::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $medical_master = $medical_master->where('status', $request->input('status_filter'));
            }else{
                $medical_master = $medical_master->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $medical_master = $medical_master->where('description', 'like', '%'.$request->input('q').'%');
        }

        $medical_master = $medical_master->paginate($item);

        $parameters = [
            'medical_master' => $medical_master,
        ];

        return View('master_data.medical_certificate_type.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = MedicalCertificateType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.medical_certificate_type.create')->with($parameters);
    }

    public function store(StoreMedicalCertificateTypeRequest $request): RedirectResponse
    {
        $medical_master = new MedicalCertificateType();
        $medical_master->description = $request->input('description');
        $medical_master->status = $request->input('status');
        $medical_master->creator_id = auth()->id();
        $medical_master->save();

        return redirect(route('medical_master.index'))->with('flash_success', 'Master Data Medical Certificate Type captured successfully');
    }

    public function show($medical_master_id): View
    {
        $parameters = [
            'medical_master' => MedicalCertificateType::where('id', '=', $medical_master_id)->get(),
        ];

        return View('master_data.medical_certificate_type.show')->with($parameters);
    }

    public function edit($medical_master_id): View
    {
        $autocomplete_elements = MedicalCertificateType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'medical_master' => MedicalCertificateType::where('id', '=', $medical_master_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.medical_certificate_type.edit')->with($parameters);
    }

    public function update(UpdateMedicalCertificateTypeRequest $request, $medical_master_id): RedirectResponse
    {
        $medical_master = MedicalCertificateType::find($medical_master_id);
        $medical_master->description = $request->input('description');
        $medical_master->status = $request->input('status');
        $medical_master->creator_id = auth()->id();
        $medical_master->save();

        return redirect(route('medical_master.index'))->with('flash_success', 'Master Data Medical Certificate Type saved successfully');
    }

    public function destroy($medical_master_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // MedicalCertificateType::destroy($medical_master_id);
        $item = MedicalCertificateType::find($medical_master_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('medical_master.index')->with('success', 'Master Data Medical Certificate Type suspended successfully');
    }
}
