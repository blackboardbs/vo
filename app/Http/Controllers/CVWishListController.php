<?php

namespace App\Http\Controllers;

use App\Models\Cv;
use App\Models\CvSubmit;
use App\Models\CVWishList;
use App\Models\JobSpec;
use App\Models\JobSpecActivityLog;
use App\Models\Module;
use App\Models\Wishlist;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CVWishListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cvWishLists = CVWishList::orderBy('id')->where('status_id', '=', 1);

        $module = Module::where('name', '=', 'App\WishList')->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $cvWishLists = $cvWishLists->whereIn('creator_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $cvWishLists = $cvWishLists->get();

        $parameters = [
            'cvWishLists' => $cvWishLists,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        return view('cv/wishlist/index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $job_spec_drop_down = JobSpec::orderBy('id')->where('status_id', '=', 1)->pluck('reference_number', 'id');

        $parameters = [
            'job_spec_drop_down' => $job_spec_drop_down,
        ];

        return view('cv.wishlist.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        /*$cv_wish_list = CVWishList::where('job_spec_id', '=', $request->input('job_spec_id'))->first();

        if(!isset($cv_wish_list->id)) {
            $cv_wish_list = new CVWishList();
            $cv_wish_list->name = $request->input('name');
            $cv_wish_list->job_spec_id = $request->input('job_spec_id');
            $cv_wish_list->creator_id = auth()->id();
            $cv_wish_list->status_id = 1;
            $cv_wish_list->save();
        }
        else{
            return redirect(route('cvwishlist.index'))->with('flash_danger', 'Wishlist for Job Spec with reference number '.$cv_wish_list->reference_number.' already exists.');
        }*/

        $jobSpecID = $request->input('jobspec_id');
        $cv_wish_list = new CVWishList();
        $cv_wish_list->name = $request->input('name');
        $cv_wish_list->creator_id = auth()->id();
        $cv_wish_list->status_id = 1;
        $cv_wish_list->save();

        $job_spec_activity = new JobSpecActivityLog();
        $job_spec_activity->date = now();
        $job_spec_activity->activity = 'Wishlist Is Created';
        $job_spec_activity->job_spec_id = $request->input('jobspec_id');
        $job_spec_activity->author_id = auth()->user()->id;
        $job_spec_activity->status_id = 1;
        $job_spec_activity->save();

        if ($request->input('0') == 1) {
            return redirect(route('cvwishlist.index'))->with('flash_success', 'Wishlist added successfully.');
        }

        return redirect(route('jobspec.show', $jobSpecID))->with('flash_success', 'Wishlist added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CVWishList  $cVWishList
     */
    public function show($id): View
    {
        $jobSpecDropDown = JobSpec::orderBy('id')->where('status_id', '=', 1)->pluck('reference_number', 'id');

        $cvWishList = CVWishList::find($id);

        $wishlist = Wishlist::where('cv_wishlist_id', '=', $id)->get();
        $cv_ids = [];
        $cv_sent_at = [];
        $wishlist_id = [];
        $job_spec_ref = [];
        foreach ($wishlist as $list) {
            $job_spec_ref[$list->cv_id] = CvSubmit::where('cv_id', $list->cv_id)->get();
            array_push($cv_ids, $list->cv_id);
            $cv_sent_at[$list->cv_id] = $list->sent_at;
            $wishlist_id[$list->cv_id] = $list->id;
        }

        $cvs = Cv::whereIn('id', $cv_ids)->get();

        $jobSpecs = JobSpec::where('cv_wishlist_id', '=', $cvWishList->id)->get();

        $parameters = [
            'cvWishList' => $cvWishList,
            'jobSpecDropDown' => $jobSpecDropDown,
            'cvs' => $cvs,
            'jobSpecs' => $jobSpecs,
            'cv_sent_at' => $cv_sent_at,
            'wishlist_id' => $wishlist_id,
            'job_spec_ref' => $job_spec_ref,
        ];

        return view('cv.wishlist.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(CVWishList $cVWishList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CVWishList $cVWishList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CVWishList  $cVWishList
     */
    public function destroy($cvWishList_id): RedirectResponse
    {
        CVWishList::destroy($cvWishList_id);

        return redirect(route('cvwishlist.index'))->with('flash_success', 'CV removed successfully to wish list');
    }
}
