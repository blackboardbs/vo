<?php

namespace App\Http\Controllers;

use App\Models\Epic;
use App\Models\Feature;
use App\Http\Requests\FeatureRequest;
use App\Models\ProjectStatus;
use App\Models\Sprint;
use App\Models\UserStory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\Validator;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $features = Feature::orderBy('id');
        if ($request->has('p_id') && $request->input('p_id') != 0) {
            $features = $features->where('project_id', '=', $request->input('p_id'))->get();
        }

        $features = $features->get();

        $parameters = [
            'features' => $features,
        ];

        return view('project.feature.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $epicDropDown = Epic::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $featureDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $epic = Epic::find($request->input('epic_id'));

        $project_id = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $epicDropDown = Epic::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $featureDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $parameters = [
            'epic' => $epic,
            'project_id' => $project_id,
            'epicDropDown' => $epicDropDown,
            'featureDropDown' => $featureDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.feature.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FeatureRequest $request): RedirectResponse
    {
        $epic = Epic::find($request->input('epic_id'));
        if ($epic->status_id == 4) {
            return redirect()->back()->with('flash_info', 'You can\'t add feature to a completed epic.');
        }

        $feature = new Feature();
        $feature->name = $request->input('name');
        $feature->epic_id = $request->input('epic_id');
        $feature->start_date = $request->input('start_date');
        $feature->end_date = $request->input('end_date');
        $feature->hours_planned = $request->input('hours_planned');
        $feature->confidence_percentage = $request->input('confidence_percentage');
        $feature->estimate_cost = $request->input('estimate_cost');
        $feature->estimate_income = $request->input('estimate_income');
        $feature->billable = $request->input('billable');
        $feature->sprint_id = $request->input('sprint_id');
        $feature->dependency_id = $request->input('dependency_id');
        $feature->status_id = $request->input('status_id');
        $feature->save();

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Feature created successfully.');
        }

        return redirect(route('feature.index'))->with('flash_success', 'Feature successfully added.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Feature $feature): View
    {
        $epicDropDown = Epic::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $featureDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $parameters = [
            'feature' => $feature,
            'epicDropDown' => $epicDropDown,
            'featureDropDown' => $featureDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.feature.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Feature $feature, Request $request): View
    {
        $epicDropDown = Epic::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $featureDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $epic = Epic::find($feature->epic_id);

        $project_id = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $epicDropDown = Epic::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $featureDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $parameters = [
            'feature' => $feature,
            'epic' => $epic,
            'project_id' => $project_id,
            'epicDropDown' => $epicDropDown,
            'featureDropDown' => $featureDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.feature.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(FeatureRequest $request, Feature $feature): RedirectResponse
    {
        if ($request->status_id == 4) {
            $user_stories = UserStory::with(['tasks' => function ($task) {
                $task->where('status', '!=', 5);
            }])
                ->where('feature_id', $feature->id)
                ->where('status_id', '!=', 4)->get();

            if ($user_stories->isNotEmpty()) {
                return redirect()->back()->with('flash_info', 'There\'s user stories that are still open');
            }
        }

        $feature = Feature::find($feature->id);
        $feature->name = $request->input('name');
        $feature->epic_id = $request->input('epic_id');
        $feature->start_date = $request->input('start_date');
        $feature->end_date = $request->input('end_date');
        $feature->hours_planned = $request->input('hours_planned');
        $feature->confidence_percentage = $request->input('confidence_percentage');
        $feature->estimate_cost = $request->input('estimate_cost');
        $feature->estimate_income = $request->input('estimate_income');
        $feature->billable = $request->input('billable');
        $feature->sprint_id = $request->input('sprint_id');
        $feature->dependency_id = $request->input('dependency_id');
        $feature->status_id = $request->input('status_id');
        $feature->save();

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Feature updated successfully.');
        }

        return redirect(route('feature.index'))->with('flash_success', 'Feature successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Feature $feature, Request $request): RedirectResponse
    {
        Feature::destroy($feature->id);

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Feature deleted successfully.');
        }

        return redirect(route('feature.index'))->with('flash_success', 'Feature successfully deleted.');
    }

    public function getDropdowns(Request $request){
        $epicDropDown = Epic::orderBy('name')->get(['name', 'id']);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->get(['description', 'id']);
        $sprintDropDown = Sprint::orderBy('name')->get(['name', 'id']);
        $featureDropDown = Feature::orderBy('name')->get(['name', 'id']);

        $epic = Epic::find($request->input('epic_id'));

        $project_id = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $epicDropDown = Epic::orderBy('name')->where('project_id', '=', $project_id)->get(['name', 'id']);
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->get(['name', 'id']);
            $featureDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->get(['name', 'id']);
        }

        $parameters = [
            'epic' => $epic,
            'project_id' => $project_id,
            'epicDropDown' => $epicDropDown,
            'featureDropDown' => $featureDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return response()->json($parameters);
    }

    private function featureValidation(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'name' => 'string|required',
            'epic_id' => 'integer|required|not_in:0',
            'status_id' => 'integer|required|not_in:0',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'billable' => 'numeric|nullable',
            'hours_planned' => 'numeric|nullable',
            'estimate_cost' => 'numeric|nullable',
            'estimate_income' => 'numeric|nullable',
            'confidence_percentage' => 'numeric|nullable',  
        ]);
    }

    public function saveFeature(Request $request){
        $validator = $this->featureValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $feature = new Feature();
        $feature->name = $request->name??null;
        $feature->epic_id = $request->epic_id??null;
        $feature->dependency_id = $request->dependency_id??null;
        $feature->start_date = $request->start_date??null;
        $feature->end_date = $request->end_date??null;
        $feature->hours_planned = $request->hours_planned??null;
        $feature->confidence_percentage = $request->confidence_percentage??null;
        $feature->estimate_cost = $request->estimate_cost??null;
        $feature->estimate_income = $request->estimate_income??null;
        $feature->billable = $request->billable??0;
        $feature->sprint_id = $request->sprint_id??null;
        $feature->status_id = $request->status_id??null;
        $feature->save();
        
        $parameters = [
            'feature' => Feature::with('user_stories','user_stories.tasks','user_stories.tasks.timelines','user_stories.tasks.consultant')->where('id',$feature->id)->first()
        ];

        return response()->json($parameters);
    }

    public function updateFeature(Request $request){
        $validator = $this->featureValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        
        $feature = Feature::find($request->id);
        $feature->name = $request->name??null;
        $feature->epic_id = $request->epic_id??null;
        $feature->dependency_id = $request->dependency_id??null;
        $feature->start_date = $request->start_date??null;
        $feature->end_date = $request->end_date??null;
        $feature->hours_planned = $request->hours_planned??null;
        $feature->confidence_percentage = $request->confidence_percentage??null;
        $feature->estimate_cost = $request->estimate_cost??null;
        $feature->estimate_income = $request->estimate_income??null;
        $feature->billable = $request->billable??0;
        $feature->sprint_id = $request->sprint_id??null;
        $feature->status_id = $request->status_id??null;
        $feature->save();
        
        $parameters = [
            'feature' => Feature::with('user_stories','user_stories.tasks','user_stories.tasks.timelines','user_stories.tasks.consultant')->where('id',$feature->id)->first()
        ];

        return response()->json($parameters);
    }

    public function deleteFeature(Request $request){
        Feature::destroy($request->feature_id);
        
        return response()->json(['message'=>'Feature successfully deleted.']);
    }
    
    public function moveFeature(Request $request, $id)
    {
        $feature = Feature::findOrFail($id);
        $feature->epic_id = $request->epic_id;
        $feature->save();

        return response()->json([
            'message' => 'Feature updated successfully!',
            'feature' => $feature
        ]);
    }

}
