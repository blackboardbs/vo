<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectTermsRequest;
use App\Http\Requests\UpdateProjectTermsRequest;
use App\Models\ProjectTerms;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProjectTermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $project_terms = ProjectTerms::sortable()->paginate($item);

        if ($request->has('q') && $request->input('q') != '') {
            $project_terms = ProjectTerms::where('terms_version', 'like', "%" . $request->input('q') . "%")
                ->orWhere('start_date', 'like', "%" . $request->input('q') . "%")
                ->orWhere('start_date', 'like', "%" . $request->input('q') . "%")
                ->orWhere('end_date', 'like', "%" . $request->input('q') . "%")
                ->sortable(['terms_version' => 'asc'])->paginate($item);
        }

        $parameters = [
            'project_terms' => $project_terms,
        ];

        return View('master_data.project_terms.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ProjectTerms::orderBy('terms_version')->where('terms_version', '!=', null)->where('terms_version', '!=', '')->get();

        $parameters = [
            'autocomplete_elements' => $autocomplete_elements
        ];

        return View('master_data.project_terms.create')->with($parameters);
    }

    public function store(StoreProjectTermsRequest $request): RedirectResponse
    {
        $project_terms = new ProjectTerms();
        $project_terms->terms_version = $request->input('terms_version');
        $project_terms->start_date = $request->input('start_date');
        $project_terms->end_date = $request->input('end_date');
        $project_terms->exp_travel = $request->input('exp_travel');
        $project_terms->exp_parking = $request->input('exp_parking');
        $project_terms->exp_car_rental = $request->input('car_rental');
        $project_terms->exp_flights = $request->input('exp_flights');
        $project_terms->exp_other = $request->input('exp_other');
        $project_terms->exp_accommodation = $request->input('exp_accommodation');
        $project_terms->exp_out_of_town = $request->input('exp_out_of_town');
        $project_terms->exp_toll = $request->input('exp_toll');
        $project_terms->exp_data = $request->input('exp_data');
        $project_terms->exp_hours_of_work = $request->input('exp_hours_of_work');
        $project_terms->save();

        return redirect(route('project_terms.index'))->with('flash_success', 'Master Data Project Terms captured successfully');
    }

    public function show($project_terms_id): View
    {
        $parameter = [
            'project_terms' => $project_terms = ProjectTerms::find($project_terms_id),
        ];

        return view('master_data.project_terms.show')->with($parameter);
    }

    public function edit($project_terms_id): View
    {
        $autocomplete_elements = ProjectTerms::orderBy('terms_version')->where('terms_version', '!=', null)->where('terms_version', '!=', '')->get();

        $parameters = [
            'project_terms' => ProjectTerms::find($project_terms_id),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return view('master_data.project_terms.edit')->with($parameters);
    }

    public function update(UpdateProjectTermsRequest $request, $project_terms_id): RedirectResponse
    {
        $project_terms = ProjectTerms::find($project_terms_id);
        $project_terms->terms_version = $request->input('terms_version');
        $project_terms->start_date = $request->input('start_date');
        $project_terms->end_date = $request->input('end_date');
        $project_terms->exp_travel = $request->input('exp_travel');
        $project_terms->exp_parking = $request->input('exp_parking');
        $project_terms->exp_car_rental = $request->input('car_rental');
        $project_terms->exp_flights = $request->input('exp_flights');
        $project_terms->exp_other = $request->input('exp_other');
        $project_terms->exp_accommodation = $request->input('exp_accommodation');
        $project_terms->exp_out_of_town = $request->input('exp_out_of_town');
        $project_terms->exp_toll = $request->input('exp_toll');
        $project_terms->exp_data = $request->input('exp_data');
        $project_terms->exp_hours_of_work = $request->input('exp_hours_of_work');
        $project_terms->save();

        return redirect(route('project_terms.index'))->with('flash_success', 'Master Data Project Terms updated successfully');
    }

    public function destroy($project_terms_id): RedirectResponse
    {
        ProjectTerms::destroy($project_terms_id);

        return redirect(route('project_terms.index'))->with('flash_success', 'Master Data Project Terms deleted successfully');
    }
}
