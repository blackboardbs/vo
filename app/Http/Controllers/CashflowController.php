<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Customer;
use App\Dashboard\Cashflow;
use App\Dashboard\Commission;
use App\Dashboard\HelperFunctions;
use App\Models\Project;
use App\Models\Team;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use App\Models\Cashflow as CashflowView;
use App\Models\VendorInvoice;
use App\Models\CustomerInvoice;
use App\Models\Assignment;
use App\Models\CumalativeFlowView;

class CashflowController extends Controller
{
    private $invoicedTimesheets;

    private $commission;

    private $helperFunction;

    public function __construct()
    {
        $this->middleware('auth');
        $this->invoicedTimesheets = new Cashflow();
        $this->commission = new Commission();
        $this->helperFunction = new HelperFunctions();
    }

    public function index(Request $request): View|BinaryFileResponse
    { 
        $invoiced = $this->invoicedTimesheets->invoiced($request, 15);
        $vendor_invoiced = $this->invoicedTimesheets->invoiced($request, 15, true, 'vendor_invoiced');
        $not_invoiced = $this->invoicedTimesheets->not_invoiced($request, 15);
        $vendor_not_invoiced = $this->invoicedTimesheets->not_invoiced($request, 15, true, 'vendor_not_invoiced');
        $expense_tracking = $this->invoicedTimesheets->expense_tracking($request, 15);
        $planned_expenses = $this->invoicedTimesheets->planned_expenses($request, 15);
        $comm = new Commission();
        $comm1 = $comm->commission($request, 10, now()->subMonths(1)->year.((now()->subMonths(1)->month < 10) ? '0'.now()->subMonths(1)->month : now()->subMonths(1)->month), true);
        $comm2 = $comm->commission($request, 10, now()->subMonths(2)->year.((now()->subMonths(2)->month < 10) ? '0'.now()->subMonths(2)->month : now()->subMonths(2)->month), true);
        $year_months = Timesheet::select('year', 'month')->whereBetween('year', [now()->subYears(2)->year, now()->year])->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $yearMonths = []; 
        foreach ($year_months as $year_m) {
            $yearMonths[$year_m->year.(($year_m->month < 10) ? '0'.$year_m->month : $year_m->month)] = $year_m->year.(($year_m->month < 10) ? '0'.$year_m->month : $year_m->month);
        }

        $cashflowData = CashflowView::all(); 
        $cumalativeFlow = CumalativeFlowView::all();

        //Customer Invoiced 
        $customer_invoiced = $cashflowData->where('table_id', 1);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($customer_invoiced as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $customer_invoices_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Customer not Invoiced
        $customer_not_invoiced = $cashflowData->where('table_id', 2);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($customer_not_invoiced as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $customer_not_invoiced_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Other Income
        $other_income = $cashflowData->where('table_id', 3);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($other_income as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $other_income_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Assignment Income
        $assignment_income = $cashflowData->where('table_id', 4);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($assignment_income as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $assignment_income_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Pipeline Income
        $pipeline_income = $cashflowData->where('table_id', 5);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($pipeline_income as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $pipeline_income_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        $total_income_total = $customer_invoices_total['total'] + $customer_not_invoiced_total['total'] + $other_income_total['total'] + $assignment_income_total['total'] + $pipeline_income_total['total'];
        $total_income_overdue = $customer_invoices_total['overdue'] + $customer_not_invoiced_total['overdue'] + $other_income_total['overdue'] + $assignment_income_total['overdue'] + $pipeline_income_total['overdue'];
        $total_income_5 = $customer_invoices_total['5'] + $customer_not_invoiced_total['5'] + $other_income_total['5'] + $assignment_income_total['5'] + $pipeline_income_total['5'];
        $total_income_10 = $customer_invoices_total['10'] + $customer_not_invoiced_total['10'] + $other_income_total['10'] + $assignment_income_total['10'] + $pipeline_income_total['10'];
        $total_income_20 = $customer_invoices_total['20'] + $customer_not_invoiced_total['20'] + $other_income_total['20'] + $assignment_income_total['20'] + $pipeline_income_total['20'];
        $total_income_30 = $customer_invoices_total['30'] + $customer_not_invoiced_total['30'] + $other_income_total['30'] + $assignment_income_total['30'] + $pipeline_income_total['30'];
        $total_income_60 = $customer_invoices_total['60'] + $customer_not_invoiced_total['60'] + $other_income_total['60'] + $assignment_income_total['60'] + $pipeline_income_total['60'];
        $total_income_90 = $customer_invoices_total['90'] + $customer_not_invoiced_total['90'] + $other_income_total['90'] + $assignment_income_total['90'] + $pipeline_income_total['90'];
        $total_income_120 = $customer_invoices_total['120'] + $customer_not_invoiced_total['120'] + $other_income_total['120'] + $assignment_income_total['120'] + $pipeline_income_total['120'];
        $total_income_150 = $customer_invoices_total['150'] + $customer_not_invoiced_total['150'] + $other_income_total['150'] + $assignment_income_total['150'] + $pipeline_income_total['150'];

        //Vendor Invoiced
        $vendor_invoiced = $cashflowData->where('table_id', 6);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($vendor_invoiced as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $vendor_invoiced_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Vendor not Invoiced
        $vendor_not_invoiced = $cashflowData->where('table_id', 7);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($vendor_not_invoiced as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $vendor_not_invoiced_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];
        
        //Assignment Expense
        $assignment_expense = $cashflowData->where('table_id', 9);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($assignment_expense as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $assignment_expense_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Other Expense
        $other_expense = $cashflowData->where('table_id', 8);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($other_expense as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $other_expense_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Expense Tracking and Employee Claims
        $expense_tracking_claims = $cashflowData->where('table_id', 10);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($expense_tracking_claims as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $expense_tracking_claims_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        //Pipeline Expenses
        $pipeline_expenses = $cashflowData->where('table_id', 11);
        $total = 0;
        $overdue = 0;
        $total_5 = 0;
        $total_10 = 0;
        $total_20 = 0;
        $total_30 = 0;
        $total_60 = 0;
        $total_90 = 0;
        $total_120 = 0;
        $total_150 = 0;
        foreach($pipeline_expenses as $invoice){
            $total += $invoice->total;
            $overdue += $invoice->overdue;
            $total_5 += $invoice[5];
            $total_10 += $invoice[10];
            $total_20 += $invoice[20];
            $total_30 += $invoice[30];
            $total_60 += $invoice[60];
            $total_90 += $invoice[90];
            $total_120 += $invoice[120];
            $total_150 += $invoice[150];
        }
        $pipeline_expenses_total = ['total' => $total, 'overdue' => $overdue, '5' => $total_5, '10' => $total_10, '20' => $total_20, '30' => $total_30, '60' => $total_60, '90' => $total_90, '120' => $total_120, '150' => $total_150];

        $total_expense_total = $vendor_invoiced_total['total'] + $vendor_not_invoiced_total['total'] + $other_expense_total['total'] + $assignment_expense_total['total'] + $pipeline_expenses_total['total'];
        $total_expense_overdue = $vendor_invoiced_total['overdue'] + $vendor_not_invoiced_total['overdue'] + $other_expense_total['overdue'] + $assignment_expense_total['overdue'] + $pipeline_expenses_total['overdue'];
        $total_expense_5 = $vendor_invoiced_total['5'] + $vendor_not_invoiced_total['5'] + $other_expense_total['5'] + $assignment_expense_total['5'] + $pipeline_expenses_total['5'];
        $total_expense_10 = $vendor_invoiced_total['10'] + $vendor_not_invoiced_total['10'] + $other_expense_total['10'] + $assignment_expense_total['10'] + $pipeline_expenses_total['10'];
        $total_expense_20 = $vendor_invoiced_total['20'] + $vendor_not_invoiced_total['20'] + $other_expense_total['20'] + $assignment_expense_total['20'] + $pipeline_expenses_total['20'];
        $total_expense_30 = $vendor_invoiced_total['30'] + $vendor_not_invoiced_total['30'] + $other_expense_total['30'] + $assignment_expense_total['30'] + $pipeline_expenses_total['30'];
        $total_expense_60 = $vendor_invoiced_total['60'] + $vendor_not_invoiced_total['60'] + $other_expense_total['60'] + $assignment_expense_total['60'] + $pipeline_expenses_total['60'];
        $total_expense_90 = $vendor_invoiced_total['90'] + $vendor_not_invoiced_total['90'] + $other_expense_total['90'] + $assignment_expense_total['90'] + $pipeline_expenses_total['90'];
        $total_expense_120 = $vendor_invoiced_total['120'] + $vendor_not_invoiced_total['120'] + $other_expense_total['120'] + $assignment_expense_total['120'] + $pipeline_expenses_total['120'];
        $total_expense_150 = $vendor_invoiced_total['150'] + $vendor_not_invoiced_total['150'] + $other_expense_total['150'] + $assignment_expense_total['150'] + $pipeline_expenses_total['150'];

        $total_total = $total_income_total - $total_expense_total;
        $overdue_total = $total_income_overdue - $total_expense_overdue;
        $total_5 = $total_income_5 - $total_expense_5;
        $total_10 = $total_income_10 - $total_expense_10;
        $total_20 = $total_income_20 - $total_expense_20;
        $total_30 = $total_income_30 - $total_expense_30;
        $total_60 = $total_income_60 - $total_expense_60;
        $total_90 = $total_income_90 - $total_expense_90;
        $total_120 = $total_income_120 - $total_expense_120;
        $total_150 = $total_income_150 - $total_expense_150;

        $cumalative_flow = $cumalativeFlow->where('table_id', 99);
        // dd($cumalative_flow);
        $flow_total = $cumalative_flow[0]->total??0;
        $flow_overdue = $cumalative_flow[0]->overdue??0;
        $flow_5 = $cumalative_flow[0][5]??0;
        $flow_10 = $cumalative_flow[0][10]??0;
        $flow_20 = $cumalative_flow[0][20]??0;
        $flow_30 = $cumalative_flow[0][30]??0;
        $flow_60 = $cumalative_flow[0][60]??0;
        $flow_90 = $cumalative_flow[0][90]??0;
        $flow_120 = $cumalative_flow[0][120]??0;
        $flow_150 = $cumalative_flow[0][150]??0;
        
        $cumalative_flow_total = ['total' => $flow_total, 'overdue' => $flow_overdue, '5' => $flow_5, '10' => $flow_10, '20' => $flow_20, '30' => $flow_30, '60' => $flow_60, '90' => $flow_90, '120' => $flow_120, '150' => $flow_150];

        $parameters = [
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'yearmonth_dropdown' => $yearMonths,
            'invoiced' => $invoiced,
            'total_5_invoiced' => 0,
            'total_10_invoiced' => 0,
            'total_20_invoiced' => 0,
            'total_30_invoiced' => 0,
            'total_30_plus_invoiced' => 0,
            'vendor_total_5_invoiced' => 0,
            'vendor_total_10_invoiced' => 0,
            'vendor_total_20_invoiced' => 0,
            'vendor_total_30_invoiced' => 0,
            'vendor_total_30_plus_invoiced' => 0,
            'vendor_invoiced' => $vendor_invoiced,
            'total_vendor_invoiced' => 0,
            'not_invoiced' => $not_invoiced,
            'total_expenses' => 0,
            'total_labour' => 0,
            'total' => 0,
            'vendor_not_invoiced' => $vendor_not_invoiced,
            'vendor_total_expenses' => 0,
            'vendor_total_labour' => 0,
            'vendor_total' => 0,
            'expense_tracking' => $expense_tracking,
            'total_claim_expense' => 0,
            'total_bill_expense' => 0,
            'planned_expenses' => $planned_expenses,
            'planned_expense_5' => 0,
            'planned_expense_10' => 0,
            'planned_expense_20' => 0,
            'planned_expense_30' => 0,
            'planned_expense_30_plus' => 0,
            'commission_this_month' => $this->commission_total($comm1),
            'commission_last_month' => $this->commission_total($comm2),
            'cashflowData' => $cashflowData,
            'customer_invoiced' => $customer_invoiced,
            'customer_not_invoiced' => $customer_not_invoiced,
            'total_income_total' => $total_income_total,
            'total_income_overdue' => $total_income_overdue,
            'total_income_5' => $total_income_5,
            'total_income_10' => $total_income_10,
            'total_income_20' => $total_income_20,
            'total_income_30' => $total_income_30,
            'total_income_60' => $total_income_60,
            'total_income_90' => $total_income_90,
            'total_income_120' => $total_income_120,
            'total_income_150' => $total_income_150,
            'vendor_invoiced' => $vendor_invoiced,
            'vendor_not_invoiced' => $vendor_not_invoiced,
            'total_expense_total' => $total_expense_total,
            'total_expense_overdue' => $total_expense_overdue,
            'total_expense_5' => $total_expense_5,
            'total_expense_10' => $total_expense_10,
            'total_expense_20' => $total_expense_20,
            'total_expense_30' => $total_expense_30,
            'total_expense_60' => $total_expense_60,
            'total_expense_90' => $total_expense_90,
            'total_expense_120' => $total_expense_120,
            'total_expense_150' => $total_expense_150,
            'customer_invoices_total' => $customer_invoices_total,
            'customer_not_invoiced_total' => $customer_not_invoiced_total,
            'vendor_invoiced_total' => $vendor_invoiced_total,
            'vendor_not_invoiced_total' => $vendor_not_invoiced_total,
            'other_income' => $other_income,
            'other_income_total' => $other_income_total,
            'assignment_income_total' => $assignment_income_total,
            'assignment_income' => $assignment_income,
            'pipeline_income' => $pipeline_income,
            'pipeline_income_total' => $pipeline_income_total,
            'assignment_expense' => $assignment_expense,
            'assignment_expense_total' => $assignment_expense_total,
            'other_expense' => $other_expense,
            'other_expense_total' => $other_expense_total,
            'expense_tracking_claims' => $expense_tracking_claims,
            'expense_tracking_claims_total' => $expense_tracking_claims_total,
            'pipeline_expenses' => $pipeline_expenses,
            'pipeline_expenses_total' => $pipeline_expenses_total,
            'total_total' => $total_total,
            'overdue_total' => $overdue_total,
            'total_5' => $total_5,
            'total_10' => $total_10,
            'total_20' => $total_20,
            'total_30' => $total_30,
            'total_60' => $total_60,
            'total_90' => $total_90,
            'total_120' => $total_120,
            'total_150' => $total_150,
            'cumalative_flow_total' => $cumalative_flow_total
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'cashflow');      

        return view('reports.cashflow')->with($parameters);
    }

    private function calculate_totals($invoice)
    {
        $total_expense = 0;
        $total_labour = 0;
        $total = 0;
        foreach ($invoice as $inv) {
            $total_expense += $inv['expenses'];
            $total_labour += $inv['amount_no_expense'];
            $total += $inv['amount'];
        }

        return [
            'total_expenses' => $total_expense,
            'total_labour' => $total_labour,
            'total' => $total,
        ];
    }

    private function commission_total($commission)
    {
        $total_comm = 0;
        foreach (array_slice($commission, 1, -1) as $commission) {
            foreach ($commission as $comm) {
                $total_comm += $comm;
            }
        }

        return $total_comm;
    }
}
