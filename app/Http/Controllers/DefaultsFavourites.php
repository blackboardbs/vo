<?php

namespace App\Http\Controllers;

use App\Models\Favourite;
use App\Http\Requests\StoreDefaultFavoritesRequest;
use App\Http\Requests\UpdateDefaultFavoritesRequest;
use App\Models\UserFavourite;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class DefaultsFavourites extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = ($request->r) ? $request->r : 15;
        $favourites = UserFavourite::with(['favourites:id,module_name', 'role:id,display_name'])->whereNull('user_id');
        
        if($request->has('q') && $request->q != ''){
            $favourites = $favourites->whereHas('favourites',function ($q) use ($request){
                $q->where('module_name','like','%'.$request->q.'%');
            });
        }


        $favourites = $favourites->paginate($item);
        $parameters = [
            'favourites' => $favourites,
        ];

        return view('defaults.index')->with($parameters);
    }

    public function create(): View
    {
        $parameters = [
            'favourites' => Favourite::where('status_id', '=', 1)->pluck('module_name', 'id'),
            'roles' => Role::pluck('display_name', 'id'),
        ];

        return view('defaults.create')->with($parameters);
    }

    public function store(StoreDefaultFavoritesRequest $request): RedirectResponse
    {
        $defaults = new UserFavourite();
        $defaults->user_id = null;
        $defaults->favourite_id = $request->module_name;
        $defaults->role_id = $request->role_id;
        $defaults->save();

        return redirect()->route('default_favs.show', $defaults)->with('flash_success', 'Default added successfully');
    }

    public function user_store(Request $request): RedirectResponse
    {
        $user_id = Auth::id();
        if (isset($request->favourites_id)) {
            UserFavourite::where('user_id', '=', $user_id)->delete();
            foreach ($request->favourites_id as $favourite) {
                $defaults = new UserFavourite();
                $defaults->user_id = $user_id;
                $defaults->favourite_id = $favourite;
                $defaults->role_id = $request->role_id;
                $defaults->save();
            }

            return redirect()->route('settings')->with('flash_success', 'Your favourite has been added');
        } else {
            return redirect()->back()->with('flash_danger', 'You need to check at least one favourite');
        }
    }

    public function show($id): View
    {
        $defaults = UserFavourite::find($id);

        return view('defaults.show')->with(['favourite' => $defaults]);
    }

    public function edit($id): View
    {
        $parameters = [
            'default' => UserFavourite::find($id),
            'favourites' => Favourite::where('status_id', '=', 1)->pluck('module_name', 'id'),
            'roles' => Role::pluck('display_name', 'id'),
        ];

        return view('defaults.edit')->with($parameters);
    }

    public function update(UpdateDefaultFavoritesRequest $request, $id): RedirectResponse
    {
        $defaults = UserFavourite::find($id);
        $defaults->favourite_id = $request->module_name;
        $defaults->role_id = $request->role_id;
        $defaults->save();

        return redirect()->route('default_favs.show', $defaults)->with('flash_success', 'Default updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        UserFavourite::destroy($id);

        return redirect()->route('default_favs.index')->with('flash_success', 'Default favourite was suspended successfully');
    }
}
