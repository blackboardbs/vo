<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Cv;
use App\Models\CvSubmit;
use App\Dashboard\Anniversaries;
use App\Dashboard\AssignmentOutHours;
use App\Dashboard\Assignments;
use App\Dashboard\Cashflow;
use App\Dashboard\DebtorsAgeing;
use App\Dashboard\Expenses;
use App\Dashboard\HelperFunctions;
use App\Dashboard\Logins;
use App\Dashboard\MonthlyConsulting;
use App\Dashboard\TimesheetCountChart;
use App\Dashboard\TopComponents;
use App\Dashboard\Utilization;
use App\Dashboard\WeeklyUtilization;
use App\Models\JobSpec;
use App\Models\LandingPageDashboard;
use App\Models\Leave;
use App\Models\MessageBoard;
use App\Models\Scouting;
use App\Models\SetupStep;
use App\Models\User;
use App\Scouting\Dashboard;
use App\Services\StorageQuota;
use App\Utils;
use ErrorException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class LandingPageDashboardController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->roles->isEmpty() && (auth()->id() == 1)){
            auth()->user()->assignRole(1);
        }

        $helperFunctions = $this->helperFunction();
        $user_prefs = $helperFunctions->user_dashboard($request);
        $topComponents = new TopComponents();
        $assignmentOutHours = new AssignmentOutHours();
        $logIns = new Logins();
        $timesheetCountChart = new TimesheetCountChart();
        $dashExpenses = new Expenses();
        $anniversaries = new Anniversaries();
        $weeklyUtilization = new WeeklyUtilization();
        $utilization = new Utilization();
        $debtors = new DebtorsAgeing();
        $monthlyConsulting = new MonthlyConsulting();
        $utils = new Utils();
        $parameters = [];

        $parameters['user_prefs'] = $user_prefs;
        $parameters['configs'] = Config::first();
        $scouting = new Dashboard();

        if (in_array(str_replace(' ', '_', strtoupper('Latest 10 Candidates')), $user_prefs)) {
            $parameters['latest_10_candidates'] = $scouting->latestTen(Cv::class, ['user:id,first_name,last_name', 'role:id,name']);
        }

        if (in_array(str_replace(' ', '_', strtoupper('Latest 10 Job Specs')), $user_prefs)) {
            $parameters['latest_10_job_specs'] = $scouting->latestTen(JobSpec::class, ['positionrole:id,name', 'customer:id,customer_name']);
        }

        if (in_array(str_replace(' ', '_', strtoupper('Potential Commission Earned').'_1'), $user_prefs)) {
            $parameters['potential_commission_earned'] = $scouting->potentialCommisionEarned();
        }

        if (in_array(str_replace(' ', '_', strtoupper('Number Of Open Job Specs').'_1'), $user_prefs)) {
            $parameters['number_of_open_job_spec'] = $scouting->openJobsHeaderComponent();
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of new job specs').'_1'), $user_prefs)) {
            $parameters['number_of_new_job_specs'] = $scouting->daysDifferenceJobHeaderComponent(JobSpec::class, 'created_at');
        }

        if (in_array(str_replace(' ', '_', strtoupper('jobs submitted').'_1'), $user_prefs)) {
            $parameters['jobs_submitted'] = $scouting->openJobsHeaderComponent();
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of new candidates').'_1'), $user_prefs)) {
            $parameters['number_of_new_candidates'] = $scouting->openJobsHeaderComponent();
        }

        if (in_array(str_replace(' ', '_', strtoupper('new scouts added this week').'_1'), $user_prefs)) {
            $parameters['new_scouts_added_this_week'] = $scouting->addedThisWeek(Scouting::class);
        }

        if (in_array(str_replace(' ', '_', strtoupper('new cvs added this month_1')), $user_prefs)) {
            $parameters['new_cvs_added_this_months'] = $scouting->addedThisMonth(Cv::class);
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of active candidates_1')), $user_prefs)) {
            $parameters['number_of_active_candidates'] = $scouting->activeCandidatesHeaderComponent();
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of candidates updated_1')), $user_prefs)) {
            $parameters['number_of_candidates_updated'] = $scouting->daysDifferenceJobHeaderComponent(Cv::class, 'updated_at');
        }

        if (in_array(str_replace(' ', '_', strtoupper('new scouts added this month_1')), $user_prefs)) {
            $parameters['new_scouts_added_this_months'] = $scouting->addedThisMonth(Scouting::class);
        }

        if (in_array(str_replace(' ', '_', strtoupper('Jobs Specs To Close_1')), $user_prefs)) {
            $parameters['job_specs_to_close'] = $scouting->daysDifferenceJobHeaderComponent(JobSpec::class, 'applicaiton_closing_date');
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of candidates submitted_1')), $user_prefs)) {
            $parameters['number_of_candidates_submitted'] = $scouting->submitedCVHeaderComponent();
        }

        if (in_array(str_replace(' ', '_', strtoupper('CVs Submitted This Month_1')), $user_prefs)) {
            $parameters['cv_submitted_this_month'] = $scouting->addedThisMonth(CvSubmit::class);
        }

        if (in_array(str_replace(' ', '_', strtoupper('new cvs added this week_1')), $user_prefs)) {
            $parameters['new_cvs_added_this_week'] = $scouting->addedThisWeek(Cv::class);
        }

        if (in_array(str_replace(' ', '_', strtoupper('Expired Users_1')), $user_prefs)) {
            $parameters['expired_users'] = User::where('expiry_date', '<', now()->toDateString())->count();
        }

        if (in_array(str_replace(' ', '_', strtoupper('Failed Login Attempts_1')), $user_prefs)) {
            $parameters['failed_login_attempts'] = $topComponents->logins_top(0)->get()->count();
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of logins_1')), $user_prefs)) {
            $parameters['logins'] = $topComponents->logins_top(1)->get()->count();
        }

        if (in_array(str_replace(' ', '_', strtoupper('Usage: Timesheets Count_1')), $user_prefs)) {
            $parameters['timesheets_count'] = $topComponents->timesheets_count();
        }

        if (in_array(str_replace(' ', '_', strtoupper('Users To Expire In 30 Days_1')), $user_prefs)) {
            $parameters['users_to_expire'] = $this->expiresInLast30Days()->count();
        }

        //ASSIGNMENTS OUTSTANDING HOURS

        if (in_array(str_replace(' ', '_', strtoupper('assignment outstanding hours_1')), $user_prefs)) {
            $total_time = $assignmentOutHours->totalTime();
            $total_ass_hours = $assignmentOutHours->totalAssignmentHours();
            $parameters['assignment_out_hours'] = $total_ass_hours - $total_time;
        }

        if (in_array(str_replace(' ', '_', strtoupper('hours outstanding on assignments')), $user_prefs)) {
            $time_hours = $assignmentOutHours->timeHours();
            $parameters['assignment_project'] = $assignmentOutHours->assignments();
            $parameters['hours_outstanding_ass'] = $time_hours;
        }

        //END ASSIGNMENTS OUTSTANDING HOURS
        //UTILIZATION
        try {
            $parameters['total_income'] = $utilization->resource_utilization($request)['total_income'][0];
            $parameters['total_cost'] = $utilization->resource_utilization($request)['total_cost'][0];
        } catch (\Throwable $e) {
            if (\config('app.env') !== 'testing')
            {
                return $utilization->resource_utilization($request);
            }
        }

        //NUMBER OF FAILED LOGIN
        $logs = $helperFunctions->chartLogins();
        $parameters['logs'] = $logs;
        if (in_array(str_replace(' ', '_', strtoupper('Number Of Logins')), $user_prefs)) {
            $login_success = $logIns->logins(1);
            $parameters['login_success'] = $login_success;
        }

        if (in_array(str_replace(' ', '_', strtoupper('number of failed logins')), $user_prefs)) {
            $login_fail = $logIns->logins(0);
            $parameters['login_fail'] = $login_fail;
        }

        //END NUMBER OF FAILED LOGIN

        //USAGE: TIMESHEETS COUNT
        $last_year = $timesheetCountChart->timesheet_count_charts()[0];
        $this_year = $timesheetCountChart->timesheet_count_charts()[1];
        if (in_array(str_replace(' ', '_', strtoupper('Usage: Timesheet Count')), $user_prefs)) {
            $timesheet_last_val = $timesheetCountChart->timesheet_count_charts()[4];
            $timesheet_this_val = $timesheetCountChart->timesheet_count_charts()[5];
            $months = $timesheetCountChart->timesheet_count_charts()[6];
            $parameters['months'] = $months;
            $parameters['timesheets_past_year'] = $timesheet_last_val;
            $parameters['timesheets_this_year'] = $timesheet_this_val;
            $parameters['last_year'] = $last_year;
            $parameters['this_year'] = $this_year;
            //$parameters['timesheets_this_year'] = $timesheet_this_val;
        }

        if (in_array(str_replace(' ', '_', strtoupper('Failed Login Attempts Over 30 Days Trend')), $user_prefs)) {
            $failed_30_login = $topComponents->logins_top(0)->orderBy('date', 'desc')->paginate(5, ['*'], 'failed-logins');
            $parameters['failed_30_login'] = $failed_30_login;
        }

        if (in_array(str_replace(' ', '_', strtoupper('consultant expenses')), $user_prefs)) {
            $dash_expenses = $dashExpenses->dash_expenses();
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection = collect($dash_expenses);
            $perPage = 5;
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $expenses = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
            $expenses->setPath($request->url());
            $parameters['expenses'] = $expenses;
        }

        if (in_array(str_replace(' ', '_', strtoupper('important dates')), $user_prefs)) {
            $parameters['anniversaries'] = $anniversaries->anniversary();
        }

        if (in_array(str_replace(' ', '_', strtoupper('notice board')), $user_prefs)) {
            $parameters['notices'] = MessageBoard::where('status_id', 1)->latest()->paginate(10, ['*'], 'notice-board');
        }

        if (in_array(str_replace(' ', '_', strtoupper('Income Projects Weekly Utilization')), $user_prefs)) {
            $weekly_income_utilization = $weeklyUtilization->weekly_utilization(1, 1, $request);
            $parameters['weekly_income_utilization'] = $weekly_income_utilization;
        }

        if (in_array(str_replace(' ', '_', strtoupper('Cost Projects Weekly Utilization')), $user_prefs)) {
            $parameters['weekly_cost_utilization'] = $weeklyUtilization->weekly_utilization(0, 2, $request);
        }

        if (in_array(str_replace(' ', '_', strtoupper('cashflow')), $user_prefs)) {
            $cashflow = new Cashflow();

            $invoiced_5 = 0;
            $invoiced_30 = 0;
            $invoiced_30_plus = 0;

            foreach ($cashflow->invoiced($request, '*') as $invoiced) {
                $invoiced_5 += ($invoiced->five_days * $invoiced->rate);
                $invoiced_30 += (($invoiced->ten_days * $invoiced->rate) + ($invoiced->twenty_days * $invoiced->rate) + ($invoiced->thirty_days * $invoiced->rate));
                $invoiced_30_plus += ($invoiced->thirty_days_plus * $invoiced->rate);
            }

            $parameters['invoiced'] = [
                'five_days' => $invoiced_5,
                'thirty_days' => $invoiced_30,
                'thirty_plus_days' => $invoiced_30_plus,
            ];

            $not_invoiced = 0;

            foreach ($cashflow->not_invoiced($request, '*') as $invoice) {
                try {
                    $not_invoiced += ($invoice->hours * ($invoice->rate ? $invoice->rate : 0));
                } catch (ErrorException $e) {
                }
            }

            $parameters['not_invoiced'] = $not_invoiced;

            $vendor_invoiced_5 = 0;
            $vendor_invoiced_30 = 0;
            $vendor_invoiced_30_plus = 0;

            foreach ($cashflow->invoiced($request, '*', true) as $invoiced) {
                $vendor_invoiced_5 += ($invoiced->five_days * $invoiced->rate);
                $vendor_invoiced_30 += (($invoiced->ten_days * $invoiced->rate) + ($invoiced->twenty_days * $invoiced->rate) + ($invoiced->thirty_days * $invoiced->rate));
                $vendor_invoiced_30_plus += ($invoiced->thirty_days_plus * $invoiced->rate);
            }

            $parameters['vendor_invoiced'] = [
                'vendor_five_days' => $vendor_invoiced_5,
                'vendor_thirty_days' => $vendor_invoiced_30,
                'vendor_thirty_plus_days' => $vendor_invoiced_30_plus,
            ];

            $vendor_not_invoiced = 0;
            foreach ($cashflow->not_invoiced($request, '*', true) as $invoice) {
                $vendor_not_invoiced += ($invoice->hours * $invoice->rate);
            }

            $parameters['Vendor_not_invoiced'] = $vendor_not_invoiced;

            $expense_claim = 0;

            foreach ($cashflow->not_invoiced($request, '*') as $expense) {
                $expense_claim += (isset($expense->time_exp[0]) ? $expense->time_exp[0]->time_expense : 0);
            }

            $expense_tracking = 0;

            foreach ($cashflow->expense_tracking($request, '*') as $expense_track) {
                $expense_tracking += $expense_track->claim_expense;
            }

            $parameters['expense_claims'] = ($expense_claim + $expense_tracking);

            $five_days = 0;
            $thirty_days = 0;
            $thirty_days_plus = 0;
            foreach ($cashflow->planned_expenses($request, '*') as $planned) {
                $five_days += $planned->five_days;
                $thirty_days += ($planned->ten_days + $planned->twenty_days + $planned->thirty_days);
                $thirty_days_plus += $planned->thirty_days_plus;
            }

            $parameters['planned_expenses'] = [
                'five_days' => $five_days,
                'thirty_days' => $thirty_days,
                'thirty_days_plus' => $thirty_days_plus,
            ];

            /*$expense_claims = new PlannedIncome();
            $total_expenses_claim = 0;

            foreach ($expense_claims->expense_tracking() as $item)
                $total_expenses_claim += $item->amount;

            $parameters['total_invoiced'] = $cashflow->cashflow($request)['total_invoiced'][0];
            $parameters['total_not_invoiced'] = $cashflow->calculate_totals($cashflow->cashflow($request)["not_invoiced"]);
            $parameters['vendor_invoiced'] = $cashflow->cashflow($request)['total_vendor_invoiced'][0];
            $parameters['vendor_not_invoiced'] = $cashflow->calculate_totals($cashflow->cashflow($request)['vendor_not_invoiced']);
            $parameters['expense_claims'] = $total_expenses_claim;
            $parameters['planned_expenses_5'] = $cashflow->cashflow($request)["planned_expense"]["grand_total"]["total_5_days"];
            $parameters['planned_expenses_30'] = ($cashflow->cashflow($request)["planned_expense"]["grand_total"]["total_10_days"] + $cashflow->cashflow($request)["planned_expense"]["grand_total"]["total_20_days"] + $cashflow->cashflow($request)["planned_expense"]["grand_total"]["total_30_days"]);
            $parameters['planned_expenses_30p'] = $cashflow->cashflow($request)["planned_expense"]["grand_total"]["total_30_plus_days"];
            $parameters['total_planned_expenses'] = $cashflow->cashflow($request)["planned_expense"]["grand_total"]["grand_total_line"];*/
        }

        if (in_array(str_replace(' ', '_', strtoupper('Open Assignment Income')), $user_prefs)) {
            $parameters['open_income_assignments'] = $this->assignments()->sort_by_hours($request, 1);
        }

        if (in_array(str_replace(' ', '_', strtoupper('Open Assignment - Cost')), $user_prefs)) {
            $parameters['open_cost_assignments'] = $this->assignments()->sort_by_hours($request, 0);
        }

        if (in_array(str_replace(' ', '_', strtoupper('utilization income')), $user_prefs)) {
            try {
                $parameters['income_utilization'] = $utilization->resource_utilization($request)['income_utilization'];
                $parameters['weeks'] = $utilization->resource_utilization($request)['weeks'];
            } catch (\Throwable $e) {
                return $utilization->resource_utilization($request);
            }
        }

        //return $parameters["total_income"]["percentage_week"];

        if (in_array(str_replace(' ', '_', strtoupper('utilization Cost')), $user_prefs)) {
            try {
                $parameters['cost_utilization'] = $utilization->resource_utilization($request)['cost_utilization'];
                $parameters['weeks'] = $utilization->resource_utilization($request)['weeks'];
            } catch (\Throwable $e) {
                return $utilization->resource_utilization($request);
            }
        }

        if (in_array(str_replace(' ', '_', strtoupper('Users To Expire In 30 Days')), $user_prefs)) {
            $parameters['to_expire_30'] = User::whereBetween('expiry_date', [now()->addMonth()->toDateString(), now()->toDateString()])->paginate(5, ['*'], 'to-expire');
        }

        if (in_array(str_replace(' ', '_', strtoupper('leave not approved')), $user_prefs)) {
            $parameters['leave_not_approved'] = Leave::with(['resource'])->whereNull('leave_status')->orderBy('date_from', 'asc')->paginate(5, ['*'], 'not-approved');;
        }

        if (in_array(str_replace(' ', '_', strtoupper('debtor ageing')), $user_prefs)) {
            $parameters['debtor_ageing'] = $debtors->debtor_ageing($request);
            $parameters['grand_total_debtor'] = 0;
            $parameters['grand_total_30_debtor'] = 0;
            $parameters['grand_total_60_debtor'] = 0;
        }

        if (in_array(str_replace(' ', '_', strtoupper('creditors ageing')), $user_prefs)) {
            $parameters['creditor_ageing'] = $debtors->debtor_ageing($request, true);
            $parameters['grand_total_creditor'] = 0;
            $parameters['grand_total_30_creditor'] = 0;
            $parameters['grand_total_60_creditor'] = 0;
        }

        if (in_array(str_replace(' ', '_', strtoupper('planning and pipelines')), $user_prefs)) {
            $parameters['pipelines'] = $helperFunctions->pipelines??[];
        }

        if (in_array(str_replace(' ', '_', strtoupper('top 10 customers chart')), $user_prefs) || in_array(str_replace(' ', '_', strtoupper('top 10 customers table')), $user_prefs)) {
            $parameters['top_10_customers'] = $helperFunctions->get_top_customers(now(), $request);
        }

        if (in_array(str_replace(' ', '_', strtoupper('top 10 solutions table')), $user_prefs) || in_array(str_replace(' ', '_', strtoupper('top 10 solutions chart')), $user_prefs)) {
            $parameters['top_10_solutions'] = $helperFunctions->get_top_solutions(now(), 'system', $request);
        }

        if (in_array(str_replace(' ', '_', strtoupper('top 10 locations table')), $user_prefs) || in_array(str_replace(' ', '_', strtoupper('top 10 locations chart')), $user_prefs)) {
            $parameters['top_10_locations'] = $helperFunctions->get_top_solutions(now(), 'country_id', $request);
        }

        if (in_array(str_replace(' ', '_', strtoupper('monthly consulting')), $user_prefs)) {
            $parameters['monthly_consulting_1'] = $helperFunctions->get_monthly_consulting(now(), 0);
            $parameters['monthly_consulting_2'] = $helperFunctions->get_monthly_consulting(now(), 1);
            $parameters['monthly_consulting_3'] = $helperFunctions->get_monthly_consulting(now(), 2);
            $parameters['monthly_value_3'] = $monthlyConsulting->monthly_consulting()[0];
            $parameters['monthly_value_2'] = $monthlyConsulting->monthly_consulting()[1];
            $parameters['monthly_value_1'] = $monthlyConsulting->monthly_consulting()[2];
        }

        if (in_array(str_replace(' ', '_', strtoupper('year to date chart')), $user_prefs) || in_array(str_replace(' ', '_', strtoupper('year to date table')), $user_prefs)) {
            $parameters['year_to_date_1'] = $helperFunctions->get_year_to_date(0);
            $parameters['year_to_date_2'] = $helperFunctions->get_year_to_date(1);
            $parameters['year_to_date_3'] = $helperFunctions->get_year_to_date(2);
            $parameters['year_to_date_4'] = $helperFunctions->get_year_to_date(3);
        }

        if (in_array(str_replace(' ', '_', strtoupper('outstanding timesheets')), $user_prefs)) {
            $outstanding_timesheet = $utils->outstandingTimesheets(5);
            $parameters['outstanding_timesheets'] = array_filter(array_unique($outstanding_timesheet), 'strlen');
        }

        //Status 0 = Not Done, 1 = Started, 2 = Done
        $setup_steps = SetupStep::orderBy('id')->where('status_id', '!=', 2)->get();

        //dd($setup_steps);

        $parameters['setup_steps'] = $setup_steps;

        $parameters['configs_color'] = function ($min_quantity, $min_color, $between_color, $target_quantity, $target_color, $component) {
            if ($component <= $min_quantity) {
                return 'background-color: #'.$min_color;
            }
            if (($component > $min_quantity) && ($component < $target_quantity)) {
                return 'background-color: #'.$between_color;
            }

            return 'background-color: #'.$target_color;
        };

        return view('landing-page-dashboard.index')->with($parameters);
    }

    public function expired(): View
    {
        $parameters = [
            'expired_users' => User::where('expiry_date', '<', now()->toDateString())->sortable('first_name', 'asc')->paginate(15),
        ];

        return view('landing-page-dashboard.expired_users')->with($parameters);
    }

    public function to_expire(): View
    {
        $parameters = [
            'users_to_expire' => $this->expiresInLast30Days()->paginate(),
        ];

        return view('landing-page-dashboard.to_expire')->with($parameters);
    }

    private function expiresInLast30Days()
    {
        return User::whereBetween('expiry_date', [now()->toDateString(), now()->addMonth()->toDateString()]);
    }

    public function storePreferences(Request $request): RedirectResponse
    {
        $deactivate_curr_dashboard = LandingPageDashboard::where('user_id', '=', auth()->user()->id)->get();

        foreach ($deactivate_curr_dashboard as $dashboard) {
            $dashboard->status_id = 2;
            $dashboard->save();
        }

        $top_components = $request->lp_dashboard;

        foreach ($top_components as $component) {
            $lp_dashboard = new LandingPageDashboard;
            $lp_dashboard->user_id = auth()->user()->id;
            $lp_dashboard->component_level = 1;
            $lp_dashboard->component = str_replace(' ', '_', "$component");
            $lp_dashboard->status_id = $request->status_id;
            $lp_dashboard->dashboard_name = $request->dashboard_name;
            $lp_dashboard->save();
        }

        return redirect()->route('dashboard.index');
    }

    public function updateLandingPage(Request $request): RedirectResponse
    {
        if ($request->status_id == 1) {
            $lp_dashes = LandingPageDashboard::where('user_id', '=', auth()->user()->id)->where('status_id', '=', 1)->get();
            foreach ($lp_dashes as $dash) {
                $dash->status_id = 2;
                $dash->save();
            }
        }

        LandingPageDashboard::where('user_id', '=', auth()->user()->id)->where('dashboard_name', '=', $request->dash_name)->delete();

        $components = $request->lp_dashboard;
        foreach ($components as $component) {
            $lp_dashboard = new LandingPageDashboard;
            $lp_dashboard->user_id = auth()->user()->id;
            $lp_dashboard->component_level = 1;
            $lp_dashboard->component = str_replace(' ', '_', $component);
            $lp_dashboard->status_id = $request->status_id;
            $lp_dashboard->dashboard_name = $request->dashboard_name;
            $lp_dashboard->save();
        }

        return redirect()->route('dashboard.index');
    }

    public function destroy($dashboard_name): RedirectResponse
    {
        LandingPageDashboard::where('user_id', '=', auth()->user()->id)->where('dashboard_name', '=', $dashboard_name)->delete();

        return redirect()->route('reports.custom_dashboard')->with('flash_success', 'Your Dashboard has been successfully deleted');
    }

    private function helperFunction(): HelperFunctions
    {
        return new HelperFunctions();
    }

    private function assignments(): Assignments
    {
        return new Assignments();
    }
}
