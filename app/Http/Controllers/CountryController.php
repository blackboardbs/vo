<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Http\Requests\StoreCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $country = Country::sortable('name', 'asc')->paginate($item);

        $country = Country::with(['statusd:id,description'])->sortable('name', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $country = $country->where('status', $request->input('status_filter'));
            }else{
                $country = $country->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $country = $country->where('name', 'like', '%'.$request->input('q').'%');
        }

        $country = $country->paginate($item);

        $parameters = [
            'country' => $country,
        ];

        return View('master_data.country.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Country::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.country.create')->with($parameters);
    }

    public function store(StoreCountryRequest $request): RedirectResponse
    {
        $country = new Country();
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        $country->status = $request->input('status');
        $country->creator_id = auth()->id();
        $country->save();

        return redirect(route('master_country.index'))->with('flash_success', 'Master Country captured successfully');
    }

    public function show($countryid): View
    {
        $parameters = [
            'country' => Country::where('id', '=', $countryid)->get(),
        ];

        return View('master_data.country.show')->with($parameters);
    }

    public function edit($countryid): View
    {
        $autocomplete_elements = Country::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'country' => Country::where('id', '=', $countryid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.country.edit')->with($parameters);
    }

    public function update(UpdateCountryRequest $request, $countryid): RedirectResponse
    {
        $country = Country::find($countryid);
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        $country->status = $request->input('status');
        $country->creator_id = auth()->id();
        $country->save();

        return redirect(route('master_country.index'))->with('flash_success', 'Master Country saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // DB::table('country')->delete($id);
        //return response()->json(['success'=>"Product suspended successfully.", 'tr'=>'tr_'.$id]);
        // Country::destroy($id);
        $item = Country::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('master_country.index')->with('success', 'Master Data Country suspended successfully');
    }
}
