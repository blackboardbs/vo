<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScoutingRoleRequest;
use App\Http\GlobalForm;
use App\Models\ScoutingRole;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Status;

class ScoutingRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $roles = ScoutingRole::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $roles = $roles->where('status_id', $request->input('status_filter'));
            }else{
                $roles = $roles->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $roles = $roles->where('name', 'like', '%'.$request->input('q').'%');
        }

        $roles = $roles->paginate($item);

        return view('master_data.main_role.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        // $includeColumns = [
        //     'name',
        //     'status_id',
        // ];  

        // $parameters = GlobalForm::create('ScoutingRole', $includeColumns, 'Main Role');

        // $autocomplete_elements = ScoutingRole::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        // $parameters['autocomplete_elements'] = $autocomplete_elements;

        // return view('global_forms.create_double_column')->with($parameters);
        return view('master_data.main_role.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ScoutingRoleRequest $request, ScoutingRole $scoutingrole): RedirectResponse
    {
        // $scoutingRole = GlobalForm::store('ScoutingRole', $request);

        // return redirect(route('scoutingrole.index'))->with('flash_success', 'Main Role successfully created.');
        $this->dbValues($scoutingrole, $request);

        return redirect()->route('scoutingrole.index')->with('flash_success', 'Main Role stored successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(ScoutingRole $scoutingrole): View
    {
        // $includeColumns = [
        //     'name',
        //     'status_id',
        // ];

        // $parameters = GlobalForm::edit($id, 'ScoutingRole', $includeColumns, 'Main Role');

        // return view('global_forms.show_double_column')->with($parameters);
        return view('master_data.main_role.show')->with(['scoutingrole' => $scoutingrole]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ScoutingRole $scoutingrole): View
    {
        // $includeColumns = [
        //     'name',
        //     'status_id',
        // ];

        // $parameters = GlobalForm::edit($id, 'ScoutingRole', $includeColumns, 'Main Role');

        // $autocomplete_elements = ScoutingRole::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        // $parameters['autocomplete_elements'] = $autocomplete_elements;

        // return view('global_forms.edit_double_column')->with($parameters);
        return view('master_data.main_role.edit')->with([
            'scoutingrole' => $scoutingrole,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ScoutingRoleRequest $request, ScoutingRole $scoutingrole): RedirectResponse
    {
        // GlobalForm::update($id, 'ScoutingRole', $request);

        // return redirect(route('scoutingrole.index'))->with('flash_success', 'Main Role successfully updated.');
        $this->dbValues($scoutingrole, $request);

        return redirect()->route('scoutingrole.index')->with('flash_success', 'Main Role updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        // ScoutingRole::destroy($id);
        $item = ScoutingRole::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('scoutingrole.index'))->with('flash_success', 'Main Role successfully suspended.');
    }

    private function dbValues($scoutingrole, $request)
    {
        $scoutingrole->name = $request->name;
        $scoutingrole->status_id = $request->status_id;
        $scoutingrole->save();
    }
}
