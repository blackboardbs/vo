<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateIndustryExperienceRequest;
use App\Models\Industry;
use App\Models\IndustryExperience;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class IndustryExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $industry_experience = IndustryExperience::with(['resource', 'industry', 'statusd'])->sortable('res_id', 'asc')->paginate($item);

        if ($request->has('q') && $request->input('q') != '') {
            $industry_experience = IndustryExperience::with(['resource', 'industry', 'statusd'])
            ->whereHas('resource', function ($query) use ($request) {
                $query->where('emp_firstname', 'like', '%'.$request->q.'%');
                $query->orWhere('emp_lastname', 'like', '%'.$request->q.'%');
            })
                ->orWhereHas('industry', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->q.'%');
                })
                ->orWhereHas('statusd', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->q.'%');
                })
                ->sortable(['ind_id' => 'asc'])->paginate($item);
        }

        //return $industry_experience;

        $parameters = [
            'industry_experience' => $industry_experience,
        ];

        return View('industry_experience.index', $parameters);
    }

    public function create(Request $request): View
    {
        $cv_id = $request->input('cvid');
        $res_id = $request->input('res_id');
        $industries = IndustryExperience::orderBy('id')->where('res_id', '=', $res_id)->pluck('ind_id', 'id');
        $parameters = [
            'cv_id' => $cv_id,
            'res_id' => $res_id,
            'industries' => $industries,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'industry_dropdown' => Industry::orderBy('id')->pluck('description', 'id')->prepend('Industry', '0'),
            'resource_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', '=', $res_id)->orderBy('id')->pluck('full_name', 'id'),
        ];

        return View('industry_experience.create')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        $cv_id = $request->input('cv_id');
        $res_id = $request->input('res_id');

        $new_industry_ids = $request->input('ind_id');

        $old_industry_ids = IndustryExperience::where('res_id', '=', $res_id)->pluck('id')->toArray();

        //Add to database
        if ($request->has('ind_id')) {
            foreach ($new_industry_ids as $industry_id) {
                if (! in_array($industry_id, $old_industry_ids)) {
                    $industry_experience = new IndustryExperience();
                    $industry_experience->ind_id = (int) $industry_id;
                    $industry_experience->status = $request->input('status');
                    $industry_experience->res_id = $res_id;
                    $industry_experience->creator_id = auth()->id();
                    $industry_experience->save();
                }
            }
        }

        //Remove from database
        foreach ($old_industry_ids as $industry_id) {
            if (! in_array($industry_id, $new_industry_ids)) {
                $industry_experience = IndustryExperience::find($industry_id);
                $industry_experience->deleted_at = now();
                $industry_experience->save();
            }
        }

        return redirect(route('cv.show', ['cv' => $cv_id, 'resource' => $res_id]))->with('flash_success', 'Industry Experience added successfully');
    }

    public function show($industry_experience_id): View
    {
        $parameters = [
            'industry_experience' => IndustryExperience::where('id', '=', $industry_experience_id)->get(),
        ];

        return View('industry_experience.show')->with($parameters);
    }

    public function edit($industry_experience_id, Request $request): View
    {
        $cv_id = $request->input('cvid');
        $res_id = $request->input('res_id');
        $parameters = [
            'cv_id' => $cv_id,
            'res_id' => $res_id,
            'industry_experience' => IndustryExperience::where('id', '=', $industry_experience_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'industry_dropdown' => Industry::orderBy('id')->pluck('description', 'id')->prepend('Industry', '0'),
            'resource_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', '=', $res_id)->orderBy('id')->pluck('full_name', 'id'),
        ];

        return View('industry_experience.edit')->with($parameters);
    }

    public function update(UpdateIndustryExperienceRequest $request, $industry_experience_id): RedirectResponse
    {
        $cv_id = $request->input('cv_id');
        $res_id = $request->input('res_id');

        $industry_experience = IndustryExperience::find($industry_experience_id);
        $industry_experience->ind_id = $request->input('ind_id');
        $industry_experience->status = $request->input('status');
        $industry_experience->res_id = $request->input('res_id');
        $industry_experience->creator_id = auth()->id();
        $industry_experience->save();

        return redirect(route('cv.show', ['cvid' => $cv_id, 'res_id' => $res_id]))->with('flash_success', 'Industry Experience updated successfully');
    }

    public function destroy($industry_experience_id, Request $request): RedirectResponse
    {
        $cv_id = $request->input('cv_id');
        $res_id = $request->input('res_id');

        IndustryExperience::destroy($industry_experience_id);

        return redirect(route('cv.show', ['cvid' => $cv_id, 'res_id' => $res_id]))->with('flash_success', 'Industry Experience deleted successfully');
    }
}
