<?php

namespace App\Http\Controllers;

use App\Models\AssessmentNotes;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentNotesController extends Controller
{
    public function edit($id): View
    {
        $assessment_notes = AssessmentNotes::find($id);
        $parameters = [
            'assessment_notes' => $assessment_notes,
        ];

        return view('assessment.assessment_notes.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $assessment_notes = AssessmentNotes::find($id);
        $assessment_notes->notes = $request->notes;
        $assessment_notes->save();

        return redirect()->route('assessment.show', $assessment_notes->assessment_id)->with('flash_success', 'Assessment Note Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $assessment_notes = AssessmentNotes::find($id);
        AssessmentNotes::destroy($id);

        return redirect()->route('assessment.show', $assessment_notes->assessment_id)->with('flash_success', 'Assessment Note Deleted Successfully');
    }
}
