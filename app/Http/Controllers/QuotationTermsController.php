<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQuotationTermsRequest;
use App\Http\Requests\UpdateQuotationTermsRequest;
use App\Models\Quotation;
use App\Models\QuotationTerms;
use App\Models\Status;
use App\Models\Terms;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuotationTermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $quotation_terms = QuotationTerms::sortable('quotation_id', 'asc')->paginate($item);

        $quotation_terms = QuotationTerms::with(['statusd:id,description'])->sortable('term', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $quotation_terms = $quotation_terms->where('status', $request->input('status_filter'));
            }else{
                $quotation_terms = $quotation_terms->sortable('term', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $quotation_terms = $quotation_terms->where('term', 'like', '%'.$request->input('q').'%');
        }

        $quotation_terms = $quotation_terms->paginate($item);

        $parameters = [
            'quotation_terms' => $quotation_terms,
        ];

        return View('master_data.quotation_terms.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'quotation_dropdown' => Quotation::orderBy('id')->pluck('scope_of_work', 'id')->prepend('Quotation', '0'),
            'terms_dropdown' => Terms::orderBy('id')->orderBy('row_order')->pluck('term', 'id')->prepend('Terms', '0'),
        ];

        return View('master_data.quotation_terms.create')->with($parameters);
    }

    public function store(StoreQuotationTermsRequest $request): RedirectResponse
    {
        $quotation_terms = new QuotationTerms();
        $quotation_terms->quotation_id = $request->input('quotation');
        $quotation_terms->terms_id = $request->input('terms_id');
        $quotation_terms->term = $request->input('term');
        $quotation_terms->freeze = $request->input('freeze');
        $quotation_terms->status = $request->input('status');
        $row_order = QuotationTerms::orderBy('row_order', 'desc')->first();
        $quotation_terms->row_order = $row_order ? ++$row_order->row_order : 1;
        $quotation_terms->creator_id = auth()->id();
        $quotation_terms->save();

        return redirect(route('quotation_terms.index'))->with('flash_success', 'Master Data Quotation Term captured successfully');
    }

    public function show($marital_status_id): View
    {
        $parameters = [
            'quotation_terms' => QuotationTerms::where('id', '=', $marital_status_id)->get(),
        ];

        return View('master_data.quotation_terms.show')->with($parameters);
    }

    public function edit($marital_status_id): View
    {
        $parameters = [
            'quotation_terms' => QuotationTerms::where('id', '=', $marital_status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'quotation_dropdown' => Quotation::orderBy('id')->pluck('scope_of_work', 'id')->prepend('Quotation', '0'),
            'terms_dropdown' => Terms::orderBy('id')->orderBy('row_order')->pluck('term', 'id')->prepend('Terms', '0'),
        ];

        return View('master_data.quotation_terms.edit')->with($parameters);
    }

    public function update(UpdateQuotationTermsRequest $request, $quotation_terms_id): RedirectResponse
    {
        $quotation_terms = QuotationTerms::find($quotation_terms_id);
        $quotation_terms->quotation_id = $request->input('quotation');
        $quotation_terms->terms_id = $request->input('terms_id');
        $quotation_terms->term = $request->input('term');
        $quotation_terms->freeze = $request->input('freeze');
        $quotation_terms->status = $request->input('status');
        $quotation_terms->creator_id = auth()->id();
        $quotation_terms->save();

        return redirect(route('quotation_terms.index'))->with('flash_success', 'Master Data Quotation Term saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // QuotationTerms::destroy($id);
        $item = QuotationTerms::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('quotation_terms.index')->with('success', 'Master Data Quotation Term suspended successfully');
    }
}
