<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\Assignment;
use App\Models\Anniversary;
use App\Models\Config;
use App\Models\Task;
use App\Models\BillingPeriod;
use App\Models\Project;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Calendar;
use App\Models\User;
use App\Models\Timesheet;
use App\Models\Timeline;
use App\Models\Team;
use App\Models\InvoiceStatus;
use App\Models\VendorInvoiceStatus;
use App\Models\PlanUtilization;
use App\Models\TimeExp;
use App\Models\AssessmentHeader;
use App\Models\Leave;
use App\Models\AssetRegister;
use App\Models\Resource;
use App\Models\Cv;
use App\Models\LeaveBalance;
use App\Models\MyWorkWeekView;
use App\Models\MyWorkWeekLeaveView;
use App\Models\MyWorkWeekAssignmentView;
use App\Models\MyWorkWeekTaskView;
use App\Models\MyWorkWeekExpensesView;
use DateTime;

class MyWorkWeekController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return view('my-work-week.index');
    }

    public function show(){

    }

    public function getFilterDropDowns(Request $request)
    {
        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->filters()->get(['id','name']);
        $company_drop_down = Company::orderBy('company_name')->whereNotNull('company_name')->where('company_name', '!=', '')->filters()->get(['company_name', 'id']);
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->filters()->get(['customer_name', 'id']);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8, 9])->orderBy('first_name')->orderBy('last_name')->filters()->get(['full_name', 'id']);

        $week_to_drop_down = TimeSheet::orderBy('year_week', 'desc')->whereNotNull('year_week');
        
        if($request->has('weekfrom') && $request->weekfrom != '-1'){
            $week_to_drop_down = $week_to_drop_down->where('year_week','>=',strval($request->weekfrom));    
        }

        $week_to_drop_down = $week_to_drop_down->filters()->groupBy('year_week')->get(['year_week', 'year_week']);

        $week_from_drop_down = TimeSheet::orderBy('year_week', 'desc')->whereNotNull('year_week')->filters();
        
        if($request->has('weekto') && $request->weekto != '-1'){
            $week_from_drop_down = $week_from_drop_down->where('year_week','<=',$request->weekto);    
        }

        $week_from_drop_down = $week_from_drop_down->groupBy('year_week')->get(['year_week', 'year_week']);
        $is_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->get(['description', 'id']);
        $team_drop_down = Team::orderBy('team_name')->get(['team_name', 'id']);
        $vis_drop_down = VendorInvoiceStatus::orderBy('description')->whereNotNull('description')->get(['description', 'id']);

        $parameters = [
            'project_drop_down' => $project_drop_down,
            'company_drop_down' => $company_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'employee_drop_down' => $resource_drop_down,
            'week_from_drop_down' => $week_from_drop_down,
            'week_to_drop_down' => $week_to_drop_down,
            'team_drop_down' => $team_drop_down,
            'is_drop_down' => $is_drop_down,
            'vis_drop_down' => $vis_drop_down,
        ];
        
        return response()->json($parameters);
    }

    public function getTaskOverview(Request $request)
    {
        $tasks = MyWorkWeekTaskView::where('task_status_id','<','4');

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $tasks = $tasks;
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $tasks = $tasks->where('employee_id',auth()->id());
        }

        if($request->has('company') && $request->input('company') != '-1'){
            $tasks = $tasks->where('company_id', $request->input('company'));
        }

        if($request->has('customer') && $request->input('customer') != '-1'){
            $tasks = $tasks->where('customer_id', $request->input('customer'));
        }

        if($request->has('project') && $request->input('project') != '-1'){
            $tasks = $tasks->where('project_id', $request->input('project'));
        }
        
        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            $tasks = $tasks->where('billable', $request->input('is_billable'));
        }
        
        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $tasks = $tasks->where('employee_id', $request->input('employee'));
        }

        $tasks = $tasks->get()->unique('task_id');

        $from = Carbon::createFromFormat('Y-m-d',Carbon::parse(now())->format('Y-m-d'));
        $to = Carbon::parse(now())->format('Ymd');

        if($request->has('weekfrom') && $request->weekfrom != '-1'){
            $fdate = $this->getStartAndEndDate(substr($request->weekfrom,4,2),substr($request->weekfrom,0,4));
            $from = Carbon::parse($fdate['week_start'])->format('Ymd');
        }

        
        if($request->has('weekto') && $request->weekto != '-1'){
            $tdate = $this->getStartAndEndDate(substr($request->weekto,4,2),substr($request->weekto,0,4));
            $to = Carbon::parse($tdate['week_start'])->format('Ymd');
        }
        
        $otasks = [];
        
        foreach($tasks as $task){
            if(!in_array($task->task_id,$otasks)){
                array_push($otasks,$task->task_id);
            }
        };

        $parameters = [
            'open' => count($otasks),
            'overdue' => $tasks->filter(function($item){ $date = Carbon::createFromFormat('Y-m-d',Carbon::parse($item->task_end_date)->format('Y-m-d')); if($date->lt(Carbon::createFromFormat('Y-m-d',Carbon::parse(now())->format('Y-m-d')))){ return $item; }})->count()
        ];
        
        return response()->json($parameters);
    }

    public function getAssignmentOverview(Request $request)
    {
        $ass = MyWorkWeekAssignmentView::query();
        
        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $ass = $ass->where('employee_id', $request->input('employee'));
        }
        
        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $ass = $ass;
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $ass = $ass->where('employee_id',auth()->id());
        }
        
        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            if($request->input('is_billable') == '1'){
                $ass = $ass->where('no_of_income', '1');
            } else {
                $ass = $ass->where('no_of_cost', '1');
            }
        }

        if($request->has('project') && $request->input('project') != '-1'){
            $ass = $ass->where('project_id', $request->input('project'));
        }

        if($request->has('customer') && $request->input('customer') != '-1'){
            $ass = $ass->where('customer_id', $request->input('customer'));
        }

        if($request->has('company') && $request->input('company') != '-1'){
            $ass = $ass->where('company_id', $request->input('company'));
        }

        $ass = $ass->get();

        $income = $ass->filter(function ($item){ if($item->no_of_income == 1 && $item->no_of_cost == 0){ return $item; }});
        $cost = $ass->filter(function ($item){ if($item->no_of_income == 0 && $item->no_of_cost == 1){ return $item; }});
        $hours = $ass->filter(function ($item){ return $item;});

        $parameters = [
            'open_income' => $income->count(),
            'hours_income' => number_format($hours->sum('available_hours_on_income'),2),
            'open_cost' => $cost->count(),
            'hours_cost' => number_format($hours->sum('available_hours_on_cost'),2)
        ];

        return response()->json($parameters);
    }

    public function getAnniversariesOverview(Request $request){

        $anniversaries = Anniversary::with('anniversary_type');
        
        $anniversaries = $anniversaries->get();

        $data = [];

        foreach($anniversaries as $anniversary){
            if($request->has('weekfrom') && $request->weekfrom != '-1' && $request->has('weekto') && $request->weekto != '-1'){
                if(Carbon::parse($anniversary->anniversary_date)->weekOfYear >= substr($request->weekfrom,4,2) && Carbon::parse($anniversary->anniversary_date)->weekOfYear <= (substr($request->weekto,4,2))){
                    array_push($data,['name'=>$anniversary->name.' '.$anniversary->anniversary_type->description,'date'=>Carbon::parse($anniversary->anniversary_date)->format('d M'),'datepart'=>substr($anniversary->anniversary_date,5,2).''.substr($anniversary->anniversary_date,8,2)]);
                }
            } else {
                
                if($request->has('weekfrom') && $request->weekfrom != '-1'){
                    if(Carbon::parse($anniversary->anniversary_date)->weekOfYear >= substr($request->weekfrom,4,2) && Carbon::parse($anniversary->anniversary_date)->weekOfYear >= (substr($request->weekfrom,4,2)+2)){
                        array_push($data,['name'=>$anniversary->name.' '.$anniversary->anniversary_type->description,'date'=>Carbon::parse($anniversary->anniversary_date)->format('d M'),'datepart'=>substr($anniversary->anniversary_date,5,2).''.substr($anniversary->anniversary_date,8,2)]);
                    }
                }
                if($request->has('weekto') && $request->weekto != '-1'){
                    if(Carbon::parse($anniversary->anniversary_date)->weekOfYear <= substr($request->weekto,4,2) && Carbon::parse($anniversary->anniversary_date)->weekOfYear <= (substr($request->weekto,4,2))){
                        array_push($data,['name'=>$anniversary->name.' '.$anniversary->anniversary_type->description,'date'=>Carbon::parse($anniversary->anniversary_date)->format('d M'),'datepart'=>substr($anniversary->anniversary_date,5,2).''.substr($anniversary->anniversary_date,8,2)]);
                    }
                }
            }
            if($request->has('weekto') && $request->weekto == '-1' && $request->has('weekfrom') && $request->weekfrom == '-1'){
                if(Carbon::parse($anniversary->anniversary_date)->weekOfYear == Carbon::parse(now())->weekOfYear){
                    array_push($data,['name'=>$anniversary->name.' '.$anniversary->anniversary_type->description,'date'=>Carbon::parse($anniversary->anniversary_date)->format('d M'),'datepart'=>substr($anniversary->anniversary_date,5,2).''.substr($anniversary->anniversary_date,8,2)]);
                }
            }
        }

        array_multisort(array_column($data, 'datepart'), SORT_ASC, $data);
        $newArray = array_slice($data, 0, 5, true);

        return response()->json($newArray);
    }

    public function getTimesheets(Request $request){
        
        $currentYearWeekStart = (Carbon::now()->year.''.Carbon::now()->weekOfYear);
        $currentYearWeekEnd = Carbon::now()->year.''.Carbon::now()->weekOfYear;
        
        if($request->has('weekfrom') && $request->input('weekfrom') != '-1'){
            $currentYearWeekStart = $request->weekfrom;
            $currentYearWeekEnd = $request->weekfrom;
        }

        if($request->has('weekto') && $request->input('weekto') != '-1'){
            $currentYearWeekEnd = $request->weekto;
        }

        $timelines = Timesheet::selectRaw("timesheet.id as id,
        CONCAT(users.first_name,' ',users.last_name) AS `resource`,
        customer.customer_name AS customer_name,
        projects.name AS project,
        CASE WHEN timeline.task_id > 0 && timeline.description_of_work = '' THEN tasks.description
        WHEN timeline.task_id > 0 && timeline.description_of_work != '' THEN CONCAT(tasks.description,' - ',timeline.description_of_work)
        WHEN timeline.task_id > 0 THEN tasks.description
        ELSE timeline.description_of_work END AS 'task',
        timeline.is_billable AS 'is_billable',
        timeline.vend_is_billable AS 'vend_is_billable',
        task_delivery_types.name AS 'delivery_type',
        projects.ref AS 'ref',
        timesheet.timesheet_lock AS 'status',
        ((timeline.mon*60)+timeline.mon_m) AS 'mon',
        ((timeline.tue*60)+timeline.tue_m) AS 'tue',
        ((timeline.wed*60)+timeline.wed_m) AS 'wed',
        ((timeline.thu*60)+timeline.thu_m) AS 'thu',
        ((timeline.fri*60)+timeline.fri_m) AS 'fri',
        ((timeline.sat*60)+timeline.sat_m) AS 'sat',
        ((timeline.sun*60)+timeline.sun_m) AS 'sun'")
        ->leftJoin('timeline','timesheet.id', '=', 'timeline.timesheet_id')
        ->leftJoin('users', 'timesheet.employee_id', '=', 'users.id')
        ->leftJoin('customer', 'timesheet.customer_id', '=', 'customer.id')
        ->leftJoin('projects', 'timesheet.project_id', '=', 'projects.id')
        ->leftJoin('tasks', 'timeline.task_id', '=', 'tasks.id')
        ->leftJoin('task_delivery_types', 'timeline.task_delivery_type_id', '=', 'task_delivery_types.id');

        
        if($request->has('weekfrom') && $request->input('weekfrom') != '-1' || $currentYearWeekStart != '-1'){
            $timelines = $timelines->where('timesheet.year_week', '>=', $currentYearWeekStart);
        }

        if($request->has('weekto') && $request->input('weekto') != '-1' && $request->input('weekto') >= $request->input('weekfrom')){
            $timelines = $timelines->where('timesheet.year_week','<=', $currentYearWeekEnd);
        }
        
        if($request->has('cust_inv_status') && $request->input('cust_inv_status') != '-1'){
            $timelines = $timelines->where('timesheet.status_id', $request->input('cust_inv_status'));
        }
        
        if($request->has('vend_inv_status') && $request->input('vend_inv_status') != '-1'){
            $timelines = $timelines->where('timesheet.vendor_invoice_status', $request->input('vend_inv_status'));
        }
        
        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            $timelines = $timelines->where('timeline.is_billable', $request->input('is_billable'));
        }
        
        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $timelines = $timelines->where('users.id', $request->input('employee'));
        }

        
        
        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $timelines = $timelines;
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $timelines = $timelines->where('timesheet.employee_id',auth()->id());
        }

        $timelines = $timelines->orderBy('year_week','desc')->get();


        $data = [];

        foreach($timelines as $timeline){
            array_push($data,[
                'id' => $timeline->id,
                'resource' => $timeline->resource,
                'customer' => $timeline->customer_name,
                'project' => $timeline->project,
                'task' => $timeline->task,
                'is' => $timeline->is_billable,
                'vis' => $timeline->vend_is_billable,
                'delivery_type' => $timeline->delivery_type,
                'timesheet_ref' => $timeline->ref,
                'status' => $timeline->status,
                'mon' => $timeline->mon,
                'tue' => $timeline->tue,
                'wed' => $timeline->wed,
                'thu' => $timeline->thu,
                'fri' => $timeline->fri,
                'sat' => $timeline->sat,
                'sun' => $timeline->sun,
            ]);
        }

        return response()->json($data);
    }

    public function getTasks(Request $request){

        $dates = $this->getStartAndEndDate(Carbon::now()->weekOfYear,Carbon::now()->year);
        $to = '';

        
        if(($request->has('weekfrom') && $request->weekfrom == '-1') && ($request->has('weekto') && $request->weekto == '-1')){
            $from = $dates['week_start'];
        }

        if($request->has('weekfrom') && $request->weekfrom != '-1'){
            $dates = $this->getStartAndEndDate(substr($request->weekfrom,4,2),substr($request->weekfrom,0,4));
            $from = $dates['week_start'];
            $to = '';
        }

        if($request->has('weekto') && $request->weekto != '-1'){
            if($request->has('weekfrom') && $request->weekfrom != '-1'){
                $dates = $this->getStartAndEndDate(substr($request->weekfrom,4,2),substr($request->weekfrom,0,4));
                $from = $dates['week_start'];
            } else {
                $from = '';
            }
            $dates2 = $this->getStartAndEndDate(substr($request->weekto,4,2),substr($request->weekto,0,4));
            $to = $dates2['week_end'];
        }

        $tasks = Task::selectRaw("tasks.id as 'id',
        CONCAT(users.first_name,' ',users.last_name) AS `resource`,
        customer.customer_name AS customer_name,
        projects.name AS project,
        tasks.description AS 'task',
        tasks.billable AS 'is_billable',
        epics.name AS 'epic',
        features.name AS 'feature',
        user_stories.name AS 'user_story',
        tasks.start_date as 'start_date',
        tasks.end_date as 'end_date',
        tasks.hours_planned as 'planned',
        task_status.description as 'status',
        projects.id as 'project_id',
        tasks.employee_id as 'employee_id',
        (select SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) as 'hours' from timeline where timeline.task_id = tasks.id) as 'hours'")
        ->leftJoin('users', 'tasks.employee_id', '=', 'users.id')
        ->leftJoin('customer', 'tasks.customer_id', '=', 'customer.id')
        ->leftJoin('projects', 'tasks.project_id', '=', 'projects.id') 
        ->leftJoin('user_stories', 'tasks.user_story_id', '=', 'user_stories.id')
        ->leftJoin('features', 'user_stories.feature_id', '=', 'features.id')
        ->leftJoin('epics', 'features.epic_id', '=', 'epics.id')
        ->leftJoin('task_status', 'tasks.status', '=', 'task_status.id')
        ->where('tasks.status', '<',4);

        if($from != ''){
            $tasks = $tasks->where('tasks.end_date', '>', $from);
        }

        if($to != ''){
            $tasks = $tasks->where('tasks.end_date', '<', $to);
        }

        if($request->has('project') && $request->input('project') != '-1'){
            $tasks = $tasks->where('projects.id', $request->input('project'));
        }

        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            $tasks = $tasks->where('tasks.billable', $request->input('is_billable'));
        }
        
        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $tasks = $tasks->where('tasks.employee_id', $request->input('employee'));
        }

        // if($request->has('cust_inv_status') && $request->input('cust_inv_status') != '-1'){
        //     $timesheets = Timesheet::where('status_id', $request->input('cust_inv_status'))->pluck('id');
        //     $tasks = $tasks->where();
        // }
        
        // if($request->has('vend_inv_status') && $request->input('vend_inv_status') != '-1'){
        //     $timelines = $timelines->where('timesheet.vendor_invoice_status', $request->input('vend_inv_status'));
        // }

        
        
        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $tasks = $tasks->where('tasks.employee_id',auth()->id());
        }

        $tasks = $tasks->get();
        


        $data = [];

        foreach($tasks as $task){

            array_push($data,[
                'id' => $task->id,
                'resource' => $task->resource,
                'customer' => $task->customer_name,
                'project' => $task->project,
                'task' => $task->task,
                'is' => $task->is_billable,
                'epic' => $task->epic,
                'feature' => $task->feature,
                'user_story' => $task->user_story,
                'start_date' => $task->start_date,
                'end_date' => $task->end_date,
                'planned' => $task->planned,
                'actual' => $task->hours,
                'status' => $task->status,
                'employee_id' => $task->employee_id,
                'project_id' => $task->project_id
            ]);
        }

        return response()->json($data);
    }

    public function getAssignment(Request $request){

        $assignments = Assignment::selectRaw("assignment.id as 'id',
        CONCAT(users.first_name,' ',users.last_name) AS `resource`,
        customer.customer_name AS customer_name,
        projects.name AS project,
        business_function.description as 'function',
        assignment.billable AS 'is_billable',
        assignment.project_id AS 'project_id',
        users.id AS 'employee_id',
        assignment.start_date as 'start_date',
        assignment.end_date as 'end_date',
        assignment.hours as 'planned',
        status.name as 'status',
        (SELECT SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS 'hours' FROM timeline 
                                LEFT JOIN tasks ON timeline.task_id = tasks.id
                                LEFT JOIN projects ON tasks.project_id = projects.id
                                LEFT JOIN assignment ass ON projects.id = assignment.project_id WHERE ass.id = assignment.id) as 'hours'")
        ->leftJoin('users','assignment.employee_id', '=', 'users.id')
        ->leftJoin('projects','assignment.project_id', '=', 'projects.id') 
        ->leftJoin('customer','projects.customer_id', '=', 'customer.id')
        ->leftJoin('status','assignment.status', '=', 'status.id')
        ->leftJoin('business_function','assignment.function' , '=', 'business_function.id')
        ->whereIn('projects.status_id',['1','2','3']);
        
        if($request->has('customer') && $request->input('customer') != '-1'){
            $assignments = $assignments->where('customer.id', $request->input('customer'));
        }
        
        if($request->has('project') && $request->input('project') != '-1'){
            $assignments = $assignments->where('assignment.project_id', $request->input('project'));
        }
        
        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            $assignments = $assignments->where('assignment.billable', $request->input('is_billable'));
        }
        
        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $assignments = $assignments->where('users.id', $request->input('employee'));
        }

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $assignments = $assignments->where('users.id', '=', auth()->user()->id);
        };
        
        $assignments = $assignments->get();


        $data = [];

        foreach($assignments as $assignment){

            array_push($data,[
                'id' => $assignment->id,
                'resource' => $assignment->resource,
                'customer' => $assignment->customer_name,
                'project' => $assignment->project,
                'function' => $assignment->function,
                'is' => $assignment->is_billable,
                'start_date' => $assignment->start_date,
                'end_date' => $assignment->end_date,
                'planned' => $assignment->planned,
                'actual' => $assignment->hours,
                'status' => $assignment->status,
                'project_id' => $assignment->project_id,
                'employee_id' => $assignment->employee_id
            ]);
        }

        return response()->json($data);
    }

    public function getActualVsPlanned(Request $request){

        $data = [];
        $td = [];
        $fd = [];
        $result = [];
        $no_timesheet_tasks = [];
        $final = [];
        $manager = 0;
        $tl = [];

        // array_push($fd,['is_weekend'=> Carbon::parse(now())->isWeekend(),'day'=>0,'date'=>Carbon::parse(now())->format('Y-m-d').'-2','tooltip'=>Carbon::parse(now())->format('D d M')]);
        for($i=-12;$i<=0;$i++){
            array_push($td,['key'=>'A'.($i<0?$i*-1:$i),'is_weekend'=> Carbon::parse(now())->addDays($i)->isWeekend(),'day'=>$i,'date'=>Carbon::parse(now())->addDays($i)->format('d/m'),'tooltip'=>Carbon::parse(now())->addDays($i)->format('D d M')]);
        }
        
        for($i=0;$i<=12;$i++){
            array_push($fd,['key'=>'P'.$i,'is_weekend'=> Carbon::parse(now())->addDays($i)->isWeekend(),'day'=>$i,'date'=>Carbon::parse(now())->addDays($i)->format('d/m'),'tooltip'=>Carbon::parse(now())->addDays($i)->format('D d M')]);
        }

        
            $weeks = [['num'=>1,'abr'=>'mon','name' => 'Monday'],['num'=>2,'abr'=>'tue','name' => 'Tuesday'],['num'=>3,'abr'=>'wed','name' => 'Wednesday'],['num'=>4,'abr'=>'thu','name' => 'Thursday'],['num'=>5,'abr'=>'fri','name' => 'Friday'],['num'=>6,'abr'=>'sat','name' => 'Saturday'],['num'=>6,'abr'=>'sun','name' => 'Sunday']];


            $mww = MyWorkWeekView::query();

            
                if (auth()->user()->hasAnyRole(['consultant', 'contractor']) && !auth()->user()->hasAnyRole(['admin', 'admin_manager','manager'])) {
                    $manager = 0;
                $mww = $mww->where('employee_id',auth()->id());
            }

            if (auth()->user()->hasAnyRole(['admin', 'admin_manager','manager'])) {
            $manager = 1;
            }
            if($request->has('employee') && $request->employee != '0' && $request->employee != '-1'){
                $manager = 0;
                $mww = $mww->where('employee_id',$request->employee);
            }

            if($request->has('company') && $request->input('company') != '-1'){
                $mww = $mww->where('company_id', $request->input('company'));
            }
    
            if($request->has('customer') && $request->input('customer') != '-1'){
                $mww = $mww->where('customer_id', $request->input('customer'));
            }
    
            if($request->has('project') && $request->input('project') != '-1'){
                $mww = $mww->where('project_id', $request->input('project'));
            }

        return response()->json(['manager' => $manager, 'data' => $mww->orderBy('consultant_first_name')->orderBy('consultant_last_name')->get(),'td'=>$td,'fd'=>$fd]);
    }

    public function getUtilizationOverview(Request $request){
        $thisWeek = Carbon::now()->year.''.(Carbon::now()->weekOfYear < 10 ? '0'.Carbon::now()->weekOfYear : Carbon::now()->weekOfYear);
        $lastWeek = Carbon::now()->year.''.(Carbon::now()->subWeek()->weekOfYear < 10 ?  '0'.Carbon::now()->subWeek()->weekOfYear : Carbon::now()->subWeek()->weekOfYear);
        $lastWeek2 = Carbon::now()->year.''.(Carbon::now()->weekOfYear < 10 ? '0'.Carbon::now()->weekOfYear : Carbon::now()->weekOfYear)-2;
        // return Carbon::now()->year.''.(Carbon::now()->subWeek()->weekOfYear < 10 ?  '0'.Carbon::now()->subWeek()->weekOfYear : Carbon::now()->subWeek()->weekOfYear);
        $times = Timeline::selectRaw("timeline.is_billable as 'is_billable',
                (tasks.hours_planned * 60) as 'planned',
                timesheet.year_week AS 'year_week',
                (timeline.total * 60) + timeline.total_m AS 'actual'")
                ->leftJoin('timesheet', 'timeline.timesheet_id', '=', 'timesheet.id')
                ->leftJoin('tasks', 'timeline.task_id', '=', 'tasks.id')
                ->leftJoin('projects', 'tasks.project_id', '=', 'projects.id')
                ->where('timesheet.year_week', '>=', Carbon::now()->year.''.(Carbon::now()->subWeek()->weekOfYear < 10 ?  '0'.Carbon::now()->subWeek()->weekOfYear : Carbon::now()->subWeek()->weekOfYear));
                
        if($request->has('customer') && $request->input('customer') != '-1'){
            $times = $times->where('projects.customer_id', $request->input('customer'));
        }

        if($request->has('project') && $request->input('project') != '-1'){
            $times = $times->where('tasks.project_id', $request->input('project'));
        }
        
        if($request->has('is_billable') && $request->input('is_billable') != '-1'){
            $times = $times->where('timeline.is_billable', $request->input('is_billable'));
        }
        
        if($request->has('employee') && $request->input('employee') != '-1' && $request->input('employee') != '0'){
            $times = $times->where('timesheet.employee_id', $request->input('employee'));
        }

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
        } elseif(auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            // $times = $times->where('projects.manager_id', '=', auth()->user()->id);
            $times = $times->where('timesheet.employee_id', '=', auth()->user()->id);
        };
        
        $times = $times->get();

        $weekNow = 0;
        $weekNowBill = 0;
        $weekNowPerc = 0;
        $weekNowBillPerc = 0;
        $weekPast = 0;
        $weekPastPerc = 0;
        $weekPastBill = 0;
        $weekPastBillPerc = 0;

        foreach($times as $time){
            if($time->year_week == $thisWeek){
                $weekNow = $weekNow + $time->actual;
                if($time->is_billable == 1){
                    $weekNowBill = $weekNowBill + $time->actual;
                }
            }
            
            if($time->year_week == $lastWeek){
                $weekPast = $weekPast + $time->actual;
                if($time->is_billable == 1){
                    $weekPastBill = $weekPastBill + $time->actual;
                }
            }
        }

            if(($request->has('employee') && $request->input('employee') != '-1' && $request->input('employee') != '0')){
                $weekNowPerc = $weekNow / (2400 / 100);
                $weekNowBillPerc = $weekNowBill / (2400 / 100);
                $weekPastPerc = $weekPast / (2400 / 100);
                $weekPastBillPerc = $weekPastBill / (2400 / 100);
            }

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $resources = PlanUtilization::where('yearwk','>=',$lastWeek)->get();
    
            foreach($resources as $resource){
                
                if($resource->yearwk == $thisWeek){
                    if(($request->has('employee') && $request->input('employee') != '-1' && $request->input('employee') != '0')){
                        $weekNowPerc = $weekNow / (2400 / 100);
                        $weekNowBillPerc = $weekNowBill / (2400 / 100);
                    } else {
                        $weekNowPerc = $weekNow / ((($resource->res_no * $resource->wk_hours)* 60) / 100) + ((($resource->cost_res_no * $resource->cost_wk_hours)*60)/100);
                        $weekNowBillPerc = $weekNowBill / ((($resource->res_no * $resource->wk_hours)*60)/100);
                    }
                }
                
                if($resource->yearwk == $lastWeek){
                    if(($request->has('employee') && $request->input('employee') != '-1' && $request->input('employee') != '0')){
                        $weekPastPerc = $weekPast / (2400 / 100);
                        $weekPastBillPerc = $weekPastBill / (2400 / 100);
                    } else {
                        $weekPastPerc = $weekPast / ((($resource->res_no * $resource->wk_hours)* 60) / 100) + ((($resource->cost_res_no * $resource->cost_wk_hours)*60)/100);
                        $weekPastBillPerc = $weekPastBill / ((($resource->res_no * $resource->wk_hours)*60)/100);
                    }
    
                }
            }
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            
            $weekNowPerc = $weekNow / (2400 / 100);
            $weekNowBillPerc = $weekNowBill / (2400 / 100);
    
            $weekPastPerc = $weekPast / (2400 / 100);
            $weekPastBillPerc = $weekPastBill / (2400 / 100);

        };

        $params = [
            'weekNow' => $weekNow,
            'weekNowBill' => $weekNowBill,
            'weekNowPerc' => $weekNowPerc,
            'weekNowBillPerc' => $weekNowBillPerc,
            'weekPastPerc' => $weekPastPerc,
            'weekPastBillPerc' => $weekPastBillPerc,
            'weekPast' => $weekPast,
            'weekPastBill' => $weekPastBill,
        ];
        
        return response()->json($params);
    }

    public function getExpensesOverview(Request $request){
        $expenses = MyWorkWeekExpensesView::query();
        
        $expenses->when($request->has('employee') && $request->employee != '0' && $request->employee != '-1', function ($q){
            $q->where('employee_id', $request->employee);
        })->when(auth()->user()->hasAnyRole(['consultant', 'contractor']),function ($q) {
            $q->where('employee_id', auth()->user()->id);
        })->when($request->has('company') && $request->company != '0' && $request->company != '-1', function ($q){
            $q->where('company_id', $request->company);
        })->get();

        $params = [
            'notapproved' => $expenses->sum('Expenses_Not_Approved'),
            'notpaid' => $expenses->sum('Expenses_Not_Paid')
        ];
        
        return response()->json($params);
    }

    public function getMissingTimesheetsOverview(Request $request){
        $currentWeek = Carbon::now()->year.''.Carbon::now()->weekOfYear;
        $currentWeekStart = Carbon::parse(now())->startOfWeek();
        $currentWeekEnd = Carbon::parse(now())->startOfWeek();

        $active_assignments = Assignment::with('resource')->whereIn('assignment_status', [1, 2, 3]);

        if($request->has('employee') && $request->employee != '0'){
            $active_assignments = $active_assignments->where('employee_id',$request->employee);
        }

        if($request->has('is_billable') && $request->is_billable != '-1'){
            $active_assignments = $active_assignments->where('billable',$request->is_billable);
        }

        if($request->has('customer') && $request->customer != '-1'){
            $active_assignments = $active_assignments->whereHas('project',function ($q) use ($request){
                $q->where('customer_id',$request->customer);
            });
        }

        if($request->has('project') && $request->project != '-1'){
            $active_assignments = $active_assignments->where('project_id',$request->project);
        }

        $active_assignments = $active_assignments->where('start_date','<=',$currentWeekStart);
        $active_assignments = $active_assignments->where('end_date','>=',$currentWeekEnd);

        
        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            // $active_assignments = $active_assignments->whereHas('resource_user', function ($query) {
            //     $query->where('manager_id', '=', auth()->user()->id);
            // });
            
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $active_assignments = $active_assignments->where('employee_id', '=', auth()->user()->id);
        };
        
        $active_assignments = $active_assignments->get();

        $thisweek = 0;
        $lastweek = 0;

        foreach ($active_assignments as $a) {
            for($i = ($currentWeek-1);$i <= $currentWeek;$i++){
                $timesheet = Timesheet::with('timeline','resource')->where('year_week',$i);

                 if (auth()->user()->hasAnyRole(['manager','admin','admin-manager'])) {
                    $timesheet = $timesheet->where('project_id', '=', $a->project_id);
                } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
                    $timesheet = $timesheet->where('project_id', '=', $a->project_id)
                    ->where('employee_id', '=', auth()->id());
                } else {
                    $timesheet = $timesheet->where('project_id', '=', $a->project_id)
                    ->where('employee_id', '=', $a->employee_id);
                }

                    $timesheet = $timesheet->get();
                

                if($i == ($currentWeek-1) && count($timesheet) == 0){
                    $lastweek++;
                }
                if($i == ($currentWeek) && count($timesheet) == 0){
                    $thisweek++;
                }
        }
        }

        
        $params = [
            'thisweek' => $thisweek,
            'lastweek' => $lastweek
        ];
        
        return response()->json($params);
    }

    public function getLeaveOverview(Request $request){
        $from = Carbon::parse(now())->startOfYear()->format('Y-m-d');
        $to = Carbon::parse(now())->endOfYear()->format('Y-m-d');
        $config = Config::first();

        $total_leave = 0;
        $leave_not_approved = 0;

        $leave = MyWorkWeekLeaveView::query();

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $leave = $leave;
        } elseif (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $leave = $leave->where('employee_id',auth()->id());
        } 

        if($request->has('employee') && $request->input('employee') != '0' && $request->input('employee') != '-1'){
            $leave = $leave->where('employee_id', $request->input('employee'));
        }

        if($request->has('company') && $request->input('company') != '-1'){
            $leave = $leave->where('company_id', $request->input('company'));
        }

        $leave = $leave->get();
        
        $params = [
            'leave_not_approved' => $leave->sum('Leave_Days_Not_Approved'),
            'leave_balance' => number_format($leave->sum('Current_Leave_Balance'),2)
        ];
        
        return response()->json($params);
    }

    private function getDayDate($date,$day){

        $day_date = $date;

        if($day == 'Tuesday'){
            $day_date = Carbon::parse($date)->addDays(1)->format('Y-m-d');
            }
            if($day == 'Wednesday'){
            $day_date = Carbon::parse($date)->addDays(2)->format('Y-m-d');
            }
            if($day == 'Thursday'){
            $day_date = Carbon::parse($date)->addDays(3)->format('Y-m-d');
            }
            if($day == 'Friday'){
            $day_date = Carbon::parse($date)->addDays(4)->format('Y-m-d');
            }
            if($day == 'Saturday'){
            $day_date = Carbon::parse($date)->addDays(5)->format('Y-m-d');
            }
            if($day == 'Sunday'){
            $day_date = Carbon::parse($date)->addDays(6)->format('Y-m-d');
            }

        return $day_date;
    }

    private function plannedHours($start,$end,$planned,$actual,$array){

        $data = [];
        $dates = [];

         for($i=0;$i<=12;$i++){
                array_push($dates,Carbon::parse(now())->addDays($i)->format('Y-m-d'));
            }

        $calendars = Calendar::whereIn('date',$dates)->get();
        
        foreach($calendars as $calendar){
            
            for($i=0;$i<=12;$i++){
                if(Carbon::parse(now())->addDays($i)->format('Y-m-d') == $calendar->date){
                    $calc = 0;
                    if($start >= Carbon::parse(now())->addDays($i)->format('Y-m-d') && $start >= Carbon::parse(now())->format('Y-m-d')){
                        $calc = $start;
                    } else if($end < Carbon::parse(now())->addDays($i)->format('Y-m-d') && $start < Carbon::parse(now())->format('Y-m-d')){
                        $calc = Carbon::parse(now())->format('Y-m-d');
                    } else {
                        $calc = 0;
                    }


                    $payload = [];
                    if(Carbon::parse(now())->addDays($i)->format('Y-m-d') == $calendar->date && $calendar->weekday == "N" && $calendar->public_holiday == "Y"){
                        $data[$calendar->date] = 0;
                    } else if($calendar->date >= $calc && $calendar->date <= $end && $calendar->weekday == "Y" && $calendar->public_holiday == "N"){
                        $cnt = DB::select("select date from calendar where date >= CASE
                        WHEN '".$start."' >= '".Carbon::parse(now())->addDays(2)->format('Y-m-d')."' AND '".$start."' >= '".Carbon::parse(now())->format('Y-m-d')."'
                            THEN '".$start."'
                        WHEN '".$start."' < '".Carbon::parse(now())->addDays(2)->format('Y-m-d')."' AND '".$start."' < '".Carbon::parse(now())->format('Y-m-d')."'
                            THEN '".Carbon::parse(now())->format('Y-m-d')."'
                        ELSE '".$start."'
                    END
                    AND date <= '".$end."'
                    AND Weekday = 'Y'
                    AND public_holiday = 'N'");
                    if(strtotime($calendar->date) != strtotime(Carbon::parse(now())->format('Y-m-d'))){
                        $data[$calendar->date] = (($planned - $actual) / count($cnt) > 0 ? number_format((($planned - $actual) / count($cnt)/60),1) : 0);
                    } else {
                        $hours = ((isset($array[$calendar->date]) ? $array[$calendar->date] : 0));
                        $data[$calendar->date.'-2'] = number_format(($planned - $actual) / count($cnt) / 60,1) ;
                      }
                    } else {
                        $data[$calendar->date] = 0;
                    };
                }
            }
        };

        return $data;
    }

    private function getStartAndEndDate($week, $year)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }

    private function complete_percentage($model, $table_name, $foreign_key, $own_col)
    {
        $pos_info = DB::select('SHOW COLUMNS FROM '.$table_name);
        $base_columns = count($pos_info);
        $not_null = 0;
        if($foreign_key){
        foreach ($pos_info as $col) {
            $not_null += app('App\\Models\\'.$model)::selectRaw('SUM(CASE WHEN '.$col->Field.' IS NOT NULL THEN 1 ELSE 0 END) AS not_null')->where($own_col, '=', $foreign_key->user_id)->first()->not_null;
        }}

        return ($not_null / $base_columns) * 100;
    }

    public function getAssessmentOverview(Request $request){
        $assessmentDue = AssessmentHeader::orderBy('next_assessment','desc');

        if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $assessmentDue = $assessmentDue->where('resource_id', '=', auth()->id());
        }

        $assessmentDue = $assessmentDue->first();

        $assessmentLast = AssessmentHeader::orderBy('assessment_date','desc');

        $assessmentLast = $assessmentLast->first();

        $params = [
            'next' => ($assessmentDue ? $assessmentDue->next_assessment : '-'),
            'last' => ($assessmentDue ? $assessmentLast->assessment_date : '-')
        ];
        
        return response()->json($params);
    }

    public function getRecordsUpToDate(Request $request){

        $assets = AssetRegister::orderBy('retire_date','desc');
        $cv = '';

        if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $assets = $assets->where('issued_to',auth()->id())->first();
            $cv = Cv::with('user')->where('user_id',auth()->id())->first();
        }

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $assets = $assets->orderBy('issued_to')->first();
            $cv = Cv::with('user')->where('user_id',auth()->id())->first();
        }

        // skill - count
        // qualificatio0n - count
        // experience - cv table - count
        // industry experience - count
        // references - cv references table - count
        // talent - complete_percentage

        $user = User::find(auth()->id());

        $resource = Resource::find($user->resource_id);

        $params = [
            'cv_complete' => $this->complete_percentage('Cv', 'talent', $resource, 'user_id'),
            'retire' => $assets->retire_date ?? '',
            'cv' => $cv ?? ''
        ];
        
        return response()->json($params);
    }
}