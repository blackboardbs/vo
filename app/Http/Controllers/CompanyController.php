<?php

namespace App\Http\Controllers;

use App\Models\BankAccountType;
use App\Models\BBBEELevel;
use App\Models\Company;
use App\Models\Config;
use App\Models\Country;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Team;
use App\Services\CompanyService;
use App\Services\StorageQuota;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CompanyController extends Controller
{
    public function index(Request $request, CompanyService $service): View
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $company = Company::with(['status:id,description'])->withCount(['projects', 'users'])->orderBy('id', 'desc');

        if ($request->has('q') && $request->input('q') != '') {
            $company = $company->where('company_name', 'LIKE', '%'.$request->input('q').'%')->orWhere('email', 'like', '%'.$request->input('q').'%')
                ->orWhere('phone', 'like', '%'.$request->input('q').'%')
                ->orWhere('cell', 'like', '%'.$request->input('q').'%')
                ->orWhere('contact_firstname', 'like', '%'.$request->input('q').'%')
                ->orWhere('contact_lastname', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'LIKE', '%'.$request->input('q').'%');
                });
        }

        $company = $company->sortable(['company_name' => 'asc'])->paginate($request->input('r') ?? 15);

        $parameters = [
            'company' => $company,
            'can_create' => $service->hasCreatePermissions(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        return view('company.index', $parameters);
    }

    public function create(CompanyService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $parameters = [
            'sidebar_process_countries' => Country::orderBy('name')->pluck('name', 'id')->prepend('Country', '0'),
            'sidebar_process_bbbee_level' => BBBEELevel::orderBy('description')->pluck('description', 'id')->prepend('BBBEE Level', '0'),
            'bank_account_type_dropdown' => BankAccountType::orderBy('description')->pluck('description', 'id'),
            'company_name' => Company::find(Config::first()?->company_id)?->company_name,
        ];

        return view('company.create')->with($parameters);
    }

    public function store(StoreCompanyRequest $request, Company $company, CompanyService $service, StorageQuota $quota): RedirectResponse
    {
        $service->createCompany($request, $company, $quota);

        if ($request->hasFile('avatar') && !$quota->isStorageQuotaReached($request->file('avatar')->getSize())){
            return $service->companyResponse("Company created successfully");
        }else{
            return $service->companyResponse("Company created successfully, but logo was not uploaded because you are out of storage");
        }
    }

    public function show(Company $company, CompanyService $service): View
    {
        abort_unless($service->hasViewPermission(), 403);

        $parameters = [
            'company' => $company,
            'teams' => Team::with('status:id,description','manager','creator')->where('parent_company_id', '=', $company->id)->get(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        return view('company.show')->with($parameters);
    }

    public function edit(Company $company, CompanyService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $parameters = [
            'company' => $company,
            'country' => Country::orderby('id')->pluck('name', 'id'),
            'sidebar_process_bbbee_level' => BBBEELevel::orderby('description')->pluck('description', 'id'),
            'team' => Team::where('parent_company_id', '=', $company->id)->get(),
            'bank_account_type_dropdown' => BankAccountType::orderBy('description')->pluck('description', 'id'),
            'company_name' => Company::find(Config::first()?->company_id)?->company_name,
        ];

        return view('company.edit')->with($parameters);
    }

    public function update(UpdateCompanyRequest $request, Company $company, CompanyService $service, StorageQuota $quota)
    {
        $service->updateCompany($request, $company, $quota);

        if ($request->hasFile('avatar') && !$quota->isStorageQuotaReached($request->file('avatar')->getSize())){
            return $service->companyResponse("Company updated successfully");
        }else{
            return $service->companyResponse("Company updated successfully, but logo was not uploaded because you are out of storage");
        }
    }

    public function destroy(Company $company, CompanyService $service)
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $service->deleteLogo($company->company_logo);

        $company->delete();

        return redirect()->route('company.index')->with('flash_success', 'Company deleted successfully');
    }
}
