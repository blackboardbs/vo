<?php

namespace App\Http\Controllers;

use App\Models\Unsubscribe;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UnsubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $unsubscribe = new Unsubscribe();
        $unsubscribe->name = $request->name;
        $unsubscribe->email = $request->email;
        $unsubscribe->creator_id = 1;
        $unsubscribe->status_id = 1;
        $unsubscribe->save();

        return response()->json(['message' => 'The email address '.$unsubscribe->email.' has be successfully unsubscribed from our mailing list.']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Unsubscribe $unsubscribe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Unsubscribe $unsubscribe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unsubscribe $unsubscribe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unsubscribe $unsubscribe)
    {
        //
    }

    public function unsubscribe(Request $request): JsonResponse
    {
        $unsubscribe = new Unsubscribe();
        $unsubscribe->name = 'List';
        $unsubscribe->email = $request->email;
        $unsubscribe->list_id = 1;
        $unsubscribe->creator_id = 1;
        $unsubscribe->status_id = 1;
        $unsubscribe->save();

        return response()->json(['message' => 'The email address '.$unsubscribe->email.' has be successfully unsubscribed from our mailing list.']);
    }
}
