<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTermsRequest;
use App\Http\Requests\UpdateTermsRequest;
use App\Models\Status;
use App\Models\Terms;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $terms = Terms::sortable('row_order', 'asc')->paginate($item);

        $terms = Terms::with(['statusd:id,description'])->sortable('term', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $terms = Terms::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $terms = Terms::with(['statusd:id,description'])->sortable('term', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $terms = $terms->where('term', 'like', '%'.$request->input('q').'%');
        }

        $terms = $terms->paginate($item);

        $parameters = [
            'terms' => $terms,
        ];

        return View('master_data.terms.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.terms.create')->with($parameters);
    }

    public function store(StoreTermsRequest $request): RedirectResponse
    {
        $terms = new Terms();
        $terms->term = $request->input('term');
        $terms->row_order = $request->input('row_order');
        $terms->status = $request->input('status');
        $terms->creator_id = auth()->id();
        $terms->save();

        return redirect(route('master_term.index'))->with('flash_success', 'Master Term captured successfully');
    }

    public function edit($termid): View
    {
        $parameters = [
            'terms' => Terms::where('id', '=', $termid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.terms.edit')->with($parameters);
    }

    public function update(UpdateTermsRequest $request, $termid): RedirectResponse
    {
        $terms = Terms::find($termid);
        $terms->term = $request->input('term');
        $terms->row_order = $request->input('row_order');
        $terms->status = $request->input('status');
        $terms->creator_id = auth()->id();
        $terms->save();

        return redirect(route('master_term.index'))->with('flash_success', 'Master Term saved successfully');
    }

    public function show($termid): View
    {
        $parameters = [
            'terms' => Terms::where('id', '=', $termid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.terms.show')->with($parameters);
    }

    public function destroy($id): RedirectResponse
    {
        // DB::table('terms')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // Terms::destroy($id);
        $item = Terms::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('master_term.index')->with('success', 'Master Data Term suspended successfully');
    }
}
