<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Exports\AnniversariesExport;
use App\Exports\AssessmentsExport;
use App\Jobs\NonDisclosureAgreementJob;
use App\Models\AdvancedTemplates;
use App\Models\AssessmentActivities;
use App\Models\AssessmentConcern;
use App\Models\AssessmentHeader;
use App\Models\AssessmentMaster;
use App\Models\AssessmentMasterDetails;
use App\Models\AssessmentMeasureScore;
use App\Models\AssessmentNotes;
use App\Models\AssessmentPlannedNext;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Config;
use App\Models\Document;
use App\Http\Requests\StoreAssessmentHeaderRequest;
use App\Http\Requests\UpdateAssessmentHeaderRequest;
use App\Jobs\SendAssessmentEmailJob;
use App\Models\Module;
use App\Models\Resource;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AssessmentHeaderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('r') ?? 15;
        $res_assessment = AssessmentHeader::with(['user.resource', 'customer'])->whereHas('user.resource');

        $module = Module::where('name', '=', 'App\Models\Assessment')->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $res_assessment->whereIn('resource_id', Auth::user()->team());
            }
        } else {
            abort(403);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $res_assessment = $res_assessment->whereHas('user', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('q').'%');
                $query->orWhere('last_name', 'like', '%'.$request->input('q').'%');
            })
            ->orWhereHas('customer', function ($query) use ($request) {
                $query->where('customer_name', 'like', '%'.$request->input('q').'%');
            })
            ->orWhere('assessment_date', 'like', '%'.$request->input('q').'%');
        }

        $assess = $res_assessment->get();
        $res_assessment = $res_assessment->sortable('created_at', 'desc')->paginate($item);

        $data = [];

        foreach($assess as $result){
            $data[$result->id] = $result->created_at == AssessmentHeader::where('resource_id',$result->resource_id)->max('created_at') ? 1 : 0;
        }
        
        $parameters = [
            'assessment_headers' => $res_assessment,
            'data' => $data
        ];

        if ($request->has('export')) return $this->export($parameters, 'assessment.assessment_header');

        return view('assessment.assessment_header.index')->with($parameters);
    }

    public function create(): View
    {
        $consultants = User::select(DB::raw('CONCAT(first_name, " ",COALESCE(`last_name`," ")) AS full_name'), 'id');
        $module = Module::where('name', '=', \App\Models\AssessmentHeader::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $consultants->whereIn('resource_id', Auth::user()->team());
            }
        } else {
            $consultants->where('resource_id', '=', Auth::user()->id);
            $timesheets = Timesheet::where('employee_id', '=', Auth::user()->id)->get();
            $customers = $timesheets->pluck('customer.customer_name', 'customer_id');
            $manager = Resource::select('manager_id')->where('user_id', '=', Auth::user()->id)->first();
        }
        $consultants = $consultants->excludes([7])->orderBy('id', 'desc')->pluck('full_name', 'id')->prepend('Select Resource', '0');
        // dd($consultants);
        $parameters = [
            'resource_dropdown' => $consultants,
            'assesor_dropdown' => User::select(DB::raw('CONCAT(first_name, " ",COALESCE(`last_name`," ")) AS full_name'), 'id')->excludes([7])->orderBy('first_name', 'asc')->orderBy('last_name', 'asc')->pluck('full_name', 'id')->prepend('Select Resource', '0'),
            'customer_dropdown' => $customers??[null => 'Select Customer'],
            'manager_id' => isset($manager) ? $manager->manager_id : 0,
            'templates_dropdown' => AdvancedTemplates::where('template_type_id', 4)->pluck('name', 'id')->prepend('Select Template', '0'),
        ];

        return view('assessment.assessment_header.create')->with($parameters);
    }

    public function store(StoreAssessmentHeaderRequest $request): RedirectResponse
    {
        $now = Carbon::now();
        $today = $now->toDateString();
        $assessment_frequancy = Config::select('assessment_frequency')->first()->assessment_frequency;
        $next_assessment = $now->addDays($assessment_frequancy)->toDateString();
        $assessment_period_start = $now->subDays($assessment_frequancy * 2)->toDateString();
        $prev_assessment_header = AssessmentHeader::where('resource_id', '=', $request->resource_id)->orderBy('created_at', 'desc')->first();

        $assessment_header = new AssessmentHeader();
        $assessment_header->resource_id = ($prev_assessment_header != null) ? $prev_assessment_header->resource_id : $request->resource_id;
        $assessment_header->assessment_date = $today;
        $assessment_header->next_assessment = $next_assessment;
        $assessment_header->assessment_period_start = ($prev_assessment_header != null) ? $prev_assessment_header->assessment_period_start : $assessment_period_start;
        $assessment_header->assessment_period_end = $today;
        $assessment_header->customer_id = $prev_assessment_header->customer_id ?? $request->customer_id;
        $assessment_header->assessed_by = $prev_assessment_header->assessed_by ?? $request->assessed_by;
        $assessment_header->updated_by = auth()->id();
        $assessment_header->template_id = $request->template_id;
        $assessment_header->save();

        return redirect(route('assessment.show', $assessment_header))->with('flash_success', 'assessment header captured successfully');
    }

    public function show(Request $request, $id)
    {
        $module = Module::where('name', '=', 'App\Models\Assessment')->get();

        if (Auth::user()->canAccess($module[0]->id, 'show_all') || Auth::user()->canAccess($module[0]->id, 'show_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'show_all') && Auth::user()->canAccess($module[0]->id, 'show_team')) {
                $assessment_header = AssessmentHeader::where('resource_id', '=', Auth::user()->id)->get()->toArray();
                if (empty($assessment_header)) {
                    abort(403);
                }

                if ($id != $assessment_header[0]['id']) {
                    abort(403);
                }
            }
        } else {
            abort(403);
        }

        $config = Config::first();
        $config_assessmnet_master = $config->assessment_master;
        $company = Company::find((isset($config->company_id) ? $config->company_id : 1));
        $assessment_master = [];
        $assessment_master_details = [];
        foreach (explode(',', $config_assessmnet_master) as $master) {
            $assessment_master[] = AssessmentMaster::find($master);
            $assessment_master_details[] = AssessmentMasterDetails::where('assessment_master_id', '=', $master)->get();
        }

        $assessment_header = AssessmentHeader::find($id);
        $assessment_activity = AssessmentActivities::where('assessment_id', '=', $id)->paginate(10);
        $assessment_plan = AssessmentPlannedNext::where('assessment_id', '=', $id)->paginate(10);
        $assessment_concerns = AssessmentConcern::where('assessment_id', '=', $id)->paginate(10);
        $assessment_notes = AssessmentNotes::where('assessment_id', '=', $id)->paginate(10);
        $assessment_measure = AssessmentMeasureScore::with(['assessment_master_details:id,assessment_measure'])->where('assessment_id', '=', $assessment_header->id)->get();

        $assessment_measure_scores = [];

        foreach($assessment_measure as $measure){
            $assessment_measure_scores[$measure->assessment_master_detail_id]['score'] = $measure->score;
            $assessment_measure_scores[$measure->assessment_master_detail_id]['note'] = $measure->notes; 
        }

        // dd($assessment_measure_scores);
        $parameters = [
            'assessment_activities' => $assessment_activity,
            'assessment_header' => $assessment_header,
            'competency_level_dropdown' => [0 => 'Select Level', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            'assessment_plan' => $assessment_plan,
            'assessment_concerns' => $assessment_concerns,
            'assessment_notes' => $assessment_notes,
            'assessment_master' => $assessment_master,
            'assessment_master_details' => $assessment_master_details,
            'assessment_measure' => $assessment_measure,
            'assessment_measure_scores' => $assessment_measure_scores,
            'company' => $company,
        ];
        $storage = storage_path('app/Assessment/'.str_replace(' ', '_', $assessment_header->user?->name()).date('Y-m-d_H_i_s').'.pdf');
        if ($request->print) {
            if (! Storage::exists('Assessment/')) {
                Storage::makeDirectory('Assessment/');
            }
            try {


               Pdf::view('assessment.assessment_header.print', $parameters)
                   ->format('a4')
                   ->save($storage);

                return response()->file($storage);
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        } elseif ($request->email) {
            //return $request->all();
            $emails = isset($request->user_email) ? json_decode($request->user_email) : [];
            array_push($emails, $request->assessor);
            array_push($emails, $request->resource);

            if (! Storage::exists('Assessment/')) {
                Storage::makeDirectory('Assessment/');
            }
            try {

                Pdf::view('assessment.assessment_header.print', $parameters)
                    ->format('a4')
                    ->save($storage);

                $document = new Document();
                //$document->type_id = $template->id;
                $document->document_type_id = 5;
                $document->name = 'Customer Tax Invoice';
                $document->file = str_replace(' ', '_', $assessment_header->user?->name()).date('Y-m-d_H_i_s').'.pdf';
                $document->creator_id = auth()->id();
                $document->owner_id = $assessment_header->resource_id;
                $document->digisign_status_id = 2;
                $document->reference_id = $assessment_header->id;
                $document->digisign_approver_user_id = $assessment_header->assessor->id;
                $document->save();

                //return $assessment_header->user->first_name;

                SendAssessmentEmailJob::dispatch($emails, $assessment_header->customer->customer_name ?? null, $assessment_header->user->name() ?? null, $storage)->delay(now()->addMinutes(5));

                return redirect()->back()->with('flash_success', 'Assessment has been sent');
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }

            return $emails;
        } else {
            $parameters['subject'] = 'Assessment Digisign - '.(isset($assessment_header->user) ? $assessment_header->user->first_name.' '.$assessment_header->user->last_name : '');
            $parameters['resource'] = $assessment_header->user;

            return view('assessment.assessment_header.show')->with($parameters);
        }
    }

    public function edit($id): View
    {
        $module = Module::where('name', '=', 'App\Models\Assessment')->get();

        if (Auth::user()->canAccess($module[0]->id, 'update_all') || Auth::user()->canAccess($module[0]->id, 'update_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'update_all') && Auth::user()->canAccess($module[0]->id, 'update_team')) {
                $assessment_header = AssessmentHeader::where('resource_id', '=', Auth::user()->id)->get()->toArray();
                if (empty($assessment_header)) {
                    abort(403);
                }

                if ($id != $assessment_header[0]['id']) {
                    abort(403);
                }
            }
        } else {
            abort(403);
        }

        $assessment = AssessmentHeader::find($id);

        $customers = Timesheet::with(['customer:id,customer_name'])
            ->select('customer_id')
            ->where('employee_id', '=', $assessment->resource_id)
            ->groupBy('customer_id')
            ->get()->pluck('customer.customer_name', 'customer_id');

        $parameters = [
            'assessment_header' => $assessment,
            'user_dropdown' => [$assessment->resource_id => $assessment->load(['user:id,first_name,last_name'])->user?->name()],
            'assessor_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([7])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Assessor', '0'),
            'customer_dropdown' => $customers,
            'templates_dropdown' => AdvancedTemplates::where('template_type_id', 4)->pluck('name', 'id')->prepend('Select Template', 0),
        ];

        return view('assessment.assessment_header.edit')->with($parameters);
    }

    public function update(UpdateAssessmentHeaderRequest $request, $id): RedirectResponse
    {
        $assessment_header = AssessmentHeader::find($id);
        $assessment_header->resource_id = $request->user_id;
        $assessment_header->customer_id = $request->customer_id;
        $assessment_header->assessed_by = $request->assessor_id;
        $assessment_header->assessment_date = $request->assessment_date;
        $assessment_header->next_assessment = $request->next_assessment;
        $assessment_header->assessment_period_start = $request->assessment_start;
        $assessment_header->assessment_period_end = $request->assessment_end;
        $assessment_header->template_id = $request->template_id;
        $assessment_header->save();

        return redirect()->route('assessment.show', $assessment_header)->with('flash_success', 'Assesment Header updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        AssessmentHeader::destroy($id);

        return redirect(route('assessment.index'))->with('flash_success', 'Assessment header was deleted successfully');
    }

    public function prev_assessment(Request $request): JsonResponse
    {
        $assessment_header = AssessmentHeader::where('resource_id', '=', $request->resource_id)->orderBy('created_at', 'desc')->first();
        $parameters = [
            'assessment_activity' => AssessmentActivities::where('assessment_id', '=', $assessment_header->id)->paginate(10),
        ];

        return response()->json($parameters);
    }

    public function add_activity_line(Request $request): JsonResponse
    {
        $assessment_activity = new AssessmentActivities;
        $assessment_activity->assessment_id = $request->assessment_id;
        $assessment_activity->assessment_task_description = $request->task_description;
        $assessment_activity->competency_level = $request->competency_level;
        $assessment_activity->comfort_level = $request->comfort_level;
        $assessment_activity->notes = $request->notes;
        $assessment_activity->save();

        return response()->json($assessment_activity);
    }

    public function add_assessment_plan(Request $request): JsonResponse
    {
        $assessment_plan = new AssessmentPlannedNext;
        $assessment_plan->assessment_id = $request->assessment_id;
        $assessment_plan->assessment_task_description = $request->task_description;
        $assessment_plan->notes = $request->notes;
        $assessment_plan->save();

        return response()->json($assessment_plan);
    }

    public function add_assessment_concerns(Request $request): JsonResponse
    {
        $assessment_concerns = new AssessmentConcern;
        $assessment_concerns->assessment_id = $request->assessment_id;
        $assessment_concerns->notes = $request->notes;
        $assessment_concerns->save();

        return response()->json($assessment_concerns);
    }

    public function add_assessment_note(Request $request): JsonResponse
    {
        $assessment_notes = new AssessmentNotes;
        $assessment_notes->assessment_id = $request->assessment_id;
        $assessment_notes->notes = $request->notes;
        $assessment_notes->save();

        return response()->json($assessment_notes);
    }

    public function get_customers($employee_id): JsonResponse
    {
        /*$customers = Timesheet::select('timesheet.customer_id')
            ->leftJoin('assignment', function ($join){
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
            })
            ->whereIn('assignment.assignment_status', [2,3])
            ->where('assignment.employee_id', '=', $employee_id)
            ->groupBy('timesheet.customer_id')
            ->get();*/

        $customers = Assignment::select('project_id')
            ->where('employee_id', '=', $employee_id)
            ->whereIn('assignment_status', [2, 3])
            ->groupBy('project_id')
            ->get();
        $customer_dropdown = [];
        foreach ($customers as $customer) {
            array_push($customer_dropdown, [
                'id' => (isset($customer->project) ? $customer->project->customer_id : ''),
                'customer_name' => (isset($customer->project->customer) ? $customer->project->customer->customer_name : ''),
            ]);
        }

        return response()->json($customer_dropdown);
    }

    public function get_approver($resource_id): JsonResponse
    {
        $manager = Resource::select('manager_id')->where('user_id', '=', $resource_id)->first();

        return response()->json($manager);
    }

    public function copy($id): RedirectResponse
    {
        $now = Carbon::now();
        $assessment_frequancy = Config::select('assessment_frequency')->first()->assessment_frequency;
        $next_assessment = $now->now()->addDays($assessment_frequancy)->toDateString();
        $assessment_period_start = $now->now()->subDays($assessment_frequancy)->toDateString();
        $old_assessment = AssessmentHeader::find($id);
        $new_assessment = new AssessmentHeader();
        $new_assessment->resource_id = $old_assessment->resource_id;
        $new_assessment->assessment_date = $now->now()->toDateString();
        $new_assessment->next_assessment = $next_assessment;
        $new_assessment->assessment_period_start = $assessment_period_start;
        $new_assessment->assessment_period_end = $now->now()->toDateString();
        $new_assessment->customer_id = $old_assessment->customer_id;
        $new_assessment->assessed_by = $old_assessment->assessed_by;
        $new_assessment->updated_by = Auth::id();
        $new_assessment->save();

        $old_activity_items = AssessmentActivities::where('assessment_id', '=', $old_assessment->id)->get();

        foreach ($old_activity_items as $activity_item) {
            $new_activity_item = new AssessmentActivities();
            $new_activity_item->assessment_id = $new_assessment->id;
            $new_activity_item->assessment_task_description = $activity_item->assessment_task_description;
            $new_activity_item->competency_level = $activity_item->competency_level;
            $new_activity_item->comfort_level = $activity_item->comfort_level;
            $new_activity_item->notes = $activity_item->notes;
            $new_activity_item->save();
        }

        $old_assessment_plan = AssessmentPlannedNext::where('assessment_id', '=', $old_assessment->id)->get();

        foreach ($old_assessment_plan as $assessment_plan) {
            $new_assessment_plan = new AssessmentPlannedNext();
            $new_assessment_plan->assessment_id = $new_assessment->id;
            $new_assessment_plan->assessment_task_description = $assessment_plan->assessment_task_description;
            $new_assessment_plan->notes = $assessment_plan->notes;
            $new_assessment_plan->save();
        }

        $old_assessment_concern = AssessmentConcern::where('assessment_id', '=', $old_assessment->id)->get();

        foreach ($old_assessment_concern as $concern) {
            $new_concern = new AssessmentConcern();
            $new_concern->assessment_id = $new_assessment->id;
            $new_concern->notes = $concern->notes;
            $new_concern->save();
        }

        $old_assessment_notes = AssessmentNotes::where('assessment_id', '=', $old_assessment->id)->get();

        foreach ($old_assessment_notes as $note) {
            $new_note = new AssessmentNotes();
            $new_note->assessment_id = $new_assessment->id;
            $new_note->notes = $note->notes;
            $new_note->save();
        }

        return redirect()->route('assessment.show', $new_assessment)->with('flash_success', 'New Assessment Created successfully');
    }

    public function print(AssessmentHeader $assessment)
    {
        try {

            $template = $assessment->mapVariables($assessment->template);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name("Assessment for ".$assessment->user?->name() . ".pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('assessment.edit', $assessment))->with('flash_danger', 'Edit your assesment and select template.');
        }
    }

    public function send(AssessmentHeader $assessment)
    {
        $template = $assessment->mapVariables($assessment->template);

        try {
            if (! Storage::exists('assessment/templates')) {
                Storage::makeDirectory('assessment/templates');
            }

            $filename = "assessment_".str_replace(' ', '_',$assessment->user?->name())."_".date('Y_m_d_H_i_s').'.pdf';

            $storage = storage_path("app/assessment/templates/{$filename}");

            Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                ->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->save($storage);

            $assessment->documents()->create([
                'name' => "Assessment of {$assessment->user?->name()}",
                'file' => $filename,
                //'document_type_id' => 4,
                'creator_id' => auth()->id(),
                'status_id' => Status::ACTIVE->value,
            ]);

            $emails = [$assessment->user?->email, auth()->user()->email];

            $data = [
                'emails' => array_values(array_filter(array_unique($emails))),
                'subject' => "Assessment of {$assessment->user?->name()}",
                'body' => "Please find the assessment attached.",
                'attachments' => $storage
            ];

            NonDisclosureAgreementJob::dispatch($data);

            return redirect(route('assessment.show', $assessment))->with('flash_success', 'Assessment was sent successfully');

        }catch (\TypeError $e){
            return redirect(route('assessment.edit', $assessment))->with('flash_danger', 'Edit your assessment and select NDA template.');
        }
    }
}
