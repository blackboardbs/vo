<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Leave;
use App\Models\Project;
use App\Models\Team;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AssignmentReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function assignments(Request $request): View
    {
        $timesheet = Assignment::with(['project','resource'])->select('assignment.employee_id', 'assignment.project_id', 'assignment.start_date', 'assignment.end_date', 'assignment.invoice_rate',
            DB::raw('assignment.hours AS ahours, (assignment.hours * assignment.invoice_rate) AS aamount,
                SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS bhours,
                SUM(CASE WHEN timeline.is_billable = 0 THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS nbhours'))
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
            })
            ->leftJoin('timeline', 'timesheet.id', 'timeline.timesheet_id')
            ->with(['resource', 'project'])
            ->when($request->has('company') && $request->company != null,function ($q) {
                $q->where('timesheet.company_id', request()->company);
            })
            ->when($request->has('team') && $request->team != null, function ($q) {
                $q->whereHas('resource.resource', function ($query) {
                    $query->where('team_id', '=', request()->team);
                });
            })
            ->when($request->has('employee') && $request->employee != null, function ($q) {
                $q->where('assignment.employee_id', request()->employee);
            })
            ->when($request->has('yearmonth') && $request->yearmonth != null, function ($q) {
                $q->where(function ($query) {
                    $query->where('timesheet.year', substr(request()->yearmonth, 0, 4));
                    $query->where('timesheet.month', substr(request()->yearmonth, 4, 2));
                });
            })
            ->when($request->has('dates') && $request->dates != null, function ($q) {
                $q->whereBetween('timesheet.date', request()->dates);
            })
            ->when($request->has('customer') && $request->customer != null, function ($q) {
                $q->whereHas('project', function ($query) use ($request) {
                    $query->where('timesheet.customer_id', request()->customer);
                });
            })
            ->when($request->has('project') && $request->project != null, function ($q) {
                $q->whereHas('project', function ($query) use ($request) {
                    $query->where('id', '=', $request->project);
                });
            })
            ->when($request->has('billable') && $request->billable != null, function ($q) {
                $q->where('timesheet.is_billable', '=', $request->billable);
            })
            ->whereHas('project')
            ->groupBy('assignment.employee_id')
            ->groupBy('assignment.project_id')
            ->groupBy('assignment.start_date')
            ->groupBy('assignment.end_date')
            ->groupBy('assignment.invoice_rate')
            ->groupBy('assignment.hours')
            ->groupBy('assignment.id')
            ->orderBy('assignment.id', 'desc')
            ->paginate($request->r??15);

        $year_months = Timesheet::select('year', 'month')->orderBy('id', 'desc')->get();

        $conc_year_month = [];

        foreach ($year_months as $year_month) {
            $conc_year_month[$year_month->year.$year_month->month] = ($year_month->month < 10) ? $year_month->year.'0'.$year_month->month : $year_month->year.$year_month->month;
        }
        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $conc_year_month,
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'timesheet' => $timesheet,
        ];

        return view('reports.assignment')->with($parameters);
    }

    public function planning(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 10;

        $date = Carbon::now();
        $assignments = Assignment::with(['project','resource'])->whereIn('assignment_status', [2, 3]);

        if ($request->has('project') && $request->input('project') != '') {
            $assignments = $assignments->where('project_id', $request->input('project'));
        }

        if ($request->has('company') && $request->input('company') != '') {
            $assignments = $assignments->where('company_id', $request->input('company'));
        }

        if ($request->has('resource') && $request->input('resource') != '') {
            $assignments = $assignments->where('employee_id', $request->input('resource'));
        }

        $assignments = $assignments->sortable(['name' => 'asc'])->paginate($item);

        $working_days = [];
        $actual_hours = [];
        $hours_forecast = [];
        $assignment_months = [];
        $total_project_budget = [];
        $working_hours = [];
        $invoice_rate = [];
        $po_value = [];
        $planned_spend = [];
        $variance_po = [];
        $year_week_array = [];

        $assignment_actual_hours_array = [];
        $assignment_forecast_hours_array = [];

        // $year = 2017;
        $year = Carbon::now()->year;
        if ($request->has('year') && $request->input('year') != '') {
            $year = $request->input('year');
        }

        foreach ($assignments as $assignment) {
            $leave_days_from_now = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $date->now()->toDateString())->where('date_to', '<=', $assignment->planned_end_date)->get();
            $leave_days_from_planned_start = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $assignment->planned_start_date)->where('date_to', '<=', $assignment->planned_end_date)->get();

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_now = 0;
            foreach ($leave_days_from_now as $leave_day_from_now) {
                $resource_leave_days_taken_now += $leave_day_from_now->no_of_days;
            }

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_start = 0;
            foreach ($leave_days_from_planned_start as $leave_day_from_start) {
                $resource_leave_days_taken_start += $leave_day_from_start->no_of_days;
            }

            $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $calendar = $calendar > 0 ? $calendar - $resource_leave_days_taken_start : $calendar;
            $forecast_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $assignment->planned_end_date])->get()->count();
            $forecast_days = $forecast_days > 0 ? $forecast_days - $resource_leave_days_taken_now : $forecast_days;
            $assignment_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $assignment_days = $assignment_days > 0 ? $assignment_days - $resource_leave_days_taken_start : $assignment_days;
            $number_of_month = Calendar::select('month', 'year')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->groupBy('month', 'year')->get()->count();
            $time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();

            $capacity_allocation_hours = $assignment->capacity_allocation_hours > 0 ? $assignment->capacity_allocation_hours : 8;
            $budget = Assignment::selectRaw('SUM(CASE WHEN billable = '.$assignment->billable?->value.' THEN hours * '.(($assignment->billable == 1) ? 'invoice_rate' : 'internal_cost_rate + external_cost_rate').' ELSE 0 END) AS value')->where('customer_po', '=', $assignment->customer_po)->where('project_id', '=', $assignment->project_id)->first();
            $rate = ($assignment->billable == 1) ? $assignment->invoice_rate : ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            array_push($working_days, $calendar);
            array_push($actual_hours, $time);
            array_push($hours_forecast, $forecast_days * $capacity_allocation_hours);
            array_push($assignment_months, $number_of_month);
            array_push($total_project_budget, $budget);
            array_push($working_hours, $calendar * 8);
            array_push($invoice_rate, $rate);
            array_push($po_value, $assignment->hours * $rate);
            array_push($planned_spend, ($time->hours * $rate) + (($forecast_days * 8) * $rate));
            array_push($variance_po, ($assignment->hours - $assignment_days) * $capacity_allocation_hours);

            $year_week_array = [];
            $actual_hours_array = [];
            $forecast_days_remaining = $forecast_days;
            $forecast_hours_array = [];
            for ($i = $year; $i <= date('Y'); $i++) {
                for ($j = 1; $j <= 12; $j++) {
                    $j = $j < 10 ? '0'.$j : $j;

                    $actual_hours_captured = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.project_id', '=', $assignment->project_id)
                        ->where('timesheet.employee_id', '=', $assignment->employee_id)
                        ->where('timesheet.year', '=', $i)
                        ->where('timesheet.month', '=', $j)
                        ->first();

                    $year_week_array[] = $i.$j;
                    $actual_hours_array[] = $actual_hours_captured;
                    if ($i == date('Y') && $j >= date('m')) {
                        $days_for_month = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->where('year', '=', $i)->where('month', '=', $j)->whereBetween('date', [$date->now()->toDateString(), $assignment->planned_end_date])->get()->count();
                        $days_for_month = $days_for_month > 0 ? $days_for_month - $resource_leave_days_taken_now : $days_for_month;
                        $month_forecast_hours = $days_for_month * $capacity_allocation_hours;

                        $forecast_hours_array[] = $month_forecast_hours;
                        $forecast_days_remaining -= $month_forecast_hours;
                    } else {
                        $forecast_hours_array[] = '';
                    }
                }
            }

            $assignment_actual_hours_array[] = $actual_hours_array;
            $assignment_forecast_hours_array[] = $forecast_hours_array;
        }

        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', '');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');

        $parameters = [
            'company_drop_down' => Company::filters()->select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_drop_down' => Team::filters()->select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_drop_down' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id')->unique(),
            'assignments' => $assignments,
            'company' => Company::orderBy('company_name')->whereNotNull('company_name')->where('company_name', '!=', '')->pluck('company_name', 'id')->prepend('All', ''),
            'projects' => $project_drop_down,
            'resource' => $resource_drop_down,
            'working_days' => $working_days,
            'actual_hours' => $actual_hours,
            'hours_forecast' => $hours_forecast,
            'assignment_months' => $assignment_months,
            'total_project_budget' => $total_project_budget,
            'working_hours' => $working_hours,
            'invoice_rate' => $invoice_rate,
            'po_value' => $po_value,
            'planned_spend' => $planned_spend,
            'variance_po' => $variance_po,
            'assignment_actual_hours_array' => $assignment_actual_hours_array,
            'assignment_forecast_hours_array' => $assignment_forecast_hours_array,
            'year_week_array' => $year_week_array,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'assignmentplanning');

        return view('reports.assignmentplanning')->with($parameters);
    }

    public function status(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 15;

        $date = Carbon::now();
        $assignments = Assignment::with(['project','statusd'])->whereIn('assignment_status', [2, 3]);

        if ($request->has('project') && $request->input('project') != '') {
            $assignments = $assignments->where('project_id', $request->input('project'));
        }

        if ($request->has('company') && $request->input('company') != '') {
            $assignments = $assignments->where('company_id', $request->input('company'));
        }

        if ($request->has('resource') && $request->input('resource') != '') {
            $assignments = $assignments->where('employee_id', $request->input('resource'));
        }

        $assignments = $assignments->sortable(['name' => 'asc'])->paginate($item);

        $working_days = [];
        $actual_hours = [];
        $hours_forecast = [];
        $assignment_months = [];
        $total_project_budget = [];
        $working_hours = [];
        $invoice_rate = [];
        $po_value = [];
        $planned_spend = [];
        $variance_po = [];
        $customer_name = [];
        $capacity_allocation_hours_array = [];
        $days_before_assignment_end_date_array = [];
        $total_expense_amount_array = [];
        $total_actual_expense_claim_array = [];
        $year_week_array = [];

        $assignment_actual_hours_array = [];
        $assignment_forecast_hours_array = [];

        $year = 2017;
        if ($request->has('year') && $request->input('year') != '') {
            $year = $request->input('year');
        }

        foreach ($assignments as $assignment) {
            $leave_days_from_now = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $date->now()->toDateString())->where('date_to', '<=', $assignment->planned_end_date)->get();
            $leave_days_from_planned_start = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $assignment->planned_start_date)->where('date_to', '<=', $assignment->planned_end_date)->get();

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_now = 0;
            foreach ($leave_days_from_now as $leave_day_from_now) {
                $resource_leave_days_taken_now += $leave_day_from_now->no_of_days;
            }

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_start = 0;
            foreach ($leave_days_from_planned_start as $leave_day_from_start) {
                $resource_leave_days_taken_start += $leave_day_from_start->no_of_days;
            }

            //Total Expenses block - start
            $timesheets = DB::select("SELECT timesheet.id, timesheet.invoice_date, timesheet.invoice_due_date, timesheet.bill_status, timesheet.year_week, timesheet.inv_ref,
            timesheet.vendor_invoice_status, timesheet.vendor_invoice_date, timesheet.vendor_due_date, timesheet.vendor_reference, timesheet.invoice_number, timesheet.vendor_invoice_number,
            SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS bill_hours,
            SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS non_bill_hours,vendor_invoice_status.description as inv_status,invoice_status.description as bil_status 
            FROM `timesheet` left join timeline on timesheet.id = timeline.timesheet_id 
            left join vendor_invoice_status on timesheet.vendor_invoice_status = vendor_invoice_status.id 
            left join invoice_status on timesheet.bill_status = invoice_status.id 
            where timesheet.employee_id ='".$assignment->employee_id."' and timesheet.project_id ='".$assignment->project_id."' and (timesheet.bill_status = '2' or timesheet.vendor_invoice_status = '1') group by timesheet.vendor_reference,timesheet.vendor_invoice_status,timesheet.vendor_invoice_date,timesheet.vendor_due_date,timesheet.id,timesheet.invoice_date,timesheet.invoice_due_date,timesheet.year_week,timesheet.inv_ref,timesheet.bill_status,timesheet.vendor_invoice_number,timesheet.invoice_number,vendor_invoice_status.description,invoice_status.description");

            $time_exp = [];

            foreach ($timesheets as $timesheet) {
                if ($timesheet->bill_status == 2) {
                    $expense = TimeExp::selectRaw('description, account_id, amount, account_element_id, SUM(CASE WHEN is_billable = 1 THEN amount ELSE 0 END) AS billable_expense,
                        SUM(CASE WHEN claim = 1 THEN amount ELSE 0 END) AS claimable_expense')
                        ->where('timesheet_id', '=', $timesheet->id)
                        ->groupBy('description')
                        ->groupBy('account_element_id')
                        ->groupBy('account_id')
                        ->groupBy('amount')
                        ->first();
                    array_push($time_exp, $expense);
                }
            }

            $time_exp = array_filter($time_exp, 'strlen');

            $total_expense_amount = 0;
            $total_actual_expense_claim = 0;
            foreach ($time_exp as $exp) {
                $total_expense_amount += $exp->amount;
                $total_actual_expense_claim += $exp->claimable_expense;
            }
            //Total Expenses block - end

            $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $calendar = $calendar > 0 ? $calendar - $resource_leave_days_taken_start : $calendar;

            $days_before_assignment_end_date = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [date('Y-m-d'), $assignment->planned_end_date])->get()->count();
            $days_before_assignment_end_date = $days_before_assignment_end_date > 0 ? $days_before_assignment_end_date - $resource_leave_days_taken_now : $days_before_assignment_end_date;

            $forecast_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $assignment->planned_end_date])->get()->count();
            $forecast_days = $forecast_days > 0 ? $forecast_days - $resource_leave_days_taken_now : $forecast_days;
            $assignment_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $assignment_days = $assignment_days > 0 ? $assignment_days - $resource_leave_days_taken_start : $assignment_days;
            $number_of_month = Calendar::select('month', 'year')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->groupBy('month', 'year')->get()->count();
            $time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();

            $project = Project::where('id', '=', $assignment->project_id)->first();
            $customer = Customer::find($project->customer_id);

            $capacity_allocation_hours = $assignment->capacity_allocation_hours > 0 ? $assignment->capacity_allocation_hours : 8;
            $budget = Assignment::selectRaw('SUM(CASE WHEN billable = '.$assignment->billable?->value.' THEN hours * '.(($assignment->billable?->value == 1) ? 'invoice_rate' : 'internal_cost_rate + external_cost_rate').' ELSE 0 END) AS value')->where('customer_po', '=', $assignment->customer_po)->where('project_id', '=', $assignment->project_id)->first();
            $rate = ($assignment->billable == 1) ? $assignment->invoice_rate : ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            array_push($working_days, $calendar);
            array_push($actual_hours, $time);
            array_push($hours_forecast, $forecast_days * $capacity_allocation_hours);
            array_push($assignment_months, $number_of_month);
            array_push($total_project_budget, $budget);
            array_push($working_hours, $calendar * 8);
            array_push($invoice_rate, $rate);
            array_push($po_value, $assignment->hours * $rate);
            array_push($planned_spend, ($time->hours * $rate) + (($forecast_days * 8) * $rate));
            array_push($variance_po, ($assignment->hours - $assignment_days) * $capacity_allocation_hours);
            array_push($customer_name, $customer->customer_name);
            array_push($capacity_allocation_hours_array, $capacity_allocation_hours);
            array_push($days_before_assignment_end_date_array, $days_before_assignment_end_date);
            array_push($total_expense_amount_array, $total_expense_amount);
            array_push($total_actual_expense_claim_array, $total_actual_expense_claim);

            $year_week_array = [];
            $actual_hours_array = [];
            $forecast_days_remaining = $forecast_days;
            $forecast_hours_array = [];
            for ($i = $year; $i <= date('Y'); $i++) {
                for ($j = 1; $j <= 12; $j++) {
                    $j = $j < 10 ? '0'.$j : $j;

                    $actual_hours_captured = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.project_id', '=', $assignment->project_id)
                        ->where('timesheet.employee_id', '=', $assignment->employee_id)
                        ->where('timesheet.year', '=', $i)
                        ->where('timesheet.month', '=', $j)
                        ->first();

                    $year_week_array[] = $i.$j;
                    $actual_hours_array[] = $actual_hours_captured;
                    if ($i == date('Y') && $j >= date('m')) {
                        $days_for_month = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->where('year', '=', $i)->where('month', '=', $j)->whereBetween('date', [$date->now()->toDateString(), $assignment->planned_end_date])->get()->count();
                        $days_for_month = $days_for_month > 0 ? $days_for_month - $resource_leave_days_taken_now : $days_for_month;
                        $month_forecast_hours = $days_for_month * $capacity_allocation_hours;

                        $forecast_hours_array[] = $month_forecast_hours;
                        $forecast_days_remaining -= $month_forecast_hours;
                    } else {
                        $forecast_hours_array[] = '';
                    }
                }
            }

            $assignment_actual_hours_array[] = $actual_hours_array;
            $assignment_forecast_hours_array[] = $forecast_hours_array;
        }

        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', '');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');

        $parameters = [
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'company_drop_down' => Company::filters()->select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_drop_down' => Team::filters()->select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_drop_down' => User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'assignments' => $assignments,
            'projects' => $project_drop_down,
            'resource' => $resource_drop_down,
            'working_days' => $working_days,
            'actual_hours' => $actual_hours,
            'hours_forecast' => $hours_forecast,
            'assignment_months' => $assignment_months,
            'total_project_budget' => $total_project_budget,
            'working_hours' => $working_hours,
            'invoice_rate' => $invoice_rate,
            'po_value' => $po_value,
            'planned_spend' => $planned_spend,
            'variance_po' => $variance_po,
            'assignment_actual_hours_array' => $assignment_actual_hours_array,
            'assignment_forecast_hours_array' => $assignment_forecast_hours_array,
            'year_week_array' => $year_week_array,
            'customer_name' => $customer_name,
            'capacity_allocation_hours_array' => $capacity_allocation_hours_array,
            'days_before_assignment_end_date_array' => $days_before_assignment_end_date_array,
            'total_expense_amount_array' => $total_expense_amount_array,
            'total_actual_expense_claim_array' => $total_actual_expense_claim_array,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'assignmentstatus');

        return view('reports.assignmentstatus')->with($parameters);
    }
}
