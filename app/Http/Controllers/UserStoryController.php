<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Epic;
use App\Models\Feature;
use App\Http\Requests\UserStoryRequest;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\Sprint;
use App\Models\UserStory;
use App\Models\UserStoryDiscussion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Validator;

class UserStoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $userStories = UserStory::orderBy('id');
        /*if($request->has('p_id') && $request->input('p_id') != 0){
            $userStories = $userStories->where('project_id', '=', $request->input('p_id'))->get();
        }*/

        $userStories = $userStories->get();

        $parameters = [
            'userStories' => $userStories,
        ];

        return view('project.user_story.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $featuresDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $userStoryDropDown = UserStory::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $feature = Feature::find($request->input('feature_id'));

        $project_id = null;
        $from_task = 0;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            if ($request->has('from_task') && $request->input('from_task') > 0) {
                $from_task = $request->input('from_task');
            }

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $featuresDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('name', 'id')->prepend('Please select...', 0);
            $featuressArray = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('id')->toArray();
            $userStoryDropDown = UserStory::orderBy('name')->whereIn('feature_id', $featuressArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $parameters = [
            'feature' => $feature,
            'project_id' => $project_id,
            'from_task' => $from_task,
            'featuresDropDown' => $featuresDropDown,
            'userStoryDropDown' => $userStoryDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.user_story.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserStoryRequest $request): RedirectResponse
    {
        $userStory = new UserStory();
        $userStory->name = $request->input('name');
        $userStory->feature_id = $request->input('feature_id');
        $userStory->start_date = $request->input('start_date');
        $userStory->end_date = $request->input('end_date');
        $userStory->hours_planned = $request->input('hours_planned');
        $userStory->confidence_percentage = $request->input('confidence_percentage');
        $userStory->estimate_cost = $request->input('estimate_cost');
        $userStory->estimate_income = $request->input('estimate_income');
        $userStory->billable = $request->input('billable');
        $userStory->sprint_id = $request->input('sprint_id');
        $userStory->dependency_id = $request->input('dependency_id');
        $userStory->status_id = $request->input('status_id');
        $userStory->details = $request->input('details');
        $userStory->save();

        if ($request->has('from_task') && $request->input('from_task') > 0) {
            return redirect(route('task.create', $request->input('project_id')))->with('flash_success', 'User story created successfully.');
        }

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'User story created successfully.');
        }

        return redirect(route('userstory.index'))->with('flash_success', 'User story successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Epic  $userStory
     */
    public function show($id): View
    {
        $userStory = UserStory::with('feature.epic.project')->find($id);
        $featureDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $userStoryDropDown = UserStory::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $parameters = [
            'userStory' => $userStory,
            'featureDropDown' => $featureDropDown,
            'userStoryDropDown' => $userStoryDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.user_story.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Epic  $userStory
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $userStory = UserStory::find($id);

        if ($userStory->status_id == 4) {
            return redirect()->back()->with('flash_info', "You can't edit a completed user story");
        }
        $featureDropDown = Feature::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $userStoryDropDown = UserStory::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $feature = Feature::find($userStory->feature_id);

        $project_id = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->pluck('name', 'id')->prepend('Please select...', 0);
            $featureDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('name', 'id')->prepend('Please select...', 0);
            $featuressArray = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('id')->toArray();
            $userStoryDropDown = UserStory::orderBy('name')->whereIn('feature_id', $featuressArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $parameters = [
            'userStory' => $userStory,
            'feature' => $feature,
            'project_id' => $project_id,
            'featureDropDown' => $featureDropDown,
            'userStoryDropDown' => $userStoryDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.user_story.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Feature  $userStory
     */
    public function update(UserStoryRequest $request, $id): RedirectResponse
    {
        $userStory = UserStory::find($id);
        $tasks = $userStory->load(['tasks' => function ($task) {
            $task->where('status', '!=', 5);
        }]);

        if ($request->input('status_id') == 4) {
            if (! $tasks->tasks->isEmpty()) {
                return redirect()->back()->with('flash_info', 'This user story still have open tasks.');
            }
        }

        $userStory->name = $request->input('name');
        $userStory->feature_id = $request->input('feature_id');
        $userStory->start_date = $request->input('start_date');
        $userStory->end_date = $request->input('end_date');
        $userStory->hours_planned = $request->input('hours_planned');
        $userStory->confidence_percentage = $request->input('confidence_percentage');
        $userStory->estimate_cost = $request->input('estimate_cost');
        $userStory->estimate_income = $request->input('estimate_income');
        $userStory->billable = $request->input('billable');
        $userStory->sprint_id = $request->input('sprint_id');
        $userStory->dependency_id = $request->input('dependency_id');
        $userStory->status_id = $request->input('status_id');
        $userStory->details = $request->input('details');
        $userStory->save();

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'User story updated successfully.');
        }

        return redirect(route('userstory.index'))->with('flash_success', 'Feature successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feature  $userStory
     */
    public function destroy($id, Request $request): RedirectResponse
    {
        UserStory::destroy($id);

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'User story deleted successfully.');
        }

        return redirect(route('userstory.index'))->with('flash_success', 'User story successfully deleted.');
    }

    public function userStoryDiscuss(Request $request, $user_story_id): JsonResponse
    {
        $discussion = new UserStoryDiscussion();
        $discussion->user_story_id = $user_story_id;
        $discussion->user_id = auth()->id();
        $discussion->discussion = $request->discussion;
        $discussion->save();

        return response()->json(['message' => 'Your message was captured successfully']);
    }

    public function getDiscussion($user_story_id): JsonResponse
    {
        $discussion = UserStoryDiscussion::with('user')
            ->where('user_story_id', $user_story_id)
            ->orderBy('id', 'desc')->get();

        return response()->json(['discussion' => $discussion]);
    }

    public function APIupdateUserStory(Request $request, UserStory $userstory): JsonResponse
    {
        $userstory->name = $request->name;
        $userstory->sprint_id = $request->sprint_id;
        $userstory->dependency_id = $request->dependency_id;
        $userstory->status_id = $request->status_id;
        $userstory->details = $request->details;
        $userstory->start_date = $request->start_date;
        $userstory->end_date = $request->end_date;
        $userstory->billable = $request->billable;
        $userstory->hours_planned = $request->hours_planned;
        $userstory->save();

        return response()->json(['message' => 'User story updated successfully']);
    }

    public function getDocuments($userstory_id)
    {
        $documents = Document::orderBy('id')->where('document_type_id', 11)->where('reference_id', $userstory_id)->where('status_id', 1)->get();

        return $documents;
    }

    public function uploadFile($userstory_id, Request $request): JsonResponse
    {
        $file = 'userstory_'.date('Y_m_d_H_i_s').'.'.$request->file->getClientOriginalExtension();
        $fileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('files/userstory'), $file);

        $document = new Document();
        $document->name = $fileName;
        $document->type_id = 11;
        $document->file = $file;
        $document->document_type_id = 11;
        $document->reference = 'User Story';
        $document->reference_id = $userstory_id;
        $document->owner_id = $request->user_id;
        $document->creator_id = $request->user_id;
        $document->status_id = 1;
        $document->save();

        return response()->json(['success' => 'You have successfully upload file.']);
    }

    public function deleteDocument($document_id): Response
    {
        Document::destroy($document_id);

        return response()
            ->json([
                'success' => 'File successfully deleted.',
                'uri' => \request()->getUri(),
            ]);
    }

    public function getDropdowns(Request $request){
        $featuresDropDown = Feature::orderBy('name')->get(['name', 'id']);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->get(['description', 'id']);
        $sprintDropDown = Sprint::orderBy('name')->get(['name', 'id']);
        $userStoryDropDown = UserStory::orderBy('name')->get(['name', 'id']);

        $feature = Feature::find($request->input('feature_id'));

        $project_id = null;
        $from_task = 0;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            if ($request->has('from_task') && $request->input('from_task') > 0) {
                $from_task = $request->input('from_task');
            }

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $sprintDropDown = Sprint::orderBy('name')->where('project_id', '=', $project_id)->get(['name', 'id']);
            $featuresDropDown = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->get(['name', 'id']);
            $featuressArray = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('id')->toArray();
            $userStoryDropDown = UserStory::orderBy('name')->whereIn('feature_id', $featuressArray)->get(['name', 'id']);
        }

        $parameters = [
            'feature' => $feature,
            'project_id' => $project_id,
            'from_task' => $from_task,
            'featuresDropDown' => $featuresDropDown,
            'userStoryDropDown' => $userStoryDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return response()->json($parameters);
    }

    private function userStoryValidation(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'name' => 'string|required',
            'feature_id' => 'integer|required|not_in:0',
            'status_id' => 'integer|required|not_in:0',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'billable' => 'numeric|nullable',
            'hours_planned' => 'numeric|nullable',
            'estimate_cost' => 'numeric|nullable',
            'estimate_income' => 'numeric|nullable',
            'confidence_percentage' => 'numeric|nullable',  
        ]);
    }

    public function saveUserStory(Request $request){
        $validator = $this->userStoryValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $user_story = new UserStory();
        $user_story->name = $request->name??null;
        $user_story->feature_id = $request->feature_id??null;
        $user_story->dependency_id = $request->dependency_id??null;
        $user_story->start_date = $request->start_date??null;
        $user_story->end_date = $request->end_date??null;
        $user_story->hours_planned = $request->hours_planned??null;
        $user_story->confidence_percentage = $request->confidence_percentage??null;
        $user_story->estimate_cost = $request->estimate_cost??null;
        $user_story->estimate_income = $request->estimate_income??null;
        $user_story->billable = $request->billable??0;
        $user_story->sprint_id = $request->sprint_id??null;
        $user_story->status_id = $request->status_id??null;
        $user_story->details = $request->details??null;
        $user_story->save();
        
        $parameters = [
            'user_story' => UserStory::with('tasks','tasks.timelines','tasks.consultant')->where('id',$user_story->id)->first()
        ];

        return response()->json($parameters);
    }

    public function updateUserStory(Request $request){
        $validator = $this->userStoryValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        
        $user_story = UserStory::find($request->id);
        $user_story->name = $request->name??null;
        $user_story->feature_id = $request->feature_id??null;
        $user_story->dependency_id = $request->dependency_id??null;
        $user_story->start_date = $request->start_date??null;
        $user_story->end_date = $request->end_date??null;
        $user_story->hours_planned = $request->hours_planned??null;
        $user_story->confidence_percentage = $request->confidence_percentage??null;
        $user_story->estimate_cost = $request->estimate_cost??null;
        $user_story->estimate_income = $request->estimate_income??null;
        $user_story->billable = $request->billable??0;
        $user_story->sprint_id = $request->sprint_id??null;
        $user_story->status_id = $request->status_id??null;
        $user_story->details = $request->details??null;
        $user_story->save();
        
        $parameters = [
            'user_story' => UserStory::with('tasks','tasks.timelines','tasks.consultant')->where('id',$user_story->id)->first()
        ];

        return response()->json($parameters);
    }

    public function deleteUserStory(Request $request){
        UserStory::destroy($request->user_story_id);
        
        return response()->json(['message'=>'User Story successfully deleted.']);
    }

    public function moveStory(Request $request, $id)
    {
        $userStory = UserStory::findOrFail($id);
        $userStory->feature_id = $request->feature_id;
        $userStory->save();

        return response()->json([
            'message' => 'User Story updated successfully!',
            'user_story' => $userStory
        ]);
    }

}
