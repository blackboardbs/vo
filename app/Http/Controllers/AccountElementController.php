<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountElement;
use App\Http\Requests\StoreAccountElementRequest;
use App\Http\Requests\UdateAccountElementRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AccountElementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $account_element = AccountElement::sortable('description', 'asc')->paginate($item);

        $account_element = AccountElement::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $account_element = AccountElement::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $account_element = AccountElement::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $account_element = $account_element->where('description', 'like', '%'.$request->input('q').'%');
        }

        $account_element = $account_element->paginate($item);

        $parameters = [
            'account_element' => $account_element,
        ];

        return View('master_data.account_element.index', $parameters);
    }

    public function create(): View
    {
        $account_elements = AccountElement::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => Account::orderBy('id')->pluck('description', 'id')->prepend('AccountElement', '0'),
            'account_elements' => $account_elements,
        ];

        return View('master_data.account_element.create')->with($parameters);
    }

    public function store(StoreAccountElementRequest $request): RedirectResponse
    {
        $account_element = new AccountElement();
        $account_element->description = $request->input('description');
        $account_element->status = $request->input('status');
        $account_element->account_id = $request->input('account_id');
        $account_element->account_element = $request->input('account_element');
        $account_element->creator_id = auth()->id();
        $account_element->save();

        return redirect(route('account_element.index'))->with('flash_success', 'Master Data AccountElement Element captured successfully');
    }

    public function show($account_element_id): View
    {
        $parameters = [
            'account_element' => AccountElement::where('id', '=', $account_element_id)->get(),
        ];
        //return $parameters;
        return View('master_data.account_element.show')->with($parameters);
    }

    public function edit($account_element_id): View
    {
        $account_elements = AccountElement::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'account_element' => AccountElement::where('id', '=', $account_element_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => Account::orderBy('id')->pluck('description', 'id')->prepend('AccountType', '0'),
            'account_elements' => $account_elements,
        ];

        return View('master_data.account_element.edit')->with($parameters);
    }

    public function update(UdateAccountElementRequest $request, $account_element_id): RedirectResponse
    {
        $account_element = AccountElement::find($account_element_id);
        $account_element->description = $request->input('description');
        $account_element->status = $request->input('status');
        $account_element->account_id = $request->input('account_id');
        $account_element->account_element = $request->input('account_element');
        $account_element->creator_id = auth()->id();
        $account_element->save();

        return redirect(route('account_element.index'))->with('flash_success', 'Master Data AccountElement Element saved successfully');
    }

    public function destroy($account_element_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // AccountElement::destroy($account_element_id);

        $item = AccountElement::find($account_element_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('account_element.index')->with('success', 'Master Data AccountElement Element suspended successfully');
    }
}
