<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlannedExpenseCopyRequest;
use App\Http\Requests\StorePlannedExpenseRequest;
use App\Http\Requests\UpdatePlannedExpenseRequest;
use App\Models\PlannedExpense;
use App\Models\PlannedExpenseView;
use App\Services\PlannedExpenseService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Carbon\Carbon;

class PlannedExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, PlannedExpenseService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasViewPermission(), 403);

        // dd($request);

        $expenses = PlannedExpense::with(['account:id,description', 'account_element:id,description', 'status_desc:id,description'])
            ->select(['id', 'plan_exp_date', 'account_id', 'account_element_id', 'description', 'plan_exp_value', 'status'])
            ->orderBy('description', 'asc')
            ->when($request->q, fn($planned) => $planned->where(fn($expense) => $expense->where(fn($query) => $query->where('description', 'LIKE', '%'.$request->input('q').'%')
                ->orWhereHas('account', function ($query) use ($request) {
                    $query->where('description', 'LIKE', '%'.$request->input('q').'%');
                })
                ->orWhereHas('account_element', function ($query) use ($request) {
                    $query->where('description', 'LIKE', '%'.$request->input('q').'%');
                }))))
            ->when($request->company_id, fn($expense) => $expense->where('company_id', '=', $request->company_id))
            ->when($request->account_id, fn($expense) => $expense->where('account_id', $request->account_id))
            ->when($request->account_element_id, fn($expense) => $expense->where('account_element_id', '=', $request->account_element_id))
            ->when($request->from, fn($expense) => $expense->where('plan_exp_date', '>=', $request->from))
            ->when($request->to, fn($expense) => $expense->where('plan_exp_date', '<=', $request->to))
            ->when($request->plan_exp_filter, fn($expense) => $expense->where('plan_exp_date', '<=', $request->to))
            // ->when($request->plan_exp_company, fn($expense) => $expense->where('company_id', '=', $request->plan_exp_company))
            // ->when($request->period, fn($expense) => $expense->where('plan_exp_date', '>=', Carbon::now()->subMonth()))
            ->latest('plan_exp_date')
            ->paginate($request->input('r') ?? 15);

        $planned_expenses = PlannedExpenseView::all();
        $income_expenses = PlannedExpenseView::where('account_type_id', 1)->orderBy('description', 'asc')->get();
        $expense_expenses = PlannedExpenseView::where('account_type_id', 2)->orderBy('description', 'asc')->get();
        $asset_expenses = PlannedExpenseView::where('account_type_id', 3)->orderBy('description', 'asc')->get();
        $liability_expenses = PlannedExpenseView::where('account_type_id', 4)->orderBy('description', 'asc')->get();

        $parameters = [
            'expenses' => $expenses,
            'planned_expenses' => $planned_expenses,
            'income_expenses' => $income_expenses,
            'expense_expenses' => $expense_expenses,
            'asset_expenses' => $asset_expenses,
            'liability_expenses' => $liability_expenses,
            'source_range' => $service->yearMonthDropdown(2),
            'target_range' => $service->yearMonthDropdown(1,1),
            'can_create' => $service->hasCreatePermissions()
        ];

        if ($request->has('export')) return $this->export($parameters, 'plan_exp');

        return view('plan_exp.index')->with($parameters);
    }

    public function create(PlannedExpenseService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        return view('plan_exp.create');
    }

    public function store(StorePlannedExpenseRequest $request, PlannedExpense $expense, PlannedExpenseService $service): RedirectResponse
    {
        $service->createPlannedExpense($request, $expense);

        return redirect(route('plan_exp.index'))->with('flash_success', 'Expense captured successfully');
    }

    public function show(PlannedExpense $plan_exp, PlannedExpenseService $service): View
    {
        abort_unless($service->hasViewPermission(), 403);

        return view('plan_exp.show')->with(['plan_exp' => $plan_exp]);
    }

    public function edit(PlannedExpense $plan_exp, PlannedExpenseService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        return view('plan_exp.edit')->with(['plan_exp' => $plan_exp]);
    }

    public function update(UpdatePlannedExpenseRequest $request, PlannedExpense $plan_exp, PlannedExpenseService $service): RedirectResponse
    {
        $service->updatePlannedExpense($request, $plan_exp);
        
        return redirect(route('plan_exp.index'))->with('flash_success', 'Expense updated successfully');
    }

    public function destroy(PlannedExpense $plan_exp, PlannedExpenseService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $plan_exp->delete();

        return redirect()->route('plan_exp.index')->with('success', 'Expense deleted successfully');
    }

    public function copy(PlannedExpenseCopyRequest $request, PlannedExpenseService $service): RedirectResponse
    {
        $service->copyPlannedExpenses($request);

        return redirect()->back()->with('flash_success', 'Planned expenses copied successfully');
    }

}
