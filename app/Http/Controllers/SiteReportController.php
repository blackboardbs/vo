<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Http\Requests\SiteReportEscalationRequest;
use App\Http\Requests\SiteReportIssueRequest;
use App\Http\Requests\SiteReportNextStepRequest;
use App\Http\Requests\SiteReportRiskRequest;
use App\Http\Requests\SiteReportSuggestionRequest;
use App\Http\Requests\SiteReportTaskRequest;
use App\Http\Requests\StoreSiteReportRequest;
use App\Http\Requests\UpdateSiteReportRequest;
use App\Jobs\SendSiteReportEmailJob;
use App\Models\Module;
use App\Models\Project;
use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Config;
use App\Models\Risk;
use App\Models\Company;
use App\Models\TaskStatus;
use App\Models\CustomerSiteReportTemplate;
use App\Models\CustomerSiteReport;
use App\Models\CustomerSiteReportActual;
use App\Models\CustomerSiteReportPlanned;
use App\Models\CustomerSiteReportHoursSummary;
use App\Models\CustomerSiteReportRisks;
// use App\Models\SiteReportEscalation;
// use App\Models\SiteReportIssue;
// use App\Models\SiteReportNextStep;
// use App\Models\SiteReportRisk;
// use App\Models\SiteReportSuggestion;
// use App\Models\SiteReportTask;
// use App\Models\Status;
use App\Services\SiteReportService;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelPdf\Facades\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SiteReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $site_report = CustomerSiteReport::with(['template','project','resource','customer']);

        $module = Module::where('name', '=', \App\Models\SiteReport::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $site_report = $site_report->whereIn('employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $site_report->where('ref', 'like', '%'.$request->q.'%')
                ->orWhereHas('customer', function ($query) use ($request) {
                    $query->where('customer_name', 'like', '%'.$request->q.'%');
                })
                ->orWhereHas('project', function ($query) use ($request) {
                    $query->where('name', 'like', '%'.$request->q.'%');
                });
        }

        $site_report = $site_report->orderBy('id','desc')->sortable('id', 'desc')->paginate($item);

        $parameters = [
            'site_reports' => $site_report,
            'templates' => CustomerSiteReportTemplate::orderBy('name')->pluck('name','id')->prepend('Please select a template',0)
        ];
        // dd($parameters);

        return View('customer.site_report.index', $parameters);
    }

    public function create(Request $request): View
    {
        if($request->has('template')){
            $site_report = CustomerSiteReportTemplate::find($request->template);
        }
        $weeks = [];
        $now = Carbon::now();
        $prev = Carbon::now()->subWeek(1);
        $next = Carbon::now()->addWeek(1);
        
        $weeks['prev_week_start'] = $prev->year.''.$prev->weekOfYear.' ('.$prev->startOfWeek()->format('Y-m-d').')';
        $weeks['this_week_start'] = $now->year.''.$now->weekOfYear.' ('.$now->startOfWeek()->format('Y-m-d').')';
        $weeks['next_week_start'] = $next->year.''.$next->weekOfYear.' ('.$next->startOfWeek()->format('Y-m-d').')';
        $weeks['prev_week_end'] = $prev->year.''.$prev->weekOfYear.' ('.$prev->endOfWeek()->format('Y-m-d').')';
        $weeks['this_week_end'] = $now->year.''.$now->weekOfYear.' ('.$now->endOfWeek()->format('Y-m-d').')';
        $weeks['next_week_end'] = $next->year.''.$next->weekOfYear.' ('.$next->endOfWeek()->format('Y-m-d').')';
        
        $parameters = [
            'site_report' => $site_report??[],
            'is_template' => $request->has('template') && $request->template > 0 ? true : false,
            'configs' => Config::first(),
            'weeks' => $weeks,
            'reviewWeeks' => $this->getReviewWeeksOfYear(),
            'planWeeks' => $this->getPlanWeeksOfYear(),
            'reviewWeeksEnd' => $this->getReviewWeeksEndOfYear(),
            'planWeeksEnd' => $this->getPlanWeeksEndOfYear(),
            'customers' => Customer::with('projects')->where('id', '>', 0)->where('status', '=', 1)->whereHas('projects')->orderBy('customer_name', 'asc')->get(['customer_name','id']),
        ];
        
        return View('customer.site_report.create', $parameters);
    }

    public function store(Request $request): JsonResponse
    {
        $validator = $this->siteReportValidation($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        
        // $site_report = $request->has('id') && $request->input('id') > 0 ? CustomerSiteReportTemplate::find($request->input('id')) : new CustomerSiteReportTemplate();
        $site_report = new CustomerSiteReportTemplate();
        $site_report->name = $request->input('template_name')??null;
        $site_report->customer_id = $request->input('customer_id')??null;
        $site_report->project_id = $request->input('project_id')??null;
        $site_report->employee_id = $request->input('employee_id')??null;
        $site_report->review_week_from = substr($request->input('review_date_from'),0,6)??null;
        $site_report->review_week_to = substr($request->input('review_date_to'),0,6)??null;
        $site_report->review_date_from = substr($request->input('review_date_from'),8,18)??null;
        $site_report->review_date_to = substr($request->input('review_date_to'),8,18)??null;
        $site_report->plan_week_from = substr($request->input('plan_date_from'),0,6)??null;
        $site_report->plan_week_to = substr($request->input('plan_date_to'),0,6)??null;
        $site_report->plan_date_from = substr($request->input('plan_date_from'),8,18)??null;
        $site_report->plan_date_to = substr($request->input('plan_date_to'),8,18)??null;
        $site_report->include_consultant = $request->input('include_consultants')??0;
        $site_report->include_task_hours = $request->input('include_task_hours')??0;
        $site_report->include_summary_hours = $request->input('include_hours_summary')??0;
        $site_report->include_consultant_name = $request->input('include_consultant_name')??0;
        $site_report->include_report_to = $request->input('include_report_to')??0;
        $site_report->include_risks = $request->input('include_risks')??0;
        $site_report->include_company_logo = $request->input('include_company_logo')??0;
        $site_report->include_customer_logo = $request->input('include_customer_logo')??0;

        $report_to = Assignment::with('resource.resource.manager')->where('project_id',$request->input('project_id'))->first();
        $site_report->assigned_to_name = $report_to->report_to && $report_to->report_to != '' ? $report_to->report_to : ($report_to->resource->resource->manager ? $report_to->resource->resource->manager->first_name.' '.$report_to->resource->resource->manager->last_name : '');


        $site_report->site_report_title = $request->input('site_report_title')??'';
        $site_report->site_report_footer = $request->input('site_report_footer')??'';

        $site_report->save();

        return response()->json(['template_id'=>$site_report->id]);
    }

    public function show($site_report_id): View
    {
        $site_report = CustomerSiteReport::with(['actual','planned','hours_summary','risks'])->where('id',$site_report_id)->first();

        $customer = Customer::find($site_report->customer_id);
        $company = Company::orderBy('id','asc')->first();

        $parameters = [
            'site_report' => $site_report,
            'company' => $company,
            'customer' => $customer
        ];

        //return $parameters;

        return view('customer.site_report.show')->with($parameters);
    }

    public function edit($site_report_id): View
    {
        $site_report = CustomerSiteReport::with(['actual','planned','hours_summary','risks'])->where('id',$site_report_id)->first();
        // dd($site_report);
        $weeks = [];
        
        $now = Carbon::now();
        $prev = Carbon::now()->subWeek(1);
        $next = Carbon::now()->addWeek(1);
        // dd($now->week);
        $weeks['prev_week_start'] = $prev->year.''.$prev->weekOfYear.' ('.$prev->startOfWeek()->format('Y-m-d').')';
        $weeks['this_week_start'] = $now->year.''.$now->weekOfYear.' ('.$now->startOfWeek()->format('Y-m-d').')';
        $weeks['next_week_start'] = $next->year.''.$next->weekOfYear.' ('.$next->startOfWeek()->format('Y-m-d').')';
        $weeks['prev_week_end'] = $prev->year.''.$prev->weekOfYear.' ('.$prev->endOfWeek()->format('Y-m-d').')';
        $weeks['this_week_end'] = $now->year.''.$now->weekOfYear.' ('.$now->endOfWeek()->format('Y-m-d').')';
        $weeks['next_week_end'] = $next->year.''.$next->weekOfYear.' ('.$next->endOfWeek()->format('Y-m-d').')';

        $parameter = [
            'site_report' => $site_report,
            'configs' => Config::first(),
            'weeks' => $weeks,
            'reviewWeeks' => $this->getReviewWeeksOfYear(),
            'planWeeks' => $this->getPlanWeeksOfYear(),
            'reviewWeeksEnd' => $this->getReviewWeeksEndOfYear(),
            'planWeeksEnd' => $this->getPlanWeeksEndOfYear(),
            'customers' => Customer::with('projects')->where('id', '>', 0)->where('status', '=', 1)->whereHas('projects')->orderBy('customer_name', 'asc')->get(['customer_name','id']),
        ];

        return view('customer.site_report.edit')->with($parameter);
    }

    public function destroy($site_report_id): RedirectResponse
    {
        CustomerSiteReport::destroy($site_report_id);

        return redirect()->route('site_report.index')->with('success', 'Site report deleted successfully');
    }

    public function getCustomerProjects(Request $request, $customer_id){
        $customer = Customer::find($customer_id);
        $company = Company::orderBy('id','asc')->first();
        $projects = Project::where('customer_id',$customer_id)->orderBy('name','asc')->get();

        return response()->json(['projects' => $projects??[],'customer'=>$customer,'company'=>$company]);
    }

    public function getProjectConsultants(Request $request, $project_id){
        $projects = Assignment::select('employee_id')->where('project_id',$project_id)->get();
        $resources = User::whereIn('id',collect($projects)->toArray())->get();

        return response()->json(['consultants' => $resources??[]]);
    }

    private function siteReportValidation(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'customer_id' => 'required|not_in:0',
            'project_id' => 'required|not_in:0',
        ]);
    }

    public function getReviewWeeksOfYear()
    {
        // Get today's date
        $today = Carbon::now();
        $lastWeek = Carbon::now()->subWeek(1);
        
        // Get the week of the year for today
        $prevWeekOfYear = $today->copy()->subWeek(1)->weekOfYear;
        $currentWeekOfYear = $today->weekOfYear;

        // Initialize an array to store the weeks
        $weeks = [];


        // Get the next 4 weeks
        for ($i = 1; $i <= 4; $i++) {
            $weekDate = $today->copy()->addWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->startOfWeek()->year.''.$weekDate->copy()->startOfWeek()->weekOfYear.' ('.$weekDate->copy()->startOfWeek()->format('Y-m-d').')'
            ];
        }

        // Add today's week
        $weeks[] = [
            'date' => $today->copy()->startOfWeek()->year.''.$today->copy()->startOfWeek()->weekOfYear.' ('.$today->copy()->startOfWeek()->format('Y-m-d').')'
        ];

        // Add previous week
        $weeks[] = [
            'date' => $lastWeek->copy()->startOfWeek()->year.''.$lastWeek->copy()->startOfWeek()->weekOfYear.' ('.$lastWeek->copy()->startOfWeek()->format('Y-m-d').')'
        ];

        // Get the previous 12 weeks
        for ($i = 12; $i > 0; $i--) {
            $weekDate = $lastWeek->copy()->subWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->startOfWeek()->year.''.$weekDate->copy()->startOfWeek()->weekOfYear.' ('.$weekDate->copy()->startOfWeek()->format('Y-m-d').')'
            ];
        }

        usort($weeks, function ($a, $b) {
            return strcmp($b['date'], $a['date']);
        });

        return $weeks;
    }
    
    public function getPlanWeeksOfYear()
    {
        // Get today's date
        $today = Carbon::now();
        $lastWeek = $today->copy()->subWeek(1);
        
        // Get the week of the year for today
        $prevWeekOfYear = $today->copy()->subWeek(1)->weekOfYear;
        $currentWeekOfYear = $today->weekOfYear;

        // Initialize an array to store the weeks
        $weeks = [];


        // Get the next 12 weeks
        for ($i = 1; $i <= 12; $i++) {
            $weekDate = $today->copy()->addWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->startOfWeek()->year.''.$weekDate->copy()->startOfWeek()->weekOfYear.' ('.$weekDate->copy()->startOfWeek()->format('Y-m-d').')'
            ];
        }

        // Add today's week
        $weeks[] = [
            'date' => $today->copy()->startOfWeek()->year.''.$today->copy()->startOfWeek()->weekOfYear.' ('.$today->copy()->startOfWeek()->format('Y-m-d').')'
        ];

        // Add previous week
        $weeks[] = [
            'date' => $lastWeek->copy()->startOfWeek()->year.''.$lastWeek->copy()->startOfWeek()->weekOfYear.' ('.$lastWeek->copy()->startOfWeek()->format('Y-m-d').')'
        ];

        usort($weeks, function ($a, $b) {
            return strcmp($b['date'], $a['date']);
        });

        return $weeks;
    }
    public function getReviewWeeksEndOfYear()
    {
        // Get today's date
        $today = Carbon::now();
        $lastWeek = Carbon::now()->subWeek(1);
        
        // Get the week of the year for today
        $prevWeekOfYear = $today->copy()->subWeek(1)->weekOfYear;
        $currentWeekOfYear = $today->weekOfYear;

        // Initialize an array to store the weeks
        $weeks = [];


        // Get the next 4 weeks
        for ($i = 1; $i <= 4; $i++) {
            $weekDate = $today->copy()->addWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->startOfWeek()->year.''.$weekDate->copy()->endOfWeek()->weekOfYear.' ('.$weekDate->copy()->endOfWeek()->format('Y-m-d').')'
            ];
        }

        // Add today's week
        $weeks[] = [
            'date' => $today->copy()->endOfWeek()->year.''.$today->copy()->endOfWeek()->weekOfYear.' ('.$today->copy()->endOfWeek()->format('Y-m-d').')'
        ];

        // Add previous week
        $weeks[] = [
            'date' => $lastWeek->copy()->endOfWeek()->year.''.$lastWeek->copy()->endOfWeek()->weekOfYear.' ('.$lastWeek->copy()->endOfWeek()->format('Y-m-d').')'
        ];

        // Get the previous 12 weeks
        for ($i = 12; $i > 0; $i--) {
            $weekDate = $lastWeek->copy()->subWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->endOfWeek()->year.''.$weekDate->copy()->endOfWeek()->weekOfYear.' ('.$weekDate->copy()->endOfWeek()->format('Y-m-d').')'
            ];
        }

        usort($weeks, function ($a, $b) {
            return strcmp($b['date'], $a['date']);
        });

        return $weeks;
    }
    
    public function getPlanWeeksEndOfYear()
    {
        // Get today's date
        $today = Carbon::now();
        $lastWeek = $today->copy()->subWeek(1);
        
        // Get the week of the year for today
        $prevWeekOfYear = $today->copy()->subWeek(1)->weekOfYear;
        $currentWeekOfYear = $today->weekOfYear;

        // Initialize an array to store the weeks
        $weeks = [];


        // Get the next 12 weeks
        for ($i = 1; $i <= 12; $i++) {
            $weekDate = $today->copy()->addWeeks($i);
            $weeks[] = [
                'date' => $weekDate->copy()->endOfWeek()->year.''.$weekDate->copy()->endOfWeek()->weekOfYear.' ('.$weekDate->copy()->endOfWeek()->format('Y-m-d').')'
            ];
        }

        // Add today's week
        $weeks[] = [
            'date' => $today->copy()->endOfWeek()->year.''.$today->copy()->endOfWeek()->weekOfYear.' ('.$today->copy()->endOfWeek()->format('Y-m-d').')'
        ];

        // Add previous week
        $weeks[] = [
            'date' => $lastWeek->copy()->endOfWeek()->year.''.$lastWeek->copy()->endOfWeek()->weekOfYear.' ('.$lastWeek->copy()->endOfWeek()->format('Y-m-d').')'
        ];

        usort($weeks, function ($a, $b) {
            return strcmp($b['date'], $a['date']);
        });

        return $weeks;
    }

    public function getActivities(Request $request){
        if($request->has('incl_consultant') && $request->incl_consultant == 'yes'){
            try {
                if($request->has('employee_id') && $request->employee_id > 0){
                    $query = DB::select('CALL site_report_detail_actual_1_sproc('.substr($request->from, 0, 6).','.substr($request->to, 0, 6).','.$request->project_id.','.$request->employee_id.')');
                } else {
                    $query = DB::select('CALL site_report_detail_actual_1_sproc('.substr($request->from, 0, 6).','.substr($request->to, 0, 6).','.$request->project_id.',NULL)');
                }
            } catch (QueryException $ex) {
                // Handle the exception
                dd($ex->getMessage());
            }
        } else {
            try {
                $query = DB::select('CALL site_report_detail_actual_2_sproc('.substr($request->from, 0, 6).','.substr($request->to, 0, 6).','.$request->project_id.',NULL)');
            } catch (QueryException $ex) {
                // Handle the exception
                dd($ex->getMessage());
            }
        }

        return $query;
    }

    public function getPlannedActivities(Request $request){
        // 202430 (2024-01-01)
        // 8,10
        // return substr($request->to, 8,10);s  
        if($request->has('incl_consultant') && $request->incl_consultant == 'yes'){
            try {
                if($request->has('employee_id') && $request->employee_id > 0){
                    $query = DB::select('CALL site_report_detail_plan_1_sproc("'.substr($request->to, 8,10).'","'.substr($request->from, 8,10).'",'.$request->project_id.','.$request->employee_id.')');
                } else {
                    $query = DB::select('CALL site_report_detail_plan_1_sproc("'.substr($request->to, 8,10).'","'.substr($request->from, 8,10).'",'.$request->project_id.',NULL)');
                }
            } catch (QueryException $ex) {
                // Handle the exception
                dd($ex->getMessage());
            }
        } else {
            try {
                $query = DB::select('CALL site_report_detail_plan_2_sproc("'.substr($request->to, 8,10).'","'.substr($request->from, 8,10).'",'.$request->project_id.',NULL)');
            } catch (QueryException $ex) {
                // Handle the exception
                dd($ex->getMessage());
            }
        }

        return $query;
    }

    public function getHoursSummary(Request $request){
        try {
            $query = DB::select('CALL site_report_hours_summary_sproc('.substr($request->from, 0, 6).','.$request->project_id.',NULL)');
        } catch (QueryException $ex) {
            // Handle the exception
            dd($ex->getMessage());
        }
        
        return $query;
    }

    public function getSiteRisks(Request $request, $project_id){
        $query = Risk::with(['likeylihood','impact'])->where('project_id',$project_id)->get();

        $risks = $query->map(function ($risk) {
            return (object) [
                'name' => $risk->name,
                'likelihood' => $risk->likeylihood?->name,
                'impact' => $risk->impact?->name,
            ];
        });

        return response()->json(['risks' => $risks??[]]);
    }

    public function getTempHeader(Request $request, SiteReportService $service){
        $project = Project::find($request->project_id);
        $customer = Customer::find($request->customer_id);
        
        if($request->has('employee_id') && $request->employee_id > 0){
            $resource = User::where('id',$request->employee_id)->first();
        }
        if($request->has('project_id') && $request->project_id > 0){
        $report_to = Assignment::with('resource.resource.manager')->where('project_id',$request->project_id)->first();
        $assigned_to_name = $report_to->report_to && $report_to->report_to != '' ? $report_to->report_to : ($report_to->resource->resource->manager ? $report_to->resource->resource->manager->first_name.' '.$report_to->resource->resource->manager->last_name : '');
        }

        return response()->json(['project' => $project,'ref'=>$service->refNumberString(),'report_to'=>$assigned_to_name??'','employee'=>$resource??null,'customer_contact'=>$customer,'company_contact'=>isset($this->sysConfig()->company_id)?Company::find($this->sysConfig()->company_id):Company::first()]);
    }

    public function copySiteReport(Request $request, $site_report_id, SiteReportService $service){

        $old_site_report = CustomerSiteReport::find($site_report_id);

        $site_report = new CustomerSiteReport();
        $site_report->template_id = $old_site_report->template_id??null;
        $site_report->ref = $service->refNumberString();
        $site_report->customer_id = $old_site_report->customer_id??null;
        $site_report->project_id = $old_site_report->project_id??null;
        $site_report->project_name = $old_site_report->project_name??null;
        $site_report->employee_id = $old_site_report->employee_id??null;
        $site_report->consultant_name = $old_site_report->consultant_name??null;
        $site_report->review_week_from = $old_site_report->review_week_from??null;
        $site_report->review_week_to = $old_site_report->review_week_to??null;
        $site_report->review_date_from = $old_site_report->review_date_from??null;
        $site_report->review_date_to = $old_site_report->review_date_to??null;
        $site_report->plan_week_from = $old_site_report->plan_week_from??null;
        $site_report->plan_week_to = $old_site_report->plan_week_to??null;
        $site_report->plan_date_from = $old_site_report->plan_date_from??null;
        $site_report->plan_date_to = $old_site_report->plan_date_to??null;
        $site_report->include_consultant = $old_site_report->include_consultant??0;
        $site_report->include_task_hours = $old_site_report->include_task_hours??0;
        $site_report->include_summary_hours = $old_site_report->include_summary_hours??0;
        $site_report->include_consultant_name = $old_site_report->include_consultant_name??0;
        $site_report->include_report_to = $old_site_report->include_report_to??0;
        $site_report->report_to = $old_site_report->report_to??null;
        $site_report->include_risks = $old_site_report->include_risks??0;
        $site_report->include_company_logo = $old_site_report->include_company_logo??0;
        $site_report->include_customer_logo = $old_site_report->include_customer_logo??0;

        $site_report->assigned_to_name = $old_site_report->assigned_to_name;

        $site_report->site_report_title = $old_site_report->site_report_title??'';
        $site_report->site_report_footer = $old_site_report->site_report_footer??'';

        $site_report->save();

        $old_actuals = CustomerSiteReportActual::where('site_report_id',$site_report_id)->get();
        
        foreach($old_actuals as $old_actual){
            $actual = new CustomerSiteReportActual();
            $actual->site_report_id = $site_report->id;
            $actual->employee_name = $old_actual->employee_name;
            $actual->task_status = $old_actual->task_status;
            $actual->task = $old_actual->task;
            $actual->total_hours = $old_actual->total_hours;
            $actual->hours_billable = $old_actual->hours_billable;
            $actual->hours_non_billable = $old_actual->hours_non_billable;
            $actual->notes = $old_actual->notes;
            $actual->created_by = auth()->user()->id;
            $actual->save();
            
        }

        $old_planned = CustomerSiteReportPlanned::where('site_report_id',$site_report_id)->get();

        foreach($old_planned as $old_plan){
            $plan = new CustomerSiteReportPlanned();
            $plan->site_report_id = $site_report->id;
            $plan->employee_name = $old_plan->employee_name;
            $plan->task_status = $old_plan->task_status;
            $plan->task = $old_plan->task;
            $plan->actual_hours = $old_plan->actual_hours;
            $plan->hours_available = $old_plan->hours_available;
            $plan->hours_planned = $old_plan->hours_planned;
            $plan->notes = $old_plan->notes;
            $plan->created_by = auth()->user()->id;
            $plan->save();
        }

        $old_summaries = CustomerSiteReportHoursSummary::where('site_report_id',$site_report_id)->get();

        foreach($old_summaries as $old_summary){
            $summary = new CustomerSiteReportHoursSummary();
            $summary->site_report_id = $site_report->id;
            $summary->as_at = $old_summary->as_at;
            $summary->po_hours_budgeted = $old_summary->po_hours_budgeted;
            $summary->po_billed_hours = $old_summary->po_billed_hours;
            $summary->po_hours_unbilled_previous = $old_summary->po_hours_unbilled_previous;
            $summary->po_hours_unbilled_last = $old_summary->po_hours_unbilled_last;
            $summary->po_hours_unbilled = $old_summary->po_hours_unbilled;
            $summary->po_hours_remaining = $old_summary->po_hours_remaining;
            $summary->save();
        }

        $old_risks = CustomerSiteReportRisks::where('site_report_id',$site_report_id)->get();

        foreach($old_risks as $old_risk){
            $risk = new CustomerSiteReportRisks();
            $risk->site_report_id = $site_report->id;
            $risk->name = $old_risk->name;
            $risk->likelihood = $old_risk->likelihood;
            $risk->impact = $old_risk->impact;
            $risk->save();
        }

        return redirect()->route('site_report.index')->with('flash_success', 'Site Report copied successfully!');
    }

    public function generateSiteReport(Request $request, SiteReportService $service){

        $site_report = $service->createSiteReport($request,new CustomerSiteReport);

        return redirect()->route('site_report.index')->with('flash_success', 'Site Report generated successfully!');
    }

    public function siteReportToPdf(Request $request, $site_report_id, SiteReportService $service){

        if (!$request->query->has('print')){        
            // if($request->has('id') && $request->id > 0){
            //     $site_report = CustomerSiteReport::with(['customer','actual','planned','hours_summary','risks'])->where('id',$site_report_id)->first();
            //     $service->updateSiteReport($request,$site_report);
            // } else {
                $site_report = $service->createSiteReport($request,new CustomerSiteReport);
            // }
            $customer = Customer::find($site_report->customer_id);
            $company = Company::orderBy('id','asc')->first();
        }

        if ($request->query->has('print')){
            $site_report = CustomerSiteReport::with(['customer','actual','planned','hours_summary','risks'])->where('id',$site_report_id)->first();
            $customer = Customer::find($site_report->customer_id);
            $company = Company::orderBy('id','asc')->first();
            $file_name = date('Y-m-d_H_i_s').'.pdf';

            $customer_name = str_replace(' ', '_', $site_report->customer->customer_name);

            if (! Storage::exists('site_report/'.$customer_name)) {
                Storage::makeDirectory('site_report/'.$customer_name);
            }

            $storage = storage_path('app/site_report/'.$customer_name.'/'.$file_name);
            
            return Pdf::view('customer.site_report.pdf', ['site_report'=>$site_report,'customer'=>$customer,'company'=>$company])
            ->margins(7, 7, 7, 7)
            ->landscape()->format('a4');
        }


        return response()->json(['site_report'=>$site_report]);
    }

    public function sendSiteReport(Request $request, SiteReportService $service)
    {
        // if($request->has('id') && $request->id > 0){
        //     $site_report = CustomerSiteReport::with(['customer','actual','planned','hours_summary','risks'])->where('id',$request->id)->first();
        //     $service->updateSiteReport($request,$site_report);
        // } else {
            $site_report = $service->createSiteReport($request,new CustomerSiteReport);
        // }
        $customer = Customer::find($site_report->customer_id);
        $company = Company::orderBy('id','asc')->first();

        $file_name = date('Y-m-d_H_i_s').'.pdf';

        $customer_name = str_replace(' ', '_', $site_report->customer->customer_name);

        if (! Storage::exists('site_report/'.$customer_name)) {
            Storage::makeDirectory('site_report/'.$customer_name);
        }
            
        $emails_array = $request->sendEmails;
        
        try {
            $multiple_emails = (isset($emails_array['emails']) && count($emails_array['emails']) > 0) ? $emails_array['emails'] : [];
            $emails = [];
            if (isset($multiple_emails)) {
                foreach ($multiple_emails as $user_email) {
                    array_push($emails, $user_email);
                }
            }
            
            $body = 'Please find site report attached for your attention';
               
            $storage = storage_path('app/site_report/'.$customer_name.'/'.$file_name);
        
            Pdf::view('customer.site_report.pdf', ['site_report'=>$site_report,'customer'=>$customer,'company'=>$company])
                            ->margins(7, 7, 7, 7)
                            ->landscape()->format('a4')->save($storage);

            // $document = new Document();
            // $document->document_type_id = 5;
            // $document->name = 'Customer Tax Invoice';
            // $document->file = $file_name;
            // $document->creator_id = auth()->id();
            // $document->owner_id = isset($customerInvoice->resource->employee_id) ? $customerInvoice->resource->employee_id : null;
            // $document->reference_id = 5;
            // $document->save();

            $subject = isset($emails_array['subject']) && $emails_array['subject'] != '' ? $emails_array['subject'] : (isset($site_report->customer) ? $site_report->customer->customer_name : '<Customer Name>').' - '.$site_report->site_report_title;

            SendSiteReportEmailJob::dispatch(array_filter($emails), $customer, $company, $subject, $body, $storage)->delay(now()->addMinutes(5));

            CustomerSiteReport::where('id',$site_report->id)->update(['emailed_at' => now()]);

            return response()->json(['message' => 'Site Report sent successfully']);
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return response()->json(['message'=> $e->getMessage()]);
        }
    }

    private function sysConfig(): Config
    {
        return Config::first(
            [
                'company_id'
            ]
        );
    }


}
