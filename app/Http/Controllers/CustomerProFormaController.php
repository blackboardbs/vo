<?php

namespace App\Http\Controllers;

use App\Enum\YesOrNoEnum;
use App\Models\Company;
use App\Http\Requests\CreateCustomerInvoiceRequest;
use App\Models\Timesheet;
use App\Services\ProFormaInvoiceService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CustomerProFormaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(): View
    {
        $customers = Timesheet::with(['customer:id,customer_name'])->select('customer_id')->groupBy('customer_id')->groupBy('id')->orderBy('id', 'desc')->whereHas('customer')->get();

        $parameters = [
            'customers' => $customers->pluck('customer.customer_name', 'customer_id'),
        ];

        return view('customer.pro-forma.index')->with($parameters);
    }

    public function calculate(CreateCustomerInvoiceRequest $request, ProFormaInvoiceService $service)
    {
        $period = $request->input('period');
        $year = substr($period, 0, 4);
        $month = substr($period, 4);

        $timesheets = Timesheet::with([
            'employee:id,first_name,last_name',
            'customer' => fn($customer) => $customer->select([
                'id',
                'customer_name',
                'postal_address_line1',
                'postal_address_line2',
                'postal_address_line3',
                'city_suburb',
                'state_province',
                'postal_zipcode',
                'contact_firstname',
                'contact_lastname',
                'vat_no',
            ]),
            'time_exp' => fn($expenses) => $expenses->select(['timesheet_id', 'amount', 'description', 'date'])
                ->where('is_billable', YesOrNoEnum::Yes->value),
            'company' => fn($company) => $company->select([
                'id',
                'company_name',
                'company_logo',
                'postal_address_line1',
                'postal_address_line2',
                'postal_address_line3',
                'state_province',
                'postal_zipcode',
                'phone',
                'email',
                'vat_no',
                'bank_name',
                'branch_code',
                'bank_acc_no'
            ]),
            'project:id,name,ref,end_date',
            'timeline' => fn($timesheets) => $timesheets->select(['timesheet_id', 'total', 'total_m', 'task_id', 'description_of_work'])
                ->with(['task:id,description'])->where('vend_is_billable', YesOrNoEnum::Yes->value),
        ])->leftJoin('assignment', function ($join){
            $join->on('assignment.employee_id', '=', 'timesheet.employee_id')
                ->on('assignment.project_id', '=', 'timesheet.project_id');
        })->select(['timesheet.id', 'timesheet.company_id', 'timesheet.customer_id', 'timesheet.employee_id', 'timesheet.project_id', 'assignment.invoice_rate', 'assignment.currency'])
            ->where('timesheet.customer_id', $request->customer)
            ->where('timesheet.project_id', '=', $request->project)
            ->when($year, fn($query) => $query->where("year", $year)->where("month", $month))
            ->unless($year, fn($query) => $query->where('first_day_of_week', '>=', $request->date_from)->where('last_day_of_week', '<=', $request->date_to))
            ->groupBy(['timesheet.id', 'timesheet.company_id', 'timesheet.customer_id', 'timesheet.employee_id', 'timesheet.project_id', 'assignment.internal_cost_rate', 'assignment.currency'])
            ->get();

        if ($timesheets->isEmpty()) {
            return redirect()->back()->with('flash_danger', 'No timesheets found for the selected period');
        }

        $lineItems = $service->lineItemsGrouped($timesheets, 'invoice_rate');
        $expenses =  $timesheets->map(fn($time) => $time->time_exp)->flatten();

        $total_time_exp = $expenses->sum('amount');
        $exclusive_total = $lineItems->sum(fn($item) => $item->sum('exc_price')) + $total_time_exp;
        $total_vat = $lineItems->sum(fn($item) => $item->sum('exc_price'))*$service->vatRate();;
        $inclusive_total = $lineItems->sum(fn($item) => $item->sum('inc_price')) + $total_time_exp;
        $company = $timesheets->first()?->company ?? Company::first();

        $parameters = [
            'customer' => $timesheets->first()?->customer,
            'company' => $company,
            'expenses' => $expenses,
            'invoice_no' => $request->invoice_number,
            'timesheet' => $timesheets,
            'inclusive_total' => $inclusive_total,
            'exclusive_total' => $exclusive_total,
            'invoice_message' => $request->invoice_message,
            'date' => Carbon::now()->toDateString(),
            'period' => $request->period,
            'date_range' => $request->date_from.' to '.$request->date_to,
            'total_vat' => $total_vat,
            'lineItems' => $lineItems,
        ];

        return view('customer.pro-forma.show')->with($parameters);
    }

    public function get_customer(Request $request): JsonResponse
    {
        $project = Timesheet::select('project_id')->with(['project'])->where('customer_id', '=', $request->customer_id)->groupBy('project_id')->get();

        return response()->json($project);
    }

    public function get_yearmonth(Request $request): JsonResponse
    {
        $period = Timesheet::select('year', 'month')->where('project_id', '=', $request->project_id)->groupBy('year')->groupBy('month')->orderBy('year', 'desc')->get();

        return response()->json($period);
    }
}
