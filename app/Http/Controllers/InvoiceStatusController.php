<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInvoiceStatusRequest;
use App\Http\Requests\UpdateInvoiceStatusRequest;
use App\Models\InvoiceStatus;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class InvoiceStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $invoice_status = InvoiceStatus::sortable('description', 'asc')->paginate($item);

        $invoice_status = InvoiceStatus::with('statusd:id,description')->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $invoice_status = InvoiceStatus::with('statusd:id,description')->where('status_id', $request->input('status_filter'));
            }else{
                $invoice_status = InvoiceStatus::with('statusd:id,description')->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $invoice_status = $invoice_status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $invoice_status = $invoice_status->paginate($item);

        $parameters = [
            'invoice_status' => $invoice_status,
        ];

        return View('master_data.invoice_status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = InvoiceStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.invoice_status.create')->with($parameters);
    }

    public function store(StoreInvoiceStatusRequest $request): RedirectResponse
    {
        $invoice_status = new InvoiceStatus();
        $invoice_status->description = $request->input('description');
        $invoice_status->status = $request->input('status');
        $invoice_status->creator_id = auth()->id();
        $invoice_status->save();

        return redirect(route('invoice_status.index'))->with('flash_success', 'Master Data Invoice Status captured successfully');
    }

    public function show($invoice_status_id): View
    {
        $parameters = [
            'invoice_status' => InvoiceStatus::where('id', '=', $invoice_status_id)->get(),
        ];

        return View('master_data.invoice_status.show')->with($parameters);
    }

    public function edit($invoice_status_id): View
    {
        $autocomplete_elements = InvoiceStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'invoice_status' => InvoiceStatus::where('id', '=', $invoice_status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.invoice_status.edit')->with($parameters);
    }

    public function update(UpdateInvoiceStatusRequest $request, $invoice_status_id): RedirectResponse
    {
        $invoice_status = InvoiceStatus::find($invoice_status_id);
        $invoice_status->description = $request->input('description');
        $invoice_status->status = $request->input('status');
        $invoice_status->creator_id = auth()->id();
        $invoice_status->save();

        return redirect(route('invoice_status.index'))->with('flash_success', 'Master Data Invoice Status saved successfully');
    }

    public function destroy($invoice_status_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // InvoiceStatus::destroy($invoice_status_id);
        $item = InvoiceStatus::find($invoice_status_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('invoice_status.index')->with('success', 'Master Data Invoice Status deleted successfully');
    }
}
