<?php

namespace App\Http\Controllers;

use App\Models\AdvancedTemplates;
use App\Http\Requests\StoreAdvancedTemplatesRequest;
use App\Http\Requests\UpdateAdvancedTemplatesRequest;
use App\Models\Module;
use App\Models\TemplateType;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;

class AdvancedTemplatesController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $templates = AdvancedTemplates::with(['user:id,first_name,last_name', 'temptype:id,name'])
            ->select(['id', 'name', 'template_type_id', 'created_at']);

        $module = Module::where('name', '=', 'App\Models\DocumentUploadedTemplates')->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $templates = $templates->whereIn('user_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if (($request->has('search') && $request->input('search') != '') || ($request->has('type') && $request->input('type') != '')) {
            $templates = $templates->where('name', 'LIKE', '%'.$request->input('search').'%')
                ->orWhere('template_type_id', 'like', '%'.$request->input('search').'%')
                ->orWhereHas('type', function ($query) use ($request) {
                    $query->where('name', 'like', '%'.$request->search.'%');
                });
        }

        $templates = $templates->sortable(['name' => 'asc'])->paginate($item);

        $parameters = [
            'templates' => $templates,
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
        ];

        return view('advanced_template.newindex', $parameters);
    }

    public function create(): View
    {
        $assignment_columns = Schema::getColumnListing('assignment');
        $template_fields[1] = $assignment_columns;

        $parameters = [
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
            'template_fields' => $template_fields,
        ];

        return view('advanced_template.create')->with($parameters);
    }

    public function store(StoreAdvancedTemplatesRequest $request): RedirectResponse
    {
        $name = '';
        if ($request->hasFile('file')) {
            //$request->file('file')->store('templates');
            //ToDo: The above save every file as .bin, Please fix if you have a better way of uploading documents
            $file = $request->file('file');
            $name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$file->getClientOriginalExtension();
            $stored = $file->storeAs('templates', $name);
        }

        $template = new AdvancedTemplates;
        $template->name = $request->input('name');
        $template->template_type_id = $request->input('template_type_id');
        $template->file = $name;
        $template->user_id = auth()->id();
        $template->save();

        //return redirect(route('template.index'))->with('flash_success', 'Temp Template uploaded successfully');
        return redirect(route('advancedtemplate.index'))->with('flash_success', 'Temp Template uploaded successfully');
    }

    public function show(AdvancedTemplates $advanced_template)
    {
        $templateType = $advanced_template->temptype;

        $templateBody = str_replace('class="mention"', '', $advanced_template->template_body);

        foreach ($templateType->variables as $variable) {
            if (($variable->variable === "company.company_logo") || ($variable->variable === "vendor.vendor_logo")){
                $templateBody = str_replace("[{$variable->variable}]", "<img src='/assets/consulteaze_logo.png' style='max-height: 80px;width: auto' alt='$variable->display_name' />", $templateBody);
            }else{
                $templateBody = str_replace("[{$variable->variable}]", "<strong>{$variable->display_name}</strong>", $templateBody);
            }

        }

        return view('advanced_template.show')->with([
            'templateBody' => $templateBody,
            'template' => $advanced_template
        ]);
    }

    public function edit($template): View
    {
        $template = AdvancedTemplates::where('id', $template)->first();

        //$assignment_columns = Schema::getColumnListing('assignment');
        //$template_fields[1] = $assignment_columns;

        $parameters = [
            'template' => $template,
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
           // 'template_fields' => $template_fields,
        ];

        return view('advanced_template.edit')->with($parameters);
    }

    public function update(UpdateAdvancedTemplatesRequest $request, $tempid): RedirectResponse
    {
        $template = AdvancedTemplates::find($tempid);
        if ($request->hasFile('file')) {
            //$request->file('file')->store('templates');
            $file = $request->file('file');
            $name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$file->getClientOriginalExtension();
            $stored = $file->storeAs('templates', $name);

            $template->file = $name; //$request->file('file')->hashName();
        }

        $template->name = $request->input('name');
        $template->template_type_id = $request->input('template_type_id');
        $template->save();

        return redirect(route('template.index'))->with('flash_success', 'Temp Template updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        //DB::table("templates")->delete($id);
        AdvancedTemplates::destroy($id);
        //File::delete($request->input('q'));
        return redirect(route('template.index'))->with('flash_success', 'Temp Template deleted successfully');
    }

    public function getVariables($template_id, Request $request): JsonResponse
    {
        $template_fields = '';
        switch ($template_id) {
            case 1:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Project</p>";
                $template_fields_tmp = Schema::getColumnListing('projects');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{P'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Assignment</p>";
                $template_fields_tmp = Schema::getColumnListing('assignment');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{A'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 2:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>CV</p>";
                $template_fields_tmp = Schema::getColumnListing('talent');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{CV'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Availability</p>";
                $template_fields_tmp = Schema::getColumnListing('availability');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{A'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Skill</p>";
                $template_fields_tmp = Schema::getColumnListing('skill');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{S'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Qualification</p>";
                $template_fields_tmp = Schema::getColumnListing('qualification');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{Q'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Experience</p>";
                $template_fields_tmp = Schema::getColumnListing('cv');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{E'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Industry Experience</p>";
                $template_fields_tmp = Schema::getColumnListing('industry_experience');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{IE'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{V'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }

        return response()->json(['template_fields' => $template_fields]);
    }

    public function getVariables2($id, $template_id, Request $request): JsonResponse
    {
        $template_fields = '';
        $exclude_fields = '';
        $extra_exclude_fields = '';
        switch ($template_id) {
            case 1:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Project</p>";
                $template_fields_tmp = Schema::getColumnListing('projects');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{P'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Assignment</p>";
                $template_fields_tmp = Schema::getColumnListing('assignment');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{A'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 2:
                $exclude_fields .= 'id,res_id,creator_id,created_at,updated_at';
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>CV</p>";
                $template_fields .= '{CVIDNO} ';
                $template_fields_tmp = Schema::getColumnListing('users');
                $extra_exclude_fields = 'password,avatar,resource_id,vendor_id,company_id,customer_id,active_office_id,remember_token';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && strpos($extra_exclude_fields, $template_field) === false) {
                        $template_fields .= '{CV'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }
                $template_fields_tmp = Schema::getColumnListing('talent');
                $extra_exclude_fields = 'cv_template_id,status_id,user_id,criminal_offence_ind,drivers_license_ind,resource_middle_name,id_no';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && ! preg_match('~\\b'.$template_field.'\\b~i', $extra_exclude_fields, $m)) {
                        $template_fields .= '{CV'.strtoupper(str_replace('_', '', str_replace('id', '', $template_field))).'} ';
                    }
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Availability</p>";
                $template_fields_tmp = Schema::getColumnListing('availability');
                $extra_exclude_fields = 'row_order,status_id,primary_ind';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && strpos($extra_exclude_fields, $template_field) === false) {
                        $template_fields .= '{A'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Skill</p>";
                $template_fields_tmp = Schema::getColumnListing('skill');
                $extra_exclude_fields = 'row_order,status_id';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && strpos($extra_exclude_fields, $template_field) === false) {
                        $template_fields .= '{S'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Qualification</p>";
                $template_fields_tmp = Schema::getColumnListing('qualification');
                $extra_exclude_fields = 'row_order,status_id';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && strpos($extra_exclude_fields, $template_field) === false) {
                        $template_fields .= '{Q'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Experience</p>";
                $template_fields_tmp = Schema::getColumnListing('cv');
                $extra_exclude_fields = 'row_order,status_id,company_id';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && ! preg_match('~\\b'.$template_field.'\\b~i', $extra_exclude_fields, $m)) {
                        $template_fields .= '{E'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Industry Experience</p>";
                $template_fields_tmp = Schema::getColumnListing('industry');
                $extra_exclude_fields = 'row_order,status';
                foreach ($template_fields_tmp as $template_field) {
                    if (strpos($exclude_fields, $template_field) === false && strpos($extra_exclude_fields, $template_field) === false) {
                        $template_fields .= '{IE'.strtoupper(str_replace('_', '', $template_field)).'} ';
                    }
                }

                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{V'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }

        return response()->json(['template_fields' => $template_fields]);
    }
}
