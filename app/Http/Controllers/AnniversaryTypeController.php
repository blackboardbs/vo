<?php

namespace App\Http\Controllers;

use App\Models\AnniversaryType;
use App\Http\Requests\StoreAnniversaryTypeRequest;
use App\Http\Requests\UpdateAnniversaryTypeRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AnniversaryTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $anniversarytype = AnniversaryType::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $anniversarytype = $anniversarytype->where('status', $request->input('status_filter'));
            }else{
                $anniversarytype = $anniversarytype->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $anniversarytype = $anniversarytype->where('description', 'like', '%'.$request->input('q').'%');
        }

        $anniversarytype = $anniversarytype->paginate($item);

        $parameters = [
            'anniversarytype' => $anniversarytype,
            //'status_description' => Status::where('status', '=', $status->status)->pluck('description')
        ];

        return View('master_data.anniversary_type.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = AnniversaryType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.anniversary_type.create')->with($parameters);
    }

    public function store(StoreAnniversaryTypeRequest $request): RedirectResponse
    {
        $status = new AnniversaryType();
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('anniversarytype.index'))->with('flash_success', 'Master Data Anniversary Type captured successfully');
    }

    public function show($anniversarytype_id): View
    {
        $parameters = [
            'anniversarytype' => AnniversaryType::where('id', '=', $anniversarytype_id)->get(),
        ];

        return View('master_data.anniversary_type.show')->with($parameters);
    }

    public function edit($anniversarytype_id): View
    {
        $autocomplete_elements = AnniversaryType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'anniversarytype' => AnniversaryType::where('id', '=', $anniversarytype_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.anniversary_type.edit')->with($parameters);
    }

    public function update(UpdateAnniversaryTypeRequest $request, $anniversarytype_id): RedirectResponse
    {
        $status = AnniversaryType::find($anniversarytype_id);
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('anniversarytype.index'))->with('flash_success', 'Master Data Status saved successfully');
    }

    public function destroy($anniversarytype_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // AnniversaryType::destroy($anniversarytype_id);

        $item = AnniversaryType::find($anniversarytype_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('anniversarytype.index')->with('success', 'Master Data Anniversary Type suspended successfully');
    }
}
