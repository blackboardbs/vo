<?php

namespace App\Http\Controllers;

use App\Models\AssessmentHeader;
use App\Models\AssessmentMeasureScore;
use App\Http\Requests\StoreAssessmentRequest;
use App\Models\Notification;
use App\Models\Resource;
use App\Models\UserNotification;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class AssessmentMeasureScoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(StoreAssessmentRequest $request): RedirectResponse
    {
        $assessment_measure_db = AssessmentMeasureScore::where('assessment_id', '=', $request->assessment_id)->first();
        if (isset($assessment_measure_db)) {
            AssessmentMeasureScore::where('assessment_id', '=', $request->assessment_id)->delete();
            for ($i = 0; $i < count($request->assessment_measure_detail_id); $i++) {
                $assessment_measure = new AssessmentMeasureScore();
                $assessment_measure->assessment_id = $request->assessment_id;
                $assessment_measure->assessment_master_id = isset($request->assessment_master_id[$i]) ? $request->assessment_master_id[$i] : 0;
                $assessment_measure->assessment_master_detail_id = isset($request->assessment_measure_detail_id[$i]) ? $request->assessment_measure_detail_id[$i] : 0;
                $assessment_measure->assessment_group = null;
                $assessment_measure->score = isset($request->assessment_measure_score[$i]) ? $request->assessment_measure_score[$i] : 0;
                $assessment_measure->notes = isset($request->assessment_note[$i]) ? $request->assessment_note[$i] : '';
                $assessment_measure->save();
            }
        } else {
            for ($i = 0; $i < count($request->assessment_measure_detail_id); $i++) {
                $assessment_measure = new AssessmentMeasureScore();
                $assessment_measure->assessment_id = $request->assessment_id;
                $assessment_measure->assessment_master_id = isset($request->assessment_master_id[$i]) ? $request->assessment_master_id[$i] : 0;
                $assessment_measure->assessment_master_detail_id = isset($request->assessment_measure_detail_id[$i]) ? $request->assessment_measure_detail_id[$i] : 0;
                $assessment_measure->assessment_group = null;
                $assessment_measure->score = isset($request->assessment_measure_score[$i]) ? $request->assessment_measure_score[$i] : 0;
                $assessment_measure->notes = isset($request->assessment_note[$i]) ? $request->assessment_note[$i] : '';
                $assessment_measure->save();
            }
        }

        $assessment = AssessmentHeader::find($assessment_measure->assessment_id);
        $manager = isset(Resource::where('user_id', '=', $assessment->resource_id)->first()->manager) ? Resource::where('user_id', '=', $assessment->resource_id)->first()->manager : '';

        $notification = new Notification;
        $notification->name = 'Assessment hast been completed';
        $notification->link = route('assessment.show', $assessment_measure);
        $notification->save();

        $user_notifications = [];

        if (! is_null($assessment_measure->assessment->user)) {
            array_push($user_notifications, [
                'user_id' => $assessment_measure->assessment->user->id,
                'notification_id' => $notification->id,
            ]);
        }

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);

        Mail::to(isset($manager->user) ? $assessment->user->email : '');

        return redirect()->route('assessment.show', $assessment_measure->assessment_id);
    }
}
