<?php

namespace App\Http\Controllers;

use App\Exports\ResourcesExport;
use App\Exports\ScoutingProfilesExport;
use App\Models\BusinessRating;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Cv;
use App\Models\Document;
use App\Http\Requests\StoreScoutingRequest;
use App\Http\Requests\UpdateScoutingRequest;
use App\Models\InterviewStatus;
use App\Models\Module;
use App\Models\ProcessStatus;
use App\Models\Profession;
use App\Models\Resource;
use App\Models\ResourceLevel;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Scouting;
use App\Models\ScoutingRole;
use App\Models\Speciality;
use App\Models\Status;
use App\Models\System;
use App\Models\TechnicalRating;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class ScoutingController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $scoutings = Scouting::with([
            'processstatus:id,name',
            'interviewstatus:id,name',
            'role:id,name',
            'vendor:id,vendor_name',
            'businessrating:id,name',
            'technicalrating:id,name',
            'resourcelevel:id,description'
        ])->orderby('resource_first_name');

        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                //Filters to only show team, continue for now
            }
        } else {
            return abort(403);
        }

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        // This wasn't working so i went old school

        /*$scoutings = $scoutings->when($request->process_status_id,
            fn($scouts) => $scouts->where('process_status_id', $request->process_status_id)
        )->unless($request->process_status_id,
            fn($scouts) => $scouts->whereNotIn('process_status_id', [1, 7])
        )->when($request->process_interview_id,
            fn($scouts) => $scouts->where('interview_status_id', $request->process_interview_id)
        )->when($request->scouting_role_id,
            fn($scouts) => $scouts->where('role_id', $request->scouting_role_id)
        )/*->when($request->vendor_id,
            fn($scouts) => $scouts->where('vendor_id', $request->vendor_id)
        )->when($request->resource_level_id,
            fn($scouts) => $scouts->where('resource_level_id', $request->resource_level_id)
        )->when($request->business_rating_id,
            fn($scouts) => $scouts->where('business_rating_id', $request->business_rating_id)
        )->when($request->technical_rating_id,
            fn($scouts) => $scouts->where('technical_rating_id', $request->technical_rating_id)
        )->when($request->profession_id,
            fn($scouts) => $scouts->where('profession_id', $request->profession_id)
        )->when($request->speciality_id,
            fn($scouts) => $scouts->where('speciality_id', $request->speciality_id)
        )->when($request->skill_id,
            fn($scouts) => $scouts->where('skills', 'like', '%'.$request->skill_id.'%')
        )->when($request->reference_code,
            fn($scouts) => $scouts->where('reference_code', $request->reference_code)
        )->when($request->has('permanent'),
            fn($scouts) => $scouts->where('is_permanent', $request->permanent)
        )->when($request->has('contracting'),
            fn($scouts) => $scouts->where('is_contract', $request->contracting)
        )->when($request->has('temporary'),
            fn($scouts) => $scouts->where('is_temporary', $request->temporary)
        )*/;

        // $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('All', -1);

        if (! $request->has('process_status_id')) {
            $scoutings = $scoutings->whereNotIn('process_status_id', [1, 7]);
        }

        if ($request->has('process_status_id') && $request->input('process_status_id') != '') {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status_id'));
        }

        if ($request->has('process_interview_id') && $request->input('process_interview_id') != '') {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview_id'));
        }

        if ($request->has('scouting_role_id') && $request->input('scouting_role_id') != '') {
            $scoutings = $scoutings->where('role_id', $request->input('scouting_role_id'));
        }

        if ($request->has('vendor_id') && $request->input('vendor_id') != '') {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor_id'));
        }

        if ($request->has('resource_level_id') && $request->input('resource_level_id') != '') {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level_id'));
        }

        if ($request->has('business_rating_id') && $request->input('business_rating_id') != '') {
            $scoutings = $scoutings->where('business_rating_id', $request->input('business_rating_id'));
        }

        if ($request->has('technical_rating_id') && $request->input('technical_rating_id') != '') {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating_id'));
        }

        if ($request->has('profession_id') && $request->input('profession_id') != '') {
            $scoutings = $scoutings->where('profession_id', $request->input('profession_id'));
        }

        if ($request->has('speciality_id') && $request->input('speciality_id') != '') {
            $scoutings = $scoutings->where('speciality_id', $request->input('speciality_id'));
        }

        if ($request->has('reference_code') && $request->input('reference_code') != '') {
            $scoutings = $scoutings->where('reference_code', $request->input('reference_code'));
        }

        if ($request->has('permanent') && $request->input('permanent') != '') {
            $scoutings = $scoutings->where('is_permanent', $request->input('permanent'));
        }

        if ($request->has('contracting') && $request->input('contracting') != '') {
            $scoutings = $scoutings->where('is_contract', $request->input('contracting'));
        }

        if ($request->has('temporary') && $request->input('temporary') != '') {
            $scoutings = $scoutings->where('is_temporary', $request->input('temporary'));
        }

        if ($request->has('skill_id') && $request->input('skill_id') != '') {
            $scoutings = $scoutings->where('skills','like', '%'.$request->input('skill_id').'%');
        }

        $scoutings = $scoutings->paginate($item);

        $parameters = [
            'scoutings' => $scoutings,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        if ($request->has('export')) return $this->export($parameters, 'scouting');

        return view('scouting.index')->with($parameters);
    }

    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please Select', 0);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', 0);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);

        $parameters = [
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'users_drop_down' => $users_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
        ];

        return view('scouting.create')->with($parameters);
    }

    public function edit($id)
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please Select', 0);
        $scouting = Scouting::find($id);
        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', 0);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        if (isset($scouting->profession_id) && ($scouting->profession_id > 0)) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('profession_id', '=', $scouting->profession_id)->pluck('name', 'id')->prepend('Please Select', 0);
        }
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);

        $skills = explode('|', $scouting->skills);

        $documents = Document::where('reference_id', '=', $scouting->id)->where('document_type_id', '=', 11)->get();

        $parameters = [
            'scouting' => $scouting,
            'documents' => $documents,
            'users_drop_down' => $users_drop_down,
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'skills' => $skills,
        ];

        return view('scouting.edit')->with($parameters);
    }

    public function show($id)
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'show_all') || Auth::user()->canAccess($module->id, 'show_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please Select', 0);
        $scouting = Scouting::withTrashed()->where('id', $id)->first();
        $documents = Document::where('reference_id', '=', $id)->orderBy('id')->get();
        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', 0);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Please Select', 0);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', 0);

        $skills = explode('|', $scouting->skills);

        $documents = Document::where('reference_id', '=', $scouting->id)->where('document_type_id', '=', 11)->get();

        $parameters = [
            'scouting' => $scouting,
            'documents' => $documents,
            'users_drop_down' => $users_drop_down,
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'skills' => $skills,
            'documents' => $documents,
        ];

        return view('scouting.show')->with($parameters);
    }

    public function convert($id): View
    {
        $scouting = Scouting::find($id);

        $parameters = [
            'scouting' => $scouting,
            'roles' => Role::orderBy('name')->get()->pluck('display_name', 'id'),
            'company' => Company::orderBy('company_name')->pluck('company_name', 'id')->prepend('Select Company', '0'),
            'from_cv' => '0',
            'vendor_drop_down' => Vendor::orderBy('vendor_name')->get()->pluck('vendor_name', 'id')->prepend('Select Vendor', '0'),
            //'resource' => Resource::select(DB::raw("CONCAT(emp_firstname,' ',COALESCE(`emp_lastname`,'')) AS full_name"),'id')->orderBy('emp_firstname')->orderBy('emp_lastname')->pluck('full_name', 'id')->prepend('Select Resource','0'),
            'customer_drop_down' => Customer::orderBy('customer_name')->get()->pluck('customer_name', 'id')->prepend('Select Customer', '0'),
            'config' => Config::first(),
            'status_drop_down' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return view('users.create')->with($parameters);
    }

    public function convertToCV($id): RedirectResponse
    {
        $scouting = Scouting::find($id);

        $config = Config::orderBy('id')->first();

        $password = Str::random(8);

        $check_user = User::where('email', '=', $scouting->email)->first();

        if (isset($check_user) && $check_user->email == $scouting->email) {
            return redirect(route('scouting.index'))->with('flash_danger', 'Could not convert this scouting, user with the same email address already exist. Try creating the CV Resouce manually.');
        }

        $user = new User;
        $user->first_name = $scouting->resource_first_name;
        $user->last_name = $scouting->resource_last_name;
        $user->email = $scouting->email;
        $user->phone = $scouting->phone;
        $user->password = Hash::make($password);
        $user->company_id = $config->company_id;
        $user->status_id = 1;
        $user->expiry_date = now();

        $resource = new Resource;
        $resource->status_id = 1;
        $resource->save();

        $user->resource_id = $resource->id;
        $user->vendor_id = $scouting->vendor_id != null ? $scouting->vendor_id : null;

        $user->login_user = 0;
        $user->save();

        $user_id = Resource::find($user->resource_id);
        $user_id->user_id = $user->id;
        $user_id->status_id = 1;

        $user_id->save();

        RoleUser::insert(['user_id' => $user->id, 'role_id' => 7]);

        $scouting->convertion_date = Carbon::now();
        $scouting->user_id = $user->id;
        $scouting->status_id = 7;
        $scouting->save();

        //Scouting::destroy($scouting->id);

        $talent = new Cv;
        $talent->resource_pref_name = $scouting->resource_first_name;
        //$talent->resource_middle_name = $scouting->resource_last_name;
        $talent->user_id = $user->id;
        $talent->cell = $scouting->phone;
        $talent->personal_email = $scouting->email;
        $talent->is_permanent = $scouting->is_permanent;
        $talent->is_contract = $scouting->is_contract;
        $talent->is_temporary = $scouting->is_temporary;
        $talent->profession_id = $scouting->profession_id;
        $talent->speciality_id = $scouting->speciality_id;
        $talent->skills = $scouting->skills;
        $talent->reference_code = $scouting->reference_code;
        $talent->note = $scouting->note;
        $talent->status_id = 1;
        $talent->creator_id = Auth()->id();
        $talent->cv_template_id = 1;
        $talent->scouting_id = $scouting->id;
        $talent->vendor_id = $scouting->vendor_id;
        $talent->hourly_rate_minimum = $scouting->hourly_rate_minimum;
        $talent->hourly_rate_maximum = $scouting->hourly_rate_maximum;
        $talent->monthly_salary_minimum = $scouting->monthly_salary_minimum;
        $talent->monthly_salary_maximum = $scouting->monthly_salary_maximum;
        $talent->save();

        // Scouting documents
        $documents = Document::where('reference_id', '=', $scouting->id)->where('document_type_id', '=', 11)->get();
        foreach ($documents as $document) {
            $cvDocument = new Document();
            $cvDocument->type_id = 3;
            $cvDocument->document_type_id = 3; //Scouting type document
            $cvDocument->name = $document->name;
            $cvDocument->file = $document->file;
            $cvDocument->creator_id = $document->creator_id;
            $cvDocument->owner_id = $document->owner_id;
            $cvDocument->digisign_status_id = $document->digisign_status_id;
            $cvDocument->reference_id = $talent->id;
            $cvDocument->digisign_approver_user_id = $document->digisign_approver_user_id;
            $cvDocument->save();
        }

        return redirect(route('cv.show', ['cvid' => $talent->id, 'res_id' => $resource->id]))->with('flash_success', 'Scouting converted to CV successfully');
    }

    public function store(StoreScoutingRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $skills = '';
        if ($request->has('skills') && ! empty($request->input('skills'))) {
            foreach ($request->input('skills') as $skill) {
                if ($skills != '') {
                    $skills .= '|'.$skill;
                } else {
                    $skills = $skill;
                }
            }
        }

        $scouting = new Scouting();
        $scouting->resource_first_name = $request->input('resource_first_name');
        $scouting->resource_last_name = $request->input('resource_last_name');
        $scouting->role_id = $request->input('role_id');
        $scouting->email = $request->input('email');
        $scouting->phone = $request->input('phone');
        $scouting->process_status_id = $request->input('process_status_id');
        $scouting->interview_status_id = $request->input('interview_status_id');
        $scouting->vendor_id = $request->input('vendor_id');
        $scouting->technical_rating_id = $request->input('technical_rating_id');
        $scouting->business_rating_id = $request->input('business_rating_id');
        $scouting->resource_level_id = $request->input('resource_level_id');
        $scouting->assessed_by = $request->input('assessed_by');
        $scouting->estimated_hourly_rate = $request->input('estimated_hourly_rate');
        $scouting->availability = $request->input('availability');
        $scouting->referral = $request->input('referral');
        $scouting->reference_code = $request->input('reference_code');
        $scouting->profession_id = $request->input('profession_id');
        $scouting->speciality_id = (int) $request->input('speciality_id');
        $scouting->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $scouting->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $scouting->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $scouting->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $scouting->skills = $skills;
        $scouting->note = $request->input('note');
        $scouting->is_permanent = $request->has('permanent') && $request->has('permanent') != null ? $request->input('permanent') : 0;
        $scouting->is_contract = $request->has('contracting') && $request->has('contracting') != null ? $request->input('contracting') : 0;
        $scouting->is_temporary = $request->has('temporary') && $request->has('temporary') != null ? $request->input('temporary') : 0;
        $scouting->status_id = 1;
        $scouting->save();

        if ($request->hasFile('file')) {
            foreach ($request->file as $file) {
                $name = $file->getClientOriginalName();
                $file_name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$file->getClientOriginalExtension();
                //$stored = $file->storeAs('scouting', $file_name);
                $file->move(public_path().'/files/documents/', $file_name);

                $document = new Document();
                $document->type_id = 11;
                $document->document_type_id = 11; //Scouting type document
                $document->name = $name;
                $document->file = 'files/documents/'.$file_name;
                $document->creator_id = auth()->id();
                $document->owner_id = auth()->id();
                $document->digisign_status_id = null;
                $document->reference_id = $scouting->id;
                $document->digisign_approver_user_id = null;
                $document->save();
            }
        }

        return redirect(route('scouting.index'))->with(['flash_success' => 'Scouting added successfully']);
    }

    public function update($id, UpdateScoutingRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $skills = '';
        if ($request->has('skills') && ! empty($request->input('skills'))) {
            foreach ($request->input('skills') as $skill) {
                if ($skills != '') {
                    $skills .= '|'.$skill;
                } else {
                    $skills = $skill;
                }
            }
        }

        $scouting = Scouting::find($id);
        $scouting->resource_first_name = $request->input('resource_first_name');
        $scouting->resource_last_name = $request->input('resource_last_name');
        $scouting->role_id = $request->input('role_id');
        $scouting->email = $request->input('email');
        $scouting->phone = $request->input('phone');
        $scouting->process_status_id = $request->input('process_status_id');
        $scouting->interview_status_id = $request->input('interview_status_id');
        $scouting->vendor_id = $request->input('vendor_id');
        $scouting->technical_rating_id = $request->input('technical_rating_id');
        $scouting->business_rating_id = $request->input('business_rating_id');
        $scouting->resource_level_id = $request->input('resource_level_id');
        $scouting->assessed_by = $request->input('assessed_by');
        $scouting->estimated_hourly_rate = $request->input('estimated_hourly_rate');
        $scouting->availability = $request->input('availability');
        $scouting->referral = $request->input('referral');
        $scouting->reference_code = $request->input('reference_code');
        $scouting->profession_id = $request->input('profession_id');
        $scouting->speciality_id = $request->input('speciality_id');
        $scouting->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $scouting->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $scouting->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $scouting->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $scouting->skills = $skills;
        $scouting->note = $request->input('note');
        $scouting->is_permanent = $request->has('permanent') && $request->has('permanent') != null ? $request->input('permanent') : 0;
        $scouting->is_contract = $request->has('contracting') && $request->has('contracting') != null ? $request->input('contracting') : 0;
        $scouting->is_temporary = $request->has('temporary') && $request->has('temporary') != null ? $request->input('temporary') : 0;
        $scouting->referral = $request->input('referral');
        $scouting->deleted_at = null;
        $scouting->save();

        return redirect(route('scouting.index'))->with(['flash_success' => 'Scouting updated successfully']);
    }

    public function destroy($id)
    {
        $module = Module::where('name', '=', \App\Models\Scouting::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        Scouting::destroy($id);

        return redirect(route('scouting.index'))->with(['flash_success' => 'Scouting deleted successfully']);
    }
}
