<?php

namespace App\Http\Controllers;

use App\Models\Prospect;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(Prospect $prospect)
    {
        $prospect->load(['comments' => fn($comment) => $comment->latest()->with(['commenter:id,first_name,last_name'])]);

        return response()->json($prospect->comments);
    }
    public function store(Prospect $prospect, Request $request): JsonResponse
    {
        $prospect->comments()->create([
            'employee_id' => auth()->id(),
            'body' => $request->comment
        ]);

        return response()->json(['message' => 'Successful']);
    }
}
