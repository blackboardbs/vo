<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Config;
use App\Models\CustomerInvoice;
use App\Jobs\SendCustomerStatementEmailJob;
use App\Models\Module;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Knp\Snappy\Exception\FileAlreadyExistsException;

class StatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\CustomerStatement')->first();

        if (isset($module) && (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'))) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                // Filter customer invoices here per team
            }
        } else {
            return abort(403);
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        $items = ($request->has('r') && $request->r != '0') ? $request->r : 15;
        $configs = Config::first();
        $customer_invoices = CustomerInvoice::selectRaw('customer_id, 
               SUM(IF(due_date > CURRENT_DATE() AND bill_status = 1, invoice_value, 0)) AS current,
               SUM(IF(due_date < CURRENT_DATE() AND DATEDIFF(due_date, CURRENT_DATE()) >= -30 AND bill_status = 1, invoice_value, 0)) AS thirty_days,
               SUM(IF(DATEDIFF(due_date, CURRENT_DATE()) < -30 AND DATEDIFF(due_date, CURRENT_DATE()) >= -60 AND bill_status = 1, invoice_value, 0)) AS sixty_days,
               SUM(IF(DATEDIFF(due_date, CURRENT_DATE()) < -60 AND bill_status = 1, invoice_value, 0)) AS ninety_days_plus, currency
           ')->with('customer')
            //->whereIn('bill_status', [1,2])
            ->groupBy('customer_id', 'currency');
        $customers = $customer_invoices->get();

        if ($request->has('q') && $request->s != '') {
            $customer_invoices = $customer_invoices->whereHas('customer', function ($query) use ($request) {
                $query->where('customer_name', 'like', '%'.$request->q.'%');
            });
        }

        if ($request->has('company') && $request->company != '') {
            $customer_invoices = $customer_invoices->where('company_id', $request->company);
        } else {
            $customer_invoices = $customer_invoices->where('company_id', $configs?->company_id);
        }
        if ($request->has('customer_id') && $request->customer_id != '') {
            $customer_invoices = $customer_invoices->where('customer_id', $request->customer_id);
        }

        if (($request->has('period_from') && $request->period_from != '') && ($request->has('period_to') && $request->period_to != '')) {
            $customer_invoices = $customer_invoices->whereBetween('invoice_date', [
                Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->startOfMonth()->day)->toDateString(),
                Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->lastOfMonth()->day)->toDateString(), ]);
        } else {
            $customer_invoices = $customer_invoices->whereBetween('invoice_date', [now()->subMonths(3)->startOfMonth()->toDateString(), now()->lastOfMonth()->toDateString()]);
        }
        $customer_invoices = $customer_invoices->orderBy('customer_id')
            ->paginate($items);
        //return $customer_invoices;
        $customer_drop_down = [];
        foreach ($customers as $customer) {
            $customer_drop_down[$customer->customer_id] = isset($customer->customer->customer_name) ? $customer->customer->customer_name : null;
        }

        $dates = [];
        for ($i = 0; $i <= 23; $i++) {
            $dates[now()->subMonths($i)->year.now()->subMonths($i)->month] = now()->subMonths($i)->year.'/'.((now()->subMonths($i)->month < 10) ? '0'.now()->subMonths($i)->month : now()->subMonths($i)->month);
        }

        $currencies = [];
        $total = [];
        $total30 = [];
        $total60 = [];
        $total90 = [];
        foreach($customer_invoices as $vi){
            // $total = $total + $vi->current;
            // $total30 = $total30 + $vi->thirty_days;
            // $total60 = $total60 + $vi->sixty_days;
            // $total90 = $total90 + $vi->ninety_days_plus;
            if($vi->currency){
                if(!in_array($vi->currency,$currencies)){
                    array_push($currencies,$vi->currency);
                }
            } else {
                if(!in_array('ZAR',$currencies)){
                    array_push($currencies,'ZAR');
                }
            }
            if($vi->currency){
                if(isset($total[$vi->currency])){
                $total[$vi->currency] = $total[$vi->currency] + $vi->current;
                } else {
                    $total[$vi->currency] = $vi->current;
                }
            } else {
                if(isset($total['ZAR'])){
                $total['ZAR'] = $total['ZAR'] + $vi->current;
                } else {
                    $total['ZAR'] = $vi->current;
                }
            }
            if($vi->currency){
                if(isset($total30[$vi->currency])){
                $total30[$vi->currency] = $total30[$vi->currency] + $vi->thirty_days;
                } else {
                    $total30[$vi->currency] = $vi->thirty_days;
                }
            } else {
                if(isset($total30['ZAR'])){
                $total30['ZAR'] = $total30['ZAR'] + $vi->thirty_days;
                } else {
                    $total30['ZAR'] = $vi->thirty_days;
                }
            }
            if($vi->currency){
                if(isset($total60[$vi->currency])){
                $total60[$vi->currency] = $total60[$vi->currency] + $vi->sixty_days;
                } else {
                    $total60[$vi->currency] = $vi->sixty_days;
                }
            } else {
                if(isset($total60['ZAR'])){
                $total60['ZAR'] = $total60['ZAR'] + $vi->sixty_days;
                } else {
                    $total60['ZAR'] = $vi->sixty_days;
                }
            }
            if($vi->currency){
                if(isset($total90[$vi->currency])){
                $total90[$vi->currency] = $total90[$vi->currency] + $vi->ninety_days_plus;
                } else {
                    $total90[$vi->currency] = $vi->ninety_days_plus;
                }
            } else {
                if(isset($total90['ZAR'])){
                $total90['ZAR'] = $total90['ZAR'] + $vi->ninety_days_plus;
                } else {
                    $total90['ZAR'] = $vi->ninety_days_plus;
                }
            }
        }

        $parameters = [
            'customer_invoices' => $customer_invoices,
            'company_drop_down' => Company::orderBy('company_name')->pluck('company_name', 'id'),
            'config' => $configs,
            'customer_drop_down' => $customer_drop_down,
            'dates_drop_down' => $dates,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'total'=>$total,
            'total30'=>$total30,
            'total60'=>$total60,
            'total90'=>$total90,
            'currencies'=>$currencies
        ];

        if ($request->has('export')) return $this->export($parameters, 'customer.statements');

        return view('customer.statements.index')->with($parameters);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request, $id)
    {
        $customer_invoices = CustomerInvoice::with('customer')
            ->where('customer_id', $id)
            ->when(request()->currency, function ($statement){
                $statement->where('currency', request()->currency);
            })
            ->whereBetween('invoice_date', [
                Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->startOfMonth()->day)->toDateString(),
                Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->lastOfMonth()->day)->toDateString(), ])
            ->get();

        $from_date = Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->startOfMonth()->day)->toDateString();

        $total_by_months = CustomerInvoice::selectRaw('
            SUM(IF(due_date > CURRENT_DATE(), invoice_value, 0)) AS current,
               SUM(IF(due_date < CURRENT_DATE() AND DATEDIFF(due_date, CURRENT_DATE()) >= -30, invoice_value, 0)) AS thirty_days,
               SUM(IF(DATEDIFF(due_date, CURRENT_DATE()) < -30 AND DATEDIFF(due_date, CURRENT_DATE()) >= -60, invoice_value, 0)) AS sixty_days,
               SUM(IF(DATEDIFF(due_date, CURRENT_DATE()) < -60, due_date, 0)) AS ninety_days_plus,
               SUM(IF(invoice_date < DATE("'.$from_date.'"),invoice_value ,0)) AS opening_balance
        ')->where('bill_status', 1)->where('customer_id', $id)->where('currency', $request->currency)->groupBy('currency')->first();

        //return $total_by_months;
        $parameters = [
            'customer_invoices' => $customer_invoices,
            'statement_date' => Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->day)->lastOfMonth(),
            'opening_bal_date' => Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->day)->startOfMonth(),
            'total_by_month' => $total_by_months,
        ];
        if ($request->print) {

            if (! Storage::exists('statements/customer/')) {
                Storage::makeDirectory('statements/customer/');
            }
            try {
                $storage = storage_path('app/statements/customer/'.$customer_invoices[0]?->customer?->customer_name.'_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('customer.statements.pdf', $parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }

        if ($request->send) {
            if (! Storage::exists('statements/customer/')) {
                Storage::makeDirectory('statements/customer/');
            }

            $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
            $emails = [(isset($request->company_email) ? $request->company_email : null), (isset($request->customer_email) ? $request->customer_email : null)];
            if (isset($multiple_emails)) {
                foreach ($multiple_emails as $user_email) {
                    array_push($emails, $user_email);
                }
            }
            try {
                $storage = storage_path('app/statements/customer/'.$customer_invoices[0]?->customer?->customer_name.'_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('customer.statements.pdf', $parameters)
                    ->format('a4')
                    ->save($storage);

                //Mail::to(Auth::user()->email)->cc(array_filter($emails))->send(new CustomerStatementEmail($customer_invoices[0], $storage));
                SendCustomerStatementEmailJob::dispatch(Auth::user()->email, array_filter($emails), $customer_invoices[0], $storage)->delay(now()->addMinutes(5));

                return redirect()->back()->with('flash_success', 'Your statement has been sent successfully');
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }

        return view('customer.statements.show')->with($parameters);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
