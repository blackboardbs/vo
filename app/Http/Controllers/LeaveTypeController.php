<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLeaveTypeRequest;
use App\Http\Requests\UpdateLeaveTypeRequest;
use App\Models\LeaveType;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LeaveTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $leave_type = LeaveType::sortable('description', 'asc')->paginate($item);

        $leave_type = LeaveType::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $leave_type = LeaveType::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $leave_type = LeaveType::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $leave_type = $leave_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $leave_type = $leave_type->paginate($item);

        $parameters = [
            'leave_type' => $leave_type,
        ];

        return View('master_data.leave_type.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = LeaveType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.leave_type.create')->with($parameters);
    }

    public function store(StoreLeaveTypeRequest $request): RedirectResponse
    {
        $leave_type = new LeaveType();
        $leave_type->description = $request->input('description');
        if ($request->has('accrue')) {
            $leave_type->accrue = '1';
        } else {
            $leave_type->accrue = '0';
        }
        $leave_type->cycle_length = $request->input('cycle_length');
        $leave_type->max_days = $request->input('max_length');
        $leave_type->status = $request->input('status');
        $leave_type->creator_id = auth()->id();
        $leave_type->save();

        return redirect(route('leave_type.index'))->with('flash_success', 'Master Data Leave Type captured successfully');
    }

    public function show($leave_type_id): View
    {
        $parameters = [
            'leave_type' => LeaveType::where('id', '=', $leave_type_id)->get(),
        ];

        return View('master_data.leave_type.show')->with($parameters);
    }

    public function edit(Request $request, $leave_type_id)
    {
        $autocomplete_elements = LeaveType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'leave_type' => LeaveType::where('id', '=', $leave_type_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.leave_type.edit')->with($parameters);
    }

    public function update(UpdateLeaveTypeRequest $request, $leave_type_id)
    {
        $leave_type = LeaveType::find($leave_type_id);
        $leave_type->description = $request->input('description');
        if ($request->has('accrue')) {
            $leave_type->accrue = '1';
        } else {
            $leave_type->accrue = '0';
        }
        $leave_type->cycle_length = $request->input('cycle_length');
        $leave_type->max_days = $request->input('max_length');
        $leave_type->status = $request->input('status');
        $leave_type->creator_id = auth()->id();
        $leave_type->save();

        return redirect(route('leave_type.index'))->with('flash_success', 'Master Data Leave Type saved successfully');
    }

    public function destroy($leave_type_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // LeaveType::destroy($leave_type_id);
        $item = LeaveType::find($leave_type_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('leave_type.index')->with('flash_success', 'Master Data Leave Type suspended successfully');
    }
}
