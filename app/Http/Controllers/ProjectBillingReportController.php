<?php

namespace App\Http\Controllers;

use App\Models\ProjectBillingView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProjectExport;

class ProjectBillingReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $items = $request->input('r') ?? 15;
        
        $projects = ProjectBillingView::query()
        ->when(request()->q, function ($proj){
            $proj->where('project_name','like','%'.request()->q.'%');
        })->when(request()->company, function ($proj){
            $proj->where('company_id',request()->company);
        })->when(request()->customer, function ($proj){
            $proj->where('customer_id',request()->customer);
        })->paginate($items);

        $company_drop_down = [];
        $customer_drop_down = [];

        foreach($projects as $project){
            $company_drop_down[$project->company_id] = $project->company_name;
            $customer_drop_down[$project->customer_id] = $project->customer_name;
        }

        $parameters = [
            'projects' => $projects,
            'company_drop_down'=>$company_drop_down,
            'customer_drop_down'=>$customer_drop_down,
        ];

        if ($request->has('export')) return Excel::download(new ProjectExport($parameters, 'reports.management.excel.project-billing-report'), 'project-billing-report.xlsx');

        return view('reports.management.project_billing')->with($parameters);
    }
}
