<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SystemHealth;
use App\Models\SystemHealthCategory;
use Illuminate\View\View;

class SystemHealthMasterDataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $systems = SystemHealth::with(['category_name:id,name'])->orderBy('category','asc')->paginate($item);

        if ($request->has('q') && $request->input('q') != '') {
            $systems = SystemHealth::with(['category_name:id,name'])->where('kpi', 'like', '%'.$request->input('q').'%')
                ->orderBy('category','asc')->paginate($item);
        }

        $parameters = [
            'systems' => $systems,
        ];

        return View('master_data.system_health.index', $parameters);
    }

    public function create(): View
    {

        $parameters = [
            'categories_dropdown' => SystemHealthCategory::orderBy('id')->pluck('name', 'id')->prepend('Category', '0'),
            'cf_dropdown' => [''=>'Colour','Green'=>'Green','Yellow'=>'Yellow','Red'=>'Red']
        ];

        return View('master_data.system_health.create')->with($parameters);
    }

    public function store(Request $request)
    {
        $system = new SystemHealth();
        $system->category = $request->category;
        $system->unit_of_measure = $request->unit_of_measure;
        $system->kpi = $request->kpi;
        $system->definition = $request->definition;
        $system->problem = $request->problem;
        $system->resolution = $request->resolution;
        $system->cf_coulour_01 = $request->cf1;
        $system->cf_coulour_02 = $request->cf2;
        $system->cf_coulour_03 = $request->cf3;
        $system->health_score_01 = $request->hs1;
        $system->health_score_02 = $request->hs2;
        $system->health_score_03 = $request->hs3;
        $system->measure_calculation = $request->measure_calculation;
        $system->list_url = $request->url;
        $system->list_filters = $request->filters;
        $system->fix = $request->fix;
        $system->save();

        return redirect(route('system_health_master.index'))->with('flash_success', 'Master Data System Health captured successfully');
    }

    public function show($id): View
    {
        $parameters = [
            'system' => SystemHealth::where('id', '=', $id)->first(),
        ];

        return View('master_data.system_health.show')->with($parameters);
    }

    public function edit($id): View
    {
        $parameters = [
            'categories_dropdown' => SystemHealthCategory::orderBy('id')->pluck('name', 'id')->prepend('Category', '0'),
            'cf_dropdown' => [''=>'Colour','Green'=>'Green','Amber'=>'Amber','Red'=>'Red'],
            'system' => SystemHealth::where('id', $id)->first(),
        ];
        // dd(SystemHealth::where('id', $id)->first());

        return View('master_data.system_health.edit')->with($parameters);
    }

    public function update(Request $request,$id)
    {
        $system = SystemHealth::find($id);
        $system->category = $request->category;
        $system->unit_of_measure = $request->unit_of_measure;
        $system->kpi = $request->kpi;
        $system->definition = $request->definition;
        $system->problem = $request->problem;
        $system->resolution = $request->resolution;
        $system->cf_coulour_01 = $request->cf1;
        $system->cf_coulour_02 = $request->cf2;
        $system->cf_coulour_03 = $request->cf3;
        $system->health_score_01 = $request->hs1;
        $system->health_score_02 = $request->hs2;
        $system->health_score_03 = $request->hs3;
        $system->measure_calculation = $request->measure_calculation;
        $system->list_url = $request->url;
        $system->list_filters = $request->filters;
        $system->fix = $request->fix;
        $system->save();

        return redirect(route('system_health_master.index'))->with('flash_success', 'Master Data System Health updated successfully');
    }
    
    public function destroy($id)
    {
        SystemHealth::destroy($id);

        return redirect(route('system_health_master.index'))->with('flash_success', 'Master Data System Health deleted successfully');
    }
}
