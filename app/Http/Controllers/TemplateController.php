<?php

namespace App\Http\Controllers;

use App\Models\AssessmentActivities;
use App\Models\AssessmentConcern;
use App\Models\AssessmentHeader;
use App\Models\AssessmentMeasure;
use App\Models\AssessmentNotes;
use App\Models\AssessmentPlannedNext;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Cv;
use App\Models\Expense;
use App\Http\Requests\StoreTemplateRequest;
use App\Http\Requests\UpdateTemplateRequest;
use App\Models\Module;
use App\Models\Quotation;
use App\Models\Template;
use App\Models\TemplateType;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;
use TCPDF;

class TemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $templates = Template::with(['type:id,name']);
        //$temptemplates = AdvancedTemplates::with('user');

        $module = Module::where('name', '=', 'App\Models\DocumentTemplates')->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $templates = $templates->whereIn('creator_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if (($request->has('q') && $request->input('q') != '')) {
            $templates = $templates->where('name', 'LIKE', '%'.$request->input('q').'%')
                ->orWhere('template_type_id', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('type', function ($query) use ($request) {
                    $query->where('name', 'like', '%'.$request->q.'%');
                });
        }

        if (($request->has('type') && $request->input('type') != '')) {
            $templates = $templates->where('template_type_id', $request->type);
        }

        $templates = $templates->paginate($item);
        //$temptemplates = $temptemplates->get();

        $parameters = [
            'templates' => $templates,
            //'temptemplates' => $temptemplates,
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
        ];

        return view('template.index', $parameters);
    }

    public function create(): View
    {
        $template = [];
        $assignment_columns = Schema::getColumnListing('assignment');
        $template_fields[1] = $assignment_columns;

        $parameters = [
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
            'template_fields' => $template_fields,
        ];

        return view('template.create', $parameters);
    }

    public function preview(): View
    {
        $parameters = [
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
        ];

        return view('template.preview', $parameters);
    }

    public function store(StoreTemplateRequest $request): RedirectResponse
    {
        $template = new Template;
        $template->name = $request->input('name');
        $template->template_type_id = $request->input('template_type_id');
        //$template->content = $request->input('content');
        $template->content = $request->input('area_editor');

        $file_name_html = 'app\\public\\templates\\file_'.date('Y_m_d_H_i_s').'.html';
        $file_name_pdf = 'app\\public\\templates\\file_'.date('Y_m_d_H_i_s').'.pdf';
        //PDF::loadHTML($template->content)->save(storage_path($file_name_pdf));

        $template->file_url = $file_name_html;

        $template_file = fopen(storage_path($file_name_html), 'w') or exit('Unable to open file!');
        fwrite($template_file, $template->content);
        fclose($template_file);

        $template->save();

        return redirect(route('template.index'))->with('flash_success', 'Template captured successfully');
    }

    public function edit($template_id): View
    {
        $template = Template::find($template_id);
        $parameters = [
            'template' => $template,
            'template_type_drop_down' => TemplateType::orderBy('name')->pluck('name', 'id'),
        ];

        return view('template.edit')->with($parameters);
    }

    public function update(UpdateTemplateRequest $request, $template_id): RedirectResponse
    {
        $template = Template::find($template_id);
        $template->name = $request->input('name');
        $template->template_type_id = $request->input('template_type_id');
        //$template->file_url = "test.html";
        $file_name_html = 'app\\public\\templates\\file_'.date('Y_m_d_H_i_s').'.html';
        //$template->content = $request->input('content');
        $template->content = $request->input('area_editor');

        $template->file_url = $file_name_html;

        $template_file = fopen(storage_path($file_name_html), 'w') or exit('Unable to open file!');
        fwrite($template_file, $template->content);
        fclose($template_file);

        $template->save();

        return redirect(route('template.index'))->with('flash_success', 'Template updated successfully');
    }

    public function destroy($template_id): RedirectResponse
    {
        DB::table('template')->delete($template_id);
        Template::destroy($template_id);

        return redirect()->route('template.index')->with('flash_success', 'Template deleted successfully');
    }

    public function download_old($template_id)
    {
        $template = Template::find($template_id);
        $file_name_pdf = 'app\\public\\templates\\file_'.date('Y_m_d_H_i_s').'.pdf';

        return PDF::loadFile(storage_path($template->file_url))->save(storage_path($file_name_pdf))->stream('download.pdf');
    }

    public function getVariables($template_id, Request $request): JsonResponse
    {
        $template_fields = '';
        switch ($template_id) {
            case 1:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Resource Contract</p>";
                $template_fields .= '{SUPPLIERNAME}, {RESOURCENAME}, {SERVICESRENDERED}, {PROJECTNAME}, {DESCRIPTIONOFASSIGNMENT}, {EFFECTIVEDATE}, {TERMINATIONDATE}, {RATEPERHOUR}, {TOTALHOURS}, {HOURSOFWORK}, {LOCATION}, {TRAVEL}, {ACCOMODATION}, {PARKING}, {PERDIEM}, {OUTOFTOWN}, {DATA}, {OTHER}, {BILLING}, {PAYMENTS}';
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Project</p>";
                $template_fields_tmp = Schema::getColumnListing('projects');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{P'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Assignment</p>";
                $template_fields_tmp = Schema::getColumnListing('assignment');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{A'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 2:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>CV</p>";
                $template_fields_tmp = Schema::getColumnListing('cv');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{CV'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Supplier Contract</p>";
                $template_fields .= '{SUPPLIERNAME}, {RESOURCENAME}, {SERVICESRENDERED}, {PROJECTNAME}, {DESCRIPTIONOFASSIGNMENT}, {EFFECTIVEDATE}, {TERMINATIONDATE}, {RATEPERHOUR}, {TOTALHOURS}, {HOURSOFWORK}, {LOCATION}, {TRAVEL}, {ACCOMODATION}, {PARKING}, {PERDIEM}, {OUTOFTOWN}, {DATA}, {OTHER}, {BILLING}, {PAYMENTS}';
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{V'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }

        return response()->json(['template_fields' => $template_fields]);
    }

    public function previewTemplate_old($template_type, $template_id, $assignment_id, $name_type, Request $request)
    {
        $template = Template::find($template_id);

        $template_fields = '';

        $content = $template->content;
        $template_fields_tmp = [];
        $old = [];
        $new = [];
        switch ($template_type) {
            //switch ($template_type){
            case 1:

                $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->get()->ToArray();
                //dd($assignment);
                if ($name_type == 1) {
                    $vendor_user = User::find($assignment[0]['vendor_id']);
                    $resource_user = User::find($assignment[0]['consultant']['id']);
                } else {
                    $vendor_user = User::find($assignment[0]['vendor_id']);
                    $resource_user = User::find($assignment[0]['consultant']['id']);
                }

                $variables = ['{SUPPLIERNAME}', '{RESOURCENAME}'];
                $replaced_with = ['', ''];
                //$replaced_with = Array($vendor_user->first_name.' '.$vendor_user->last_name, $resource_user->first_name.' '.$resource_user->last_name);
                $content = str_replace($variables, $replaced_with, $template->content);

                $variables = ['{PID}', '{PNAME}', '{PREF}', '{PTYPE}', '{PCUSTOMERID}', '{PCUSTOMERPO}', '{PPROJECTTYPEID}', '{PQUOTATIONID}', '{PCUSTOMERREF}', '{PCONSULTANTS}', '{PMANAGERID}', '{PASSIGNMENTAPPROVERID}', '{PSTARTDATE}', '{PENDDATE}', '{PESTLABOURCOST}', '{PESTOTHERCOST}', '{PEXPID}', '{PBILLABLE}', '{PBILLINGNOTE}', '{PSTATUSID}', '{PCREATORID}', '{PCOMPANYID}', '{PISVENDOR}', '{PVENDORBILLINGNOTE}', '{PLOCATION}', '{PHOURSOFWORK}', '{PSITEID}', '{PCREATEDAT}', '{PUPDATEDAT}'];
                $replaced_with = [$assignment[0]['project']['id'], $assignment[0]['project']['name'], $assignment[0]['project']['ref'], $assignment[0]['project']['type'], $assignment[0]['project']['customer_id'], $assignment[0]['project']['customer_po'], $assignment[0]['project']['project_type_id'], $assignment[0]['project']['quotation_id'], $assignment[0]['project']['customer_ref'], $assignment[0]['project']['consultants'], $assignment[0]['project']['manager_id'], $assignment[0]['project']['assignment_approver_id'], $assignment[0]['project']['start_date'], $assignment[0]['project']['end_date'], $assignment[0]['project']['est_labour_cost'], $assignment[0]['project']['est_other_cost'], $assignment[0]['project']['exp_id'], $assignment[0]['project']['billable'], $assignment[0]['project']['billing_note'], $assignment[0]['project']['status_id'], $assignment[0]['project']['creator_id'], $assignment[0]['project']['company_id'], $assignment[0]['project']['is_vendor'], $assignment[0]['project']['vendor_billing_note'], $assignment[0]['project']['location'], $assignment[0]['project']['hours_of_work'], $assignment[0]['project']['site_id'], $assignment[0]['project']['created_at'], $assignment[0]['project']['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{AID}', '{AEMPLOYEEID}', '{APROJECTID}', '{ASTARTDATE}', '{AENDDATE}', '{ARATE}', '{ABONUSRATE}', '{AHOURS}', '{ANUMBERHOURS}', '{AVENDORNO}', '{AREPORTTO}', '{AREPORTTOEMAIL}', '{AREPORTPHONE}', '{ABILLABLE}', '{ANOTE1}', '{ANOTE2}', '{AEXPID}', '{AFUNCTION}', '{ASYSTEM}', '{ACOUNTRYID}', '{AVATRATEID}', '{ACURRENCY}', '{AINTERNALCOSTRATE}', '{AEXTERNALCOSTRATE}', '{AINVOICERATE}', '{AEXTENSIONID}', '{AASSIGNMENTSTATUS}', '{ACLAIMAPPROVERID}', '{AMEDICALCERTIFICATETYPE}', '{AMEDICALCERTIFICATETEXT}', '{AASSIGNMENTAPPROVER}', '{APROJECTTYPE}', '{ASTATUS}', '{ACREATORID}', '{ACOMPANYID}', '{ALOCATION}', '{AHOURSOFWORK}', '{AVENDORTEMPLATEID}', '{ARESOURCETEMPLATEID}', '{AROLE}', '{AISSUEDATE}', '{AAPPROVEDDATE}', '{ASITEID}', '{ACREATEDAT}', '{AUPDATEDAT}'];
                $replaced_with = [$assignment[0]['id'], $assignment[0]['employee_id'], $assignment[0]['project_id'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['rate'], $assignment[0]['bonus_rate'], $assignment[0]['number_hours'], $assignment[0]['vendor_id'], $assignment[0]['report_to'], $assignment[0]['report_to_email'], $assignment[0]['report_phone'], $assignment[0]['billable'], $assignment[0]['note_1'], $assignment[0]['note_2'], $assignment[0]['exp_id'], $assignment[0]['function'], $assignment[0]['system'], $assignment[0]['country_id'], $assignment[0]['vat_rate_id'], $assignment[0]['currency'], $assignment[0]['internal_cost_rate'], $assignment[0]['external_cost_rate'], $assignment[0]['invoice_rate'], $assignment[0]['extension_id'], $assignment[0]['assignment_status'], $assignment[0]['claim_approver_id'], $assignment[0]['medical_certificate_type'], $assignment[0]['medical_certificate_text'], $assignment[0]['assignment_approver'], $assignment[0]['project_type'], $assignment[0]['status'], $assignment[0]['creator_id'], $assignment[0]['company_id'], $assignment[0]['location'], $assignment[0]['hours_of_work'], $assignment[0]['vendor_template_id'], $assignment[0]['resource_template_id'], $assignment[0]['role'], $assignment[0]['issue_date'], $assignment[0]['approved_date'], $assignment[0]['site_id'], $assignment[0]['created_at'], $assignment[0]['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                break;
            case 2:
                $cv = CV::with('user', 'cv')->where('id', '=', $assignment_id)->get()->ToArray();
                //dd($cv);
                $variables = ['{UFIRSTNAME}', '{ULASTNAME}', '{URESPREFNAME}', '{UIDNO}', '{UPASSPORTNO}', '{UNATIONALITY}', '{URACE}', '{UGENDER}', '{UHEALTH}', '{UDOB}', '{UPLACEOFBIRTH}', '{UOTHERLANGUAGE}', '{UDRIVERSLICENSE}', '{UMAINQUALIFICATION}', '{UMAINSKILL}'];
                $replaced_with = [$cv[0]['user']['first_name'], $cv[0]['user']['last_name'], $cv[0]['resource_pref_name'], $cv[0]['id_no'], $cv[0]['nationality'], $cv[0]['race'], $cv[0]['gender'], $cv[0]['health'], $cv[0]['date_of_birth'], $cv[0]['place_of_birth'], $cv[0]['home_language'], $cv[0]['drivers_license'], $cv[0]['main_qualification'], $cv[0]['main_skill']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{UEMAIL}', '{UFAX}', '{UCELL}'];
                $replaced_with = [$cv[0]['user']['email'], $cv[0]['fax'], $cv[0]['user']['phone']];
                $content = str_replace($variables, $replaced_with, $content);

                if (isset($cv[0]['cv'][0])) {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [$cv[0]['cv'][0]['period'], $cv[0]['cv'][0]['company'], $cv[0]['cv'][0]['project'], $cv[0]['cv'][0]['role'], $cv[0]['cv'][0]['responsibility'], $cv[0]['cv'][0]['tools']];
                    $content = str_replace($variables, $replaced_with, $content);
                } else {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [' ', ' ', ' ', ' ', ' ', ' '];
                    $content = str_replace($variables, $replaced_with, $content);
                }
                $template_fields_tmp = Schema::getColumnListing('cv');
                foreach ($template_fields_tmp as $template_field) {
                    $template .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }
    }

    public function previewTemplate_revisit($template_type, $template_id, $assignment_id, $name_type, Request $request)
    {
        $template = Template::find($template_id);

        $template_fields = '';

        $content = $template->content;

        $template_fields_tmp = [];
        $old = [];
        $new = [];
        switch ($template_type) {
            //switch ($template_type){
            case 1:

                $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->get()->ToArray();
                //dd($assignment);
                if ($name_type == 1) {
                    $vendor_user = User::find($assignment[0]['vendor_id']);
                    $resource_user = User::find($assignment[0]['consultant']['id']);
                } else {
                    $vendor_user = User::find($assignment[0]['vendor_id']);
                    $resource_user = User::find($assignment[0]['consultant']['id']);
                }

                $variables = ['{SUPPLIERNAME}', '{RESOURCENAME}'];
                $replaced_with = ['', ''];
                //$replaced_with = Array($vendor_user->first_name.' '.$vendor_user->last_name, $resource_user->first_name.' '.$resource_user->last_name);
                $content = str_replace($variables, $replaced_with, $template->content);

                $variables = ['{PID}', '{PNAME}', '{PREF}', '{PTYPE}', '{PCUSTOMERID}', '{PCUSTOMERPO}', '{PPROJECTTYPEID}', '{PQUOTATIONID}', '{PCUSTOMERREF}', '{PCONSULTANTS}', '{PMANAGERID}', '{PASSIGNMENTAPPROVERID}', '{PSTARTDATE}', '{PENDDATE}', '{PESTLABOURCOST}', '{PESTOTHERCOST}', '{PEXPID}', '{PBILLABLE}', '{PBILLINGNOTE}', '{PSTATUSID}', '{PCREATORID}', '{PCOMPANYID}', '{PISVENDOR}', '{PVENDORBILLINGNOTE}', '{PLOCATION}', '{PHOURSOFWORK}', '{PSITEID}', '{PCREATEDAT}', '{PUPDATEDAT}'];
                $replaced_with = [$assignment[0]['project']['id'], $assignment[0]['project']['name'], $assignment[0]['project']['ref'], $assignment[0]['project']['type'], $assignment[0]['project']['customer_id'], $assignment[0]['project']['customer_po'], $assignment[0]['project']['project_type_id'], $assignment[0]['project']['quotation_id'], $assignment[0]['project']['customer_ref'], $assignment[0]['project']['consultants'], $assignment[0]['project']['manager_id'], $assignment[0]['project']['assignment_approver_id'], $assignment[0]['project']['start_date'], $assignment[0]['project']['end_date'], $assignment[0]['project']['est_labour_cost'], $assignment[0]['project']['est_other_cost'], $assignment[0]['project']['exp_id'], $assignment[0]['project']['billable'], $assignment[0]['project']['billing_note'], $assignment[0]['project']['status_id'], $assignment[0]['project']['creator_id'], $assignment[0]['project']['company_id'], $assignment[0]['project']['is_vendor'], $assignment[0]['project']['vendor_billing_note'], $assignment[0]['project']['location'], $assignment[0]['project']['hours_of_work'], $assignment[0]['project']['site_id'], $assignment[0]['project']['created_at'], $assignment[0]['project']['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{AID}', '{AEMPLOYEEID}', '{APROJECTID}', '{ASTARTDATE}', '{AENDDATE}', '{ARATE}', '{ABONUSRATE}', '{AHOURS}', '{ANUMBERHOURS}', '{AVENDORNO}', '{AREPORTTO}', '{AREPORTTOEMAIL}', '{AREPORTPHONE}', '{ABILLABLE}', '{ANOTE1}', '{ANOTE2}', '{AEXPID}', '{AFUNCTION}', '{ASYSTEM}', '{ACOUNTRYID}', '{AVATRATEID}', '{ACURRENCY}', '{AINTERNALCOSTRATE}', '{AEXTERNALCOSTRATE}', '{AINVOICERATE}', '{AEXTENSIONID}', '{AASSIGNMENTSTATUS}', '{ACLAIMAPPROVERID}', '{AMEDICALCERTIFICATETYPE}', '{AMEDICALCERTIFICATETEXT}', '{AASSIGNMENTAPPROVER}', '{APROJECTTYPE}', '{ASTATUS}', '{ACREATORID}', '{ACOMPANYID}', '{ALOCATION}', '{AHOURSOFWORK}', '{AVENDORTEMPLATEID}', '{ARESOURCETEMPLATEID}', '{AROLE}', '{AISSUEDATE}', '{AAPPROVEDDATE}', '{ASITEID}', '{ACREATEDAT}', '{AUPDATEDAT}'];
                $replaced_with = [$assignment[0]['id'], $assignment[0]['employee_id'], $assignment[0]['project_id'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['rate'], $assignment[0]['bonus_rate'], $assignment[0]['number_hours'], $assignment[0]['vendor_id'], $assignment[0]['report_to'], $assignment[0]['report_to_email'], $assignment[0]['report_phone'], $assignment[0]['billable'], $assignment[0]['note_1'], $assignment[0]['note_2'], $assignment[0]['exp_id'], $assignment[0]['function'], $assignment[0]['system'], $assignment[0]['country_id'], $assignment[0]['vat_rate_id'], $assignment[0]['currency'], $assignment[0]['internal_cost_rate'], $assignment[0]['external_cost_rate'], $assignment[0]['invoice_rate'], $assignment[0]['extension_id'], $assignment[0]['assignment_status'], $assignment[0]['claim_approver_id'], $assignment[0]['medical_certificate_type'], $assignment[0]['medical_certificate_text'], $assignment[0]['assignment_approver'], $assignment[0]['project_type'], $assignment[0]['status'], $assignment[0]['creator_id'], $assignment[0]['company_id'], $assignment[0]['location'], $assignment[0]['hours_of_work'], $assignment[0]['vendor_template_id'], $assignment[0]['resource_template_id'], $assignment[0]['role'], $assignment[0]['issue_date'], $assignment[0]['approved_date'], $assignment[0]['site_id'], $assignment[0]['created_at'], $assignment[0]['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                break;
            case 2:
                $cv = CV::with('user', 'cv')->where('id', '=', $assignment_id)->get()->ToArray();
                //dd($cv);
                $variables = ['{UFIRSTNAME}', '{ULASTNAME}', '{URESPREFNAME}', '{UIDNO}', '{UPASSPORTNO}', '{UNATIONALITY}', '{URACE}', '{UGENDER}', '{UHEALTH}', '{UDOB}', '{UPLACEOFBIRTH}', '{UOTHERLANGUAGE}', '{UDRIVERSLICENSE}', '{UMAINQUALIFICATION}', '{UMAINSKILL}'];
                $replaced_with = [$cv[0]['user']['first_name'], $cv[0]['user']['last_name'], $cv[0]['resource_pref_name'], $cv[0]['id_no'], $cv[0]['nationality'], $cv[0]['race'], $cv[0]['gender'], $cv[0]['health'], $cv[0]['date_of_birth'], $cv[0]['place_of_birth'], $cv[0]['home_language'], $cv[0]['drivers_license'], $cv[0]['main_qualification'], $cv[0]['main_skill']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{UEMAIL}', '{UFAX}', '{UCELL}'];
                $replaced_with = [$cv[0]['user']['email'], $cv[0]['fax'], $cv[0]['user']['phone']];
                $content = str_replace($variables, $replaced_with, $content);

                if (isset($cv[0]['cv'][0])) {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [$cv[0]['cv'][0]['period'], $cv[0]['cv'][0]['company'], $cv[0]['cv'][0]['project'], $cv[0]['cv'][0]['role'], $cv[0]['cv'][0]['responsibility'], $cv[0]['cv'][0]['tools']];
                    $content = str_replace($variables, $replaced_with, $content);
                } else {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [' ', ' ', ' ', ' ', ' ', ' '];
                    $content = str_replace($variables, $replaced_with, $content);
                }
                $template_fields_tmp = Schema::getColumnListing('cv');
                foreach ($template_fields_tmp as $template_field) {
                    $template .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        /*// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //$pdf->SetFont('dejavusans', '', 14, '', true);

        $pdf->AddPage();
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        // Set some content to print
        $html = $content;

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->Output('example_001.pdf', 'I');

        /*$file_name_html = "app\\public\\templates\\file_".date("Y_m_d_H_i_s").".html";

        $template_file = fopen(storage_path($file_name_html), "w") or die("Unable to open file!");
        fwrite($template_file, $content);
        fclose($template_file);

        $file_name_pdf = "app\\public\\templates\\file_".date("Y_m_d_H_i_s").".pdf";
        return PDF::loadFile(storage_path($file_name_html))->save(storage_path($file_name_pdf))->stream('download.pdf');*/
    }

    public function previewTemplate($template_type, $template_id, $assignment_id, $name_type, Request $request)
    {
        $template = Template::find($template_id);

        $template_fields = '';

        $content = $template->content;

        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->get()->ToArray();
        $expense = Expense::find($assignment[0]['exp_id']);

        $resource_user = User::find($assignment[0]['consultant']['id']);

        $supplier_name = '';
        if ($resource_user->vendor_id > 0 && $resource_user->vendor_id != '' && $resource_user->vendor_id != null) {
            $vendor = Vendor::find($resource_user->vendor_id);
            $supplier_name = $vendor->vendor_name;
        }
        $resource_name = $resource_user->first_name.' '.$resource_user->last_name;

        $variables = ['{SUPPLIERNAME}', '{RESOURCENAME}', '{SERVICESRENDERED}', '{PROJECTNAME}', '{DESCRIPTIONOFASSIGNMENT}', '{EFFECTIVEDATE}', '{TERMINATIONDATE}', '{RATEPERHOUR}', '{TOTALHOURS}', '{HOURSOFWORK}', '{LOCATION}', '{TRAVEL}', '{ACCOMODATION}', '{PARKING}', '{PERDIEM}', '{OUTOFTOWN}', '{DATA}', '{OTHER}', '{BILLING}', '{PAYMENTS}'];
        $replaced_with = [$supplier_name, $resource_name, $assignment[0]['note_1'], $assignment[0]['project']['name'], $assignment[0]['note_2'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['invoice_rate'], $assignment[0]['hours'], $assignment[0]['hours_of_work'], $assignment[0]['location'], isset($expense->travel) ? $expense->travel : '', isset($expense->accomodation) ? $expense->accomodation : '', isset($expense->parking) ? $expense->parking : '', isset($expense->per_diem) ? $expense->per_diem : '', isset($expense->out_of_town) ? $expense->out_of_town : '', isset($expense->data) ? $expense->data : '', isset($expense->other) ? $expense->other : '', $assignment[0]['billable'] == 1 ? 'Yes' : 'No', $assignment[0]['invoice_rate'] * $assignment[0]['hours']];
        //$replaced_with = Array($vendor_user->first_name.' '.$vendor_user->last_name, $resource_user->first_name.' '.$resource_user->last_name);
        $content = str_replace($variables, $replaced_with, $template->content);

        switch ($template_type) {
            //switch ($template_type){
            case 1:

                $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->get()->ToArray();
                $expense = Expense::find($assignment[0]['exp_id']);

                $resource_user = User::find($assignment[0]['consultant']['id']);

                $supplier_name = '';
                if ($resource_user->vendor_id > 0 && $resource_user->vendor_id != '' && $resource_user->vendor_id != null) {
                    $vendor = Vendor::find($resource_user->vendor_id);
                    $supplier_name = $vendor->vendor_name;
                }
                $resource_name = $resource_user->first_name.' '.$resource_user->last_name;

//                $variables = Array('{SUPPLIERNAME}', '{RESOURCENAME}', '{SERVICESRENDERED}', '{PROJECTNAME}', '{DESCRIPTIONOFASSIGNMENT}', '{EFFECTIVEDATE}', '{TERMINATIONDATE}', '{RATEPERHOUR}', '{TOTALHOURS}', '{HOURSOFWORK}', '{LOCATION}', '{TRAVEL}', '{ACCOMODATION}', '{PARKING}', '{PERDIEM}', '{OUTOFTOWN}', '{DATA}', '{OTHER}', '{BILLING}', '{PAYMENTS}');
//                $replaced_with = Array($supplier_name, $resource_name, $assignment[0]['note_1'], $assignment[0]['project']['name'], $assignment[0]['note_2'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['invoice_rate'], $assignment[0]['hours'], $assignment[0]['hours_of_work'], $assignment[0]['location'], isset($expense->travel)?$expense->travel:'', isset($expense->accomodation)?$expense->accomodation:'', isset($expense->parking)?$expense->parking:'', isset($expense->per_diem)?$expense->per_diem:'', isset($expense->out_of_town)?$expense->out_of_town:'', isset($expense->data)?$expense->data:'', isset($expense->other)?$expense->other:'', $assignment[0]['billable'] == 1 ? 'Yes' : 'No', $assignment[0]['invoice_rate'] * $assignment[0]['hours']);
                //$replaced_with = Array($vendor_user->first_name.' '.$vendor_user->last_name, $resource_user->first_name.' '.$resource_user->last_name);
                $content = str_replace($variables, $replaced_with, $template->content);

                $variables = ['{PID}', '{PNAME}', '{PREF}', '{PTYPE}', '{PCUSTOMERID}', '{PCUSTOMERPO}', '{PPROJECTTYPEID}', '{PQUOTATIONID}', '{PCUSTOMERREF}', '{PCONSULTANTS}', '{PMANAGERID}', '{PASSIGNMENTAPPROVERID}', '{PSTARTDATE}', '{PENDDATE}', '{PESTLABOURCOST}', '{PESTOTHERCOST}', '{PEXPID}', '{PBILLABLE}', '{PBILLINGNOTE}', '{PSTATUSID}', '{PCREATORID}', '{PCOMPANYID}', '{PISVENDOR}', '{PVENDORBILLINGNOTE}', '{PLOCATION}', '{PHOURSOFWORK}', '{PSITEID}', '{PCREATEDAT}', '{PUPDATEDAT}'];
                $replaced_with = [$assignment[0]['project']['id'], $assignment[0]['project']['name'], $assignment[0]['project']['ref'], $assignment[0]['project']['type'], $assignment[0]['project']['customer_id'], $assignment[0]['project']['customer_po'], $assignment[0]['project']['project_type_id'], $assignment[0]['project']['quotation_id'], $assignment[0]['project']['customer_ref'], $assignment[0]['project']['consultants'], $assignment[0]['project']['manager_id'], $assignment[0]['project']['assignment_approver_id'], $assignment[0]['project']['start_date'], $assignment[0]['project']['end_date'], $assignment[0]['project']['est_labour_cost'], $assignment[0]['project']['est_other_cost'], $assignment[0]['project']['exp_id'], $assignment[0]['project']['billable'], $assignment[0]['project']['billing_note'], $assignment[0]['project']['status_id'], $assignment[0]['project']['creator_id'], $assignment[0]['project']['company_id'], $assignment[0]['project']['is_vendor'], $assignment[0]['project']['vendor_billing_note'], $assignment[0]['project']['location'], $assignment[0]['project']['hours_of_work'], $assignment[0]['project']['site_id'], $assignment[0]['project']['created_at'], $assignment[0]['project']['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{AID}', '{AEMPLOYEEID}', '{APROJECTID}', '{ASTARTDATE}', '{AENDDATE}', '{ARATE}', '{ABONUSRATE}', '{AHOURS}', '{ANUMBERHOURS}', '{AVENDORNO}', '{AREPORTTO}', '{AREPORTTOEMAIL}', '{AREPORTPHONE}', '{ABILLABLE}', '{ANOTE1}', '{ANOTE2}', '{AEXPID}', '{AFUNCTION}', '{ASYSTEM}', '{ACOUNTRYID}', '{AVATRATEID}', '{ACURRENCY}', '{AINTERNALCOSTRATE}', '{AEXTERNALCOSTRATE}', '{AINVOICERATE}', '{AEXTENSIONID}', '{AASSIGNMENTSTATUS}', '{ACLAIMAPPROVERID}', '{AMEDICALCERTIFICATETYPE}', '{AMEDICALCERTIFICATETEXT}', '{AASSIGNMENTAPPROVER}', '{APROJECTTYPE}', '{ASTATUS}', '{ACREATORID}', '{ACOMPANYID}', '{ALOCATION}', '{AHOURSOFWORK}', '{AVENDORTEMPLATEID}', '{ARESOURCETEMPLATEID}', '{AROLE}', '{AISSUEDATE}', '{AAPPROVEDDATE}', '{ASITEID}', '{ACREATEDAT}', '{AUPDATEDAT}'];
                $replaced_with = [$assignment[0]['id'], $assignment[0]['employee_id'], $assignment[0]['project_id'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['rate'], $assignment[0]['bonus_rate'], $assignment[0]['number_hours'], $assignment[0]['vendor_id'], $assignment[0]['report_to'], $assignment[0]['report_to_email'], $assignment[0]['report_phone'], $assignment[0]['billable'], $assignment[0]['note_1'], $assignment[0]['note_2'], $assignment[0]['exp_id'], $assignment[0]['function'], $assignment[0]['system'], $assignment[0]['country_id'], $assignment[0]['vat_rate_id'], $assignment[0]['currency'], $assignment[0]['internal_cost_rate'], $assignment[0]['external_cost_rate'], $assignment[0]['invoice_rate'], $assignment[0]['extension_id'], $assignment[0]['assignment_status'], $assignment[0]['claim_approver_id'], $assignment[0]['medical_certificate_type'], $assignment[0]['medical_certificate_text'], $assignment[0]['assignment_approver'], $assignment[0]['project_type'], $assignment[0]['status'], $assignment[0]['creator_id'], $assignment[0]['company_id'], $assignment[0]['location'], $assignment[0]['hours_of_work'], $assignment[0]['vendor_template_id'], $assignment[0]['resource_template_id'], $assignment[0]['role'], $assignment[0]['issue_date'], $assignment[0]['approved_date'], $assignment[0]['site_id'], $assignment[0]['created_at'], $assignment[0]['updated_at']];
                $content = str_replace($variables, $replaced_with, $content);

                break;
            case 2:
                $cv = CV::with('user', 'cv')->where('id', '=', $assignment_id)->get()->ToArray();
                //dd($cv);
                $variables = ['{UFIRSTNAME}', '{ULASTNAME}', '{URESPREFNAME}', '{UIDNO}', '{UPASSPORTNO}', '{UNATIONALITY}', '{URACE}', '{UGENDER}', '{UHEALTH}', '{UDOB}', '{UPLACEOFBIRTH}', '{UOTHERLANGUAGE}', '{UDRIVERSLICENSE}', '{UMAINQUALIFICATION}', '{UMAINSKILL}'];
                $replaced_with = [$cv[0]['user']['first_name'], $cv[0]['user']['last_name'], $cv[0]['resource_pref_name'], $cv[0]['id_no'], $cv[0]['nationality'], $cv[0]['race'], $cv[0]['gender'], $cv[0]['health'], $cv[0]['date_of_birth'], $cv[0]['place_of_birth'], $cv[0]['home_language'], $cv[0]['drivers_license'], $cv[0]['main_qualification'], $cv[0]['main_skill']];
                $content = str_replace($variables, $replaced_with, $content);

                $variables = ['{UEMAIL}', '{UFAX}', '{UCELL}'];
                $replaced_with = [$cv[0]['user']['email'], $cv[0]['fax'], $cv[0]['user']['phone']];
                $content = str_replace($variables, $replaced_with, $content);

                if (isset($cv[0]['cv'][0])) {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [$cv[0]['cv'][0]['period'], $cv[0]['cv'][0]['company'], $cv[0]['cv'][0]['project'], $cv[0]['cv'][0]['role'], $cv[0]['cv'][0]['responsibility'], $cv[0]['cv'][0]['tools']];
                    $content = str_replace($variables, $replaced_with, $content);
                } else {
                    $variables = ['{EPERIOD}', '{ECOMPANY}', '{EPROJECT}', '{EROLE}', '{ERESPOSIBILITY}', '{ETOOLS}'];
                    $replaced_with = [' ', ' ', ' ', ' ', ' ', ' '];
                    $content = str_replace($variables, $replaced_with, $content);
                }
                $template_fields_tmp = Schema::getColumnListing('cv');
                foreach ($template_fields_tmp as $template_field) {
                    $template .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            case 3:
                $template_fields .= "<p style='font-weight: bold; padding-top: 10px; padding-bottom: 0px;'>Vendor</p>";
                $template_fields_tmp = Schema::getColumnListing('vendor');
                foreach ($template_fields_tmp as $template_field) {
                    $template_fields .= '{'.strtoupper(str_replace('_', '', $template_field)).'} ';
                }
                break;
            default:
                break;
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        /*// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //$pdf->SetFont('dejavusans', '', 14, '', true);

        $pdf->AddPage();
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        // Set some content to print
        $html = $content;

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->Output('example_001.pdf', 'I');

        /*$file_name_html = "app\\public\\templates\\file_".date("Y_m_d_H_i_s").".html";

        $template_file = fopen(storage_path($file_name_html), "w") or die("Unable to open file!");
        fwrite($template_file, $content);
        fclose($template_file);

        $file_name_pdf = "app\\public\\templates\\file_".date("Y_m_d_H_i_s").".pdf";
        return PDF::loadFile(storage_path($file_name_html))->save(storage_path($file_name_pdf))->stream('download.pdf');*/
    }

    public function download($template_id)
    {
        $template = Template::find($template_id);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        /*$pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 001');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));*/

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        /*// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //$pdf->SetFont('dejavusans', '', 14, '', true);

        $pdf->AddPage();
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        // Set some content to print
        $html = $template->content;

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->Output('tmp_document.pdf', 'I');
    }

    public function getTemplate($document_id, Request $request)
    {
        //dd($request->input('resource'));

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            //return view('template.asset_aknowledgement_form');

            switch ($document_id) {
                case 1:

                    $assignment_id = $request->has('id') ? $request->input('id') : 0;

                    $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();

                    $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
                    $claim_approver_user = User::find(isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0);
                    $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
                    $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
                    $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

                    $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

                    $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
                    $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

                    $vendor = Vendor::find(isset($resource_user->vendor_id) ? $resource_user->vendor_id : 0);

                    $resource_name = $resource_first_name.' '.$resource_last_name;
                    $vendor_terms = isset($vendor->payment_terms_days) ? $vendor->payment_terms_days : 0;

                    $resource = User::find($request->id);
                    $parameters = [
                        'resource' => $resource,
                        'resource_name' => $resource_name,
                        'claim_approver' => $claim_approver,
                        'assignment' => $assignment,
                        'expense' => $expense,
                        'vendor_terms' => $vendor_terms,
                    ];
                    $pdf = Pdf::view('template.resource_assignment', $parameters);

                    break;
                case 2:
                    $assignment_id = $request->has('id') ? $request->input('id') : 0;

                    $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();
                    $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
                    $claim_approver_user = User::find(isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0);
                    $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
                    $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
                    $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

                    $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

                    $company_name = '';
                    if (isset($assignment->project->company_id) && $assignment->project->company_id > 0 && $assignment->project->company_id != '' && $assignment->project->company_id != null) {
                        $vendor = Company::find($assignment->project->company_id);
                        $company_name = $vendor->company_name;
                    }

                    $supplier_name = '';
                    $supplier_billing_period = '';
                    $supplier_payments = '';

                    if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0 && $resource_user->vendor_id != '' && $resource_user->vendor_id != null) {
                        $vendor = Vendor::find($resource_user->vendor_id);
                        $supplier_name = $vendor->vendor_name;
                        $supplier_billing_period = $vendor->billing_period;
                        $supplier_payments = $vendor->payment_terms_days;
                    }

                    $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
                    $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

                    $resource_name = $resource_first_name.' '.$resource_last_name;

                    $resource = User::find($request->id);
                    $parameters = [
                        'resource' => $resource,
                        'resource_name' => $resource_name,
                        'claim_approver' => $claim_approver,
                        'assignment' => $assignment,
                        'expense' => $expense,
                        'company_name' => $company_name,
                        'supplier_name' => $supplier_name,
                        'supplier_billing_period' => $supplier_billing_period,
                        'supplier_payments' => $supplier_payments,
                    ];
                    $pdf = Pdf::view('template.supplier_assignment', $parameters);
                    break;
                case 9:
                    $config = Config::get()->first();
                    if (isset($config->company_id)) {
                        $company = Company::find($config->company_id);
                    } else {
                        $company = Company::find($config->company_id);
                    }
                    $parameters = [
                        'signature' => ' __________________________________',
                        'signed_at' => '_______________________',
                        'date' => '______/____/____',
                        'company' => $company,
                    ];
                    $pdf = Pdf::view('template.asset_aknowledgement_form', $parameters);
                    break;
                case 4:
                    $assessment_header = AssessmentHeader::find($request->has('id') ? $request->input('id') : 0);
                    $resource = User::find(isset($assessment_header->resource_id) ? $assessment_header->resource_id : 0);
                    $assessed_by = User::find(isset($assessment_header->assessed_by) ? $assessment_header->assessed_by : 0);
                    $customer = Customer::find(isset($assessment_header->customer_id) ? $assessment_header->customer_id : 0);

                    $this_assessment_period_activities = AssessmentActivities::where('assessment_id', '=', isset($assessment_header->id) ? $assessment_header->id : 0)->get();
                    $next_assessment_period_activities = AssessmentPlannedNext::where('assessment_id', '=', isset($assessment_header->id) ? $assessment_header->id : 0)->get();
                    $assessment_concerns = AssessmentConcern::where('assessment_id', '=', isset($assessment_header->id) ? $assessment_header->id : 0)->get();
                    $assessment_notes = AssessmentNotes::where('assessment_id', '=', isset($assessment_header->id) ? $assessment_header->id : 0)->get();
                    $assessment_measures = AssessmentMeasure::where('assessment_detail_id', '=', -1)->get(); //Todo: Ask Stefan what needs to be done here, -1 will always return an empty result set
                    $parameters = [
                        'resource' => $resource,
                        'assessment_header' => $assessment_header,
                        'assessed_by' => $assessed_by,
                        'this_assessment_period_activities' => $this_assessment_period_activities,
                        'next_assessment_period_activities' => $next_assessment_period_activities,
                        'assessment_concerns' => $assessment_concerns,
                        'assessment_notes' => $assessment_notes,
                        'assessment_measures' => $assessment_measures,
                        'customer' => $customer,
                    ];
                    $pdf = Pdf::view('template.assessment', $parameters);
                    break;
                case 5:

                    $quotaion = Quotation::find($request->has('id') ? $request->input('id') : 0);
                    $customer = Customer::find(isset($quotaion->customer_id) ? $quotaion->customer_id : 0);
                    $prepared_by_use = User::find(Auth()->id());
                    $prepared_by_first_name = isset($prepared_by_use->first_name) ? $prepared_by_use->first_name : '';
                    $prepared_by_last_name = isset($prepared_by_use->last_name) ? $prepared_by_use->last_name : '';
                    $prepared_by = $prepared_by_first_name.' '.$prepared_by_last_name;

                    //dd($quotaion);

                    $parameters = [
                        'quotation' => $quotaion,
                        'customer' => $customer,
                        'prepared_by' => $prepared_by,
                    ];
                    $pdf = Pdf::view('template.customer_resource_proposal', $parameters);
                    break;
            }

            $pdf->save($file_name_pdf);

            if ($request->has('print') && $request->input('print') == 1) {
                return response()->file($file_name_pdf);
            }
        } catch (\Exception $e) {
            report($e);

            return redirect(route('template.index')->with('flash_danger', $e->getMessage()));
        }
    }
}
