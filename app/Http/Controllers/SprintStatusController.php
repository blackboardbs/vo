<?php

namespace App\Http\Controllers;

use App\Http\Requests\SprintStatusRequest;
use App\Models\SprintStatus;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SprintStatusController extends Controller
{
    public function index(Request $request): View
    {
        // $sprint_statuses = SprintStatus::with('status:id,description')->paginate();

        $item = $request->input('s') ?? 15;

        $sprint_statuses = SprintStatus::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $sprint_statuses = SprintStatus::with(['status:id,description'])->where('status_id', $request->input('status_filter'));
            }else{
                $sprint_statuses = SprintStatus::with(['status:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $sprint_statuses = $sprint_statuses->where('description', 'like', '%'.$request->input('q').'%');
        }

        $sprint_statuses = $sprint_statuses->paginate($item);

        return view('master_data.sprint-status.index')->with(['sprint_status' => $sprint_statuses]);
    }

    public function create(): View
    {
        return view('master_data.sprint-status.create');
    }

    public function store(SprintStatusRequest $request): RedirectResponse
    {
        $sprintstatus = new SprintStatus();
        $sprintstatus->description = $request->description;
        $sprintstatus->status_id = $request->status_id;
        $sprintstatus->save();

        // dd($sprintstatus);

        return redirect()->route('sprintstatus.index')->with('flash_success', 'Sprint status was saved successfully');
    }

    public function show(SprintStatus $sprintstatus): View
    {
        return view('master_data.sprint-status.show')->with(['sprintstatus' => $sprintstatus]);
    }

    public function edit(SprintStatus $sprintstatus): View
    {
        return view('master_data.sprint-status.edit')->with(['sprintstatus' => $sprintstatus]);
    }

    public function update(SprintStatusRequest $request, SprintStatus $sprintstatus): RedirectResponse
    {
        $sprintstatus->description = $request->description;
        $sprintstatus->status_id = $request->status_id;
        $sprintstatus->save();

        return redirect()->route('sprintstatus.index')->with('flash_success', 'Sprint status was updated successfully');
    }

    public function destroy($sprintstatus)
    {
        // $sprintstatus->delete();
        $item = SprintStatus::find($sprintstatus);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('sprintstatus.index')->with('flash_success', 'Sprint status was suspended successfully');
    }
}
