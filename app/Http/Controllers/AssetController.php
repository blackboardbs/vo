<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProofOfBankAccount(Request $request)
    {
        if ($request->has('q') && file_exists(storage_path('app/'.$request->input('q')))) {
            return response()->file(storage_path('app/'.$request->input('q')));
        }
    }
    public function getProofOfAddress(Request $request)
    {
        if ($request->has('q') && file_exists(storage_path('app/'.$request->input('q')))) {
            return response()->file(storage_path('app/'.$request->input('q')));
        }
    }
    public function getIdOrPassport(Request $request)
    {
        if ($request->has('q') && file_exists(storage_path('app/'.$request->input('q')))) {
            return response()->file(storage_path('app/'.$request->input('q')));
        }
    }

    public function getUserAvatar(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/user/'.$request->input('q')))) {
            logger("here");
            return response()->file(storage_path('app/avatars/user/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/users/default.png'));
        }
    }

    public function getTalentAvatar(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/cv/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/cv/'.$request->input('q')));
        } else {
            return response()->file(storage_path('app/avatars/cv/default.png'));
        }
    }

    public function getResourceAvatar(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/resource/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/resource/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/users/default.png'));
        }
    }

    public function getCompanyAvatar(Request $request): BinaryFileResponse| Response
    {
        $config = Config::first(['site_logo']);
        if ($request->site_logo && isset($config->site_logo) && file_exists(storage_path("app/public/assets/".$config->site_logo))){
            return response()->file(storage_path("app/public/assets/".$config->site_logo));
        }elseif ($request->has('q') && file_exists(storage_path('app/avatars/company/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/company/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/default.png'));
        }
    }

    public function getCustomerAvatar(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/customer/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/customer/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/companies/default.png'));
        }
    }

    public function getVendorAvatar(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/vendor/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/vendor/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/companies/default.png'));
        }
    }

    public function getCvCompanyLogo(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/cv_company/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/cv_company/'.$request->input('q')));
        } else {
            return response()->file(public_path('assets/companies/default.png'));
        }
    }

    public function assetRegister(Request $request): BinaryFileResponse
    {
        if ($request->has('q') && file_exists(storage_path('app/avatars/asset/'.$request->input('q')))) {
            return response()->file(storage_path('app/avatars/asset/'.$request->input('q')));
        } else {
            return response()->file(storage_path('app/avatars/asset/default.png'));
        }
    }

    public function getExpTracking(Request $request): BinaryFileResponse
    {
        if (file_exists(storage_path('app/exp_tracking/'.$request->input('q')))) {
            return response()->file(storage_path('app/exp_tracking/'.$request->input('q')));
        } else {
            abort(404);
        }
    }

    public function imgUpload(Request $request)
    {
        $imgpath = request()->file('name')->store('app/uploads', 'public');

        return json_encode(['location' => $imgpath]);
    }

    public function getTemplate(Request $request): BinaryFileResponse
    {
        if (file_exists(storage_path('app/templates/'.$request->input('q')))) {
            return response()->file(storage_path('app/templates/'.$request->input('q')));
        } else {
            abort(404);
        }
    }

    public function getTimeSheetAttachement($filename): BinaryFileResponse
    {
        if (file_exists(storage_path('app/attachments/'.$filename))) {
            return response()->file(storage_path('app/attachments/'.$filename));
        } else {
            abort(404);
        }
    }

    public function getFile($id): BinaryFileResponse
    {
        $document = Document::find($id);

        $url = '';
        switch ($document->document_type_id) {
            case 7:
                $url = 'app/scouting/'.$document->file;
                break;
        }

        if (file_exists(storage_path($url))) {
            return response()->file(storage_path($url));
        } else {
            abort(404);
        }
    }

    public function jobSpecDocuments($id): BinaryFileResponse
    {
        $document = Document::find($id);

        $url = '';
        switch ($document->document_type_id) {
            case 8:
                $url = 'app/job_spec/'.$document->file;
                break;
        }

        if (file_exists(storage_path($url))) {
            return response()->file(storage_path($url));
        } else {
            abort(404);
        }
    }

    public function getCalendarEvents($id)
    {
        $document = Document::find($id);

        /*$url = $document->filter(function ($file){
            return $file->document_type_id == 9;
        })->map(function ($file){
            return 'app/events/'.$file->name;
        });*/
        $url = '';
        switch ($document->document_type_id) {
            case 9:
                $url = 'app/events/'.$document->file;
                break;
        }

        if (file_exists(storage_path($url))) {
            return response()->file(storage_path($url));
        } else {
            abort(404);
        }
    }

    public function getClientAgreement(string $file)
    {
        if (file_exists(storage_path('app/client/templates/'.$file))) {
            return response()->file(storage_path('app/client/templates/'.$file));
        }elseif(file_exists(storage_path('app/vendor/templates/'.$file))) {
            return response()->file(storage_path('app/vendor/templates/'.$file));
        } else {
            abort(404);
        }
    }

    public function getProviderAgreement(string $file)
    {
        if (file_exists(storage_path('app/ServiceProvider/templates/'.$file))) {
            return response()->file(storage_path('app/ServiceProvider/templates/'.$file));
        }elseif(file_exists(public_path('files/customers/'.$file))) {

            return response()->file(public_path('files/customers/'.$file));
        } else {
            abort(404);
        }
    }

    public function getAssignmentDocument(string $file)
    {
        if (file_exists(storage_path('app/Assignment/templates/'.$file))) {
            return response()->file(storage_path('app/Assignment/templates/'.$file));
        }else {
            abort(404);
        }
    }
}
