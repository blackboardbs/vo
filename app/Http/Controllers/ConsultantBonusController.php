<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Company;
use App\Dashboard\Commission;
use App\Models\Module;
use App\Models\Team;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConsultantBonusController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $commission = new Commission();
        $commission_all = $commission->commission($request, $item);

        $yearmonth_dropdown = [];

        $year_months = Timesheet::select('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->groupBy('year', 'month')->get();
        foreach ($year_months as $month) {
            $yearmonth_dropdown[$month->year.(($month->month < 10) ? '0'.$month->month : $month->month)] = $month->year.(($month->month < 10) ? '0'.$month->month : $month->month);
        }

        $teams = Team::filters()->pluck('team_name','id');

        asort($commission_all['employee_dropdown']);
        
        $parameters = [
            'commission' => $commission_all,
            'employee_dropdown' => $commission_all['employee_dropdown'],
            'yearmonth_dropdown' => $yearmonth_dropdown,
            'teams_dropdown' => $teams,
        ];

        return view('reports.consultant_bonus')->with($parameters);
    }
}
