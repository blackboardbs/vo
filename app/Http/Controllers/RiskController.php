<?php

namespace App\Http\Controllers;

use App\Models\Deliverable;
use App\Models\Financial;
use App\Http\Requests\RiskRequest;
use App\Models\Impact;
use App\Models\Likely;
use App\Models\Project;
use App\Models\Risk;
use App\Models\RiskArea;
use App\Models\Security;
use App\Models\TaskStatus;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class RiskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $risks = Risk::with(['project:id,name', 'riskarea:id,name', 'status:id,description'])->orderBy('id')->sortable(['name', 'description'])->paginate($item);

        $parameters = [
            'risks' => $risks,
        ];

        return view('project.risk.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id');
        $statusDropDown = TaskStatus::orderBy('description')->where('status', '=', 1)->pluck('description', 'id');
        $riskAreaDropDown = RiskArea::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $usersDropDown = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $likelyDropDown = Likely::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $impactDropDown = Impact::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $securityDropDown = Security::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');

        $parameters = [
            'projectDropDown' => $projectDropDown,
            'riskAreaDropDown' => $riskAreaDropDown,
            'riskItemDropDown' => $this->getRiskItem(0),
            'usersDropDown' => $usersDropDown,
            'likelyDropDown' => $likelyDropDown,
            'impactDropDown' => $impactDropDown,
            'securityDropDown' => $securityDropDown,
            'statusDropDown' => $statusDropDown,
        ];

        return view('project.risk.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RiskRequest $request): RedirectResponse
    {
        $risk = new Risk();
        $risk->name = $request->name;
        $risk->description = $request->description;
        $risk->project_id = $request->project_id;
        $risk->risk_date = $request->risk_date;
        $risk->risk_area_id = $request->risk_area;
        if ($request->risk_area != 4) {
            $risk->risk_item = json_encode($request->risk_item);
        } else {
            $risk->risk_item = $request->risk_item1;
        }
        $risk->resposibility_user_id = $request->resposibility_user_id;
        $risk->accountablity_user_id = $request->accountablity_user_id;
        $risk->likelihood_id = $request->likelihood_id;
        $risk->impact_id = $request->impact_id;
        $risk->security_id = $request->security_id;
        $risk->creator_id = auth()->id();
        $risk->status_id = $request->status_id;
        $risk->risk_mitigation_strategy = $request->risk_mitigation_strategy;
        $risk->save();

        return redirect(route('risk.index'))->with('flash_success', 'Risk successfully added.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Risk $risk): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id');
        $statusDropDown = TaskStatus::orderBy('description')->where('status', '=', 1)->pluck('description', 'id');
        $riskAreaDropDown = RiskArea::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $usersDropDown = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $likelyDropDown = Likely::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $impactDropDown = Impact::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $securityDropDown = Security::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');

        $risk_items = json_decode($risk->risk_item);
        $parameters = [
            'risk' => $risk,
            'risk_items' => $risk_items,
            'projectDropDown' => $projectDropDown,
            'riskAreaDropDown' => $riskAreaDropDown,
            'riskItemDropDown' => $this->getRiskItem($risk->risk_area_id),
            'usersDropDown' => $usersDropDown,
            'likelyDropDown' => $likelyDropDown,
            'impactDropDown' => $impactDropDown,
            'securityDropDown' => $securityDropDown,
            'statusDropDown' => $statusDropDown,
            'riskClassification' => $this->riskClassification($risk->likelihood_id, $risk->impact_id),
        ];

        return view('project.risk.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Risk $risk): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id');
        $statusDropDown = TaskStatus::orderBy('description')->where('status', '=', 1)->pluck('description', 'id');
        $riskAreaDropDown = RiskArea::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $usersDropDown = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $likelyDropDown = Likely::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $impactDropDown = Impact::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $securityDropDown = Security::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');

        $risk_items = json_decode($risk->risk_item);

        $parameters = [
            'risk' => $risk->load('feedback'),
            'risk_items' => $risk_items,
            'projectDropDown' => $projectDropDown,
            'riskAreaDropDown' => $riskAreaDropDown,
            'riskItemDropDown' => $this->getRiskItem($risk->risk_area_id),
            'usersDropDown' => $usersDropDown,
            'likelyDropDown' => $likelyDropDown,
            'impactDropDown' => $impactDropDown,
            'securityDropDown' => $securityDropDown,
            'statusDropDown' => $statusDropDown,
        ];

        return view('project.risk.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Risk  $risk
     */
    public function update(Request $request, $riskID): RedirectResponse
    {
        $risk = Risk::find($riskID);
        $risk->name = $request->name;
        $risk->description = $request->description;
        $risk->project_id = $request->project_id;
        $risk->risk_date = $request->risk_date;
        $risk->risk_area_id = $request->risk_area;
        if ($request->risk_area != 4) {
            $risk->risk_item = json_encode($request->risk_item);
        } else {
            $risk->risk_item = $request->risk_item1;
        }
        $risk->resposibility_user_id = $request->resposibility_user_id;
        $risk->accountablity_user_id = $request->accountablity_user_id;
        $risk->likelihood_id = $request->likelihood_id;
        $risk->impact_id = $request->impact_id;
        $risk->security_id = $request->security_id;
        $risk->creator_id = auth()->id();
        $risk->status_id = $request->status_id;
        $risk->risk_mitigation_strategy = $request->risk_mitigation_strategy;
        $risk->save();

        return redirect(route('risk.index'))->with('flash_success', 'Risk successfully updated.');
    }

    /**
     * Get Risk Item items for the risk area.
     *
     * @param    $riskArea_ID
     * @return \Illuminate\Http\Response
     */
    public function getRiskItem($riskAreaID)
    {
        $riskItem = [];

        switch ($riskAreaID) {
            case 1:
                $riskItem = Deliverable::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
                break;
            case 2:
                $riskItem = Financial::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
                break;
            case 3:
                $riskItem = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
                break;
            case 4:
                $riskItem = '';
                break;
        }

        return $riskItem;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Risk $risk): RedirectResponse
    {
        Risk::destroy($risk->id);

        return redirect(route('risk.index'))->with('flash_success', 'Risk successfully deleted.');
    }

    private function riskClassification($likelihood, $impact)
    {
        $low = '#b7df42';
        $medium = 'yellow';
        $high = 'orange';
        $extreme = '#ef6963';
        $padding = 'btn w-100 mb-2';
        if ($likelihood == 6 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$extreme.';color:#0e0e0e;">Extreme Risk Classification</span>';
        }
        if ($likelihood == 6 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$extreme.';color:#0e0e0e;">Extreme Risk Classification</span>';
        }
        if ($likelihood == 6 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$extreme.';color:#0e0e0e;">Extreme Risk Classification</span>';
        }
        if ($likelihood == 6 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 6 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 6 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$extreme.';color:#0e0e0e;">Extreme Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 5 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$extreme.';color:#0e0e0e;">Extreme Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 4 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$high.';color:#0e0e0e;">High Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 3 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$medium.';color:#0e0e0e;">Medium Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 2 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 1 && $impact == 6) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 1 && $impact == 5) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 1 && $impact == 4) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification/span>';
        }
        if ($likelihood == 1 && $impact == 3) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 1 && $impact == 2) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }
        if ($likelihood == 1 && $impact == 1) {
            return '<span class="'.$padding.'" style="pointer-events:none;background-color:'.$low.';color:#0e0e0e;">Low Risk Classification</span>';
        }

        return '<span class="'.$padding.'" style="pointer-events:none;">No Risk Classification</span>';
    }
}
