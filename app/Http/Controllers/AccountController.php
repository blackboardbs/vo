<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountType;
use App\Http\Requests\StoreAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AccountController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $account = Account::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $account = Account::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $account = Account::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $account = $account->where('description', 'like', '%'.$request->input('q').'%');
        }

        $account = $account->paginate($item);

        $parameters = [
            'account' => $account,
        ];

        return View('master_data.account.index', $parameters);
    }

    public function create(): View
    {
        $accounts = Account::orderBy('description')->where('description', '!=', '')->where('description', '!=', null)->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => AccountType::orderBy('order')->pluck('description', 'order')->prepend('Account Type', '0'),
            'accounts' => $accounts,
        ];

        return View('master_data.account.create')->with($parameters);
    }

    public function store(StoreAccountRequest $request): RedirectResponse
    {
        $account = new Account();
        $account->description = $request->input('description');
        $account->status = $request->input('status');
        $account->account_type_id = $request->input('account_type');
        $account->account_group = $request->input('account_group');
        $account->creator_id = auth()->id();
        $account->save();

        return redirect(route('account.index'))->with('flash_success', 'Master Data Account captured successfully');
    }

    public function show($account_id): View
    {
        $parameters = [
            'account' => Account::where('id', '=', $account_id)->get(),
        ];
        //return $parameters;
        return View('master_data.account.show')->with($parameters);
    }

    public function edit($account_id): View
    {
        $accounts = Account::orderBy('description')->where('description', '!=', '')->where('description', '!=', null)->get();

        $parameters = [
            'account' => Account::where('id', '=', $account_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => AccountType::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('AccountType', '0'),
            'accounts' => $accounts,
        ];

        return View('master_data.account.edit')->with($parameters);
    }

    public function update(UpdateAccountRequest $request, $account_id): RedirectResponse
    {
        $account = Account::find($account_id);
        $account->description = $request->input('description');
        $account->status = $request->input('status');
        $account->account_type_id = $request->input('account_type');
        $account->account_group = $request->input('account_group');
        $account->creator_id = auth()->id();
        $account->save();

        return redirect(route('account.index'))->with('flash_success', 'Master Data Account saved successfully');
    }

    public function destroy($account_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // Account::destroy($account_id);

        $account = Account::find($account_id);
        $account->status = 2;
        $account->save();

        return redirect()->route('account.index')->with('success', 'Master Data Account suspended successfully');
    }
}
