<?php

namespace App\Http\Controllers;

use App\Models\BillingPeriod;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\InvoiceItems;
use App\Jobs\SendVendorInvoiceEmailJob;
use App\Models\Module;
use App\Models\Project;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\VatRate;
use App\Models\Vendor;
use App\Models\VendorInvoice;
use App\Models\VendorInvoiceLine;
use App\Models\VendorInvoiceLineExpense;
use App\Services\VendorInvoiceService;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;
use Symfony\Component\HttpFoundation\Response;

class VendorInvoiceFunctionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request): View
    {
        if (! $request->has('page')) {
            $request->session()->forget('invoice_timesheets');
        }

        $timesheets_raw = $this->timesheet($request);

        $projects = [];
        foreach($timesheets_raw as $t){
            array_push($projects,$t->project_id);
        }
        $year_weeks = [];
        $project = Project::filters()->orderBy('name')->pluck('name', 'id');
        $employees = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->filters()->whereHas('vendor')->whereHas('timesheets',function ($q){
            $q->whereNull('vendor_invoice_number');
        })->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $date = Carbon::now();
        for ($i = 0; $i < 53; $i++) {
            $year_weeks[$date->now()->subWeeks($i)->year.(($date->now()->subWeeks($i)->weekOfYear < 10) ? '0'.$date->now()->subWeeks($i)->weekOfYear : $date->now()->subWeeks($i)->weekOfYear)] = $date->now()->subWeeks($i)->year.(($date->now()->subWeeks($i)->weekOfYear < 10) ? '0'.$date->now()->subWeeks($i)->weekOfYear : $date->now()->subWeeks($i)->weekOfYear);
        }

        $parameters = [
            'timesheets' => $timesheets_raw,
            'total_billable_hours' => 0,
            'total_billable_minutes' => 0,
            'total_non_billable_hours' => 0,
            'total_non_billable_minutes' => 0,
            'year_weeks' => $year_weeks,
            'project' => $project,
            'employees' => $employees,
            'vendor_drop' => Vendor::orderBy('vendor_name')->whereHas('users.timesheets',function ($q){
                $q->whereNull('vendor_invoice_status');
            })->pluck('vendor_name', 'id'),
            'selected_timesheets' => $request->session()->get('invoice_timesheets'),
        ];

        return view('vendor.vendor_invoice_function.create')->with($parameters);
    }

    public function prepare(Request $request): View|RedirectResponse
    {
        $timesheets = $request->session()->get('invoice_timesheets') ?? [];

        if (!count($timesheets)){
            return redirect()->back()->with('flash_danger', 'Something went wrong');
        }

        $time_exp = TimeExp::with(['account' => function ($account) {
            $account->select('id', 'description');
        }, 'account_element' => function ($acc_element) {
            $acc_element->select('id', 'description');
        }])->select(['id', 'description', 'account_id', 'account_element_id', 'date', 'is_billable', 'claim', 'amount', 'is_approved'])
            ->whereIn('timesheet_id', $timesheets)->get();

        $payment_due_date = Timesheet::find($timesheets[0]??0);
        $vendor = $payment_due_date?->employee?->vendor;
        $due_date = $vendor->payment_terms_days ?? 30;

        $parameters = [
            'timesheet' => $timesheets,
            'invoice_date' => now()->toDateString(),
            'payment_due_date' => now()->addDays($due_date)->toDateString(),
            'expenses' => $time_exp,
            'vat_rate_id' => (new VendorInvoiceService())->vatRate($vendor),
            'vat_rate_dropdown' => VatRate::selectRaw('id, CONCAT(description, " - ", vat_rate,"%") AS description')->where('end_date', '>=', now()->toDateString())->pluck('description', 'id'),
        ];

        return view('vendor.vendor_invoice_function.prepare')->with($parameters);
    }

    public function generate(Request $request)
    {
        // dd($request->session()->get('invoice_timesheets'));
        $invoice = $this->calculate_invoice($request);

        if (isset($invoice['is_same_vendor']) && ! $invoice['is_same_vendor']) {
            $request->session()->forget('invoice_timesheets');

            return redirect()->route('function_newinvoice.create')->with('flash_danger', 'Please select a user that is linked to a vendor');
        }

        if (isset($invoice['is_currency_the_same']) && ! $invoice['is_currency_the_same']) {
            $request->session()->forget('invoice_timesheets');

            return redirect()->route('function_newinvoice.create')->with('flash_danger', 'Please Select assignments that uses the same currency.');
        }

        if (session()->has('invoice_timesheets')) {
            return view('vendor.vendor_invoice_function.invoice')->with($invoice);
        } else {
            session()->forget('invoice_timesheets');

            return redirect()->route('function_newinvoice.create')->with('flash_info', 'Please select timesheets to invoice');
        }
    }

    public function save(Request $request): RedirectResponse
    {
        if (session()->has('invoice_timesheets')) {
            $invoice_calculations = $this->calculate_invoice($request, 1);

            $vendor_invoice = new VendorInvoice();
            $vendor_invoice->vendor_id = $invoice_calculations['vendor']->id;
            $vendor_invoice->vendor_invoice_ref = session('invoice_reference');
            $vendor_invoice->vendor_invoice_date = session('invoice_date');
            $vendor_invoice->vendor_due_date = session('invoice_due_date');
            $vendor_invoice->vendor_invoice_value = (string) ($invoice_calculations['invoice_total_amount'] + $invoice_calculations['invoice_expense_line_items']->sum('amount'));
            $vendor_invoice->vendor_invoice_status = 1;
            $vendor_invoice->company_id = $invoice_calculations['company']->id;
            $vendor_invoice->vendor_invoice_note = session('invoice_note');
            $vendor_invoice->vendor_reference = session('vendor_reference');
            $vendor_invoice->currency = $invoice_calculations['invoice_line_items'][0]['currency'] ?? 'ZAR';
            $vendor_invoice->vat_rate_id = session('vat_rate_id');
            if (! isset($this->sysConfig()->vendor_invoice_start_number)) {
                $vendor_invoice->save();
            }

            $this->saveInvoiceNumber($vendor_invoice);



            activity()->on($vendor_invoice)->log('created');

            $project_id = 0;

            foreach ($invoice_calculations['invoice_line_items'] as $line_item) {
                $timesheet = Timesheet::find($line_item['timesheet_id']);
// dd($line_item);
                $timesheet->timesheet_lock = 1;
                $timesheet->vendor_invoice_number = $vendor_invoice->id;
                $timesheet->vendor_invoice_status = $vendor_invoice->vendor_invoice_status;
                $timesheet->vendor_invoice_date = $vendor_invoice->vendor_invoice_date;
                $timesheet->vendor_due_date = $vendor_invoice->vendor_due_date;
                $timesheet->vendor_invoice_ref = $vendor_invoice->vendor_invoice_ref;
                $timesheet->vendor_reference = $vendor_invoice->vendor_reference;
                $timesheet->save();

                $project_id = $timesheet->project_id;

                $vendor_invoice_line = new VendorInvoiceLine();
                $vendor_invoice_line->vendor_invoice_number = $vendor_invoice->id;
                $vendor_invoice_line->time_id = $line_item['timesheet_id'];
                $vendor_invoice_line->emp_id = $timesheet->employee_id;
                $vendor_invoice_line->line_text = $line_item['description'];
                $vendor_invoice_line->quantity = (string) $line_item['hours'];
                $vendor_invoice_line->price = $line_item['consultant_rate'] != '' ? (string) $line_item['consultant_rate'] : 0;
                $vendor_invoice_line->invoice_line_value = (string) $line_item['line_item_total_vat'];
                $vendor_invoice_line->save();
            }

            $vendor_invoice->project_id = $project_id;
            $vendor_invoice->save();

            foreach ($invoice_calculations['invoice_expense_line_items'] as $expense) {
                $vendor_inv_line_exp = new VendorInvoiceLineExpense();
                $vendor_inv_line_exp->vendor_invoice_number = $vendor_invoice->id;
                $vendor_inv_line_exp->time_id = $expense['timesheet_id'];
                $vendor_inv_line_exp->time_exp_id = $expense['id'];
                $vendor_inv_line_exp->line_text = $expense['description'];
                $vendor_inv_line_exp->amount = (string) $expense['amount'];
                $vendor_inv_line_exp->creator_id = auth()->id();
                $vendor_inv_line_exp->save();
            }

            session()->forget('invoice_timesheets');
            session()->forget('invoice_reference');
            session()->forget('invoice_date');
            session()->forget('invoice_due_date');
            session()->forget('invoice_note');
            session()->forget('vendor_reference');
            session()->forget('vat_rate_id');

            if ($request->send) {
                $pdf_params = [
                    'send' => $request->send,
                    'subject' => $request->subject,
                    'company_email' => $request->companyEmail,
                    'vendor_email' => $request->vendorEmail,
                    'multi_emails' => $request->multiEmails,
                ];
                return redirect()->route('invoice.document', ['vendor_invoice' => $vendor_invoice, 'data' => $pdf_params]);
            }

            return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice saved successfully');
        } else {
            session()->forget('invoice_timesheets');

            return redirect()->route('function_newinvoice.create')->with('flash_info', 'Please select timesheets to invoice');
        }
    }

    public function generatePDF(Request $request, VendorInvoice $vendor_invoice)
    {
        $line_items = $vendor_invoice->load('vendorInvoiceLine');

        $invoice_line_items = $line_items->vendorInvoiceLine->map(function ($item) use ($vendor_invoice) {
            $vat = '0%';
            if ($item->quantity > 0 && $item->price > 0) {
                $vat = number_format(((($item->invoice_line_value - ($item->quantity * $item->price)) / ($item->quantity * $item->price)) * 100)).'%';
            }

            return [
                'line_text' => $item->line_text,
                'hours' => number_format($item->quantity, 2, '.', ','),
                'consultant_rate' => ($vendor_invoice->currency ?? 'ZAR').' '.number_format($item->price, 2, '.', ','),
                'vat' => $vendor_invoice->vatRate ? $vendor_invoice->vatRate?->vat_rate : $vat,
                'exclude_vat' => ($vendor_invoice->currency ?? 'ZAR').' '.number_format(($item->quantity * $item->price), 2, '.', ','),
                'vat_included' => ($vendor_invoice->currency ?? 'ZAR').' '.number_format($item->invoice_line_value, 2, '.', ','),
            ];
        });

        $total_without_tax = $line_items->vendorInvoiceLine->sum(function ($line_item) {
            return $line_item->quantity * $line_item->price;
        });

        $total_hours = $line_items->vendorInvoiceLine->sum('quantity');

        $total_exclude_vat = $line_items->vendorInvoiceLine->sum(function ($item) {
            return $item->quantity * $item->price;
        });

        $line_expenses = $vendor_invoice->load('vendorInvoiceLineExpense');

        $expenses = $line_expenses->vendorInvoiceLineExpense->map(function ($expense) use ($vendor_invoice) {
            return [
                'line_text' => $expense->line_text,
                'amount' => number_format($expense->amount,2),
            ];
        });

        $total_expenses = $line_expenses->vendorInvoiceLineExpense->sum('amount');

        $parameter = [
            'total_without_tax' => $total_without_tax,
            'line_items' => $invoice_line_items,
            'invoice' => $vendor_invoice,
            'total_hours' => $total_hours,
            'total_vat_excluded' => $total_exclude_vat,
            'expenses' => $expenses,
            'total_expenses' => $total_expenses,
            'vendor' => $vendor_invoice->load(['vendor.account_managerd', 'resource'])->vendor,
            'company' => $vendor_invoice->load('company')->company,
            'currency' => $this->sysConfig()->currency ?? 'ZAR',
        ];

// dd($parameter);
        if (!isset($parameter['vendor']->vendor_logo)){
            $parameter['logo'] = public_path('assets/default.png');
        }else{
            $parameter['logo'] = storage_path('/app/avatars/vendor/'.($parameter['vendor']->vendor_logo));
        }
        $vendor_name = str_replace(' ', '_', $vendor_invoice->vendor->vendor_name);

        if (! Storage::exists('invoices/'.$vendor_name)) {
            Storage::makeDirectory('invoices/'.$vendor_name);
        }

        try {

            $storage = storage_path('app/invoices/'.$vendor_name.'/'.$vendor_name.'_'.substr(isset($vendor_line->resource->first_name) ? $vendor_line?->resource->first_name : '', 0, 1).substr(isset($vendor_line?->resource->last_name) ? $vendor_line?->resource->last_name : '', 0, 1).'_'.date('Y-m-d_H_i_s').'.pdf');

            $pdf = Pdf::view('vendor.vendor_invoice_function.send', $parameter);
            if ($request->has('print')){
                return $pdf->name(str_replace(" ", "_", $vendor_invoice->vendor?->vendor_name).'_'.now()->format('Y_m_d_H_i_s').'.pdf');
            }else{
                $pdf->save($storage);
            }
            activity()->on($vendor_invoice)->log('emailed');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('customer.invoice_create'))->with('flash_danger', $e->getMessage());
        }

        if ($request->show) {
            $emails = $request->multiEmails ? json_decode($request->multiEmails) : [];
            array_push($emails, $request->has('companyEmail') ? $request->companyEmail : null);
            array_push($emails, $request->has('vendorEmail') ? $request->vendorEmail : null);
            $emails = array_filter(array_unique($emails));

            $email_subject = $request->has('subject') ? $request->subject : ($vendor_invoice->vendor->vendor_name ?? null).' Account Payable Invoice';
            $email_body = $this->sysConfig()->invoice_body_msg ?? 'Please find Accounts Payable invoice attached for your attention';
        } else {
            $emails = isset($request->data['multi_emails']) ? json_decode($request->data['multi_emails']) : [];
            array_push($emails, isset($request->data['company_email']) ? $request->data['company_email'] : null);
            array_push($emails, isset($request->data['vendor_email']) ? $request->data['vendor_email'] : null);
            $emails = array_filter(array_unique($emails));

            $email_subject = isset($request->data['subject']) ? $request->data['subject'] : ($vendor_invoice->vendor->vendor_name ?? null).' Account Payable Invoice';
            $email_body = $this->sysConfig()->invoice_body_msg ?? 'Please find Accounts Payable invoice attached for your attention';
        }

        //Mail::to(Auth::user()->email)->cc(array_filter($emails))->send(new \App\Mail\VendorInvoiceMail($vendor_invoice->vendor, $vendor_invoice->company, $email_subject, $email_body, $storage));
        SendVendorInvoiceEmailJob::dispatch(Auth::user()->email, array_filter($emails), $vendor_invoice->vendor, $vendor_invoice->company, $email_subject, $email_body, $storage)->delay(now()->addMinutes(5));
    }

    public function show(Request $request, VendorInvoice $vendorInvoice)
    {
        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $invoice_lines = InvoiceItems::where('invoice_id', '=', $vendorInvoice->id)->where('invoice_type_id', '=', 2)->get()->toArray();

        if (! empty($invoice_lines)) {
            return redirect(route('nontimesheetinvoice.generate_vendor', $vendorInvoice->id));
        }

        $vendorInvoice = $vendorInvoice->load(['vendorInvoiceLine', 'vendorInvoiceLineExpense', 'vendor.account_managerd', 'company', 'vatRate:id,vat_rate']);

        $total_without_tax = $vendorInvoice->vendorInvoiceLine->sum(function ($line_item) {
            return $line_item->quantity * $line_item->price;
        });

        $parameters = [
            'invoice' => $vendorInvoice,
            'total_without_tax' => $total_without_tax,
            'path' => $path,
            //'currency' => $this->sysConfig()->currency??"ZAR"
        ];

        return view('vendor.vendor_invoice_function.view')->with($parameters);
    }

    public function unallocateNonTimesheetInvoice($vendor_invoice_id): RedirectResponse
    {
        $vendor_invoice = VendorInvoice::find($vendor_invoice_id);
        $date = Carbon::now()->toDateString();
        $vendor_invoice->vendor_invoice_status = 3;
        $vendor_invoice->vendor_unallocate_date = $date;
        $vendor_invoice->save();

        activity()->on($vendor_invoice)->withProperties(['invoice_type' => 'Vendor'])->log('cancelled');

        return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice un-allocated successfully');
    }

    public function unallocate($vendor_invoice_id): RedirectResponse
    {
        $vendor_invoice = VendorInvoice::find($vendor_invoice_id);
        $times = Timesheet::where('vendor_invoice_number', '=', $vendor_invoice->id)
            ->get();
        $date = Carbon::now()->toDateString();
        $vendor_invoice->vendor_invoice_status = 3;
        //$vendor_invoice->vendor_invoice_date = null;
        $vendor_invoice->vendor_unallocate_date = $date;
        $vendor_invoice->save();

        foreach ($times as $time) {
            $time->vendor_unallocate_date = $date;
            $time->vendor_invoice_status = null;
            $time->vendor_invoice_number = null;
            $time->vendor_invoice_date = null;
            $time->vendor_due_date = null;
            $time->vendor_invoice_ref = null;
            $time->vendor_reference = null;
            $time->timesheet_lock = null;
            $time->save();
        }

        activity()->on($vendor_invoice)->withProperties(['invoice_type' => 'Vendor'])->log('cancelled');

        return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice un-allocated successfully');
    }

    public function edit($vendor_invoice_id)
    {
        $customerInvoiceItems = InvoiceItems::where('status_id', '=', 1)->where('invoice_id', '=', $vendor_invoice_id)->where('invoice_type_id', '=', 2)->get()->toArray();

        // Customer invoice in a non timesheet, redirect to the correct edit page
        if (isset($customerInvoiceItems) && ! empty($customerInvoiceItems)) {
            $params['invoice_id'] = $vendor_invoice_id;
            if (\request()->recurring)
            {
                $params['recurring'] = request()->recurring;
            }
            return redirect(route('nontimesheetinvoice.edit_vendor', $params));
        }

        $vendor_invoice = VendorInvoice::find($vendor_invoice_id);
        $parameters = [
            'vendor_invoice' => $vendor_invoice,
        ];

        return view('vendor.vendor_invoice.edit')->with($parameters);
    }

    public function update(Request $request, $vendor_invoice_id)
    {
        $vendor_invoice = VendorInvoice::find($vendor_invoice_id);
        $times = Timesheet::where('vendor_invoice_number', '=', $vendor_invoice->id)->get();

        foreach ($times as $time) {
            $time->vendor_due_date = $request->input('due_date');
            $time->vendor_invoice_ref = $request->input('invoice_ref');
            $time->vendor_due_date = $request->input('due_date');
            $time->vendor_invoice_date = $request->input('invoice_date');
            $time->vendor_reference = $request->input('vendor_ref');
            $time->save();
        }
        if($request->has('vendor_invoice_status')){
            $vendor_invoice->vendor_invoice_status = $request->input('vendor_invoice_status');
        }
        $vendor_invoice->vendor_invoice_note = $request->input('invoice_notes');
        $vendor_invoice->vendor_invoice_ref = $request->input('invoice_ref');
        $vendor_invoice->vendor_due_date = $request->input('due_date');
        $vendor_invoice->vendor_invoice_date = $request->input('invoice_date');
        $vendor_invoice->vendor_reference = $request->input('vendor_ref');
        $vendor_invoice->company_id = $times[0]->company_id??null;
        $vendor_invoice->save();

        activity()->on($vendor_invoice)->withProperties(['invoice_type' => 'Vendor'])->log('updated');

        if ($request->ajax()) {
            $data = [];
           return response()->json('lll');
       }
       
        return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice updated successfully');
    }

    private function timesheet($request)
    {
        $module = Module::where('name', '=', "App\Models\VendorInvoice")->get();

        abort_unless((Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')), Response::HTTP_FORBIDDEN);

        $item = $request->input('s') ?? 15;

        $timesheet = Timesheet::with(['employee.vendor', 'project'])->select(DB::raw('assignment.external_cost_rate,assignment.id as ass_id, timesheet.id, timesheet.employee_id, 
                                                timesheet.year_week, timesheet.project_id,timesheet.status_id, timesheet.first_day_of_month,
                                                timesheet.last_day_of_month,
                                                SUM(CASE WHEN timeline.vend_is_billable = 1 THEN total ELSE 0 END) AS billable_hours,
                                                SUM(CASE WHEN timeline.vend_is_billable = 1 THEN total_m ELSE 0 END) AS billable_minutes,
                                                SUM(CASE WHEN timeline.vend_is_billable = 0 THEN total ELSE 0 END) AS non_billable_hours,
                                                SUM(CASE WHEN timeline.vend_is_billable = 0 THEN total_m ELSE 0 END) AS non_billable_minutes
                                                '))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('users', 'users.id', '=', 'timesheet.employee_id')
            ->whereHas('employee.vendor')
            ->whereNull('timesheet.vendor_invoice_status')
            ->join('assignment',function($join){
                $join->on('assignment.employee_id','=','timesheet.employee_id');
                $join->on('assignment.project_id','=','timesheet.project_id');
            })
            ->groupBy([
                'timesheet.id', 'timesheet.employee_id', 'timesheet.year_week', 'timesheet.project_id',
                'timesheet.status_id', 'timesheet.first_day_of_month', 'timesheet.last_day_of_month', 'assignment.external_cost_rate', 'assignment.id'
            ])
            ->orderBy('timesheet.id', 'desc');

        if ($request->has('employees') && $request->input('employees') != '') {
            $timesheet = $timesheet->where('timesheet.employee_id', '=', $request->employees);
        }

        if ($request->has('week') && $request->input('week') != '') {
            $timesheet = $timesheet->where('timesheet.year_week', '=', $request->input('week'));
        }

        if ($request->has('project') && $request->input('project') != '') {
            $timesheet = $timesheet->where('timesheet.project_id', '=', $request->input('project'));
        }

        if ($request->has('billing-period') && $request->input('billing-period') != '') {
            $billing_period = BillingPeriod::find($request->input('billing-period'));
            $timesheet = $timesheet->where('timesheet.first_day_of_month', '>=', $billing_period->start_date)
                ->where('timesheet.last_day_of_month', '<=', $billing_period->end_date);
        }

        if ($request->has('vendor') && $request->input('vendor') != '') {
            $timesheet = $timesheet->whereHas('user', function ($query) use ($request) {
                $query->where('vendor_id', $request->vendor);
            });
        }

        if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
            $vendor = Vendor::find(Auth::user()->vendor_id);

            abort_if(empty($vendor), Response::HTTP_FORBIDDEN);

            if (Auth::user()->hasRole('vendor')) {
                $users_ids = $vendor->users?->pluck('id')->toArray();
                $timesheet->whereIn('timesheet.employee_id', $users_ids);
            } else {
                $timesheet->where('timesheet.employee_id', Auth::user()->id);
            }
        }

        $timesheet = $timesheet->paginate($item);

        return $timesheet;
    }

    private function calculate_invoice($request, $save = 0)
    {
        $timesheets_id = $request->session()->get('invoice_timesheets') ?? [];

        $company_fields = [
            'id', 'company_name', 'phone', 'cell', 'email', 'postal_address_line1', 'postal_address_line2',
            'postal_address_line3', 'state_province', 'postal_zipcode', 'vat_no', 'country_id',
            'contact_firstname', 'contact_lastname',
        ];

        $company = isset($this->sysConfig()->company_id) ?
            Company::find($this->sysConfig()->company_id, $company_fields) :
            Company::first($company_fields);

        $timesheets = Timesheet::with(['employee.vendor', 'project', 'customer', 'time_exp' => function ($query) {
            $query->select('timesheet_id', 'id', 'amount', 'description')->where('claim', 1);
        }, 'timeline' => function ($timeline) {
            return $timeline->select(['timesheet_id', 'total', 'total_m'])->where('vend_is_billable', 1);
        }])
            ->selectRaw('timesheet.id, timesheet.first_day_of_week, timesheet.employee_id, timesheet.project_id, timesheet.customer_id, IF('.$request->currency_type.' = 1, assignment.external_cost_rate_sec, assignment.external_cost_rate) AS external_cost_rate,
            timesheet.year_week, assignment.customer_po, IF('.$request->currency_type.' = 1, assignment.currency_sec, assignment.currency) AS currency')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            })
            ->whereIn('timesheet.id', $timesheets_id)
            ->groupBy(['timesheet.id', 'timesheet.employee_id', 'timesheet.project_id', 'timesheet.first_day_of_week', 'assignment.external_cost_rate'])
            ->groupBy(['timesheet.year_week', 'assignment.customer_po', 'timesheet.customer_id'])
            ->groupBy(['assignment.external_cost_rate_sec', 'assignment.currency_sec', 'assignment.currency'])
            ->get();

        $is_same_vendor = $timesheets->map(function ($timesheet) {
            return $timesheet->employee->vendor->id ?? null;
        })->filter()->flatten()->unique()->toArray();
// dd($is_same_vendor);
        $vendor_fields = [
            'id', 'vendor_name', 'vat_no', 'contact_firstname', 'contact_lastname', 'phone', 'cell', 'email', 'postal_address_line1',
            'postal_address_line2', 'postal_address_line3', 'state_province', 'postal_zipcode', 'country_id', 'bank_name',
            'branch_code', 'bank_acc_no', 'vendor_logo', 'account_manager',
        ];
        $vendor = Vendor::find($is_same_vendor, $vendor_fields)->first() ?? null;

        if(session('vat_rate_id')){
            $vat_rate = (VatRate::find(session('vat_rate_id'), ['vat_rate'])->vat_rate??0)/100;
        } else {
            $vat_rate = (VatRate::find($request->vat_rate_id, ['vat_rate'])->vat_rate??0)/100;
        }

        /*
         * If is_same_vendor is empty or has more than one value in the
         * array the invoice should be invalid and return you back
         * back to create a valid one
        */

        if (empty($is_same_vendor) || (count($is_same_vendor) > 1) || ! isset($vendor)) {
            return ['is_same_vendor' => false];
        }

        $line_items = $timesheets->map(function ($timesheet) use ($company, $vendor, $save, $vat_rate) {

            $billable_hours_ = $this->billable_hours($timesheet);

            return [
                'description' => 'Timesheet for '.($timesheet->employee->name() ?? 'Employee Name').' for week '.$timesheet->year_week.' on assignment '.($timesheet->project->name ?? 'Assignment name ').' customer po number '.$timesheet->customer_po,
                'hours' => $billable_hours_, //round($timesheet->billable_hours,2),
                'consultant_rate' => $timesheet->external_cost_rate,
                'vat' => $vat_rate,
                'line_item_total_no_vat' => $billable_hours_ * $timesheet->external_cost_rate,
                'line_item_total_vat' => (($billable_hours_ * $timesheet->external_cost_rate) * (1 + $vat_rate)),
                'timesheet_id' => ($save) ? $timesheet->id : null,
                'timeline' => $timesheet->timeline,
                'currency' => $timesheet->currency,
            ];
        });

        $total_vat_amount = $timesheets->sum(fn ($timesheet) => ($this->billable_hours($timesheet) * $timesheet->external_cost_rate) * $vat_rate);

        $total_amount_no_vat = $timesheets->sum(function ($timesheet) {
            return round($this->billable_hours($timesheet), 2) * $timesheet->external_cost_rate;
        });

        $total_amount_with_vat = $total_vat_amount + $total_amount_no_vat;

        $expense_line_item = $timesheets->map(function ($timesheet) {
            return $timesheet->time_exp->map(function ($expense) use ($timesheet) {
                return [
                    'id' => $expense->id,
                    'timesheet_id' => $expense->timesheet_id,
                    'description' => 'Expenses for '.($timesheet->employee->name() ?? '%ConsultantName%').' for week '.($timesheet->year_week ?? '%000000%').' on assignment '.($timesheet->project->name ?? '%ProjectName%').' for '.$expense->description.'.',
                    'amount' => $expense->amount,
                    'claim' => $expense->claim,
                ];
            });
        })->flatten(1);

        $invoice_number = VendorInvoice::select(['id', 'vendor_invoice_number'])->latest()->first();

        /**
         * Passing data between multiple pages
         */
        if (! $save) {
            session(['invoice_reference' => $request->invoice_ref]);
            session(['invoice_date' => $request->invoice_date]);
            session(['vendor_reference' => $request->vendor_ref]);
            session(['invoice_due_date' => $request->due_date]);
            session(['invoice_note' => $request->invoice_notes]);
            session(['vat_rate_id' => $request->vat_rate_id]);
        }

        return [
            'company' => $company,
            'vendor' => $vendor,
            'vat_rate_id' => $request->vat_rate_id,
            'invoice_line_items' => $line_items,
            'invoice_vat_amount' => $total_vat_amount,
            'invoice_amount_no_vat' => $total_amount_no_vat,
            'invoice_total_amount' => $total_amount_with_vat,
            'invoice_expense_line_items' => $expense_line_item,
            'placeholder_invoice_number' => isset($invoice_number) ? $this->invoiceNumberString($invoice_number) : 1,
            'is_currency_the_same' => $this->isCurrencyTheSame($line_items),
            'currency_type' => $request->currency_type,
            'currency' => $this->sysConfig()->currency,
            'contact_person' => $this->contact($timesheets[0]->project, $timesheets[0]->customer),
        ];
    }

    private function vat_rate($end_date)
    {
        return VatRate::where('vat_code', '=', $this->sysConfig()->vat_rate_id)
            ->where('end_date', '>=', $end_date)
            ->first();
    }

    private function billable_hours($timesheet)
    {
        return $timesheet->timeline->sum(function ($line) {
            return $line->total + ($line->total_m / 60);
        });
    }

    private function isCurrencyTheSame($items)
    {
        $currencies = $items->map(function ($currency) {
            return $currency['currency'];
        })->unique()->values()->toArray();

        if (count($currencies) > 1) {
            return 0;
        }

        return 1;
    }

    private function newInvoiceNumberInt($vendor_invoice)
    {
        if (! isset($this->sysConfig()->vendor_invoice_start_number)) {
            return $vendor_invoice->id;
        }

        $last_invoice = VendorInvoice::select(['id', 'vendor_invoice_number'])->latest()->first();
        if (isset($last_invoice->vendor_invoice_number)) {
            return  $vendor_invoice->extractIntFromString($this->sysConfig()->vendor_invoice_prefix ?? null, $last_invoice->vendor_invoice_number) + 1;
        }

        return (int) $this->sysConfig()->vendor_invoice_start_number;
    }

    private function invoiceNumberString($vendor_invoice)
    {
        return $vendor_invoice->formatInvoiceNumber(
            $this->sysConfig()->vendor_invoice_prefix ?? null,
            $this->newInvoiceNumberInt($vendor_invoice)
        );
    }

    private function saveInvoiceNumber($vendor_invoice)
    {
        $vendor_invoice = $vendor_invoice->refresh();
        try {
            $vendor_invoice->vendor_invoice_number = $this->invoiceNumberString($vendor_invoice);
            $vendor_invoice->save();
        } catch (QueryException $e) {
            $this->saveInvoiceNumber($vendor_invoice);
        }
    }

    private function contact(Project $project, Customer $customer)
    {
        $project = $project->load('customerInvoiceContact');
        $customer = $customer->load('invoiceContact');

        if (isset($project->customerInvoiceContact)) {
            return (object) [
                'name' => $project->customerInvoiceContact->full_name ?? null,
                'number' => $project->customerInvoiceContact->contact_number ?? null,
                'email' => $project->customerInvoiceContact->email ?? null,
            ];
        }

        if (isset($customer->invoiceContact)) {
            return (object) [
                'name' => $customer->invoiceContact->full_name ?? null,
                'number' => $customer->invoiceContact->contact_number ?? null,
                'email' => $customer->invoiceContact->email ?? null,
            ];
        }

        return (object) [
            'name' => $customer->contact_firstname.' '.$customer->contact_lastname,
            'number' => $customer->phone ?? $customer->cell,
            'email' => $customer->email,
        ];
    }

    private function sysConfig(): Config
    {
        return Config::first();
    }
}
