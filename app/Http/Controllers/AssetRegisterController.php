<?php

namespace App\Http\Controllers;

use App\Exports\EquipmentsExport;
use App\Exports\JobSpecsExport;
use App\Models\AdvancedTemplates;
use App\Models\AssetClass;
use App\Models\AssetConditionNote;
use App\Models\AssetRegister;
use App\Models\Company;
use App\Models\Config;
use App\Models\DigisignUsers;
use App\Models\Document;
use App\Http\Requests\StoreAssetRegisterRequest;
use App\Http\Requests\UpdateAssetRegisterRequest;
use App\Jobs\SendAssertAknowledgementDigiSignEmailJob;
use App\Jobs\SendAssetRegisterEmailJob;
use App\Mail\AssetAknowledgementDigiSignMail;
use App\Models\Insurance;
use App\Models\Module;
use App\Models\Status;
use App\Models\TemplateType;
use App\Models\User;
use App\Services\EquipmentService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Number;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;

class AssetRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, EquipmentService $service)
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $item = $request->input('r') ?? 15;

        $assetreg = AssetRegister::select(['id', 'asset_nr', 'asset_class', 'make', 'model', 'issued_to', 'aquire_date', 'retire_date', 'status_id'])
            ->with([
                'assetClass:id,description',
                'resource:id,first_name,last_name',
                'insurance:asset_register_id,insurer',
                'status:id,description',
                'assetHistoryActive.user:id,first_name,last_name',
            ]);

        if ($service->hasViewTeamPermission()) {
            $assetreg = $assetreg->whereIn('issued_to', Auth::user()->team());
        }

        $assetreg = $assetreg->when($request->q, fn($asset) => $asset->where(fn($reg) => $reg->where('asset_nr', 'LIKE', '%'.$request->q.'%')
        ->orWhere('make', 'LIKE', '%'.$request->q.'%')
        ->orWhere('model', 'LIKE', '%'.$request->q.'%')
        ->orWhere('aquire_date', 'LIKE', '%'.$request->q.'%')
        ->orWhere('retire_date', 'LIKE', '%'.$request->q.'%')))
            ->when($request->insurer == 1, fn($asset) => $asset->whereHas('insurance'))
            ->when($request->insurer == 0 && $request->insurer != null, fn($asset) => $asset->whereDoesntHave('insurance'))
            ->when($request->asset_class, fn($asset) => $asset->where('asset_class', $request->asset_class))
            ->when($request->status_id, fn($asset) => $asset->where('status_id', $request->status_id))
            ->latest()->paginate($item);

        $parameters = [
            'assetreg' => $assetreg,
            'assetclass_dropdown' => AssetClass::orderBy('id')->pluck('description', 'id'),
        ];

        if ($request->has('export')) return $this->export($parameters, 'assetreg');

        return view('assetreg.index')->with($parameters);
    }

    public function show(AssetRegister $assetreg, EquipmentService $service): View
    {
        abort_unless($service->hasViewPermission(), 403);

        $subject = 'Acknowledgement of Receipt - '.$assetreg->issuedTo?->name();
        $equipment = $assetreg->only(['written_off_at', 'sold_at', 'original_value', 'retire_date', 'est_life']);
        $equipment['cost_of_repairs'] = $assetreg->additionalCost?->sum('cost');

        $parameters = [
            'assetreg' => $assetreg,
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '0'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'assetclass' => AssetClass::orderBy('id')->pluck('description', 'id'),
            'subject' => $subject,
            'claim_approver' => $service->claimApprover(),
            'remaining_months' => $service->remainingMonth($equipment),
            'additional_cost' => Number::format($equipment['cost_of_repairs'],2),
            'current_book_value' => Number::format($service->currentBookValue($equipment), 2),
        ];

        return view('assetreg.show')->with($parameters);
    }

    public function create(): View
    {
        $parameters = [
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereHas('roles', fn($role) => $role->whereNotIn('id', [7,8,11]))->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '0'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'assetclass' => AssetClass::orderBy('id')->pluck('description', 'id'),
            'templates' => AdvancedTemplates::where('template_type_id', 5)->pluck('name', 'id')
        ];

        return view('assetreg.create')->with($parameters);
    }

    public function store(StoreAssetRegisterRequest $request, EquipmentService $service): RedirectResponse
    {
        $referrer = new AssetRegister();
        $referrer->asset_class = $request->input('assetclass');
        $referrer->asset_nr = $request->input('asset_nr');
        $referrer->make = $request->input('make');
        $referrer->model = $request->input('model');
        $referrer->serial_nr = $request->input('serial_nr');

        if ($request->hasFile('picture')) {
            $file = $request->file('picture')->hashName();
            $request->file('picture')->storeAs('avatars/asset',$file);

            $referrer->picture = $file;
        }

        $referrer->aquire_date = $request->input('aquire_date');
        $referrer->retire_date = $request->input('retire_date');
        $referrer->original_value = $request->input('original_value');
        $referrer->issued_to = $request->input('resource');
        $referrer->note = $request->input('notes');
        $referrer->est_life = $request->input('est_life');
        $referrer->processor = $request->input('processor');
        $referrer->ram = $request->input('ram');
        $referrer->hdd = $request->input('hdd');
        $referrer->screen_size = $request->input('screen_size');
        $referrer->supplier = $request->input('supplier');
        $referrer->additional_info = $request->input('additional_info');
        $referrer->acc_form = $request->input('maint_cost');

        $referrer->creator_id = \auth()->id();

        $referrer->warranty_months = $request->input('warranty_months');
        $referrer->warranty_expiry_date = $request->input('warranty_expiry_date');
        $referrer->warranty_note = $request->input('warranty_note');
        $referrer->support_months = $request->input('support_months');
        $referrer->support_expiry_date = $request->input('support_expiry_date');
        $referrer->support_provided_by = $request->input('support_provided_by');
        $referrer->contact_number = $request->input('contact_number');
        $referrer->contact_email = $request->input('contact_email');
        $referrer->reference_number = $request->input('reference_number');
        $referrer->support_note = $request->input('support_note');
        $referrer->template_id = $request->input('template_id');
        $referrer->status_id = $request->input('status');
        $referrer->creator_id = auth()->id();
        $referrer->save();

        $service->storeInsurance($request, $referrer);

        $service->storeAssetHistory($request, $referrer);

        $service->storeAdditionalCost($request, $referrer);

        $service->notifyUser($referrer);

        return redirect(route('assetreg.index'))->with('flash_success', 'Equipment captured successfully');
    }

    public function edit(AssetRegister $assetreg, EquipmentService $service): View
    {
        $assetreg->loadMissing([
            'status:id,description',
            'insurance',
            'assetConditionNotes:asset_id,note,include_in_acceptance_letter',
            'assetHistoryActive.user:id,first_name,last_name',
        ]);

        $equipment = $assetreg->only(['written_off_at', 'sold_at', 'original_value', 'retire_date', 'est_life']);
        $equipment['cost_of_repairs'] = $assetreg->additionalCost?->sum('cost');

        $parameters = [
            'assetreg' => $assetreg,
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '0'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'assetclass' => AssetClass::orderBy('id')->pluck('description', 'id'),
            'remaining_months' => $service->remainingMonth($equipment),
            'additional_cost' => Number::format($equipment['cost_of_repairs'],2),
            'current_book_value' => Number::format($service->currentBookValue($equipment), 2),
            'templates' => AdvancedTemplates::where('template_type_id', 5)->pluck('name', 'id')
        ];

        return view('assetreg.edit')->with($parameters);
    }

    public function update(UpdateAssetRegisterRequest $request,AssetRegister $assetreg): RedirectResponse
    {
        $assetreg->asset_class = $request->input('assetclass');
        $assetreg->asset_nr = $request->input('asset_nr');
        $assetreg->make = $request->input('make');
        $assetreg->model = $request->input('model');
        $assetreg->serial_nr = $request->input('serial_nr');

        if ($request->hasFile('picture')) {
            $file = $request->file('picture')->hashName();
            $request->file('picture')->storeAs('avatars/asset',$file);

            $assetreg->picture = $file;
        }

        $assetreg->aquire_date = $request->input('aquire_date');
        $assetreg->retire_date = $request->input('retire_date');
        $assetreg->original_value = $request->input('original_value');
        $assetreg->written_off_at = $request->input('write_off_at');
        $assetreg->written_off_value = $request->input('written_off_value');
        $assetreg->sold_at = $request->input('sold_at');
        $assetreg->sold_value = $request->input('sold_value');
        $assetreg->note = $request->input('notes');
        $assetreg->est_life = $request->input('est_life');
        $assetreg->processor = $request->input('processor');
        $assetreg->ram = $request->input('ram');
        $assetreg->hdd = $request->input('hdd');
        $assetreg->screen_size = $request->input('screen_size');
        $assetreg->supplier = $request->input('supplier');
        $assetreg->additional_info = $request->input('additional_info');
        $assetreg->acc_form = $request->input('maint_cost');
        $assetreg->company_id = $request->input('template_id');
        $assetreg->creator_id = $request->input('maint_cost');
        $assetreg->status_id = $request->input('status');
        $assetreg->template_id = $request->input('template_id');

        $assetreg->warranty_months = $request->input('warranty_months');
        $assetreg->warranty_expiry_date = $request->input('warranty_expiry_date');
        $assetreg->warranty_note = $request->input('warranty_note');
        $assetreg->support_months = $request->input('support_months');
        $assetreg->support_expiry_date = $request->input('support_expiry_date');
        $assetreg->support_provided_by = $request->input('support_provided_by');
        $assetreg->contact_number = $request->input('contact_number');
        $assetreg->contact_email = $request->input('contact_email');
        $assetreg->reference_number = $request->input('reference_number');
        $assetreg->support_note = $request->input('support_note');

        $assetreg->save();

        if ($request->insurer || $request->insured_value || $request->covered_at || $request->excess_amount || $request->note){
            Insurance::updateOrCreate(
                ['asset_register_id' => $assetreg->id],
                [
                    'insurer' => $request->insurer,
                    'insured_value' => $request->insured_value,
                    'covered_at' => $request->covered_at,
                    'excess_amount' => $request->excess_amount,
                    'note' => $request->note,
                    'policy_ref' => $request->policy_ref,
                ]
            );
        }

        return redirect(route('assetreg.index'))->with('flash_success', 'Equipment updated successfully.');
    }

    public function print(Request $request, $asset_id)
    {
        $asset = AssetRegister::findOrFail($asset_id);

        $template = $asset->template;

        try {
            $final_template = $asset->mapVariables($template);
            $final_template .= '<div>
                Signature  <span class="signature">__________________________________</span> At _______________________ On ______/____/____
            </div>';

            return Pdf::view('template.asset_aknowledgement_form', [
                'template' => $final_template
            ])->format('a4')
                ->name($asset->asset_nr.".pdf");

            //return response()->file($file_name_pdf);
        } catch (\TypeError $e) {
            report($e);
            return redirect(route('assetreg.edit', $asset_id))->with('flash_danger', 'Edit your equipment and select a template.');
        }
    }

    public function sendDigiSign($asset_id, Request $request): RedirectResponse
    {
        $asset = AssetRegister::with('asset_class_name')->find($asset_id);
        $asset_condition_notes = AssetConditionNote::where('asset_id', '=', $asset->id)->where('include_in_acceptance_letter', '=', 1)->get();
        $resource = User::find(Auth::id());

        $subject = 'Aknowledgement of Receipt - '.$resource->first_name.' '.$resource->last_name;

        $config = Config::first();
        $company = $resource->company??Company::first();
        if (isset($config->company_id)) {
            $company = Company::find($config->company_id);
        } else {
            $company = Company::find($config->company_id);
        }

        $parameters = [
            'asset' => $asset,
            'resource' => $resource,
            'asset_condition_notes' => $asset_condition_notes,
            'signature' => ' __________________________________',
            'signed_at' => '_______________________',
            'date' => '______/____/____',
            'company' => $company,
        ];

        try {
            if (! Storage::exists('Equipment/')) {
                Storage::makeDirectory('Equipment/');
            }

            $file_name_pdf = storage_path('app/Equipment/file_'.date('Y_m_d_H_i_s').'.pdf');

            Pdf::view('template.asset_aknowledgement_form', $parameters)
                ->format('a4')
                ->save($file_name_pdf);

            $document = new Document();
            $document->type_id = 9;
            $document->document_type_id = 9; //document type Assignment(documenttype table in db)
            $document->name = 'Aknowledgement of Receipt Digisign - '.$resource->first_name.' '.$resource->last_name;
            $document->file = "file_".date('Y_m_d_H_i_s').'.pdf';
            $document->creator_id = auth()->id();
            $document->owner_id = $resource->id;
            $document->digisign_status_id = 2;
            $document->reference_id = $asset_id;
            $document->digisign_approver_user_id = $config->claim_approver_id ?? 0;
            $document->save();

            if ($request->has('resource_email')) {
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = '';
                $digisign_users->user_email = $request->input('resource_email');
                $digisign_users->user_id = $resource->id;
                $digisign_users->user_number = 1;
                $digisign_users->is_claim_approver = 0;
                $digisign_users->signed = 0;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                /*Mail::to(trim($request->input('resource_email')))->send(new AssetAknowledgementDigiSignMail($asset->id, $digisign_users->id, $resource->first_name.' '.$resource->last_name, $subject, $digisign_users->signed));*/
                SendAssertAknowledgementDigiSignEmailJob::dispatch(trim($request->input('resource_email')), $asset->id, $digisign_users->id, $resource->first_name.' '.$resource->last_name, $subject, $digisign_users->signed)->delay(now()->addMinutes(5));
            }

            if ($request->has('report_to_email')) {
                $report_to = User::where('email', 'like', $request->input('report_to_email'))->first();
                $report_to_name = $request->input('report_to_email');
                $report_to_id = 2;
                if (isset($report_to->first_name)) {
                    $report_to_name = $report_to->first_name.' '.$report_to->last_name;
                    $report_to_id = $report_to->id;
                }

                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = '';
                $digisign_users->user_email = $request->input('report_to_email');
                $digisign_users->user_id = $report_to_id;
                $digisign_users->user_number = 2;
                $digisign_users->is_claim_approver = 1;
                $digisign_users->signed = 0;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                /*Mail::to(trim($request->input('report_to_email')))->send(new AssetAknowledgementDigiSignMail($asset->id, $digisign_users->id, $report_to_name, $subject, $digisign_users->signed));*/
                SendAssertAknowledgementDigiSignEmailJob::dispatch(trim($request->input('resource_email')), $asset->id, $digisign_users->id, $resource->first_name.' '.$resource->last_name, $subject, $digisign_users->signed)->delay(now()->addMinutes(5));
            }
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('assetreg.show', $asset_id)->with('flash_danger', $e->getMessage()));
        }

        return redirect(route('assetreg.show', $asset))->with('flash_success', 'Digisign successfully sent');
    }

    public function destroy($emp_id): RedirectResponse
    {
        AssetRegister::destroy($emp_id);

        return redirect()->route('assetreg.index')->with('success', 'Equipment deleted successfully');
    }

    public function writeOff(Request $request, AssetRegister $asset)
    {
        $request->validate([
            'type' => 'required|string',
            'date' => 'required|date',
            'value' => 'required|numeric'
        ]);
        $date = $request->type."_at";
        $value = $request->type."_value";

        $asset->$date = $request->date;
        $asset->$value = $request->value;
        $asset->save();

        return redirect()->back()->with('flash_success', 'Equipment was written off successfully');
    }
}
