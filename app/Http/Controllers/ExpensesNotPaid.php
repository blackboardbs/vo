<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\Time;
use App\Models\TimeExp;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ExpensesNotPaid extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
            ->with(['timesheet' => function ($query) {
                $query->select('employee_id', 'id', 'project_id', 'year_week');
                $query->where('exp_paid', '=', 0);
                $query->groupBy('employee_id');
                $query->groupBy('year_week');
                $query->groupBy('project_id');
                $query->groupBy('id');
                $query->orderBy('employee_id', 'desc');
            }])
            ->whereHas('timesheet', function ($query) {
                $query->where('exp_paid', '=', 0);
            })
            ->where('claim', '=', 1)
            ->groupBy('timesheet_id')
            ->groupBy('yearwk')
            ->orderBy('yearwk', 'desc')
            ->paginate($item);

        if ($request->has('company') && $request->company != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('time', function ($query) {
                    $query->where('exp_paid', '=', 0);
                })
                ->where('claim', '=', 1)
                ->where('company_id', '=', $request->company)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('timesheet') && $request->team != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) use ($request) {
                    $query->where('exp_paid', '=', 0);
                    $query->whereHas('employee.resource', function ($query) use ($request) {
                        $query->where('team_id', '=', $request->team);
                    });
                })
                ->where('claim', '=', 1)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('employee') && $request->employee != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) use ($request) {
                    $query->where('exp_paid', '=', 0);
                    $query->where('employee_id', '=', $request->employee);
                })
                ->where('claim', '=', 1)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }, 'project'])
                ->whereHas('timesheet', function ($query) use ($request) {
                    $query->where('exp_paid', '=', 0);
                    $query->where('year', '=', substr($request->yearmonth, 0, 4));
                    $query->where('month', '=', (substr($request->yearmonth, 4, 2) < 10) ? substr($request->yearmonth, 5, 1) : substr($request->yearmonth, 4, 2));
                })
                ->where('claim', '=', 1)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('dates') && $request->dates != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) {
                    $query->where('exp_paid', '=', 0);
                })
                ->where('claim', '=', 1)
                ->whereBetween('date', [substr($request->dates, 0, 10), substr($request->dates, 13, 10)])
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('customer') && $request->customer != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) {
                    $query->where('exp_paid', '=', 0);
                })
                ->whereHas('project')
                ->where('claim', '=', 1)
                ->where('customer_id', $request->customer)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('project') && $request->project != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
           timesheet_id, yearwk, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('year_week');
                    $query->groupBy('project_id');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) use ($request) {
                    $query->where('exp_paid', '=', 0);
                    $query->where('project_id', '=', $request->project);
                })
                ->where('claim', '=', 1)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        if ($request->has('billable') && $request->billable != null) {
            $exp_not_paid = TimeExp::select(DB::raw('
            timesheet_id, yearweek, SUM(amount) AS exp_amount
        '))
                ->with(['timesheet' => function ($query) {
                    $query->select('employee_id', 'id', 'project_id', 'year_week');
                    $query->where('exp_paid', '=', 0);
                    $query->groupBy('employee_id');
                    $query->groupBy('project_id');
                    $query->groupBy('year_week');
                    $query->groupBy('id');
                    $query->orderBy('employee_id', 'desc');
                }])
                ->whereHas('timesheet', function ($query) {
                    $query->where('exp_paid', '=', 0);
                    $query->whereHas('project');
                })
                ->where('claim', '=', 1)
                ->where('billable', $request->billable)
                ->groupBy('timesheet_id')
                ->groupBy('yearwk')
                ->orderBy('yearwk', 'desc')
                ->paginate($item);
        }

        $total_amount = 0;

        foreach ($exp_not_paid as $timesheet) {
            $total_amount += $timesheet->exp_amount;
        }

        //return $exp_not_paid;

        $calendar = Calendar::where('status', '=', 1)->whereDate('date', '<=', Carbon::now()->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get()->unique();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'dates_dropdown' => Time::select(DB::raw("CONCAT(start_date,' - ',COALESCE(`end_date`,'')) AS dates"), 'id')->orderBy('start_date', 'desc')->get()->pluck('dates', 'dates')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'timesheets' => $exp_not_paid,
            'total_amount' => $total_amount,

        ];

        return view('reports.expenses_not_paid')->with($parameters);
    }
}
