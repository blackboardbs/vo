<?php

namespace App\Http\Controllers;

use App\Exports\ActionsExport;
use App\Exports\BoardsExport;
use App\Models\Board;
use App\Http\Requests\BoardRequest;
use App\Models\Module;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class BoardController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;
        $module = Module::where('name', '=', \App\Models\Board::class)->first();

        $boards = Board::with(['status:id,name,description'])->orderBy('id')->sortable(['name' => 'asc'])->paginate($item);
        if($request->has('q') && $request->q != ''){
            $boards = Board::orderBy('id')->where('name','like','%'.$request->q.'%')->sortable()->paginate($item);
        }

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                // $projects = $projects->whereIn('manager_id', Auth::user()->team());
                $boards = Board::orderBy('id')->sortable(['name' => 'asc'])->whereIn('creator_id', Auth::user()->team())->paginate(15);
            }
        } else {
            return abort(403);
        }

        $parameters = [
            'boards' => $boards,
        ];

        if ($request->has('export')) return $this->export($parameters, 'board');

        return view('board.index')->with($parameters);
    }

    public function create(): View
    {
        $statusDropDown = Status::where('status', '=', 1)->orderBy('name')->pluck('name', 'id');

        $parameters = [
            'statusDropDown' => $statusDropDown,
        ];

        return view('board.create')->with($parameters);
    }

    public function store(BoardRequest $request): RedirectResponse
    {
        $board = new Board();
        $board->name = $request->input('name');
        $board->status_id = $request->input('status_id');
        $board->save();

        return redirect(route('board.index'))->with('flash_success', 'Board added successfuly.');
    }

    public function edit(Board $board): View
    {
        $statusDropDown = Status::where('status', '=', 1)->orderBy('name')->pluck('name', 'id');

        $parameters = [
            'board' => $board,
            'statusDropDown' => $statusDropDown,
        ];

        return view('board.edit')->with($parameters);
    }

    public function update($id, BoardRequest $request): RedirectResponse
    {
        $board = Board::find($id);
        $board->name = $request->input('name');
        $board->status_id = $request->input('status_id');
        $board->save();

        return redirect(route('board.index'))->with('flash_success', 'Board updated successfuly.');
    }

    public function show(Board $board): View
    {
        $statusDropDown = Status::where('status', '=', 1)->orderBy('name')->pluck('name', 'id');

        $parameters = [
            'board' => $board,
            'statusDropDown' => $statusDropDown,
        ];

        return view('board.show')->with($parameters);
    }

    public function destroy($id): RedirectResponse
    {
        Board::destroy($id);

        return redirect(route('board.index'))->with('flash_success', 'Board deleted successfuly.');
    }
}
