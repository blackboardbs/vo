<?php

namespace App\Http\Controllers;

use App\Models\Availability;
use App\Models\AvailStatus;
use App\Models\BBBEERace;
use App\Models\Customer;
use App\Models\CustomerPreference;
use App\Models\Cv;
use App\Models\Disability;
use App\Models\DistributionList;
use App\Models\DistributionListConfig;
use App\Models\Experience;
use App\Http\Requests\DistributionListRequest;
use App\Models\Industry;
use App\Models\IndustryExperience;
use App\Models\JobOrigin;
use App\Models\JobSpec;
use App\Models\JobSpecStatus;
use App\Models\Module;
use App\Models\Profession;
use App\Models\Qualification;
use App\Models\Resource;
use App\Models\ResourceType;
use App\Models\RoleUser;
use App\Models\ScheduleRunFrequency;
use App\Models\ScoutingRole;
use App\Models\Skill;
use App\Models\SkillLevel;
use App\Models\Speciality;
use App\Models\Status;
use App\Models\System;
use App\Models\User;
use App\Models\WorkType;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DistributionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;
        $distributionLists = DistributionList::with(['frequency:id,name', 'status:id,description'])->orderBy('id', 'desc');

        if($request->has('q') && $request->q != ''){
            $distributionLists = $distributionLists->where('name','like','%'.$request->q.'%');
        }
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $distributionLists = $distributionLists->whereIn('creator_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $distributionLists = $distributionLists->paginate($item);

        // Todo: Create a migration table to store this
        $distributionListType = [1 => 'Candidate List', 2 => 'Job List', 3 => 'Customer List', 1000000 => 'Uploaded List'];

        $parameters = [
            'distributionLists' => $distributionLists,
            'distributionListType' => $distributionListType,
        ];

        return view('distributionlist.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            if (! Auth::user()->canAccess($module->id, 'create_all') && Auth::user()->canAccess($module->id, 'create_team')) {
                // Continue
            }
        } else {
            return abort(403);
        }

        $run_frequency_drow_down = ScheduleRunFrequency::orderBy('id')->pluck('name', 'id');
        $status_drow_down = Status::where('status', '=', 1)->orderBy('id')->pluck('description', 'id');

        /*Start Block - Job Spec*/
        $job_specs = JobSpec::with('positiontype')->orderby('id')->where('status_id', '=', 1)->get();
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $position_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = JobSpecStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1=>'Open', 2=>'closed'];
        $job_origin_drop_down = JobOrigin::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = WorkType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Full Time', 2 => 'Part Time'];
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->pluck('description', 'id');
        //$role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', -1);
        $money_drop_down = [];
        for ($i = 1000; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $recruiters = [];
        $users = User::select(['id', 'first_name', 'last_name'])->whereHas('roles', fn($role) => $role->where("id", 11))->get();
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->name();
        }

        /*End Block - Job Spec*/

        /*Start Block - Customer*/
        //$customers = Customer::orderBy('customer_name', 'asc')->get();
        $customer_preferences = CustomerPreference::orderBy('id')->get();
        /*End Block - Customer*/

        /*Start Block - CV*/
        $cv = Cv::with('user')->whereHas('user')->sortable('id', 'desc')->get();
        /*End Block - CV*/

        $distributionListDropDown = DistributionListConfig::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id')->prepend('Please select..', '');

        //Candidate - Start
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        //Candidate - End

        $hours_drop_down = [];
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $hours_drop_down[$i] = $i;
        }

        $minutes_drop_down = [];
        for ($i = 0; $i < 60; $i += 5) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $minutes_drop_down[$i] = $i;
        }

        $parameters = [
            'run_frequency_drow_down' => $run_frequency_drow_down,
            'status_drow_down' => $status_drow_down,
            'job_specs' => $job_specs,
            /*Start - Job spec block*/
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'position_type_drop_down' => $position_type_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'money_drop_down' => $money_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'industry_drop_down' => Industry::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'bbbee_race_drop_down' => BBBEERace::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'role_drop_down' => $role_drop_down,
            'priority_drop_down' => $priority,
            'recruiters_drop_down' => $recruiters,
            /*End - Job spec block*/
            /*Start - Customer block*/
            'customer_preferences' => $customer_preferences,
            'cv' => $cv, 'availability_drop_down' => AvailStatus::where('status_id', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Availability', -1),
            'skill_drop_down' => SkillLevel::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Skill Level', -1),
            'resource_type_drop_down' => ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Resource Type', -1),
            /*End - Customer block*/
            'distributionListDropDown' => $distributionListDropDown,
            'hours_drop_down' => $hours_drop_down,
            'minutes_drop_down' => $minutes_drop_down,
        ];

        return view('distributionlist.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DistributionListRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            if (! Auth::user()->canAccess($module->id, 'create_all') && Auth::user()->canAccess($module->id, 'create_team')) {
                //
            }
        } else {
            return abort(403);
        }

        $distributionList = new DistributionList();
        $distributionList->name = $request->input('name');
        $distributionList->from = $request->input('from');
        $distributionList->subject = $request->input('subject');
        $distributionList->email_body = $request->input('email_body');
        $distributionList->from_query = $request->input('from_query');
        $distributionList->to_query = $request->input('to_query');

        if ($request->hasFile('to_list')) {

            try {
                $request->file('to_list')->store('distributionlist/');
                $file = $request->file('to_list')->hashName();
                Storage::copy('distributionlist/'.$file, 'public/assets/'.$file);

                $distributionList->to_query = $file;
            } catch (\Exception $e) {
                $exception_message = 'Content file could not be uploaded.';
            }
        }

        if ($request->hasFile('from_list')) {
            try {
                $request->file('from_list')->store('distributionlist/');
                $file = $request->file('from_list')->hashName();
                Storage::copy('distributionlist/'.$file, 'public/assets/'.$file);

                $distributionList->from_query = $file;
            } catch (\Exception $e) {
                $exception_message = 'Destination file could not be uploaded.';
            }
        }

        $distributionList->from_query_filters = $request->input('from_query_filters');
        $distributionList->to_query_filters = $request->input('to_query_filters');
        $distributionList->from_query_type_id = $request->input('from_query_type_id');
        $distributionList->to_query_type_id = $request->input('to_query_type_id');
        $distributionList->run_frequency_id = $request->input('run_frequency_id');
        $distributionList->last_run_date = $request->input('last_run_date');
        $distributionList->next_run_date = $request->input('next_run_date').' '.$request->input('hour').':'.$request->input('minute').':00';
        $distributionList->status_id = $request->input('status_id');
        $distributionList->save();

        return redirect(route('distributionlist.index'))->with('flash_success', 'Distribution list created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DistributionList  $distributionList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'show_all') || Auth::user()->canAccess($module->id, 'show_team')) {
            if (! Auth::user()->canAccess($module->id, 'show_all') && Auth::user()->canAccess($module->id, 'show_team')) {
                //
            }
        } else {
            return abort(403);
        }

        $distributionList = DistributionList::find($id);

        $run_frequency_drow_down = ScheduleRunFrequency::orderBy('id')->pluck('name', 'id');
        $status_drow_down = Status::where('status', '=', 1)->orderBy('id')->pluck('description', 'id');

        /*Start Block - Job Spec*/
        $job_specs = JobSpec::orderby('id')->where('status_id', '=', 1)->get();
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $position_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = JobSpecStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1=>'Open', 2=>'closed'];
        $job_origin_drop_down = JobOrigin::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = WorkType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Full Time', 2 => 'Part Time'];
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->pluck('description', 'id');
        //$role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', -1);
        $money_drop_down = [];
        for ($i = 1000; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $recruiters = [];
        $users = User::select(['id', 'first_name', 'last_name'])->whereHas('roles', fn($role) => $role->where("id", 11))->get();
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->name();
        }
        /*End Block - Job Spec*/

        /*Start Block - Customer*/
        //$customers = Customer::orderBy('customer_name', 'asc')->get();
        $customer_preferences = CustomerPreference::orderBy('id')->get();
        /*End Block - Customer*/

        /*Start Block - CV*/
        $cv = Cv::with('user')->whereHas('user')->sortable('id', 'desc')->get();
        /*End Block - CV*/

        $distributionListDropDown = DistributionListConfig::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id')->prepend('Please select..', '');

        //Candidate - Start
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        //Candidate - End

        $hours_drop_down = [];
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $hours_drop_down[$i] = $i;
        }

        $minutes_drop_down = [];
        for ($i = 0; $i < 60; $i += 5) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $minutes_drop_down[$i] = $i;
        }

        $parameters = [
            'distributionList' => $distributionList,
            'run_frequency_drow_down' => $run_frequency_drow_down,
            'status_drow_down' => $status_drow_down,
            'job_specs' => $job_specs,
            /*Start - Job spec block*/
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'position_type_drop_down' => $position_type_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'money_drop_down' => $money_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'industry_drop_down' => Industry::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'bbbee_race_drop_down' => BBBEERace::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'role_drop_down' => $role_drop_down,
            'priority_drop_down' => $priority,
            'recruiters_drop_down' => $recruiters,
            /*End - Job spec block*/
            /*Start - Customer block*/
            'customer_preferences' => $customer_preferences,
            'cv' => $cv, 'availability_drop_down' => AvailStatus::where('status_id', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Availability', -1),
            'skill_drop_down' => SkillLevel::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Skill Level', -1),
            'resource_type_drop_down' => ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Resource Type', -1),
            /*End - Customer block*/
            'distributionListDropDown' => $distributionListDropDown,
            'hours_drop_down' => $hours_drop_down,
            'minutes_drop_down' => $minutes_drop_down,
        ];

        return view('distributionlist.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DistributionList  $distributionList
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            if (! Auth::user()->canAccess($module->id, 'update_all') && Auth::user()->canAccess($module->id, 'update_team')) {
                // Continue
            }
        } else {
            return abort(403);
        }

        $distributionList = DistributionList::find($id);

        $run_frequency_drow_down = ScheduleRunFrequency::orderBy('id')->pluck('name', 'id');
        $status_drow_down = Status::where('status', '=', 1)->orderBy('id')->pluck('description', 'id');

        /*Start Block - Job Spec*/
        $job_specs = JobSpec::orderby('id')->where('status_id', '=', 1)->get();
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $position_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = JobSpecStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1=>'Open', 2=>'closed'];
        $job_origin_drop_down = JobOrigin::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = WorkType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id'); //[1 => 'Full Time', 2 => 'Part Time'];
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->pluck('description', 'id');
        //$role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', -1);
        $money_drop_down = [];
        for ($i = 1000; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $recruiters = [];
        $users = User::select(['id', 'first_name', 'last_name'])->whereHas('roles', fn($role) => $role->where("id", 11))->get();
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->name();
        }
        /*End Block - Job Spec*/

        /*Start Block - Customer*/
        //$customers = Customer::orderBy('customer_name', 'asc')->get();
        $customer_preferences = CustomerPreference::orderBy('id')->get();
        /*End Block - Customer*/

        /*Start Block - CV*/
        $cv = Cv::with('user')->whereHas('user')->sortable('id', 'desc')->get();
        /*End Block - CV*/

        $distributionListDropDown = DistributionListConfig::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id')->prepend('Please select..', '');

        //Candidate - Start
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        //Candidate - End

        $hours_drop_down = [];
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $hours_drop_down[$i] = $i;
        }

        $minutes_drop_down = [];
        for ($i = 0; $i < 60; $i += 5) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $minutes_drop_down[$i] = $i;
        }

        $parameters = [
            'distributionList' => $distributionList,
            'run_frequency_drow_down' => $run_frequency_drow_down,
            'status_drow_down' => $status_drow_down,
            'job_specs' => $job_specs,
            /*Start - Job spec block*/
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'position_type_drop_down' => $position_type_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'money_drop_down' => $money_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'industry_drop_down' => Industry::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'bbbee_race_drop_down' => BBBEERace::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'role_drop_down' => $role_drop_down,
            'priority_drop_down' => $priority,
            'recruiters_drop_down' => $recruiters,
            /*End - Job spec block*/
            /*Start - Customer block*/
            'customer_preferences' => $customer_preferences,
            'cv' => $cv, 'availability_drop_down' => AvailStatus::where('status_id', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Availability', -1),
            'skill_drop_down' => SkillLevel::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Skill Level', -1),
            'resource_type_drop_down' => ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('Select Resource Type', -1),
            /*End - Customer block*/
            'distributionListDropDown' => $distributionListDropDown,
            'hours_drop_down' => $hours_drop_down,
            'minutes_drop_down' => $minutes_drop_down,
        ];

        return view('distributionlist.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\DistributionList  $distributionList
     * @return \Illuminate\Http\Response
     */
    public function update(DistributionListRequest $request, $id)
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            if (! Auth::user()->canAccess($module->id, 'update_all') && Auth::user()->canAccess($module->id, 'update_team')) {
                //
            }
        } else {
            return abort(403);
        }

        $path = DB::select(DB::raw('select absolute_path from configs')->getValue(DB::connection()->getQueryGrammar()));

        $distributionList = DistributionList::find($id);
        $distributionList->name = $request->input('name');
        $distributionList->from = $request->input('from');
        $distributionList->subject = $request->input('subject');
        $distributionList->email_body = $request->input('email_body');
        $distributionList->from_query = $request->input('from_query');
        $distributionList->to_query = $request->input('to_query');

        if ($request->hasFile('to_list')) {
            $targetPath = $path;
            try {
                $request->file('to_list')->store('distributionlist/');
                $file = $request->file('to_list')->hashName();
                Storage::copy('distributionlist/'.$file, 'public/assets/'.$file);

                $distributionList->to_query = $file;
            } catch (\Exception $e) {
                $exception_message = 'Content file could not be uploaded.';
            }
        }

        if ($request->hasFile('from_list')) {
            $targetPath = $path;
            try {
                $request->file('from_list')->store('distributionlist/');
                $file = $request->file('from_list')->hashName();
                Storage::copy('distributionlist/'.$file, 'public/assets/'.$file);

                $distributionList->from_query = $file;
            } catch (\Exception $e) {
                $exception_message = 'Destination file could not be uploaded.';
            }
        }

        $distributionList->from_query_filters = $request->input('from_query_filters');
        $distributionList->to_query_filters = $request->input('to_query_filters');
        $distributionList->from_query_type_id = $request->input('from_query_type_id');
        $distributionList->to_query_type_id = $request->input('to_query_type_id');
        $distributionList->run_frequency_id = $request->input('run_frequency_id');
        $distributionList->last_run_date = $request->input('last_run_date');
        $distributionList->next_run_date = $request->input('next_run_date').' '.$request->input('hour').':'.$request->input('minute').':00';
        $distributionList->status_id = $request->input('status_id');
        $distributionList->save();

        return redirect(route('distributionlist.index'))->with('flash_success', 'Distribution list updated successfully.');
    }

    public function getEmailTemplate($id): JsonResponse
    {
        $distributionListConfigs = DistributionListConfig::where('id', '=', $id)->first();

        return response()->json(['message' => 'Success', 'emailTemplate' => $distributionListConfigs]);
    }

    public function getJobSpec(Request $request): JsonResponse
    {
        $data = $request->all();

        $job_specs = $this->getJobSpecData($data);

        return response()->json(['message' => 'Success', 'jobSpecs' => $job_specs['job_specs'], 'searched_for' => $job_specs['searched_for'], 'post_data' => $data]);
    }

    public function getJobSpecData($data)
    {
        $job_specs = JobSpec::orderBy('id');

        $searched_for = [];

        if ($data['data']['reference_number'] != null && $data['data']['reference_number'] != '') {
            $job_specs = $job_specs->where('reference_number', $data['data']['reference_number']);

            $tmp_search = 'Reference Number: '.$data['data']['reference_number'];

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['profession'] != null && $data['data']['profession'] != '') {
            $professions = [];

            foreach ($data['data']['profession'] as $key => $value) {
                $professions[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('profession_id', $professions);

            $search_options = Profession::whereIn('id', $professions)->get();

            $tmp_search = 'Profession: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['speciality'] != null && $data['data']['speciality'] != '') {
            $specialities = [];

            foreach ($data['data']['speciality'] as $key => $value) {
                $specialities[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('speciality_id', $specialities);

            $search_options = Speciality::whereIn('id', $specialities)->get();

            $tmp_search = 'Speciality: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['position_type'] != null && $data['data']['position_type'] != '') {
            $positionTypes = [];

            foreach ($data['data']['position_type'] as $key => $value) {
                $positionTypes[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('employment_type_id', $positionTypes);

            $search_options = ResourceType::whereIn('id', $positionTypes)->where('status', '=', 1)->get();

            $tmp_search = 'Position Type: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['job_origin'] != null && $data['data']['job_origin'] != '') {
            $jobOrigins = [];

            foreach ($data['data']['job_origin'] as $key => $value) {
                $jobOrigins[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('origin_id', $jobOrigins);

            $search_options = JobOrigin::whereIn('id', $jobOrigins)->where('status_id', '=', 1)->get();

            $tmp_search = 'Job Origin: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['customer']) && ($data['data']['customer'] != '')) {
            $customers = [];

            foreach ($data['data']['customer'] as $key => $value) {
                $customers[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('customer_id', $customers);

            $search_options = Customer::whereIn('id', $customers)->get();

            $tmp_search = 'Customers: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->customer_name;
                } else {
                    $tmp_search .= $search_option->customer_name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['recruiter']) && ($data['data']['recruiter'] != '')) {
            $selected_values = [];

            foreach ($data['data']['recruiter'] as $key => $value) {
                $selected_values[] = (int) $value;
            }

            $job_specs = $job_specs->whereIn('recruiter_id', $selected_values);

            $search_options = User::whereIn('id', $selected_values)->get();

            $tmp_search = 'Recruiters: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->first_name.' '.$search_option->last_name;
                } else {
                    $tmp_search .= $search_option->first_name.' '.$search_option->last_name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['skills']) && ($data['data']['skills'] != '')) {
            $selected_values = [];

            foreach ($data['data']['skills'] as $key => $value) {
                $selected_values[] = (int) $value;
            }

            $tmp_job_specs = $job_specs->get();

            $skills_ids = [];
            foreach ($tmp_job_specs as $tmp_job_spec) {
                if ($tmp_job_spec->skills != '') {
                    $tmp_values = explode('|', $tmp_job_spec->skills);
                    foreach ($tmp_values as $tmp_value) {
                        if (in_array($tmp_value, $selected_values)) {
                            $skills_ids[] = $tmp_job_spec->id;
                        }
                    }
                }
            }

            $job_specs = $job_specs->whereIn('id', $skills_ids);

            $search_options = System::whereIn('id', $selected_values)->get();

            $tmp_search = 'Skills: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['job_status'] != null && $data['data']['job_status'] != '') {
            $job_specs = $job_specs->where('status_id', $data['data']['job_status']);

            $search_option = Status::where('id', '=', $data['data']['job_status'])->first();

            $tmp_search = 'Status: '.$search_option->name;

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['priority'] != null && $data['data']['priority'] != '') {
            $job_specs = $job_specs->where('priority', $data['data']['priority']);

            $tmp_search = 'Priority: '.$data['data']['priority'];

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['bee'] != null && $data['data']['bee'] != '') {
            $job_specs = $job_specs->where('b_e_e', $data['data']['bee']);

            $search_option = BBBEERace::where('id', $data['data']['bee'])->first();

            $tmp_search = 'Bee: '.($data['data']['bee'] == 1 ? 'Yes' : 'No');

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['rate_minimum'] != null && $data['data']['rate_minimum'] != '') {
            $job_specs = $job_specs->where('hourly_rate_minimum', '>=', $data['data']['rate_minimum']);

            $tmp_search = 'Rate Minimum: '.$data['data']['rate_minimum'];

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['rate_maximum'] != null && $data['data']['rate_maximum'] != '') {
            $job_specs = $job_specs->where('hourly_rate_maximum', '<=', $data['data']['rate_maximum']);

            $tmp_search = 'Rate Maximum: '.$data['data']['rate_maximum'];

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['salary_minimum'] != null && $data['data']['salary_minimum'] != '') {
            $job_specs = $job_specs->where('monthly_salary_minimum', '>=', $data['data']['salary_minimum']);

            $tmp_search = 'Salary Minimum: '.$data['data']['salary_minimum'];

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['salary_maximum'] != null && $data['data']['salary_maximum'] != '') {
            $job_specs = $job_specs->where('monthly_salary_maximum', '<=', $data['data']['salary_maximum']);

            $tmp_search = 'Salary Miximum: '.$data['data']['salary_maximum'];

            $searched_for[] = $tmp_search;
        }

        $job_specs_results = [];

        $job_specs_results['job_specs'] = $job_specs->get();
        $job_specs_results['searched_for'] = $searched_for;

        return $job_specs_results;
    }

    public function getCustomerPreference(Request $request): JsonResponse
    {
        $data = $request->all();

        $customer_preferences = $this->getCustomerPreferanceData($data);

        return response()->json(['message' => 'Success', 'customer_preferences' => $customer_preferences['customer_preferences'], 'searched_for' => $customer_preferences['searched_for'], 'post_data' => $data]);
    }

    public function getCustomerPreferanceData($data)
    {
        $customer_preferences = CustomerPreference::with('customer', 'profession', 'speciality')->orderBy('id');

        $searched_for = [];

        if ($data['data']['cust_profession'] != null && $data['data']['cust_profession'] != '') {
            $professions = [];

            foreach ($data['data']['cust_profession'] as $key => $value) {
                $professions[] = (int) $value;
            }

            $customer_preferences = $customer_preferences->whereIn('profession_id', $data['data']['cust_profession']);

            $search_options = Profession::whereIn('id', $professions)->get();

            $tmp_search = 'Profession: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['cust_speciality'] != null && $data['data']['cust_speciality'] != '') {
            $specialities = [];

            foreach ($data['data']['cust_speciality'] as $key => $value) {
                $specialities[] = (int) $value;
            }

            $customer_preferences = $customer_preferences->whereIn('speciality_id', $data['data']['cust_speciality']);

            $search_options = Speciality::whereIn('id', $specialities)->get();

            $tmp_search = 'Speciality: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['cust_customer']) && ($data['data']['cust_customer'] != '')) {
            $customers = [];

            foreach ($data['data']['cust_customer'] as $key => $value) {
                $customers[] = (int) $value;
            }

            $customer_preferences = $customer_preferences->whereIn('customer_id', $customers);

            $search_options = Customer::whereIn('id', $customers)->get();

            $tmp_search = 'Customers: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->customer_name;
                } else {
                    $tmp_search .= $search_option->customer_name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['cust_skills']) && ($data['data']['cust_skills'] != '')) {
            $selected_values = [];

            foreach ($data['data']['cust_skills'] as $key => $value) {
                $selected_values[] = (int) $value;
            }

            $tmp_customer_preferences = $customer_preferences->get();

            $skills_ids = [];
            foreach ($tmp_customer_preferences as $tmp_customer_preference) {
                if ($tmp_customer_preference->skill_id != '') {
                    $tmp_values = explode('|', $tmp_customer_preference->skill_id);
                    foreach ($tmp_values as $tmp_value) {
                        if (in_array($tmp_value, $selected_values)) {
                            $skills_ids[] = $tmp_customer_preference->id;
                        }
                    }
                }
            }

            $customer_preferences = $customer_preferences->whereIn('id', $skills_ids);

            $search_options = ScoutingRole::whereIn('id', $selected_values)->get();

            $tmp_search = 'Skills: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['cust_credit_check'] != null && $data['data']['cust_credit_check'] != '') {
            $customer_preferences = $customer_preferences->where('credit_check', $data['data']['cust_credit_check']);

            $tmp_search = 'Credit Check: '.($data['data']['cust_credit_check'] == 1 ? 'Yes' : 'No');

            $searched_for[] = $tmp_search;
        }

        if ($data['data']['cust_criminal_record'] != null && $data['data']['cust_criminal_record'] != '') {
            $customer_preferences = $customer_preferences->where('criminal_record', $data['data']['cust_criminal_record']);

            $tmp_search = 'Criminal Record: '.($data['data']['cust_criminal_record'] == 1 ? 'Yes' : 'No');

            $searched_for[] = $tmp_search;
        }

        $customer_preferences_result = [];

        $customer_preferences_result['customer_preferences'] = $customer_preferences->get();
        $customer_preferences_result['searched_for'] = $searched_for;

        return $customer_preferences_result;
    }

    public function getCanditates(Request $request): JsonResponse
    {
        $data = $request->all();

        $candidates = $this->getCandidatesData($data);

        return response()->json(['message' => 'Success', 'cv' => $candidates['cv'], 'searched_for' => $candidates['searched_for'], 'post_data' => $data]);
    }

    public function getCandidatesData($data)
    {
        $cv = Cv::with('user', 'skill', 'qualification', 'availability', 'resourcetype', 'role')->whereHas('user');

        $searched_for = [];

        $cv->orderBy('id');

        //Check if $data is being passed through correctly
        /*if(isset($data['data']['p'])){
            foreach ($data['data']['p'] as $asd){
                //die();
            }
        }*/

        if (isset($data['data']['q']) && ($data['data']['q'] != '')) {
            $cv = $cv->whereHas('user', function ($query) use ($data) {
                $query->where('first_name', 'like', '%'.$data['data']['q'].'%');
                $query->orWhere('last_name', 'like', '%'.$data['data']['q'].'%');
            })
                ->orWhereHas('status', function ($query) use ($data) {
                    $query->where('description', 'like', '%'.$data['data']['q'].'%');
                });

            $searched_for[] = 'Search: '.$data['data']['q'];
        }

        $cv = $cv->get();

        if (isset($data['data']['a']) && ($data['data']['a'] != '')) {
            $availab = [];
            foreach ($data['data']['a'] as $key => $value) {
                $availab[] = (int) $value;
            }

            $availability = Availability::whereIn('avail_status', $availab)
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $availability_statuses = AvailStatus::whereIn('id', $availab)->get();

            $tmp_search = 'Availability: ';
            $not_empty_flag = false;
            foreach ($availability_statuses as $availability_status) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$availability_status->description;
                } else {
                    $tmp_search .= $availability_status->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;

            $cv = $cv->filter(function ($cv) use ($availability) {
                return in_array($cv->user_id, $availability);
            });
        }

        if (isset($data['data']['i']) && ($data['data']['i'] != '')) {
            $industries = [];

            foreach ($data['data']['i'] as $key => $value) {
                $industries[] = (int) $value;
            }

            $industry = IndustryExperience::whereIn('ind_id', $industries)
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($industry) {
                return in_array($cv->user_id, $industry);
            });

            $search_options = Industry::whereIn('id', $industries)->get();

            $tmp_search = 'Industry: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['skl']) && ($data['data']['skl'] != '')) {
            $skills = [];
            foreach ($data['data']['skl'] as $key => $value) {
                $skills[] = (int) $value;
            }

            $skill = Skill::whereIN('skill_level', $skills)
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($skill) {
                return in_array($cv->user_id, $skill);
            });

            $search_options = SkillLevel::whereIn('id', $skills)->get();

            $tmp_search = 'Skill Level: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['bbe']) && ($data['data']['bbe'] != '')) {
            $bbbee_race = [];
            foreach ($data['data']['bbe'] as $key => $value) {
                $bbbee_race[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($bbbee_race) {
                return in_array($cv->bbbee_race, $bbbee_race);
            });

            $search_options = BBBEERace::whereIn('id', $bbbee_race)->get();

            $tmp_search = 'BBE Race: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['d']) && ($data['data']['d'] != '')) {
            $disability = [];

            foreach ($data['data']['d'] as $key => $value) {
                $disability[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($disability) {
                return in_array($cv->disability, $disability);
            });

            $search_options = Disability::whereIn('id', $disability)->get();

            $tmp_search = 'Disability: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['p']) && ($data['data']['p'] != '')) {
            $profession = [];
            foreach ($data['data']['p'] as $key => $value) {
                $profession[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($profession) {
                return in_array($cv->profession_id, $profession);
            });

            $search_options = Profession::whereIn('id', $profession)->get();

            $tmp_search = 'Profession: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['sp']) && ($data['data']['sp'] != '')) {
            $speciality = [];
            foreach ($data['data']['sp'] as $key => $value) {
                $speciality[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($speciality) {
                return in_array($cv->speciality_id, $speciality);
            });

            $search_options = speciality::whereIn('id', $speciality)->get();

            $tmp_search = 'Speciality: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['rm']) && ($data['data']['rm'] != '')) {
            $hourly_rate_mimimum = $data['data']['rm'];

            $cv = $cv->filter(function ($cv) use ($hourly_rate_mimimum) {
                $hourly_rate = isset($cv->hourly_rate_minimum) && $cv->hourly_rate_minimum > 0 ? $cv->hourly_rate_minimum : 0; //Set minimum to 0 if not set

                return (int) $hourly_rate_mimimum >= $hourly_rate;
            });
        }

        if (isset($data['data']['rx']) && ($data['data']['rx'] != '')) {
            $hourly_rate_maximum = $data['data']['rx'];

            $cv = $cv->filter(function ($cv) use ($hourly_rate_maximum) {
                $hourly_rate = isset($cv->hourly_rate_maximum) && $cv->hourly_rate_maximum > 0 ? $cv->hourly_rate_minimum : 9000000; //Set Maximum to billion if not set

                return $hourly_rate_maximum <= $hourly_rate;
            });
        }

        if (isset($data['data']['sm']) && ($data['data']['sm'] != '')) {
            $monthly_salary_minimum = $data['data']['sm'];

            $cv = $cv->filter(function ($cv) use ($monthly_salary_minimum) {
                $monthly_salary = isset($cv->monthly_salary_minimum) && $cv->monthly_salary_minimum > 0 ? $cv->monthly_salary_minimum : 0; //Set Maximum to billion if not set

                return $monthly_salary >= $monthly_salary_minimum;
            });
        }

        if (isset($data['data']['sx']) && ($data['data']['sx'] != '')) {
            $monthly_salary_maximum = $data['data']['sx'];

            $cv = $cv->filter(function ($cv) use ($monthly_salary_maximum) {
                $monthly_salary = isset($cv->monthly_salary_maximum) && $cv->monthly_salary_maximum > 0 ? $cv->monthly_salary_maximum : 90000000; //Set Maximum to billion if not set

                return $monthly_salary <= $monthly_salary_maximum;
            });
        }

        if (isset($data['data']['qf']) && ($data['data']['qf'] != '')) {
            $qualification = Qualification::where('qualification', 'like', '%'.$request->input('qf').'%')
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $qf = $data['data']['qf'];

            $cv = $cv->filter(function ($cv) use ($qualification, $qf) {
                if (in_array($cv->user_id, $qualification)) {
                    return in_array($cv->user_id, $qualification);
                }

                $string = strtolower($cv->main_qualification);
                $sub_string = strtolower($qf);
                $flag = false;
                if (strpos($string, $sub_string) !== false) {
                    $flag = true;
                }

                return $flag;
            });

            $searched_for[] = 'Qualification: '.$data['data']['qf'];
        }

        if (isset($data['data']['rt']) && ($data['data']['rt'] != '')) {
            $rt = $data['data']['rt'];

            $cv = $cv->filter(function ($cv) use ($rt) {
                $user = User::find($cv->user_id);
                if (isset($user)) {
                    $resource = Resource::where('user_id', '=', $user->id)->first();
                    if (isset($resource)) {
                        return ($resource->resource_type > 0) && in_array($resource->resource_type, $rt);
                    }
                }
            });

            $search_options = ResourceType::whereIn('id', $rt)->get();

            $tmp_search = 'Resource Type: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['po']) && ($data['data']['po'] != '')) {
            $placement_options = $data['data']['po'];

            $cv = $cv->filter(function ($cv) use ($placement_options) {
                if ($cv->is_permanent == 1) {
                    return in_array(1, $placement_options);
                }

                if ($cv->is_temporary == 1) {
                    return in_array(2, $placement_options);
                }

                if ($cv->is_contract == 1) {
                    return in_array(3, $placement_options);
                }
            });

            $tmp_search = 'Placement Options: ';
            if (in_array(1, $placement_options)) {
                $tmp_search .= 'Permanent, ';
            }

            if (in_array(2, $placement_options)) {
                $tmp_search .= 'Temporary, ';
            }

            if (in_array(3, $placement_options)) {
                $tmp_search .= 'Contract';
            }

            $tmp_search = trim($tmp_search);

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['role']) && ($data['data']['role'] != '')) {
            $roles = [];

            foreach ($data['data']['role'] as $key => $value) {
                $roles[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($roles) {
                return in_array($cv->role_id, $roles);
            });

            $search_options = ScoutingRole::whereIn('id', $roles)->get();

            $tmp_search = 'Main Role: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['n']) && ($data['data']['n'] != '')) {
            $na = $data['data']['n'];

            $cv = $cv->filter(function ($cv) use ($na) {
                $string = strtolower($cv->nationality);
                $sub_string = strtolower($na);
                $flag = false;
                if (strpos($string, $sub_string) !== false) {
                    $flag = true;
                }

                return $flag;
            });

            $tmp_search = 'Nationality: '.$data['data']['n'];

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['ss']) && ($data['data']['ss'] != '')) {
            $skills = $data['data']['ss'];

            $cv = $cv->filter(function ($cv) use ($skills) {
                foreach ($cv->skill as $skill) {
                    return in_array($skill->tool_id, $skills);
                }
            });

            $search_options = System::whereIn('id', $skills)->get();

            $tmp_search = 'Skill: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if (isset($data['data']['e']) && ($data['data']['e'] != '')) {
            $experience = Experience::where('tools', 'like', '%'.$data['data']['e'].'%')
                ->orWhere('company', 'like', '%'.$data['data']['e'].'%')
                ->orWhere('current_project', 'like', '%'.$data['data']['e'].'%')
                ->orWhere('role', 'like', '%'.$data['data']['e'].'%')
                ->orWhere('project', 'like', '%'.$data['data']['e'].'%')
                ->orWhere('responsibility', 'like', '%'.$data['data']['e'].'%')
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($experience) {
                return in_array($cv->user_id, $experience);
            });
        }

        $months_experince_drop_down[''] = 'All';
        for ($i = 6; $i < 244; $i += 6) {
            $months_experince_drop_down[$i] = '> '.$i.' months';
        }

        $rate_drop_down_tmp = [-1, 100, 200, 400, 500, 600, 700, 800, 900, 1000];
        foreach ($rate_drop_down_tmp as $key => $value) {
            $rate_drop_down[$value] = '< '.$value;
        }

        $rate_drop_down[-1] = 'All';

        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');

        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        if (isset($data['data']['p']) && $data['data']['p'] > 0) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->whereIn('profession_id', $data['data']['p'])->where('name', '!=', '')->pluck('name', 'id');
        }
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->pluck('description', 'id');

        $hourly_rate_drop_down = ['' => ''];
        for ($i = 10; $i <= 10000; $i += 10) {
            $hourly_rate_drop_down[$i] = $i;
        }

        $money_drop_down = ['' => ''];
        for ($i = 500; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }

        $candidates = [];

        $candidates['cv'] = $cv;
        $candidates['searched_for'] = $searched_for;

        return $candidates;
    }

    public function dropzoneUpload($id, Request $request): JsonResponse
    {
        $true = false;
        $file_name = '';
        if ($request->hasFile('file')) {
            $true = true;
            try {
                $file = $request->file('file');
                $file_name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$file->getClientOriginalExtension();

                if (strtolower($file->getClientOriginalExtension()) !== 'html') {
                    return response()->json(['success' => '1', 'message' => 'File could not be uploaded, please select an html template to upload.', 'request' => $request->all(), 'true' => $true, 'file_name' => $file_name]);
                }

                $stored = $file->storeAs('distributionlist', $file_name);

                $distributionListConfig = DistributionListConfig::find(3);

                $contents = Storage::get($stored);

                if (isset($distributionListConfig)) {
                    $distributionListConfig->name = 'Uploaded Html Document - '.date('Y_m_d_H_i_s');
                    $distributionListConfig->from = 'info@blackboardbs.com';
                    $distributionListConfig->subject = 'Subject';
                    $distributionListConfig->updated_at = now();
                    $distributionListConfig->status_id = 1;
                    $distributionListConfig->email_body = $contents;
                    $distributionListConfig->save();
                } else {
                    $distributionListConfig = new DistributionListConfig();
                    $distributionListConfig->id = 3;
                    $distributionListConfig->name = 'Uploaded Html Document - '.date('Y_m_d_H_i_s');
                    $distributionListConfig->from = 'info@blackboardbs.com';
                    $distributionListConfig->subject = 'Subject';
                    $distributionListConfig->created_at = now();
                    $distributionListConfig->status_id = 1;
                    $distributionListConfig->creator_id = 1;
                    $distributionListConfig->email_body = $contents;
                    $distributionListConfig->save();
                }

                /*Storage::copy('app/distributionlist/'.$file_name, 'public/html/'.$file_name);
                Storage::copy('app/distributionlist/'.$file_name, 'html/'.$file_name);
                Storage::copy('app/distributionlist/'.$file_name, $file_name);*/
            } catch (\Exception $e) {
                $exception_message = 'Content file could not be uploaded.';
            }
        }

        return response()->json(['success' => '1', 'message' => 'File uploaded successfully.', 'request' => $request->all(), 'true' => $true, 'file_name' => $file_name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DistributionList  $distributionList
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module = Module::where('name', '=', \App\Models\DistributionList::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            if (! Auth::user()->canAccess($module->id, 'update_all') && Auth::user()->canAccess($module->id, 'update_team')) {
                //
            }
        } else {
            return abort(403);
        }

        DistributionList::destroy($id);

        return redirect(route('distributionlist.index'))->with('flash_success', 'Distribution list deleted successfully.');
    }
}
