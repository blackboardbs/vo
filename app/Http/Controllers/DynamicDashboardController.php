<?php

namespace App\Http\Controllers;

use App\Models\DynamicDashboard;
use App\Http\Requests\DynamicDashboardRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Redirect;

class DynamicDashboardController extends Controller
{
    public function index(Request $request): View
    {
        // $dynamic_dashboard = DynamicDashboard::where('status_id', 1);

        $item = $request->input('r') ?? 15;
        $dynamic_dashboard = DynamicDashboard::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $dynamic_dashboard = $dynamic_dashboard->where('status_id', $request->input('status_filter'));
            }else{
                $dynamic_dashboard = $dynamic_dashboard->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $dynamic_dashboard = $dynamic_dashboard->where('description', 'like', '%'.$request->input('q').'%');
        }

        $dynamic_dashboard = $dynamic_dashboard->paginate($item);

        $parameters = [
            'dynamic_dashboard' => $dynamic_dashboard
        ];
        return view("master_data.dynamic_dashboard.index")->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = DynamicDashboard::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $status_drop_down = Status::where('status', 1)->pluck('description', 'id');
        $parameters = [
            'status_drop_down' => $status_drop_down,
            'roles_drop_down' => Role::pluck('display_name', 'id'),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return view('master_data.dynamic_dashboard.create')->with($parameters);
    }

    public function store(DynamicDashboardRequest $request): RedirectResponse
    {
        $roles = '';
        if ($request->has('roles')){
            foreach ($request->roles as $key => $role){
                $roles .= ($key == count($request->roles) - 1)?$role:$role.'|';
            }
        }
        $dynamic_dashboard = new DynamicDashboard();
        $dynamic_dashboard->description = $request->description;
        $dynamic_dashboard->roles = $roles;
        $dynamic_dashboard->top_component = $request->top_component;
        $dynamic_dashboard->status_id = $request->status_id;
        $dynamic_dashboard->save();

        return Redirect::to('/master/customise_dashboard');
        // return redirect()->back()->with('flash_success', 'Your Component was captured successfully');
    }

    public function show($id): View
    {
        $dynamic_dashboard = DynamicDashboard::find($id);
        $roles = [];
        foreach (explode('|', $dynamic_dashboard->roles) as $role){
            array_push($roles, Role::find($role)->display_name);
        }
        $parameters = [
            'dynamic_dashboard' => $dynamic_dashboard,
            'roles' => $roles
        ];
        return view('master_data.dynamic_dashboard.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = DynamicDashboard::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $dynamic_dashboard = DynamicDashboard::find($id);
        $status_drop_down = Status::where('status', 1)->pluck('description', 'id');
        $parameters = [
            'status_drop_down' => $status_drop_down,
            'roles_drop_down' => Role::pluck('display_name', 'id'),
            'dynamic_dashboard' => $dynamic_dashboard,
            'autocomplete_elements' => $autocomplete_elements
        ];

        return view('master_data.dynamic_dashboard.edit')->with($parameters);
    }

    public function update(DynamicDashboardRequest $request, $id): RedirectResponse
    {
        $roles = '';
        if ($request->has('roles')){
            foreach ($request->roles as $key => $role){
                $roles .= ($key == count($request->roles) - 1)?$role:$role.'|';
            }
        }
        $dynamic_dashboard = DynamicDashboard::find($id);
        $dynamic_dashboard->description = $request->description;
        $dynamic_dashboard->roles = $roles;
        $dynamic_dashboard->top_component = $request->top_component;
        $dynamic_dashboard->status_id = $request->status_id;
        $dynamic_dashboard->save();

        return redirect()->back()->with('flash_success', 'Component has been edited successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // DynamicDashboard::destroy($id);
        $item = DynamicDashboard::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->back()->with('flash_success', 'Dashboard Component suspended successfully');
    }
}
