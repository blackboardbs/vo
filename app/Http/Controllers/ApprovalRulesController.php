<?php

namespace App\Http\Controllers;

use App\Models\ApprovalRules;
use App\Models\Assignment;
use App\Http\Requests\ApprovalRulesRequest;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;

class ApprovalRulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $approver_rules = ApprovalRules::with(['approver' => function ($approver) {
            $approver->select(['id', 'first_name', 'last_name']);
        }, 'substituteApprover' => function ($substitute) {
            $substitute->select(['id', 'first_name', 'last_name']);
        }, 'status' => function ($status) {
            $status->select(['id', 'description']);
        }])->latest()->paginate($item);

        $parameters = [
            'can_create' => true,
            'can_update' => true,
            'approver_rules' => $approver_rules,
        ];

        return view('approval_rules.index')->with($parameters);
    }

    public function create(): View
    {
        $parameters = [
            'approvers' => $this->aprrovers(),
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ];

        return view('approval_rules.create')->with($parameters);
    }

    public function store(ApprovalRulesRequest $request, ApprovalRules $rules): RedirectResponse
    {
        $rules->approver_id = $request->approver_id;
        $rules->substitute_approver = $request->substitute_approver;
        $rules->substitute_approver_id = $request->substitute_approver_id??0;
        $rules->date_from = $request->date_from;
        $rules->date_to = $request->date_to;
        $rules->status_id = $request->status_id;
        $rules->save();

        return redirect()->route('approvalroles.index')->with('flash_success', 'Approval rule created successfully');
    }

    public function show(ApprovalRules $approvalrole): View
    {
        return view('approval_rules.show')->with(['approvalrole' => $approvalrole->load(['approver' => function ($approver) {
            $approver->select(['id', 'first_name', 'last_name']);
        }, 'substituteApprover' => function ($substitute) {
            $substitute->select(['id', 'first_name', 'last_name']);
        }, 'status' => function ($status) {
            $status->select(['id', 'description']);
        }])]);
    }

    public function edit(ApprovalRules $approvalrole): View
    {
        $parameters = [
            'approvers' => $this->aprrovers(),
            'approvalrole' => $approvalrole,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ];

        return view('approval_rules.edit')->with($parameters);
    }

    public function update(ApprovalRulesRequest $request, ApprovalRules $approvalrole): RedirectResponse
    {
        $approvalrole->approver_id = $request->approver_id;
        $approvalrole->substitute_approver = $request->substitute_approver;
        $approvalrole->substitute_approver_id = $request->substitute_approver_id??0;
        $approvalrole->date_from = $request->date_from;
        $approvalrole->date_to = $request->date_to;
        $approvalrole->status_id = $request->status_id;
        $approvalrole->save();

        return redirect()->back()->with('flash_success', 'Approval role was updated successfully');
    }

    public function destroy(ApprovalRules $approvalrole): RedirectResponse
    {
        $approvalrole->delete();

        return redirect()->back()->with('flash_success', 'Approval role deleted successfully');
    }

    private function aprrovers()
    {
        $assignment_approvers = Assignment::with(['project' => function ($project) {
            $project->select(['id', 'manager_id']);
        }])->select([
            'product_owner_id', 'project_manager_new_id', 'line_manager_id', 'assignment_approver', 'resource_manager_id', 'project_id', ])
            ->get()->map(function ($assignment) {
                $ids = [];

                if (isset($assignment->project->manager_id)) {
                    array_push($ids, $assignment->project->manager_id);
                }

                if ($assignment->product_owner_id != 0 && $assignment->product_owner_id != null) {
                    array_push($ids, $assignment->product_owner_id);
                }

                if ($assignment->project_manager_new_id != 0 && $assignment->project_manager_new_id != null) {
                    array_push($ids, $assignment->project_manager_new_id);
                }

                if ($assignment->line_manager_id != 0 && $assignment->line_manager_id != null) {
                    array_push($ids, $assignment->line_manager_id);
                }

                if ($assignment->assignment_approver != 0 && $assignment->assignment_approver != null) {
                    array_push($ids, $assignment->assignment_approver);
                }

                if ($assignment->resource_manager_id != 0 && $assignment->resource_manager_id != null) {
                    array_push($ids, $assignment->resource_manager_id);
                }

                return $ids;
            })->flatten()->unique()->values()->toArray();

        return User::whereIn('id', $assignment_approvers)
            ->selectRaw("id, CONCAT(first_name, ' ', last_name) AS full_name")
            ->pluck('full_name', 'id');
    }
}
