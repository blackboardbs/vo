<?php

namespace App\Http\Controllers;

use App\Exports\GeneralExport;
use App\Exports\QuotationsExport;
use App\Http\Requests\StoreProspectRequest;
use App\Http\Requests\UpdateProspectRequest;
use App\Models\Module;
use App\Models\Prospect;
use App\Models\System;
use App\Models\Customer;
use App\Models\InvoiceContact;
use App\Models\ProspectStatus;
use App\Models\Status;
use App\Models\Config;
use App\Models\Document;
use App\Models\User;
use App\Models\Lead;
use App\Models\Industry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ProspectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        $config = Config::first();
        $prospects = Prospect::selectRaw('id,prospect_name,account_manager_id,status_id,decision_date,chance,solution,value,scope,hours,resources,converted_date,est_start_date,partners,note,contact,customer_id,company_id,duration,margin,industry_id,lead_from,emote_id,0 as canEdit');

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
            $prospects = $prospects->whereIn('creator_id', Auth::user()->team());
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        if ($request->has('am') && $request->input('am') != '') {
            $prospects = $prospects->where('account_manager_id', '=', $request->input('am'));
        }

        if ($request->has('s') && $request->input('s') != '' && $request->input('s') != '0') {
            $prospects = $prospects->where('status_id', '=', $request->input('s'));
        }

        if ($request->has('s') && $request->input('s') == '0') {
            $prospects = $prospects;
        } else {
            $prospects =  $prospects->whereIn('status_id',[1,2,3,4,5]);
        }

        if ($request->has('ds') && $request->input('ds') != '') {
            // dd($request->input('ds'));
            $prospects = $prospects->where('decision_date', '>=', $request->input('ds'));
        }

        if ($request->has('de') && $request->input('de') != '') {
            $prospects = $prospects->where('decision_date', '<=', $request->input('de'));
        }

        if ($request->has('q') && $request->input('q') != '') {
            $prospects = $prospects->where(fn($item) => $item->where('prospect_name', 'LIKE', '%'.$request->input('q').'%')
            ->orWhere('contact', 'like', '%'.$request->input('q').'%')
            ->orWhere('partners', 'like', '%'.$request->input('q').'%')
            ->orWhere('solution', 'like', '%'.$request->input('q').'%')
            ->orWhere('scope', 'like', '%'.$request->input('q').'%')
            ->orWhere('decision_date', 'like', '%'.$request->input('q').'%')
            ->orWhere('value', 'like', '%'.$request->input('q').'%')
            ->orWhere('chance', 'like', '%'.$request->input('q').'%'));
        }

        $prospects = $prospects->get();

        $arr = [];
        foreach($prospects as $id){
            array_push($arr,$id->account_manager_id);
        }
        
        $account_manager_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id',$arr)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $status_drop_down = ProspectStatus::orderBy('id')->pluck('description', 'id');
        $customers = Customer::orderBy('customer_name')->get();
        $contacts = InvoiceContact::orderBy('first_name')->orderBy('last_name')->get();
        $systems = System::orderBy('description')->get();
        $leads = Lead::orderBy('name')->get();
        $industries = Industry::orderBy('description')->get();

        $parameters = [
            'prospects' => $prospects,
            'customers' => $customers,
            'contacts' => $contacts,
            'systems' => $systems,
            'leads' => $leads,
            'industries' => $industries,
            'status_drop_down' => $status_drop_down,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'account_manager_drop_down' => $account_manager_drop_down
        ];

        if ($request->has('export')) return $this->export($parameters, 'prospects');

        return View('prospects.index', $parameters);
    }

    public function create()
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team'), 403);

        $account_manager_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please select...', '0');

        $parameters = [
            'sidebar_process_statuses' => ProspectStatus::orderBy('id')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'account_manager_drop_down' => $account_manager_drop_down,
        ];

        return View('prospects.create', $parameters);
    }

    public function store(StoreProspectRequest $request)
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $prospect = new Prospect;
        $prospect->prospect_name = $request->input('prospect_name');
        $prospect->solution = $request->input('solution');
        $prospect->scope = $request->input('scope');
        $prospect->hours = $request->input('hours');
        $prospect->value = $request->input('value');
        $prospect->resources = $request->input('resources');
        $prospect->decision_date = $request->input('decision_date');
        $prospect->converted_date = $request->input('converted_date');
        $prospect->est_start_date = $request->input('est_start_date');
        $prospect->chance = $request->input('chance');
        $prospect->partners = $request->input('partners');
        $prospect->note = $request->input('note');
        $prospect->contact = $request->input('contact');
        $prospect->customer_id = $request->input('customer_id');
        $prospect->status_id = $request->input('status_id');
        $prospect->company_id = $request->input('company_id');
        $prospect->account_manager_id = $request->input('account_manager_id');
        $prospect->creator_id = auth()->id();
        $prospect->duration = $request->input('duration');
        $prospect->margin = $request->input('margin');
        $prospect->industry_id = $request->input('industry_id');
        $prospect->lead_from = $request->input('lead_from');
        $prospect->save();

        if ($request->hasFile('new_file')) {
            if (isset($request->new_file) && $request->new_file != null) {
                $file = 'task_'.date('Y_m_d_H_i_s').'.'.$request->new_file->getClientOriginalExtension();
                $fileName = $request->new_file->getClientOriginalName();
                $request->new_file->move(public_path('files/tasks'), $file);

                $document = new Document();
                $document->name = $fileName;
                $document->type_id = 11;
                $document->file = $file;
                $document->document_type_id = 11;
                $document->reference = 'Prospect';
                $document->reference_id = $prospect->id;
                $document->owner_id = auth()->id();
                $document->creator_id = auth()->id();
                $document->status_id = 1;
                $document->save();
            }
        }

        if (\request()->ajax()){
            return response()->json(['prospect' => $prospect]);
        }

        return redirect(route('prospects.index'))->with('flash_success', 'Prospect captured successfully');
    }

    public function show($prospectid)
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        $parameters = [
            'prospect' => Prospect::where('id', '=', $prospectid)->get(),
        ];

        return View('prospects.show')->with($parameters);
    }

    public function edit($prospectid)
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $account_manager_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please select...', '0');

        $parameters = [
            'prospect' => Prospect::where('id', '=', $prospectid)->get(),
            'sidebar_process_statuses' => ProspectStatus::orderBy('id')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'account_manager_drop_down' => $account_manager_drop_down,
        ];

        return View('prospects.edit')->with($parameters);
    }

    public function update(UpdateProspectRequest $request, $prospectid)
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $prospect = Prospect::find($prospectid);
        $prospect->prospect_name = $request->input('prospect_name');
        $prospect->solution = $request->input('solution');
        $prospect->scope = $request->input('scope');
        $prospect->hours = $request->input('hours');
        $prospect->value = $request->input('value');
        $prospect->resources = $request->input('resources');
        $prospect->decision_date = $request->input('decision_date');
        $prospect->converted_date = $request->input('converted_date');
        $prospect->est_start_date = $request->input('est_start_date');
        $prospect->chance = $request->input('chance');
        $prospect->partners = $request->input('partners');
        $prospect->note = $request->input('note');
        $prospect->contact = $request->input('contact');
        $prospect->customer_id = $request->input('customer_id');
        $prospect->status_id = $request->input('status_id');
        $prospect->company_id = $request->input('company_id');
        $prospect->account_manager_id = $request->input('account_manager_id');
        $prospect->creator_id = auth()->id();
        $prospect->duration = $request->input('duration');
        $prospect->margin = $request->input('margin');
        $prospect->industry_id = $request->input('industry_id');
        $prospect->lead_from = $request->input('lead_from');
        $prospect->created_at = $request->input('created_at');
        $prospect->save();

        if (\request()->ajax()){
            return response()->json(['prospect' => $prospect]);
        }

        return redirect(route('prospects.index'))->with('flash_success', 'Prospect saved successfully');
    }

    public function destroy($id)
    {
        $module = Module::where('name', '=', 'App\Models\Prospects')->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        DB::table('prospect')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Prospect::destroy($id);

        return redirect()->route('prospects.index')->with('flash_success', 'Prospect deleted successfully');
    }

    public function copy($prospectid)
    {
        $old = Prospect::find($prospectid);

        $new = new Prospect();
        $new->prospect_name = $old->prospect_name.' - copy';
        $new->solution = $old->solution;
        $new->scope = $old->scope;
        $new->hours = $old->hours;
        $new->value = $old->value;
        $new->resources = $old->resources;
        $new->decision_date = $old->decision_date;
        $new->converted_date = $old->converted_date;
        $new->est_start_date = $old->est_start_date;
        $new->chance = $old->chance;
        $new->partners = $old->partners;
        $new->note = $old->note;
        $new->contact = $old->contact;
        $new->customer_id = $old->customer_id;
        $new->status_id = $old->status_id;
        $new->creator_id = $old->creator_id;
        $new->company_id = $old->company_id;
        $new->account_manager_id = $old->account_manager_id;
        $new->duration = $old->duration;
        $new->margin = $old->margin;
        $new->industry_id = $old->industry_id;
        $new->file = $old->file;
        $new->lead_from = $old->lead_from;
        $new->emote_id = $old->emote_id;
        $new->save();
        
        return response()->json(['success' => 'Prospect successfully copied.']);
    }

    public function overview(){
        
        $new_deals = $this->NewDeals();
        $open_deals = $this->OpenDeals();
        $no_action = $this->NoAction();
        $action = $this->Action();
        $total = $this->PotentialValue();
        
        return response()->json([
            'newDeals' => $new_deals,
            'openDeals' => $open_deals,
            'noAction' => $no_action,
            'action' => $action,
            'total' =>$total,
            'probability' => $this->Probability(),
            'prospect' => $this->Prospect(),
            'decision' => $this->Decision(),
            'start' => $this->StartDate(),
            'account' => $this->AccountManager()
        ]);
    }

    public function NewDeals(){
        $query = DB::select('SELECT account_manager_id, count(id) as `count` FROM `prospect` WHERE week(`created_at`) = week(now()) AND year(`created_at`) = year(now()) AND `status_id` not in (6,7) GROUP BY account_manager_id');


        return array_sum(array_column($query,'count'));
    }

    public function OpenDeals(){
        $query = DB::select('SELECT `account_manager_id`, count(id) AS `count` FROM `prospect` WHERE `status_id` not in (6,7) GROUP BY `account_manager_id`');

        return array_sum(array_column($query,'count'));
    }

    public function NoAction(){
        $query = DB::select("SELECT p.`account_manager_id`, count(p.`id`) AS `count` FROM `prospect` p left outer join `comments` c on (p.id = c.commentable_id AND c.commentable_type = 'App\Models\Prospect')
        WHERE ( CASE WHEN (week(p.`updated_at`) = week(now()) AND year(p.`updated_at`) = year(now())) THEN 1 ELSE 0 END = 0
            AND CASE WHEN (week(c.`updated_at`) = week(now()) AND year(c.`updated_at`) = year(now())) THEN 1 ELSE 0 END = 0
               ) AND p.`status_id` not in (6,7)
        GROUP BY p.`account_manager_id`");

        return array_sum(array_column($query,'count'));
    }

    public function Action(){
        $query = DB::select("SELECT p.account_manager_id, count(p.id) FROM `prospect` p left outer join `comments` c on (p.id = c.commentable_id AND c.commentable_type = 'App\Models\Prospect')
        WHERE 
            (week(p.`updated_at`) = week(now()) AND year(p.`updated_at`) = year(now()))
            OR 
            (week(c.`updated_at`) = week(now()) AND year(c.`updated_at`) = year(now()))
        group by  p.account_manager_id");

        return array_sum(array_column($query,'count'));
    }

    public function PotentialValue(){
        $query = DB::select("SELECT `account_manager_id`, sum(`value` * `chance`) AS potential_value FROM `prospect` WHERE `status_id` not in (6,7) GROUP BY `account_manager_id`");

        return array_sum(array_column($query,'potential_value'));
    }

    public function Probability(){
        $query = DB::select("SELECT 
        `account_manager_id`, 
        CASE 
            WHEN `chance` <= 25 THEN '0%' 
            WHEN `chance` <= 50 THEN '25%' 
            WHEN `chance` <= 75 THEN '50%' 
            ELSE '75%' END AS from_range,
        CASE 
            WHEN `chance` <= 25 THEN '25%' 
            WHEN `chance` <= 50 THEN '50%' 
            WHEN `chance` <= 75 THEN '75%' 
            ELSE '100%' END AS to_range,    
        COUNT(id) AS cnt, 
        SUM(`value`) AS val 
    FROM `prospect` 
    WHERE `status_id` NOT IN (6,7)
    GROUP BY `account_manager_id`,CASE 
            WHEN `chance` <= 25 THEN '0%' 
            WHEN `chance` <= 50 THEN '25%' 
            WHEN `chance` <= 75 THEN '50%' 
            ELSE '75%' END,CASE 
            WHEN `chance` <= 25 THEN '25%' 
            WHEN `chance` <= 50 THEN '50%' 
            WHEN `chance` <= 75 THEN '75%' 
            ELSE '100%' END order by CASE 
            WHEN `chance` <= 25 THEN '0%' 
            WHEN `chance` <= 50 THEN '25%' 
            WHEN `chance` <= 75 THEN '50%' 
            ELSE '75%' END asc");

        $data = [];
        foreach($query as $result){
            if(isset($data[$result->from_range])){
                $data[$result->from_range]['cnt'] = $data[$result->from_range]['cnt'] + $result->cnt;
                $data[$result->from_range]['val'] = $data[$result->from_range]['val'] + $result->val;
            } else {
                $data[$result->from_range] = [
                    'from_range' => $result->from_range,
                    'to_range' => $result->to_range,
                    'cnt' => $result->cnt,
                    'val' => $result->val
                ];
            }
        }

        return $data;
    }

    public function Prospect(){
        $query = DB::select("SELECT 
        `account_manager_id`, 
        CASE 
            WHEN `value` <= 10000 THEN '0' 
            WHEN `value` <= 100000 THEN '10,001' 
            WHEN `value` <= 1000000 THEN '100,001' 
            ELSE '1,000,001' END AS from_range,
        CASE 
            WHEN `value` <= 10000 THEN '10,000' 
            WHEN `value` <= 100000 THEN '100,000' 
            WHEN `value` <= 1000000 THEN '1,000,000' 
            ELSE '10,000,000' END AS to_range,
        COUNT(id) AS cnt, 
        SUM(`value`) AS val 
    FROM `prospect` 
    WHERE `status_id` NOT IN (6,7)
    GROUP BY `account_manager_id`,CASE WHEN `value` <= 10000 THEN '0' WHEN `value` <= 100000 THEN '10,001' WHEN `value` <= 1000000 THEN '100,001' ELSE '1,000,001' END,CASE WHEN `value` <= 10000 THEN '10,000' WHEN `value` <= 100000 THEN '100,000' WHEN `value` <= 1000000 THEN '1,000,000' ELSE '10,000,000' END
    ORDER BY CASE 
            WHEN `value` <= 10000 THEN '0' 
            WHEN `value` <= 100000 THEN '10001' 
            WHEN `value` <= 1000000 THEN '100001' 
            ELSE '1000001' END DESC");

        $data = [];
        foreach($query as $result){
            if(isset($data[$result->from_range])){
                $data[$result->from_range]['cnt'] = $data[$result->from_range]['cnt'] + $result->cnt;
                $data[$result->from_range]['val'] = $data[$result->from_range]['val'] + $result->val;
            } else {
                $data[$result->from_range] = [
                    'from_range' => $result->from_range,
                    'to_range' => $result->to_range,
                    'cnt' => $result->cnt,
                    'val' => $result->val
                ];
            }
        }

        // krsort($data);

        return $data;
    }

    public function Decision(){
        $query = DB::select("SELECT 
        `account_manager_id`, 
        '<=' AS from_range,
        CASE 
            WHEN `decision_date` <= now() THEN 'Today'
            WHEN `decision_date` <= date_add(now(), INTERVAL 1 month) THEN '1 month'
            WHEN `decision_date` <= date_add(now(), INTERVAL 2 month) THEN '2 months'
            WHEN `decision_date` <= date_add(now(), INTERVAL 3 month) THEN '3 months'
            WHEN `decision_date` <= date_add(now(), INTERVAL 4 month) THEN '4 months'
            ELSE '5+ months' END AS to_range,
        count(id) AS cnt, 
        sum(`value`) AS val 
    FROM `prospect` 
    WHERE `status_id` not in (6,7)
    GROUP BY `account_manager_id`,CASE 
            WHEN `decision_date` <= now() THEN 'Today'
            WHEN `decision_date` <= date_add(now(), INTERVAL 1 month) THEN '1 month'
            WHEN `decision_date` <= date_add(now(), INTERVAL 2 month) THEN '2 months'
            WHEN `decision_date` <= date_add(now(), INTERVAL 3 month) THEN '3 months'
            WHEN `decision_date` <= date_add(now(), INTERVAL 4 month) THEN '4 months'
            ELSE '5+ months' END");

        $data = [];
        foreach($query as $result){
            if(isset($data[$result->to_range])){
                $data[$result->to_range]['cnt'] = $data[$result->to_range]['cnt'] + $result->cnt;
                $data[$result->to_range]['val'] = $data[$result->to_range]['val'] + $result->val;
            } else {
                $data[$result->to_range] = [
                    'from_range' => $result->from_range,
                    'to_range' => $result->to_range,
                    'cnt' => $result->cnt,
                    'val' => $result->val
                ];
            }
        }

        return $data;
    }

    public function StartDate(){
        $query = DB::select("SELECT 
        `account_manager_id`, 
        '<=' AS from_range,
        CASE 
            WHEN `est_start_date` <= now() THEN 'Today'
            WHEN `est_start_date` <= date_add(now(), INTERVAL 1 month) THEN '1 month'
            WHEN `est_start_date` <= date_add(now(), INTERVAL 2 month) THEN '2 months'
            WHEN `est_start_date` <= date_add(now(), INTERVAL 3 month) THEN '3 months'
            WHEN `est_start_date` <= date_add(now(), INTERVAL 4 month) THEN '4 months'
            ELSE '5+ months' END AS to_range,
        count(id) AS cnt, 
        sum(`value`) AS val 
    FROM `prospect` 
    WHERE `status_id` not in (6,7)
    GROUP BY `account_manager_id`,`est_start_date` order by to_range");

        $data = [];
        $today = [];
        $result = [];
        foreach($query as $result){
            if($result->to_range != 'Today'){
                if(isset($data[$result->to_range])){
                    $data[$result->to_range]['cnt'] = $data[$result->to_range]['cnt'] + $result->cnt;
                    $data[$result->to_range]['val'] = $data[$result->to_range]['val'] + $result->val;
                } else {
                    $data[$result->to_range] = [
                        'from_range' => $result->from_range,
                        'to_range' => $result->to_range,
                        'cnt' => $result->cnt,
                        'val' => $result->val
                    ];
                }
            } else {
                if(isset($today[$result->to_range])){
                    $today[$result->to_range]['cnt'] = $today[$result->to_range]['cnt'] + $result->cnt;
                    $today[$result->to_range]['val'] = $today[$result->to_range]['val'] + $result->val;
                } else {
                    $today[$result->to_range] = [
                        'from_range' => $result->from_range,
                        'to_range' => $result->to_range,
                        'cnt' => $result->cnt,
                        'val' => $result->val
                    ];
                }
            }
        }
        
        $result = array_merge($today,$data);
        // dd($data);

        return $result;
    }

    public function AccountManager(){
        $query = DB::select("SELECT 
        p.`account_manager_id`, 
        concat(u.first_name, ' ', u.last_name) AS account_manager,
        sum(CASE WHEN p.`status_id` = 1 THEN 1 ELSE 0 END) AS new,
        sum(CASE WHEN p.`status_id` in (2,3,4) THEN 1 ELSE 0 END) AS in_progress,
        sum(CASE WHEN p.`status_id` = 5 THEN 1 ELSE 0 END) AS on_hold,
        sum(CASE WHEN p.`status_id` = 6 THEN 1 ELSE 0 END) AS converted,
        (sum(CASE WHEN p.`status_id` = 6 THEN 1 ELSE 0 END) / sum(CASE WHEN p.`status_id` in (2,3,4,5,6) THEN 1 ELSE 0 END)) * 100 AS converted_perc,
        sum(CASE WHEN p.`status_id` in (2,3,4,5,6) THEN p.`value` ELSE 0 END) AS val
    FROM `prospect` p left outer join users u on (p.account_manager_id = u.id)
    WHERE p.`status_id` not in (7)
    GROUP BY p.`account_manager_id`");

        return $query;
    }

    public function kanban(Request $request){

        $config = Config::first(['prospect_columns']);
         $prospect_status = ProspectStatus::whereIn('id',explode(',',$config->prospect_columns))->orderBy('id')->get();

        $kanban = $prospect_status->map(function ($status) use ($request) {
            return $this->kanbanData($request, $status);
        });

        return response()->json([
           'status' =>  $prospect_status,
           'kanban' => $kanban
        ]);
    }

    private function kanbanData($request, $status)
    {
        $config = Config::first();
        $page = $request->page ?? 0;
        $items_per_page = $config->kanban_nr_of_tasks ?? 7;
        $offset = $request->page * $items_per_page;

        $tasks_count = Prospect::where('status_id', $status->id)->count();

        $tasks = Prospect::with(['discussion','discussion.user','account_manager' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        }]);

        if($request->has('account_manager_id') && $request->account_manager_id != ''){
            $tasks = $tasks->where('account_manager_id',$request->account_manager_id);
        }

        if($request->has('prospect_id') && $request->prospect_id != ''){
            $tasks = $tasks->where('id',$request->prospect_id);
        }

        if($request->has('customer_id') && $request->customer_id != ''){
            $tasks = $tasks->where('customer_id',$request->customer_id);
        }

        if($request->has('start_date') && $request->start_date != ''){
            $tasks = $tasks->where('decision_date','>=',$request->start_date);
        }
        
        if($request->has('end_date') && $request->end_date != ''){
            $tasks = $tasks->where('decision_date','<=',$request->end_date);
        }

        if($request->has('status_id') && $request->status_id != ''){
            $tasks = $tasks->where('status_id',$request->status_id);
        } else {
            $tasks = $tasks->where('status_id', $status->id);
        }

        if($request->has('solution_id') && $request->solution_id != ''){
            $tasks = $tasks->where('solution','like','%'.$request->solution_id.'%');
        }

        $tasks = $tasks->skip($items_per_page * $page)->take($items_per_page)->latest()->get();

        return [
            'id' => $status->id,
            'description' => $status->description,
            'prospects' => $tasks,
            'total_count' => $tasks_count,
            'load_more' => ($offset + $items_per_page) < $tasks_count,
            'page' => ++$page

        ];
    }

    public function setEmote($prospect_id, Request $request)
    {
        $prospect = Prospect::find($prospect_id);

        $prospect->emote_id = $request->emote_id;
        $prospect->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }

    public function loadMore(Request $request, ProspectStatus $status)
    {
        return response()->json(['kanban' => $this->kanbanData($request, $status)]);
    }
}
