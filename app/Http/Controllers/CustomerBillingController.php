<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\Time;
use App\Models\User;
use App\Models\V_Time;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CustomerBillingController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
            ->where('time_id', '>', 0)
            ->with('user', 'customer', 'project', 'invoice_status')
            ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
            ->paginate($item);

        if ($request->has('company') && $request->company != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->where('company_id', '=', $request->company)
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('team') && $request->team != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->whereHas('user.resource', function ($query) use ($request) {
                    $query->where('team_id', '=', $request->team);
                })
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('employee') && $request->employee != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->where('employee_id', '=', $request->employee)
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                //->with(['time'])
                ->where('time_id', '>', 0)
                ->whereHas('time', function ($query) use ($request) {
                    $query->where('year', '=', substr($request->yearmonth, 0, 4));
                    $query->where('month', '=', (substr($request->yearmonth, 4, 2) < 10) ? substr($request->yearmonth, 5, 1) : substr($request->yearmonth, 4, 2));
                })
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('dates') && $request->dates != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                //->with(['time'])
                ->where('time_id', '>', 0)
                ->whereHas('time', function ($query) use ($request) {
                    $query->where('start_date', '=', substr($request->dates, 0, 10));
                    $query->where('end_date', '=', substr($request->dates, 13, 10));
                })
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('customer') && $request->customer != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->where('customer_id', '=', $request->customer)
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        if ($request->has('project') && $request->project != null) {
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->where('project_id', '=', $request->project)
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }

        /*if ($request->has('billable') && $request->billable != null){
            $v_time = V_Time::select('time_id', 'employee_id', 'customer_id', 'project_id', 'yearwk', 'bill_status', 'invoice_date', 'due_date', 'rate', 'hours', 'expenses')
                ->where('time_id', '>', 0)
                ->where('billable', '=', $request->billable)
                ->with('user', 'customer', 'project', 'invoice_status')
                ->sortable(['customer_id' => 'desc', 'project_id' => 'desc', 'yearwk' => 'desc', 'employee_id' => 'desc'])
                ->paginate($item);
        }*/

        $total_value = 0;
        $total_expenses = 0;
        foreach ($v_time as $time) {
            $total_value += ($time->hours * $time->rate);
            $total_expenses += $time->expenses;
        }

        $calendar = Calendar::where('status', '=', 1)->whereDate('date', '<=', Carbon::now()->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get()->unique();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'dates_dropdown' => Time::select(DB::raw("CONCAT(start_date,' - ',COALESCE(`end_date`,'')) AS dates"), 'id')->orderBy('start_date', 'desc')->get()->pluck('dates', 'dates')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'timesheets' => $v_time,
            'total_value' => $total_value,
            'total_expenses' => $total_expenses,
        ];

        return view('reports.customer_billing')->with($parameters);
    }
}
