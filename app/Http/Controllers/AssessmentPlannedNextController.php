<?php

namespace App\Http\Controllers;

use App\Models\AssessmentPlannedNext;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentPlannedNextController extends Controller
{
    public function edit($id): View
    {
        $assessment_plan = AssessmentPlannedNext::find($id);
        $parameters = [
            'assessment_plan' => $assessment_plan,
        ];

        return view('assessment.assessment_plan.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $assessment_plan = AssessmentPlannedNext::find($id);
        $assessment_plan->assessment_task_description = $request->assessment_task_description;
        $assessment_plan->notes = $request->notes;
        $assessment_plan->save();

        return redirect()->route('assessment.show', $assessment_plan->assessment_id)->with('flash_success', 'Assessment Plan Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $assessment_plan = AssessmentPlannedNext::find($id);
        AssessmentPlannedNext::destroy($id);

        return redirect()->route('assessment.show', $assessment_plan->assessment_id)->with('flash_success', 'Assessment Plan Delete');
    }
}
