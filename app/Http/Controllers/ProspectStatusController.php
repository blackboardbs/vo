<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProspectStatusRequest;
use App\Models\ProspectStatus;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProspectStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $prospect_statuses = ProspectStatus::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $prospect_statuses = ProspectStatus::with(['status:id,description'])->where('status_id', $request->input('status_filter'));
            }else{
                $prospect_statuses = ProspectStatus::with(['status:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $prospect_statuses = $prospect_statuses->where('description', 'like', '%'.$request->input('q').'%');
        }

        $prospect_statuses = $prospect_statuses->paginate($item);

        return view('master_data.prospect_statuses.index')->with(['prospect_statuses' => $prospect_statuses]);
    }

    public function create(): View
    {
        return view('master_data.prospect_statuses.create');
    }

    public function store(ProspectStatusRequest $request): RedirectResponse
    {
        ProspectStatus::insert($request->only(['description', 'status_id']));

        return redirect()->route('prospectstatus.index')->with('flash_success', 'Prospect Status Created Successfully');
    }

    public function show(ProspectStatus $prospectstatus)
    {
        return view('master_data.prospect_statuses.show')->with(['prospectstatus' => $prospectstatus]);
    }

    public function edit(ProspectStatus $prospectstatus): View
    {
        return view('master_data.prospect_statuses.edit')->with(['prospectstatus' => $prospectstatus]);
    }

    public function update(ProspectStatusRequest $request, ProspectStatus $prospectstatus): RedirectResponse
    {
        $prospectstatus->update($request->only(['description', 'status_id']));

        return redirect()->route('prospectstatus.index')->with('flash_success', 'Prospect Status Updated Successfully');
    }

    public function destroy($prospectstatus)
    {
        // $prospectstatus->delete();
        $item = ProspectStatus::find($prospectstatus);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('prospectstatus.index')->with('flash_success', 'Prospect Status Deleted Successfully');
    }
}
