<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\RoleAbilities;
use App\Models\Roles;
use App\Models\SimplePermissions;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ModuleController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $system_modules = Module::with(['status:id,description'])->orderBy('display_name')->where('status_id', '=', 1);
        if ($request->has('module') && $request->input('module') != -1 && $request->input('module') != 0) {
            $system_modules = $system_modules->where('id', $request->input('module'));
        }
        $system_modules = $system_modules->sortable(['display_name' => 'asc'])->paginate($item);
        $parameters = [
            'system_modules' => $system_modules,
        ];

        return view('modules.index')->with($parameters);
    }

    public function show($id): View
    {
        $module = Module::find($id);
        $roles = Roles::with(['simplepermissions' => function ($query) use ($id) {
            $query->where('module_id', '=', $id);
        }])->orderBy('id')->get();

        $role_abilities = RoleAbilities::orderBy('display_name')->where('status_id', '=', 1)->pluck('display_name', 'id')->prepend('System Module', -1);
        $parameters = [
            'module' => $module,
            'roles' => $roles,
            'role_abilities' => $role_abilities,
        ];

        return view('modules.show')->with($parameters);
    }

    public function edit($id): View
    {
        $module = Module::find($id);
        $roles = Roles::with(['simplepermissions' => function ($query) use ($id) {
            $query->where('module_id', '=', $id);
        }])->orderBy('id')->get();

        $role_abilities = RoleAbilities::orderBy('display_name')->where('status_id', '=', 1)->pluck('display_name', 'id')->prepend('System Module', -1);
        $parameters = [
            'module' => $module,
            'roles' => $roles,
            'role_abilities' => $role_abilities,
        ];

        return view('modules.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $roles = Roles::orderBy('id')->get();
        foreach ($roles as $role) {
            $permissions = SimplePermissions::where('module_id', '=', $id)->where('role_id', '=', $role->id)->get()->toArray();
            if (! isset($permissions[0]['id'])) {
                $simple_permissions = new SimplePermissions();
                $simple_permissions->module_id = $id;
                $simple_permissions->role_id = $role->id;
                $simple_permissions->creator_id = Auth::id();
                $simple_permissions->status_id = 1;
            } else {
                $simple_permissions = SimplePermissions::find($permissions[0]['id']);
            }
            $simple_permissions->list_all_records = $request->input('list_all_records_'.$role->id);
            $simple_permissions->list_own_and_team_records = $request->input('list_own_and_team_records_'.$role->id);
            $simple_permissions->show_all_records = $request->input('show_all_records_'.$role->id);
            $simple_permissions->show_own_and_team_records = $request->input('show_own_and_team_records_'.$role->id);
            $simple_permissions->save();
        }

        return redirect(route('module.index'))->with('flash_success', 'Permissions updated successfully');
    }
}
