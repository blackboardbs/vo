<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Http\Requests\StoreCommissionRequest;
use App\Http\Requests\UpdateCommissionRequest;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CommissionController extends Controller
{
    public function index(Request $request): View
    {
        $r = User::find(Auth::id());

        $role = Auth::user()->roles()->get();

        $item = $request->input('r') ?? 15;

        $comm = Commission::with(['statusd:id,description'])->sortable(['description' => 'asc']);

        if ($request->has('q') && $request->input('q') != '') {
            $comm = $comm->where('description', 'LIKE', '%'.$request->input('q').'%')
                ->orWhere('min_rate', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('statusd', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                });
        }

        $comm = $comm->paginate($item);

        $parameters = [
            'comm' => $comm,
        ];

        return view('commission.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
        ];

        return view('commission.create')->with($parameters);
    }

    public function store(StoreCommissionRequest $request): RedirectResponse
    {
        $comm = new Commission();
        $comm->description = $request->input('description');
        $comm->min_rate = $request->input('min_rate');
        $comm->hour01 = $request->input('hour01');
        $comm->comm01 = $request->input('comm01');
        $comm->hour02 = $request->input('hour02');
        $comm->comm02 = $request->input('comm02');
        $comm->hour03 = $request->input('hour03');
        $comm->comm03 = $request->input('comm03');
        $comm->hour04 = $request->input('hour04');
        $comm->comm04 = $request->input('comm04');
        $comm->hour05 = $request->input('hour05');
        $comm->comm05 = $request->input('comm05');
        $comm->hour06 = $request->input('hour06');
        $comm->comm06 = $request->input('comm06');
        $comm->hour07 = $request->input('hour07');
        $comm->comm07 = $request->input('comm07');
        $comm->hour08 = $request->input('hour08');
        $comm->comm08 = $request->input('comm08');
        $comm->hour09 = $request->input('hour09');
        $comm->comm09 = $request->input('comm09');
        $comm->hour10 = $request->input('hour10');
        $comm->comm10 = $request->input('comm10');
        $comm->hour11 = $request->input('hour11');
        $comm->comm11 = $request->input('comm11');
        $comm->hour12 = $request->input('hour12');
        $comm->comm12 = $request->input('comm12');
        $comm->status_id = $request->input('status');
        $comm->creator_id = auth()->id();
        $comm->save();

        return redirect(route('commission.index'))->with('flash_success', 'Commission captured successfully');
    }

    public function edit($commid): View
    {
        $parameters = [
            'comm' => Commission::where('id', '=', $commid)->get(),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
        ];

        return view('commission.edit')->with($parameters);
    }

    public function update(UpdateCommissionRequest $request, $commid): RedirectResponse
    {
        $comm = Commission::find($commid);
        $comm->description = $request->input('description');
        $comm->min_rate = $request->input('min_rate');
        $comm->hour01 = $request->input('hour01');
        $comm->comm01 = $request->input('comm01');
        $comm->hour02 = $request->input('hour02');
        $comm->comm02 = $request->input('comm02');
        $comm->hour03 = $request->input('hour03');
        $comm->comm03 = $request->input('comm03');
        $comm->hour04 = $request->input('hour04');
        $comm->comm04 = $request->input('comm04');
        $comm->hour05 = $request->input('hour05');
        $comm->comm05 = $request->input('comm05');
        $comm->hour06 = $request->input('hour06');
        $comm->comm06 = $request->input('comm06');
        $comm->hour07 = $request->input('hour07');
        $comm->comm07 = $request->input('comm07');
        $comm->hour08 = $request->input('hour08');
        $comm->comm08 = $request->input('comm08');
        $comm->hour09 = $request->input('hour09');
        $comm->comm09 = $request->input('comm09');
        $comm->hour10 = $request->input('hour10');
        $comm->comm10 = $request->input('comm10');
        $comm->hour11 = $request->input('hour11');
        $comm->comm11 = $request->input('comm11');
        $comm->hour12 = $request->input('hour12');
        $comm->comm12 = $request->input('comm12');
        $comm->status_id = $request->input('status');
        $comm->creator_id = auth()->id();
        $comm->save();

        return redirect(route('commission.index'))->with('flash_success', 'Commission saved successfully');
    }

    public function show($commid): View
    {
        $parameters = [
            'comm' => Commission::where('id', '=', $commid)->get(),
        ];

        return view('commission.show')->with($parameters);
    }

    public function destroy($id): RedirectResponse
    {
        Commission::destroy($id);

        return redirect()->back()->with('flash_success', 'Commission has been deleted');
    }
}
