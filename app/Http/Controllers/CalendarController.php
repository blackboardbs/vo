<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Anniversary;
use App\Models\AssessmentHeader;
use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Config;
use App\Models\CustomerInvoice;
use App\Models\Event;
use App\Models\Leave;
use App\Models\Module;
use App\Models\Project;
use App\Models\PublicHoliday;
use App\Models\Resource;
use App\Models\ResourceTask;
use App\Models\Status;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VendorInvoice;
use App\Models\VendorInvoiceLine;
use App\Services\ActionService;
use App\Services\AnniversaryService;
use App\Services\AssessmentService;
use App\Services\AssignmentService;
use App\Services\CustomerInvoiceService;
use App\Services\EventService;
use App\Services\LeaveService;
use App\Services\ResourceService;
use App\Services\TaskService;
use App\Services\UserService;
use App\Services\VendorInvoiceService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $calendar_events = Calendar::whereDate('creator_id', '=', auth()->id())
            ->get();

        $module = Module::where('name', '=', \App\Models\Calendar::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $calendar_events = $calendar_events->whereIn('manager_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $user_roles = Role::pluck('display_name', 'id');
        $teams = Team::pluck('team_name', 'id');
        $users = User::selectRaw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name, id")->excludes([6, 7, 8])->pluck('full_name', 'id');
        $projets = Project::pluck('name', 'id');

        $parameters = [
            'calendar_events' => $calendar_events,
            'status_drop_down' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'user_role_drop_down' => $user_roles,
            'team_drop_down' => $teams,
            'user_drop_down' => $users,
            'project_drop_down' => $projets,
        ];

        return view('calendar.index')->with($parameters);
    }

    public function get_events(): JsonResponse
    {
        $calendar = Action::whereBetween('due_date',
            [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()]
        )
            ->when(
                (new ActionService())->hasViewTeamPermission(),
                fn($action) => $action->whereIn('assigned_user_id', Auth::user()->team())
            )->unless(
                !(new ActionService())->hasViewTeamPermission(),
                fn($action) => $action->where('assigned_user_id', -100)
            )
            ->whereIn('status_id', [1, 2, 4])
            ->get(['id', 'subject', 'due_date'])
            ->map(fn ($action) => [
                'id' => $action->id,
                'title' => $action->subject,
                'start' => $action->due_date,
                'end' => $action->due_date,
                'risk' => 'action',
                'url' => route('action.show', $action->id),
            ]);

        $tasks = Task::with(['project:id,name'])
            ->when(
                (new TaskService())->hasViewTeamPermission(),
                fn($task) => $task->whereIn('employee_id', Auth::user()->team())
            )->unless(
                !(new TaskService())->hasViewTeamPermission(),
                fn($task) => $task->where('employee_id', -100)
            )->where('start_date', '<=', now()->addMonth()->endOfMonth()->toDateString())
            ->where('end_date', '>=', now()->subMonth()->startOfMonth()->toDateString())
            ->whereNotIn('status', [4, 5])
            ->get(['id', 'description', 'start_date', 'end_date', 'project_id'])
            ->map(fn ($task) => [
                'id' => $task->id,
                'title' => $task->project?->name.' - '.$task->description,
                'start' => $task->start_date,
                'end' => Carbon::parse($task->end_date)->addDay()->toDateString(),
                'risk' => 'task',
                'url' => route('task.view', $task->id),
            ]);

        $calendar = [...$calendar, ...$tasks];

        $leaves = Leave::with(['resource:id,first_name,last_name', 'leave_type:id,description'])
            ->when(
                (new LeaveService())->hasViewTeamPermission(),
                fn($leaves) => $leaves->whereIn('emp_id', \auth()->user()->team())
            )->unless(
                !(new LeaveService())->hasViewTeamPermission(),
                fn($leaves) => $leaves->where('emp_id', -100)
            )->where('date_from', '<=', now()->addMonth()->endOfMonth()->toDateString())
            ->where('date_to', '>=', now()->subMonth()->startOfMonth()->toDateString())
            ->where('leave_status', '<>', 2)
            ->get(['id', 'date_from', 'date_to', 'emp_id', 'leave_type_id'])
            ->map(fn ($leave) => [
                'id' => $leave->id,
                'title' => $leave->resource?->name().' - '.$leave->leave_type?->description,
                'start' => $leave->date_from,
                'end' => Carbon::parse($leave->date_to)->addDay()->toDateString(),
                'risk' => 'leave',
                'url' => route('leave.show', $leave->id),
            ]);

        $calendar = [...$calendar, ...$leaves];

        $assignments = Assignment::with(['project:id,name'])
            ->where('start_date', '<=', now()->addMonth()->endOfMonth()->toDateString())
            ->where('end_date', '>=', now()->subMonth()->startOfMonth()->toDateString())
            ->whereIn('assignment_status', [1, 2, 3])
            ->when(
                (new AssignmentService())->hasViewTeamPermission(),
                fn($assignment) => $assignment->whereIn('employee_id', \auth()->user()->team())
            )->unless(
                !(new AssignmentService())->hasViewTeamPermission(),
                fn($assignment) => $assignment->where('employee_id', -100)
            )
            ->get(['id', 'project_id', 'start_date', 'end_date'])
            ->map(fn ($assignment) => [
                'id' => $assignment->id,
                'title' => $assignment->project?->name,
                'start' => $assignment->start_date,
                'end' => Carbon::parse($assignment->end_date)->addDay()->toDateString(),
                'risk' => 'assignment',
                'url' => route('assignment.show', $assignment->id),
            ]);

        $calendar = [...$calendar, ...$assignments];


        $assessments = AssessmentHeader::with(['user:id,first_name,last_name'])
            ->distinct(['assessment_date', 'next_assessment'])
            ->when(
                (new AssessmentService())->hasViewTeamPermission(),
                fn($assessments) => $assessments->whereIn('resource_id', \auth()->user()->team())
            )->unless(
                !(new AssessmentService())->hasViewTeamPermission(),
                fn($assessments) => $assessments->where('resource_id', -100)
            )->get(['id', 'assessment_date', 'resource_id'])
            ->map(fn ($assessment) => [
                'id' => $assessment->id,
                'title' => $assessment->user?->name().' - Assessment',
                'start' => $assessment->assessment_date,
                'end' => $assessment->assessment_date,
                'risk' => 'assessment',
                'url' => route('assessment.show', $assessment->id),
            ]);

        $calendar = [...$calendar, ...$assessments];


        $anniversaries = Anniversary::with(['resource:id,first_name,last_name', 'anniversary_type:id,description'])
            ->when(
                (new AnniversaryService())->hasViewTeamPermission(),
                fn($anniversary) => $anniversary->whereIn('emp_id', \auth()->user()->team())
            )->unless(
                !(new AnniversaryService())->hasViewTeamPermission(),
                fn($anniversary) => $anniversary->where('emp_id', -100)
            )->where('status_id', 1)
            ->where('relationship', 1)
            ->get(['id', 'emp_id', 'anniversary_date', 'anniversary'])
            ->map(fn ($anniversary) => [
                'id' => $anniversary->id,
                'title' => $anniversary->resource?->name().' - '.$anniversary->anniversary_type?->description,
                'start' => now()->format('Y').'-'.Carbon::parse($anniversary->anniversary_date)->format('m-d'),
                'end' => now()->format('Y').'-'.Carbon::parse($anniversary->anniversary_date)->format('m-d'),
                'risk' => 'anniversary',
                'url' => route('anniversary.show', $anniversary->id),
            ]);

        $calendar = [...$calendar, ...$anniversaries];

        $events = Event::whereBetween('event_date',
            [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()]
        )->where('status_id', \App\Enum\Status::ACTIVE->value)
            //->when((new EventService())->hasViewTeamPermission(), fn($event) => $event->whereIn('employee_id', \auth()->user()->team()))
            ->get(['id', 'event', 'event_date'])
            ->map(fn ($event) => [
                'id' => $event->id,
                'title' => $event->event,
                'start' => $event->event_date,
                'end' => $event->event_date,
                'risk' => 'cale',
                'url' => route('event.show', $event->id),
            ]);

        $calendar = [...$calendar, ...$events];

        $users = User::whereBetween('expiry_date', [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()])
            ->when(
                (new UserService())->hasViewTeamPermission(),
                fn($user) => $user->whereIn('id', \auth()->user()->team())
            )->unless(
                !(new UserService())->hasViewTeamPermission(),
                fn($user) => $user->where('id', -100)
            )->where('status_id', \App\Enum\Status::ACTIVE->value)
            ->get(['id', 'first_name', 'last_name', 'expiry_date'])
            ->map(fn ($user) => [
                'id' => $user->id,
                'title' => $user->name(),
                'start' => $user->expiry_date,
                'end' => $user->expiry_date,
                'risk' => 'user',
                route('profile', $user->id)
            ]);

        $calendar = [...$calendar, ...$users];

        $customerInvoices = CustomerInvoice::with(['customer:id,customer_name'])
            ->whereBetween('due_date', [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()])
            ->where('bill_status', 1)
            ->when(
                (new CustomerInvoiceService())->hasViewTeamPermission(),
                fn($invoice) => $invoice->whereHas('resource',
                    fn($line) => $line->whereIn('employee_id', \auth()->user()->team()
                    )
                )
            )->unless(
                !(new CustomerInvoiceService())->hasViewTeamPermission(),
                fn($invoice) => $invoice->whereHas('resource',
                    fn($line) => $line->where('employee_id', -100
                    )
                )
            )->get(['id', 'customer_id', 'due_date', 'inv_ref'])
            ->map(fn ($invoice) => [
                'id' => $invoice->id,
                'title' => $invoice->customer?->customer_name.' - '.$invoice->inv_ref,
                'start' => $invoice->due_date,
                'end' => $invoice->due_date,
                'risk' => 'cinvoice',
                'url' => route('customer.invoice_show', $invoice->id),
            ]);

        $calendar = [...$calendar, ...$customerInvoices];

        $vendorInvoices = VendorInvoice::with(['vendor:id,vendor_name'])
            ->whereBetween('vendor_due_date', [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()])
            ->where('vendor_invoice_status', 1)
            ->when(
                (new VendorInvoiceService())->hasViewTeamPermission(),
                fn($invoice) => $invoice->whereHas('vendorInvoiceLine',
                    fn($line) => $line->whereIn('emp_id', \auth()->user()->team()
                    )
                )
            )->unless(
                !(new VendorInvoiceService())->hasViewTeamPermission(),
                fn($invoice) => $invoice->whereHas('vendorInvoiceLine',
                    fn($line) => $line->where('emp_id', -100
                    )
                )
            )->get(['id', 'vendor_id', 'vendor_due_date', 'vendor_invoice_ref'])
            ->map(fn ($invoice) => [
                'id' => $invoice->id,
                'title' => $invoice->vendor?->vendor_name.' - '.$invoice->vendor_invoice_ref,
                'start' => $invoice->vendor_due_date,
                'end' => $invoice->vendor_due_date,
                'risk' => 'vinvoice',
                'url' => route('view.invoice', $invoice->id),
            ]);

        $calendar = [...$calendar, ...$vendorInvoices];

        $medicalCertificates = Resource::with(['user:id,first_name,last_name'])
            ->when(
                (new ResourceService())->hasViewTeamPermission(),
                fn($resource) => $resource->whereIn('user_id', \auth()->user()->team())
            )->unless(
                !(new ResourceService())->hasViewTeamPermission(),
                fn($resource) => $resource->whereIn('user_id', -100)
            )->whereBetween('medical_certificate_expiry', [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()])
            ->get(['id', 'user_id', 'medical_certificate_expiry'])
            ->map(fn ($medicalCertificate) => [
                'id' => $medicalCertificate->id,
                'title' => $medicalCertificate->user?->name().' - Medical',
                'start' => $medicalCertificate->medical_certificate_expiry,
                'end' => $medicalCertificate->medical_certificate_expiry,
                'risk' => 'medcert',
                'url' => route('resource.show', $medicalCertificate->id),
            ]);

        $calendar = [...$calendar, ...$medicalCertificates];

        $holidays = PublicHoliday::whereBetween('date', [now()->subMonth()->startOfMonth()->toDateString(), now()->addMonth()->endOfMonth()->toDateString()])
            ->get(['id', 'date', 'holiday_name'])
            ->map(fn ($holiday) => [
                'id' => $holiday->id,
                'title' => $holiday->holiday_name,
                'start' => $holiday->date,
                'end' => $holiday->date,
                'risk' => 'holidays',
                'url' => null,
            ]);

        $calendar = [...$calendar, ...$holidays];


        return response()->json($calendar);
    }
}
