<?php

namespace App\Http\Controllers;

use App\Models\BusinessRating;
use App\Models\CvSubmit;
use App\Exports\ScoutingExport;
use App\Models\InterviewStatus;
use App\Models\JobSpec;
use App\Models\ProcessStatus;
use App\Models\ResourceLevel;
use App\Models\Scouting;
use App\Models\ScoutingRole;
use App\Models\TechnicalRating;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use TCPDF;

class ScoutingReportController extends Controller
{
    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->has('s') && $request->input('s') > 0 ? $request->input('s') : 15;
        $scoutings = Scouting::with(['processstatus', 'interviewstatus', 'role', 'vendor', 'resourcelevel', 'businessrating', 'technicalrating'])->orderby('resource_first_name');

        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('All', -1);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('All', -1);

        if ($request->has('process_status') && $request->input('process_status') != -1) {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status'));
        }

        if ($request->has('process_interview') && $request->input('process_interview') != -1) {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview'));
        }

        if ($request->has('role') && $request->input('role') != -1) {
            $scoutings = $scoutings->where('role_id', $request->input('role'));
        }

        if ($request->has('vendor') && $request->input('vendor') != -1) {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor'));
        }

        if ($request->has('resource_level') && $request->input('resource_level') != -1) {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level'));
        }

        if ($request->has('businees_rating') && $request->input('businees_rating') != -1) {
            $scoutings = $scoutings->where('business_rating_id', $request->input('businees_rating'));
        }

        if ($request->has('technical_rating') && $request->input('technical_rating') != -1) {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating'));
        }

        $scoutings = $scoutings->paginate($item);

        $parameters = [
            'scoutings' => $scoutings,
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'scouting');

        return view('reports.scouting')->with($parameters);
    }

    public function exporting(Request $request)
    {
        $scoutings = Scouting::with(['processstatus', 'interviewstatus', 'role', 'vendor', 'ResourceLevel', 'businessrating', 'technicalrating'])->orderby('resource_first_name');

        if ($request->has('process_status') && $request->input('process_status') != -1) {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status'));
        }

        if ($request->has('process_interview') && $request->input('process_interview') != -1) {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview'));
        }

        if ($request->has('role') && $request->input('role') != -1) {
            $scoutings = $scoutings->where('role_id', $request->input('role'));
        }

        if ($request->has('vendor') && $request->input('vendor') != -1) {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor'));
        }

        if ($request->has('resource_level') && $request->input('resource_level') != -1) {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level'));
        }

        if ($request->has('businees_rating') && $request->input('businees_rating') != -1) {
            $scoutings = $scoutings->where('business_rating_id', $request->input('businees_rating'));
        }

        if ($request->has('technical_rating') && $request->input('technical_rating') != -1) {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating'));
        }

        $scoutings = $scoutings->get();

        $scouting_data = new Collection();
        foreach ($scoutings as $scouting) {
            $scouting_data->push([
                'resource_name' => $scouting->resource_last_name.', '.$scouting->resource_first_name,
                'email' => $scouting->email,
                'process_status' => isset($scouting->processstatus->name) ? $scouting->processstatus->name : '',
                'interview_status' => isset($scouting->interviewstatus->name) ? $scouting->interviewstatus->name : '',
                'vendor' => isset($scouting->vendor->vendor_name) ? $scouting->vendor->vendor_name : '',
                'resource_level' => isset($scouting->resourcelevel->description) ? $scouting->resourcelevel->description : '',
                'business_rating' => isset($scouting->businessrating->name) ? $scouting->businessrating->name : '',
                'technical_rating' => isset($scouting->technicalrating->name) ? $scouting->technicalrating->name : '',
            ]);
        }

        return Excel::download(new ScoutingExport($scouting_data), 'resources_'.date('Y_m_d_H_i_s').'.xlsx');
    }

    public function pdf(Request $request)
    {
        $scoutings = Scouting::with(['processstatus', 'interviewstatus', 'role', 'vendor', 'ResourceLevel', 'businessrating', 'technicalrating'])->orderby('resource_first_name');

        if ($request->has('process_status') && $request->input('process_status') != -1) {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status'));
        }

        if ($request->has('process_interview') && $request->input('process_interview') != -1) {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview'));
        }

        if ($request->has('role') && $request->input('role') != -1) {
            $scoutings = $scoutings->where('role_id', $request->input('role'));
        }

        if ($request->has('vendor') && $request->input('vendor') != -1) {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor'));
        }

        if ($request->has('resource_level') && $request->input('resource_level') != -1) {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level'));
        }

        if ($request->has('businees_rating') && $request->input('businees_rating') != -1) {
            $scoutings = $scoutings->where('business_rating_id', $request->input('businees_rating'));
        }

        if ($request->has('technical_rating') && $request->input('technical_rating') != -1) {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating'));
        }

        $scoutings = $scoutings->get();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        $pdf->AddPage();
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        $file_name = 'resources_'.date('Y_m_d_H_i_s').'.pdf';
        $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
        $file_name_pdf = $dir.'file_'.date('Y_m_d_H_i_s').'.pdf';

        $html = "<table border='1'>
                        <tbody>
                            <tr>
                                <th>Resource Name</th><th>Email</th><th>Process Status</th><th>Intverview status</th><th>Vendor</th><th>Resource Level</th><th>Business Rating</th><th>Technical Rating</th>
                            </tr>";

        foreach ($scoutings as $scouting) {
            $process_status = isset($scouting->processstatus->name) ? $scouting->processstatus->name : '';
            $vendor = isset($scouting->vendor->vendor_name) ? $scouting->vendor->vendor_name : '';
            $interviewstatus = isset($scouting->interviewstatus->name) ? $scouting->interviewstatus->name : '';
            $resource_level = isset($scouting->resourcelevel->description) ? $scouting->resourcelevel->description : '';
            $business_rating = isset($scouting->businessrating->name) ? $scouting->businessrating->name : '';
            $technical_rating = isset($scouting->technicalrating->name) ? $scouting->technicalrating->name : '';
            $html .= '<tr>
                            <td>'.$scouting->resource_last_name.', '.$scouting->resource_first_name.'</td>
                            <td>'.$scouting->email.'</td>
                            <td>'.$process_status.'</td>
                            <td>'.$interviewstatus.'</td>
                            <td>'.$vendor.'</td>
                            <td>'.$resource_level.'</td>
                            <td>'.$business_rating.'</td>
                            <td>'.$technical_rating.'</td>
                        </tr>';
        }

        $html .= '    </tbody>
                </table>';

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->Output($file_name_pdf, 'I');
    }

    public function analysis(Request $request): View
    {
        $item = $request->has('s') && $request->input('s') > 0 ? $request->input('s') : 15;
        $scoutings = Scouting::with(['processstatus', 'interviewstatus', 'role', 'vendor', 'ResourceLevel', 'businessrating', 'technicalrating'])->orderby('resource_first_name');

        $process_status_counter = [];
        $process_status_counter['declined'] = Scouting::select(DB::raw('COUNT(process_status_id) AS declined'))->where('process_status_id', '=', 1)->first()->declined;
        $process_status_counter['contract_end'] = Scouting::select(DB::raw('COUNT(process_status_id) AS contract_end'))->where('process_status_id', '=', 2)->first()->contract_end;
        $process_status_counter['opportunity'] = Scouting::select(DB::raw('COUNT(process_status_id) AS opportunity'))->where('process_status_id', '=', 3)->first()->opportunity;
        $process_status_counter['resigned'] = Scouting::select(DB::raw('COUNT(process_status_id) AS resigned'))->where('process_status_id', '=', 4)->first()->resigned;
        $process_status_counter['pending'] = Scouting::select(DB::raw('COUNT(process_status_id) AS pending'))->where('process_status_id', '=', 5)->first()->pending;
        $process_status_counter['offer_not_accepted'] = Scouting::select(DB::raw('COUNT(process_status_id) AS offer_not_accepted'))->where('process_status_id', '=', 6)->first()->offer_not_accepted;

        $interview_status_counter = [];
        $interview_status_counter['interviewed'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS interviewed'))->where('interview_status_id', '=', 1)->first()->interviewed;
        $interview_status_counter['to_interview'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS to_interview'))->where('interview_status_id', '=', 2)->first()->to_interview;
        $interview_status_counter['aes_experience'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS aes_experience'))->where('interview_status_id', '=', 3)->first()->aes_experience;

        $vendors_counter = Scouting::select(DB::raw('count(scouting.id) as counter, vendor.vendor_name'))
            ->leftJoin('vendor', 'scouting.vendor_id', '=', 'vendor.id')
            ->groupBy('vendor.vendor_name')
            ->get();

        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('All', -1);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('All', -1);

        if ($request->has('process_status') && $request->input('process_status') != -1) {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status'));
        }

        if ($request->has('process_interview') && $request->input('process_interview') != -1) {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview'));
        }

        if ($request->has('role') && $request->input('role') != -1) {
            $scoutings = $scoutings->where('role_id', $request->input('role'));
        }

        if ($request->has('vendor') && $request->input('vendor') != -1) {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor'));
        }

        if ($request->has('resource_level') && $request->input('resource_level') != -1) {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level'));
        }

        if ($request->has('businees_rating') && $request->input('businees_rating') != -1) {
            $scoutings = $scoutings->where('business_rating_id', $request->input('businees_rating'));
        }

        if ($request->has('technical_rating') && $request->input('technical_rating') != -1) {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating'));
        }

        $scoutings = $scoutings->paginate($item);

        $parameters = [
            'scoutings' => $scoutings,
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
            'process_status_counter' => $process_status_counter,
            'interview_status_counter' => $interview_status_counter,
            'vendors_counter' => $vendors_counter,
        ];

        return view('reports.scouting_analysis')->with($parameters);
    }

    public function jobSpecSummary(Request $request): View|BinaryFileResponse
    {
        $item = $request->has('s') && $request->input('s') > 0 ? $request->input('s') : 15;
        $job_specs = JobSpec::with(['positionrole','customer','positiontype','recruiter'])->paginate($item);
        $cv_count = [];
        foreach ($job_specs as $job_spec) {
            $cv_count[$job_spec->id] = CvSubmit::where('job_spec_id', $job_spec->id)->get()->count();
        }
        $parameters = [
            'job_specs' => $job_specs,
            'cv_count' => $cv_count,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'job_spec');

        return view('reports.job_spec')->with($parameters);
    }

    public function recruitment(): View
    {
        return view('reports.recruitment');
    }
}
