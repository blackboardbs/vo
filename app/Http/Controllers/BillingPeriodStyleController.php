<?php

namespace App\Http\Controllers;

use App\Models\BillingPeriodStyle;
use App\Models\Status;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class BillingPeriodStyleController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $styles = BillingPeriodStyle::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $styles = $styles->where('status_id', $request->input('status_filter'));
            }else{
                $styles = $styles->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $styles = $styles->where('name', 'like', '%'.$request->input('q').'%');
        }

        $styles = $styles->paginate($item);

        // $styles = $styles->latest()->paginate(request()->r??15);

        return view('master_data.template_styles.index')->with(['styles' => $styles]);
    }

    public function create(): View
    {
        $parameters = [
            'template_dropdown' => BillingPeriodStyle::TEMPLATES,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            'logo_dropdown' => BillingPeriodStyle::LOGO
        ];

        return view('master_data.template_styles.create')->with($parameters);

    }

    public function store(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'name' => 'required|string',
            'template_id' => 'required|int',
            'status_id' => 'required|int'
        ]);

        $file_left = $request->hasFile("left_logo_file")?$this->interventionImage($request, "left_logo_file"):$request->left_logo;
        $file_center = $request->hasFile("center_logo_file")?$this->interventionImage($request, "center_logo_file"):$request->center_logo;
        $file_right = $request->hasFile("right_logo_file")?$this->interventionImage($request, "right_logo_file"):$request->right_logo;

        $style = new BillingPeriodStyle();
        $style->name = $request->name;
        $style->template_id = $request->template_id;
        $style->status_id = $request->status_id;
        $style->left_logo = $file_left;
        $style->center_logo = $file_center;
        $style->right_logo = $file_right;
        $style->logos_vertical_alignment = $request->logos_vertical_alignment;
        $style->resource_label = $request->resource_label;
        $style->project_label = $request->project_label;
        $style->start_date_label = $request->start_date_label;
        $style->end_date_label = $request->end_date_label;
        $style->po_number_label = $request->po_number_label;
        $style->is_billing_period_included = $request->is_billing_period_included;
        $style->comment = $request->comment;
        $style->header_color = $request->header_color;
        $style->header_background = $request->header_background;
        $style->detail_color = $request->detail_color;
        $style->detail_background = $request->detail_background;
        $style->footer_color = $request->footer_color;
        $style->footer_background = $request->footer_background;
        $style->is_signed = $request->is_signed;
        $style->save();

        return redirect()->route('templatestyles.index', $style)->with('flash_success', 'Template was created successfully');
    }

    public function show($id)
    {
        if (!$style = BillingPeriodStyle::with(['status' => function($status){
            $status->select('id', 'description');
        }])->find($id)){
            return abort(404);
        }

        return view('master_data.template_styles.show')->with(['style' => $style]);
    }

    public function edit($id)
    {
        if (!$style = BillingPeriodStyle::with(['status' => function($status){
            $status->select('id', 'description');
        }])->find($id)){
            return abort(404);
        }

        $parameters = [
            'template_dropdown' => BillingPeriodStyle::TEMPLATES,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            'logo_dropdown' => BillingPeriodStyle::LOGO,
            'style' => $style,
        ];

        return view('master_data.template_styles.edit')->with($parameters);
    }

    public function update(Request $request,$id)
    {
        if (!$style = BillingPeriodStyle::with(['status' => function($status){
            $status->select('id', 'description');
        }])->find($id)){
            return abort(404);
        }

        $data = $request->validate([
            'name' => 'required|string',
            'template_id' => 'required|int',
            'status_id' => 'required|int'
        ]);


        if ($request->hasFile("left_logo_file")){
            $file_left = $this->interventionImage($request, "left_logo_file");
        }elseif (isset($style->left_logo) && !is_numeric($style->left_logo) && ($request->left_logo == null)   ){
            $file_left = $style->left_logo;
        }else{
            $file_left = $request->left_logo;
        }

        if ($request->hasFile("center_logo_file")){
            $file_center = $this->interventionImage($request, "center_logo_file");
        }elseif (!isset($request->center_logo) && isset($style->center_logo) && !is_numeric($style->center_logo)){
            $file_center = $style->center_logo;
        }else{
            $file_center = $request->center_logo;
        }

        if ($request->hasFile("right_logo_file")){
            $file_right = $this->interventionImage($request, "right_logo_file");
        }elseif (!isset($request->right_logo) && isset($style->right_logo) && !is_numeric($style->right_logo)){
            $file_right = $style->right_logo;
        }else{
            $file_right = $request->right_logo;
        }

        $style->name = $request->name;
        $style->template_id = $request->template_id;
        $style->status_id = $request->status_id;
        $style->left_logo = $file_left;
        $style->center_logo = $file_center;
        $style->right_logo = $file_right;
        $style->logos_vertical_alignment = $request->logos_vertical_alignment;
        $style->resource_label = $request->resource_label;
        $style->project_label = $request->project_label;
        $style->start_date_label = $request->start_date_label;
        $style->end_date_label = $request->end_date_label;
        $style->po_number_label = $request->po_number_label;
        $style->is_billing_period_included = $request->is_billing_period_included;
        $style->comment = $request->comment;
        $style->header_color = $request->header_color;
        $style->header_background = $request->header_background;
        $style->detail_color = $request->detail_color;
        $style->detail_background = $request->detail_background;
        $style->footer_color = $request->footer_color;
        $style->footer_background = $request->footer_background;
        $style->is_signed = $request->is_signed;
        $style->save();

        return redirect()->route('templatestyles.index')->with('flash_success', 'Template Styles Updated Successfully');
    }

    public function destroy($id)
    {
        // if (!$style = BillingPeriodStyle::with(['status' => function($status){
        //     $status->select('id', 'description');
        // }])->find($id)){
        //     return abort(404);
        // }

        // $style->delete();
        $item = BillingPeriodStyle::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('templatestyles.index')->with('flash_success', 'Template Styles suspended Successfully');
    }

    public function getCustomTemplate(): JsonResponse
    {
        return response()->json(BillingPeriodStyle::where('status_id', 1)->pluck('name', 'id'));
    }

    private function interventionImage(Request $request, $field)
    {
        if ($request->hasFile($field)) {

            try {
                if (!Storage::exists('public/template')) {
                    Storage::makeDirectory('public/template');
                }

                $request->file($field)->store('public/template');
                $file = $request->file($field)->hashName();
                $image = Image::make(storage_path().'/app/public/template/'.$file);

                if ($image->height() > 180){
                    $image->resize(null, 180, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $image->save();

                return $file;
            }
            catch (\Exception $e){
                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }
    }
}
