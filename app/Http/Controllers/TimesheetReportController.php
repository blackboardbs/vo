<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\BillingCycle;
use App\Models\BillingPeriod;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Customer;
use App\Models\Epic;
use App\Exports\TasksExport;
use App\Exports\TimesheetReportExport;
use App\Exports\TimesheetListReportExport;
use App\Models\Feature;
use App\Models\InvoiceStatus;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Models\ApiRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\UserStory;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Api;

class TimesheetReportController extends Controller
{
    public function timesheet(Request $request)
    {
        $timelines = Timeline::with(['task' => function($query){
            $query->select('id', 'description');
        }, 'timesheet' => function($q){
            $q->select(['id', 'employee_id', 'project_id', 'customer_id', 'company_id', 'year_week'])
                ->with([
                    'user' => function($q){
                        $q->selectRaw('id, CONCAT(`first_name`," ",`last_name`) AS resource');
                    },
                    'project' => function($q){
                    $q->select('id', 'name', 'customer_id');
                }]);
        }])->selectRaw(
            'timesheet_id, description_of_work, SUM((mon * 60) + mon_m) AS monday, SUM((tue * 60) + tue_m) AS tuesday,
             SUM((wed * 60) + wed_m) AS wednesday, SUM((thu * 60) + thu_m) AS thursday, SUM((fri * 60) + fri_m) AS friday,
              SUM((sat * 60) + sat_m) AS saturday, SUM((sun * 60) + sun_m) AS sunday, SUM((total * 60) + total_m) AS total,
              is_billable, task_id'
        )->when(request()->customer, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('customer_id', request()->customer);
            });
        })->when(request()->project, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('project_id', request()->project);
            });
        })->when(request()->company, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(request()->resource, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('employee_id', request()->resource);
            });
        })->when(request()->billable, function ($timeline){
            $timeline->where('is_billable', request()->billable);
        })->when(request()->task, function ($timeline){
            $timeline->where('task_id', request()->task);
        })->when(request()->user_story, function ($timeline){
            $timeline->whereHas('task', function ($q){
                $q->where('user_story_id', request()->user_story);
            });
        })->when(request()->feature, function ($timeline){
            $timeline->whereHas('task.user_story', function ($q){
                $q->where('feature_id', request()->feature);
            });
        })->when(request()->epic, function ($timeline){
            $timeline->whereHas('task.user_story.feature', function ($q){
                $q->where('epic_id', request()->epic);
            });
        })->when(request()->invoice_status, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('bill_status', request()->invoice_status);
            });
        })->when(request()->from_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '>=', request()->from_week);
            });
        })->when(request()->to_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '<=', request()->to_week);
            });
        })->when(request()->from_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','>=', substr(request()->from_month,5))
                    ->where('year', '>=', substr(request()->from_month, 0, 4));
            });
        })->when(request()->filled('billing_period_id'), function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $billing_period = BillingPeriod::find(request()->billing_period_id);
                if (isset($billing_period)){
                    $q->where('first_day_of_month', '>=', $billing_period->start_date)
                        ->where('last_day_of_month', '<=', $billing_period->end_date);
                }
            });
        })->when(request()->to_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','<=', substr(request()->to_month,5))
                ->where('year', '<=', substr(request()->to_month, 0, 4));
            });
        })->when(request()->cost_center, function ($timeline){
            $timeline->whereHas('timesheet.project.assignment.resource_user', function ($q){
                $q->where('cost_center_id', request()->cost_center);
            });
        })->groupBy(['timesheet_id', 'description_of_work', 'is_billable', 'task_id'])
            ->orderBy('timesheet_id', 'desc')->paginate(request()->s??15);
// dd($timelines->sum('monday'));
        if (request()->has('export')){
            $totals = [
                'monday' => $timelines->sum('monday'),
                'tuesday' => $timelines->sum('tuesday'),
                'wednesday' => $timelines->sum('wednesday'),
                'thursday' => $timelines->sum('thursday'),
                'friday' => $timelines->sum('friday'),
                'saturday' => $timelines->sum('saturday'),
                'sunday' => $timelines->sum('sunday'),
                'total' => $timelines->sum('total')
            ];
            return Excel::download(new TimesheetListReportExport(['timelines' => $timelines->items()],['totals'=>$totals], 'reports.exports.export-timesheet-report'), 'timesheet-report.xlsx');
        }

        
        $resources = collect($this->resources($request))->toArray();
        asort($resources);

        $parameters = [
            'timelines' => $timelines,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => $resources,
            'task_drop_down' => $this->generateTaskDropdown($request),
            'user_stories_dropdown' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
            'feature_dropdown' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
            'epic_dropdown' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
            'invoice_status_dropdown' => InvoiceStatus::orderBy('description')->pluck('description', 'id'),
            'weeks_dropdown' => $this->generateYearWeek($request),
            'months_dropdown' => $this->generateYearMonth($request),
            'cost_center_dropdown' => CostCenter::orderBy('description')->pluck('description', 'id'),
            'billing_cycle_dropdown' => BillingCycle::orderBy('name')->pluck('name', 'id')
        ];

        return view('reports.timesheet')->with($parameters);
    }

    public function summary(Request $request)
    {
        if ((request()->filled('from_week') && request()->filled('to_week')) && ((int)request()->from_week > (int)request()->to_week))
            return redirect()->back()->with('flash_danger', 'Week To must be greater than week from');

        $report = $this->report()["report"];

        $perPage = request()->s??15;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentItems = array_slice($report, $perPage * ($currentPage - 1), $perPage);
        $report = new LengthAwarePaginator($currentItems, count($report), $perPage, $currentPage, ['path' => '/'.request()->path()]);
        $total = $this->total($report);

        $resources = collect($this->resources($request))->toArray();
        asort($resources);

        $parameters = [
            'reports' => $report,
            'weeks' => isset($this->report()["weeks"])?$this->report()["weeks"]:$this->weeksList(),
            'grand_total' => $total,
            'customer_drop_down' => Customer::where('status', 1)->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::whereIn('status_id', [1,2,3])->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::where('status_id', 1)->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => $resources,
            'task_drop_down' => $this->generateTaskDropdown($request),
            'epics_dropdown' => Epic::orderBy('name')->pluck('name', 'id'),
            'features_dropdown' => Feature::orderBy('name')->pluck('name', 'id'),
            'user_stories_dropdown' => UserStory::orderBy('name')->pluck('name', 'id'),
            'invoice_statuses_dropdown' => InvoiceStatus::orderBy('description')->pluck('description', 'id'),
            'weeks_dropdown' => $this->generateYearWeek($request),
            'months_dropdown' => $this->generateYearMonth($request),
            'cost_center_dropdown' => CostCenter::orderBy('description')->pluck('description', 'id'),
            'billing_cycle_dropdown' => BillingCycle::orderBy('name')->pluck('name', 'id')
        ];

        return view('reports.timesummary')->with($parameters);
    }

    public function exportxls()
    {
        return Excel::download(new TimesheetReportExport($this->report()['report'], isset($this->report()['weeks'])?$this->report()['weeks']:$this->weeksList(), $this->total($this->report()['report'])), 'timesheetsummary.xlsx');
    }

    private function resources(Request $request)
    {
        return Assignment::filters()->with(['consultant' => function($query){
            $query->selectRaw('id, CONCAT(`first_name`, " ", `last_name`) AS resource')->orderBy('first_name');
        }])->whereHas('consultant')->select('employee_id')->get()
            ->keyBy('employee_id')
            ->map(function ($assignment){
                return $assignment->consultant && $assignment->consultant->resource != '' ? $assignment->consultant->resource : null;
            })->sortBy('resource');
    }

    private function weeksDropdown()
    {
        $weeks_dropdown = [];

        for ($i = 0; $i <= 52; $i++){
            $week = now()->subWeeks($i)->endOfWeek()->year.' - '.(now()->subWeeks($i)->weekOfYear < 10 ? '0'.now()->subWeeks($i)->weekOfYear:now()->subWeeks($i)->weekOfYear);
            $weeks_dropdown[substr_replace($week,'',4,3)] =  $week;
        }

        return $weeks_dropdown;
    }

    private function monthsDropdowns()
    {
        $months_dropdown = [];

        for ($i = 0; $i <= 12; $i++){
            $months_dropdown[
            now()->subMonths($i)->year.(now()->subMonths($i)->month)
            ] =  now()->subMonths($i)->englishMonth.' Of '.now()->subMonths($i)->endOfWeek()->year;
        }

        return $months_dropdown;
    }

    private function startWeek()
    {
        return (request()->filled('from_week')) ?
            request()->from_week:
            now()->subWeeks(6)->year.(now()->subWeeks(6)->weekOfYear < 10?'0'.now()->subWeeks(6)->weekOfYear:now()->subWeeks(6)->weekOfYear);
    }

    private function endWeek()
    {
        return (request()->filled('to_week')) ?
            request()->to_week :
            now()->subWeek()->year.(now()->subWeek()->weekOfYear < 10?'0'.now()->subWeek()->weekOfYear:now()->subWeek()->weekOfYear);
    }

    private function weeksList()
    {
        $weeks = [];
        for ($i = 0 ; $i <= 5; $i++){
            if (request()->filled('from_week')){
                array_push($weeks, ((int)request()->from_week + $i));
            }elseif (request()->filled('to_week')){
                array_push($weeks, ((int)request()->to_week - $i));
            }else{
                $date = request()->from_date ? now()->setISODate(substr((int)request()->to_week, 0, 4), (int)substr(request()->to_week, 4)):now();
                array_push($weeks,
                           $date->copy()->subWeeks($i + 1)->year.($date->copy()->subWeeks($i + 1)->weekOfYear < 10?'0'.$date->copy()->subWeeks($i + 1)->weekOfYear:$date->copy()->subWeeks($i + 1)->weekOfYear));
            }
        }
        asort($weeks);

        return array_values($weeks);
    }

    private function report()
    {
        $start_week = $this->startWeek();
        $end_week = $this->endWeek();

        $timesheets = Timesheet::with(['employee' => function($employee){
            $employee->select(['id', 'first_name', 'last_name']);
        }, 'customer' => function($customer){
            $customer->select(['id', 'customer_name']);
        }, 'project' => function($project){
            $project->select(['id', 'name']);
        }, 'timeline' => function($timeline){
            $timeline->select(['timesheet_id', 'is_billable', 'mon', 'mon_m', 'tue', 'tue_m', 'wed', 'wed_m', 'thu', 'thu_m', 'fri', 'fri_m', 'sat', 'sat_m', 'sun', 'sun_m'])
                ->when(request()->filled('billable'), function ($timeline){
                    $timeline->where('is_billable', request()->billable);
                });
        }])->select(['id', 'employee_id', 'customer_id', 'project_id', 'year_week'])
            ->whereHas('timeline')
            ->when(request()->filled('customer'), function ($timesheet){
                $timesheet->where('customer_id', request()->customer);
            })
            ->when(request()->filled('project'), function ($timesheet){
                $timesheet->where('project_id', request()->project);
            })
            ->when(request()->filled('company'), function ($timesheet){
                $timesheet->where('company_id', request()->company);
            })
            ->when(request()->filled('resource'), function ($timesheet){
                $timesheet->where('employee_id', request()->resource);
            })
            ->when(request()->filled('billable'), function ($timesheet){
                $timesheet->whereHas('timeline', function ($timeline){
                    $timeline->where('is_billable', request()->billable);
                });
            })
            ->when(request()->filled('epic'), function ($timesheet){
                $timesheet->whereHas('project.epics', function ($epic){
                    $epic->where('id', request()->epic);
                });
            })
            ->when(request()->filled('feature'), function ($timesheet){
                $timesheet->whereHas('project.epics', function ($epic){
                    $epic->whereHas('features', function ($feature){
                        $feature->where('id', request()->feature);
                    });
                });
            })
            ->when(request()->filled('user_story'), function ($timesheet){
                $timesheet->whereHas('project.epics', function ($epic){
                    $epic->whereHas('features', function ($feature){
                        $feature->whereHas('user_stories', function ($story){
                            $story->where('id', request()->user_story);
                        });
                    });
                });
            })
            ->when(request()->filled('task'), function ($timesheet){
                $timesheet->whereHas('timeline', function ($timeline){
                    $timeline->where('task_id', request()->task);
                });
            })
            ->when(request()->filled('invoice_status'), function ($timesheet){
                $timesheet->where('bill_status', request()->invoice_status);
            })
            ->when(request()->filled('from_week'), function ($timesheet){
                $timesheet->where('year_week', '>=', request()->from_week);
            })
            ->when(request()->filled('to_week'), function ($timesheet){
                $timesheet->where('year_week', '<=', request()->to_week);
            })
            ->when(request()->filled('from_month'), function ($timesheet){
                $timesheet->where('year', substr(request()->from_month, 0, 4))
                    ->where('month', '>=', substr(request()->from_month, 4));
            })
            ->when(request()->filled('to_month'), function ($timesheet){
                $timesheet->where('year', substr(request()->to_month, 0,4))
                    ->where('month', '<=', substr(request()->to_month, 4));
            })
            ->when(request()->filled('cost_center'), function ($timesheet){
                $timesheet->whereHas('project.assignment.resource_user', function ($resource){
                    $resource->where('cost_center_id', request()->cost_center);
                });
            })
            ->when(request()->filled('billing_period_id'), function ($timesheet){
                $billing_period = BillingPeriod::find(request()->billing_period_id, ['start_date', 'end_date']);
                if (isset($billing_period)){
                    $start_week = Carbon::parse($billing_period->start_date);
                    $start_week = $start_week->copy()->year.($start_week->copy()->weekOfYear < 10 ? '0'.$start_week->copy()->weekOfYear:$start_week->copy()->weekOfYear);
                    $end_week = Carbon::parse($billing_period->end_date);
                    $end_week = $end_week->copy()->year.($end_week->copy()->weekOfYear < 10 ? '0'.$end_week->copy()->weekOfYear:$end_week->copy()->weekOfYear);
                    $timesheet->whereBetween('year_week', [$start_week, $end_week]);
                }
            });

        if ($this->isDateFilterPresent()){
            $timesheets = $timesheets->whereBetween('year_week', [$this->weeksList()[0]??$start_week, $this->weeksList()[count($this->weeksList()) - 1]??$end_week])
                ->get();
        }else{
            $timesheets = $timesheets->get()->groupBy('year_week');

            if (request()->filled('from_week')){
                $timesheets = $timesheets->take(6);
                $weeks = $timesheets->keys();
                $timesheets = $timesheets->values()->flatten();
            }

            if (!request()->filled('from_week') && request()->filled('to_week')){
                $timesheets = $timesheets->take(-6);
                $weeks = $timesheets->keys();
                $timesheets = $timesheets->values()->flatten();
            }

            if (request()->filled('from_month')){
                $timesheets = $timesheets->take(6);
                $weeks = $timesheets->keys();
                $timesheets = $timesheets->values()->flatten();
            }

            if (!request()->filled('from_month') && request()->filled('to_month')){
                $timesheets = $timesheets->take(-6);
                $weeks = $timesheets->keys();
                $timesheets = $timesheets->values()->flatten();
            }

            if (request()->filled('billing_period_id')){
                $timesheets = $timesheets->take(6);
                $weeks = $timesheets->keys();
                $timesheets = $timesheets->values()->flatten();
            }
        }

        $tmp_timesheets = $timesheets->map(function ($timesheet){
            return [
                'employee' => $timesheet->employee->name()??null,
                'customer' => $timesheet->customer->customer_name??null,
                'project' => $timesheet->project->name??null,
                'weeks' => [],
            ];
        })->unique()->values();

        $report = [];

        foreach ($tmp_timesheets as $tmp_timesheet){
            foreach ($timesheets as $timesheet){
                if (
                    $tmp_timesheet["employee"] === $timesheet->employee->name()
                    && $tmp_timesheet["customer"] === $timesheet->customer->customer_name
                    && $tmp_timesheet["project"] === $timesheet->project->name
                ){
                    $hours = (($timesheet->timeline->sum(function($timeline){
                                return $timeline->mon + $timeline->tue +$timeline->wed +$timeline->thu +$timeline->fri
                                    +$timeline->sat +$timeline->sun;
                            }) * 60) + $timesheet->timeline->sum(function($timeline){
                            return $timeline->mon_m +$timeline->tue_m +$timeline->wed_m +$timeline->thu_m
                                +$timeline->fri_m +$timeline->sat_m +$timeline->sun_m;
                        }));
                    $tmp_timesheet["weeks"][$timesheet->year_week] = isset($tmp_timesheet["weeks"][$timesheet->year_week]) ?
                        ($tmp_timesheet["weeks"][$timesheet->year_week] + $hours) : $hours;
                }
            }
            $tmp_timesheet["total"] = array_sum(array_values($tmp_timesheet["weeks"]));
            array_push($report, $tmp_timesheet);
            $tmp_timesheet["weeks"][$timesheet->year_week] = 0;
        }

        $parameters = ['report' => $report];
        if (isset($weeks)){
            $parameters['weeks'] = $weeks->toArray();
        }

        return $parameters;
    }

    private function total( $report)
    {
        $grand_total = [];
        $weeks = $this->report()['weeks']??$this->weeksList();
        foreach ($weeks as $week){
            $report = gettype($report) === "array" ? collect($report) : $report;
            $local = $report->sum(function ($rep) use($week){
                return $rep["weeks"][$week]??0;
            });

            $grand_total[$week] = $local > 0 ? $local/60 : 0;
        }
        return $grand_total;
    }

    private function isDateFilterPresent()
    {
        return !request()->filled('from_week')
            && !request()->filled('to_week')
            && !request()->filled('from_month')
            && !request()->filled('to_month')
            && !request()->filled('billing_period_id');
    }
    
    public function generateYearMonth(Request $request)
    {
        $years = TimeSheet::filters()->selectRaw("DISTINCT(CONCAT(`year`,'-',CASE WHEN `month` < 10 THEN CONCAT('0',`month`) ELSE `month` END)) as yearmonth")->pluck('yearmonth');

        if(count($years) == 0){
            $years2 = TimeSheet::distinct('year')->orderBy('year', 'desc')->pluck('year');

            $year_month_drop_down = [];
            foreach ($years2 as $year) {
                for ($i = 12; $i >= 1; $i--) {
                    if (($year == date('Y')) && ($i > date('m'))) {
                        continue;
                    }
                    if ($i < 10) {
                        $i = '0' . $i;
                    }
                    $year_month_drop_down[$year . '-' . $i] = $year . $i;
                }
            }
        } else {
            $year_month_drop_down = [];
            if(isset($request->from_month)){ $year_month_drop_down[$request->from_month] = str_replace('-','',$request->from_month); }
            if(isset($request->to_month) && $request->to_month != $request->from_month){ $year_month_drop_down[$request->to_month] = str_replace('-','',$request->to_month); }
            foreach ($years as $key => $year) {
                if(str_replace('-','',$year) != $request->to_month && str_replace('-','',$year) != $request->from_month){
                    $year_month_drop_down[$year] = str_replace('-','',$year);
                }
            }
        }
        arsort($year_month_drop_down);
        return $year_month_drop_down;
    }

    public function generateYearWeek(Request $request)
    {
        $weeks = TimeSheet::filters()->orderBy('year_week', 'desc')->whereNotNull('year_week')->pluck('year_week', 'year_week');

        if(count($weeks) == 0){
            $year_week_drop_down = TimeSheet::distinct('year_week')->orderBy('year_week', 'desc')->pluck('year_week','year_week');
        } else {
            $year_week_drop_down = collect($weeks)->toArray();
            if(isset($request->from_week)){ $year_week_drop_down[$request->from_week] = $request->from_week; }
            if(isset($request->to_week) && $request->to_week != $request->from_week){ $year_week_drop_down[$request->to_week] = $request->to_week; }
        arsort($year_week_drop_down);
        }
        return $year_week_drop_down;
    }

    
    public function generateTaskDropdown(Request $request){
        $list = Task::when(request()->employee,function($q){
            $q->where('employee_id', request()->employee);
        })->when(request()->resource,function($q){
            $q->where('employee_id', request()->resource);
        })->when(request()->project,function($q){
            $q->where('project_id','=', request()->project);
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
            $task->where('customer_id', request()->customer);
        })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
            $task->whereHas('project.company', function ($t) {
                $t->where('id', '=', request()->company);
            });
        })->orderBy('description')->pluck('description', 'id');

        return $list;
    }

    public function timesheetapi(Request $request)
    {
        $timelines = Timeline::with(['task' => function($query){
            $query->select('id', 'description');
        }, 'timesheet' => function($q){
            $q->select(['id', 'employee_id', 'project_id', 'customer_id', 'company_id', 'year_week'])
                ->with([
                    'user' => function($q){
                        $q->selectRaw('id, CONCAT(`first_name`," ",`last_name`) AS resource');
                    },
                    'project' => function($q){
                    $q->select('id', 'name', 'customer_id');
                }]);
        }])->selectRaw(
            'timesheet_id, description_of_work, SUM((mon * 60) + mon_m) AS monday, SUM((tue * 60) + tue_m) AS tuesday,
             SUM((wed * 60) + wed_m) AS wednesday, SUM((thu * 60) + thu_m) AS thursday, SUM((fri * 60) + fri_m) AS friday,
              SUM((sat * 60) + sat_m) AS saturday, SUM((sun * 60) + sun_m) AS sunday, SUM((total * 60) + total_m) AS total,
              is_billable, task_id'
        )
        ->when(request()->customer, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('customer_id', request()->customer);
            });
        })->when(request()->project, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('project_id', request()->project);
            });
        })->when(request()->company, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(request()->resource, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('employee_id', request()->resource);
            });
        })->when(request()->billable != null, function ($timeline){
            $timeline->where('is_billable', request()->billable);
        })->when(request()->task, function ($timeline){
            $timeline->where('task_id', request()->task);
        })->when(request()->user_story, function ($timeline){
            $timeline->whereHas('task', function ($q){
                $q->where('user_story_id', request()->user_story);
            });
        })->when(request()->feature, function ($timeline){
            $timeline->whereHas('task.user_story', function ($q){
                $q->where('feature_id', request()->feature);
            });
        })->when(request()->epic, function ($timeline){
            $timeline->whereHas('task.user_story.feature', function ($q){
                $q->where('epic_id', request()->epic);
            });
        })->when(request()->invoice_status, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('bill_status', request()->invoice_status);
            });
        })->when(request()->from_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '>=', request()->from_week);
            });
        })->when(request()->to_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '<=', request()->to_week);
            });
        })->when(request()->from_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','>=', substr(request()->from_month,5))
                    ->where('year', '>=', substr(request()->from_month, 0, 4));
            });
        })->when(request()->filled('billing_period_id'), function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $billing_period = BillingPeriod::find(request()->billing_period_id);
                if (isset($billing_period)){
                    $q->where('first_day_of_month', '>=', $billing_period->start_date)
                        ->where('last_day_of_month', '<=', $billing_period->end_date);
                }
            });
        })->when(request()->to_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','<=', substr(request()->to_month,5))
                ->where('year', '<=', substr(request()->to_month, 0, 4));
            });
        })->when(request()->cost_center, function ($timeline){
            $timeline->whereHas('timesheet.project.assignment.resource_user', function ($q){
                $q->where('cost_center_id', request()->cost_center);
            });
        })
        ->groupBy(['timesheet_id', 'description_of_work', 'is_billable', 'task_id'])
            ->orderBy('timesheet_id', 'desc')->get();

        $auth_token = Str::replaceFirst('Bearer ', '', $request->header('Authorization'));
        $api_user = Api::where('api_token', $auth_token)->first();
        $user = User::find($api_user->user);

        $api_requests = new ApiRequests();
        $api_requests->user = $user->id;
        $api_requests->access_token = $auth_token;
        $api_requests->request_url = $request->fullUrl();
        $api_requests->request_from = $request->ip();
        $api_requests->save();

        return $timelines;
    }

    public function timesheetApiRender(Request $request)
    {
        $timelines = Timeline::with(['task' => function($query){
            $query->select('id', 'description');
        }, 'timesheet' => function($q){
            $q->select(['id', 'employee_id', 'project_id', 'customer_id', 'company_id', 'year_week'])
                ->with([
                    'user' => function($q){
                        $q->selectRaw('id, CONCAT(`first_name`," ",`last_name`) AS resource');
                    },
                    'project' => function($q){
                    $q->select('id', 'name', 'customer_id');
                }]);
        }])->selectRaw(
            'timesheet_id, description_of_work, SUM((mon * 60) + mon_m) AS monday, SUM((tue * 60) + tue_m) AS tuesday,
             SUM((wed * 60) + wed_m) AS wednesday, SUM((thu * 60) + thu_m) AS thursday, SUM((fri * 60) + fri_m) AS friday,
              SUM((sat * 60) + sat_m) AS saturday, SUM((sun * 60) + sun_m) AS sunday, SUM((total * 60) + total_m) AS total,
              is_billable, task_id'
        )->when(request()->customer, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('customer_id', request()->customer);
            });
        })->when(request()->project, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('project_id', request()->project);
            });
        })->when(request()->company, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(request()->resource, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('employee_id', request()->resource);
            });
        })->when(request()->billable, function ($timeline){
            $timeline->where('is_billable', request()->billable);
        })->when(request()->task, function ($timeline){
            $timeline->where('task_id', request()->task);
        })->when(request()->user_story, function ($timeline){
            $timeline->whereHas('task', function ($q){
                $q->where('user_story_id', request()->user_story);
            });
        })->when(request()->feature, function ($timeline){
            $timeline->whereHas('task.user_story', function ($q){
                $q->where('feature_id', request()->feature);
            });
        })->when(request()->epic, function ($timeline){
            $timeline->whereHas('task.user_story.feature', function ($q){
                $q->where('epic_id', request()->epic);
            });
        })->when(request()->invoice_status, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('bill_status', request()->invoice_status);
            });
        })->when(request()->from_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '>=', request()->from_week);
            });
        })->when(request()->to_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '<=', request()->to_week);
            });
        })->when(request()->from_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','>=', substr(request()->from_month,5))
                    ->where('year', '>=', substr(request()->from_month, 0, 4));
            });
        })->when(request()->filled('billing_period_id'), function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $billing_period = BillingPeriod::find(request()->billing_period_id);
                if (isset($billing_period)){
                    $q->where('first_day_of_month', '>=', $billing_period->start_date)
                        ->where('last_day_of_month', '<=', $billing_period->end_date);
                }
            });
        })->when(request()->to_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','<=', substr(request()->to_month,5))
                ->where('year', '<=', substr(request()->to_month, 0, 4));
            });
        })->when(request()->cost_center, function ($timeline){
            $timeline->whereHas('timesheet.project.assignment.resource_user', function ($q){
                $q->where('cost_center_id', request()->cost_center);
            });
        })->groupBy(['timesheet_id', 'description_of_work', 'is_billable', 'task_id'])
            ->orderBy('timesheet_id', 'desc')->paginate(request()->s??15);
        
        $resources = collect($this->resources($request))->toArray();
        asort($resources);

        $parameters = [
            'timelines' => $timelines,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => $resources,
            'task_drop_down' => $this->generateTaskDropdown($request),
            'user_stories_dropdown' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
            'feature_dropdown' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
            'epic_dropdown' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
            'invoice_status_dropdown' => InvoiceStatus::orderBy('description')->pluck('description', 'id'),
            'weeks_dropdown' => $this->generateYearWeek($request),
            'months_dropdown' => $this->generateYearMonth($request),
            'cost_center_dropdown' => CostCenter::orderBy('description')->pluck('description', 'id'),
            'billing_cycle_dropdown' => BillingCycle::orderBy('name')->pluck('name', 'id')
        ];

        return view('reports.timesheet_api')->with($parameters);
    }
}
