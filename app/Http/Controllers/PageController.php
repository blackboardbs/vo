<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function getFavoritablePages()
    {
        $pages = Page::where('user_id', null)->orWhere('user_id', Auth::user()->id)->get();
        return response()->json(['pages' => $pages]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            'icon' => 'required',
        ]);

        $page = Page::create($request->all());
        return redirect()->back()->with('success', 'Favourite created successfully.');
    }
    
}
