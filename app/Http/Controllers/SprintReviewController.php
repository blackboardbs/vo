<?php

namespace App\Http\Controllers;

use App\Models\SprintReview;
use Illuminate\Http\Request;

class SprintReviewController extends Controller
{
    public function index($sprint_id)
    {
        $sprint_reviews = SprintReview::where('sprint_id', $sprint_id)->get()->groupBy('review_type');

        return response()->json($sprint_reviews);
    }

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $sprint_id)
    {
        SprintReview::insert([
            'sprint_id' => $sprint_id,
            'review_type' => $request->review_type,
            'body' => $request->body
        ]);

        return response()->json(['message' => 'Sprint review saved successfully']);
    }

    /**
     * Display the specified resource.
     */
    public function show(SprintReview $sprintReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SprintReview $sprintReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SprintReview $sprintreview)
    {
        $sprintreview->body = $request->body;
        $sprintreview->save();

        return response()->json(['message' => 'Sprint review was updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SprintReview $sprintreview)
    {
        $sprintreview->delete();

        return response()->json(['message' => 'Sprint review was deleted']);
    }
}
