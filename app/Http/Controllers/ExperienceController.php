<?php

namespace App\Http\Controllers;

use App\Models\CvCompany;
use App\Models\Experience;
use App\Http\Requests\StoreExperienceRequest;
use App\Http\Requests\UpdateExperienceRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    }

    public function create($cvid, $res_id): View
    {
        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }
        $parameters = [
            'cv' => $cvid,
            'res' => $res_id,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'company_drop_down' => CvCompany::orderBy('id')->pluck('description', 'id')->prepend('Company', '0'),
            'row_order' => $order_number,
        ];

        return view('experience.create')->with($parameters);
    }

    public function store(StoreExperienceRequest $request, $cvid, $res_id): RedirectResponse
    {
        $start = $request->input('start_year').$request->input('start_month');
        $end = $request->input('end_year').$request->input('end_month');

        $experience = new Experience;
        $experience->res_id = $res_id;
        $experience->company = $request->input('company');
        $experience->company_id = $request->input('company_id');
        $experience->period_start = $start;
        $experience->period_end = $end;
        $experience->current_project = $request->input('current');
        $experience->role = $request->input('role');
        $experience->project = $request->input('project');
        $experience->responsibility = $request->input('responsibility');
        $experience->tools = $request->input('tools');
        $experience->reason_for_leaving = $request->input('reason_for_leaving');
        $experience->row_order = $request->input('row_order');
        $experience->status_id = $request->input('status_id');
        $experience->creator_id = auth()->id();
        $experience->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Experience captured successfully');
    }

    public function show($experience, Request $request): View
    {
        $cvid = $request->input('cvid');

        $parameters = [
            'experience' => Experience::where('id', '=', $experience)->first(),
            'cvid' => $cvid,
        ];

        return view('experience.show')->with($parameters);
    }

    public function edit($experienceid, $cvid, $res_id): View
    {
        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }
        $parameters = [
            'experience' => Experience::where('id', '=', $experienceid)->where('res_id', '=', $res_id)->get(),
            'cv' => $cvid,
            'res' => $res_id,
            'company_drop_down' => CvCompany::orderBy('id')->pluck('description', 'id')->prepend('Availability Status', '0'),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'row_order' => $order_number,
        ];

        return view('experience.edit')->with($parameters);
    }

    public function update(UpdateExperienceRequest $request, $experienceid, $cvid, $res_id): RedirectResponse
    {
        $start = $request->input('start_year').$request->input('start_month');
        $end = $request->input('end_year').$request->input('end_month');

        $experience = Experience::find($experienceid);
        $experience->company = $request->input('company');
        $experience->company_id = $request->input('company_id');
        $experience->period_start = $start;
        $experience->period_end = $end;
        $experience->current_project = $request->input('current');
        $experience->role = $request->input('role');
        $experience->project = $request->input('project');
        $experience->responsibility = $request->input('responsibility');
        $experience->tools = $request->input('tools');
        $experience->reason_for_leaving = $request->input('reason_for_leaving');
        $experience->row_order = $request->input('row_order');
        $experience->status_id = $request->input('status_id');
        $experience->creator_id = auth()->id();
        $experience->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Experience saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        DB::table('cv')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Experience::destroy($id);

        return redirect()->route('cv.index')->with('success', 'Experience deleted successfully');
    }
}
