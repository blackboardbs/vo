<?php

namespace App\Http\Controllers;

use App\Models\BillingPeriod;
use App\Http\Requests\BillingPeriodRequest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class BillingPeriodController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(BillingPeriod::where('billing_cycle_id', \request()->bill_id)->orderBy('start_date')->get());
    }

    public function create(): JsonResponse
    {
        $period = BillingPeriod::where('billing_cycle_id', \request()->bill_cycle)->latest('period_number')->first();

        if (! isset($period->start_date)) {
            $start_date = now();
            $end_date = $start_date->copy()->addMonth();
        } else {
            $start_date = Carbon::parse($period->end_date)->addDay();
            $end_date = $start_date->copy()->addDays(Carbon::parse($period->start_date)->diffInDays(Carbon::parse($period->end_date)));
        }

        $parameter = [
            'year' => $period->year ?? null,
            'period_number' => isset($period->period_number) ? ($period->period_number + 1) : 1,
            'start_date' => $start_date->toDateString(),
            'end_date' => $end_date->toDateString(),
        ];

        return response()->json($parameter);
    }

    public function store(BillingPeriodRequest $request): JsonResponse
    {
        if ($this->validatePeriod($request)) {
            return response()->json(['error' => $this->validatePeriod($request)], 500);
        }

        $period = new BillingPeriod();
        $period->billing_cycle_id = $request->billing_cycle_id;
        $period->year = $request->year;
        $period->period_name = $request->period_name;
        $period->period_number = $request->period_number;
        $period->start_date = Carbon::parse($request->start_date)->toDateString();
        $period->end_date = Carbon::parse($request->end_date)->toDateString();
        $period->save();

        return response()->json(['message' => 'your item was created successfully']);
    }

    public function show(BillingPeriod $period)
    {
    }

    public function edit(BillingPeriod $period)
    {
    }

    public function update(BillingPeriodRequest $request, BillingPeriod $period): JsonResponse
    {
        $validation = $this->updateValidation($request, $period);
        if ($validation) {
            return response()->json(['error' => $validation], 500);
        }

        $period->year = $request->year;
        $period->period_name = $request->period_name;
        $period->period_number = $request->period_number;
        $period->start_date = Carbon::parse($request->start_date)->toDateString();
        $period->end_date = Carbon::parse($request->end_date)->toDateString();
        $period->save();

        return response()->json(['message' => 'Your operation was successful']);
    }

    public function destroy(BillingPeriod $period): JsonResponse
    {
        $period->delete();

        return response()->json(['message' => 'deleted successfully']);
    }

    public function updateValidation($request, $billing_period)
    {
        $periods = BillingPeriod::where('billing_cycle_id', $request->billing_cycle_id);
        $periods2 = clone $periods;
        $periods3 = clone $periods;

        if ($request->period_number != $billing_period->period_number) {
            if ($periods->where('period_number', $request->period_number)->exists()) {
                return 'Period number already exist';
            }
        }

        if ($request->start_date != $billing_period->start_date) {
            $periods3 = $periods3->where('start_date', '<', $billing_period->start_date)->orderBy('start_date', 'DESC')->first();
            if (isset($periods3) && Carbon::parse($request->start_date)->toDateString() <= $periods3->end_date) {
                return "Your start date should be greater than $periods3->end_date";
            }
        }

        if ($request->end_date != $billing_period->end_date) {
            $periods2 = $periods2->where('period_number', '>', $request->period_number)->orderBy('start_date')->first();
            if (isset($periods2) && Carbon::parse($request->end_date)->toDateString() >= $periods2->start_date) {
                return "Your end date should be less $periods2->start_date";
            }
        }
    }

    public function validatePeriod($request)
    {
        $periods = BillingPeriod::where('billing_cycle_id', $request->billing_cycle_id);
        $periods2 = clone $periods;
        $periods3 = clone $periods;
        if ($periods->where('period_number', $request->period_number)->exists()) {
            return 'Period number already exist';
        }

        $periods2 = $periods2->where('period_number', '>', $request->period_number)->first();

        if (isset($periods2) && Carbon::parse($request->end_date)->toDateString() >= $periods2->start_date) {
            return "Your end date should less $periods2->start_date";
        }

        $periods3 = $periods3->orderBy('start_date', 'DESC')->first();
        if (! isset($periods2) && isset($periods3) && Carbon::parse($request->start_date)->toDateString() <= $periods3->end_date) {
            return "Your start date should be greater than $periods3->end_date";
        }
    }
}
