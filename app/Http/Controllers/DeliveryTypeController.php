<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryTypeRequest;
use App\Models\Status;
use App\Models\TaskDeliveryType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DeliveryTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;
        
        $delivery_type = TaskDeliveryType::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $delivery_type = $delivery_type->where('status_id', $request->input('status_filter'));
            }else{
                $delivery_type = $delivery_type->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $delivery_type = $delivery_type->where('name', 'like', '%'.$request->input('q').'%');
        }

        $delivery_type = $delivery_type->paginate($item);

        $parameters = [
            'delivery_type' => $delivery_type,
        ];


        return view('master_data.delivery_type.index')->with($parameters);
    }


    public function create()
    {

    }

    public function store(Request $request): RedirectResponse
    {
        $delivery_type = new TaskDeliveryType();
        $delivery_type->name = $request->name;
        $delivery_type->status_id = $request->status;
        $delivery_type->creator_id = auth()->id();
        $delivery_type->save();

        return redirect()->back();
    }

    public function show($id): View
    {
        return view('master_data.delivery_type.show')->with(['type' => TaskDeliveryType::find($id)]);
    }

    public function edit($id): View
    {
        $delivery_type = TaskDeliveryType::find($id, ['name', 'status_id', 'id']);
        $status_dropdown = Status::pluck('description', 'id')->prepend('Select Status', '0');
        $parameters = [
            'delivery_type' => $delivery_type,
            'status_dropdown' => $status_dropdown
        ];

        return view('master_data.delivery_type.edit')->with($parameters);
    }

    public function update(DeliveryTypeRequest $request, $id): RedirectResponse
    {
        $delivery_type = TaskDeliveryType::find($id);
        $delivery_type->name = $request->name;
        $delivery_type->status_id = $request->status;
        $delivery_type->save();

        return redirect(route('deliverytype.index'))->with('flash_success', 'Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // TaskDeliveryType::destroy($id);

        $item = TaskDeliveryType::find($id);
        $item->status_id = 2;
        $item->save();
        return redirect()->back()->with('flash_success', 'suspended successfully');
    }
}
