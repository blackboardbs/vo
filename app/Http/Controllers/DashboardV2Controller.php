<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Company;
use App\Models\Config;
use App\Dashboard\Assignments;
use App\Dashboard\Cashflow;
use App\Dashboard\DebtorsAgeing;
use App\Dashboard\HelperFunctions;
use App\Dashboard\MonthlyConsulting;
use App\Dashboard\PlannedIncome;
use App\Dashboard\Utilization;
use App\Dashboard\VatRate as vat_rate;
use App\Dashboard\WeeklyUtilization;
use App\Models\PlannedExpense;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\VatRate;
use Illuminate\Http\Request;

class DashboardV2Controller extends Controller
{
    //private  $dashUtils;
    private $helperFunctions;

    private $weeklyUtilization;

    private $cashflow;

    private $assignment;

    private $utilization;

    private $debtors;

    private $monthlyConsulting;

    private $vat_rate;

    private $config;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function init(){
        $this->vat_rate = new vat_rate();
        $this->helperFunctions = new HelperFunctions();
        $this->utilization = new Utilization();
        $this->assignment = new Assignments();
        $this->weeklyUtilization = new WeeklyUtilization();
        $this->cashflow = new Cashflow();
        $this->debtors = new DebtorsAgeing();
        $this->monthlyConsulting = new MonthlyConsulting();
        $this->config = Config::first();
    }

    public function index(Request $request)
    {
        $this->init();
        //OPEN ASSIGNMENT

        $open_assignments = $this->assignment->open_assignments($request, 10, 1)[0];
        $completion_perc = $this->assignment->open_assignments($request, 10, 1)[2];
        $outstanding_amount = $this->assignment->open_assignments($request, 10, 1)[3];
        $total_outstanding_amount = $this->assignment->open_assignments($request, 10, 1)[1];
        $company = $this->assignment->open_assignments($request, 10, 1)[5];
        $configs = $this->assignment->open_assignments($request, 10, 1)[4];

        //END OF OPEN ASSIGNMENT

        $date = $this->helperFunctions->date??null;
        //WEEKLY UTILIZATION
        $weekly_income_utilization = $this->weeklyUtilization->weekly_utilization(1, 1, $request);
        $weekly_cost_utilization = $this->weeklyUtilization->weekly_utilization(0, 2, $request);

        //END OF WEEKLY UTILIZATION

        $this_month_vat = 0;
        $next_month_vat = 0;
        $next_2_month_vat = 0;
        $next_3_month_vat = 0;
        foreach ($this->get_planned_income_tax(now(), 0, 2) as $this_month_tax) {
            if (isset($this_month_tax->company) && $this_month_tax->company->vat_no != null && isset($this_month_tax->customer) && $this_month_tax->company->country_id == $this_month_tax->customer->country_id) {
                $this_month_vat += $this_month_tax->value * (VatRate::where('vat_code', '=', $configs->vat_rate_id)->where('end_date', '>', now()->now()->toDateString())->first()->vat_rate / 100);
            }
        }
        foreach ($this->get_planned_income_tax(now(), 1, 2) as $next_month_tax) {
            if (isset($next_month_tax->company) && $next_month_tax->company->vat_no != null && isset($next_month_tax->customer) && $next_month_tax->company->country_id == $next_month_tax->customer->country_id) {
                $next_month_vat += $next_month_tax->value * (VatRate::where('vat_code', '=', $configs->vat_rate_id)->where('end_date', '>', now()->now()->toDateString())->first()->vat_rate / 100);
            }
        }
        foreach ($this->get_planned_income_tax(now(), 2, 2) as $next_2_month_tax) {
            if (isset($next_2_month_tax->company) && $next_2_month_tax->company->vat_no != null && isset($next_2_month_tax->customer) && $next_2_month_tax->company->country_id == $next_2_month_tax->customer->country_id) {
                $next_2_month_vat += $next_2_month_tax->value * (VatRate::where('vat_code', '=', $configs->vat_rate_id)->where('end_date', '>', now()->now()->toDateString())->first()->vat_rate / 100);
            }
        }
        foreach ($this->get_planned_income_tax(now(), 3, 2) as $next_3_month_tax) {
            if (isset($next_3_month_tax->company) && $next_3_month_tax->company->vat_no != null && isset($next_3_month_tax->customer) && $next_3_month_tax->company->country_id == $next_3_month_tax->customer->country_id) {
                $next_3_month_vat += $next_3_month_tax->value * (VatRate::where('vat_code', '=', $configs->vat_rate_id)->where('end_date', '>', now()->now()->toDateString())->first()->vat_rate / 100);
            }
        }

        $other_expenses_account_1 = [];
        $other_expenses_account_2 = [];
        $other_expenses_account_3 = [];
        $other_expenses_Value_1 = [];
        $other_expenses_value_2 = [];
        $other_expenses_value_3 = [];

        foreach ($this->other_expenses(now(), 0) as $expense) {
            if ($expense->plan_exp_date >= now()->now()->startOfMonth()->toDateString() && $expense->plan_exp_date >= now()->now()->lastOfMonth()->toDateString()) {
                array_push($other_expenses_account_1, isset($expense->account) ? $expense->account->description : '');
                array_push($other_expenses_Value_1, $expense->plan_exp_value);

                if (! array_unique($other_expenses_Value_1)) {
                    //for($i == 0; $i <= con)
                }
            }
            if ($expense->plan_exp_date >= now()->addMonth()->startOfMonth()->toDateString() && $expense->plan_exp_date >= now()->addMonth()->lastOfMonth()->toDateString()) {
                array_push($other_expenses_account_2, isset($expense->account) ? $expense->account->description : '');
                array_push($other_expenses_value_2, $expense->plan_exp_value);
            }
            if ($expense->plan_exp_date >= now()->addMonths(2)->startOfMonth()->toDateString() && $expense->plan_exp_date >= now()->addMonths(2)->lastOfMonth()->toDateString()) {
                array_push($other_expenses_account_3, isset($expense->account) ? $expense->account->description : '');
                array_push($other_expenses_value_3, $expense->plan_exp_value);
            }
        }

        //return array_unique($other_expenses_account_1);
        $planned = new PlannedIncome();
        $company_1 = $this->get_company(now(), 0);
        $company_2 = $this->get_company(now(), 1);
        $company_3 = $this->get_company(now(), 2);
        $company_4 = $this->get_company(now(), 3);

        try {
            $income_utilization = $this->utilization->resource_utilization($request)['income_utilization'];
            $total_income = $this->utilization->resource_utilization($request)['total_income'][0];
            $cost_utilization = $this->utilization->resource_utilization($request)['cost_utilization'];
            $total_cost = $this->utilization->resource_utilization($request)['total_cost'][0];
            $weeks = $this->utilization->resource_utilization($request)['weeks'];
        } catch (\Throwable $e) {
            return $this->utilization->resource_utilization($request);
        }

        $invoiced = $this->invoiceTotal($this->cashflow->invoiced($request, '*'));

        //return $this->cashflow->invoiced($request, '*');

        /*$expense_claims = $this->cashflow->invoiced($request, '*')->map(function ($expense){
            return $expense->time_exp;
        })
            ->flatten()
            ->groupBy('account_id')
            ->sum('amount');

        return $expense_claims;*/

        $not_invoiced = $this->cashflow->not_invoiced($request, '*')->map(function ($value) {
            if (($value->company->country_id == $value->customer->country_id) && ($value->company->vat_no != null)) {
                $local_vat = VatRate::where('vat_code', '=', $this->config->vat_rate_id)->where('end_date', '>=', $value->first_day_of_week)->first()->vat_rate;

                return ($value->hours * $value->rate) + (($value->hours * $value->rate) * ($local_vat / 100));
            } else {
                return $value->hours * $value->rate;
            }
            //return $value->hours * $value->rate;
        })->sum();

        $not_invoiced_tax = $this->cashflow->not_invoiced($request, '*')->map(function ($value) {
            if (($value->company->country_id == $value->customer->country_id) && ($value->company->vat_no != null)) {
                $local_vat = VatRate::where('vat_code', '=', $this->config->vat_rate_id)->where('end_date', '>=', $value->first_day_of_week)->first()->vat_rate;

                return ($value->hours * $value->rate) * ($local_vat / 100);
            } else {
                return 0;
            }
            //return $value->hours * $value->rate;
        })->sum();

        $vendor_invoiced = $this->invoiceTotal($this->cashflow->invoiced($request, '*', true), true);

        $vendor_not_invoiced = $this->cashflow->not_invoiced($request, '*', true)->map(function ($value) {
            return $value->hours * $value->rate;
        })->sum();

        $planned_expenses = $this->cashflow->planned_expenses($request, '*', true);

        $planned_exp = [
            'five_days' => $planned_expenses->sum('five_days'),
            'ten_days' => $planned_expenses->sum('ten_days'),
            'twenty_days' => $planned_expenses->sum('twenty_days'),
            'thirty_days' => $planned_expenses->sum('thirty_days'),
            'thirty_days_plus' => $planned_expenses->sum('thirty_days_plus'),
        ];

        $year_to_date_1 = $this->helperFunctions->get_year_to_date(0);
        $year_to_date_2 = $this->helperFunctions->get_year_to_date(1);
        $year_to_date_3 = $this->helperFunctions->get_year_to_date(2);
        $year_to_date_4 = $this->helperFunctions->get_year_to_date(3);

        $parameters = [
            'assignments' => $open_assignments,
            'completion_perc' => $completion_perc,
            'outstanding_amount' => $outstanding_amount,
            'total_outstanding_amount' => $total_outstanding_amount,
            'company_dropdown' => Company::where('status_id', '=', 1)->pluck('company_name', 'id'),
            'company' => $company,
            'date' => $date,
            'income_utilization' => $income_utilization,
            'total_income' => $total_income,
            'cost_utilization' => $cost_utilization,
            'total_cost' => $total_cost,
            'weeks' => $weeks,
            'weekly_income_utilization' => $weekly_income_utilization,
            'weekly_cost_utilization' => $weekly_cost_utilization,
            'planned_income' => $invoiced,
            'not_invoiced_tax' => $not_invoiced_tax,
            'planned_income_not_invoiced' => $not_invoiced,
            'invoiced_vendor' => $vendor_invoiced,
            'not_invoiced_vendor' => $vendor_not_invoiced,
            'planned_expenses' => $planned_expenses,
            'total_planned_exp' => $planned_exp,
            'expense_claim' => $planned->expense_tracking(),
            'total_this_month' => 0,
            'total_month_plus' => 0,
            'total_month_plus_2' => 0,
            'total_month_plus_3' => 0,
            'debtor_ageing' => $this->debtors->debtor_ageing($request),
            'grand_total_debtor' => 0,
            'grand_total_30_debtor' => 0,
            'grand_total_60_debtor' => 0,
            'grand_total_90_debtor' => 0,
            'grand_total_90_plus_debtor' => 0,
            'grand_total_creditor' => 0,
            'grand_total_30_creditor' => 0,
            'grand_total_60_creditor' => 0,
            'grand_total_90_creditor' => 0,
            'grand_total_90_plus_creditor' => 0,
            'creditors_ageing' => $this->debtors->debtor_ageing($request, true),
            'pipelines' => $this->helperFunctions->pipelines??[],
            'config' => $configs,
            'company_1' => $company_1,
            'company_2' => $company_2,
            'company_3' => $company_3,
            'company_4' => $company_4,
            'top_10_customers' => $this->helperFunctions->get_top_customers(now(), $request),
            'top_10_solutions' => $this->helperFunctions->get_top_solutions(now(), 'system', $request),
            'top_10_locations' => $this->helperFunctions->get_top_solutions(now(), 'country_id', $request),
            'monthly_consulting_1' => $this->helperFunctions->get_monthly_consulting(now(), 0),
            'monthly_consulting_2' => $this->helperFunctions->get_monthly_consulting(now(), 1),
            'monthly_consulting_3' => $this->helperFunctions->get_monthly_consulting(now(), 2),
            'monthly_value_3' => $this->monthlyConsulting->monthly_consulting()[0],
            'monthly_value_2' => $this->monthlyConsulting->monthly_consulting()[1],
            'monthly_value_1' => $this->monthlyConsulting->monthly_consulting()[2],
            'total_year_1_value' => $this->monthlyConsulting->monthly_consulting()[3],
            'total_year_2_value' => $this->monthlyConsulting->monthly_consulting()[5],
            'total_year_3_value' => $this->monthlyConsulting->monthly_consulting()[7],
            'total_year_1_hours' => $this->monthlyConsulting->monthly_consulting()[4],
            'total_year_2_hours' => $this->monthlyConsulting->monthly_consulting()[6],
            'total_year_3_hours' => $this->monthlyConsulting->monthly_consulting()[8],
            'total_year_4_value' => $this->monthlyConsulting->monthly_consulting()[9],
            'total_year_4_hours' => $this->monthlyConsulting->monthly_consulting()[10],
            'year_to_date_1' => $year_to_date_1,
            'year_to_date_2' => $year_to_date_2,
            'year_to_date_3' => $year_to_date_3,
            'year_to_date_4' => $year_to_date_4,
            'change_from_last_year' => $this->catchDivisionByZero($year_to_date_1->value??0,$year_to_date_2->value??0) * 100,
            'avg_rate' => $this->catchDivisionByZero($year_to_date_1->value??0, $year_to_date_1->hours??0),
            'change_from_last_year_1' => $this->catchDivisionByZero($year_to_date_2->value??0, $year_to_date_3->value??0) * 100,
            'avg_rate_1' => $this->catchDivisionByZero($year_to_date_2->value??0, $year_to_date_2->hours??0),
            'change_from_last_year_2' => $this->catchDivisionByZero($year_to_date_3->value??0, $year_to_date_4->value??0) * 100,
            'avg_rate_2' => $this->catchDivisionByZero($year_to_date_3->value??0, $year_to_date_3->hours??0)
        ];

        return view('reports.management.dashboard')->with($parameters);
    }

    private function get_planned_income($date, $month, $invoice_status)
    {
        $this->init();
        $amount = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))* assignment.invoice_rate),2) ELSE 0 END) AS value')
            ->leftJoin('timeline', 'timesheet.id', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->whereMonth('timesheet.invoice_due_date', '=', $date->now()->addMonths($month)->month)
            ->where('timesheet.bill_status', '=', $invoice_status)
            ->first();

        return $amount;
    }

    private function get_planned_income_tax($date, $month, $invoice_status)
    {
        $this->init();
        $amount = Timesheet::with(['company','customer'])->selectRaw('timesheet.company_id, timesheet.customer_id, SUM(CASE WHEN timeline.is_billable = 1 THEN ROUND(((timeline.total + (timeline.total_m/60))* assignment.invoice_rate),2) ELSE 0 END) AS value')
            ->leftJoin('timeline', 'timesheet.id', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->whereMonth('timesheet.invoice_due_date', '=', $date->now()->addMonths($month)->month)
            ->where('timesheet.bill_status', '=', $invoice_status)
            ->groupBy('timesheet.company_id')
            ->groupBy('timesheet.customer_id')
            ->get();

        return $amount;
    }

    private function get_refundable_expenses($month, $date)
    {
        $this->init();
        $expense = TimeExp::where('is_billable', '=', 1)
            ->whereHas('timesheet', function ($query) use ($month, $date) {
                $query->whereBetween('invoice_due_date', [$date->now()->addMonths($month)->startOfMonth()->toDateString(), $date->now()->addMonths($month)->endOfMonth()->toDateString()]);
            })
            ->sum('amount');

        return $expense;
    }

    private function get_company($date, $month)
    {
        $this->init();
        $company = Timesheet::select('company_id')
            ->whereBetween('invoice_due_date', [$date->now()->addMonths($month)->startOfMonth()->toDateString(), $date->now()->addMonths($month)->endOfMonth()->toDateString()])
            ->where('bill_status', '=', 2)
            ->groupBy('company_id')
            ->first();

        return $company;
    }

    private function planned_expenses($date, $months)
    {
        $this->init();
        $planned_expenses = PlannedExpense::whereMonth('plan_exp_date', '=', $date->now()->addMonths($months)->month)->sum('plan_exp_value');

        return $planned_expenses;
    }

    private function other_expenses($date, $months)
    {
        $this->init();
        $planned_expenses = PlannedExpense::with(['account'])->selectRaw('account_id, plan_exp_date, SUM(plan_exp_value) AS expense')->whereBetween('plan_exp_date', [$date->now()->startOfMonth()->toDateString(), $date->now()->addMonths(2)->lastOfMonth()->toDateString()])->groupBy('account_id', 'plan_exp_date')->get();

        return $planned_expenses;
    }

    /*private function exp_tracking($date, $month){
        $exp_tracking = ExpenseTracking::whereMonth('transaction')
    }*/

    private function invoiceTotal($invoice, $is_vendor = false)
    {
        $this->init();
        $temp_not_invoiced = $invoice->map(function ($invoice) use ($is_vendor) {
            $company = ($is_vendor) ? $invoice->user->vendor : $invoice->company;
            if (($company->country_id == $company->country_id) && ($company->vat_no != null)) {
                $local_vat = VatRate::where('vat_code', '=', $this->config->vat_rate_id)->where('end_date', '>=', $invoice->first_day_of_week)->first()->vat_rate;

                return [
                    'five_days_tax' => (($invoice->five_days * $invoice->rate) * ($local_vat / 100)),
                    'five_days' => ($invoice->five_days * $invoice->rate),
                    'ten_days_tax' => (($invoice->ten_days * $invoice->rate) * ($local_vat / 100)),
                    'ten_days' => ($invoice->ten_days * $invoice->rate),
                    'twenty_days_tax' => (($invoice->twenty_days * $invoice->rate) * ($local_vat / 100)),
                    'twenty_days' => ($invoice->twenty_days * $invoice->rate),
                    'thirty_days_tax' => (($invoice->thirty_days * $invoice->rate) * ($local_vat / 100)),
                    'thirty_days' => ($invoice->thirty_days * $invoice->rate),
                    'thirty_days_plus_tax' => (($invoice->thirty_days_plus * $invoice->rate) * ($local_vat / 100)),
                    'thirty_days_plus' => ($invoice->thirty_days_plus * $invoice->rate),
                ];
            } else {
                return [
                    'five_days_tax' => 0,
                    'five_days' => ($invoice->five_days * $invoice->rate),
                    'ten_days_tax' => 0,
                    'ten_days' => ($invoice->ten_days * $invoice->rate),
                    'twenty_days_tax' => 0,
                    'twenty_days' => ($invoice->twenty_days * $invoice->rate),
                    'thirty_days_tax' => 0,
                    'thirty_days' => ($invoice->thirty_days * $invoice->rate),
                    'thirty_days_plus_tax' => 0,
                    'thirty_days_plus' => ($invoice->thirty_days_plus * $invoice->rate),
                ];
            }
        });

        $collect_not_invoice = collect($temp_not_invoiced);

        $invoiced = [
            'five_days_tax' => $collect_not_invoice->sum('five_days_tax'),
            'five_days' => $collect_not_invoice->sum('five_days'),
            'ten_days_tax' => $collect_not_invoice->sum('ten_days_tax'),
            'ten_days' => $collect_not_invoice->sum('ten_days'),
            'twenty_days_tax' => $collect_not_invoice->sum('twenty_days_tax'),
            'twenty_days' => $collect_not_invoice->sum('twenty_days'),
            'thirty_days' => $collect_not_invoice->sum('thirty_days'),
            'thirty_days_tax' => $collect_not_invoice->sum('thirty_days_tax'),
            'thirty_days_plus' => $collect_not_invoice->sum('thirty_days_plus'),
            'thirty_days_plus_tax' => $collect_not_invoice->sum('thirty_days_plus_tax'),
        ];

        return $invoiced;
    }

    private function catchDivisionByZero(int|float $num1, int|float $num2): int|float
    {
        try {
            return ($num1/$num2);
        }catch (\DivisionByZeroError $error){
            return 0;
        }
    }
}
