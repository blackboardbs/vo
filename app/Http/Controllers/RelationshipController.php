<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRelationshipRequest;
use App\Http\Requests\UpdateRelationshipRequest;
use App\Models\Relationship;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RelationshipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $relationship = Relationship::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $relationship = $relationship->where('status', $request->input('status_filter'));
            }else{
                $relationship = $relationship->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $relationship = $relationship->where('description', 'like', '%'.$request->input('q').'%');
        }

        $relationship = $relationship->paginate($item);

        $parameters = [
            'relationship' => $relationship,
            //'status_description' => Status::where('status', '=', $status->status)->pluck('description')
        ];

        return View('master_data.relationship.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Relationship::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.relationship.create')->with($parameters);
    }

    public function store(StoreRelationshipRequest $request): RedirectResponse
    {
        $status = new Relationship();
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('relationship.index'))->with('flash_success', 'Master Data Relationship captured successfully');
    }

    public function show($relationship_id): View
    {
        $parameters = [
            'relationship' => Relationship::where('id', '=', $relationship_id)->get(),
        ];

        return View('master_data.relationship.show')->with($parameters);
    }

    public function edit($relationship_id): View
    {
        $autocomplete_elements = Relationship::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'relationship' => Relationship::where('id', '=', $relationship_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.relationship.edit')->with($parameters);
    }

    public function update(UpdateRelationshipRequest $request, $relationship_id): RedirectResponse
    {
        $status = Relationship::find($relationship_id);
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('relationship.index'))->with('flash_success', 'Master Data Relationship saved successfully');
    }

    public function destroy($relationship_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // Relationship::destroy($relationship_id);
        $item = Relationship::find($relationship_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('relationship.index')->with('success', 'Master Data Relationship suspended successfully');
    }
}
