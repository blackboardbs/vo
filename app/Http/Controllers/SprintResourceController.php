<?php

namespace App\Http\Controllers;

use App\Models\SprintResource;
use Illuminate\Http\Request;

class SprintResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($sprint_id)
    {
        $sprints = SprintResource::with([
            'resource:id,first_name,last_name',
            'function:id,description'
        ])->where('sprint_id', $sprint_id)->get();
        return response()->json($sprints);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(SprintResource $sprintResource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SprintResource $sprintResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SprintResource $sprintresource)
    {
        $sprintresource->capacity = $request->capacity;
        $sprintresource->save();

        return response()->json(['message' => 'Capacity updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SprintResource $sprintResource)
    {
        //
    }
}
