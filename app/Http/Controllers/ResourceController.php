<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Jobs\MasterServiceAgreementJob;
use App\Jobs\NonDisclosureAgreementJob;
use App\Models\AdvancedTemplates;
use App\Models\BBBEERace;
use App\Models\Commission;
use App\Models\Disability;
use App\Models\Gender;
use App\Http\Requests\UpdateResourceRequest;
use App\Models\MedicalCertificateType;
use App\Models\Race;
use App\Models\Resource;
use App\Models\Vendor;
use App\Services\ResourceService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ResourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, ResourceService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasViewPermission(), 403);

        $resource = Resource::with(['user','user.timesheets','user.assignments','cv','user.expense_tracking','user.tasks','user.invoice','user.vinvoice','status:id,description'])
            ->withCount(['team'])
            ->when($request->q, fn($resource) => $resource->whereHas('user', function ($res) use($request){
                $res->where('first_name', 'like', '%'.$request->q.'%')
                    ->orwhere('last_name', 'like', '%'.$request->q.'%')
                    ->orWhere('email', 'like', '%'.$request->q.'%')
                    ->orWhere('phone', 'like', '%'.$request->q.'%');
            }))->when(\auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager']),
                fn($resource) => $resource->when($request->resource_type_id, fn($res) => $res->where('resource_type', $request->resource_type_id))
                ->when($request->position_id, fn($res) => $res->where('resource_position', $request->position_id))
            ->when($request->level_id, fn($res) => $res->where('resource_level', $request->level_id))
            ->when($request->standard_cost_bracket_id, fn($res) => $res->where('standard_cost_bracket', $request->standard_cost_bracket_id))
            ->when($request->team_id, fn($res) => $res->where('team_id', $request->team_id))
            ->when($request->status_id, fn($res) => $res->where('status_id', $request->status_id))
            ->when(($request->util || $request->util == 0) && $request->util != null, fn($res) => $res->where('util', $request->util))
            ->when($request->cost_center_id, fn($res) => $res->where('cost_center_id', $request->cost_center_id))
            ->when($request->report_to_id, fn($res) => $res->where('manager_id', $request->report_to_id))
            )->latest()
            ->paginate($request->input('r') ?? 15);

        $parameters = [
            'resource' => $resource,
            'can_update' => $service->hasUpdatePermissions(),
            'managers' => $service->managersDropdown()
        ];

        if ($request->has('export')) return $this->export($parameters, 'resource');

        return view('resource.index', $parameters);
    }

    public function show(Resource $resource, ResourceService $service)
    {
        abort_unless($service->hasViewPermission(), 403);

        $parameters = [
            'resource' => $resource->load($service->relationships()),
            'can_update' => $service->hasUpdatePermissions(),
            'msg' => 'Create a cv profile',
        ];

        return view('resource.show')->with($parameters);
    }

    public function edit(Request $request, Resource $resource, ResourceService $service)
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $parameters = [
            'dropdown_process_commission' => Commission::orderBy('id')->pluck('description', 'id')->prepend('Commission', '0'),
            'msg' => 'Create a cv profile',
            'dropdown_process_medical_certificate_type' => MedicalCertificateType::orderBy('id')->pluck('description', 'id')->prepend('Medical Certificate Type', '0'),
            'dropdown_process_race' => Race::orderBy('id')->pluck('description', 'id')->prepend('Race', '0'),
            'dropdown_process_bee_race' => BBBEERace::orderBy('id')->pluck('description', 'id')->prepend('BEE Race', '0'),
            'dropdown_process_gender' => Gender::orderBy('id')->pluck('description', 'id')->prepend('Gender', '0'),
            'dropdown_process_disability' => Disability::orderBy('id')->pluck('description', 'id')->prepend('Disability', '0'),
            'resource' => $resource->load($service->relationships()),
            'leave_bal' => ($request->has('leave_bal')) ? $request->leave_bal : null,
            'nda_template_dropdown' => AdvancedTemplates::where('template_type_id', 8)->pluck('name', 'id')->prepend('Select NDA Template', '0'),
        ];

        return view('resource.edit')->with($parameters);
    }

    public function update(UpdateResourceRequest $request, Resource $resource, ResourceService $service): RedirectResponse
    {
        $service->updateResource($request, $resource);

        return $service->response($request->leave_bal);
    }

    public function destroy(Resource $resource, ResourceService $service)
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $resource->delete();

        return redirect()->route('resource.index')->with('success', 'Resource deleted successfully');
    }

    public function print(Resource $resource)
    {
        try {

            $template = $resource->mapVariables($resource->NDATemplate);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name("NDA for ".$resource->user?->name() . ".pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('resource.edit', $resource))->with('flash_danger', 'Edit your resource and select NDA template.');
        }
    }

    public function send(Resource $resource)
    {
        $template = $resource->mapVariables($resource->NDATemplate);

        try {
            if (! Storage::exists('resource/templates')) {
                Storage::makeDirectory('resource/templates');
            }

            $filename = "non_disclosure_agreement_".str_replace(' ', '_',$resource->user?->name())."_".date('Y_m_d_H_i_s').'.pdf';

            $storage = storage_path("app/resource/templates/{$filename}");

            Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                ->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->save($storage);

            $resource->documents()->create([
                'name' => "Non disclosure Agreement for {$resource->user?->name()}",
                'file' => $filename,
                //'document_type_id' => 4,
                'creator_id' => auth()->id(),
                'status_id' => Status::ACTIVE->value,
            ]);

            $emails = [$resource->user?->email, auth()->user()->email];

            $data = [
                'emails' => array_values(array_filter(array_unique($emails))),
                'subject' => "NDA for {$resource->user?->name()}",
                'body' => "Please find the non disclosure agreement attached.",
                'attachments' => $storage
            ];

            NonDisclosureAgreementJob::dispatch($data);

            return redirect(route('resource.edit', $resource))->with('flash_success', 'Non Disclosure Agreement was sent successfully');

        }catch (\TypeError $e){
            return redirect(route('resource.edit', $resource))->with('flash_danger', 'Edit your resource and select NDA template.');
        }
    }
}
