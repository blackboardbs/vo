<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Page;

class FavoriteController extends Controller
{
    public function toggleFavorite(Request $request) {
        $user = auth()->user();
        $pageId = $request->page_id;
    
        
        // This will toggle the favorite status
        $toggleStatus = $user->favorites()->toggle($pageId);
    
        // Check if the page is now favorited or not
        $isFavorited = $toggleStatus['attached'] ? true : false;
    
        $pages = Page::all();
        $favorites = [];

        foreach($pages as $page) {
            if($user->favorites->contains($page->id)) {
                $favorites[] = $page;
            }
        }
        return response()->json([
            'favorited' => $isFavorited,
            'success' => 'Favorites updated.',
            'favorites' => $favorites
        ]);
    }   
    
    public function checkFavoriteStatus(Request $request, $pageId) {
        $user = auth()->user();
    
        // Check if the specific page ID is favorited
        $isFavorited = $user->favorites()->where('page_id', $pageId)->exists();
    
        return response()->json([
            'isFavorited' => $isFavorited
        ]);
    }

    public function getFavoritedPages()
    {
        $pages = Page::all();
        $user = Auth::user();
        $favorites = [];

        foreach($pages as $page) {
            if($user->favorites->contains($page->id)) {
                $favorites[] = $page;
            }
        }
        return response()->json(['favorites' => $favorites]);
    }
    
}
