<?php

namespace App\Http\Controllers;

use App\Models\DynamicDashboard;
use App\Models\LandingPageDashboard;
use App\Models\Status;
use Illuminate\View\View;

class CustomDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(): View
    {
        $lp_dasboard = LandingPageDashboard::with('status:id,description')->where('user_id', '=', auth()->user()->id)->where('status_id', '=', 1)->get();
        $list_dashboards = LandingPageDashboard::with('status:id,description')->select('dashboard_name', 'status_id')->where('user_id', '=', auth()->user()->id)->groupBy('dashboard_name', 'status_id')->orderBy('status_id', 'asc')->get();
        $components = [];

        $dynamic_components = DynamicDashboard::where('status_id', 1)->get();

        //return auth()->user()->roles->first();

        foreach ($lp_dasboard as $dashboard_comp) {
            array_push($components, $dashboard_comp->component);
        }

        $parameters = [
            'user_prefs' => $components,
            'status_drop_down' => Status::where('status', '=', 1)->orderBy('id')->pluck('description', 'id')->prepend('Status', '0'),
            'list_dashboards' => $list_dashboards,
            'dynamic_components' => $dynamic_components,
        ];

        return view('reports.custom-dashboard')->with($parameters);
    }

    public function edit($dashboard_name): View
    {
        $dashboards = LandingPageDashboard::where('user_id', '=', auth()->user()->id)->where('dashboard_name', '=', $dashboard_name)->get();
        $dynamic_components = DynamicDashboard::where('status_id', 1)->get();
        $components = [];
        $dash_name = '';
        $status = 0;
        foreach ($dashboards as $dashboard) {
            array_push($components, $dashboard->component);
            $dash_name = $dashboard->dashboard_name;
            $status = $dashboard->status_id;
        }

        $parameters = [
            'user_prefs' => $components,
            'status_drop_down' => Status::where('status', '=', 1)->orderBy('id')->pluck('description', 'id')->prepend('Status', '0'),
            'dash_name' => $dash_name,
            'status' => $status,
            'dynamic_components' => $dynamic_components,
        ];

        return view('reports.dashboard-edit')->with($parameters);
    }
}
