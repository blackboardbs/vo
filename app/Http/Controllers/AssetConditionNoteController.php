<?php

namespace App\Http\Controllers;

use App\Models\AssetConditionNote;
use App\Http\Requests\AssetConditionRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AssetConditionNoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add($asset_id): View
    {
        $parameters = [
            'asset_id' => $asset_id,
        ];

        return view('assetreg.asset_condition.create_condition')->with($parameters);
    }

    public function store(AssetConditionRequest $request): RedirectResponse
    {
        $asset_condition = new AssetConditionNote();
        $asset_condition->note = $request->input('note');
        $asset_condition->include_in_acceptance_letter = $request->input('include_in_acceptance_letter');
        $asset_condition->asset_id = $request->input('asset_id');
        $asset_condition->creator_id = Auth::id();
        $asset_condition->save();

        return redirect(route('assetreg.show', $request->input('asset_id')));
    }

    public function edit($asset_condition_id): View
    {
        $asset_condition = AssetConditionNote::find($asset_condition_id);
        $parameters = [
            'asset_condition' => $asset_condition,
        ];

        return view('assetreg.asset_condition.edit_condition')->with($parameters);
    }

    public function update(AssetConditionRequest $request, $asset_condition_id): RedirectResponse
    {
        $asset_condition = AssetConditionNote::find($asset_condition_id);
        $asset_condition->note = $request->input('note');
        $asset_condition->include_in_acceptance_letter = $request->input('include_in_acceptance_letter');
        $asset_condition->creator_id = Auth::id();
        $asset_condition->save();

        return redirect(route('assetreg.show', $request->input('asset_id')));
    }

    public function destroy($asset_condtion_id): RedirectResponse
    {
        AssetConditionNote::destroy($asset_condtion_id);

        return redirect(route('assetreg.show', $asset_condtion_id));
    }
}
