<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\ModuleSection;
use App\Models\PermissionType;
use App\Models\StorePermission;
use App\Models\ViewPermission;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\View\View;

class ViewPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $modules = Module::where('name', \App\Models\Project::class)->where('status_id', 1)->paginate(15);
        $modules_drop_down = Module::where('name', \App\Models\Project::class)->where('status_id', 1)->pluck('display_name', 'id');

        $parameters = [
            'modules_view' => $modules,
            'modules_drop_down' => $modules_drop_down,
        ];

        return view('permission.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ViewPermission  $viewPermission
     */
    public function show($module_id): View
    {
        $module = Module::find($module_id);
        $module_sections = ModuleSection::where('module_id', $module_id)->where('status_id', 1)->orderBy('id')->get();
        $module_roles = ModuleRole::where('module_id', $module_id)->where('status_id', 1)->orderBy('id')->get();
        $permissions_drop_down = PermissionType::orderBy('id')->pluck('name', 'id');

        $parameters = [
            'module' => $module,
            'module_sections' => $module_sections,
            'module_roles' => $module_roles,
            'permissions_drop_down' => $permissions_drop_down,
        ];

        return view('permission.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ViewPermission  $viewPermission
     */
    public function edit($module_id): View
    {
        $module = Module::find($module_id);
        $module_sections = ModuleSection::where('module_id', $module_id)->where('status_id', 1)->orderBy('id')->get();
        $module_roles = ModuleRole::where('module_id', $module_id)->where('status_id', 1)->orderBy('id')->get();
        $permissions_drop_down = PermissionType::orderBy('id')->pluck('name', 'id');

        $parameters = [
            'module' => $module,
            'module_sections' => $module_sections,
            'module_roles' => $module_roles,
            'permissions_drop_down' => $permissions_drop_down,
        ];

        return view('permission.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\ViewPermission  $viewPermission
     */
    public function update(Request $request, $module_id): RedirectResponse
    {
        // dd($request);
        $module = Module::find($module_id);

        $module_roles = ModuleRole::where('module_id', $module_id)->where('status_id', 1)->get();
        $module_sections = ModuleSection::where('module_id', $module_id)->where('status_id', 1)->get();

        foreach ($module_roles as $module_role) {
            foreach ($module_sections as $module_section) {
                $storePermission = StorePermission::where('module_id', $module_id)->where('module_role_id', $module_role->id)->where('module_section_id', $module_section->id)->first();
                if (! isset($storePermission)) {
                    $storePermission = new StorePermission();
                }

                $permission = str_replace(' ', '-', strtolower($module_section->name)).'_'.$module_role->id.'_'.$module_section->id;
                // dd($permission);
                $storePermission->module_id = $module_id;
                $storePermission->module_section_id = $module_section->id;
                $storePermission->module_role_id = $module_role->id;
                $storePermission->permission_id = isset($request[$permission]) ? $request[$permission] : null;
                $storePermission->created_at = now();
                $storePermission->save();
            }
        }

        return redirect(route('viewpermission.edit', $module_id))->with('flash_success', 'View permissions updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ViewPermission $viewPermission)
    {
        //
    }
}
