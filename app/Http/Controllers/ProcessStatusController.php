<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProcessStatusRequest;
use App\Http\Requests\UpdateProcessStatusRequest;
use App\Models\ProcessStatus;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProcessStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $process_status = ProcessStatus::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $process_status = $process_status->where('status_id', $request->input('status_filter'));
            }else{
                $process_status = $process_status->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $process_status = $process_status->where('name', 'like', '%'.$request->input('q').'%');
        }

        $process_status = $process_status->paginate($item);
        // $process_status = $process_status->paginate($items);
        $parameters = [
            'process_status' => $process_status,
        ];

        return view('master_data.process_status.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ProcessStatus::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::where('status', 1)->orderBy('description')->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.process_status.create')->with($parameters);
    }

    public function store(StoreProcessStatusRequest $request): RedirectResponse
    {
        $process_status = new ProcessStatus();
        $process_status->name = $request->name;
        $process_status->status_id = $request->status;
        $process_status->save();

        return redirect()->route('process_status.index')->with('flash_success', 'New Process Status created successfully');
    }

    public function show($id): View
    {
        $process_status = ProcessStatus::find($id);
        $parameters = [
            'process_status' => $process_status,
        ];

        return view('master_data.process_status.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = ProcessStatus::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $process_status = ProcessStatus::find($id);
        $parameters = [
            'process_status' => $process_status,
            'status_dropdown' => Status::where('status', 1)->orderBy('description')->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.process_status.edit')->with($parameters);
    }

    public function update(UpdateProcessStatusRequest $request, $id): RedirectResponse
    {
        $process_status = ProcessStatus::find($id);
        $process_status->name = $request->name;
        $process_status->status_id = $request->status;
        $process_status->save();

        return redirect()->route('process_status.index')->with('flash_success', 'Process Status was updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // ProcessStatus::destroy($id);
        $item = ProcessStatus::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('process_status.index')->with('flash_success', 'Process Status was suspended successfully');
    }
}
