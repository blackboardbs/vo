<?php

namespace App\Http\Controllers;

use App\PaymentType;
use App\Status;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $types = PaymentType::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $types = $types->where('status_id', $request->input('status_filter'));
            }else{
                $types = $types->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $types = $types->where('description', 'like', '%'.$request->input('q').'%');
        }

        $types = $types->paginate($item);

        return view('master_data.payment_type.index')->with(['types' => $types]);
    }

    public function create()
    {
        return view('master_data.payment_type.create');
    }

    public function store(Request $request, PaymentType $paymentType)
    {
        $this->saveData($request, $paymentType);

        return redirect()->route('paymenttype.index')->with('flash_success', 'Payment type stored successfully');
    }

    public function show(PaymentType $paymenttype)
    {
        return view('master_data.payment_type.show')->with(['type' => $paymenttype]);
    }

    public function edit(PaymentType $paymenttype)
    {
        $data = [
            'type' => $paymenttype,
        ];

        return view('master_data.payment_type.edit')->with($data);
    }

    public function update(Request $request, PaymentType $paymenttype)
    {
        $this->saveData($request, $paymenttype);
        return redirect()->route('paymenttype.index')->with('flash_success', 'Payment type updated successfully');
    }

    public function destroy($paymenttype)
    {
        // $paymenttype->delete();
        $item = PaymentType::find($paymenttype);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('paymenttype.index')->with('flash_success', 'Payment type suspended successfully');
    }

    private function saveData($request, $type)
    {
        $type->description = $request->description;
        $type->status_id = $request->status_id;
        $type->save();

        return $type;
    }
}
