<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\ProjectType;
use App\Models\Team;
use App\Models\Task;
use App\Models\Time;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerProjectSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 10;

        $timesheet = Timesheet::with(['customer','project','project.status'])->selectRaw('timesheet.project_id, timesheet.customer_id, assignment.hours, assignment.invoice_rate,
        SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS billable_hours,
        SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS non_billable_hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            });
        if ($request->has('company') && $request->company != '') {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('team') && $request->team != null) {
            $timesheet = $timesheet->whereHas('employee.resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee) {
            $timesheet = $timesheet->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $timesheet = $timesheet->where('timesheet.year', '=', substr($request->yearmonth, 0, 4))->where('timesheet.month', '=', substr($request->yearmonth, 4));
        }
        if (($request->has('date_from') && $request->date_from != '') || ($request->has('date_to') && $request->date_to != '')) {
            $timesheet = $timesheet->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })->whereBetween('calendar.date', [$request->date_from, $request->date_to]);
        }
        if ($request->has('customer') && $request->customer) {
            $timesheet = $timesheet->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project) {
            $timesheet = $timesheet->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable) {
            $timesheet = $timesheet->where('timeline.is_billable', '=', $request->billable);
        }
        if ($request->has('project_status') && $request->project_status) {
            $timesheet = $timesheet->whereHas('project', function ($query) use ($request) {
                $query->where('status_id', '=', $request->project_status);
            });
        }
        if ($request->has('project_type') && $request->project_type) {
            $timesheet = $timesheet->whereHas('project', function ($query) use ($request) {
                $query->where('project_type_id', '=', $request->project_type);
            });
        }
        $timesheet = $timesheet->groupBy('timesheet.project_id', 'timesheet.customer_id', 'assignment.hours', 'assignment.invoice_rate')
            ->orderBy('timesheet.project_id', 'desc')
            ->paginate($item);

        $total_billable_hours = 0;
        $total_non_billable_hour = 0;
        $total_po_hours = 0;
        $total_po_value = 0;
        $total_outstanding_hours = 0;
        $total_outstanding_value = 0;
        foreach ($timesheet as $time) {
            $total_billable_hours += $time->billable_hours;
            $total_non_billable_hour += $time->non_billable_hours;
            $total_po_hours += $time->hours;
            $total_po_value += ($time->hours * $time->invoice_rate);
            $total_outstanding_hours += ($time->hours - $time->billable_hours);
            $total_outstanding_value += (($time->hours - $time->billable_hours) >= 0) ? ($time->hours - $time->billable_hours) * $time->invoice_rate : 0;
        }
        $total_percentage = ($total_billable_hours > 0 && $total_po_hours > 0) ? ($total_billable_hours / $total_po_hours) * 100 : 0;

        $calendar = Calendar::select('year', 'month')->whereDate('date', '<=', Carbon::now()->toDateString())->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $yearmonth = [];
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.$month] = $cal->year.$month;
        }

        $parameters = [
            'projects' => $timesheet,
            'total_billable_hours' => $total_billable_hours,
            'total_non_billable_hour' => $total_non_billable_hour,
            'total_po_hours' => $total_po_hours,
            'total_po_value' => $total_po_value,
            'total_outstanding_hours' => $total_outstanding_hours,
            'total_outstanding_value' => $total_outstanding_value,
            'total_percentage' => $total_percentage,
            'yearmonth_dropdown' => $yearmonth,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'team_drop_down' => Team::filters()->orderBy('team_name')->pluck('team_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'task_dropdown' => Task::filters()->orderBy('description')->pluck('description', 'id'),
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'customer_project_summary');

        return view('reports.customer_project_summary')->with($parameters);
    }
}
