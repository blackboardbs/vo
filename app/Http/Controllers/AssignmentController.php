<?php

namespace App\Http\Controllers;

use App\Exports\AssignmentsExport;
use App\Exports\ContactsExport;
use App\Jobs\ContractJob;
use App\Jobs\MasterServiceAgreementJob;
use App\Models\Account;
use App\Models\AccountElement;
use App\API\Currencies\Currency;
use App\Models\Assignment;
use App\Models\AssignmentCost;
use App\Models\AssignmentExtension;
use App\Models\BusinessFunction;
use App\Models\Company;
use App\Models\Config as SiteConfig;
use App\Models\Customer;
use App\Models\DigisignUsers;
use App\Models\Document;
use App\Models\Expense;
use App\Http\Requests\StoreAddAssignmentCostRequest;
use App\Http\Requests\StoreAssignmentExtensionRequest;
use App\Http\Requests\StoreAssignmentRequest;
use App\Http\Requests\UpdateAddAssignmentCostRequest;
use App\Http\Requests\UpdateAssignmentExtensionRequest;
use App\Http\Requests\UpdateAssignmentRequest;
use App\Jobs\SendAssignmentEmailJob;
use App\Jobs\SendResourceAssignmentEmailJob;
use App\Jobs\SendSupplierAssignmentEmailJob;
use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\Notification;
use App\Models\PaymentMethod;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\Status;
use App\Models\System;
use App\Models\Task;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\UserNotification;
use App\Utils;
use App\Models\Vendor;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;

class AssignmentController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $assignments = Assignment::with([
            'consultant:id,first_name,last_name', 'project_status:id,description',
            'project' => fn($project) => $project->select(['id', 'name', 'customer_id','project_type_id'])->with(['customer:id,customer_name']), 'system_desc', 'function_desc',
            'project.timesheets' => fn($timesheet) => $timesheet->select(['id','employee_id']) ])
            ->withCount('expense_tracking');

        $module = Module::where('name', '=', 'App\Models\Assignment')->first();


        if (Auth::user()->canAccess($module?->id, 'view_all') || Auth::user()->canAccess($module?->id, 'view_team')) {
            if (! Auth::user()->canAccess($module?->id, 'view_all') && Auth::user()->canAccess($module?->id, 'view_team')) {
                $_projects = Project::select('id', 'consultants')->orderBy('name')->get()->toArray();
                $project_ids = [];
                foreach ($_projects as $_project) {
                    $ids = explode(',', $_project['consultants']);
                    $_team_ids = Auth::user()->team();
                    foreach ($_team_ids as $_team_id) {
                        if (in_array($_team_id, $ids)) {
                            $project_ids[] = $_project['id'];
                            break;
                        }
                    }
                }

                $assignments = $assignments->whereIn('employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }



        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $assignments = $assignments->when($request->company,
            fn($assign) => $assign->whereHas('project',
                fn($project) => $project->where('company_id', $request->company)
            )
        )->when($request->customer,
            fn($assign) => $assign->whereHas('project',
                fn($project) => $project->where('customer_id', '=', $request->customer)
            )
        )->when($request->project,
            fn($assign) => $assign->where('project_id', $request->project)
        )->when($request->resource_id,
            fn($assign) => $assign->where('employee_id', $request->resource_id)
        )->when($request->function_id,
            fn($assign) => $assign->where('function', $request->function_id)
        )->when($request->project_status_id,
            fn($assign) => $assign->where('assignment_status', $request->project_status_id)
        )->when($request->start_date,
            fn($assign) => $assign->where('start_date', '>', $request->start_date)
        )->when($request->end_date,
            fn($assign) => $assign->where('end_date', '<', $request->end_date)
        )->when($request->q, fn($assign) => $assign->where(
            fn($query) => $query->whereHas('project',
                fn($project) => $project->where('name', 'like', '%'.$request->q.'%')->orWhereHas('customer', fn($customer) => $customer->where('customer_name', 'like', '%'.$request->q.'%'))
            )->orWhereHas('resource', fn($resource) => $resource->where('first_name', 'like', '%'.$request->q.'%')->orWhere('last_name','like', '%'.$request->q.'%')))
        )->sortable(['id' => 'desc'])->paginate($item);

        $assignments_comple_perc = [];
        $worked_hours = [];
        $worked_bill_hours = [];
        $worked_non_bill_hours = [];
        $timesheet_count = [];
        $total_hours = 0;
        $total_actual = 0;
        foreach ($assignments as $assignment) {

            $timesheets = Timesheet::selectRaw('timeline.is_billable as billable,SUM(timeline.total + (timeline.total_m/60)) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->groupBy('timeline.is_billable')
                ->get();

            $timesheet_count[$assignment->id] = 0;
            foreach($timesheets as $timesheet){
                $timesheet_count[$assignment->id]++;
                if($timesheet->billable == '1'){
                    $worked_bill_hours[$assignment->id] = $timesheet->actual_hours ?? 0;
                }

                if($timesheet->billable == '0'){
                    $worked_non_bill_hours[$assignment->id] = $timesheet->actual_hours ?? 0;
                }
            }
            try {
                if($assignment->project){
                    $assignments_comple_perc[$assignment->id] = (($assignment->project->project_type_id == 1 ? ($worked_bill_hours[$assignment->id] ?? 0) : ($worked_bill_hours[$assignment->id] ?? 0) + ($worked_non_bill_hours[$assignment->id]??0)) / $assignment->hours) * 100;
                }
            } catch (\DivisionByZeroError $e) {
                $assignments_comple_perc[$assignment->id] = 0;
            }

            if($assignment->project){
                $total_hours += $assignment->hours;
                $worked_hours[$assignment->id] = $assignment->project->project_type_id == 1 ? ($worked_bill_hours[$assignment->id] ?? 0) : (($worked_bill_hours[$assignment->id] ?? 0) + ($worked_non_bill_hours[$assignment->id]??0));
                $total_actual += $assignment->project->project_type_id == 1 ? $assignment->hours - ($worked_bill_hours[$assignment->id] ?? 0) : $assignment->hours - (($worked_bill_hours[$assignment->id] ?? 0) + ($worked_non_bill_hours[$assignment->id]??0));
            }
        }

        try {
            $total_percentage = (array_sum($worked_hours) / $total_hours)*100;
        } catch (\DivisionByZeroError $e) {
            $total_percentage = 0;
        }

        $parameters = [
            'assignments' => $assignments,
            'completion_perc' => $assignments_comple_perc,
            'can_update' => $can_update,
            'worked_hours' => $worked_hours,
            'worked_bill_hours' => $worked_bill_hours,
            'worked_non_bill_hours' => $worked_non_bill_hours,
            'total_hours' => $total_hours,
            'total_remaining' => $total_actual,
            'total_percentage' => $total_percentage,
            'timesheet_count' => $timesheet_count,
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name','id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name','id'),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name','id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'status_drop_down' => ProjectStatus::orderBy('id')->pluck('description', 'id'),
        ];

        if ($request->has('export')) return $this->export($parameters, 'assignment');

        return view('assignment.index', $parameters);
    }

    public function create()
    {
        $module = Module::where('name', '=', 'App\Models\Assignment')->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $assignment_nr = Assignment::max('id');

        $parameters = [
            'assignment_nr' => $assignment_nr == null ? 1 : $assignment_nr + 1,
            'customer_drop_down' => Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Customer', '0'),
            'consultant_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Consultant', '0'),
            'system_drop_down' => System::orderBy('description')->pluck('description', 'id')->prepend('Select System', '0'),
            'function_drop_down' => BusinessFunction::orderBy('description')->pluck('description', 'id')->prepend('Select System', '0'),
            'status_drop_down' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id'),
            'company_drop_down' => Company::orderBy('id')->pluck('company_name', 'id')->prepend('Company', '0'),
        ];

        return view('assignment.create')->with($parameters);
    }

    public function add(Project $project, User $resource, Currency $currency)
    {
        $module = Module::where('name', '=', 'App\Models\Assignment')->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            abort_unless(Auth::user()->canAccess($module->id, 'create_all') && Auth::user()->canAccess($module->id, 'create_team'), 403);
        }

        $project->load(['project_type:id,description', 'customer:id,customer_name', 'status:id,description', 'expense', 'resource.standard_cost']);

        $parameters = [
            'project' => $project,
            'resource' => $resource,
            'minutes_dropdown' => $this->iteratorDropdown(60),
            'hours_dropdown' => $this->iteratorDropdown(25)
        ];
// dd($project);
        $currencies = $currency->currencies();

        if (! isset($currencies['errors'])) {
            $currency_list = [];
            foreach ($currencies['currencies'] as $key => $value) {
                $currency_list[$key] = "$value ($key)";
            }
            asort($currency_list);
            $parameters['currencies'] = $currency_list;
        }

        return view('assignment.add')->with($parameters);
    }

    public function store(StoreAssignmentRequest $request)
    {
        if ($this->doesAnAssisnmentExist()) {
            return redirect(route('project.show', $request->project_id))->with('flash_danger', 'Consultant already has an assignment');
        }

        $assignment = new Assignment;
        $assignment->employee_id = $request->input('employee_id');
        $assignment->project_id = $request->input('project_id');
        $assignment->customer_po = $request->input('customer_po');
        $assignment->start_date = $request->input('start_date');
        $assignment->end_date = $request->input('end_date');
        $assignment->rate = $request->input('rate');
        $assignment->bonus_rate = $request->input('bonus_rate');
        $assignment->hours = $request->input('hours');
        $assignment->number_hours = $request->input('number_hours');
        $assignment->report_to = $request->input('report_to');
        $assignment->report_to_email = $request->input('report_to_email');
        $assignment->report_phone = $request->input('report_phone');
        $assignment->billable = $request->input('billable');
        $assignment->note_1 = $request->input('note_1');
        $assignment->note_2 = $request->input('note_2');
        $assignment->function = $request->input('function');
        $assignment->system = $request->input('system');
        $assignment->country_id = $request->input('country_id');
        $assignment->vat_rate_id = $request->input('vat_rate_id');
        $assignment->currency = $request->input('currency');
        $assignment->internal_cost_rate = $request->input('internal_cost_rate');
        $assignment->external_cost_rate = $request->input('external_cost_rate');
        $assignment->invoice_rate = $request->input('invoice_rate');
        $assignment->currency_sec = $request->input('currency_sec');
        $assignment->internal_cost_rate_sec = $request->input('internal_cost_rate_sec');
        $assignment->external_cost_rate_sec = $request->input('external_cost_rate_sec');
        $assignment->invoice_rate_sec = $request->input('invoice_rate_sec');
        $assignment->rate_sec = $request->input('rate_sec');
        $assignment->assignment_status = $request->input('assignment_status');
        $assignment->claim_approver_id = $request->input('claim_approver_id');
        $assignment->medical_certificate_type = $request->input('medical_certificate_type');
        $assignment->medical_certificate_text = $request->input('medical_certificate_text');
        $assignment->assignment_approver = $request->input('assignment_approver');
        $assignment->project_type = $request->input('project_type');
        $assignment->status = $request->input('status');
        $assignment->creator_id = $request->input('creator_id');
        $assignment->company_id = $request->input('company_id');
        $assignment->location = $request->input('location');
        $assignment->hours_of_work = $request->input('hours_of_work');
        $assignment->role = $request->input('role');
        $assignment->vendor_template_id = $request->input('vendor_template_id');
        $assignment->resource_template_id = $request->input('resource_template_id');
        $assignment->max_time_per_day_hours = $request->input('max_time_per_day_hours');
        $assignment->max_time_per_day_min = $request->input('max_time_per_day_min');
        $assignment->capacity_allocation_hours = $request->input('capacity_allocation_hours');
        $assignment->capacity_allocation_min = $request->input('capacity_allocation_min');
        $assignment->capacity_allocation_percentage = $request->input('capacity_allocation_percentage');
        $assignment->planned_start_date = $request->input('planned_start_date');
        $assignment->planned_end_date = $request->input('planned_end_date');
        $assignment->product_owner_id = $request->input('product_owner_id');
        $assignment->project_manager_ts_approver = $request->input('project_manager_ts_approver');
        $assignment->product_owner_ts_approver = $request->input('product_owner_ts_approver');
        $assignment->claim_approver_ts_approver = $request->input('claim_approver_ts_approver');
        $assignment->report_to_ts_approver = $request->input('report_to_ts_approver');

        //Start - New fields
        $assignment->project_manager_new_id = $request->input('project_manager_new_id');
        $assignment->project_manager_new_ts_approver = $request->input('project_manager_new_ts_approver');
        $assignment->line_manager_id = $request->input('line_manager_id');
        $assignment->line_manager_ts_approver = $request->input('line_manager_ts_approver');
        $assignment->resource_manager_id = $request->input('resource_manager_id');
        $assignment->resource_manager_ts_approver = $request->input('resource_manager_ts_approver');
        $assignment->internal_owner_known_as = $request->input('internal_owner_known_as');
        $assignment->product_owner_known_as = $request->input('product_owner_known_as');
        $assignment->project_manager_new_known_as = $request->input('project_manager_new_known_as');
        $assignment->line_manager_known_as = $request->input('line_manager_known_as');
        $assignment->claim_approver_known_as = $request->input('claim_approver_known_as');
        $assignment->resource_manager_known_as = $request->input('resource_manager_known_as');
        $assignment->copy_from_user = $request->input('copy_from_user');
        //End - New fields

        $expense = new Expense();
        $expense->travel = $request->input('travel');
        $expense->parking = $request->input('parking');
        $expense->accommodation = $request->input('accommodation');
        $expense->per_diem = $request->input('per_diem');
        $expense->out_of_town = $request->input('out_of_town');
        $expense->data = $request->input('data');
        $expense->other = $request->input('other');
        $expense->save();

        /*$extension = new Extension();
        $extension->travel = $request->input('travel');
        $extension->parking = $request->input('parking');
        //$extension->car_rental = $request->input('car_rental');
        //$extension->flights = $request->input('flights');
        $extension->other = $request->input('other');
        $extension->accommodation = $request->input('accommodation');
        $extension->out_of_town = $request->input('out_of_town');
        $extension->per_diem = $request->input('per_diem');
        //$extension->toll = $request->input('toll');
        $extension->data = $request->input('data');
        $extension->save();*/

        $assignment->exp_id = $expense->id;
        //$assignment->extension_id = $extension->id;

        $assignment->save();

        activity()->on($assignment)->withProperties(['full_name' => $assignment->resource->first_name.' '.$assignment->resource->last_name])->log('created');
        $emails = [];
        isset($assignment->resource) ? array_push($emails, $assignment->resource->email) : null;
        isset($assignment->resource_user->manager) ? array_push($emails, $assignment->resource_user->manager->email) : null;

        $notification = new Notification;
        $notification->name = 'New Assignment has been created';
        $notification->link = route('assignment.show', $assignment->id);
        $notification->save();

        $user_notifications = [];

        array_push($user_notifications, [
            'user_id' => $assignment->employee_id,
            'notification_id' => $notification->id,
        ]);

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);
        $utils = new Utils();
        //Mail::to(array_filter(array_unique($emails)))->send(new AssignmentCreated($assignment,$utils->helpPortal));
        SendAssignmentEmailJob::dispatch(array_filter(array_unique($emails)), $assignment, $utils->helpPortal)->delay(now()->addMinutes(5));

        return redirect(route('project.show', $assignment->project_id))->with('flash_success', 'Assignment captured successfully');
    }

    public function getAssignmentRoles(Request $request){
        $roles = ModuleRole::where('module_id',30)->get(['name','id']);

        return response()->json(['roles'=>$roles]);
    }

    public function show(Request $request, Assignment $assignment)
    {
        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_if((!Auth::user()->canAccess($module->id, 'show_all') || !Auth::user()->canAccess($module->id, 'show_team')), 403);

        $assignment->load([
            'documents.type',
            'consultant:id,first_name,last_name,email,vendor_id',
            'project' => fn($project) => $project->select(['id', 'name', 'customer_id', 'ref', 'start_date', 'end_date', 'project_type_id', 'customer_po', 'hours_of_work', 'status_id', 'billable'])->with(['customer:id,customer_name', 'project_type:id,description', 'status:id,description']),
            'project_status:id,description',
            'internalOwner:id,first_name,last_name',
            'productOwner:id,first_name,last_name',
            'projectManager:id,first_name,last_name',
            'lineManager:id,first_name,last_name',
            'claimApprover:id,first_name,last_name',
            'resourceManager:id,first_name,last_name',
            'copyFromUser:id,first_name,last_name',
            'vendorTemplate:id,name',
            'resourceTemplate:id,name',
            'assignmentCosts' => fn($cost) => $cost->select(['id', 'ass_id', 'description', 'payment_frequency', 'payment_method', 'cost'])->with(['payment_meth:id,description']),
            'expense:id,travel,parking,accommodation,per_diem,out_of_town,data,other',
            'extensions:id,assignment_id,extension_date,extension_hours,extension_notes,extension_start_date,extension_end_date,extension_ref'
        ]);

        $time = Timeline::with([
            'timesheet' => fn($timesheet) => $timesheet->select([
                'id',
                'invoice_number',
                'invoice_date',
                'invoice_due_date',
                'year_week',
                'inv_ref',
                'bill_status',
                'vendor_invoice_number',
                'vendor_invoice_date',
                'vendor_due_date',
                'vendor_paid_date',
                'vendor_invoice_ref',
                'vendor_invoice_status'
            ])->with([
                'time_exp' => fn($expense) => $expense
                    ->with([
                        'account:id,description',
                        'account_element:id,description'
                    ]),
                'bill_statusd:id,description',
                'vendor_inv_status:id,description'
            ])
        ])->select([
            'total',
            'total_m',
            'is_billable',
            'vend_is_billable',
            'timesheet_id'
        ])->whereHas('timesheet',
            fn($timesheet) => $timesheet->where('employee_id', $assignment->employee_id)->where('project_id', $assignment->project_id)
        )->get();

        $billable_hours = $time->where('is_billable', 1)->sum(fn($billed) => $billed->total + ($billed->total_m/60));

        $non_billable_hours = $time->where('is_billable', 0)->sum(fn($billed) => $billed->total + ($billed->total_m/60));

        $actual_expense = $time->sum(fn($expense) => $expense->timesheet?->time_exp?->sum('amount'));
        $actual_labour = $billable_hours * ($assignment->internal_cost_rate + $assignment->external_cost_rate);
        $actual_invoice = $billable_hours * $assignment->invoice_rate;
        $profit_loss = $actual_invoice - $actual_labour;
        $profit_percentage = $assignment->profitPercentage($actual_invoice,$actual_labour);

        $total_labour_cost = ($assignment->internal_cost_rate + $assignment->external_cost_rate) * $assignment->hours;
        $total_additional_cost = $assignment->assignmentCosts?->sum('cost');
        $total_invoice = ($assignment->invoice_rate * $assignment->hours);
        $profit = $total_invoice - $total_labour_cost - $total_additional_cost;
        $profit_with_percentage = $this->profitPercentage($profit, $total_invoice);

        $total_labour_cost_sec = ($assignment->internal_cost_rate_sec + $assignment->external_cost_rate_sec) * $assignment->hours;
        $total_additional_cost_sec = $assignment->assignmentCosts?->sum('cost');
        $total_invoice_sec = ($assignment->invoice_rate_sec * $assignment->hours);
        $profit_sec = $total_invoice_sec - $total_labour_cost_sec - $total_additional_cost_sec;
        $profit_with_percentage_sec = $this->profitPercentage($profit_sec, $total_invoice_sec);


        $expenses = $time->map(fn($timeline) => $timeline->timesheet?->time_exp)->flatten(1);

        $parameters = [
            'assignment' => $assignment,
            'expenses' => $expenses,
            'billed_hours' => $billable_hours,
            'non_billable_hours' => $non_billable_hours,
            'percentage' => $assignment->project?->percentage($assignment->hours, $billable_hours, $non_billable_hours),
            'actual_expenses' => number_format($actual_expense,2,'.',','),
            'actual_labour' => number_format($actual_labour,2,'.',','),
            'actual_invoice' => number_format($actual_invoice,2,'.',','),
            'profit_loss' => number_format($profit_loss,2,'.',','),
            'profit_percentage' => number_format($profit_percentage, 0,'.',',').'%',
            'customer_invoices' => $assignment->customerInvoice($time),
            'vendor_invoices' => $assignment->vendorInvoice($time),
            'subject' => '',
            'resource' => new User(),
            'total_labour_cost' => $total_labour_cost,
            'total_additional_cost' => $total_additional_cost,
            'total_invoice' => $total_invoice,
            'profit_with_percentage' => $profit_with_percentage,
            'total_labour_cost_sec' => $total_labour_cost_sec,
            'total_additional_cost_sec' => $total_additional_cost_sec,
            'total_invoice_sec' => $total_invoice_sec,
            'profit_with_percentage_sec' => $profit_with_percentage_sec
        ];

        return view('assignment.show')->with($parameters);
    }

    public function edit(Assignment $assignment, Currency $currency)
    {
        $assignment->load([
            'project' => fn($project) => $project->select([
                'id',
                'name',
                'ref',
                'project_type_id',
                'customer_po',
                'customer_id',
                'hours_of_work',
                'start_date',
                'end_date',
                'status_id',
                'billable',
                'exp_id'
            ])->with([
                'project_type:id,description',
                'status:id,description',
                'customer:id,customer_name',
                'expense'
            ]),
            'resource:id,first_name,last_name',
            'extensions'
        ]);

        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $parameters = [
            'assignment' => $assignment,
            'project' => $assignment->project,
            'resource' => $assignment->resource,
            'hours_dropdown' => $this->iteratorDropdown(60),
            'minutes_dropdown' => $this->iteratorDropdown(25),
        ];

        $currencies = $currency->currencies();

        if (! isset($currencies['errors'])) {
            $currency_list = [];
            foreach ($currencies['currencies'] as $key => $value) {
                $currency_list[$key] = "$value ($key)";
            }
            asort($currency_list);
            $parameters['currencies'] = $currency_list;
        }

        // $parameters['currencies'] = [];
        return view('assignment.edit')->with($parameters);
    }

    public function update(UpdateAssignmentRequest $request, $assignmentid)
    {

        $assignment = Assignment::find($assignmentid);
        $assignment->employee_id = $request->input('employee_id');
        $assignment->customer_po = $request->input('customer_po');
        $assignment->start_date = $request->input('start_date');
        $assignment->end_date = $request->input('end_date');
        $assignment->rate = $request->input('rate');
        $assignment->bonus_rate = $request->input('bonus_rate');
        $assignment->hours = $request->input('hours');
        $assignment->number_hours = $request->input('number_hours');
        //$assignment->vendor_no = $request->input('vendor_no');
        $assignment->report_to = $request->input('report_to');
        $assignment->report_to_email = $request->input('report_to_email');
        $assignment->report_phone = $request->input('report_phone');
        $assignment->billable = $request->input('billable');
        $assignment->note_1 = $request->input('note_1');
        $assignment->note_2 = $request->input('note_2');
        $assignment->function = $request->input('function');
        $assignment->system = $request->input('system');
        $assignment->country_id = $request->input('country_id');
        $assignment->vat_rate_id = $request->input('vat_rate_id');
        $assignment->currency = $request->input('currency');
        $assignment->internal_cost_rate = $request->input('internal_cost_rate');
        $assignment->external_cost_rate = $request->input('external_cost_rate');
        $assignment->invoice_rate = $request->input('invoice_rate');
        $assignment->currency_sec = $request->input('currency_sec');
        $assignment->internal_cost_rate_sec = $request->input('internal_cost_rate_sec');
        $assignment->external_cost_rate_sec = $request->input('external_cost_rate_sec');
        $assignment->invoice_rate_sec = $request->input('invoice_rate_sec');
        $assignment->rate_sec = $request->input('rate_sec');
        $assignment->fixed_labour_cost_sec = $request->input('fixed_labour_cost_sec');
        $assignment->fixed_price_sec = $request->input('fixed_price_sec');
        $assignment->assignment_status = $request->input('assignment_status');
        $assignment->claim_approver_id = $request->input('claim_approver_id');
        $assignment->medical_certificate_type = $request->input('medical_certificate_type');
        $assignment->medical_certificate_text = $request->input('medical_certificate_text');
        $assignment->assignment_approver = $request->input('assignment_approver');
        $assignment->project_type = $request->input('project_type');
        $assignment->status = $request->input('status');
        $assignment->creator_id = $request->input('creator_id');
        $assignment->company_id = $request->input('company_id');
        $assignment->location = $request->input('location');
        $assignment->hours_of_work = $request->input('hours_of_work');
        $assignment->role = $request->input('role');
        $assignment->vendor_template_id = $request->input('vendor_template_id');
        $assignment->resource_template_id = $request->input('resource_template_id');
        $assignment->max_time_per_day_hours = $request->input('max_time_per_day_hours');
        $assignment->max_time_per_day_min = $request->input('max_time_per_day_min');
        $assignment->capacity_allocation_hours = $request->input('capacity_allocation_hours');
        $assignment->capacity_allocation_min = $request->input('capacity_allocation_min');
        $assignment->capacity_allocation_percentage = $request->input('capacity_allocation_percentage');
        $assignment->planned_start_date = $request->input('planned_start_date');
        $assignment->planned_end_date = $request->input('planned_end_date');
        $assignment->product_owner_id = $request->input('product_owner_id');
        $assignment->project_manager_ts_approver = $request->input('project_manager_ts_approver');
        $assignment->product_owner_ts_approver = $request->input('product_owner_ts_approver');
        $assignment->claim_approver_ts_approver = $request->input('claim_approver_ts_approver');
        $assignment->report_to_ts_approver = $request->input('report_to_ts_approver');

        //Start - New fields
        $assignment->project_manager_new_id = $request->input('project_manager_new_id');
        $assignment->project_manager_new_ts_approver = $request->input('project_manager_new_ts_approver');
        $assignment->line_manager_id = $request->input('line_manager_id');
        $assignment->line_manager_ts_approver = $request->input('line_manager_ts_approver');
        $assignment->resource_manager_id = $request->input('resource_manager_id');
        $assignment->resource_manager_ts_approver = $request->input('resource_manager_ts_approver');
        $assignment->internal_owner_known_as = $request->input('internal_owner_known_as');
        $assignment->product_owner_known_as = $request->input('product_owner_known_as');
        $assignment->project_manager_new_known_as = $request->input('project_manager_new_known_as');
        $assignment->line_manager_known_as = $request->input('line_manager_known_as');
        $assignment->claim_approver_known_as = $request->input('claim_approver_known_as');
        $assignment->resource_manager_known_as = $request->input('resource_manager_known_as');
        $assignment->copy_from_user = $request->input('copy_from_user');

        $assignment->fixed_labour_cost = $request->input('fixed_labour_cost');
        $assignment->fixed_price = $request->input('fixed_price');
        //End - New fields

        $expense = Expense::find($assignment->exp_id);
        if (isset($expense->id) && $expense->id > 0) {
            $expense->travel = $request->input('travel');
            $expense->parking = $request->input('parking');
            $expense->accommodation = $request->input('accommodation');
            $expense->per_diem = $request->input('per_diem');
            $expense->out_of_town = $request->input('out_of_town');
            $expense->data = $request->input('data');
            $expense->other = $request->input('other');
            $expense->save();
        }

        $assignment->save();

        activity()->on($assignment)->withProperties(['full_name' => $assignment->resource->first_name.' '.$assignment->resource->last_name])->log('updated');

        return redirect(route('assignment.index'))->with('flash_success', 'Assignment updated successfully');
    }

    public function complete(Assignment $assignment)
    {
        if (!in_array($assignment->assignment_status, [4,5])){
            $tasks = Task::where('employee_id', $assignment->employee_id)->where('project_id', $assignment->project_id)->get();
            foreach ($tasks as $task){
                $task->status = 5;
                $task->save();
            }

            $assignment->assignment_status = 4;
            $assignment->save();
        }

        return redirect()->back()->with('flash_success', 'Assignment completed successfully');

    }


    public function getUserDetails(Request $request): JsonResponse
    {
        $user_id = $request->user_id;
        $user = User::find($user_id);

        return response()->json($user);
    }

    public function addAssCost($assignmentid)
    {
        $r = User::find(Auth::id());

        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $assignment = Assignment::with(['resource', 'project.customer'])->where('id', '=', $assignmentid)->first();
        $accounts = Account::with('account_type')->orderBy('account_type_id', 'desc')->orderBy('description', 'desc')->get();
        $payment_method_dropdown = PaymentMethod::orderBy('description')->pluck('description', 'id')->prepend('Select Payment Method', '0');
        $status = Status::orderBy('description')->pluck('description', 'id')->prepend('Select Status', '0');
        $assignment_costs = AssignmentCost::with('payment_meth')->where('id', '>', 0)->where('ass_id', '=', $assignmentid)->paginate(15);
        foreach ($accounts as $account) {
            $account_dropdown[$account->id] = substr($account->account_type->description, 0, 1).' - '.$account->description;
        }

        $parameters = [
            'assignment' => $assignment,
            'account_dropdown' => $account_dropdown,
            'payment_method_dropdown' => $payment_method_dropdown,
            'status_dropdown' => $status,
            'assignment_costs' => $assignment_costs,
        ];

        return view('assignment.add_cost.add')->with($parameters);
    }

    public function get_account_type(Request $request): JsonResponse
    {
        $account_element = AccountElement::select('id', 'description', 'account_element')
            ->where('account_id', '=', $request->account)
            ->distinct('description')
            ->latest()
            ->get();
        if ($request->ajax()) {
            $response = [
                'account_element' => $account_element,
                'status' => 'success',
            ];

            return response()->json($response);
        } else {
            return response()->json(['msg' => 'false']);
        }
    }

    public function getAccountType(Request $request): JsonResponse
    {
        $account_element = AccountElement::select('id', 'description', 'account_element')
            ->where('account_id', '=', $request->account)
            ->orderBy('description')
            ->get();

        $response = [
            'account_element' => $account_element,
            'status' => 'success',
        ];

        return response()->json($response);
    }

    public function storeAssCost(StoreAddAssignmentCostRequest $request)
    {

        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        //return $request->all();
        $assignment_cost = new AssignmentCost();
        $assignment_cost->ass_id = $request->input('ass_id');
        $assignment_cost->account_id = $request->input('account');
        $assignment_cost->account_element_id = $request->input('account_element');
        $assignment_cost->description = $request->input('description');
        $assignment_cost->note = $request->input('note');
        $assignment_cost->cost = $request->input('cost');
        $assignment_cost->payment_frequency = $request->input('payment_freq');
        $assignment_cost->payment_method = $request->input('payment_method');
        $assignment_cost->status = $request->input('status');
        $assignment_cost->creator_id = auth()->id();
        $assignment_cost->company_id = $request->input('company');
        $assignment_cost->save();

        return redirect()->route('assignment.show', $request->input('ass_id'))->with('flash_success', 'Assignment Cost Added successfully');
    }

    public function editAssCost($ass_cost_id)
    {

        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $accounts = Account::with('account_type')->orderBy('account_type_id', 'desc')->orderBy('description', 'desc')->get();
        foreach ($accounts as $account) {
            $account_dropdown[$account->id] = substr($account->account_type->description, 0, 1).' - '.$account->description;
        }
        $parameters = [
            'assignment_costs' => AssignmentCost::find($ass_cost_id),
            'account_dropdown' => $account_dropdown,
            'payment_method_dropdown' => PaymentMethod::orderBy('description')->pluck('description', 'id')->prepend('Select Payment Method', '0'),
            'status_dropdown' => Status::orderBy('description')->pluck('description', 'id')->prepend('Select Status', '0'),
        ];

        return view('assignment.add_cost.edit')->with($parameters);
    }

    public function updateAssCost(UpdateAddAssignmentCostRequest $request, $assignment_cost_id)
    {
        $module = Module::where('name', '=', \App\Models\Assignment::class)->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        $assignment_cost = AssignmentCost::find($assignment_cost_id);
        $assignment_cost->ass_id = $request->input('ass_id');
        $assignment_cost->account_id = $request->input('account');
        $assignment_cost->account_element_id = $request->input('account_element');
        $assignment_cost->description = $request->input('description');
        $assignment_cost->note = $request->input('note');
        $assignment_cost->cost = $request->input('cost');
        $assignment_cost->payment_frequency = $request->input('payment_freq');
        $assignment_cost->payment_method = $request->input('payment_method');
        $assignment_cost->status = $request->input('status');
        $assignment_cost->creator_id = auth()->id();
        $assignment_cost->company_id = $request->input('company');
        $assignment_cost->save();

        return redirect()->route('assignment.show', $request->input('ass_id'))->with('flash_success', 'Assignment Cost Added successfully');
    }

    public function sendResourceAssignmentDigisign($assignment_id, Request $request): RedirectResponse
    {
        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();

        $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
        $claim_approver_user = User::find(isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0);
        $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
        $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
        $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

        $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

        $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
        $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

        $resource_name = $resource_first_name.' '.$resource_last_name;

        $resource = User::find($request->id);
        $parameters = [
            'resource' => $resource,
            'resource_name' => $resource_name,
            'claim_approver' => $claim_approver,
            'assignment' => $assignment,
            'expense' => $expense,
        ];
        $pdf = Pdf::view('template.resource_assignment', $parameters);

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $document = new Document();
            $document->type_id = 1;
            $document->document_type_id = 1; //document type Assignment(documenttype table in db)
            $document->name = 'Resource Assignment Digisign - '.$resource_name;
            $document->file = $file_name_pdf;
            $document->creator_id = auth()->id();
            $document->owner_id = $assignment->employee_id;
            $document->digisign_status_id = 2;
            $document->reference_id = $assignment_id;
            $document->digisign_approver_user_id = isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0;
            $document->save();

            if ($request->has('resource_email')) {
                //Mail::to(trim($request->input('resource_email')))->send(new ResourceAssignmentMail($resource_name, $document->id));
                SendResourceAssignmentEmailJob::dispatch(trim($request->input('resource_email')), $resource_name, $document->id)->delay(now()->addMinutes(5));
            }

            $pdf->save($file_name_pdf);

            return redirect(route('assignment.show', $assignment_id))->with('flash_success', 'Resource Assignemnt digisign sent successfully.');
        } catch (\Exception $e) {
            report($e);

            return redirect(route('assignment.show', $assignment_id)->with('flash_danger', $e->getMessage()));
        }
    }

    public function sendSupplierAssignmentDigisign($assignment_id): RedirectResponse
    {
        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();
        $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
        $claim_approver_user = User::find(isset($assignment->assignment_approver) ? $assignment->assignment_approver : 0);
        $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
        $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
        $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

        $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

        $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
        $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

        $resource_name = $resource_first_name.' '.$resource_last_name;

        $config = SiteConfig::get()->first();
        if (isset($config->company_id)) {
            $company = Company::find($config->company_id);
        } else {
            $company = Company::find($config->company_id);
        }

        $vendor = null;
        if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0) {
            $vendor = Vendor::find($resource_user->vendor_id);
        }

        $parameters = [
            'resource' => $resource_user,
            'resource_name' => $resource_name,
            'claim_approver' => $claim_approver,
            'assignment' => $assignment,
            'expense' => $expense,
            'company_name' => isset($company->company_name) ? $company->company_name : '',
            'supplier_name' => isset($vendor->vendor_name) ? $vendor->vendor_name : '',
        ];

        $pdf = Pdf::view('template.supplier_assignment', $parameters);

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $pdf->save($file_name_pdf);

            $document = new Document();
            $document->type_id = 1;
            $document->document_type_id = 1; //document type Assignment(documenttype table in db)
            $document->name = 'Supplier Assignment Digisign - '.$resource_name;
            $document->file = $file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = $assignment->employee_id;
            $document->digisign_status_id = 2;
            $document->reference_id = $assignment_id;
            $document->digisign_approver_user_id = isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0;
            $document->save();

            if (isset($claim_approver_user)) {
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = '';
                $digisign_users->user_email = $claim_approver_user->email;
                $digisign_users->user_id = $claim_approver_user->id;
                $digisign_users->owner_id = $resource_user->id;
                $digisign_users->user_number = 1;
                $digisign_users->is_claim_approver = 1;
                $digisign_users->signed = 0;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();
                //Mail::to(trim($claim_approver_user->email))->send(new SupplierAssignmentMail($claim_approver_user->first_name.' '.$claim_approver_user->last_name, $digisign_users->id, $assignment_id));
                SendSupplierAssignmentEmailJob::dispatch(trim($claim_approver_user->email), $claim_approver_user->first_name.' '.$claim_approver_user->last_name, $digisign_users->id, $assignment_id)->delay(now()->addMinutes(5));
            }

            if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0) {
                $vendor = Vendor::find($resource_user->vendor_id);
                $vendor_user = User::find($vendor->assignment_approver);

                if (isset($vendor_user)) {
                    $digisign_users = new DigisignUsers();
                    $digisign_users->document_id = $document->id;
                    $digisign_users->user_name = '';
                    $digisign_users->user_email = $claim_approver_user->email;
                    $digisign_users->user_id = $vendor_user->id;
                    $digisign_users->owner_id = $resource_user->id;
                    $digisign_users->user_number = 1;
                    $digisign_users->is_claim_approver = 0;
                    $digisign_users->signed = 0;
                    $digisign_users->creator_id = auth()->id();
                    $digisign_users->save();

                    //Mail::to(trim($vendor_user->email))->send(new SupplierAssignmentMail($vendor_user->first_name . ' ' . $vendor_user->last_name, $digisign_users->id, $assignment_id));

                    SendSupplierAssignmentEmailJob::dispatch(trim($claim_approver_user->email), $vendor_user->first_name.' '.$vendor_user->last_name, $digisign_users->id, $assignment_id)->delay(now()->addMinutes(5));
                }
            }

            $assignment->issue_date = now()->toDateString();
            $assignment->save();

            return redirect(route('assignment.show', $assignment_id))->with('flash_success', 'Supplier Assignemnt digisign sent successfully.');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('assignment.show', $assignment_id)->with('flash_danger', $e->getMessage()));
        }
    }

    public function sendEmployeeAssignmentDigisign($assignment_id): RedirectResponse
    {
        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();
        $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
        $claim_approver_user = User::find(isset($assignment->assignment_approver) ? $assignment->assignment_approver : 0);
        $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
        $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
        $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

        $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

        $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
        $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

        $resource_name = $resource_first_name.' '.$resource_last_name;

        $config = SiteConfig::get()->first();
        if (isset($config->company_id)) {
            $company = Company::find($config->company_id);
        } else {
            $company = Company::find($config->company_id);
        }

        $vendor = null;
        if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0) {
            $vendor = Vendor::find($resource_user->vendor_id);
        }

        $parameters = [
            'resource' => $resource_user,
            'resource_name' => $resource_name,
            'claim_approver' => $claim_approver,
            'assignment' => $assignment,
            'expense' => $expense,
            'company_name' => isset($company->company_name) ? $company->company_name : '',
            'supplier_name' => isset($vendor->vendor_name) ? $vendor->vendor_name : '',
        ];

        $pdf = Pdf::view('template.resource2_assignment', $parameters);

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $pdf->save($file_name_pdf);

            $document = new Document();
            $document->type_id = 1;
            $document->document_type_id = 1; //document type Assignment(documenttype table in db)
            $document->name = 'Supplier Assignment Digisign - '.$resource_name;
            $document->file = $file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = $assignment->employee_id;
            $document->digisign_status_id = 2;
            $document->reference_id = $assignment_id;
            $document->digisign_approver_user_id = isset($assignment->claim_approver_id) ? $assignment->claim_approver_id : 0;
            $document->save();

            if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0) {
                $vendor = Vendor::find($resource_user->vendor_id);
                $vendor_user = User::find($vendor->assignment_approver);

                if (isset($vendor_user)) {
                    $digisign_users = new DigisignUsers();
                    $digisign_users->document_id = $document->id;
                    $digisign_users->user_name = '';
                    $digisign_users->user_email = $claim_approver_user->email;
                    $digisign_users->user_id = $vendor_user->id;
                    $digisign_users->owner_id = $resource_user->id;
                    $digisign_users->user_number = 1;
                    $digisign_users->is_claim_approver = 0;
                    $digisign_users->signed = 0;
                    $digisign_users->creator_id = auth()->id();
                    $digisign_users->save();
                    //Mail::to(trim($vendor_user->email))->send(new SupplierAssignmentMail($vendor_user->first_name . ' ' . $vendor_user->last_name, $digisign_users->id, $assignment_id));

                    SendSupplierAssignmentEmailJob::dispatch(trim($claim_approver_user->email), $vendor_user->first_name.' '.$vendor_user->last_name, $digisign_users->id, $assignment_id)->delay(now()->addMinutes(5));
                }
            }

            $assignment->approved_date = now()->toDateString();
            $assignment->save();

            return redirect(route('assignment.show', $assignment_id))->with('flash_success', 'Resource Assignemnt digisign sent successfully.');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('assignment.show', $assignment_id)->with('flash_danger', $e->getMessage()));
        }
    }

    public function addExtension($id): View
    {
        $parameters = [
            'assignment_id' => $id,
        ];

        return view('assignment.extension.add')->with($parameters);
    }

    public function editExtension($extension_id): View
    {
        $assignment_extension = AssignmentExtension::find($extension_id);

        $parameters = [
            'assignment_extension' => $assignment_extension,
        ];

        return view('assignment.extension.edit')->with($parameters);
    }

    public function saveExtension($id, StoreAssignmentExtensionRequest $request): RedirectResponse
    {
        $extension_end_date = $request->input('extension_end_date');

        $extension = new AssignmentExtension();
        $extension->assignment_id = $id;
        $extension->extension_date = $request->input('extension_date');
        $extension->extension_hours = $request->input('extension_hours');
        $extension->extension_notes = $request->input('extension_notes');
        $extension->extension_start_date = $request->input('extension_start_date');
        $extension->extension_end_date = $extension_end_date;
        $extension->extension_ref = $request->input('extension_ref');
        $extension->status_id = $request->input('status_id');
        $extension->creator_id = Auth::id();
        $extension->save();

        $assignment = Assignment::find($id);
        /*if ($assignment->end_date < $extension_end_date):
            $assignment->end_date = $extension_end_date;
            $assignment->save();
        endif;*/
        $project = Project::find($assignment->project_id);
        if ($project->end_date < $extension_end_date) {
            $project->end_date = $extension_end_date;
            $project->save();
        }

        return redirect()->route('assignment.edit', $id)->with('flash_success', 'Assignment Extension added successfully');
    }

    public function updateExtension($id, UpdateAssignmentExtensionRequest $request): RedirectResponse
    {
        $extension = AssignmentExtension::find($id);
        $extension->extension_date = $request->input('extension_date');
        $extension->extension_hours = $request->input('extension_hours');
        $extension->extension_notes = $request->input('extension_notes');
        $extension->extension_start_date = $request->input('extension_start_date');
        $extension->extension_end_date = $request->input('extension_end_date');
        $extension->extension_ref = $request->input('extension_ref');
        $extension->status_id = $request->input('status_id');
        $extension->creator_id = Auth::id();
        $extension->save();

        return redirect()->route('assignment.edit', $id)->with('flash_success', 'Assignment Extension added successfully');
    }

    public function deleteExtension($extension_id): RedirectResponse
    {
        $assignment_extention = AssignmentExtension::find($extension_id);
        $assignment_id = $assignment_extention->assignment_id;

        AssignmentExtension::destroy($extension_id);

        return redirect()->route('assignment.edit', $assignment_id)->with('flash_success', 'Assignment extension deleted successfully');
    }

    public function destroy($id)
    {
        $r = User::find(Auth::id());

        if (! $r->authoriseAction('delete', Assignment::class)) {
            return abort(403);
        }

        $assignment = Assignment::find($id);

        activity()->on($assignment)->withProperties(['name' => auth()->user()->first_name.' '.auth()->user()->last_name.' deleted an assignment '.substr($assignment->project->name, 0, 10).'('.substr($assignment->resource->first_name, 0, 1).substr($assignment->resource->last_name, 0, 1).')'])->log('deleted');
        DB::table('assignment')->delete($id);
        Assignment::destroy($id);

        return redirect()->route('assignment.index')->with('flash_success', 'Assignment deleted successfully');
    }

    public function currency(Request $request, Currency $currency): JsonResponse
    {
        $rate = $currency->conversionRate($request->currency);

        if (isset($rate['success'])) {
            $currency_rate = $rate['rate'] ?? 0.00;
            $payload = [
                'rate' => $currency_rate,
                'internal_cost_rate_sec' => round(($request->internal_cost_rate ?? 0) * $currency_rate, 2),
                'external_cost_rate_sec' => round((($request->external_cost_rate ?? 0) * $currency_rate), 2),
                'invoice_rate_sec' => round((($request->invoice_rate ?? 0) * $currency_rate), 2),
                'rate_sec' => round((($request->rate ?? 0) * $currency_rate), 2),
            ];

            if ($request->fixed_labour_cost) {
                $payload['fixed_labour_cost_sec'] = round((($request->fixed_labour_cost ?? 0) * $currency_rate), 2);
            }

            if ($request->fixed_price) {
                $payload['fixed_price_sec'] = round((($request->fixed_price ?? 0) * $currency_rate), 2);
            }

            return response()->json($payload);
        }

        throw new \Exception($rate['errors']);
    }

    private function doesAnAssisnmentExist()
    {
        return Assignment::where([
            'employee_id' => request()->employee_id,
            'project_id' => request()->project_id,
        ])->count();
    }

    private function iteratorDropdown(int $max): array
    {
        $results = [];
        for ($i = 0; $i < $max; $i++) {
            array_push($results, $i);
        }

        return $results;
    }

    private function profitPercentage(int|float $profit, int|float $invoice): string
    {
        try {
            $percentage = number_format(($profit/$invoice),2,'.',',')*100;
        }catch (\DivisionByZeroError $exception){
            $percentage = 0.00;
        }

        return $profit.' => '. $percentage."%";
    }

    public  function getTasks($assignment_id){
        $assignment = Assignment::find($assignment_id);

        $tasks = Task::where('project_id',$assignment->project_id)->where('employee_id',$assignment->employee_id)->whereIn('status',[1,2,3])->get()->count();

        return response()->json(['tasks'=>$tasks]);
    }

    public function printSupplierAgreement(Assignment $assignment)
    {
        if (!isset($assignment->consultant->vendor))
        {
            return redirect(route('assignment.show', $assignment))
                ->with('flash_danger', 'The assigned consultant doesn\'t have a vendor.');
        }

        try {

            $template = $assignment->mapVariables($assignment->vendorTemplate);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name($assignment->project?->name . ".pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('assignment.edit', $assignment))
                ->with('flash_danger', 'Edit your Assignment and select a vendor agreement.');
        }
    }

    public function sendSupplierAgreement(Assignment $assignment)
    {
        $template = $assignment->mapVariables($assignment->vendorTemplate);

        try {
            if (! Storage::exists('Assignment/templates')) {
                Storage::makeDirectory('Assignment/templates');
            }

            $filename = "supplier_agreement_".str_replace(' ', '_',$assignment->project?->name)."_".date('Y_m_d_H_i_s').'.pdf';

            $storage = storage_path("app/Assignment/templates/{$filename}");

            Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                ->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->save($storage);

            $assignment->documents()->create([
                'name' => "Supplier Agreement {$assignment->consultant?->name()}",
                'file' => $filename,
                'document_type_id' => 2,
                'creator_id' => auth()->id(),
                'status_id' => \App\Enum\Status::ACTIVE->value,
            ]);

            $message = [
                "subject" => "Supplier Agreement for {$assignment->consultant?->vendor?->vendor_name}",
                "body" => "Please find the supplier agreement for {$assignment->consultant?->vendor?->vendor_name} attached.",
            ];

            ContractJob::dispatch(
                [$assignment->claimApprover?->email, $assignment->consultant?->vendor?->account_managerd?->email, $assignment->report_to_email],
                $message,
                $storage
            );

            return redirect(route('assignment.show', $assignment))->with('flash_success', 'The supplier agreement has been sent successfully');

        }catch (\TypeError $e){
            return redirect(route('assignment.edit', $assignment))->with('flash_danger', 'Edit your Assignment and select a vendor agreement.');
        }
    }

    public function printResourceAgreement(Assignment $assignment)
    {
        try {

            $template = $assignment->mapVariables($assignment->resourceTemplate);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name($assignment->project?->name . ".pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('assignment.edit', $assignment))
                ->with('flash_danger', 'Edit your Assignment and select a resource agreement.');
        }
    }

    public function sendResourceAgreement(Assignment $assignment)
    {
        $template = $assignment->mapVariables($assignment->resourceTemplate);

        try {
            if (! Storage::exists('Assignment/templates')) {
                Storage::makeDirectory('Assignment/templates');
            }

            $filename = "resource_agreement_".str_replace(' ', '_',$assignment->project?->name)."_".date('Y_m_d_H_i_s').'.pdf';

            $storage = storage_path("app/Assignment/templates/{$filename}");

            Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                ->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->save($storage);

            $assignment->documents()->create([
                'name' => "Resource Agreement {$assignment->consultant?->name()}",
                'file' => $filename,
                'document_type_id' => 2,
                'creator_id' => auth()->id(),
                'status_id' => \App\Enum\Status::ACTIVE->value,
            ]);

            $message = [
                "subject" => "Resource Agreement for {$assignment->consultant?->name()}",
                "body" => "Please find the supplier agreement for {$assignment->consultant?->name()} attached.",
            ];

            ContractJob::dispatch(
                [$assignment->claimApprover?->email, $assignment->consultant?->email, $assignment->report_to_email],
                $message,
                $storage
            );

            return redirect(route('assignment.show', $assignment))->with('flash_success', 'The supplier agreement has been sent successfully');

        }catch (\TypeError $e){
            return redirect(route('assignment.edit', $assignment))->with('flash_danger', 'Edit your Assignment and select a vendor agreement.');
        }
    }
}
