<?php

namespace App\Http\Controllers;

use App\Models\AssessmentConcern;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentConcernController extends Controller
{
    public function edit($id): View
    {
        $assessmnet_concern = AssessmentConcern::find($id);
        $parameters = [
            'assessment_concern' => $assessmnet_concern,
        ];

        return view('assessment.assessment_concern.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $assessment_concern = AssessmentConcern::find($id);
        $assessment_concern->notes = $request->notes;
        $assessment_concern->save();

        return redirect()->route('assessment.show', $assessment_concern->assessment_id)->with('flash_success', 'Assessment Concern Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $assessment_concern = AssessmentConcern::find($id);
        AssessmentConcern::destroy($id);

        return redirect()->route('assessment.show', $assessment_concern->assessment_id)->with('flash_success', 'Assessment Concern Deleted Successfully');
    }
}
