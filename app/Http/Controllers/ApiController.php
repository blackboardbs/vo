<?php

namespace App\Http\Controllers;

use App\Models\Api;
use App\Models\ApiRequests;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Company;
use App\Models\UserStory;
use App\Models\Feature;
use App\Models\Epic;
use App\Models\InvoiceStatus;
use App\Models\CostCenter;
use App\Models\BillingCycle;
use App\Models\Task;
use App\Models\Timesheet;
use App\Models\Assignment;
use App\Models\Timeline;
use App\Models\BillingPeriod;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function handle(Request $request)
    {
        return response()->json(['message' => 'API received'], 200);
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $token = Api::where('user', $user->id)->first();
        $port = $request->getPort();

        if(Auth::user()->hasRole('admin')){
            $api_requests = ApiRequests::with(['api_user:id,email'])->orderBy('created_at', 'desc')->paginate(request()->s??15);
        }else{
            $api_requests = ApiRequests::with(['api_user:id,email'])->where('user', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(request()->s??15);
        }

        if($request->has('q')){
            $api_requests = ApiRequests::with(['api_user:id,email'])->where('user', 'like', '%'.$request->q.'%')
                ->orWhere('request_url', 'like', '%'.$request->q.'%')
                ->orWhere('request_from', 'like', '%'.$request->q.'%')
                ->orderBy('created_at', 'desc')->paginate(request()->s??15);
        }

        $parameters = [
            'user' => $user,
            'token' => $token,
            'port' => $port,
            'timelines' => [],
            'customer_drop_down' => [],
            'project_drop_down' => [],
            'company_drop_down' => [],
            'resource_drop_down' => [],
            'task_drop_down' => [],
            'user_stories_dropdown' => [],
            'feature_dropdown' => [],
            'epic_dropdown' => [],
            'invoice_status_dropdown' => [],
            'weeks_dropdown' => [],
            'months_dropdown' => [],
            'cost_center_dropdown' => [],
            'billing_cycle_dropdown' => [],
            'api_requests' => $api_requests,
        ];

        return view('api.index')->with($parameters);
    }

    public function requests(Request $request)
    {
        if(Auth::user()->hasRole('admin')){
            $api_requests = ApiRequests::orderBy('created_at', 'desc')->paginate(request()->s??15);
        }else{
            $api_requests = ApiRequests::where('user', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(request()->s??15);
        }

        if($request->has('q')){
            $api_requests = ApiRequests::where('user', 'like', '%'.$request->q.'%')
                ->orWhere('request_url', 'like', '%'.$request->q.'%')
                ->orWhere('request_from', 'like', '%'.$request->q.'%')
                ->orderBy('created_at', 'desc')->paginate(request()->s??15);
        }

        return view('api.requests', ['api_requests' => $api_requests]);
    }

    private function resources(Request $request)
    {
        return Assignment::filters()->with(['consultant' => function($query){
            $query->selectRaw('id, CONCAT(`first_name`, " ", `last_name`) AS resource')->orderBy('first_name');
        }])->whereHas('consultant')->select('employee_id')->get()
            ->keyBy('employee_id')
            ->map(function ($assignment){
                return $assignment->consultant && $assignment->consultant->resource != '' ? $assignment->consultant->resource : null;
            })->sortBy('resource');
    }

    public function generateYearMonth(Request $request)
    {
        $years = TimeSheet::filters()->selectRaw("DISTINCT(CONCAT(`year`,'-',CASE WHEN `month` < 10 THEN CONCAT('0',`month`) ELSE `month` END)) as yearmonth")->pluck('yearmonth');

        if(count($years) == 0){
            $years2 = TimeSheet::distinct('year')->orderBy('year', 'desc')->pluck('year');

            $year_month_drop_down = [];
            foreach ($years2 as $year) {
                for ($i = 12; $i >= 1; $i--) {
                    if (($year == date('Y')) && ($i > date('m'))) {
                        continue;
                    }
                    if ($i < 10) {
                        $i = '0' . $i;
                    }
                    $year_month_drop_down[$year . '-' . $i] = $year . $i;
                }
            }
        } else {
            $year_month_drop_down = [];
            if(isset($request->from_month)){ $year_month_drop_down[$request->from_month] = str_replace('-','',$request->from_month); }
            if(isset($request->to_month) && $request->to_month != $request->from_month){ $year_month_drop_down[$request->to_month] = str_replace('-','',$request->to_month); }
            foreach ($years as $key => $year) {
                if(str_replace('-','',$year) != $request->to_month && str_replace('-','',$year) != $request->from_month){
                    $year_month_drop_down[$year] = str_replace('-','',$year);
                }
            }
        }
        arsort($year_month_drop_down);
        return $year_month_drop_down;
    }

    public function generateYearWeek(Request $request)
    {
        $weeks = TimeSheet::filters()->orderBy('year_week', 'desc')->whereNotNull('year_week')->pluck('year_week', 'year_week');

        if(count($weeks) == 0){
            $year_week_drop_down = TimeSheet::distinct('year_week')->orderBy('year_week', 'desc')->pluck('year_week','year_week');
        } else {
            $year_week_drop_down = collect($weeks)->toArray();
            if(isset($request->from_week)){ $year_week_drop_down[$request->from_week] = $request->from_week; }
            if(isset($request->to_week) && $request->to_week != $request->from_week){ $year_week_drop_down[$request->to_week] = $request->to_week; }
        arsort($year_week_drop_down);
        }
        return $year_week_drop_down;
    }

    
    public function generateTaskDropdown(Request $request){
        $list = Task::when(request()->employee,function($q){
            $q->where('employee_id', request()->employee);
        })->when(request()->resource,function($q){
            $q->where('employee_id', request()->resource);
        })->when(request()->project,function($q){
            $q->where('project_id','=', request()->project);
        })->when(isset(request()->customer) && request()->customer != '-1' && request()->customer != '' && request()->customer != 0, function ($task) {
            $task->where('customer_id', request()->customer);
        })->when(isset(request()->company) && request()->company != '-1' && request()->company != '' && request()->company != 0, function ($task) {
            $task->whereHas('project.company', function ($t) {
                $t->where('id', '=', request()->company);
            });
        })->orderBy('description')->pluck('description', 'id');

        return $list;
    }

    public function timesheetApi(Request $request)
    {
        $timelines = Timeline::with(['task' => function($query){
            $query->select('id', 'description');
        }, 'timesheet' => function($q){
            $q->select(['id', 'employee_id', 'project_id', 'customer_id', 'company_id', 'year_week'])
                ->with([
                    'user' => function($q){
                        $q->selectRaw('id, CONCAT(`first_name`," ",`last_name`) AS resource');
                    },
                    'project' => function($q){
                    $q->select('id', 'name', 'customer_id');
                }]);
        }])->selectRaw(
            'timesheet_id, description_of_work, SUM((mon * 60) + mon_m) AS monday, SUM((tue * 60) + tue_m) AS tuesday,
             SUM((wed * 60) + wed_m) AS wednesday, SUM((thu * 60) + thu_m) AS thursday, SUM((fri * 60) + fri_m) AS friday,
              SUM((sat * 60) + sat_m) AS saturday, SUM((sun * 60) + sun_m) AS sunday, SUM((total * 60) + total_m) AS total,
              is_billable, task_id'
        )->when(request()->customer, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('customer_id', request()->customer);
            });
        })->when(request()->project, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('project_id', request()->project);
            });
        })->when(request()->company, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('company_id', request()->company);
            });
        })->when(request()->resource, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('employee_id', request()->resource);
            });
        })->when(request()->billable, function ($timeline){
            $timeline->where('is_billable', request()->billable);
        })->when(request()->task, function ($timeline){
            $timeline->where('task_id', request()->task);
        })->when(request()->user_story, function ($timeline){
            $timeline->whereHas('task', function ($q){
                $q->where('user_story_id', request()->user_story);
            });
        })->when(request()->feature, function ($timeline){
            $timeline->whereHas('task.user_story', function ($q){
                $q->where('feature_id', request()->feature);
            });
        })->when(request()->epic, function ($timeline){
            $timeline->whereHas('task.user_story.feature', function ($q){
                $q->where('epic_id', request()->epic);
            });
        })->when(request()->invoice_status, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('bill_status', request()->invoice_status);
            });
        })->when(request()->from_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '>=', request()->from_week);
            });
        })->when(request()->to_week, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('year_week', '<=', request()->to_week);
            });
        })->when(request()->from_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','>=', substr(request()->from_month,5))
                    ->where('year', '>=', substr(request()->from_month, 0, 4));
            });
        })->when(request()->filled('billing_period_id'), function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $billing_period = BillingPeriod::find(request()->billing_period_id);
                if (isset($billing_period)){
                    $q->where('first_day_of_month', '>=', $billing_period->start_date)
                        ->where('last_day_of_month', '<=', $billing_period->end_date);
                }
            });
        })->when(request()->to_month, function ($timeline){
            $timeline->whereHas('timesheet', function ($q){
                $q->where('month','<=', substr(request()->to_month,5))
                ->where('year', '<=', substr(request()->to_month, 0, 4));
            });
        })->when(request()->cost_center, function ($timeline){
            $timeline->whereHas('timesheet.project.assignment.resource_user', function ($q){
                $q->where('cost_center_id', request()->cost_center);
            });
        })->groupBy(['timesheet_id', 'description_of_work', 'is_billable', 'task_id'])
            ->orderBy('timesheet_id', 'desc')->take(15)->get();;        
        
        $resources = collect($this->resources($request))->toArray();
        asort($resources);

        $parameters = [
            'timelines' => $timelines,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => $resources,
            'task_drop_down' => $this->generateTaskDropdown($request),
            'user_stories_dropdown' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
            'feature_dropdown' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
            'epic_dropdown' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
            'invoice_status_dropdown' => InvoiceStatus::orderBy('description')->pluck('description', 'id'),
            'weeks_dropdown' => $this->generateYearWeek($request),
            'months_dropdown' => $this->generateYearMonth($request),
            'cost_center_dropdown' => CostCenter::orderBy('description')->pluck('description', 'id'),
            'billing_cycle_dropdown' => BillingCycle::orderBy('name')->pluck('name', 'id')
        ];

        $dropdownData = [
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => $resources,
            'task_drop_down' => $this->generateTaskDropdown($request),
            'user_stories_dropdown' => UserStory::filters()->orderBy('name')->pluck('name', 'id'),
            'feature_dropdown' => Feature::filters()->orderBy('name')->pluck('name', 'id'),
            'epic_dropdown' => Epic::filters()->orderBy('name')->pluck('name', 'id'),
            'invoice_status_dropdown' => InvoiceStatus::orderBy('description')->pluck('description', 'id'),
            'weeks_dropdown' => $this->generateYearWeek($request),
            'months_dropdown' => $this->generateYearMonth($request),
            'cost_center_dropdown' => CostCenter::orderBy('description')->pluck('description', 'id'),
            'billing_cycle_dropdown' => BillingCycle::orderBy('name')->pluck('name', 'id')
        ];

        $port = $request->getPort();
        $port = [
            'port' => $port,
        ];

        $html = view('reports.timesheet_api', compact('timelines', 'dropdownData', 'port'))->render();
    
        return response()->json(['html' => $html]);
        // return $parameters;
    }
    

    public function show(Request $request){

    }
}
