<?php

namespace App\Http\Controllers;

use App\Models\Wishlist;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class WishListController extends Controller
{
    public function add(Request $request): JsonResponse
    {
        $wishlist = new Wishlist();
        $wishlist->cv_id = $request->input('cv_name_id');
        $wishlist->cv_wishlist_id = $request->input('cv_wishlist_id');
        $wishlist->save();

        return response()->json(['message' => 'Added successfuly to wishlist']);
    }

    public function destroy($cv_id, Request $request): RedirectResponse
    {
        $cv_wishlist_id = $request->input('cv_wishlist_id');
        $wishlist = Wishlist::where('cv_wishlist_id', '=', $cv_wishlist_id)->where('cv_id', '=', $cv_id)->first();

        Wishlist::destroy($wishlist->id);

        return redirect(route('cvwishlist.show', $cv_wishlist_id))->with('flash_success', 'CV removed successfully from wishlist.');
    }
}
