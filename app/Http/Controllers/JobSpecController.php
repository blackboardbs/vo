<?php

namespace App\Http\Controllers;

use App\Exports\CVsExport;
use App\Exports\JobSpecsExport;
use App\Models\Customer;
use App\Models\CVWishList;
use App\Models\Document;
use App\Http\Requests\JobSpecRequest;
use App\Models\JobOrigin;
use App\Models\JobSpec;
use App\Models\JobSpecActivityLog;
use App\Models\JobSpecStatus;
use App\Models\Module;
use App\Models\Profession;
use App\Models\ResourceType;
use App\Models\RoleUser;
use App\Models\ScoutingRole;
use App\Models\Speciality;
use App\Models\System;
use App\Models\User;
use App\Models\Wishlist;
use App\Models\WorkType;
use App\Services\StorageQuota;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Activitylog\Models\Activity;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class JobSpecController extends Controller
{
    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('r') ?? 15;

        $module = Module::where('name', '=', \App\Models\JobSpec::class)->first();

        $job_specs = JobSpec::with([
            'positiontype:id,description',
            'customer:id,customer_name',
            'status:id,name'
        ]); //->where('status_id', '=', 1);

        $money_drop_down = [];
        for ($i = 1000; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }

        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                //Filters to only show team, continue for now
            }
        } else {
            return abort(403);
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        $job_specs = $job_specs->when($request->c,
            fn($job) => $job->where('customer_id', $request->c)
        )->when($request->rn,
            fn($job) => $job->where('reference_number', $request->rn)
        )->when($request->p,
            fn($job) => $job->where('profession_id', $request->p)
        )->when($request->sp,
            fn($job) => $job->where('speciality_id', $request->sp)
        )->when($request->e,
            fn($job) => $job->where('employment_type_id', $request->e)
        )->when($request->o,
            fn($job) => $job->where('origin_id', $request->o)
        )->when($request->s,
            fn($job) => $job->where('status_id', $request->s)
        )->when($request->rm,
            fn($job) => $job->where('hourly_rate_minimum', '>=', $request->rm)
        )->when($request->priority,
            fn($job) => $job->where('priority', $request->priority)
        )->when($request->has('bee'), function ($job) use ($request){
            $value = $request->bee ? 1 : 2;
            $job->where('b_e_e', $value);
        })->when($request->recruiter,
            fn($job) => $job->where('recruiter_id', $request->recruiter)
        )->when($request->rx,
            fn($job) => $job->where('hourly_rate_maximum', '<=', $request->rx)
        )->when($request->sm,
            fn($job) => $job->where('monthly_salary_minimum', '>=', $request->sm)
        )->when($request->sx,
            fn($job) => $job->where('monthly_salary_maximum', '<=', $request->sx)
        );

        $job_specs = $job_specs->paginate($item);

        $documents = [];
        foreach ($job_specs as $job_spec) {
            $documents[$job_spec->id] = Document::where('reference_id', '=', $job_spec->id)->where('document_type_id', 8)->orderBy('id')->get();
        }

        $parameters = [
            'job_specs' => $job_specs,
            'money_drop_down' => $money_drop_down,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'priority_drop_down' => $priority,
            'documents' => $documents,
            'recruiter_ids' => JobSpec::selectRaw('DISTINCT recruiter_id')->whereNotNull('recruiter_id')->pluck('recruiter_id')
        ];

        if ($request->has('export')) return $this->export($parameters, 'jobspec');

        return view('jobspec.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $max_id = JobSpec::withTrashed()->max('id');

        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $resource_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = [1 => 'Open', 2 => 'closed'];
        $job_origin_drop_down = [1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = [1 => 'Full Time', 2 => 'Part Time'];
        $skills_drop_down = System::orderBy('description')->pluck('description', 'id');
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $users = User::role('recruitment')->get();
        $recruiters = [];
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->first_name.' '.$recruiter->last_name;
        }

        $parameters = [
            'users_drop_down' => $users_drop_down->pluck('full_name', 'id'),
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'role_drop_down' => $role_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'work_type_drop_down' => $work_type_drop_down,
            'resource_type_drop_down' => $resource_type_drop_down,
            'next_id' => $max_id + 1,
            'skills_drop_down' => $skills_drop_down,
            'priority' => $priority,
            'recruiters_drop_down' => $recruiters,
        ];

        return view('jobspec.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(JobSpecRequest $request, StorageQuota $quota): RedirectResponse
    {
        $skills = '';
        if ($request->has('skills_id')) {
            foreach ($request->skills_id as $key => $skill) {
                $skills .= ($key < (count($request->skills_id) - 1)) ? $skill.'|' : $skill;
            }
        }

        $job_spec = new JobSpec();
        $job_spec->role_id = $request->input('role_id');
        $job_spec->position_description = $request->input('position_description');
        $job_spec->profession_id = $request->input('profession_id');
        $job_spec->speciality_id = $request->input('speciality_id');
        $job_spec->client = $request->input('client');
        $job_spec->placement_by = $request->input('placement_by');
        $job_spec->customer_id = $request->input('customer_id');
        $job_spec->location = $request->input('location');
        $job_spec->employment_type_id = $request->input('employment_type_id');
        $job_spec->work_type_id = $request->input('work_type_id');
        $job_spec->salary = $request->input('salary');
        $job_spec->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $job_spec->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $job_spec->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $job_spec->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $job_spec->start_date = $request->input('start_date');
        $job_spec->applicaiton_closing_date = $request->input('applicaiton_closing_date');
        $job_spec->skills = $skills;
        $job_spec->priority = $request->input('priority');
        $job_spec->b_e_e = $request->input('b_e_e');
        $job_spec->recruiter_id = $request->input('recruiter_id');
        $job_spec->gender = $request->input('gender');
        $job_spec->duties_and_reponsibilities = $request->input('duties_and_reponsibilities');
        $job_spec->desired_experience_and_qualification = $request->input('desired_experience_and_qualification');
        $job_spec->package_and_renumeration = $request->input('package_and_renumeration');
        $job_spec->respond_to = $request->input('respond_to');
        $job_spec->special_note = $request->input('special_note');
        $job_spec->creator_id = Auth()->id();
        $job_spec->status_id = $request->input('status_id');
        $job_spec->origin_id = $request->input('origin_id');
        $job_spec->save();

        $job_spec->reference_number = 'JS'.$job_spec->id;
        $job_spec->save();

        if ($request->hasFile('document')) {
            if (!$quota->isStorageQuotaReached($request->file('document')->getSize())){
                $name = $request->document->getClientOriginalName();
                $file_name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$request->document->getClientOriginalExtension();
                $stored = $request->document->storeAs('job_spec', $file_name);

                $document = new Document();
                $document->type_id = null;
                $document->document_type_id = 8; //Job Spec documents
                $document->name = $name;
                $document->file = $file_name;
                $document->creator_id = auth()->id();
                $document->owner_id = auth()->id();
                $document->digisign_status_id = null;
                $document->reference_id = $job_spec->id;
                $document->digisign_approver_user_id = null;
                $document->save();
            }
        }

        activity()->on($job_spec)->withProperties(['by' => Auth()->user()->email, 'reference' => $job_spec->reference_number])->log('created');

        $job_spec_activity = new JobSpecActivityLog();
        $job_spec_activity->date = now();
        $job_spec_activity->activity = 'Job Spec Is Created';
        $job_spec_activity->job_spec_id = $job_spec->id;
        $job_spec_activity->author_id = auth()->user()->id;
        $job_spec_activity->status_id = 1;
        $job_spec_activity->save();

        return redirect(route('jobspec.index'))->with('flash_success', 'Job Spec successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobSpec  $jobSpec
     */
    public function show($id): View
    {
        $job_spec = JobSpec::find($id);
        $document = Document::where('reference_id', '=', $id)->where('document_type_id', 8)->orderBy('id')->get();
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $resource_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = [1 => 'Open', 2 => 'closed'];
        $job_origin_drop_down = [1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = [1 => 'Full Time', 2 => 'Part Time'];
        $activities = Activity::where('subject_id', '=', $job_spec->id)->where('subject_type', '=', \App\Models\JobSpec::class)/*->orWhere('subject_type', '=', 'App\CV')->orWhere('subject_type', '=', 'App\Models\CVWishList')*/->orderBy('created_at', 'desc')->get();
        $skills_drop_down = System::orderBy('description')->pluck('description', 'id');
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $users = User::role('recruitment')->get();
        $recruiters = [];
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->first_name.' '.$recruiter->last_name;
        }
        // dd($users);

        $wishlist_drop_down = CVWishList::orderBy('name')->pluck('name', 'id');

        $activity_log = JobSpecActivityLog::with(['jobSpec', 'author'])->where('job_spec_id', $job_spec->id)->get();

        $parameters = [
            'job_spec' => $job_spec,
            'users_drop_down' => $users_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'role_drop_down' => $role_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'work_type_drop_down' => $work_type_drop_down,
            'resource_type_drop_down' => $resource_type_drop_down,
            'activities' => $activities,
            'wishlist_drop_down' => $wishlist_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'priority' => $priority,
            'recruiters_drop_down' => $recruiters,
            'documents' => $document,
            'actitvities_log' => $activity_log,

        ];

        return view('jobspec.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobSpec  $jobSpec
     */
    public function edit($id): View
    {
        $job_spec = JobSpec::find($id);

        $document = Document::where('reference_id', '=', $id)->where('document_type_id', 8)->orderBy('id')->get();
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        if (isset($job_spec->profession_id) && ($job_spec->profession_id > 0)) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('profession_id', '=', $job_spec->profession_id)->pluck('name', 'id');
        }
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id');
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        $resource_type_drop_down = ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id');
        $job_spec_status_drop_down = [1 => 'Open', 2 => 'closed'];
        $job_origin_drop_down = [1 => 'Direct', 2 => 'Web', 3 => 'Email', 4 => 'Other'];
        $work_type_drop_down = [1 => 'Full Time', 2 => 'Part Time'];
        $skills_drop_down = System::orderBy('description')->pluck('description', 'id');
        $priority = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10];
        $users = User::role('recruitment')->get();
        $recruiters = [];
        foreach ($users as $recruiter) {
            $recruiters[$recruiter->id] = $recruiter->first_name.' '.$recruiter->last_name;
        }

        $parameters = [
            'job_spec' => $job_spec,
            'users_drop_down' => $users_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'role_drop_down' => $role_drop_down,
            'job_spec_status_drop_down' => $job_spec_status_drop_down,
            'job_origin_drop_down' => $job_origin_drop_down,
            'work_type_drop_down' => $work_type_drop_down,
            'resource_type_drop_down' => $resource_type_drop_down,
            'skills_drop_down' => $skills_drop_down,
            'priority' => $priority,
            'recruiters_drop_down' => $recruiters,
            'documents' => $document,
        ];

        return view('jobspec.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\JobSpec  $jobSpec
     */
    public function update(JobSpecRequest $request, $id, StorageQuota $quota): RedirectResponse
    {
        $job_spec = JobSpec::find($id);
        $job_spec->role_id = $request->input('role_id');
        $job_spec->position_description = $request->input('position_description');
        $job_spec->profession_id = $request->input('profession_id');
        $job_spec->speciality_id = $request->input('speciality_id');
        $job_spec->client = $request->input('client');
        $job_spec->placement_by = $request->input('placement_by');
        $job_spec->customer_id = $request->input('customer_id');
        $job_spec->location = $request->input('location');
        $job_spec->employment_type_id = $request->input('employment_type_id');
        $job_spec->work_type_id = $request->input('work_type_id');
        $job_spec->salary = $request->input('salary');
        $job_spec->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $job_spec->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $job_spec->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $job_spec->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $job_spec->start_date = $request->input('start_date');
        $job_spec->applicaiton_closing_date = $request->input('applicaiton_closing_date');
        $job_spec->skills = $request->has('skills_id') ? implode('|', $request->skills_id) : null;
        $job_spec->priority = $request->input('priority');
        $job_spec->b_e_e = $request->input('b_e_e');
        $job_spec->recruiter_id = $request->input('recruiter_id');
        $job_spec->gender = $request->input('gender');
        $job_spec->duties_and_reponsibilities = $request->input('duties_and_reponsibilities');
        $job_spec->desired_experience_and_qualification = $request->input('desired_experience_and_qualification');
        $job_spec->package_and_renumeration = $request->input('package_and_renumeration');
        $job_spec->respond_to = $request->input('respond_to');
        $job_spec->special_note = $request->input('special_note');
        $job_spec->creator_id = Auth()->id();
        $job_spec->status_id = $request->status_id;
        $job_spec->origin_id = $request->input('origin_id');
        $job_spec->save();

        if ($request->hasFile('document')) {
            if (!$quota->isStorageQuotaReached($request->file('document')->getSize())){
                $old_Document = Document::select('file')->where('reference_id', $job_spec->id)->latest()->first();

                $name = $request->document->getClientOriginalName();
                $file_name = Carbon::now()->format('Y-m-d').'-'.strtotime(Carbon::now()).'.'.$request->document->getClientOriginalExtension();
                $stored = $request->document->storeAs('job_spec', $file_name);

                $document = new Document();
                $document->type_id = null;
                $document->document_type_id = 8; //Job Spec documents
                $document->name = $name;
                $document->file = $file_name;
                $document->creator_id = auth()->id();
                $document->owner_id = auth()->id();
                $document->digisign_status_id = null;
                $document->reference_id = $job_spec->id;
                $document->digisign_approver_user_id = null;
                $document->save();
            }

        }

        activity()->on($job_spec)->withProperties(['by' => Auth()->user()->email, 'reference' => $job_spec->reference_number])->log('updated');

        $job_spec_activity = new JobSpecActivityLog();
        $job_spec_activity->date = now();
        $job_spec_activity->activity = 'Job Spec Is Updated';
        $job_spec_activity->job_spec_id = $job_spec->id;
        $job_spec_activity->author_id = auth()->user()->id;
        $job_spec_activity->status_id = 1;
        $job_spec_activity->save();

        return redirect(route('jobspec.index'))->with('flash_success', 'Job Spec successfully updated');
    }

    /*
     * Update a job spec wishlist
     * */
    public function updateWishList($id, Request $request): RedirectResponse
    {
        $job_spec = JobSpec::find($id);
        $job_spec->cv_wishlist_id = $request->input('cv_wishlist_id');
        $job_spec->save();

        return redirect(route('jobspec.show', $id))->with('flash_success', 'Job spec wishlist successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id, JobSpec $jobSpec): RedirectResponse
    {
        $job_spec = JobSpec::find($id);

        activity()->on($job_spec)->withProperties(['by' => Auth()->user()->email, 'reference' => $job_spec->reference_number])->log('deleted');

        $jobSpec::destroy($id);

        return redirect(route('jobspec.index'))->with('flash_success', 'Job Spec successfully deleted.');
    }
}
