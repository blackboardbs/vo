<?php

namespace App\Http\Controllers;

use App\Models\Epic;
use App\Models\Feature;
use App\Http\Requests\EpicRequest;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\Sprint;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\Validator;

class EpicController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $epics = Epic::orderBy('id');
        if ($request->has('p_id') && $request->input('p_id') != 0) {
            $epics = $epics->where('project_id', '=', $request->input('p_id'))->get();
        }

        $epics = $epics->get();

        $parameters = [
            'epics' => $epics,
        ];

        return view('project.epic.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $project = null;
        if ($request->has('project_id') && ($request->input('project_id') > 0)) {
            $project_id = $request->input('project_id');
            $project = Project::find($project_id);
        }

        $parameters = [
            'project' => $project,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.epic.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EpicRequest $request): RedirectResponse
    {
        $epic = new Epic();
        $epic->name = $request->input('name');
        $epic->project_id = $request->input('project_id');
        $epic->start_date = $request->input('start_date');
        $epic->end_date = $request->input('end_date');
        $epic->hours_planned = $request->input('hours_planned');
        $epic->confidence_percentage = $request->input('confidence_percentage');
        $epic->estimate_cost = $request->input('estimate_cost');
        $epic->estimate_income = $request->input('estimate_income');
        $epic->billable = $request->input('billable');
        $epic->sprint_id = $request->input('sprint_id');
        $epic->note = $request->input('note');
        $epic->status_id = $request->input('status_id');
        $epic->save();

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Epic successfully added.');
        }

        return redirect(route('epic.index'))->with('flash_success', 'Epic successfully added.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Epic $epic): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id');
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id');

        $parameters = [
            'epic' => $epic,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.epic.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Epic $epic): View
    {
        $projectDropDown = Project::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id')->prepend('Please select...', 0);
        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $project = Project::find($epic->project_id);

        $parameters = [
            'epic' => $epic,
            'project' => $project,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return view('project.epic.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EpicRequest $request, Epic $epic): RedirectResponse
    {
        if ($request->input('status_id') == 4) {
            $features = Feature::where('status_id', '!=', 4)
                ->where('epic_id', $epic->id)
                ->get();

            if ($features->isNotEmpty()) {
                return redirect()->back()->with('flash_info', 'There is still open features that needs to be completed');
            }
        }

        $epic = Epic::find($epic->id);
        $epic->name = $request->input('name');
        $epic->project_id = $request->input('project_id');
        $epic->start_date = $request->input('start_date');
        $epic->end_date = $request->input('end_date');
        $epic->hours_planned = $request->input('hours_planned');
        $epic->confidence_percentage = $request->input('confidence_percentage');
        $epic->estimate_cost = $request->input('estimate_cost');
        $epic->estimate_income = $request->input('estimate_income');
        $epic->billable = $request->input('billable');
        $epic->sprint_id = $request->input('sprint_id');
        $epic->note = $request->input('note');
        $epic->status_id = $request->input('status_id');
        $epic->save();

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Epic updated successfully.');
        }

        return redirect(route('epic.index'))->with('flash_success', 'Epic successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Epic $epic, Request $request): RedirectResponse
    {
        Epic::destroy($epic->id);

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Epic successfully deleted.');
        }

        return redirect(route('epic.index'))->with('flash_success', 'Epic successfully deleted.');
    }

    public function getDropdowns(Request $request){
        $projectDropDown = Project::orderBy('name')->get(['name', 'id']);
        $statusDropDown = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->get(['description', 'id']);
        $sprintDropDown = Sprint::orderBy('name')->get(['name', 'id']);

        $project = null;
        if ($request->has('project_id') && ($request->input('project_id') > 0)) {
            $project_id = $request->input('project_id');
            $project = Project::find($project_id);
        }

        $parameters = [
            'project' => $project,
            'projectDropDown' => $projectDropDown,
            'statusDropDown' => $statusDropDown,
            'sprintDropDown' => $sprintDropDown,
        ];

        return response()->json($parameters);
    }

    private function epicValidation(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'name' => 'string|required',
            'project_id' => 'integer|required|not_in:0',
            'status_id' => 'integer|required|not_in:0',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'billable' => 'numeric|nullable',
            'hours_planned' => 'numeric|nullable',
            'estimate_cost' => 'numeric|nullable',
            'estimate_income' => 'numeric|nullable',
            'confidence_percentage' => 'numeric|nullable',
        ]);
    }

    public function saveEpic(Request $request){
        $validator = $this->epicValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        
        $epic = new Epic();
        $epic->name = $request->name??null;
        $epic->project_id = $request->project_id??null;
        $epic->start_date = $request->start_date??null;
        $epic->end_date = $request->end_date??null;
        $epic->hours_planned = $request->hours_planned??null;
        $epic->confidence_percentage = $request->confidence_percentage??null;
        $epic->estimate_cost = $request->estimate_cost??null;
        $epic->estimate_income = $request->estimate_income??null;
        $epic->billable = $request->billable??0;
        $epic->sprint_id = $request->sprint_id??null;
        $epic->note = $request->note??'';
        $epic->status_id = $request->status_id??null;
        $epic->save();
        
        $parameters = [
            'epic' => Epic::with('features','features.user_stories','features.user_stories.tasks','features.user_stories.tasks.timelines','features.user_stories.tasks.consultant')->where('id',$epic->id)->first()
        ];

        return response()->json($parameters);
    }

    public function updateEpic(Request $request){
        $validator = $this->epicValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        
        $epic = Epic::find($request->id);
        $epic->name = $request->name??null;
        $epic->project_id = $request->project_id??null;
        $epic->start_date = $request->start_date??null;
        $epic->end_date = $request->end_date??null;
        $epic->hours_planned = $request->hours_planned??null;
        $epic->confidence_percentage = $request->confidence_percentage??null;
        $epic->estimate_cost = $request->estimate_cost??null;
        $epic->estimate_income = $request->estimate_income??null;
        $epic->billable = $request->billable??0;
        $epic->sprint_id = $request->sprint_id??null;
        $epic->note = $request->note??'';
        $epic->status_id = $request->status_id??null;
        $epic->save();
        
        $parameters = [
            'epic' => Epic::with('features','features.user_stories','features.user_stories.tasks','features.user_stories.tasks.timelines','features.user_stories.tasks.consultant')->where('id',$epic->id)->first()
        ];

        return response()->json($parameters);
    }

    public function deleteEpic(Request $request){
        Epic::destroy($request->epic_id);
        
        return response()->json(['message'=>'Epic successfully deleted.']);
    }

    public function moveEpic(Request $request, $id)
    {
        $epic = Epic::findOrFail($id);
        $epic->project_id = $request->project_id;
        $epic->save();

        return response()->json([
            'message' => 'Epic updated successfully!',
            'epic' => $epic
        ]);
    }

}
