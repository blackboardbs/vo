<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Jobs\MasterServiceAgreementJob;
use App\Jobs\NonDisclosureAgreementJob;
use App\Models\AdvancedTemplates;
use App\Models\BankAccountType;
use App\Models\BBBEELevel;
use App\Models\Company;
use App\Models\Config;
use App\Models\Country;
use App\Http\Requests\StoreVendorRequest;
use App\Http\Requests\UpdateVendorRequest;
use App\Models\PaymentTerm;
use App\Models\Vendor;
use App\Services\VendorService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;
use Spatie\LaravelPdf\PdfBuilder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class VendorController extends Controller
{

    public function index(Request $request, VendorService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasViewPermission(), 403);

        $vendor = Vendor::with(['status:id,description'])->withCount(['users', 'invoices'])
            ->when($service->hasViewTeamPermission(), fn($vendor) => $vendor->where('id', '=', Auth::user()->vendor_id))
            ->when($request->q, fn($vendor) => $vendor
                ->where(fn($query) => $query->where('vendor_name', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('vat_no', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('business_reg_no', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('contact_firstname', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('contact_lastname', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('account_manager', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('assignment_approver', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('cell', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('email', 'LIKE', '%'.$request->input('q').'%')
                    ->orWhere('phone', 'LIKE', '%'.$request->input('q').'%')))
            ->paginate($request->input('r') ?? 15);


        $parameters = [
            'vendor' => $vendor,
            'can_create' => $service->hasCreatePermissions(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        if ($request->has('export')) return $this->export($parameters, 'vendor');

        return View('vendor.index', $parameters);
    }

    public function create(VendorService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $company_id = Config::first()?->company_id;
        $company_name = Company::find($company_id??1)?->company_name;

        $parameters = [
            'countries_dropdown' => Country::orderBy('name')->pluck('name', 'id')->prepend('Country', '0'),
            'bbbee_level_dropdown' => BBBEELevel::orderBy('description')->pluck('description', 'id')->prepend('BBBEE Level', '0'),
            'payment_terms_days_dropdown' => PaymentTerm::select('description')->pluck('description', 'description')->prepend('Select Payment Terms', '0'),
            'default_company_name' => $company_name,
            'bank_account_type_drop_down' => BankAccountType::orderBy('description')->pluck('description', 'id'),
            'appointment_manager_dropdown' => $service->managers(),
            'templates_dropdown' => AdvancedTemplates::where('template_type_id', 6)->pluck('name', 'id')->prepend('Select Template', '0'),
            'nda_templates_dropdown' => AdvancedTemplates::where('template_type_id', 8)->pluck('name', 'id')->prepend('Select NDA Template', '0'),
            'default_template' => Config::first(['client_service_agreement_id'])?->client_service_agreement_id
        ];

        return view('vendor.create')->with($parameters);
    }

    public function store(StoreVendorRequest $request, VendorService $service, Vendor $vendor): RedirectResponse
    {
        $vendor = $service->createVendor($request, $vendor);

        $service->assignVendorToUser($vendor->assignment_approver, $vendor->id);

        return redirect(route('vendors.show', $vendor))->with('flash_success', 'Vendor was created successfully');
    }

    public function show(Vendor $vendor, VendorService $service): View
    {
        abort_unless($service->hasShowPermissions(), 403);

        $parameters = [
            'vendor' => $vendor,
        ];

        return view('vendor.show')->with($parameters);
    }

    public function edit(Vendor $vendor, VendorService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $parameters = [
            'vendor' => $vendor,
            'countries_dropdown' => Country::orderBy('name')->pluck('name', 'id')->prepend('Country', '0'),
            'bbbee_level_dropdown' => BBBEELevel::orderBy('description')->pluck('description', 'id')->prepend('BBBEE Level', '0'),
            'payment_terms_days_dropdown' => PaymentTerm::select('description')->pluck('description', 'description')->prepend('Select Payment Terms', '0'),
            'bank_account_type_drop_down' => BankAccountType::orderBy('description')->pluck('description', 'id'),
            'appointment_manager_dropdown' => $service->managers(),
            'templates_dropdown' => AdvancedTemplates::where('template_type_id', 6)->pluck('name', 'id')->prepend('Select Template', '0'),
            'nda_templates_dropdown' => AdvancedTemplates::where('template_type_id', 8)->pluck('name', 'id')->prepend('Select NDA Template', '0'),
        ];

        return view('vendor.edit')->with($parameters);
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor, VendorService $service): RedirectResponse
    {
        $service->updateVendor($request, $vendor);

        return redirect(route('vendors.index'))->with('flash_success', "Vendor was Successfully updated");
    }

    public function destroy(Vendor $vendor, VendorService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $vendor->delete();

        return redirect()->route('vendors.index')->with('success', 'Vendor deleted successfully');
    }

    public function print(Vendor $vendor): PdfBuilder|RedirectResponse
    {
        try {

            $template = $vendor->mapVariables($vendor->serviceAgreement);
            return $this->masterPrint($vendor->vendor_name, $template);

        }catch (\TypeError $e) {
            logger($e);
            return redirect(route('vendors.edit', $vendor))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    public function printNDA(Vendor $vendor): PdfBuilder|RedirectResponse
    {
        try {

            $template = $vendor->mapVariables($vendor->NDATemplate);
            return $this->masterPrint($vendor->vendor_name, $template);

        }catch (\TypeError $e) {
            logger($e);
            return redirect(route('vendors.edit', $vendor))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    public function send(Vendor $vendor)
    {
        $template = $vendor->mapVariables($vendor->serviceAgreement);

        try {
            $storage = $this->masterSend($vendor, 'client/templates', $template);

            MasterServiceAgreementJob::dispatch($storage, $vendor->email);

            return redirect(route('vendors.show', $vendor))->with('flash_success', 'Master Service Agreement was sent successfully');

        }catch (\TypeError $e){
            logger($e);
            return redirect(route('vendors.edit', $vendor))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    public function sendNDA(Vendor $vendor)
    {
        $template = $vendor->mapVariables($vendor->NDATemplate);

        try {
            $storage = $this->masterSend($vendor, 'vendor/templates', $template);

            $emails = [$vendor->email, auth()->user()->email];

            $data = [
                'emails' => array_values(array_filter(array_unique($emails))),
                'subject' => "NDA for {$vendor->vendor_name}",
                'body' => "Please find the non disclosure agreement attached.",
                'attachments' => $storage
            ];

            NonDisclosureAgreementJob::dispatch($data);

            return redirect(route('vendors.show', $vendor))->with('flash_success', 'Master Service Agreement was sent successfully');

        }catch (\TypeError $e){
            logger($e);
            return redirect(route('vendors.edit', $vendor))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    private function masterPrint(string $vendorName, string $template): PdfBuilder
    {
        return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name($vendorName . ".pdf");
    }

    private function masterSend(Vendor $vendor, string $path, $template): string
    {
        if (! Storage::exists($path)) {
            Storage::makeDirectory($path);
        }

        $filename = "master_service_agreement_client_".str_replace(' ', '_',$vendor->vendor_name)."_".date('Y_m_d_H_i_s').'.pdf';

        $storage = storage_path("app/{$path}/{$filename}");

        Pdf::view('template.default-template', ['template' => $template])
            ->headerView('template.header')
            ->footerView('template.footer')
            ->format('a4')
            ->margins(30, 20, 30, 20, Unit::Pixel)
            ->save($storage);

        $vendor->documents()->create([
            'name' => "Master Service Agreement - Client {$vendor->vendor_name}",
            'file' => $filename,
            'document_type_id' => 4,
            'creator_id' => auth()->id(),
            'status_id' => Status::ACTIVE->value,
        ]);

        return $storage;
    }
}
