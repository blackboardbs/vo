<?php

namespace App\Http\Controllers;

use App\API\Clockify\ClockifyAPI;
use App\Models\User;
use Illuminate\Http\Request;

class ClockifyController extends Controller
{
    public function handleEntry(Request $request, ClockifyAPI $api)
    {
        $email = $this->userEmail($api, $request->userId);
        if (isset($email)){
            $wo_user = User::select('id')->where('email', $email)->first();
            logger($wo_user);
        }
    }

    private function userEmail(ClockifyAPI $api, string $user_id)
    {
        $users = $api->getEndPoint(config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').'/users');
        $user_email = null;

        if ($users->getStatusCode() == ClockifyAPI::STATUS_OK){
            $user_email = collect(json_decode($users->getBody()->getContents(), true))
                ->map(function ($user) use ($user_id){
                    if ($user["id"] === $user_id){
                        return $user["email"];
                    }
                    return null;
                })->filter()->values();

        }

        return $user_email[0]??null;
    }

    /*private function getWeek(string $date): string
    {

    }*/
}
