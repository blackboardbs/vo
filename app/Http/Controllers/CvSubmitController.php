<?php

namespace App\Http\Controllers;

use App\Models\AvailStatus;
use App\Models\Cv;
use App\Models\CvSubmit;
use App\Models\CvTemplate;
use App\Models\Document;
use App\Models\JobSpec;
use App\Models\JobSpecActivityLog;
use App\Mail\CVMail;
use App\Mail\CVSubmitMail;
use App\Models\SkillLevel;
use App\Models\System;
use App\Models\Wishlist;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;
use PDF;

class CvSubmitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $cv_submit = new CvSubmit();
        $cv_submit->cv_id = $request->input('cv_id');
        $cv_submit->sent_to_email = $request->input('job_spec_email');
        $cv_submit->job_spec_id = $request->input('job_spec_id');
        $cv_submit->rate = $request->input('customer_quote_hourly_rate');
        $cv_submit->ctc = $request->input('customer_quote_expected_salary');
        $cv_submit->template = $request->input('template');
        $cv_submit->format = $request->input('attachment');
        $cv_submit->text_message = $request->input('text_message');
        $cv_submit->status_id = 1;
        $cv_submit->save();

        $job_spec_activity = new JobSpecActivityLog();
        $job_spec_activity->date = now();
        $job_spec_activity->activity = 'CV Is Submitted';
        $job_spec_activity->job_spec_id = $request->input('job_spec_id');
        $job_spec_activity->author_id = auth()->user()->id;
        $job_spec_activity->status_id = 1;
        $job_spec_activity->save();

        return redirect(route('cvsubmit.show', $cv_submit->id));
    }

    public function submit(Request $request): JsonResponse
    {
        $cv_submit = new CvSubmit();
        $cv_submit->cv_id = $request->input('cv_id');
        $cv_submit->subject = $request->input('subject');
        $cv_submit->sent_to_email = $request->input('job_spec_email');
        $cv_submit->sent_to_email_cc = $request->input('job_spec_email_cc');
        $cv_submit->sent_to_email_bcc = $request->input('job_spec_email_bcc');
        $cv_submit->job_spec_id = $request->input('job_spec_id');
        $cv_submit->rate = $request->input('rate');
        $cv_submit->ctc = $request->input('ctc');
        $cv_submit->commission = $request->input('commission');
        $cv_submit->template = $request->input('template');
        $cv_submit->format = $request->input('attachment');
        $cv_submit->text_message = $request->input('text_message');
        $cv_submit->status_id = 1;
        $cv_submit->save();

        $cv = CV::with(['user', 'qualification', 'skill', 'experience'])->where('id', '=', $cv_submit->cv_id)->first();
        $skill_level = SkillLevel::orderBy('id')->pluck('description', 'id')->toArray();
        $tool = System::orderBy('id')->pluck('description', 'id')->toArray();

        $preferred_name = $cv->resource_pref_name;
        $last_name = isset($cv->user->last_name) ? $cv->user->last_name : '';
        $main_skill = $cv->main_skill;
        $main_qualification = $cv->main_qualification;
        $email_cc = isset($cv_submit->sent_to_email_cc) ? explode(',', str_replace(' ', '', $cv_submit->sent_to_email_cc)) : [];
        $email_bcc = isset($cv_submit->sent_to_email_bcc) ? explode(',', str_replace(' ', '', $cv_submit->sent_to_email_bcc)) : [];

        $parameters = [
            'preferred_name' => $preferred_name,
            'last_name' => $last_name,
            'main_skill' => $main_skill,
            'main_qualification' => $main_qualification,
            'cv' => $cv,
            'tool' => $tool,
            'skill_level' => $skill_level,
        ];

        try {

            $mail = Mail::to(trim($cv_submit->sent_to_email));
            if (count($email_cc) && count($email_bcc)) {
                $mail->cc($email_cc);
                foreach ($email_bcc as $email) {
                    $mail->bcc($email);
                }
            }
            if (count($email_cc) && ! count($email_bcc)) {
                $mail->cc($email_cc);
            }
            if (! count($email_cc) && count($email_bcc)) {
                foreach ($email_bcc as $email) {
                    $mail->bcc($email);
                }
            }

            $file_name_pdf = $request->input('attachment_cv');
            //$mail->send(new CVSubmitMail($preferred_name.' '.$last_name, $cv_submit->subject, $cv_submit->text_message, $file_name_pdf));
            //$mail->send(new CVSubmitMail($preferred_name.' '.$last_name, $cv_submit->subject, $cv_submit->text_message, $file_name_pdf));

            $generate_system_cv = $request->input('generate_system_cv');
            $attachments = '';
            if ($request->input('attachments') != '') {
                $attachments = $request->input('attachments');
            }

            //return response()->json(['message' => $attachments]);

            $mail->send(new CVMail($preferred_name.' '.$last_name, $cv_submit->subject, $cv_submit->text_message, $file_name_pdf??'', $generate_system_cv, $attachments));

            $cv_track = array_merge($cv->toArray(), ['generated_cv' => $file_name_pdf, 'attachments' => $attachments, 'request' => $request->all()]);
            activity()->on($cv)
            ->withProperties(
                $cv_track
            )
            ->log('CV Submitted');
        } catch (\Exception $e) {
            report($e);

            return response()->json(['message' => 'Error sending CV '.$e]);
        }

        if ($request->has('w')) {
            if (isset($request->w)) {
                $wishlist = Wishlist::find($request->w);
                $wishlist->sent_at = now()->toDateTimeString();
                $wishlist->save();
            }
        }

        $job_spec_activity = new JobSpecActivityLog();
        $job_spec_activity->date = now();
        $job_spec_activity->activity = 'CV Is Submitted';
        $job_spec_activity->job_spec_id = $request->input('job_spec_id');
        $job_spec_activity->cv_id = $request->input('cv_id');
        $job_spec_activity->author_id = auth()->user()->id;
        $job_spec_activity->status_id = 1;
        $job_spec_activity->save();

        return response()->json(['message' => 'CV successfully sent.']);
    }

    public function getCustomerEmail($job_spec_id): JsonResponse
    {
        $jobSpec = JobSpec::find($job_spec_id);

        return response()->json([
            'customer_email' => (isset($jobSpec->customer->email) ? $jobSpec->customer->email : ''),
            'commission' => (isset($jobSpec->customer->commission) ? $jobSpec->customer->commission : ''),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($Cv_id, Request $request): View
    {
        $cv = CV::find($Cv_id);

        $latest_date = null;
        $latest_availability = null;
        foreach ($cv->availability as $availability) {
            if ($latest_date == null) {
                $latest_date = $availability->avail_date;
                $latest_availability = $availability;
            } else {
                if (date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))) {
                    $latest_date = $availability->avail_date;
                    $latest_availability = $availability;
                }
            }
        }

        $availability_status = '';
        if (isset($latest_availability->avail_status)) {
            $availability_status = AvailStatus::find($latest_availability->avail_status);
        }

        $job_spec_drop_down = JobSpec::orderBy('id')->pluck('reference_number', 'id')->prepend('No job selected', -1);

        $cv_type = 2;
        if ($request->has('cv_type')) {
            $cv_type = $request->input('cv_type');
        }

        $cvTemplatesDropDown = CvTemplate::orderBy('id')->where('status_id', '=', 1)->pluck('name', 'id');
        $documents = Document::where('document_type_id', '=', 3)->where('reference_id', '=', $cv->id)->get();

        $parameters = [
            'id' => $Cv_id,
            'cv' => $cv,
            'documents' => $documents,
            'cv_type' => $cv_type,
            'estimated_hourly_rate' => '',
            'latest_availability' => $latest_availability,
            'availability_status' => $availability_status,
            'job_spec_drop_down' => $job_spec_drop_down,
            'cvTemplatesDropDown' => $cvTemplatesDropDown,
        ];

        return view('cv.submit.show')->with($parameters);
    }

    public function jobSpecActivityLogStatus(JobSpecActivityLog $jobSpecActivityLog, $status_id): RedirectResponse
    {
        switch ($status_id) {
            case 2:
                $activity = 'CV has been resubmitted';
                break;
            case 3:
                $activity = 'CV has been rejected';
                break;
            case 4:
                $activity = 'Candidate has been interviewed';
                break;
            case 5:
                $activity = 'Candidate has been made an offer';
                break;
            default:
                $activity = 'Candidate has been appointed';
        }

        $activity_log = new JobSpecActivityLog();
        $activity_log->date = now();
        $activity_log->activity = $activity;
        $activity_log->job_spec_id = $jobSpecActivityLog->job_spec_id;
        $activity_log->cv_id = $jobSpecActivityLog->cv_id;
        $activity_log->author_id = $jobSpecActivityLog->author_id;
        $activity_log->status_id = $status_id;
        $activity_log->save();

        return redirect()->back()->with('flash_success', 'Status has been updated');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
