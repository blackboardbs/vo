<?php

namespace App\Http\Controllers;

use App\Models\AccountType;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AccountTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $account_type = AccountType::sortable('description', 'asc')->paginate($item);

        $account_type = AccountType::with('statusd:id,description')->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $account_type = AccountType::with('statusd:id,description')->where('status', $request->input('status_filter'));
            }else{
                $account_type = AccountType::with('statusd:id,description')->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $account_type = $account_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $account_type = $account_type->paginate($item);

        $parameters = [
            'account_type' => $account_type,
        ];

        return View('master_data.account_type.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = AccountType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.account_type.create')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        $account_type = new AccountType();
        $row_order = AccountType::all()->last();
        $account_type->description = $request->input('description');
        $account_type->status = $request->input('status');
        $account_type->order = (empty($row_order)) ? 1 : ++$row_order->order;
        $account_type->creator_id = auth()->id();
        $account_type->save();

        return redirect(route('account_type.index'))->with('flash_success', 'Master Data Account Type captured successfully');
    }

    public function show($account_type_id): View
    {
        $parameters = [
            'account_type' => AccountType::where('id', '=', $account_type_id)->get(),
        ];

        return View('master_data.account_type.show')->with($parameters);
    }

    public function edit($account_type_id): View
    {
        $autocomplete_elements = AccountType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'account_type' => AccountType::where('id', '=', $account_type_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.account_type.edit')->with($parameters);
    }

    public function update(Request $request, $account_type_id): RedirectResponse
    {
        $account_type = AccountType::find($account_type_id);
        $account_type->description = $request->input('description');
        $account_type->status = $request->input('status');
        $account_type->creator_id = auth()->id();
        $account_type->save();

        return redirect(route('account_type.index'))->with('flash_success', 'Master Date Account Type saved successfully');
    }

    public function destroy($account_type_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // AccountType::destroy($account_type_id);
        $item = AccountType::find($account_type_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('account_type.index')->with('success', 'Master Data Account Type suspended successfully');
    }
}
