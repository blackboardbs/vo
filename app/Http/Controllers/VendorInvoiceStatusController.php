<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\VendorInvoiceStatus;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VendorInvoiceStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $vendor_invoice_status = VendorInvoiceStatus::sortable('description','asc')->paginate($item);

        $vendor_invoice_status = VendorInvoiceStatus::with(['statusd:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $vendor_invoice_status = VendorInvoiceStatus::with(['statusd:id,description'])->where('status_id', $request->input('status_filter'));
            }else{
                $vendor_invoice_status = VendorInvoiceStatus::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $vendor_invoice_status = $vendor_invoice_status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $vendor_invoice_status = $vendor_invoice_status->paginate($item);

        $parameters = [
            'vendor_invoice_status' => $vendor_invoice_status,
        ];

        return View('master_data.vendor_invoice_status.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status','=',1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.vendor_invoice_status.create')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        $vendor_invoice_status = new VendorInvoiceStatus();
        $vendor_invoice_status->description = $request->input('description');
        $vendor_invoice_status->status_id = $request->input('status');
        $vendor_invoice_status->creator_id = auth()->id();
        $vendor_invoice_status->save();

        return redirect(route('vendor_invoice_status.index'))->with('flash_success', 'Master Data Vendor Invoice Status captured successfully');
    }

    public function show($id): View
    {
        $vendor_invoice_status = VendorInvoiceStatus::find($id);

        $parameters = [
            'vendor_invoice_status' => $vendor_invoice_status,
        ];
        return view('master_data.vendor_invoice_status.show')->with($parameters);
    }

    public function edit($id): View
    {
        $vendor_invoice_status = VendorInvoiceStatus::find($id);

        $parameters = [
            'vendor_invoice_status' => $vendor_invoice_status,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status','=',1)->pluck('description', 'id')->prepend('Status', '0'),
        ];
        return view('master_data.vendor_invoice_status.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $vendor_invoice_status = VendorInvoiceStatus::find($id);
        $vendor_invoice_status->description = $request->input('description');
        $vendor_invoice_status->status_id = $request->input('status');
        $vendor_invoice_status->creator_id = auth()->id();
        $vendor_invoice_status->save();

        return redirect(route('vendor_invoice_status.index'))->with('flash_success', 'Master Data Vendor Invoice Status updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // VendorInvoiceStatus::destroy($id);
        $item = VendorInvoiceStatus::find($id);
        $item->status_id = 2;
        $item->save();
        return redirect(route('vendor_invoice_status.index'))->with('flash_success', 'Master Data Vendor Invoice Status deleted successfully');
    }
}
