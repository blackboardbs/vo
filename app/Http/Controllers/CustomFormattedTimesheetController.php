<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\CFTS;
use App\Models\CFTSTemplate;
use App\Models\CFTSTimesheet;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Module;
use App\Models\Project;
use App\Models\Status;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;
use TCPDF;

class CustomFormattedTimesheetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = 15;
        $cfts = CFTS::with('customer', 'status', 'template')->sortable('additional_text');

        $module = Module::where('name', '=', 'App\Models\CustomFormattedTimesheet')->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $cfts = $cfts->whereIn('creator_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        if ($request->has('customer') && $request->input('customer') != 0) {
            $cfts = $cfts->where('customer_id', '=', $request->input('customer'));
        }

        if ($request->has('status') && $request->input('status') != 0) {
            $cfts = $cfts->where('status_id', '=', $request->input('status'));
        }

        $cfts = $cfts->paginate($item);

        $cfts_collections = [];
        $counter = 0;
        foreach ($cfts as $_cfts) {
            $cftsTemplate = CFTSTimesheet::where('cfts_id', '=', $_cfts->id)->first();
            $timesheet = Timesheet::find($cftsTemplate->timesheet_id);
            $project = Project::find($timesheet->project_id);
            $resource = User::find($timesheet->employee_id);

            $cfts_collections[$counter]['id'] = $_cfts->id;
            $cfts_collections[$counter]['customer_name'] = isset($_cfts->customer->customer_name) ? $_cfts->customer->customer_name : '';
            $cfts_collections[$counter]['reference'] = $project->name.' - '.$timesheet->first_day_of_week.' - '.$resource->first_name.' '.$resource->last_name;
            $cfts_collections[$counter]['template'] = isset($_cfts->template->name) ? $_cfts->template->name : '';
            $cfts_collections[$counter]['digisign'] = ($_cfts->digisign == 2) ? 'Digisign' : 'Exteranl';
            $cfts_collections[$counter]['created_at'] = $_cfts->created_at;
            $cfts_collections[$counter]['status'] = isset($_cfts->status->description) ? $_cfts->status->description : '';
            $counter++;
        }

        $customer_drop_down = Customer::orderBy('customer_name')->where('status', '=', 1)->pluck('customer_name', 'id')->prepend('All', 0);
        $status_drop_down = Status::orderBy('description')->where('status', '=', 1)->pluck('description', 'id')->prepend('All', 0);

        $parameters = [
            'cfts' => $cfts,
            'cfts_collections' => $cfts_collections,
            'customer_drop_down' => $customer_drop_down,
            'status_drop_down' => $status_drop_down,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        return view('cfts.index')->with($parameters);
    }

    public function create(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $cfts_timesheets = CFTSTimesheet::orderBy('id')->pluck('timesheet_id');

        /*$timesheets = Timesheet::select(DB::raw("timesheet.id, timesheet.employee_id, timesheet.customer_id, timesheet.year_week, timesheet.project_id,timesheet.status_id,
                                                SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total ELSE 0 END) AS billable_hours,
                                                SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total ELSE 0 END) AS non_billable_hours,
                                                SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total_m ELSE 0 END) AS billable_minutes,
                                                SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total_m ELSE 0 END) AS non_billable_minutes
                                                "))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereNotIn('timesheet.id', $cfts_timesheets)
            ->groupBy('timesheet.id')
            ->groupBy('timesheet.employee_id')
            ->groupBy('timesheet.customer_id')
            ->groupBy('timesheet.year_week')
            ->groupBy('timesheet.project_id')
            ->groupBy('timesheet.status_id')
            ->orderBy('timesheet.id', 'desc');*/

        $timesheets = Timesheet::select(DB::raw('timesheet.id, timesheet.customer_id, timesheet.employee_id, timesheet.first_day_of_week, timesheet.project_id, timesheet.bill_status, timesheet.year_week, timesheet.timesheet_lock, timesheet.vendor_invoice_status,
            SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS billable_hours,
            SUM(CASE WHEN timeline.is_billable = 0 THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS non_billable_hours,
            SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total * 60 + timeline.total_m) ELSE 0 END) AS billable_minutes,
            SUM(CASE WHEN timeline.is_billable = 0 THEN (timeline.total * 60 + timeline.total_m) ELSE 0 END) AS non_billable_minutes,
            sum(timeline.total_m) as total_minutes,
            sum(timeline.total) as total_hours,
            timesheet.exp_paid, timesheet.inv_ref, timesheet.cust_inv_ref,timesheet.status_id'))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->whereNotIn('timesheet.id', $cfts_timesheets)
            ->groupBy('timesheet.id')
            ->groupBy('timesheet.employee_id')
            ->groupBy('timesheet.customer_id')
            ->groupBy('timesheet.project_id')
            ->groupBy('timesheet.bill_status')
            ->groupBy('timesheet.exp_paid')
            ->groupBy('timesheet.inv_ref')
            ->groupBy('timesheet.cust_inv_ref')
            ->groupBy('timesheet.year_week')
            ->groupBy('timesheet.status_id')
            ->groupBy('timesheet.timesheet_lock')
            ->groupBy('timesheet.vendor_invoice_status')
            ->groupBy('timesheet.first_day_of_week');

        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $config = Config::find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $week_counter = 0;
        $week_number = $week < 10 ? '0'.$week : $week;
        $selected_week = $year.$week_number;
        $five_weeks = $year.($week_number + 5);
        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $date = $date->startOfWeek();

                $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                if ($week_counter == 5) {
                    $selected_week = $year.($i < 10 ? '0'.$i : $i);
                    $five_weeks = $year.(($i + 5) < 10 ? '0'.($i + 5) : $i);
                }
                $week_counter++;
            }

            $number_of_weeks -= $week;
            $week = 52;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $date = $date->startOfWeek();
            $week_drop_down[$year.$i] = $year.$i.'('.$date2->format('Y-m-d').')';
            if ($week_counter == 5) {
                $selected_week = $year.($i < 10 ? '0'.$i : $i);
                $five_weeks = $year.(($i + 5) < 10 ? '0'.($i + 5) : $i);
            }
            $week_counter++;
        }

        $module = Module::where('name', '=', 'App\Models\CustomFormattedTimesheet')->first(); //TODO: Check permission for this

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $timesheets->whereIn('timesheet.employee_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }
        $all_timesheets = $timesheets->get();

        $billable_minutes_and_hours = [];
        $non_billable_minutes_and_hours = [];
        $year_weeks = [];
        $project = [];
        $employees = [];
        $customer = [];
        $total_billable_hours = 0;
        $total_non_billable_hours = 0;
        $time_exp = [];

        //$counter = 0;
        $customer_ = Customer::where('status', '=', 1)->orderBy('customer_name')->first();
        $first_customer = $customer_->id;
        $selected_customer_id = $request->has('customer') ? $request->input('customer') : $first_customer;
        foreach ($all_timesheets as $timesheet) {
            array_push($billable_minutes_and_hours, round(($timesheet->billable_minutes / 60) + $timesheet->billable_hours));
            array_push($non_billable_minutes_and_hours, round(($timesheet->non_billable_minutes / 60) + $timesheet->non_billable_hours));
            $year_weeks[$timesheet->year_week] = $timesheet->year_week;
            if (isset($timesheet->project)) {
                $project[$timesheet->project_id] = $timesheet->project->name;
            }
            $employees[$timesheet->employee_id] = $timesheet->employee->first_name.' '.$timesheet->employee->last_name;
            $customer[$timesheet->customer_id] = ($timesheet->customer->customer_name) ? $timesheet->customer->customer_name : '';
            $total_billable_hours += round(($timesheet->billable_minutes / 60) + $timesheet->billable_hours);
            $total_non_billable_hours += round(($timesheet->non_billable_minutes / 60) + $timesheet->non_billable_hours);
            array_push($time_exp, TimeExp::select('id', 'claim_auth_date', 'description')->where('timesheet_id', '=', $timesheet->id)->get());
            /*if($counter == 1){
                $selected_customer_id = $timesheet->customer_id;
            }
            $counter++;*/
        }

        if ($request->has('employees') && $request->input('employees') != '') {
            $timesheets = $timesheets->where('timesheet.employee_id', '=', $request->employees);
        }

        if ($request->has('week_from') && $request->input('week_from') != '') {
            $timesheets = $timesheets->where('timesheet.year_week', '>=', $request->input('week_from'));
        }

        $end_date = $request->input('start_date');
        if ($request->has('start_date') && $request->input('start_date') != '') {
            $timesheets = $timesheets->where('timesheet.year_week', '>=', $request->input('start_date'));
            $end_date_week = substr($end_date, -2);
            $end_date_year = substr($end_date, 0, 4);

            $balance = $end_date_week + 5;
            if ($balance > 52) {
                $extra_weeks = $balance - 52;
                $extra_week_number = ($extra_weeks >= 10) ? $extra_weeks : '0'.$extra_weeks;
                $end_date_filter = ($end_date_year + 1).$extra_week_number;
            } else {
                $extra_weeks = ($balance > 10) ? $balance : '0'.$balance;
                $end_date_filter = $end_date_year.$extra_weeks;
            }

            $timesheets = $timesheets->where('timesheet.year_week', '<=', $end_date_filter);
        } elseif (isset($selected_week)) {
            $timesheets = $timesheets->where('timesheet.year_week', '>=', $selected_week);
            $timesheets = $timesheets->where('timesheet.year_week', '<=', $five_weeks);
        }

        if ($request->has('week_to') && $request->input('week_to') != '') {
            $timesheets = $timesheets->where('timesheet.year_week', '<=', $request->input('week_to'));
        }

        if ($request->has('project') && $request->input('project') != '') {
            $timesheets = $timesheets->where('timesheet.project_id', '=', $request->input('project'));
        }

        if ($request->has('customer') && $request->input('customer') != '') {
            $timesheets = $timesheets->where('timesheet.customer_id', '=', $request->input('customer'));
        } elseif ($selected_customer_id > 0) {
            $timesheets = $timesheets->where('timesheet.customer_id', '=', $selected_customer_id);
        }

        $cfts_template = CFTSTemplate::where('status_id', '=', 1)->orderBy('id')->first();
        $selected_template = $cfts_template->id;

        $template_drop_down = CFTSTemplate::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id');
        $customer_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id');

        $selected_project = 0;
        if ($request->has('template') && $request->input('template') != '' && $request->input('template') == 1) {
            $project_ = Project::where('customer_id', '=', $selected_customer_id)->orderBy('name')->first();
            if ($request->has('project') && $request->input('project') != '') {
                $selected_project = $request->input('project');
            } else {
                $selected_project = isset($project_->id) ? $project_->id : 0;
            }
            $project_drop_down = Project::where('customer_id', '=', $selected_customer_id)->orderBy('name')->pluck('name', 'id');
        } else {
            $project_drop_down = Project::where('customer_id', '=', $selected_customer_id)->pluck('name', 'id')->prepend('All', '');
        }

        if ($selected_project != 0) {
            $timesheets = $timesheets->where('timesheet.project_id', '=', $selected_project);
        }

        $selected_resource = 0;
        if ($request->has('project') && $request->input('project') != '') {
            $project_ = Project::find($request->input('project'));
            $resource_ids = explode(',', $project_->consultants);

            $resource_ = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('status_id', '=', 1)->whereIn('id', $resource_ids)->orderBy('first_name')->orderBy('last_name')->first();
            //$selected_resource = isset($resource_->id) ? $resource_->id : 0;

            $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8, 9])->where('status_id', '=', 1)->whereIn('id', $resource_ids)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');
        } else {
            $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8, 9])->where('status_id', '=', 1)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');
        }

        $timesheets = $timesheets->paginate($item);

        $parameters = [
            'timesheets' => $timesheets,
            'billable_hours' => $billable_minutes_and_hours,
            'non_billable_hours' => $non_billable_minutes_and_hours,
            'total_billable_hours' => $total_billable_hours,
            'total_non_billable_hours' => $total_non_billable_hours,
            'year_weeks' => $year_weeks,
            'project' => $project,
            'employees' => $employees,
            'time_exp' => $time_exp,
            'week_drop_down' => $week_drop_down,
            'customer_dropdown' => $customer,
            'selected_customer_id' => $selected_customer_id,
            'selected_week' => $selected_week,
            'selected_template' => $selected_template,
            'template_drop_down' => $template_drop_down,
            'project_drop_down' => $project_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'selected_project' => $selected_project,
            'selected_resource' => $selected_resource,
        ];

        return view('cfts.create')->with($parameters);
    }

    public function prepare(Request $request): View
    {
        $timesheets = $request->input('timesheet');
        $template = $request->input('template');
        $start_date = $request->input('start_date_h');

        $year = substr($start_date, 0, 4);
        $week = substr($start_date, -2);

        $date = Carbon::now();
        $date->setISODate($year, $week);
        $start_date = $date->format('Y-m-d');

        $date = Carbon::now();
        $payment_due_date = Timesheet::find($timesheets[0]);
        $due_date = isset($payment_due_date->customer->payment_terms) ? $payment_due_date->customer->payment_terms : 30;
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please Select', 0);
        $cfts_template_drop_down = CFTSTemplate::orderBy('name')->where('status_id', '=', 1)->pluck('name', 'id')->prepend('Please Select', 0);

        $parameters = [
            'timesheets' => $timesheets,
            'template' => $template,
            'start_date' => $start_date,
            'cfts_template_drop_down' => $cfts_template_drop_down,
        ];

        return view('cfts.prepare')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        $timesheet_ids = $request->input('timesheets');

        $cfts_timesheets = Timesheet::whereIn('id', $timesheet_ids)->where('status_id', '=', 1)->get();

        $cfts = new CFTS;
        $cfts->template_id = $request->input('template_q');
        $cfts->additional_text = $request->input('additional_text');
        $cfts->start_date = $request->input('start_date_q');
        $cfts->digisign = $request->input('signature');
        $cfts->status_id = 1;
        $cfts->creator_id = auth()->id();
        $cfts->save();

        $customer_id = 0;
        foreach ($cfts_timesheets as $cfts_timesheet) {
            $customer_id = $cfts_timesheet->customer_id;
            $new_cfts_timesheet = new CFTSTimesheet;
            $new_cfts_timesheet->cfts_id = $cfts->id;
            $new_cfts_timesheet->timesheet_id = $cfts_timesheet->id;
            $new_cfts_timesheet->status_id = 1;
            $new_cfts_timesheet->creator_id = auth()->id();
            $new_cfts_timesheet->save();
        }

        $cfts_update = CFTS::find($cfts->id);
        $cfts_update->customer_id = $customer_id;
        $cfts_update->save();

        return redirect(route('cfts.show', $cfts->id))->with('flash_success', 'Custom formatted timesheet created successfully');
    }

    public function show($id, Request $request)
    {
        $cfts = CFTS::find($id);
        //dd($cfts->start_date);

        if ($cfts->template_id == 3) {
            return redirect(route('cfts.tsweeks', $id));
        }

        if ($cfts->template_id == 5) {
            return redirect(route('cfts.blackboardmonthly', $id));
        }

        if ($cfts->template_id == 6) {
            return redirect(route('cfts.matrilogixmonthly', $id));
        }

        $cfts_timesheet = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->first();
        $cfts_timesheet_ids = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->orderBy('id')->pluck('timesheet_id');
        $timesheet = Timesheet::find($cfts_timesheet->timesheet_id);
        $timesheets = Timesheet::whereIn('id', $cfts_timesheet_ids)->orderBy('year_week')->get();
        $mon_date = [];
        $timeline_mon_hour = [];
        $timeline_mon_min = [];
        $timeline_tue_hour = [];
        $timeline_tue_min = [];
        $timeline_wed_hour = [];
        $timeline_wed_min = [];
        $timeline_thu_hour = [];
        $timeline_thu_min = [];
        $timeline_fri_hour = [];
        $timeline_fri_min = [];
        $timeline_sat_hour = [];
        $timeline_sat_min = [];
        $timeline_sun_hour = [];
        $timeline_sun_min = [];
        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->orderBy('yearwk')->get();
            foreach ($timelines as $timeline) {
                //If Eim template, do not include non-billable hours
                if ($cfts->template_id == 1) {
                    if ($timeline->is_billable != 1) {
                        continue;
                    }
                }

                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('d-M-y', strtotime($date->startOfWeek()));
                //Monday
                $mon_date[$timesheet->year_week] = $start_of_week_date;
                $tue_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                if (isset($timeline_mon_hour[$timesheet->year_week])) {
                    $timeline_mon_hour[$timesheet->year_week] += $timeline->mon;
                } else {
                    $timeline_mon_hour[$timesheet->year_week] = $timeline->mon;
                }

                if (isset($timeline_mon_min[$timesheet->year_week])) {
                    $timeline_mon_min[$timesheet->year_week] += $timeline->mon_m;
                } else {
                    $timeline_mon_min[$timesheet->year_week] = $timeline->mon_m;
                }

                //Tuesday
                if (isset($timeline_tue_hour[$timesheet->year_week])) {
                    $timeline_tue_hour[$timesheet->year_week] += $timeline->tue;
                } else {
                    $timeline_tue_hour[$timesheet->year_week] = $timeline->tue;
                }

                if (isset($timeline_tue_min[$timesheet->year_week])) {
                    $timeline_tue_min[$timesheet->year_week] += $timeline->tue_m;
                } else {
                    $timeline_tue_min[$timesheet->year_week] = $timeline->tue_m;
                }

                //Wednesday
                if (isset($timeline_wed_hour[$timesheet->year_week])) {
                    $timeline_wed_hour[$timesheet->year_week] += $timeline->wed;
                } else {
                    $timeline_wed_hour[$timesheet->year_week] = $timeline->wed;
                }

                if (isset($timeline_wed_min[$timesheet->year_week])) {
                    $timeline_wed_min[$timesheet->year_week] += $timeline->wed_m;
                } else {
                    $timeline_wed_min[$timesheet->year_week] = $timeline->wed_m;
                }

                //Thursday
                if (isset($timeline_thu_hour[$timesheet->year_week])) {
                    $timeline_thu_hour[$timesheet->year_week] += $timeline->thu;
                } else {
                    $timeline_thu_hour[$timesheet->year_week] = $timeline->thu;
                }

                if (isset($timeline_thu_min[$timesheet->year_week])) {
                    $timeline_thu_min[$timesheet->year_week] += $timeline->thu_m;
                } else {
                    $timeline_thu_min[$timesheet->year_week] = $timeline->thu_m;
                }

                //Friday
                if (isset($timeline_fri_hour[$timesheet->year_week])) {
                    $timeline_fri_hour[$timesheet->year_week] += $timeline->fri;
                } else {
                    $timeline_fri_hour[$timesheet->year_week] = $timeline->fri;
                }

                if (isset($timeline_fri_min[$timesheet->year_week])) {
                    $timeline_fri_min[$timesheet->year_week] += $timeline->fri_m;
                } else {
                    $timeline_fri_min[$timesheet->year_week] = $timeline->fri_m;
                }

                //Saturday
                if (isset($timeline_sat_hour[$timesheet->year_week])) {
                    $timeline_sat_hour[$timesheet->year_week] += $timeline->sat;
                } else {
                    $timeline_sat_hour[$timesheet->year_week] = $timeline->sat;
                }

                if (isset($timeline_sat_min[$timesheet->year_week])) {
                    $timeline_sat_min[$timesheet->year_week] += $timeline->sat_m;
                } else {
                    $timeline_sat_min[$timesheet->year_week] = $timeline->sat_m;
                }

                //Sunday
                if (isset($timeline_sun_hour[$timesheet->year_week])) {
                    $timeline_sun_hour[$timesheet->year_week] += $timeline->sun;
                } else {
                    $timeline_sun_hour[$timesheet->year_week] = $timeline->sun;
                }

                if (isset($timeline_sun_min[$timesheet->year_week])) {
                    $timeline_sun_min[$timesheet->year_week] += $timeline->sun_m;
                } else {
                    $timeline_sun_min[$timesheet->year_week] = $timeline->sun_m;
                }
            }
        }

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();

        $resouce_name = '';
        $project = Project::find($assignment->project_id);
        $project_manager_name = User::find($project->manager_id);
        $line_manager_name = $assignment->report_to;

        /*Start - Dynamic digisign users*/
        $_digisign_users = [];

        $employee_ts = User::find($assignment->employee_id);
        $tmp_user = new \stdClass();
        $tmp_user->user_number = 1;
        $tmp_user->signed = 0;
        $tmp_user->user_name = '';
        $tmp_user->user_sign_date = '';
        $_digisign_users[] = $tmp_user;

        $project_manager_ts_user = null;
        if ($assignment->project_manager_ts_approver == 1) {
            $project_manager_ts_user = User::find($project->manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 2;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_owner_ts_user = null;
        if ($assignment->product_owner_ts_approver == 1) {
            $project_owner_ts_user = User::find($assignment->product_owner_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 3;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_manager_new_ts_user = null;
        if ($assignment->project_manager_new_ts_approver == 1) {
            $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 4;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $line_manager_ts_user = null;
        if ($assignment->line_manager_ts_approver == 1) {
            $line_manager_ts_user = User::find($assignment->line_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 5;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $claim_approver_ts_user = null;
        if ($assignment->claim_approver_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 6;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $resource_manager_ts_user = null;
        if ($assignment->resource_manager_ts_approver == 1) {
            $resource_manager_ts_user = User::find($assignment->resource_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 7;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }
        /*End - Dynamic digisign users*/

        $parameters = [
            'cfts' => $cfts,
            'timesheet' => $timesheet,
            'cfts_timesheets' => $cfts_timesheet,
            'resouce_name' => $resouce_name,
            'project_manager_name' => $project_manager_name->last_name.' '.$project_manager_name->first_name,
            'line_manager_name' => $line_manager_name,
            'po_number' => isset($assignment->customer_po) ? $assignment->customer_po : '',
            'mon_date' => $mon_date,
            'tue_date' => $tue_date,
            'wed_date' => $wed_date,
            'thu_date' => $thu_date,
            'fri_date' => $fri_date,
            'sat_date' => $sat_date,
            'sun_date' => $sun_date,
            'timeline_mon_hour' => $timeline_mon_hour,
            'timeline_mon_min' => $timeline_mon_min,
            'timeline_tue_hour' => $timeline_tue_hour,
            'timeline_tue_min' => $timeline_tue_min,
            'timeline_wed_hour' => $timeline_wed_hour,
            'timeline_wed_min' => $timeline_wed_min,
            'timeline_thu_hour' => $timeline_thu_hour,
            'timeline_thu_min' => $timeline_thu_min,
            'timeline_fri_hour' => $timeline_fri_hour,
            'timeline_fri_min' => $timeline_fri_min,
            'timeline_sat_hour' => $timeline_sat_hour,
            'timeline_sat_min' => $timeline_sat_min,
            'timeline_sun_hour' => $timeline_sun_hour,
            'timeline_sun_min' => $timeline_sun_min,
            'assignment' => $assignment,
            'digisign_users' => $_digisign_users,
            'employee_ts' => $employee_ts,
            'project_manager_ts_user' => $project_manager_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'resource_manager_ts_user' => $resource_manager_ts_user,
        ];

        if ($request->input('print') == 1) {
            if (! Storage::exists('app/cfts')) {
                Storage::makeDirectory('app/cfts');
            }
            try {
                $storage = storage_path('app/cfts/'.'cfts_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('cfts.sasol_weeks_print', $parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);

                //return view('cfts.sasol_weeks_print')->with($parameters);
            } catch (\Exception $e) {
                report($e);

                return redirect(route('cfts.sasol_weeks_print', $timesheet))->with('flash_danger', $e->getMessage());
            }
        }

        /*if($request->input('print') == 1){
            return view('cfts.sasol_print')->with($parameters);
        }*/

        switch ($cfts->template_id) {
            case 1:
                return view('cfts.sasol_weeks')->with($parameters);
                break;
            case 2:
                return view('cfts.sasol_month')->with($parameters);
                break;
        }
    }

    public function treesScapeWeeks($id, Request $request)
    {
        $cfts = CFTS::find($id);
        //dd($cfts->start_date);
        $cfts_timesheet = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->first();
        $cfts_timesheet_ids = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->orderBy('id')->pluck('timesheet_id');
        $timesheet = Timesheet::find($cfts_timesheet->timesheet_id);
        $timesheets = Timesheet::whereIn('id', $cfts_timesheet_ids)->orderBy('year_week')->get();
        $mon_date = [];
        $timeline_mon_hour = [];
        $timeline_mon_min = [];
        $timeline_tue_hour = [];
        $timeline_tue_min = [];
        $timeline_wed_hour = [];
        $timeline_wed_min = [];
        $timeline_thu_hour = [];
        $timeline_thu_min = [];
        $timeline_fri_hour = [];
        $timeline_fri_min = [];
        $timeline_sat_hour = [];
        $timeline_sat_min = [];
        $timeline_sun_hour = [];
        $timeline_sun_min = [];

        $start_period_counter = 0;
        $start_biling_period = null;
        $end_biling_period = null;
        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->orderBy('yearwk')->get();

            $mon_date = [];
            $tue_date = [];
            $wed_date = [];
            $thu_date = [];
            $fri_date = [];
            $sat_date = [];
            $sun_date = [];

            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('d-M-y', strtotime($date->startOfWeek()));

                //Saturday
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(-2)));
                if ($start_period_counter == 0) {
                    $start_biling_period = $sat_date[$timesheet->year_week];
                }
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                //Monday
                //$mon_date[$timesheet->year_week] = $start_of_week_date;
                $mon_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $tue_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $end_biling_period = $fri_date[$timesheet->year_week];

                if (isset($timeline_mon_hour[$timesheet->year_week])) {
                    $timeline_mon_hour[$timesheet->year_week] += $timeline->mon;
                } else {
                    $timeline_mon_hour[$timesheet->year_week] = $timeline->mon;
                }

                if (isset($timeline_mon_min[$timesheet->year_week])) {
                    $timeline_mon_min[$timesheet->year_week] += $timeline->mon_m;
                } else {
                    $timeline_mon_min[$timesheet->year_week] = $timeline->mon_m;
                }

                //Tuesday
                if (isset($timeline_tue_hour[$timesheet->year_week])) {
                    $timeline_tue_hour[$timesheet->year_week] += $timeline->tue;
                } else {
                    $timeline_tue_hour[$timesheet->year_week] = $timeline->tue;
                }

                if (isset($timeline_tue_min[$timesheet->year_week])) {
                    $timeline_tue_min[$timesheet->year_week] += $timeline->tue_m;
                } else {
                    $timeline_tue_min[$timesheet->year_week] = $timeline->tue_m;
                }

                //Wednesday
                if (isset($timeline_wed_hour[$timesheet->year_week])) {
                    $timeline_wed_hour[$timesheet->year_week] += $timeline->wed;
                } else {
                    $timeline_wed_hour[$timesheet->year_week] = $timeline->wed;
                }

                if (isset($timeline_wed_min[$timesheet->year_week])) {
                    $timeline_wed_min[$timesheet->year_week] += $timeline->wed_m;
                } else {
                    $timeline_wed_min[$timesheet->year_week] = $timeline->wed_m;
                }

                //Thursday
                if (isset($timeline_thu_hour[$timesheet->year_week])) {
                    $timeline_thu_hour[$timesheet->year_week] += $timeline->thu;
                } else {
                    $timeline_thu_hour[$timesheet->year_week] = $timeline->thu;
                }

                if (isset($timeline_thu_min[$timesheet->year_week])) {
                    $timeline_thu_min[$timesheet->year_week] += $timeline->thu_m;
                } else {
                    $timeline_thu_min[$timesheet->year_week] = $timeline->thu_m;
                }

                //Friday
                if (isset($timeline_fri_hour[$timesheet->year_week])) {
                    $timeline_fri_hour[$timesheet->year_week] += $timeline->fri;
                } else {
                    $timeline_fri_hour[$timesheet->year_week] = $timeline->fri;
                }

                if (isset($timeline_fri_min[$timesheet->year_week])) {
                    $timeline_fri_min[$timesheet->year_week] += $timeline->fri_m;
                } else {
                    $timeline_fri_min[$timesheet->year_week] = $timeline->fri_m;
                }

                //Saturday
                if (isset($timeline_sat_hour[$timesheet->year_week])) {
                    $timeline_sat_hour[$timesheet->year_week] += $timeline->sat;
                } else {
                    $timeline_sat_hour[$timesheet->year_week] = $timeline->sat;
                }

                if (isset($timeline_sat_min[$timesheet->year_week])) {
                    $timeline_sat_min[$timesheet->year_week] += $timeline->sat_m;
                } else {
                    $timeline_sat_min[$timesheet->year_week] = $timeline->sat_m;
                }

                //Sunday
                if (isset($timeline_sun_hour[$timesheet->year_week])) {
                    $timeline_sun_hour[$timesheet->year_week] += $timeline->sun;
                } else {
                    $timeline_sun_hour[$timesheet->year_week] = $timeline->sun;
                }

                if (isset($timeline_sun_min[$timesheet->year_week])) {
                    $timeline_sun_min[$timesheet->year_week] += $timeline->sun_m;
                } else {
                    $timeline_sun_min[$timesheet->year_week] = $timeline->sun_m;
                }

                $start_period_counter += 1;
            }
        }

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();

        $resouce_name = '';
        $project = Project::find($assignment->project_id);
        $project_manager_name = User::find($project->manager_id);
        $line_manager_name = $assignment->report_to;

        $timesheetPeriod = date('F y', strtotime($end_biling_period));
        $timesheetBillingPeriod = '';

        /*Start - Dynamic digisign users*/
        $_digisign_users = [];

        $employee_ts = User::find($assignment->employee_id);
        $tmp_user = new \stdClass();
        $tmp_user->user_number = 1;
        $tmp_user->signed = 0;
        $tmp_user->user_name = '';
        $tmp_user->user_sign_date = '';
        $_digisign_users[] = $tmp_user;

        $project_manager_ts_user = null;
        if ($assignment->project_manager_ts_approver == 1) {
            $project_manager_ts_user = User::find($project->manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 2;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_owner_ts_user = null;
        if ($assignment->product_owner_ts_approver == 1) {
            $project_owner_ts_user = User::find($assignment->product_owner_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 3;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_manager_new_ts_user = null;
        if ($assignment->project_manager_new_ts_approver == 1) {
            $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 4;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $line_manager_ts_user = null;
        if ($assignment->line_manager_ts_approver == 1) {
            $line_manager_ts_user = User::find($assignment->line_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 5;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $claim_approver_ts_user = null;
        if ($assignment->claim_approver_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 6;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $resource_manager_ts_user = null;
        if ($assignment->resource_manager_ts_approver == 1) {
            $resource_manager_ts_user = User::find($assignment->resource_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 7;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }
        /*End - Dynamic digisign users*/

        $parameters = [
            'cfts' => $cfts,
            'timesheet' => $timesheet,
            'cfts_timesheets' => $cfts_timesheet,
            'resouce_name' => $resouce_name,
            'project_manager_name' => $project_manager_name->last_name.' '.$project_manager_name->first_name,
            'line_manager_name' => $line_manager_name,
            'po_number' => isset($assignment[0]->customer_po) ? $assignment[0]->customer_po : '',
            'mon_date' => $mon_date,
            'tue_date' => $tue_date,
            'wed_date' => $wed_date,
            'thu_date' => $thu_date,
            'fri_date' => $fri_date,
            'sat_date' => $sat_date,
            'sun_date' => $sun_date,
            'timeline_mon_hour' => $timeline_mon_hour,
            'timeline_mon_min' => $timeline_mon_min,
            'timeline_tue_hour' => $timeline_tue_hour,
            'timeline_tue_min' => $timeline_tue_min,
            'timeline_wed_hour' => $timeline_wed_hour,
            'timeline_wed_min' => $timeline_wed_min,
            'timeline_thu_hour' => $timeline_thu_hour,
            'timeline_thu_min' => $timeline_thu_min,
            'timeline_fri_hour' => $timeline_fri_hour,
            'timeline_fri_min' => $timeline_fri_min,
            'timeline_sat_hour' => $timeline_sat_hour,
            'timeline_sat_min' => $timeline_sat_min,
            'timeline_sun_hour' => $timeline_sun_hour,
            'timeline_sun_min' => $timeline_sun_min,
            'timesheetPeriod' => $timesheetPeriod,
            'timesheetBillingPeriod' => $timesheetBillingPeriod,
            'start_biling_period' => $start_biling_period,
            'end_biling_period' => $end_biling_period,
            'assignment' => $assignment,
            'digisign_users' => $_digisign_users,
            'employee_ts' => $employee_ts,
            'project_manager_ts_user' => $project_manager_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'resource_manager_ts_user' => $resource_manager_ts_user,
        ];

        if ($request->input('print') == 1) {
            if (! Storage::exists('app/cfts')) {
                Storage::makeDirectory('app/cfts');
            }
            try {
                $storage = storage_path('app/cfts/'.'cfts_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('cfts.treescape_weeks_print', $parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);

                //return view('cfts.sasol_weeks_print')->with($parameters);
            } catch (\Exception $e) {
                report($e);

                return redirect(route('cfts.treescape_weeks_print', $timesheet))->with('flash_danger', $e->getMessage());
            }
        }

        return view('cfts.treescape_weeks')->with($parameters);
    }

    public function blackboardMonthly($id, Request $request)
    {
        $cfts = CFTS::find($id);
        //dd($cfts->start_date);
        $cfts_timesheet = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->first();
        $cfts_timesheet_ids = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->orderBy('id')->pluck('timesheet_id');
        $timesheet = Timesheet::find($cfts_timesheet->timesheet_id);
        $timesheets = Timesheet::whereIn('id', $cfts_timesheet_ids)->orderBy('year_week')->get();
        $mon_date = [];
        $timeline_mon_hour = [];
        $timeline_mon_min = [];
        $timeline_tue_hour = [];
        $timeline_tue_min = [];
        $timeline_wed_hour = [];
        $timeline_wed_min = [];
        $timeline_thu_hour = [];
        $timeline_thu_min = [];
        $timeline_fri_hour = [];
        $timeline_fri_min = [];
        $timeline_sat_hour = [];
        $timeline_sat_min = [];
        $timeline_sun_hour = [];
        $timeline_sun_min = [];

        $start_period_counter = 0;
        $start_biling_period = null;
        $end_biling_period = null;
        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->orderBy('yearwk')->get();
            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('d-M-y', strtotime($date->startOfWeek()));

                //Saturday
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(-2)));
                if ($start_period_counter == 0) {
                    $start_biling_period = $sat_date[$timesheet->year_week];
                }
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                //Monday
                //$mon_date[$timesheet->year_week] = $start_of_week_date;
                $mon_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $tue_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $end_biling_period = $fri_date[$timesheet->year_week];

                if (isset($timeline_mon_hour[$timesheet->year_week])) {
                    $timeline_mon_hour[$timesheet->year_week] += $timeline->mon;
                } else {
                    $timeline_mon_hour[$timesheet->year_week] = $timeline->mon;
                }

                if (isset($timeline_mon_min[$timesheet->year_week])) {
                    $timeline_mon_min[$timesheet->year_week] += $timeline->mon_m;
                } else {
                    $timeline_mon_min[$timesheet->year_week] = $timeline->mon_m;
                }

                //Tuesday
                if (isset($timeline_tue_hour[$timesheet->year_week])) {
                    $timeline_tue_hour[$timesheet->year_week] += $timeline->tue;
                } else {
                    $timeline_tue_hour[$timesheet->year_week] = $timeline->tue;
                }

                if (isset($timeline_tue_min[$timesheet->year_week])) {
                    $timeline_tue_min[$timesheet->year_week] += $timeline->tue_m;
                } else {
                    $timeline_tue_min[$timesheet->year_week] = $timeline->tue_m;
                }

                //Wednesday
                if (isset($timeline_wed_hour[$timesheet->year_week])) {
                    $timeline_wed_hour[$timesheet->year_week] += $timeline->wed;
                } else {
                    $timeline_wed_hour[$timesheet->year_week] = $timeline->wed;
                }

                if (isset($timeline_wed_min[$timesheet->year_week])) {
                    $timeline_wed_min[$timesheet->year_week] += $timeline->wed_m;
                } else {
                    $timeline_wed_min[$timesheet->year_week] = $timeline->wed_m;
                }

                //Thursday
                if (isset($timeline_thu_hour[$timesheet->year_week])) {
                    $timeline_thu_hour[$timesheet->year_week] += $timeline->thu;
                } else {
                    $timeline_thu_hour[$timesheet->year_week] = $timeline->thu;
                }

                if (isset($timeline_thu_min[$timesheet->year_week])) {
                    $timeline_thu_min[$timesheet->year_week] += $timeline->thu_m;
                } else {
                    $timeline_thu_min[$timesheet->year_week] = $timeline->thu_m;
                }

                //Friday
                if (isset($timeline_fri_hour[$timesheet->year_week])) {
                    $timeline_fri_hour[$timesheet->year_week] += $timeline->fri;
                } else {
                    $timeline_fri_hour[$timesheet->year_week] = $timeline->fri;
                }

                if (isset($timeline_fri_min[$timesheet->year_week])) {
                    $timeline_fri_min[$timesheet->year_week] += $timeline->fri_m;
                } else {
                    $timeline_fri_min[$timesheet->year_week] = $timeline->fri_m;
                }

                //Saturday
                if (isset($timeline_sat_hour[$timesheet->year_week])) {
                    $timeline_sat_hour[$timesheet->year_week] += $timeline->sat;
                } else {
                    $timeline_sat_hour[$timesheet->year_week] = $timeline->sat;
                }

                if (isset($timeline_sat_min[$timesheet->year_week])) {
                    $timeline_sat_min[$timesheet->year_week] += $timeline->sat_m;
                } else {
                    $timeline_sat_min[$timesheet->year_week] = $timeline->sat_m;
                }

                //Sunday
                if (isset($timeline_sun_hour[$timesheet->year_week])) {
                    $timeline_sun_hour[$timesheet->year_week] += $timeline->sun;
                } else {
                    $timeline_sun_hour[$timesheet->year_week] = $timeline->sun;
                }

                if (isset($timeline_sun_min[$timesheet->year_week])) {
                    $timeline_sun_min[$timesheet->year_week] += $timeline->sun_m;
                } else {
                    $timeline_sun_min[$timesheet->year_week] = $timeline->sun_m;
                }

                $start_period_counter += 1;
            }
        }

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();

        $resouce_name = '';
        $project = Project::find($assignment->project_id);
        $project_manager_name = User::find($project->manager_id);
        $line_manager_name = $assignment->report_to;

        $timesheetPeriod = date('F y', strtotime($end_biling_period));
        $timesheetBillingPeriod = '';

        /*Start - Dynamic digisign users*/
        $_digisign_users = [];

        $employee_ts = User::find($assignment->employee_id);
        $tmp_user = new \stdClass();
        $tmp_user->user_number = 1;
        $tmp_user->signed = 0;
        $tmp_user->user_name = '';
        $tmp_user->user_sign_date = '';
        $_digisign_users[] = $tmp_user;

        $project_manager_ts_user = null;
        if ($assignment->project_manager_ts_approver == 1) {
            $project_manager_ts_user = User::find($project->manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 2;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_owner_ts_user = null;
        if ($assignment->product_owner_ts_approver == 1) {
            $project_owner_ts_user = User::find($assignment->product_owner_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 3;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_manager_new_ts_user = null;
        if ($assignment->project_manager_new_ts_approver == 1) {
            $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 4;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $line_manager_ts_user = null;
        if ($assignment->line_manager_ts_approver == 1) {
            $line_manager_ts_user = User::find($assignment->line_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 5;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $claim_approver_ts_user = null;
        if ($assignment->claim_approver_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 6;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $resource_manager_ts_user = null;
        if ($assignment->resource_manager_ts_approver == 1) {
            $resource_manager_ts_user = User::find($assignment->resource_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 7;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }
        /*End - Dynamic digisign users*/

        $parameters = [
            'cfts' => $cfts,
            'timesheet' => $timesheet,
            'cfts_timesheets' => $cfts_timesheet,
            'resouce_name' => $resouce_name,
            'project_manager_name' => $project_manager_name->last_name.' '.$project_manager_name->first_name,
            'line_manager_name' => $line_manager_name,
            'po_number' => isset($assignment[0]->customer_po) ? $assignment[0]->customer_po : '',
            'mon_date' => $mon_date,
            'tue_date' => $tue_date,
            'wed_date' => $wed_date,
            'thu_date' => $thu_date,
            'fri_date' => $fri_date,
            'sat_date' => $sat_date,
            'sun_date' => $sun_date,
            'timeline_mon_hour' => $timeline_mon_hour,
            'timeline_mon_min' => $timeline_mon_min,
            'timeline_tue_hour' => $timeline_tue_hour,
            'timeline_tue_min' => $timeline_tue_min,
            'timeline_wed_hour' => $timeline_wed_hour,
            'timeline_wed_min' => $timeline_wed_min,
            'timeline_thu_hour' => $timeline_thu_hour,
            'timeline_thu_min' => $timeline_thu_min,
            'timeline_fri_hour' => $timeline_fri_hour,
            'timeline_fri_min' => $timeline_fri_min,
            'timeline_sat_hour' => $timeline_sat_hour,
            'timeline_sat_min' => $timeline_sat_min,
            'timeline_sun_hour' => $timeline_sun_hour,
            'timeline_sun_min' => $timeline_sun_min,
            'timesheetPeriod' => $timesheetPeriod,
            'timesheetBillingPeriod' => $timesheetBillingPeriod,
            'start_biling_period' => $start_biling_period,
            'end_biling_period' => $end_biling_period,
            'assignment' => $assignment,
            'digisign_users' => $_digisign_users,
            'employee_ts' => $employee_ts,
            'project_manager_ts_user' => $project_manager_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'resource_manager_ts_user' => $resource_manager_ts_user,
        ];

        if ($request->input('print') == 1) {
            if (! Storage::exists('cfts')) {
                Storage::makeDirectory('cfts');
            }
            try {
                $storage = storage_path('app/cfts/'.'cfts_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('cfts.blackboard_monthly_timesheet_print', $parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);
            } catch (\Exception $e) {
                report($e);

                return redirect(route('cfts.blackboard_monthly_timesheet_print', $timesheet))->with('flash_danger', $e->getMessage());
            }
        }

        return view('cfts.blackboard_monthly_timesheet')->with($parameters);
    }

    public function matrilogixMonthly($id, Request $request)
    {
        $cfts = CFTS::find($id);
        //dd($cfts->start_date);
        $cfts_timesheet = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->first();
        $cfts_timesheet_ids = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->orderBy('id')->pluck('timesheet_id');
        $timesheet = Timesheet::find($cfts_timesheet->timesheet_id);
        $timesheets = Timesheet::whereIn('id', $cfts_timesheet_ids)->orderBy('year_week')->get();
        $mon_date = [];
        $timeline_mon_hour = [];
        $timeline_mon_min = [];
        $timeline_tue_hour = [];
        $timeline_tue_min = [];
        $timeline_wed_hour = [];
        $timeline_wed_min = [];
        $timeline_thu_hour = [];
        $timeline_thu_min = [];
        $timeline_fri_hour = [];
        $timeline_fri_min = [];
        $timeline_sat_hour = [];
        $timeline_sat_min = [];
        $timeline_sun_hour = [];
        $timeline_sun_min = [];

        $start_period_counter = 0;
        $start_biling_period = null;
        $end_biling_period = null;
        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->orderBy('yearwk')->get();
            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('d-M-y', strtotime($date->startOfWeek()));

                /*
                //Print wrong order, please investigate
                //Saturday
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(-2)));
                if($start_period_counter == 0){
                    $start_biling_period = $sat_date[$timesheet->year_week];
                }
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                */
                //Monday
                //$mon_date[$timesheet->year_week] = $start_of_week_date;
                $mon_date[$timesheet->year_week] = date('d-M-y', strtotime($date));
                //$mon_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $tue_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                //Moved from the commented out code above, for matrilogix
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $end_biling_period = $fri_date[$timesheet->year_week];

                if (isset($timeline_mon_hour[$timesheet->year_week])) {
                    $timeline_mon_hour[$timesheet->year_week] += $timeline->mon;
                } else {
                    $timeline_mon_hour[$timesheet->year_week] = $timeline->mon;
                }

                if (isset($timeline_mon_min[$timesheet->year_week])) {
                    $timeline_mon_min[$timesheet->year_week] += $timeline->mon_m;
                } else {
                    $timeline_mon_min[$timesheet->year_week] = $timeline->mon_m;
                }

                //Tuesday
                if (isset($timeline_tue_hour[$timesheet->year_week])) {
                    $timeline_tue_hour[$timesheet->year_week] += $timeline->tue;
                } else {
                    $timeline_tue_hour[$timesheet->year_week] = $timeline->tue;
                }

                if (isset($timeline_tue_min[$timesheet->year_week])) {
                    $timeline_tue_min[$timesheet->year_week] += $timeline->tue_m;
                } else {
                    $timeline_tue_min[$timesheet->year_week] = $timeline->tue_m;
                }

                //Wednesday
                if (isset($timeline_wed_hour[$timesheet->year_week])) {
                    $timeline_wed_hour[$timesheet->year_week] += $timeline->wed;
                } else {
                    $timeline_wed_hour[$timesheet->year_week] = $timeline->wed;
                }

                if (isset($timeline_wed_min[$timesheet->year_week])) {
                    $timeline_wed_min[$timesheet->year_week] += $timeline->wed_m;
                } else {
                    $timeline_wed_min[$timesheet->year_week] = $timeline->wed_m;
                }

                //Thursday
                if (isset($timeline_thu_hour[$timesheet->year_week])) {
                    $timeline_thu_hour[$timesheet->year_week] += $timeline->thu;
                } else {
                    $timeline_thu_hour[$timesheet->year_week] = $timeline->thu;
                }

                if (isset($timeline_thu_min[$timesheet->year_week])) {
                    $timeline_thu_min[$timesheet->year_week] += $timeline->thu_m;
                } else {
                    $timeline_thu_min[$timesheet->year_week] = $timeline->thu_m;
                }

                //Friday
                if (isset($timeline_fri_hour[$timesheet->year_week])) {
                    $timeline_fri_hour[$timesheet->year_week] += $timeline->fri;
                } else {
                    $timeline_fri_hour[$timesheet->year_week] = $timeline->fri;
                }

                if (isset($timeline_fri_min[$timesheet->year_week])) {
                    $timeline_fri_min[$timesheet->year_week] += $timeline->fri_m;
                } else {
                    $timeline_fri_min[$timesheet->year_week] = $timeline->fri_m;
                }

                //Saturday
                if (isset($timeline_sat_hour[$timesheet->year_week])) {
                    $timeline_sat_hour[$timesheet->year_week] += $timeline->sat;
                } else {
                    $timeline_sat_hour[$timesheet->year_week] = $timeline->sat;
                }

                if (isset($timeline_sat_min[$timesheet->year_week])) {
                    $timeline_sat_min[$timesheet->year_week] += $timeline->sat_m;
                } else {
                    $timeline_sat_min[$timesheet->year_week] = $timeline->sat_m;
                }

                //Sunday
                if (isset($timeline_sun_hour[$timesheet->year_week])) {
                    $timeline_sun_hour[$timesheet->year_week] += $timeline->sun;
                } else {
                    $timeline_sun_hour[$timesheet->year_week] = $timeline->sun;
                }

                if (isset($timeline_sun_min[$timesheet->year_week])) {
                    $timeline_sun_min[$timesheet->year_week] += $timeline->sun_m;
                } else {
                    $timeline_sun_min[$timesheet->year_week] = $timeline->sun_m;
                }

                $start_period_counter += 1;
            }
        }

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();

        $resouce_name = '';
        $project = Project::find($assignment->project_id);
        $project_manager_name = User::find($project->manager_id);
        $line_manager_name = $assignment->report_to;

        $timesheetPeriod = date('F y', strtotime($end_biling_period));
        $timesheetBillingPeriod = '';

        /*Start - Dynamic digisign users*/
        $_digisign_users = [];

        $employee_ts = User::find($assignment->employee_id);
        $tmp_user = new \stdClass();
        $tmp_user->user_number = 1;
        $tmp_user->signed = 0;
        $tmp_user->user_name = '';
        $tmp_user->user_sign_date = '';
        $_digisign_users[] = $tmp_user;

        $project_manager_ts_user = null;
        if ($assignment->project_manager_ts_approver == 1) {
            $project_manager_ts_user = User::find($project->manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 2;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_owner_ts_user = null;
        if ($assignment->product_owner_ts_approver == 1) {
            $project_owner_ts_user = User::find($assignment->product_owner_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 3;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $project_manager_new_ts_user = null;
        if ($assignment->project_manager_new_ts_approver == 1) {
            $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 4;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $line_manager_ts_user = null;
        if ($assignment->line_manager_ts_approver == 1) {
            $line_manager_ts_user = User::find($assignment->line_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 5;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $claim_approver_ts_user = null;
        if ($assignment->claim_approver_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 6;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }

        $resource_manager_ts_user = null;
        if ($assignment->resource_manager_ts_approver == 1) {
            $resource_manager_ts_user = User::find($assignment->resource_manager_id);
            $tmp_user = new \stdClass();
            $tmp_user->user_number = 7;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $_digisign_users[] = $tmp_user;
        }
        /*End - Dynamic digisign users*/

        $parameters = [
            'cfts' => $cfts,
            'timesheet' => $timesheet,
            'cfts_timesheets' => $cfts_timesheet,
            'resouce_name' => $resouce_name,
            'project_manager_name' => $project_manager_name->last_name.' '.$project_manager_name->first_name,
            'line_manager_name' => $line_manager_name,
            'po_number' => isset($assignment[0]->customer_po) ? $assignment[0]->customer_po : '',
            'mon_date' => $mon_date,
            'tue_date' => $tue_date,
            'wed_date' => $wed_date,
            'thu_date' => $thu_date,
            'fri_date' => $fri_date,
            'sat_date' => $sat_date,
            'sun_date' => $sun_date,
            'timeline_mon_hour' => $timeline_mon_hour,
            'timeline_mon_min' => $timeline_mon_min,
            'timeline_tue_hour' => $timeline_tue_hour,
            'timeline_tue_min' => $timeline_tue_min,
            'timeline_wed_hour' => $timeline_wed_hour,
            'timeline_wed_min' => $timeline_wed_min,
            'timeline_thu_hour' => $timeline_thu_hour,
            'timeline_thu_min' => $timeline_thu_min,
            'timeline_fri_hour' => $timeline_fri_hour,
            'timeline_fri_min' => $timeline_fri_min,
            'timeline_sat_hour' => $timeline_sat_hour,
            'timeline_sat_min' => $timeline_sat_min,
            'timeline_sun_hour' => $timeline_sun_hour,
            'timeline_sun_min' => $timeline_sun_min,
            'timesheetPeriod' => $timesheetPeriod,
            'timesheetBillingPeriod' => $timesheetBillingPeriod,
            'start_biling_period' => $start_biling_period,
            'end_biling_period' => $end_biling_period,
            'assignment' => $assignment,
            'digisign_users' => $_digisign_users,
            'employee_ts' => $employee_ts,
            'project_manager_ts_user' => $project_manager_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'resource_manager_ts_user' => $resource_manager_ts_user,
        ];

        if ($request->input('print') == 1) {
            if (! Storage::exists('cfts')) {
                Storage::makeDirectory('cfts');
            }
            try {
                $storage = storage_path('app/cfts/'.'cfts_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('cfts.matrilogix_monthly_timesheet_print', $parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);
            } catch (\Exception $e) {
                report($e);

                return redirect(route('cfts.matrilogix_monthly_timesheet_print', $timesheet))->with('flash_danger', $e->getMessage());
            }
        }

        return view('cfts.matrilogix_monthly_timesheet')->with($parameters);
    }

    public function weeklyDetail($id, Request $request)
    {
        $timesheet = Timesheet::find($id);

        $project = Project::find($timesheet->project_id);

        $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)->where('project_id', '=', $timesheet->project_id)->first();

        $employee = User::find($timesheet->employee_id);

        $subject = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';
        $subject_send_email = 'Timesheet - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';

        $product_ownder = User::find($assignment->product_owner_id);

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();
        $time_exp = TimeExp::where('timesheet_id', '=', $timesheet->id)->get();
        $timelines_drop_down = ['Select Group Task Number'];

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $total_exp_bill = 0;
        $total_exp_claim = 0;
        foreach ($time_exp as $expense) {
            $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
            $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
        }

        $total_time_in_minutes = 0;
        foreach ($timelines as $timeline) {
            $total_minutes = 0;

            if ($project->type == 1) {
                if ($timeline->is_billable == 1) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            } else {
                if ($timeline->is_billable == 0) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            }

            $total_time_in_minutes += $total_minutes;
        }

        //dd($project);

        $parameters = [
            'project' => $project,
            'subject' => $subject,
            'subject_send_email' => $subject_send_email,
            'timesheet' => $timesheet,
            'assignment' => $assignment,
            'timelines' => $timelines,
            'time_exp' => $time_exp,
            'employee' => $employee,
            'product_ownder' => $product_ownder,
            'total_exp_bill' => $total_exp_bill,
            'total_exp_claim' => $total_exp_claim,
            'hours' => $hours,
            'timelines_drop_down' => $timelines_drop_down,
        ];

        if ($request->print == '1') {
            if (! Storage::exists('app/timesheets/'.(isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : ''))) {
                Storage::makeDirectory('app/timesheets/'.(isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : ''));
            }
            try {
                $storage = storage_path('app/timesheets/'.(isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : '').'/'.(isset($timesheet->user) ? substr($timesheet->user->first_name, 0, 4).''.substr($timesheet->user->last_name, 0, 4) : '').(isset($timesheet->project) ? str_replace(' ', '_', $timesheet->project->name) : '').($timesheet->year_week).'_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('timesheet.print', $parameters)
                    ->format('a4')
                    ->save($storage);

                $document = new Document();
                //$document->type_id = $template->id;
                $document->document_type_id = 5;
                $document->name = 'Customer Tax Invoice';
                $document->file = (isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : '').'/'.(isset($timesheet->user) ? substr($timesheet->user->first_name, 0, 4).''.substr($timesheet->user->last_name, 0, 4) : '').(isset($timesheet->project) ? str_replace(' ', '_', $timesheet->project->name) : '').($timesheet->year_week).'_'.date('Y-m-d_H_i_s').'.pdf';
                $document->creator_id = auth()->id();
                $document->owner_id = $timesheet->employee_id;
                //$document->digisign_status_id = 2;
                //$document->reference_id = $timesheets[0]->project->ref;
                //$document->digisign_approver_user_id = $assignment_approver_resource->id;
                $document->save();

                return response()->file($storage);
            } catch (\Exception $e) {
                report($e);

                return redirect(route('timesheet', $timesheet))->with('flash_danger', $e->getMessage());
            }
        } else {
            return view('cfts.weekly_detail')->with($parameters);
        }
    }

    public function pdf($id)
    {
        $cfts = CFTS::find($id);
        //dd($cfts->start_date);
        $cfts_timesheet = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->first();
        $cfts_timesheet_ids = CFTSTimesheet::where('cfts_id', '=', $cfts->id)->orderBy('id')->pluck('timesheet_id');
        $timesheet = Timesheet::find($cfts_timesheet->timesheet_id);
        $timesheets = Timesheet::whereIn('id', $cfts_timesheet_ids)->orderBy('year_week')->get();
        $mon_date = [];
        $timeline_mon_hour = [];
        $timeline_mon_min = [];
        $timeline_tue_hour = [];
        $timeline_tue_min = [];
        $timeline_wed_hour = [];
        $timeline_wed_min = [];
        $timeline_thu_hour = [];
        $timeline_thu_min = [];
        $timeline_fri_hour = [];
        $timeline_fri_min = [];
        $timeline_sat_hour = [];
        $timeline_sat_min = [];
        $timeline_sun_hour = [];
        $timeline_sun_min = [];
        foreach ($timesheets as $timesheet) {
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->orderBy('yearwk')->get();
            foreach ($timelines as $timeline) {
                $date = Carbon::now();

                $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                $start_of_week_date = date('d-M-y', strtotime($date->startOfWeek()));
                //Monday
                $mon_date[$timesheet->year_week] = $start_of_week_date;
                $tue_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $wed_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $thu_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $fri_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $sat_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                $sun_date[$timesheet->year_week] = date('d-M-y', strtotime($date->addDays(1)));
                if (isset($timeline_mon_hour[$timesheet->year_week])) {
                    $timeline_mon_hour[$timesheet->year_week] += $timeline->mon;
                } else {
                    $timeline_mon_hour[$timesheet->year_week] = $timeline->mon;
                }

                if (isset($timeline_mon_min[$timesheet->year_week])) {
                    $timeline_mon_min[$timesheet->year_week] += $timeline->mon_m;
                } else {
                    $timeline_mon_min[$timesheet->year_week] = $timeline->mon_m;
                }

                //Tuesday
                if (isset($timeline_tue_hour[$timesheet->year_week])) {
                    $timeline_tue_hour[$timesheet->year_week] += $timeline->tue;
                } else {
                    $timeline_tue_hour[$timesheet->year_week] = $timeline->tue;
                }

                if (isset($timeline_tue_min[$timesheet->year_week])) {
                    $timeline_tue_min[$timesheet->year_week] += $timeline->tue_m;
                } else {
                    $timeline_tue_min[$timesheet->year_week] = $timeline->tue_m;
                }

                //Wednesday
                if (isset($timeline_wed_hour[$timesheet->year_week])) {
                    $timeline_wed_hour[$timesheet->year_week] += $timeline->wed;
                } else {
                    $timeline_wed_hour[$timesheet->year_week] = $timeline->wed;
                }

                if (isset($timeline_wed_min[$timesheet->year_week])) {
                    $timeline_wed_min[$timesheet->year_week] += $timeline->wed_m;
                } else {
                    $timeline_wed_min[$timesheet->year_week] = $timeline->wed_m;
                }

                //Thursday
                if (isset($timeline_thu_hour[$timesheet->year_week])) {
                    $timeline_thu_hour[$timesheet->year_week] += $timeline->thu;
                } else {
                    $timeline_thu_hour[$timesheet->year_week] = $timeline->thu;
                }

                if (isset($timeline_thu_min[$timesheet->year_week])) {
                    $timeline_thu_min[$timesheet->year_week] += $timeline->thu_m;
                } else {
                    $timeline_thu_min[$timesheet->year_week] = $timeline->thu_m;
                }

                //Friday
                if (isset($timeline_fri_hour[$timesheet->year_week])) {
                    $timeline_fri_hour[$timesheet->year_week] += $timeline->fri;
                } else {
                    $timeline_fri_hour[$timesheet->year_week] = $timeline->fri;
                }

                if (isset($timeline_fri_min[$timesheet->year_week])) {
                    $timeline_fri_min[$timesheet->year_week] += $timeline->fri_m;
                } else {
                    $timeline_fri_min[$timesheet->year_week] = $timeline->fri_m;
                }

                //Saturday
                if (isset($timeline_sat_hour[$timesheet->year_week])) {
                    $timeline_sat_hour[$timesheet->year_week] += $timeline->sat;
                } else {
                    $timeline_sat_hour[$timesheet->year_week] = $timeline->sat;
                }

                if (isset($timeline_sat_min[$timesheet->year_week])) {
                    $timeline_sat_min[$timesheet->year_week] += $timeline->sat_m;
                } else {
                    $timeline_sat_min[$timesheet->year_week] = $timeline->sat_m;
                }

                //Sunday
                if (isset($timeline_sun_hour[$timesheet->year_week])) {
                    $timeline_sun_hour[$timesheet->year_week] += $timeline->sun;
                } else {
                    $timeline_sun_hour[$timesheet->year_week] = $timeline->sun;
                }

                if (isset($timeline_sun_min[$timesheet->year_week])) {
                    $timeline_sun_min[$timesheet->year_week] += $timeline->sun_m;
                } else {
                    $timeline_sun_min[$timesheet->year_week] = $timeline->sun_m;
                }
            }
        }

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->get();

        $resouce_name = '';
        $project = Project::find($assignment[0]->project_id);
        $project_manager_name = User::find($project->manager_id);
        $line_manager_name = $assignment[0]->report_to;

        $cfts_timesheets = $cfts_timesheet;
        $project_manager_name = $project_manager_name->last_name.' '.$project_manager_name->first_name;
        $po_number = isset($assignment[0]->customer_po) ? $assignment[0]->customer_po : '';

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        $pdf->AddPage();
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        $file_name = 'resources_'.date('Y_m_d_H_i_s').'.pdf';
        $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
        $file_name_pdf = $dir.'file_'.date('Y_m_d_H_i_s').'.pdf';

        $html = '<table class="table table-sm mt-3">
                    <thead>
                        <tr>
                            <th colspan="3">
                                <img src="'.asset('assets/templates/eim_template_header.png').'" width="100%;" style="border: none;">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3" class="eim-template-heading" style="text-align: center;">'.$cfts->additional_text.'</th>
                        </tr>
                        <tr>
                            <th colspan="3" style="color: #ff1320">Note: All overtime will require pre-approval by your Line Manager</th>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">Resource Name:</th><td colspan="2">'.(isset($timesheet->employee->last_name) ? $timesheet->employee->last_name : ''.' '.(isset($timesheet->employee->first_name) ? $timesheet->employee->first_name : '')).'</td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">Project Name:</th><td colspan="2">'.isset($timesheet->project->name) ? $timesheet->project->name : ''.'</td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">PPO Number:</th><td colspan="2"></td>
                        </tr>
                        <tr>
                            <th class="eim-template-heading" style="width: 33%;">PO Number: </th><td colspan="2">'.$po_number.'</td>
                        </tr>
                        <tr>
                            <th colspan="3"></td>
                        </tr>
                        <tr>
                            <th colspan="3">
                                <div class="row">';

        $total_billing_hours = 0;
        $Weeks_counter = 1;

        foreach ($timeline_mon_hour as $key => $value) {
            $total_minutes = ($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]) + ($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]) + ($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]) + ($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]) + ($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]) + ($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]) + ($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]);
            $total_time = $this->minutesToTime($total_minutes);

            $html .= '<div class="col-sm-4">
                                                        <table class="table borderless table-sm mt-3">
                                                            <thead>
                                                                <tr><th colspan="3" class="eim-template-heading" style="text-align: center;">Week '.$Weeks_counter.'</th></tr>
                                                                <tr><td class="eim-template-heading">Date</td><td class="eim-template-heading">Day</td><td class="eim-template-heading">Hours</td></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr><td>'.$mon_date[$key].'</td><td>Monday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_mon_hour[$key] * 60 + $timeline_mon_min[$key]).'</td></tr>
                                                                <tr><td>'.$tue_date[$key].'</td><td>Tuesday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_tue_hour[$key] * 60 + $timeline_tue_min[$key]).'</td></tr>
                                                                <tr><td>'.$wed_date[$key].'</td><td>Wednesday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_wed_hour[$key] * 60 + $timeline_wed_min[$key]).'</td></tr>
                                                                <tr><td>'.$thu_date[$key].'</td><td>Thursday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_thu_hour[$key] * 60 + $timeline_thu_min[$key]).'</td></tr>
                                                                <tr><td>'.$fri_date[$key].'</td><td>Friday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_fri_hour[$key] * 60 + $timeline_fri_min[$key]).'</td></tr>
                                                                <tr><td>'.$sat_date[$key].'</td><td>Saturday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_sat_hour[$key] * 60 + $timeline_sat_min[$key]).'</td></tr>
                                                                <tr><td>'.$sun_date[$key].'</td><td>Sunday</td><td style="text-align: right;">'.$this->minutesToTime($timeline_sun_hour[$key] * 60 + $timeline_sun_min[$key]).'</td></tr>
                                                                <tr><th colspan="2" class="eim-template-heading">Week Total</th><th class="eim-template-heading" style="text-align: right;">'.$total_time.'</th></tr>
                                                            </tbody>
                                                        </table>
                                                    </div>';

            $total_billing_hours += $total_minutes;
            $Weeks_counter++;
        }
        $html .= '<div class="col-sm-4">
                                        <table class="table borderless table-sm mt-3">
                                            <thead>
                                                <tr><th colspan="2" class="eim-template-heading">Total Billing Hours</th><th class="eim-template-heading" style="text-align: right;">'.$this->minutesToTime($total_billing_hours).'</th></tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="3">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <table class="table borderless table-sm mt-3">
                                            <thead>
                                                <tr><th class="eim-template-heading">Resource</th></tr>
                                                <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                                <tr><td>'.(isset($timesheet->employee->last_name) ? $timesheet->employee->last_name : ''.' '.(isset($timesheet->employee->first_name) ? $timesheet->employee->first_name : '')).'</td></tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-sm-4">
                                        <table class="table borderless table-sm mt-3">
                                            <thead>
                                            <tr><th class="eim-template-heading">Project Manager</th></tr>
                                            <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                            <tr><td>'.$project_manager_name.'</td></tr>
                                        </table>
                                    </div>
                                    <div class="col-sm-4">
                                        <table class="table borderless table-sm mt-3">
                                            <thead>
                                            <tr><th class="eim-template-heading">Line Manager</th></tr>
                                            <tr><td style="height: 70px; border: 1px #000000 solid;"></td></tr>
                                            <tr><td>'.$line_manager_name.'</td></tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    </tbody>
                </table>';

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->Output($file_name_pdf, 'I');
    }

    public function destroy($id): RedirectResponse
    {
        $cfts_timesheets = CFTSTimesheet::where('cfts_id', '=', $id)->orderBy('id')->pluck('id');
        foreach ($cfts_timesheets as $cfts_timesheet) {
            CFTSTimesheet::destroy($cfts_timesheet);
        }

        CFTS::destroy($id);

        return redirect(route('cfts.index'))->with('flash_success', 'Custom formatted timesheet deleted successfully');
    }

    public function minutesToTime($minutes)
    {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;
        $negation_number = '';
        if ($hours < 0) {
            $hours *= -1;
            $negation_number = '-';
        }

        if ($minutes < 0) {
            $minutes *= -1;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }
}
