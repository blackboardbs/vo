<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerPreference;
use App\Http\Requests\CustomerPreferenceRequest;
use App\Models\Module;
use App\Models\Profession;
use App\Models\ScoutingRole;
use App\Models\Speciality;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class CustomerPreferenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_preferences = CustomerPreference::with([
            'customer:id,customer_name',
            'profession:id,name',
            'speciality:id,name',
        ])->orderBy('id')->get();

        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $customer_preferences = $customer_preferences->whereIn('updated_by_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $skills = [];
        foreach ($customer_preferences as $customer_preference) {
            $tmp_skill_ids = explode('|', $customer_preference->skill_id);
            $tmp_skills = ScoutingRole::whereIn('id', $tmp_skill_ids)->orderBy('name')->pluck('name')->toArray();
            $skills[] = implode(', ', $tmp_skills);
        }

        $parameters = [
            'customer_preferences' => $customer_preferences,
            'skills' => $skills,
            'can_update' => $can_update,
            'can_create' => $can_create,
        ];

        return view('customerpreference.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $customer_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->pluck('name', 'id');
        $skill_drop_down = ScoutingRole::orderBy('name')->pluck('name', 'id');

        $parameters = [
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skill_drop_down' => $skill_drop_down,
        ];

        return view('customerpreference.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerPreferenceRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\CustomerPreference::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $skills = '';
        if ($request->has('skill_id')) {
            foreach ($request->input('skill_id') as $key => $skill) {
                $skills .= ($key < count($request->input('skill_id')) - 1) ? $skill.'|' : $skill;
            }
        }

        $customerPreference = new CustomerPreference();
        $customerPreference->customer_id = $request->input('customer_id');
        $customerPreference->profession_id = $request->input('profession_id');
        $customerPreference->speciality_id = $request->input('speciality_id');
        $customerPreference->skill_id = $skills;
        $customerPreference->credit_check = $request->input('credit_check');
        $customerPreference->criminal_record = $request->input('criminal_record');
        $customerPreference->note = $request->input('note');
        $customerPreference->save();

        return redirect(route('customerpreference.index'))->with('flash_success', 'Customer Preference successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerPreference  $customerPreference
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $customer_preference = CustomerPreference::find($id);

        $customer_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->pluck('name', 'id');
        $skill_drop_down = ScoutingRole::orderBy('name')->pluck('name', 'id');

        $parameters = [
            'customer_preference' => $customer_preference,
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skill_drop_down' => $skill_drop_down,
        ];

        return view('customerpreference.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerPreference  $customerPreference
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $customer_preference = CustomerPreference::find($id);

        $customer_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id');
        $profession_drop_down = Profession::orderBy('name')->pluck('name', 'id');
        $speciality_drop_down = Speciality::orderBy('name')->pluck('name', 'id');
        if (isset($customer_preference->profession_id) && ($customer_preference->profession_id > 0)) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('profession_id', '=', $customer_preference->profession_id)->pluck('name', 'id');
        }
        $skill_drop_down = ScoutingRole::orderBy('name')->pluck('name', 'id');

        $parameters = [
            'customer_preference' => $customer_preference,
            'customer_drop_down' => $customer_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'skill_drop_down' => $skill_drop_down,
        ];

        return view('customerpreference.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\CustomerPreference  $customerPreference
     * @return \Illuminate\Http\Response
     */
    public function update($id, CustomerPreferenceRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $skills = '';
        if ($request->has('skill_id')) {
            foreach ($request->input('skill_id') as $key => $skill) {
                $skills .= ($key < count($request->input('skill_id')) - 1) ? $skill.'|' : $skill;
            }
        }

        $customerPreference = CustomerPreference::find($id);
        $customerPreference->customer_id = $request->input('customer_id');
        $customerPreference->profession_id = $request->input('profession_id');
        $customerPreference->speciality_id = $request->input('speciality_id');
        $customerPreference->skill_id = $skills;
        $customerPreference->credit_check = $request->input('credit_check');
        $customerPreference->criminal_record = $request->input('criminal_record');
        $customerPreference->note = $request->input('note');
        $customerPreference->save();

        return redirect(route('customerpreference.index'))->with('flash_success', 'Customer Preference successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerPreference  $customerPreference
     */
    public function destroy($id): RedirectResponse
    {
        CustomerPreference::destroy($id);

        return redirect(route('customerpreference.index'))->with('flash_success', 'Customer Preference successfully deleted.');
    }
}
