<?php

namespace App\Http\Controllers;

use App\API\Clockify\ClockifyAPI;
use App\Exports\ContactsExport;
use App\Exports\ProjectsExport;
use App\Models\Assignment;
use App\Models\Config;
use App\Models\Epic;
use App\Models\Expense;
use App\Models\Feature;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\Project;
use App\Models\Customer;
use App\Models\Company;
use App\Models\ProjectTerms;
use App\Models\ResourceViewRole;
use App\Models\Status;
use App\Models\Task;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\UserStory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $projects = Project::with([
            'assignment:project_id,hours',
            'status:id,description',
            'customer:id,customer_name'
        ])->withCount(['assignment','tasks','sprints','timesheets','invoices','vinvoices','risks','epics']);

        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        $consultant_projects = [];
        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $resource_projects = $projects;
                foreach ($resource_projects->get() as $project_) {
                    $tmp_consultants = explode(',', $project_->consultants);
                    if (in_array(Auth::id(), $tmp_consultants)) {
                        $consultant_projects[] = $project_->id;
                    }
                }

                $tmp_team_projects = Project::whereIn('manager_id', Auth::user()->team())->get();
                foreach ($tmp_team_projects as $tmp_team_project) {
                    $consultant_projects[] = $tmp_team_project->id;
                }
                $projects = $projects->whereIn('id', $consultant_projects);
                // $projects = $projects->whereIn('manager_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');



        if ($request->has('status_id') && $request->status_id != 0) {
            $projects->where('status_id', '=', $request->status_id);
        }
        if(!$request->has('status_id')){
            $projects->whereNotIn('status_id',[4,5]);
        }
        if ($request->has('active_only') && $request->input('active_only') != 0) {
            $projects->whereIn('status_id',[1,2,3]);
        }

        $resource_project_ids = [];
        if ($request->has('resource_id') && $request->resource_id != 0) {
            $tmp_projects = $projects->orderBy('id','desc');
            foreach ($tmp_projects as $tmp_project) {
                $tmp_resourct_project = explode(',', $tmp_project->consultants);
                foreach($tmp_resourct_project as $key => $value){
                    if ($request->input('resource_id') == $value) {
                        array_push($resource_project_ids,$tmp_project->id);
                    }
                }
            }

// dd(count($resource_project_ids));
            if (count($resource_project_ids) > 0) {
                $projects = $projects->whereIn('id', $resource_project_ids)->orWhereHas('assignment',function($q) use ($request){
                    $q->where('employee_id',$request->resource_id)->whereHas('project',function ($q) use ($request){
                        if ($request->has('status_id') && $request->status_id != 0) {
                            $q->where('status_id', '=', $request->status_id);
                        }
                        if(!$request->has('status_id')){
                            $q->whereNotIn('status_id',[4,5]);
                        }
                    });
                });
            } else {
                $projects = $projects->whereHas('assignment',function($q) use ($request){
                    $q->where('employee_id',$request->resource_id);
                });
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $projects->where('manager_id', Auth::id())->where('name', 'LIKE', '%'.$request->input('q').'%')
                ->orWhere('type', 'like', '%'.$request->input('q').'%')
                ->orWhere('ref', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('customer', function ($query) use ($request) {
                    $query->where('customer_name', 'like', '%'.$request->input('q').'%');
                })
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                });
        }

        if ($request->has('company_id') && $request->company_id != 0) {
            $projects->where('company_id', '=', $request->company_id);
        }

        if ($request->has('customer_id') && $request->customer_id != 0) {
            $projects->where('customer_id', '=', $request->customer_id);
        }

        if ($request->has('start_date') && $request->start_date != 0) {
            $projects->where('start_date', '>=', $request->start_date);
        }

        if ($request->has('end_date') && $request->end_date != 0) {
            $projects->where('end_date', '<=', $request->end_date);
        }



        $projects = $projects->sortable(['id' => 'desc'])->paginate($item);

        // dd($projects);

        $total_po_hours = 0;
        $total_actual = 0;
        $worked_hours = [];
        $worked_bill_hours = [];
        $worked_non_bill_hours = [];

        foreach ($projects as $project) {


            $timesheets = Timesheet::selectRaw('timeline.is_billable as billable,SUM(timeline.total + (timeline.total_m/60)) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $project->id)
                ->groupBy('timeline.is_billable')
                ->get();

            foreach($timesheets as $timesheet){
                if($timesheet->billable == '1'){
                    $worked_bill_hours[$project->id] = $timesheet->actual_hours;
                }

                if($timesheet->billable == '0'){
                    $worked_non_bill_hours[$project->id] = $timesheet->actual_hours;
                }
            }
            try {
                $project_comple_perc[$project->id] = (($project->project_type_id == 1 ? ($worked_bill_hours[$project->id] ?? 0) : ($worked_bill_hours[$project->id] ?? 0) + ($worked_non_bill_hours[$project->id]??0)) / $project->assignment->sum('hours')) * 100;
            } catch (\DivisionByZeroError $e) {
                $project_comple_perc[$project->id] = 0;
            }

            $total_po_hours += $project->assignment->sum('hours') ?? 0;
            $worked_hours[$project->id] = ($project->project_type_id == 1 ? ($worked_bill_hours[$project->id] ?? 0) : (($worked_bill_hours[$project->id] ?? 0)) + ($worked_non_bill_hours[$project->id] ?? 0));
            $total_actual += ($project->project_type_id == 1 ? $project->assignment->sum('hours') - (($worked_bill_hours[$project->id] ?? 0)*1) : $project->assignment->sum('hours') - (($worked_bill_hours[$project->id] ?? 0)*1 + ($worked_non_bill_hours[$project->id] ?? 0)*1));
        }

        try {
            $total_percentage = (array_sum($worked_hours) / $total_po_hours)*100;
        } catch (\DivisionByZeroError $e) {
            $total_percentage = 0;
        }

        $parameters = [
            'projects' => $projects,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'can_create' => $can_create,
            'can_update' => $can_update,
            'module' => $module,
            'percentage' => $project_comple_perc ?? 0,
            'worked_bill_hours' => $worked_bill_hours,
            'worked_non_bill_hours' => $worked_non_bill_hours,
            'total_po_hours' => $total_po_hours ?? 0,
            'total_actual' => $total_actual ?? 0,
            'total_percentage' => $total_percentage,
        ];

        if ($request->has('export')) return $this->export($parameters, 'project');

        return view('project.index', $parameters);
    }

    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        $consultant_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([7])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Consultant', '0');

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            if (! Auth::user()->canAccess($module->id, 'create_all') && Auth::user()->canAccess($module->id, 'create_team')) {
                $consultant_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([7])->whereIn('id', Auth::user()->team())->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Consultant', '0');
            }
        } else {
            return abort(403);
        }

        $config = Config::first();
        $roles_drop_down = ModuleRole::orderBy('id')->where('module_id', 30)->get(['name', 'id']);

        $parameters = [
            'terms' => ProjectTerms::find($config->project_terms_id),
            'roles_drop_down'=>$roles_drop_down,
            'config' => $config,
        ];

        return view('project.create')->with($parameters);
    }

    public function store(StoreProjectRequest $request, ClockifyAPI $clockify)
    {
        $project = new Project;
        $project->name = $request->input('name');
        $project->ref = $request->input('ref');
        $project->type = $request->input('project_type_id');
        $project->customer_id = $request->input('customer_id');
        $project->customer_po = $request->input('customer_po');
        $project->quotation_id = $request->input('quotation_id');
        $project->customer_ref = $request->input('customer_ref');
        $project->project_type_id = $request->input('project_type_id');
        $project->billing_cycle_id = $request->input('billing_cycle_id');
        $project->total_project_hours = $request->input('total_project_hours');
        $project->consultants = '';
        if ($request->has('consultants')) {
            foreach ($request->input('consultants') as $consultant) {
                if ($project->consultants == '') {
                    $project->consultants = $consultant;
                } else {
                    $project->consultants .= ','.$consultant;
                }
            }
        }
        $project->manager_id = $request->input('manager_id');
        $project->assignment_approver_id = $request->input('assignment_approver_id');
        $project->start_date = $request->input('start_date');
        $project->end_date = $request->input('end_date');
        $project->est_labour_cost = $request->input('est_labour_cost');
        $project->est_other_cost = $request->input('est_other_cost');

        $project->billable = $request->input('billable');
        $project->billing_note = $request->input('billing_note');
        $project->status_id = $request->input('status_id');
        $project->creator_id = $request->input('creator_id');
        $project->company_id = $request->input('company_id');
        $project->opex_project = $request->input('opex_project');
        $project->billing_note = $request->input('billing_note');
        $project->location = $request->input('location');
        $project->hours_of_work = $request->input('hours_of_work');
        $project->customer_po = $request->input('cust_order_number');
        $project->timesheet_template_id = $request->input('timesheet_template_id');
        $project->terms_version = $request->input('terms_version');

        $expense = new Expense();
        $expense->travel = $request->input('travel');
        $expense->parking = $request->input('parking');
        $expense->car_rental = $request->input('car_rental');
        $expense->flights = $request->input('flights');
        $expense->toll = $request->input('toll');
        $expense->other = $request->input('other');
        $expense->accommodation = $request->input('accommodation');
        $expense->out_of_town = $request->input('out_of_town');
        $expense->per_diem = $request->input('per_diem');
        $expense->data = $request->input('data');

        $expense->save();

        $project->exp_id = $expense->id;

        $project->is_fixed_price = $request->input('is_fixed_price');
        $project->fixed_price_labour = $request->input('fixed_price_labour');
        $project->fixed_price_expenses = $request->input('fixed_price_expenses');
        $project->total_fixed_price = $request->input('total_fixed_price');
        $project->project_fixed_cost_labour = $request->input('project_fixed_cost_labour');
        $project->project_fixed_cost_expense = $request->input('project_fixed_cost_expense');
        $project->project_total_fixed_cost = $request->input('project_total_fixed_cost');

        $project->customer_invoice_contact_id = $request->input('customer_invoice_contact_id');
        $project->vendor_invoice_contact_id = $request->input('vendor_invoice_contact_id');

        $project->billing_timesheet_templete_id = $request->input('billing_timesheet_templete_id');
        $project->scope = $request->input('scope');

        $project->save();

        /*--------Create an Epic - Start--------------*/
        $epic = new Epic();
        $epic->name = $project->name;
        $epic->project_id = $project->id;
        $epic->start_date = $project->start_date;
        $epic->end_date = $project->end_date;
        $epic->hours_planned = null;
        $epic->confidence_percentage = null;
        $epic->estimate_cost = null;
        $epic->estimate_income = null;
        $epic->billable = null;
        $epic->sprint_id = null;
        $epic->note = null;
        $epic->status_id = 1;
        $epic->save();
        /*--------Create an Feature - End--------------*/

        /*--------Create an Feature - Start--------------*/
        $feature = new Feature();
        $feature->name = $epic->name;
        $feature->epic_id = $epic->id;
        $feature->start_date = $epic->start_date;
        $feature->end_date = $epic->end_date;
        $feature->hours_planned = null;
        $feature->confidence_percentage = null;
        $feature->estimate_cost = null;
        $feature->estimate_income = null;
        $feature->billable = null;
        $feature->sprint_id = null;
        $feature->dependency_id = null;
        $feature->status_id = 1;
        $feature->save();
        /*--------Create an Epic - End--------------*/

        /*--------Create an User Story - Start--------------*/
        $userStory = new UserStory();
        $userStory->name = $feature->name;
        $userStory->feature_id = $feature->id;
        $userStory->start_date = $feature->start_date;
        $userStory->end_date = $feature->end_date;
        $userStory->hours_planned = null;
        $userStory->confidence_percentage = null;
        $userStory->estimate_cost = null;
        $userStory->estimate_income = null;
        $userStory->billable = null;
        $userStory->sprint_id = null;
        $userStory->dependency_id = null;
        $userStory->status_id = 1;
        $userStory->save();
        /*--------Create an User Story - End--------------*/

        /*-------Add Resource roles - Start --------------*/
        if ($request->has('consultants')) {
            foreach ($request->input('consultants') as $consultant) {
                $_resource_view_role = ResourceViewRole::where('resource_id', $consultant)
                    ->where('module_id', 30)->where('module_item_id', $project->id)->first();

                if (! isset($_resource_view_role)) {
                    $resource_view_role = new ResourceViewRole();
                } else {
                    $resource_view_role = ResourceViewRole::find($_resource_view_role->id);
                }
                $resource_view_role->module_id = 30;
                $resource_view_role->resource_id = $consultant;
                $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
                $resource_view_role->role_id = $request['consultant_role_'.$consultant];
                $resource_view_role->save();
            }
        }

        $resource_view_role = new ResourceViewRole();
        $resource_view_role->module_id = 30;
        $resource_view_role->resource_id = Auth::user()->id;
        $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
        $resource_view_role->role_id = 1;
        $resource_view_role->save();
        /*-------Add Resource roles - End ----------------*/

        $feedback = [
            'feedback' => 'Project captured successfully',
            'feedback_type' => 'flash_success',
        ];

        /*--------Save a project to Clockify - Start--------------*/

        if (env('CLOCKIFY_ENABLED', false)){
            $project = $project->refresh()->load(['customer' => function ($customer) {
                return $customer->select(['id', 'customer_name']);
            }]);

            $clockify_customer_id = $clockify->singleClient($project->customer->customer_name ?? '')[0]->id ?? '';

            $clockify_projects = $clockify->formatProject(collect($clockify->projects()), false);

            $local_project = [
                'name' => $project->name,
                'clientId' => $clockify_customer_id,
                'note' => $project->id,
                'isPublic' => false,
            ];

            try {
                if (! in_array($local_project, $clockify_projects) && config('app.clockify.is_enabled')) {
                    $clockify->postEndPoint(
                        config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').'/projects',
                        $local_project
                    );
                }
            } catch (\Exception $e) {
                $feedback = [
                    'feedback' => 'Project captured successfully but failed to push to clockify',
                    'feedback_type' => 'flash_warning',
                ];
                logger($e->getMessage());
            }
        }

        /*--------Save a project to Clockify - End--------------*/

        activity()->on($project)->log('created');

        return redirect(route('project.show', $project->id))->with($feedback['feedback_type'], $feedback['feedback']);
    }

    public function show(Project $project)
    {
        $module = Module::where('name', '=', 'App\Models\Project')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'show_all') || Auth::user()->canAccess($module->id, 'show_team'), 403);

        $project->load([
            'status:id,description',
            'customer:id,customer_name',
            'company:id,company_name',
            'project_type:id,description',
            'manager:id,first_name,last_name',
            'defaultBillingTemplate:id,name',
            'customerInvoiceContact:id,first_name,last_name',
            'vendorInvoiceContact:id,first_name,last_name',
            'billingCycle:id,name',
            'expense',
            'epics' => fn($epic) => $epic->with([
                'sprint:id,name',
                'status:id,description',
                'features' => fn($feature) => $feature->select(['id','name', 'epic_id', 'sprint_id', 'hours_planned', 'status_id'])->with([
                    'sprint:id,name',
                    'status:id,description',
                    'user_stories' => fn($userStory) => $userStory->select(['id','name', 'feature_id', 'sprint_id', 'hours_planned', 'status_id'])->with([
                        'sprint:id,name',
                        'status:id,description',
                        'tasks' => fn($task) => $task->select(['id', 'description','user_story_id', 'employee_id', 'start_date', 'end_date', 'sprint_id', 'hours_planned', 'status'])->with([
                            'consultant:id,first_name,last_name',
                            'bugTask:id,task_id',
                            'sprint:id,name',
                            'taskstatus:id,description'
                        ])
                    ])
                ])
            ]),
            'epics.features',
            'risks' => fn($risk) => $risk->with(['riskarea:id,name', 'status:id,description']),
            'actions' => fn($action) => $action->with(['assigneduser','status']),
            'tasks' => fn($task) => $task->with([
                'timelines',
                'bugTask:id,task_id',
                'consultant:id,first_name,last_name',
                'sprint:id,name',
                'taskstatus:id,description'
            ])->withSum('timelines', 'total')->withSum('timelines', 'total_m'),
            'sprints.status:id,description',
            'invoices' => fn($invoice) => $invoice->select(['id', 'project_id'])->with(['nonTimesheetCustomerInvoices:id,invoice_id,invoice_type_id,price_excl', 'nonTimesheetVendorInvoices:id,invoice_id,invoice_type_id,price_excl']),
            'assignment' => fn($assignment) => $assignment
                ->selectRaw("(select role_id from resource_view_roles where resource_view_roles.resource_id = assignment.employee_id and resource_view_roles.module_id = 30 and resource_view_roles.module_item_id = assignment.project_id group by role_id limit 1) as 'roled_id',assignment.id, assignment.project_id, assignment.employee_id, assignment.role, assignment.function, assignment.hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 0),0.0) AS actual_non_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 1),0.00) AS actual_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 1 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_bill_hours,IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 0 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_non_bill_hours, assignment.start_date, assignment.end_date, assignment.assignment_status, assignment.invoice_rate, IF(assignment.internal_cost_rate = 0.00, assignment.external_cost_rate, assignment.internal_cost_rate) AS cost_rate, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.billable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.is_billable = 1) AS billable_expense, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.claimable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.claim = 1) AS claimable_expense, assignment.external_cost_rate, assignment.internal_cost_rate")
                ->where('employee_id', '>', 0)
                ->with([
                    'function_desc:id,description',
                    'project_status:id,description',
                    'consultant:id,first_name,last_name'
                ])

        ]);


        $total_hours = $project->assignment?->sum('hours');
        // dd($total_hours);
        $project_po_value = $project->assignment?->sum(fn($assignment) => $assignment->hours * $assignment->invoice_rate);
        $planned_cost_labour = $project->assignment?->sum(fn($assignment) => $assignment->hours * ($assignment->internal_cost_rate??$assignment->external_cost_rate));
        $actual_hours = $project->actualHours($project->assignment?->sum('actual_bill_hours'), $project->assignment?->sum('actual_non_bill_hours'));
        $difference = $total_hours - $actual_hours;
        $completed_percentage = round($this->totalPercentage($actual_hours,$total_hours)*100);
        $actual_billable_labour_income = $project->assignment?->sum(fn($assignment) => $assignment->actual_bill_hours * $assignment->invoice_rate);
        $actual_non_billable_labour_income = $project->assignment?->sum(fn($assignment) => $assignment->actual_non_bill_hours * $assignment->invoice_rate);
        $actual_billable_expenses =  $project->assignment?->sum('billable_expense');
        $actual_claimable_expenses =  $project->assignment?->sum('claimable_expense');
        $internal_labour_cost = $project->assignment?->sum(fn($assignment) =>$assignment->actual_bill_hours * $assignment->internal_cost_rate);
        $external_labour_cost = $project->assignment?->sum(fn($assignment) => $assignment->vendor_bill_hours * $assignment->external_cost_rate);
        $non_timesheet_invoice = $project->invoices?->sum(fn($invoice) => $invoice->nonTimesheetCustomerInvoices?->sum('price_excl'));
        $non_timesheet_expense = $project->invoices?->sum(fn($invoice) => $invoice->nonTimesheetVendorInvoices?->sum('price_excl'));
        $total_income = $actual_billable_labour_income + $actual_billable_expenses + $non_timesheet_invoice;
        $total_expenses = $actual_claimable_expenses + $non_timesheet_expense + $internal_labour_cost + $external_labour_cost;
        $profit_or_loss = $total_income - $total_expenses;
        $profit_or_loss_percentage = $this->totalPercentage($profit_or_loss,$total_income) * 100;

        $consultants = explode(',', $project->consultants);

        $consultant_role_user_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants)->orderBy('first_name')->orderBy('last_name')->get(['full_name', 'id']);
        $roles_drop_down = ModuleRole::orderBy('id')->where('module_id', 30)->get(['name', 'id']);

        $features = [];
        $user_stories = [];
        foreach($project->epics as $epic){
            $query = Feature::with(['status:id,description'])->where('epic_id',$epic->id)->get();

            foreach($query as $result){
                array_push($features,$result);
            }
        }

        foreach($features as $feature){
            $query = UserStory::with(['status:id,description'])->where('feature_id',$feature->id)->get();

            foreach($query as $result){
                array_push($user_stories,$result);
            }
        }

        // $gantt_chart = Epic::with('features.user_stories.tasks')
        //     ->whereProjectId($project->id)
        //     ->get()->map(function ($epic) {
        //         return [
        //             'epic_id' => $epic->id,
        //             'epic_name' => $epic->name,
        //             'epic_start_date' => Carbon::parse($epic->start_date),
        //             'epic_end_date' => Carbon::parse($epic->end_date),
        //             'features' => $epic->features->map(function ($feature) {
        //                 return [
        //                     'feature_id' => $feature->id,
        //                     'feature_name' => $feature->name,
        //                     'feature_start_date' => Carbon::parse($feature->start_date),
        //                     'feature_end_date' => Carbon::parse($feature->end_date),
        //                     'user_stories' => $feature->user_stories->map(function ($story) {
        //                         return $story->tasks->map(function ($task) {
        //                             return [
        //                                 'task_id' => $task->id,
        //                                 'task_name' => $task->description,
        //                                 'task_start_date' => Carbon::parse($task->start_date),
        //                                 'task_end_date' => Carbon::parse($task->end_date),
        //                                 'consultant' => substr($task->consultant->first_name ?? '', 0, 1).'. '.$task->consultant->last_name ?? null,
        //                                 'planned_hours' => $task->hours_planned,
        //                                 'status' => $task->taskstatus->description ?? null,
        //                             ];
        //                         });
        //                     }),
        //                 ];
        //             }),
        //         ];
        //     });

        // $gantt_chart_end_date = '0000-00-00';
        // $gantt_chart_start_date = $gantt_chart[0]['epic_start_date'] ?? '0000-00-00';

        // foreach ($gantt_chart as $chart) {
        //     if ($chart['epic_end_date'] > $gantt_chart_end_date) {
        //         $gantt_chart_end_date = $chart['epic_end_date'];
        //     }

        //     if ($chart['epic_start_date'] < $gantt_chart_start_date) {
        //         $gantt_chart_start_date = $chart['epic_start_date'];
        //     }
        // }

        // $totalHrsAssigned =

        // foreach($project->assignment as $value){
        //     dd($value);
        // }

        // dd($project->tasks);

        $parameters = [
            'project' => $project,
            // 'projects_drop_down' => Project::pluck('name','id'),
            // 'user_story_drop_down' => [],
            // 'task_delivery_type_drop_down' => [],
            // 'customer_drop_down' => [],
            // 'dependency_drop_down' => [],
            // 'permissions_drop_down' => [],
            // 'sprints_drop_down' => [],
            'epics' => $project->epics,
            'features' => $features,
            'user_stories' => $user_stories,
            // 'gantt_chart' => $gantt_chart,
            // 'gantt_start_date' => Carbon::parse($gantt_chart_start_date),
            // 'gantt_last_date' => Carbon::parse($gantt_chart_end_date),
            'project_po_value' => number_format($project_po_value, 2, '.', ','),
            'planned_cost_labour' => number_format($planned_cost_labour, 2,'.',','),
            'total_actual_hours' => number_format($actual_hours,2,'.',','),
            'difference' => number_format($difference,2,'.',','),
            'completed' => $completed_percentage.'%',
            'actual_bill_labour_income' => number_format($actual_billable_labour_income,2,'.',','),
            'actual_non_billable_labour_income' => number_format($actual_non_billable_labour_income,2,'.',','),
            'actual_billable_expenses' => number_format($actual_billable_expenses,2,'.',','),
            'actual_claimable_expenses' => number_format($actual_claimable_expenses,2,'.',','),
            'non_timesheet_invoice' => number_format($non_timesheet_invoice,2,'.',','),
            'internal_labour_cost' => number_format($internal_labour_cost,2,'.',','),
            'external_labour_cost' => number_format($external_labour_cost,2,'.',','),
            'total_income' => number_format($total_income,2,'.',','),
            'total_expenses' => number_format($total_expenses,2,'.',','),
            'non_timesheet_expense' => number_format($non_timesheet_expense,2,'.',','),
            'profit_or_loss' => number_format($profit_or_loss,2,'.',','),
            'profit_or_loss_percentage' => round($profit_or_loss_percentage),
            'permissions' => $project->projectPermissions(),
            'consultant_role_user_drop_down' => $consultant_role_user_drop_down,
            'roles_drop_down' => $roles_drop_down,
            'can_view_costs' => \auth()->user()->hasRole('admin') || ($project->canView(1) == 'View') || ($project->canView(1) == 'Create') || ($project->canView(1) == 'Update') || ($project->canView(1) == 'Full'),
            'can_view_assignment' => \auth()->user()->hasRole('admin') || ($project->canView(2) == 'View') || ($project->canView(2) == 'Create') || ($project->canView(2) == 'Update') || ($project->canView(2) == 'Full'),
            'can_view_expenses' => \auth()->user()->hasRole('admin') || ($project->canView(4) == 'View') || ($project->canView(4) == 'Create') || ($project->canView(4) == 'Update') || ($project->canView(4) == 'Full'),
            'can_view_tasks' => \auth()->user()->hasRole('admin') || ($project->canView(6) == 'View') || ($project->canView(6) == 'Create') || ($project->canView(6) == 'Update') || ($project->canView(6) == 'Full'),
            'can_view_analysis' => \auth()->user()->hasRole('admin') || ($project->canView(1) == 'View') || ($project->canView(1) == 'Create') || ($project->canView(1) == 'Update') || ($project->canView(1) == 'Full'),
            'total_hours' => number_format($total_hours,2,'.',',')
        ];

        return view('project.show')->with($parameters);
    }

    public function edit($projectid)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            if (! Auth::user()->canAccess($module->id, 'update_all') && Auth::user()->canAccess($module->id, 'update_team')) {
                $consultant_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([7, 9])->whereIn('id', Auth::user()->team())->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Consultant', '0');
            }
        } else {
            return abort(403);
        }

        $project = Project::find($projectid);

        $project->load(['status:id,description', 'customer:id,customer_name', 'company:id,company_name','assignment' => fn($assignment) => $assignment
            ->selectRaw("(select role_id from resource_view_roles where resource_view_roles.resource_id = assignment.employee_id and resource_view_roles.module_id = 30 and resource_view_roles.module_item_id = assignment.project_id group by role_id limit 1) as 'roled_id',assignment.id, assignment.project_id, assignment.employee_id, assignment.role, assignment.function, assignment.hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 0),0.0) AS actual_non_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 1),0.00) AS actual_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 1 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_bill_hours,IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 0 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_non_bill_hours, assignment.start_date, assignment.end_date, assignment.assignment_status, assignment.invoice_rate, IF(assignment.internal_cost_rate = 0.00, assignment.external_cost_rate, assignment.internal_cost_rate) AS cost_rate, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.billable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.is_billable = 1) AS billable_expense, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.claimable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.claim = 1) AS claimable_expense, assignment.external_cost_rate, assignment.internal_cost_rate")
            ->where('employee_id', '>', 0)
            ->with([
                'function_desc:id,description',
                'project_status:id,description',
                'consultant:id,first_name,last_name'
            ])]);
        $consultants = explode(',', $project->consultants);
// dd($project);
        $consultant_role_user_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please select...', '0');
        $consultant_role_users = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants)->orderBy('first_name')->orderBy('last_name')->get();
        $roles_drop_down = ModuleRole::orderBy('id')->where('module_id', 30)->get(['name', 'id']);

        $consultants_2 = [];
        foreach ($consultants as $consultant) {
            $consultants_2[] = (int) $consultant;
        }

        $parameters = [
            'wbs_nr' => $project->id,
            'project' => $project,
            'consultants' => $consultants_2,
            'expense' => Expense::find($project->exp_id),
            'status_drop_down' => Status::where('status', '=', 1)->orderBy('id')->orderBy('order')->get(['description', 'id']),
            'consultant_role_user_drop_down' => $consultant_role_user_drop_down,
            'roles_drop_down' => $roles_drop_down,
            'consultant_role_users' => $consultant_role_users,
            'consultants' => $consultants,
        ];

        return view('project.edit')->with($parameters);
    }

    public function update(UpdateProjectRequest $request, $projectid)
    {
        $project = Project::find($projectid);
        $project->name = $request->input('name');
        $project->ref = $request->input('ref');
        $project->type = $request->input('project_type_id');
        $project->customer_id = $request->input('customer_id');
        $project->customer_po = $request->input('customer_po');
        $project->quotation_id = $request->input('quotation_id');
        $project->customer_ref = $request->input('customer_ref');
        $project->manager_id = $request->input('manager_id');
        $project->project_type_id = $request->input('project_type_id');
        $project->billing_cycle_id = $request->input('billing_cycle_id');
        $project->total_project_hours = $request->input('total_project_hours');
        $old_consultants = $project->consultants !== "" ? explode(',', $project->consultants):[];

        $project->consultants = '';
        if ($request->has('consultants')) {
            foreach ($request->input('consultants') as $consultant) {
                if ($project->consultants == '') {
                    $project->consultants = $consultant;
                } else {
                    $project->consultants .= ','.$consultant;
                }

                $_resource_view_role = ResourceViewRole::where('resource_id', $consultant)->where('module_item_id', $project->id)->where('module_id', 30)->first();
                // dd($_resource_view_role->id);
                if (! isset($_resource_view_role)) {
                    $resource_view_role = new ResourceViewRole();
                } else {
                    $resource_view_role = ResourceViewRole::find($_resource_view_role->id);
                }
                $resource_view_role->module_id = 30;
                $resource_view_role->resource_id = $consultant;
                $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
                $resource_view_role->role_id = ($request->input('consultant_role_'.$consultant) !== "null") ? $request['consultant_role_'.$consultant] : 2;
                $resource_view_role->save();

            }
        }

        foreach ($old_consultants as $old_consultant) {
            if($request->has('consultants')){
                if (! in_array($old_consultant, $request->input('consultants'))) {
                    $tmp_assignment = Assignment::where('project_id', '=', $project->id)->where('employee_id', '=', $old_consultant)->first();

                    if (! empty($tmp_assignment)) {
                        $remove_assignment = Assignment::find($tmp_assignment->id);
                        $remove_assignment->status = 5;
                        $remove_assignment->save();
                    }
                }
            }
        }

        $project->assignment_approver_id = $request->input('assignment_approver_id');
        $project->start_date = $request->input('start_date');
        $project->end_date = $request->input('end_date');
        $project->est_labour_cost = $request->input('est_labour_cost');
        $project->est_other_cost = $request->input('est_other_cost');

        $project->billable = $request->input('billable');
        $project->billing_note = $request->input('billing_note');
        $project->status_id = $request->input('status_id');
        $project->creator_id = $request->input('creator_id');
        $project->company_id = $request->input('company_id');
        $project->opex_project = $request->input('opex_project');
        $project->billing_note = $request->input('billing_note');
        $project->location = $request->input('location');
        $project->hours_of_work = $request->input('hours_of_work');
        $project->customer_po = $request->input('cust_order_number');
        $project->timesheet_template_id = $request->input('timesheet_template_id');
        $project->terms_version = $request->input('terms_version');

        $expense = Expense::find($project->exp_id);
        if ($expense != null) {
            $expense->travel = $request->input('travel');
            $expense->parking = $request->input('parking');
            $expense->car_rental = $request->input('car_rental');
            $expense->flights = $request->input('flights');
            $expense->toll = $request->input('toll');
            $expense->other = $request->input('other');
            $expense->accommodation = $request->input('accommodation');
            $expense->out_of_town = $request->input('out_of_town');
            $expense->per_diem = $request->input('per_diem');
            $expense->data = $request->input('data');
            $expense->save();
        }

        $project->is_fixed_price = $request->input('is_fixed_price');
        $project->fixed_price_labour = $request->input('fixed_price_labour');
        $project->fixed_price_expenses = $request->input('fixed_price_expenses');
        $project->total_fixed_price = $request->input('total_fixed_price');
        $project->project_fixed_cost_labour = $request->input('project_fixed_cost_labour');
        $project->project_fixed_cost_expense = $request->input('project_fixed_cost_expense');
        $project->project_total_fixed_cost = $request->input('project_total_fixed_cost');

        $project->customer_invoice_contact_id = $request->input('customer_invoice_contact_id');
        $project->vendor_invoice_contact_id = $request->input('vendor_invoice_contact_id');

        $project->billing_timesheet_templete_id = $request->input('billing_timesheet_templete_id');
        $project->scope = $request->input('scope');

        $project->save();

        activity()->on($project)->log('updated');

        return redirect(route('project.show', $projectid))->with('flash_success', 'Project updated successfully');
    }

    public function destroy($id)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $project = Project::find($id);
        DB::table('projects')->delete($id);
        Project::destroy($id);
        activity()->on($project)->withProperties(['name' => auth()->user()->first_name.' '.auth()->user()->last_name.' deleted '.$project->name])->log('deleted');

        return redirect()->route('project.index')->with('flash_success', 'project deleted successfully');
    }

    public function copy($project_id)
    {
        $module = Module::where('name', '=', \App\Models\Project::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $new_project = new Project;
        $project = Project::find($project_id);
        $new_project->name = $project->name.' - Copy';
        $new_project->ref = $project->ref;
        $new_project->type = $project->type;
        $new_project->customer_id = $project->customer_id;
        $new_project->customer_po = $project->customer_po;
        $new_project->project_type_id = $project->project_type_id;
        $new_project->quotation_id = $project->quotation_id;
        $new_project->customer_ref = $project->customer_ref;
        $new_project->consultants = $project->consultants;
        $new_project->manager_id = $project->manager_id;
        $new_project->assignment_approver_id = $project->assignment_approver_id;
        $new_project->start_date = $project->start_date;
        $new_project->end_date = $project->end_date;
        $new_project->est_labour_cost = $project->est_labour_cost;
        $new_project->est_other_cost = $project->est_other_cost;
        $new_project->billable = $project->billable;
        $new_project->billing_note = $project->billing_note;
        $new_project->status_id = $project->status_id;
        $new_project->creator_id = $project->creator_id;
        $new_project->company_id = $project->company_id;
        $new_project->is_vendor = $project->is_vendor;
        $new_project->vendor_billing_note = $project->vendor_billing_note;
        $new_project->location = $project->location;
        $new_project->hours_of_work = $project->hours_of_work;
        $new_project->site_id = $project->site_id;
        $new_project->billing_timesheet_templete_id = $project->billing_timesheet_templete_id;
        $new_project->created_at = $project->created_at;
        $new_project->updated_at = $project->updated_at;

        $expense = Expense::find($project->exp_id);
        $new_expense = new Expense();
        $new_expense->travel = $expense->travel;
        $new_expense->parking = $expense->parking;
        $new_expense->other = $expense->other;
        $new_expense->accommodation = $expense->accommodation;
        $new_expense->out_of_town = $expense->out_of_town;
        $new_expense->per_diem = $expense->per_diem;
        $new_expense->data = $expense->data;
        $new_expense->save();

        $new_project->exp_id = $new_expense->id;
        $new_project->save();

        /*--------Create an Epic - Start--------------*/
        $epic = new Epic();
        $epic->name = $new_project->name;
        $epic->project_id = $new_project->id;
        $epic->start_date = $new_project->start_date;
        $epic->end_date = $new_project->end_date;
        $epic->hours_planned = null;
        $epic->confidence_percentage = null;
        $epic->estimate_cost = null;
        $epic->estimate_income = null;
        $epic->billable = null;
        $epic->sprint_id = null;
        $epic->note = null;
        $epic->status_id = 1;
        $epic->save();
        /*--------Create an Feature - End--------------*/

        /*--------Create an Feature - Start--------------*/
        $feature = new Feature();
        $feature->name = $epic->name;
        $feature->epic_id = $epic->id;
        $feature->start_date = $epic->start_date;
        $feature->end_date = $epic->end_date;
        $feature->hours_planned = null;
        $feature->confidence_percentage = null;
        $feature->estimate_cost = null;
        $feature->estimate_income = null;
        $feature->billable = null;
        $feature->sprint_id = null;
        $feature->dependency_id = null;
        $feature->status_id = 1;
        $feature->save();
        /*--------Create an Epic - End--------------*/

        /*--------Create an User Story - Start--------------*/
        $userStory = new UserStory();
        $userStory->name = $feature->name;
        $userStory->feature_id = $feature->id;
        $userStory->start_date = $feature->start_date;
        $userStory->end_date = $feature->end_date;
        $userStory->hours_planned = null;
        $userStory->confidence_percentage = null;
        $userStory->estimate_cost = null;
        $userStory->estimate_income = null;
        $userStory->billable = null;
        $userStory->sprint_id = null;
        $userStory->dependency_id = null;
        $userStory->status_id = 1;
        $userStory->save();
        /*--------Create an User Story - End--------------*/

        foreach (explode(',', $new_project->consultants) as $consultant) {

            $resource_view_role = ResourceViewRole::where('resource_id', $consultant)->where('module_item_id', $new_project->id)->where('module_id', 30)->first();
            if($resource_view_role){
                $resource_view_role->module_item_id = $new_project->id; // ProjectID, CustomerID, etc
                $resource_view_role->save();

            }
        }

        activity()->on($new_project)->log('copied');

        return redirect(route('project.edit', $new_project->id))->with('flash_success', 'Project successfully copied');
    }

    public function complete(Project $project)
    {
        $project->status_id = 4;
        $project->save();
        $assignments = Assignment::where('project_id', $project->id)->whereNotIn('assignment_status', [4, 5])->get();

        foreach ($assignments as $assignment){
            $assignment->assignment_status = 4;
            $assignment->save();
        }

        $tasks = Task::where('project_id', $project->id)->get();

        foreach ($tasks as $task){
            $task->status = 5;
            $task->save();
        }

        return back()->with('flash_success', 'Project completed successfully');
    }

    public function getTerms(Request $request): JsonResponse
    {
        $terms = ProjectTerms::find($request->input('terms_id'));

        return response()->json(['success' => 'Terms found.', 'terms' => $terms]);
    }

    public function setDefaultProjectRoles()
    {
        $projects = Project::orderBy('id')->get();

        $status = [];
        $counter = 0;
        foreach ($projects as $project) {
            $counter++;
            $assigned_consultants = explode(',', $project->consultants);
            $is_set_flag = false;
            foreach ($assigned_consultants as $assigned_consultant) {
                /*-------Add Resource roles - Start --------------*/
                $_resource_view_role = ResourceViewRole::where('resource_id', $assigned_consultant)
                    ->where('module_id', 30)
                    ->where('module_item_id', $project->id)
                    ->first();

                if (! isset($_resource_view_role)) {
                    $resource_view_role = new ResourceViewRole();
                    $resource_view_role->module_id = 30;
                    $resource_view_role->resource_id = $assigned_consultant;
                    $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
                    $resource_view_role->role_id = 2;
                    $resource_view_role->save();
                    $is_set_flag = true;
                }
                /*-------Add Resource roles - End ----------------*/
            }
            if ($is_set_flag) {
                $status[] = 'Set for project number: '.$project->id.', For '.count($assigned_consultants).' resources';
            }
        }
        dd($status);
    }

    private function getPercentage($projects)
    {
        $project_percentage = [];
        foreach ($projects as $project) {
            try {
                $total_actual = $project->timesheets->sum(function ($timesheet) {
                    return $timesheet->timeline->sum('total') + ($timesheet->timeline->sum('total_m') / 60);
                });
                logger($total_actual);
                $project_percentage[$project->id] = ($project->assignment->sum('hours') > 0 ? number_format(($total_actual / $project->assignment->sum('hours')) * 100,2) : 0);
            } catch (\ErrorException $e) {
                $project_percentage[$project->id] = 0;
            }
        }

        return $project_percentage;
    }

    private function totalPOHours($projects)
    {
        return $projects->sum(function ($project) {
            return $project->assignment->sum('hours');
        });
    }

    private function totalActualHours($projects)
    {
        return $projects->sum(function ($project) {
            return $project->timesheets->sum(function ($timesheet) {
                return number_format($timesheet->timeline->sum('total') + ($timesheet->timeline->sum('total_m') / 60));
            });
        });
    }

    private function totalPercentage($actual, $hours)
    {
        try {
            return ($actual / $hours);
        } catch (\DivisionByZeroError $e) {
            return 0;
        }
    }

    public function updateProjectConsultant(Request $request){
        $project = Project::find($request->project_id);
        $old_consultants = $project->consultants !== "" ? explode(',', $project->consultants):[];

        $new_consultants = [];
        $new_consultant = [];
        $project->consultants = '';
        if ($request->has('consultants')) {
            foreach ($request->input('consultants') as $key => $consultant) {
                if ($project->consultants == '') {
                    $project->consultants = $consultant['emp_id'];
                } else {
                    $project->consultants .= ','.$consultant['emp_id'];
                }

                $_resource_view_role = ResourceViewRole::where('resource_id', $consultant['emp_id'])->where('module_item_id', $project->id)->where('module_id', 30)->orderBy('updated_at','desc')->first();
                // dd($_resource_view_role->id);
                if (! isset($_resource_view_role)) {
                    $resource_view_role = new ResourceViewRole();
                } else {
                    $resource_view_role = ResourceViewRole::find($_resource_view_role->id);
                }
                $resource_view_role->module_id = 30;
                $resource_view_role->resource_id = $consultant['emp_id'];
                $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
                $resource_view_role->role_id = $consultant['role'] ?? 2;
                $resource_view_role->save();

            }
        }

        $project->save();


        $project->load(['assignment' => fn($assignment) => $assignment
            ->selectRaw("(select role_id from resource_view_roles where resource_view_roles.resource_id = assignment.employee_id and resource_view_roles.module_id = 30 and resource_view_roles.module_item_id = assignment.project_id group by role_id limit 1) as 'roled_id',assignment.id, assignment.project_id, assignment.employee_id, assignment.role, assignment.function, assignment.hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 0),0.0) AS actual_non_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 1),0.00) AS actual_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 1 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_bill_hours,IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 0 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_non_bill_hours, assignment.start_date, assignment.end_date, assignment.assignment_status, assignment.invoice_rate, IF(assignment.internal_cost_rate = 0.00, assignment.external_cost_rate, assignment.internal_cost_rate) AS cost_rate, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.billable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.is_billable = 1) AS billable_expense, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.claimable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.claim = 1) AS claimable_expense, assignment.external_cost_rate, assignment.internal_cost_rate")
            ->where('employee_id', '>', 0)
            ->with([
                'function_desc:id,description',
                'project_status:id,description',
                'consultant:id,first_name,last_name'
            ])

        ]);

        $projectAssignments= $project->assignment;
        $unassignedConsultants = $project->consultantsWithoutAssignment($project->assignment);

        return response()->json(['assignments'=>$projectAssignments,'nonassignments'=>$unassignedConsultants]);
    }

    public function addProjectConsultant(Request $request){
        $project = Project::find($request->project_id);
        $old_consultants = $project->consultants !== "" ? explode(',', $project->consultants):[];

        $new_consultants = [];
        $new_consultant = [];
        $project->consultants = '';
        if ($request->has('consultants')) {
            foreach ($request->input('consultants') as $key => $consultant) {
                array_push($new_consultants,$consultant['emp_id']);
                $new_consultant['emp_id'] = $consultant['emp_id'];
                if ($project->consultants == '') {
                    $project->consultants = $consultant['emp_id'];
                } else {
                    $project->consultants .= ','.$consultant['emp_id'];
                }

                $_resource_view_role = ResourceViewRole::where('resource_id', $consultant['emp_id'])->where('module_item_id', $project->id)->where('module_id', 30)->first();
                // dd($_resource_view_role->id);
                if (! isset($_resource_view_role)) {
                    $resource_view_role = new ResourceViewRole();
                } else {
                    $resource_view_role = ResourceViewRole::find($_resource_view_role->id);
                }
                $resource_view_role->module_id = 30;
                $resource_view_role->resource_id = $consultant['emp_id'];
                $resource_view_role->module_item_id = $project->id; // ProjectID, CustomerID, etc
                $resource_view_role->role_id = $consultant['role'] ?? 2;
                $resource_view_role->save();
                $new_consultant['role_id'] = $resource_view_role->role_id;
                $user = User::find(($consultant['emp_id']));
                $new_consultant['consultant'] = $user->first_name.' '.$user->last_name;
            }
        }

        foreach ($old_consultants as $old_consultant) {
            if (! in_array($old_consultant, $new_consultants)) {
                $tmp_assignment = Assignment::where('project_id', '=', $project->id)->where('employee_id', '=', $old_consultant)->first();

                if (! empty($tmp_assignment)) {
                    $remove_assignment = Assignment::find($tmp_assignment->id);
                    $remove_assignment->status = 5;
                    $remove_assignment->save();
                }
            }
        }
        $project->save();


        return response()->json($new_consultant);
    }

    public function removeProjectConsultant(Request $request){
        $project = Project::find($request->project_id);
        $old_consultants = $project->consultants !== "" ? explode(',', $project->consultants):[];
        $new_consultants = '';

        if($request->has('consultants')){
            foreach($request->consultants as $key => $value){
                if($value != $request->emp_id){
                    if ($new_consultants == '') {
                        $new_consultants = $value;
                    } else {
                        $new_consultants .= ','.$value;
                    }
                }
            }
        }

        $project->consultants = $new_consultants;
        $project->save();

        if($request->has('emp_id') && $request->emp_id != ''){
            $assignment = Assignment::where('employee_id',$request->emp_id)->where('project_id',$project->id)->delete();
            $role = ResourceViewRole::where('resource_id',$request->emp_id)->where('module_item_id', $project->id)->where('module_id', 30)->delete();
        }

        $project->load(['assignment' => fn($assignment) => $assignment
            ->selectRaw("(select role_id from resource_view_roles where resource_view_roles.resource_id = assignment.employee_id and resource_view_roles.module_id = 30 and resource_view_roles.module_item_id = assignment.project_id group by role_id limit 1) as 'roled_id',assignment.id, assignment.project_id, assignment.employee_id, assignment.role, assignment.function, assignment.hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 0),0.0) AS actual_non_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.is_billable = 1),0.00) AS actual_bill_hours, IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 1 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_bill_hours,IFNULL((SELECT SUM(timeline.total + (timeline.total_m/60)) FROM timeline WHERE timeline.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND timeline.vend_is_billable = 0 AND timeline.vend_is_billable IS NOT NULL),0.00) AS vendor_non_bill_hours, assignment.start_date, assignment.end_date, assignment.assignment_status, assignment.invoice_rate, IF(assignment.internal_cost_rate = 0.00, assignment.external_cost_rate, assignment.internal_cost_rate) AS cost_rate, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.billable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.is_billable = 1) AS billable_expense, (SELECT SUM(time_exp.amount + IFNULL((SELECT SUM(exp_tracking.amount) FROM exp_tracking WHERE exp_tracking.assignment_id = assignment.id AND exp_tracking.claimable = 1),0.00)) FROM time_exp WHERE time_exp.timesheet_id IN (SELECT timesheet.id FROM timesheet WHERE timesheet.project_id = assignment.project_id AND timesheet.employee_id = assignment.employee_id) AND time_exp.claim = 1) AS claimable_expense, assignment.external_cost_rate, assignment.internal_cost_rate")
            ->where('employee_id', '>', 0)
            ->with([
                'function_desc:id,description',
                'project_status:id,description',
                'consultant:id,first_name,last_name'
            ])

        ]);

        $projectAssignments= $project->assignment;
        $unassignedConsultants = $project->consultantsWithoutAssignment($project->assignment);

        return response()->json(['assignments'=>$projectAssignments,'nonassignments'=>$unassignedConsultants]);
    }

    public function getProjectTimesheets($project_id){
        $timelines = Timesheet::selectRaw("timesheet.id as id,
        CONCAT(users.first_name,' ',users.last_name) AS `resource`,
        customer.customer_name AS customer_name,
        projects.name AS project,
        CASE WHEN timeline.task_id > 0 && timeline.description_of_work = '' THEN tasks.description
        WHEN timeline.task_id > 0 && timeline.description_of_work != '' THEN CONCAT(tasks.description,' - ',timeline.description_of_work)
        WHEN timeline.task_id > 0 THEN tasks.description
        ELSE timeline.description_of_work END AS 'task',
        timeline.is_billable AS 'is_billable',
        timeline.vend_is_billable AS 'vend_is_billable',
        task_delivery_types.name AS 'delivery_type',
        projects.ref AS 'ref',
        timesheet.timesheet_lock AS 'status',
        ((timeline.mon*60)+timeline.mon_m) AS 'mon',
        ((timeline.tue*60)+timeline.tue_m) AS 'tue',
        ((timeline.wed*60)+timeline.wed_m) AS 'wed',
        ((timeline.thu*60)+timeline.thu_m) AS 'thu',
        ((timeline.fri*60)+timeline.fri_m) AS 'fri',
        ((timeline.sat*60)+timeline.sat_m) AS 'sat',
        ((timeline.sun*60)+timeline.sun_m) AS 'sun'")
            ->leftJoin('timeline','timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('users', 'timesheet.employee_id', '=', 'users.id')
            ->leftJoin('customer', 'timesheet.customer_id', '=', 'customer.id')
            ->leftJoin('projects', 'timesheet.project_id', '=', 'projects.id')
            ->leftJoin('tasks', 'timeline.task_id', '=', 'tasks.id')
            ->leftJoin('task_delivery_types', 'timeline.task_delivery_type_id', '=', 'task_delivery_types.id')
            ->where('timesheet.project_id',$project_id);

        if (auth()->user()->hasAnyRole(['consultant', 'contractor'])) {
            $timelines = $timelines->where('employee_id',auth()->id());
        }

        if (auth()->user()->hasAnyRole(['manager','admin','admin_manager'])) {
            $timelines = $timelines;
        }

        $timelines = $timelines->orderBy('year_week','desc')->get();


        $data = [];

        foreach($timelines as $timeline){
            array_push($data,[
                'id' => $timeline->id,
                'resource' => $timeline->resource,
                'customer' => $timeline->customer_name,
                'project' => $timeline->project,
                'task' => $timeline->task,
                'is' => $timeline->is_billable,
                'vis' => $timeline->vend_is_billable,
                'delivery_type' => $timeline->delivery_type,
                'timesheet_ref' => $timeline->ref,
                'status' => $timeline->status,
                'mon' => $timeline->mon,
                'tue' => $timeline->tue,
                'wed' => $timeline->wed,
                'thu' => $timeline->thu,
                'fri' => $timeline->fri,
                'sat' => $timeline->sat,
                'sun' => $timeline->sun,
            ]);
        }

        return response()->json($data);
    }

    public function getAssignmentTasks($project_id){

        $assignments = Assignment::where('project_id',$project_id)->whereIn('assignment_status',[1,2,3])->get()->count();

        $tasks = Task::where('project_id',$project_id)->whereIn('status',[1,2,3])->get()->count();

        return response()->json(['assignments'=>$assignments,'tasks'=>$tasks]);
    }
}
