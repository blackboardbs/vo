<?php

namespace App\Http\Controllers;

use App\Models\DigisignUsers;
use App\Models\Document;
use App\Models\DocumentType;
use App\Http\Requests\DocumentRequest;
use App\Models\Module;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Storage;
use TCPDF;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $documents = Document::with('type', 'user', 'owner')->sortable(['name' => 'asc']);

        $module = Module::where('name', '=', 'App\Models\DocumentFiles')->get();

        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', -1);

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $documents = $documents->whereIn('owner_id', Auth::user()->team());
                $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', Auth::user()->team())->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', -1);
            }
        } else {
            return abort(403);
        }

        if (($request->has('q') && $request->input('q') != '')) {
            $documents = $documents->where('name', 'LIKE', '%'.$request->input('q').'%');
        }

        if ($request->has('type') && $request->input('type') != -1) {
            $documents = $documents->where('document_type_id', '=', $request->input('type'));
        }

        if ($request->has('resource') && $request->input('resource') != -1) {
            $documents = $documents->where('owner_id', '=', $request->input('resource'));
        }

        if ($request->has('from') && $request->input('from') != '') {
            $documents = $documents->where('created_at', '>=', $request->input('from'));
        }

        if ($request->has('to') && $request->input('to') != '') {
            $documents = $documents->where('created_at', '<=', $request->input('to'));
        }

        $parameters = [
            'documents' => $documents->paginate($item),
            'resource_drop_down' => $resource_drop_down,
            'document_type_drop_down' => DocumentType::orderBy('name')->pluck('name', 'id')->prepend('All', '-1'),
        ];

        return view('document.index', $parameters);
    }

    public function viewer($document_id, Request $request): View
    {
        $is_signed = false;
        if ($request->has('user')) {
            $document_user = DigisignUsers::where('document_id', '=', $document_id)->where('user_number', '=', $request->input('user'))->first();
            if ($document_user->signed == 1) {
                $is_signed = true;
            }
        } else {
            $is_signed = true;
        }

        $has_digisign = false;
        if ($request->has('digisign') && $request->input('digisign') == 1) {
            $has_digisign = true;
        }

        $file = Document::find($document_id);
        //$resource = User::find($file->digisign_approver_user_id);
        //$resoure_name = (isset($resource->first_name)?$resource->first_name:'').' '.(isset($resource->last_name)?$resource->last_name:'');
        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);
        $parameters = [
            'resource_name' => $request->name,
            'is_signed' => $is_signed,
            'has_digisign' => $has_digisign,
            'document_id' => $document_id,
            'resource_id' => $request->input('user'),
            'pdf_url' => public_path('//tmp/'.$file_url),
        ];

        return view('document.viewer')->with($parameters);
    }

    public function asset($digisign_user_id, Request $request): View
    {
        $asset_id = $request->input('asset_id');
        $is_signed = false;
        if ($request->has('user')) {
            $digisign_user = DigisignUsers::find($digisign_user_id);
            if ($digisign_user->signed == 1) {
                $is_signed = true;
            }
        } else {
            $is_signed = true;
        }

        $has_digisign = false;
        if ($request->has('digisign') && $request->input('digisign') == 1) {
            $has_digisign = true;
        }

        $file = Document::find($digisign_user->document_id);

        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);
        $parameters = [
            'resource_name' => $request->name,
            'is_signed' => $is_signed,
            'has_digisign' => $has_digisign,
            'digisign_user_id' => $digisign_user_id,
            'asset_id' => $asset_id,
            'document_id' => $file->id,
            'resource_id' => $request->input('user'),
            'pdf_url' => '../../tmp/'.$file_url,
        ];

        return view('document.asset')->with($parameters);
    }

    public function supplierAssignment($digisign_user_id, Request $request): View
    {
        $assignemnt_id = $request->input('assignment_id');
        $is_signed = false;

        $digisign_user = DigisignUsers::find($digisign_user_id);
        if ($digisign_user->signed == 1) {
            $is_signed = true;
        }

        $has_digisign = false;
        if ($request->has('digisign') && $request->input('digisign') == 1) {
            $has_digisign = true;
        }

        $file = Document::find($digisign_user->document_id);

        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);

        $resource_user = User::find($digisign_user->user_id);
        //dd($resource_user);
        $parameters = [
            'resource_name' => $resource_user->first_name.' '.$resource_user->last_name,
            'is_signed' => $is_signed,
            'has_digisign' => $has_digisign,
            'digisign_user_id' => $digisign_user_id,
            'assignment_id' => $assignemnt_id,
            'document_id' => $file->id,
            'resource_id' => $request->input('user'),
            'pdf_url' => '../../tmp/'.$file_url,
        ];

        return view('document.supplier_assignment')->with($parameters);
    }

    public function resourceAssignment($digisign_user_id, Request $request): View
    {
        $assignemnt_id = $request->input('assignment_id');
        $is_signed = false;

        $digisign_user = DigisignUsers::find($digisign_user_id);
        if ($digisign_user->signed == 1) {
            $is_signed = true;
        }

        $has_digisign = false;
        if ($request->has('digisign') && $request->input('digisign') == 1) {
            $has_digisign = true;
        }

        $file = Document::find($digisign_user->document_id);

        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);
        $parameters = [
            'resource_name' => $request->name,
            'is_signed' => $is_signed,
            'has_digisign' => $has_digisign,
            'digisign_user_id' => $digisign_user_id,
            'assignment_id' => $assignemnt_id,
            'document_id' => $file->id,
            'resource_id' => $request->input('user'),
            'pdf_url' => '../../tmp/'.$file_url,
        ];

        return view('document.supplier_assignment')->with($parameters);
    }

    public function assessmentDigisign($digisign_user_id, Request $request): View
    {
        $assessment_id = $request->input('assessment_id');
        $is_signed = false;

        $digisign_user = DigisignUsers::find($digisign_user_id);
        if ($digisign_user->signed == 1) {
            $is_signed = true;
        }

        $has_digisign = false;
        if ($request->has('digisign') && $request->input('digisign') == 1) {
            $has_digisign = true;
        }

        $file = Document::find($digisign_user->document_id);

        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);
        $parameters = [
            'resource_name' => $digisign_user->user_name/*$request->name*/,
            'is_signed' => $is_signed,
            'has_digisign' => $has_digisign,
            'digisign_user_id' => $digisign_user_id,
            'assessment_id' => $assessment_id,
            'document_id' => $file->id,
            'resource_id' => $request->input('user'),
            'pdf_url' => '../../files/documents/assessments/'.$file_url,
        ];

        return view('document.assessment_digisign')->with($parameters);
    }

    public function pdfViewer($document_id): View
    {
        $file = Document::find($document_id);
        //dd($file);
        $resource = User::find($file->digisign_approver_user_id);
        $resoure_name = (isset($resource->first_name) ? $resource->first_name : '').' '.(isset($resource->last_name) ? $resource->last_name : '');
        $file_url = str_replace('app\\public\\', '', $file->file);
        $file_url = str_replace('\\', '/', $file_url);
        $parameters = [
            //'pdf_url' => Storage::url($file_url)
            'resource_name' => $resoure_name,
            'document_id' => $document_id,
            'resource_id' => $resource->id ?? 0,
            'pdf_url' => '../../tmp/'.$file_url,
        ];

        return view('document.pdfviewer')->with($parameters);
    }

    public function download()
    {
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 001');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, [0, 64, 255], [0, 64, 128]);
        $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once dirname(__FILE__).'/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        // set text shadow effect
        $pdf->setTextShadow(['enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => [196, 196, 196], 'opacity' => 1, 'blend_mode' => 'Normal']);

        $html = '<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
        <i>This is the first example of TCPDF library.</i>
        <p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
        <p>Please check the source code documentation and other examples for further information.</p>
        <p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>';
        // Set some content to print
        /*$html = <<<EOD
        <h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
        <i>This is the first example of TCPDF library.</i>
        <p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
        <p>Please check the source code documentation and other examples for further information.</p>
        <p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;*/

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output('example_001.pdf', 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    public function create(Request $request): View
    {
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');

        $parameters = [
            'reference_id' => $reference_id,
            'document_type_id' => $document_type_id,
        ];

        return view('document.create')->with($parameters);
    }

    public function store(DocumentRequest $request): RedirectResponse
    {
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');
        $documentType = DocumentType::find($document_type_id);

        if ($request->hasfile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/files/documents/', $filename);

            $document = new Document();
            $document->type_id = $document_type_id;
            $document->name = $request->input('name');
            $document->file = 'files/documents/'.$filename;
            $document->document_type_id = $document_type_id;
            $document->reference = $documentType->name;
            $document->reference_id = $reference_id;
            $document->creator_id = auth()->id();
            $document->owner_id = auth()->id();
            $document->status_id = 1;
            $document->digisign_status_id = null;
            $document->digisign_approver_user_id = null;
            $document->save();
        }

        $message = 'Document added successfully.';

        switch ($document_type_id) {
            case 10:
                return redirect(route('customer.edit', $reference_id))->with('flash_success', $message);
                break;
            case 11:
                return redirect(route('scouting.edit', $reference_id))->with('flash_success', $message);
                break;
            default:
                return redirect(route('document.index'))->with('flash_success', $message);
                break;
        }
    }

    public function edit($id, Request $request): View
    {
        $document = Document::find($id);
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');

        $parameters = [
            'document' => $document,
            'reference_id' => $reference_id,
            'document_type_id' => $document_type_id,
        ];

        return view('document.edit')->with($parameters);
    }

    public function update($id, DocumentRequest $request): RedirectResponse
    {
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');
        $documentType = DocumentType::find($document_type_id);

        if ($request->hasfile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/files/documents/', $filename);

            $document = Document::find($id);
            $document->type_id = $document_type_id;
            $document->name = $request->input('name');
            $document->file = 'files/documents/'.$filename;
            $document->document_type_id = $document_type_id;
            $document->reference = $documentType->name;
            $document->reference_id = $reference_id;
            $document->creator_id = auth()->id();
            $document->owner_id = auth()->id();
            $document->status_id = 1;
            $document->digisign_status_id = null;
            $document->digisign_approver_user_id = null;
            $document->save();
        }

        $message = 'Document updated successfully';

        switch ($document_type_id) {
            case 10:
                return redirect(route('customer.edit', $reference_id))->with('flash_success', $message);
                break;
            case 11:
                return redirect(route('scouting.edit', $reference_id))->with('flash_success', $message);
                break;
            default:
                return redirect(route('document.index'))->with('flash_success', $message);
                break;
        }
    }

    public function updateDocumentName($id, Request $request): RedirectResponse
    {
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');

        $document = Document::find($id);
        $document->name = $request->input('document_name_'.$id);
        $document->save();

        $mesage = 'Document deleted successfully';
        switch ($document_type_id) {
            case 9:
                return redirect(route('action.edit', $reference_id))->with('flash_success', $mesage);
                break;
            case 10:
                return redirect(route('customer.edit', $reference_id))->with('flash_success', $mesage);
                break;
            case 11:
                return redirect(route('scouting.edit', $reference_id))->with('flash_success', $mesage);
                break;
            default:
                return redirect(route('document.index'))->with('flash_success', $mesage);
                break;
        }
    }

    public function destroy($id, Request $request): RedirectResponse
    {
        $reference_id = $request->input('reference_id');
        $document_type_id = $request->input('document_type_id');

        Document::destroy($id);

        $mesage = 'Document deleted successfully';
        switch ($document_type_id) {
            case 9:
                return redirect(route('action.edit', $reference_id))->with('flash_success', $mesage);
                break;
            case 10:
                return redirect(route('customer.edit', $reference_id))->with('flash_success', $mesage);
                break;
            case 11:
                return redirect(route('scouting.edit', $reference_id))->with('flash_success', $mesage);
                break;
            default:
                return redirect(route('document.index'))->with('flash_success', $mesage);
                break;
        }
    }
}
