<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LogsController extends Controller
{
    public function index(Request $request): View
    {
        $paginate = ($request->has('s') && $request->s != 0) ? $request->s : 15;

        if ($request->has('q')) {
            $logs = Log::where('user_name', 'LIKE', "%{$request->input('q')}%")
                ->orWhere('user_ip', 'LIKE', "%{$request->input('q')}%")
                ->orWhere('password', 'LIKE', "%{$request->input('q')}%")
                ->orWhere('date', 'LIKE', "%{$request->input('q')}%")
                ->orWhere('login_status', 'LIKE', "%{$request->input('q')}%")
                ->orderBy('created_at', 'desc')->paginate($paginate);
        } else {
            $logs = Log::orderBy('created_at', 'desc')->paginate($paginate);
        }

        $parameters = [
            'user_logs' => $logs,
        ];

        return view('logins.index')->with($parameters);
    }

    public function failed_logins(): View
    {
        $date = Carbon::now();
        $parameters = [
            'failed_attempts' => Log::where('login_status', '=', 0)->whereBetween('date', [$date->now()->subMonth()->toDateString(), $date->now()->toDateString()])->sortable('user_name', 'asc')->paginate(15),
        ];

        return view('landing-page-dashboard.failed-attempts')->with($parameters);
    }

    public function logins(): View
    {
        $date = Carbon::now();
        $parameters = [
            'logins' => Log::where('login_status', '=', 1)->whereBetween('date', [$date->now()->subMonth()->toDateString(), $date->now()->toDateString()])->sortable('user_name', 'asc')->paginate(15),
        ];

        return view('landing-page-dashboard.logins')->with($parameters);
    }

    public function destroy($logins): RedirectResponse
    {
        Log::destroy($logins);

        return redirect()->route('logins.index')->with('flash_success', 'The log was deleted successfully');
    }
}
