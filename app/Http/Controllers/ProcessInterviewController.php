<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProcessInterviewRequest;
use App\Http\Requests\UpdateProcessInterviewRequest;
use App\Models\ProcessInterview;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProcessInterviewController extends Controller
{
    public function index(Request $request): View
    {

        // $process_interview = ProcessInterview::where('status_id', 1)->orderBy('name');

        $item = $request->input('s') ?? 15;

        $process_interview = ProcessInterview::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $process_interview = $process_interview->where('status_id', $request->input('status_filter'));
            }else{
                $process_interview = $process_interview->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $process_interview = $process_interview->where('name', 'like', '%'.$request->input('q').'%');
        }

        $process_interview = $process_interview->paginate($item);

        $parameters = [
            'process_interviews' => $process_interview,
        ];

        return view('master_data.process_interview.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ProcessInterview::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.process_interview.create')->with($parameters);
    }

    public function store(StoreProcessInterviewRequest $request): RedirectResponse
    {
        $process_interview = new ProcessInterview();
        $process_interview->name = $request->name;
        $process_interview->status_id = $request->status;
        $process_interview->save();

        return redirect()->route('process_interview.index')->with('flash_success', 'New Process Interview was created successfully');
    }

    public function show($id): View
    {
        $process_interview = ProcessInterview::find($id);
        $parameters = [
            'process_interview' => $process_interview,
        ];

        return view('master_data.process_interview.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = ProcessInterview::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $process_interview = ProcessInterview::find($id);
        $parameters = [
            'process_interview' => $process_interview,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.process_interview.edit')->with($parameters);
    }

    public function update(UpdateProcessInterviewRequest $request, $id): RedirectResponse
    {
        $process_interview = ProcessInterview::find($id);
        $process_interview->name = $request->name;
        $process_interview->status_id = $request->status;
        $process_interview->save();

        return redirect()->route('process_interview.index')->with('flash_success', 'Process Interview was updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // ProcessInterview::destroy($id);
        $item = ProcessInterview::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('process_interview.index')->with('flash_success', 'Process Interview was suspended successfully');
    }
}
