<?php

namespace App\Http\Controllers;

use App\Models\AssetAdditionalCost;
use App\Models\AssetRegister;
use App\Services\EquipmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssetAdditionalCostController extends Controller
{
    public function index(AssetRegister $equipment)
    {
        $equipment->loadMissing('additionalCost');

        return response()->json(['costs' => $equipment->additionalCost]);
    }

    public function store(AssetRegister $equipment, Request $request, EquipmentService $service)
    {
        $validator = $service->validating($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        return response()->json(['cost' => $equipment->additionalCost()->create($validator->getData())]);
    }

    public function update(AssetRegister $equipment, Request $request, EquipmentService $service)
    {
        $validator = $service->validating($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        $data = $validator->getData();
        $data['cost'] = $request->cost * 100;
        return response()->json(['cost' => $equipment->additionalCost()->update($data)]);
    }

    public function destroy(AssetAdditionalCost $cost)
    {
        $cost->delete();

        return response()->json(['message' => 'Success']);
    }
}
