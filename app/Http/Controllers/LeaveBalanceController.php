<?php

namespace App\Http\Controllers;

use App\Exports\LeaveBalancesExport;
use App\Exports\LeavesExport;
use App\Models\Config;
use App\Http\Requests\StoreLeaveBalanceRequest;
use App\Http\Requests\UpdateLeaveBalanceRequest;
use App\Models\LeaveBalance;
use App\Models\LeaveType;
use App\Models\Module;
use App\Models\Status;
use App\Models\User;
use App\Models\Resource;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class LeaveBalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $items = $request->input('r') ?? 15;

        $leave_balance = LeaveBalance::with([
            'resource:id,first_name,last_name',
            'leave_type:id,description',
            'statusd:id,description',
        ])->sortable(['id' => 'asc']);

        $module = Module::where('name', '=', \App\Models\LeaveBalance::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $leave_balance = $leave_balance->whereIn('resource_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $leave_balance = $leave_balance->whereHas('resource', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('q').'%')->orWhere('last_name', 'like', '%'.$request->input('q').'%');
            })->orWhereHas('leave_type', function ($query) use ($request) {
                $query->where('description', 'like', '%'.$request->input('q').'%');
            })->orWhere('balance_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('leave_per_cycle', 'like', '%'.$request->input('q').'%')
                ->orWhere('cycle_start', 'like', '%'.$request->input('q').'%')
                ->orWhere('cycle_end', 'like', '%'.$request->input('q').'%')
                ->orWhere('opening_balance', 'like', '%'.$request->input('q').'%')
                ->orWhere('closing_balance', 'like', '%'.$request->input('q').'%')
                ->orWhere('leave_taken', 'like', '%'.$request->input('q').'%')
                ->orWhere('total_accrued', 'like', '%'.$request->input('q').'%')
                ->orWhere('leave_taken', 'like', '%'.$request->input('q').'%');
        }

        $leave_balance = $leave_balance->paginate($items);

        $parameters = [
            'leavebalances' => $leave_balance,
        ];

        if ($request->has('export')) return $this->export($parameters, 'leave_balance');


        return view('leave_balance.index')->with($parameters);
    }

    public function create(): View
    {
        $balance_date = Carbon::now()->endOfMonth();
        $default_annual_days = Config::first()->annual_leave_days;

        $parameters = [
            'annual_leave_days' => $default_annual_days,
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Select Resource', 0),
            'leave' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Leave Type', '0'),
            'balance_date' => Carbon::parse($balance_date)->toDateString(),
            'status_dropdown' => Status::where('status', '=', 1)->orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return view('leave_balance.add')->with($parameters);
    }

    public function store(StoreLeaveBalanceRequest $request): RedirectResponse
    {
        $leave = LeaveBalance::where('resource_id', $request->input('resource'))->where('leave_type_id', 1)->get();

        if (count($leave) == 0) {
            $leavebalance = new LeaveBalance();
            $leavebalance->resource_id = $request->input('resource');
            $leavebalance->leave_type_id = 1;
            $leavebalance->balance_date = $request->input('balance_date');
            $leavebalance->leave_per_cycle = $request->input('leave_per_cycle');
            $leavebalance->opening_balance = $request->input('opening_balance');
            $leavebalance->status = $request->input('status');
            $leavebalance->created_by = auth()->id();
            if($request->has('pay_out') && $request->pay_out == 'on'){
                $leavebalance->pay_out  = 1;
                $leavebalance->pay_out_date = $request->input('pay_out_date');
                $leavebalance->pay_out_balance = $request->input('pay_out_leave');
            }
            $leavebalance->save();

            return redirect(route('leave_balance.index'))->with('flash_success', 'Leave Balance saved successfully');
        } else {
            $leavebalance = LeaveBalance::find($leave[0]->id);
            $leavebalance->resource_id = $request->input('resource');
            $leavebalance->leave_type_id = 1;
            $leavebalance->balance_date = $request->input('balance_date');
            $leavebalance->leave_per_cycle = $request->input('leave_per_cycle');
            $leavebalance->opening_balance = $request->input('opening_balance');
            $leavebalance->status = $request->input('status');
            $leavebalance->created_by = auth()->id();
            if($request->has('pay_out') && $request->pay_out == 'on'){
                $leavebalance->pay_out  = 1;
                $leavebalance->pay_out_date = $request->input('pay_out_date');
                $leavebalance->pay_out_balance = $request->input('pay_out_leave');
            }
            $leavebalance->save();


            if($request->has('pay_out') && $request->pay_out == 'on'){
                $resource = Resource::where('user_id',$leavebalance->resource_id)->whereNull('termination_date')->update(['termination_date' => $request->pay_out_date]);
            }

            return redirect(route('leave_balance.index'))->with('flash_success', 'Existing Leave Balance updated successfully');
        }
    }

    public function edit($id): View
    {
        $leave_balance = LeaveBalance::find($id);
        $resource = Resource::where('user_id',$leave_balance->resource_id)->first();
        $balance_date = Carbon::now()->endOfMonth();
        $default_annual_days = Config::first()->annual_leave_days;

        $parameters = [
            'resource' => $resource,
            'leave_balance' => $leave_balance,
            'annual_leave_days' => $default_annual_days,
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Select Resource', 0),
            'leave' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Leave Type', '0'),
            'balance_date' => Carbon::parse($balance_date)->toDateString(),
            'status_dropdown' => Status::where('status', '=', 1)->orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return view('leave_balance.edit')->with($parameters);
    }

    public function update(UpdateLeaveBalanceRequest $request, $leave_balance): RedirectResponse
    {
        $leavebalance = LeaveBalance::find($leave_balance);
        $leavebalance->resource_id = $request->input('resource');
        $leavebalance->balance_date = $request->input('balance_date');
        $leavebalance->leave_per_cycle = $request->input('leave_per_cycle');
        $leavebalance->opening_balance = $request->input('opening_balance');
        $leavebalance->status = $request->input('status');
        $leavebalance->created_by = auth()->id();
        if($request->has('pay_out') && $request->pay_out == 'on'){
            $leavebalance->pay_out  = 1;
            $leavebalance->pay_out_date = $request->input('pay_out_date');
            $leavebalance->pay_out_balance = $request->input('pay_out_leave');
        } else {
            $leavebalance->pay_out  = null;
            $leavebalance->pay_out_date = null;
            $leavebalance->pay_out_balance = null;
        }
        $leavebalance->save();

        if($request->has('pay_out') && $request->pay_out == 'on'){
            $resource = Resource::where('user_id',$leavebalance->resource_id)->whereNull('termination_date')->update(['termination_date' => $request->pay_out_date]);
        }

        return redirect(route('leave_balance.index'))->with('flash_success', 'Leave Balance successfully updated');
    }

    public function show($leave_balance): View
    {
        $parameters = [
            'leave_balance' => LeaveBalance::where('id', $leave_balance)->get(),
        ];

        return view('leave_balance.show')->with($parameters);
    }

    public function destroy($leave_balance): RedirectResponse
    {
        LeaveBalance::destroy($leave_balance);

        return redirect()->route('leave_balance.index')->with('flash_success', 'Leave Balance deleted successfully');
    }

    public function getResource(Request $request, $user_id){
        $resource = Resource::where('user_id',$user_id)->first();

        return response()->json($resource);
    }
}
