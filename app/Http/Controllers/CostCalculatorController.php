<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Company;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CostCalculatorController extends Controller
{
    public function __construct()
    {
        // $this->middleware('permission:cost_calculator_1|cost_calculator_2|cost_calculator_3|cost_calculator_4|cost_calculator_5|cost_calculator_la_1|cost_calculator_la_2|cost_calculator_la_3|cost_calculator_la_4|cost_calculator_la_5|cost_calculator_lo_1|cost_calculator_lo_2|cost_calculator_lo_3|cost_calculator_lo_4|cost_calculator_lo_5|cost_calculator_or_1|cost_calculator_or_2|cost_calculator_or_3|cost_calculator_or_4|cost_calculator_or_5|cost_calculator_ar_1|cost_calculator_ar_2|cost_calculator_ar_3|cost_calculator_ar_4|cost_calculator_ar_5', ['only' => ['index', 'show']]);
        // $this->middleware('permission:cost_calculator_2|cost_calculator_3|cost_calculator_4|cost_calculator_5|cost_calculator_la_2|cost_calculator_la_3|cost_calculator_la_4|cost_calculator_la_5|cost_calculator_lo_2|cost_calculator_lo_3|cost_calculator_lo_4|cost_calculator_lo_5|cost_calculator_or_2|cost_calculator_or_3|cost_calculator_or_4|cost_calculator_or_5|cost_calculator_ar_2|cost_calculator_ar_3|cost_calculator_ar_4|cost_calculator_ar_5', ['only' => ['create', 'store', 'createTimeline', 'storeTimeline']]);
        // $this->middleware('permission:cost_calculator_3|cost_calculator_4|cost_calculator_5|cost_calculator_la_3|cost_calculator_la_4|cost_calculator_la_5|cost_calculator_lo_3|cost_calculator_lo_4|cost_calculator_lo_5|cost_calculator_or_3|cost_calculator_or_4|cost_calculator_or_5|cost_calculator_ar_3|cost_calculator_ar_4|cost_calculator_ar_5', ['only' => ['edit', 'update', 'editTimeline', 'updateTimeline', 'maintain', 'store_maintain']]);
        // $this->middleware('permission:cost_calculator_4|cost_calculator_5|cost_calculator_la_4|cost_calculator_la_5|cost_calculator_lo_4|cost_calculator_lo_5|cost_calculator_or_4|cost_calculator_or_5|cost_calculator_ar_4|cost_calculator_ar_5', ['only' => ['destroy', 'destroyTimeline']]);
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        //$costcentre = CostCenter::sortable('description','asc')->paginate(15);
        $company = Company::first();
        $commission = Commission::first();
        $package = 1;
        $rate = 1;
        $exp_p = 1.05;
        $bill_p = 1.05;
        $ctc_rate = 146;
        $ber_rate = 120;
        $pbr_rate = 88;
        $cr01 = $commission->min_rate * $commission->hour01 * ($commission->comm01 / 100);
        $cr02 = $commission->min_rate * $commission->hour02 * ($commission->comm02 / 100);
        $cr03 = $commission->min_rate * $commission->hour03 * ($commission->comm03 / 100);
        $cr04 = $commission->min_rate * $commission->hour04 * ($commission->comm04 / 100);
        $cr05 = $commission->min_rate * $commission->hour05 * ($commission->comm05 / 100);
        $cr06 = $commission->min_rate * $commission->hour06 * ($commission->comm06 / 100);
        $cr07 = $commission->min_rate * $commission->hour07 * ($commission->comm06 / 100);
        $cr08 = $commission->min_rate * $commission->hour08 * ($commission->comm08 / 100);
        $cr09 = $commission->min_rate * $commission->hour09 * ($commission->comm09 / 100);
        $cr10 = $commission->min_rate * $commission->hour10 * ($commission->comm10 / 100);
        $pack1 = $package * $exp_p;
        $pack2 = $package;
        $bill_rate = $rate;
        $bill1 = $rate;
        $bill2 = $rate * $bill_p;
        $ctc = $package / $ctc_rate;
        if ($bill_rate == 0) {
            $ctc_margin = 0;
        } else {
            $ctc_margin = (($bill_rate - $ctc) / $bill_rate) * 100;
        }
        if ($ctc_rate == 0) {
            $ctc1 = 0;
        } else {
            $ctc1 = $pack1 / $ctc_rate;
        }
        if ($bill1 == 0) {
            $ctc1_margin = 0;
        } else {
            $ctc1_margin = (($bill1 - $ctc1) / $bill1) * 100;
        }
        if ($bill2 == 0) {
            $ctc2 = 0;
        } else {
            $ctc2 = $pack2 / $ctc_rate;
        }
        if ($bill2 == 0) {
            $ctc2_margin = 0;
        } else {
            $ctc2_margin = (($bill2 - $ctc2) / $bill2) * 100;
        }
        $ber = $package / $ber_rate;
        if ($bill_rate == 0) {
            $ber_margin = 0;
        } else {
            $ber_margin = (($bill_rate - $ber) / $bill_rate) * 100;
        }
        $ber1 = $pack1 / $ber_rate;
        if ($bill1 == 0) {
            $ber1_margin = 0;
        } else {
            $ber1_margin = (($bill1 - $ber1) / $bill1) * 100;
        }
        $ber2 = $pack2 / $ber_rate;
        if ($bill2 == 0) {
            $ber2_margin = 0;
        } else {
            $ber2_margin = (($bill2 - $ber2) / $bill2) * 100;
        }

        $pbr = $package / $pbr_rate;
        $pbr_margin = (($bill_rate - $pbr) / $bill_rate) * 100;

        $profit160 = ($bill_rate * 160) - $package;
        $profit1160 = ($bill1 * 160) - $pack1;
        $profit2160 = ($bill2 * 160) - $pack2;
        $profit140 = ($bill_rate * 140) - $package;
        $profit1140 = ($bill1 * 140) - $pack1;
        $profit2140 = ($bill2 * 140) - $pack2;

        /*if ($request->has('q') && $request->input('q') != '') {
            $costcentre = Terms::where('description', 'like', "%" . $request->input('q') . "%")
                ->sortable(['description' => 'asc'])->paginate(15);
        }*/
        //return $commission;
        $parameters = [
            'company' => $company,
            'commission' => $commission,
            'package' => $package,
            'ctc_margin' => $ctc_margin,
            'ctc1_margin' => $ctc1_margin,
            'ctc2_margin' => $ctc2_margin,
            'ber_margin' => $ber_margin,
            'ber_rate' => $ber_rate,
            'ber1_margin' => $ber1_margin,
            'ber2_margin' => $ber2_margin,
            'pbr_margin' => $pbr_margin,
            'profit160' => $profit160,
            'profit1160' => $profit1160,
            'profit2160' => $profit2160,
            'profit140' => $profit140,
            'profit1140' => $profit1140,
            'profit2140' => $profit2140,
            'cr01' => $cr01,
            'cr02' => $cr02,
            'cr03' => $cr03,
            'cr04' => $cr04,
            'cr05' => $cr05,
            'cr06' => $cr06,
            'cr07' => $cr07,
            'cr08' => $cr08,
            'cr09' => $cr09,
            'cr10' => $cr10,
            'pack1' => $pack1,
            'pack2' => $pack2,
            'rate' => $rate,
            'bill1' => $bill1,
            'bill2' => $bill2,
            'ctc_rate' => $ctc_rate,
            'ctc' => $ctc,
            'ctc1' => $ctc1,
            'ctc2' => $ctc2,
            'ber' => $ber,
            'ber1' => $ber1,
            'ber2' => $ber2,
            'pbr_rate' => $pbr_rate,
            'pbr' => $pbr,
            'comm_dropdown' => $commission->pluck('description', 'id'),
            'configs' => Config::first(),

        ];

        return View('customer.cost_calculator.index', $parameters);
    }

    public function calculate(Request $request): View
    {
        //return $request->all();
        $company = Company::find(1);
        $commission = Commission::find($request->input('commission'));
        $package = $request->input('package');
        $rate = $request->input('bill_rate');
        $exp_p = 1.05;
        $bill_p = 1.05;
        $ctc_rate = 146;
        $ber_rate = 120;
        $pbr_rate = 88;
        $cr01 = $commission->min_rate * $commission->hour01 * ($commission->comm01 / 100);
        $cr02 = $commission->min_rate * $commission->hour02 * ($commission->comm02 / 100);
        $cr03 = $commission->min_rate * $commission->hour03 * ($commission->comm03 / 100);
        $cr04 = $commission->min_rate * $commission->hour04 * ($commission->comm04 / 100);
        $cr05 = $commission->min_rate * $commission->hour05 * ($commission->comm05 / 100);
        $cr06 = $commission->min_rate * $commission->hour06 * ($commission->comm06 / 100);
        $cr07 = $commission->min_rate * $commission->hour07 * ($commission->comm06 / 100);
        $cr08 = $commission->min_rate * $commission->hour08 * ($commission->comm08 / 100);
        $cr09 = $commission->min_rate * $commission->hour09 * ($commission->comm09 / 100);
        $cr10 = $commission->min_rate * $commission->hour10 * ($commission->comm10 / 100);
        $pack1 = $package * $exp_p;
        $pack2 = $package;
        $bill_rate = $rate;
        $bill1 = $rate;
        $bill2 = $rate * $bill_p;
        $ctc = $package / $ctc_rate;
        if ($bill_rate == 0) {
            $ctc_margin = 0;
        } else {
            $ctc_margin = (($bill_rate - $ctc) / $bill_rate) * 100;
        }
        if ($ctc_rate == 0) {
            $ctc1 = 0;
        } else {
            $ctc1 = $pack1 / $ctc_rate;
        }
        if ($bill1 == 0) {
            $ctc1_margin = 0;
        } else {
            $ctc1_margin = (($bill1 - $ctc1) / $bill1) * 100;
        }
        if ($bill2 == 0) {
            $ctc2 = 0;
        } else {
            $ctc2 = $pack2 / $ctc_rate;
        }
        if ($bill2 == 0) {
            $ctc2_margin = 0;
        } else {
            $ctc2_margin = (($bill2 - $ctc2) / $bill2) * 100;
        }
        $ber = $package / $ber_rate;
        if ($bill_rate == 0) {
            $ber_margin = 0;
        } else {
            $ber_margin = (($bill_rate - $ber) / $bill_rate) * 100;
        }
        $ber1 = $pack1 / $ber_rate;
        if ($bill1 == 0) {
            $ber1_margin = 0;
        } else {
            $ber1_margin = (($bill1 - $ber1) / $bill1) * 100;
        }
        $ber2 = $pack2 / $ber_rate;
        if ($bill2 == 0) {
            $ber2_margin = 0;
        } else {
            $ber2_margin = (($bill2 - $ber2) / $bill2) * 100;
        }

        $pbr = $package / $pbr_rate;
        $pbr_margin = (($bill_rate - $pbr) / $bill_rate) * 100;

        $profit160 = ($bill_rate * 160) - $package;
        $profit1160 = ($bill1 * 160) - $pack1;
        $profit2160 = ($bill2 * 160) - $pack2;
        $profit140 = ($bill_rate * 140) - $package;
        $profit1140 = ($bill1 * 140) - $pack1;
        $profit2140 = ($bill2 * 140) - $pack2;

        /*if ($request->has('q') && $request->input('q') != '') {
            $costcentre = Terms::where('description', 'like', "%" . $request->input('q') . "%")
                ->sortable(['description' => 'asc'])->paginate(15);
        }*/
        //return $commission;
        $parameters = [
            'company' => $company,
            'commission' => $commission,
            'package' => $package,
            'ctc_margin' => $ctc_margin,
            'ctc1_margin' => $ctc1_margin,
            'ctc2_margin' => $ctc2_margin,
            'ber_margin' => $ber_margin,
            'ber_rate' => $ber_rate,
            'ber1_margin' => $ber1_margin,
            'ber2_margin' => $ber2_margin,
            'pbr_margin' => $pbr_margin,
            'profit160' => $profit160,
            'profit1160' => $profit1160,
            'profit2160' => $profit2160,
            'profit140' => $profit140,
            'profit1140' => $profit1140,
            'profit2140' => $profit2140,
            'cr01' => $cr01,
            'cr02' => $cr02,
            'cr03' => $cr03,
            'cr04' => $cr04,
            'cr05' => $cr05,
            'cr06' => $cr06,
            'cr07' => $cr07,
            'cr08' => $cr08,
            'cr09' => $cr09,
            'cr10' => $cr10,
            'pack1' => $pack1,
            'pack2' => $pack2,
            'rate' => $rate,
            'bill1' => $bill1,
            'bill2' => $bill2,
            'ctc_rate' => $ctc_rate,
            'ctc' => $ctc,
            'ctc1' => $ctc1,
            'ctc2' => $ctc2,
            'ber' => $ber,
            'ber1' => $ber1,
            'ber2' => $ber2,
            'pbr_rate' => $pbr_rate,
            'pbr' => $pbr,
            'comm_dropdown' => $commission->pluck('description', 'id'),
            'configs' => Config::first(),
        ];

        return View('customer.cost_calculator.index', $parameters);
    }
}
