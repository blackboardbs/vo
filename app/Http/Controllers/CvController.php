<?php

namespace App\Http\Controllers;

use App\Models\AdvancedTemplates;
use App\Models\Availability;
use App\Models\AvailStatus;
use App\Models\BBBEERace;
use App\Models\Company;
use App\Models\Config;
use App\Models\Cv;
use App\Models\CVQuality;
use App\Models\CVReference;
use App\Models\CvTemplate;
use App\Models\CVWishList;
use App\Models\Disability;
use App\Models\Document;
use App\Models\Experience;
use App\Models\Gender;
use App\Http\Requests\DocumentRequest;
use App\Http\Requests\StoreCvRequest;
use App\Http\Requests\UpdateCvRequest;
use App\Models\Industry;
use App\Models\IndustryExperience;
use App\Models\JobSpec;
use App\Mail\CVMail;
use App\Models\MaritalStatus;
use App\Models\Module;
use App\Models\Naturalization;
use App\Models\Profession;
use App\Models\Qualification;
use App\Models\Race;
use App\Models\Resource;
use App\Models\ResourceLevel;
use App\Models\ResourceType;
use App\Models\Scouting;
use App\Models\ScoutingRole;
use App\Models\Skill;
use App\Models\SkillLevel;
use App\Models\Speciality;
use App\Models\Status;
use App\Models\System;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use PhpOffice\PhpWord\TemplateProcessor;
use Spatie\Activitylog\Models\Activity;
use Spatie\LaravelPdf\Facades\Pdf;

class CvController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //Temp fix until pagination aand filters work together
        $item = $request->has('r') && $request->input('r') != -1 ? $request->input('r') : 15;

        $cv = Cv::with([
            'user:id,first_name,last_name,email,phone,resource_id',
            'status:id,description',
            'role:id,name',
            'quality:cv_id,quality_percentage',
            'availability:res_id,avail_date',
        ])->select(['id', 'user_id', 'status_id', 'role_id']);

        $module = Module::where('name', '=', 'App\Models\CV')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
            $cv = $cv->whereIn('user_id', Auth::user()->team());
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        $cv = $cv->when($request->skill_level_id, fn($c) => $c->whereHas('skill', fn($skill) => $skill->where('skill_level', $request->skill_level_id)))
            ->when($request->avail_status_id, fn($c) => $c->whereHas('availability', fn($availability) => $availability->where('avail_status', $request->avail_status_id)))
            ->when($request->rt, fn($c) => $c->whereHas('resource', fn($resource) => $resource->where('resource_type', $request->rt)))
            ->when($request->qf, fn($c) => $c->whereHas('qualification', fn($qualification) => $qualification->where('qualification', 'LIKE', '%'.$request->qf.'%')))
            ->when($request->e, fn($c) => $c->whereHas('experience', fn($experience) => $experience->where(function ($exp) use($request){
                $exp->where('tools', 'like', '%'.$request->e.'%')
                    ->orWhere('company', 'like', '%'.$request->e.'%')
                    ->orWhere('current_project', 'like', '%'.$request->e.'%')
                    ->orWhere('role', 'like', '%'.$request->e.'%')
                    ->orWhere('project', 'like', '%'.$request->e.'%')
                    ->orWhere('responsibility', 'like', '%'.$request->e.'%');
            })))
            ->when($request->q, fn($c) => $c->whereHas('user', fn($user) => $user->where(function ($user) use($request){
                $user->where('first_name', 'LIKE', '%'.$request->q.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$request->q.'%')
                    ->orWhere('email', 'LIKE', '%'.$request->q.'%');
            })))->paginate($item);

        $parameters = [
            'cv' => $cv,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        if ($request->has('export')) return $this->export($parameters, 'cv');

        return view('cv.index')->with($parameters);
    }

    public function search(Request $request)
    {
        $item = $request->has('s') && $request->input('s') != -1 ? $request->input('s') : 15;

        $cv_wish_lists = CVWishList::orderBy('id')->pluck('cv_id', 'id')->toArray();

        $cv = Cv::with('user', 'skill', 'qualification', 'availability', 'resourcetype')->whereHas('user');

        $module = Module::where('name', '=', 'App\Models\CV')->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $cv = $cv->whereIn('user_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        //Flags so the search result is showed
        $availability_flag = false;
        $bbbe_flag = false;
        $experience_months_flag = false;
        $industry_flag = false;
        $nationality_flag = false;
        $qualification_flag = false;
        $resource_type_flag = false;
        $main_role_flag = false;
        $skill_level_flag = false;
        $disability_flag = false;
        $skills_flag = false;
        $placement_options_flag = false;

        $searched_for = [];

        $cv->orderBy('id', 'desc');

        if ($request->has('q') && $request->input('q') != '') {
            $cv = $cv->whereHas('user', function ($query) use ($request) {
                $query->where('first_name', 'like', '%'.$request->input('q').'%');
                $query->orWhere('last_name', 'like', '%'.$request->input('q').'%');
            })
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                });
            $searched_for[] .= 'search string: '.$request->input('q');
        }

        $cv = $cv->get();

        if ($request->has('a') && $request->input('a') != '-1') {
            $availab = [];
            foreach ($request->input('a') as $key => $value) {
                $availab[] = (int) $value;
            }

            $availability = Availability::whereIn('avail_status', $availab)
            ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $availability_statuses = AvailStatus::whereIn('id', $availab)->get();

            $tmp_search = 'Availability: ';
            $not_empty_flag = false;
            foreach ($availability_statuses as $availability_status) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$availability_status->description;
                } else {
                    $tmp_search .= $availability_status->description;
                }

                $not_empty_flag = true;
                $availability_flag = true;
            }

            $searched_for[] = $tmp_search;

            $cv = $cv->filter(function ($cv) use ($availability) {
                return in_array($cv->user_id, $availability);
            });
        }

        if ($request->has('sk') && $request->input('sk') != '') {
            $skill = $request->input('sk');

            $cv = $cv->filter(function ($cv) use ($skill) {
                $string = strtolower($cv->main_skill);
                $sub_string = strtolower($skill);
                $flag = false;
                if (strpos($string, $sub_string) !== false) {
                    $flag = true;
                }

                return $flag;
            });

            $tmp_search = 'Main Skill: '.$request->input('sk');

            $searched_for[] = $tmp_search;
        }

        if ($request->has('i') && $request->input('i') != '-1') {
            $industries = [];

            foreach ($request->input('i') as $key => $value) {
                $industries[] = (int) $value;
            }

            $industry = IndustryExperience::whereIn('ind_id', $industries)
                ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($industry) {
                return in_array($cv->user_id, $industry);
            });

            $search_options = Industry::whereIn('id', $industries)->get();

            $tmp_search = 'Industry: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
                $industry_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($request->has('skl') && $request->input('skl') != '-1') {
            $skills = [];
            foreach ($request->input('skl') as $key => $value) {
                $skills[] = (int) $value;
            }

            $skill = Skill::whereIN('skill_level', $skills)
            ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($skill) {
                return in_array($cv->user_id, $skill);
            });

            $search_options = SkillLevel::whereIn('id', $skills)->get();

            $tmp_search = 'Skill Level: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;

                $skill_level_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($request->has('bbe') && $request->input('bbe') != '-1') {
            $bbbee_race = [];
            foreach ($request->input('bbe') as $key => $value) {
                $bbbee_race[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($bbbee_race) {
                return in_array($cv->bbbee_race, $bbbee_race);
            });

            $search_options = BBBEERace::whereIn('id', $bbbee_race)->get();

            $tmp_search = 'BBE Race: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
                $bbbe_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($request->has('d') && $request->input('d') != '-1') {
            $disability = [];

            foreach ($request->input('d') as $key => $value) {
                $disability[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($disability) {
                return in_array($cv->disability, $disability);
            });

            $search_options = Disability::whereIn('id', $disability)->get();

            $tmp_search = 'Disability: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->description;
                } else {
                    $tmp_search .= $search_option->description;
                }

                $not_empty_flag = true;
                $disability_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        $profession_flag = false;
        if ($request->has('p') && $request->input('p') != '') {
            $profession = [];
            foreach ($request->input('p') as $key => $value) {
                $profession[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($profession) {
                return in_array($cv->profession_id, $profession);
            });

            $profession_flag = true;
        }

        $speciality_flag = false;
        if ($request->has('sp') && $request->input('sp') != '') {
            $speciality = [];
            foreach ($request->input('sp') as $key => $value) {
                $speciality[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($speciality) {
                return in_array($cv->speciality_id, $speciality);
            });

            $speciality_flag = true;
        }

        $hourly_rate_minimum_flag = false;
        if ($request->has('rm') && $request->input('rm') != '') {
            $hourly_rate_mimimum = $request->input('rm');

            $cv = $cv->filter(function ($cv) use ($hourly_rate_mimimum) {
                $hourly_rate = isset($cv->hourly_rate_minimum) && $cv->hourly_rate_minimum > 0 ? $cv->hourly_rate_minimum : 0; //Set minimum to 0 if not set

                return (int) $hourly_rate_mimimum >= $hourly_rate;
            });

            $hourly_rate_minimum_flag = true;
        }

        $hourly_rate_maximum_flag = false;
        if ($request->has('rx') && $request->input('rx') != '') {
            $hourly_rate_maximum = $request->input('rx');

            $cv = $cv->filter(function ($cv) use ($hourly_rate_maximum) {
                $hourly_rate = isset($cv->hourly_rate_maximum) && $cv->hourly_rate_maximum > 0 ? $cv->hourly_rate_minimum : 9000000; //Set Maximum to billion if not set

                return $hourly_rate_maximum <= $hourly_rate;
            });

            $hourly_rate_maximum_flag = true;
        }

        $monthly_salary_minimum_flag = false;
        if ($request->has('sm') && $request->input('sm') != '') {
            $monthly_salary_minimum = $request->input('sm');

            $cv = $cv->filter(function ($cv) use ($monthly_salary_minimum) {
                $monthly_salary = isset($cv->monthly_salary_minimum) && $cv->monthly_salary_minimum > 0 ? $cv->monthly_salary_minimum : 0; //Set Maximum to billion if not set

                return $monthly_salary >= $monthly_salary_minimum;
            });

            $monthly_salary_minimum_flag = true;
        }

        $monthly_salary_maximum_flag = false;
        if ($request->has('sx') && $request->input('sx') != '') {
            $monthly_salary_maximum = $request->input('sx');

            $cv = $cv->filter(function ($cv) use ($monthly_salary_maximum) {
                $monthly_salary = isset($cv->monthly_salary_maximum) && $cv->monthly_salary_maximum > 0 ? $cv->monthly_salary_maximum : 90000000; //Set Maximum to billion if not set

                return $monthly_salary <= $monthly_salary_maximum;
            });

            $monthly_salary_maximum_flag = true;
        }

        if ($request->has('qf') && $request->input('qf') != '') {
            $qualification = Qualification::where('qualification', 'like', '%'.$request->input('qf').'%')
            ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $qf = $request->input('qf');

            $cv = $cv->filter(function ($cv) use ($qualification, $qf) {
                if (in_array($cv->user_id, $qualification)) {
                    return in_array($cv->user_id, $qualification);
                }

                $string = strtolower($cv->main_qualification);
                $sub_string = strtolower($qf);
                $flag = false;
                if (strpos($string, $sub_string) !== false) {
                    $flag = true;
                }

                return $flag;
            });

            $searched_for[] = 'Qualification: '.$request->input('qf');

            $qualification_flag = true;
        }

        if ($request->has('rt') && $request->input('rt') != '-1') {
            $rt = $request->input('rt');
            //dd($rt);
            $cv = $cv->filter(function ($cv) use ($rt) {
                $user = User::find($cv->user_id);
                if (isset($user)) {
                    $resource = Resource::where('user_id', '=', $user->id)->first();
                    //dd($resource);
                    if (isset($resource)) {
                        return ($resource->resource_type > 0) && in_array($resource->resource_type, $rt);
                    }
                }
            });

            $resource_type_flag = true;
        }

        if ($request->has('po') && $request->input('po') != '-1') {
            $placement_options = $request->input('po');

            $cv = $cv->filter(function ($cv) use ($placement_options) {
                if ($cv->is_permanent == 1) {
                    return in_array(1, $placement_options);
                }

                if ($cv->is_temporary == 1) {
                    return in_array(2, $placement_options);
                }

                if ($cv->is_contract == 1) {
                    return in_array(3, $placement_options);
                }
            });

            $placement_options_flag = true;
        }

        if ($request->has('role') && $request->input('role') != '-1') {
            $roles = [];

            foreach ($request->input('role') as $key => $value) {
                $roles[] = (int) $value;
            }

            $cv = $cv->filter(function ($cv) use ($roles) {
                return in_array($cv->role_id, $roles);
            });

            $search_options = ScoutingRole::whereIn('id', $roles)->get();

            $tmp_search = 'Main Role: ';
            $not_empty_flag = false;
            foreach ($search_options as $search_option) {
                if ($not_empty_flag == true) {
                    $tmp_search .= ', '.$search_option->name;
                } else {
                    $tmp_search .= $search_option->name;
                }

                $not_empty_flag = true;
                $main_role_flag = true;
            }

            $searched_for[] = $tmp_search;
        }

        if ($request->has('n') && $request->input('n') != '') {
            $na = $request->input('n');

            $cv = $cv->filter(function ($cv) use ($na) {
                $string = strtolower($cv->nationality);
                $sub_string = strtolower($na);
                $flag = false;
                if (strpos($string, $sub_string) !== false) {
                    $flag = true;
                }

                return $flag;
            });

            $tmp_search = 'Nationality: '.$request->input('n');

            $nationality_flag = true;

            $searched_for[] = $tmp_search;
        }

        if ($request->has('ss') && $request->input('ss') != '') {
            //$search_word = strtolower($request->input('ss'));
            $skills_array = $request->input('ss');

            $cv = $cv->filter(function ($cv) use ($skills_array) {
                foreach ($cv->skill as $skill) {
                    if (in_array($skill->tool_id, $skills_array)) {
                        return true;
                    }
                    //return in_array($skill->tool_id, $skills_array);
                    /*if (strpos(strtolower($skill->res_skill), $search_word) == true) {
                        return true;
                    }*/
                }
            });

            $skills_flag = true;
        }

        if ($request->has('e') && $request->input('e') != '') {
            $experience = Experience::where('tools', 'like', '%'.$request->input('e').'%')
            ->orWhere('company', 'like', '%'.$request->input('e').'%')
            ->orWhere('current_project', 'like', '%'.$request->input('e').'%')
            ->orWhere('role', 'like', '%'.$request->input('e').'%')
            ->orWhere('project', 'like', '%'.$request->input('e').'%')
            ->orWhere('responsibility', 'like', '%'.$request->input('e').'%')
            ->select('res_id')->groupby('res_id')->distinct('res_id')->pluck('res_id')->toArray();

            $cv = $cv->filter(function ($cv) use ($experience) {
                return in_array($cv->user_id, $experience);
            });

            //Todo: find out what this experience is
        }

        $months_experince_drop_down[''] = 'All';
        for ($i = 6; $i < 244; $i += 6) {
            $months_experince_drop_down[$i] = '> '.$i.' months';
        }

        $rate_drop_down_tmp = [-1, 100, 200, 400, 500, 600, 700, 800, 900, 1000];
        foreach ($rate_drop_down_tmp as $key => $value) {
            $rate_drop_down[$value] = '< '.$value;
        }

        $rate_drop_down[-1] = 'All';

        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');

        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id');
        if ($request->has('p')) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->whereIn('profession_id', $request->input('p'))->where('name', '!=', '')->pluck('name', 'id');
        }
        $skills_drop_down = System::orderBy('description')->whereNotNull('description')->pluck('description', 'id');

        $hourly_rate_drop_down = ['' => ''];
        for ($i = 10; $i <= 10000; $i += 10) {
            $hourly_rate_drop_down[$i] = $i;
        }

        $money_drop_down = ['' => ''];
        for ($i = 500; $i <= 200000; $i += 500) {
            $money_drop_down[$i] = $i;
        }

        $cvWishListDropDown = CVWishList::orderBy('name')->pluck('name', 'id')->prepend('Please select', -1);

        /*Wishlist button permissions - Start*/
        $wishListModule = Module::where('name', '=', 'App\Models\WishList')->first();

        $can_view_wish_list = false;
        if (Auth::user()->canAccess($wishListModule->id, 'view_all') || Auth::user()->canAccess($wishListModule->id, 'view_team')) {
            $can_view_wish_list = true;
        }
        /*Wishlist button permissions - End*/

        $parameters = [
            'cv' => $cv,
            'cv_wish_lists' => $cv_wish_lists,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'money_drop_down' => $money_drop_down,
            'profession_flag' => $profession_flag,
            'speciality_flag' => $speciality_flag,
            'hourly_rate_minimum_flag' => $hourly_rate_minimum_flag,
            'hourly_rate_maximum_flag' => $hourly_rate_maximum_flag,
            'monthly_salary_minimum_flag' => $monthly_salary_minimum_flag,
            'monthly_salary_maximum_flag' => $monthly_salary_maximum_flag,
            'hourly_rate_drop_down' => $hourly_rate_drop_down,
            'searched_for' => $searched_for,
            'availability_drop_down' => AvailStatus::where('status_id', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'industry_drop_down' => Industry::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'resource_type_drop_down' => ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'skill_drop_down' => SkillLevel::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'role_drop_down' => ScoutingRole::where('status_id', '=', 1)->orderBy('name')->pluck('name', 'id'),
            'months_experince_drop_down' => $months_experince_drop_down,
            'rate_drop_down' => $rate_drop_down,
            'bbbee_race_drop_down' => BBBEERace::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'resource_type_drop_down' => ResourceType::where('status', '=', 1)->orderBy('description')->pluck('description', 'id')->prepend('All', -1),
            'skills_drop_down' => $skills_drop_down,
            'main_role_flag' => $main_role_flag,
            'availability_flag' => $availability_flag,
            'bbbe_flag' => $bbbe_flag,
            'experience_months_flag' => $experience_months_flag,
            'industry_flag' => $industry_flag,
            'nationality_flag' => $nationality_flag,
            'qualification_flag' => $qualification_flag,
            'resource_type_flag' => $resource_type_flag,
            'skill_level_flag' => $skill_level_flag,
            'disability_flag' => $disability_flag,
            'cvWishListDropDown' => $cvWishListDropDown,
            'skills_flag' => $skills_flag,
            'placement_options_flag' => $placement_options_flag,
            'can_view_wish_list' => $can_view_wish_list,
        ];

        return view('cv.search')->with($parameters);
    }

    public function create(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\CV')->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', '0');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', '0');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        $number_drop_down = [];
        for ($i = 0; $i <= 100; $i++) {
            $number_drop_down[] = $i;
        }

        $parameters = [
            'status_dropdown' => Status::orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Please select', '0'),
            'marital_dropdown' => MaritalStatus::pluck('description', 'id')->prepend('Please select', '0'),
            'res_type' => ResourceType::pluck('description', 'id')->prepend('Please select', '0'),
            'res_level' => ResourceLevel::pluck('description', 'id')->prepend('Please select', '0'),
            'naturalization' => Naturalization::pluck('description', 'id')->prepend('Please select', '0'),
            'role_dropdown' => ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', -1),
            'race' => Race::orderBy('description')->pluck('description', 'id')->prepend('Please select', '0'),
            'bbbee_race' => BBBEERace::orderBy('description')->pluck('description', 'id')->prepend('Please select', '0'),
            'gender' => Gender::orderBy('description')->pluck('description', 'id')->prepend('Please select', '0'),
            'disability' => Disability::orderBy('description')->pluck('description', 'id')->prepend('Please select', '0'),
            'resource' => $resource,
            'vendor_drop_down' => $vendor_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'user_id' => ($request->has('r')) ? $request->input('r') : 0,
            'template_drop_down' => AdvancedTemplates::where('template_type_id', '=', 2)->orderBy('id')->pluck('name', 'id')->prepend('Please select', '0'),
            'user' => User::find($request->input('r')),
            'number_drop_down' => $number_drop_down,
        ];

        return view('cv.create')->with($parameters);
    }

    public function store(StoreCvRequest $request): RedirectResponse
    {
        $cv = new CV;
        $cv->user_id = $request->input('user_id');
        $cv->resource_pref_name = $request->input('resource_pref_name');
        $cv->resource_middle_name = $request->input('resource_middle_name');
        $cv->id_no = $request->input('id_no');
        $cv->date_of_birth = $request->input('date_of_birth');
        $cv->place_of_birth = $request->input('place_of_birth');
        $cv->nationality = $request->input('nationality');
        $cv->passport_no = $request->input('passport_no');
        $cv->drivers_license = $request->input('drivers_license');
        $cv->criminal_offence = $request->input('criminal_offence');
        $cv->health = $request->input('health');
        $cv->marital_status_id = $request->input('marital_status');
        $cv->home_language = $request->input('home_language');
        $cv->other_language = $request->input('other_language');
        $cv->cell = $request->input('cell');
        $cv->cell2 = $request->input('cell2');
        $cv->fax = $request->input('fax');
        $cv->personal_email = $request->input('personal_email');
        $cv->main_qualification = $request->input('main_qualification');
        $cv->main_skill = $request->input('main_skill');
        $cv->contract_house = $request->input('contract_house');
        $cv->naturalization_id = $request->input('naturalization_id');
        $cv->race = $request->input('race');
        $cv->bbbee_race = $request->input('bbbee_race');
        $cv->gender = $request->input('gender');
        $cv->disability = $request->input('disability');
        $cv->role_id = $request->input('role_id');
        $cv->status_id = $request->input('status_id');
        $cv->creator_id = auth()->id();
        $cv->note = $request->input('note');
        $cv->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $cv->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $cv->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $cv->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $cv->charge_out_rate = $request->input('charge_out_rate');

        $cv->main_qualification = $request->input('main_qualification');
        $cv->vendor_id = $request->input('vendor_id');
        $cv->profession_id = $request->input('profession_id');
        $cv->speciality_id = $request->input('speciality_id');
        $cv->is_permanent = $request->input('permanent') && $request->input('permanent') > 0 ? $request->input('permanent') : 0;
        $cv->is_contract = $request->has('contracting') && $request->input('contracting') > 0 ? $request->input('contracting') : 0;
        $cv->is_temporary = $request->has('temporary') && $request->input('temporary') > 0 ? $request->input('temporary') : 0;

        $cv->current_location = $request->input('current_location');
        $cv->dependants = $request->input('dependants');
        $cv->own_transport = $request->input('own_transport');
        $cv->current_salary_nett = $request->input('current_salary_nett');
        $cv->expected_salary_nett = $request->input('expected_salary_nett');
        $cv->school_matriculated = $request->input('school_matriculated');
        $cv->highest_grade = $request->input('highest_grade');
        $cv->school_completion_year = $request->input('school_completion_year');

        $cv->save();

        $resource = Resource::where('user_id', '=', $request->user_id)->first();
        $resource->cv_id = $cv->id;
        $resource->save();

        activity()->on($cv)->log('created');

        return redirect(route('cv.show', ['cv' => $cv->id, 'resource' => $cv->user_id]))->with('flash_success', 'Cv captured successfully');
    }

    public function show(Cv $cv)
    {
        //Main CV
        $module = Module::where('name', '=', 'App\Models\CV')->first();

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            $can_update = true;
        }

        $cvAvalabilityModule = Module::where('name', '=', 'App\Models\CVAvailability')->first();

        $cvAvalabilityModuleCanView = false;
        if (Auth::user()->canAccess($cvAvalabilityModule->id, 'view_all') || Auth::user()->canAccess($cvAvalabilityModule->id, 'view_team')) {
            $cvAvalabilityModuleCanView = true;
        }

        $canCreateCvAvalability = false;
        if (Auth::user()->canAccess($cvAvalabilityModule->id, 'create_all') || Auth::user()->canAccess($cvAvalabilityModule->id, 'create_team')) {
            $canCreateCvAvalability = true;
        }

        $canUpdateCvAvalability = false;
        if (Auth::user()->canAccess(isset($cvAvalabilityModule->id) ? $cvAvalabilityModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvAvalabilityModule->id) ? $cvAvalabilityModule->id : -1, 'update_team')) {
            $canUpdateCvAvalability = true;
        }
        /*-----*/

        $cvExpenseModule = Module::where('name', '=', 'App\Models\CVExpense')->first();

        $cvExpenseModuleCanView = false;
        if (Auth::user()->canAccess($cvExpenseModule->id, 'view_all') || Auth::user()->canAccess($cvExpenseModule->id, 'view_team')) {
            $cvExpenseModuleCanView = true;
        }

        $canCreateCvExpense = false;
        if (Auth::user()->canAccess($cvExpenseModule->id, 'create_all') || Auth::user()->canAccess($cvExpenseModule->id, 'create_team')) {
            $canCreateCvExpense = true;
        }

        $canUpdateCvExpense = false;
        if (Auth::user()->canAccess(isset($cvExpenseModule->id) ? $cvExpenseModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvExpenseModule->id) ? $cvExpenseModule->id : -1, 'update_team')) {
            $canUpdateCvExpense = true;
        }
        /*---*/

        $cvIndustryExperienceModule = Module::where('name', '=', 'App\Models\CVIndustryExperience')->first();

        $cvIndustryExperienceModuleCanView = false;
        if (Auth::user()->canAccess($cvIndustryExperienceModule->id, 'view_all') || Auth::user()->canAccess($cvIndustryExperienceModule->id, 'view_team')) {
            $cvIndustryExperienceModuleCanView = true;
        }

        $canCreateCvIndustryExperience = false;
        if (Auth::user()->canAccess($cvIndustryExperienceModule->id, 'create_all') || Auth::user()->canAccess($cvIndustryExperienceModule->id, 'create_team')) {
            $canCreateCvIndustryExperience = true;
        }

        $canUpdateCvIndustryExperience = false;
        if (Auth::user()->canAccess(isset($cvIndustryExperienceModule->id) ? $cvIndustryExperienceModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvIndustryExperienceModule->id) ? $cvIndustryExperienceModule->id : -1, 'update_team')) {
            $canUpdateCvIndustryExperience = true;
        }
        /*---*/

        $cvPersonalInformationModule = Module::where('name', '=', 'App\Models\CVPersonalInformation')->first();

        $cvPersonalInformationModuleCanView = false;
        if (Auth::user()->canAccess($cvPersonalInformationModule->id, 'view_all') || Auth::user()->canAccess($cvPersonalInformationModule->id, 'view_team')) {
            $cvPersonalInformationModuleCanView = true;
        }

        $canCreateCvPersonalInformation = false;
        if (Auth::user()->canAccess($cvPersonalInformationModule->id, 'create_all') || Auth::user()->canAccess($cvPersonalInformationModule->id, 'create_team')) {
            $canCreateCvPersonalInformation = true;
        }

        $canUpdateCvPersonalInformation = false;
        if (Auth::user()->canAccess(isset($cvPersonalInformationModule->id) ? $cvPersonalInformationModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvPersonalInformationModule->id) ? $cvPersonalInformationModule->id : -1, 'update_team')) {
            $canUpdateCvPersonalInformation = true;
        }
        /*---*/

        $cvQualificationModule = Module::where('name', '=', 'App\Models\CVQualification')->first();

        $cvQualificationModuleCanView = false;
        if (Auth::user()->canAccess($cvQualificationModule->id, 'view_all') || Auth::user()->canAccess($cvQualificationModule->id, 'view_team')) {
            $cvQualificationModuleCanView = true;
        }

        $canCreateCvQualification = false;
        if (Auth::user()->canAccess($cvQualificationModule->id, 'create_all') || Auth::user()->canAccess($cvQualificationModule->id, 'create_team')) {
            $canCreateCvQualification = true;
        }

        $canUpdateCvQualification = false;
        if (Auth::user()->canAccess(isset($cvQualificationModule->id) ? $cvQualificationModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvQualificationModule->id) ? $cvQualificationModule->id : -1, 'update_team')) {
            $canUpdateCvQualification = true;
        }
        /*---*/

        $cvSkillModule = Module::where('name', '=', 'App\Models\CVSkill')->first();

        $cvSkillModuleCanView = false;
        if (Auth::user()->canAccess($cvSkillModule->id, 'view_all') || Auth::user()->canAccess($cvSkillModule->id, 'view_team')) {
            $cvSkillModuleCanView = true;
        }

        $canCreateCvSkill = false;
        if (Auth::user()->canAccess($cvSkillModule->id, 'create_all') || Auth::user()->canAccess($cvSkillModule->id, 'create_team')) {
            $canCreateCvSkill = true;
        }

        $canUpdateCvSkill = false;
        if (Auth::user()->canAccess(isset($cvSkillModule->id) ? $cvSkillModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvSkillModule->id) ? $cvSkillModule->id : -1, 'update_team')) {
            $canUpdateCvSkill = true;
        }
        /*---*/

        $scouting = Scouting::where('user_id', $cv->user_id)->first();

        if (isset($cv)) {
            $user = User::find($cv->user_id);
            $subject = 'CV - '.$user->first_name.' '.$user->last_name;

            $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', 0);
            $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
            $cv_references = CVReference::orderBy('id')->where('cv_id', '=', $cv->id)->get();
            $cv_quality = CVQuality::orderBy('id', 'desc')->where('cv_id', '=', $cv->id)->first();

            $resource = $user->resource;

            //Resource Compliance Tottal Percentage - Start
            $personal_info_perce = $this->complete_percentage('Cv', 'talent', $resource, 'user_id');
            $availability = Availability::where('res_id', '=', $resource?->user_id)->count();
            $availability = ($availability > 0) ? 100 : 0;
            $skill = Skill::where('res_id', '=', $resource->user_id)->count();
            $skill = ($skill != 0) ? ((($skill > 5) ? 5 : $skill) / 5) * 100 : 0;
            $qualification = Qualification::where('res_id', '=', $resource->user_id)->count();
            $qualification = ($qualification != 0) ? ((($qualification > 2) ? 2 : $qualification) / 2) * 100 : 0;
            $experience = Experience::where('res_id', '=', $resource->user_id)->count();
            $experience = ($experience != 0) ? ((($experience > 5) ? 5 : $experience) / 5) * 100 : 0;
            $industry_experience = IndustryExperience::where('res_id', '=', $resource->user_id)->count();
            $industry_experience = ($industry_experience != 0) ? ((($industry_experience > 5) ? 5 : $industry_experience) / 5) * 100 : 0;

            $total_compliance = number_format(((($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience) > 0) ? ($personal_info_perce + $availability + $skill + $qualification + $experience + $industry_experience) / 6 : 0), 0, '.', ',');
            //Resource Compliance Tottal Percentage - End

            $cv_checks = 0;
            if (isset($cv_quality)) {
                $cv_checks = (is_numeric($cv_quality->introduction_letter) ? $cv_quality->introduction_letter : 0) + (is_numeric($cv_quality->terms_and_conditions) ? $cv_quality->terms_and_conditions : 0) + (is_numeric($cv_quality->criminal_record) ? $cv_quality->criminal_record : 0) + (is_numeric($cv_quality->credit_check) ? $cv_quality->credit_check : 0);

                $cv_checks = $cv_checks / 4 * 100;
            }

            $reference_checked = 0;
            if ($cv_references->count() >= 1) {
                $reference_checked = 100;
            }

            $quality_percentage = number_format(($total_compliance + $cv_checks + $reference_checked) / 3, 2, '.', ',');

            if (! isset($cv_quality)) {
                $cv_quality = new CVQuality();
                $cv_quality->cv_id = $cv->id;
                $cv_quality->quality_percentage = $quality_percentage;
                $cv_quality->save();
            }

            $cv_quality = CVQuality::orderBy('id', 'desc')->where('cv_id', '=', $cv->id)->first();

            $quality_percentage = $cv_quality->quality_percentage;

            $selected_industry_eperiencies = IndustryExperience::orderBy('id')->where('res_id', '=', $resource->user_id)->pluck('ind_id', 'id');
            $industry_experience_dropdown = Industry::orderBy('id')->pluck('description', 'id')->prepend('Industry', '0');

            $parameters['cvAvalabilityModuleCanView'] = $cvAvalabilityModuleCanView;
            $parameters['cvExpenseModuleCanView'] = $cvExpenseModuleCanView;
            $parameters['cvIndustryExperienceModuleCanView'] = $cvIndustryExperienceModuleCanView;
            $parameters['cvPersonalInformationModuleCanView'] = $cvPersonalInformationModuleCanView;
            $parameters['cvQualificationModuleCanView'] = $cvQualificationModuleCanView;
            $parameters['cvSkillModuleCanView'] = $cvSkillModuleCanView;
            for ($i = 0; $i <= 100; $i++) {
                $number_drop_down[] = $i;
            }

            $cvTemplatesDropDown = CvTemplate::orderBy('id')->pluck('name', 'id');

            $config = Config::orderBy('id')->first();

            $documents = Document::where('reference_id', '=', $cv->id)->where('document_type_id', '=', 3)->get();

            $activities = Activity::with(['causer', 'subject'])->where('subject_type', '=', \App\Models\Cv::class)->where('subject_id', '=', $cv->id)->orderBy('id', 'desc')->get();

            $parameters = [
                'configDefaultCV' => isset($config->recruitment_default_cv_id) ? $config->recruitment_default_cv_id : 1,
                'subject' => $subject,
                'cv' => $cv,
                'activities' => $activities,
                'documents' => $documents,
                'cv_references' => $cv_references,
                'cv_quality' => $cv_quality,
                'quality_percentage' => $quality_percentage,
                'vendor_drop_down' => $vendor_drop_down,
                'profession_drop_down' => $profession_drop_down,
                'speciality_drop_down' => $speciality_drop_down,
                'availability' => Availability::where('res_id', '=', $resource->user_id)->orderBy('status_date', 'desc')->get(),
                'qualification' => Qualification::where('res_id', '=', $resource->user_id)->get(),
                'skill' => Skill::where('res_id', '=', $resource->user_id)->orderBy('row_order')->get(),
                'experience' => Experience::where('res_id', '=', $resource->user_id)->get(),
                'iexperience' => IndustryExperience::where('res_id', '=', $resource->user_id)->get(),
                'industry_experience_ids' => IndustryExperience::where('res_id', '=', $resource->user_id)->pluck('id')->toArray(),
                'role_dropdown' => ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', -1),
                'industry_dropdown' => Industry::orderBy('id')->pluck('description', 'id'),
                'template_drop_down' => AdvancedTemplates::where('template_type_id', '=', 2)->orderBy('id')->pluck('name', 'id')->prepend('Template', '0'),
                'selected_industry_eperiencies' => $selected_industry_eperiencies,
                'industry_experience_dropdown' => $industry_experience_dropdown,
                'cvAvalabilityModuleCanView' => $cvAvalabilityModuleCanView,
                'cvExpenseModuleCanView' => $cvExpenseModuleCanView,
                'cvIndustryExperienceModuleCanView' => $cvIndustryExperienceModuleCanView,
                'cvPersonalInformationModuleCanView' => $cvPersonalInformationModuleCanView,
                'cvQualificationModuleCanView' => $cvQualificationModuleCanView,
                'cvSkillModuleCanView' => $cvSkillModuleCanView,
                'canCreateCvAvalability' => $canCreateCvAvalability,
                'canUpdateCvAvalability' => $canUpdateCvAvalability,
                'canCreateCvExpense' => $canCreateCvExpense,
                'canUpdateCvExpense' => $canUpdateCvExpense,
                'canCreateCvIndustryExperience' => $canCreateCvIndustryExperience,
                'canUpdateCvIndustryExperience' => $canUpdateCvIndustryExperience,
                'canCreateCvPersonalInformation' => $canCreateCvPersonalInformation,
                'canUpdateCvPersonalInformation' => $canUpdateCvPersonalInformation,
                'canCreateCvQualification' => $canCreateCvQualification,
                'canUpdateCvQualification' => $canUpdateCvQualification,
                'canCreateCvSkill' => $canCreateCvSkill,
                'canUpdateCvSkill' => $canUpdateCvSkill,
                'can_create' => $can_create,
                'can_update' => $can_update,
                'scouting_id' => $scouting,
                'number_drop_down' => $number_drop_down,
                'cvTemplatesDropDown' => $cvTemplatesDropDown,
            ];

            return view('cv.show')->with($parameters);
        } else {
            return redirect()->back()->with('flash_danger', 'The CV you\'re looking for was not found');
        }
    }

    public function edit($cvid, $res_id): View
    {
        //Main CV
        $module = Module::where('name', '=', 'App\Models\CV')->first();

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            $can_update = true;
        }

        $cvAvalabilityModule = Module::where('name', '=', 'App\Models\CVAvailability')->first();

        $cvAvalabilityModuleCanView = false;
        if (Auth::user()->canAccess($cvAvalabilityModule->id, 'view_all') || Auth::user()->canAccess($cvAvalabilityModule->id, 'view_team')) {
            $cvAvalabilityModuleCanView = true;
        }

        $canCreateCvAvalability = false;
        if (Auth::user()->canAccess($cvAvalabilityModule->id, 'create_all') || Auth::user()->canAccess($cvAvalabilityModule->id, 'create_team')) {
            $canCreateCvAvalability = true;
        }

        $canUpdateCvAvalability = false;
        if (Auth::user()->canAccess(isset($cvAvalabilityModule->id) ? $cvAvalabilityModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvAvalabilityModule->id) ? $cvAvalabilityModule->id : -1, 'update_team')) {
            $canUpdateCvAvalability = true;
        }
        /*-----*/

        $cvExpenseModule = Module::where('name', '=', 'App\Models\CVExpense')->first();

        $cvExpenseModuleCanView = false;
        if (Auth::user()->canAccess($cvExpenseModule->id, 'view_all') || Auth::user()->canAccess($cvExpenseModule->id, 'view_team')) {
            $cvExpenseModuleCanView = true;
        }

        $canCreateCvExpense = false;
        if (Auth::user()->canAccess($cvExpenseModule->id, 'create_all') || Auth::user()->canAccess($cvExpenseModule->id, 'create_team')) {
            $canCreateCvExpense = true;
        }

        $canUpdateCvExpense = false;
        if (Auth::user()->canAccess(isset($cvExpenseModule->id) ? $cvExpenseModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvExpenseModule->id) ? $cvExpenseModule->id : -1, 'update_team')) {
            $canUpdateCvExpense = true;
        }
        /*---*/

        $cvIndustryExperienceModule = Module::where('name', '=', 'App\Models\CVIndustryExperience')->first();

        $cvIndustryExperienceModuleCanView = false;
        if (Auth::user()->canAccess($cvIndustryExperienceModule->id, 'view_all') || Auth::user()->canAccess($cvIndustryExperienceModule->id, 'view_team')) {
            $cvIndustryExperienceModuleCanView = true;
        }

        $canCreateCvIndustryExperience = false;
        if (Auth::user()->canAccess($cvIndustryExperienceModule->id, 'create_all') || Auth::user()->canAccess($cvIndustryExperienceModule->id, 'create_team')) {
            $canCreateCvIndustryExperience = true;
        }

        $canUpdateCvIndustryExperience = false;
        if (Auth::user()->canAccess(isset($cvIndustryExperienceModule->id) ? $cvIndustryExperienceModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvIndustryExperienceModule->id) ? $cvIndustryExperienceModule->id : -1, 'update_team')) {
            $canUpdateCvIndustryExperience = true;
        }
        /*---*/

        $cvPersonalInformationModule = Module::where('name', '=', 'App\Models\CVPersonalInformation')->first();

        $cvPersonalInformationModuleCanView = false;
        if (Auth::user()->canAccess($cvPersonalInformationModule->id, 'view_all') || Auth::user()->canAccess($cvPersonalInformationModule->id, 'view_team')) {
            $cvPersonalInformationModuleCanView = true;
        }

        $canCreateCvPersonalInformation = false;
        if (Auth::user()->canAccess($cvPersonalInformationModule->id, 'create_all') || Auth::user()->canAccess($cvPersonalInformationModule->id, 'create_team')) {
            $canCreateCvPersonalInformation = true;
        }

        $canUpdateCvPersonalInformation = false;
        if (Auth::user()->canAccess(isset($cvPersonalInformationModule->id) ? $cvPersonalInformationModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvPersonalInformationModule->id) ? $cvPersonalInformationModule->id : -1, 'update_team')) {
            $canUpdateCvPersonalInformation = true;
        }
        /*---*/

        $cvQualificationModule = Module::where('name', '=', 'App\Models\CVQualification')->first();

        $cvQualificationModuleCanView = false;
        if (Auth::user()->canAccess($cvQualificationModule->id, 'view_all') || Auth::user()->canAccess($cvQualificationModule->id, 'view_team')) {
            $cvQualificationModuleCanView = true;
        }

        $canCreateCvQualification = false;
        if (Auth::user()->canAccess($cvQualificationModule->id, 'create_all') || Auth::user()->canAccess($cvQualificationModule->id, 'create_team')) {
            $canCreateCvQualification = true;
        }

        $canUpdateCvQualification = false;
        if (Auth::user()->canAccess(isset($cvQualificationModule->id) ? $cvQualificationModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvQualificationModule->id) ? $cvQualificationModule->id : -1, 'update_team')) {
            $canUpdateCvQualification = true;
        }
        /*---*/

        $cvSkillModule = Module::where('name', '=', 'App\Models\CVSkill')->first();

        $cvSkillModuleCanView = false;
        if (Auth::user()->canAccess($cvSkillModule->id, 'view_all') || Auth::user()->canAccess($cvSkillModule->id, 'view_team')) {
            $cvSkillModuleCanView = true;
        }

        $canCreateCvSkill = false;
        if (Auth::user()->canAccess($cvSkillModule->id, 'create_all') || Auth::user()->canAccess($cvSkillModule->id, 'create_team')) {
            $canCreateCvSkill = true;
        }

        $canUpdateCvSkill = false;
        if (Auth::user()->canAccess(isset($cvSkillModule->id) ? $cvSkillModule->id : -1, 'update_all') || Auth::user()->canAccess(isset($cvSkillModule->id) ? $cvSkillModule->id : -1, 'update_team')) {
            $canUpdateCvSkill = true;
        }
        /*---*/

        $cv = Cv::where('id', '=', $cvid)->first();
        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('Please Select', '0');
        $profession_drop_down = Profession::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please Select', '0');
        if (isset($cv->profession_id) && ($cv->profession_id > 0)) {
            $speciality_drop_down = Speciality::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('profession_id', '=', $cv->profession_id)->pluck('name', 'id')->prepend('Please Select', '0');
        }

        $number_drop_down = [];
        for ($i = 0; $i <= 100; $i++) {
            $number_drop_down[] = $i;
        }

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Please select', '0'),
            'marital_dropdown' => MaritalStatus::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'res_type' => ResourceType::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'res_level' => ResourceLevel::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'naturalization' => Naturalization::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'race' => Race::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'bbbee_race' => BBBEERace::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'gender' => Gender::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'disability' => Disability::orderBy('id')->pluck('description', 'id')->prepend('Please select', '0'),
            'role_dropdown' => ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select', '0'),
            'cv' => $cv,
            'res' => $res_id,
            'vendor_drop_down' => $vendor_drop_down,
            'profession_drop_down' => $profession_drop_down,
            'speciality_drop_down' => $speciality_drop_down,
            'availability' => Availability::where('res_id', '=', $res_id)->get(),
            'qualification' => Qualification::where('res_id', '=', $res_id)->get(),
            'skill' => Skill::where('res_id', '=', $res_id)->get(),
            'experience' => Experience::where('res_id', '=', $res_id)->get(),
            'iexperience' => IndustryExperience::where('res_id', '=', $res_id)->get(),
            'resource' => $resource,
            'template_drop_down' => AdvancedTemplates::where('template_type_id', '=', 2)->orderBy('id')->pluck('name', 'id')->prepend('Template', '0'),
            'cvAvalabilityModuleCanView' => $cvAvalabilityModuleCanView,
            'cvExpenseModuleCanView' => $cvExpenseModuleCanView,
            'cvIndustryExperienceModuleCanView' => $cvIndustryExperienceModuleCanView,
            'cvPersonalInformationModuleCanView' => $cvPersonalInformationModuleCanView,
            'cvQualificationModuleCanView' => $cvQualificationModuleCanView,
            'cvSkillModuleCanView' => $cvSkillModuleCanView,
            'canCreateCvAvalability' => $canCreateCvAvalability,
            'canUpdateCvAvalability' => $canUpdateCvAvalability,
            'canCreateCvExpense' => $canCreateCvExpense,
            'canUpdateCvExpense' => $canUpdateCvExpense,
            'canCreateCvIndustryExperience' => $canCreateCvIndustryExperience,
            'canUpdateCvIndustryExperience' => $canUpdateCvIndustryExperience,
            'canCreateCvPersonalInformation' => $canCreateCvPersonalInformation,
            'canUpdateCvPersonalInformation' => $canUpdateCvPersonalInformation,
            'canCreateCvQualification' => $canCreateCvQualification,
            'canUpdateCvQualification' => $canUpdateCvQualification,
            'canCreateCvSkill' => $canCreateCvSkill,
            'canUpdateCvSkill' => $canUpdateCvSkill,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'industry_experience_dropdown' => Industry::orderBy('id')->pluck('description', 'id')->prepend('Industry', '0'),
            'selected_industry_eperiencies' => IndustryExperience::orderBy('id')->where('res_id', '=', $res_id)->pluck('ind_id', 'id'),
            'number_drop_down' => $number_drop_down,
        ];

        return view('cv.edit')->with($parameters);
    }

    public function update(UpdateCvRequest $request, $cvid): RedirectResponse
    {
        $path = DB::select(DB::raw('select absolute_path from configs')->getValue(DB::connection()->getQueryGrammar()));

        $cv = CV::find($cvid);
        $cv->resource_pref_name = $request->input('resource_pref_name');
        $cv->resource_middle_name = $request->input('resource_middle_name');
        $cv->id_no = $request->input('id_no');
        $cv->date_of_birth = $request->input('date_of_birth');
        $cv->place_of_birth = $request->input('place_of_birth');
        $cv->nationality = $request->input('nationality');
        $cv->passport_no = $request->input('passport_no');
        $cv->drivers_license = $request->input('drivers_license');
        $cv->criminal_offence = $request->input('criminal_offence');
        $cv->health = $request->input('health');
        $cv->marital_status_id = $request->input('marital_status');
        $cv->home_language = $request->input('home_language');
        $cv->other_language = $request->input('other_language');
        $cv->cell = $request->input('cell');
        $cv->cell2 = $request->input('cell2');
        $cv->fax = $request->input('fax');
        $cv->personal_email = $request->input('personal_email');
        $cv->main_qualification = $request->input('main_qualification');
        $cv->main_skill = $request->input('main_skill');
        $cv->contract_house = $request->input('contract_house');
        $cv->naturalization_id = $request->input('naturalization_id');
        $cv->race = $request->input('race');
        $cv->bbbee_race = $request->input('bbbee_race');
        $cv->gender = $request->input('gender');
        $cv->disability = $request->input('disability');
        $cv->status_id = $request->input('status_id');
        $cv->creator_id = auth()->id();
        $cv->note = $request->input('note');
        $cv->hourly_rate_minimum = $request->input('hourly_rate_minimum');
        $cv->hourly_rate_maximum = $request->input('hourly_rate_maximum');
        $cv->monthly_salary_minimum = $request->input('monthly_salary_minimum');
        $cv->monthly_salary_maximum = $request->input('monthly_salary_maximum');
        $cv->charge_out_rate = $request->input('charge_out_rate');

        $cv->main_qualification = $request->input('main_qualification');
        $cv->role_id = $request->input('role_id');
        $cv->vendor_id = $request->input('vendor_id');
        $cv->profession_id = $request->input('profession_id');
        $cv->speciality_id = $request->input('speciality_id');
        $cv->is_permanent = $request->input('permanent') && $request->input('permanent') > 0 ? $request->input('permanent') : 0;
        $cv->is_contract = $request->has('contracting') && $request->input('contracting') > 0 ? $request->input('contracting') : 0;
        $cv->is_temporary = $request->has('temporary') && $request->input('temporary') > 0 ? $request->input('temporary') : 0;

        $cv->current_location = $request->input('current_location');
        $cv->dependants = $request->input('dependants');
        $cv->own_transport = $request->input('own_transport');
        $cv->current_salary_nett = $request->input('current_salary_nett');
        $cv->expected_salary_nett = $request->input('expected_salary_nett');
        $cv->school_matriculated = $request->input('school_matriculated');
        $cv->highest_grade = $request->input('highest_grade');
        $cv->school_completion_year = $request->input('school_completion_year');

        $cv->save();

        //activity()->on($cv)->log('updated');

        return redirect(route('cv.index'))->with('flash_success', 'Cv updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $user = Resource::where('cv_id', $id)->first();

        if (isset($user)) {
            $user->cv_id = null;
            $user->save();
        }

        $cv = Cv::find($id);
        activity()->on($cv)->withProperties([
            'name' => substr(auth()->user()->first_name, 0, 1).'. '.auth()->user()->last_name.' deleted '
                .substr($cv->user->first_name, 0, 1).'. '.$cv->user->last_name.'\'s CV',
        ])->log('deleted');
        DB::table('talent')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Cv::destroy($id);

        return redirect()->route('cv.index')->with('success', 'Cv deleted successfully');
    }

    public function user(): View
    {
        $resources = Resource::where('cv_id', '=', null)->select('user_id')->with('user',function ($q){
            $q->orderBy('first_name');
        })->get();
        //dd($resources);
        $user = [];
        foreach ($resources as $resource) {
            if (isset($resource->user_id) && $resource->user_id != null) {
                $user[$resource->user_id] = $resource->user->first_name.' '.$resource->user->last_name;
            }
        }

        asort($user);

        $parameters = [
            'resource' => $user,
            //'resource' => $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')
        ];

        return view('cv.user')->with($parameters);
    }

    public function processTemplate(Cv $resource, $template_file)
    {
        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext != 'docx') {
            return $template_file;
        }

        $cv = CV::with('user', 'cv', 'raced', 'genderd', 'naturalizationd', 'marital_status', 'bbbee_raced', 'disabilityd', 'raced', 'skill', 'skill.skill_leveld', 'skill.tool_idd', 'availability', 'availability.avail_statusd', 'qualification', 'experience', 'experience.cv_companyd', 'iexperience.industry')->where('id', '=', $resource->id)->get()->ToArray();
        //dd($cv);
        //

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/'.$template_file));

        $variables = ['{CVFIRSTNAME}', '{CVLASTNAME}', '{CVPHONE}', '{CVRESOURCEPREFNAME}', '{CVIDNO}', '{CVPASSPORTNO}', '{CVNATIONALITY}', '{CVRACE}', '{CVGENDER}', '{CVHEALTH}', '{CVDOB}', '{CVPLACEOFBIRTH}', '{CVOTHERLANGUAGE}', '{CVMAINQUALIFICATION}', '{CVMAINSKILL}'];
        //$variables = Array('{UFIRSTNAME}', '{ULASTNAME}', '{URESPREFNAME}', '{UIDNO}', '{UPASSPORTNO}', '{UNATIONALITY}', '{URACE}', '{UGENDER}', '{UHEALTH}', '{UDOB}', '{UPLACEOFBIRTH}', '{UOTHERLANGUAGE}', '{UDRIVERSLICENSE}', '{UMAINQUALIFICATION}', '{UMAINSKILL}', '{URESLEVEL}', '{URESPOSITION}');
        $replaced_with = [($cv[0]['user']['first_name'] ? $cv[0]['user']['first_name'] : ''), ($cv[0]['user']['last_name'] ? $cv[0]['user']['last_name'] : ''), ($cv[0]['user']['phone'] ? $cv[0]['user']['phone'] : ''), ($cv[0]['resource_pref_name'] ? $cv[0]['resource_pref_name'] : ''), ($cv[0]['id_no'] ? $cv[0]['id_no'] : ''), ($cv[0]['passport_no'] ? $cv[0]['passport_no'] : ''), ($cv[0]['nationality'] ? $cv[0]['nationality'] : ''), ($cv[0]['raced']['description'] ? $cv[0]['raced']['description'] : ''), ($cv[0]['genderd']['description'] ? $cv[0]['genderd']['description'] : ''), ($cv[0]['health'] ? $cv[0]['health'] : ''), ($cv[0]['date_of_birth'] ? $cv[0]['date_of_birth'] : ''), ($cv[0]['place_of_birth'] ? $cv[0]['place_of_birth'] : ''), ($cv[0]['home_language'] ? $cv[0]['home_language'] : ''), ($cv[0]['main_qualification'] ? $cv[0]['main_qualification'] : ''), ($cv[0]['main_skill'] ? $cv[0]['main_skill'] : '')];
        //$replaced_with = Array($cv[0]['user']['first_name'], $cv[0]['user']['last_name'], $cv[0]['res_pref_name'], $cv[0]['id_no'], $cv[0]['nationality'], $cv[0]['race'], $cv[0]['gender'], $cv[0]['health'], $cv[0]['dob'], $cv[0]['place_of_birth'], $cv[0]['home_language'], $cv[0]['drivers_license'], $cv[0]['main_qualification'], $cv[0]['main_skill'], $cv[0]['res_level'], $cv[0]['res_position']);
        //$content = str_replace($variables, $replaced_with, $content);
        $templateProcessor->setValue($variables, $replaced_with);

        $variables = ['{CVCONTRACTHOUSE}', '{CVNATURALIZATION}', '{CVBBBEERACE}', '{CVDISABILITY}', '{CVMARITALSTATUS}', '{CVHOMELANGUAGE}', '{CVDATEOFBIRTH}', '{CVCRIMINALOFFENCE}', '{CVDRIVERSLICENSE}', '{CVCELL2}', '{CVPERSONALEMAIL}'];
        $replaced_with = [($cv[0]['contract_house'] ? $cv[0]['contract_house'] : ''), ($cv[0]['naturalizationd']['description'] ? $cv[0]['naturalizationd']['description'] : ''), ($cv[0]['bbbee_raced']['description'] ? $cv[0]['bbbee_raced']['description'] : ''), ($cv[0]['disabilityd']['description'] ? $cv[0]['disabilityd']['description'] : ''), ($cv[0]['marital_status']['description'] ? $cv[0]['marital_status']['description'] : ''), ($cv[0]['home_language'] ? $cv[0]['home_language'] : ''), ($cv[0]['date_of_birth'] ? $cv[0]['date_of_birth'] : ''), ($cv[0]['criminal_offence'] ? $cv[0]['criminal_offence'] : ''), ($cv[0]['drivers_license'] ? $cv[0]['drivers_license'] : ''), ($cv[0]['cell2'] ? $cv[0]['cell2'] : ''), ($cv[0]['personal_email'] ? $cv[0]['personal_email'] : '')];
        //$content = str_replace($variables, $replaced_with, $content);
        $templateProcessor->setValue($variables, $replaced_with);

        $variables = ['{CVEMAIL}', '{CVFAX}', '{CVCELL}'];
        $replaced_with = [($cv[0]['user']['email'] ? $cv[0]['user']['email'] : ''), ($cv[0]['fax'] ? $cv[0]['fax'] : ''), ($cv[0]['user']['phone'] ? $cv[0]['user']['phone'] : '')];
        //$content = str_replace($variables, $replaced_with, $content);
        $templateProcessor->setValue($variables, $replaced_with);

        if (count($cv[0]['availability']) > 0) {
            for ($i = 0; $i < count($cv[0]['availability']); $i++) {
                $variables = ['{AAVAILSTATUS}', '{ASTATUSDATE}', '{AAVAILNOTE}', '{AAVAILDATE}', '{ARATEHOUR}', '{ARATEMONTH}'];
                $replaced_with = [$cv[0]['availability'][$i]['avail_statusd']['description'], $cv[0]['availability'][$i]['status_date'], $cv[0]['availability'][$i]['avail_note'], $cv[0]['availability'][$i]['avail_date'], $cv[0]['availability'][$i]['rate_hour'], $cv[0]['availability'][$i]['rate_month']];
                //$content = str_replace($variables, $replaced_with, $content);
                $templateProcessor->setValue($variables, $replaced_with);
            }
        } else {
            $variables = ['{AAVAILSTATUS}', '{ASTATUSDATE}', '{AAVAILNOTE}', '{AAVAILDATE}', '{ARATEHOUR}', '{ARATEMONTH}'];
            $replaced_with = ['', '', '', '', '', ''];
            //$content = str_replace($variables, $replaced_with, $content);
            $templateProcessor->setValue($variables, $replaced_with);
        }
        //dd($cv);

        if (count($cv[0]['skill']) > 0) {
            for ($i = 0; $i < count($cv[0]['skill']); $i++) {
                $variables = ['{SRESSKILL}', '{STOOLID}', '{STOOL}', '{SYEARSEXPERIENCE}', '{SSKILLLEVEL}'];
                $replaced_with = [$cv[0]['skill'][$i]['res_skill'], $cv[0]['skill'][$i]['tool_idd']['description'], $cv[0]['skill'][$i]['tool'], $cv[0]['skill'][$i]['years_experience'], $cv[0]['skill'][$i]['skill_leveld']['description']];
                //$content = str_replace($variables, $replaced_with, $content);
                $templateProcessor->setValue($variables, $replaced_with);
            }
        } else {
            $variables = ['{SRESSKILL}', '{STOOLID}', '{STOOL}', '{SYEARSEXPERIENCE}', '{SSKILLLEVEL}'];
            $replaced_with = ['', '', '', '', ''];
            //$content = str_replace($variables, $replaced_with, $content);
            $templateProcessor->setValue($variables, $replaced_with);
        }

        if (count($cv[0]['qualification']) > 0) {
            for ($i = 0; $i < count($cv[0]['qualification']); $i++) {
                $variables = ['{QQUALIFICATION}', '{QSUBJECTS}', '{QINSTITUTION}', '{QYEARCOMPLETE}'];
                $replaced_with = [($cv[0]['qualification'][$i]['qualification'] ? $cv[0]['qualification'][$i]['qualification'] : '&nbsp;'), ($cv[0]['qualification'][$i]['subjects'] ? $cv[0]['qualification'][$i]['subjects'] : ''), ($cv[0]['qualification'][$i]['institution'] ? $cv[0]['qualification'][$i]['institution'] : ''), ($cv[0]['qualification'][$i]['year_complete'] ? $cv[0]['qualification'][$i]['year_complete'] : '')];
                //$content = str_replace($variables, $replaced_with, $content);
                $templateProcessor->setValue($variables, $replaced_with);
            }
        } else {
            $variables = ['{QQUALIFICATION}', '{QSUBJECTS}', '{QINSTITUTION}', '{QYEARCOMPLETE}'];
            $replaced_with = ['', '', '', ''];
            //$content = str_replace($variables, $replaced_with, $content);
            $templateProcessor->setValue($variables, $replaced_with);
        }

        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        if (count($cv[0]['experience']) > 0) {
            for ($i = 0; $i < count($cv[0]['experience']); $i++) {
                $variables = ['{ECOMPANY}', '{EPERIOD}', '{EPERIODSTART}', '{EPERIODEND}', '{ECURRENTPROJECT}', '{EROLE}', '{EPROJECT}', '{ERESPONSIBILITY}', '{ETOOLS}'];
                $replaced_with = [($cv[0]['experience'][$i]['cv_companyd'] == null ? $cv[0]['experience'][$i]['company'] : $cv[0]['experience'][$i]['cv_companyd']['description']), $cv[0]['experience'][$i]['period'], /*$cv[0]['experience'][$i]['period_start']*/$months[substr($cv[0]['experience'][$i]['period_start'], 4, 6)].' '.substr($cv[0]['experience'][$i]['period_start'], 0, 4), ($cv[0]['experience'][$i]['period_end'] > 0 ? $months[substr($cv[0]['experience'][$i]['period_end'], 4, 6)].' '.substr($cv[0]['experience'][$i]['period_end'], 0, 4)/*$cv[0]['experience'][$i]['period_end']*/ : ''), ($cv[0]['experience'][$i]['current_project'] == '1' ? 'Yes' : 'No'), $cv[0]['experience'][$i]['role'], $cv[0]['experience'][$i]['project'], ($cv[0]['experience'][$i]['responsibility'] ? $cv[0]['experience'][$i]['responsibility'] : ''), $cv[0]['experience'][$i]['tools']];
                //$content = str_replace($variables, $replaced_with, $content);
                $templateProcessor->setValue($variables, $replaced_with);
            }
        } else {
            $variables = ['{ECOMPANY}', '{EPERIOD}', '{EPERIODSTART}', '{EPERIODEND}', '{ECURRENTPROJECT}', '{EROLE}', '{EPROJECT}', '{ERESPONSIBILITY}', '{ETOOLS}'];
            $replaced_with = ['', '', '', '', '', '', '', '', ''];
            //$content = str_replace($variables, $replaced_with, $content);
            $templateProcessor->setValue($variables, $replaced_with);
        }

        if (count($cv[0]['iexperience']) > 0) {
            for ($i = 0; $i < count($cv[0]['iexperience']); $i++) {
                $variables = ['{IEDESCRIPTION}'];
                $replaced_with = [($cv[0]['iexperience'][$i]['industry']['description'] ? $cv[0]['iexperience'][$i]['industry']['description'] : '')];
                //$content = str_replace($variables, $replaced_with, $content);
                $templateProcessor->setValue($variables, $replaced_with);
            }
        } else {
            $variables = ['{IEDESCRIPTION}'];
            $replaced_with = [''];
            //$content = str_replace($variables, $replaced_with, $content);
            $templateProcessor->setValue($variables, $replaced_with);
        }

        $templateProcessor->setValue($variables, $replaced_with);

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $cv_template_path = 'cvs/'.date('Y').DIRECTORY_SEPARATOR.date('m');
        if (! File::exists(storage_path('app/templates/'.$cv_template_path))) {
            Storage::makeDirectory('templates/'.$cv_template_path);
        }

        $cv_template = $cv_template_path.DIRECTORY_SEPARATOR.'cv_'.$resource->id.'_'.date('Y_m_d_H_i_s').'_'.uniqid().'.docx';

        $templateProcessor->saveAs(storage_path('app/templates/'.$cv_template));

        return $cv_template;
    }

    public function download($cvid, $res_id, $template_id)
    {
        $config = Config::first();
        $data = [];
        $resource = Cv::find($cvid);
        $template = AdvancedTemplates::find($template_id);

        $file = $this->processTemplate($resource, $template->file);

        $data['file'] = $file;
        /*      $template = TempTemplates::find($cv_id);
                $file_name_pdf = "app\\public\\templates\\file_".date("Y_m_d_H_i_s").".pdf";
                return PDF::loadFile(storage_path($template->file_url))->save(storage_path($file_name_pdf))->stream('download.pdf');*/

        //dd(storage_path('app/templates/' . $file));
        //return response()->file(storage_path('app/templates/' . $file), ['Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
        //return $file->file;
        return response()->json($data);
    }

    public function get_user(Request $request): JsonResponse
    {
        $user = User::find($request->user_id);

        return response()->json($user);
    }

    public function generateCV($cv_id, Request $request)
    {
        $cv = CV::with(['user', 'qualification', 'skill', 'experience', 'role', 'availability'])->where('id', '=', $cv_id)->first();
        $skill_level = SkillLevel::orderBy('id')->pluck('description', 'id')->toArray();
        $tool = System::orderBy('id')->pluck('description', 'id')->toArray();

        $config = Config::orderBy('id')->first();
        $company = Company::find($config->recruitment_default_company_id);

        $availability = null;
        $latest_date = null;
        foreach ($cv->availability as $availability) {
            if ($latest_date == null) {
                $latest_date = $availability->avail_date;
                $availability = $availability;
            } else {
                if (date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))) {
                    $latest_date = $availability->avail_date;
                    $availability = $availability;
                }
            }
        }

        $data = $request->all();

        $cv_type = $data['data']['cv_type'];
        $cv_format = $data['data']['cv_format'];
        $named_cv = $data['data']['named_cv'];

        //If is word document
        if ($cv_format == 2) {
            $cvTemplate = CvTemplate::find($cv_type);
            if ($cv_type == 2) {
                $templateProcessor = new TemplateProcessor('Standard_CV.docx');
            }

            if ($cv_type == 4) {
                $templateProcessor = new TemplateProcessor('Comprehensive_CV_Template.docx');
            }

            if ($named_cv == 2) {
                $templateProcessor->setValue('firstName', 'Candidate #'.$cv->id);
                $templateProcessor->setValue('lastName', '');
            } else {
                $templateProcessor->setValue('firstName', isset($cv->user->first_name) ? $cv->user->first_name : '');
                $templateProcessor->setValue('lastName', isset($cv->user->last_name) ? $cv->user->last_name : '');
            }

            $templateProcessor->setValue('mainRole', isset($cv->role->name) ? $cv->role->name : '');
            $templateProcessor->setValue('defaultCompanyName', isset($company->company_name) ? $company->company_name : '');
            $templateProcessor->setValue('recruitmentDefaultContactNumber', isset($config->recruitment_default_contact_number) ? $config->recruitment_default_contact_number : '');
            $templateProcessor->setValue('gender', isset($cv->genderd->description) ? $cv->genderd->description : '');
            $templateProcessor->setValue('race', isset($cv->raced->description) ? $cv->raced->description : '');
            $templateProcessor->setValue('nationality', isset($cv->nationality) ? $cv->nationality : '');
            $templateProcessor->setValue('DOB', isset($cv->date_of_birth) ? $cv->date_of_birth : '');
            $templateProcessor->setValue('ID', isset($cv->id_no) ? $cv->id_no : '');
            $templateProcessor->setValue('driversLicence', isset($cv->drivers_license) ? $cv->drivers_license : '');
            $templateProcessor->setValue('ownTransport', isset($cv->own_transport) && ($cv->own_transport == 1) ? 'Yes' : 'No');
            $templateProcessor->setValue('disability', isset($cv->disabilityd->description) ? $cv->disabilityd->description : '');
            $templateProcessor->setValue('homeLanguage', isset($cv->home_language) ? $cv->home_language : '');
            $templateProcessor->setValue('otherLanguage', isset($cv->other_language) ? $cv->other_language : '');
            $templateProcessor->setValue('currentLocation', isset($cv->current_location) ? $cv->current_location : '');
            $templateProcessor->setValue('maritalStatus', isset($cv->marital_status->description) ? $cv->marital_status->description : '');
            $templateProcessor->setValue('dependants', isset($cv->dependants) ? $cv->dependants : '');
            $templateProcessor->setValue('availableStatus', isset($availability->avail_statusd->description) ? $availability->avail_statusd->description : '');
            $templateProcessor->setValue('availableDate', isset($availability->avail_date) ? $availability->avail_date : '');
            $templateProcessor->setValue('rateMonth', isset($availability->rate_month) ? $cv->rate_month : '');
            $templateProcessor->setValue('rateHour', isset($availability->rate_hour) ? $cv->rate_hour : '');
            $templateProcessor->setValue('currentSalaryNett', isset($cv->current_salary_nett) ? $cv->current_salary_nett : '');
            $templateProcessor->setValue('monthlySalaryMinimum', isset($cv->monthly_salary_minimum) ? $cv->monthly_salary_minimum : '');
            $templateProcessor->setValue('monthlySalaryMaximum', isset($cv->monthly_salary_maximum) ? $cv->monthly_salary_maximum : '');
            $templateProcessor->setValue('monthlyRateMinimum', '');
            $templateProcessor->setValue('monthlyRateMaximum', '');
            $templateProcessor->setValue('expectedSalaryNett', isset($cv->expected_salary_nett) ? $cv->expected_salary_nett : '');
            $templateProcessor->setValue('schoolMatriculated', isset($cv->school_matriculated) ? $cv->school_matriculated : '');
            $templateProcessor->setValue('highestSchoolGrade', isset($cv->highest_grade) ? $cv->highest_grade : '');
            $templateProcessor->setValue('schoolCompletionYear', isset($cv->school_completion_year) ? $cv->school_completion_year : '');
            $templateProcessor->setValue('overview', isset($cv->note) ? htmlspecialchars($cv->note) : '');
            $templateProcessor->setImageValue('defaultCompanyLogo', ['path' => 'assets/logo.jpg', 'width' => 250, 'height' => 40, 'ratio' => false]);

            //dd($cv->qualification);
            //Qualification - Block Code Start
            $qualifications = [];
            foreach ($cv->qualification as $qualification) {
                $qualifications[] = [
                    //'institution' => '',
                    'institution' => htmlspecialchars($qualification->institution),
                    'yearCompleted' => htmlspecialchars($qualification->year_complete),
                    'qualification' => htmlspecialchars($qualification->qualification),
                ];
            }

            $templateProcessor->cloneBlock('qualificationsBlock', 0, true, false, $qualifications);
            //Qualification - Block Code End

            //Skill - Block Code Start
            $skills = [];
            foreach ($cv->skill as $skill) {
                $skills[] = [
                    'skill' => isset($skill->tool_idd->description) ? htmlspecialchars(isset($skill->tool_idd->description) ? $skill->tool_idd->description : '') : htmlspecialchars($skill->res_skill),
                    'level' => htmlspecialchars(isset($skill->skill_leveld->description) ? $skill->skill_leveld->description : ''),
                    'tool' => htmlspecialchars(isset($skill->tool_idd->description) ? $skill->tool_idd->description : ''),
                    'years' => htmlspecialchars($skill->years_experience),
                ];
            }

            $templateProcessor->cloneBlock('skillsBlock', 0, true, false, $skills);
            //Skill - Block Code End

            $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            //Experiences - Block Code End
            $experiences = [];
            foreach ($cv->experience as $experience) {
                $tmpPeriodEnd = htmlspecialchars($months[substr($experience->period_end, 4, 6)]??"0".' '.substr($experience->period_end, 0, 4));
                            if($tmpPeriodEnd == '0 0'){
                                $tmpPeriodEnd = 'Current';
                            }

                $experiences[] = [
                    'company' => htmlspecialchars($experience->company),
                    'role' => htmlspecialchars($experience->role),
                    'project' => htmlspecialchars($experience->project),
                    'periodStart' => htmlspecialchars($months[substr($experience->period_start, 4, 6)].' '.substr($experience->period_start, 0, 4)),
                    'periodEnd' => $tmpPeriodEnd,
                    'reasonForLeaving' => htmlspecialchars($experience->reason_for_leaving),
                    'tools' => htmlspecialchars($experience->tools),
                    'responsibility' => htmlspecialchars($experience->responsibility),
                ];
            }

            $templateProcessor->cloneBlock('experiencesBlock', 0, true, false, $experiences);
            //Experiences - Block Code End

            $templateProcessor->cloneBlock('currentPreviousCompanyBlock', 0, true, false, $experiences);
            //Company experience - Block Code End

            $file_name = 'tmp/cvs/cv_'.$cv_type.'_'.$cv_format.'_'.date('Y-m-d_H_i_s').'.docx';
            $templateProcessor->saveAs($file_name);

            $document_name = $file_name;
        } else {
            $preferred_name = $cv->resource_pref_name;
            $last_name = isset($cv->user->last_name) ? $cv->user->last_name : '';
            $main_skill = $cv->main_skill;
            $main_qualification = $cv->main_qualification;

            if ($named_cv == 2) {
                $preferred_name = 'Candidate #'.$cv->id;
                $last_name = '';
            }

            $parameters = [
                'preferred_name' => $preferred_name,
                'last_name' => $last_name,
                'main_skill' => $main_skill,
                'main_qualification' => $main_qualification,
                'cv' => $cv,
                'tool' => $tool,
                'skill_level' => $skill_level,
                //New fields - Start
                'mainRole' => isset($cv->role->name) ? $cv->role->name : '',
                'defaultCompanyName' => isset($company->company_name) ? $company->company_name : '',
                'recruitmentDefaultContactNumber' => isset($config->recruitment_default_contact_number) ? $config->recruitment_default_contact_number : '',
                'gender' => isset($cv->genderd->description) ? $cv->genderd->description : '',
                'race' => isset($cv->raced->description) ? $cv->raced->description : '',
                'nationality' => isset($cv->nationality) ? $cv->nationality : '',
                'DOB' => isset($cv->date_of_birth) ? $cv->date_of_birth : '',
                'ID' => isset($cv->id_no) ? $cv->id_no : '',
                'driversLicence' => isset($cv->drivers_license) ? $cv->drivers_license : '',
                'ownTransport' => isset($cv->own_transport) && ($cv->own_transport == 1) ? 'Yes' : 'No',
                'disability' => isset($cv->disabilityd->description) ? $cv->disabilityd->description : '',
                'homeLanguage' => isset($cv->home_language) ? $cv->home_language : '',
                'otherLanguage' => isset($cv->other_language) ? $cv->other_language : '',
                'currentLocation' => isset($cv->current_location) ? $cv->current_location : '',
                'maritalStatus' => isset($cv->marital_status->description) ? $cv->marital_status->description : '',
                'dependants' => isset($cv->dependants) ? $cv->dependants : '',
                'availableStatus' => isset($availability->avail_statusd->description) ? $availability->avail_statusd->description : '',
                'availableDate' => isset($availability->avail_date) ? $availability->avail_date : '',
                'rateMonth' => isset($availability->rate_month) ? $cv->rate_month : '',
                'rateHour' => isset($availability->rate_hour) ? $cv->rate_hour : '',
                'currentSalaryNett' => isset($cv->current_salary_nett) ? $cv->current_salary_nett : '',
                'monthlySalaryMinimum' => isset($cv->monthly_salary_minimum) ? $cv->monthly_salary_minimum : '',
                'monthlySalaryMaximum' => isset($cv->monthly_salary_maximum) ? $cv->monthly_salary_maximum : '',
                'monthlyRateMinimum' => '',
                'monthlyRateMaximum' => '',
                'expectedSalaryNett' => isset($cv->expected_salary_nett) ? $cv->expected_salary_nett : '',
                'schoolMatriculated' => isset($cv->school_matriculated) ? $cv->school_matriculated : '',
                'highestSchoolGrade' => isset($cv->highest_grade) ? $cv->highest_grade : '',
                'schoolCompletionYear' => isset($cv->school_completion_year) ? $cv->school_completion_year : '',
                'overview' => isset($cv->note) ? $cv->note : '',
                'defaultCompanyName' => isset($company->company_name) ? $company->company_name : '',
                //New fields - End
            ];

            try {
                //$file_name = "file_" . date("Y_m_d_H_i_s") . ".pdf";
                $file_name = 'tmp/cvs/cv_'.$cv_type.'_'.$cv_format.'_'.date('Y-m-d_H_i_s').'.pdf';
                $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
                //$file_name_pdf = $dir . $file_name;
                $file_name_pdf = $file_name;

                switch ($cv_type) {
                    case 1:
                        $pdf = Pdf::view('template.cv_short', $parameters);
                        break;
                    case 2:
                        $pdf = Pdf::view('template.cv_long', $parameters);
                        break;
                    case 3:
                        $pdf = Pdf::view('template.cv_third', $parameters);
                        break;
                    case 4:
                        $pdf = Pdf::view('cv.template.comprehensive_cv', $parameters);
                        break;
                }

                $pdf->save($file_name_pdf);

                $document_name = $file_name_pdf;
            } catch (\Exception $e) {
                report($e);

                return redirect(route('cv.show', [$cv_id, $cv->user_id]))->with('flash_danger', $e->getMessage());
            }
        }

        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST']; //$_SERVER[REQUEST_URI]";

        $dcoumentUrl = $actual_link.'/';
        $generatedName = $document_name;
        $serverName = $actual_link.'/'.$document_name;

        return response()->json(['success' => 1, 'document_name' => $serverName, 'document_url' => $dcoumentUrl, 'generated_document_name' => $generatedName]);
    }

    public function emailCandidate(Request $request): JsonResponse
    {
        $data = $request->all();

        $to = $data['email_data']['to'] != '' ? explode(',', $data['email_data']['to']) : [];
        $cc = $data['email_data']['cc'] != '' ? explode(',', $data['email_data']['cc']) : [];
        $bcc = $data['email_data']['bcc'] != '' ? explode(',', $data['email_data']['bcc']) : [];

        $mail = null;
        $message = 'CV could not be sent.';

        $valid_address = false;
        if (count($to) > 0) {
            $to_counter = 0;
            foreach ($to as $_to) {
                if ($to_counter == 0) {
                    $mail = Mail::to(trim($_to));
                } else {
                    $mail = $mail->to(trim($_to));
                }
                $to_counter++;
            }
            $valid_address = true;
        }

        if (count($cc) > 0) {
            foreach ($cc as $_cc) {
                $mail = $mail->cc(trim($_cc));
            }
        }

        if (count($bcc) > 0) {
            foreach ($bcc as $_bcc) {
                $mail = $mail->bcc(trim($_bcc));
            }
        }

        if ($valid_address == true) {
            $mail = $mail->send(new CVMail($data['email_data']['name'], $data['email_data']['subject'], $data['email_data']['mail_body'], $data['email_data']['attachment'], 1, ''));
            $message = 'CV sent successfully.';
        }

        return response()->json(['success' => 1, 'message' => $message, 'data' => $data]);
    }

    public function send($cv_id, Request $request): RedirectResponse
    {
        $cv = CV::with(['user', 'qualification', 'skill', 'experience'])->where('id', '=', $cv_id)->first();
        $skill_level = SkillLevel::orderBy('id')->pluck('description', 'id')->toArray();
        $tool = System::orderBy('id')->pluck('description', 'id')->toArray();

        $cv_type = $request->input('cv_type_new');

        $preferred_name = $cv->resource_pref_name;
        $last_name = isset($cv->user->last_name) ? $cv->user->last_name : '';
        $main_skill = $cv->main_skill;
        $main_qualification = $cv->main_qualification;

        $parameters = [
            'preferred_name' => $preferred_name,
            'last_name' => $last_name,
            'main_skill' => $main_skill,
            'main_qualification' => $main_qualification,
            'cv' => $cv,
            'tool' => $tool,
            'skill_level' => $skill_level,
        ];

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $file_name_pdf = storage_path('app/cvs/'.$file_name);
            //$dir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "tmp" . DIRECTORY_SEPARATOR;
            //$file_name_pdf = $dir . $file_name;

            if ($cv_type == 3) {
                $pdf = Pdf::view('template.cv_third', $parameters);
            } else {
                $pdf = Pdf::view('template.cv_long', $parameters);
            }

            $pdf->save($file_name_pdf);

            //return response()->file($file_name_pdf);
            Mail::to(trim($request->email))->send(new CVMail($preferred_name.' '.$last_name, $request->subject, $request->message, $file_name_pdf));

            return redirect(route('cv.show', [$cv_id, $cv->user_id]))->with('flash_success', 'Cv sent successfully');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('cv.show', [$cv_id, $cv->user_id])->with('flash_danger', $e->getMessage()));
        }
    }

    private function complete_percentage($model, $table_name, $foreign_key, $own_col)
    {
        $pos_info = DB::select(DB::raw('SHOW COLUMNS FROM '.$table_name)->getValue(DB::connection()->getQueryGrammar()));
        $base_columns = count($pos_info);
        $not_null = 0;
        foreach ($pos_info as $col) {
            $not_null += app('App\\Models\\'.$model)::selectRaw('SUM(CASE WHEN '.$col->Field.' IS NOT NULL THEN 1 ELSE 0 END) AS not_null')->where($own_col, '=', $foreign_key?->user_id)->first()->not_null;
        }

        return ($not_null / $base_columns) * 100;
    }

    public function submit($id): View
    {
        $cv = CV::find($id);

        $latest_date = null;
        $latest_availability = null;
        foreach ($cv->availability as $availability) {
            if ($latest_date == null) {
                $latest_date = $availability->avail_date;
                $latest_availability = $availability;
            } else {
                if (date('Y-m-d', strtotime($latest_date)) < date('Y-m-d', strtotime($availability->avail_date))) {
                    $latest_date = $availability->avail_date;
                    $latest_availability = $availability;
                }
            }
        }

        $availability_status = '';
        if (isset($latest_availability->avail_status)) {
            $availability_status = AvailStatus::find($latest_availability->avail_status);
        }

        $job_spec_drop_down = JobSpec::orderBy('id')->pluck('reference_number', 'id');

        $parameters = [
            'id' => $id,
            'cv' => $cv,
            'estimated_hourly_rate' => '',
            'latest_availability' => $latest_availability,
            'availability_status' => $availability_status,
            'job_spec_drop_down' => $job_spec_drop_down,
        ];

        return view('cv.submit.index')->with($parameters);
    }

    public function getSpeciality($profession_id, Request $request): JsonResponse
    {
        $speciality_drop_down = [];
        if ($profession_id == 0) {
            if ($request->profession_ids == null) {
                $speciality_drop_down = Speciality::whereNotNull('name')->where('name', '!=', '')->orderBy('name')->pluck('name', 'id');
            } else {
                $speciality_drop_down = Speciality::whereIn('profession_id', $request->profession_ids)->whereNotNull('name')->where('name', '!=', '')->orderBy('name')->pluck('name', 'id');
            }
        } else {
            $speciality_drop_down = Speciality::where('profession_id', '=', $profession_id)->whereNotNull('name')->where('name', '!=', '')->orderBy('name')->pluck('name', 'id');
        }

        return response()->json(['success' => 1, 'speciality' => $speciality_drop_down]);
    }

    public function uploadDocument(Request $request): View
    {
        $parameters = [
            'cvid' => $request->input('cvid'),
        ];

        return view('cv.uploaddocument')->with($parameters);
    }

    public function editDocument($id): View
    {
        $document = Document::find($id);
        $parameters = [
            'document' => $document,
            'cvid' => $document->reference_id,
        ];

        return view('cv.editdocument')->with($parameters);
    }

    public function storeDocument(DocumentRequest $request): RedirectResponse
    {
        $cv_id = $request->input('cvid');
        $cv = CV::find($cv_id);

        if ($request->hasfile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/files/cvs/', $filename);

            $document = new Document();
            $document->type_id = 3;
            $document->name = $request->input('name');
            $document->file = 'files/cvs/'.$filename;
            $document->document_type_id = 3;
            $document->reference = 'CV';
            $document->reference_id = $cv->id;
            $document->creator_id = auth()->id();
            $document->owner_id = $cv->user_id;
            $document->status_id = 1;
            $document->digisign_status_id = null;
            $document->digisign_approver_user_id = null;
            $document->save();
        }

        return redirect(route('cv.show', [$cv_id, $cv->user_id]))->with('flash_success', 'Document uploaded successfully.');
    }

    public function updateDocument($id, DocumentRequest $request): RedirectResponse
    {
        $cv_id = $request->input('cvid');
        $cv = CV::find($cv_id);

        if ($request->hasfile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/files/cvs/', $filename);

            $document = Document::find($id);
            $document->name = $request->input('name');
            $document->file = 'files/cvs/'.$filename;
            $document->save();
        }

        return redirect(route('cv.show', [$cv_id, $cv->user_id]))->with('flash_success', 'Document updated successfully.');
    }

    public function deleteDocument($id): RedirectResponse
    {
        $document = Document::find($id);
        $cv = Cv::find($document->reference_id);
        Document::destroy($id);

        return redirect(route('cv.show', [$cv->id, $cv->user_id]))->with('flash_success', 'Document deleted successfully.');
    }
}
