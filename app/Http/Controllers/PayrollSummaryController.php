<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Config;
use App\Models\ExpenseTracking;
use App\Models\Leave;
use App\Models\LeaveBalance;
use App\Models\Resource;
use App\Models\ResourceType;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Services\LeaveService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class PayrollSummaryController extends Controller
{
    public function index(Request $request,LeaveService $service): View|BinaryFileResponse
    {
        $config = Config::first();
        $year_month = [];
        for ($i = 0; $i <= 36; $i++) {
            $tempt_year_month = (now()->subMonth($i)->year.((now()->subMonth($i)->month < 10) ? '0'.now()->subMonth($i)->month : now()->subMonth($i)->month));
            $year_month[$tempt_year_month] = $tempt_year_month;
        }

        $resources = Resource::with(['team','commission', 'user' => function ($query) {
            $query->addSelect('id', 'first_name', 'last_name');
        }])->whereIn('resource_type',[1,2,3])
            ->select('user_id', 'resource_no', 'commission_id', 'team_id', 'join_date','created_at')
            ->whereHas('user', function ($query) use ($request) {
                $query->where('expiry_date', '>', now()->endOfMonth()->toDateString());
                if ($request->has('company') && $request->company != '') {
                    $query->where('company_id', $request->company);
                }
            })
            ->whereHas('user.roles', function ($query) {
                $query->whereNotIn('id', [6, 7, 9]);
            });

        $all_items = $resources->get();
        $teams_drop_down = [];
        $resources_drop_down = [];

        foreach ($all_items as $item) {
            $teams_drop_down[$item->team_id] = isset($item->team) ? $item->team->team_name : null;
            $resources_drop_down[$item->user_id] = isset($item->user) ? $item->user->first_name.' '.$item->user->last_name : null;
        }

        if ($request->has('team') && $request->team != '') {
            $resources = $resources->where('team_id', $request->team);
        }

        if ($request->has('resource') && $request->resource != '') {
            $resources = $resources->where('user_id', $request->resource);
        }

        if ($request->has('resource_type') && $request->resource_type != '') {
            $resources = $resources->where('resource_type', $request->resource_type);
        }

        $resources = $resources->where('status_id', 1)
            ->paginate((($request->has('r') ? $request->r : 15)));

        $payroll_summary = [];
        $total_commision = 0;
        $total_expense_not_approved = 0;
        $total_expense_approved = 0;

        foreach ($resources as $resource) {
            if ($resource->commission) {
                $comm = Timesheet::selectRaw('IFNULL(assignment.rate, 0) AS rate, 
                                SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                    ->leftJoin('assignment', function ($join) {
                        $join->on('timesheet.employee_id', 'assignment.employee_id');
                        $join->on('timesheet.project_id', 'assignment.project_id');
                    })
                    ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id');
                if ($request->has('commission') && $request->commission) {
                    $comm = $comm->where('year', substr($request->commission, 0, 4))
                        ->where('month', substr($request->commission, 4));
                } else {
                    $comm = $comm->where('year', now()->subMonth()->year)
                        ->where('month', ((now()->subMonth()->month < 10) ? '0'.now()->subMonth()->month : now()->subMonth()->month));
                }
                $comm = $comm->where('timesheet.employee_id', $resource->user_id)
                    ->groupBy('assignment.rate')
                    ->get();

                $temp_commission = [];
                $comm_value = 0;

                foreach ($comm as $commission) {
                    $hours = $commission->hours;
                    $value = ($hours * $commission->rate);

                    if (($hours >= $resource->commission->hour01) && ($hours < $resource->commission->hour02) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm01 / 100));
                    }
                    if (($hours >= $resource->commission->hour02) && ($hours < $resource->commission->hour03) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm02 / 100));
                    }
                    if (($hours >= $resource->commission->hour03) && ($hours < $resource->commission->hour04) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm03 / 100));
                    }
                    if (($hours >= $resource->commission->hour04) && ($hours < $resource->commission->hour05) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm04 / 100));
                    }
                    if (($hours >= $resource->commission->hour05) && ($hours < $resource->commission->hour06) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm05 / 100));
                    }
                    if (($hours >= $resource->commission->hour06) && ($hours < $resource->commission->hour07) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm06 / 100));
                    }
                    if (($hours >= $resource->commission->hour07) && ($hours < $resource->commission->hour08) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm07 / 100));
                    }
                    if (($hours >= $resource->commission->hour08) && ($hours < $resource->commission->hour09) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm08 / 100));
                    }
                    if (($hours >= $resource->commission->hour09) && ($hours < $resource->commission->hour10) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm09 / 100));
                    }
                    if (($hours >= $resource->commission->hour10) && ($hours < $resource->commission->hour11) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm10 / 100));
                    }
                    if (($hours >= $resource->commission->hour11) && ($hours < $resource->commission->hour12) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm11 / 100));
                    }
                    if (($hours >= $resource->commission->hour12) && ($commission->rate > $resource->commission->min_rate)) {
                        $comm_value = ($value * ($resource->commission->comm12 / 100));
                    }

                    $temp_commission[] = (int) $comm_value;
                }
            }

            $leave_balance = LeaveBalance::where('resource_id', $resource->user_id)->latest()->first();
            $annual_leave = $service->calculateAnnualLeave($request,$config,$resource,$leave_balance);
            $sick_leave = $service->calculateSickLeave($request,$config,$resource);
            $unpaid_leave = $service->calculateUnpaidLeave($request,$config,$resource);
            $other_leave = $service->calculateOtherLeave($request,$config,$resource);

            $expense_tracking_not_approved = $this->ExpenseTracking($resource, 0);
            $time_expense_not_approved = $this->TimeExpense($resource, 0);

            $expenses_not_approved = [((count($this->ExpenseTracking($resource, 0, false)) > 0) ? ['et' => $this->ExpenseTracking($resource, 0, false)] : null), ((count($this->TimeExpense($resource, 0, false)) > 0) ? ['te' => $this->TimeExpense($resource, 0, false)] : null)];

            $expense_tracking_approved = $this->ExpenseTracking($resource, 1);
            $time_expense_approved = $this->TimeExpense($resource, 1);

            $expenses_approved = [((count($this->ExpenseTracking($resource, 1, false)) > 0) ? ['et' => $this->ExpenseTracking($resource, 1, false)] : null), ((count($this->TimeExpense($resource, 1, false)) > 0) ? ['te' => $this->TimeExpense($resource, 1, false)] : null)];

            $actual_commission = ((isset($temp_commission) ? array_sum($temp_commission) : 0));

            $not_approved = ($expense_tracking_not_approved->amount ?? 0) + ($time_expense_not_approved->sum('amount') ?? 0);
            $approved = ($expense_tracking_approved->amount ?? 0) + ($time_expense_approved->sum('amount') ?? 0);
            $total_commision += $actual_commission;
            $total_expense_not_approved += $not_approved;
            $total_expense_approved += $approved;

            $payroll_summary[$resource->user_id] = [
                'expense_claim_not_approved' => $not_approved,
                'expense_claim_approved' => $approved,
                'commission' => $actual_commission,
                'opening_balance' => $annual_leave['opening_balance'],
                'closing_balance' => $annual_leave['closing_balance'],
                'accrued' => $annual_leave['days_accrued'],
                'taken' => $annual_leave['leave_taken'],
                'sick' => $sick_leave,
                'unpaid' => $unpaid_leave,
                'other' => $other_leave,
                'expenses' => array_filter($expenses_not_approved),
                'expenses_approved' => array_filter($expenses_approved),
            ];

            unset($temp_commission);
        }

        asort($resources_drop_down);

        $parameters = [
            'configs' => Config::first(),
            'resources' => $resources,
            'payroll_summary' => $payroll_summary,
            'total_commission' => $total_commision,
            'total_expense_not_approved' => $total_expense_not_approved,
            'total_expense_approved' => $total_expense_approved,
            'company_drop_down' => Company::pluck('company_name', 'id'),
            'team_drop_down' => array_unique($teams_drop_down),
            'resource_drop_down' => $resources_drop_down,
            'resource_type_drop_down' => ResourceType::pluck('description','id'),
            'year_month_drop_down' => $year_month,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'payroll_summary');

        return view('reports.management.payroll_summary')->with($parameters);
    }

    private function ExpenseTracking($resource, $approved, $is_total = true)
    {
        $expense_tracking = ExpenseTracking::whereMonth('transaction_date', now()->month)
            ->where('employee_id', $resource->user_id)
            ->where('claimable', 1);
        if ($approved) {
            $expense_tracking = $expense_tracking->whereNotNull('approved_on')
                ->whereNull('payment_date');
        } else {
            $expense_tracking = $expense_tracking->whereNull('approved_on');
        }
        if (! $is_total) {
            $expense_tracking = $expense_tracking->selectRaw('exp_tracking.id, SUBSTRING(exp_tracking.yearwk, 1, 6) AS year_week, account.description')
                ->leftJoin('account', 'exp_tracking.account_id', '=', 'account.id')
                ->limit(5)->get();
        } else {
            $expense_tracking = $expense_tracking->selectRaw('IFNULL(SUM(amount),0) AS amount')->first();
        }

        return $expense_tracking;
    }

    private function TimeExpense($resource, $approved, $is_total = true)
    {
        $time_expense = Timesheet::whereHas('time_exp', function ($query) use ($approved) {
            $query->where('claim', 1);
            if ($approved) {
                $query->whereNotNull('claim_auth_date')
                    ->whereNull('paid_date');
            } else {
                $query->whereNull('claim_auth_date')
                    ->whereNull('is_approved');
            }
            $query->whereMonth('date', now()->month);
        })
            ->where('employee_id', $resource->user_id);

        if (! $is_total) {
            $time_expense = $time_expense->selectRaw('timesheet.id, timesheet.year_week, account.description')
                ->leftJoin('time_exp', 'timesheet.id', '=', 'time_exp.timesheet_id')
                ->leftJoin('account', 'time_exp.account_id', '=', 'account.id')
                ->get();
        } else {
            $time_expense = $time_expense->with(['time_exp' => function ($query) {
                $query->SelectRaw('timesheet_id, amount')
                    ->groupBy('timesheet_id', 'amount');
            }])
                ->select('id')
                ->get()->map(function ($expense) {
                    return $expense->time_exp;
                })->flatten();
        }

        return $time_expense;
    }

    public function approveExpenses(Request $request): RedirectResponse
    {
        if (isset($request->te)) {
            $time_exps = TimeExp::whereIn('timesheet_id', $request->te)->get();
        }

        if (isset($time_exps)) {
            foreach ($time_exps as $te) {
                $te->claim_auth_date = now()->toDateString();
                $te->is_approved = 1;
                $te->save();

                activity()->on($te)->log('approve');
            }
        }

        if (isset($request->et)) {
            $exp_tracking = ExpenseTracking::whereIn('id', $request->et)->get();
        }

        if (isset($exp_tracking)) {
            foreach ($exp_tracking as $et) {
                $et->approved_by = auth()->user()->id;
                $et->approved_on = now()->toDateString();
                $et->approval_status = 2;
                $et->save();

                activity()->on($et)->log('approve');
            }
        }

        return redirect()->back()->with('flash_success', 'Your Expenses have been approved');
    }

    public function payExpenses(Request $request): RedirectResponse
    {
        if (isset($request->te)) {
            $time_exps = TimeExp::whereIn('timesheet_id', $request->te)->get();
        }

        if (isset($time_exps)) {
            foreach ($time_exps as $te) {
                $te->paid_date = now()->toDateString();
                $te->save();

                activity()->on($te)->log('paid');
            }
        }

        if (isset($request->te)) {
            $timesheet = Timesheet::whereIn('id', $request->te)->get();
        }

        if (isset($timesheet)) {
            foreach ($timesheet as $time) {
                $time->exp_paid = 1;
                $time->save();
            }
        }

        if (isset($request->et)) {
            $exp_tracking = ExpenseTracking::whereIn('id', $request->et)->get();
        }

        if (isset($exp_tracking)) {
            foreach ($exp_tracking as $et) {
                $et->payment_date = now()->toDateString();
                $et->payment_status = 2;
                $et->save();

                activity()->on($et)->log('paid');
            }
        }

        return redirect()->back()->with('flash_success', 'Your Expenses have been Paid');
    }
}
