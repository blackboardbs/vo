<?php

namespace App\Http\Controllers;

use App\Models\Leave;
use App\Models\LeaveBalance;
use App\Models\Module;
use App\Models\Resource;
use App\Models\User;
use App\Models\Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\LeaveService;

class LeaveStatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, LeaveService $service)
    {
        $module = Module::where('name', '=', \App\Models\LeaveStatement::class)->get();

        $resource_drop_down = User::join('resource', 'users.id', '=', 'resource.user_id')->select(DB::raw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS full_name"), 'users.id')->excludes([6, 7, 8])->whereIn('resource.resource_type',[1,2,3])->orderBy('users.first_name')->orderBy('users.last_name')->pluck('full_name', 'users.id')->prepend('Resource', '0');

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $resource_drop_down = User::join('resource', 'users.id', '=', 'resource.user_id')->select(DB::raw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS full_name"), 'users.id')->excludes([6, 7, 8])->whereIn('resource.resource_type',[1,2,3])->whereIn('users.id', Auth::user()->team())->orderBy('users.first_name')->orderBy('users.last_name')->pluck('full_name', 'users.id')->prepend('Resource', '0');
            }
        } else {
            return abort(403);
        }

        $leave_balance = LeaveBalance::where('resource_id', '=', $request->input('resource_id'))->latest()->first();
        
        $resource_balance_found = false;
        
        if ($request->has('resource_id') == false || empty($leave_balance) == true) {
            $parameters = [
                'resource_drop_down' => $resource_drop_down,
                'resource_balance_found' => $resource_balance_found,
                'year_months' => [date('Ym') => date('Ym')],
            ];

            return view('leave_statement.leave')->with($parameters);
        } else {
            $resource_balance_found = true;

            $config = Config::first();
            $resource = Resource::where('user_id',$leave_balance->resource_id)->first();
            
            $annual_leave = $service->calculateAnnualLeave($request,$config,$resource,$leave_balance);
        }


        $balance_start_date = $leave_balance->balance_date;

        $selected_period = date('Ym');

        if ($request->has('leave')) {
            $selected_period = $request->input('leave');
        }

        $leave_balance_array = explode('-', $leave_balance->balance_date);

        $_month_leave_balance = $leave_balance_array[1];
        $_year_leave_balance = $leave_balance_array[0];
        $_day_leave_balance = $leave_balance_array[2];

        if ($_day_leave_balance >= 28) {
            $_month_leave_balance = $_month_leave_balance == 12 ? 1 : $_month_leave_balance + 1;
            $_year_leave_balance = $_month_leave_balance == 12 ? $_year_leave_balance + 1 : $_year_leave_balance;
        }

        $_month = substr($selected_period, 4);
        $_year = substr($selected_period, 0, 4);

        $months_difference = 0;
        if ($_month >= $_month_leave_balance) {
            $months_difference = (($_year - $_year_leave_balance) * 12) + $_month - $_month_leave_balance + 1;
        } else {
            $months_difference = (($_year - $_year_leave_balance - 1) * 12) + 12 - $_month_leave_balance + $_month;
        }

        $selected_period_start_date = Carbon::create($_year, $_month, 1)->format('Y-m-d');
        $selected_period_end_date = Carbon::create($_year, $_month, 1)->endOfMonth()->format('Y-m-d');

        if ($_month == 1) {
            $_year -= 1;
            $_month = 12;
        } else {
            $_month -= 1;
        }

        $selected_period_last_month_end_date = Carbon::create($_year, $_month, 1)->endOfMonth()->format('Y-m-d');

        $leaves_selected_month = Leave::where('emp_id', '=', $request->input('resource_id'))->where('date_from', '>=', $selected_period_start_date)->where('date_from', '<=', $selected_period_end_date)->where('leave_type_id', '=', 1)->get();
        $leaves_before_selected_month = Leave::where('emp_id', '=', $request->input('resource_id'))->where('date_from', '>=', $balance_start_date)->where('date_from', '<=', $selected_period_last_month_end_date)->where('leave_type_id', '=', 1)->get();

        $leave_days_taken_current_month = 0;
        foreach ($leaves_selected_month as $leave_taken) {
            $leave_days_taken_current_month += $leave_taken->no_of_days;
        }

        $leave_days_taken_before_current_month = 0;
        foreach ($leaves_before_selected_month as $leave_taken) {
            $leave_days_taken_before_current_month += $leave_taken->no_of_days;
        }

        $leave_balanace_start_date = Carbon::parse($leave_balance->balance_date);

        $leave_balanace_start_date_year = $leave_balanace_start_date->year;
        $leave_balanace_start_date_month = $leave_balanace_start_date->month;

        $year_months = [];
        if($resource->termination_date && $resource->termination_date != ''){
            for ($i = $leave_balanace_start_date_year; $i <= date('Y'); $i++) {
                for ($j = ($i == $leave_balanace_start_date_year ? $leave_balanace_start_date_month : 1); $j <= ($i == Carbon::parse($resource->termination_date)->year ? Carbon::parse($resource->termination_date)->format('m') : 12); $j++) {
                    $key = $i.($j < 10 ? '0'.$j : $j);
                    $year_months[$key] = $i.($j < 10 ? '0'.$j : $j);
                }
            }
        } else {
            for ($i = $leave_balanace_start_date_year; $i <= date('Y'); $i++) {
                for ($j = ($i == $leave_balanace_start_date_year ? $leave_balanace_start_date_month : 1); $j <= ($i == date('Y') ? date('m') : 12); $j++) {
                    $key = $i.($j < 10 ? '0'.$j : $j);
                    $year_months[$key] = $i.($j < 10 ? '0'.$j : $j);
                }
            }
        }

        $leave_allocation_per_month = $leave_balance->leave_per_cycle / 12;

        $leave_allocation_selected_period_last_month = ($months_difference - 1) * $leave_allocation_per_month;

        $opening_balance_selected_month = $leave_balance->opening_balance - $leave_days_taken_before_current_month + $leave_allocation_selected_period_last_month;

        $leave_allocation_current_month = $leave_allocation_per_month * 1; //Multiply by 1 month(Current Month)

        $closing_balance_current_month = $opening_balance_selected_month + $leave_allocation_current_month - $leave_days_taken_current_month;

        $parameters = [
            'resource_balance_found' => $resource_balance_found,
            'resource_drop_down' => $resource_drop_down,
            'year_months' => $year_months,
            'selected_period_start_date' => $selected_period_start_date,
            'selected_period_end_date' => $selected_period_end_date,
            // 'opening_balance_selected_month' => number_format($opening_balance_selected_month, 2, '.', ','),
            // 'leave_allocation_current_month' => number_format($leave_allocation_current_month, 2, '.', ','),
            // 'leave_days_taken_current_month' => number_format($leave_days_taken_current_month, 2, '.', ','),
            // 'closing_balance_current_month' => number_format($closing_balance_current_month, 2, '.', ','),
            'annual_leave' => $annual_leave
        ];

        return view('leave_statement.leave')->with($parameters);
    }

    public function getToPeriod($from)
    {
        $t = new Carbon();

        $now = $t->now()->year.''.($t->now()->month < 10 ? '0'.$t->now()->month : $t->now()->month);

        $year = substr($from, 0, 4);
        $month = substr($from, 4, 2);

        $yearmonth = DB::select(DB::raw('select distinct(yearmonth) from v_yearwk where yearmonth <='.$now.' and yearmonth >= '.$year.''.$month.' order by yearmonth desc'));

        return $yearmonth;
    }

    public function checkjoinDate($resourceid)
    {
        $query = Resource::where('user_id', $resourceid)->first();

        $data = [];

        if ($query->join_date != null) {
            $data['result'] = '1';
            $data['join_date'] = $query->join_date;
        } else {
            $data['result'] = '0';
        }

        return $data;
    }
}
