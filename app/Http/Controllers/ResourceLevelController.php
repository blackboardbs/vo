<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResourceLevelRequest;
use App\Http\Requests\UpdateResourceLevelRequest;
use App\Models\ResourceLevel;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ResourceLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $emp_level = ResourceLevel::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $emp_level = $emp_level->where('status', $request->input('status_filter'));
            }else{
                $emp_level = $emp_level->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $emp_level = $emp_level->where('description', 'like', '%'.$request->input('q').'%');
        }

        $emp_level = $emp_level->paginate($item);

        $parameters = [
            'emp_level' => $emp_level,
        ];

        return View('master_data.resource_level.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ResourceLevel::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.resource_level.create')->with($parameters);
    }

    public function store(StoreResourceLevelRequest $request): RedirectResponse
    {
        $emp_level = new ResourceLevel();
        $emp_level->description = $request->input('description');
        $emp_level->status = $request->input('status');
        $emp_level->creator_id = auth()->id();
        $emp_level->save();

        return redirect(route('resource_level.index'))->with('flash_success', 'Master Resource Level captured successfully');
    }

    public function show($emp_level_id): View
    {
        $parameters = [
            'emp_level' => ResourceLevel::where('id', '=', $emp_level_id)->get(),
        ];

        return View('master_data.resource_level.show')->with($parameters);
    }

    public function edit($emp_level_id): View
    {
        $autocomplete_elements = ResourceLevel::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'country' => ResourceLevel::where('id', '=', $emp_level_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.resource_level.edit')->with($parameters);
    }

    public function update(UpdateResourceLevelRequest $request, $emp_level_id): RedirectResponse
    {
        $country = ResourceLevel::find($emp_level_id);
        $country->description = $request->input('description');
        $country->status = $request->input('status');
        $country->creator_id = auth()->id();
        $country->save();

        return redirect(route('resource_level.index'))->with('flash_success', 'Master Resource Level saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // ResourceLevel::destroy($id);
        $item = ResourceLevel::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('resource_level.index')->with('success', 'Master Data Employee Level suspended successfully');
    }
}
