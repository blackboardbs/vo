<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Status;
use App\Models\WarningMaintenance;
use App\Models\WarningRecipientRole;
use App\Models\WarningTriger;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class WarningMaintenanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $item = isset($request->r) ? $request->r : 15;
        $module = Module::where('name', '=', \App\Models\WarningMaintenance::class)->first();

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $warning_maintenances = WarningMaintenance::with(['status:id,description']);

        if ($request->has('q') && ($request->input('q') != '')) {
            $warning_maintenances->where('function_area', 'LIKE', '%'.$request->input('q').'%');
        }

        if ($request->has('type') && ($request->input('type') != '')) {
            $warning_maintenances = $warning_maintenances->where('type', '=', $request->input('type'));
        }

        $warning_maintenances = $warning_maintenances->sortable(['function_area' => 'asc'])->paginate($item);

        $warning_maintenance_triggers = '';
        $notification_values['E'] = 'Email';
        $notification_values['N'] = 'Notify';
        $trigger_values = WarningTriger::where('status_id', '=', 1)->pluck('name', 'id')->toArray();
        $warning_recipient_values_values = WarningRecipientRole::where('status_id', '=', 1)->pluck('name', 'id')->toArray();

        $parameters = [
            'warning_maintenances' => $warning_maintenances,
            'warning_maintenance_triggers' => $warning_maintenance_triggers,
            'warning_recipient_values_values' => $warning_recipient_values_values,
            'trigger_values' => $trigger_values,
            'notification_values' => $notification_values,
            'can_update' => $can_update,
        ];

        return view('warning.maintenance.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(WarningMaintenance $warningMaintenance): View
    {
        $time_drop_down = [];

        for ($i = 0; $i < 24; $i++) {
            $time_drop_down[$i] = $i < 10 ? '0'.$i.':00' : $i.':00';
        }

        $trigger_drop_down = WarningTriger::where('status_id', '=', 1)->where('type', '=', 1)->pluck('name', 'id');
        $recipient_role_drop_down = WarningRecipientRole::where('status_id', '=', 1)->pluck('name', 'id');
        $status_drop_down = Status::where('status', '=', 1)->pluck('description', 'id');
        $notification_drop_down = ['E' => 'Email', 'N' => 'Notify'];

        $parameters = [
            'warning_maintenance' => $warningMaintenance,
            'trigger_drop_down' => $trigger_drop_down,
            'recipient_role_drop_down' => $recipient_role_drop_down,
            'status_drop_down' => $status_drop_down,
            'notification_drop_down' => $notification_drop_down,
            'time_drop_down' => $time_drop_down,
        ];

        return view('warning.maintenance.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WarningMaintenance $warningMaintenance): View
    {
        $time_drop_down = [];

        for ($i = 0; $i < 24; $i++) {
            $time_drop_down[$i] = $i < 10 ? '0'.$i.':00' : $i.':00';
        }

        $trigger_drop_down = WarningTriger::where('status_id', '=', 1)->where('type', '=', 1)->pluck('name', 'id');
        $recipient_role_drop_down = WarningRecipientRole::where('status_id', '=', 1)->pluck('name', 'id');
        $status_drop_down = Status::where('status', '=', 1)->pluck('description', 'id');
        $notification_drop_down = ['E' => 'Email', 'N' => 'Notify'];

        $parameters = [
            'warning_maintenance' => $warningMaintenance,
            'trigger_drop_down' => $trigger_drop_down,
            'recipient_role_drop_down' => $recipient_role_drop_down,
            'status_drop_down' => $status_drop_down,
            'notification_drop_down' => $notification_drop_down,
            'time_drop_down' => $time_drop_down,
        ];

        return view('warning.maintenance.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, WarningMaintenance $warningMaintenance): RedirectResponse
    {
        $warningMaintenance->function_area = $request->function_area;
        $warningMaintenance->type = $request->type;
        $warningMaintenance->event = $request->event;
        $warningMaintenance->trigger = $request->type == 1 ? implode('|', $request->trigger) : $request->trigger;
        $warningMaintenance->time = $request->time;
        $warningMaintenance->recipient_role = implode('|', $request->recipient_role);
        $warningMaintenance->notifications = implode('|', $request->notifications);
        $warningMaintenance->status_id = $request->status_id;
        $warningMaintenance->save();

        return redirect(route('warning_maintenance.index'))->with('flash_success', 'Warning maintenance updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(WarningMaintenance $warningMaintenance)
    {
        //
    }
}
