<?php

namespace App\Http\Controllers;

use App\Enum\Status as RecordStatus;
use App\Models\Favourite;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\SendWelcomeEmailJob;
use App\Models\LandingPage;
use App\Models\User;
use App\Models\UserFavourite;
use App\Models\UserNotification;
use App\Models\UserType;
use App\Services\UserService;
use App\Models\Api;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use App\Models\Favorite;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\PersonalAccessToken;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, UserService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $users = User::with(['status:id,description', 'api:id,user'])->select(['id', 'first_name', 'last_name', 'email', 'phone', 'created_at', 'status_id'])
            ->with(['roles:id,display_name'])
            ->when($request->sort, fn($user) => $user->orderBy($request->sort, $request->direction))
            ->when($request->exp_date_from, fn($user) => $user->whereBetween('expiry_date', [$request->exp_date_from, $request->exp_date_to]))
            ->when($request->customer, fn($user) => $user->where('customer_id', $request->customer))
            ->when($request->vendor, fn($user) => $user->where('vendor_id', $request->vendor))
            ->when($request->company, fn($user) => $user->where('company_id', $request->company))
            ->when($request->role, fn($user) => $user->whereHas('roles', fn($roles) => $roles->where('id', $request->role)))
            ->when($request->q, fn($user) => $user->where(
                fn($query) => $query->where('first_name', 'like', '%'.$request->input('q').'%')
                    ->orWhere('last_name', 'like', '%'.$request->q.'%')
                    ->orWhere('email', 'like', '%'.$request->q.'%')
                    ->orWhere('phone', 'like', '%'.$request->q.'%')
            ))
            ->when($service->hasViewTeamPermission(), fn($user) => $user->whereIn('id', Auth::user()->team()))
            ->paginate($request->input('r') ?? 15);

        $api_tokens = Api::all();
        foreach ($users as $user) {
            foreach ($api_tokens as $api_token) {
                if ($user->id == $api_token->user) {
                    $user->api_token = $api_token->api_token;
                }
            }
        }

        $parameters = [
            'users' => $users,
            'roles' => Role::all()->pluck('display_name', 'id')->prepend('All', null),
            'can_create' => $service->hasCreatePermissions(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        if ($request->has('export')) return $this->export($parameters, 'users');

        return view('users.index')->with($parameters);
    }

    public function profile(Request $request, User $user, UserService $service): View
    {
        abort_unless($service->hasListAllPermissions(), 403);

        if (!$user->resource) {
            $user = $service->userDoesNotHaveResource($user);
        }

        $parameters = [
            'user' => $user,
            'path' => ($request->headers->get('referer') == route('calendar.index')) ? '1' : '0',
            'can_update' => $service->hasUpdatePermissions(),
        ];

        return view('auth.profile')->with($parameters);
    }

    public function settings(UserService $service): View
    {
        $user_id = Auth::id();
        $user_role = min(auth()?->user()?->roles()?->pluck('id')?->toArray());
        $favourites = Favourite::select(['id', 'module_name', 'role_id'])
            ->where('status_id', '=', RecordStatus::ACTIVE->value)->get()
            ->filter(fn($favourite) => in_array($user_role, explode(',', $favourite->role_id)))
            ->values();

        $user_favourite = UserFavourite::where('user_id', '=', $user_id)->pluck('favourite_id')->toArray();

        $pages = Page::select(['id', 'title', 'icon'])->get();
        $user = Auth::user();
        $favorites = [];

        foreach($pages as $page) {
            if($user->favorites->contains($page->id)) {
                $favorites[] = $page->id;
            }
        }

        $parameters = [
            'favourites' => $favourites,
            'user_role' => $user_role,
            'fav_ids' => $user_favourite,
            'landing_page' => LandingPage::where('user_id', $user_id)->latest()->first(),
            'pages' => $pages,
            'favorites' => $favorites,
            'user' => $user,
            'can_update' => $service->hasUpdatePermissions(),
        ];

        return view('auth.settings')->with($parameters);
    }

    public function handleSettings(Request $request)
    {
        // Define your validation rules
        $rules = [
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'avatar' => 'image|max:2048',
            'email' => 'required|email',
        ];

        // Validate the request data
        $validator = Validator::make($request->all(), $rules);

        // Check if the validation fails
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $user = auth()->user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');

        if ($request->hasFile('avatar')) {
            $user->thumbnail(request: $request, field: 'avatar', type: 'user', height: 200, width: 200);
            $user->avatar = $request->file('avatar')->hashName();
        }

        $user->calendar_view = '';

        $user = auth()->user();
        if($request->has('calendar')){
            foreach ($request->input('calendar') as $pref) {
                if ($user->calendar_view == '') {
                    $user->calendar_view = $pref;
                } else {
                    $user->calendar_view .= ','.$pref;
                }
            }
        }

        if($request->has('favorites_id')){
            Favorite::where('user_id',auth()->id())->delete();
            foreach($request->favorites_id as $key => $value){
                $fav = new Favorite();
                $fav->user_id = auth()->id();
                $fav->page_id = $value;
                $fav->save();
            }
        }

        $user->save();

        LandingPage::updateOrCreate(
            ['user_id' => $request->user_id ?? \auth()->id()],
            ['page' => $request->page ?? url('/dashboard')]
        );

        return redirect()->back()->with('flash_success', 'Settings successfully updated.');
    }

    public function handleProfile(ProfileRequest $request): RedirectResponse
    {
        $user = auth()->user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');

        if ($request->hasFile('avatar')) {
            $user->thumbnail(request: $request, field: 'avatar', type: 'user', height: 200, width: 200);
            $user->avatar = $request->file('avatar')->hashName();
        }

        $user->save();

        return redirect()->back()->with('flash_success', 'Profile updated successfully.');
    }

    public function handlePreferences(Request $request): RedirectResponse
    {
        $user = auth()->user();
        $user->calendar_view = '';
        $user->save();

        $user = auth()->user();
        if($request->has('calendar')){
            foreach ($request->input('calendar') as $pref) {
                if ($user->calendar_view == '') {
                    $user->calendar_view = $pref;
                } else {
                    $user->calendar_view .= ','.$pref;
                }
            }
            $user->save();
        }

        return redirect()->back()->with('flash_success', 'Preferences updated successfully.');
    }

    public function handlePassword(PasswordRequest $request): RedirectResponse
    {
        //https://www.5balloons.info/setting-up-change-password-with-laravel-authentication/
        if (! (Hash::check($request->get('old_password'), auth()->user()->password))) {
            return redirect()->back()->with('flash_danger', 'Incorrect password.');
        }

        $user = auth()->user();
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect()->back()->with('flash_success', 'Password updated successfully.');
    }

    public function handleLandingPage(Request $request): RedirectResponse
    {
        LandingPage::updateOrCreate(
            ['user_id' => $request->user_id ?? \auth()->id()],
            ['page' => $request->page ?? url('/dashboard')]
        );

        return redirect()->back()->with('flash_success', 'Landing page successfully set');
    }

    public function create(Request $request, UserService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        // if(auth()->user()->hasAnyRole(['admin'])){
        $client = new \GuzzleHttp\Client();
        $owner = User::first();
        try {
            $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);

            $request->session()->put('subscriptionDetail', json_encode($response->json()));
        } catch (Exception $e) {
            logger($e);
        }
        // }

        $license = $response->json();

        $subscription_package = $this->subscriptionPackage();
        if($subscription_package && $license){
            $details = $subscription_package[$license['subscription']['subscription_tier']];
        } else {
            abort(403);
        }

        if(count(User::where('login_user',1)->where('status_id',1)->get()) < $details['users']){
            $addLoginUser = 1;
        } else {
            $addLoginUser = 0;
        }


        $parameters = [
            'can_add_login_user' => $addLoginUser,
            'roles' => Role::selectRaw('id, REPLACE(`name`, "_", " ") AS display_name')->where('id', '>', 0)->orderBy('name')->pluck('display_name', 'id'),
            'from_cv' => $request->cv,
            'landing_page' => null,
            'usertype_dropdown' => UserType::where('status_id', RecordStatus::ACTIVE->value)->pluck('description', 'id'),
            'appointment_manager_dropdown' => $service->managers(),
        ];

        return view('users.create')->with($parameters);
    }

    public function store(StoreUserRequest $request, User $user, UserService $service)
    {
        $resource_type = 0;
        $util = 0;
        if(in_array(5,$request->input('role'))){
            $resource_type = 2;
            $util = 1;
        }
        if(in_array(1,$request->input('role')) || in_array(2,$request->input('role')) || in_array(3,$request->input('role')) || in_array(4,$request->input('role'))){
            $resource_type = 1;
            $util = 1;
        }

        $password = Str::random(8);
        $user = $service->createUser($request, $user);
        $user->password = Hash::make($password);
        $user->resource_id = $service->resourceId();
        $user->login_user = ($request->has('login_user')) ? $request->login_user : 0;
        $user->save();

        $user->resource()?->update(['user_id' => $user->id,'join_date' => now(),'resource_type'=>$resource_type]);

        $service->createUserLandingPage($user->id);
        $service->createLeaveBalance($user->id);

        if ($request->input('role')) {
            $service->assignUserRoles($request, $user);
        }

        if ($request->has('onboarding')) {
            $service->createUserOnboarding($user->id);
            $message = $service->sendOnboardingEmails($user, $password);
        } elseif ($request->has('login_user')) {
            $message = $service->sendUserEmail($user, $password);
        }

        // if(auth()->user()->hasAnyRole(['admin'])){
        $client = new \GuzzleHttp\Client();
        $owner = User::first();
        try {
            $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);

            $request->session()->put('subscriptionDetail', json_encode($response->json()));
        } catch (Exception $e) {
            logger($e);
        }
        // }

        $license = $response->json();
        $subscription_package = $this->subscriptionPackage();
        $details = $subscription_package[$license['subscription']['subscription_tier']];

        if(count(User::where('login_user',1)->get()) > $details['users']){
            $message = $service->sendAdminSubscriptionEmail($details);
        }

        return redirect(route('profile', $user))->with('flash_success', $message??'User captured successfully')->with('flash_info', 'Password is: '.(($request->has('login_user')) ? $password : ''));
    }

    public function edit(User $user, UserService $service)
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $user->load('roles');

        $parameters = [
            'user' => $user,
            'user_roles' => $user->roles->pluck('id'),
            'roles' => Role::selectRaw('id, REPLACE(`name`, "_", " ") AS display_name')->where('id', '>', 0)->orderBy('name')->pluck('display_name', 'id'),
            'landing_page' => LandingPage::where('user_id', $user->id)->latest()->first(),
            'usertype_dropdown' => UserType::where('status_id', 1)->pluck('description', 'id'),
            'appointment_manager_dropdown' => $service->managers(),
        ];

        if ($user->email == 'admin@blackboardbs.com') {
            return view('users.super_edit')->with($parameters);
        }

        return view('users.edit')->with($parameters);
    }

    public function update(UpdateUserRequest $request, User $user, UserService $service)
    {
        $user = $service->updateUser($request, $user);

        $service->createUserLandingPage($user->id);

        if ($request->input('role')) {
            $roles = Role::find($request->input('role'), ['name'])
                ->map(fn ($role) => $role->name)->toArray();
            $user->syncRoles($roles);
        }

        return redirect(route('users.index'))->with('flash_success', 'User updated successfully');
    }

    public function updateSuper(Request $request, User $user): RedirectResponse
    {
        $user->expiry_date = $request->input('expiry_date');
        $user->status_id = $request->input('status');
        $user->save();

        return redirect(route('users.index'))->with('flash_success', 'User updated successfully');
    }

    public function destroy(Request $request, User $user, UserService $service)
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        if ($user->status_id == '1') {
            $user->status_id = 2;
            $user->login_user = 0;
            $user->save();

            return redirect()->route('users.index',['r'=>90])->with('flash_success', 'User successfully deactivated');
        }

        $user->status_id = 1;
        $user->login_user = 1;
        $user->save();

        return redirect()->route('users.index',['r'=>90])->with('flash_success', 'User successfully activated');
    }

    public function readNotificationsHistory(Request $request): JsonResponse
    {
        $notification = UserNotification::find($request->id);
        $notification->seen_at = now();
        $notification->save();

        return response()->json(['success' => 'success']);
    }

    public function expired_users(): View
    {
        $users = User::where('expiry_date', '<', Carbon::now()->toDateString())->paginate(15);

        $parameters = [
            'users' => $users,
        ];

        return view('users.index')->with($parameters);
    }

    public function expires_in_30(): View
    {
        $users = User::whereBetween('expiry_date', [Carbon::now()->toDateString(), Carbon::now()->addDays(30)->toDateString()])->paginate(15);

        $parameters = [
            'users' => $users,
        ];

        return view('users.index')->with($parameters);
    }

    public function resendCredentials(User $user)
    {

        $password = Str::random(8);
        $user->password = bcrypt($password);
        $user->save();

        try {
            SendWelcomeEmailJob::dispatch($user, $password,null)->delay(now()->addMinutes(5));
        }catch (\Exception $e){
            logger($e);
            return redirect()->back()->with('flash_danger', $e->getMessage());
        }


        return redirect()->back()->with('flash_success', 'User credentials has been sent successfully');
    }
    public function resendCredentialsToAdmin(User $user)
    {
        $password = Str::random(8);
        $user->password = bcrypt($password);
        $user->save();

        $admin = User::find(auth()->id());

        try {
            SendWelcomeEmailJob::dispatch($user, $password, $admin)->delay(now()->addMinutes(5));
        }catch (\Exception $e){
            logger($e);
            return redirect()->back()->with('flash_danger', $e->getMessage());
        }


        return redirect()->back()->with('flash_success', 'User credentials has been sent successfully');
    }

    public function generateUniqueCode()
    {

        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*';
        $charactersNumber = strlen($characters);
        $codeLength = 25;

        $code = '';

        while (strlen($code) < 25) {
            $position = rand(0, $charactersNumber - 1);
            $character = $characters[$position];
            $code = $code.$character;
        }

        return $code;

    }

    public function generateToken($userId){
        $user = User::find($userId);
        $token = $user->createToken('API Token')->plainTextToken;

        $api = new Api();
        $api->user = $user->id;
        $api->api_token = $token;
        $api->save();

        // return response()->json(['token' => $token], 201);
        return redirect()->back()->with('flash_success', 'User API access has been granted successfully');
    }

    public function revokeToken(User $user, $apiId)
    {
        Api::find($apiId)->delete();
        $user->tokens()->delete();
        // return response()->json(['message' => 'Tokens revoked'], 200);
        return redirect()->back()->with('flash_success', 'User API access has been revoked successfully');
    }

}
