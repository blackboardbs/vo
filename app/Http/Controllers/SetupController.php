<?php

namespace App\Http\Controllers;

use App\Models\AssessmentMaster;
use App\Models\Company;
use App\Models\Config;
use App\Models\CostCenter;
use App\Models\ModuleLinks;
use App\Models\ModuleSettings;
use App\Models\ProjectTerms;
use App\Models\Resource;
use App\Models\Roles;
use App\Models\RoleUser;
use App\Models\SetupStep;
use App\Models\SimplePermissions;
use App\Models\Site;
use App\Models\BankAccountType;
use App\Models\Template;
use App\Models\User;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Vendor;
use App\Models\InvoiceContact;
use App\Models\VatRate;
use App\Models\BillingCycle;
use App\Jobs\SendWelcomeEmailJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class SetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request): View
    {
        return view('setup.index');
    }

    /*
     * The Configurations step
     * */
    public function updateAdmin(Request $request)
    {
       if(auth()->user()->email == request()->email){ 
        $user = User::find(auth()->user()->id);
       } else {
        $user = new User;
        $user->login_user = 1;
        $user->new_setup = 0;
        $user->started_setup = 1;
        $user->expiry_date = date('Y-m-d', strtotime('2030-12-31'));
       }
       if(request()->password != '' && request()->password != 'password'){
          $user->password = Hash::make(request()->password);
       }
       $user->first_name = request()->first_name;
       $user->last_name = request()->last_name;
       $user->email = request()->email;
       $user->phone = request()->phone;
       $user->save();

       $company = Company::first();
       $company->contact_firstname = request()->first_name;
       $company->contact_lastname = request()->last_name;
       $company->email = request()->email;
       $company->phone = request()->phone;
       $company->cell = request()->cell;
       $company->save();

       
       if(auth()->user()->email != request()->email){
            $user->assignRole('admin');

            $resource = new Resource;
            $resource->user_id = $user->id;
            $resource->resource_pref_name = $request->first_name;
            $resource->creator_id = Auth()->id();
            $resource->status_id = 1;
            $resource->save();
       }

       $users = User::with('roles')->select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"),'email','phone')->get();

       $user_roles = [];
       foreach($users as $user){
        //    $user_roles[$user->id] = RoleUser::select('role_id')->where('user_id',$user->id)->pluck('role_id');
       }

       $parameters = [
           'users' => $users,
           'user_roles' => $user_roles,
       ];

       return response()->json($parameters);
    }
    
    public function updateCompany($company_id, Request $request)
    {
        // return $request->all();
        $company = Company::find($company_id);
        $company->company_name = request()->company_name == 'null' ? '' : request()->company_name;
        $company->business_reg_no = request()->busines_reg_no == 'null' ? '' : request()->busines_reg_no;
        $company->vat_no = request()->vat_no == 'null' ? '' : request()->vat_no;
        $company->web = request()->web == 'null' ? '' : request()->web;
        $company->contact_firstname = request()->contact_firstname == 'null' ? '' : request()->contact_firstname;
        $company->contact_lastname = request()->contact_lastname == 'null' ? '' : request()->contact_lastname;
        $company->email = request()->email == 'null' ? '' : request()->email;
        $company->phone = request()->phone == 'null' ? '' : request()->phone;
        $company->cell = request()->cell == 'null' ? '' : request()->cell;
        $company->postal_address_line1 = request()->postal_address_line1 == 'null' ? '' : request()->postal_address_line1;
        $company->postal_address_line2 = request()->postal_address_line2 == 'null' ? '' : request()->postal_address_line2;
        $company->postal_address_line3 = request()->postal_address_line3 == 'null' ? '' : request()->postal_address_line3;
        $company->postal_zipcode = request()->postal_zipcode == 'null' ? '' : request()->postal_zipcode;
        $company->state_province = request()->state_province == 'null' ? '' : request()->state_province;
        if(request()->country_id > 0 && request()->country_id != 'null'){
            $company->country_id = request()->country_id;
        }
        $company->account_name = request()->account_name == 'null' ? '' : request()->account_name;
        $company->bank_name = request()->bank_name == 'null' ? '' : request()->bank_name;
        $company->branch_name = request()->branch_name == 'null' ? '' : request()->branch_name;
        $company->branch_code = request()->branch_code == 'null' ? '' : request()->branch_code;
        $company->bank_acc_no = request()->bank_acc_no == 'null' ? '' : request()->bank_acc_no;
        if(request()->bank_account_type_id > 0 && request()->bank_account_type_id != 'null'){
            $company->bank_account_type_id = request()->bank_account_type_id;
        }
        $company->swift_code = request()->swift_code == 'null' ? '' : request()->swift_code;
        $company->invoice_text = request()->invoice_text == 'null' ? '' : request()->invoice_text;
        
        if ($request->hasFile('logo')) {
            try {
                $company->logo(
                    request: $request,
                    field: 'logo',
                    type: 'company',
                    height: 200
                );

                $company->company_logo = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }
        $company->save();

        $config = Config::first();
        $config->site_name = $config->site_name == '' || $config->site_name == 'null' ? (request()->company_name == 'null' ? '' : request()->company_name) : $config->site_name;
        $config->save();

       $parameters = [
        'company' => $company
       ];

        return response()->json($parameters);
    }

    public function storeUser(Request $request)
    {
        $owner = User::first();
                try {
                    $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);
                    if ($response->status() != 404){

                        $request->session()->put('subscriptionDetail', json_encode($response->json()));
                        $license = $response->json();
                    }
                } catch (\Exception $e) {
                    logger($e);
                }
                
        $subscription_package = $this->subscriptionPackage();
        $details = $subscription_package[$license['subscription']['subscription_tier']];

            $name = explode(' ',$request->name);
            $user = new User();
            $user->first_name = $name[0];
            $user->last_name = (isset($name[1])?' '.$name[1]:'').(isset($name[2])?' '.$name[2]:'').(isset($name[3])?' '.$name[3]:'').(isset($name[4])?' '.$name[4]:'').(isset($name[5])?' '.$name[5]:'');
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->company_id = $owner->company_id;
            if(count(User::where('login_user',1)->get()) < $details['users']){
                $user->login_user = 1;
            } else {
                $user->login_user = 0;
            }
            $user->expiry_date = date('Y-m-d', strtotime('2030-12-31'));
            $user->password = Hash::make('secret');

            if ($request->hasFile('logo')) {
                try {
                    $user->thumbnail(
                        request: $request,
                        field: 'logo',
                        type: 'user',
                        height: 200,
                        width: 200
                    );
    
                    $user->avatar = $request->file('logo')->hashName();
                } catch (\Exception $e) {
                    $exception_message = 'Logo could not be uploaded.';
                }
            }

            $user->save();

            $roles = explode(',',$request->role);

            foreach($roles as $role){
                $user->assignRole($role);
            }

            $resource = new Resource;
            $resource->user_id = $user->id;
            $resource->resource_pref_name = $name[0];
            $resource->creator_id = Auth()->id();
            $resource->status_id = 1;
            $resource->save();

            $users = User::with('roles')->select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"),'email','phone','avatar')->get();

            $user_roles = [];
            foreach($users as $user){
                // $user_roles[$user->id] = RoleUser::select('role_id')->where('user_id',$user->id)->pluck('role_id');
            }

            $parameters = [
                'users' => $users,
                'user_roles' => $user_roles,
            ];

            return response()->json($parameters);
    }

    public function updateUser(Request $request,$user_id){

        $name = explode(' ',$request->name);
        $user = User::find($user_id);
        $user->first_name = $name[0];
        $user->last_name = (isset($name[1])?' '.$name[1]:'').(isset($name[2])?' '.$name[2]:'').(isset($name[3])?' '.$name[3]:'').(isset($name[4])?' '.$name[4]:'').(isset($name[5])?' '.$name[5]:'');
        $user->email = $request->email;
        $user->phone = $request->phone;

        
        if ($request->hasFile('logo')) {
            try {
                $user->thumbnail(
                    request: $request,
                    field: 'logo',
                    type: 'user',
                    height: 200,
                    width: 200
                );

                $user->avatar = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }

        $user->save();

        $roles = explode(',',$request->role);

        // RoleUser::where('user_id',$user_id)->delete();

        // foreach($roles as $role){
            $user->syncRoles($roles);
        // }

        $resource = Resource::where('user_id',$user_id)->update(['resource_pref_name' => $name[0],'creator_id' => Auth()->id(),'status_id' => 1]);

        $users = User::with('roles')->select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"),'email','phone','avatar')->get();

        $user_roles = [];
        foreach($users as $user){
            // $user_roles[$user->id] = RoleUser::select('role_id')->where('user_id',$user->id)->pluck('role_id');
        }

        $parameters = [
            'users' => $users,
            'user_roles' => $user_roles,
        ];

        return response()->json($parameters);
    }
    
    public function deleteUser(Request $request,$user_id){

        $user = User::find($user_id);

        $user->syncRoles([]);

        User::destroy($user_id);

        $users = User::select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"),'email','phone')->get();

        $parameters = [
            'users' => $users
        ];

        return response()->json($parameters);
    }

    public function storeContact(Request $request){
        $contact = new InvoiceContact;
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->contact_number = $request->contact_number;
        $contact->birthday = Carbon::parse($request->birthday)->format('Y-m-d');
        $contact->status_id = 1;
        $contact->save();

        $parameters = [
            'contacts' => InvoiceContact::get()
        ];

        return response()->json($parameters);
    }

    public function updateContact(Request $request,$contact_id){
        $contact = InvoiceContact::find($contact_id);
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->contact_number = $request->contact_number;
        $contact->birthday = Carbon::parse($request->birthday)->format('Y-m-d');
        $contact->status_id = 1;
        $contact->save();

        $parameters = [
            'contacts' => InvoiceContact::get()
        ];

        return response()->json($parameters);
    }
    
    public function deleteContact(Request $request,$contact_id){

        InvoiceContact::destroy($contact_id);

        $contacts = InvoiceContact::get();

        $parameters = [
            'contacts' => $contacts
        ];

        return response()->json($parameters);
    }

    public function storeCustomer(Request $request){
        $customer = new Customer();
        $customer->customer_name = $request->input('customer_name');
        $customer->business_reg_no = $request->input('business_reg_no');
        $customer->vat_no = $request->input('vat_no');
        $customer->account_manager = $request->input('account_manager');
        $customer->street_address_line1 = $request->input('street_address_line1');
        $customer->street_address_line2 = $request->input('street_address_line2');
        $customer->street_address_line3 = $request->input('street_address_line3');
        $customer->street_city_suburb = $request->input('street_city_suburb');
        $customer->street_zipcode = $request->input('street_zipcode');
        $customer->state_province = $request->input('state_province');
        $customer->country_id = $request->input('country_id');
        $customer->payment_terms = $request->input('payment_terms');
        $customer->creator_id = auth()->id();
        $customer->billing_cycle_id = $request->input('billing_cycle_id');
        if($request->input('invoice_contact_id') != 'null'){
            $customer->invoice_contact_id = $request->input('invoice_contact_id');
        }
        $customer->status = 1;
        $exception_message = '';

        
        if ($request->hasFile('logo')) {
            try {
                $customer->logo(
                    request: $request,
                    field: 'logo',
                    type: 'customer',
                    height: 300
                );

                $customer->logo = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }
        $customer->save();

        
        $parameters = [
            'customers' => Customer::get()
        ];

        return response()->json($parameters);
    }

    public function updateCustomer(Request $request,$customer_id){
        $customer = Customer::find($customer_id);
        $customer->customer_name = $request->input('customer_name') == 'null' ? '' : $request->input('customer_name');
        $customer->business_reg_no = $request->input('business_reg_no') == 'null' ? '' : $request->input('business_reg_no');
        $customer->vat_no = $request->input('vat_no') == 'null' ? '' : $request->input('vat_no');
        $customer->account_manager = $request->input('account_manager');
        $customer->street_address_line1 = $request->input('street_address_line1') == 'null' ? '' : $request->input('street_address_line1');
        $customer->street_address_line2 = $request->input('street_address_line2') == 'null' ? '' : $request->input('street_address_line2');
        $customer->street_address_line3 = $request->input('street_address_line3') == 'null' ? '' : $request->input('street_address_line3');
        $customer->street_city_suburb = $request->input('street_city_suburb') == 'null' ? '' : $request->input('street_city_suburb');
        $customer->street_zipcode =  $request->input('street_zipcode') == 'null' ? '' : $request->input('street_zipcode');
        $customer->state_province = $request->input('state_province') == 'null' ? '' : $request->input('state_province');
        $customer->country_id = $request->input('country_id');
        $customer->payment_terms = $request->input('payment_terms');
        $customer->creator_id = auth()->id();
        $customer->status = 1;
        if($request->input('billing_cycle_id') > 0){
            $customer->billing_cycle_id = $request->input('billing_cycle_id');
        }
        if($request->input('invoice_contact_id') != 'null'){
            $customer->invoice_contact_id = $request->input('invoice_contact_id');
        }
        $exception_message = '';

        if ($request->hasFile('logo')) {
            try {
                $customer->logo(
                    request: $request,
                    field: 'logo',
                    type: 'customer',
                    height: 300
                );

                $customer->logo = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }
        $customer->save();

        
        $parameters = [
            'customers' => Customer::get()
        ];

        return response()->json($parameters);
    }

    public function deleteCustomer(Request $request,$customer_id){
        Customer::destroy($customer_id);
        $parameters = [
            'customers' => Customer::get()
        ];

        return response()->json($parameters);
    }

    public function storeVendor(Request $request){
        $customer = new Vendor();
        $customer->vendor_name = $request->input('vendor_name');
        $customer->business_reg_no = $request->input('business_reg_no');
        $customer->vat_no = $request->input('vat_no');
        $customer->account_manager = $request->input('account_manager');
        $customer->street_address_line1 = $request->input('street_address_line1');
        $customer->street_address_line2 = $request->input('street_address_line2');
        $customer->street_address_line3 = $request->input('street_address_line3');
        $customer->street_city_suburb = $request->input('street_city_suburb');
        $customer->street_zipcode = $request->input('street_zipcode');
        $customer->state_province = $request->input('state_province');
        $customer->country_id = $request->input('country_id');
        $customer->payment_terms = $request->input('payment_terms_days');
        $customer->creator_id = auth()->id();
        $customer->status_id = 1;
        if($request->input('invoice_contact_id') != 'null'){
        $customer->invoice_contact_id = $request->input('invoice_contact_id');
        }
        $exception_message = '';

        if ($request->hasFile('logo')) {
            try {
                $customer->logo(
                    request: $request,
                    field: 'logo',
                    type: 'vendor',
                    height: 200
                );

                $customer->vendor_logo = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $feedback_message = 'Logo could not be uploaded.';
            }
        }
        $customer->save();

        
        $parameters = [
            'vendors' => Vendor::get()
        ];

        return response()->json($parameters);
    }

    public function updateVendor(Request $request,$vendor_id){
        $customer = Vendor::find($vendor_id);
        $customer->vendor_name = $request->input('vendor_name') == 'null' ? '' : $request->input('vendor_name');
        $customer->business_reg_no = $request->input('business_reg_no') == 'null' ? '' : $request->input('business_reg_no');
        $customer->vat_no = $request->input('vat_no') == 'null' ? '' : $request->input('vat_no');
        $customer->account_manager = $request->input('account_manager');
        $customer->street_address_line1 = $request->input('street_address_line1') == 'null' ? '' : $request->input('street_address_line1');
        $customer->street_address_line2 = $request->input('street_address_line2') == 'null' ? '' : $request->input('street_address_line2');
        $customer->street_address_line3 = $request->input('street_address_line3') == 'null' ? '' : $request->input('street_address_line3');
        $customer->street_city_suburb = $request->input('street_city_suburb') == 'null' ? '' : $request->input('street_city_suburb');
        $customer->street_zipcode = $request->input('street_zipcode') == 'null' ? '' : $request->input('street_zipcode');
        $customer->state_province = $request->input('state_province') == 'null' ? '' : $request->input('state_province');
        $customer->country_id = $request->input('country_id');
        $customer->payment_terms = $request->input('payment_terms_days');
        $customer->creator_id = auth()->id();
        $customer->status_id = 1;
        if($request->input('invoice_contact_id') != 'null'){
        $customer->invoice_contact_id = $request->input('invoice_contact_id');
        }
        $exception_message = '';

        if ($request->hasFile('logo')) {
            try {
                $customer->logo(
                    request: $request,
                    field: 'logo',
                    type: 'vendor',
                    height: 200
                );

                $customer->vendor_logo = $request->file('logo')->hashName();
            } catch (\Exception $e) {
                $feedback_message = 'Logo could not be uploaded.';
            }
        }
        $customer->save();

        
        $parameters = [
            'vendors' => Vendor::get()
        ];

        return response()->json($parameters);
    }

    public function deleteVendor(Request $request,$vendor_id){
        Vendor::destroy($vendor_id);
        $parameters = [
            'vendors' => Vendor::get()
        ];

        return response()->json($parameters);
    }

    public function configs(Request $request)
    {

        $admin = User::role('admin')->latest()->get()->first();
        if(!$admin){
            $user = User::first();
            $user->assignRole('admin');
        }
        $users = User::with('roles')->select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"),'email','phone','avatar')->get();
        $all_users = User::select('id', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as name"))->pluck('name','id');

        $user_roles = [];
        foreach($users as $user){
            // $user_roles[$user->id] = RoleUser::select('role_id')->where('user_id',$user->id)->pluck('role_id');
        }
        
        $owner = User::first();
                try {
                    $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);
                    if ($response->status() != 404){

                        $request->session()->put('subscriptionDetail', json_encode($response->json()));
                        $license = $response->json();
                    }
                } catch (\Exception $e) {
                    logger($e);
                }

        $parameters = [
            'subscription_package'=>$this->subscriptionPackage(),
            'license'=>$license,
            'admin'=>$admin,
            'config' => Config::first(),
            'company'=> Company::first(),
            'contacts' => InvoiceContact::get(),
            'customers' => Customer::get(),
            'vendors' => Vendor::get(),
            'countries' => Country::get(),
            'users' => $users,
            'all_users' => $all_users,
            'user_roles' => $user_roles,
            'roles_drop_down' => Roles::select('display_name', 'id')->pluck('display_name', 'id'),
            'bank_account_type_drop_down' => BankAccountType::select('description', 'id')->pluck('description', 'id'),
            'vat_rate_drop_down' => VatRate::select(DB::raw("CONCAT(description, ' ', vat_rate, '%') AS vat"), 'vat_code')->pluck('vat', 'vat_code')->prepend('Select Default Vat Rate',null),
            'billing_cycles' => BillingCycle::where('status_id', 1)->pluck('name', 'id'),
        ];

        
        return response()->json($parameters);
    }

    public function configsUpdate(Request $request, $config_id)
    {
        $config = Config::first();
        $config->site_title = $request->input('site_title');
        $config->site_name = $request->input('site_name');
        $config->admin_email = $request->input('admin_email');
        $config->days_to_approve_leave = $request->input('days_to_approve_leave');
        $config->annual_leave_days = $request->input('annual_leave_days');
        $config->vat_rate_id = $request->input('vat_rate_id');
        $config->logo_placement = $request->input('logo_placement');
        $config->customer_invoice_prefix = $request->input('customer_invoice_prefix');
        $config->customer_invoice_format = $request->input('customer_invoice_format');
        $config->customer_invoice_start_number = $request->input('customer_invoice_start_number');
        $config->vendor_invoice_prefix = $request->input('vendor_invoice_prefix');
        $config->vendor_invoice_format = $request->input('vendor_invoice_format');
        $config->vendor_invoice_start_number = $request->input('vendor_invoice_start_number');
        if ($request->background_color != $request->font_color) {
            $config->background_color = str_replace('#','',request()->background_color);
            $config->font_color = str_replace('#','',request()->font_color);
            $config->active_link = str_replace('#','',request()->active_link);
        }
        $exception_message = '';
        if ($request->hasFile('logo')) {
            try {
                $request->file('logo')->store('public/assets');
                $file = $request->file('logo')->hashName();
                $config->site_logo = $file;

                $image = Image::make(base_path().'/public/assets/'.$file);
                $image->resize(null, 120, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save();
            } catch (\Exception $e) {
                $exception_message = 'Logo could not be uploaded.';
            }
        }
        $config->save();

        $parameters = [
            'config' => $config
        ];

        return response()->json($parameters);
    }

    public function finishSetup(Request $request){
        
        $users = User::where('id','!=',auth()->user()->id)->get();

        foreach($users as $user){
            $password = Str::password(12);
            $u = User::find($user->id);
            $u->password = Hash::make($password);
            $u->save();
            try {
                SendWelcomeEmailJob::dispatch($u, $password,null)->delay(now()->addMinutes(5));
            }catch (\Exception $e){
                logger($e);
                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }

        $user = User::find(auth()->user()->id);
        $user->new_setup = 0;
        $user->save();

        if($user){
            return response()->json(['message'=>'success']);
        } else {
            return response()->json(['message'=>'error']);
        }
    }

    public function startedSetup(Request $request){
        $user = User::find(auth()->user()->id);
        $user->started_setup = 1;
        $user->save();

        if($user){
            return response()->json(['message'=>'success']);
        } else {
            return response()->json(['message'=>'error']);
        }
    }

    public function welcome(){
        
        return view('welcome');
    }
}
