<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProjectHoursSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 15;
        $timesheet = [];
        $forecast_hours = [];
        $po_hours = [];
        $year_dropdown = [];
        $year_month_dropdown = [];
        $date = Carbon::now();
        $projects = Project::with(['customer'])->whereIn('status_id', [2, 3]);
        if ($request->has('company') && $request->company != null) {
            $projects = $projects->where('company_id', '=', $request->company);
        }
        if ($request->has('customer') && $request->customer != null) {
            $projects = $projects->where('customer_id', '=', $request->customer);
        }
        if ($request->has('billable') && $request->billable != null) {
            $projects = $projects->where('billable', '=', $request->billable);
        }
        $projects = $projects->sortable('name')->paginate($item);
        $years = Timesheet::select('year')->groupBy('year')->get();
        $year_mon = Timesheet::select('year', 'month')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        foreach ($years as $year) {
            $year_dropdown[$year->year] = $year->year;
        }
        foreach ($year_mon as $period) {
            $year_month_dropdown[$period->year.$period->month] = $period->year.(($period->month < 10) ? '0'.$period->month : $period->month);
        }
        foreach ($projects as $project) {
            $time = Timesheet::selectRaw('IFNULL(SUM(CASE WHEN timeline.is_billable = '.$project->billable?->value.' THEN timeline.total + (timeline.total_m/60) ELSE 0 END), 0) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $project->id);
            if ($request->has('year') && $request->year != '') {
                $time = $time->where('timesheet.year', '=', $request->year);
            }
            if (($request->has('period_from') && $request->period_from != '') && ($request->has('period_to') && $request->period_to != '')) {
                $time = $time->whereBetween('timesheet.year', [substr($request->period_from, 0, 4), substr($request->period_to, 0, 4)])->whereBetween('timesheet.month', [substr($request->period_from, 4), substr($request->period_to, 4)]);
            }
            $time = $time->first();

            $ass = Assignment::where('project_id', '=', $project->id)->whereIn('assignment_status', [2, 3])->get();

            $ass_hours = 0;
            $forecast_h = 0;
            foreach ($ass as $hours) {
                $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $hours->planned_end_date])->get()->count();
                $ass_hours += $hours->hours;
                $forecast_h += ($calendar * (($hours->capacity_allocation_hours == null && $hours->capacity_allocation_min == null) ? 8 : ($hours->capacity_allocation_hours + ($hours->capacity_allocation_min / 60))));
            }
            array_push($timesheet, $time);
            array_push($forecast_hours, ($forecast_h));
            array_push($po_hours, $ass_hours);
        }
        $total_hours = 0;
        foreach ($timesheet as $time) {
            $total_hours += $time->hours;
        }
        $total_forecast_hours = 0;
        foreach ($forecast_hours as $forecast) {
            $total_forecast_hours += $forecast;
        }
        $total_po_hours = 0;
        foreach ($po_hours as $po_hour) {
            $total_po_hours += $po_hour;
        }
        $parameters = [
            'company_drop_down' => Company::filters()->select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'projects' => $projects,
            'timesheets' => $timesheet,
            'forecast_hours' => $forecast_hours,
            'po_hours' => $po_hours,
            'year_dropdown' => $year_dropdown,
            'year_month_dropdown' => $year_month_dropdown,
            'date' => $date,
            'total_hours' => $total_hours,
            'total_forecast_hours' => $total_forecast_hours,
            'total_po_hours' => $total_po_hours,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'projecthours');

        return view('reports.management.projecthours')->with($parameters);
    }
}
