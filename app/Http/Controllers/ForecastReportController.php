<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Resource;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ForecastReportController extends Controller
{
    public function index(Request $request): View
    {
        $assignments = Assignment::whereIn('assignment_status', [2, 3]);

        $project_counter = Project::whereNotIn('status_id', [4, 5])->get()->count();
        $resource_counter = Resource::where('status_id', 1)->get()->count();
        $vendor_counter = Vendor::where('status_id', 1)->get()->count();
        $customer_counter = Customer::where('status', 1)->get()->count();

        if ($request->has('project') && $request->input('project') != '') {
            $assignments = $assignments->where('project_id', $request->input('project'));

            $project_counter = Project::whereNotIn('status_id', [4, 5])->where('id', '=', $request->input('project'))->get()->count();
            $_project = Project::whereNotIn('status_id', [4, 5])->where('id', '=', $request->input('project'))->first();

            $resource_counter = 0;
            $vendor_counter = 0;
            if (isset($_project->consultants)) {
                $_resources = explode(',', $_project->consultants);
                $vendor_counter = 0;
                foreach ($_resources as $_resource) {
                    $_user = User::find($_resource);
                    if ($_user->vendor_id > 0) {
                        $vendor_counter += 1;
                    }
                }
                $resource_counter = count($_resources);
            }

            $customer_counter = 0;
            if (isset($_project->customer_id)) {
                if ($_project->customer_id > 0);
                $customer_counter += 1;
            }
        }

        if ($request->has('company') && $request->input('company') != '') {
            $assignments = $assignments->where('company_id', $request->input('company'));
        }

        if ($request->has('resource') && $request->input('resource') != '') {
            $assignments = $assignments->where('employee_id', $request->input('resource'));

            $_projects = Project::whereNotIn('status_id', [4, 5])->get();
            $project_counter = 0;
            $customer_counter = 0;
            $vendor_counter = 0;
            foreach ($_projects as $_project) {
                $consultants_array = explode(',', $_project->consultants);
                if (in_array($request->input('resource'), $consultants_array)) {
                    $project_counter += 1;

                    if (isset($_project->customer_id)) {
                        if ($_project->customer_id > 0);
                        $customer_counter += 1;
                    }
                }
            }

            $_user = User::where('id', '=', $request->input('resource'))->first();
            $resource_counter = User::where('id', '=', $request->input('resource'))->get()->count();

            if ($_user->vendor_id > 0) {
                $vendor_counter += 1;
            }
        }

        $assignments = $assignments->get();

        $assignment_actual_hours_array = [];
        $assignment_forecast_hours_array = [];

        $total_actual_hour_month = [];
        $total_forecust_hour_month = [];
        $total_budget_hour_month = [];
        $total_forecust_hour_month_spend = [];
        $total_actual_hour_month_spend = [];

        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $actual_hours_total = 0;
        $forecast_hours_total = 0;
        $po_hours_total = 0;
        $planned_hours_total = 0;
        $variance_hours_total = 0;
        $actual_spend_total = 0;
        $forecast_spend_total = 0;
        $budget_spend_total = 0;
        $planned_spend_total = 0;
        $variance_spend_total = 0;

        $date = Carbon::now();

        $year = 2019;
        if ($request->has('year') && $request->input('year') != '') {
            $year = $request->input('year');
        }

        foreach ($assignments as $assignment) {
            $calendar_working_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->start_date, $assignment->end_date])->get()->count();
            $forecast_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $assignment->end_date])->get()->count();
            $assignment_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->start_date, $assignment->end_date])->get()->count();
            $number_of_month = Calendar::select('month', 'year')->whereBetween('date', [$assignment->start_date, $assignment->end_date])->groupBy('month', 'year')->get()->count();

            $actual_time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();

            $capacity_allocation_hours = $assignment->capacity_allocation_hours > 0 ? $assignment->capacity_allocation_hours : 8;
            $budget = Assignment::selectRaw('SUM(CASE WHEN billable = '.$assignment->billable?->value.' THEN hours * '.(($assignment->billable?->value == 1) ? 'invoice_rate' : 'internal_cost_rate + external_cost_rate').' ELSE 0 END) AS value')->where('customer_po', '=', $assignment->customer_po)->where('project_id', '=', $assignment->project_id)->first();
            $rate = ($assignment->billable == 1) ? $assignment->invoice_rate : ($assignment->internal_cost_rate + $assignment->external_cost_rate);

            $actual_hours_total += $actual_time->hours;
            $forecast_hours_total += $forecast_days * $capacity_allocation_hours;
            $po_hours_total += $assignment->hours;
            $planned_hours_total += $actual_time->hours + ($forecast_days * $capacity_allocation_hours);
            $variance_hours_total += $assignment->hours - ($actual_time->hours + ($forecast_days * $capacity_allocation_hours));
            $actual_spend_total += $actual_time->hours * $rate;
            $forecast_spend_total += ($forecast_days * $capacity_allocation_hours) * $rate;
            $budget_spend_total += $assignment->hours * $rate;
            $planned_spend_total += ($actual_time->hours * $rate) + (($forecast_days * $capacity_allocation_hours) * $rate);
            $variance_spend_total += ($assignment->hours * $rate) - ($actual_time->hours * $rate) + (($forecast_days * $capacity_allocation_hours) * $rate);

            $year_week_array = [];
            $actual_hours_array = [];
            $forecast_days_remaining = $forecast_days;
            $forecast_hours_array = [];

            for ($i = $year; $i <= date('Y'); $i++) {
                for ($j = 1; $j <= 12; $j++) {
                    $j = $j < 10 ? '0'.$j : $j;

                    $actual_hours_captured = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.project_id', '=', $assignment->project_id)
                        ->where('timesheet.employee_id', '=', $assignment->employee_id)
                        ->where('timesheet.year', '=', $i)
                        ->where('timesheet.month', '=', $j)
                        ->first();

                    $year_week_array[] = $i.$j;
                    $actual_hours_array[] = $actual_hours_captured;
                    if ($i == date('Y') && $j >= date('m')) {
                        $days_for_month = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->where('year', '=', $i)->where('month', '=', $j)->whereBetween('date', [$date->now()->toDateString(), $assignment->end_date])->get()->count();
                        $month_forecast_hours = $days_for_month * $capacity_allocation_hours;

                        $forecast_hours_array[] = $month_forecast_hours;

                        if (isset($total_focust_hour_month[$i.$j])) {
                            $total_forecust_hour_month[$i.$j] += $month_forecast_hours;
                            $total_forecust_hour_month_spend[$i.$j] += $total_forecust_hour_month[$i.$j] * $rate;
                        } else {
                            $total_forecust_hour_month[$i.$j] = $month_forecast_hours;
                            $total_forecust_hour_month_spend[$i.$j] = $total_forecust_hour_month[$i.$j] * $rate;
                        }

                        /*if(isset($total_forecust_hour_month_spend[$i.$j])){
                            $total_forecust_hour_month_spend[$i . $j] += $month_forecast_hours * $rate;
                        }
                        else{
                            $total_forecust_hour_month_spend[$i.$j] = $month_forecast_hours * $rate;
                        }*/

                        $forecast_days_remaining -= $month_forecast_hours;
                    } else {
                        $forecast_hours_array[] = '';
                        $total_forecust_hour_month[$i.$j] = 0;
                    }

                    if (isset($total_actual_hour_month[$i.$j])) {
                        $total_actual_hour_month[$i.$j] += $actual_hours_captured->hours != null ? $actual_hours_captured->hours : 0;
                    } else {
                        $total_actual_hour_month[$i.$j] = $actual_hours_captured->hours != null ? $actual_hours_captured->hours : 0;
                    }

                    if (isset($total_actual_hour_month_spend[$i.$j])) {
                        $total_actual_hour_month_spend[$i.$j] += $actual_hours_captured->hours != null ? $actual_hours_captured->hours * $rate : 0;
                    } else {
                        $total_actual_hour_month_spend[$i.$j] = $actual_hours_captured->hours != null ? $actual_hours_captured->hours * $rate : 0;
                    }

                    if (isset($total_budget_hour_month[$i.$j])) {
                        $total_budget_hour_month[$i.$j] += $number_of_month > 0 ? $assignment->hours / $number_of_month : 0;
                    } else {
                        $total_budget_hour_month[$i.$j] = $number_of_month > 0 ? $assignment->hours / $number_of_month : 0;
                    }
                }
            }

            $assignment_actual_hours_array[] = $actual_hours_array;
            $assignment_forecast_hours_array[] = $forecast_hours_array;
        }

        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', '');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');

        $tmp_total_forecust_hour_month_spend = [];
        foreach ($total_actual_hour_month_spend as $key => $value) {
            if (! isset($total_forecust_hour_month_spend[$key])) {
                $tmp_total_forecust_hour_month_spend[$key] = 0;
            } else {
                $tmp_total_forecust_hour_month_spend[$key] = $total_forecust_hour_month_spend[$key];
            }
        }

        $total_forecust_hour_month_spend = $tmp_total_forecust_hour_month_spend;

        $parameters = [
            'projects' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'company' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'project_counter' => $project_counter,
            'resource_counter' => $resource_counter,
            'vendor_counter' => $vendor_counter,
            'customer_counter' => $customer_counter,
            'actual_hours_total' => $actual_hours_total,
            'forecast_hours_total' => $forecast_hours_total,
            'po_hours_total' => $po_hours_total,
            'planned_hours_total' => $planned_hours_total,
            'planned_hours_total' => $planned_hours_total,
            'variance_hours_total' => $variance_hours_total,
            'actual_spend_total' => $actual_spend_total,
            'forecast_spend_total' => $forecast_spend_total,
            'budget_spend_total' => $budget_spend_total,
            'planned_spend_total' => $planned_spend_total,
            'planned_spend_total' => $planned_spend_total,
            'variance_spend_total' => $variance_spend_total,
            'assignment_actual_hours_array' => $assignment_actual_hours_array,
            'assignment_forecast_hours_array' => $assignment_forecast_hours_array,
            'total_actual_hour_month' => $total_actual_hour_month,
            'total_forecust_hour_month' => $total_forecust_hour_month,
            'total_budget_hour_month' => $total_budget_hour_month,
            'total_forecust_hour_month_spend' => $total_forecust_hour_month_spend,
            'total_actual_hour_month_spend' => $total_actual_hour_month_spend,
        ];

        return view('reports.forecast.analysis')->with($parameters);
    }
}
