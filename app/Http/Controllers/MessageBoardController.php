<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageBoardRequest;
use App\Models\MessageBoard;
use App\Services\MessageBoardService;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class MessageBoardController extends Controller
{
    public function index(Request $request, MessageBoardService $service): View
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $messages = MessageBoard::with(['status:id,description'])
            ->select(['id', 'message', 'status_id'])
            ->when($request->q, fn($message) => $message->where('message','like','%'.$request->q.'%'))
            ->latest()
            ->paginate($request->input('r') ?? 15);

        $parameters = [
            'messages' => $messages,
            'ability' => $service->hasCreatePermissions()
        ];

        return view('messages.index')->with($parameters);
    }

    public function create(MessageBoardService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        return view('messages.create');
    }

    public function store(MessageBoardRequest $request, MessageBoard $message, MessageBoardService $service): RedirectResponse
    {
        $service->createMessage($request, $message);

        return redirect()->route('messages.index')->with('flash_success', 'Your Message have been captured successfully');
    }

    public function show(MessageBoard $message, MessageBoardService $service): View
    {
        abort_unless($service->hasViewPermission(), 403);

        return view('messages.show')->with(['message' => $message]);
    }

    public function edit(MessageBoard $message, MessageBoardService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        return view('messages.edit')->with(['message' => $message]);
    }

    public function update(MessageBoardRequest $request, MessageBoard $message, MessageBoardService $service): RedirectResponse
    {
        $service->updateMessage($request, $message);

        return redirect()->route('messages.index')->with('flash_success', 'Your Message has been updated successfully');
    }

    public function destroy(MessageBoard $message, MessageBoardService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $message->delete();

        return redirect()->route('messages.index')->with('flash_success', 'Your Message has been deleted');
    }
}
