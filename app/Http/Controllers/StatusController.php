<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStatusRequest;
use App\Http\Requests\UpdateStatusRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StatusController extends Controller
{
    private $row_order;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $status = Status::sortable('order', 'asc')->paginate($item);

        $status = Status::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $status = Status::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $status = Status::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $status = $status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $status = $status->paginate($item);

        $parameters = [
            'status' => $status,
            //'status_description' => Status::where('status', '=', $status->status)->pluck('description')
        ];

        return View('master_data.status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Status::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.status.create')->with($parameters);
    }

    public function store(StoreStatusRequest $request): RedirectResponse
    {
        $status = new Status();
        $status->description = $request->input('description');
        $status->status = $request->input('status_id');
        $status->order = Status::max('order') > 1 ? Status::max('order') + 1 : 1; //++Status::last()->order;
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('status.index'))->with('flash_success', 'Master Data Status captured successfully');
    }

    public function show($status_id): View
    {
        $parameters = [
            'status' => Status::where('id', '=', $status_id)->get(),
        ];

        return View('master_data.status.show')->with($parameters);
    }

    public function edit($status_id): View
    {
        $autocomplete_elements = Status::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status' => Status::where('id', '=', $status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.status.edit')->with($parameters);
    }

    public function update(UpdateStatusRequest $request, $status_id): RedirectResponse
    {
        $status = Status::find($status_id);
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->order = $status->order;
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('status.index'))->with('flash_success', 'Master Data Status saved successfully');
    }

    public function destroy($industry_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // Status::destroy($industry_id);
        $item = Status::find($industry_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('status.index')->with('success', 'Master Data Status deleted successfully');
    }
}
