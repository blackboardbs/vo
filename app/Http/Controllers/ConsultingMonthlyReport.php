<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ConsultingMonthlyReport extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $timesheet = Timesheet::select(DB::raw('
            timesheet.year, timesheet.month, timesheet.employee_id, timesheet.customer_id, timesheet.project_id, 
            SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS hours
        '))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->with(['employee', 'customer', 'project']);
        if ($request->has('company') && $request->company != null) {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }

        if ($request->has('team') && $request->team != null) {
            $timesheet = $timesheet->whereHas('employee.resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee != null) {
            $timesheet = $timesheet->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $timesheet = $timesheet->where('year', '=', substr($request->yearmonth, 0, 4))
                ->where('month', '=', (substr($request->yearmonth, 4, 2) < 10) ? substr($request->yearmonth, 5, 1) : substr($request->yearmonth, 4, 2));
        }
        if (($request->has('date_from') && $request->date_from != '') || ($request->has('date_to') && $request->date_to != '')) {
            $timesheet = $timesheet->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })
            ->whereBetween('calendar.date', [$request->date_from, $request->date_to]);
        }
        if ($request->has('customer') && $request->customer != null) {
            $timesheet = $timesheet->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project != null) {
            $timesheet = $timesheet->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable != null) {
            $timesheet = $timesheet->where('timeline.is_billable', '=', $request->billable);
        }

        $timesheet = $timesheet->whereHas('employee')
            ->whereHas('customer')
            ->whereHas('project')
            ->groupBy('timesheet.year')
            ->groupBy('timesheet.month')
            ->groupBy('timesheet.employee_id')
            ->groupBy('timesheet.customer_id')
            ->groupBy('timesheet.project_id')
            ->orderBy('timesheet.year', 'desc')
            ->orderBy('timesheet.month', 'desc')
            ->orderBy('timesheet.employee_id', 'desc')
            ->paginate($item);

        $total_hours = 0;
        foreach ($timesheet as $time) {
            $total_hours += $time->hours;
        }

        $calendar = Calendar::select('year', 'month')->whereDate('date', '<=', Carbon::now()->toDateString())->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'timesheet' => $timesheet,
            'total_hours' => $total_hours,

        ];

        return view('reports.consulting_monthly_report')->with($parameters);
    }
}
