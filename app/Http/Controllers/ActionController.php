<?php

namespace App\Http\Controllers;

use App\Exports\ActionsExport;
use App\Exports\LeaveBalancesExport;
use App\Models\Action;
use App\Models\ActionNote;
use App\Models\ActionStatus;
use App\Models\Board;
use App\Models\Customer;
use App\Models\CustomerInvoice;
use App\Models\Document;
use App\Http\Requests\ActionRequest;
use App\Jobs\SendActionEmailJob;
use App\Models\JobSpec;
use App\Models\Module;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Prospect;
use App\Models\RejectionReason;
use App\Models\Sprint;
use App\Models\Status;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\Team;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\UserStory;
use App\Models\Vendor;
use App\Models\VendorInvoice;
use App\Services\StorageQuota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Response;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ActionController extends Controller
{
    public function index(Request $request)
    {
        $item = isset($request->r) ? $request->r : 15;

        $actions = Action::with(['assignedUser:id,first_name,last_name', 'status:id,name'])->orderBy('id', 'desc');

        $module = Module::where('name', '=', \App\Models\Action::class)->first();

        $team_ids = [];
        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            $team_ids = $actions->pluck('id')->toArray();
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $team_ids = $actions->whereIn('assigned_user_id', Auth::user()->team())->pluck('id')->toArray();
            }
        } else {
            return abort(403);
        }

        $actions = $actions->paginate($item);

        $user_ids = [];
        foreach ($actions as $action) {
            $is_in = false;
            if ($action->assigned_user_id == Auth::user()->id) {
                $user_ids[] = $action->id;
                $is_in = true;
            }

            $related_user_ids = explode('|', $action->related_users);
            if (in_array(Auth::user()->id, $related_user_ids) && ($is_in == false)) {
                $user_ids[] = $action->id;
            }
        }

        $team_and_related_users_ids = array_merge($team_ids, $user_ids);

        $actions = Action::with(['assignedUser:id,first_name,last_name', 'status:id,name'])->orderBy('id')->whereIn('id', $team_and_related_users_ids);

        if ($request->has('date_from') && $request->input('date_from') != '') {
            $actions->where(function ($query) use ($request) {
                $query->where('created_at', '>=', $request->input('date_from'));
            });
        }

        if ($request->has('date_to') && $request->input('date_to') != '') {
            $actions->where(function ($query) use ($request) {
                $query->where('created_at', '<=', $request->input('date_to'));
            });
        }

        if ($request->has('resource') && $request->input('resource') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('assigned_user_id', '=', $request->input('resource'));
            });
        }

        if ($request->has('team') && $request->input('team') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('team_id', '=', $request->input('team'));
            });
        }

        if ($request->has('customer') && $request->input('customer') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_id', '=', $request->input('customer'));
            });
        }

        if ($request->has('project') && $request->input('project') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('project'));
            });
        }

        if ($request->has('prospect') && $request->input('prospect') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('prospect_id', '=', $request->input('prospect'));
            });
        }

        if ($request->has('vendor') && $request->input('vendor') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('vendor_id', '=', $request->input('vendor'));
            });
        }

        if ($request->has('status') && $request->input('status') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('status_id', '=', $request->input('status'));
            });
        }

        if ($request->has('ru') && $request->input('ru') != 0) {
            $actions->where(function ($query) {
                // Todo: in users
                // $query->where('status_id', '=', $request->input('ru'));
            });
        }

        if ($request->has('rp') && $request->input('rp') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('rp'));
            });
        }

        if ($request->has('rt') && $request->input('rt') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_task_id', '=', $request->input('rt'));
            });
        }

        if ($request->has('rp') && $request->input('rp') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('rp'));
            });
        }

        if ($request->has('rc') && $request->input('rc') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_id', '=', $request->input('rc'));
            });
        }

        if ($request->has('rci') && $request->input('rci') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_invoice_id', '=', $request->input('rci'));
            });
        }

        if ($request->has('rvi') && $request->input('rvi') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_vendor_invoice_id', '=', $request->input('rvi'));
            });
        }

        if ($request->has('rjs') && $request->input('rjs') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_job_spec_id', '=', $request->input('rjs'));
            });
        }

        if ($request->has('sprint') && $request->input('sprint') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('sprint_id', '=', $request->input('sprint'));
            });
        }

        if ($request->has('board') && $request->input('board') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('board_id', '=', $request->input('board'));
            });
        }

        $actions = $actions->paginate($item);

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        $action_status_filter = ActionStatus::orderBy('name')->pluck('name', 'id')->prepend('Status', 0);
        $customers_filter = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Customer', 0);
        $vendors_filter = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Vendor', 0);
        $teams_filter = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Team', 0);
        $projects_filter = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Project', 0);
        $resources_filter = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Resource', 0);
        $prospect_filter = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Prospect', 0);
        $action_statuses = ActionStatus::orderBy('id')->get();

        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Related Task', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Related Customer', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Related Customer Invoice', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Related Vendor Invoice', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Related Job Spec', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Related Project', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('User', 0);
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Board', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', 0);

        $parameters = [
            'actions' => $actions,
            'tasks_drop_down' => $tasks_drop_down,
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_status_filter' => $action_status_filter,
            'customers_filter' => $customers_filter,
            'vendors_filter' => $vendors_filter,
            'teams_filter' => $teams_filter,
            'projects_filter' => $projects_filter,
            'prospect_filter' => $prospect_filter,
            'resources_filter' => $resources_filter,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        if ($request->has('export')) return $this->export($parameters, 'action');

        return view('action.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Action::class)->first();

        if (! Auth::user()->canAccess($module->id, 'create_all') || ! Auth::user()->canAccess($module->id, 'create_team')) {
            return abort(403);
        }

        $roles_drop_down = Role::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Please select...', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please select...', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Please select...', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Please select...', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Please select...', 0);
        $teams_drop_down = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Please select...', 0);
        $action_status_drop_down = ActionStatus::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please select...', 0);
        $rejection_reason_drop_down = RejectionReason::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please select...', 0);
        $vendors_drop_down = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please select...', 0);
        $prospectDropDown = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Please select...', 0);
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $actions_drop_down = Action::orderBy('subject')->pluck('subject', 'id')->prepend('Please select...', 0);
        $task_status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $user_stories = UserStory::orderby('name')->pluck('name', 'id');

        $parameters = [
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'actions_drop_down' => $actions_drop_down,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'roles_drop_down' => $roles_drop_down,
            'prospectDropDown' => $prospectDropDown,
            'teams_drop_down' => $teams_drop_down,
            'vendors_drop_down' => $vendors_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_status_drop_down' => $action_status_drop_down,
            'tasks_drop_down' => $tasks_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'rejection_reason_drop_down' => $rejection_reason_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'user_stories_drop_down' => $user_stories,
        ];

        return view('action.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActionRequest $request, StorageQuota $quota)
    {
        $action = new Action();
        $action->subject = $request->input('subject');
        $action->description = $request->input('description');
        $action->detail = $request->input('detail');
        $action->due_date = $request->input('due_date');
        $action->prospect_id = $request->input('prospect_id');
        $action->updated_user_id = auth()->id();
        $action->assigned_user_id = $request->input('assigned_user_id');
        $related_users = '';
        $related_user_counter = 0;

        if ($request->input('related_users') != null) {
            foreach ($request->input('related_users') as $related_user) {
                if ($related_user_counter == 0) {
                    $related_users = $related_user;
                } else {
                    $related_users .= '|'.$related_user;
                }
                $related_user_counter++;
            }
        }

        $action->related_users = $related_users;
        $action->acceptance_date = $request->input('acceptance_date');
        $action->completed_date = $request->input('completed_date');
        $action->rejection_reason_id = $request->input('rejection_reason_id');
        $action->reject_timestamp = $request->input('reject_timestamp');
        $action->original_user_id = $request->input('original_user_id');
        $action->re_assign_date = $request->input('re_assign_date');
        $action->related_project_id = $request->input('related_project_id');
        $action->related_task_id = $request->input('related_task_id');
        $action->related_customer_id = $request->input('related_customer_id');
        $action->related_job_spec_id = $request->input('related_job_spec_id');
        $action->related_customer_invoice_id = $request->input('related_customer_invoice_id');
        $action->related_vendor_invoice_id = $request->input('related_vendor_invoice_id');
        $action->priority_id = $request->input('priority_id');
        $action->document_name = $request->input('document_name');
        $action->document_url = $request->input('document_url');
        $action->link = $request->input('link');
        $action->send_notification_assigned = $request->input('send_notification_assigned');
        $action->send_notification_created = $request->input('send_notification_created');
        $action->send_notification_related = $request->input('send_notification_related');
        $action->creator_id = auth()->id();
        $action->created_at = now();
        $action->status_id = $request->input('status_id');
        $action->team_id = $request->input('team_id');
        $action->vendor_id = $request->input('vendor_id');
        $action->estimated_effort = $request->input('estimated_effort');
        $action->actual_hours = $request->input('actual_hours');
        $action->dependency_id = $request->input('dependency_id');
        $action->board_id = $request->input('board_id');
        $action->sprint_id = $request->input('sprint_id');
        $action->save();

        if ($request->hasfile('file')) {
            foreach ($request->file('file') as $file) {
                if ($quota->isStorageQuotaReached($file->getSize())){
                    $filename = $file->getClientOriginalName();
                    $file->move(public_path().'/files/actions/', $filename);

                    $document = new Document();
                    $document->type_id = 9;
                    $document->name = $filename;
                    $document->file = '/files/actions/'.$filename;
                    $document->document_type_id = 9;
                    $document->reference = 'Actions';
                    $document->reference_id = $action->id;
                    $document->creator_id = auth()->id();
                    $document->owner_id = $action->assigned_user_id;
                    $document->status_id = 1;
                    $document->digisign_status_id = null;
                    $document->digisign_approver_user_id = null;
                    $document->save();
                }
            }
        }

        $notification = new Notification;
        $notification->name = 'Action created';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->assignedUser->email;
            $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
            $subject = 'Action created #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->createdUser->email;
            $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
            $subject = 'Action created #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action created #'.$action->id;

                try {
                    SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Created');

        return redirect(route('action.index'))->with('flash_success', 'Action created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Action $action): View
    {
        $roles_drop_down = Role::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Please select...', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please select...', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Please select...', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Please select...', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Please select...', 0);
        $teams_drop_down = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Please select...', 0);
        $action_status_drop_down = ActionStatus::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please select...', 0);
        $rejection_reason_drop_down = RejectionReason::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please select...', 0);
        $vendors_drop_down = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please select...', 0);
        $prospectDropDown = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Please select...', 0);

        $actions_flag = false;
        $actions_complete_flag = false;
        if ($action->assigned_user_id == Auth::user()->id) {
            if (($action->status_id != 2) && ($action->status_id != 3)) {
                $actions_flag = true;
            }

            if (($action->status_id == 2) || ($action->status_id == 3)) {
                $actions_complete_flag = true;
            }
        }

        $documents = Document::where('reference_id', '=', $action->id)->where('document_type_id', '=', 9)->get();
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $actions_drop_down = Action::orderBy('subject')->pluck('subject', 'id')->prepend('Please select...', 0);

        $parameters = [
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'actions_drop_down' => $actions_drop_down,
            'action' => $action,
            'documents' => $documents,
            'prospectDropDown' => $prospectDropDown,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'roles_drop_down' => $roles_drop_down,
            'teams_drop_down' => $teams_drop_down,
            'vendors_drop_down' => $vendors_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_status_drop_down' => $action_status_drop_down,
            'tasks_drop_down' => $tasks_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'rejection_reason_drop_down' => $rejection_reason_drop_down,
            'actions_flag' => $actions_flag,
            'actions_complete_flag' => $actions_complete_flag,
        ];

        return view('action.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        $module = Module::where('name', '=', \App\Models\Action::class)->first();

        if (! Auth::user()->canAccess($module->id, 'update_all') || ! Auth::user()->canAccess($module->id, 'update_team')) {
            return abort(403);
        }

        $roles_drop_down = Role::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Please select...', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please select...', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Please select...', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Please select...', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Please select...', 0);
        $teams_drop_down = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Please select...', 0);
        $action_status_drop_down = ActionStatus::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please select...', 0);
        $rejection_reason_drop_down = RejectionReason::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please select...', 0);
        $vendors_drop_down = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please select...', 0);

        $actions_flag = false;
        $actions_complete_flag = false;
        if ($action->assigned_user_id == Auth::user()->id) {
            if (($action->status_id != 2) && ($action->status_id != 3)) {
                $actions_flag = true;
            }

            if (($action->status_id == 2) || ($action->status_id == 3)) {
                $actions_complete_flag = true;
            }
        }

        $documents = Document::where('reference_id', '=', $action->id)->where('document_type_id', '=', 9)->get();
        $prospectDropDown = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Please select...', 0);
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);
        $actions_drop_down = Action::orderBy('subject')->pluck('subject', 'id')->prepend('Please select...', 0);

        $parameters = [
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'actions_drop_down' => $actions_drop_down,
            'action' => $action,
            'documents' => $documents,
            'prospectDropDown' => $prospectDropDown,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'roles_drop_down' => $roles_drop_down,
            'teams_drop_down' => $teams_drop_down,
            'vendors_drop_down' => $vendors_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_status_drop_down' => $action_status_drop_down,
            'tasks_drop_down' => $tasks_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'rejection_reason_drop_down' => $rejection_reason_drop_down,
            'actions_flag' => $actions_flag,
            'actions_complete_flag' => $actions_complete_flag,
        ];

        return view('action.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(ActionRequest $request, $id)
    {
        $action = Action::find($id);
        $action->subject = $request->input('subject');
        $action->description = $request->input('description');
        $action->detail = $request->input('detail');
        $action->due_date = $request->input('due_date');
        $action->prospect_id = $request->input('prospect_id');
        $action->updated_user_id = auth()->id();
        $action->assigned_user_id = $request->input('assigned_user_id');
        $related_users = '';
        $related_user_counter = 0;

        if ($request->input('related_users') != null) {
            foreach ($request->input('related_users') as $related_user) {
                if ($related_user_counter == 0) {
                    $related_users = $related_user;
                } else {
                    $related_users .= '|'.$related_user;
                }
                $related_user_counter++;
            }
        }

        $action->related_users = $related_users;
        $action->acceptance_date = $request->input('acceptance_date');
        $action->completed_date = $request->input('completed_date');
        $action->rejection_reason_id = $request->input('rejection_reason_id');
        $action->reject_timestamp = $request->input('reject_timestamp');
        $action->original_user_id = $request->input('original_user_id');
        $action->re_assign_date = $request->input('re_assign_date');
        $action->related_project_id = $request->input('related_project_id');
        $action->related_task_id = $request->input('related_task_id');
        $action->related_customer_id = $request->input('related_customer_id');
        $action->related_job_spec_id = $request->input('related_job_spec_id');
        $action->related_customer_invoice_id = $request->input('related_customer_invoice_id');
        $action->related_vendor_invoice_id = $request->input('related_vendor_invoice_id');
        $action->priority_id = $request->input('priority_id');
        $action->document_name = $request->input('document_name');
        $action->document_url = $request->input('document_url');
        $action->link = $request->input('link');
        $action->send_notification_assigned = $request->input('send_notification_assigned');
        $action->send_notification_created = $request->input('send_notification_created');
        $action->send_notification_related = $request->input('send_notification_related');
        $action->creator_id = auth()->id();
        $action->created_at = now();
        $action->status_id = $request->input('status_id');
        $action->team_id = $request->input('team_id');
        $action->vendor_id = $request->input('vendor_id');
        $action->estimated_effort = $request->input('estimated_effort');
        $action->actual_hours = $request->input('actual_hours');
        $action->dependency_id = $request->input('dependency_id');
        $action->board_id = $request->input('board_id');
        $action->sprint_id = $request->input('sprint_id');
        $action->save();

        if ($request->hasfile('file')) {
            foreach ($request->file('file') as $file) {
                $filename = $file->getClientOriginalName();
                $file->move(public_path().'/files/actions/', $filename);

                $document = new Document();
                $document->type_id = 9;
                $document->name = $filename;
                $document->file = '/files/actions/'.$filename;
                $document->document_type_id = 9;
                $document->reference = 'Actions';
                $document->reference_id = $action->id;
                $document->creator_id = auth()->id();
                $document->owner_id = $action->assigned_user_id;
                $document->status_id = 1;
                $document->digisign_status_id = null;
                $document->digisign_approver_user_id = null;
                $document->save();
            }
        }

        $notification = new Notification;
        $notification->name = 'Action created';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->assignedUser->email;
            $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
            $subject = 'Action created #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->createdUser->email;
            $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
            $subject = 'Action created #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action created #'.$action->id;

                try {
                    SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Created');

        return redirect(route('action.index'))->with('flash_success', 'Action updated successfully.');
    }

    public function updateAction($action_id, Request $actionRequest)
    {
        $request = (object) $actionRequest->input('request');
        // return json_encode($request);
        $action = Action::find($action_id);
        $action->subject = $request->subject;
        $action->description = $request->description;
        $action->detail = $request->detail;
        $action->due_date = $request->due_date;
        $action->prospect_id = $request->prospect_id;
        $action->updated_user_id = auth()->id();
        $action->assigned_user_id = $request->assigned_user_id;
        $related_users = '';
        $related_user_counter = 0;

        if ($request->related_users_array != null) {
            foreach ($request->related_users_array as $related_user) {
                if ($related_user_counter == 0) {
                    $related_users = $related_user;
                } else {
                    $related_users .= '|'.$related_user;
                }
                $related_user_counter++;
            }
        }

        // $action->related_users = $related_users;
        $action->acceptance_date = $request->acceptance_date;
        $action->completed_date = $request->completed_date;
        $action->rejection_reason_id = $request->rejection_reason_id;
        $action->reject_timestamp = $request->reject_timestamp;
        $action->original_user_id = $request->original_user_id;
        $action->re_assign_date = $request->re_assign_date;
        $action->related_project_id = $request->related_project_id;
        $action->related_task_id = $request->related_task_id;
        $action->related_customer_id = $request->related_customer_id;
        $action->related_job_spec_id = $request->related_job_spec_id;
        $action->related_customer_invoice_id = $request->related_customer_invoice_id;
        $action->related_vendor_invoice_id = $request->related_vendor_invoice_id;
        $action->priority_id = $request->priority_id;
        $action->document_name = $request->document_name;
        $action->document_url = $request->document_url;
        $action->link = $request->link;
        $action->send_notification_assigned = $request->send_notification_assigned;
        $action->send_notification_created = $request->send_notification_created;
        $action->send_notification_related = $request->send_notification_related;
        $action->creator_id = auth()->id();
        $action->updated_at = now();
        $action->status_id = $request->status_id;
        $action->team_id = $request->team_id;
        $action->vendor_id = $request->vendor_id;
        $action->estimated_effort = $request->estimated_effort;
        $action->actual_hours = $request->actual_hours;
        $action->dependency_id = $request->dependency_id;
        $action->board_id = $request->board_id;
        $action->sprint_id = $request->sprint_id;
        $action->save();

        /*if($request->hasfile('file')){
            foreach($request->file('file') as $file){
                $filename = $file->getClientOriginalName();
                $file->move(public_path().'/files/actions/', $filename);

                $document = new Document();
                $document->type_id = 9;
                $document->name = $filename;
                $document->file = '/files/actions/'.$filename;
                $document->document_type_id = 9;
                $document->reference = 'Actions';
                $document->reference_id = $action->id;
                $document->creator_id = auth()->id();
                $document->owner_id = $action->assigned_user_id;
                $document->status_id = 1;
                $document->digisign_status_id = null;
                $document->digisign_approver_user_id = null;
                $document->save();
            }
        }*/

        $notification = new Notification;
        $notification->name = 'Action created';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            if (isset($action->assignedUser->email)) {
                $user_email = $action->assignedUser->email;
                $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
                $subject = 'Action created #'.$action->id;

                try {
                    SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            if (isset($action->createdUser->email)) {
                $user_email = $action->createdUser->email;
                $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
                $subject = 'Action created #'.$action->id;

                try {
                    SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action created #'.$action->id;

                try {
                    SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Updated');

        return ['message', 'Action updated successfully.', 'action' => $request];
    }

    public function accept($id)
    {
        $action = Action::find($id);
        $action->status_id = 2;
        $action->acceptance_date = now();
        $action->save();

        $notification = new Notification;
        $notification->name = 'Action accepted';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->assignedUser->email;
            $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
            $subject = 'Action accepted #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->createdUser->email;
            $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
            $subject = 'Action accepted #'.$action->id;

            try {
                SendActionEmailJob::dispatch($user_email, $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action accepted #'.$action->id;

                try {
                    SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Accepted');

        return redirect(route('action.index'))->with('flash_success', 'Action accepted successfully.');
    }

    public function complete($id)
    {
        $action = Action::find($id);
        $action->status_id = 6;
        $action->acceptance_date = now();
        $action->save();

        $notification = new Notification;
        $notification->name = 'Action completed';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->assignedUser->email;
            $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
            $subject = 'Action completed #'.$action->id;

            try {
                SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->createdUser->email;
            $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
            $subject = 'Action completed #'.$action->id;

            try {
                SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action completed #'.$action->id;

                try {
                    SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Completed');

        return redirect(route('action.index'))->with('flash_success', 'Action completed successfully.');
    }

    public function reject($id, Request $request)
    {
        $action = Action::find($id);
        $action->status_id = 3;
        $action->rejection_reason_id = $request->input('reject_reason_id');
        $action->reject_timestamp = now();
        $action->save();

        $notification = new Notification;
        $notification->name = 'Action rejected';
        $notification->link = route('action.show', $action->id);
        $notification->save();

        if ($action->send_notification_assigned == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->assignedUser->email;
            $full_name = $action->assignedUser->first_name.' '.$action->assignedUser->last_name;
            $subject = 'Action rejected #'.$action->id;

            try {
                SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_created == 1) {
            $user_notification = [
                'user_id' => $action->assigned_user_id,
                'notification_id' => $notification->id,
                'created_at' => now(),
            ];

            UserNotification::insert($user_notification);

            $user_email = $action->createdUser->email;
            $full_name = $action->createdUser->first_name.' '.$action->createdUser->last_name;
            $subject = 'Action rejected #'.$action->id;

            try {
                SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
            } catch (\Exception $e) {
                report($e);

                return response()->json(['message' => 'Error sending notification email to user '.$e]);
            }
        }

        if ($action->send_notification_related == 1) {
            $related_user_ids = explode('|', $action->related_users);
            foreach ($related_user_ids as $related_user_id) {
                $user_notification = [
                    'user_id' => $related_user_id,
                    'notification_id' => $notification->id,
                    'created_at' => now(),
                ];

                UserNotification::insert($user_notification);

                $relatedUser = User::find($related_user_id);

                $user_email = $relatedUser->email;
                $full_name = $relatedUser->first_name.' '.$relatedUser->last_name;
                $subject = 'Action rejected #'.$action->id;

                try {
                    SendActionEmailJob::dispatch(trim($user_email), $subject, $full_name, $action)->delay(now()->addMinutes(5));
                } catch (\Exception $e) {
                    report($e);

                    return response()->json(['message' => 'Error sending notification email to user '.$e]);
                }
            }
        }

        activity()->on($action)
            ->withProperties(
                $action->toArray()
            )
            ->log('Rejected');

        return redirect(route('action.index'))->with('flash_success', 'Action rejected successfully.');
    }

    public function kanban(Request $request)
    {
        $status_filter = array_filter($request->status ?? []);

        if ($request->has('status') && ! empty($status_filter)) {
            $action_status_ids = ActionStatus::get()->map(function ($status) use ($status_filter) {
                if (! in_array($status->id, $status_filter)) {
                    return $status->id;
                }
            })->filter()->values()->toArray();
        } else {
            $action_status_ids = [2, 3, 5, 7];
        }
        $actions = Action::whereNotIn('status_id', $action_status_ids)->orderBy('id', 'desc');

        $module = Module::where('name', '=', \App\Models\Action::class)->first();

        $team_ids = [];
        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            $team_ids = $actions->pluck('id')->toArray();
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $team_ids = $actions->whereIn('assigned_user_id', Auth::user()->team())->pluck('id')->toArray();
            }
        } else {
            return abort(403);
        }

        $actions = $actions->get();

        $user_ids = [];
        foreach ($actions as $action) {
            $is_in = false;
            if ($action->assigned_user_id == Auth::user()->id) {
                $user_ids[] = $action->id;
                $is_in = true;
            }

            $related_user_ids = explode('|', $action->related_users);
            if (in_array(Auth::user()->id, $related_user_ids) && ($is_in == false)) {
                $user_ids[] = $action->id;
            }
        }

        $team_and_related_users_ids = array_merge($team_ids, $user_ids);

        $actions = Action::with(['assignedUser'])->orderBy('id')->whereIn('id', $team_and_related_users_ids);

        if ($request->has('date_from') && $request->input('date_from') != '') {
            $actions->where(function ($query) use ($request) {
                $query->where('created_at', '>=', $request->input('date_from'));
            });
        }

        if ($request->has('date_to') && $request->input('date_to') != '') {
            $actions->where(function ($query) use ($request) {
                $query->where('created_at', '<=', $request->input('date_to'));
            });
        }

        if ($request->has('resource') && $request->input('resource') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('assigned_user_id', '=', $request->input('resource'));
            });
        }

        if ($request->has('team') && $request->input('team') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('team_id', '=', $request->input('team'));
            });
        }

        if ($request->has('customer') && $request->input('customer') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_id', '=', $request->input('customer'));
            });
        }

        if ($request->has('project') && $request->input('project') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('project'));
            });
        }

        if ($request->has('prospect') && $request->input('prospect') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('prospect_id', '=', $request->input('prospect'));
            });
        }

        if ($request->has('vendor') && $request->input('vendor') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('vendor_id', '=', $request->input('vendor'));
            });
        }

        if ($request->has('ru') && $request->input('ru') != 0) {
            $actions->where(function ($query) {
                // Todo: in users
                // $query->where('status_id', '=', $request->input('ru'));
            });
        }

        if ($request->has('rp') && $request->input('rp') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('rp'));
            });
        }

        if ($request->has('rt') && $request->input('rt') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_task_id', '=', $request->input('rt'));
            });
        }

        if ($request->has('rp') && $request->input('rp') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_project_id', '=', $request->input('rp'));
            });
        }

        if ($request->has('rc') && $request->input('rc') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_id', '=', $request->input('rc'));
            });
        }

        if ($request->has('rci') && $request->input('rci') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_customer_invoice_id', '=', $request->input('rci'));
            });
        }

        if ($request->has('rvi') && $request->input('rvi') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_vendor_invoice_id', '=', $request->input('rvi'));
            });
        }

        if ($request->has('rjs') && $request->input('rjs') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('related_job_spec_id', '=', $request->input('rjs'));
            });
        }

        if ($request->has('sprint') && $request->input('sprint') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('sprint_id', '=', $request->input('sprint'));
            });
        }

        if ($request->has('board') && $request->input('board') != 0) {
            $actions->where(function ($query) use ($request) {
                $query->where('board_id', '=', $request->input('board'));
            });
        }

        $actions = $actions->get();

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $action_status_filter = ActionStatus::orderBy('name')->pluck('name', 'id')->prepend('Please Select Status', 0);
        $customers_filter = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please Select Customer', 0);
        $vendors_filter = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please Select Vendor', 0);
        $teams_filter = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Please Select Team', 0);
        $projects_filter = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please Select Project', 0);
        $resources_filter = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please Select Resource', 0);
        $prospect_filter = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Please Select Prospect', 0);
        $action_statuses = ActionStatus::whereNotIn('id', $action_status_ids)->orderBy('id')->get();

        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Please Select Related Task', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please Select Related Customer', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Please Select Related Customer Invoice', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Please Select Related Vendor Invoice', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Please Select Related Job Spec', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please Select Related Project', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please Select User', 0);
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Please Select Board', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please Select Sprint', 0);

        $roles_drop_down = Role::orderBy('name')->pluck('name', 'id')->prepend('Please Select Role', 0);
        $tasks_drop_down = Task::orderBy('description')->pluck('description', 'id')->prepend('Please Select Task', 0);
        $customers_drop_down = Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please Select Customer', 0);
        $customer_invoices_drop_down = CustomerInvoice::orderBy('inv_ref')->pluck('inv_ref', 'id')->prepend('Please Select Customer Invoice', 0);
        $vendor_invoices_drop_down = VendorInvoice::orderBy('vendor_invoice_ref')->pluck('vendor_invoice_ref', 'id')->prepend('Please Select Vendor Invoice', 0);
        $job_specs_drop_down = JobSpec::orderBy('reference_number')->pluck('reference_number', 'id')->prepend('Please Select Job Spec', 0);
        $teams_drop_down = Team::orderBy('team_name')->pluck('team_name', 'id')->prepend('Please select...', 0);
        $action_status_drop_down = ActionStatus::whereNotIn('id', $action_status_ids)->orderBy('name')->pluck('name', 'id')->prepend('Please Select Action Status', 0);
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5])->prepend('Please Select Project', 0);
        $rejection_reason_drop_down = RejectionReason::orderBy('name')->pluck('name', 'id')->prepend('Please Select Rejection Reason', 0);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')->prepend('Please Select User', 0);
        $vendors_drop_down = Vendor::orderBy('vendor_name')->pluck('vendor_name', 'id')->prepend('Please select Vendor', 0);
        $prospectDropDown = Prospect::orderBy('prospect_name')->where('status_id', '=', 1)->pluck('prospect_name', 'id')->prepend('Please Select Prospect', 0);
        $boards_drop_down = Board::orderBy('name')->pluck('name', 'id')->prepend('Please Select Board', 0);
        $sprints_drop_down = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please Select Sprint', 0);
        $actions_drop_down = Action::orderBy('subject')->pluck('subject', 'id')->prepend('Please Select Action', 0);

        $cards = null;
        foreach ($action_statuses as $key => $action_status) {
            $cards[$key]['status'] = $action_status;
            foreach ($actions as $action) {
                $action->related_users_array = trim($action->related_users) != '' ? explode('|', $action->related_users) : [];
                $tempRelatedUsers = [];
                foreach ($action->related_users_array as $related_user) {
                    $tempRelatedUsers[] = (int) $related_user;
                }
                $action->related_users_array = $tempRelatedUsers;
                if ($action->status_id == $action_status->id) {
                    $cards[$key]['actions'][] = $action;
                }
            }
        }

        $vueParameters = [
            'cards' => $cards,
            'boards_drop_down' => Board::orderBy('name')->get(),
            'sprints_drop_down' => Sprint::orderBy('name')->get(),
            'actions_drop_down' => Action::orderBy('subject')->get(),
            'statuses_drop_down' => ActionStatus::orderBy('name')->get(),
            'prospect_drop_down' => Prospect::orderBy('prospect_name')->get(),
            'teams_drop_down' => Team::orderBy('team_name')->get(),
            'vendors_drop_down' => Vendor::orderBy('vendor_name')->get(),
            'projects_drop_down' => Project::orderBy('name')->get(),
            'tasks_drop_down' => Task::orderBy('description')->get(),
            'customers_drop_down' => Customer::orderBy('customer_name')->get(),
            'customer_invoices_drop_down' => CustomerInvoice::where('inv_ref', '!=', '')->orderBy('inv_ref')->get(),
            'vendor_invoices_drop_down' => VendorInvoice::where('vendor_invoice_ref', '!=', '')->orderBy('vendor_invoice_ref')->get(),
            'job_specs_drop_down' => JobSpec::orderBy('reference_number')->get(),
            'rejection_reason_drop_down' => RejectionReason::orderBy('name')->get(),
            'users_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->get(),
        ];
        //return $vueParameters["cards"];

        $parameters = [
            'vueParameters' => $vueParameters,
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'actions_drop_down' => $actions_drop_down,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'roles_drop_down' => $roles_drop_down,
            'prospectDropDown' => $prospectDropDown,
            'teams_drop_down' => $teams_drop_down,
            'vendors_drop_down' => $vendors_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_status_drop_down' => $action_status_drop_down,
            'tasks_drop_down' => $tasks_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'rejection_reason_drop_down' => $rejection_reason_drop_down,
            'actions' => $actions,
            'tasks_drop_down' => $tasks_drop_down,
            'boards_drop_down' => $boards_drop_down,
            'sprints_drop_down' => $sprints_drop_down,
            'customers_drop_down' => $customers_drop_down,
            'customer_invoices_drop_down' => $customer_invoices_drop_down,
            'vendor_invoices_drop_down' => $vendor_invoices_drop_down,
            'job_specs_drop_down' => $job_specs_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
            'action_statuses' => $action_statuses,
            'prospect_filter' => $prospect_filter,
            'action_status_filter' => $action_status_filter,
            'customers_filter' => $customers_filter,
            'vendors_filter' => $vendors_filter,
            'teams_filter' => $teams_filter,
            'projects_filter' => $projects_filter,
            'resources_filter' => $resources_filter,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        return view('action.kanban')->with($parameters);
    }

    public function addMessage($action_id, Request $request)
    {
        $actionNote = new ActionNote();
        $actionNote->action_id = $action_id;
        $actionNote->note = $request->input('message');
        $actionNote->creator_id = auth()->id();
        $actionNote->status_id = 1;
        $actionNote->save();

        return ['message' => 'Action note successfully added.'];
    }

    public function getMessages($action_id)
    {
        $actionNotes = ActionNote::with(['user'])->where('action_id', '=', $action_id)->where('status_id', '=', 1)->orderBy('id', 'desc')->get();

        return $actionNotes;
    }

    public function getAction($action_id)
    {
        $action = Action::find($action_id);
        $action->related_users_array = trim($action->related_users) != '' ? explode('|', $action->related_users) : [];
        $tempRelatedUsers = [];
        foreach ($action->related_users_array as $related_user) {
            $tempRelatedUsers[] = (int) $related_user;
        }
        $action->related_users_array = $tempRelatedUsers;

        return $action;
    }

    public function getCards()
    {
        $actions = Action::with(['assignedUser'])->orderBy('id')->get();

        $action_statuses = ActionStatus::whereNotIn('id', [2, 3, 5, 7])->orderBy('id')->get();

        $cards = null;
        foreach ($action_statuses as $key => $action_status) {
            $cards[$key]['status'] = $action_status;
            foreach ($actions as $action) {
                $action->related_users_array = trim($action->related_users) != '' ? explode('|', $action->related_users) : [];
                $tempRelatedUsers = [];
                foreach ($action->related_users_array as $related_user) {
                    $tempRelatedUsers[] = (int) $related_user;
                }
                $action->related_users_array = $tempRelatedUsers;
                if ($action->status_id == $action_status->id) {
                    $cards[$key]['actions'][] = $action;
                }
            }
        }

        return $cards;
    }

    public function move($action_id, Request $request): JsonResponse
    {
        $action = Action::find($action_id);

        $before = $action;

        $action->status_id = $request->input('status');
        $action->save();

        return response()->json(['message' => 'Action updated successfully', 'New Status' => $request->input('status'), 'Before' => $before, 'After' => $action]);
    }

    public function getRelatedTasks(Project $project)
    {
        $tasks = $project->tasks()->get(['id', 'description'])->map(function ($task) {
            return [
                'id' => $task->id,
                'name' => $task->description,
            ];
        });

        return response()->json(['tasks' => $tasks]);
    }

    public function storeActionTask(Request $request): JsonResponse
    {
        $task = new Task();
        $task->employee_id = $request->taskFields['consultant'];
        $task->customer_id = $request->taskFields['customer'];
        $task->dependency = $request->taskFields['dependance'];
        $task->description = $request->taskFields['description'];
        $task->start_date = $request->taskFields['start_date'];
        $task->end_date = $request->taskFields['end_date'];
        $task->hours_planned = $request->taskFields['hours'];
        $task->billable = $request->taskFields['is_billable'];
        $task->note_1 = $request->taskFields['note'];
        $task->project_id = $request->taskFields['project'];
        $task->sprint_id = $request->taskFields['task_sprint'];
        $task->status = $request->taskFields['task_status'];
        $task->user_story_id = $request->taskFields['user_story'];
        $task->weight = $request->taskFields['weight'];
        $task->save();

        return response()->json(['task' => $task]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        //
    }

    public function emoteFrown($action_id): JsonResponse
    {
        $action = Action::find($action_id);

        $action->emote_id = 1;
        $action->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }

    public function emoteExclamation($action_id): JsonResponse
    {
        $action = Action::find($action_id);

        $action->emote_id = 2;
        $action->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }

    public function emoteSmile($action_id): JsonResponse
    {
        $action = Action::find($action_id);

        $action->emote_id = 3;
        $action->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }

    public function emoteNone($action_id): JsonResponse
    {
        $action = Action::find($action_id);

        $action->emote_id = 0;
        $action->save();

        return response()->json(['success' => 'Emote successfully removed.']);
    }

    public function uploadDocument($action_id, Request $request): JsonResponse
    {
        $file = 'action_'.date('Y_m_d_H_i_s').'.'.$request->file->getClientOriginalExtension();
        $fileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('files/actions'), $file);

        $document = new Document();
        $document->name = $fileName;
        $document->type_id = 9;
        $document->file = $file;
        $document->document_type_id = 9;
        $document->reference = 'Action';
        $document->reference_id = $action_id;
        $document->owner_id = auth()->user()->id;
        $document->creator_id = auth()->user()->id;
        $document->status_id = 1;
        $document->save();

        return response()->json(['success' => 'File successfully uploaded.']);
    }

    public function deleteDocument($document_id): JsonResponse
    {
        Document::destroy($document_id);

        return response()->json(['success' => 'File successfully deleted.']);
    }

    public function getDocuments($action_id)
    {
        $documents = Document::where('reference_id', $action_id)->where('reference', 'Action')->get();

        return $documents;
    }

    public function downloadDocument($document_id): \Illuminate\Http\Response
    {
        $document = Document::where('id', $document_id)->first();

        $file = public_path()."/files/actions/$document->file";

        return Response::download($file, $document->name, $headers);
    }

    public function setEmote($action_id, Request $request): JsonResponse
    {
        $action = Action::find($action_id);

        $action->emote_id = $request->emote_id;
        $action->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }
}
