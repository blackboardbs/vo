<?php

namespace App\Http\Controllers;

use App\Models\BBBEELevel;
use App\Models\BBBEERace;
use App\Models\BEEMasterData;
use App\Models\Disability;
use App\Models\Gender;
use App\Http\Requests\StoreBEEMasterDataRequest;
use App\Http\Requests\UpdateBEEMasterDataRequest;
use App\Models\Naturalization;
use App\Models\ProjectType;
use App\Models\Race;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BEEMasterDataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $bee_master = BEEMasterData::with('project_t')
                                    ->with('naturalization')
                                    ->with('race')->with('bbbee_l')
                                    ->with('bbbee_r')
                                    ->with('genderd')
                                    ->with('disabilityd')
                                    ->paginate($item);

        if ($request->has('q') && $request->input('q') != '') {
            $bee_master = BEEMasterData::where('description', 'like', '%'.$request->input('q').'%')
                ->sortable(['description' => 'asc'])
                ->with('project_t')
                ->with('naturalization')
                ->with('race')->with('bbbee_l')
                ->with('bbbee_r')
                ->with('genderd')
                ->with('disabilityd')
                ->paginate($item);
        }

        $parameters = [
            'bee_master' => $bee_master,
        ];
        //return $parameters;
        return View('master_data.bee_master.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'project_t_dropdown' => ProjectType::orderBy('id')->pluck('description', 'id')->prepend('Project Type', '0'),
            'naturalization_dropdown' => Naturalization::orderBy('id')->pluck('description', 'id')->prepend('Naturalization', '0'),
            'race_dropdown' => Race::orderBy('id')->pluck('description', 'id')->prepend('Race', '0'),
            'bbbee_l_dropdown' => BBBEELevel::orderBy('id')->pluck('description', 'id')->prepend('BEE Level', '0'),
            'bbbee_r_dropdown' => BBBEERace::orderBy('id')->pluck('description', 'id')->prepend('BEE Race', '0'),
            'gender_dropdown' => Gender::orderBy('id')->pluck('description', 'id')->prepend('Gender', '0'),
            'disability_dropdown' => Disability::orderBy('id')->pluck('description', 'id')->prepend('Disability', '0'),
        ];

        //return $parameters;

        return View('master_data.bee_master.create')->with($parameters);
    }

    public function store(StoreBEEMasterDataRequest $request): RedirectResponse
    {
        $bee_master = new BEEMasterData();
        $bee_master->project_type = $request->input('project_type');
        $bee_master->naturalization_id = $request->input('naturalization_id');
        $bee_master->race_id = $request->input('race_id');
        $bee_master->bbbee_level = $request->input('bbbee_level');
        $bee_master->bbbee_race = $request->input('bbbee_race');
        $bee_master->gender = $request->input('gender');
        $bee_master->disability = $request->input('disability');
        $bee_master->save();

        return redirect(route('bee_master.index'))->with('flash_success', 'BEE Master Data captured successfully');
    }

    public function show($bee_master_id): View
    {
        $parameters = [
            'bee_master' => BEEMasterData::where('id', '=', $bee_master_id)->get(),
        ];

        return View('master_data.bee_master.show')->with($parameters);
    }

    public function edit($bee_master_id): View
    {
        $parameters = [
            'bee_master' => BEEMasterData::where('id', '=', $bee_master_id)->get(),
            'project_t_dropdown' => ProjectType::orderBy('id')->pluck('description', 'id')->prepend('Project Type', '0'),
            'naturalization_dropdown' => Naturalization::orderBy('id')->pluck('description', 'id')->prepend('Naturalization', '0'),
            'race_dropdown' => Race::orderBy('id')->pluck('description', 'id')->prepend('Race', '0'),
            'bbbee_l_dropdown' => BBBEELevel::orderBy('id')->pluck('description', 'id')->prepend('BEE Level', '0'),
            'bbbee_r_dropdown' => BBBEERace::orderBy('id')->pluck('description', 'id')->prepend('BEE Race', '0'),
            'gender_dropdown' => Gender::orderBy('id')->pluck('description', 'id')->prepend('Gender', '0'),
            'disability_dropdown' => Disability::orderBy('id')->pluck('description', 'id')->prepend('Disability', '0'),
        ];

        return View('master_data.bee_master.edit')->with($parameters);
    }

    public function update(UpdateBEEMasterDataRequest $request, $invoice_status_id): RedirectResponse
    {
        $bee_master = BEEMasterData::find($invoice_status_id);
        $bee_master->project_type = $request->input('project_type');
        $bee_master->naturalization_id = $request->input('naturalization_id');
        $bee_master->race_id = $request->input('race_id');
        $bee_master->bbbee_level = $request->input('bbbee_level');
        $bee_master->bbbee_race = $request->input('bbbee_race');
        $bee_master->gender = $request->input('gender');
        $bee_master->disability = $request->input('disability');
        $bee_master->save();

        return redirect(route('bee_master.index'))->with('flash_success', 'BEE Master Data updated successfully');
    }

    public function destroy($bee_master_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$bee_master_id]);
        BEEMasterData::destroy($bee_master_id);

        return redirect()->route('bee_master.index')->with('success', 'Master Data BEE deleted successfully');
    }
}
