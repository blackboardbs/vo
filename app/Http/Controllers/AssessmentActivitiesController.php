<?php

namespace App\Http\Controllers;

use App\Models\AssessmentActivities;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentActivitiesController extends Controller
{
    public function edit($activity_id): View
    {
        $assessment_activity = AssessmentActivities::find($activity_id);
        $parameters = [
            'assessment_activity' => $assessment_activity,
            'competency_level_dropdown' => [0 => 'Select Level', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
        ];

        return view('assessment.assessment_activity.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $assessment_activity = AssessmentActivities::find($id);
        $assessment_activity->assessment_task_description = $request->assessment_task_description;
        $assessment_activity->competency_level = $request->competency_level;
        $assessment_activity->comfort_level = $request->comfort_level;
        $assessment_activity->notes = $request->notes;
        $assessment_activity->save();

        return redirect()->route('assessment.show', $assessment_activity->assessment_id)->with('flash_success', 'Assessment Activity Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $assessment_plan = AssessmentActivities::find($id);
        AssessmentActivities::destroy($id);

        return redirect()->route('assessment.show', $assessment_plan->assessment_id)->with('flash_success', 'Assessment Activity Deleted successfully');
    }
}
