<?php

namespace App\Http\Controllers;

use App\Models\Availability;
use App\Models\AvailStatus;
use App\Http\Requests\StoreAvailabilityRequest;
use App\Http\Requests\UpdateAvailabilityRequest;
use App\Models\Cv;
use App\Models\Resource;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AvailabilityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    }

    public function create($cvid, $res_id): View
    {
        $availabilities = Availability::orderBy('status_date', 'desc')->where('res_id', '=', $res_id)->get();

        $parameters = [
            'cv' => $cvid,
            'availabilities' => $availabilities,
            'res' => $res_id,
            'date' => Carbon::now()->toDateString(),
            'availstatus_dropdown' => AvailStatus::orderBy('id')->pluck('description', 'id')->prepend('Availability Status', '0'),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return view('availability.create')->with($parameters);
    }

    public function store(StoreAvailabilityRequest $request, $cvid, $res_id): RedirectResponse
    {
        $availability = new Availability;
        $availability->avail_status = $request->input('avail_status');
        $availability->status_date = $request->input('status_date');
        $availability->avail_note = $request->input('avail_note');
        $availability->avail_date = $request->input('avail_date');
        $availability->rate_hour = $request->input('rate_hour');
        $availability->rate_month = $request->input('rate_month');
        $availability->status_id = $request->input('status_id');
        $availability->creator_id = auth()->id();
        $availability->res_id = $res_id;
        $availability->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Availability captured successfully');
    }

    public function show(Request $request, Availability $availability): View
    {
        $cv_id = $request->input('cvid');
        $res_id = $request->input('res_id');

        $parameters = [
            'availability' => $availability,
            'cv_id' => $request->cv,
            'res_id' => $request->resource,
        ];

        return view('availability.show')->with($parameters);
    }

    public function edit(Availability $availability, Cv $cv, Resource $resource): View
    {
        $parameters = [
            'availability' => $availability,
            'cv' => $cv,
            'availstatus_dropdown' => AvailStatus::orderBy('id')->pluck('description', 'id')->prepend('Availability Status', '0'),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'res' => $resource,
        ];

        return view('availability.edit')->with($parameters);
    }

    public function update(UpdateAvailabilityRequest $request, $availabilityid, $cvid, $res_id): RedirectResponse
    {
        $availability = Availability::find($availabilityid);
        $availability->avail_status = $request->input('avail_status');
        $availability->status_date = $request->input('status_date');
        $availability->avail_note = $request->input('avail_note');
        $availability->avail_date = $request->input('avail_date');
        $availability->rate_hour = $request->input('rate_hour');
        $availability->rate_month = $request->input('rate_month');
        $availability->status_id = $request->input('status_id');
        $availability->creator_id = auth()->id();
        $availability->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Availability updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        DB::table('availability')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Availability::destroy($id);

        return redirect()->route('cv.index')->with('success', 'Availability deleted successfully');
    }
}
