<?php

namespace App\Http\Controllers;

use App\API\Currencies\Currency;
use App\Enum\InvoiceType;
use App\Models\Assignment;
use App\Models\Customer;
use App\Models\Document;
use App\Http\Requests\InvoiceRequest;
use App\Http\Requests\StoreVendorInvoiceRequest;
use App\Models\InvoiceItems;
use App\Jobs\SendVendorInvoiceEmailJob;
use App\Models\Module;
use App\Models\Project;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VatRate;
use App\Models\Config;
use App\Models\VendorInvoice;
use App\Models\VendorInvoiceLine;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;

class VendorInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $all_vendors = VendorInvoice::all();
        if (! $request->input('r')) {
            $item = 15;
        } elseif ($request->input('r') == 0) {
            $item = $all_vendors->count();
        } else {
            $item = $request->input('r');
        }

        $vendor_invoice = VendorInvoice::with('invoice_status:id,description', 'vendor:id,vendor_name');

        $module = Module::where('name', '=', \App\Models\VendorInvoice::class)->get();

        abort_unless(Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
            $vendor = Vendor::find(Auth::user()->vendor_id);
            if (empty($vendor)) {
                return abort(403);
            }

            $assignment_approver = $vendor->assignment_approver;
            $assignment_approver = (int) $assignment_approver;

            if ($assignment_approver == Auth::user()->id) {
                $users_ids = User::where('vendor_id', '=', $vendor->id)->pluck('id')->toArray();
                $vendor_invoice_ids = VendorInvoiceLine::whereIn('emp_id', $users_ids)->pluck('vendor_invoice_number')->toArray();
                $vendor_invoice->whereIn('id', $vendor_invoice_ids);
            } else {
                $vendor_invoice_ids = VendorInvoiceLine::where('emp_id', '=', Auth::user()->id)->pluck('vendor_invoice_number')->toArray();
                $vendor_invoice->whereIn('id', $vendor_invoice_ids);
            }
        }

        $can_create = Auth::user()->canAccess($module[0]->id, 'create_all') || Auth::user()->canAccess($module[0]->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module[0]->id) ? $module[0]->id : -1, 'update_all') || Auth::user()->canAccess(isset($module[0]->id) ? $module[0]->id : -1, 'update_team');

        if ($request->has('q') && $request->input('q') != '') {
            $vendor_invoice = $vendor_invoice->where('vendor_invoice_number', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_ref', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_value', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_due_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_paid_date', 'like', '%'.$request->input('q').'%');
        }

        $vendor_invoice = $vendor_invoice->when($request->filter_from,
            fn($ven_invoice) => $ven_invoice->where('vendor_invoice_date', '>=', $request->filter_from)
        )->when($request->filter_to,
            fn($ven_invoice) => $ven_invoice->where('vendor_invoice_date', '<=', $request->filter_to)
        );

        if ($request->has('invoice_status') && $request->input('invoice_status') > 0) {
            $vendor_invoice = $vendor_invoice->with('invoice_status')->where('vendor_invoice_status', '=', $request->input('invoice_status'));
        }

        if ($request->has('vendor_id') && $request->input('vendor_id') != '') {
            $vendor_invoice = $vendor_invoice->with('invoice_status')->where('vendor_id', '=', $request->input('vendor_id'));
        }

        $vendor_invoice = $vendor_invoice->sortable(['id' => 'desc'])->paginate($item)->appends($request->query());

        $total = [];
        foreach($vendor_invoice as $vi){
            if($vi->currency){
                if(isset($total[$vi->currency])){
                $total[$vi->currency] = $total[$vi->currency] + $vi->vendor_invoice_value;
                } else {
                    $total[$vi->currency] = $vi->vendor_invoice_value;
                }
            } else {
                if(isset($total['ZAR'])){
                $total['ZAR'] = $total['ZAR'] + $vi->vendor_invoice_value;
                } else {
                    $total['ZAR'] = $vi->vendor_invoice_value;
                }
            }
        }

        $parameters = [
            'vendor_invoices' => $vendor_invoice,
            'can_create' => $can_create,
            'can_update' => $can_update,
            'total'=>$total
        ];

        if ($request->has('export')) return $this->export($parameters, 'vendor.vendor_invoice');

        return view('vendor.vendor_invoice.index')->with($parameters);
    }

    public function getAjax(Request $request): JsonResponse
    {
        if ($request->consultant_id != null) {
            $data = Timesheet::select(DB::raw('DISTINCT project_id'))->where('employee_id', '=', $request->consultant_id)->with('project')->get();
        }

        if ($request->wbs_id != null) {
            $data = Timesheet::select('month', 'year')
                ->where('project_id', '=', $request->wbs_id)
                ->orderBy('year', 'desc')
                ->distinct('month')
                ->get();
        }

        if ($request->ajax()) {
            $response = [
                'data' => $data,
                'status' => 'success',
            ];

            return response()->json($response);
        } else {
            return response()->json(['msg' => 'false']);
        }
    }

    public function calculate(StoreVendorInvoiceRequest $request)
    {
        $date = Carbon::now()->toDateString();
        $period = $request->input('period');
        $year = substr($period, 0, 4);
        $month = substr($period, 5, 2);

        $time_exps = Timeline::with('customer')
           ->with('project')
           ->with('time.resource')
           ->with('company')
           ->get();

        $employee = $time_exps->filter(function ($time_exps) use ($request) {
            return $time_exps->time->resource->id == $request->input('consultant');
        })->first();

        $project = $time_exps->filter(function ($time_exps) use ($request) {
            return $time_exps->wbs_id == $request->input('project');
        })->first();

        $customer = $time_exps->filter(function ($time_exps) use ($project) {
            return $time_exps->customer_id == $project->project->customer_id;
        })->first();

        $assignment = Assignment::where('project_id', '=', $project->project->id)
                                ->where('employee_id', '=', $employee->time->resource->id)
                                ->first();
        if ($request->has('period') || $request->has('date_date') && $request->has('date_to')) {
            $invoicea = TimeExp::leftJoin('time', function ($join) {
                $join->on('time_exp.time_id', '=', 'time.id');
            })
                ->where('time.emp_id', '=', $request->input('consultant'))
                ->where('time_exp.wbs_id', '=', $request->input('project'))
                ->where('time_exp.claim', '=', 1)
                ->where(function ($query) use ($year, $month) {
                    if (isset($month) && isset($year)) {
                        $query->where('time.year', '=', $year)
                            ->where('time.month', '=', $month);
                    }
                })
                ->orWhere(function ($query) use ($request) {
                    if ($request->has('date_from') && $request->has('date_to')) {
                        $query->whereBetween('time_exp.date', [$request->input('date_from'), $request->input('date_to')]);
                    }
                })
                ->orderBy('time_exp.date', 'asc')
                ->get();
        } else {
            Session::flash('message', 'Invalid selection');

            return Redirect::back();
        }

        $bhours = ($assignment->billable == 1) ? $assignment->hours : 0;

        $parameters = [
            'data' => $employee,
            'invoice_no' => $request->input('invoice_number'),
            'date' => $date,
            'vendor' => $customer,
            'project' => $project,
            'bhours' => $bhours,
            'assignments' => $assignment,
            'ex_total' => $assignment->external_cost_rate * $bhours,
            'paid' => '',
            'invoiceas' => $invoicea,
            'inv_message' => $period.' '.$request->input('invoice_message'),
        ];

        return view('vendor.vendor_invoice.show')->with($parameters);
    }

    public function storeTimesheetId(Request $request, $id): JsonResponse
    {
        if ($id == 0) {
            $request->session()->forget('vendor_invoice_timesheets');
        } elseif (strpos($id, '.')) {
            $actual_id = explode('.', $id)[0];

            if ($request->session()->has('vendor_invoice_timesheets')) {
                foreach ($request->session()->get('vendor_invoice_timesheets') as $key => $value) {
                    if ($value === $actual_id) {
                        $request->session()->pull('vendor_invoice_timesheets.'.$key);
                        break;
                    }
                }
            }
        } else {
            if ($request->session()->has('vendor_invoice_timesheets')) {
                $timesheets = $request->session()->get('vendor_invoice_timesheets');

                if (in_array($id, $timesheets)) {
                    $posted_timesheet[0] = $id;
                    $timesheets = array_diff($timesheets, $posted_timesheet);
                } else {
                    array_push($timesheets, $id);
                }
                $timesheets = array_values($timesheets);
                $request->session()->put('vendor_invoice_timesheets', $timesheets);
            } else {
                $timesheets = [];
                array_push($timesheets, $id);
                $request->session()->put('vendor_invoice_timesheets', $timesheets);
            }
        }

        return response()->json('success');
    }

    public function createNonTimesheetInvoice(Currency $currency): View
    {
        $date = Carbon::now();
        $customer_drop_down = Customer::where('status', '=', 1)->orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Please select...', 0);
        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Please select...', 0);
        $resource_drop_down = [];
        // $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Please select...', 0);
        $payment_due_date = null;
        $due_date = isset($payment_due_date->customer->payment_terms) ? $payment_due_date->customer->payment_terms : 30;

        $invoiceNumber = 0;
        $invoiceTypeID = 2; //Type 1 for customer invoice, type 2 for vendor invoice

        $parameters = [
            'invoiceNumber' => $invoiceNumber,
            'invoiceTypeID' => $invoiceTypeID,
            'customer_drop_down' => $customer_drop_down,
            'project_drop_down' => $project_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'invoice_date' => $date->now()->toDateString(),
            'due_date' => $date->now()->addDays($due_date)->toDateString(),
            'currency_drop_down' => $currency->currencies()['currencies'] ?? [],
        ];

        return view('customer.invoice.header_non_timesheet_create')->with($parameters);
    }

    public function storeNonTimesheetInvoice(InvoiceRequest $request, Currency $currency): RedirectResponse
    {
        $vendor_invoice = new VendorInvoice();
        $vendor_invoice->vendor_id = $request->input('customer_id');
        $vendor_invoice->vendor_invoice_date = $request->input('invoice_date');
        $vendor_invoice->vendor_due_date = $request->input('due_date');
        $vendor_invoice->company_id = $request->input('company_id');
        $vendor_invoice->vendor_paid_date = null;
        $vendor_invoice->vendor_unallocate_date = null;
        $vendor_invoice->vendor_invoice_value = null;
        $vendor_invoice->vendor_invoice_status = 1;
        $vendor_invoice->vendor_invoice_note = $request->input('invoice_notes');
        $vendor_invoice->vendor_invoice_ref = $request->input('inv_ref');
        $vendor_invoice->vendor_reference = $request->input('cust_inv_ref');
        $vendor_invoice->project_id = $request->input('project_id');
        $vendor_invoice->resource_id = $request->input('resource_id');
        $vendor_invoice->currency = $request->input('currency');
        $vendor_invoice->conversion_rate = $currency->conversionRate($request->input('currency'))['rate']??0;
        if (! isset($this->configTable()->vendor_invoice_start_number)) {
            $vendor_invoice->save();
        }else{
            $vendor_invoice->saveInvoiceNumber();
        }


        $routeParams['invoice_id'] = $vendor_invoice->id;

        if ($request->is_recurring){
            $vendor_invoice->saveRecurringInvoice($request);
            $routeParams['recurring'] = $request->is_recurring;

            if (Carbon::parse($request->first_invoice_date)->toDateString() === now()->toDateString()){
                $invoiceCopy = $vendor_invoice->replicate();
                $invoiceCopy->vendor_invoice_date = now()->toDateString();
                $invoiceCopy->vendor_due_date = now()->addDays($vendor_invoice->recurring->due_days_from_invoice_date)->toDateTimeString();
                $invoiceCopy->save();

                $vendor_invoice->recurring()->update([
                    'last_invoiced_at' => now()->toDateTimeString()
                ]);

                $vendor_invoice->histories()->attach($invoiceCopy->id, ['type' => InvoiceType::Vendor->value]);
            }
        }

        activity()->on($vendor_invoice)->log('created');

        return redirect(route('nontimesheetinvoice.edit_vendor', $routeParams))->with('flash_success', 'Invoice added successfully');
    }

    public function editNonTimesheetInvoice($vendorInvoice_id, Currency $currency): View
    {
        $vendorInvoice = VendorInvoice::find($vendorInvoice_id);
        $invoiceNumber = $vendorInvoice->id;
        $invoiceTypeID = 2;

        // Map this values, so it is the same as the customer invoice
        $vendorInvoiceMapped = [];
        $vendorInvoiceMapped['id'] = $vendorInvoice->id;
        $vendorInvoiceMapped['customer_invoice_number'] = $vendorInvoice->id;
        $vendorInvoiceMapped['inv_ref'] = $vendorInvoice->vendor_invoice_ref;
        $vendorInvoiceMapped['invoice_date'] = $vendorInvoice->vendor_invoice_date;
        $vendorInvoiceMapped['due_date'] = $vendorInvoice->vendor_due_date;
        $vendorInvoiceMapped['paid_date'] = $vendorInvoice->vendor_paid_date;
        $vendorInvoiceMapped['customer_id'] = $vendorInvoice->vendor_id;
        $vendorInvoiceMapped['company_id'] = $vendorInvoice->company_id;
        $vendorInvoiceMapped['customer_unallocate_date'] = null;
        $vendorInvoiceMapped['bill_status'] = $vendorInvoice->vendor_invoice_status;
        $vendorInvoiceMapped['invoice_notes'] = $vendorInvoice->vendor_invoice_note;
        $vendorInvoiceMapped['invoice_value'] = $vendorInvoice->vendor_invoice_value;
        $vendorInvoiceMapped['cust_inv_ref'] = $vendorInvoice->vendor_reference;
        $vendorInvoiceMapped['project_id'] = $vendorInvoice->project_id;
        $vendorInvoiceMapped['resource_id'] = $vendorInvoice->resource_id;
        $vendorInvoiceMapped['currency'] = $vendorInvoice->currency;
        $vendorInvoiceMapped['conversion_rate'] = $vendorInvoice->conversion_rate;
        $vendorInvoiceMapped['status_id'] = $vendorInvoice->vendor_invoice_status;

        if (\request()->recurring){
            $history_count = $vendorInvoice->histories()->count();
            $vendorInvoiceMapped['first_invoice_date'] = $vendorInvoice->recurring?->first_invoice_at;
            $vendorInvoiceMapped['due_days_from_invoice_date'] = $vendorInvoice->recurring?->due_days_from_invoice_date;
            $vendorInvoiceMapped['frequency'] = $vendorInvoice->recurring?->frequency;
            $vendorInvoiceMapped['interval'] = $vendorInvoice->recurring?->interval;
            $vendorInvoiceMapped['occurrences'] = $vendorInvoice->recurring?->occurrences;
            $vendorInvoiceMapped['expiry_date'] = $vendorInvoice->recurring?->expires_at;
            $vendorInvoiceMapped['email_subject'] = $vendorInvoice->recurring?->email_subject;
            $vendorInvoiceMapped['emails'] = explode(',',$vendorInvoice->recurring?->emails);
            $vendorInvoiceMapped['full_name'] = $vendorInvoice->recurring?->full_name;
            $vendorInvoiceMapped['history_count'] = $history_count;
            $vendorInvoiceMapped['remaining_invoices'] = $vendorInvoice->invoicesRemaining($vendorInvoice->recurring, $history_count);
        }

        $parameters = [
            'customerInvoice' => $vendorInvoiceMapped, //Do not change this, it uses the same vue component as customer invoice
            'invoiceNumber' => $invoiceNumber,
            'invoiceTypeID' => $invoiceTypeID,
            'currency_drop_down' => $currency->currencies()['currencies'] ?? [],
            'vat_rates' => VatRate::where('end_date','>',Carbon::parse(now())->format('Y-m-d'))->where('status','1')->orderBy('vat_code')->get(['vat_code','description']),
            'default_vat' => Config::first()
        ];

        return view('customer.invoice.add_lines_non_timesheet')->with($parameters);
    }

    public function updateNonTimesheetInvoice(InvoiceRequest $request, VendorInvoice $invoice): JsonResponse
    {
        $invoice->company_id = $request->company_id;
        $invoice->vendor_id = $request->customer_id;
        $invoice->project_id = $request->project_id;
        $invoice->resource_id = $request->resource_id;
        $invoice->vendor_invoice_date = $request->invoice_date;
        $invoice->vendor_due_date = $request->due_date;
        $invoice->currency = $request->currency;
        $invoice->vendor_invoice_ref = $request->inv_ref;
        $invoice->vendor_reference = $request->cust_inv_ref;
        $invoice->vendor_invoice_note = $request->invoice_notes;
        if ($request->is_recurring){
            $invoice->recurring->first_invoice_at = $request->first_invoice_date;
            $invoice->recurring->due_days_from_invoice_date = $request->due_days_from_invoice_date;
            $invoice->recurring->frequency = $request->frequency;
            $invoice->recurring->interval = $request->interval;
            $invoice->recurring->occurrences = $request->occurrences;
            $invoice->recurring->expires_at = $request->expiry_date;
            $invoice->recurring->email_subject = $request->email_subject;
            $invoice->recurring->emails = implode(',', $request->emails);
            $invoice->recurring->full_name = $request->full_name;
            $invoice->push();
        }else{
            $invoice->save();
        }

        return response()->json(['msg' => 'success']);
    }

    public function generateNonTimesheetInvoice(Request $request, VendorInvoice $invoice): View
    {
        $vendorInvoice = $invoice;
        $vendorInvoiceItems = $vendorInvoice->nonTimesheetVendorInvoices;

        if ($vendorInvoice->recurring && !$vendorInvoice->recurring->scheduled_at){
            if (Carbon::parse($vendorInvoice->recurring->first_invoice_at)->toDateString() === now()->toDateString()){
                $emails = $vendorInvoice->recurring->emails ? explode(",", $vendorInvoice->recurring->emails) : [];
                $request['user_email'] = json_encode($emails);
                $this->sendNonTimesheetInvoice($vendorInvoice, $request);
            }

            $vendorInvoice->recurring()->update([
                'scheduled_at' => now()->toDateString()
            ]);
        }

        $parameters = [
            'vendorInvoice' => $vendorInvoice,
            'vendorInvoiceItems' => $vendorInvoiceItems,
        ];

        return view('vendor.vendor_invoice_function.non_timesheet_show')->with($parameters);
    }

    public function sendNonTimesheetInvoice(VendorInvoice $vendorInvoice, Request $request)
    {
        $vendorInvoiceItems = $vendorInvoice->nonTimesheetVendorInvoices;

        $parameters = [
            'vendorInvoice' => $vendorInvoice,
            'vendorInvoiceItems' => $vendorInvoiceItems,
        ];

        try {
            $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
            $emails = [(isset($request->company_email) ? $request->company_email : null), (isset($request->customer_email) ? $request->customer_email : null)];
            if (isset($multiple_emails)) {
                foreach ($multiple_emails as $user_email) {
                    array_push($emails, $user_email);
                }
            }

            if (isset($this->configTable()->invoice_body_msg)) {
                $body = $this->configTable()->invoice_body_msg;
            } else {
                $body = 'Please find Accounts Receivable invoice attached for your attention';
            }
            $vendor_name = str_replace(' ', '_', $vendorInvoice->vendor->vendor_name);

            $storage = $vendorInvoice->invoiceStoragePath("vendor")?->storage;

            $pdf = Pdf::view('vendor.vendor_invoice_function.non_timesheet_pdf', $parameters)
                ->format('a4');

            if ($request->has('print')){
                return $pdf->name($vendorInvoice->invoiceStoragePath("vendor")?->file_name);
            }else{
                $pdf->save($storage);
            }

            $document = new Document();
            $document->document_type_id = 5;
            $document->name = 'Vendor Tax Invoice';
            $document->file = $vendorInvoice->invoiceStoragePath("vendor")?->file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = $vendorInvoice->resource?->employee_id;
            $document->reference_id = 5;
            $document->save();
            $subject = ($vendorInvoice->vendor->vendor_name ?? '<Vendor Name>').' Accounts Recievable Invoice';

            activity()->on($vendorInvoice)->withProperties(['customer_name' => $vendorInvoice->vendor?->vendor_name])->log('emailed');

            SendVendorInvoiceEmailJob::dispatch(Auth::user()->email, array_filter($emails), $vendorInvoice->vendor, $vendorInvoice->company, $subject, $body, $storage)->delay(now()->addMinutes(5));

            return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice sent successful');
        } catch (\Exception $e) {
            report($e);

            return redirect(route('nontimesheetinvoice.generate_vendor', $vendorInvoice->id))->with('flash_danger', $e->getMessage());
        }
    }

    private function newInvoiceNumberInt($vendor_invoice)
    {
        if (! isset($this->configTable()->vendor_invoice_start_number)) {
            return $vendor_invoice->id;
        }

        $last_invoice = VendorInvoice::select(['id', 'vendor_invoice_number'])->latest()->first();
        if (isset($last_invoice->vendor_invoice_number)) {
            return  $vendor_invoice->extractIntFromString($this->configTable()->vendor_invoice_prefix ?? null, $last_invoice->vendor_invoice_number) + 1;
        }

        return (int) $this->configTable()->vendor_invoice_start_number;
    }

    private function invoiceNumberString($vendor_invoice)
    {
        return $vendor_invoice->formatInvoiceNumber(
            $this->configTable()->vendor_invoice_prefix ?? null,
            $this->newInvoiceNumberInt($vendor_invoice)
        );
    }

    private function saveInvoiceNumber($vendor_invoice)
    {
        $vendor_invoice = $vendor_invoice->refresh();
        try {
            $vendor_invoice->vendor_invoice_number = $this->invoiceNumberString($vendor_invoice);
            $vendor_invoice->save();
        } catch (QueryException $e) {
            $this->saveInvoiceNumber($vendor_invoice);
        }
    }

    private function configTable(): Config
    {
        return Config::first();
    }
}
