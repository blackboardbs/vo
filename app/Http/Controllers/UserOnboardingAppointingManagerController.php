<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Jobs\EmploymentContractJob;
use App\Models\AdvancedTemplates;
use App\Models\Config;
use App\Models\ContractType;
use App\Mail\AppointingManagerMail;
use App\Models\PaymentBase;
use App\Models\Template;
use App\Models\User;
use App\Models\UserOnboarding;
use App\Models\UserOnboardingAppointingManager;
use App\Models\Vendor;
use App\PaymentType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;

class UserOnboardingAppointingManagerController extends Controller
{
    public function create(UserOnboarding $useronboarding): View
    {
        $parameters = [
            'leave_days' => Config::first(['annual_leave_days'])->annual_leave_days ?? 0,
            'useronboarding' => $useronboarding,
            'permanent_term_dropdown' => AdvancedTemplates::where('template_type_id', 10)->pluck('name', 'id'),
            'fixed_term_dropdown' => AdvancedTemplates::where('template_type_id', 9)->pluck('name', 'id'),
        ];

        return view('useronboarding.appointing_manager.create')
            ->with(array_merge($parameters, $this->dropDowns()));
    }

    public function store(Request $request, UserOnboarding $useronboarding): RedirectResponse
    {
        $appointing_manager = new userOnboardingAppointingManager();
        $appointing_manager->user_onboarding_id = $useronboarding->id;
        $this->saveData($appointing_manager, $request);

        return redirect()->route('useronboarding.show', $useronboarding->id)->with('flash_success', 'Appointing manager successfully saved');
    }

    public function confirmAppointment(Request $request, UserOnboarding $useronboarding): RedirectResponse
    {
        $useronboarding->appointment_confirmed_at = now();
        $useronboarding->status_id = 3;
        $useronboarding->save();

        $config = Config::with(['approvalManager' => function ($approvalManager) {
            $approvalManager->selectRaw('id, CONCAT(first_name, " ", last_name) AS full_name, email');
        }, 'company' => function ($company) {
            $company->select(['id', 'company_name']);
        }])->select(['hr_user_id', 'hr_approval_manager_id', 'admin_email', 'company_id'])->first();

        $hr_emails = isset($config->hrUser->email) ? trim($config->hrUser->email) : null;
        $approval_manager_email = isset($config->approvalManager->email) ? trim($config->approvalManager->email) : null;
        $appointing_manager_email = $useronboarding->user->appointmentManager->email ?? null;

        try {
            Mail::to(array_filter([$hr_emails, $approval_manager_email, $appointing_manager_email]))
                ->send(new AppointingManagerMail(
                    $useronboarding->user ?? null,
                    $useronboarding->user->appointmentManager ?? null,
                    $config->company->company_name ?? null,
                    $useronboarding
                )
                );
        } catch (\Exception $e) {
            logger($e->getMessage());
        }
        
        return redirect()->route('useronboarding.show', $useronboarding->id)->with('flash_success', 'Appointment successfully confirmed');
    }

    public function edit(UserOnboardingAppointingManager $appontmentmanager): View
    {
        return view('useronboarding.appointing_manager.edit')->with(array_merge([
            'appointment_manager' => $appontmentmanager,
            'permanent_term_dropdown' => AdvancedTemplates::where('template_type_id', 10)->pluck('name', 'id'),
            'fixed_term_dropdown' => AdvancedTemplates::where('template_type_id', 9)->pluck('name', 'id'),
        ], $this->dropDowns()));
    }

    public function update(Request $request, UserOnboardingAppointingManager $appontmentmanager): RedirectResponse
    {
        $this->saveData($appontmentmanager, $request);

        return redirect()->route('useronboarding.show', $appontmentmanager->user_onboarding_id)->with('flash_success', 'Appointing manager successfully updated');;
    }

    public function printPermanent(User $user)
    {
        if (!isset($user->userOnboarding->appointingManager->permanentContractTemplate))
        {
            return redirect()->route('users.edit',$user)->with('flash_warning', 'This action cannot be performed without appointing manager');
        }

        try {

            $template = $user->mapVariables($user->userOnboarding->appointingManager->permanentContractTemplate);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name($user->name() . "permanent employment Contract.pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('appontmentmanager.edit',$user->appointmentManager))->with('flash_danger', 'Edit your appointing manager and select permanent contract template.');
        }
    }

    public function printFixed(User $user)
    {
        if (!isset($user->userOnboarding->appointingManager->fixedContractTemplate))
        {
            return redirect()->route('users.edit',$user)->with('flash_warning', 'This action cannot be performed without appointing manager');
        }

        try {

            $template = $user->mapVariables($user->userOnboarding->appointingManager->fixedContractTemplate);

            return Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                //->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->name($user->name() . "permanent employment Contract.pdf");
        }catch (\TypeError $e) {
            report($e);
            return redirect(route('appontmentmanager.edit',$user->appointmentManager))->with('flash_danger', 'Edit your appointing manager and select permanent contract template.');
        }
    }

    public function send(User $user)
    {
        if (\request()->type == "fixed"){
            $templateRelationship = $user->userOnboarding?->appointingManager?->fixedContractTemplate;
        }else{
            $templateRelationship = $user->userOnboarding?->appointingManager?->permanentContractTemplate;
        }

        if (!isset($templateRelationship))
        {
            return redirect()->route('users.edit',$user)->with('flash_warning', 'This action cannot be performed without appointing manager');
        }

        try {
            if (! Storage::exists('useronboarding/templates')) {
                Storage::makeDirectory('useronboarding/templates');
            }

            $filename = str_replace(" ", "_", $user->name())."_fixed_contract_".date('Y_m_d_H_i_s').'.pdf';

            $storage = storage_path("app/useronboarding/templates/{$filename}");

            $template = $user->mapVariables($templateRelationship);

            Pdf::view('template.default-template', ['template' => $template])
                ->headerView('template.header')
                ->footerView('template.footer')
                ->format('a4')
                ->margins(30, 20, 30, 20, Unit::Pixel)
                ->save($storage);

            $type = \request()->type == "fixed" ? "Fixed Term" : "Permanent";

            $user->userOnboarding?->appointingManager?->documents()->create([
                'name' => "{$type} Term Contract For {$user->name()}",
                'file' => $filename,
                'document_type_id' => null,
                'creator_id' => auth()->id(),
                'status_id' => Status::ACTIVE->value,
            ]);

            $emails = [$user->email, $user->appointmentManager?->email];

            $mail = [
                'emails' => $emails,
                'attachments' => $storage,
                'subject' => "{$type} Contract For {$user->name()}",
                'body' => "Please find {$type} contract attached"
            ];

            EmploymentContractJob::dispatch($mail);

            return redirect()->back()->with('flash_success', 'Employment contract was successfully sent');

        }catch (\TypeError $e) {
            report($e);
            return redirect(route('appontmentmanager.edit',$user->appointmentManager))->with('flash_danger', 'Edit your appointing manager and select permanent contract template.');
        }
    }

    public function dropDowns()
    {
        return [
            'contract_type_dropdown' => ContractType::where('status_id', 1)->pluck('description', 'id'),
            'payment_type_dropdown' => PaymentType::where('status_id', 1)->pluck('description', 'id'),
            'payment_base_dropdown' => PaymentBase::where('status_id', 1)->pluck('description', 'id'),
            'reporting_manager_dropdown' => User::selectRaw('id, CONCAT(first_name, " ", last_name) AS full_name')->where('expiry_date', '>', now()->toDateString())->where('status_id', 1)->pluck('full_name', 'id'),
        ];
    }

    private function saveData($appointing_manager, $request)
    {
        $appointing_manager->contract_type_id = $request->contract_type_id;
        $appointing_manager->payment_base_id = $request->payment_base_id;
        $appointing_manager->payment_type_id = $request->payment_type_id;
        $appointing_manager->start_date = $request->start_date;
        $appointing_manager->end_date = $request->end_date;
        $appointing_manager->position = $request->position;
        $appointing_manager->annual_salary = $request->annual_salary;
        $appointing_manager->leave_days = $request->leave_days;
        $appointing_manager->notice_period = $request->notice_period;
        $appointing_manager->reporting_manager = $request->reporting_manager;
        $appointing_manager->other_income = $request->other_income;
        $appointing_manager->other_conditions = $request->other_conditions;
        $appointing_manager->require_laptop = $request->require_laptop;
        $appointing_manager->require_phone = $request->require_phone;
        $appointing_manager->require_mobile_internet = $request->require_mobile_internet;
        $appointing_manager->notes = $request->notes;
        $appointing_manager->responsibilities = $request->responsibilities;
        $appointing_manager->permenant_contract_template_id = $request->permenant_contract_template_id;
        $appointing_manager->fixed_contract_template_id = $request->fixed_contract_template_id;
        $appointing_manager->save();

        return $appointing_manager;
    }
}
