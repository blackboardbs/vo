<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResourceTaskRequest;
use App\Http\Requests\UpdateResourceTaskRequest;
use App\Jobs\SendResourceTaskEmailJob;
use App\Mail\ResourceTaskMail;
use App\Mail\ResourceTaskUpdateMail;
use App\Models\Module;
use App\Models\Notification;
use App\Models\ResourceTask;
use App\Models\Status;
use App\Models\User;
use App\Models\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ResourceTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $task = ResourceTask::with(['user:id,first_name,last_name', 'status:id,description'])->sortable(['company_name' => 'asc']);

        $module = Module::where('name', '=', \App\Models\Task::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $task = $task->whereIn('task_user', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $task->where('due_date', 'LIKE', '%'.$request->input('q').'%')
                ->orWhere('subject', 'like', '%'.$request->input('q').'%')
                ->orWhere('description', 'like', '%'.$request->input('q').'%')
                ->orWhere('task_number', 'like', '%'.$request->input('q').'%')
                ->orWhere('task_user', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                });
        }

        $task = $task->paginate($item);

        $parameters = [
            'task' => $task,
        ];

        return view('resource_task.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'date' => Carbon::now()->toDateString(),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'task_user_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Task User', 'all'),
        ];

        return view('resource_task.create')->with($parameters);
    }

    public function store(StoreResourceTaskRequest $request): RedirectResponse
    {
        $tasknumber = ResourceTask::orderBy('id')->get();

        $task = new ResourceTask;
        $task->task_number = $tasknumber->count() + 1;
        $task->subject = $request->input('subject');
        $task->description = $request->input('description');
        $task->due_date = $request->input('due_date');
        $task->task_user = $request->input('task_user');
        if ($request->has('email_task')) {
            $task->email_task = $request->input('email_task');
        }
        $task->created_by = auth()->id();
        $task->updated_by = auth()->id();
        $task->status_id = $request->input('status') ? $request->input('status') : 0;
        $task->save();

        $id = $task->id;

        if ($request->has('email_task')) {
            $resource = User::where('id', '=', $request->input('task_user'))->first(['email']);
            $user = User::where('id', '=', auth()->id())->first();

            //Mail::to($resource->email)->send(new ResourceTaskMail($task,$user,$resource));
            SendResourceTaskEmailJob::dispatch($resource->email, $task, $user, $resource)->delay(now()->addMinutes(5));
        }

        $notification = new Notification;
        $notification->name = 'New task: '.$request->input('subject');
        $notification->link = route('resource_task.show', $id);
        $notification->save();

        $taskuser = User::where('id', $request->input('task_user'))->first(['id']);

        $user_notifications = [];

        if (! is_null($taskuser->user_id)) {
            array_push($user_notifications, [
                'user_id' => $taskuser->user_id,
                'notification_id' => $notification->id,
            ]);
        }

        //User::find($taskuser->user_id)->notify(new NotifyUsersTask($task));
        UserNotification::insert($user_notifications);

        return redirect(route('resource_task.index'))->with('flash_success', 'Task captured successfully');
    }

    public function edit($taskid): View
    {
        $parameters = [
            'task' => ResourceTask::where('id', '=', $taskid)->get(),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'task_user_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Task User', 'all'),
        ];

        return view('resource_task.edit')->with($parameters);
    }

    public function update(UpdateResourceTaskRequest $request, $taskid): RedirectResponse
    {
        $task = ResourceTask::find($taskid);
        $task->task_number = $request->input('task_number');
        $task->subject = $request->input('subject');
        $task->description = $request->input('description');
        $task->due_date = $request->input('due_date');
        $task->task_user = $request->input('task_user');
        $task->rejection_reason = $request->input('rejection_reason');
        $task->rejection_date = $request->input('rejection_date');
        if ($request->has('email_task')) {
            $task->email_task = $request->input('email_task');
        }
        $task->created_by = auth()->id();
        $task->updated_by = auth()->id();
        $task->status_id = $request->input('status');
        $task->save();

        if ($request->has('email_task')) {
            $resource = User::where('id', '=', $request->input('task_user'))->first(['email']);
            $user = User::where('id', '=', auth()->id())->first();

            //Mail::to($resource->email)->send(new ResourceTaskUpdateMail($task,$user,$resource));
            SendResourceTaskEmailJob::dispatch($resource->email, $task, $user, $resource, 2)->delay(now()->addMinutes(5));
        }

        return redirect(route('resource_task.index'))->with('flash_success', 'Task updated successfully');
    }

    public function show(Request $request, $taskid): View
    {
        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $parameters = [
            'task' => ResourceTask::where('id', '=', $taskid)->get(),
            'path' => $path,
        ];

        return view('resource_task.show')->with($parameters);
    }

    public function destroy($taskid): RedirectResponse
    {
        ResourceTask::destroy($taskid);

        return redirect()->route('resource_task.index')->with('success', 'Task deleted successfully');
    }
}
