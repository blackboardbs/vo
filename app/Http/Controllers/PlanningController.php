<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Leave;
use App\Models\Project;
use App\Models\Team;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PlanningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $timestamp = time() - 86400;
        $date = strtotime('+1 day', $timestamp);
        $tday = date('Y-m-d', $date);
        $end_date = strtotime('Y-m-d', strtotime($date.'+30 days'));
        $tdayss = $tday;

        $timesheet_days = Calendar::select('id', 'day', 'weekend', 'date')
            ->where('date', '>=', $tday)
            ->orderBy('id')
            ->limit(31)
            ->get();

        $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
            ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
            ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
            ->where('assignment.end_date', '>=', $tday)
            ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
            ->orderBy('assignment.employee_id')
            ->orderBy('assignment.start_date')
            ->get();

        if ($request->has('company') && $request->company != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->where('assignment.company_id', '=', $request->company)
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        if ($request->has('team') && $request->team != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->with('resource')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->whereHas('resource', function ($query) use ($request) {
                    $query->where('team_id', '=', $request->team);
                })
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        if ($request->has('employee') && $request->employee != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->where('assignment.employee_id', '=', $request->employee)
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        if ($request->has('dates') && $request->dates != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->where('assignment.start_date', '=', substr($request->dates, 0, 10))
                ->where('assignment.end_date', '=', substr($request->dates, 13, 10))
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        if ($request->has('customer') && $request->customer != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->where('projects.customer_id', '=', $request->customer)
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        if ($request->has('billable') && $request->billable != null) {
            $timesheet_31 = User::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.rate,
            assignment.hours AS ahours, (assignment.hours * assignment.rate) AS aamount, projects.customer_id, assignment.billable, users.first_name, users.last_name'))
                ->leftJoin('assignment', 'users.id', '=', 'assignment.employee_id')
                ->rightJoin('projects', 'assignment.project_id', '=', 'projects.id')
                ->where('assignment.end_date', '>=', $tday)
                ->where('assignment.start_date', '<=', Carbon::now()->addDays(30)->toDateString())
                ->where('assignment.billable', '=', $request->billable)
                ->orderBy('assignment.employee_id')
                ->orderBy('assignment.start_date')
                ->get();
        }

        $a_leave_date = [];
        $leave_date = [];
        $projects = [];
        $customers = [];

        foreach ($timesheet_31 as $key => $plan) {
            $leave_date = [];
            for ($i = 0; $i <= 30; $i++) {
                $day = Carbon::now();
                $tday = $day->addDay($i)->toDateString();
                array_push($leave_date, Leave::where('emp_id', '=', $plan->employee_id)
                    ->whereDate('date_from', '<=', $tday)
                    ->whereDate('date_to', '>=', $tday)
                    ->where('status', '=', 1)
                    ->first());
            }
            $a_leave_date[$plan->employee_id] = $leave_date;
            array_push($projects, Project::find($plan->project_id)->name);
            array_push($customers, Customer::find($plan->customer_id)->customer_name);
        }

        $get_leave = [];
        foreach ($a_leave_date as $key => $leave_days) {
            $leave_type = [];
            foreach ($leave_days as $leave) {
                if ($leave != null) {
                    array_push($leave_type, $this->leave_type($leave->leave_type_id));
                } else {
                    array_push($leave_type, null);
                }
            }
            $get_leave[$key] = $leave_type;
        }

        //return  $a_leave_date[1][5]->emp_id;

        $calendar = Calendar::where('status', '=', 1)->whereDate('date', '<=', Carbon::now()->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get()->unique();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'dates_dropdown' => Assignment::select(DB::raw("CONCAT(start_date,' - ',COALESCE(`end_date`,'')) AS dates"), 'id')->orderBy('start_date', 'desc')->get()->pluck('dates', 'dates')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'tdayss' => $tdayss,
            'timesheet_days' => $timesheet_days,
            'timesheet_31' => $timesheet_31,
            'projects' => $projects,
            'customers' => $customers,
            'leave_days' => $leave_date,
            'leave_type' => $get_leave,
        ];

        return view('reports.planning')->with($parameters);
    }

    private function leave_type($leave_type_id)
    {
        switch ($leave_type_id) {
            case 1:
                return 'bg-info';
                break;
            case 2:
                return 'bg-danger';
                break;
            case 3:
                return 'bg-warning';
                break;
            case 4:
                return 'bg-unpaid';
                break;
            case 5:
                return 'bg-primary';
                break;
            case 6:
                return 'bg-purple';
                break;
            default:
                return 'bg-dark';
        }
    }
}
