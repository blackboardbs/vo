<?php

namespace App\Http\Controllers;

use App\Models\EmailList;
use Illuminate\Http\Request;
use Illuminate\View\View;

class EmailListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $mail_lists = EmailList::orderBy('id')->get();

        $parameters = [
            'mail_lists' => $mail_lists
        ];

        return view('emaillist.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('emaillist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(EmailList $emailList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailList $emailList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailList $emailList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailList $emailList)
    {
        //
    }
}
