<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRolesRequest;
use App\Permission;
use App\Models\PermissionRole;
use App\Models\Role;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        /*$roles = Role::orderBy('id','ASC')->with('permissions');
        $permissionsp = Permission::where('parent_id','0');
        $permissionsc = Permission::where('parent_id','!=','0')->where('level',null);
        $lvl = Permission::where('parent_id','!=','0')->where('level','!=',null)->orderBy('level','ASC');


        if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }

        return view('roles.index')->with(['roles' => $roles->get(), 'permissionsp' => $permissionsp->get(),'permissionsc' => $permissionsc->get(),'lvl' => $lvl->get()]);*/

        $roles = Role::orderBy('display_name')->with('permissions');

        if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', '%'.$request->input('q').'%');
        }

        return view('roles.index')->with(['roles' => $roles->get(), 'permissions' => Permission::all()]);
    }

    public function index_utilities(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'utilities');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'utilities')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        //dd(collect($roles->get())->toArray());
        return view('roles.utilities.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_company(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'company');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'company')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.company.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_resource(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'resource');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'resource')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.resource.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_customer(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'customer');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'customer')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.customer.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_vendor(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'vendor');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'vendor')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.vendor.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_insight(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'insights');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'insights')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.insights.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function index_admin(Request $request)
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'admin');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'admin')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        return view('roles.admin.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function loadpermissions()
    {
        $roles = Role::orderBy('id', 'ASC')->with('permissions');
        $data = Permission::all()->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        //dd(collect($roles->get())->toArray());
        return view('roles.index')->with(['roles' => $roles->get(), 'count' => count(collect($roles->get())->toArray()), 'permissions' => $itemsByReference]);
    }

    public function update_utilities(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['106', '175'])->delete();
        PermissionRole::whereBetween('permission_id', ['351', '385'])->delete();
        PermissionRole::whereBetween('permission_id', ['1996', '2065'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_utilities'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_company(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['176', '350'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_company'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_resource(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['386', '1015'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_resource'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_customer(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['1016', '1680'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_customer'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_vendor(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['1680', '1855'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_vendor'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_insights(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['1856', '1995'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_insight'))->with('flash_success', 'Roles updated successfully.');
    }

    public function update_admin(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        PermissionRole::whereBetween('permission_id', ['1', '105'])->delete();
        PermissionRole::whereBetween('permission_id', ['2066', '2170'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index_admin'))->with('flash_success', 'Roles updated successfully.');
    }

    public function create(): View
    {
        return view('roles.create')->with(['permissions' => Permission::all()]);
    }

    public function store(StoreRolesRequest $request): RedirectResponse
    {
        /*
        dd($request);*/
        $role = new Role;
        $role->name = $request->input('name');
        $role->display_name = $request->input('name');
        $role->description = 'User created role';
        $role->save();

        foreach ($request->input('permissions') as $permission_input) {
            $permission_role = new PermissionRole;
            $permission_role->permission_id = $permission_input;
            $permission_role->role_id = $role->id;
            $permission_role->save();
        }

        return redirect(route('roles.index'))->with('flash_success', 'Role created successfully.');
    }

    public function update(Request $request): RedirectResponse
    {
        /*dd($request);*/
        /*echo '<pre>';
        var_dump($request->input('permissions'));
        exit();*/
        /*PermissionRole::whereBetween('permission_id',['1000','2000'])->delete();

        foreach ($request->input('permissions') as $role_key => $role_input) {
            foreach ($role_input as $permission_input) {
                $permission_role = new PermissionRole;
                $permission_role->permission_id = $permission_input;
                $permission_role->role_id = $role_key;
                $permission_role->save();
            }
        }

        return redirect(route('roles.index'))->with('flash_success', 'Roles updated successfully.');*/
    }

    public function destroy(Role $role): RedirectResponse
    {
        PermissionRole::where('role_id', $role->id)->delete();

        $role->delete();

        return redirect(route('roles.index'))->with('flash_success', 'Role removed successfully.');
    }

    public function get_roles()
    {
        $roles = Role::orderBy('id', 'ASC')->with('permissions');
        $data = Permission::all()->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_utilities_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'utilities');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'utilities')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_company_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'company');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'company')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_resource_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'resource');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'resource')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_customer_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'utilities');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'customer')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_vendor_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'utilities');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'vendor')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_insights_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'insights');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'insights')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }

    public function get_admin_roles()
    {
        $roles = Role::with('permissions')
            ->whereHas('permissions', function ($query) {
                $query->where('menu_cat', '=', 'admin');
            })->orderBy('id', 'ASC');
        $data = collect(Permission::where('menu_cat', '=', 'admin')->get())->toArray();
        $rolepermissions = PermissionRole::all()->toArray();

        foreach ($data as $value) {
            $sql[] = $value;
        }
        $itemsByReference = [];
        // Build array of item references:
        foreach ($sql as $key => $item) {
            $itemsByReference[$item['id']] = $item;
        }

        // Set items as children of the relevant parent item.
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                $itemsByReference[$item['parent_id']]['children'][] = $item;
            }
        }
        // Remove items that were added to parents elsewhere:
        foreach ($sql as $key => $item) {
            if ($item['parent_id'] > '0' && isset($itemsByReference[$item['parent_id']])) {
                unset($sql[$key]);
            }
        }
        // Encode:
        /*echo json_encode($data);*/
        /*echo '<pre>';
        var_dump($itemsByReference);
        exit();*/
//        return json_encode($sql);

        $html = '';
        $roles = Role::orderBy('id')->with('permissions')->get();

        /*if ($request->has('q') && $request->input('q') != '') {
            $roles->where('display_name', 'like', "%" . $request->input('q') . "%");
        }*/

        $permissions = $itemsByReference;
        foreach ($permissions as $permission) {
            if ($permission['parent_id'] == '0' && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td><strong>'.$permission['display_name'].'</strong></td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] == '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == null && $permission['is_sub_level'] == '0') {
                $html .= '<tr>';
                $html .= '<td>'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }

            if ($permission['parent_id'] > '0' && $permission['level'] == '0' && $permission['is_sub_level'] == '1' and $permission['sub_level'] == '0') {
                $html .= '<tr class="accordian">';
                $html .= '<td style="padding-left:30px;">'.$permission['display_name'].'</td>';

                foreach ($roles as $role) {
                    if ($permission['parent_id'] > '0' && $permission['subparent_id'] != null) {
                        $html .= '<td><select name="permissions['.$role->id.']['.$permission['id'].']" class="form-control form-control-sm">';
                        $html .= '<option value="">Select</option>';
                        $html .= '<option value="'.($permission['id'] + 1).'" '.($role->permissions->where('id', $permission['id'] + 1)->count() > 0 ? 'selected' : '').'>5 - Full</option>';
                        $html .= '<option value="'.($permission['id'] + 2).'" '.($role->permissions->where('id', $permission['id'] + 2)->count() > 0 ? 'selected' : '').'>4 - Delete</option>';
                        $html .= '<option value="'.($permission['id'] + 3).'" '.($role->permissions->where('id', $permission['id'] + 3)->count() > 0 ? 'selected' : '').'>3 - Update</option>';
                        $html .= '<option value="'.($permission['id'] + 4).'" '.($role->permissions->where('id', $permission['id'] + 4)->count() > 0 ? 'selected' : '').'>2 - Create</option>';
                        $html .= '<option value="'.($permission['id'] + 5).'" '.($role->permissions->where('id', $permission['id'] + 5)->count() > 0 ? 'selected' : '').'>1 - View</option>';
                        $html .= '<option value="'.($permission['id'] + 6).'" '.($role->permissions->where('id', $permission['id'] + 6)->count() > 0 ? 'selected' : '').'>0 - None</option>';

                        $html .= '</select></td>';
                    }
                }

                $html .= '</tr>';
            }
        }

        return response()->json($html);
    }
}
