<?php

namespace App\Http\Controllers;

use App\Jobs\Data\ActionDataJob;
use App\Jobs\Data\AssessmentActivityDataJob;
use App\Jobs\Data\AssessmentConcernDataJob;
use App\Jobs\Data\AssessmentDataJob;
use App\Jobs\Data\AssessmentNextDataJob;
use App\Jobs\Data\AssessmentNoteDataJob;
use App\Jobs\Data\AssignmentDataJob;
use App\Jobs\Data\BoardDataJob;
use App\Jobs\Data\ContactDataJob;
use App\Jobs\Data\CustomerDataJob;
use App\Jobs\Data\CustomerInvoiceDataJob;
use App\Jobs\Data\CustomerInvoiceLineDataJob;
use App\Jobs\Data\CustomerInvoiceLineExpenseDataJob;
use App\Jobs\Data\CVDataJob;
use App\Jobs\Data\EpicDataJob;
use App\Jobs\Data\EquipmentDataJob;
use App\Jobs\Data\ExpenseTrackingDataJob;
use App\Jobs\Data\FeatureDataJob;
use App\Jobs\Data\GoalsDataJob;
use App\Jobs\Data\InsuranceDataJob;
use App\Jobs\Data\JobSpecDataJob;
use App\Jobs\Data\LeaveBalanceDataJob;
use App\Jobs\Data\LeaveDataJob;
use App\Jobs\Data\MessageBoardDataJob;
use App\Jobs\Data\ModelHasRoleDataJob;
use App\Jobs\Data\PlannedExpensesDataJob;
use App\Jobs\Data\PlannedUtilizationDataJob;
use App\Jobs\Data\ProjectDataJob;
use App\Jobs\Data\ProspectDataJob;
use App\Jobs\Data\QuotationDataJob;
use App\Jobs\Data\ResourceDataJob;
use App\Jobs\Data\RiskDataJob;
use App\Jobs\Data\ScoutingDataJob;
use App\Jobs\Data\SprintDataJob;
use App\Jobs\Data\SprintResourceDataJob;
use App\Jobs\Data\SprintReviewDataJob;
use App\Jobs\Data\TasksDataJob;
use App\Jobs\Data\TimeExpDataJob;
use App\Jobs\Data\TimelineDataJob;
use App\Jobs\Data\TimesheetDataJob;
use App\Jobs\Data\UserDataJob;
use App\Jobs\Data\UserOnboardingDataJob;
use App\Jobs\Data\UserStoryDataJob;
use App\Jobs\Data\VendorDataJob;
use App\Jobs\Data\VendorInvoiceDataJob;
use App\Jobs\Data\VendorInvoiceExpenseDataJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Bus;

class TrialDataContoller extends Controller
{
    public function __invoke(): JsonResponse
    {
        $batch = Bus::batch([
            new PlannedUtilizationDataJob(),
            new PlannedExpensesDataJob(),
            new ExpenseTrackingDataJob(),
            new MessageBoardDataJob(),
            new ContactDataJob(),
            new ProjectDataJob(),
            new AssignmentDataJob(),
            new TimesheetDataJob(),
            new TimelineDataJob(),
            new TimeExpDataJob(),
            new SprintDataJob(),
            new SprintResourceDataJob(),
            new GoalsDataJob(),
            new SprintReviewDataJob(),
            new RiskDataJob(),
            new ResourceDataJob(),
            new ScoutingDataJob(),
            new CVDataJob(),
            new JobSpecDataJob(),
            new TasksDataJob(),
            new EpicDataJob(),
            new FeatureDataJob(),
            new UserStoryDataJob(),
            new EquipmentDataJob(),
            new InsuranceDataJob(),
            new AssessmentDataJob(),
            new AssessmentActivityDataJob(),
            new AssessmentConcernDataJob(),
            new AssessmentNextDataJob(),
            new AssessmentNoteDataJob(),
            new LeaveDataJob(),
            new LeaveBalanceDataJob(),
            new ActionDataJob(),
            new BoardDataJob(),
            new CustomerDataJob(),
            new QuotationDataJob(),
            new ProspectDataJob(),
            new CustomerInvoiceDataJob(),
            new CustomerInvoiceLineDataJob(),
            new CustomerInvoiceLineExpenseDataJob(),
            new VendorDataJob(),
            new VendorInvoiceDataJob(),
            new VendorInvoiceExpenseDataJob(),
            new UserOnboardingDataJob(),
            new UserDataJob(),
            new ModelHasRoleDataJob()
        ])->dispatch();

        return response()->json(['batchId' => $batch->id]);
    }
}
