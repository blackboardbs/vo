<?php

namespace App\Http\Controllers;

use App\Models\Anniversary;
use App\Models\Assignment;
use App\Models\Config;
use App\Models\CustomerInvoice;
use App\Models\Event;
use App\Models\Leave;
use App\Models\Log;
use App\Models\PlanUtilization;
use App\Recents\Recents;
use App\Models\Resource;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\VendorInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
//use Silber\Bouncer\BouncerFacade as Bouncer;
use Silber\Bouncer\Bouncer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $recents;

    public function __construct()
    {
        $this->recents = new Recents();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->check()) {
            $date = Carbon::now();

            if (isset($request->default)) {
                if ($request->default == -1) {
                    return redirect(route('setup.stepone'));
                }

                if (Auth::user()->isAn('admin')) {
                    $logs = Log::select('date', 'login_status')->whereBetween('date', [$date->subDays(14)->toDateString(), $date->now()->toDateString()])->orderBy('date', 'desc')->groupBy('date')->groupBy('login_status')->get();
                    $login_success = [];
                    $login_fail = [];

                    foreach ($logs as $log) {
                        if ($log->login_status == 1) {
                            array_push($login_success, Log::where('date', '=', $log->date)->where('login_status', '=', $log->login_status)->get()->count());
                        } else {
                            array_push($login_fail, Log::where('date', '=', $log->date)->where('login_status', '=', 0)->get()->count());
                        }
                    }

                    $parameters = [
                        'expired_users' => User::where('expiry_date', '<=', $date->toDateString())->get()->count(),
                        'users_to_expire_30' => User::where('expiry_date', '>=', $date->toDateString())->where('expiry_date', '<=', $date->addDays(30)->toDateString())->get()->count(),
                        'timesheets_count' => Timesheet::get()->count(),
                        'failed_logins' => Log::where('login_status', '=', 0)->get()->count(),
                        'login_success' => $login_success,
                        'login_fail' => $login_fail,
                        'logs' => $logs,
                    ];

                    return view('dashboard')->with($parameters);
                } elseif (Auth::user()->isAn('admin_manager')) {
                    $util_past_3_weeks = $this->utils(3, 1, $date->now());
                    $util_this_week = $this->utils(1, 1, $date->now());
                    $ty_value = Timesheet::select(DB::raw('SUM(CASE WHEN (timeline.is_billable = 1) THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS actual'))
                        ->join('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.year_week', '>=', $date->year.($date->weekOfYear - ($date->now()->weekOfYear - 1)))
                        ->where('timesheet.year_week', '<=', $date->year.($date->weekOfYear - 1))
                        ->first();
                    $ly_value = Timesheet::select(DB::raw('SUM(CASE WHEN (timeline.is_billable = 1) THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS actual'))
                        ->join('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.year_week', '>=', ($date->year - 1).($date->weekOfYear - ($date->now()->weekOfYear - 1)))
                        ->where('timesheet.year_week', '<=', ($date->year - 1).($date->weekOfYear - 1))
                        ->first();
                    $ytd_value_variance = (isset($ty_value->actual) && isset($ly_value->actual) && isset($ly_value->actual) && (($ty_value->actual - $ly_value->actual) > 0) && $ly_value->actual > 0) ? ($ty_value->actual - $ly_value->actual) / $ly_value->actual : 0;
                    $events = Event::whereYear('event_date', '=', $date->now()->year)
                        ->whereMonth('event_date', '>=', ($date->now()->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('event_date', '<=', ($date->now()->month < 10) ? '0'.($date->now()->addMonth(1)->month) : $date->now()->addMonth(1)->month)
                        ->orderBy('event_date')->get();

                    $leaves = Leave::where('leave_status', '=', 0)->whereNull('leave_status')->get();
                    $expense_claims = TimeExp::where('claim_auth_user', '!=', 0)
                        ->whereHas('timesheet', function ($query) use ($date) {
                            $query->where('year_week', '<=', $date->now()->year.($date->now()->weekOfYear - 1));
                            $query->where('year_week', '>=', $date->now()->year.($date->now()->weekOfYear - 6));
                        })
                        ->orderBy('timesheet_id', 'desc')->get();
                    $expenses_total = 0;
                    foreach ($expense_claims as $claim) {
                        $expenses_total += $claim->amount;
                    }
                    $important_dates = Anniversary::whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->month : $date->month)
                        ->whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('anniversary_date', '<=', ($date->month < 10) ? '0'.$date->now()->addMonth(1)->month : $date->now()->addMonth(1)->month)
                        ->orderBy('anniversary_date')
                        ->get();
                    $assignments = Assignment::select('project_id', 'end_date')
                        ->whereBetween('end_date', [$date->now()->toDateString(), $date->now()->addDays(30)->toDateString()])
                        ->groupBy('project_id')
                        ->groupBy('end_date')
                        ->get();

                    $vendor_inv_outstanding = VendorInvoice::where('vendor_invoice_status', '!=', 2)->get();

                    $weeks = PlanUtilization::select('yearwk')->whereBetween('yearwk', [$date->now()->year.$date->now()->subWeeks(5)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])->orderBy('yearwk', 'desc')->get();

                    $resources = Assignment::select('employee_id')
                        ->whereDate('end_date', '>=', $date->now()->toDateString())
                        ->where('billable', '=', 1)
                        ->groupBy('employee_id')
                        ->get();

                    $outstanding_timesheet = [];

                    $current_week = Carbon::parse(now())->year.''.(Carbon::parse(now())->weekOfYear < 10 ? '0'.Carbon::parse(now())->weekOfYear : Carbon::parse(now())->weekOfYear);

                    foreach ($weeks as $week) {
                        foreach ($resources as $resource) {
                            $time = Timesheet::select('year_week', 'employee_id')->where('year_week', '=', $week->yearwk)->where('employee_id', '=', $resource->employee_id)->groupBy('year_week')->groupBy('employee_id')->first();
                            if ($week->yearwk <= $current_week) {
                                $outstanding_timesheet[] = (isset($time) ? null : $resource->resource->first_name.' '.$resource->resource->last_name.'-'.$week->yearwk);
                            }
                        }
                    }

                    $parameters = [
                        'utilization_from_3_weeks_ago' => $util_past_3_weeks,
                        'utilization_this_week' => $util_this_week,
                        'ytd_value_variance' => $ytd_value_variance,
                        'timesheets_count' => Timesheet::get()->count(),
                        'failed_logins' => Log::where('login_status', '=', 0)->get()->count(),
                        'events' => $events,
                        'leaves' => $leaves,
                        'assignments' => $assignments,
                        'expense_claims' => $expense_claims,
                        'expenses_total' => $expenses_total,
                        'important_dates' => $important_dates,
                        'debtors_outstanding' => CustomerInvoice::where('bill_status', '=', 1)->get()->count(),
                        'vendor_inv_outstanding' => $vendor_inv_outstanding,
                        'outstanding_timesheet' => $outstanding_timesheet,
                    ];

                    return view('dashboard')->with($parameters);
                } elseif (Auth::user()->isAn('manager')) {
                    $util_this_week = $this->utils(1, 1, $date->now());

                    $plan_util = PlanUtilization::first();
                    $util_hours = ($plan_util != null ? $plan_util->wk_hours : 0);

                    $resources = Assignment::select('assignment.employee_id')
                        ->leftJoin('resource', 'assignment.employee_id', '=', 'resource.user_id')
                        ->whereDate('assignment.end_date', '>=', $date->now()->toDateString())
                        ->where('resource.manager_id', '=', Auth::user()->id)
                        ->where('assignment.billable', '=', 1)
                        ->groupBy('assignment.employee_id')
                        ->get();

                    $planned_3 = ($resources != null && $util_hours != null ? (count($resources) * $util_hours) * 3 : 0);
                    $actual_3 = 0;

                    foreach ($resources as $resource) {
                        $actual_3 += Timeline::selectRaw('SUM(total + (total_m/60)) AS actual_hours')
                            ->whereHas('timesheet', function ($query) use ($resource, $date) {
                                $query->whereBetween('year_week', [$date->now()->year.$date->now()->subWeeks(3)->weekOfYear, $date->now()->year.$date->now()->subWeek()->weekOfYear]);
                                $query->where('employee_id', '=', $resource->employee_id);
                            })
                            ->where('is_billable', '=', 1)
                            ->first()->actual_hours;
                    }

                    $util_past_3_weeks = ($actual_3 != 0) ? ($actual_3 / $planned_3) * 100 : 0;

                    $planned_1 = count($resources) * $util_hours;
                    $actual_1 = 0;

                    foreach ($resources as $resource) {
                        $actual_1 += Timeline::selectRaw('SUM(total + (total_m/60)) AS actual_hours')
                            ->whereHas('timesheet', function ($query) use ($resource, $date) {
                                $query->where('year_week', '=', $date->now()->year.$date->now()->subWeek()->weekOfYear);
                                $query->where('employee_id', '=', $resource->employee_id);
                            })
                            ->where('is_billable', '=', 1)
                            ->first()->actual_hours;
                    }

                    $util_this_week = ($actual_1 != 0) ? ($actual_1 / $planned_1) * 100 : 0;

                    $assignments_total_hours = Assignment::select('project_id', 'hours', 'end_date')
                        ->whereDate('end_date', '>=', $date->now()->toDateString())
                        ->whereHas('resource_user', function ($query) {
                            $query->where('manager_id', '=', Auth::user()->id);
                        })
                        ->where('billable', '=', 1)
                        ->distinct()
                        ->get();

                    //return $assignments_total_hours;
                    $assignment_hours = 0;
                    $worked_hours = [];
                    foreach ($assignments_total_hours as $assignments_total_hour) {
                        $assignment_hours += $assignments_total_hour->hours;
                        $worked_hours[] = Timeline::select(DB::raw('SUM(CASE WHEN `is_billable` = 1 THEN`total` + ROUND(total_m/60 , 2) ELSE 0 END) AS worked_hours'))
                            ->whereHas('timesheet', function ($query) use ($assignments_total_hour) {
                                $query->where('project_id', '=', $assignments_total_hour->project_id);
                            })->first();
                    }
                    $total_worked_hours = 0;
                    foreach ($worked_hours as $hours) {
                        $total_worked_hours += isset($hours->worked_hours) ? $hours->worked_hours : 0;
                    }

                    $assignment_hours -= $total_worked_hours;

                    //END Assignment Hours Available

                    $leave_days = Leave::select(DB::raw('IFNULL(SUM(no_of_days),0) AS days'))
                        ->whereBetween('date_from', [$date->now()->subMonths(1)->toDateString(), $date->now()->addWeek()->toDateString()])
                        ->whereBetween('date_to', [$date->now()->toDateString(), $date->now()->addMonths(2)->toDateString()])
                        ->where('leave_status', '=', 1)
                        ->first();

                    $events = Event::whereYear('event_date', '=', $date->now()->year)
                        ->whereMonth('event_date', '>=', ($date->now()->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('event_date', '<=', ($date->now()->month < 10) ? '0'.($date->now()->addMonth(1)->month) : $date->now()->addMonth(1)->month)
                        ->orderBy('event_date')->get();

                    $timesheet = Timesheet::where('company_id', '=', Config::first()->company_id)
                        ->where('year', '=', $date->now()->year)
                        ->where('week', '>=', ($date->now()->weekOfYear - 6))
                        ->where('week', '<=', ($date->now()->weekOfYear - 1))
                        ->whereNull('invoice_paid_date')
                        ->orderBy('year_week', 'desc')
                        ->take(4)
                        ->get();

                    $leaves = Leave::where('leave_status', '=', 0)->whereNull('leave_status')->get();
                    $expense_claims = TimeExp::where('claim_auth_user', '!=', 0)
                        ->whereHas('timesheet', function ($query) use ($date) {
                            $query->where('year_week', '<=', $date->now()->year.($date->now()->weekOfYear - 1));
                            $query->where('year_week', '>=', $date->now()->year.($date->now()->weekOfYear - 6));
                            $query->whereHas('resource', function ($q) {
                                $q->where('manager_id', '=', Auth::user()->id);
                            });
                        })
                        ->orderBy('timesheet_id', 'desc')->get();
                    $expenses_total = 0;
                    foreach ($expense_claims as $claim) {
                        $expenses_total += $claim->amount;
                    }
                    $important_dates = Anniversary::whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->month : $date->month)
                        ->whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('anniversary_date', '<=', ($date->month < 10) ? '0'.$date->now()->addMonth(1)->month : $date->now()->addMonth(1)->month)
                        ->orderBy('anniversary_date')
                        ->get();
                    $assignments = Assignment::select('project_id', 'end_date')
                        ->whereBetween('end_date', [$date->now()->toDateString(), $date->now()->addDays(30)->toDateString()])
                        ->whereHas('resource_user', function ($query) {
                            $query->where('manager_id', '=', Auth::user()->id);
                        })
                        ->groupBy('project_id')
                        ->groupBy('end_date')
                        ->get();

                    $vendor_inv_outstanding = VendorInvoice::where('vendor_invoice_status', '!=', 2)->get();

                    $weeks = PlanUtilization::select('yearwk')->whereBetween('yearwk', [$date->now()->year.$date->now()->subWeeks(5)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])->orderBy('yearwk', 'desc')->get();

                    $outstanding_timesheet = [];

                    $current_week = Carbon::parse(now())->year.''.(Carbon::parse(now())->weekOfYear < 10 ? '0'.Carbon::parse(now())->weekOfYear : Carbon::parse(now())->weekOfYear);

                    foreach ($weeks as $week) {
                        foreach ($resources as $resource) {
                            $time = Timesheet::select('year_week', 'employee_id')->where('year_week', '=', $week->yearwk)->where('employee_id', '=', $resource->employee_id)->groupBy('year_week')->groupBy('employee_id')->first();
                            if ($week->yearwk <= $current_week) {
                                $outstanding_timesheet[] = (isset($time)) ? null : $resource->resource->first_name.' '.$resource->resource->last_name.'-'.$week->yearwk;
                            }
                        }
                    }

                    $parameters = [
                        'utilization_from_3_weeks_ago' => $util_past_3_weeks,
                        'utilization_this_week' => $util_this_week,
                        'assignment_hours' => $assignment_hours,
                        'leave_days' => $leave_days->days,
                        'timesheets_count' => Timesheet::get()->count(),
                        'failed_logins' => Log::where('login_status', '=', 0)->get()->count(),
                        'timesheets' => $timesheet,
                        'events' => $events,
                        'leaves' => $leaves,
                        'assignments' => $assignments,
                        'expense_claims' => $expense_claims,
                        'expenses_total' => $expenses_total,
                        'important_dates' => $important_dates,
                        'debtors_outstanding' => CustomerInvoice::where('bill_status', '=', 1)->get()->count(),
                        'vendor_inv_outstanding' => $vendor_inv_outstanding,
                        'outstanding_timesheet' => $outstanding_timesheet,
                    ];

                    return view('dashboard')->with($parameters);
                } elseif (Auth::user()->isAn('consultant') || Auth::user()->is('contractor')) {
                    $actual = Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60)) AS actual'))
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.employee_id', '=', Auth::user()->id)
                        ->whereBetween('timesheet.year_week', [$date->now()->year.$date->now()->subWeeks(3)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])
                        ->where('timeline.is_billable', '=', 1)
                        ->first();

                    $actual1 = Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60)) AS actual'))
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.employee_id', '=', Auth::user()->id)
                        ->where('timesheet.year_week', '=', $date->now()->year.$date->now()->subWeeks(1)->weekOfYear)
                        ->where('timeline.is_billable', '=', 1)
                        ->first();

                    $ass_hours = Assignment::select(DB::raw('SUM(hours) AS hours, project_id'))
                        ->where('employee_id', '=', Auth::user()->id)
                        ->whereDate('end_date', '>', $date->now()->toDateString())
                        ->groupBy('project_id')
                        ->get();

                    // return $date->now()->year;

                    $expenses = TimeExp::whereHas('timesheet', function ($query) use ($date) {
                        $query->where('employee_id', '=', Auth::user()->id);
                        $query->where('year_week', '>=', $date->now()->year.$date->now()->subWeeks(5)->weekOfYear);
                    })
                        ->get();

                    $important_dates = Anniversary::whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->month : $date->month)
                        ->whereMonth('anniversary_date', '>=', ($date->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('anniversary_date', '<=', ($date->month < 10) ? '0'.$date->now()->addMonth(1)->month : $date->now()->addMonth(1)->month)
                        ->orderBy('anniversary_date')
                        ->get();

                    $leave_not_approved = Leave::where('emp_id', '=', Auth::user()->id)->where('leave_status', '!=', 1)->get();

                    $events = Event::whereYear('event_date', '=', $date->now()->year)
                        ->whereMonth('event_date', '>=', ($date->now()->month < 10) ? '0'.$date->now()->month : $date->now()->month)
                        ->whereMonth('event_date', '<=', ($date->now()->month < 10) ? '0'.($date->now()->addMonth(1)->month) : $date->now()->addMonth(1)->month)
                        ->orderBy('event_date')->get();

                    $time_hour = [];

                    $assignment_outstanding_hours = 0;

                    foreach ($ass_hours as $key => $ass_hour) {
                        array_push($time_hour, Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60)) AS thours'))
                            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                            ->where('timesheet.employee_id', '=', Auth::user()->id)
                            ->where('timesheet.project_id', '=', $ass_hour->project_id)
                            ->first()
                        );

                        $assignment_outstanding_hours += ($ass_hour->hours - $time_hour[$key]->thours);
                    }

                    $assignment_to_expire = Assignment::whereBetween('end_date', [$date->now()->toDateString(), $date->now()->addDays(30)->toDateString()])
                        ->where('employee_id', '=', Auth::user()->id)
                        ->get();

                    $join_date = Resource::where('user_id', '=', Auth::user()->id)->first();

                    $start_date = ($date->now()->toDateString() <= $date->now()->year.substr($join_date->join_date, 4)) ? $date->now()->subYear()->year.substr($join_date->join_date, 4) : $date->now()->year.substr($join_date->join_date, 4);
                    $end_date = ($date->now()->toDateString() <= $date->now()->year.substr($join_date->join_date, 4)) ? $date->now()->year.substr($join_date->join_date, 4) : $date->now()->addYear()->year.substr($join_date->join_date, 4);

                    $date1 = strtotime(($join_date->join_date != null) ? $start_date : Carbon::parse('first day of January'));
                    $date2 = strtotime($date->now()->toDateString());

                    $year1 = date('Y', $date1);
                    $year2 = date('Y', $date2);

                    $month1 = date('m', $date1);
                    $month2 = date('m', $date2);

                    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

                    $leave = Leave::where('emp_id', '=', Auth::user()->id)
                        ->where('leave_type_id', '=', 1)
                        ->whereBetween('date_from', [($join_date->join_date != null) ? $start_date : Carbon::parse('first day of January'), ($join_date->join_date != null) ? $end_date : Carbon::parse('last day of December')])
                        ->get();

                    $util_past_3_weeks = ($actual->actual != 0) ? ($actual->actual / 120) * 100 : 0;
                    $util_this_week = ($actual1->actual != 0) ? ($actual1->actual / 40) * 100 : 0;

                    $plan_util = PlanUtilization::select('yearwk')->whereBetween('yearwk', [$date->now()->year.$date->now()->subWeeks(5)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])->get();
                    $outstanding_timesheets = [];

                    $current_week = Carbon::parse(now())->year.''.(Carbon::parse(now())->weekOfYear < 10 ? '0'.Carbon::parse(now())->weekOfYear : Carbon::parse(now())->weekOfYear);

                    foreach ($plan_util as $plan) {
                        if ($plan->yearwk <= $current_week) {
                            array_push($outstanding_timesheets, Timesheet::select('employee_id')->where('year_week', '=', $plan->yearwk)->where('employee_id', '=', Auth::user()->id)->first());
                        }
                    }

                    $parameters = [
                        'utilization_from_3_weeks_ago' => $util_past_3_weeks,
                        'utilization_this_week' => $util_this_week,
                        'assignment_outstanding_hours' => $assignment_outstanding_hours,
                        'leave_days' => isset($leave->no_of_days) ? ($diff * 1.25) - $leave->no_of_days : ($diff * 1.25),
                        'expenses' => $expenses,
                        'leave_not_approved' => $leave_not_approved,
                        'events' => $events,
                        'important_dates' => $important_dates,
                        'assignment_to_expire' => $assignment_to_expire,
                        'plan_util' => $plan_util,
                        'outstanding_timesheets' => $outstanding_timesheets,
                        'user' => User::find(Auth::user()->id),
                    ];

                    return view('dashboard')->with($parameters);
                } elseif (Auth::user()->isAn('vendor') || Auth::user()->is('customer_client')) {
                    $actual = Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60)) AS actual'))
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.employee_id', '=', Auth::user()->id)
                        ->whereBetween('timesheet.year_week', [$date->now()->year.$date->now()->subWeeks(3)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])
                        ->where('timeline.is_billable', '=', 1)
                        ->first();

                    $actual1 = Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60) ) AS actual'))
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->where('timesheet.employee_id', '=', Auth::user()->id)
                        ->where('timesheet.year_week', '=', $date->now()->year.$date->now()->subWeeks(1)->weekOfYear)
                        ->where('timeline.is_billable', '=', 1)
                        ->first();

                    $ass_hours = Assignment::select(DB::raw('SUM(hours) AS hours, project_id'))
                        ->where('employee_id', '=', Auth::user()->id)
                        ->whereDate('end_date', '>', $date->now()->toDateString())
                        ->groupBy('project_id')
                        ->get();

                    $time_hour = [];

                    $assignment_outstanding_hours = 0;

                    foreach ($ass_hours as $key => $ass_hour) {
                        array_push($time_hour, Timesheet::select(DB::raw('SUM(timeline.total + (timeline.total_m/60) ) AS thours'))
                            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                            ->where('timesheet.employee_id', '=', Auth::user()->id)
                            ->where('timesheet.project_id', '=', $ass_hour->project_id)
                            ->first()
                        );

                        $assignment_outstanding_hours += ($ass_hour->hours - $time_hour[$key]->thours);
                    }

                    $open_assignments = Assignment::select(DB::raw('assignment.employee_id, assignment.project_id, assignment.start_date, assignment.end_date, assignment.hours, assignment.project_type,
                    SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS billable_hours,
                    SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS non_billable_hours'))
                        ->leftJoin('timesheet', function ($join) {
                            $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
                            $join->on('assignment.project_id', '=', 'timesheet.project_id');
                        })
                        ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                        ->whereDate('assignment.end_date', '>=', $date->now()->toDateString())
                        ->where('assignment.assignment_status', '!=', 5)
                        ->where('assignment.assignment_status', '!=', 4)
                        ->where('assignment.assignment_approver', '=', Auth::user()->id)
                        ->orWhere('assignment.employee_id', '=', Auth::user()->id)
                        ->groupBy('assignment.employee_id')
                        ->groupBy('assignment.project_id')
                        ->groupBy('assignment.start_date')
                        ->groupBy('assignment.end_date')
                        ->groupBy('assignment.hours')
                        ->groupBY('assignment.project_type')
                        ->get();

                    $plan_util = PlanUtilization::select('yearwk')->whereBetween('yearwk', [$date->now()->year.$date->now()->subWeeks(5)->weekOfYear, $date->now()->year.$date->now()->subWeeks(1)->weekOfYear])->get();
                    $outstanding_timesheets = [];
                    $current_week = Carbon::parse(now())->year.''.(Carbon::parse(now())->weekOfYear < 10 ? '0'.Carbon::parse(now())->weekOfYear : Carbon::parse(now())->weekOfYear);

                    foreach ($plan_util as $plan) {
                        if ($plan->yearwk <= $current_week) {
                            array_push($outstanding_timesheets, Timesheet::select('employee_id')->where('year_week', '=', $plan->yearwk)->where('employee_id', '=', Auth::user()->id)->first());
                        }
                    }
                    $outstanding_timesheet_number = 0;
                    foreach ($outstanding_timesheets as $t) {
                        ($t == null) ? $outstanding_timesheet_number += 1 : $outstanding_timesheet_number += 0;
                    }

                    $util_past_3_weeks = ($actual->actual != 0) ? ($actual->actual / 120) * 100 : 0;
                    $util_this_week = ($actual1->actual != 0) ? ($actual1->actual / 40) * 100 : 0;

                    $parameters = [
                        'utilization_from_3_weeks_ago' => $util_past_3_weeks,
                        'utilization_this_week' => $util_this_week,
                        'assignment_outstanding_hours' => $assignment_outstanding_hours,
                        'open_assignments' => $open_assignments,
                        'date' => $date->now()->toDateString(),
                        'plan_util' => $plan_util,
                        'outstanding_timesheets' => $outstanding_timesheets,
                        'user' => User::find(Auth::user()->id),
                        'outstanding_timesheet_number' => $outstanding_timesheet_number,
                    ];

                    return view('dashboard')->with($parameters);
                }
            } else {
                return redirect(route('dashboard.index'));
            }
            /**/
        } else {
            return view('welcome');
        }
    }

    public function recents(): View
    {
        $projects = $this->recents->recent_activities(\App\Models\Project::class, 'project.show');
        $invoices_vendor = $this->recents->recent_activities(\App\Models\VendorInvoice::class, 'view.invoice');
        $invoices_customer = $this->recents->recent_activities(\App\Models\CustomerInvoice::class, 'customer.invoice_show');
        $assignments = $this->recents->recent_activities(\App\Models\Assignment::class, 'assignment.show');
        $leave = $this->recents->recent_activities(\App\Models\Leave::class, 'leave.show');
        $timesheet = $this->recents->recent_activities(\App\Models\Timesheet::class, 'timesheet.show');
        $cv = $this->recents->recent_activities(\App\Models\Cv::class, 'cv.show', true);
        $time_exps = $this->recents->recent_activities(\App\Models\TimeExp::class, 'timesheet.show');
        $expense_tracing = $this->recents->recent_activities(\App\Models\ExpenseTracking::class, 'exptracking.show');
        $expenses = array_merge($time_exps, $expense_tracing);

        $parameters = [
            'projects' => $projects,
            'assignments' => $assignments,
            'vendor_invoice' => $invoices_vendor,
            'customer_invoices' => $invoices_customer,
            'leaves' => $leave,
            'timesheets' => $timesheet,
            'cvs' => $cv,
            'expenses' => array_slice(collect($expenses)->sortBy('created_at', 0, true)->values()->all(), 0, 10),
        ];

        return view('recents')->with($parameters);
    }

    private function utils($start, $end, $date, $year = 0)
    {
        $planned = PlanUtilization::select(DB::raw('SUM(`res_no` * `wk_hours`) AS planned_util'))
            ->where('yearwk', '>=', ($date->year - $year).($date->weekOfYear - $start))
            ->where('yearwk', '<=', ($date->year - $year).($date->weekOfYear - $end))
            ->first();

        $actual = Timesheet::select(DB::raw('SUM(CASE WHEN (timeline.is_billable = 1) THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS actual'))
            ->join('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.year_week', '>=', ($date->year - $year).($date->weekOfYear - $start))
            ->where('timesheet.year_week', '<=', ($date->year - $year).($date->weekOfYear - $end))
            ->first();

        return ((($actual->actual == null) || ($actual->actual == 0)) && (($planned->planned_util == null) || ($planned->planned_util == 0))) ? 0 : ($actual->actual / $planned->planned_util) * 100;
    }
}
