<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Project;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    /*
     * This function return all parameters for the vue component create invoice
     * @return parameters
     * */
    public function getInvoiceParameters(Request $request)
    {

        $companies = Company::where('status_id', '=', 1)->orderBy('company_name')->get();
        $customers = Customer::where('status', '=', 1)->orderBy('customer_name')->get();
        $vendors = Vendor::where('status_id', '=', 1)->orderBy('vendor_name')->get();

        $invoiceNumber = 0;
        $invoiceTypeID = 1; //Type 1 for customer invoice, type 2 for vendor invoice

        $parameters = [
            'invoiceNumber' => $invoiceNumber,
            'invoiceTypeID' => $invoiceTypeID,
            'companies' => $companies,
            'company_id' => $this->defaultCompanyId(),
            'customers' => $customers,
            'vendors' => $vendors,
            'invoice_date' => now()->toDateString(),
            'due_date' => now()->addDays(30)->toDateString(),
        ];

        return $parameters;
    }

    /*
     * This function returns all the resources assigned to a project
     * @parameter $request
     * @return resource
     * */
    public function getInvoiceResources(Request $request)
    {
        $project = Project::find($request->input('project_id'));

        $resources = null;
        if (isset($project)) {
            $resources_ids = explode(',', $project->consultants);

            $resources = User::whereIn('id', $resources_ids)->where('status_id', '=', 1)->get();
        }

        return $resources;
    }

    public function defaultCompanyId(): int
    {
        $config = Config::with(['company:id,company_name'])->first();
        return filled($config->company) ? $config->company_id : Company::first(['id'])?->id;
    }
}
