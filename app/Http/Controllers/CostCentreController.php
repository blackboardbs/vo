<?php

namespace App\Http\Controllers;

use App\Models\CostCenter;
use App\Http\Requests\StoreCostCentreRequest;
use App\Http\Requests\UpdateCostCentreRequest;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CostCentreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $costcentre = CostCenter::sortable('description', 'asc')->where('status', 1)->paginate($item);

        $costcentre = CostCenter::with(['appr_name:id,first_name,last_name', 'statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $costcentre = $costcentre->where('status', $request->input('status_filter'));
            }else{
                $costcentre = $costcentre->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $costcentre = $costcentre->where('description', 'like', '%'.$request->input('q').'%');
        }

        $costcentre = $costcentre->paginate($item);

        $parameters = [
            'costcentre' => $costcentre,
        ];

        return View('master_data.costcentre.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = CostCenter::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'approver_dropdown' => User::select(DB::raw("CONCAT(first_name,' ', last_name) AS full_name"), 'id')->pluck('full_name', 'id')->prepend('Select Approver', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.costcentre.create')->with($parameters);
    }

    public function store(StoreCostCentreRequest $request): RedirectResponse
    {
        $cost = new CostCenter();
        $cost->description = $request->input('description');
        $cost->status = $request->input('status');
        $cost->approver = $request->input('approver');
        $cost->creator_id = auth()->id();
        $cost->save();

        return redirect(route('master_cost.index'))->with('flash_success', 'Master Cost Center captured successfully');
    }

    public function show($costid): View
    {
        $parameters = [
            'cost' => CostCenter::where('id', '=', $costid)->get(),
        ];

        return View('master_data.costcentre.show')->with($parameters);
    }

    public function edit($costid): View
    {
        $autocomplete_elements = CostCenter::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'cost' => CostCenter::where('id', '=', $costid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'approver_dropdown' => User::select(DB::raw("CONCAT(first_name,' ', last_name) AS full_name"), 'id')->pluck('full_name', 'id')->prepend('Select Approver', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.costcentre.edit')->with($parameters);
    }

    public function update(UpdateCostCentreRequest $request, $costid): RedirectResponse
    {
        $cost = CostCenter::find($costid);
        $cost->description = $request->input('description');
        $cost->status = $request->input('status');
        $cost->approver = $request->input('approver');
        $cost->creator_id = auth()->id();
        $cost->save();

        return redirect(route('master_cost.index'))->with('flash_success', 'Master Cost Center saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // DB::table('cost_center')->delete($id);
        //return response()->json(['success'=>"Product suspended successfully.", 'tr'=>'tr_'.$id]);
        // CostCenter::destroy($id);
        $item = CostCenter::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('master_cost.index')->with('success', 'Master Data Cost Center suspended successfully');
    }
}
