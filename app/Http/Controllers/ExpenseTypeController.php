<?php

namespace App\Http\Controllers;

use App\Models\ExpenseType;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ExpenseTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        // $exp_type = ExpenseType::where('status_id', 1);
        $item = $request->input('s') ?? 15;
        
        $exp_type = ExpenseType::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        $exp_type = ExpenseType::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $exp_type = $exp_type->where('status_id', $request->input('status_filter'));
            }else{
                $exp_type = $exp_type->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $exp_type = $exp_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $exp_type = $exp_type->paginate($item);

        $paginate = [
            'expense_type' => $exp_type,
        ];

        return view('master_data.expense_type.index')->with($paginate);
    }

    public function create(): View
    {
        return view('master_data.expense_type.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    public function store(ExpenseType $expenseType): RedirectResponse
    {
        $expenseType->description = \request()->description;
        $expenseType->status_id = \request()->status;
        $expenseType->save();

        return redirect()->route('expense_type.index')->with('flush_success', 'Expense Type added successfully');
    }

    public function edit(ExpenseType $expense_type): View
    {
        $parameters = [
            'expense_type' => $expense_type,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ];

        return view('master_data.expense_type.edit')->with($parameters);
    }

    public function update(ExpenseType $expense_type): RedirectResponse
    {
        $expense_type->description = \request()->description;
        $expense_type->status_id = \request()->status;
        $expense_type->save();

        return redirect()->route('expense_type.show', $expense_type)->with('flash_success', 'Expense type updated successfully');
    }

    public function show(ExpenseType $expense_type): View
    {
        return view('master_data.expense_type.show')->with(['expense_type' => $expense_type]);
    }

    public function destroy($expense_type): RedirectResponse
    {
        // $expense_type->delete();
        $item = ExpenseType::find($expense_type);
        $item->status_id = 2;
        $item->save();

        return redirect()->back()->with('flash_success', 'Expense type suspended successfully');
    }
}
