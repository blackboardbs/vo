<?php

namespace App\Http\Controllers;

use App\API\Clockify\ClockifyAPI;
use App\Enum\Status;
use App\Jobs\MasterServiceAgreementJob;
use App\Jobs\NonDisclosureAgreementJob;
use App\Models\AdvancedTemplates;
use App\Models\BBBEELevel;
use App\Models\Config;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Document;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Module;
use App\Models\ModuleRole;
use App\Models\MedicalCertificateType;
use App\Models\PaymentTerm;
use App\Models\Vendor;
use App\Services\CustomerService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Enums\Unit;
use Spatie\LaravelPdf\Facades\Pdf;
use Spatie\LaravelPdf\PdfBuilder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, CustomerService $service): View|BinaryFileResponse
    {
        $item = $request->input('r') ?? 15;

        $customer = Customer::with(['statusd:id,description'])->withCount(['projects', 'users', 'quotations', 'invoices'])
            ->sortable(['customer_name' => 'asc']);

        $module = Module::where('name', '=', 'App\Models\CustomerProfile')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
            $customer->where('id', '=', Auth::user()->customer_id);
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');

        if ($request->has('q') && $request->input('q') != '') {
            $customer->where('email', 'like', '%'.$request->input('q').'%')
                ->orWhere('phone', 'like', '%'.$request->input('q').'%')
                ->orWhere('cell', 'like', '%'.$request->input('q').'%')
                ->orWhere('customer_name', 'like', '%'.$request->input('q').'%');
        }

        $customer = $customer->paginate($item);

        $parameters = [
            'customer' => $customer,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        if ($request->has('export')) return $this->export($parameters, 'customer');

        return view('customer.index', $parameters);
    }

    public function create(CustomerService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $parameters = [
            'sidebar_process_countries' => Country::orderBy('name')->pluck('name', 'id')->prepend('Country', '0'),
            'medical_certificate_type_dropdown' => MedicalCertificateType::orderBy('description')->pluck('description', 'id')->prepend('Medical Certificate Type', '0'),
            'bbbee_level_dropdown' => BBBEELevel::orderBy('description')->pluck('description', 'id')->prepend('BBBEE Level', '0'),
            'payment_terms_days_dropdown' => PaymentTerm::select('description')->pluck('description', 'description')->prepend('Select Payment Terms', '0'),
            'appointment_manager_dropdown' => $service->managers(),
            'provider_master_service_agreement' => AdvancedTemplates::where('template_type_id', 7)->pluck('name', 'id')->prepend('Select Service Agreement', '0'),
            'nda_templates_dropdown' => AdvancedTemplates::where('template_type_id', 8)->pluck('name', 'id')->prepend('Select NDA Template', '0'),
            'default_service_agreement' => Config::first(['provider_service_agreement_id'])?->provider_service_agreement_id
        ];

        return view('customer.create')->with($parameters);
    }

    public function store(StoreCustomerRequest $request, ClockifyAPI $api, Customer $customer, CustomerService $service): RedirectResponse
    {
        $customer = $service->createCustomer($request, $customer);

        $service->updateClockifyWithCustomer($api, $customer);

        $service->storeDocuments($request, $customer);

        return redirect(route('customer.index'))->with('flash_success', 'Customer captured successfully');
    }

    public function show(Customer $customer, CustomerService $service): View
    {
        abort_unless($service->hasShowPermissions(), 403);

        $parameters = [
            'customer' => $customer->load($service->relationships()),
            'documents' => Document::where('document_type_id', '=', 10)->where('reference_id', '=', $customer->id)->get(),
        ];

        return view('customer.show')->with($parameters);
    }

    public function edit(Customer $customer, CustomerService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $documents = Document::where('document_type_id', '=', 10)->where('reference_id', '=', $customer->id)->get();

        $parameters = [
            'customer' => $customer,
            'documents' => $documents,
            'sidebar_process_countries' => Country::orderBy('name')->pluck('name', 'id')->prepend('Country', '0'),
            'medical_certificate_type_dropdown' => MedicalCertificateType::orderBy('description')->pluck('description', 'id')->prepend('Medical Certificate Type', '0'),
            'bbbee_level_dropdown' => BBBEELevel::orderBy('description')->pluck('description', 'id')->prepend('BBBEE Level', '0'),
            'payment_terms_days_dropdown' => PaymentTerm::select('description')->pluck('description', 'description')->prepend('Select Payment Terms', '0'),
            'appointment_manager_dropdown' => $service->managers(),
            'provider_master_service_agreement' => AdvancedTemplates::where('template_type_id', 7)->pluck('name', 'id')->prepend('Select Service Agreement', '0'),
            'nda_templates_dropdown' => AdvancedTemplates::where('template_type_id', 8)->pluck('name', 'id')->prepend('Select NDA Template', '0'),
        ];

        return view('customer.edit')->with($parameters);
    }

    public function update(UpdateCustomerRequest $request, Customer $customer, CustomerService $service): RedirectResponse
    {
        $service->updateCustomer($request, $customer);

        return redirect(route('customer.index'))->with('flash_success', 'Customer saved successfully');
    }

    public function destroy(Customer $customer, CustomerService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $customer->delete();

        return redirect()->route('customer.index')->with('success', 'Customer deleted successfully');
    }

    public function print(Customer $customer)
    {
        try {

            $template = $customer->mapVariables($customer->serviceAgreement);

            return $this->masterPrint($customer->customer_name, $template);
        }catch (\TypeError $e) {
            logger($e);
            return redirect(route('customer.edit', $customer))->with('flash_danger', 'Edit your customer and select a service agreement.');
        }
    }

    public function printNDA(Customer $customer)
    {
        try {

            $template = $customer->mapVariables($customer->NDATemplate);

            return $this->masterPrint($customer->customer_name, $template);
        }catch (\TypeError $e) {
            logger($e);
            return redirect(route('customer.edit', $customer))->with('flash_danger', 'Edit your customer and select NDA Template.');
        }
    }

    public function send(Customer $customer)
    {
        $template = $customer->mapVariables($customer->serviceAgreement);

        try {
            $storage = $this->masterSend($customer, $template, 'ServiceProvider/templates');

            MasterServiceAgreementJob::dispatch($storage, $customer->email);

            return redirect(route('customer.show', $customer))->with('flash_success', 'Master Service Agreement was sent successfully');

        }catch (\TypeError $e){
            return redirect(route('customer.edit', $customer))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    public function sendNDA(Customer $customer)
    {
        $template = $customer->mapVariables($customer->NDATemplate);

        try {
            $storage = $this->masterSend($customer, $template, 'customer/templates');

            $emails = [$customer->email, auth()->user()->email];

            $data = [
                'emails' => array_values(array_filter(array_unique($emails))),
                'subject' => "NDA for {$customer->customer_name}",
                'body' => "Please find the non disclosure agreement attached.",
                'attachments' => $storage
            ];

            NonDisclosureAgreementJob::dispatch($data);

            return redirect(route('customer.show', $customer))->with('flash_success', 'Master Service Agreement was sent successfully');

        }catch (\TypeError $e){
            return redirect(route('customer.edit', $customer))->with('flash_danger', 'Edit your vendor and select a service agreement.');
        }
    }

    private function masterPrint(string $customerName, string $template): PdfBuilder
    {
        return Pdf::view('template.default-template', ['template' => $template])
            ->headerView('template.header')
            ->footerView('template.footer')
            //->format('a4')
            ->margins(30, 20, 30, 20, Unit::Pixel)
            ->name($customerName . ".pdf");
    }

    public function masterSend(Customer $customer, string $template, string $path): string
    {
        if (! Storage::exists($path)) {
            Storage::makeDirectory($path);
        }

        $filename = "master_service_agreement_client_".str_replace(' ', '_',$customer->customer_name)."_".date('Y_m_d_H_i_s').'.pdf';

        $storage = storage_path("app/{$path}/{$filename}");

        Pdf::view('template.default-template', ['template' => $template])
            ->headerView('template.header')
            ->footerView('template.footer')
            ->format('a4')
            ->margins(30, 20, 30, 20, Unit::Pixel)
            ->save($storage);

        $customer->documents()->create([
            'name' => "Master Service Agreement - Service Provider {$customer->customer_name}",
            'file' => $filename,
            'document_type_id' => 8,
            'creator_id' => auth()->id(),
            'status_id' => Status::ACTIVE->value,
        ]);

        return $storage;
    }
}
