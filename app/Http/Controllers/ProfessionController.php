<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfessionRequest;
use App\Models\Profession;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProfessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        // $professions = Profession::orderBy('id');

        $item = $request->input('s') ?? 15;

        $professions = Profession::with(['status:id,description'])->sortable('name', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $professions = $professions->where('status_id', $request->input('status_filter'));
            }else{
                $professions = $professions->sortable('name', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $professions = $professions->where('name', 'like', '%'.$request->input('q').'%');
        }

        $professions = $professions->paginate($item);

        $parameters = [
            'professions' => $professions,
        ];

        return view('master_data.profession.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $autocomplete_elements = Profession::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $parameters = [
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('master_data.profession.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProfessionRequest $request): RedirectResponse
    {
        $profession = new Profession();
        $profession->name = $request->input('name');
        $profession->creator_id = Auth()->id();
        $profession->status_id = $request->input('status_id');
        $profession->save();

        return redirect(route('profession.index'))->with('flash_success', 'Profession saved successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Profession $profession): View
    {
        $profession = Profession::find($profession->id);
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $parameters = [
            'profession' => $profession,
            'status_drop_down' => $status_drop_down,
        ];

        return view('master_data.profession.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Profession $profession): View
    {
        $autocomplete_elements = Profession::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $profession = Profession::find($profession->id);

        $parameters = [
            'profession' => $profession,
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('master_data.profession.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProfessionRequest $request, Profession $profession): RedirectResponse
    {
        $profession = Profession::find($profession->id);
        $profession->name = $request->input('name');
        $profession->creator_id = Auth()->id();
        $profession->status_id = $request->input('status_id');
        $profession->save();

        return redirect(route('profession.index'))->with('flash_success', 'Profession updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $profession): RedirectResponse
    {
        // Profession::destroy($profession->id);
        $item = Profession::find($profession);
        $item->status_id = 2;
        $item->save();

        return redirect(route('profession.index'))->with('flash_success', 'Profession suspended successfully.');
    }
}
