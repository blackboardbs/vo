<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ViewForReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(): View
    {
        return view('reports.index');
    }

    public function customer(): View
    {
        return view('reports.customer');
    }

    public function manager(): View
    {
        return view('reports.manager');
    }

    public function financial(): View
    {
        return view('reports.financial');
    }

    public function expenses(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 10;

        $timesheet = TimeExp::select('timesheet.employee_id')->leftJoin('timesheet', 'time_exp.timesheet_id', '=', 'timesheet.id')->groupBy('timesheet.employee_id');
        if ($request->has('company') && $request->company) {
            $timesheet = $timesheet->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('team') && $request->team != '') {
            $timesheet = $timesheet->whereHas('timesheet.resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee != '') {
            $timesheet = $timesheet->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('customer') && $request->customer != '') {
            $timesheet = $timesheet->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project != '') {
            $timesheet = $timesheet->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable != '') {
            $timesheet = $timesheet->where('time_exp.is_billable', '=', $request->billable);
        }
        if ($request->has('claimable') && $request->claimable != '') {
            $timesheet = $timesheet->where('time_exp.claim', '=', $request->claimable);
        }
        if ($request->has('payment_status') && ($request->payment_status != '') && ($request->payment_status == 0)) {
            $timesheet = $timesheet->whereNull('time_exp.paid_date');
        } elseif ($request->has('payment_status') && ($request->payment_status != '') && ($request->payment_status == 1)) {
            $timesheet = $timesheet->whereNotNull('time_exp.paid_date');
        }
        if (($request->has('from') && $request->from != '') || ($request->has('to') && $request->to != '')) {
            $timesheet = $timesheet->whereBetween('time_exp.date', [$request->from, $request->to]);
        }else{
            $timesheet = $timesheet->whereBetween('time_exp.date', [now()->subMonths(3)->toDateString(), now()->toDateString()]);
        }
        $timesheet = $timesheet->paginate($item);
        $expenses = [];
        $resources = [];
        foreach ($timesheet as $resource) {
            if ($resource->employee_id != null) {
                $users = User::find($resource->employee_id);
                $expense = TimeExp::with(['timesheet','timesheet.project','account','account_element'])->whereHas('timesheet', function ($query) use ($resource) {
                    $query->where('employee_id', '=', $resource->employee_id);
                });

                if (($request->has('from') && $request->from != '') || ($request->has('to') && $request->to != '')) {
                    $expense = $expense->whereBetween('date', [$request->from, $request->to]);
                }
                if ($request->has('payment_status') && ($request->payment_status != '') && ($request->payment_status == 0)) {
                    $expense = $expense->whereNull('paid_date');
                } elseif ($request->has('payment_status') && ($request->payment_status != '') && ($request->payment_status == 1)) {
                    $expense = $expense->whereNotNull('paid_date');
                }

                $expense = $expense->orderBy('date', 'desc')->paginate(5, ['*'], substr($users->first_name, 0, 4).'_'.substr($users->last_name, 0, 1));

                array_push($expenses, $expense);
                array_push($resources, $users);
            }
        }
// dd($expenses);
        $year_months = Timesheet::select('year', 'month')->orderBy('id', 'desc')->get();

        $conc_year_month = [];

        foreach ($year_months as $year_month) {
            $conc_year_month[$year_month->year.$year_month->month] = ($year_month->month < 10) ? $year_month->year.'0'.$year_month->month : $year_month->year.$year_month->month;
        }

        $parameters = [
            'company_dropdown' => Company::select('company_name', 'id')->orderBy('company_name')->pluck('company_name', 'id'),
            'team_dropdown' => Team::select('team_name', 'id')->orderBy('team_name')->pluck('team_name', 'id'),
            'employee_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([7])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'yearmonth_dropdown' => $conc_year_month,
            'dates_dropdown' => Timesheet::select(DB::raw("CONCAT(first_day_of_week,' - ',COALESCE(`last_day_of_week`,'')) AS dates"), 'id')->orderBy('first_day_of_week', 'desc')->get()->pluck('dates', 'id')->unique(),
            'customer_dropdown' => Customer::orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_dropdown' => Project::orderBy('name')->pluck('name', 'id')->unique(),
            'expenses' => $expenses,
            'resources' => $resources,
        ];
        if ($request->has('export')) return $this->export($parameters, 'reports', 'expenses');
        //return $parameters;
        return view('reports.expenses')->with($parameters);
    }
}
