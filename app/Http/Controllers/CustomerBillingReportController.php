<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\ExpenseTracking;
use App\Models\Project;
use App\Models\Team;
use App\Models\Time;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerBillingReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 15;

        $cust_bill_report = Timesheet::select(DB::raw('
            timesheet.customer_id, timesheet.project_id, timesheet.employee_id, assignment.invoice_rate, assignment.id, assignment.customer_po, assignment.hours,
            SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS bhours,
            SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS nbhours
        '))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            });

        if ($request->has('company') && $request->company != '') {
            $cust_bill_report = $cust_bill_report->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('team') && $request->team != null) {
            $cust_bill_report = $cust_bill_report->whereHas('employee.resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee != null) {
            $cust_bill_report = $cust_bill_report->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $cust_bill_report = $cust_bill_report->where('timesheet.year', '=', substr($request->yearmonth, 0, 4))->where('timesheet.month', '=', (substr($request->yearmonth, 4, 2) < 10) ? substr($request->yearmonth, 5, 1) : substr($request->yearmonth, 4, 2));
        }
        if (($request->has('date_from') && $request->date_from != '') || ($request->has('date_to') && $request->date_to != '')) {
            $cust_bill_report = $cust_bill_report->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })->whereBetween('calendar.date', [$request->date_from, $request->date_to]);
        }
        if ($request->has('customer') && $request->customer != null) {
            $cust_bill_report = $cust_bill_report->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project != null) {
            $cust_bill_report = $cust_bill_report->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable != null) {
            $cust_bill_report = $cust_bill_report->where('timeline.is_billable', '=', $request->billable);
        }

        $cust_bill_report = $cust_bill_report->with(['customer', 'project', 'employee'])
            ->groupBy('timesheet.customer_id')
            ->groupBy('timesheet.project_id')
            ->groupBy('timesheet.employee_id')
            ->groupBy('assignment.invoice_rate')
            ->groupBy('assignment.id')
            ->groupBy('assignment.customer_po')
            ->groupBy('assignment.hours')
            ->orderBy('assignment.id', 'desc')
            ->paginate($item);

        $total_exp = 0;
        $cust_bill_exp = [];
        $cust_bill_exp_tracking = [];
        $cust_claim_exp = [];
        $cust_claim_exp_tracking = [];

        foreach ($cust_bill_report as $cust_bill) {
            $time_exp = TimeExp::select(DB::raw('
                timesheet.customer_id, timesheet.project_id, timesheet.employee_id, time_exp.date, time_exp.description, time_exp.is_billable, time_exp.claim, time_exp.amount
            '))
                ->leftJoin('timesheet', 'time_exp.timesheet_id', '=', 'timesheet.id')
                ->where('timesheet.employee_id', '=', $cust_bill->employee_id)
                ->orderBy('timesheet.employee_id')
                ->orderBy('time_exp.date', 'desc')
                ->get();

            $exp_tracking = ExpenseTracking::where('employee_id', '=', $cust_bill->employee_id)->where('assignment_id', '=', $cust_bill->id)->get();

            $total_bill_time_exp = 0;
            $total_claim_time_exp = 0;
            $total_bill_exp_tracking = 0;
            $total_claim_exp_tracking = 0;

            foreach ($exp_tracking as $exp_track) {
                $total_bill_time_exp += ($exp_track->billable == 1) ? $exp_track->amount : 0;
                $total_claim_time_exp += ($exp_track->claim == 1) ? $exp_track->amount : 0;
            }
            foreach ($time_exp as $exp) {
                $total_bill_exp_tracking += ($exp->billable == 1) ? $exp->amount : 0;
                $total_claim_exp_tracking += ($exp->claim == 1) ? $exp->amount : 0;
            }
            array_push($cust_bill_exp, ($total_bill_time_exp + $total_bill_exp_tracking));
            array_push($cust_bill_exp_tracking, ($total_claim_time_exp + $total_claim_exp_tracking));
        }

        $total_po_hours = 0;
        $total_po_value = 0;
        $total_bill_hours = 0;
        $total_non_bill_hours = 0;
        $total_bill_value = 0;
        $total_outstanding_value = 0;
        foreach ($cust_bill_report as $time) {
            $total_po_hours += $time->hours;
            $total_po_value += ($time->hours * $time->invoice_rate);
            $total_bill_hours += $time->bhours;
            $total_non_bill_hours += $time->nbhours;
            $total_bill_value += ($time->bhours * $time->invoice_rate);
            $total_outstanding_value += (($time->hours - $time->bhours) * $time->invoice_rate);
        }

        $total_bill_exp = 0;
        foreach ($cust_bill_exp as $exp) {
            $total_bill_exp += $exp;
        }
        $total_claim_exp = 0;
        foreach ($cust_bill_exp_tracking as $claim_exp) {
            $total_claim_exp += $claim_exp;
        }
        $calendar = Calendar::select('year', 'month')->whereDate('date', '<=', Carbon::now()->toDateString())->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $yearmonth[null] = 'All';
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'yearmonth_dropdown' => $yearmonth,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'team_drop_down' => Team::filters()->orderBy('team_name')->pluck('team_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'cust_billing_report' => $cust_bill_report,
            'cust_bill_exp' => $cust_bill_exp,
            'total_po_hours' => $total_po_hours,
            'total_po_value' => $total_po_value,
            'total_bill_hours' => $total_bill_hours,
            'total_non_bill_hours' => $total_non_bill_hours,
            'total_bill_value' => $total_bill_value,
            'total_bill_exp' => $total_bill_exp,
            'total_claim_exp' => $total_claim_exp,
            'total_outstanding_hours' => ($total_po_hours - $total_bill_hours),
            'total_outstanding_value' => $total_outstanding_value,
            'cust_bill_exp_tracking' => $cust_bill_exp_tracking,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'customer_billing_report');

        return view('reports.customer_billing_report')->with($parameters);
    }
}
