<?php

namespace App\Http\Controllers;

use App\Models\ContractType;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ContractTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $types = ContractType::sortable('description', 'asc')->where('status_id', 1)->paginate($item);

        $types = ContractType::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $types = $types->where('status_id', $request->input('status_filter'));
            }else{
                $types = $types->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $types = $types->where('description', 'like', '%'.$request->input('q').'%');
        }

        $types = $types->paginate($item);

        return view('master_data.contract_type.index')
            ->with(['contract_types' => $types]);
    }

    public function create(): View
    {
        return view('master_data.contract_type.create')
            ->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    /**
     * TODO: Add input request
     */
    public function store(Request $request): RedirectResponse
    {
        $contract = new ContractType();
        $this->data($request, $contract);

        return redirect()->route('contracttype.index')
            ->with(['flash_success', 'Contract type created successfully']);
    }

    public function show(ContractType $contracttype): View
    {
        return view('master_data.contract_type.show')->with(['contract' => $contracttype]);
    }

    public function edit(ContractType $contracttype): View
    {
        return view('master_data.contract_type.edit')
            ->with(
                [
                    'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
                    'contract_type' => $contracttype,
                ]
            );
    }

    /**
     * TODO: Add input request
     */
    public function update(Request $request, ContractType $contracttype): RedirectResponse
    {
        $this->data($request, $contracttype);

        return redirect()->route('contracttype.index')->with('flash_success', 'Contract type updated successfully');
    }

    public function destroy($contracttype): RedirectResponse
    {
        // $contracttype->delete();
        $item = ContractType::find($contracttype);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('contracttype.index')->with('flash_success', 'Contract type suspended successfully');
    }

    private function data($request, $contract)
    {
        $contract->description = $request->description;
        $contract->status_id = $request->status;
        $contract->save();

        return $contract;
    }
}
