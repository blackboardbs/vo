<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LandingPage;
use App\Models\Log;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use App\Services\StorageQuota;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    $this->middleware('guest')->except('logout');
        $this->redirectTo = route('dashboard.index');
    }

    /**
     * Check either username or email.
     *
     * @return string
     */
    public function username()
    {
        $identity = request()->get('identity');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);

        return $fieldName;
    }

    /**
     * Validate the user login.
     */
    protected function validateLogin(Request $request)
    {
        if (App::environment('local')) {
            $this->validate(
                $request,
                [
                    'identity' => 'required|string',
                    'password' => 'required|string',
                    //'g-recaptcha-response' => 'required|captcha'
                ]
            );
        } else {
            $this->validate(
                $request,
                [
                    'identity' => 'required|string',
                    'password' => 'required|string',
                    //'g-recaptcha-response' => 'required|recaptchav3:captcha,0.5',
                ]
            );
        }
    }

    /**
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $logs = new Log;
        $logs->user_ip = $request->getClientIp();
        $logs->user_name = $request->identity;
        $logs->password = $request->password;
        $logs->date = Carbon::now()->toDateString();
        $logs->login_status = 0;
        $logs->status_id = 1;
        $logs->save();

        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }

    protected function authenticated(Request $request, $user)
    {
        $service = new StorageQuota();
        if ($user->status_id != '1') {
            auth()->logout();

            return back()->with('login_error', 'Your account has been suspended. Please contact the administrator.');
        }

        if(auth()->user()->id == 1){
            if(!auth()->user()->hasAnyRole(['admin'])){
                auth()->user()->assignRole('admin');
            }
        }

        $logs = new Log;
        $logs->user_ip = $request->getClientIp();
        $logs->user_name = $request->identity;
        $logs->date = Carbon::now()->toDateString();
        $logs->login_status = 1;
        $logs->status_id = 1;
        $logs->save();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($user->expiry_date <= now()) {
            $message = 'Your access has expired.';

            // Log the user out.
            $this->logout($request);

            // Return them to the log in form.
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->with(['login_error' => $message]);
        }

        if ($user->status_id != '1') {
            $message = 'Your account is inactive. ';

            // Log the user out.
            $this->logout($request);

            // Return them to the log in form.
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->with(['login_error' => $message]);
        }

        $remember_me = $request->has('remember');

        if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $remember_me))
        {
            $license = [];

                $owner = User::first();
                try {
                    $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);
                    if ($response->status() != 404){

                        $request->session()->put('subscriptionDetail', json_encode($response->json()));
                        $license = $response->json();
                        $license_exp_date = Carbon::parse($license["transactions"][0]["billed_at"])->addMonth(1)->format('Y-m-d');
                        $exp_date = Carbon::parse($license["transactions"][0]["billed_at"])->addMonth(1)->addDays(7)->format('Y-m-d');
                        $today = now()->format('Y-m-d');

                        // if($license_exp_date == $today){
                            // $request->session()->put('subscriptionExpDays', Carbon::parse($today)->diffInDays(Carbon::parse($exp_date)));
                        // }
                        // if(Carbon::parse($today)->diffInDays(Carbon::parse($exp_date)) > 7 && Carbon::parse($today)->addMonth(1) <= Carbon::parse($license_exp_date) ){
                        //     Auth::guard('web')->logout();
                        //     return back()->with('license_error', 'Your license expired, contact your administrator to renew your license.');
                        // }

                        // if(Carbon::parse($today)->diffInDays(Carbon::parse($exp_date)) <= 7 && Carbon::parse($today)->addMonth(1) <= Carbon::parse($license_exp_date) ){
                        //     $request->session()->put('subscriptionExp','Your license expired, contact your administrator to renew your license before '.$exp_date.'.');
                        // }
                
                if($service->isQuotaPercentage()){
                    $request->session()->put('subscriptionQuotaPercentage', 'You are close to reaching your storage quota.<br /><br />Please consider upgrading your subscription or contacting <a href="mailto:support@consulteaze.com">support</a>');
                }
                if($service->isStorageQuotaReached()){
                    $request->session()->put('subscriptionQuotaPercentage', 'You have exceeded your storage quota.<br /><br />Please consider upgrading your subscription or contacting <a href="mailto:support@consulteaze.com">support</a>');
                }}

                } catch (\Exception $e) {
                    logger($e);
                }
// dd($request->session()->get('subscriptionDetail'));x
                        if(!isset($license["transactions"]) || count($license["transactions"]) < 1){
                            // Auth::guard('web')->logout();
                            // return back()->with('license_error', "You don't have a subscription.<br />Please visit <a href='https://subscription.consulteaze.com'>https://subscription.consulteaze.com/</a> and subscribe.");
                        }
             

                if(strpos($_SERVER['SERVER_NAME'], "tryme") !== false || strpos($_SERVER['SERVER_NAME'], "127.0.0.1") !== false || strpos($_SERVER['SERVER_NAME'], "localhost") !== false){
                // } else {
                    $request->session()->put('tryMeInstance', 'isTryMe');
                }
            if(auth()->user()->hasAnyRole(['admin'])){
                if(auth()->user()->new_setup == 1){
                    return redirect()->route('setup');
                }
            }

            if (session()->has('url_intended')) {
                $redirectURL = session()->get('url_intended');
                // session()->forget('url_intended');
                return redirect($redirectURL);
            }

            $landing_page = LandingPage::where('user_id', auth()->id())->latest()->first();

            //i couldn't find the seeder that was inserting app.ofsy
            if(!isset($landing_page) || $landing_page->page == 'https://app.ofsy.co.za/dashboard' || $landing_page->page == 'https://app.consulteaze.com/dashboard'){
                $landing = '/dashboard';
            } else {
                $landing = $landing_page->page;
            }

            return redirect()->intended($landing??'/dashboard');
        } else {
            return back()->with('error', 'your username and password are wrong.');
        }
    }

    public function logout(Request $request) 
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return response()->json(['message' => 'successful']);
    }
}
