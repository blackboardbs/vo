<?php

namespace App\Http\Controllers;

use App\Exports\CustomersExport;
use App\Exports\QuotationsExport;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Expense;
use App\Http\Requests\StoreQuotationRequest;
use App\Http\Requests\UpdateQuotationRequest;
use App\Models\Module;
use App\Models\ProjectTerms;
use App\Models\Quotation;
use App\Models\Status;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class QuotationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $quotations = Quotation::with([
            'customer:id,customer_name',
            'consultant:id,first_name,last_name',
            'status:id,description'
        ])->sortable(['company_name' => 'asc']);

        $module = Module::where('name', '=', \App\Models\Quotation::class)->get();

        abort_unless(Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
            $quotations = $quotations->whereIn('consultant_emp_id', Auth::user()->team());
        }

        if ($request->has('q') && $request->input('q') != '') {
            $quotations->orWhere('start_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('customer_ref', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('customer', function ($query) use ($request) {
                    $query->where('customer_name', 'like', '%'.$request->input('q').'%');
                })
                ->orWhereHas('consultant', function ($query) use ($request) {
                    $query->where('first_name', 'like', '%'.$request->input('q').'%');
                    $query->orWhere('last_name', 'like', '%'.$request->input('q').'%');
                })
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                })
                ->sortable(['company_name' => 'asc'])->paginate($item);
        }

        $quotations = $quotations->paginate($item);

        $parameters = [
            'quotations' => $quotations,
        ];

        if ($request->has('export')) return $this->export($parameters, 'quotation');

        return view('quotation.index', $parameters);
    }

    public function create(): View
    {
        $quote_nr = Quotation::max('id');
        $parameters = [
            'default_project_terms' => ProjectTerms::find(Config::first()->project_terms_id),
            'terms_drop_down' => ProjectTerms::orderBy('terms_version')->pluck('terms_version', 'id'),
            'quote_nr' => $quote_nr == null ? 1 : $quote_nr + 1,
            'customer_drop_down' => Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Customer', '0'),
            'status_drop_down' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'consultant_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Acount Manager', '0'),
        ];
        //return $parameters;
        return view('quotation.create')->with($parameters);
    }

    public function store(StoreQuotationRequest $request): RedirectResponse
    {
        $quotation = new Quotation;
        $quotation->customer_id = $request->input('customer_id');
        $quotation->customer_ref = $request->input('customer_ref');
        $quotation->consultant_emp_id = $request->input('consultant_emp_id');
        $quotation->hour_rate = $request->input('hour_rate');
        $quotation->duration = $request->input('duration');
        $quotation->amount = $request->input('amount');
        $quotation->start_date = $request->input('start_date');
        $quotation->end_date = $request->input('end_date');
        $quotation->scope_of_work = $request->input('scope_of_work');
        $quotation->status_id = $request->input('status_id');

        $expense = new Expense();
        $expense->travel = $request->input('travel');
        $expense->parking = $request->input('parking');
        $expense->car_rental = $request->input('car_rental');
        $expense->flights = $request->input('flights');
        $expense->other = $request->input('other');
        $expense->accommodation = $request->input('accommodation');
        $expense->out_of_town = $request->input('out_of_town');
        $expense->toll = $request->input('toll');
        $expense->data = $request->input('data');
        $expense->save();

        $quotation->exp_id = $expense->id;
        $quotation->save();

        return redirect(route('quotation.index'))->with('flash_success', 'Quotation captured successfully');
    }

    public function show($quotationid): View
    {
        $parameters = [
            'quotation' => Quotation::where('id', '=', $quotationid)->get(),
        ];

        return view('quotation.show')->with($parameters);
    }

    public function edit($quotationid): View
    {
        $quotation = Quotation::find($quotationid);
        $parameters = [
            'default_project_terms' => ProjectTerms::find(Config::first()->project_terms_id),
            'quotation' => $quotation,
            'terms_drop_down' => ProjectTerms::orderBy('terms_version')->pluck('terms_version', 'id'),
            'customer_drop_down' => Customer::orderBy('customer_name')->pluck('customer_name', 'id')->prepend('Customer', '0'),
            'expense' => Expense::find($quotation->exp_id),
            'status_drop_down' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'consultant_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Acount Manager', '0'),
        ];

        return view('quotation.edit')->with($parameters);
    }

    public function update(UpdateQuotationRequest $request, $quotationid): RedirectResponse
    {
        $quotation = Quotation::find($quotationid);
        $quotation->customer_id = $request->input('customer_id');
        $quotation->customer_ref = $request->input('customer_ref');
        $quotation->consultant_emp_id = $request->input('consultant_emp_id');
        $quotation->hour_rate = $request->input('hour_rate');
        $quotation->duration = $request->input('duration');
        $quotation->amount = $request->input('amount');
        $quotation->start_date = $request->input('start_date');
        $quotation->scope_of_work = $request->input('scope_of_work');
        $quotation->status_id = $request->input('status_id');
        $quotation->save();

        $expense = Expense::find($quotation->exp_id) ?? new Expense();
        $expense->travel = $request->input('travel');
        $expense->parking = $request->input('parking');
        $expense->car_rental = $request->input('car_rental');
        $expense->flights = $request->input('flights');
        $expense->other = $request->input('other');
        $expense->accommodation = $request->input('accommodation');
        $expense->out_of_town = $request->input('out_of_town');
        $expense->toll = $request->input('toll');
        $expense->data = $request->input('data');
        $expense->save();

        return redirect(route('quotation.index'))->with('flash_success', 'Quotation updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        DB::table('quotation')->delete($id);
        Quotation::destroy($id);

        return redirect()->route('quotation.index')->with('flash_success', 'quotation deleted successfully');
    }
}
