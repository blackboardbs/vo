<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\Task;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CustomerReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $date = Carbon::now();

        $cust_summary = Timesheet::with(['customer','project','user'])->selectRaw('timesheet.customer_id, timesheet.project_id, timesheet.year, timesheet.month, timesheet.employee_id,
            SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS billable_hours,
            SUM(CASE WHEN timeline.is_billable = 0 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS non_billable_hours,
            assignment.invoice_rate, assignment.hours')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            });
        if ($request->has('company') && $request->company) {
            $cust_summary = $cust_summary->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('team') && $request->team != null) {
            $cust_summary = $cust_summary->whereHas('employee.resource', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee) {
            $cust_summary = $cust_summary->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $cust_summary = $cust_summary->where('timesheet.year', '=', substr($request->yearmonth, 0, 4))->where('timesheet.month', '=', substr($request->yearmonth, 4));
        }
        if (($request->has('date_from') && $request->date_from != '') || ($request->has('date_to') && $request->date_to != '')) {
            $cust_summary = $cust_summary->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })->whereBetween('calendar.date', [$request->date_from, $request->date_to]);
        }elseif (!$request->yearmonth){
            $cust_summary = $cust_summary->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })->whereBetween('calendar.date', [now()->subMonths(3)->toDateString(), now()->toDateString()]);
        }
        if ($request->has('customer') && $request->customer) {
            $cust_summary = $cust_summary->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project) {
            $cust_summary = $cust_summary->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable) {
            $cust_summary = $cust_summary->where('timeline.is_billable', '=', $request->billable);
        }
        $cust_summary = $cust_summary->groupBy('timesheet.customer_id', 'timesheet.project_id', 'timesheet.year', 'timesheet.month', 'assignment.invoice_rate', 'assignment.hours', 'timesheet.employee_id')
            ->orderBy('timesheet.year')
            ->orderBy('timesheet.month')
            ->get();
        $customer_name = [];
        $project_name = [];
        $customer_summary = [];
        foreach ($cust_summary as $summary) {
            $customer_name[$summary->customer_id] = /*isset($summary->customer)?*/$summary->customer?->customer_name/*:''*/;
            $project_name[$summary->project_id] = [
                'customer_id' => $summary->customer_id,
                'project' => isset($summary->project) ? $summary->project?->name : '',
            ];

            $po_hours = Assignment::where('project_id', '=', $summary->project_id)->sum('hours');
            array_push($customer_summary, [
                'project_id' => $summary->project_id,
                'period' => $summary->year.'/'.(($summary->month < 10) ? '0'.$summary->month : $summary->month),
                'customer_id' => $summary->customer_id,
                'billable_hours' => $summary->billable_hours,
                'non_billable_hours' => $summary->non_billable_hours,
                'po_hours' => $po_hours,
                'po_value' => $po_hours * $summary->invoice_rate,
                'invoice_rate' => $summary->invoice_rate,
                'outstanding_hours' => $po_hours - $summary->billable_hours,
                'outstanding_value' => ($po_hours * $summary->invoice_rate) - ($summary->billable_hours * $summary->invoice_rate),
                'completion_perc' => ($summary->billable_hours != 0 && $po_hours != 0) ? ($summary->billable_hours / $po_hours) * 100 : 0,
                'resource' => $summary->user?->name(),
            ]);
        }

        $calendar = Calendar::where('date', '<=', $date->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $year_month = [];
        foreach ($calendar as $cal) {
            $year_month[$cal->year.$cal->month] = $cal->year.(($cal->month < 10) ? '0'.$cal->month : $cal->month);
        }

        $parameters = [
            'cust_summary' => $cust_summary,
            'customer_name' => $customer_name,
            'project_name' => $project_name,
            'customer_summary' => $customer_summary,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'team_drop_down' => Team::filters()->orderBy('team_name')->pluck('team_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'task_dropdown' => Task::filters()->orderBy('description')->pluck('description', 'id'),
            'yearmonth_dropdown' => $year_month,
        ];

        return view('reports.customer_report')->with($parameters);
    }
}
