<?php

namespace App\Http\Controllers;

use App\Models\AssessmentHeader;
use App\Models\AssessmentMaster;
use App\Http\Requests\StoreAssessmentKIPRequest;
use App\Http\Requests\UpdateAssessmentKIPRequest;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentMasterController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $assessment = AssessmentMaster::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $assessment = AssessmentMaster::with(['status:id,description'])->where('status_id', $request->input('status_filter'));
            }else{
                $assessment = AssessmentMaster::with(['status:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $assessment = $assessment->where('description', 'like', '%'.$request->input('q').'%');
        }

        $assessment = $assessment->paginate($item);
        $parameters = [
            'assessments' => $assessment,
        ];

        return view('master_data.assessment.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = AssessmentMaster::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.assessment.create')->with($parameters);
    }

    public function store(StoreAssessmentKIPRequest $request): RedirectResponse
    {
        $assessment = new AssessmentMaster();
        $assessment->description = $request->description;
        $assessment->status_id = $request->status;
        $assessment->save();

        return redirect(route('assessment_master.index'))->with('flash_success', 'Assessment captured successfully');
    }

    public function show($id): View
    {
        $parameters = [
            'assessment' => AssessmentMaster::find($id),
        ];

        return view('master_data.assessment.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = AssessmentMaster::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'assessment' => AssessmentMaster::find($id),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.assessment.edit')->with($parameters);
    }

    public function update(UpdateAssessmentKIPRequest $request, $id): RedirectResponse
    {
        $assessment = AssessmentMaster::find($id);
        $assessment->description = $request->description;
        $assessment->status_id = $request->status;
        $assessment->save();

        return redirect(route('assessment_master.index'))->with('flash_success', 'Assessment updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // AssessmentMaster::destroy($id);
        $item = AssessmentMaster::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('assessment_master.index'))->with('flash_success', 'Assessment deleted successfully');
    }
}
