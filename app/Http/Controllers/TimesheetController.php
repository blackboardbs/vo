<?php

namespace App\Http\Controllers;

use App\Enum\TimesheetTemplatesEnum;
use App\Models\Account;
use App\Models\AccountElement;
use App\Models\ApprovalRules;
use App\Models\Assignment;
use App\Models\BillingCycle;
use App\Models\BillingPeriod;
use App\Models\BillingPeriodStyle;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Config;
use App\Models\Customer;
use App\Models\DigisignUsers;
use App\Models\Document;
use App\Models\Epic;
use App\Http\Requests\StoreTimeLineRequest;
use App\Http\Requests\StoreTimeSheetExpenseRequest;
use App\Http\Requests\StoreTimeSheetRequest;
use App\Http\Requests\TimeSheetAttachmentRequest;
use App\Http\Requests\UpdateTimeLineRequest;
use App\Http\Requests\UpdateTimeSheetExpenseRequest;
use App\Http\Requests\UpdateTimeSheetRequest;
use App\Models\InvoiceStatus;
use App\Jobs\SendAssignmentMaxHoursExceededEmailJob;
use App\Jobs\SendTimesheetDigiSignEmailJob;
use App\Jobs\SendUploadLogoEmailJob;
use App\Models\Module;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Resource;
use App\Models\RoleUser;
use App\Models\Status;
use App\Models\Task;
use App\Models\TaskDeliveryType;
use App\Models\TaskStatus;
use App\Models\Time;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Timesheet\PreviousTimesheet;
use App\Timesheet\TimesheetHelperTrait;
use App\Timesheet\TimesheetTemplate;
use App\Models\User;
use App\Models\UserNotification;
use App\Utils;
use App\Models\VendorInvoiceStatus;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Spatie\LaravelPdf\Facades\Pdf;

class TimesheetController extends Controller
{
    use TimesheetHelperTrait;

    private $utils;

    public function __construct()
    {
        $this->middleware('auth');
        $this->utils = new Utils();
    }

    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $timesheets = Timesheet::with(['customer:id,customer_name', 'project:id,name', 'bill_statusd:id,description', 'vendor_inv_status:id,description'])
            ->select(DB::raw("timesheet.id, timesheet.employee_id, timesheet.customer_id, timesheet.first_day_of_week, timesheet.project_id, timesheet.bill_status, timesheet.year_week, timesheet.timesheet_lock, timesheet.vendor_invoice_status,timesheet.vendor_invoice_ref,invoice_note,
            SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS hours_bill,
            SUM(CASE WHEN timeline.is_billable = 0 THEN (timeline.total + ROUND((timeline.total_m/60),2)) ELSE 0 END) AS hours_no_bill,
            SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total * 60 + timeline.total_m) ELSE 0 END) AS minutes_bill,
            SUM(CASE WHEN timeline.is_billable = 0 THEN (timeline.total * 60 + timeline.total_m) ELSE 0 END) AS minutes_no_bill,
            sum(timeline.total_m) as total_minutes,
            sum(timeline.total) as total_hours,
                concat(users.last_name,', ', users.first_name) as resource,
            timesheet.exp_paid, timesheet.inv_ref, timesheet.cust_inv_ref,timesheet.status_id"))
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('users', 'timesheet.employee_id', '=', 'users.id')
            ->with(['time_exp' => function ($query) {
                $query->selectRaw('timesheet_id, SUM(CASE WHEN claim = 1 THEN amount ELSE 0 END) AS amount, 
                                    group_concat(paid_date) AS paid_date, group_concat(claim_auth_date) AS claim_auth_date')
                    ->groupBy('timesheet_id');
            }]);

        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {

                $_projects = Project::select('id', 'consultants')->orderBy('name')->get()->toArray();
                $project_ids = [];

                foreach ($_projects as $_project) {
                    $ids = explode(',', $_project['consultants']);
                    $_team_ids = Auth::user()->team();
                    foreach ($_team_ids as $_team_id) {
                        if (in_array($_team_id, $ids)) {
                            $project_ids[] = $_project['id'];
                            break;
                        }
                    }
                }

                $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->where('name', '!=', '')->whereIn('id', $project_ids)->pluck('name', 'id')->prepend('All', '-1');

                $user_company_ids = User::select('company_id')->excludes([6, 7, 8, 9])->whereIn('id', Auth::user()->team()->toArray())->pluck('company_id')->toArray();
                $user_customer_ids = Timesheet::select('customer_id')->whereIn('employee_id', Auth::user()->team()->toArray())->get()->toArray();
                $company_drop_down = Company::orderBy('company_name')->whereNotNull('company_name')->where('company_name', '!=', '')->whereIn('id', $user_company_ids)->pluck('company_name', 'id')->prepend('All', '-1');
                $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->whereIn('id', $user_customer_ids)->pluck('customer_name', 'id')->prepend('All', '-1');
                $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8, 9])->whereIn('id', Auth::user()->team()->toArray())->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '-1');
                $team_ids = Auth::user()->team();
                $timesheets->where(function ($query) use ($team_ids) {
                    $query->whereIn('timesheet.employee_id', $team_ids);
                });
            }
        } else {
            return abort(403);
        }

        if (($request->has('s') && $request->input('s') != '')) {

            if ($request->has('project_status') && $request->input('project_status') != 0) {
                if ($request->input('project_status') == 1) {
                    $timesheets->whereHas('project', function ($query) {
                        $query->where('status_id', '=', 1)->orWhere('status_id', '=', 2)->orWhere('status_id', '=', 3);
                    });
                }
                if ($request->input('project_status') == 2) {
                    $timesheets->whereHas('project', function ($query) {
                        $query->where('status_id', '=', 4)->orWhere('status_id', '=', 5);
                    });
                }
            }

            if ($request->has('project') && $request->input('project') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.project_id', '=', $request->input('project'));
                });
            }

            if ($request->has('employee') && $request->input('employee') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.employee_id', '=', $request->input('employee'));
                });
            }

            if ($request->has('company') && $request->input('company') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.company_id', '=', $request->input('company'));
                });
            }

            if ($request->has('cir') && $request->input('cir') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.cust_inv_ref', '=', $request->input('cir'));
                });
            }
            if ($request->has('vir') && $request->input('vir') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.vendor_invoice_ref', '=', $request->input('vir'));
                });
            }

            if ($request->has('customer') && $request->input('customer') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.customer_id', '=', $request->input('customer'));
                });
            }

            if ($request->has('wf') && $request->input('wf') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.year_week', '>=', $request->input('wf'));
                });
            }

            if ($request->has('wt') && $request->input('wt') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.year_week', '<=', $request->input('wt'));
                });
            }

            if ($request->has('year_week')) {
                $timesheets = $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.year_week', $request->input('year_week'));
                });
            }

            if ($request->has('cis') && $request->input('cis') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.bill_status', '=', $request->input('cis'));
                });
            }
            if ($request->has('vis') && $request->input('vis') != -1) {
                if($request->vis == 4){
                    $timesheets->whereNull('timesheet.vendor_invoice_status');
                } else {
                    $timesheets->whereNotNull('vendor_invoice_ref')->where(function ($query) use ($request) {
                        $query->where('timesheet.vendor_invoice_status', '=', $request->input('vis'));
                    });
                }
            }

            $timesheets = $timesheets
                ->groupBy('timesheet.id')
                ->groupBy('timesheet.employee_id')
                ->groupBy('timesheet.project_id')
                ->groupBy('timesheet.bill_status')
                ->groupBy('timesheet.exp_paid')
                ->groupBy('timesheet.inv_ref')
                ->groupBy('timesheet.cust_inv_ref')
                ->groupBy('timesheet.year_week')
                ->groupBy('timesheet.status_id')
                ->groupBy('timesheet.timesheet_lock')
                ->groupBy('timesheet.vendor_invoice_status')
                ->groupBy('timesheet.vendor_invoice_ref')
                ->groupBy('timesheet.first_day_of_week')
                ->groupBy('timesheet.customer_id')
                ->groupBy('timesheet.invoice_note')
                ->groupBy('users.first_name')
                ->groupBy('users.last_name')
                ->sortable(['id' => 'desc'])
                ->paginate($item);
        } else {

            if ($request->has('project_status') && $request->input('project_status') != 0) {
                if ($request->input('project_status') == 1) {
                    $timesheets->whereHas('project', function ($query) {
                        $query->where('status_id', '=', 1)->orWhere('status_id', '=', 2)->orWhere('status_id', '=', 3);
                    });
                }
                if ($request->input('project_status') == 2) {
                    $timesheets->whereHas('project', function ($query) {
                        $query->where('status_id', '=', 4)->orWhere('status_id', '=', 5);
                    });
                }
            }

            if ($request->has('company') && $request->input('company') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.company_id', '=', $request->input('company'));
                });
            }

            if ($request->has('customer') && $request->input('customer') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.customer_id', '=', $request->input('customer'));
                });
            }

            if ($request->has('project') && $request->input('project') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.project_id', '=', $request->input('project'));
                });
            }

            if ($request->has('project_status') && $request->input('project_status') != 0) {
                if($request->input('project_status') == 1){
                    $timesheets->where(function ($query) use ($request) {
                        $query->where('timesheet.project.status_id', '=', 1);
                    });
                }
                if($request->input('project_status') == 2){
                    $timesheets->where(function ($query) use ($request) {
                        $query->where('timesheet.project.status_id', '!=', 1);
                    });
                }
            }

            if ($request->has('employee') && $request->input('employee') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.employee_id', '=', $request->input('employee'));
                });
            }

            if ($request->has('wf') && $request->input('wf') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.year_week', '>=', $request->input('wf'));
                });
            }

            if ($request->has('wt') && $request->input('wt') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.year_week', '<=', $request->input('wt'));
                });
            }

            if ($request->has('cis') && $request->input('cis') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.bill_status', '=', $request->input('cis'));
                });
            }
            if ($request->has('vis') && $request->input('vis') != -1) {
                if($request->vis == 4){
                    $timesheets->whereNull('timesheet.vendor_invoice_status');
                } else {
                    $timesheets->whereNotNull('vendor_invoice_ref')->where(function ($query) use ($request) {
                        $query->where('timesheet.vendor_invoice_status', '=', $request->input('vis'));
                    });
                }
            }

            if ($request->has('cir') && $request->input('cir') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.cust_inv_ref', '=', $request->input('cir'));
                });
            }
            if ($request->has('vir') && $request->input('vir') != -1) {
                $timesheets->where(function ($query) use ($request) {
                    $query->where('timesheet.vendor_invoice_ref', '=', $request->input('vir'));
                });
            }

            $timesheets = $timesheets
                ->groupBy('timesheet.id')
                ->groupBy('timesheet.employee_id')
                ->groupBy('timesheet.project_id')
                ->groupBy('timesheet.bill_status')
                ->groupBy('timesheet.exp_paid')
                ->groupBy('timesheet.inv_ref')
                ->groupBy('timesheet.cust_inv_ref')
                ->groupBy('timesheet.year_week')
                ->groupBy('timesheet.status_id')
                ->groupBy('timesheet.timesheet_lock')
                ->groupBy('timesheet.vendor_invoice_status')
                ->groupBy('timesheet.vendor_invoice_ref')
                ->groupBy('timesheet.first_day_of_week')
                ->groupBy('timesheet.customer_id')
                ->groupBy('timesheet.invoice_note')
                ->groupBy('users.first_name')
                ->groupBy('users.last_name')
                ->sortable(['id' => 'desc'])
                ->paginate($item);
        }

        if ($request->ajax()) {
            return response()->json(['timesheets' => $timesheets]);
        }

        $time_expenses = [];
        $total_billable_hours = 0;
        $total_non_billable_hours = 0;
        $total_hours = 0;
        $total_claim = 0;
        $invoice_amounts = [];
        $have_timelines = [];
        $customers = [];
        foreach ($timesheets as $timesheet) {
            $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->first();
            $have_timelines[$timesheet->id] = Timeline::where('timesheet_id', $timesheet->id)->get()->count();
            $invoice_amount = 0;
            if (isset($assignment->invoice_rate)) {
                try {
                    $invoice_amount = $assignment->invoice_rate * $timesheet->total_hours + ($timesheet->total_minutes / 60);
                } catch (ErrorException $e) {
                }
            }

            $customers[$timesheet->id] = Customer::find($timesheet->customer_id);
            $invoice_amounts[$timesheet->id] = $invoice_amount;
            $total_billable_hours += $timesheet->minutes_bill;
            $total_non_billable_hours += $timesheet->minutes_no_bill;
            $total_hours += $timesheet->total_hours * 60 + $timesheet->total_minutes;
        }

        //return $time_expenses;
        $date = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $project_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->filters()->pluck('name', 'id')->prepend('All', '-1');
        $company_drop_down = Company::orderBy('company_name')->whereNotNull('company_name')->where('company_name', '!=', '')->filters()->pluck('company_name', 'id')->prepend('All', '-1');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->filters()->pluck('customer_name', 'id')->prepend('All', '-1');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8, 9])->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', '-1');
        $week_drop_down = TimeSheet::orderBy('year_week', 'desc')->whereNotNull('year_week')->filters()->pluck('year_week', 'year_week')->prepend('All', '-1');
        $vendor_inv_ref_drop_down = Timesheet::orderBy('vendor_invoice_ref', 'desc')->whereNotNull('vendor_invoice_ref')->filters()->pluck('vendor_invoice_ref', 'vendor_invoice_ref')->prepend('All', '-1');
        $cust_inv_ref_drop_down = Timesheet::orderBy('cust_inv_ref', 'desc')->filters()->whereNotNull('cust_inv_ref')->pluck('cust_inv_ref', 'cust_inv_ref')->prepend('All', '-1');
        $is_drop_down = InvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', '-1');
        $vis_drop_down = VendorInvoiceStatus::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->put('4','New')->prepend('All', '-1');

        $parameters = [
            'invoice_amounts' => $invoice_amounts,
            'timesheets' => $timesheets,
            'project_drop_down' => $project_drop_down,
            'company_drop_down' => $company_drop_down,
            'employee_drop_down' => $resource_drop_down,
            'vendor_inv_ref_drop_down' => $vendor_inv_ref_drop_down,
            'cust_inv_ref_drop_down' => $cust_inv_ref_drop_down,
            'week_from_drop_down' => $week_drop_down,
            'week_to_drop_down' => $week_drop_down,
            // 'status_drop_down' => Status::orderBy('description')->whereNotNull('description')->pluck('description', 'id')->prepend('All', '-1'),
            'is_drop_down' => $is_drop_down,
            'vis_drop_down' => $vis_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'total_billable_hours' => $total_billable_hours,
            'total_non_billable_hours' => $total_non_billable_hours,
            'total_hours' => $total_hours,
            'total_claim' => 0,
            'have_timelines' => $have_timelines,
            'customers' => $customers,
            'invoice_amount_total' => 0
        ];

        if ($request->has('export')) return $this->export($parameters, 'timesheet');

        return view('timesheet.index')->with($parameters);
    }

    public function create()
    {
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        $flag = false;

        if (Auth::user()->canAccess($module[0]->id, 'create_all')) {
            $flag = true;
        }

        if (Auth::user()->canAccess($module[0]->id, 'create_team')) {
            $flag = true;
        }

        if ($flag == false) {
            return abort(403);
        }

        $employee_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'users.id')
            ->join('resource', 'users.id', '=', 'resource.user_id')->where('users.id', '=', Auth::id())
            ->where('resource.status_id', '=', 1)->orderBy('first_name')->orderBy('last_name')
            ->pluck('full_name', 'id')->toArray();

        $assignments = Assignment::with(['project' => function ($project) {
            $project->select('id', 'name', 'customer_id');
        }, 'project.customer' => function ($customer) {
            $customer->select('id', 'customer_name');
        }])->select('employee_id', 'project_id', 'end_date')
            ->where('assignment_status', 3)
            ->groupBy(['employee_id', 'project_id', 'end_date']);

        if (Auth::user()->hasAnyRole(['admin', 'admin_manager', 'manager'])) {
            $employees = $assignments->with(['resource' => function ($query) {
                $query->select('first_name', 'last_name', 'id')->orderBy('first_name')->orderBy('last_name');
            }])->get();
            foreach ($employees as $resource) {
                $employee_drop_down[$resource->employee_id] = (isset($resource->resource) ? $resource->resource->first_name.' '.$resource->resource->last_name : null);
            }
        }

        asort($employee_drop_down);
        // dd($employee_drop_down);
        $parameters = [
            'employee' => isset($employee_drop_down) ? $employee_drop_down : [],
            'creator_id' => auth()->user()->id,
        ];

        return view('timesheet.create')->with($parameters);
    }

    public function store(StoreTimeSheetRequest $request)
    {
        $project = Project::find($request->project_id);

        $timesheet = new Timesheet;
        $timesheet->employee_id = $request->input('employee_id');
        $timesheet->company_id = $project->company_id ?? Company::first()->id;
        $timesheet->project_id = $request->input('project_id');
        $timesheet->customer_id = $project->customer_id ?? 0;

        $year_week_arr = explode('_', $request->input('year_week'));

        $timesheet->year_week = $year_week_arr[0];

        $year = substr($timesheet->year_week, 0, 4);
        $week = substr($timesheet->year_week, -2);

        $year = (int) $year;
        $week = (int) $week;

        $timesheet->year = $year;
        if ($request->input('year_week') == '202053_2') {
            $timesheet->year = 2021;
        }

        $billing_cycle = $this->billingCycle($project, Config::first());

        if($request->input('year_week') == 202501){
            $date = Carbon::createFromFormat('Y-m-d', '2025-01-01')->startOfDay();
        } else {
            $date = now()->setISODate($year, $week);
        }

        $billing_period = $this->getBillingPeriod($billing_cycle, $date, $year_week_arr[1] ?? 0);
    
                //  \Illuminate\Support\Facades\Log::info($year.' '.$week);
                //  \Illuminate\Support\Facades\Log::info($date);
                //  \Illuminate\Support\Facades\Log::info($billing_period);
                //  \Illuminate\Support\Facades\Log::info($year_week_arr[1]);
        $timesheet->week = $week;
        $timesheet->month = $date->month;
        $timesheet->billing_period_id = $billing_period->id;
        $timesheet->first_day_of_week = $timesheet->year_week == 202453 ? $date->copy()->startOfWeek() : $date->copy()->startOfWeek();
        $timesheet->last_day_of_week = $timesheet->year_week == 202453 ?   $date->copy()->endOfMonth() : $date->copy()->endOfWeek();
        $timesheet->first_day_of_month = $billing_period->start_date ?? $date->copy()->startOfMonth();
        $timesheet->last_day_of_month = $billing_period->end_date ?? $date->copy()->endOfMonth();
        $timesheet->first_date_of_week = $timesheet->year_week == 202453 ? $date->copy()->startOfWeek()->format('d') : $date->copy()->startOfWeek()->format('d');
        $timesheet->last_date_of_week = $timesheet->year_week == 202453 ? $date->copy()->endOfMonth()->format('d') : $date->copy()->endOfWeek()->format('d');
        $timesheet->first_date_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : $date->copy()->startOfMonth()->format('d');
        $timesheet->last_date_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date)->format('d') : $date->copy()->endOfMonth()->format('d');
        $timesheet->creator_id = auth()->id();
        $timesheet->status_id = 1;

        if (count($year_week_arr) == 2) {
            if ($year_week_arr[1] == 1) {
                $timesheet->last_date_of_week = $timesheet->last_date_of_month;
                $timesheet->last_day_of_week = $timesheet->last_day_of_month;
                $timesheet->last_day_of_week = $timesheet->last_day_of_month;
            }

            if ($year_week_arr[1] == 2) {
                $temp_last_date_of_month_2 = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
                $timesheet->first_date_of_week = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : 1;
                $timesheet->month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->month : $date->copy()->month;
                $timesheet->first_day_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date) : $date->copy()->addWeek()->startOfMonth();
                $timesheet->last_day_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
                $timesheet->last_date_of_month = $temp_last_date_of_month_2->format('d');
                $timesheet->first_day_of_week = $billing_period->start_date;
            }
        }

        $timesheet->save();

        activity()->on($timesheet)->log('created');

        return response()->json(['timesheet_id' => $timesheet->id]);
    }

    public function copy($timesheet_id)
    {
        $timesheet_month_end = Timesheet::find($timesheet_id);
        $timesheet_month_start = new Timesheet;
        $timesheet_month_start->company_id = $timesheet_month_end->company_id;
        $timesheet_month_start->employee_id = $timesheet_month_end->employee_id;
        $timesheet_month_start->customer_id = $timesheet_month_end->customer_id;
        $timesheet_month_start->project_id = $timesheet_month_end->project_id;
        $timesheet_month_start->year_week = $timesheet_month_end->year_week;
        $timesheet_month_start->billing_period_id = $timesheet_month_end->billing_period_id;
        $timesheet_month_start->year = $timesheet_month_end->year;
        $timesheet_month_start->month = $timesheet_month_end->month < 12 ? $timesheet_month_end->month + 1 : 1;
        $timesheet_month_start->week = $timesheet_month_end->week;
        $timesheet_month_start->first_day_of_week = $timesheet_month_end->first_day_of_week;
        $timesheet_month_start->last_day_of_week = $timesheet_month_end->last_day_of_week;
        $timesheet_month_start->first_day_of_month = $timesheet_month_end->first_day_of_month;
        $timesheet_month_start->last_day_of_month = $timesheet_month_end->last_day_of_month;
        $timesheet_month_start->first_date_of_week = 1;
        $timesheet_month_start->last_date_of_week = $timesheet_month_end->last_date_of_week;
        $timesheet_month_start->first_date_of_month = $timesheet_month_end->first_date_of_month;
        $timesheet_month_start->last_date_of_month = $timesheet_month_end->last_date_of_month;
        $timesheet_month_start->day = $timesheet_month_end->day;
        $timesheet_month_start->day_name = $timesheet_month_end->day_name;
        $timesheet_month_start->hours = $timesheet_month_end->hours;
        $timesheet_month_start->wbs_id = $timesheet_month_end->wbs_id;
        $timesheet_month_start->mileage = $timesheet_month_end->mileage;
        $timesheet_month_start->invoice_number = $timesheet_month_end->invoice_number;
        $timesheet_month_start->invoice_note = $timesheet_month_end->invoice_note;
        $timesheet_month_start->cf_timesheet_id = $timesheet_month_end->cf_timesheet_id;
        $timesheet_month_start->invoice_date = $timesheet_month_end->invoice_date;
        $timesheet_month_start->invoice_due_date = $timesheet_month_end->invoice_due_date;
        $timesheet_month_start->invoice_paid_date = $timesheet_month_end->invoice_paid_date;
        $timesheet_month_start->inv_ref = $timesheet_month_end->inv_ref;
        $timesheet_month_start->timeline_id = $timesheet_month_end->timeline_id;
        $timesheet_month_start->billable = $timesheet_month_end->billable;
        $timesheet_month_start->is_billable = $timesheet_month_end->is_billable;
        $timesheet_month_start->cust_inv_ref = $timesheet_month_end->cust_inv_ref;
        $timesheet_month_start->vendor_invoice_number = $timesheet_month_end->vendor_invoice_number;
        $timesheet_month_start->creator_id = $timesheet_month_end->creator_id;
        $timesheet_month_start->system_id = $timesheet_month_end->system_id;
        $timesheet_month_start->timesheet_lock = $timesheet_month_end->timesheet_lock;
        $timesheet_month_start->vendor_invoice_status = $timesheet_month_end->vendor_invoice_status;
        $timesheet_month_start->vendor_invoice_date = $timesheet_month_end->vendor_invoice_date;
        $timesheet_month_start->vendor_due_date = $timesheet_month_end->vendor_due_date;
        $timesheet_month_start->vendor_paid_date = $timesheet_month_end->vendor_paid_date;
        $timesheet_month_start->vendor_unallocate_date = $timesheet_month_end->vendor_unallocate_date;
        $timesheet_month_start->bill_status = $timesheet_month_end->bill_status;
        $timesheet_month_start->exp_paid = $timesheet_month_end->exp_paid;
        $timesheet_month_start->status_id = $timesheet_month_end->status_id;
        $timesheet_month_start->save();

        activity()->on($timesheet_month_start)->log('copied');

        return $timesheet_month_start->id;
    }

    public function show($timesheet_id, Request $request)
    {
        // dd("true");
        $timesheet = Timesheet::with(['employee', 'project', 'customer'])->find($timesheet_id);
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        abort_unless((Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')), 403);

        if (isset($timesheet)) {
            $timesheets = Timesheet::where('employee_id', '=', $timesheet->employee_id)->orderBy('id')->pluck('id')->toArray();

            $next_timesheet_id = $timesheet->id;
            $previous_timesheet_id = $timesheet->id;

            foreach ($timesheets as $key => $value) {
                if ($timesheet->id == $value) {
                    $next_timesheet_id = isset($timesheets[$key + 1]) ? $timesheets[$key + 1] : $timesheets[0];
                    $previous_timesheet_id = isset($timesheets[$key - 1]) ? $timesheets[$key - 1] : count($timesheets) - 1;
                }
            }

            $project = Project::find($timesheet->project_id);

            $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)->where('project_id', '=', $timesheet->project_id)->first();

            //$assignment_hours_left = $assignment->hours;    variable not used

            $employee = User::find($timesheet->employee_id);

            $subject = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';
            $subject_send_email = 'Timesheet - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';

            $product_ownder = User::find($assignment->product_owner_id);

            //$timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->get()->toArray();
            $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();

            $time_exp = TimeExp::with('account')->where('timesheet_id', '=', $timesheet->id)->get();
            $timelines_drop_down = ['Select Group Task Number'];

            for ($i = 0; $i < 25; $i++) {
                $hours[$i] = $i;
            }

            $total_exp_bill = 0;
            $total_exp_claim = 0;
            foreach ($time_exp as $expense) {
                $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
                $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
            }

            $total_time_in_minutes = 0;
            foreach ($timelines as $timeline) {
                $total_minutes = 0;

                if ($project->type == 1) {
                    if ($timeline->is_billable == 1) {
                        $total_minutes += $timeline->total * 60 + $timeline->total_m;
                    }
                } else {
                    if ($timeline->is_billable == 0) {
                        $total_minutes += $timeline->total * 60 + $timeline->total_m;
                    }
                }

                $total_time_in_minutes += $total_minutes;
            }

            //$remaining_assignment_hours = ($assignment_hours_left * 60) - ($total_time_in_minutes);

            $outstanding_hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable->value.' THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();
            $remaining_assignment_hours = ($assignment->hours - $outstanding_hours->actual_hours) * 60;

            $attachments = Document::where('reference_id', '=', $timesheet->id)->where('document_type_id', '=', 7)->get();

            $timesheet_template_id = TimesheetTemplatesEnum::Standard?->value;
            $config_data = Config::orderBy('id')->first();
            if (isset($project->timesheet_template_id) && $project->timesheet_template_id > 0) {
                $timesheet_template_id = $project->timesheet_template_id;
            } else {
                if (isset($config_data->timesheet_template_id) && $config_data->timesheet_template_id > 0) {
                    $timesheet_template_id = $config_data->timesheet_template_id;
                }
            }

            $digi_sign = $this->digiSign($assignment, $project);

            $parse_week_start_date = Carbon::parse($timesheet->first_day_of_week);
            $parse_week_end_date = Carbon::parse($timesheet->last_day_of_week);
            $week_days = [];

            for ($parse_week_start_date; $parse_week_start_date <= $parse_week_end_date; $parse_week_start_date->addDay()) {
                array_push($week_days, $parse_week_start_date->format('D d'));
            }
            $combined_timesheet = $this->combinedTimesheet($timesheet, $config_data, $project);

            $parameters = [
                'project_name' => $project->name,
                'assignment_hours_left' => $remaining_assignment_hours,
                'subject' => $subject,
                'subject_send_email' => $subject_send_email,
                'timesheet' => $timesheet,
                'assignment' => $assignment,
                'timelines' => $timelines,
                'time_exp' => $time_exp,
                'employee' => $employee,
                'product_ownder' => $product_ownder,
                'total_exp_bill' => $total_exp_bill,
                'total_exp_claim' => $total_exp_claim,
                'hours' => $hours,
                'timelines_drop_down' => $timelines_drop_down,
                'attachments' => $attachments,
                'next_timesheet_id' => $next_timesheet_id,
                'previous_timesheet_id' => $previous_timesheet_id,
                'week_days' => $week_days,
                /*Timesheet approvers*/
                'project_manager_ts_user' => $digi_sign['project_manager_ts_user'],
                'project_owner_ts_user' => $digi_sign['project_owner_ts_user'],
                'project_manager_new_ts_user' => $digi_sign['project_manager_new_ts_user'],
                'line_manager_ts_user' => $digi_sign['line_manager_ts_user'],
                'claim_approver_ts_user' => $digi_sign['claim_approver_ts_user'],
                'resource_manager_ts_user' => $digi_sign['resource_manager_ts_user'],
                'timesheet_template_id' => $timesheet_template_id,
                'digisign_users' => $digi_sign['digisign_users'],
                'billing_cycle_id' => $combined_timesheet->billing_cycle_id,
                'use_custom_template' => $combined_timesheet->use_custom_template,
                'current_billing_period_id' => $combined_timesheet->current_billing_period_id,
                'project_timelines' => $combined_timesheet->project_timesheets,
            ];

            if ($request->print == '1') {
                if (! Storage::exists('timesheets/')) {
                    Storage::makeDirectory('timesheets/');
                }
                try {
                    $storage = storage_path('app/timesheets/'.str_replace(" ", '_', $timesheet->employee?->name()).'.pdf');

                    $pdf = Pdf::view('timesheet.print', $parameters);


                    //Same timesheets, different logo & company name
                    if (($timesheet_template_id == TimesheetTemplatesEnum::Weekly?->value) || ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value)) {
                        $timeline_weekly_details = [];
                        $timeline_counter = 0;
                        $timeline_minutes_total_billable = 0;
                        $timeline_minutes_total_non_billable = 0;
                        foreach ($timelines as $timeline) {
                            $date = Carbon::now();

                            $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                            $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                            //Monday
                            $mon_date[$timesheet->year_week] = $start_of_week_date;
                            $tue_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                            $wed_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                            $thu_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                            $fri_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                            $sat_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                            $sun_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));

                            $description_of_work = isset($timeline->task->description) ? $timeline->task->description : '';
                            if ($description_of_work != '') {
                                if ($timeline->description_of_work != '') {
                                    $description_of_work .= ' - '.$timeline->description_of_work;
                                }
                            } else {
                                $description_of_work = $timeline->description_of_work;
                            }

                            if ((($timeline->mon * 60) + $timeline->mon_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->mon * 60) + $timeline->mon_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->tue * 60) + $timeline->tue_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->tue * 60) + $timeline->tue_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $tue_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->wed * 60) + $timeline->wed_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->wed * 60) + $timeline->wed_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $wed_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->thu * 60) + $timeline->thu_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->thu * 60) + $timeline->thu_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $thu_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->fri * 60) + $timeline->fri_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->fri * 60) + $timeline->fri_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $fri_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->sat * 60) + $timeline->sat_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sat * 60) + $timeline->sat_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $sat_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }

                            if ((($timeline->sun * 60) + $timeline->sun_m) > 0) {
                                $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                                $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sun * 60) + $timeline->sun_m;
                                $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                                $timeline_weekly_details[$timeline_counter]['date'] = $sun_date[$timesheet->year_week];
                                $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                                $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                                $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                                if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                                    $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                } else {
                                    $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                                }

                                $timeline_counter += 1;
                            }
                        }

                        $total_time_billable = $timeline_minutes_total_billable;
                        $total_time_non_billable = $timeline_minutes_total_non_billable;

                        $parameters['timeline_weekly_details'] = $timeline_weekly_details;
                        $parameters['total_time_billable'] = $total_time_billable;
                        $parameters['total_time_non_billable'] = $total_time_non_billable;

                        $pdf = Pdf::view('timesheet.weekly_detail_print', $parameters);

                        if ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value) {
                            $pdf = Pdf::view('timesheet.weekly_detail_arbour_print', $parameters);
                        }
                    }

                    $pdf->format('a4')->save($storage);

                    $document = new Document();
                    $document->document_type_id = 5;
                    $document->name = 'Customer Tax Invoice';
                    $document->file = (isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : '').'/'.(isset($timesheet->user) ? substr($timesheet->user->first_name, 0, 4).''.substr($timesheet->user->last_name, 0, 4) : '').(isset($timesheet->project) ? str_replace(' ', '_', $timesheet->project->name) : '').($timesheet->year_week).'_'.date('Y-m-d_H_i_s').'.pdf';
                    $document->creator_id = auth()->id();
                    $document->owner_id = $timesheet->employee_id;
                    $document->save();

                    return response()->file($storage);
                } catch (FileAlreadyExistsException $e) {
                    report($e);

                    return redirect(route('timesheet', $timesheet))->with('flash_danger', $e->getMessage());
                }
            } else {
                return view('timesheet.timeline.show')->with($parameters);
            }
        } else {
            return redirect()->back()->with('flash_danger', 'The timesheet you\'re looking for was not found');
        }
    }

    public function edit($id)
    {
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        abort_unless((Auth::user()->canAccess($module[0]->id, 'update_all') || Auth::user()->canAccess($module[0]->id, 'update_team')) , 403);

        $timesheet = Timesheet::find($id);

        $date = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $week_drop_down[0] = 'Select Week';
        for ($i = $week - 12; $i <= $week; $i++) {
            $date->setISODate($year, $i);
            $date = $date->startOfWeek();
            $week_drop_down[$year.$i] = $year.$i.'('.$date->format('Y-m-d').')';
        }

        $project_drop_down = Project::where('status_id', '=', 1)->pluck('name', 'id');

        $creator_id = Resource::select('id')->where('user_id', '=', auth()->id())->get()->toArray();

        if (! isset($creator_id)) {
            $creator_id = 0;
        }

        $parameters = [
            'timesheet' => $timesheet,
            'employee' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'users.id')->join('resource', 'users.id', '=', 'resource.user_id')->where('resource.status_id', '=', 1)->orderBy('id', 'desc')->pluck('full_name', 'id')->prepend('Select Resource', '0'),
            'customer_dropdown' => Customer::where('status', '=', 1)->orderBy('id', 'desc')->pluck('customer_name', 'id')->prepend('Select Client', '0'),
            'week_dropdown' => $week_drop_down,
            'project_drop_down' => $project_drop_down,
            'creator_id' => $creator_id,
        ];

        return view('timesheet.edit')->with($parameters);
    }

    public function update(UpdateTimesheetRequest $request, $id)
    {
        $timesheet = Timesheet::find($id);
        $timesheet->employee_id = $request->input('employee_id');
        $timesheet->year_week = $request->input('year_week');
        $timesheet->project_id = $request->input('project_id');
        $timesheet->customer_id = $request->input('client_id');

        $year = substr($timesheet->year_week, 0, 4);
        $week = substr($timesheet->year_week, -2);

        $year = (int) $year;
        $week = (int) $week;

        $date = Carbon::now();
        $date2 = Carbon::now();
        $date3 = Carbon::now();
        $date4 = Carbon::now();
        $date5 = Carbon::now();
        $date6 = Carbon::now();
        $date7 = Carbon::now();
        $date8 = Carbon::now();

        $date->setISODate($year, $week);
        $date2->setISODate($year, $week);
        $date3->setISODate($year, $week);
        $date4->setISODate($year, $week);
        $date5->setISODate($year, $week);
        $date6->setISODate($year, $week);
        $date7->setISODate($year, $week);
        $date8->setISODate($year, $week);

        $timesheet->year = $year;
        $timesheet->week = $week;
        $timesheet->month = $date->month;
        $timesheet->first_day_of_week = $date->startOfWeek();
        $timesheet->last_day_of_week = $date2->endOfWeek(); //Todo: Investigate why date changes first_day_of_week if date is used, it seems date is passed by reference
        $timesheet->first_day_of_month = $date3->startOfMonth(); //Todo: Investigate why date changes first_day_of_month if date is used, it seems date is passed by reference
        $timesheet->last_day_of_month = $date4->endOfMonth(); //Todo: Investigate why date changes first_day_of_month if date is used, it seems date is passed by reference

        $temp_first_date_of_week = $date5->startOfWeek();
        $temp_last_date_of_week = $date6->endOfWeek();
        $timesheet->first_date_of_week = $temp_first_date_of_week->format('d');
        $timesheet->last_date_of_week = $temp_last_date_of_week->format('d');

        $temp_first_date_of_month = $date7->startOfMonth();
        $temp_last_date_of_month = $date8->endOfMonth();
        $timesheet->first_date_of_month = $temp_first_date_of_month->format('d');
        $timesheet->last_date_of_month = $temp_last_date_of_month->format('d');
        $timesheet->creator_id = auth()->id();
        $timesheet->status_id = 1;

        $timesheet->save();

        activity()->on($timesheet)->log('updated');

        return redirect()->back()->with('flash_success', 'Timesheet updated successfully');
    }

    public function destroy(Request $request, $timesheet_id)
    {
        $timesheet = Timesheet::find($timesheet_id);
        $module = Module::where('name', '=', \App\Models\Timesheet::class)->get();

        $flag = false;

        if (Auth::user()->canAccess($module[0]->id, 'update_all')) {
            $flag = true;
        }

        if (Auth::user()->canAccess($module[0]->id, 'update_team')) {
            $flag = true;
        }

        if ($flag == false) {
            return abort(403);
        }

        $timesheet = $timesheet->load(['project', 'user']);
        activity()->on($timesheet)->withProperties(['name' => $timesheet->user->first_name.' '.$timesheet->user->last_name.' deleted  timesheet for '.substr($timesheet->project->name, 0, 1).' ('.$timesheet->year_week.')'])->log('deleted');
        $timesheet->timeline()->delete();
        $timesheet->delete();

        if ($request->ajax()) {
            return response()->json($timesheet_id);
        }
        return redirect()->back()->with('flash_success', 'Timesheet successfully deleted');
    }

    //Begin Timeline functions
    public function createTimeLine(Timesheet $timesheet)
    {
        $config = Config::select(['timesheet_template_id', 'billing_cycle_id', 'timesheet_weeks', 'timesheet_weeks_future', 'default_custom_template'])->first();

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->where('status', '!=', '4')->where('status', '!=', '5')->get();

        $project = Project::find($timesheet->project_id);
        $project_name = $project->name;
        $config_temp = $config->timesheet_template_id ?? TimesheetTemplatesEnum::Standard?->value;
        $project_template_id = $project->timesheet_template_id ?? $config_temp;
        $assignment_hours_left = 0;
        $empty = $assignment->isEmpty();

        $is_billable = 0;
        if (! $empty) {
            $assignment_hours_left = $assignment[0]->hours;

            if ($assignment[0]->billable == null || $assignment[0]->billable == '') {
                $is_billable = 0;
            } else {
                $is_billable = $assignment[0]->billable;
            }
        }

        $employee = User::find($timesheet->employee_id);

        $timelines = Timeline::with('task')->where('timesheet_id', '=', $timesheet->id)->get();
        //dd($timelines[1]['task']['description']);
        $task_drop_down = Task::where('project_id', '=', $timesheet->project_id)
            ->where('status', '!=', 5)
            ->where('employee_id', '=', $timesheet->employee_id)
            ->pluck('description', 'id');

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }
        for ($i = 0; $i < 60; $i++) {
            $minutes[$i] = $i;
        }

        $total_time_in_minutes = 0;
        foreach ($timelines as $timeline) {
            $total_minutes = 0;

            if ($project->type == 1) {
                if ($timeline->is_billable == 1) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            } else {
                if ($timeline->is_billable == 0) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            }

            $total_time_in_minutes += $total_minutes;
        }

        //$remaining_assignment_hours = ($assignment_hours_left * 60) - ($total_time_in_minutes);
        if (isset($assignment[0])) {
            $outstanding_hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment[0]['billable']->value.' THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment[0]['project_id'])
                ->where('timesheet.employee_id', '=', $assignment[0]['employee_id'])
                ->first();
            $remaining_assignment_hours = ($assignment[0]['hours'] - $outstanding_hours->actual_hours) * 60;
        } else {
            $remaining_assignment_hours = 'No assignment is selected';
        }

        $combined_timesheets = $this->combinedTimesheet($timesheet, $config, $project);
        $project_timesheets = $combined_timesheets->project_timesheets;
        $billing_cycle = $combined_timesheets->billing_cycle_id;
        $parse_week_start_date = Carbon::parse($timesheet->first_day_of_week);
        $parse_week_end_date = Carbon::parse($timesheet->last_day_of_week);
        $week_days = [];

        for ($parse_week_start_date; $parse_week_start_date <= $parse_week_end_date; $parse_week_start_date->addDay()) {
            array_push($week_days, $parse_week_start_date->format('D d'));
        }

        $year_week_drop_down = $this->weeks($config, $billing_cycle ?? 0)->keyBy('year_week')->map(function ($week) {
            return $week['date'];
        });

        $epics_drop_down = Epic::with(['features' => function ($feature) {
            return $feature->select(['id', 'name', 'epic_id'])->with(['user_stories' => function ($story) {
                return $story->select(['id', 'name', 'feature_id']);
            }]);
        }])->where('project_id', $timesheet->project_id)->get(['name', 'id']);

        $parameters = [
            'project_name' => $project_name,
            'template_id' => $project_template_id,
            'assignment_hours_left' => $remaining_assignment_hours,
            'use_custom_template' => $config->default_custom_template,
            'timesheet' => $timesheet,
            'timelines' => $timelines,
            'employee' => $employee,
            'hours' => $hours,
            'minutes' => $minutes,
            'is_billable' => $is_billable,
            'task_drop_down' => $task_drop_down,
            'project_timelines' => $project_timesheets,
            'year_week_drop_down' => $year_week_drop_down,
            'week_days' => $week_days,
            'billing_cycle_id' => $billing_cycle,
            'current_billing_period_id' => $combined_timesheets->current_billing_period_id,
            'task_delivery_type_id' => TaskDeliveryType::orderBy('name')->pluck('name', 'id')->prepend('Select Task Delivery Type'),
            'epics_drop_down' => $epics_drop_down,
            'status_drop_down' => TaskStatus::pluck('description', 'id'),
            'yes_or_no_dropdown' => Task::KANBAN_DISPLAY,
        ];

        // dd($parameters);

        return view('timesheet.timeline.create')->with($parameters);
    }

    public function storeTimeLine(StoreTimeLineRequest $request, $timesheet_id)
    {
        if ($request->has('task_id')) {
            $task = Task::select('status')->find($request->task_id);

            if (isset($task) && $task->status == 5) {
                if ($request->ajax()) {
                    return response()->json(['error',"You can't capture time against a completed task"]);
                }
                return redirect()->back()->with('flash_info', "You can't capture time against a completed task");
            }
        }

        $timeline = new Timeline;
        $timeline->description_of_work = $request->input('description_of_work');
        $timeline->timesheet_id = $timesheet_id;
        $timeline->task_id = $request->has('task_id') && $request->input('task_id') > 0 ? $request->input('task_id') : 0;
        $timeline->mon = $request->has('mon') ? $request->input('mon') : 0;
        $timeline->tue = $request->has('tue') ? $request->input('tue') : 0;
        $timeline->wed = $request->has('wed') ? $request->input('wed') : 0;
        $timeline->thu = $request->has('thu') ? $request->input('thu') : 0;
        $timeline->fri = $request->has('fri') ? $request->input('fri') : 0;
        $timeline->sat = $request->has('sat') ? $request->input('sat') : 0;
        $timeline->sun = $request->has('sun') ? $request->input('sun') : 0;
        $timeline->total = $timeline->mon + $timeline->tue + $timeline->wed + $timeline->thu + $timeline->fri + $timeline->sat + $timeline->sun;
        $timeline->mon_m = $request->has('mon_m') ? $request->input('mon_m') : 0;
        $timeline->tue_m = $request->has('tue_m') ? $request->input('tue_m') : 0;
        $timeline->wed_m = $request->has('wed_m') ? $request->input('wed_m') : 0;
        $timeline->thu_m = $request->has('thu_m') ? $request->input('thu_m') : 0;
        $timeline->fri_m = $request->has('fri_m') ? $request->input('fri_m') : 0;
        $timeline->sat_m = $request->has('sat_m') ? $request->input('sat_m') : 0;
        $timeline->sun_m = $request->has('sun_m') ? $request->input('sun_m') : 0;
        $timeline->total_m = $timeline->mon_m + $timeline->tue_m + $timeline->wed_m + $timeline->thu_m + $timeline->fri_m + $timeline->sat_m + $timeline->sun_m;
        $timeline->is_billable = $request->input('is_billable');
        $timeline->vend_is_billable = $request->input('vend_is_billable');
        $timeline->mileage = $request->input('mileage');
        $timeline->creator_id = auth()->id();
        $timeline->status = 1;
        $timeline->task_delivery_type_id = $request->input('task_delivery_type_id');
        $timeline->save();

        $timesheet = Timesheet::find($timesheet_id);

        $employee = User::find($timesheet->employee_id);

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->get()->toArray();

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        for ($i = 0; $i < 60; $i++) {
            $minutes[$i] = $i;
        }

        $parameters = [
            'timesheet' => $timesheet,
            'timelines' => $timelines,
            'employee' => $employee,
            'hours' => $hours,
            'minutes' => $minutes,
        ];

        if ($request->has('task_id') && $request->input('task_id') > 0) {
            $task = Task::find($request->input('task_id'));
            $task->status = 3;
            $task->save();
        }
        $this->utils->hoursOverspent($timesheet->employee_id, $timesheet->project_id);

        $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)
            ->where('project_id', '=', $timesheet->project_id)
            ->first();
        //return $assignment;

        $hours = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable->value.' THEN timeline.total ELSE 0 END) AS hours, SUM(CASE WHEN timeline.is_billable = '.$assignment->billable->value.' THEN timeline.total_m ELSE 0 END) AS minutes')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.project_id', '=', $assignment->project_id)
            ->where('timesheet.employee_id', '=', $assignment->employee_id)
            ->first();

        $hours_to_date = sprintf('%d:%02d', floor((($hours->hours * 60) + $hours->minutes) / 60), (($hours->hours * 60) + $hours->minutes) % 60);

        $claimApprover = User::find($this->utils->claimApprover());

        if ($claimApprover != null) {
            if (($timeline->mon + ($timeline->mon_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->mon + ($timeline->mon_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->mon + ($timeline->mon_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->tue + ($timeline->tue_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->tue + ($timeline->tue_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->tue + ($timeline->tue_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->wed + ($timeline->wed_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->wed + ($timeline->wed_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->wed + ($timeline->wed_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->thu + ($timeline->thu_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->thu + ($timeline->thu_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->thu + ($timeline->thu_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->fri + ($timeline->fri_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->fri + ($timeline->fri_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->fri + ($timeline->fri_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->sat + ($timeline->sat_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->sat + ($timeline->sat_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->sat + ($timeline->sat_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            } elseif (($timeline->sun + ($timeline->sun_m / 60)) > ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60))) {
                $this->notifications($timesheet);
                //Mail::to($claimApprover->email)->send(new AssignmentMaxHoursExceeded($assignment, $timesheet, ($timeline->sun + ($timeline->sun_m/60)), $this->utils->helpPortal, ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min/60)), ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min/60)), $hours_to_date));
                SendAssignmentMaxHoursExceededEmailJob::dispatch(
                    $claimApprover->email,
                    $assignment,
                    $timesheet,
                    ($timeline->sun + ($timeline->sun_m / 60)),
                    $this->utils->helpPortal,
                    ($assignment->max_time_per_day_hours + ($assignment->max_time_per_day_min / 60)),
                    ($assignment->capacity_allocation_hours + ($assignment->capacity_allocation_min / 60)),
                    $hours_to_date
                )->delay(now()->addMinutes(5));
            }
        }

        if ($request->ajax()) {

            $total_non_bill = 0;
            $total_bill = 0;
            $timelines2 = Timesheet::selectRaw('timeline.is_billable as billable,SUM(timeline.total + (timeline.total_m/60)) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->groupBy('timeline.is_billable')
                ->get();

            foreach ($timelines2 as $timeline2) {

                if ($timeline2->billable == 1) {
                    $total_bill += $timeline2->actual_hours;
                }
                if ($timeline2->billable == 0) {
                    $total_non_bill += $timeline2->actual_hours;
                }
            }

            $remaining_assignment_hours = $assignment->project->project_type_id == 1 ? ($assignment->hours) - ($total_bill) : ($assignment->hours) - ($total_bill+$total_non_bill);

            $task = Task::find($timeline->task_id);
            if($task){
                $data = collect($timeline);
                $data['task'] = $task->description;
            } else {
                $data = collect($timeline);
                $data['task'] = $timeline->description_of_work;
            }
            return response()->json(['task' => $data,'remaining_assignment_hours'=>$remaining_assignment_hours]);
        }

        return redirect(route('timesheet.createtimeline', $timesheet_id))->with('flash_success', 'Timeline added successfully');
    }

    public function editTimeline(Timesheet $timesheet, $timeline_id)
    {
        if ($timeline_id != 0) {
            $edit_timeline = Timeline::find($timeline_id);
        } else {
            $edit_timeline = Timeline::where('timesheet_id', $timesheet->id)
                ->latest('id')
                ->first();

            if (! isset($edit_timeline)) {
                return redirect(route('timesheet.index'))->with('flash_success', 'Please add a timeline');
            }
        }

        $project = Project::find($timesheet->project_id);

        $config = Config::select(['timesheet_template_id', 'billing_cycle_id', 'default_custom_template'])->first();
        $config_temp = $config->timesheet_template_id ?? TimesheetTemplatesEnum::Standard?->value;
        $project_template_id = $project->timesheet_template_id ?? $config_temp;

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->where('status', '!=', '4')->where('status', '!=', '5')->get();

        $assignment_hours_left = $assignment[0]->hours;

        $employee = User::find($timesheet->employee_id);

        $timelines = Timeline::with('task')->where('timesheet_id', '=', $timesheet->id)->get();

        $task_drop_down = Task::where('project_id', '=', $timesheet->project_id)
            ->where('status', '!=', 5)
            ->where('employee_id', '=', $timesheet->employee_id)
            ->pluck('description', 'id');

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        for ($i = 0; $i < 60; $i++) {
            $minutes[$i] = $i;
        }

        $total_time_in_minutes = 0;
        foreach ($timelines as $timeline) {
            $total_minutes = 0;

            if ($project->type == 1) {
                if ($timeline->is_billable == 1) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            } else {
                if ($timeline->is_billable == 0) {
                    $total_minutes += $timeline->total * 60 + $timeline->total_m;
                }
            }

            $total_time_in_minutes += $total_minutes;
        }

        $remaining_assignment_hours = ($assignment_hours_left * 60) - ($total_time_in_minutes);
        $combined_timesheets = $this->combinedTimesheet($timesheet, $config, $project);
        $project_timesheets = $combined_timesheets->project_timesheets;
        $billing_cycle = $combined_timesheets->billing_cycle_id;
        $parse_week_start_date = Carbon::parse($timesheet->first_day_of_week);
        $parse_week_end_date = Carbon::parse($timesheet->last_day_of_week);
        $week_days = [];

        for ($parse_week_start_date; $parse_week_start_date <= $parse_week_end_date; $parse_week_start_date->addDay()) {
            array_push($week_days, $parse_week_start_date->format('D d'));
        }

        $year_week_drop_down = $this->weeks($config, $billing_cycle ?? 0)->keyBy('year_week')->map(function ($week) {
            return $week['date'];
        });

        $epics_drop_down = Epic::with(['features' => function ($feature) {
            return $feature->select(['id', 'name', 'epic_id'])->with(['user_stories' => function ($story) {
                return $story->select(['id', 'name', 'feature_id']);
            }]);
        }])->where('project_id', $timesheet->project_id)->get(['name', 'id']);

        $parameters = [
            'project_name' => $project->name,
            'assignment_hours_left' => $remaining_assignment_hours,
            'timesheet' => $timesheet,
            'edit_timeline' => $edit_timeline,
            'use_custom_template' => $config->default_custom_template,
            'timelines' => $timelines,
            'employee' => $employee,
            'hours' => $hours,
            'minutes' => $minutes,
            'task_drop_down' => $task_drop_down,
            'template_id' => $project_template_id,
            'year_week_drop_down' => $year_week_drop_down,
            'project_timelines' => $project_timesheets,
            'week_days' => $week_days,
            'billing_cycle_id' => $billing_cycle,
            'current_billing_period_id' => $combined_timesheets->current_billing_period_id,
            'task_delivery_type_id' => TaskDeliveryType::orderBy('name')->pluck('name', 'id')->prepend('Select Task Delivery Type'),
            'epics_drop_down' => json_encode($epics_drop_down),
            'status_drop_down' => TaskStatus::pluck('description', 'id'),
            'yes_or_no_dropdown' => Task::KANBAN_DISPLAY,
        ];

        return view('timesheet.timeline.edit')->with($parameters);
    }

    public function updateTimeLine(UpdateTimeLineRequest $request, $timeline_id)
    {
        // return $request;
        if ($request->has('task_id')) {
            $task = Task::select('status')->find($request->task_id);

            if (isset($task->status) && $task->status == 5) {

                if ($request->ajax()) {
                    return response()->json(['error'=>true,'message'=>"You can't capture time against a completed task",'timeline'=>Timeline::find($timeline_id)]);
                }
                return redirect()->back()->with('flash_info', 'You can capture time against a completed task');
            }
        }

        $timeline = Timeline::find($timeline_id);

        $timeline->description_of_work = $request->input('description_of_work');
        $timeline->task_id = $request->has('task_id') && $request->input('task_id') > 0 ? $request->input('task_id') : 0;
        $timeline->mon = $request->has('mon') ? $request->input('mon') : 0;
        $timeline->tue = $request->has('tue') ? $request->input('tue') : 0;
        $timeline->wed = $request->has('wed') ? $request->input('wed') : 0;
        $timeline->thu = $request->has('thu') ? $request->input('thu') : 0;
        $timeline->fri = $request->has('fri') ? $request->input('fri') : 0;
        $timeline->sat = $request->has('sat') ? $request->input('sat') : 0;
        $timeline->sun = $request->has('sun') ? $request->input('sun') : 0;
        $timeline->total = $timeline->mon + $timeline->tue + $timeline->wed + $timeline->thu + $timeline->fri + $timeline->sat + $timeline->sun;
        $timeline->mon_m = $request->has('mon_m') ? $request->input('mon_m') : 0;
        $timeline->tue_m = $request->has('tue_m') ? $request->input('tue_m') : 0;
        $timeline->wed_m = $request->has('wed_m') ? $request->input('wed_m') : 0;
        $timeline->thu_m = $request->has('thu_m') ? $request->input('thu_m') : 0;
        $timeline->fri_m = $request->has('fri_m') ? $request->input('fri_m') : 0;
        $timeline->sat_m = $request->has('sat_m') ? $request->input('sat_m') : 0;
        $timeline->sun_m = $request->has('sun_m') ? $request->input('sun_m') : 0;
        $timeline->total_m = $timeline->mon_m + $timeline->tue_m + $timeline->wed_m + $timeline->thu_m + $timeline->fri_m + $timeline->sat_m + $timeline->sun_m;
        $timeline->is_billable = $request->input('is_billable');
        $timeline->vend_is_billable = $request->input('vend_is_billable');
        $timeline->mileage = $request->input('mileage');
        $timeline->task_delivery_type_id = $request->input('task_delivery_type_id');
        $timeline->save();

        $data = collect($timeline);
        $data['canEdit'] = false;

        if ($request->ajax()) {
            $timesheet = Timesheet::find($timeline->timesheet_id);
            // $project = Project::find($timesheet->project_id);
            $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->get();

            $assignment_hours_left = $assignment[0]->hours;

            $empty = $assignment->isEmpty();

            if (! $empty) {
                $assignment_hours_left = $assignment[0]->hours;
            }

            $total_non_bill = 0;
            $total_bill = 0;
            $timelines2 = Timesheet::selectRaw('timeline.is_billable as billable,SUM(timeline.total + (timeline.total_m/60)) AS actual_hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.employee_id', '=', $assignment[0]->employee_id)
                ->where('timesheet.project_id', '=', $assignment[0]->project_id)
                ->groupBy('timeline.is_billable')
                ->get();

            foreach ($timelines2 as $timeline2) {

                if ($timeline2->billable == 1) {
                    $total_bill += $timeline2->actual_hours;
                }
                if ($timeline2->billable == 0) {
                    $total_non_bill += $timeline2->actual_hours;
                }
            }


            $this->utils->hoursOverspent($timesheet->employee_id, $timesheet->project_id);

            $remaining_assignment_hours = $assignment[0]->project->project_type_id == 1 ? ($assignment[0]->hours) - ($total_bill) : ($assignment[0]->hours) - ($total_bill+$total_non_bill);

            return response()->json(['timeline'=>$data,'remaining_assignment_hours'=>$remaining_assignment_hours]);
        }

        return redirect(route('timesheet.edittimeline', [$timeline->timesheet_id, $timeline->id]))->with('flash_success', 'Timeline updated successfully');
    }

    public function taskDeliveryId(Task $task): JsonResponse
    {
        return response()->json(['task_delivery_type_id' => $task->task_delivery_type_id]);
    }

    public function destroyTimeline(Request $request, $timeline_id)
    {
        $timeline = Timeline::find($timeline_id);
        $timesheet_id = $timeline->timesheet_id;
        DB::table('timeline')->delete($timeline_id);
        // Project::destroy($timeline_id);
        if ($request->ajax()) {
            return response()->json($timeline_id);
        }
        return redirect(route('timesheet.edittimeline', [$timesheet_id, 0]))->with('flash_success', 'Timeline deleted successfully');
    }
    //End Timeline functions

    //Expense functions Begin
    public function createExpense($id): View
    {
        $accounts = Account::with('account_type')->select('id', 'account_type_id', 'description', 'account_group', 'status')->orderBy('account_type_id')->orderBy('description')->where('status', '=', 1)->get();
        $account_dropdown = [];
        foreach ($accounts as $account) {
            $account_dropdown[$account->id] = $account->account_type->description.' - '.$account->description;
        }

        $timesheet = Timesheet::find($id);

        $employee = User::find($timesheet->employee_id);
        $expenses = TimeExp::where('timesheet_id', '=', $id)->get();

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->get()->toArray();
        $timelines_drop_down = ['Select Group Task Number'];

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->where('status', '!=', '4')->where('status', '!=', '5')->get();

        $is_billable = 0;
        if ($assignment[0]->billable == null || $assignment[0]->billable == '') {
            $is_billable = 0;
        } else {
            $is_billable = $assignment[0]->billable;
        }

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $parameters = [
            'timesheet' => $timesheet,
            'timelines' => $timelines,
            'employee' => $employee,
            'hours' => $hours,
            'timelines_drop_down' => $timelines_drop_down,
            'account_dropdown' => $account_dropdown,
            'expenses' => $expenses,
            'is_billable' => $is_billable,
        ];

        return view('timesheet.expenses.create')->with($parameters);
    }

    public function storeExpense($timesheet_id,StoreTimeSheetExpenseRequest $request)
    {
        $expense = new TimeExp;
        $expense->timesheet_id = $timesheet_id;
        $expense->date = $request->input('date');
        $expense->is_billable = $request->input('is_billable');
        $expense->claim = $request->input('claim');
        $expense->description = $request->input('description');
        $expense->amount = $request->input('amount');
        $expense->account_id = $request->has('account_id') ? $request->input('account_id') : 0;
        $expense->account_element_id = $request->has('element') ? $request->input('element') : 0;
        $expense->status = 1;
        $expense->save();

        activity()->on($expense)->withProperties(['expense_description' => $expense->description])->log('created');

        if ($request->ajax()) {
            return response()->json($expense);
        }

        return redirect(route('timesheet.createexpense', $timesheet_id))->with('flash_success', 'Expense added successfully');
    }

    public function editExpense($timesheet_id, $expense_id)
    {
        if ($expense_id != 0) {
            $edit_expense = TimeExp::find($expense_id);
        } else {
            $edit_expense = TimeExp::where('timesheet_id', $timesheet_id)
                ->latest('id')
                ->first();

            if (! isset($edit_expense)) {
                return redirect(route('timesheet.index'))->with('flash_success', 'Please add at-least 1 expense before you can edit');
            }
        }

        $accounts = Account::with('account_type')->select('id', 'account_type_id', 'description', 'account_group', 'status')->orderBy('account_type_id')->orderBy('description')->where('status', '=', 1)->get();
        $account_dropdown = [];
        foreach ($accounts as $account) {
            $account_dropdown[$account->id] = $account->account_type->description.' - '.$account->description;
        }

        $account_elements_drop_down = AccountElement::with('account.account_type')
            ->where('account_id', '=', $edit_expense->account_id)
            ->where('status', '=', 1)
            ->pluck('description', 'id');

        $timesheet = Timesheet::find($timesheet_id);

        $employee = User::find($timesheet->employee_id);
        $expenses = TimeExp::where('timesheet_id', '=', $timesheet_id)->get();

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->get()->toArray();
        $timelines_drop_down = ['Select Group Task Number'];

        $assignment = Assignment::where('project_id', '=', $timesheet->project_id)->where('employee_id', '=', $timesheet->employee_id)->where('status', '!=', '4')->where('status', '!=', '5')->get();

        $is_billable = 0;
        if ($assignment[0]->billable == null || $assignment[0]->billable == '') {
            $is_billable = 0;
        } else {
            $is_billable = $assignment[0]->billable;
        }

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $parameters = [
            'timesheet' => $timesheet,
            'timelines' => $timelines,
            'employee' => $employee,
            'hours' => $hours,
            'timelines_drop_down' => $timelines_drop_down,
            'account_dropdown' => $account_dropdown,
            'expenses' => $expenses,
            'is_billable' => $is_billable,
            'edit_expense' => $edit_expense,
            'account_elements_drop_down' => $account_elements_drop_down,
        ];

        return view('timesheet.expenses.edit')->with($parameters);
    }

    public function updateExpense($expense_id,UpdateTimeSheetExpenseRequest $request)
    {
        $expense = TimeExp::find($expense_id);
        $expense->date = Carbon::parse($request->input('date'))->format('Y-m-d');
        $expense->is_billable = $request->input('is_billable');
        $expense->claim = $request->input('claim');
        $expense->description = $request->input('description');
        $expense->amount = $request->input('amount');
        $expense->account_id = $request->has('account_id') ? $request->input('account_id') : 0;
        $expense->account_element_id = $request->has('account_element_id') ? $request->input('account_element_id') : 0;
        $expense->save();

        activity()->on($expense)->withProperties(['expense_description' => $expense->description])->log('updated');

        if ($request->ajax()) {
            return response()->json($expense);
        }

        return redirect(route('timesheet.editexpense', [$expense->timesheet_id, $expense->id]))->with('flash_success', 'Timeline added successfully');
    }

    public function destroyExpense(Request $request,$expense_id)
    {
        $timeexp = TimeExp::find($expense_id);
        activity()->on($timeexp)->withProperties(['name' => auth()->user()->first_name.' '.auth()->user()->last_name.' deleted an expense for timesheet (#'.$timeexp->timesheet_id.')'])->log('deleted');
        $timesheet_id = $timeexp->timesheet_id;
        DB::table('time_exp')->delete($expense_id);
        TimeExp::destroy($expense_id);

        if ($request->ajax()) {
            return response()->json($expense_id);
        }

        return redirect(route('timesheet.editexpense', [$timesheet_id, 0]))->with('flash_success', 'Expense deleted successfully');
    }
    //Expense functins End

    public function maintain($time_id)
    {
        $r = User::find(Auth::id());
        if ($r->hasRole('admin') || $r->hasRole('admin_manager')) {
            //Continue
        } else {
            return abort(403);
        }

        $time = Timesheet::find($time_id);

        $projects = Assignment::with('project')
            ->where('employee_id', $time->employee_id)
            ->whereIn('assignment_status', [2, 3])
            ->get();
        $projects_dropdown = [];

        foreach ($projects as $pro) {
            if (isset($pro->project)) {
                $projects_dropdown[$pro->project->id] = $pro->project->name.' ('.$pro->project->id.')';
            }
        }

        $time_exp = TimeExp::select('amount', 'paid_date', 'claim_auth_date')->where('timesheet_id', '=', $time_id)->where('status', '=', 1)->where('claim', '=', 1)->first();
        $amount = ($time_exp == null) ? 0 : $time_exp->amount;
        $expam = ($amount > 0) ? 'Expense Amount = R '.$amount : 'Not Applicable';
        $assignment = Assignment::where('project_id', '=', $time->project_id)->where('employee_id', '=', $time->employee_id)->first();
        $hours = Timeline::selectRaw('IFNULL(SUM(total + (total_m/60)), 0) AS hours')->where('is_billable', '=', 1)->where('timesheet_id', '=', $time->id)->first();

        $expenses_approve = TimeExp::with(['account', 'account_element'])->where('timesheet_id', '=', $time_id)->where('status', '=', 1)->get();
        $total_exp_bill = 0;
        $total_exp_claim = 0;
        foreach ($expenses_approve as $expense) {
            $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
            $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
        }

        if ($assignment != null) {
            $parameters = [
                'time' => $time,
                'hours' => $hours->hours,
                'amount' => $amount,
                'customer' => Customer::where('status', '=', 1)->orderBy('customer_name')->pluck('customer_name', 'id'),
                'project' => $projects_dropdown,
                'invoice' => InvoiceStatus::orderBy('description')->pluck('description', 'id')->prepend('Select Invoice Status', '0'),
                'time_exp' => $time_exp,
                'expam' => $expam,
                'ext_lab_cost' => ($assignment->external_cost_rate != 0) ? 1 : 0,
                'ext_lab_cost_out' => ($assignment->external_cost_rate != 0 || $assignment->external_cost_rate != null) ? 1 : 0,
                'assignment' => $assignment,
                'ext_labour_amount' => 0,
                'status_dropdown' => Status::orderBy('description')->pluck('description', 'id'),
                'invoice_status' => InvoiceStatus::orderBy('description')->pluck('description')->prepend('Select Invoice Status', '0'),
                'vendor_invoice_status' => VendorInvoiceStatus::orderBy('description')->pluck('description', 'id')->prepend('Select Invoice Status', '0'),
                'expenses_approve' => $expenses_approve,
                'total_exp_claim' => $total_exp_claim,
                'total_exp_bill' => $total_exp_bill,
            ];

            return view('timesheet.maintain-v2')->with($parameters);
        } else {
            return redirect()->back()->with('error_detected', 'There\'s no assignment associated with this project');
        }
    }

    public function store_maintain(Request $request, $id): RedirectResponse
    {
        $timesheet = Timesheet::find($id);
        //$timesheet->customer_id = $request->client;
        $timesheet->project_id = $request->project;
        $timesheet->version = isset($request->version) ? $request->version : null;
        $timesheet->bill_status = ($request->invoice_status != '0') ? $request->invoice_status : 1;
        $timesheet->invoice_note = isset($request->note) ? $request->note : null;
        $timesheet->invoice_date = isset($request->invoice_date) ? $request->invoice_date : null;
        $timesheet->invoice_due_date = isset($request->due_date) ? $request->due_date : null;
        $timesheet->invoice_paid_date = isset($request->invoice_paid_date) ? $request->invoice_paid_date : null;
        $timesheet->cust_inv_ref = isset($request->cust_inv_ref) ? $request->cust_inv_ref : null;
        $timesheet->inv_ref = isset($request->inv_ref) ? $request->inv_ref : null;
        $timesheet->timesheet_lock = ($request->tlock) ? $request->tlock : 0;
        $timesheet->cf_timesheet_id = isset($request->cf_timesheet_id) ? $request->cf_timesheet_id : null;
        $timesheet->save();

        return redirect()->back()->with('flash_success', 'Timesheet maintained successfully');
    }

    public function approveExpense(Request $request, $time_expense_id)
    {
        $timesheet_expense = TimeExp::with(['account', 'account_element'])->where('id', $time_expense_id)->first();
        $timesheet_expense->claim_auth_date = now()->toDateString();
        $timesheet_expense->is_approved = 1;
        $timesheet_expense->save();
        activity()->on($timesheet_expense)->log('approved');
        if ($request->has('from')) {
            return response()->json($timesheet_expense, 200);
        }

        if ($request->ajax()) {
            return response()->json($timesheet_expense);
        }

        return redirect()->back();
    }

    public function declineExpense(Request $request, $time_expense_id)
    {
        $timesheet_expense = TimeExp::with(['account', 'account_element'])->where('id', $time_expense_id)->first();
        $timesheet_expense->is_approved = 0;
        $timesheet_expense->save();
        activity()->on($timesheet_expense)->log('Declined');
        if ($request->has('from')) {
            return response()->json($timesheet_expense, 200);
        }

        if ($request->ajax()) {
            return response()->json($timesheet_expense);
        }

        return redirect()->back();
    }

    public function payExpense(Request $request, $time_expense_id)
    {
        $timesheet_expense = TimeExp::find($time_expense_id);
        $timesheet_expense->paid_date = Carbon::now()->toDateString();
        $timesheet_expense->save();
        $timesheet = Timesheet::find($timesheet_expense->timesheet_id);
        $timesheet->exp_paid = 1;
        $timesheet->save();
        activity()->on($timesheet_expense)->log('paid');

        if ($request->ajax()) {
            return response()->json($timesheet_expense);
        }

        return redirect()->back();
    }

    public function get_timesheet(Request $request): JsonResponse
    {
        $project = Project::select('id', 'name')
            ->where('id', '>', 1)
            ->where('customer_id', '=', $request->client)
            ->where('status_id', '=', 1)
            ->get();

        $account_elements = AccountElement::with('account.account_type')
            ->where('account_id', '=', $request->account)
            ->where('status', '=', 1)
            ->get();

        if ($request->ajax()) {
            $response = [
                'project' => $project,
                'account' => $account_elements,
                'status' => 'success',
            ];

            return response()->json($response);
        } else {
            return response()->json(['msg' => 'false']);
        }
    }

    public function addExpenses(): View
    {
        $tasks = Task::select(DB::raw("CONCAT(grouptask_id,' - ',COALESCE(`description`,'')) AS display_name"), 'id')->where('status', '=', 1)->orderBy('id', 'desc')->pluck('display_name', 'id')->prepend('Select Group Task ID', '0');
        $accounts = Account::with('account_type')->select('id', 'account_type_id', 'description', 'account_group', 'status')->orderBy('account_type_id')->orderBy('description')->where('status', '=', 1)->get();
        foreach ($accounts as $account) {
            $account_dropdown[$account->id] = $account->account_type->description.' - '.$account->description;
        }
        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }
        $parameters = $this->normalize_date($tasks, $hours, $account_dropdown);

        return view('timesheet.expenses.index')->with($parameters);
    }

    private function normalize_date($data = null, $data1 = null, $data2 = null, $data3 = null, $data4 = null, $data5 = null)
    {
        $date = Carbon::now();
        $today = $date->toDateString();
        $startOfWeek = $date->startOfWeek()->toDateString();
        $endOfWeek = $date->endOfWeek()->toDateString();
        $endOfMonth = $date->endOfMonth()->toDateString();
        $start_of_week = substr($date->startOfWeek()->toDayDateTimeString(), 0, 3);
        $end_of_week = substr($date->endOfWeek()->toDayDateTimeString(), 0, 3);
        $end_of_month = substr($date->endOfMonth()->toDayDateTimeString(), 0, 3);
        $yearwk = $date->year.$date->weekOfYear;

        if ($endOfMonth > $startOfWeek && $endOfMonth < $endOfWeek) {
            $endOfWeek = $endOfMonth;
        }

        $parameters = [
            'data' => $data,
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3,
            'data4' => $data4,
            'data5' => $data5,
            'today' => $today,
            'start_week' => $startOfWeek,
            'end_week' => $endOfWeek,
            'end_month' => $endOfMonth,
            'last_day_month' => $end_of_month,
            'first_day_week' => $start_of_week,
            'end_day_week' => $end_of_week,
            'yearwk' => $yearwk,
        ];

        return $parameters;
    }

    public function addTime(): View
    {
        $timelines = Timeline::where('status', '=', 1)->where('time_id', '=', 253)->with(['customer', 'project'])->get();

        $company = '';
        $project = '';
        foreach ($timelines  as $timeline) {
            $company = $timeline->company;
            $project = $timeline->project;
        }
        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }
        for ($i = 0; $i < 60; $i++) {
            $minutes[$i] = $i;
        }

        $parameters = $this->normalize_date($timelines, $company, $project, $hours, $minutes);

        return view('timesheet.add_time.addtime')->with($parameters);
    }

    public function editTime($time_id): View
    {
        $time = Time::find($time_id);
        $calendar = Calendar::where('start_of_week_date', '=', $time->start_date)->first();
        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }
        for ($i = 0; $i < 60; $i++) {
            $minutes[$i] = $i;
        }

        $parameters = $this->normalize_date($calendar, $time, null, $hours, $minutes);

        return view('timesheet.add_time.edittime')->with($parameters);
    }

    public function getClients($resource_id): JsonResponse
    {
        $assignments = Assignment::with(['project' => function ($project) {
            $project->select('id', 'name', 'customer_id');
        }, 'project.customer' => function ($customer) {
            $customer->select('id', 'customer_name');
        }])->select('employee_id', 'project_id', 'end_date')
            ->where(['assignment_status' => 3, 'employee_id' => $resource_id])
            ->groupBy(['employee_id', 'project_id', 'end_date']);

        $dropdowns = $assignments->get();
        foreach ($dropdowns as $assignment) {
            $customers[(isset($assignment->project->customer_id) ? $assignment->project->customer_id : 0)] = (isset($assignment->project->customer->customer_name) ? $assignment->project->customer->customer_name : null);
            $projects[$assignment->project_id] = (isset($assignment->project->name) ? $assignment->project->name : null);
        }

        $active_customer = $assignments->latest('end_date')->first()->project->customer_id ?? 0;
        $active_project = $assignments->latest('end_date')->first()->project_id ?? 0;

        return response()->json([
            'customers' => $customers ?? [],
            'projects' => $projects ?? [],
            'active_customer' => $active_customer,
            'active_project' => $active_project,
        ]);
    }

    public function getProjects($resource_id): JsonResponse
    {
        $assignments = Assignment::select('project_id')->where('employee_id', '=', $resource_id)->where('status', '!=', '4')->where('status', '!=', '5')->get()->toArray();
        $consultatns = [];
        foreach ($assignments as $assignment) {
            $consultants[] = $assignment['project_id'];
        }
        $projects = Project::whereIn('id', $consultants)->pluck('name', 'id');

        return response()->json(['message' => 'Success', 'projects' => $projects]);
    }

    public function maintain_timesheet(Request $request): JsonResponse
    {
        $projects = DB::select("SELECT a.*,b.* FROM projects a INNER JOIN assignment b ON a.id = b.project_id WHERE a.customer_id = '".$request->customer_id."' AND b.employee_id = '".$request->resource_id."'");
        $projects = Project::select('projects.name', 'projects.id')
            ->leftJoin('assignment', 'projects.id', '=', 'assignment.project_id')
            ->where('assignment.employee_id', '=', $request->resource_id)
            ->whereIn('assignment.assignment_status', [2, 3])
            ->groupBy('projects.name')
            ->groupBy('projects.id')
            ->get();

        return response()->json($projects);
    }

    public function getClientProjects($resource_id)
    {
        $assignments = Assignment::with(['project' => function ($query) {
            $query->addSelect('id', 'name', 'customer_id')
                ->with(['customer' => function ($q) {
                    $q->select('id', 'customer_name');
                }]);
        }])->select('project_id')->where('employee_id', $resource_id)
            ->whereIn('assignment_status', [3])
            ->whereHas('project', function ($project) {
                $project->whereNotIn('status_id', [4, 5]);
            });

        $projects = $assignments->get()->map(function ($assignment) {
            return [
                'id' => $assignment->project_id,
                'name' => ($assignment->project->name ?? null).' ('.($assignment->project->customer->customer_name ?? null).')',
            ];
        })->toArray();

        return response()->json([
            'projects' => $projects,
            'active_projects' => $assignments->latest('end_date')->first()->project_id ?? null,
        ]);
    }

    public function sendDigiSign($id, Request $request): RedirectResponse
    {
        $subject = $request->input('subject');

        $timesheet = Timesheet::find($id);

        $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)->where('project_id', '=', $timesheet->project_id)->first();

        $project = Project::find($assignment->project_id);

        $employee = User::find($timesheet->employee_id);

        //$product_ownder = User::find($assignment->product_owner_id);

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();
        $time_exp = TimeExp::with(['account','account_element'])->where('timesheet_id', '=', $timesheet->id)->get();
        $timelines_drop_down = ['Select Group Task Number'];

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $total_exp_bill = 0;
        $total_exp_claim = 0;
        foreach ($time_exp as $expense) {
            $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
            $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
        }

        try {

            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $document = new Document();
            $document->type_id = 6;
            $document->document_type_id = 6; //document type Assignment(documenttype table in db)
            $document->name = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';
            $document->file = $file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = $employee->id;
            $document->digisign_status_id = 2;
            $document->reference_id = $timesheet->id;
            $document->digisign_approver_user_id = isset($product_ownder->id) ? $product_ownder->id : 0;
            $document->save();

            //Timesheet Approvers
            $project_manager_ts_user = null;
            $product_owner_ts_user = null;
            $project_owner_ts_user = null;
            $project_manager_new_ts_user = null;
            $line_manager_ts_user = null;
            $claim_approver_ts_user = null;
            $resource_manager_ts_user = null;

            if ($assignment->project_manager_ts_approver == 1) {
                $project_manager_ts_user = User::find($project->manager_id);
            }

            if ($assignment->product_owner_ts_approver == 1) {
                $product_owner_ts_user = User::find($assignment->product_owner_id);
            }

            if ($assignment->project_manager_new_ts_approver == 1) {
                $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
            }

            if ($assignment->line_manager_ts_approver == 1) {
                $line_manager_ts_user = User::find($assignment->line_manager_id);
            }

            if ($assignment->claim_approver_ts_approver == 1) {
                $claim_approver_ts_user = User::find($assignment->assignment_approver);
            }

            if ($assignment->resource_manager_ts_approver == 1) {
                $resource_manager_ts_user = User::find($assignment->resource_manager_id);
            }

            $resource_user = 0;
            if ($request->has('resource_email') && isset($employee)) {
                //Mail::to(trim($request->input('resource_email')))->send(new TimesheetDigiSignMail($document->id,1,$employee->first_name.' '.$employee->last_name, $subject));
                SendTimesheetDigiSignEmailJob::dispatch(trim($request->input('resource_email')), $document->id, 1, $employee->first_name.' '.$employee->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $employee->first_name.' '.$employee->last_name;
                $digisign_users->user_email = $request->resource_email;
                $digisign_users->user_id = $employee->id;
                $digisign_users->user_number = 1;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $resource_user = 1;
            }

            $project_manager_user = 0;
            if ($request->has('project_manager_email') && isset($project_manager_ts_user->email)) {
                //Mail::to(trim($request->project_manager_email))->send(new TimesheetDigiSignMail($document->id, 2, $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name, $subject));
                SendTimesheetDigiSignEmailJob::dispatch(trim($request->project_manager_email), $document->id, 2, $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $project_manager_ts_user->first_name.' '.$project_manager_ts_user->last_name;
                $digisign_users->user_email = $project_manager_ts_user->email;
                $digisign_users->user_id = $project_manager_ts_user->id;
                $digisign_users->user_number = 2;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $project_manager_user = 1;
            }

            $product_ownder_user = 0;
            if ($request->has('product_owner_email') && isset($product_owner_ts_user)) {
                //Mail::to(trim($request->product_owner_email))->send(new TimesheetDigiSignMail($document->id,3,$product_owner_ts_user->first_name.' '.$product_owner_ts_user->last_name, $subject));

                SendTimesheetDigiSignEmailJob::dispatch(trim($request->product_owner_email), $document->id, 3, $product_owner_ts_user->first_name.' '.$product_owner_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $product_owner_ts_user->first_name.' '.$product_owner_ts_user->last_name;
                $digisign_users->user_email = $request->product_owner_email;
                $digisign_users->user_id = $product_owner_ts_user->id;
                $digisign_users->user_number = 3;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();
                $product_ownder_user = 1;
            }

            $project_manager_new_user = 0;
            if ($request->has('project_manager_new_email') && isset($project_manager_new_ts_user->email)) {
                //Mail::to(trim($request->project_manager_new_email))->send(new TimesheetDigiSignMail($document->id, 4, $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name, $subject));

                SendTimesheetDigiSignEmailJob::dispatch(trim($request->project_manager_new_email), $document->id, 4, $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $project_manager_new_ts_user->first_name.' '.$project_manager_new_ts_user->last_name;
                $digisign_users->user_email = $request->project_manager_new_email;
                $digisign_users->user_id = $project_manager_new_ts_user->id;
                $digisign_users->user_number = 4;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $project_manager_new_user = 1;
            }

            $line_manager_user = 0;
            if ($request->has('line_manager_email') && isset($line_manager_ts_user->email)) {
                //Mail::to(trim($request->line_manager_email))->send(new TimesheetDigiSignMail($document->id, 5, $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name, $subject));

                SendTimesheetDigiSignEmailJob::dispatch(trim($request->line_manager_email), $document->id, 5, $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $line_manager_ts_user->first_name.' '.$line_manager_ts_user->last_name;
                $digisign_users->user_email = $request->line_manager_email;
                $digisign_users->user_id = $line_manager_ts_user->id;
                $digisign_users->user_number = 5;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $line_manager_user = 1;
            }

            $claim_approver_user = 0;
            if ($request->has('claim_approver_email') && isset($claim_approver_ts_user->email)) {
                //Mail::to(trim($request->claim_approver_email))->send(new TimesheetDigiSignMail($document->id, 6, $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name, $subject));

                SendTimesheetDigiSignEmailJob::dispatch(trim($request->claim_approver_email), $document->id, 6, $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $claim_approver_ts_user->first_name.' '.$claim_approver_ts_user->last_name;
                $digisign_users->user_email = $request->claim_approver_email;
                $digisign_users->user_id = $claim_approver_ts_user->id;
                $digisign_users->user_number = 6;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $claim_approver_user = 1;
            }

            $resource_manager_user = 0;
            if ($request->has('resource_manager_email') && isset($resource_manager_ts_user->email)) {
                //Mail::to(trim($request->resource_manager_email))->send(new TimesheetDigiSignMail($document->id, 7, $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name, $subject));

                SendTimesheetDigiSignEmailJob::dispatch(trim($request->resource_manager_email), $document->id, 7, $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name, $subject)->delay(now()->addMinutes(5));
                $digisign_users = new DigisignUsers();
                $digisign_users->document_id = $document->id;
                $digisign_users->user_name = $resource_manager_ts_user->first_name.' '.$resource_manager_ts_user->last_name;
                $digisign_users->user_email = $request->resource_manager_email;
                $digisign_users->user_id = $resource_manager_ts_user->id;
                $digisign_users->user_number = 7;
                $digisign_users->creator_id = auth()->id();
                $digisign_users->save();

                $resource_manager_user = 1;
            }

            $user_emails = str_replace('"', '', $request->user_email);
            $user_emails = str_replace('[', '', $user_emails);
            $user_emails = str_replace(']', '', $user_emails);

            $user_emails_ = explode(',', $user_emails);

            $other_users = [];
            if (! empty($user_emails_)) {
                $user_counter = 8;
                foreach ($user_emails_ as $user_email) {
                    if ($user_email != '') {
                        //Mail::to(trim($user_email))->send(new TimesheetDigiSignMail($document->id, $user_counter, $user_email, $subject));

                        SendTimesheetDigiSignEmailJob::dispatch(trim($user_email), $document->id, $user_counter, $user_email, $subject)->delay(now()->addMinutes(5));
                        $digisign_users = new DigisignUsers();
                        $digisign_users->document_id = $document->id;
                        $digisign_users->user_name = $user_email;
                        $digisign_users->user_email = $user_email;
                        $digisign_users->user_id = 0;
                        $digisign_users->user_number = $user_counter;
                        $digisign_users->creator_id = auth()->id();
                        $digisign_users->save();
                        $user_counter += 1;

                        $other_users[] = $user_counter;
                    }
                }
            }

            $digisign_users = DigisignUsers::where('document_id', '=', $document->id)->get();

            $parameters = [
                'timesheet' => $timesheet,
                'assignment' => $assignment,
                'timelines' => $timelines,
                'time_exp' => $time_exp,
                'employee' => $employee,
                'product_ownder' => $product_owner_ts_user,
                'total_exp_bill' => $total_exp_bill,
                'total_exp_claim' => $total_exp_claim,
                'hours' => $hours,
                'timelines_drop_down' => $timelines_drop_down,
                //'resource_user' => $resource_user,
                //'report_to_user' => $report_to_user,
                //'product_ownder_user' => $product_ownder_user,
                'other_users' => $other_users,
                //Timesheet approver users
                'project_manager_ts_user' => $project_manager_ts_user,
                'project_owner_ts_user' => $project_owner_ts_user,
                'project_manager_new_ts_user' => $project_manager_new_ts_user,
                'line_manager_ts_user' => $line_manager_ts_user,
                'claim_approver_ts_user' => $claim_approver_ts_user,
                'resource_manager_ts_user' => $resource_manager_ts_user,
                'resource_user' => $resource_user,
                'project_manager_user' => $project_manager_user,
                'product_ownder_user' => $product_ownder_user,
                'project_manager_new_user' => $project_manager_new_user,
                'line_manager_user' => $line_manager_user,
                'claim_approver_user' => $claim_approver_user,
                'resource_manager_user' => $resource_manager_user,
                'digisign_users' => $digisign_users,
            ];

            $pdf = Pdf::view('timesheet.digisign', $parameters);

            $timesheet_template_id = TimesheetTemplatesEnum::Standard?->value;
            if (isset($project->timesheet_template_id) && $project->timesheet_template_id > 0) {
                $timesheet_template_id = $project->timesheet_template_id;
            } else {
                $config_data = Config::orderBy('id')->first();
                if (isset($config_data->timesheet_template_id) && $config_data->timesheet_template_id > 0) {
                    $timesheet_template_id = $config_data->timesheet_template_id;
                }
            }

            if (($timesheet_template_id == TimesheetTemplatesEnum::Weekly?->value) || ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value)) {
                $timeline_weekly_details = [];
                $timeline_counter = 0;
                $timeline_minutes_total_billable = 0;
                $timeline_minutes_total_non_billable = 0;
                foreach ($timelines as $timeline) {
                    $date = Carbon::now();

                    $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                    $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                    //Monday
                    $mon_date[$timesheet->year_week] = $start_of_week_date;
                    $tue_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $wed_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $thu_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $fri_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sat_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sun_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));

                    $description_of_work = isset($timeline->task->description) ? $timeline->task->description : '';
                    if ($description_of_work != '') {
                        if ($timeline->description_of_work != '') {
                            $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                    } else {
                        $description_of_work = $timeline->description_of_work;
                    }

                    if ((($timeline->mon * 60) + $timeline->mon_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->mon * 60) + $timeline->mon_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->tue * 60) + $timeline->tue_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->tue * 60) + $timeline->tue_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $tue_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->wed * 60) + $timeline->wed_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->wed * 60) + $timeline->wed_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $wed_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->thu * 60) + $timeline->thu_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->thu * 60) + $timeline->thu_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $thu_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->fri * 60) + $timeline->fri_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->fri * 60) + $timeline->fri_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $fri_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sat * 60) + $timeline->sat_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sat * 60) + $timeline->sat_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sat_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sun * 60) + $timeline->sun_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sun * 60) + $timeline->sun_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sun_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }
                }

                $total_time_billable = minutesToTime($timeline_minutes_total_billable);
                $total_time_non_billable = minutesToTime($timeline_minutes_total_non_billable);

                $parameters['timeline_weekly_details'] = $timeline_weekly_details;
                $parameters['total_time_billable'] = $total_time_billable;
                $parameters['total_time_non_billable'] = $total_time_non_billable;

                $pdf = Pdf::view('timesheet.weekly_detail_print', $parameters);

                if ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value) {
                    $pdf = Pdf::view('timesheet.weekly_detail_arbour_print', $parameters);
                }
            }

            //$pdf->save($storage);
            $pdf->save($file_name_pdf);

            activity()->on($timesheet)->log('emailed for digisign');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('timesheet', $timesheet))->with('flash_danger', $e->getMessage());
        }

        return redirect(route('timeline.show', $timesheet))->with('flash_success', 'Digisign successfully sent');
    }

    public function emailTimesheet($id, Request $request)
    {
        $subject = $request->input('subject');

        $timesheet = Timesheet::find($id);

        $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)->where('project_id', '=', $timesheet->project_id)->first();

        $employee = User::find($timesheet->employee_id);

        $product_ownder = User::find($assignment->product_owner_id);

        $project = Project::find($assignment->project_id);

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();
        $time_exp = TimeExp::where('timesheet_id', '=', $timesheet->id)->get();
        $timelines_drop_down = ['Select Group Task Number'];
        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $total_exp_bill = 0;
        $total_exp_claim = 0;
        foreach ($time_exp as $expense) {
            $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
            $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
        }

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $document = new Document();
            $document->type_id = 6;
            $document->document_type_id = 6; //document type Assignment(documenttype table in db)
            $document->name = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.')';
            $document->file = $file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = $employee->id;
            $document->digisign_status_id = 2;
            $document->reference_id = $timesheet->id;
            $document->digisign_approver_user_id = (isset($product_ownder->id) ? $product_ownder->id : 0);
            $document->save();

            //Timesheet Approvers
            $project_manager_ts_user = null;
            $product_owner_ts_user = null;
            $project_owner_ts_user = null;
            $project_manager_new_ts_user = null;
            $line_manager_ts_user = null;
            $claim_approver_ts_user = null;
            $resource_manager_ts_user = null;

            $digisign_users = [];

            $tmp_user = new \stdClass();
            $tmp_user->user_number = 1;
            $tmp_user->signed = 0;
            $tmp_user->user_name = '';
            $tmp_user->user_sign_date = '';
            $digisign_users[] = $tmp_user;

            if ($assignment->project_manager_ts_approver == 1) {
                $project_manager_ts_user = User::find($project->manager_id);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 2;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            if ($assignment->product_owner_ts_approver == 1) {
                $product_owner_ts_user = User::find($assignment->product_owner_id);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 3;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            if ($assignment->project_manager_new_ts_approver == 1) {
                $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 4;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            if ($assignment->line_manager_ts_approver == 1) {
                $line_manager_ts_user = User::find($assignment->line_manager_id);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 5;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            if ($assignment->claim_approver_ts_approver == 1) {
                $claim_approver_ts_user = User::find($assignment->assignment_approver);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 6;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            if ($assignment->resource_manager_ts_approver == 1) {
                $resource_manager_ts_user = User::find($assignment->resource_manager_id);
                $tmp_user = new \stdClass();
                $tmp_user->user_number = 7;
                $tmp_user->signed = 0;
                $tmp_user->user_name = '';
                $tmp_user->user_sign_date = '';
                $digisign_users[] = $tmp_user;
            }

            $user_emails = str_replace('"', '', $request->user_email);
            $user_emails = str_replace('[', '', $user_emails);
            $user_emails = str_replace(']', '', $user_emails);

            $user_emails_ = explode(',', $user_emails);

            $other_users = [];
            if (! empty($user_emails_)) {
                $user_counter = 4;
                foreach ($user_emails_ as $user_email) {
                    if ($user_email != '') {
                        //Mail::to(trim($user_email))->send(new TimesheetMail($document->id, $user_counter, $user_email, $subject));

                        SendTimesheetDigiSignEmailJob::dispatch(trim($user_email), $document->id, $user_counter, $user_email, $subject)->delay(now()->addMinutes(5));
                        $digisign_users2 = new DigisignUsers();
                        $digisign_users2->document_id = $document->id;
                        $digisign_users2->user_name = $user_email;
                        $digisign_users2->user_email = $user_email;
                        $digisign_users2->user_id = 0;
                        $digisign_users2->user_number = $user_counter;
                        $digisign_users2->creator_id = auth()->id();
                        $digisign_users2->save();
                        $user_counter += 1;

                        $other_users[] = $user_counter;
                    }
                }
            }

            $parameters = [
                'timesheet' => $timesheet,
                'assignment' => $assignment,
                'timelines' => $timelines,
                'time_exp' => $time_exp,
                'employee' => $employee,
                'product_ownder' => $product_ownder,
                'total_exp_bill' => $total_exp_bill,
                'total_exp_claim' => $total_exp_claim,
                'hours' => $hours,
                'timelines_drop_down' => $timelines_drop_down,
                'digisign_users' => $digisign_users,

            ];

            $pdf = Pdf::view('timesheet.digisign', $parameters);

            $timesheet_template_id = TimesheetTemplatesEnum::Standard?->value;
            if (isset($project->timesheet_template_id) && $project->timesheet_template_id > 0) {
                $timesheet_template_id = $project->timesheet_template_id;
            } else {
                $config_data = Config::orderBy('id')->first();
                if (isset($config_data->timesheet_template_id) && $config_data->timesheet_template_id > 0) {
                    $timesheet_template_id = $config_data->timesheet_template_id;
                }
            }

            if (($timesheet_template_id == TimesheetTemplatesEnum::Weekly?->value) || ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value)) {
                $timeline_weekly_details = [];
                $timeline_counter = 0;
                $timeline_minutes_total_billable = 0;
                $timeline_minutes_total_non_billable = 0;
                foreach ($timelines as $timeline) {
                    $date = Carbon::now();

                    $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                    $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                    //Monday
                    $mon_date[$timesheet->year_week] = $start_of_week_date;
                    $tue_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $wed_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $thu_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $fri_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sat_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sun_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));

                    $description_of_work = (isset($timeline->task->description) ? $timeline->task->description : '');
                    if ($description_of_work != '') {
                        if ($timeline->description_of_work != '') {
                            $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                    } else {
                        $description_of_work = $timeline->description_of_work;
                    }

                    if ((($timeline->mon * 60) + $timeline->mon_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->mon * 60) + $timeline->mon_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->tue * 60) + $timeline->tue_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->tue * 60) + $timeline->tue_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $tue_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->wed * 60) + $timeline->wed_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->wed * 60) + $timeline->wed_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $wed_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->thu * 60) + $timeline->thu_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->thu * 60) + $timeline->thu_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $thu_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->fri * 60) + $timeline->fri_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->fri * 60) + $timeline->fri_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $fri_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sat * 60) + $timeline->sat_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sat * 60) + $timeline->sat_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sat_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sun * 60) + $timeline->sun_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sun * 60) + $timeline->sun_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sun_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = (isset($timesheet->project->ref) ? $timesheet->project->ref : '');
                        $timeline_weekly_details[$timeline_counter]['project_name'] = (isset($timesheet->project->name) ? $timesheet->project->name : '');
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }
                }

                $total_time_billable = minutesToTime($timeline_minutes_total_billable);
                $total_time_non_billable = minutesToTime($timeline_minutes_total_non_billable);

                $parameters['timeline_weekly_details'] = $timeline_weekly_details;
                $parameters['total_time_billable'] = $total_time_billable;
                $parameters['total_time_non_billable'] = $total_time_non_billable;

                $pdf = Pdf::view('timesheet.weekly_detail_print', $parameters);

                if ($timesheet_template_id == TimesheetTemplatesEnum::Arbour?->value) {
                    $pdf = Pdf::view('timesheet.weekly_detail_arbour_print', $parameters);
                }
            }

            $pdf->format('a4')->save($file_name_pdf);
            activity()->on($timesheet)->log('emailed');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('timesheet', $timesheet))->with('flash_danger', $e->getMessage());
        }

        if ($request->ajax()) {
            return response()->json('Success');
        }

        return redirect(route('timesheet.show', $timesheet))->with('flash_success', 'Digisign successfully sent');
    }

    public function addAttachment($id): View
    {
        $timesheet = Timesheet::find($id);

        $parameters = [
            'timesheet' => $timesheet,
        ];

        return view('timesheet.add_attachment')->with($parameters);
    }

    public function saveAttachment( $id,TimeSheetAttachmentRequest $request)
    {
        // return $request->file();
        $timesheet = Timesheet::find($id);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = 'timesheet_attachment_'.$timesheet->id.'_'.Carbon::now()->format('Y_m_d').'_'.strtotime(Carbon::now()).'.'.$file->getClientOriginalExtension();
            $stored = $file->storeAs('attachments', $name);

            $document = new Document();
            $document->type_id = 7;
            $document->name = $request->input('description');
            $document->file = $name;
            $document->document_type_id = 7;
            $document->reference_id = $id;
            $document->creator_id = Auth::id();
            $document->owner_id = $timesheet->employee_id;
            $document->save();
        }


        if ($request->ajax()) {
            $attachments = Document::where('reference_id', '=', $timesheet->id)->where('document_type_id', '=', 7)->get();

            return response()->json($attachments);
        }

        return redirect(route('timesheet.show', $id))->with('flash_success', 'Attachment added successfully');
    }

    public function previous_timesheets($user): JsonResponse
    {
        $previous_timesheets = new PreviousTimesheet();

        return response()->json($previous_timesheets->previousTimesheets($user));
    }

    public function print(Request $request, $timesheet_id)
    {
        $timesheet = Timesheet::with(['timeline' => function ($query) {
            $query->select('timesheet_id', 'description_of_work', 'is_billable', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun', 'total', 'mon_m', 'tue_m', 'wed_m', 'thu_m', 'fri_m', 'sat_m', 'sun_m', 'total_m', 'task_id')->with(['task' => function ($task) {
                $task->select('id', 'description');
            }]);
        }, 'project' => function ($proj) {
            $proj->select('id', 'ref', 'name', 'manager_id');
        }, 'employee' => function ($q) {
            $q->select('id', 'first_name', 'last_name');
        }, 'time_exp'])->select('id', 'project_id', 'employee_id', 'week', 'first_day_of_week', 'last_day_of_week', 'year', 'month', 'first_date_of_week', 'last_date_of_week', 'last_date_of_month')
            ->where('id', $timesheet_id)->first();

        if (! Storage::exists('timesheets/')) {
            Storage::makeDirectory('timesheets/');
        }

        try {
            $storage = storage_path('app/timesheets/'.str_replace(" ", '', $timesheet->employee?->name()).'.pdf');

            $timeline_minutes_total_billable = $timesheet->timeline->sum(function ($timeline) {
                if ($timeline->is_billable == 1) {
                    return ($timeline->total * 60) + $timeline->total_m;
                }
            });

            $timeline_minutes_total_non_billable = $timesheet->timeline->sum(function ($line) {
                if ((int) ! $line->is_billable) {
                    return (int) (($line->total * 60) + $line->total_m);
                }
            });

            $total_exp_bill = $timesheet->time_exp->sum(function ($exp) {
                if ($exp->claim == 1) {
                    return $exp->amout;
                }
            });

            $total_exp_claim = $timesheet->time_exp->sum(function ($exp) {
                if ($exp->is_billable == 1) {
                    return $exp->amout;
                }
            });

            $helper = new TimesheetTemplate();
            $assignment = $helper->assignment((object) [
                'employee_id' => $timesheet->employee_id,
                'project_id' => $timesheet->project_id, ]
            );
            $_digi_sign = $this->billingDigiSign($assignment, $assignment->project);

            $parameters = [
                'timeline_weekly_details' => $helper->weeklyTemplateData($timesheet),
                'total_time_billable' => $timeline_minutes_total_billable,
                'total_time_non_billable' => $timeline_minutes_total_non_billable,
                'employee' => $timesheet->employee,
                'timesheet' => $timesheet,
                'timelines' => $timesheet->timeline,
                'time_exp' => $timesheet->time_exp,
                'total_exp_bill' => $total_exp_bill,
                'total_exp_claim' => $total_exp_claim,
                'digisign_users' => $helper->digiSignUsers($_digi_sign, $assignment, $timesheet->user)->flatten(),
            ];

            switch ($request->template_id) {
                case TimesheetTemplatesEnum::Standard?->value:
                    $pdf = Pdf::view('timesheet.print', $parameters);
                    break;
                case TimesheetTemplatesEnum::Weekly?->value:
                    $pdf = Pdf::view('timesheet.weekly_detail_print', $parameters);
                    break;
                case TimesheetTemplatesEnum::Arbour?->value:
                    $pdf = Pdf::view('timesheet.weekly_detail_arbour_print', $parameters);
                    break;
            }

            $pdf->format('a4')->save($storage);

            $document = new Document();
            //$document->type_id = $template->id;
            $document->document_type_id = 5;
            $document->name = 'Customer Tax Invoice';
            $document->file = (isset($timesheet->user) ? $timesheet->user->first_name.'_'.$timesheet->user->last_name : '').'/'.(isset($timesheet->user) ? substr($timesheet->user->first_name, 0, 4).''.substr($timesheet->user->last_name, 0, 4) : '').(isset($timesheet->project) ? str_replace(' ', '_', $timesheet->project->name) : '').($timesheet->year_week).'_'.date('Y-m-d_H_i_s').'.pdf';
            $document->creator_id = auth()->id();
            $document->owner_id = $timesheet->employee_id;
            //$document->digisign_status_id = 2;
            //$document->reference_id = $timesheets[0]->project->ref;
            //$document->digisign_approver_user_id = $assignment_approver_resource->id;
            $document->save();

            return response()->file($storage);
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return response()->json(['Message' => $e->getMessage()]);
        }
    }

    private function notifications($timesheet)
    {
        $notification = new Notification;
        $notification->name = 'Maximum hours per day exceeded';
        $notification->link = route('timesheet.show', $timesheet->id);
        $notification->save();

        $user_notifications = [];
        array_push($user_notifications, [
            'user_id' => $timesheet->employee_id,
            'notification_id' => $notification->id,
        ]);

        UserNotification::insert($user_notifications);
    }

    private function yearWeekDropDown()
    {
        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $date_today = date('Y-m-d');
        $year_2020_week_1_flag = false;
        if (($year == 2019) && ($date_today == '2019-12-31')) {
            $year += 1;
            $year_2020_week_1_flag == true;
        }

        $config = Config::select(['timesheet_weeks', 'timesheet_weeks_future'])->find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $number_of_weeks_future = isset($config->timesheet_weeks_future) && (int) $config->timesheet_weeks_future > 0 ? $config->timesheet_weeks_future : 0;

        if (($week + $number_of_weeks_future) <= 53) {
            $week += $number_of_weeks_future;
        }

        //$week_drop_down[0] = "Select Week";

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week <= 6) {
                    if (($i == 1) && ($year == 2020)) {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$year.'-'.$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'(2019-12-31)-1';
                    } else {
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                        $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';
                    }

                    continue;
                }

                // Disable first week for 2021, this worked for 2020
                /*if($i == 1){
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_2'] = $year.($i<10?'0'.$i:$i)."(".date('Y-m')."-01)-2";
                    $week_drop_down[$year.($i<10?'0'.$i:$i).'_1'] = $year.($i<10?'0'.$i:$i)."(".$date2->format('Y-m-d').")-1";
                }
                else{*/
                $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                /*}*/
            }

            $number_of_weeks -= $week;
            $week = 53;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week <= 6) {
                // Weird week 53 for 2021
                if ($i == 53) {
                    $week_drop_down[$year.$i.'_2'] = ($year + 1).$i.'(2021-'.$week_two_month.'-01)-2';
                } else {
                    $week_drop_down[$year.$i.'_2'] = $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2';
                }
                $week_drop_down[$year.$i.'_1'] = $year.$i.'('.$date2->format('Y-m-d').')-1';

                continue;
            }
            $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
        }

        return $week_drop_down;
    }

    private function digiSign($assignment, $project)
    {
        $project_manager_ts_user = null;
        $project_owner_ts_user = null;
        $project_manager_new_ts_user = null;
        $line_manager_ts_user = null;
        $claim_approver_ts_user = null;
        $resource_manager_ts_user = null;

        $digisign_users = [];

        $digisign_users[] = $this->digiSignTmpUser(1);

        if ($assignment->project_manager_ts_approver == 1) {
            $project_manager_ts_user = User::find($project->manager_id);

            $digisign_users[] = $this->digiSignTmpUser(2);
        }

        if ($assignment->product_owner_ts_approver == 1) {
            $project_owner_ts_user = User::find($assignment->product_owner_id);

            $digisign_users[] = $this->digiSignTmpUser(3);
        }

        if ($assignment->project_manager_new_ts_approver == 1) {
            $project_manager_new_ts_user = User::find($assignment->project_manager_new_id);

            $digisign_users[] = $this->digiSignTmpUser(4);
        }

        if ($assignment->line_manager_ts_approver == 1) {
            $line_manager_ts_user = User::find($assignment->line_manager_id);

            $digisign_users[] = $this->digiSignTmpUser(5);
        }

        if ($assignment->claim_approver_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);

            $digisign_users[] = $this->digiSignTmpUser(6);
        }

        if ($assignment->resource_manager_ts_approver == 1) {
            $claim_approver_ts_user = User::find($assignment->assignment_approver);

            $digisign_users[] = $this->digiSignTmpUser(7);
        }

        return [
            'digisign_users' => $digisign_users,
            'resource_manager_ts_user' => $resource_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_ts_user' => $project_manager_ts_user,
        ];
    }

    private function billingDigiSign($assignment, $project)
    {
        $project_manager_ts_user = null;
        $project_owner_ts_user = null;
        $project_manager_new_ts_user = null;
        $line_manager_ts_user = null;
        $claim_approver_ts_user = null;
        $resource_manager_ts_user = null;

        $digisign_users = [];

        $digisign_users[] = $this->digiSignTmpUser(1);

        if ($assignment->project_manager_ts_approver?->value == 1) {

            $approvalrole = $this->approver($project->manager_id);
            if (isset($approvalrole->substitute_approver)) {
                $project_manager_ts_user = $approvalrole->substitute_approver;
            } else {
                $PMTS_user = User::find($project->manager_id);
                $project_manager_ts_user = $PMTS_user->name();
            }

            $digisign_users[] = $this->digiSignTmpUser(2);
        }

        if ($assignment->product_owner_ts_approver?->value == 1) {
            $approvalrole = $this->approver($assignment->product_owner_id);
            if (isset($approvalrole->substitute_approver)) {
                $project_owner_ts_user = $approvalrole->substitute_approver;
            } else {
                $POTS_user = User::find($assignment->product_owner_id);
                $project_owner_ts_user = $POTS_user->name();
            }

            $digisign_users[] = $this->digiSignTmpUser(3);
        }

        if ($assignment->project_manager_new_ts_approver?->value == 1) {
            $approvalrole = $this->approver($assignment->project_manager_new_id);
            if (isset($approvalrole->substitute_approver)) {
                $project_manager_new_ts_user = $approvalrole->substitute_approver;
            } else {
                $PMNTS_user = User::find($assignment->project_manager_new_id);
                $project_manager_new_ts_user = $PMNTS_user->name();
            }

            $digisign_users[] = $this->digiSignTmpUser(4);
        }

        if ($assignment->line_manager_ts_approver?->value == 1) {
            $approvalrole = $this->approver($assignment->line_manager_id);
            if (isset($approvalrole->substitute_approver)) {
                $line_manager_ts_user = $approvalrole->substitute_approver;
            } else {
                //dd($assignment->line_manager_id);
                $LMTS_user = User::find($assignment->line_manager_id);
                $line_manager_ts_user = isset($LMTS_user) ? $LMTS_user->name() : null;
            }

            $digisign_users[] = $this->digiSignTmpUser(5);
        }

        if ($assignment->claim_approver_ts_approver?->value == 1) {
            $approvalrole = $this->approver($assignment->assignment_approver);
            if (isset($approvalrole->substitute_approver)) {
                $claim_approver_ts_user = $approvalrole->substitute_approver;
            } else {
                $CATS_user = User::find($assignment->assignment_approver);
                $claim_approver_ts_user = $CATS_user->name();
            }

            $digisign_users[] = $this->digiSignTmpUser(6);
        }

        if ($assignment->resource_manager_ts_approver?->value == 1) {
            $approvalrole = $this->approver($assignment->resource_manager_id);
            if (isset($approvalrole->substitute_approver)) {
                $resource_manager_ts_user = $approvalrole->substitute_approver;
            } else {
                $RMTS_user = User::find($assignment->resource_manager_id);
                $resource_manager_ts_user = $RMTS_user ? $RMTS_user->name() : null;
            }

            $digisign_users[] = $this->digiSignTmpUser(7);

        }

        return [
            'digisign_users' => $digisign_users,
            'resource_manager_ts_user' => $resource_manager_ts_user,
            'claim_approver_ts_user' => $claim_approver_ts_user,
            'line_manager_ts_user' => $line_manager_ts_user,
            'project_manager_new_ts_user' => $project_manager_new_ts_user,
            'project_owner_ts_user' => $project_owner_ts_user,
            'project_manager_ts_user' => $project_manager_ts_user,
        ];
    }

    private function digiSignTmpUser($user_number)
    {
        $tmp_user = new \stdClass();
        $tmp_user->user_number = $user_number;
        $tmp_user->signed = 0;
        $tmp_user->user_name = '';
        $tmp_user->user_sign_date = '';

        return $tmp_user;
    }

    private function weeks(Config $config, int $billingCycle = 0)
    {
        $weeks = [];
        $date = Carbon::now();
        $config_weeks = (int) ($config->timesheet_weeks ?? 12);
        $ceil_week = $date->copy()->addWeeks((int) $config->timesheet_weeks_future)->endOfWeek();
        $floor_week = $date->copy()->subWeeks($config_weeks)->startOfWeek();
        $billing_periods = BillingPeriod::query()
            ->where('billing_cycle_id', $billingCycle)
            ->whereBetween('start_date', [$floor_week->copy()->toDateString(), $ceil_week->copy()->toDateString()])
            ->orderBy('start_date', 'ASC')
            ->get();

        if ($billing_periods->isEmpty() || ! $billingCycle) {
            $weeks = collect($this->yearWeekDropDown())->map(function ($week, $key) {
                return [
                    'year_week' => (string) $key,
                    'date' => $week,
                ];
            })->sortByDesc('year_week')->values();
        } else {
            foreach ($billing_periods as $period) {
                for ($i = Carbon::parse($period->start_date)->startOfWeek(); $i <= Carbon::parse($period->end_date)->endOfWeek(); $i->addWeeks(1)) {
                    $week = str_pad($i->copy()->weekOfYear, 2, '0', STR_PAD_LEFT);
                    $year_week = $i->copy()->year.$week;

                    if ($i->copy()->toDateString() > now()->addWeeks(((int) $config->timesheet_weeks_future) ?? 0)->endOfWeek()->toDateString()) {
                        break;
                    }

                    $split_2_start = Carbon::parse($period->end_date);
                    $short_week = $i->copy()->startOfWeek()->diffInDays($split_2_start);

                    if ($short_week < 6) {
                        array_push($weeks, [
                            'year_week' => ($year_week == 202401 ? '202453_1' : $year_week).'_1',
                            'date' => ($year_week == 202401 ? 202453 : $year_week).'('.$i->copy()->toDateString().')-1',
                            'period' => $period->id,
                        ]);
                    } elseif ((Carbon::parse($period->start_date)->diffInDays($i->copy()->endOfWeek()) < 6) && Carbon::parse($period->start_date) <= now()) {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? '202501_2' : $year_week.'_2',
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.Carbon::parse($period->start_date)->toDateString().')-2',
                            'period' => $year_week == 202401 ? $period->id+1 : $period->id,
                        ]);
                    } else {
                        array_push($weeks, [
                            'year_week' => $year_week == 202401 ? 202501 : $year_week,
                            'date' => ($year_week == 202401 ? 202501 : $year_week).'('.$i->copy()->toDateString().')',
                            'period' => $period->id,
                        ]);
                    }
                }
            }
        }

        return collect($weeks)->sortByDesc('year_week')->values();
    }

    public function timesheetYearWeek(Project $project): JsonResponse
    {
        $project = $project->load('customer');
        $config = Config::select(['billing_cycle_id', 'timesheet_weeks', 'timesheet_weeks_future'])->first();
        if ($project->billing_cycle_id) {
            $billing_cycle = $project->billing_cycle_id;
        } elseif ($project->customer->billing_cycle_id) {
            $billing_cycle = $project->customer->billing_cycle_id;
        } else {
            $billing_cycle = $config->billing_cycle_id ?? null;
        }

        return response()->json($this->weeks($config, $billing_cycle));
    }

    private function strcontains(array $week_days): bool
    {
        foreach ($week_days as $day) {
            if (strpos($day, substr($day, 0, 3)) !== false) {
                return true;
            }
        }

        return false;
    }

    public function printBillingCycle(Request $request, BillingPeriod $billingperiod, $template_id)
    {
        $helper = new TimesheetTemplate();
        // dd($helper->gridTemplateTimesheets($request));
        if (empty($request->timesheets_id)) {
            return redirect()->back()->with('flash_danger', 'Please select timesheets you want to print');
        }

        $user = User::select('first_name', 'last_name')->find($request->employee_id);

        $assignment = $helper->assignment($request);

        $custom_style = null;
        if ($request->exists('custom_template_id')) {
            $custom_style = BillingPeriodStyle::find($request->custom_template_id);
            $template_id = $custom_style->template_id;
        }

        if ($template_id == TimesheetTemplatesEnum::Weekly?->value) {
            $timesheets = $helper->horizontalTemplateTimesheets($request);

            $first_days_of_week = $timesheets->map(function ($q) {
                return $q->first_day_of_week;
            })->unique();

            $week_days = $timesheets->groupBy('description');

            if (! empty($request->timesheets_id) && $timesheets->isEmpty()) {
                return redirect()->back()->with('flash_info', 'The template you selected use billable timesheets');
            }

            $start_date = $timesheets[0]->first_day_of_month ?? '0000-00-00';
        } elseif (in_array($template_id, [TimesheetTemplatesEnum::Monthly?->value, TimesheetTemplatesEnum::ArbourMonthly?->value, TimesheetTemplatesEnum::ArbourMonthlyGreen?->value])) {
            $timesheets = Timesheet::with(['employee' => function ($user) {
                $user->select(['id', 'first_name', 'last_name']);
            }, 'timeline' => function ($timeline) {
                $timeline->selectRaw('CASE WHEN IFNULL(task_id, 0) = 0 THEN description_of_work ELSE
                    (SELECT tasks.description FROM tasks WHERE tasks.id = timeline.task_id) END AS description, 
                    ((mon * 60) + mon_m) AS monday, ((tue * 60) + tue_m) AS tuesday, ((wed * 60) + wed_m) AS wednesday, 
                    ((thu * 60) + thu_m) AS thursday, ((fri * 60) + fri_m) AS friday, ((sat * 60) + sat_m) AS saturday,
                    ((sun * 60) + sun_m) AS sunday, timesheet_id, is_billable
                ');
            }, 'project' => function ($project) {
                $project->select(['id', 'name', 'ref', 'customer_po']);
            }])->select(
                [
                    'id', 'project_id', 'employee_id', 'first_day_of_week',
                    'last_day_of_week', 'first_day_of_month', 'last_day_of_month',
                ]
            )->whereIn('id', $request->timesheets_id)->get();

            $monthly_data = (object) [
                'name' => $timesheets[0]->employee->name() ?? 'No user',
                'billing_period' => $billingperiod->period_name,
                'from' => $billingperiod->start_date,
                'to' => $billingperiod->end_date,
                'po_number' => $timesheets[0]->project->customer_po ?? null,
                'monthly_data' => $helper->monthlyTemplateData($billingperiod, $timesheets),
            ];
            /*return $billingperiod;
            return $helper->monthlyTemplateData($billingperiod, $timesheets);*/

            $week_days = collect([]);
        } elseif ($template_id != TimesheetTemplatesEnum::Arbour?->value) {
            $timesheet = $helper->gridTemplateTimesheets($request);

            if (! empty($request->timesheets_id) && $timesheet->isEmpty()) {
                return redirect()->back()->with('flash_info', 'The template you selected use billable timesheets');
            }

            $week_days = $timesheet->map(function ($time) {
                return [
                    'start_date' => Carbon::parse($time->first_day_of_week)->startOfWeek(),
                    'monday' => $time->monday,
                    'tuesday' => $time->tuesday,
                    'wednesday' => $time->wednesday,
                    'thursday' => $time->thursday,
                    'friday' => $time->friday,
                    'saturday' => $time->saturday,
                    'sunday' => $time->sunday,
                    'total' => $time->total,
                ];
            });
        } else {
            $timesheet = $helper->verticalTemplateTimesheets($request);
            $week_days = $helper->verticalTemplateData($timesheet);
        }

        $_digi_sign = $this->billingDigiSign($assignment, $assignment->project);

        $digi_sign = $helper->digiSignUsers($_digi_sign, $assignment, $user);

        if (! Storage::exists('billing-cycle/billing-period/')) {
            Storage::makeDirectory('billing-cycle/billing-period/');
        }
        try {
            $storage = storage_path('app'.DIRECTORY_SEPARATOR.'billing-cycle'.DIRECTORY_SEPARATOR.'billing-period'.DIRECTORY_SEPARATOR.str_replace(" ", '_', $user->name()).'_'.time().'.pdf');

            $header = '<p style="padding: 25px"></p>';
            $footer = 'Powered By Consulteaze';

            $logos = [];
            if (! isset($custom_style)
                || (! isset($custom_style->left_logo)
                    && ! isset($custom_style->center_logo)
                    && ! isset($custom_style->right_logo))) {
                array_push($logos, public_path('assets'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'default.jpg'));
            }

            if (isset($custom_style)) {
                if (isset($custom_style->left_logo) && $custom_style->left_logo == 1) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'customer'.DIRECTORY_SEPARATOR.$assignment->project->customer->logo));
                } elseif (isset($custom_style->left_logo) && $custom_style->left_logo == 2) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.$assignment->project->company->company_logo));
                } elseif ($custom_style->left_logo != null) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.$custom_style->left_logo));
                }

                if (isset($custom_style->center_logo) && $custom_style->center_logo == 1) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'customer'.DIRECTORY_SEPARATOR.$assignment->project->customer->logo));
                } elseif (isset($custom_style->center_logo) && $custom_style->center_logo == 2) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.$assignment->project->company->company_logo));
                } elseif ($custom_style->center_logo != null) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.$custom_style->center_logo));
                }

                if (isset($custom_style->right_logo) && $custom_style->right_logo == 1) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'customer'.DIRECTORY_SEPARATOR.$assignment->project->customer->logo));
                } elseif (isset($custom_style->right_logo) && $custom_style->right_logo == 2) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'avatars'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR.$assignment->project->company->company_logo));
                } elseif ($custom_style->right_logo != null) {
                    array_push($logos, storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.$custom_style->right_logo));
                }
            }

            if (isset($custom_style)) {
                if (($custom_style->left_logo == 1 || $custom_style->center_logo == 1 || $custom_style->right_logo == 1)
                    && ($assignment->project->customer->logo == '' || ! isset($assignment->project->customer->logo))) {
                    $details = [
                        'id' => $assignment->project->customer_id ?? 0,
                        'name' => $assignment->project->customer->customer_name ?? null,
                    ];
                    $this->logoNotFound($details, 'customer');
                }

                if (($custom_style->left_logo == 2 || $custom_style->center_logo == 2 || $custom_style->right_logo == 2)
                    && ($assignment->project->company->company_logo == '' || ! isset($assignment->project->company->company_logo))) {
                    $details = [
                        'id' => $assignment->project->company_id ?? 0,
                        'name' => $assignment->project->company->company_name ?? null,
                    ];
                    $this->logoNotFound($details, 'company');
                }
            }

            $parameters = [
                'user' => $user,
                'logos' => array_filter($logos),
                'project' => $assignment,
                'digisign' => $digi_sign->flatten(1),
                'week_days' => $week_days,
                'bill_total' => $week_days->sum('_total'),
                'non_bill_total' => $week_days->sum('non_bill_total_in_min'),
                'custom_style' => $custom_style,
                'billing_period' => $billingperiod,
            ];

            switch ($template_id) {
                case TimesheetTemplatesEnum::Standard?->value:
                    //return \view('billing_cycle_template.grid', $parameters);
                    Pdf::view('billing_cycle_template.grid', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->save($storage);
                    break;
                case TimesheetTemplatesEnum::Weekly?->value:
                    $parameters['start_date'] = $start_date ?? '0000-00-00';
                    $parameters['start_of_weeks'] = $first_days_of_week;

                    Pdf::view('billing_cycle_template.horizontal', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->landscape()
                        ->save($storage);
                    break;
                case TimesheetTemplatesEnum::Arbour?->value:
                    $parameters['date_from'] = $timesheet[0]['begining_of_month'] ?? '0000-00-00';
                    $parameters['date_to'] = $timesheet[0]['end_of_month'] ?? '0000-00-00';
                    Pdf::view('billing_cycle_template.vertical', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->save($storage);
                    break;
                case TimesheetTemplatesEnum::Monthly?->value:
                    $parameters['monthly_data'] = $monthly_data;
                    Pdf::view('billing_cycle_template.monthly_print', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->save($storage);
                    break;
                case TimesheetTemplatesEnum::ArbourMonthly?->value:
                    $parameters['monthly_data'] = $monthly_data;

                    Pdf::view('billing_cycle_template.arbour_monthly_print', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->save($storage);
                    break;
                case TimesheetTemplatesEnum::ArbourMonthlyGreen?->value:
                    $parameters['monthly_data'] = $monthly_data;
                    Pdf::view('billing_cycle_template.arbour_monthly_green_print', $parameters)
                        ->format('a4')
                        ->headerHtml($header)
                        ->footerHtml($footer)
                        ->save($storage);
                    break;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
        try {
            return response()->file($storage);
        }catch (FileNotFoundException $exception){
            return redirect()->back()->with('flash_danger', 'File not fount');
        }

    }

    public function billingTimesheets(Request $request)
    {
        $billingperiod = BillingPeriod::with('billingCycle')->find($request->billing_period_id);

        $timesheets = Timesheet::selectRaw('id, employee_id, project_id, customer_id, year_week')
            ->with(['employee' => function ($user) {
                $user->select('id', 'first_name', 'last_name');
            }, 'project' => function ($project) {
                $project->select('id', 'name');
            }, 'customer' => function ($customer) {
                $customer->select('id', 'customer_name');
            }, 'timeline' => function ($query) {
                $query->select('id', 'timesheet_id', 'total', 'total_m', 'is_billable');
            }, 'time_exp' => function ($expense) {
                $expense->select('id', 'timesheet_id', 'is_billable', 'amount');
            }])
            ->where(['employee_id' => $request->employee_id, 'project_id' => $request->project_id])
            ->where(function($timesheet) use($billingperiod){
                $timesheet->where(function ($time) use($billingperiod){
                    $time->whereNotNull('billing_period_id')
                        ->where('billing_period_id', $billingperiod->id);
                })
                    ->orWhere(function($time) use($billingperiod){
                        // $time->whereNull('billing_period_id')
                        //     ->where('first_day_of_month', $billingperiod->start_date)
                        //     ->where('last_day_of_month', $billingperiod->end_date);
                        $time->where('first_day_of_month', $billingperiod->start_date)
                            ->where('last_day_of_month', $billingperiod->end_date);
                    });
            })
            // ->whereHas('timeline')
            ->orderBy('year_week')
            ->get();

        if ($timesheets->isEmpty()) {
            return response()->json([]);
        }

        $timesheets = $timesheets->map(function ($time) {
            return [
                'id' => $time->id,
                'customer' => $time->customer->customer_name ?? null,
                'resource' => $time->employee->name() ?? null,
                'project' => $time->project->name ?? null,
                'year_week' => $time->year_week,
                'billable' => _minutes_to_time($time->timeline->sum(function ($query) {
                    return $query->is_billable ? ($query->total * 60) + $query->total_m : 0;
                })),
                'bill_tot' => $time->timeline->sum(function ($query) {
                    return $query->is_billable ? ($query->total * 60) + $query->total_m : 0;
                }),
                'non_bill_tot' => $time->timeline->sum(function ($query) {
                    return ! $query->is_billable ? ($query->total * 60) + $query->total_m : 0;
                }),
                'non_billable' => _minutes_to_time($time->timeline->sum(function ($query) {
                    return ! $query->is_billable ? ($query->total * 60) + $query->total_m : 0;
                })),
                'claim' => $time->time_exp->sum(function ($query) {
                    return $query->is_billable ? $query->amount : 0;
                }),
            ];
        });

        return response()->json($timesheets);
    }

    public function getBillinPeriods(BillingCycle $billingCycle): JsonResponse
    {
        $billingCycle = $billingCycle->load(['billingPeriods' => function ($periods) {
            $periods->orderBy('start_date');
        }]);

        return response()->json($billingCycle->billingPeriods ?? []);
    }

    private function logoNotFound($details, $type)
    {
        $body = 'Please edit '.$type.' '.$details['name'].' and upload the logo, Someone tried to print timesheets which require this logo.';
        $button_text = 'Edit Customer';
        $url = ['url' => route('customer.edit', $details['id'])];
        $administrators = User::select('id', 'first_name', 'last_name', 'email')->whereHas('roles', fn($roles) => $roles->where('name', 'admin'))->get()->map(function ($user) {
            return $user->email;
        })->filter()->unique()->values();

        try {
            //Mail::to($administrators)->send(new UploadLogoMail($body, $url, $button_text));
            SendUploadLogoEmailJob::dispatch($administrators, $body, $url, $button_text)->delay(now()->addMinutes(5));
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
        }
    }

    public function checkTimesheets(Request $request)
    {
        $timesheet = Timesheet::with(['project' => function ($query) {
            $query->select(['id', 'name']);
        }, 'timeline' => function ($timeline) {
            $timeline->selectRaw('timesheet_id, ((total*60)+total_m) AS total_minutes');
        }])
            ->select(['id', 'project_id', 'year_week'])
            ->where([
                'employee_id' => $request->input('employee'),
                'project_id' => $request->input('project'),
                'year_week' => $request->input('year_week'),
            ])
            ->where(function ($query) {
                return $query->whereNull('timesheet_lock')
                    ->orWhere('timesheet_lock', 0);
            })
            ->get()->map(function ($timesheet) {
                return [
                    'id' => $timesheet->id,
                    'year_week' => $timesheet->year_week,
                    'project' => $timesheet->project->name ?? null,
                    'time' => _minutes_to_time($timesheet->timeline->sum('total_minutes')),
                ];
            });

        return response()->json(['timesheet' => $timesheet]);
    }

    private function approver($user_id)
    {
        return ApprovalRules::select(['id', 'substitute_approver'])->where('date_from', '<=', now()->toDateString())
            ->where('date_to', '>=', now()->toDateString())
            ->where('approver_id', $user_id)->first();
    }
}