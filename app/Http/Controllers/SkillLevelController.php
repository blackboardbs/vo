<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSkillLevelRequest;
use App\Http\Requests\UpdateSkillLevelRequest;
use App\Models\SkillLevel;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SkillLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $payment_method = SkillLevel::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_method = $payment_method->where('status', $request->input('status_filter'));
            }else{
                $payment_method = $payment_method->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_method = $payment_method->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_method = $payment_method->paginate($item);

        $parameters = [
            'skill_level' => $payment_method,
        ];

        return View('master_data.skill_level.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = SkillLevel::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.skill_level.create')->with($parameters);
    }

    public function store(StoreSkillLevelRequest $request): RedirectResponse
    {
        $skill_level = new SkillLevel();
        $skill_level->description = $request->input('description');
        $skill_level->status = $request->input('status');
        $skill_level->creator_id = auth()->id();
        $skill_level->save();

        return redirect(route('skill_level.index'))->with('flash_success', 'Master Data Skill Level captured successfully');
    }

    public function show($skill_level_id): View
    {
        $parameters = [
            'skill_level' => SkillLevel::where('id', '=', $skill_level_id)->get(),
        ];

        return View('master_data.skill_level.show')->with($parameters);
    }

    public function edit($skill_level_id): View
    {
        $autocomplete_elements = SkillLevel::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'skill_level' => SkillLevel::where('id', '=', $skill_level_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.skill_level.edit')->with($parameters);
    }

    public function update(UpdateSkillLevelRequest $request, $skill_level_id): RedirectResponse
    {
        $skill_level = SkillLevel::find($skill_level_id);
        $skill_level->description = $request->input('description');
        $skill_level->status = $request->input('status');
        $skill_level->creator_id = auth()->id();
        $skill_level->save();

        return redirect(route('skill_level.index'))->with('flash_success', 'Master Data Skill Level saved successfully');
    }

    public function destroy($skill_level_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // SkillLevel::destroy($skill_level_id);
        $item = SkillLevel::find($skill_level_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('skill_level.index')->with('success', 'Master Data Skill Level suspended successfully');
    }
}
