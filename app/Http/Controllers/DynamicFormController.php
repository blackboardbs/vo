<?php

namespace App\Http\Controllers;

use App\Models\DynamicForm;
use App\Http\Permission;
use App\LookUp;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DynamicFormController extends Controller
{
    public $user = null;

    public function __construct()
    {
        //$this->middleware(['auth', 'permission']);
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        //dd(Permission::hasPermission());
        $look_up_id = $request->has('m') ? $request->input('m') : null;

        $dynamic_form = new DynamicForm();

        $parameters = $dynamic_form->list($look_up_id);

        return view('dynamic_form.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $look_up_id = $request->input('m');

        $dynamic_form = new DynamicForm();

        $parameters = $dynamic_form->createDynamicForm($look_up_id);

        return view('dynamic_form.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $look_up_id = $request->input('look_up_id');

        $dynamic_form = new DynamicForm();

        $result = $dynamic_form->storeDynamicForm($look_up_id, $request);

        $alert_type = 'flash_danger';

        if ($result['status'] == true) {
            $alert_type = 'flash_success';
        }

        return redirect(route('dynamic.index', ['m' => $result['look_up']->id]))->with($alert_type, $result['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DynamicForm  $dynamicForm
     */
    public function show($model_id, Request $request): View
    {
        $dynamic_form = new DynamicForm();

        $look_up_id = $request->input('m');

        $parameters = $dynamic_form->editDynamicForm($look_up_id, $model_id);

        return view('dynamic_form.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DynamicForm  $dynamicForm
     */
    public function edit($model_id, Request $request): View
    {
        $dynamic_form = new DynamicForm();

        $look_up_id = $request->input('m');

        $parameters = $dynamic_form->editDynamicForm($look_up_id, $model_id);

        return view('dynamic_form.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\DynamicForm  $dynamicForm
     */
    public function update($model_id, Request $request): RedirectResponse
    {
        $look_up_id = $request->input('look_up_id');
        $dynamic_form = new DynamicForm();

        $result = $dynamic_form->updateDynamicForm($look_up_id, $model_id, $request);

        return redirect(route('dynamic.index', ['m' => $look_up_id]))->with('flash_success', $result->name.' successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DynamicForm  $dynamicForm
     */
    public function destroy($id, Request $request): RedirectResponse
    {
        $look_up_id = $request->input('look_up_id');

        $look_up = LookUp::find($look_up_id);

        $model_name = 'App\\'.$look_up->model;

        $model_name::destroy($id);

        return redirect(route('dynamic.index', ['m' => $look_up_id]))->with('flash_success', $look_up->name.' successfully deleted.');
    }
}
