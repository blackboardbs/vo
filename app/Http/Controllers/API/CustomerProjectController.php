<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerProjectResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerProjectController extends Controller
{
    public function __invoke(Customer $customer)
    {
        return CustomerProjectResource::collection($customer->projects);
    }
}
