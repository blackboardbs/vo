<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\VatRate;
use App\Services\CustomerInvoiceService;
use Illuminate\Http\Request;

class CustomerProFormaInvoiceController extends Controller
{
    public function __invoke(CustomerInvoiceService $service)
    {
        return response()->json([
            'vat_rate_id' => $service->vatRate(),
            'vat_rate_dropdown' => VatRate::selectRaw('id, CONCAT(description, " - ", vat_rate,"%") AS description')->where('end_date', '>=', now()->toDateString())->pluck('description', 'id')
        ]);
    }
}
