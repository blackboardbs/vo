<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AdvancedTemplates;
use App\Models\TemplateType;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function previewTemplate(Request $request)
    {
        $templateType = TemplateType::findOrFail($request->template_type_id);

        $templateBody = str_replace('class="mention"', '', $request->template_body);

        foreach ($templateType->variables as $variable) {
            if (($variable->variable === "company.company_logo") || ($variable->variable === "vendor.vendor_logo")){
                $templateBody = str_replace("[{$variable->variable}]", "<img src='/assets/consulteaze_logo.png' style='max-height: 80px;width: auto' alt='$variable->display_name' />", $templateBody);
            }else{
                $templateBody = str_replace("[{$variable->variable}]", "<strong>{$variable->display_name}</strong>", $templateBody);
            }

        }

        return response()->json(['template' => $templateBody]);
    }

    public function store(Request $request)
    {
        AdvancedTemplates::create([
            'template_type_id' => $request->template_type_id,
            'name' => $request->name,
            'template_body' => $request->template_body
        ]);

        return response()->json(['message' => 'Template created']);
    }

    public function update(AdvancedTemplates $template, Request $request)
    {
        $template->update([
            'template_type_id' => $request->template_type_id,
            'name' => $request->name,
            'template_body' => $request->template_body
        ]);

        return response()->json(['message' => 'Template updated']);
    }
}
