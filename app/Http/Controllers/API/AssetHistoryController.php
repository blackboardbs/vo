<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AssetHistoryResource;
use App\Models\AssetHistory;
use App\Models\AssetRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssetHistoryController extends Controller
{
    public function index(AssetRegister $asset)
    {
        $asset->loadMissing(['assetHistory.user:id,first_name,last_name']);

        return AssetHistoryResource::collection($asset->assetHistory);
    }

    public function store(Request $request)
    {
        $validator = $this->assetHistoryValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        AssetHistory::where('asset_id', $request->asset_id)->update(['is_active' => 0]);

        $issue = AssetHistory::create($this->massageValidatedData($request, $validator));

        return response()->json([
            'issue' => new AssetHistoryResource($issue->loadMissing(['user:id,first_name,last_name']))
        ], 201);
    }

    public function update(AssetHistory $issue, Request $request)
    {
        $validator = $this->assetHistoryValidation($request);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $issue->update($this->massageValidatedData($request, $validator));

        return response()->json([
            'issue' => new AssetHistoryResource($issue->loadMissing(['user:id,first_name,last_name']))
        ], 201);
    }

    public function destroy(AssetHistory $issue)
    {
        $issue->delete();

        return response()->json(['message' => 'Asset History deleted successfully.']);
    }

    private function assetHistoryValidation(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'in_acceptance_letter' => 'boolean|required',
            'asset_id' => 'integer|required|exists:asset_register,id',
            'issued_to' => 'integer|nullable|exists:users,id',
            'note' => 'nullable|string',
            'issued_at' => 'required|date_format:Y-m-d',
        ]);
    }

    private function massageValidatedData(Request $request, \Illuminate\Validation\Validator $validator): array
    {
        $massageData = [
            'is_included_in_letter' => $request->in_acceptance_letter,
            'user_id' => $request->issued_to,
            'is_active' => 1
        ];

        $validator->setData(array_merge($validator->getData(), $massageData));

        return $validator->getData();
    }
}
