<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Config;
use Illuminate\Http\Request;

class DefaultCompanyController extends Controller
{
    public function __invoke()
    {
        $config = Config::with(['company:id'])->first();
    }
}
