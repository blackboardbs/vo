<?php

namespace App\Http\Controllers\API;

use App\API\Clockify\ClockifyAPI;
use App\Models\Assignment;
use App\Models\BillingPeriod;
use App\Models\ClockifyTime;
use App\Models\Company;
use App\Models\Config;
use App\Http\Controllers\Controller;
use App\Mail\ClockifyNotificationMail;
use App\Models\Project;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Timesheet\TimesheetHelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClockifyController extends Controller
{
    use TimesheetHelperTrait;

    public function handleEntry(Request $request, ClockifyAPI $api)
    {
        abort_if(! config('app.clockify.is_enabled'), ClockifyAPi::STATUS_FORBIDDEN);

        $base_url = config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace');
        $email = $this->userEmail($api, $request->userId);
        $wo_user = null;
        if (isset($email)) {
            $wo_user = User::select('id', 'first_name', 'last_name')->where('email', $email)->first();
            abort_if(! isset($wo_user), ClockifyAPi::STATUS_FORBIDDEN);
        }

        $project = $this->clockifyProject($api, $request->projectId);
        $wo_project_id = $project->note ?? null;

        $this->validateProjectAndAssignment($wo_project_id, $email, $wo_user);

        $customer = json_decode($api->getEndPoint($base_url.'/clients/'.$request->project['clientId'])->getBody()->getContents());
        $wo_customer_id = $customer->note ?? null;

        $time = $request->timeInterval;
        $date = Carbon::parse($time['start']);
        $year_week = $date->copy()->year.str_pad($date->copy()->weekOfYear ?? 0, 2, '0', STR_PAD_LEFT);

        $project = Project::with(['customer' => function ($customer) {
            return $customer->select(['id', 'billing_cycle_id']);
        }])->find($wo_project_id, ['id', 'billing_cycle_id', 'customer_id', 'company_id']);

        $billing_cycle = $this->billingCycle($project, Config::select(['billing_cycle_id'])->first());
        $billing_period = $this->billing_period($billing_cycle, $date);
        $split_week = $this->splitWeek($date, $billing_period);

        $task_id = (explode(' ', $request->task['name'])[0]) ?? null;
        $task_id = is_numeric($task_id) ? $task_id : null;

        $timesheet = Timesheet::select(['id'])->where('employee_id', $wo_user->id)
            ->where('customer_id', $wo_customer_id)
            ->where('project_id', $wo_project_id)
            ->where('year_week', $year_week)
            ->where('first_day_of_month', $billing_period->start_date)
            ->latest()->first();

        if (! isset($timesheet)) {
            $timesheet = $this->timesheet(
                $wo_user,
                $date,
                $year_week,
                $split_week,
                $project,
                $billing_period
            );

            $this->timeline(
                $timesheet,
                $date,
                $time,
                $task_id,
                $wo_user,
                $request->id
            );
        } else {
            $this->timeline(
                $timesheet,
                $date,
                $time,
                $task_id,
                $wo_user,
                $request->id
            );
        }
    }

    public function update(Request $request)
    {
        $start_time = Carbon::parse($request->timeInterval['start']);
        $end_time = Carbon::parse($request->timeInterval['end']);
        $hours = $start_time->copy()->diffInMinutes($end_time) / 60;
        $day = strtolower($start_time->format('D'));
        $day_min = strtolower($day.'_m');

        $timelineId = ClockifyTime::where('clockify_time_id', $request->id)->first(['timeline_id']);

        $timeline = Timeline::findOrFail($timelineId->timeline_id);
        $timeline->$day = floor($hours);
        $timeline->$day_min = number_format($start_time->copy()->diffInMinutes($end_time) % 60);
        $timeline->total = floor(($start_time->copy()->diffInMinutes($end_time)) / 60);
        $timeline->total_m = ($start_time->copy()->diffInMinutes($end_time)) % 60;
        $timeline->save();
    }

    private function clockifyProject(ClockifyAPI $clockify, $projectId)
    {
        foreach ($clockify->projects() as $project) {
            if ($project->id === $projectId) {
                return $project;
            }
        }

        return '';
    }

    private function userEmail(ClockifyAPI $api, string $user_id)
    {
        $users = $api->getEndPoint(config('app.clockify.base_endpoint').'/workspaces/'.config('app.clockify.workspace').'/users');
        $user_email = null;

        if ($users->getStatusCode() == ClockifyAPI::STATUS_OK) {
            $user_email = collect(json_decode($users->getBody()->getContents(), true))
                ->map(function ($user) use ($user_id) {
                    if ($user['id'] === $user_id) {
                        return $user['email'];
                    }

                    return null;
                })->filter()->values();
        }

        return $user_email[0] ?? null;
    }

    private function splitWeek(Carbon $date, $billing_period): int
    {
        if (isset($billing_period)) {
            $start_date = $billing_period->start_date;
            $end_date = $billing_period->end_date;
        } else {
            $start_date = $date->copy()->startOfMonth()->toDateString();
            $end_date = $date->copy()->endOfMonth()->toDateString();
        }

        if (
            (isset($start_date) && (Carbon::parse($start_date)->diffInDays($date->copy()->endOfWeek()) < 6))
            || ($date->copy()->startOfMonth()->diffInDays($date->copy()->endOfWeek()) < 6)) {
            return 2;
        } elseif (
            (isset($end_date) && $date->copy()->startOfWeek()->diffInDays(Carbon::parse($end_date)) < 6)
            || ($date->copy()->startOfWeek()->diffInDays($date->copy()->endOfMonth()) < 6)) {
            return 1;
        }

        return 0;
    }

    private function timesheet($wo_user, $date, $year_week, $split_week, $project, $billing_cycle)
    {
        $billing_period = $billing_cycle;

        $timesheet = new Timesheet;
        $timesheet->employee_id = $wo_user->id;
        $timesheet->company_id = $project->company_id ?? Company::first()->id;
        $timesheet->project_id = $project->id;
        $timesheet->customer_id = $project->customer_id ?? 0;
        $timesheet->year_week = $year_week;
        $timesheet->year = $date->copy()->year;
        $timesheet->week = $date->copy()->weekOfYear;
        $timesheet->month = $date->copy()->month;
        $timesheet->first_day_of_week = $date->copy()->startOfWeek();
        $timesheet->last_day_of_week = $date->copy()->endOfWeek();
        $timesheet->first_day_of_month = $billing_period->start_date ?? $date->copy()->startOfMonth();
        $timesheet->last_day_of_month = $billing_period->end_date ?? $date->copy()->endOfMonth();
        $timesheet->first_date_of_week = $date->copy()->startOfWeek()->format('d');
        $timesheet->last_date_of_week = $date->copy()->endOfWeek()->format('d');
        $timesheet->first_date_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : $date->copy()->startOfMonth()->format('d');
        $timesheet->last_date_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date)->format('d') : $date->copy()->endOfMonth()->format('d');
        $timesheet->creator_id = $wo_user->id;
        $timesheet->status_id = 1;

        if ($split_week == 1) {
            $timesheet->last_date_of_week = $timesheet->last_date_of_month;
            $timesheet->last_day_of_week = $timesheet->last_day_of_month;
            $timesheet->last_day_of_week = $timesheet->last_day_of_month;
        }

        if ($split_week == 2) {
            $temp_last_date_of_month_2 = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
            $timesheet->first_date_of_week = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : 1;
            $timesheet->month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->month : $date->copy()->month;
            $timesheet->first_day_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date) : $date->copy()->addWeek()->startOfMonth();
            $timesheet->last_day_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
            $timesheet->last_date_of_month = $temp_last_date_of_month_2->format('d');
            $timesheet->first_day_of_week = $billing_period->start_date;
        }

        $timesheet->save();

        activity()->on($timesheet)->log('created');

        return $timesheet;
    }

    private function timeline($timesheet, $date, $time, $task, $wo_user, $clockify_time_id)
    {
        $hours = (int) number_format(($date->copy()->diffInMinutes($time['end'])) / 60);
        $day = strtolower($date->format('D'));
        $day_min = strtolower($day.'_m');
        $timeline = new Timeline();
        $timeline->description_of_work = request()->description;
        $timeline->timesheet_id = $timesheet->id;
        $timeline->$day = $hours;
        $timeline->$day_min = (int) number_format($date->copy()->diffInMinutes($time['end']) % 60);
        $timeline->total = floor(($date->copy()->diffInMinutes($time['end'])) / 60);
        $timeline->total_m = ($date->copy()->diffInMinutes($time['end'])) % 60;
        $timeline->is_billable = request()->billable ? 1 : 0;
        $timeline->task_id = $task ?? null;
        $timeline->status = 1;
        $timeline->creator_id = $wo_user->id;
        $timeline->save();

        $balance = new ClockifyTime();
        $balance->timeline_id = $timeline->id;
        $balance->clockify_time_id = $clockify_time_id;
        $balance->save();
    }

    private function validateProjectAndAssignment($wo_project_id, $email, $wo_user)
    {
        if (isset($wo_project_id)) {
            $local_projects = Project::with(['manager' => function ($manager) {
                $manager->select(['id', 'email']);
            }])->find($wo_project_id, ['consultants', 'name', 'manager_id']);
            $admin_emails = User::where('status_id', 1)->where('expiry_date', '>=', now()->toDateString())->whereHas('roles', function ($role) {
                $role->where('name', 'admin');
            })->pluck('email');

            $mailBody = '';
            if (! in_array($wo_user->id, explode(',', $local_projects->consultants))) {
                $mailBody .= '<p>Dear '.$wo_user->name().",</p><p>You are not assigned to this project ($local_projects->name), please talk to your manager to assign you to this project or capture time to the correct project.</p>";
                Mail::to($email)->cc([$local_projects->manager->email ?? null])->send(new ClockifyNotificationMail($mailBody));
                abort_if(! in_array($wo_user->id, explode(',', $local_projects->consultants)), ClockifyAPi::STATUS_FORBIDDEN);
            }

            $assignment = Assignment::where('employee_id', $wo_user->id)->where('project_id', $wo_project_id)->first();

            if (! isset($assignment)) {
                $mailBody .= '<p>Dear '.$wo_user->name().",</p><p>You don't have an assignment for project ($local_projects->name), please talk to your manager to create an assignment for you.</p>";
                Mail::to($email)->cc([$local_projects->manager->email ?? null])->send(new ClockifyNotificationMail($mailBody));
                abort_if(! in_array($wo_user->id, explode(',', $local_projects->consultants)), ClockifyAPi::STATUS_FORBIDDEN);
            }
        }
    }

    private function billing_period($billing_cycle, $date)
    {
        return BillingPeriod::where('billing_cycle_id', $billing_cycle)
            ->where('start_date', '<=', $date->copy()->toDateTimeString())
            ->where('end_date', '>=', $date->copy()->toDateTimeString())
            ->where('year', $date->copy()->year)
            ->first();
    }
}
