<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Models\UserNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function notificationsCount(Request $request): JsonResponse
    {
        $notifications = UserNotification::with('notification:id,name,link,created_at')
            ->select('notification_id')
            ->where('user_id', auth()->id())
            ->whereNull('seen_at')
            ->where('notify', 1)
            ->whereHas('notification')
            ->latest()
            ->get();

        return response()->json(new NotificationResource($notifications));
    }

    public function markAllNotifications(): JsonResponse
    {
        UserNotification::where('user_id', Auth()->id())->update(['notify' => 0]);

        return response()->json(['success' => 'success']);
    }

    public function readNotifications(Request $request): JsonResponse
    {
        UserNotification::where('notification_id', $request->id)->where('user_id', Auth()->id())->update(['seen_at' => now()]);

        return response()->json(['success' => 'success']);
    }

}
