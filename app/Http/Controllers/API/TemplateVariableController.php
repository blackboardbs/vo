<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TemplateType;
use Illuminate\Http\Request;

class TemplateVariableController extends Controller
{
    public function __invoke(TemplateType $templatetype)
    {
        return $templatetype->variables->map(fn($variable) => [
            'id' => $variable->id,
            'value' => $variable->display_name,
            'variable' => $variable->variable,
        ]);
    }
}
