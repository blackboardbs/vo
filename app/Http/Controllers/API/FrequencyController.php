<?php

namespace App\Http\Controllers\API;

use App\Enum\FrequencyEnum;
use App\Http\Controllers\Controller;
use App\Http\Resources\FrequencyResource;
use Illuminate\Http\Request;

class FrequencyController extends Controller
{
    public function __invoke()
    {
        return FrequencyResource::collection(FrequencyEnum::cases());
    }
}
