<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use App\Services\UserService;

class SubscriptionManagementController extends Controller
{
    public function cancel(): JsonResponse|null
    {
        $owner = User::first();


        // $query = Http::get('https://subscription.consulteaze.com/api/cancel-subscription?email='.$owner->email);

        // if($query->status() == 200){
        //     User::query()->update(['subscription_cancelled' => 1]);
        // }

        return response()->json(['status'=>200]);
    }

    public function cancelEmail(Request $request, UserService $service){

        $reason = $request->reason;

        if($reason == "Other"){
            $reason = $request->reasonOther;
        }

        $service->sendCancelSubscriptionEmail($reason);

        return true;
    }

    public function update(Request $request)
    {
        $package = $this->subscriptionPackage();
        
        $price_id = '';

        $response = Http::get('https://subscription.consulteaze.com/api/prices-list');

        $result = $response->json();

        $prices = [];

        foreach($result["prices_list"]["data"] as $response){
            $prices[strtolower($response["name"])] = ['prod_id' => $response["product_id"],'price_id'=>$response["id"]];	
        }

        // return response()->json($response->json());

        foreach($package as $key => $value){
            if($value['id'] == $request->updateId){
                $price_id = $prices[strtolower($key)]["price_id"];
            }
        }
        // return response()->json($prices);

        $owner = User::first();

        $query = Http::get('https://subscription.consulteaze.com/api/change-plan?email='.$owner->email.'&price_id='.$price_id);

        return response()->json(['status'=>$query->status()]);
    }

    private function generateSignature($data, $passPhrase = null): string
    {
        // Create parameter string
        $pfOutput = '';
        foreach( $data as $key => $val ) {
            if($val !== '') {
                $pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
            }
        }
        // Remove last ampersand
        $getString = substr( $pfOutput, 0, -1 );
        if( $passPhrase !== null ) {
            $getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
        }
        return md5( $getString );
    }

    private function adminUser(): string
    {
        return User::select('email')
            ->whereHas('roles', fn($role) => $role->where('id', 1))
            ->first()?->email;
    }

    private function token(): string|null
    {
        $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$this->adminUser());

        if($response->status() != 404)
            return $response->json()['subscription']['token']??null;
        return null;
    }

    private function defaultResponse(): JsonResponse
    {
        return response()->json([
            'code' => 404,
            'status' => 'failed',
            'data' => [
                'response' => false,
                'message' => "Subscription not found"
            ]
        ], 404);
    }
}
