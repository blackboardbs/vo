<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\VatRate;
use App\Services\VendorInvoiceService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VendorProFormaInvoiceController extends Controller
{
    public function project(int $id): JsonResponse
    {
        $is_customer = (int) \request()->is_customer;

        $projects = Timesheet::with(['project:id,name'])->select(['project_id'])->distinct('project_id')
            ->whereHas('project')
            ->when($is_customer, fn($timesheet) => $timesheet->where('customer_id', $id))
            ->unless($is_customer, fn($timesheet) => $timesheet->where('employee_id', $id))
            ->get()->map(fn ($timesheet) => [
                'id' => $timesheet->project->id,
                'name' => $timesheet->project->name
            ]);

        return response()->json($projects);
    }

    public function periods(Request $request): JsonResponse
    {
        $periods = Timesheet::select(['year', 'month'])
            ->where('project_id', $request->project)
            ->when($request->consultant, fn($timesheet) => $timesheet->where('employee_id', $request->consultant))
            ->orderBy('year', 'desc')
            ->distinct('month')
            ->get()->map(fn ($timesheet) => [
                'key' => $timesheet->year.$timesheet->month,
                'period' => $timesheet->year.str_pad($timesheet->month, 2, '0', STR_PAD_LEFT)
            ])->pluck('period', 'key');

        return response()->json($periods);
    }

    public function vat(User $user, VendorInvoiceService $service): JsonResponse
    {
        $params = [
            'vat_rate_id' => $service->vatRate($user->vendor),
            'vat_rate_dropdown' => VatRate::selectRaw('id, CONCAT(description, " - ", vat_rate,"%") AS description')->where('end_date', '>=', now()->toDateString())->pluck('description', 'id')
        ];

        return response()->json($params);
    }
}
