<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AccountElement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PlannedExpenseController extends Controller
{
    public function getAccountElement($accountid): JsonResponse
    {
        $accountelement = AccountElement::where('account_id', '=', $accountid)->pluck('description', 'id');

        return response()->json($accountelement);
    }
}
