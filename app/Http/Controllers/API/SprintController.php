<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Goal;
use App\Models\Sprint;
use App\Models\SprintGoalHistory;
use App\Models\SprintHoursSummary;
use App\Models\SprintHoursSummaryHistory;
use App\Models\SprintResource;
use App\Models\SprintResourceHistory;
use App\Models\SprintTaskSummary;
use App\Models\SprintTaskSummaryHistory;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SprintController extends Controller
{
    public function __invoke(Request $request, Sprint $sprint): string
    {
        $this->saveSprintGoalHistory($sprint->loadMissing('goals')->goals);
        $this->saveSprintResourceHistory($sprint);
        $this->saveSprintTaskSummaryHistories($sprint->id);
        $this->saveSprintHoursSummaryHistory($sprint->id);

        switch ($request->sprintAction){
            case "open":
                $sprint->update(['status_id' => 2]);
                return response()->json(['message' => "Success"]);
            case "complete":
                $sprint->update(['status_id' => 2]);
                $sprint->tasks?->each(function ($task) {
                    $task->status = 5;
                    $task->save();
                });
                return response()->json(['message' => "Success"]);
            case "create":
                $newSprint = $sprint->replicate();
                $newSprint->name = $this->generateSprintName($sprint->name);
                $newSprint->start_date = Carbon::parse($sprint->end_date)->addDay()->toDateTimeString();
                $newSprint->end_date = $this->generateEndDate($sprint->only(['start_date', 'end_date']));
                $newSprint->status_id = 1;
                $newSprint->save();
                $newSprint->goals()->insert($this->mapGoals($sprint, $newSprint->id));

                $newSprint->goals->each(function ($goal, $index) use ($sprint) {
                    $sprintGoal = $sprint->goals[$index];
                    $sprintGoal->resources->each(fn ($resource) => $this->createSprintResource($resource, $goal));
                });

                $sprint->tasks()->whereNot('status', 5)->get()
                    ->each(fn ($task) => $this->copyTasks($task, $newSprint));

                $sprint->tasks()->update(['status' => 5]);
                $sprint->update(['status_id' => 2]);
                return response()->json(['message' => "Success"]);
            default:
                return response()->json(['message' => "Failed"]);
        }
    }

    private function generateSprintName(string $sprintName): string
    {
        $arrayName = explode(" ", $sprintName);
        $lastWord = last($arrayName);

        if (is_numeric($lastWord)){
            $lastWord = (int)$lastWord + 1;
            $arrayName[count($arrayName) - 1] = $lastWord;
            return implode(" ", $arrayName);
        }
        return $sprintName." - copy";
    }

    private function generateEndDate(array $sprint): string
    {
        $startDate = Carbon::parse($sprint["start_date"]);
        $endDate = Carbon::parse($sprint["end_date"]);
        return $endDate->addDays($startDate->diffInDays($endDate) + 1)->toDateTimeString();
    }

    private function mapGoals(Sprint $sprint, int $newSprintId): array
    {
        return $sprint->goals?->map(fn ($goal) => [
            'sprint_id' => $newSprintId,
            'name' => $goal->name,
        ])->toArray();
    }

    private function copyTasks(Task $task, Sprint $sprint): void
    {
        $newTask = $task->replicate();
        $newTask->sprint_id = $sprint->id;
        $newTask->start_date = $sprint->start_date;
        $newTask->end_date = $sprint->end_date;
        $newTask->goal_id = Goal::where('sprint_id', $sprint->id)
            ->where('name', $task->goal?->name)
            ->latest()
            ->first()?->id;
        $newTask->save();
    }

    private function createSprintResource(SprintResource $resource, Goal $goal): void
    {
        SprintResource::create([
            'sprint_id' => $goal->sprint_id,
            'goal_id' => $goal->id,
            'employee_id' => $resource->employee_id,
            'function_id' => $resource->function_id,
            'capacity' => $resource->capacity,
        ]);
    }

    private function saveSprintGoalHistory(Collection $goals): void
    {
        $goals->loadMissing([
            'tasks:id,goal_id,hours_planned',
            'tasks.timelines:task_id,total,total_m'
        ])->each(function($goal) {
            $no_of_tasks = $goal->tasks->count()??0;
            $completed_tasks = $goal->tasks()->where('status', 5)->count()??0;

            SprintGoalHistory::create([
                'goal_id' => $goal->id,
                'sprint_id' => $goal->sprint_id,
                'no_of_tasks' => $no_of_tasks,
                'task_completed' => $completed_tasks,
                'task_outstanding' => $no_of_tasks - $completed_tasks,
                'overdue_tasks' => $goal->tasks()->where('status', '!=', 5)->where('end_date', '<', now()->toDateString())->count(),
                'impediments' => $goal->tasks()->where('status', 6)->count()??0,
                'planned_hours' => $goal->tasks->sum('hours_planned'),
                'booked_hours' => $goal->tasks->map(function ($task) {
                    return ($task->timelines->sum('total')*60) + $task->timelines->sum('total_m');
                })->sum()
            ]);
        });
    }

    private function saveSprintResourceHistory(Sprint $sprint): void
    {
        $sprint->loadMissing(['resources' => fn($resource) => $resource->selectRaw("sprint_resources.id, sprint_resources.employee_id, sprint_resources.sprint_id, sprint_resources.function_id, sprint_resources.capacity, (SELECT SUM(leave.no_of_days) FROM `leave` WHERE leave.emp_id = sprint_resources.employee_id AND leave.date_from >= ? AND leave.date_to <= ?) AS leave_days", [$sprint->start_date, $sprint->end_date])]);
        $sprint->resources?->each(fn($resource) => SprintResourceHistory::create([
            'user_id' => $resource->employee_id,
            'sprint_id' => $resource->sprint_id,
            'function_id' => $resource->function_id,
            'capacity' => $resource->capacity,
            'leave_days' => $resource->leave_days??0,
        ]));
    }

    private function saveSprintTaskSummaryHistories(int $sprintId): void
    {
        SprintTaskSummary::where('sprintId', $sprintId)->get()
            ->each(fn($taskSummary) => SprintTaskSummaryHistory::create([
                'sprint_id' => $sprintId,
                'user_id' => $taskSummary->Resource_ID,
                'no_of_tasks' => $taskSummary->No_of_Task,
                'planned_hours' => $taskSummary->HoursPlanned,
                'booked_hours' => $taskSummary->ActualHours,
                'overdue_tasks' => $taskSummary->OverdueTasks,
                'backlog' => $taskSummary->Backlog,
                'planned' => $taskSummary->Planned,
                'in_progress' => $taskSummary->InProgress,
                'done' => $taskSummary->Done,
                'completed' => $taskSummary->Completed,
                'impediments' => $taskSummary->Impediments
            ]));
    }

    private function saveSprintHoursSummaryHistory(int $sprintId): void
    {
        SprintHoursSummary::where('SprintId', $sprintId)->get()
            ->each(fn($hoursSummary) => SprintHoursSummaryHistory::create([
                'sprint_id' => $sprintId,
                'user_id' => $hoursSummary->Resource_ID,
                'planned_hours' => $hoursSummary->HoursPlanned,
                'booked_hours' => $hoursSummary->ActualHours,
                'outstanding_hours' => $hoursSummary->HoursOutstanding,
                'available_hours' => $hoursSummary->HoursAvailable,
                'potential_outcome' =>  $hoursSummary->PotentialOutcomeText,
            ]));
    }

}
