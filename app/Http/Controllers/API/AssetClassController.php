<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AssetClass;
use Illuminate\Http\Request;

class AssetClassController extends Controller
{
    public function __invoke(AssetClass $class)
    {
        return response()->json(['est_life_month' => $class->est_life_month]);
    }
}
