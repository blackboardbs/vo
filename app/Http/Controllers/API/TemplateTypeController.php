<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TemplateType;
use Illuminate\Http\Request;

class TemplateTypeController extends Controller
{
    public function __invoke()
    {
        $template_types = TemplateType::select(['id', 'name'])->get();

        return response()->json(['templates' => $template_types]);
    }
}
