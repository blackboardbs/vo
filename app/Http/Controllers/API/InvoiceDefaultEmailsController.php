<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DefaultEmailsResource;
use App\Models\Customer;
use App\Services\CompanyService;
use Illuminate\Http\Request;

class InvoiceDefaultEmailsController extends Controller
{
    public function __invoke(Customer $customer, CompanyService $service)
    {
        $customerEmails = $customer->load('invoiceContact:id,email');
        $emails["email"] = $customerEmails->invoiceContact?->email;
        $emails["email"] = $service->email();
        return array_values(array_filter(array_unique($emails)));
    }
}
