<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AccountElement;
use App\Models\Prospect;
use App\Models\ProspectNote;
use App\Models\Customer;
use App\Models\Config;
use App\Models\Document;
use App\Models\InvoiceContact;
use App\Models\System;
use App\Models\ProspectStatus;
use App\Models\User;
use App\Models\Lead;
use App\Models\Industry;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProspectController extends Controller
{
    public function getProspects(Request $request): JsonResponse
    {
        $config = Config::first();

        $prospects = Prospect::selectRaw('id,prospect_name,account_manager_id,status_id,decision_date,chance,solution,value,scope,hours,resources,converted_date,est_start_date,partners,note,contact,customer_id,company_id,duration,margin,industry_id,lead_from,emote_id,0 as canEdit');

        if ($request->has('am') && $request->input('am') != '') {
            $prospects = $prospects->where('account_manager_id', '=', $request->input('am'));
        }

        if ($request->has('s') && $request->input('s') != '' && $request->input('s') != '0') {
            $prospects = $prospects->where('status_id', '=', $request->input('s'));
        } elseif ($request->has('s') && $request->input('s') == '0') {
            $prospects = $prospects;
        } else {
             $prospects =  $prospects->whereIn('status_id',[1,2,3,4,5]);
        }

        if ($request->has('ds') && $request->input('ds') != '') {
            // dd($request->input('ds'));
            $prospects = $prospects->where('decision_date', '>=', $request->input('ds'));
        }

        if ($request->has('de') && $request->input('de') != '') {
            $prospects = $prospects->where('decision_date', '<=', $request->input('de'));
        }

        if ($request->has('q') && $request->input('q') != '') {
            $prospects = $prospects->where(fn($item) => $item->where('prospect_name', 'LIKE', '%'.$request->input('q').'%')
            ->orWhere('contact', 'like', '%'.$request->input('q').'%')
            ->orWhere('partners', 'like', '%'.$request->input('q').'%')
            ->orWhere('solution', 'like', '%'.$request->input('q').'%')
            ->orWhere('scope', 'like', '%'.$request->input('q').'%')
            ->orWhere('decision_date', 'like', '%'.$request->input('q').'%')
            ->orWhere('value', 'like', '%'.$request->input('q').'%')
            ->orWhere('chance', 'like', '%'.$request->input('q').'%'));
        }

        $prospects = $prospects->orderBy('decision_date','asc')->get();

        $arr = [];
        foreach($prospects as $id){
            array_push($arr,$id->account_manager_id);
        }
        
        $account_manager_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id',$arr)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $status_drop_down = ProspectStatus::orderBy('id')->pluck('description', 'id');
        $customers = Customer::orderBy('customer_name')->get();
        $contacts = InvoiceContact::orderBy('first_name')->orderBy('last_name')->get();
        $systems = System::orderBy('description')->get();
        $leads = Lead::orderBy('name')->get();
        $industries = Industry::orderBy('description')->get();

        $parameters = [
            'prospects' => $prospects,
            'customers' => $customers,
            'contacts' => $contacts,
            'systems' => $systems,
            'leads' => $leads,
            'industries' => $industries,
            'status_drop_down' => $status_drop_down,
            'account_manager_drop_down' => $account_manager_drop_down
        ];
        
        return response()->json($parameters);
        
    }

    public function move($prospect_id, Request $request): JsonResponse
    {
        $prospect = Prospect::find($prospect_id);
        $prospect->status_id = ($request->input('status')+1);
        $prospect->save();

        return response()->json(['message' => 'Prospect updated successfully']);
    }

    public function addContact(Request $request){

        $ic = new InvoiceContact();
        $ic->first_name = $request->first_name ?? '';
        $ic->last_name = $request->last_name ?? '';
        $ic->email = $request->email ?? '';
        $ic->contact_number = $request->contact_number ?? '';
        $ic->birthday = $request->birthday ?? '';
        $ic->status_id = 1;
        $ic->save();

        $contacts = InvoiceContact::orderBy('first_name')->orderBy('last_name')->get();

        return response()->json(['message' => 'Contact successfully saved','contacts'=>$contacts,'selected_id'=>$ic->first_name.' '.$ic->last_name]);
    }

    public function addSolution(Request $request){

        $ic = new System();
        $ic->description = $request->description ?? '';
        $ic->status = 1;
        $ic->save();

        $systems = System::orderBy('description')->get();

        return response()->json(['message' => 'Contact successfully saved','systems'=>$systems,'selected_id'=>$ic->description]);
    }

    public function addMessage(Request $request, $prospect_id){
        $prospect = Prospect::find($prospect_id);

        $prospectNote = new ProspectNote();
        $prospectNote->prospect_id = $prospect_id;
        $prospectNote->note = $request->input('message');
        $prospectNote->creator_id = auth()->user()->id;
        $prospectNote->status_id = 1;
        $prospectNote->save();

        return ['message' => 'Prospect note successfully added.'];
    }

    public function getMessages($prospect_id){
        return ProspectNote::with(['user'])->where('prospect_id',$prospect_id)->orderBy('id','desc')->get();
    }

    public function getDocuments($prospect_id)
    {
        $documents = Document::orderBy('id')->where('document_type_id', 11)->where('reference_id', $prospect_id)->where('status_id', 1)->get();

        return $documents;
    }

    public function deleteDocument($id)
    {
        $documents = Document::where('id', $id)->delete();

        return response()->json(['success' => 'You have successfully deleted the file.']);
    }

    public function uploadFile($prospect_id, Request $request): JsonResponse
    {
        $file = 'prospect_'.date('Y_m_d_H_i_s').'.'.$request->file->getClientOriginalExtension();
        $fileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('files/prospects'), $file);

        $document = new Document();
        $document->name = $fileName;
        $document->type_id = 11;
        $document->file = $file;
        $document->document_type_id = 11;
        $document->reference = 'Prospect';
        $document->reference_id = $prospect_id;
        $document->owner_id = (isset($request->user_id) ? $request->user_id : auth()->id());
        $document->creator_id = (isset($request->user_id) ? $request->user_id : auth()->id());
        $document->status_id = 1;
        $document->save();

        if ($request->ajax()) {
            return response()->json($document);
        }

        return response()->json(['success' => 'You have successfully upload file.']);
    }
}