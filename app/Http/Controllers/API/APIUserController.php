<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class APIUserController extends Controller
{
    /*public function index(Request $request): JsonResponse
    {
        return response()->json([
            'users' => User::selectRaw("CONCAT(`first_name`, ' ', `last_name`) AS full_name, `id`")
                ->where('first_name', 'LIKE', '%'.$request->q.'%')
                ->orWhere('last_name', 'LIKE', '%'. $request->q .'%')
                ->get()
        ]);
    }*/

    public function getUsersDropdown(Request $request)
    {
        return response()->json([
            'users' => User::whereHas('roles', fn($role) => $role->whereNotIn('id', [6, 7, 8, 11]))
                ->selectRaw("CONCAT(first_name, ' ', last_name) AS full_name, id")->get()
        ]);
    }
}
