<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Project;
use Illuminate\Http\JsonResponse;

class BillingCycleUsageController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke($billing_cycle_id): JsonResponse
    {
        $projects = Project::select('billing_cycle_id')->where('billing_cycle_id', $billing_cycle_id)->get();
        $customers = Customer::select('billing_cycle_id')->where('billing_cycle_id', $billing_cycle_id)->get();
        $configs = Config::select('billing_cycle_id')->where('billing_cycle_id', $billing_cycle_id)->get();
        if ($projects->count()) {
            return response()->json(['message' => 'The billing cycle you want to delete is being used by 1 or more projects']);
        }

        if ($customers->count()) {
            return response()->json(['message' => 'The billing cycle you want to delete is being used by 1 or more customers']);
        }

        if ($configs->count()) {
            return response()->json(['message' => 'The billing cycle you want to delete is being used in the configs configs']);
        }

        return response()->json(['message' => false]);
    }
}
