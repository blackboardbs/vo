<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\VatRate;
use Illuminate\Http\Request;

class VatRateInvokeController extends Controller
{
    public function __invoke(Request $request)
    {
        $vatRates = VatRate::where('vat_code', $request->vat_code)->where('end_date', '>', $request->start_date)->count();

        return response()->json(['exists' => $vatRates]);
    }
}
