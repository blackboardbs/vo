<?php

namespace App\Http\Controllers;

use App\Models\AssessmentActivities;
use App\Models\AssessmentConcern;
use App\Models\AssessmentHeader;
use App\Models\AssessmentMaster;
use App\Models\AssessmentMasterDetails;
use App\Models\AssessmentMeasureScore;
use App\Models\AssessmentNotes;
use App\Models\AssessmentPlannedNext;
use App\Models\AssetConditionNote;
use App\Models\AssetRegister;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Config;
use App\Models\DigiSignStatus;
use App\Models\DigisignUsers;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\Expense;
use App\Jobs\SendAssertAknowledgementDigiSignEmailJob;
use App\Jobs\SendAssessmentDigiSignEmailJob;
use App\Jobs\SendSupplierAssignmentEmailJob;
use App\Jobs\SendTimesheetDigiSignEmailJob;
use App\Models\Module;
use App\Models\Project;
use App\Models\Template;
use App\Models\TimeExp;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\LaravelPdf\Facades\Pdf;
use TCPDF;

class DigiSignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $documents = Document::with('type', 'user', 'owner', 'digisignstatus')->sortable(['name' => 'asc']);
        $documents = $documents->whereIn('document_type_id', [2, 4, 6]);

        $module = Module::where('name', '=', 'App\Models\DocumentDigiSign')->get();

        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', -1);

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $documents = $documents->whereIn('owner_id', Auth::user()->team());
                $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', Auth::user()->team())->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', -1);
            }
        } else {
            return abort(403);
        }

        if (($request->has('q') && $request->input('q') != '')) {
            $documents = $documents->where('name', 'LIKE', '%'.$request->input('q').'%');
        }

        if ($request->has('type') && $request->input('type') != -1) {
            $documents = $documents->where('document_type_id', '=', $request->input('type'));
        }

        if ($request->has('resource') && $request->input('resource') != -1) {
            $documents = $documents->where('owner_id', '=', $request->input('resource'));
        }

        if ($request->has('from') && $request->input('from') != '') {
            $documents = $documents->where('created_at', '>=', $request->input('from'));
        }

        if ($request->has('to') && $request->input('to') != '') {
            $documents = $documents->where('created_at', '<=', $request->input('to'));
        }

        if ($request->has('status') && $request->input('status') != -1) {
            //dd($request->input('status'));
            $documents = $documents->where('digisign_status_id', '=', $request->input('status'));
        }

        $parameters = [
            'documents' => $documents->paginate($item),
            'resource_drop_down' => $resource_drop_down,
            'document_type_drop_down' => DocumentType::orderBy('name')->pluck('name', 'id')->prepend('All', -1),
            'digisign_status_drop_down' => DigiSignStatus::orderBy('name')->pluck('name', 'id')->prepend('All', -1),
        ];

        return view('digisign.index', $parameters);
    }

    public function signTimesheet($document_id, $resource_id, Request $request)
    {
        $document = Document::find($document_id);

        $timesheet = Timesheet::find($document->reference_id);

        $assignment = Assignment::where('employee_id', '=', $timesheet->employee_id)->where('project_id', '=', $timesheet->project_id)->first();

        $project = Project::find($assignment->project_id);

        $employee = User::find($timesheet->employee_id);

        $subject = 'Timesheet Digisign - '.$employee->first_name.' '.$employee->last_name.'(YearWeek '.$timesheet->year_week.') ';

        $product_ownder = User::find($assignment->product_owner_id);
        
        $resource = User::find($document->creator_id);

        $timelines = Timeline::where('timesheet_id', '=', $timesheet->id)->with('task')->get();
        $time_exp = TimeExp::where('timesheet_id', '=', $timesheet->id)->get();
        $timelines_drop_down = ['Select Group Task Number'];

        for ($i = 0; $i < 25; $i++) {
            $hours[$i] = $i;
        }

        $total_exp_bill = 0;
        $total_exp_claim = 0;
        foreach ($time_exp as $expense) {
            $total_exp_bill += ($expense->is_billable == 1) ? $expense->amount : 0;
            $total_exp_claim += ($expense->claim == 1) ? $expense->amount : 0;
        }

        $digisign_users_ids = DigisignUsers::where('document_id', '=', $document->id)->pluck('user_number', 'id');

        foreach ($digisign_users_ids as $digisign_users_id) {
            $date = date('Y-m-d');
            if ($resource_id == $digisign_users_id) {
                $digisign_users_tmp = DigisignUsers::where('user_number', '=', $resource_id)->where('document_id', '=', $document->id)->first();
                $digisign_user = DigisignUsers::find($digisign_users_tmp->id);
                $digisign_user->user_name = $request->input('name');
                $digisign_user->user_sign_date = $date;
                $digisign_user->signed = 1;
                $digisign_user->save();

                $subject .= 'Signed by ('.$request->input('name').')';
            }
        }

        try {
            $digisign_users_ids = DigisignUsers::where('document_id', '=', $document->id)->pluck('signed', 'user_number');

            $is_all_signed = true;
            foreach ($digisign_users_ids as $digisign_users_id) {
                if ($digisign_users_id != 1) {
                    $is_all_signed = false;
                }
            }

            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $document->file = $file_name;
            if ($is_all_signed) {
                $document->digisign_status_id = 4;
            } else {
                $document->digisign_status_id = 3;
            }
            $document->save();

            $digisign_users = DigisignUsers::where('document_id', '=', $document->id)->get();

            $parameters = [
                'timesheet' => $timesheet,
                'assignment' => $assignment,
                'timelines' => $timelines,
                'time_exp' => $time_exp,
                'employee' => $employee,
                'product_ownder' => $product_ownder,
                'total_exp_bill' => $total_exp_bill,
                'total_exp_claim' => $total_exp_claim,
                'hours' => $hours,
                'timelines_drop_down' => $timelines_drop_down,
                'digisign_users' => $digisign_users,
            ];

            $pdf = Pdf::view('timesheet.digisign_signed', $parameters);

            $timesheet_template_id = 1;
            if (isset($project->timesheet_template_id) && $project->timesheet_template_id > 0) {
                $timesheet_template_id = $project->timesheet_template_id;
            } else {
                $config_data = Config::orderBy('id')->first();
                if (isset($config_data->timesheet_template_id) && $config_data->timesheet_template_id > 0) {
                    $timesheet_template_id = $config_data->timesheet_template_id;
                }
            }

            if (($timesheet_template_id == 2) || ($timesheet_template_id == 3)) {
                //if($request->has('cfts_tid') && ($request->cfts_tid == 2)){

                $timeline_weekly_details = [];
                $timeline_counter = 0;
                $timeline_minutes_total_billable = 0;
                $timeline_minutes_total_non_billable = 0;
                foreach ($timelines as $timeline) {
                    $date = Carbon::now();

                    $date->setISODate(substr($timesheet->year_week, 0, 4), substr($timesheet->year_week, -2));
                    $start_of_week_date = date('Y-m-d', strtotime($date->startOfWeek()));

                    //Monday
                    $mon_date[$timesheet->year_week] = $start_of_week_date;
                    $tue_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $wed_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $thu_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $fri_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sat_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));
                    $sun_date[$timesheet->year_week] = date('Y-m-d', strtotime($date->addDays(1)));

                    $description_of_work = isset($timeline->task->description) ? $timeline->task->description : '';
                    if ($description_of_work != '') {
                        if ($timeline->description_of_work != '') {
                            $description_of_work .= ' - '.$timeline->description_of_work;
                        }
                    } else {
                        $description_of_work = $timeline->description_of_work;
                    }

                    if ((($timeline->mon * 60) + $timeline->mon_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->mon * 60) + $timeline->mon_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $mon_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->tue * 60) + $timeline->tue_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->tue * 60) + $timeline->tue_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $tue_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->wed * 60) + $timeline->wed_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->wed * 60) + $timeline->wed_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $wed_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->thu * 60) + $timeline->thu_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->thu * 60) + $timeline->thu_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $thu_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->fri * 60) + $timeline->fri_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->fri * 60) + $timeline->fri_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $fri_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sat * 60) + $timeline->sat_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sat * 60) + $timeline->sat_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sat_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }

                    if ((($timeline->sun * 60) + $timeline->sun_m) > 0) {
                        $timeline_weekly_details[$timeline_counter]['is_billable'] = $timeline->is_billable;
                        $timeline_weekly_details[$timeline_counter]['total_minutes'] = ($timeline->sun * 60) + $timeline->sun_m;
                        $timeline_weekly_details[$timeline_counter]['total_minutes_to_time'] = minutesToTime($timeline_weekly_details[$timeline_counter]['total_minutes']);
                        $timeline_weekly_details[$timeline_counter]['date'] = $sun_date[$timesheet->year_week];
                        $timeline_weekly_details[$timeline_counter]['project_code'] = isset($timesheet->project->ref) ? $timesheet->project->ref : '';
                        $timeline_weekly_details[$timeline_counter]['project_name'] = isset($timesheet->project->name) ? $timesheet->project->name : '';
                        $timeline_weekly_details[$timeline_counter]['description_of_work'] = $description_of_work;

                        if ($timeline_weekly_details[$timeline_counter]['is_billable'] == 1) {
                            $timeline_minutes_total_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        } else {
                            $timeline_minutes_total_non_billable += $timeline_weekly_details[$timeline_counter]['total_minutes'];
                        }

                        $timeline_counter += 1;
                    }
                }

                $total_time_billable = minutesToTime($timeline_minutes_total_billable);
                $total_time_non_billable = minutesToTime($timeline_minutes_total_non_billable);

                $parameters['timeline_weekly_details'] = $timeline_weekly_details;
                $parameters['total_time_billable'] = $total_time_billable;
                $parameters['total_time_non_billable'] = $total_time_non_billable;

                $pdf = Pdf::view('timesheet.digisign_signed_weekly_detail', $parameters);

                if ($timesheet_template_id == 3) {
                    $pdf = Pdf::view('timesheet.digisign_signed_weekly_detail_arbour', $parameters);
                }
            }

            $pdf->save($file_name_pdf);

            foreach ($digisign_users as $digisign_user) {
                if ($digisign_user->user_email != '') {
                    //Mail::to(trim($digisign_user->user_email))->send(new TimesheetDigiSignMail($document->id, $digisign_user->user_number, $digisign_user->user_name, $subject));
                    SendTimesheetDigiSignEmailJob::dispatch(trim($digisign_user->user_email), $document->id, $digisign_user->user_number, $digisign_user->user_name, $subject)->delay(now()->addMinutes(5));
                }
            }

            //return response()->file($file_name_pdf);
        } catch (\Exception $e) {
            report($e);

            return redirect(route('timesheet.show', $timesheet))->with('flash_danger', $e->getMessage());
        }

        
        $parameters = [
            'resource_name' => $resource->email,
            'is_signed' => true,
            'has_digisign' => true,
            'document_id' => $document_id,
            'resource_id' => $resource->id,
            'pdf_url' => $file_name_pdf,
            'flash_success' => 'Document signed successfully'
        ];

        // return redirect(route('document.viewer', $document->id))->with('flash_success', 'Document signed successfully');
        return view('document.viewer')->with($parameters);
    }

    public function sign($document_id, $resource_id, Request $request): RedirectResponse
    {
        $resource = User::find($resource_id);
        $document = Document::find($document_id);

        $template = Template::find($document->type_id);
        if (empty($template)) {
            return redirect(route('digisign.index'))->with('flash_success', 'Template document not found');
        }
        $html = $template->content;
        $resoure_name = (isset($resource->first_name) ? $resource->first_name : '').' '.(isset($resource->last_name) ? $resource->last_name : '');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($resoure_name);
        $pdf->SetTitle('Digitally Signed Document');
        $pdf->SetSubject('Digitally Signed Document');
        $pdf->SetKeywords('digital, signature');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 052', PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        /*// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // ---------------------------------------------------------

        /*
        NOTES:
         - To create self-signed signature: openssl req -x509 -nodes -days 365000 -newkey rsa:1024 -keyout tcpdf.crt -out tcpdf.crt
         - To export crt to p12: openssl pkcs12 -export -in tcpdf.crt -out tcpdf.p12
         - To convert pfx certificate to pem: openssl pkcs12 -in tcpdf.pfx -out tcpdf.crt -nodes
        */

        // set certificate file
        //$certificate = 'file://data/cert/tcpdf.crt';
        $certificate = 'file://'.realpath('/var/www/digisign.crt');
        //$certificate = 'file://'.realpath('C:/Users/User/tcpdf.crt');

        // set additional information
        $info = [
            'Name' => $request->input('name'),
            'Location' => $request->input('place'),
            'Reason' => 'Sign Document for approval',
            'ContactInfo' => $resource->email,
        ];

        // set document signature
        $pdf->setSignature($certificate, $certificate, 'preprod', '', 2, $info);

        // set font
        $pdf->SetFont('helvetica', '', 12);

        // add a page
        $pdf->AddPage();

        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $document->reference_id)->get()->ToArray();
        $expense = Expense::find($assignment[0]['exp_id']);

        $resource_user = User::find($assignment[0]['consultant']['id']);

        $supplier_name = '';
        if ($resource_user->vendor_id > 0 && $resource_user->vendor_id != '' && $resource_user->vendor_id != null) {
            $vendor = Vendor::find($resource_user->vendor_id);
            $supplier_name = $vendor->vendor_name;
        }
        $resource_name = $resource_user->first_name.' '.$resource_user->last_name;

        $variables = ['{SUPPLIERNAME}', '{RESOURCENAME}', '{SERVICESRENDERED}', '{PROJECTNAME}', '{DESCRIPTIONOFASSIGNMENT}', '{EFFECTIVEDATE}', '{TERMINATIONDATE}', '{RATEPERHOUR}', '{TOTALHOURS}', '{HOURSOFWORK}', '{LOCATION}', '{TRAVEL}', '{ACCOMODATION}', '{PARKING}', '{PERDIEM}', '{OUTOFTOWN}', '{DATA}', '{OTHER}', '{BILLING}', '{PAYMENTS}', '{SIGNATURE_INTERNAL}', '{SIGNATURE_SUPPLIER}'];
        $replaced_with = [$supplier_name, $resource_name, $assignment[0]['note_1'], $assignment[0]['project']['name'], $assignment[0]['note_2'], $assignment[0]['start_date'], $assignment[0]['end_date'], $assignment[0]['invoice_rate'], $assignment[0]['hours'], $assignment[0]['hours_of_work'], $assignment[0]['location'], isset($expense->travel) ? $expense->travel : '', isset($expense->accomodation) ? $expense->accomodation : '', isset($expense->parking) ? $expense->parking : '', isset($expense->per_diem) ? $expense->per_diem : '', isset($expense->out_of_town) ? $expense->out_of_town : '', isset($expense->data) ? $expense->data : '', isset($expense->other) ? $expense->other : '', $assignment[0]['billable'] == 1 ? 'Yes' : 'No', $assignment[0]['invoice_rate'] * $assignment[0]['hours'], '', ''];
        //$replaced_with = Array($vendor_user->first_name.' '.$vendor_user->last_name, $resource_user->first_name.' '.$resource_user->last_name);
        $html = str_replace($variables, $replaced_with, $html);

        // print a line of text
        //$text = 'This is a <b color="#FF0000">digitally signed document</b> using the default (example) <b>tcpdf.crt</b> certificate.<br />To validate this signature you have to load the <b color="#006600">tcpdf.fdf</b> on the Arobat Reader to add the certificate to <i>List of Trusted Identities</i>.<br /><br />For more information check the source code of this example and the source code documentation for the <i>setSignature()</i> method.<br /><br /><a href="http://www.tcpdf.org">www.tcpdf.org</a>';
        $pdf->writeHTML($html, true, 0, true, 0);

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // *** set signature appearance ***

        // create content for signature (image and/or text)
        //$pdf->Image('images/tcpdf_signature.png', 180, 60, 15, 15, 'PNG');
        //pdfsign.js

        $pdf->setXY(15, 250);
        $pdf->SetFont('helveticaI', '', 12);
        $pdf->Cell(50, 10, $resoure_name, 0, $ln = 0, 'l', 0, '', 0, false, 'B', 'c');
        $pdf->setXY(140, 250);
        $pdf->Cell(50, 10, $request->input('date'), 0, $ln = 0, 'l', 0, '', 0, false, 'B', 'c');
        $linestyle = ['width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '', 'phase' => 0, 'color' => [0, 0, 0]];
        $pdf->Line(15, 250, 70, 250, $linestyle); //Name
        $pdf->Line(140, 250, 195, 250, $linestyle); //Date

        $pdf->setXY(15, 260);
        $pdf->SetFont('helveticaI', '', 12);
        $pdf->Cell(50, 10, 'Signed for '.$request->input('company'), 0, $ln = 0, 'l', 0, '', 0, false, 'B', 'c');
        $pdf->setXY(140, 260);
        $pdf->Cell(50, 10, 'Date', 0, $ln = 0, 'l', 0, '', 0, false, 'B', 'c');

        // define active area for signature appearance
        $pdf->setSignatureAppearance(20, 260, 15, 15);

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        // *** set an empty signature appearance ***
        $pdf->addEmptySignatureAppearance(20, 280, 15, 15);

        // ---------------------------------------------------------

        $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
        $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
        $file_name_pdf = $dir.'file_'.date('Y_m_d_H_i_s').'.pdf';

        //Close and output PDF document
        //$pdf->Output('example_052.pdf', 'D');
        //$pdf->Output('example_052.pdf', 'F');
        $pdf->Output($file_name_pdf, 'F');

        $document->digisign_status_id = 4;
        $document->file = $file_name;
        $document->save();

        return redirect(route('digisign.index'))->with('flash_success', 'Document successfully signed');
    }

    public function sendAssetDigiSign($asset_id, $digisign_users_id): RedirectResponse
    {
        $digisign_users = DigisignUsers::find($digisign_users_id);
        $asset = AssetRegister::with('asset_class_name')->find($asset_id);
        $asset_condition_notes = AssetConditionNote::where('asset_id', '=', $asset->id)->where('include_in_acceptance_letter', '=', 1)->get();
        $resource = User::find(Auth::id());

        $user = User::find($digisign_users->user_id);

        $subject = 'Aknowledgement of Receipt - '.$resource->first_name.' '.$resource->last_name;

        $config = Config::get()->first();
        if (isset($config->company_id)) {
            $company = Company::find($config->company_id);
        } else {
            $company = Company::find($config->company_id);
        }

        if ($digisign_users->is_claim_approver == 1) {
            if ($user->signed == 1) {
                $claim_approver_signature = $digisign_users->user_name;
                $claim_approver_location = $digisign_users->location;
                $claim_approver_date = $digisign_users->user_sign_date;
            } else {
                $claim_approver_signature = $user->first_name.' '.$user->last_name;
                $claim_approver_location = 'Pretoria';
                $claim_approver_date = date('Y-m-d');
                $digisign_users->user_name = $claim_approver_signature;
                $digisign_users->location = $claim_approver_location;
                $digisign_users->user_sign_date = $claim_approver_date;
            }

            $digisign_users->user_name = $claim_approver_signature;
        } else {
            $resource_user = DigisignUsers::where('document_id', '=', $digisign_users->document_id)->where('is_claim_approver', '=', 1)->first();
            $claim_approver_signature = $resource_user->user_name;
            $claim_approver_location = $resource_user->location;
            $claim_approver_date = $resource_user->user_sign_date;
        }

        if ($digisign_users->is_claim_approver == 0) {
            if ($user->signed == 1) {
                $resource_signature = $digisign_users->user_name;
                $resource_location = $digisign_users->location;
                $resource_date = $digisign_users->user_sign_date;
            } else {
                $resource_signature = $user->first_name.' '.$user->last_name;
                $resource_location = 'Pretoria';
                $resource_date = date('Y-m-d');
                $digisign_users->user_name = $resource_signature;
                $digisign_users->location = $resource_location;
                $digisign_users->user_sign_date = $resource_date;
            }
            $digisign_users->user_name = $resource_signature;
        } else {
            $claim_approver_user = DigisignUsers::where('document_id', '=', $digisign_users->document_id)->where('is_claim_approver', '=', 0)->first();
            $resource_signature = $claim_approver_user->user_name;
            $resource_location = $claim_approver_user->location;
            $resource_date = $claim_approver_user->user_sign_date;
        }

        //dd($claim_approver_signature.'-'.$claim_approver_location.'-'.$claim_approver_date.'-'.$resource_signature.'-'.$resource_location.'-'.$resource_date);

        $digisign_users->save();

        //$location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        //dd($location);

        $parameters = [
            'asset' => $asset,
            'resource' => $resource,
            'asset_condition_notes' => $asset_condition_notes,
            'resource_signature' => $resource_signature,
            'resource_date' => $resource_date,
            'resource_location' => $resource_location,
            'claim_approver_date' => $claim_approver_date,
            'claim_approver_signature' => $claim_approver_signature,
            'claim_approver_location' => $claim_approver_location,
            'signature' => ' __________________________________',
            'signed_at' => '_______________________',
            'date' => '______/____/____',
            'company' => $company,
        ];

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            Pdf::view('template.asset_aknowledgement_form', $parameters)
                ->save($file_name_pdf);

            $document = Document::find($digisign_users->document_id);
            $document->file = $file_name;
            $document->save();

            //Change status to signed
            $digisign_users->signed = 1;
            $digisign_users->location = 'Pretoria';
            $digisign_users->save();

            $digisign_all_users = DigisignUsers::where('document_id', '=', $digisign_users->document_id)->get();

            //dd($digisign_all_users);

            foreach ($digisign_all_users as $digisign_user) {
                $user = User::find($digisign_user->user_id);
                $report_to_name = $user->first_name.' '.$user->last_name;
                //Mail::to(trim($user->email))->send(new AssetAknowledgementDigiSignMail($asset->id, $digisign_user->id, $report_to_name, $subject, $digisign_user->signed));
                SendAssertAknowledgementDigiSignEmailJob::dispatch(trim($user->email), $asset->id, $digisign_user->id, $report_to_name, $subject, $digisign_user->signe)->delay(now()->addMinutes(5));
            }
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('assetreg.show', $asset_id)->with('flash_danger', $e->getMessage()));
        }

        return redirect(route('assetreg.show', $asset))->with('flash_success', 'Digisign successfully sent');
    }

    public function sendSupplierAssignmentDigiSign($assignment_id, $digisign_users_id): RedirectResponse
    {
        $assignment = Assignment::with('consultant', 'project')->where('id', '=', $assignment_id)->first();
        $expense = Expense::find(isset($assignment->exp_id) ? $assignment->exp_id : 0);
        $claim_approver_user = User::find(isset($assignment->assignment_approver) ? $assignment->assignment_approver : 0);
        $claim_approver_first_name = isset($claim_approver_user->first_name) ? $claim_approver_user->first_name : '';
        $claim_approver_last_name = isset($claim_approver_user->last_name) ? $claim_approver_user->last_name : '';
        $claim_approver = $claim_approver_first_name.' '.$claim_approver_last_name;

        $resource_user = User::find(isset($assignment->employee_id) ? $assignment->employee_id : 0);

        $resource_first_name = isset($resource_user->first_name) ? $resource_user->first_name : '';
        $resource_last_name = isset($resource_user->last_name) ? $resource_user->last_name : '';

        $resource_name = $resource_first_name.' '.$resource_last_name;

        $config = Config::get()->first();
        if (isset($config->company_id)) {
            $company = Company::find($config->company_id);
        } else {
            $company = Company::find($config->company_id);
        }

        try {
            $file_name = 'file_'.date('Y_m_d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $digisign_user = DigisignUsers::find($digisign_users_id);
            $document = Document::find($digisign_user->document_id);
            $document->save();

            $user = User::find($digisign_user->user_id);

            if ($digisign_user->is_claim_approver == 1) {
                if ($user->signed == 1) {
                    $claim_approver_signature = $digisign_user->user_name;
                    $claim_approver_location = $digisign_user->location;
                    $claim_approver_date = $digisign_user->user_sign_date;
                } else {
                    $claim_approver_signature = $user->first_name.' '.$user->last_name;
                    $claim_approver_location = 'Pretoria';
                    $claim_approver_date = date('Y-m-d');
                    $digisign_user->user_name = $claim_approver_signature;
                    $digisign_user->location = $claim_approver_location;
                    $digisign_user->user_sign_date = $claim_approver_date;
                }

                $digisign_user->user_name = $claim_approver_signature;
            } else {
                $vendor_user = DigisignUsers::where('document_id', '=', $digisign_user->document_id)->where('is_claim_approver', '=', 1)->first();
                $claim_approver_signature = isset($vendor_user->user_name) ? $vendor_user->user_name : '';
                $claim_approver_location = isset($vendor_user->location) ? $vendor_user->location : '';
                $claim_approver_date = isset($vendor_user->user_sign_date) ? $vendor_user->user_sign_date : '';
            }

            if ($digisign_user->is_claim_approver == 0) {
                if ($user->signed == 1) {
                    $resource_signature = $digisign_user->user_name;
                    $resource_location = $digisign_user->location;
                    $resource_date = $digisign_user->user_sign_date;
                } else {
                    $resource_signature = $user->first_name.' '.$user->last_name;
                    $resource_location = 'Pretoria';
                    $resource_date = date('Y-m-d');
                    $digisign_user->user_name = $resource_signature;
                    $digisign_user->location = $resource_location;
                    $digisign_user->user_sign_date = $resource_date;
                }
                $digisign_user->user_name = $resource_signature;
            } else {
                $claim_approver_user = DigisignUsers::where('document_id', '=', $digisign_user->document_id)->where('is_claim_approver', '=', 0)->first();
                $resource_signature = isset($claim_approver_user->user_name) ? $claim_approver_user->user_name : '';
                $resource_location = isset($claim_approver_user->location) ? $claim_approver_user->location : '';
                $resource_date = isset($claim_approver_user->user_sign_date) ? $claim_approver_user->user_sign_date : '';
            }

            $vendor = null;
            if (isset($resource_user->vendor_id) && $resource_user->vendor_id > 0) {
                $vendor = Vendor::find($resource_user->vendor_id);
            }

            $parameters = [
                'assignment' => $assignment,
                'resource_signature' => $resource_signature,
                'resource_date' => $resource_date,
                'resource_location' => $resource_location,
                'claim_approver_date' => $claim_approver_date,
                'claim_approver_signature' => $claim_approver_signature,
                'claim_approver_location' => $claim_approver_location,
                'signature' => ' __________________________________',
                'signed_at' => '_______________________',
                'date' => '______/____/____',
                'company' => $company,
                'company_name' => isset($company->company_name) ? $company->company_name : '',
                'supplier_name' => isset($vendor->vendor_name) ? $vendor->vendor_name : '',
                'resource' => $resource_user,
                'resource_name' => $resource_name,
                'claim_approver' => $claim_approver,
                'assignment' => $assignment,
                'expense' => $expense,
            ];

            //dd($parameters);

            Pdf::view('template.supplier_assignment', $parameters)
                ->save($file_name_pdf);

            $document = Document::find($digisign_user->document_id);
            $document->file = $file_name;
            $document->save();

            //Change status to signed
            $digisign_user->signed = 1;
            $digisign_user->location = 'Pretoria';
            $digisign_user->save();

            $digisign_all_users = DigisignUsers::where('document_id', '=', $digisign_user->document_id)->get();

            foreach ($digisign_all_users as $digisign_user) {
                $user = User::find($digisign_user->user_id);
                $report_to_name = $user->first_name.' '.$user->last_name;
                //Mail::to(trim($user->email))->send(new SupplierAssignmentMail($report_to_name, $digisign_user->id, $assignment_id));
                SendSupplierAssignmentEmailJob::dispatch(trim($user->email), $report_to_name, $digisign_user->id, $assignment_id)->delay(now()->addMinutes(5));
            }

            //return redirect(route('assignment.show', $assignment_id))->with('flash_success', "Supplier Assignemnt digisign sent successfully.");
            return redirect()->back()->with('flash_success', 'Supplier Assignemnt digisign sent successfully.');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect(route('assignment.show', $assignment_id)->with('flash_danger', $e->getMessage()));
        }
    }

    public function sendAssessmentDigisign($id, Request $request): RedirectResponse
    {
        $assessmentHeader = AssessmentHeader::find($id);

        $resourceFirstName = isset($assessmentHeader->user->first_name) ? $assessmentHeader->user->first_name : '';
        $resourceLastName = isset($assessmentHeader->user->last_name) ? $assessmentHeader->user->last_name : '';
        $resourceEmail = isset($assessmentHeader->user->email) ? $assessmentHeader->user->email : '';

        $resourceFullName = $resourceFirstName.' '.$resourceLastName;

        $config = Config::first();
        $config_assessmnet_master = $config->assessment_master;
        $company = Company::find((isset($config->company_id) ? $config->company_id : 1));
        $assessment_master = [];
        $assessment_master_details = [];
        foreach (explode(',', $config_assessmnet_master) as $master) {
            $assessment_master[] = AssessmentMaster::find($master);
            $assessment_master_details[] = AssessmentMasterDetails::where('assessment_master_id', '=', $master)->get();
        }

        $assessment_header = AssessmentHeader::find($id);
        $assessment_activity = AssessmentActivities::where('assessment_id', '=', $id)->paginate(10);
        $assessment_plan = AssessmentPlannedNext::where('assessment_id', '=', $id)->paginate(10);
        $assessment_concerns = AssessmentConcern::where('assessment_id', '=', $id)->paginate(10);
        $assessment_notes = AssessmentNotes::where('assessment_id', '=', $id)->paginate(10);
        $assessment_measure = AssessmentMeasureScore::where('assessment_id', '=', $assessment_header->id)->get();

        $parameters = [
            'assessment_activities' => $assessment_activity,
            'assessment_header' => $assessment_header,
            'competency_level_dropdown' => [0 => 'Select Level', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            'assessment_plan' => $assessment_plan,
            'assessment_concerns' => $assessment_concerns,
            'assessment_notes' => $assessment_notes,
            'assessment_master' => $assessment_master,
            'assessment_master_details' => $assessment_master_details,
            'assessment_measure' => $assessment_measure,
            'company' => $company,
        ];

        $_subject = $request->subject;

        try {
            $pdf = Pdf::view('assessment.assessment_header.digisign', $parameters)
                 ->format('a4');
            //$file_name = "file_" . date("Y_m_d_H_i_s") . ".pdf";
            $file_name = (isset($assessmentHeader->user) ? substr($assessmentHeader->user->first_name, 0, 4).'_'.substr($assessmentHeader->user->last_name, 0, 4) : '').'_'.(isset($assessment_header->customer) ? str_replace(' ', '_', $assessmentHeader->customer->customer_name) : '').'_'.date('Y-m-d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'documents'.DIRECTORY_SEPARATOR.'assessments'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $pdf->save($file_name_pdf);

            $document = new Document();
            $document->type_id = 12;
            $document->document_type_id = 12; //document type Assignment(documenttype table in db)
            $document->name = 'Assessment Digisign - '.$resourceFullName;
            $document->file = $file_name;
            $document->creator_id = auth()->id();
            $document->owner_id = isset($assessmentHeader->user->id) ? $assessmentHeader->user->id : 0;
            $document->digisign_status_id = 2;
            $document->reference_id = $assessmentHeader->id;
            $document->digisign_approver_user_id = isset($assessmentHeader->user->id) ? $assessmentHeader->user->id : 0;
            $document->save();

            $digisign_users = new DigisignUsers();
            $digisign_users->document_id = $document->id;
            $digisign_users->user_name = $resourceFullName;
            $digisign_users->user_email = $resourceEmail;
            $digisign_users->user_id = isset($assessmentHeader->user->id) ? $assessmentHeader->user->id : 0;
            $digisign_users->owner_id = isset($assessmentHeader->user->id) ? $assessmentHeader->user->id : 0;
            $digisign_users->user_number = 1;
            $digisign_users->is_claim_approver = 1;
            $digisign_users->signed = 0;
            $digisign_users->creator_id = auth()->id();
            $digisign_users->save();

            if (isset($assessmentHeader->user->email)) {
                /*Mail::to($assessmentHeader->user->email)->send(new AssessmentDigisignEmail($_subject, isset($assessmentHeader->customer->customer_name)?$assessmentHeader->customer->customer_name:'<CustomerName>',
                    isset($assessmentHeader->user) ? $assessmentHeader->user->first_name.' '.$assessmentHeader->user->last_name : '',
                    $digisign_users->id, $assessmentHeader->id));*/
                SendAssessmentDigiSignEmailJob::dispatch($assessmentHeader->user->email, $_subject, $assessmentHeader->customer->customer_name ?? null, $assessmentHeader->user->name() ?? null, $digisign_users->id, $assessmentHeader->id)->delay(now()->addMinutes(5));
            }

            return redirect()->back()->with('flash_success', 'Assessment digisign has been sent');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect()->back()->with('flash_danger', $e->getMessage());
        }
    }

    public function signAssessmentDigisign($assessment_id, $digisign_user_id, Request $request): RedirectResponse
    {
        $assessmentHeader = AssessmentHeader::find($assessment_id);

        $resourceFirstName = isset($assessmentHeader->user->first_name) ? $assessmentHeader->user->first_name : '';
        $resourceLastName = isset($assessmentHeader->user->last_name) ? $assessmentHeader->user->last_name : '';
        $resourceEmail = isset($assessmentHeader->user->email) ? $assessmentHeader->user->email : '';

        $resourceFullName = $resourceFirstName.' '.$resourceLastName;

        $config = Config::first();
        $config_assessmnet_master = $config->assessment_master;
        $company = Company::find((isset($config->company_id) ? $config->company_id : 1));
        $assessment_master = [];
        $assessment_master_details = [];
        foreach (explode(',', $config_assessmnet_master) as $master) {
            $assessment_master[] = AssessmentMaster::find($master);
            $assessment_master_details[] = AssessmentMasterDetails::where('assessment_master_id', '=', $master)->get();
        }

        $assessment_header = AssessmentHeader::find($assessment_id);
        $assessment_activity = AssessmentActivities::where('assessment_id', '=', $assessment_id)->paginate(10);
        $assessment_plan = AssessmentPlannedNext::where('assessment_id', '=', $assessment_id)->paginate(10);
        $assessment_concerns = AssessmentConcern::where('assessment_id', '=', $assessment_id)->paginate(10);
        $assessment_notes = AssessmentNotes::where('assessment_id', '=', $assessment_id)->paginate(10);
        $assessment_measure = AssessmentMeasureScore::where('assessment_id', '=', $assessment_header->id)->get();

        $parameters = [
            'assessment_activities' => $assessment_activity,
            'assessment_header' => $assessment_header,
            'competency_level_dropdown' => [0 => 'Select Level', 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            'assessment_plan' => $assessment_plan,
            'assessment_concerns' => $assessment_concerns,
            'assessment_notes' => $assessment_notes,
            'assessment_master' => $assessment_master,
            'assessment_master_details' => $assessment_master_details,
            'assessment_measure' => $assessment_measure,
            'company' => $company,
            'signed_resource' => $request->name,
            'signed_date' => date('Y-m-d'),
        ];

        $_subject = $request->subject;

        try {
            $pdf = Pdf::view('assessment.assessment_header.signed_digisign', $parameters)
                ->format('a4');
            //$file_name = "file_" . date("Y_m_d_H_i_s") . ".pdf";
            $file_name = (isset($assessmentHeader->user) ? substr($assessmentHeader->user->first_name, 0, 4).'_'.substr($assessmentHeader->user->last_name, 0, 4) : '').'_'.(isset($assessment_header->customer) ? str_replace(' ', '_', $assessmentHeader->customer->customer_name) : '').'_'.date('Y-m-d_H_i_s').'.pdf';
            $dir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'documents'.DIRECTORY_SEPARATOR.'assessments'.DIRECTORY_SEPARATOR;
            $file_name_pdf = $dir.$file_name;

            $pdf->save($file_name_pdf);

            $digisign_users = DigisignUsers::find($digisign_user_id);
            $digisign_users->signed = 1;
            $digisign_users->save();

            $document = Document::find($digisign_users->document_id);
            $document->file = $file_name;
            $document->digisign_status_id = 2;
            $document->save();

            if (isset($assessmentHeader->user->email)) {
                /*Mail::to($assessmentHeader->user->email)->send(new AssessmentDigisignEmail($_subject, isset($assessmentHeader->customer->customer_name)?$assessmentHeader->customer->customer_name:'<CustomerName>',
                    isset($assessmentHeader->user) ? $assessmentHeader->user->first_name.' '.$assessmentHeader->user->last_name : '',
                    $digisign_users->id, $assessmentHeader->id));*/
                SendAssessmentDigiSignEmailJob::dispatch($assessmentHeader->user->email, $_subject, $assessmentHeader->customer->customer_name ?? null, $assessmentHeader->user->name() ?? null, $digisign_users->id, $assessmentHeader->id)->delay(now()->addMinutes(5));
            }

            return redirect()->back()->with('flash_success', 'Assessment digisign has been signed successfully');
        } catch (FileAlreadyExistsException $e) {
            report($e);

            return redirect()->back()->with('flash_danger', $e->getMessage());
        }
    }
}
