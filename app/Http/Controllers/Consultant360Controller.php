<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Availability;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Experience;
use App\Models\IndustryExperience;
use App\Models\Leave;
use App\Models\LeaveBalance;
use App\Models\PlanUtilization;
use App\Models\ProjectType;
use App\Models\Qualification;
use App\Models\Resource;
use App\Models\ResourceType;
use App\Models\Skill;
use App\Models\Team;
use App\Models\TimeExp;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class Consultant360Controller extends Controller
{
    private $billable_hours = 'SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS hours';

    private $invoice_rate = 'IFNULL(assignment.invoice_rate,0) AS invoice_rate';

    private $internal_cost_rate = 'IFNULL(assignment.internal_cost_rate,0) AS internal_cost_rate';

    private $external_cost_rate = 'IFNULL(assignment.external_cost_rate,0) AS external_cost_rate';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $resource = Resource::with(['user'])->where('util', '=', 1)->whereHas('user');
        if (isset($request->company)) {
            $resource = $resource->whereHas('user', function ($query) use ($request) {
                $query->where('company_id', '=', $request->company);
            });
        }
        if (isset($request->team)) {
            $resource = $resource->where('team_id', '=', $request->team);
        }
        if (isset($request->resource)) {
            $resource = $resource->where('user_id', '=', $request->resource);
        }
        if (isset($request->resource_type)) {
            $resource = $resource->where('resource_type', '=', $request->resource_type);
        }
        if (isset($request->project_type)) {
            $resource = $resource->where('project_type', '=', $request->project_type);
        }
        if (isset($request->report_to)) {
            $resource = $resource->where('manager_id', '=', $request->report_to);
        }
        if (isset($request->cost_center)) {
            $resource = $resource->where('cost_center_id', '=', $request->cost_center);
        }
        $resource = $resource->paginate($request->has('s') ? $request->s : 15);
        $calendar = Calendar::select('year', 'month')->whereDate('date', '<=', Carbon::now()->toDateString())->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [

            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'team_drop_down' => Team::filters()->orderBy('team_name')->pluck('team_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'yearmonth_dropdown' => $yearmonth,
            'date' => Carbon::now(),
            'resources' => $resource,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'consultant360');

        return view('reports.management.consultant360')->with($parameters);
    }

    public function detailedView($res_id, Request $request): View
    {
        $resource = Resource::find($res_id);
        $date = Carbon::now();
        $margin_months = Assignment::selectRaw($this->invoice_rate.', '.$this->internal_cost_rate.', '.$this->external_cost_rate.', timesheet.month, timesheet.year, '.$this->billable_hours)
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
            })
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('assignment.employee_id', '=', $resource->user_id)
            ->whereBetween('timesheet.year', [$date->now()->subMonths(12)->year, $date->now()->year])
            ->where('assignment.billable', '=', 1)
            ->groupBy('assignment.invoice_rate', 'assignment.internal_cost_rate', 'assignment.external_cost_rate', 'timesheet.month', 'timesheet.year')
            ->orderBy('timesheet.year', 'desc')
            ->orderBy('timesheet.month', 'desc')
            ->get();

        $temp_diff = 0;
        $temp_base = 0;
        $months = [];
        foreach ($margin_months as $key => $monthly) {
            $next_index = $key + 1;
            $temp_month = isset($margin_months[$next_index]) ? $margin_months[$next_index]->month : 0;
            if ($monthly->month != $temp_month) {
                $temp_diff += (($monthly->invoice_rate * $monthly->hours) - (($monthly->external_cost_rate + $monthly->internal_cost_rate) * $monthly->hours));
                $temp_base += ($monthly->invoice_rate * $monthly->hours);
                $margin = ($temp_diff != 0 && $temp_base != 0) ? ($temp_diff / $temp_base) * 100 : 0;
                array_push($months, ['rate_perc' => round($margin), 'year_month' => $monthly->year.'/'.(($monthly->month < 10) ? '0'.$monthly->month : $monthly->month)]);
                $temp_diff = 0;
                $temp_base = 0;
            } else {
                $temp_diff += (($monthly->invoice_rate * $monthly->hours) - (($monthly->external_cost_rate + $monthly->internal_cost_rate) * $monthly->hours));
                $temp_base += ($monthly->invoice_rate * $monthly->hours);
            }
        }

        $planned_util = PlanUtilization::select('wk_hours')->first();
        $billable_hours = Timesheet::selectRaw($this->billable_hours.', timesheet.year_week')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('timesheet.employee_id', '=', $resource->user_id)
            ->whereBetween('timesheet.year_week', [$date->now()->subWeeks(26)->year.(($date->now()->subWeeks(26)->weekOfYear < 10) ? '0'.$date->now()->subWeeks(26)->weekOfYear : $date->now()->subWeeks(26)->weekOfYear), $date->now()->year.(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear)])
            ->groupBy('timesheet.year_week')
            ->orderBy('timesheet.year_week', 'desc')
            ->get();

        $util_per_week_diff = [];
        $util_per_year_week = [];

        foreach ($billable_hours as $billable) {
            array_push($util_per_week_diff, round(((($billable->hours != 0) ? $billable->hours / (isset($planned_util->wk_hours) ? $planned_util->wk_hours : 40) : 0) * 100)));
            array_push($util_per_year_week, $billable->year_week);
        }

        $this_year_income = $this->income_per_year($request, $resource, $date, 0);
        $current_year_income = 0;
        $this_year_c_t_c = 0;
        $current_year_hours = 0;
        foreach ($this_year_income as $income) {
            $current_year_income += ($income->hours * $income->invoice_rate);
            $this_year_c_t_c += (($income->internal_cost_rate + $income->external_cost_rate) * $income->hours);
            $current_year_hours += $income->hours;
        }

        $last_year_income = $this->income_per_year($request, $resource, $date, 1);
        $prev_year_income = 0;
        $last_year_c_t_c = 0;
        $prev_year_hours = 0;
        foreach ($last_year_income as $income) {
            $prev_year_income += ($income->hours * $income->invoice_rate);
            $last_year_c_t_c += (($income->internal_cost_rate + $income->external_cost_rate) * $income->hours);
            $prev_year_hours += $income->hours;
        }

        $average_c_t_c_rate = Assignment::select('internal_cost_rate', 'external_cost_rate')
            ->where('employee_id', '=', $resource->user_id)
            ->where('billable', '=', 1)
            ->groupBy('external_cost_rate', 'internal_cost_rate')
            ->get();

        $temp_avg = 0;
        foreach ($average_c_t_c_rate as $average) {
            $temp_avg += ($average->internal_cost_rate + $average->external_cost_rate);
        }
        $c_t_c_rate_average = isset($average_c_t_c_rate) ? (($temp_avg != 0) ? $temp_avg / $average_c_t_c_rate->count() : 0) : 0;

        $open_ass = Assignment::select('invoice_rate')
            ->where('employee_id', '=', $resource->user_id)
            ->whereIn('assignment_status', [2, 3])
            ->where('billable', '=', 1)
            ->get();

        $temp_avg_rate = 0;
        foreach ($open_ass as $assignment) {
            $temp_avg_rate += $assignment->invoice_rate;
        }
        $avg_invoice_rate = isset($open_ass) ? (($temp_avg_rate != 0) ? $temp_avg_rate / $open_ass->count() : 0) : 0;

        $outstanding_timesheet = [];
        $weeks = PlanUtilization::select('yearwk')->whereBetween('yearwk', [$date->now()->subWeeks(5)->year.(($date->now()->subWeeks(5)->weekOfYear < 10) ? '0'.$date->now()->subWeeks(5)->weekOfYear : $date->now()->subWeeks(5)->weekOfYear), $date->now()->year.(($date->now()->weekOfYear < 10) ? '0'.$date->now()->weekOfYear : $date->now()->weekOfYear)])->orderBy('yearwk', 'desc')->get();
        $res_weeks = [];
        $time_res = Assignment::select('employee_id')->whereIn('assignment_status', [2, 3])->where('employee_id', '=', $resource->user_id)->where('billable', '=', 1)->get()->count();

        if ($time_res) {
            foreach ($weeks as $week) {
                $time = Timesheet::select('year_week')->where('employee_id', '=', $resource->user_id)->where('year_week', '=', $week->yearwk)->first();
                if (isset($time)) {
                    array_push($res_weeks, null);
                } else {
                    array_push($res_weeks, $week->yearwk);
                }
            }
        }

        $calendar = Calendar::select('year', 'month')
            ->whereBetween('date', [$date->now()->subYears(2)->toDateString(), $date->now()->toDateString()])
            ->groupBy('year', 'month')
            ->orderBy('year', 'desc')
            ->orderBy('month', 'desc')
            ->get();
        $year_month_drop_down = [];
        foreach ($calendar as $cal) {
            $year_month_drop_down[$cal->year.(($cal->month < 10) ? '0'.$cal->month : $cal->month)] = $cal->year.(($cal->month < 10) ? '0'.$cal->month : $cal->month);
        }

        $leave_balance = LeaveBalance::where('resource_id', '=', $resource->user_id)
            ->whereYear('cycle_start', '=', $date->now()->year)->latest()->first();

        $leaves = Leave::where('emp_id', '=', $resource->user_id)->where('date_from', '>=', isset($leave_balance->cycle_start) ? $leave_balance->cycle_start : '0000-00-00')->get();
        $leave_taken = 0;
        foreach ($leaves as $leave) {
            $leave_taken += $leave->no_of_days;
        }
        $average_billable_rate = Timesheet::selectRaw($this->billable_hours.', IFNULL(assignment.rate,0)AS bonus_rate')
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
            })
            ->where('timesheet.employee_id', '=', $resource->user_id)
            ->groupBy('assignment.rate')
            ->get();

        $total_avg_hours = 0;
        $count = 0;
        foreach ($average_billable_rate as $rate) {
            $total_avg_hours += ($rate->hours != 0 && $rate->bonus_rate) ? (($rate->hours * $rate->bonus_rate) / $rate->hours) : 0;
            $count += ($rate->bonus_rate != 0) ? 1 : 0;
        }

        $total_billable = Timesheet::selectRaw($this->billable_hours)->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')->where('timesheet.employee_id', '=', $resource->user_id)->first();
        $commission_value = 0;
        if (isset($resource->commission)) {
            if ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour01 && $total_billable->hours < $resource->commission->hour01) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm01 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour02 && $total_billable->hours < $resource->commission->hour03) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm02 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour03 && $total_billable->hours < $resource->commission->hour04) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm03 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour04 && $total_billable->hours < $resource->commission->hour05) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm04 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour05 && $total_billable->hours < $resource->commission->hour06) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm05 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour06 && $total_billable->hours < $resource->commission->hour07) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm06 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour07 && $total_billable->hours < $resource->commission->hour08) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm07 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour08 && $total_billable->hours < $resource->commission->hour09) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm08 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour09 && $total_billable->hours < $resource->commission->hour10) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm09 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour10 && $total_billable->hours < $resource->commission->hour11) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm10 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour11 && $total_billable->hours < $resource->commission->hour12) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm11 / 100);
            } elseif ($avg_invoice_rate >= $resource->commission->min_rate && $total_billable->hours > $resource->commission->hour12) {
                $commission_value = ($total_billable->hours * $avg_invoice_rate) * ($resource->commission->comm12 / 100);
            }
        }

        $time_exp_not_approved = $this->expenses($request, 'claim_auth_date', $resource);
        $time_exp_not_paid = $this->expenses($request, 'paid_date', $resource);

        $total_exp_not_approved = 0;
        foreach ($time_exp_not_approved as $item) {
            $total_exp_not_approved += $item->amount;
        }
        $total_exp_not_paid = 0;
        foreach ($time_exp_not_paid as $exp) {
            $total_exp_not_paid += $exp->amount;
        }

        $personal_info_perce = $this->complete_percentage('Cv', 'talent', $resource, 'user_id');
        $availability = Availability::where('res_id', '=', $resource->user_id)->count();
        $availability = ($availability > 0) ? 100 : 0;
        $skill = Skill::where('res_id', '=', $resource->user_id)->count();
        $skill = ($skill != 0) ? ((($skill > 5) ? 5 : $skill) / 5) * 100 : 0;
        $qualification = Qualification::where('res_id', '=', $resource->user_id)->count();
        $qualification = ($qualification != 0) ? ((($qualification > 2) ? 2 : $qualification) / 2) * 100 : 0;
        $experience = Experience::where('res_id', '=', $resource->user_id)->count();
        $experience = ($experience != 0) ? ((($experience > 5) ? 5 : $experience) / 5) * 100 : 0;
        $industry_experience = IndustryExperience::where('res_id', '=', $resource->user_id)->count();
        $industry_experience = ($industry_experience != 0) ? ((($industry_experience > 5) ? 5 : $industry_experience) / 5) * 100 : 0;
        $resource_profile_perce = $this->complete_percentage('Resource', 'resource', $resource, 'user_id');

        $parameters = [
            'resource' => $resource,
            'margin_months' => $months,
            'util_perc' => $util_per_week_diff,
            'year_week' => $util_per_year_week,
            'current_year_income' => $current_year_income,
            'current_year_c_t_c' => $this_year_c_t_c,
            'current_year_hours' => $current_year_hours,
            'prev_year_income' => $prev_year_income,
            'last_year_c_t_c' => $last_year_c_t_c,
            'prev_year_hours' => $prev_year_hours,
            'date' => $date,
            'current_c_t_c_rate' => Assignment::select('internal_cost_rate', 'external_cost_rate')->where('employee_id', '=', $resource->user_id)->latest()->first(),
            'c_t_c_rate_average' => $c_t_c_rate_average,
            'avg_invoice_rate' => $avg_invoice_rate,
            'open_assignments_billable' => $this->continuity($request, 1, $resource),
            'open_assignments_non_bill' => $this->continuity($request, 0, $resource),
            'outstanding_timesheets' => array_filter(array_unique($res_weeks)),
            'year_month_drop_down' => $year_month_drop_down,
            'leave_balance' => $leave_balance,
            'leave' => $leave_taken,
            'avg_bill_rate' => ($total_avg_hours != 0 && $count != 0) ? $total_avg_hours / $count : 0,
            'total_billable_hours' => $total_billable->hours,
            'commission_value' => $commission_value,
            'total_exp_not_approved' => $total_exp_not_approved,
            'time_exp_not_approved' => $time_exp_not_approved->count(),
            'time_exp_not_paid' => $time_exp_not_paid->count(),
            'total_exp_not_paid' => $total_exp_not_paid,
            'personal_info_perce' => $personal_info_perce,
            'availability' => $availability,
            'skill' => $skill,
            'qualification' => $qualification,
            'experience' => $experience,
            'industry_experience' => $industry_experience,
            'resource_profile_perce' => $resource_profile_perce,
        ];

        return view('reports.management.consultant360view')->with($parameters);
    }

    public function payroll(Request $request): JsonResponse
    {
        return response()->json(['test' => $request->year_month]);
    }

    private function income_per_year($request, $resource, $date, $sub_years)
    {
        $income = Timesheet::selectRaw($this->invoice_rate.', '.$this->internal_cost_rate.', '.$this->external_cost_rate.', '.$this->billable_hours)
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->leftJoin('assignment', function ($join) {
                $join->on('timesheet.employee_id', '=', 'assignment.employee_id');
                $join->on('timesheet.project_id', '=', 'assignment.project_id');
            })
            ->where('timesheet.year', '=', $date->now()->subYears($sub_years)->year)
            ->where('timesheet.employee_id', '=', $resource->user_id)
            ->groupBy('assignment.invoice_rate', 'assignment.internal_cost_rate', 'assignment.external_cost_rate')
            ->get();

        return $income;
    }

    private function continuity($request, $bill_status, $resource)
    {
        $open_assignments = Assignment::selectRaw('assignment.id, assignment.project_id, assignment.customer_po,
        assignment.start_date, assignment.end_date, '.$this->invoice_rate.', '.$this->internal_cost_rate.', '.$this->external_cost_rate.', assignment.hours,
        SUM(CASE WHEN timeline.is_billable = '.$bill_status.' THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS bill_hours')
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
            })
            ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
            ->where('assignment.employee_id', '=', $resource->user_id)
            ->where('assignment.billable', '=', $bill_status)
            ->whereIn('assignment.assignment_status', [2, 3])
            ->groupBy('assignment.id', 'assignment.project_id', 'assignment.customer_po', 'assignment.start_date')
            ->groupBy('assignment.end_date', 'assignment.invoice_rate', 'assignment.internal_cost_rate')
            ->groupBy('assignment.external_cost_rate', 'assignment.hours')
            ->get();

        return $open_assignments;
    }

    private function expenses($request, $col, $resource)
    {
        $expenses = TimeExp::select('amount')->whereHas('timesheet', function ($query) use ($resource) {
            $query->where('employee_id', '=', $resource->user_id);
        })->where('claim', '=', 1)
            ->whereNull($col)
            ->get();

        return $expenses;
    }

    private function complete_percentage($model, $table_name, $foreign_key, $own_col)
    {
        $pos_info = DB::select(DB::raw('SHOW COLUMNS FROM '.$table_name)->getValue(DB::connection()->getQueryGrammar()));
        $base_columns = count($pos_info);
        $not_null = 0;
        foreach ($pos_info as $col) {
            $not_null += app('App\\Models\\'.$model)::selectRaw('SUM(CASE WHEN '.$col->Field.' IS NOT NULL THEN 1 ELSE 0 END) AS not_null')->where($own_col, '=', $foreign_key->user_id)->first()->not_null;
        }

        return ($not_null / $base_columns) * 100;
    }
}
