<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Services\StorageQuota;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use App\Jobs\SendUpgradePremiumEmailJob;

class MyAccountController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request,StorageQuota $storage)
    {
        if(auth()->user()->hasAnyRole(['admin'])){
            $owner = User::first();
            try {
                $response = Http::get('https://subscription.consulteaze.com/api/subscription?email='.$owner->email);

                $request->session()->put('subscriptionDetail', json_encode($response->json()));
            } catch (Exception $e) {
                logger($e);
            }
        }

        $license = $response->json();

        $upgrade_amount = 0;
        $downgrade_amount = 0;

        $package = $this->subscriptionPackage();

        switch($license["subscription"]["subscription_tier"]){
            case "TRIAL":
                $upgrade_amount = $package["CLASSIC"]["amount"];
                $downgrade_amount = $package["TRIAL"]["amount"];
                break;
            case "CLASSIC":
                $upgrade_amount = $package["STANDARD"]["amount"];
                $downgrade_amount = $package["TRIAL"]["amount"];
                break;
            case "STANDARD":
                $upgrade_amount = $package["PREMIUM"]["amount"];
                $downgrade_amount = $package["CLASSIC"]["amount"];
                break; 
            default:
                break;
        }
        // dd($license);
   
        $parameters = [
            'license' => $license,
            'upgrade_amount' => $upgrade_amount,
            'downgrade_amount' => $downgrade_amount,
            'login_users' => User::where('login_user','1')->get()->count(),
            'packages' => $package,
            'storageUsed' => $storage->storageUsage(),
            'owner' => $owner
        ];
// dd($storage->storageUsage());
        return view('ofsyaccount.index')->with($parameters);
    }

    public function upgradeToPremium(Request $request){
        $data = [];

        $data['user'] =  $request->user ?? '';
        $data['phone'] =  $request->phone ?? '';
        $data['instance'] =  $request->instance ?? '';
        $data['interest'] =  $request->interest ?? [];
        $data['category'] =  $request->category ?? '';
        $data['size'] =  $request->size ?? '';
        $data['notes'] =  $request->notes ?? '';
        $data['use'] =  $request->use ?? '';
        $data['time'] =  Carbon::now()->format('Y-m-d H:i:s') ?? '';
        
        try {
            SendUpgradePremiumEmailJob::dispatch($data)->delay(now()->addMinutes(5));
        }catch (\Exception $e){
            logger($e);
            return redirect()->back()->with('flash_danger', $e->getMessage());
        }
        return response()->json(['success' => 'success']);
    }
}
