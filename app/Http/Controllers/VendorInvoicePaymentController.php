<?php

namespace App\Http\Controllers;

use App\Models\Timesheet;
use App\Models\VendorInvoice;
use App\Models\VendorInvoiceStatus;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VendorInvoicePaymentController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $vendor_invoices = VendorInvoice::with('invoice_status')->sortable()->paginate($item);

        if ($request->has('q') && $request->input('q') != '') {
            $vendor_invoices = VendorInvoice::where('vendor_invoice_number', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_ref', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_invoice_value', 'like', '%'.$request->input('q').'%')
                ->orWhere('vendor_paid_date', 'like', '%'.$request->input('q').'%')->sortable()->paginate($item);
        }
        if ($request->has('invoice_status') && $request->input('invoice_status') != 0) {
            $vendor_invoices = VendorInvoice::where('vendor_invoice_status', '=', $request->input('invoice_status'))
                        ->sortable()->paginate($item);
        }
        if ($request->has(['date_from', 'date_to']) && $request->input('date_from') != '' && $request->input('date_to') != '') {
            $vendor_invoices = VendorInvoice::whereBetween('vendor_invoice_date', ['date_from', 'date_to'])->sortable()->paginate($item);
        }

        $parameters = [
            'vendor_invoices' => $vendor_invoices,
            'status_dropdown' => VendorInvoiceStatus::orderBy('id')->pluck('description', 'id')->prepend('Invoice Status', '0'),
        ];
        //return $vendor_invoices;
        return view('vendor.invoice_payment.index')->with($parameters);
    }

    public function create()
    {
        //
    }

    public function process($id): View
    {
        $parameter = [
            'id' => $id,
            'date' => Carbon::now()->toDateString(),
        ];

        return view('vendor.invoice_payment.create')->with($parameter);
    }

    public function store(Request $request): RedirectResponse
    {
        $vendor_invoice = VendorInvoice::find($request->input('vendor_id'));
        $vendor_invoice->vendor_paid_date = $request->input('vendor_paid_date');
        $vendor_invoice->vendor_invoice_status = 2;
        $vendor_invoice->save();

        $time = Timesheet::where('vendor_invoice_number', $vendor_invoice->id)->get();

        foreach ($time as $result) {
            $result->timesheet_lock = 1;
            $result->vendor_invoice_status = 2;
            //$result->bill_status = 3;
            $result->vendor_paid_date = $vendor_invoice->vendor_paid_date;
            $result->save();
        }
        activity()->on($vendor_invoice)->log('paid');

        return redirect(route('vendor.invoice'))->with('flash_success', 'Invoice payment processed successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
