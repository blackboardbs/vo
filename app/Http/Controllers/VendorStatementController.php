<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Config;
use App\Jobs\SendCustomerStatementEmailJob;
use App\Models\Module;
use App\Models\VendorInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelPdf\Facades\Pdf;

class VendorStatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $configs = Config::first();

        $items = ($request->has('r') && $request->r != '0') ? $request->r : 15;

        $vendor_invoices = VendorInvoice::selectRaw('vendor_id,
                SUM(IF(vendor_due_date > CURRENT_DATE() AND vendor_invoice_status = 1, vendor_invoice_value, 0)) AS current,
                SUM(IF(vendor_due_date < CURRENT_DATE() AND DATEDIFF(vendor_due_date, CURRENT_DATE()) >= -30 AND vendor_invoice_status = 1, vendor_invoice_value, 0)) AS thirty_days,
                SUM(IF(DATEDIFF(vendor_due_date, CURRENT_DATE()) < -30 AND DATEDIFF(vendor_due_date, CURRENT_DATE()) >= -60 AND vendor_invoice_status = 1, vendor_invoice_value, 0)) AS sixty_days,
                SUM(IF(DATEDIFF(vendor_due_date, CURRENT_DATE()) < -60 AND vendor_invoice_status = 1, vendor_invoice_value, 0)) AS ninety_days_plus, currency
            ')
            ->with('vendor')
            //->where('vendor_invoice_status', 1)
            ->groupBy('vendor_id', 'currency');

        $module = Module::where('name', '=', 'App\Models\VendorProfile')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
            $vendor_invoices->where('vendor_id', '=', Auth::user()->vendor_id);
        }

        $vendors = $vendor_invoices->get();

        if ($request->has('q') && $request->q != '') {
            $vendor_invoices = $vendor_invoices->whereHas('vendor', function ($query) use ($request) {
                $query->where('vendor_name', 'like', '%'.$request->q.'%');
            });
        }

        if ($request->has('company') && $request->company != '') {
            $vendor_invoices = $vendor_invoices->where('company_id', $request->company);
        } else {
            $vendor_invoices = $vendor_invoices->where('company_id', $configs?->company_id);
        }

        if ($request->has('vendor_id') && $request->vendor_id != '') {
            $vendor_invoices = $vendor_invoices->where('vendor_id', $request->vendor_id);
        }

        if (($request->has('period_from') && $request->period_from != '') && ($request->has('period_to') && $request->period_to != '')) {
            $vendor_invoices = $vendor_invoices->whereBetween('vendor_invoice_date', [
                Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-01')->toDateString(),
                Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->lastOfMonth()->day)->toDateString(), ]);
        } else {
            $vendor_invoices = $vendor_invoices->whereBetween('vendor_invoice_date', [now()->subMonths(3)->startOfMonth()->toDateString(), now()->lastOfMonth()->toDateString()]);
        }
        $vendor_invoices = $vendor_invoices->orderBy('vendor_id')->paginate($items);

        $currencies = [];
        $total = [];
        $total30 = [];
        $total60 = [];
        $total90 = [];
        foreach($vendor_invoices as $vi){
            // $total = $total + $vi->current;
            // $total30 = $total30 + $vi->thirty_days;
            // $total60 = $total60 + $vi->sixty_days;
            // $total90 = $total90 + $vi->ninety_days_plus;
            if($vi->currency){
                if(!in_array($vi->currency,$currencies)){
                    array_push($currencies,$vi->currency);
                }
            } else {
                if(!in_array('ZAR',$currencies)){
                    array_push($currencies,'ZAR');
                }
            }
            if($vi->currency){
                if(isset($total[$vi->currency])){
                $total[$vi->currency] = $total[$vi->currency] + $vi->current;
                } else {
                    $total[$vi->currency] = $vi->current;
                }
            } else {
                if(isset($total['ZAR'])){
                $total['ZAR'] = $total['ZAR'] + $vi->current;
                } else {
                    $total['ZAR'] = $vi->current;
                }
            }
            if($vi->currency){
                if(isset($total30[$vi->currency])){
                $total30[$vi->currency] = $total30[$vi->currency] + $vi->thirty_days;
                } else {
                    $total30[$vi->currency] = $vi->thirty_days;
                }
            } else {
                if(isset($total30['ZAR'])){
                $total30['ZAR'] = $total30['ZAR'] + $vi->thirty_days;
                } else {
                    $total30['ZAR'] = $vi->thirty_days;
                }
            }
            if($vi->currency){
                if(isset($total60[$vi->currency])){
                $total60[$vi->currency] = $total60[$vi->currency] + $vi->sixty_days;
                } else {
                    $total60[$vi->currency] = $vi->sixty_days;
                }
            } else {
                if(isset($total['ZAR'])){
                $total60['ZAR'] = $total60['ZAR'] + $vi->sixty_days;
                } else {
                    $total60['ZAR'] = $vi->sixty_days;
                }
            }
            if($vi->currency){
                if(isset($total90[$vi->currency])){
                $total90[$vi->currency] = $total90[$vi->currency] + $vi->ninety_days_plus;
                } else {
                    $total90[$vi->currency] = $vi->ninety_days_plus;
                }
            } else {
                if(isset($total90['ZAR'])){
                $total90['ZAR'] = $total90['ZAR'] + $vi->ninety_days_plus;
                } else {
                    $total90['ZAR'] = $vi->ninety_days_plus;
                }
            }
        }
        // dd($total);

        $vendor_drop_down = [];
        foreach ($vendors as $vendor) {
            if (auth()->user()->hasRole('vendor')) {
                $user = \auth()->user();
                $vendor_drop_down[$user->vendor_id] = isset($user->vendor->vendor_name) ? $user->vendor->vendor_name : null;
            }

            if (auth()->user()->hasAnyRole(['admin', 'admin_manager', 'manager'])) {
                $vendor_drop_down[$vendor->vendor_id] = isset($vendor->vendor->vendor_name) ? $vendor->vendor->vendor_name : null;
            }
        }

        $dates = [];
        for ($i = 0; $i <= 23; $i++) {
            $dates[now()->subMonths($i)->year.now()->subMonths($i)->month] = now()->subMonths($i)->year.'/'.((now()->subMonths($i)->month < 10) ? '0'.now()->subMonths($i)->month : now()->subMonths($i)->month);
        }

        $parameters = [
            'vendor_invoices' => $vendor_invoices,
            'vendor_drop_down' => $vendor_drop_down,
            'config' => $configs,
            'company_drop_down' => Company::orderBy('company_name')->pluck('company_name', 'id'),
            'dates_drop_down' => $dates,
            'total'=>$total,
            'total30'=>$total30,
            'total60'=>$total60,
            'total90'=>$total90,
            'currencies'=>$currencies
        ];
        if ($request->has('export')) return $this->export($parameters, 'vendor.statements');

        return view('vendor.statements.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id)
    {
        $vendor_invoices = VendorInvoice::with('vendor')
            ->where('vendor_id', $id)
            ->whereBetween('vendor_invoice_date', [
                Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->startOfMonth()->day)->toDateString(),
                Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->lastOfMonth()->day)->toDateString(), ])->where('currency', $request->currency)
            ->get();

        $from_date = Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->startOfMonth()->day)->toDateString();

        $total_by_months = VendorInvoice::selectRaw('
            SUM(IF(vendor_due_date > CURRENT_DATE(), vendor_invoice_value, 0)) AS current,
                SUM(IF(vendor_due_date < CURRENT_DATE() AND DATEDIFF(vendor_due_date, CURRENT_DATE()) >= -30, vendor_invoice_value, 0)) AS thirty_days,
                SUM(IF(DATEDIFF(vendor_due_date, CURRENT_DATE()) < -30 AND DATEDIFF(vendor_due_date, CURRENT_DATE()) >= -60, vendor_invoice_value, 0)) AS sixty_days,
                SUM(IF(DATEDIFF(vendor_due_date, CURRENT_DATE()) < -60, vendor_invoice_value, 0)) AS ninety_days_plus,
               SUM(IF(vendor_invoice_date < DATE("'.$from_date.'"),vendor_invoice_value ,0)) AS opening_balance
        ')->where('vendor_invoice_status', 1)->where('currency', $request->currency)->where('vendor_id', $id)->first();

        $parameters = [
            'vendor_invoices' => $vendor_invoices,
            'statement_date' => Carbon::parse(substr($request->period_to, 0, 4).'-'.substr($request->period_to, 4).'-'.now()->day)->lastOfMonth(),
            'opening_bal_date' => Carbon::parse(substr($request->period_from, 0, 4).'-'.substr($request->period_from, 4).'-'.now()->day)->startOfMonth(),
            'total_by_month' => $total_by_months,
        ];
        if ($request->print) {
            
            if (! Storage::exists('statements/vendor/')) {
                Storage::makeDirectory('statements/vendor/');
            }
            try {
                $storage = storage_path('app/statements/vendor/'.$vendor_invoices[0]?->vendor?->vendor_name.'_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('vendor.statements.pdf',$parameters)
                    ->format('a4')
                    ->save($storage);

                return response()->file($storage);
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }

        if ($request->send) {
            if (! Storage::exists('statements/vendor/')) {
                Storage::makeDirectory('statements/vendor/');
            }

            $multiple_emails = (isset($request->user_email)) ? json_decode($request->user_email) : [];
            $emails = [(isset($request->company_email) ? $request->company_email : null), (isset($request->vendor_email) ? $request->vendor_email : null)];
            if (isset($multiple_emails)) {
                foreach ($multiple_emails as $user_email) {
                    array_push($emails, $user_email);
                }
            }
            try {
                $storage = storage_path('app/statements/vendor/'.$vendor_invoices[0]?->vendor?->vendor_name.'_'.date('Y-m-d_H_i_s').'.pdf');

                Pdf::view('vendor.statements.pdf',$parameters)
                    ->format('a4')
                    ->save($storage);
                //Mail::to(Auth::user()->email)->cc(array_filter($emails))->send(new VendorStatementMail($vendor_invoices[0], $storage));
                SendCustomerStatementEmailJob::dispatch(Auth::user()->email, array_filter($emails), $vendor_invoices[0], $storage, 2)->delay(now()->addMinutes(5));

                return redirect()->back()->with('flash_success', 'Your statement has been sent successfully');
            } catch (FileAlreadyExistsException $e) {
                report($e);

                return redirect()->back()->with('flash_danger', $e->getMessage());
            }
        }

        return view('vendor.statements.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
