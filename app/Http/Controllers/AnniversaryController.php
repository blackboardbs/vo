<?php

namespace App\Http\Controllers;

use App\Models\Anniversary;
use App\Models\AnniversaryType;
use App\Http\Requests\AnniversaryRequest;
use App\Models\Relationship;
use App\Services\AnniversaryService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AnniversaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, AnniversaryService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasListAllPermissions(), 403);

        $anniversaries = Anniversary::with([
            'anniversary_type:id,description',
            'resource:id,first_name,last_name',
            'status:id,description',
            'relationshipd:id,description'
        ])->when($request->q, fn($anniversary) =>$anniversary->where('anniversary', 'LIKE', '%'.$request->input('q').'%')
            ->orwhere('name', 'LIKE', '%'.$request->input('q').'%')
            ->orwhere('email', 'LIKE', '%'.$request->input('q').'%')
            ->orwhere('phone', 'LIKE', '%'.$request->input('q').'%')
            ->orwhere('relationship', 'LIKE', '%'.$request->input('q').'%')
            ->orwhere('anniversary_date', 'LIKE', '%'.$request->input('q').'%')
            ->orWhereHas('resource', function ($query) use ($request) {
                $query->where('first_name', 'LIKE', '%'.$request->input('q').'%');
                $query->orWhere('last_name', 'LIKE', '%'.$request->input('q').'%');
            })
            ->orWhereHas('status', function ($query) use ($request) {
                $query->where('description', 'LIKE', '%'.$request->input('q').'%');
            })
            ->orWhereHas('anniversary_type', function ($query) use ($request) {
                $query->where('description', 'LIKE', '%'.$request->input('q').'%');
            })
            ->orWhereHas('relationshipd', function ($query) use ($request) {
                $query->where('description', 'LIKE', '%'.$request->input('q').'%');
            }))
            ->latest()
            ->paginate($request->input('r') ?? 15);

        $parameters = [
            'anniversary' => $anniversaries,
            'can_create' => $service->hasCreatePermissions(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        if ($request->has('export')) return $this->export($parameters, 'anniversary');

        return view('anniversary.index')->with($parameters);
    }

    public function show(Anniversary $anniversary, AnniversaryService $service): View
    {
        abort_unless($service->hasShowPermissions(), 403);

        return view('anniversary.show')->with(['anniversary' => $anniversary]);
    }

    public function create(AnniversaryService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $parameters = [
            'anniversary_type' => AnniversaryType::orderBy('id')->pluck('description', 'id')->prepend('Anniversary Type', '0'),
            'relationship' => Relationship::orderBy('id')->pluck('description', 'id'),
        ];

        return view('anniversary.create')->with($parameters);
    }

    public function store(AnniversaryRequest $request, Anniversary $anniversary, AnniversaryService $service): RedirectResponse
    {
        $service->createAnniversary($request, $anniversary);

        return redirect(route('anniversary.index'))->with('flash_success', 'Anniversary captured successfully');
    }

    public function edit(Anniversary $anniversary, AnniversaryService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $parameters = [
            'anniversary' => $anniversary,
            'anniversary_type' => AnniversaryType::orderBy('id')->pluck('description', 'id')->prepend('Anniversary Type', '0'),
            'relationship' => Relationship::orderBy('id')->pluck('description', 'id'),
        ];

        return view('anniversary.edit')->with($parameters);
    }

    public function update(AnniversaryRequest $request, Anniversary $anniversary, AnniversaryService $service): RedirectResponse
    {
        $service->updateAnniversary($request, $anniversary);

        return redirect(route('anniversary.index'))->with('flash_success', 'Expense racking updated successfully');
    }

    public function destroy(Anniversary $anniversary, AnniversaryService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $anniversary->delete();

        return redirect()->route('anniversary.index')
            ->with('success', 'Anniversary deleted successfully');
    }
}
