<?php

namespace App\Http\Controllers;

use App\Exports\ExpenseTrackingExport;
use App\Exports\PlannedExpensesExport;
use App\Http\Requests\ExpenseTrackingRequest;
use App\Models\Account;
use App\Models\AccountElement;
use App\Models\Assignment;
use App\Models\Company;
use App\Models\Config;
use App\Models\CostCenter;
use App\Models\ExpenseTracking;
use App\Models\ExpenseType;
use App\Jobs\SendExpenseTrackingEmailJob;
use App\Models\Module;
use App\Models\Notification;
use App\Models\Resource;
use App\Models\Status;
use App\Models\User;
use App\Models\UserNotification;
use App\Services\ExpenseTrackingService;
use App\Services\StorageQuota;
use App\Utils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;

class ExpenseTrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, ExpenseTrackingService $service)
    {
        abort_unless($service->hasListAllPermissions(), Response::HTTP_FORBIDDEN);
        $item = $request->input('r') ?? 15;

        $module = Module::where('name', '=', \App\Models\ExpenseTracking::class)->get();

        $actions = [
            'create' => $service->hasCreatePermissions(),
            'update' => $service->hasUpdatePermissions(),
        ];

        $expenses = ExpenseTracking::with(['expense_type' => fn ($type) => $type->select(['id', 'description']),
            'assignment' => fn($assignment) => $assignment->select(['id', 'project_id'])->with(['project' => fn($project) => $project->select(['id', 'name'])]),
            'resource' => fn($res) => $res->selectRaw('CONCAT(`first_name`," ", `last_name`) AS resource_name, id'),
            'account:id,description',
            'account_element:id,description'
        ])->orderBy('id', 'desc');

        $assignments = $expenses->get();

        $expenses = $expenses->when($request->company_id,
            fn($expense) => $expense->where('company_id', $request->company_id)
        )->when($request->account_id,
            fn($expense) => $expense->where('account_id', $request->account_id)
        )->when($request->account_element_id,
            fn($expense) => $expense->where('account_element_id', $request->account_element_id)
        )->when($request->resource_id,
            fn($expense) => $expense->where('employee_id', $request->resource_id)
        )->when($request->approval_status_id,
            fn($expense) => $expense->where('approval_status', $request->approval_status_id)
        )->when(!$request->approval_status_id,
            fn($expense) => $expense->where('approval_status', '=','1')
        )->when($request->payment_status_id,
            fn($expense) => $expense->where('payment_status', $request->payment_status_id)
        )->when(!$request->payment_status_id,
            fn($expense) => $expense->where('payment_status', '=','1')
        )->when($request->expense_type_id,
            fn($expense) => $expense->where('expense_type_id', $request->expense_type_id)
        )->when($request->approver_id,
            fn($expense) => $expense->where('approved_by', $request->approver_id)
        )->when(($request->claimable_id && ($request->claimable_id != 0)),
            fn($expense) => $expense->where('claimable', $request->claimable_id)
        )->when(($request->billable_id && ($request->billable_id != 0)),
            fn($expense) => $expense->where('billable', $request->billable_id)
        )->when($request->assignment_id,
            fn($expense) => $expense->where('assignment_id', $request->assignment_id)
        )->when($request->payment_reference,
            fn($expense) => $expense->where('payment_reference', 'like','%'.$request->payment_reference.'%')
        );

        $expenses = $expenses->sortable(['transaction_date' => 'desc'])->paginate($item);

        $assignment_drop_down = [];
        $resource_drop_down = clone $assignments->map(fn($assignment) => $assignment->employee_id)->unique()->values();
        $approved_by_drop_down = clone $assignments->map(fn($assignment) => $assignment->approved_by)->unique()->values();

        foreach ($assignments as $assignment) {
            if (isset($assignment->assignment->project)) {
                $assignment_drop_down[$assignment->assignment_id] = $assignment->assignment->project->name.' ('.(isset($assignment->resource->first_name) ? $assignment->resource->first_name : $assignment->assignment_id).')';
            }
        }

        $total_exp = 0;
        foreach($expenses as $exp){
            $total_exp = $total_exp + floatval(str_replace(',','',$exp->amount));
        }

        $parameters = [
            'expenses' => $expenses,
            'assignment_drop_down' => $assignment_drop_down,
            'resource_ids' => $resource_drop_down,
            'approved_by_ids' => $approved_by_drop_down,
            'actions' => $actions,
            'total_exp' => $total_exp,
        ];

        if ($request->has('export')) return $this->export($parameters, 'exp_tracking');

        return view('exp_tracking.index')->with($parameters);
    }

    public function show($exptrackingid, ExpenseTrackingService $service)
    {
        abort_unless($service->hasShowPermissions(), Response::HTTP_FORBIDDEN);

        $expenses = ExpenseTracking::with(['expense_type' => function ($type) {
            return $type->select(['id', 'description']);
        }])->where('id', '=', $exptrackingid)->get();
        $parameters = [
            'expenses' => $expenses,

        ];
        if (count($expenses)) {
            return view('exp_tracking.show')->with($parameters);
        } else {
            return redirect()->back()->with('flash_danger', 'The expense you\'re looking for was not found');
        }
    }

    public function create(ExpenseTrackingService $service): View
    {
        abort_unless($service->hasCreatePermissions(), Response::HTTP_FORBIDDEN);

        $t = new Carbon();
        $assignment = Assignment::leftJoin('projects', 'assignment.project_id', '=', 'projects.id')
            ->leftJoin('users', 'assignment.employee_id', '=', 'users.id')
            ->select(DB::raw("CONCAT(projects.name,' - ',COALESCE(users.first_name,''), ' ',COALESCE(users.last_name,'')) AS assignment, assignment.id AS assignment_id"))
            ->whereIn('assignment.assignment_status', [2, 3])
            ->orderBy('assignment')
            ->orderBy('assignment_id')
            ->get();
        /*->pluck('assignment','assignment_id')
        ->prepend('Assignment', '0');*/
        $assignment_dropdown = [];

        foreach ($assignment as $ass) {
            $assignment_dropdown[$ass->assignment_id] = $ass->assignment;
        }

        $assignment_dropdown = [0 => 'Select Assignment'] + $assignment_dropdown;

        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $conf = Config::find(1, ['cost_center_id', 'company_id', 'timesheet_weeks']);
        $number_of_weeks = $conf->timesheet_weeks ?? 12;

        $week_drop_down[0] = 'Select Week';

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week < 6) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';

                    continue;
                }

                if ($i == 1) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.date('Y-m').'-01)-2';
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';
                } else {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                }
            }

            $number_of_weeks -= $week;
            $week = 52;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week < 6) {
                $week_drop_down[$year.$i.'_2'] = $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2';
                $week_drop_down[$year.$i.'_1'] = $year.$i.'('.$date2->format('Y-m-d').')-1';

                continue;
            }
            $week_drop_down[$year.$i] = $year.$i.'('.$date2->format('Y-m-d').')';
        }

        $users = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')
            ->excludes([6, 7, 8, 9])
            ->orderBy('first_name')
            ->orderBy('last_name')
            ->pluck('full_name', 'id')
            ->prepend('Please Select', '0');

        //dd($week_drop_down);
        $parameters = [
            'year' => $t->now()->year,
            'wk' => $t->now()->weekOfYear,
            'yearwk' => $week_drop_down,
            'assignment' => $assignment_dropdown,
            'resource' => $users,
            'sidebar_process_statuses' => Status::orderBy('id')->where('status', '=', 1)->orderBy('order')->pluck('description', 'id')->prepend('Active', '1'),
            'approval_status_dropdown' => [1 => 'New', 2 => 'Approved', 3 => 'Rejected', 4 => 'Updated'],
            'cost_center_dropdown' => CostCenter::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Please Select', '0'),
            'conf' => $conf,
            'expense_type_dropdown' => ExpenseType::where('status_id', 1)->pluck('description', 'id'),
        ];

        if (! count($parameters['expense_type_dropdown'])) {
            $parameters['flash_info'] = 'Please go to master data and add expense type';
        }

        return view('exp_tracking.create')->with($parameters);
    }

    public function store(ExpenseTrackingRequest $request, StorageQuota $quota, ExpenseTrackingService $service): RedirectResponse
    {
        abort_unless($service->hasCreatePermissions(), Response::HTTP_FORBIDDEN);
        $consultant = User::query()
            ->with(['resource' => fn($res) => $res->select(['id', 'manager_id'])])
            ->find($request->input('resource'), ['resource_id'])
            ?? auth()->user();

        if ($request->input('approved_by')) {
            $approver = $request->input('approved_by');
        } elseif ($consultant->resource?->manager_id) {
            $approver = $consultant->resource?->manager_id;
        } else {
            $approver = Config::first()?->assessment_approver_user_id;
        }

        $flash_message = 'Expense captured successfully';

        $referrer = new ExpenseTracking();
        $referrer->employee_id = $request->input('resource')??\auth()->id();
        $referrer->transaction_date = $request->input('exp_date');
        $referrer->account_id = $request->input('account');
        $referrer->account_element_id = $request->input('account_element');
        $referrer->description = $request->input('description');
        $referrer->assignment_id = $request->input('assignment');
        $referrer->company_id = $request->input('company');
        $referrer->yearwk = $request->input('yearwk');
        $referrer->claimable = ($request->input('claimable') !== null ? $request->input('claimable') : '0');
        $referrer->billable = ($request->input('billable') !== null ? $request->input('billable') : '0');
        $referrer->payment_reference = $request->input('payment');
        $referrer->amount = $request->input('amount');
        $referrer->status_id = $request->input('status', \App\Enum\Status::ACTIVE->value);
        $referrer->cost_center = $request->input('cost_center');
        $referrer->approved_by = $approver;
        $referrer->approved_on = $request->input('approved_on');
        $referrer->approval_status = $request->input('approval_status', 1);
        $referrer->payment_date = $request->input('payment_date');
        $referrer->payment_status = $request->input('payment_status');
        $referrer->expense_type_id = $request->input('expense_type_id');
        if ($request->hasFile('document')) {
            if (!$quota->isStorageQuotaReached($request->file('document')->getSize())){
                $referrer->general(request: $request, field: 'document', type: 'exp_tracking', size: 1080);
                $referrer->document = $request->file('document')->hashName();
            }else{
                $flash_message = 'Expense captured successfully, but the file was not uploaded because the storage quota is reached';
            }
        }
        $referrer->save();

        activity()->on($referrer)->log('created');

        $helpPoertal = new Utils();

        if (isset($referrer->approver)) {
            $notification = new Notification;
            $notification->name = 'New Expense has been added.';
            $notification->link = route('exptracking.show', $referrer->id);
            $notification->save();

            $user_notifications = [];

            if (! is_null($referrer->approved_by)) {
                array_push($user_notifications, [
                    'user_id' => $referrer->approved_by,
                    'notification_id' => $notification->id,
                ]);
            }
            //link notification to the user_id of manager
            UserNotification::insert($user_notifications);

            SendExpenseTrackingEmailJob::dispatch($referrer->approver->email, $referrer, Config::first()->site_name, $helpPoertal->helpPortal)->delay(now()->addMinutes(5));

            return redirect(route('exptracking.index'))->with('flash_success', $flash_message);
        } else {
            return redirect(route('exptracking.index'))->with('flash_success', $flash_message)->with('flash_info', 'Please note that the approver will not be notified since there is no approver selected');
        }
    }

    public function edit(ExpenseTracking $exptracking, ExpenseTrackingService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), Response::HTTP_FORBIDDEN);

        $t = new Carbon();
        $assignment = Assignment::leftJoin('projects', 'assignment.project_id', '=', 'projects.id')
            ->leftJoin('users', 'assignment.employee_id', '=', 'users.id')
            ->select(DB::raw("CONCAT(projects.name,' - ',COALESCE(users.first_name,''), ' ',COALESCE(users.last_name,'')) AS assignment, assignment.id AS assignment_id"))
            ->whereIn('assignment.assignment_status', [2, 3])
            ->orderBy('assignment')
            ->orderBy('assignment_id')
            ->get();

        /*->pluck('assignment','assignment_id')
        ->prepend('Assignment', '0');*/
        $assignment_dropdown = [];

        foreach ($assignment as $ass) {
            $assignment_dropdown[$ass->assignment_id] = $ass->assignment;
        }

        $date = Carbon::now();
        $date2 = Carbon::now();
        $week_drop_down = [];
        $year = $date->format('Y');
        $week = $date->format('W');
        $year = (int) $year;
        $week = (int) $week;

        $config = Config::find(1);
        $number_of_weeks = isset($config->timesheet_weeks) && (int) $config->timesheet_weeks > 0 ? $config->timesheet_weeks : 12;

        $week_drop_down[0] = 'Select Week';

        if ($week - $number_of_weeks <= 0) {
            for ($i = $week; $i >= 1; $i--) {
                $date->setISODate($year, $i);
                $date2->setISODate($year, $i);
                $temp_last_date_of_week = $date2->startOfWeek();
                $date = $date->startOfWeek();
                $last_date_of_week = $date->endOfWeek()->format('d');
                $week_two_month = $date->endOfWeek()->format('m');
                if ((int) $last_date_of_week < 6) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-').$week_two_month.'-01)-2';
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';

                    continue;
                }

                if ($i == 1) {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_2'] = $year.($i < 10 ? '0'.$i : $i).'('.date('Y-m').'-01)-2';
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i).'_1'] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')-1';
                } else {
                    $week_drop_down[$year.($i < 10 ? '0'.$i : $i)] = $year.($i < 10 ? '0'.$i : $i).'('.$date2->format('Y-m-d').')';
                }
            }

            $number_of_weeks -= $week;
            $week = 52;
            $year -= 1;
        }

        for ($i = $week; $i >= $week - $number_of_weeks; $i--) {
            $date->setISODate($year, $i);
            $date2->setISODate($year, $i);
            $temp_last_date_of_week = $date2->startOfWeek();
            $date = $date->startOfWeek();
            $last_date_of_week = $date->endOfWeek()->format('d');
            $week_two_month = $date->endOfWeek()->format('m');
            if ((int) $last_date_of_week < 6) {
                $week_drop_down[$year.$i.'_2'] = $year.$i.'('.$date2->format('Y-').$week_two_month.'-01)-2';
                $week_drop_down[$year.$i.'_1'] = $year.$i.'('.$date2->format('Y-m-d').')-1';

                continue;
            }
            $week_drop_down[$year.$i] = $year.$i.'('.$date2->format('Y-m-d').')';
        }

        $users = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')
            ->excludes([6, 7, 8, 9])
            ->orderBy('first_name')
            ->orderBy('last_name')
            ->pluck('full_name', 'id')
            ->prepend('Please Select', '0');

        $parameters = [
            'year' => $t->now()->year,
            'wk' => $t->now()->weekOfYear,
            'yearwk' => $week_drop_down,
            'exp_tracking' => $exptracking,
            'assignment' => $assignment_dropdown,
            'resource' => $users,
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'sidebar_process_account' => Account::orderBy('id')->orderBy('description')->pluck('description', 'id')->prepend('Account', '0'),
            'sidebar_process_company' => Company::orderBy('id')->orderBy('company_name')->pluck('company_name', 'id')->prepend('Company', '0'),
            'approval_status_dropdown' => [1 => 'New', 2 => 'Approved', 3 => 'Rejected', 4 => 'Updated'],
            'cost_center_dropdown' => CostCenter::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id'),
            'expense_type_dropdown' => ExpenseType::where('status_id', 1)->pluck('description', 'id'),
        ];

        if (! count($parameters['expense_type_dropdown'])) {
            $parameters['flash_info'] = 'Please go to master data and add expense type';
        }

        return view('exp_tracking.edit')->with($parameters);
    }

    public function update(ExpenseTrackingRequest $request, ExpenseTracking $exptracking, StorageQuota $quota, ExpenseTrackingService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), Response::HTTP_FORBIDDEN);

        $consultant = User::query()
            ->with(['resource' => fn($res) => $res->select(['id', 'manager_id'])])
            ->find($request->input('resource'), ['resource_id'])
            ?? auth()->user();

        if ($request->input('approved_by')) {
            $approver = $request->input('approved_by');
        } elseif ($consultant->resource?->manager_id) {
            $approver = $consultant->resource?->manager_id;
        } else {
            $approver = Config::first()?->assessment_approver_user_id;
        }

        $flash_message = 'Expense captured successfully';

        $exp_tracking = $exptracking;
        $exp_tracking->employee_id = $request->input('resource');
        $exp_tracking->transaction_date = $request->input('exp_date');
        $exp_tracking->account_id = $request->input('account');
        $exp_tracking->account_element_id = $request->input('account_element');
        $exp_tracking->description = $request->input('description');
        $exp_tracking->assignment_id = $request->input('assignment');
        $exp_tracking->company_id = $request->input('company');
        $exp_tracking->yearwk = $request->input('yearwk');
        $exp_tracking->claimable = ($request->input('claimable') !== null ? $request->input('claimable') : '0');
        $exp_tracking->billable = ($request->input('billable') !== null ? $request->input('billable') : '0');
        $exp_tracking->payment_reference = $request->input('payment');
        $exp_tracking->amount = $request->input('amount');
        $exp_tracking->status_id = $request->input('status');
        $exp_tracking->cost_center = $request->input('cost_center');
        $exp_tracking->approved_by = $approver;
        $exp_tracking->approved_on = $request->input('approved_on');
        $exp_tracking->approval_status = $request->input('approval_status');
        $exp_tracking->payment_date = $request->input('payment_date');
        $exp_tracking->payment_status = $request->input('payment_status');
        $exp_tracking->expense_type_id = $request->input('expense_type_id');

        if ($request->hasFile('document')) {
            if (!$quota->isStorageQuotaReached($request->file('document')->getSize())){
                Storage::delete('exp_tracking/'.$exp_tracking->document);
                $exp_tracking->general(request: $request, field: 'document', type: 'exp_tracking', size: 1080);
                $exp_tracking->document = $request->file('document')->hashName();
            }else{
                $flash_message = 'Expense captured successfully, but the file was not uploaded because the storage quota is reached';
            }
        }
        $exp_tracking->save();

        activity()->on($exp_tracking)->log('updated');

        $helpPoertal = new Utils();

        if (isset($exp_tracking->approver)) {
            SendExpenseTrackingEmailJob::dispatch($exp_tracking->approver->email, $exp_tracking, Config::first()->site_name, $helpPoertal->helpPortal)->delay(now()->addMinutes(5));

            return redirect(route('exptracking.index'))->with('flash_success', $flash_message);
        } else {
            return redirect(route('exptracking.index'))->with('flash_success', $flash_message)->with('flash_info', 'Please note that the approver will not be notified since there is no approver selected');
        }
    }

    public function destroy(ExpenseTracking $exptracking, ExpenseTrackingService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), Response::HTTP_FORBIDDEN);

        Storage::delete('exp_tracking/'.$exptracking->document);
        activity()->on($exptracking)->withProperties(['name' => auth()->user()->first_name.' '.auth()->user()->last_name.'deleted expense (#'.$exptracking->id.')'])->log('deleted');
        $exptracking->delete();

        return redirect()->route('exptracking.index')
            ->with('flash_success', 'Expense tracking deleted successfully');
    }

    public function company(Request $request): JsonResponse
    {
        $assignment = Assignment::find($request->assignment_id);

        return response()->json($assignment->project->company);
    }

    public function resource(Request $request): JsonResponse
    {
        $assignment = Assignment::find($request->assignment_id);

        return response()->json($assignment);
    }

    public function ajaxValidation(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'account' => 'required|not_in:0',
            'account_element' => 'required|not_in:0',
            'description' => 'required|string',
            'company' => 'required|not_in:0',
            'date' => 'required|date',
            'amount' => 'required|numeric',
            'resource' => 'required_unless:assignment,0',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        return response()->json(['success' => 'Record is successfully added']);
    }

    public function approve(ExpenseTracking $expensetracking): RedirectResponse
    {
        $expensetracking->approved_by = auth()->user()->id;
        $expensetracking->approved_on = now()->toDateString();
        $expensetracking->approval_status = 2;
        $expensetracking->save();

        return redirect()->back()->with('flash_success', 'Expenses have been approved successfully');
    }

    public function reject(Request $request, ExpenseTracking $expensetracking): JsonResponse
    {
        $expensetracking->approval_status = 3;
        $expensetracking->rejection_message = $request->message;
        $expensetracking->save();

        return response()->json(['res' => 'successful']);
    }

    public function pay(Request $request, ExpenseTracking $expensetracking): JsonResponse
    {
        $expensetracking->payment_date = $request->payment_date;
        $expensetracking->payment_status = 2;
        $expensetracking->payment_reference = $request->reference;
        $expensetracking->save();

        return response()->json(['res' => 'Successful']);
    }

    public function batchapprove(Request $request): RedirectResponse
    {
        if (session('expenses')) {
            foreach ($request->expense_id as $expense) {
                $expense_tracking = ExpenseTracking::find($expense);
                $expense_tracking->approved_by = auth()->user()->id;
                $expense_tracking->approved_on = now()->toDateString();
                $expense_tracking->approval_status = 2;
                $expense_tracking->save();
            }
        }

        return redirect()->back()->with('flash_success', 'Expenses have been approved');
    }

    public function batchpay(Request $request): RedirectResponse
    {
        if (session('expenses')) {
            foreach ($request->expense_id as $expense) {
                $expensetracking = ExpenseTracking::find($expense);
                $expensetracking->payment_date = $request->payment_date;
                $expensetracking->payment_reference = $request->payment_reference;
                $expensetracking->payment_status = 2;
                $expensetracking->save();
            }
        }
        session()->forget('expenses');

        return redirect()->back()->with('flash_success', 'Expenses have been paid');
    }

    public function batchreject(Request $request): RedirectResponse
    {
        if ($request->has('expense_id')) {
            foreach ($request->expense_id as $expense) {
                $expensetracking = ExpenseTracking::find($expense);
                $expensetracking->approval_status = 3;
                $expensetracking->rejection_message = $request->rejection_message;
                $expensetracking->save();
            }
        }

        return redirect()->back()->with('flash_success', 'Expenses have been rejected');
    }

    public function sessionStore(): JsonResponse
    {
        if (session()->has('expenses')) {
            $ids = array_unique(array_merge(session('expenses'), \request()->ids));
            session(['expenses' => $ids]);
        } else {
            session(['expenses' => \request()->ids]);
        }

        return response()->json(['ids' => session('expenses')]);
    }

    public function sessionPull(): JsonResponse
    {
        $ids = [];
        foreach (session('expenses') as $id) {
            if (! in_array($id, \request()->ids)) {
                array_push($ids, $id);
            }
        }
        session(['expenses' => $ids]);

        return response()->json(['ids' => session('expenses')]);
    }
}
