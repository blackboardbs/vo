<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQualificationRequest;
use App\Http\Requests\UpdateQualificationRequest;
use App\Models\Qualification;
use App\Models\Status;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QualificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    }

    public function create($cvid, $res_id): View
    {
        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }
        $parameters = [
            'cv' => $cvid,
            'res' => $res_id,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'row_order' => $order_number,
        ];

        return view('qualification.create')->with($parameters);
    }

    public function store(StoreQualificationRequest $request, $cvid, $res_id): RedirectResponse
    {
        $qualification = new Qualification();
        $qualification->res_id = $res_id;
        $qualification->qualification = $request->input('qualification');
        $qualification->subjects = $request->input('subjects');
        $qualification->institution = $request->input('institution');
        $qualification->year_complete = $request->input('year_complete');
        $qualification->row_order = $request->input('row_order');
        $qualification->status_id = $request->input('status_id');
        $qualification->creator_id = auth()->id();
        $qualification->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Qualification captured successfully');
    }

    public function show(Qualification $qualification, Request $request): View
    {

        return view('qualification.show')->with([
            'qualification' => $qualification,
            'cvid' => $request->input('cvid'),
        ]);
    }

    public function edit($qualificationid, $cvid, $res_id): View
    {
        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }
        $parameters = [
            'qualification' => Qualification::where('id', '=', $qualificationid)->where('res_id', '=', $res_id)->get(),
            'cv' => $cvid,
            'res' => $res_id,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'row_order' => $order_number,

        ];

        return view('qualification.edit')->with($parameters);
    }

    public function update(UpdateQualificationRequest $request, $qualificationid, $cvid, $res_id): RedirectResponse
    {
        $qualification = Qualification::find($qualificationid);
        $qualification->qualification = $request->input('qualification');
        $qualification->subjects = $request->input('subjects');
        $qualification->institution = $request->input('institution');
        $qualification->year_complete = $request->input('year_complete');
        $qualification->row_order = $request->input('row_order');
        $qualification->status_id = $request->input('status_id');
        $qualification->save();

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Qualification saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        DB::table('qualification')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Qualification::destroy($id);

        return redirect()->route('cv.index')->with('success', 'Qualification deleted successfully');
    }
}
