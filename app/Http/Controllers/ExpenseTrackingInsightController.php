<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\ExpenseTracking;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExpenseTrackingInsightController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $expense_tracking = ExpenseTracking::with(['account','account_element'])->selectRaw('account_id, account_element_id, SUM(amount) AS amount');
        if ($request->has('account_element') && $request->account_element != ''){
            $expense_tracking = $expense_tracking->where('account_element_id', $request->account_element);
        }

        if ($request->has('assignment') && $request->assignment != ''){
            $expense_tracking = $expense_tracking->where('assignment_id', $request->assignment);
        }

        if ($request->has('claimable') && $request->claimable != ''){
            $expense_tracking = $expense_tracking->where('claimable', $request->claimable);
        }

        if ($request->has('billable') && $request->billable != ''){
            $expense_tracking = $expense_tracking->where('billable', $request->billable);
        }

        if ($request->has('company') && $request->company != ''){
            $expense_tracking = $expense_tracking->where('company_id', $request->company);
        }

        if ($request->has('cost_center') && $request->cost_center != ''){
            $expense_tracking = $expense_tracking->where('cost_center', $request->cost_center);
        }

        if ($request->has('resource') && $request->resource != ''){
            $expense_tracking = $expense_tracking->where('employee_id', $request->resource);
        }

        if ($request->has('payment_reference') && $request->payment_reference != ''){
            $expense_tracking = $expense_tracking->where('payment_reference','like', '%'. $request->payment_reference.'%');
        }

        if ($request->has('approved_by') && $request->approved_by != ''){
            $expense_tracking = $expense_tracking->where('approved_by', $request->approved_by);
        }

        if ($request->has('approval_status') && $request->approval_status != ''){
            $expense_tracking = $expense_tracking->where('approval_status', $request->approval_status);
        }

        if ($request->has('payment_status') && $request->payment_status != ''){
            $expense_tracking = $expense_tracking->where('payment_status', $request->payment_status);
        }

        if (($request->has('approved_on_from') && $request->approved_on_from != '') || ($request->has('approved_on_to') && $request->approved_on_to != '')){
            $expense_tracking = $expense_tracking->whereBetween('approved_on', [$request->approved_on_from, $request->approved_on_to]);
        }
        if (($request->has('payment_date_from') && $request->payment_date_from != '') || ($request->has('payment_date_to') && $request->payment_date_to != '')){
            $expense_tracking = $expense_tracking->whereBetween('approved_on', [$request->payment_date_from, $request->payment_date_to]);
        }

        if (($request->has('week_from') && $request->input('week_from') != '') || ($request->has('week_to') && $request->input('week_to') != '')){
            $expense_tracking = $expense_tracking->whereBetween('yearwk',[ (int)substr($request->week_from,0,6), (int)substr($request->week_to,0,6)]);
        }

        $expense_tracking = $expense_tracking->groupBy('account_id', 'account_element_id')
            ->orderBy('account_id')
            ->orderBy('account_element_id')
            ->paginate(15);
        $total_amount = 0;
        foreach ($expense_tracking as $expense){
            $total_amount += $expense->amount;
        }

        $expenses = ExpenseTracking::with(['company:company_name','resource:first_name,last_name','approver:first_name,last_name','assignment','assignment.project:name','cost_center_description:description'])->whereHas('assignment')->get();
        $assignment_drop_down = [];
        $weeks_drop_down = [];
        $company_drop_down = [];
        $cost_center_drop_down = [];
        $resource_drop_down = [];
        $approver_drop_down = [];
        foreach ($expenses as $expense){
            $assignment_drop_down[$expense->assignment_id] = isset($expense->assignment->project)?$expense->assignment
                    ->project->name.' ('.(isset($expense->resource)?$expense->resource->first_name:$expense->assignment_id).')':null;
            $weeks_drop_down[$expense->yearwk] = $expense->yearwk;
            $company_drop_down[$expense->company_id] = isset($expense->company->company_name)?$expense->company->company_name:null;
            $cost_center_drop_down[$expense->cost_center] = isset($expense->cost_center_description->description)?$expense->cost_center_description->description:null;
            $resource_drop_down[$expense->employee_id] = isset($expense->resource)?$expense->resource->first_name.' '.$expense->resource->last_name:null;
            $approver_drop_down[$expense->approved_by] = isset($expense->approver)?$expense->approver->first_name.' '.$expense->approver->last_name:null;
        }

        $parameters = [
            'expensetrackings' => $expense_tracking,
            'total_amount' => $total_amount,
            'account_drop_down' => Account::where('status', 1)->pluck('description', 'id'),
            'account_element_drop_down' => AccountElement::where('status', 1)->orderBy('description')->when($request->has('account') && $request->account != '', function ($account) {
                $account->where('account_id', request()->account);
            })->pluck('description', 'id'),
            'assignment_drop_down' => $assignment_drop_down,
            'weeks_drop_down' => array_filter($weeks_drop_down),
            'company_drop_down' => array_filter($company_drop_down),
            'cost_center_drop_down' => array_filter($cost_center_drop_down),
            'resource_drop_down' => array_filter($resource_drop_down),
            'approved_by_drop_down' => array_filter($approver_drop_down)
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'expensetracking');

        return view('reports.management.expensetracking')->with($parameters);
    }

    public function account(Request $request): View|BinaryFileResponse
    {
        $expense_tracking = ExpenseTracking::with(['account','account_element'])->selectRaw('account_id, SUM(amount) AS amount');
        if ($request->has('account_element') && $request->account_element != ''){
            $expense_tracking = $expense_tracking->where('account_element_id', $request->account_element);
        }

        if ($request->has('assignment') && $request->assignment != ''){
            $expense_tracking = $expense_tracking->where('assignment_id', $request->assignment);
        }

        if ($request->has('claimable') && $request->claimable != ''){
            $expense_tracking = $expense_tracking->where('claimable', $request->claimable);
        }

        if ($request->has('billable') && $request->billable != ''){
            $expense_tracking = $expense_tracking->where('billable', $request->billable);
        }

        if ($request->has('company') && $request->company != ''){
            $expense_tracking = $expense_tracking->where('company_id', $request->company);
        }

        if ($request->has('cost_center') && $request->cost_center != ''){
            $expense_tracking = $expense_tracking->where('cost_center', $request->cost_center);
        }

        if ($request->has('resource') && $request->resource != ''){
            $expense_tracking = $expense_tracking->where('employee_id', $request->resource);
        }

        if ($request->has('payment_reference') && $request->payment_reference != ''){
            $expense_tracking = $expense_tracking->where('payment_reference','like', '%'. $request->payment_reference.'%');
        }

        if ($request->has('approved_by') && $request->approved_by != ''){
            $expense_tracking = $expense_tracking->where('approved_by', $request->approved_by);
        }

        if ($request->has('approval_status') && $request->approval_status != ''){
            $expense_tracking = $expense_tracking->where('approval_status', $request->approval_status);
        }

        if ($request->has('payment_status') && $request->payment_status != ''){
            $expense_tracking = $expense_tracking->where('payment_status', $request->payment_status);
        }

        if (($request->has('approved_on_from') && $request->approved_on_from != '') || ($request->has('approved_on_to') && $request->approved_on_to != '')){
            $expense_tracking = $expense_tracking->whereBetween('approved_on', [$request->approved_on_from, $request->approved_on_to]);
        }
        if (($request->has('payment_date_from') && $request->payment_date_from != '') || ($request->has('payment_date_to') && $request->payment_date_to != '')){
            $expense_tracking = $expense_tracking->whereBetween('approved_on', [$request->payment_date_from, $request->payment_date_to]);
        }

        if (($request->has('week_from') && $request->input('week_from') != '') || ($request->has('week_to') && $request->input('week_to') != '')){
            $expense_tracking = $expense_tracking->whereBetween('yearwk',[ (int)substr($request->week_from,0,6), (int)substr($request->week_to,0,6)]);
        }

        $expense_tracking = $expense_tracking->groupBy('account_id')
            ->orderBy('account_id')
            ->paginate(15);
        $total_amount = 0;
        foreach ($expense_tracking as $expense){
            $total_amount += $expense->amount;
        }

        $expenses = ExpenseTracking::with(['assignment','assignment.project','resource','approver','company','cost_center_description'])->whereHas('assignment')->get();
        $assignment_drop_down = [];
        $weeks_drop_down = [];
        $company_drop_down = [];
        $cost_center_drop_down = [];
        $resource_drop_down = [];
        $approver_drop_down = [];
        foreach ($expenses as $expense){
            $assignment_drop_down[$expense->assignment_id] = isset($expense->assignment->project)?$expense->assignment
                    ->project->name.' ('.(isset($expense->resource)?$expense->resource->first_name:$expense->assignment_id).')':null;
            $weeks_drop_down[$expense->yearwk] = $expense->yearwk;
            $company_drop_down[$expense->company_id] = isset($expense->company->company_name)?$expense->company->company_name:null;
            $cost_center_drop_down[$expense->cost_center] = isset($expense->cost_center_description->description)?$expense->cost_center_description->description:null;
            $resource_drop_down[$expense->employee_id] = isset($expense->resource)?$expense->resource->first_name.' '.$expense->resource->last_name:null;
            $approver_drop_down[$expense->approved_by] = isset($expense->approver)?$expense->approver->first_name.' '.$expense->approver->last_name:null;
        }

        $parameters = [
            'expensetrackings' => $expense_tracking,
            'total_amount' => $total_amount,
            'account_drop_down' => Account::where('status', 1)->pluck('description', 'id'),
            'assignment_drop_down' => $assignment_drop_down,
            'weeks_drop_down' => array_filter($weeks_drop_down),
            'company_drop_down' => array_filter($company_drop_down),
            'cost_center_drop_down' => array_filter($cost_center_drop_down),
            'resource_drop_down' => array_filter($resource_drop_down),
            'approved_by_drop_down' => array_filter($approver_drop_down)
        ];
        if ($request->has('export')) return $this->export($parameters, 'reports.management', 'expensetrackingaccount');

        return view('reports.management.expensetrackingaccount')->with($parameters);
    }
}
