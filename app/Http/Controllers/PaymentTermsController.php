<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentTermsRequest;
use App\Http\Requests\UpdatePaymentTermsRequest;
use App\Models\PaymentTerm;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentTermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $payment_terms = PaymentTerm::sortable('description', 'asc')->paginate($item);

        $payment_terms = PaymentTerm::with(['status:id,description'])->sortable('description', 'asc')->where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_terms = PaymentTerm::with(['status:id,description'])->where('status_id', $request->input('status_filter'));
            }else{
                $payment_terms = PaymentTerm::with(['status:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_terms = $payment_terms->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_terms = $payment_terms->paginate($item);

        $parameters = [
            'payment_terms' => $payment_terms,
        ];

        return View('master_data.payment_terms.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = PaymentTerm::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.payment_terms.create', $parameters);
    }

    public function store(StorePaymentTermsRequest $request): RedirectResponse
    {
        $payment_terms = new PaymentTerm;
        $payment_terms->description = $request->description;
        $payment_terms->status_id = $request->status;
        $payment_terms->save();

        return redirect(route('payment_terms.index'))->with('flash_success', 'Master Data Payment Term captured successfully');
    }

    public function show($id): View
    {
        $parameters = [
            'payment_term' => PaymentTerm::find($id),
        ];

        return view('master_data.payment_terms.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = PaymentTerm::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'payment_term' => PaymentTerm::find($id),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('master_data.payment_terms.edit')->with($parameters);
    }

    public function update(UpdatePaymentTermsRequest $request, $id): RedirectResponse
    {
        $payment_terms = PaymentTerm::find($id);
        $payment_terms->description = $request->description;
        $payment_terms->status_id = $request->status;
        $payment_terms->save();

        return redirect(route('payment_terms.index'))->with('flash_success', 'Master Data Payment Term edited successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // PaymentTerm::destroy($id);
        $item = PaymentTerm::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('payment_terms.index'))->with('flash_success', 'Master Data Payment Term suspended successfully');
    }
}
