<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMCalendarRequest;
use App\Http\Requests\UpdateMCalendarRequest;
use App\Models\MCalendar;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MCalendarController extends Controller
{
    //
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $calendar = MCalendar::with(['statusd:id,description'])->sortable('date', 'asc');

        if (($request->has('date_from') && $request->input('date_from') != '') && ($request->has('date_to') && $request->input('date_to') != '')) {
            $calendar = $calendar->whereBetween('date', [$request->date_from, $request->date_to]);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $calendar = $calendar->where('date', 'like', '%'.$request->input('q').'%')
                ->orWhere('year', 'like', '%'.$request->input('q').'%')
                ->orWhere('month', 'like', '%'.$request->input('q').'%')
                ->orWhere('week', 'like', '%'.$request->input('q').'%')
                ->orWhere('day', 'like', '%'.$request->input('q').'%')
                ->orWhere('day_name', 'like', '%'.$request->input('q').'%');
        }

        $calendar = $calendar
            ->when($request->has('status_filter') && $request->input('status_filter') != '', function ($query) use ($request) {
                return $query->where('status', $request->input('status_filter'));
            }, function ($query) {
                // If no status filter is provided, keep the existing status filter
                return $query->where('status', 1);
            });

        $calendar = $calendar->paginate($item);

        $parameters = [
            'calendar' => $calendar,
        ];

        return View('master_data.calendar.index', $parameters);
    }

    public function create($type): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        if ($type == 'day') {
            return View('master_data.calendar.create')->with($parameters);
        } else {
            return View('master_data.calendar.createweek')->with($parameters);
        }
    }

    public function store(StoreMCalendarRequest $request): RedirectResponse
    {
        $date_exists = MCalendar::where('date', $request->input('calendar_date'))->first();

        if (! isset($date_exists->date)) {
            $mcalendar = new MCalendar();
            $mcalendar->date = $request->input('calendar_date');
            $mcalendar->year = $request->input('year');
            $mcalendar->month = $request->input('month');
            $mcalendar->week = $request->input('week');
            $mcalendar->day = $request->input('day');
            $mcalendar->day_name = $request->input('dayname');
            $mcalendar->start_of_week_date = $request->input('start_of_week_date');
            $mcalendar->end_of_week_date = $request->input('end_of_week_date');
            $mcalendar->weekday = $request->input('weekday');
            $mcalendar->weekend = $request->input('weekend');
            $mcalendar->public_holiday = $request->input('publich');
            $mcalendar->mon = $request->input('mon');
            $mcalendar->tue = $request->input('tue');
            $mcalendar->wed = $request->input('wed');
            $mcalendar->thu = $request->input('thu');
            $mcalendar->fri = $request->input('fri');
            $mcalendar->sat = $request->input('sat');
            $mcalendar->sun = $request->input('sun');
            $mcalendar->status = $request->input('status');
            $mcalendar->creator_id = auth()->id();
            $mcalendar->created_at = now();
            $mcalendar->save();

            return redirect(route('master_calendar.index'))->with('flash_success', 'Master Calendar captured successfully');
        } else {
            return redirect(route('master_calendar.index'))->with('flash_danger', 'Master Calendar selected date already exists.');
        }
    }

    public function createweek(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.calendar.createweek')->with($parameters);
    }

    public function storerange(Request $request): RedirectResponse
    {
        $start = $request->input('start_date');
        $end = $request->input('end_date');

        $datediff = Carbon::parse($end)->diffInDays($start);

        $existing_dates = MCalendar::orderBy('date', 'asc')->pluck('date');
        $existing_dates_exist = [];
        $daterange = [];
        $err = 0;

        for ($i = 0; $i <= $datediff; $i++) {
            $sdate = Carbon::parse($start);
            $edate = Carbon::parse($end);
            $date = $sdate->addDay($i)->toDateString();
            $weekday = Carbon::parse($date)->englishDayOfWeek;
            $weekstart = $sdate->setISODate(Carbon::parse($date)->year, Carbon::parse($date)->weekOfYear);

            if (in_array($date, collect($existing_dates)->toArray())) {
                $err++;
            } else {
                /*$daterange[$i]["start"] = $sdate->setISODate(Carbon::parse($date)->year, Carbon::parse($date)->weekOfYear)->toDateString();
                $daterange[$i]["end"] = $sdate->setISODate(Carbon::parse($date)->year, Carbon::parse($date)->weekOfYear,7)->toDateString();
                $daterange[$i]["day"] = Carbon::parse($date)->englishDayOfWeek;
                $daterange[$i]["dayDate"] = Carbon::parse($date)->day;
                $daterange[$i]["dayMonth"] = Carbon::parse($date)->month;
                $daterange[$i]["dayYear"] = Carbon::parse($date)->year;
                $daterange[$i]["week"] = Carbon::parse($date)->weekOfYear;*/

                $mcalendar = new MCalendar();
                $mcalendar->date = $date;
                $mcalendar->year = Carbon::parse($date)->year;
                $mcalendar->month = Carbon::parse($date)->month;
                $mcalendar->week = Carbon::parse($date)->weekOfYear;
                $mcalendar->day = Carbon::parse($date)->day;
                $mcalendar->day_name = Carbon::parse($date)->englishDayOfWeek;
                $mcalendar->start_of_week_date = $sdate->setISODate(Carbon::parse($date)->year, Carbon::parse($date)->weekOfYear)->toDateString();
                $mcalendar->end_of_week_date = $sdate->setISODate(Carbon::parse($date)->year, Carbon::parse($date)->weekOfYear, 7)->toDateString();
                if ($weekday == 'Sunday' || $weekday == 'Saturday') {
                    $mcalendar->weekday = 'N';
                } else {
                    $mcalendar->weekday = 'Y';
                }
                if ($weekday == 'Sunday' || $weekday == 'Saturday') {
                    $mcalendar->weekend = 'Y';
                } else {
                    $mcalendar->weekend = 'N';
                }
                $mcalendar->public_holiday = 'N';
                $mcalendar->mon = $weekstart->day;
                $mcalendar->tue = $weekstart->day + 1;
                $mcalendar->wed = $weekstart->day + 2;
                $mcalendar->thu = $weekstart->day + 3;
                $mcalendar->fri = $weekstart->day + 4;
                $mcalendar->sat = $weekstart->day + 5;
                $mcalendar->sun = $weekstart->day + 6;
                $mcalendar->status = $request->input('status');
                $mcalendar->created_at = now();
                $mcalendar->creator_id = auth()->id();
                $mcalendar->save();
            }
        }

        //dd($daterange);
        if ($err == 0) {
            return redirect(route('master_calendar.index'))->with('flash_success', 'Master Calendar captured successfully');
        } else {
            return redirect(route('master_calendar.index'))->with('flash_success', 'Master Calendar captured successfully. But some of the dates in the selected date range already exist.');
        }
    }

    public function edit($mcalendarid): View
    {
        $mcalendar = MCalendar::where('calendar_id', $mcalendarid)->get();

        $parameters = [
            'mcalendar' => $mcalendar,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.calendar.edit')->with($parameters);
    }

    public function update(UpdateMCalendarRequest $request, $mcalendarid): RedirectResponse
    {
        $mcalendar = MCalendar::find($mcalendarid);
        $mcalendar->date = $request->input('calendar_date');
        $mcalendar->year = $request->input('year');
        $mcalendar->month = $request->input('month');
        $mcalendar->week = $request->input('week');
        $mcalendar->day = $request->input('day');
        $mcalendar->day_name = $request->input('dayname');
        $mcalendar->start_of_week_date = $request->input('start_of_week_date');
        $mcalendar->end_of_week_date = $request->input('end_of_week_date');
        $mcalendar->weekday = $request->input('weekday');
        $mcalendar->weekend = $request->input('weekend');
        $mcalendar->public_holiday = $request->input('publich');
        $mcalendar->mon = $request->input('mon');
        $mcalendar->tue = $request->input('tue');
        $mcalendar->wed = $request->input('wed');
        $mcalendar->thu = $request->input('thu');
        $mcalendar->fri = $request->input('fri');
        $mcalendar->sat = $request->input('sat');
        $mcalendar->sun = $request->input('sun');
        $mcalendar->status = $request->input('status');
        $mcalendar->creator_id = auth()->id();
        $mcalendar->save();

        return redirect(route('master_calendar.index'))->with('flash_success', 'Master Calendar updated successfully');
    }

    public function show($mcalendarid): View
    {
        $mcalendar = MCalendar::where('calendar_id', $mcalendarid)->get();

        $parameters = [
            'mcalendar' => $mcalendar,
        ];

        return View('master_data.calendar.show')->with($parameters);
    }

    public function destroy($id){

        $item = MCalendar::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('master_calendar.index')->with('success', 'Master Data Date suspended successfully');
    }
}
