<?php

namespace App\Http\Controllers;

use App\Exports\AssessmentsExport;
use App\Exports\LeavesExport;
use App\Models\Assignment;
use App\Models\Calendar;
use App\Http\Requests\StoreLeaveRequest;
use App\Http\Requests\UpdateLeaveRequest;
use App\Jobs\SendLeaveApprovalEmailJob;
use App\Jobs\SendLeaveEmailJob;
use App\Models\Leave;
use App\Models\Config;
use App\Models\LeaveType;
use App\Models\LeaveBalance;
use App\Mail\LeaveApprovedMail;
use App\Mail\LeaveDeniedMail;
use App\Mail\LeaveRequestMail;
use App\Models\Module;
use App\Models\Notification;
use App\Models\PublicHoliday;
use App\Models\Resource;
use App\Models\Status;
use App\Models\Team;
use App\Models\User;
use App\Models\UserNotification;
use Carbon\Carbon;
use App\Services\LeaveService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $leave = Leave::with([
            'resource' => fn($user) => $user->select(['id', 'first_name', 'last_name', 'resource_id'])->with(['resource:id,manager_id']),
            'leave_type:id,description',
            'statusd:id,description',
        ])->sortable(['id' => 'desc']);

        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                $leave = $leave->whereIn('emp_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        $can_create = false;
        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            $can_create = true;
        }

        $can_update = false;
        if (Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all') || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team')) {
            $can_update = true;
        }

        $item = $request->input('r') ?? 15;

        if ($request->has('leave_type') && $request->input('leave_type') != '') {
            $leave = $leave->where('leave_type_id',$request->leave_type);
        }

        if ($request->has('from') && $request->input('from') != '') {
            $leave = $leave->where('date_from','>=',$request->from);
        }
        if ($request->has('to') && $request->input('to') != '') {
            $leave = $leave->where('date_to','<=',$request->to);
        }


        if ($request->has('q') && $request->input('q') != '') {
            $leave = $leave->where(fn($leav) => $leav->where('date_from', 'LIKE', '%'.$request->input('q').'%')
                ->orWhere('id', $request->input('q'))
                ->orWhere('date_to', 'like', '%'.$request->input('q').'%')
                ->orWhere('no_of_days', 'like', '%'.$request->input('q').'%')
                ->orWhere('approve_date', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('resource', function ($query) use ($request) {
                    $query->where('first_name', 'like', '%'.$request->input('q').'%');
                    $query->orWhere('last_name', 'like', '%'.$request->input('q').'%');
                })
                ->orWhereHas('leave_type', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                })
                ->orWhereHas('statusd', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->input('q').'%');
                }));
        }

        $leave = $leave->paginate($item);

        $projects = [];

        foreach ($leave as $l) {
            $projects[$l->id] = Assignment::select('project_id', 'start_date', 'end_date')->with('project')->whereHas('project')->where('end_date', '>', $l->date_from)->where('start_date', '<', $l->date_to)->where('employee_id', '=', $l->emp_id)->get();
        }

        $parameters = [
            'leave' => $leave,
            'leave_type_drop_down' => LeaveType::orderBy('description')->pluck('description','id'),
            'projects' => $projects,
            'can_create' => $can_create,
            'can_update' => $can_update,
        ];

        if ($request->has('export')) return $this->export($parameters, 'leave');

        return view('leave.index', $parameters);
    }

    public function create(LeaveService $service)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $users = User::join('resource', 'users.id', '=', 'resource.user_id')->select(DB::raw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS full_name"), 'users.id');

        if(!Auth::user()->hasAnyRole(['admin','admin_manager','manager'])){
            $users = $users->where('users.id', Auth::id());
        }

        $users = $users->excludes([6, 7, 8, 9])->orderBy('users.first_name')->orderBy('users.last_name')->get();

        $parameters = [
            'resource' => $users,
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'leave_type' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Select', '0'),
        ];

        return view('leave.create')->with($parameters);
    }

    public function store(StoreLeaveRequest $request)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'create_all') || Auth::user()->canAccess($module->id, 'create_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $manager = User::join('resource', 'users.id', '=', 'resource.user_id')->where('users.id', '=', $request->input('resource'))->first();

        //if user doesn't belong to a team
        if ($manager->team_id == '0' && $manager->manager_id > 0) {
            $manager_id = User::where('id', $manager->manager_id)->first();
            $approveemp = $manager_id->id;
        }

        if ($manager->team_id > 0) {
            $team_manager = Team::where('id', $manager->team_id)->first();

            if (isset($team_manager->team_manager) && $team_manager->team_manager > 0) {
                $manager_id = User::where('id', $team_manager->team_manager)->first();
                $approveemp = $manager_id->id;
            } else {
                $manager_id = User::whereHas('roles', function ($q) {
                    $q->where('name', 'admin');
                })->first(['id']);
                $approveemp = $manager_id->id;
            }
        }

        if ($manager->manager_id == null || ($manager->manager_id == '0' && $manager->team_id == '0')) {
            $manager_id = User::whereHas('roles', function ($q) {
                $q->where('name', 'admin');
            })->first(['id']);
            $approveemp = $manager_id->id;
        }

        $leave = new Leave();
        $leave->emp_id = $request->input('resource');
        $leave->date_from = $request->input('date_from');
        $leave->date_to = $request->input('date_to');
        $leave->no_of_days = $request->input('no_of_days');
        $leave->leave_type_id = $request->input('leave_type');
        $leave->contact_details = $request->input('contact_details');
        $leave->leave_status = $request->input('leave_status');
        $leave->status = 1;
        $leave->approve_emp_id = $approveemp;
        $leave->creator_id = auth()->id();
        $leave->half_day = $request->input('halfdaydate');
        $leave->save();

        activity()->on($leave)
            ->withProperties([
                'leave_type' => $leave->leave_type->description,
            ])
            ->log('applied');

        $id = $leave->id;

        //get resource full name
        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', $request->input('resource'))->first();

        //insert notification message for manager
        $notification = new Notification;
        $notification->name = 'New Leave Request: '.$resource->full_name;
        $notification->link = route('leave.edit', $id);
        $notification->save();

        $user_notifications = [];

        if (! is_null($approveemp)) {
            array_push($user_notifications, [
                'user_id' => $approveemp,
                'notification_id' => $notification->id,
            ]);
        }

        $leave_type = LeaveType::where('id', $request->input('leave_type'))->first(['description']);

        $data = [];
        $data['date_from'] = $request->input('date_from');
        $data['date_to'] = $request->input('date_to');
        $data['resource'] = $request->input('resource');

        $projects = $this->getAssignment($request, $data);

        $annual_leave = $this->getAnnual($request->input('resource'), $request->input('date_from'));

        $mailto = User::where('id', $manager_id->id)->first();

        Mail::to($mailto->email)->send(new LeaveRequestMail($leave, $leave_type, $projects, $resource, $annual_leave));

        return redirect()->route('leave.index')->with('flash_success', 'Leave captured successfully');
    }

    public function edit(Request $request, Leave $leave, LeaveService $service)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }
        // dd($leave);
        $config = Config::first();
        $resource = Resource::where('user_id',$leave->emp_id)->first();
        // dd($leave);
        $leave_balance = LeaveBalance::where('resource_id', $resource->user_id)->latest()->first();
        $annual_leave = $service->calculateAnnualLeave($request,$config,$resource,$leave_balance);
        $sick_leave = $service->calculateSickLeave($request,$config,$resource);

        if($request->has('application')){
// dd($sick_leave);
            $parameters = [
                'result' => $leave,
                'annual_leave'=> $annual_leave['closing_balance'] - ($annual_leave['total_leave_applied_for'] + $annual_leave['leave_taken']),
                'sick_leave' => $sick_leave,
                'resource' => User::join('resource', 'users.id', '=', 'resource.user_id')->select(DB::raw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS full_name"), 'users.id')->orderBy('users.first_name')->orderBy('users.last_name')->pluck('full_name', 'users.id'),
                'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
                'leave_type' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Select', '0'),
            ];

            return view('leave.edit')->with($parameters);
        }


        $parameters = [
            'result' => $leave,
            'annual_leave'=> $annual_leave['closing_balance'] - ($annual_leave['total_leave_applied_for'] + $annual_leave['leave_taken']),
            'sick_leave' => $sick_leave,
            'resource' => User::join('resource', 'users.id', '=', 'resource.user_id')->select(DB::raw("CONCAT(users.first_name,' ',COALESCE(users.last_name,'')) AS full_name"), 'users.id')->orderBy('users.first_name')->orderBy('users.last_name')->pluck('full_name', 'users.id'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'leave_type' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Select', '0'),
        ];

        return view('leave.edit')->with($parameters);
    }

    public function update(UpdateLeaveRequest $request, $leaveid)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $leave = Leave::find($leaveid);
        /*$leave->emp_id = $request->input('resource');*/
        $leave->date_from = $request->input('date_from');
        $leave->date_to = $request->input('date_to');
        $leave->no_of_days = $request->input('no_of_days');
        $leave->leave_type_id = $request->input('leave_type');
        $leave->contact_details = $request->input('contact_details');
        $leave->leave_status = $request->input('leave_status');
        $leave->status = 1;
        if ($request->input('leave_status') > 0) {
            $leave->approve_emp_id = auth()->id();
        }
        $leave->half_day = $request->input('halfdaydate');
        $leave->save();

        activity()->on($leave)->withProperties(['leave_type' => $leave->leave_type->description])->log('updated');

        $approveemp = User::where('id', $leave->emp_id)->first();
        //if(auth()->id() == $approveemp) {
        //get last insert id
        $id = $leave->id;

        //get resource full name
        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', $approveemp->id)->first();
        $user = User::where('id', auth()->id())->first();

        //insert notification message for user
        $notification = new Notification;
        $notification->name = 'Updated Leave Request';
        $notification->link = route('leave.show', $id);
        $notification->save();

        $user_notifications = [];

        if (! is_null($approveemp)) {
            array_push($user_notifications, [
                'user_id' => $resource->id,
                'notification_id' => $notification->id,
            ]);
        }

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);

        $leave_type = LeaveType::where('id', $request->input('leave_type'))->first(['description']);

        $data = [];
        $data['date_from'] = $request->input('date_from');
        $data['date_to'] = $request->input('date_to');
        $data['resource'] = $resource->id;

        $projects = $this->getAssignment($request, $data);

        if ($request->input('leave_status') == '1') {
            Mail::to($user->email)->send(new LeaveApprovedMail($leave, $leave_type, $projects, $resource, $user));
        }
        if ($request->input('leave_status') == '2') {
            Mail::to($user->email)->send(new LeaveDeniedMail($leave, $leave_type, $projects, $resource, $user));
        }
        return redirect(route('leave.index'))->with('flash_success', 'Leave saved successfully');
    }

    public function approve($leave_id)
    {
        //return Leave::find($leave_id);
        $leave = Leave::find($leave_id);
        $leave->approve_date = Carbon::now()->toDateString();
        $leave->approve_emp_id = auth()->user()->id;
        $leave->leave_status = 1;
        $leave->save();

        $leave_type = LeaveType::where('id', $leave->leave_type_id)->first(['description']);
        $projects = Assignment::select('project_id')->with('project')->whereHas('project')->where('end_date', '>', $leave->date_from)->where('employee_id', '=', $leave->emp_id)->groupBy('project_id')->get();
        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', $leave->emp_id)->first();
        $user = User::where('id', auth()->id())->first();

        //insert notification message for user
        $notification = new Notification;
        $notification->name = 'Updated Leave Request';
        $notification->link = route('leave.show', $leave->id);
        $notification->save();

        $user_notifications = [];

        if (! is_null($leave->approve_emp_id)) {
            array_push($user_notifications, [
                'user_id' => $resource->id,
                'notification_id' => $notification->id,
            ]);
        }

        activity()->on($leave)->withProperties([
            'name' => substr(auth()->user()->first_name, 0, 1).'. '.auth()->user()->last_name.' approved '
                .substr($leave->resource->first_name, 0, 1).'. '.$leave->resource->last_name.'\'s '.$leave->leave_type->description.((! strpos($leave->leave_type->description, 'Leave') ? ' Leave' : '')),
        ])->log('approved');

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);

        Mail::to($leave->resource->email)->send(new LeaveApprovedMail($leave, $leave_type, $projects, $resource, $user));


        if (\request()->ajax()){
            return response()->json(['message' => 'This leave was approved']);
        }
        //SendLeaveApprovalEmailJob::dispatch($leave->resource->email, $leave, $leave_type, $projects, $resource, $user, 1)->delay(now()->addMinutes(5));

        return redirect(route('leave.show', $leave))->with('flash_success', 'This leave was approved');
    }

    public function decline($leave_id)
    {
        //return Leave::find($leave_id);
        $leave = Leave::find($leave_id);
        $leave->approve_date = Carbon::now()->toDateString();
        $leave->approve_emp_id = auth()->user()->id;
        $leave->leave_status = 2;
        $leave->save();

        $leave_type = LeaveType::where('id', $leave->leave_type_id)->first(['description']);
        $projects = Assignment::select('project_id')->with('project')->whereHas('project')->where('end_date', '>', $leave->date_from)->where('employee_id', '=', $leave->emp_id)->groupBy('project_id')->get();
        $resource = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->where('id', $leave->emp_id)->first();
        $user = User::where('id', auth()->id())->first();

        //insert notification message for user
        $notification = new Notification;
        $notification->name = 'Updated Leave Request';
        $notification->link = route('leave.show', $leave->id);
        $notification->save();

        $user_notifications = [];

        if (! is_null($leave->approve_emp_id)) {
            array_push($user_notifications, [
                'user_id' => $resource->id,
                'notification_id' => $notification->id,
            ]);
        }

        activity()->on($leave)->withProperties([
            'name' => substr(auth()->user()->first_name, 0, 1).'. '.auth()->user()->last_name.' declined '
                .substr($leave->resource->first_name, 0, 1).'. '.$leave->resource->last_name.'\'s '.$leave->leave_type->description.((! strpos($leave->leave_type->description, 'Leave') ? ' Leave' : '')),
        ])->log('declined');

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);

        Mail::to($leave->resource->email)->send(new LeaveDeniedMail($leave, $leave_type, $projects, $resource, $user));


        if (\request()->ajax()){
            return response()->json(['message' => 'This leave has been declined']);
        }
        //SendLeaveApprovalEmailJob::dispatch($leave->resource->email, $leave, $leave_type, $projects, $resource, $user, 2)->delay(now()->addMinutes(5));

        return redirect(route('leave.show', $leave))->with('flash_success', 'This leave has been declined');
    }

    public function show(Request $request, $leaveid)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'show_all') || Auth::user()->canAccess($module->id, 'show_team')) {
            //Continue
        } else {
            return abort(403);
        }

        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $leave = Leave::where('id', '=', $leaveid)->get();
        if ($leave) {
            $parameters = [
                'leave' => $leave,
                'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', 'all'),
                'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Active', '1'),
                'leave_type' => LeaveType::orderBy('id')->pluck('description', 'id')->prepend('Select', '0'),
                'path' => $path,
            ];

            return view('leave.show')->with($parameters);
        } else {
            return redirect()->back()->with('flash_danger', 'The leave you\'re looking for was not found');
        }
    }

    public function destroy($id)
    {
        $module = Module::where('name', '=', \App\Models\Leave::class)->first();

        if (Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team')) {
            //Continue
        } else {
            return abort(403);
        }

        $leave = Leave::find($id);
        activity()->on($leave)->withProperties([
            'name' => substr(auth()->user()->first_name, 0, 1).'. '.auth()->user()->last_name.' deleted '
                .substr($leave->resource->first_name, 0, 1).'. '.$leave->resource->last_name.'\'s '.$leave->leave_type->description.((! strpos($leave->leave_type->description, 'Leave') ? ' Leave' : '')),
        ])->log('deleted');
        DB::table('leave')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Leave::destroy($id);

        return redirect()->route('leave.index')->with('success', 'Leave deleted successfully');
    }

    public function getAssignment(Request $data)
    {
        $data['from'] = $data->date_from;
        $data['to'] = $data->date_to;
        $data['emp_id'] = $data->resource;

        $data['datediff'] = Carbon::parse($data->date_to)->diffInDays($data->date_from);

        $assignment = [];
        $projects = [];
        for ($i = 0; $i <= $data['datediff']; $i++) {
            //$query = Assignment::with('wbs')->where('start_date', '<=', Carbon::parse($data["from"])->addDay($i)->toDateString())->where('end_date', '>=', Carbon::parse($data["from"])->addDay($i)->toDateString())->where('emp_id', $data->emp_id)->get();
            $query = Assignment::with('project')->whereRaw('? between start_date and end_date', [Carbon::parse($data['from'])->addDay($i)->toDateString()])->where('employee_id', $data->emp_id)->whereNotIn('id', $assignment)->get();

            foreach (collect($query)->toArray() as $ass) {
                array_push($assignment, $ass['id']);
                array_push($projects, $ass['project']['name']);
            }
        }
        $data['result'] = $assignment;
        $data['projects'] = $projects;
        $data['contact'] = User::find($data['emp_id']);

        $data['count'] = count($assignment);

        return $data;
    }

    public function getAnnual($emp, $start_date)
    {
        $resource = User::join('resource', 'users.id', '=', 'resource.user_id')->where('users.id', $emp)->first();

        $start_date = Carbon::parse($start_date)->toDateString();

        $join_date = Carbon::parse($resource->join_date)->toDateString();

        $join_year = Carbon::parse(now())->year;
        $join_month = Carbon::parse($resource->join_date)->month;
        $join_day = Carbon::parse($resource->join_date)->startOfMonth()->day;

        $current_day = Carbon::parse($start_date)->startOfMonth()->day;
        $current_month = Carbon::parse($start_date)->month;
        $current_year = Carbon::parse($start_date)->year;

        if ($join_month == '12') {  //
            if ($join_year < $current_year) {
                $leave_start_year = $current_year - 1;
                $leave_start_month = $current_month;
                $leave_start_day = $current_day;
            } else {
                $leave_start_year = $current_year;
                $leave_start_month = $current_month;
                $leave_start_day = $current_day;
            }
        } elseif (($join_month <= $current_month && $join_day <= $current_day) || ($current_year == $join_year)) {
            //get resource join date month and day with current year
            $leave_start_year = $current_year;
            $leave_start_month = $join_month;
            $leave_start_day = $join_day;
        } else {
            $leave_start_year = $current_year - 1;
            $leave_start_month = $join_month;
            $leave_start_day = $join_day;
        }

        //resource join date in current year
        $resource_start_date = $leave_start_year.'-'.$leave_start_month.'-'.$leave_start_day;
        $leave_start = Carbon::parse($resource_start_date)->format('Y-m-d');

        if ($join_month <= $current_month && $join_day <= $current_day) {
            if ($current_year == $join_year) {
                $date_difference = Carbon::parse($resource_start_date)->diffInMonths($start_date);
            } else {
                $date_difference = Carbon::parse(Carbon::parse($start_date)->startOfYear())->diffInMonths($start_date);
            }
        } else {
            //How long has the resource worked this cycle
            if (Carbon::parse($resource->join_date)->lt(Carbon::parse($start_date))) {
                $date_difference = Carbon::parse(Carbon::parse($start_date)->startOfYear())->diffInMonths($start_date) + 1;
            } else {
                $date_difference = 0;
            }
            //$date_difference = Carbon::parse($resource_start_date)->diffInMonths($start_date);
        }

        $work_days = 5;

        if ($work_days == 5) {
            $annual_accrual = 1.25;
        }

        if ($work_days == 6) {
            $annual_accrual = 1.5;
        }

        $leave_taken = Leave::where('emp_id', $emp)->where('leave_type_id', '1')->where('leave_status', '1')->get();
        $total_leave = 0;
        foreach ($leave_taken as $item) {
            if (Carbon::parse($item->date_from)->lt(Carbon::parse($start_date)->endOfMonth()) && Carbon::parse($item->date_from)->year == $current_year) {
                $total_leave = $total_leave + $item->no_of_days;
            }
        }

        //dd($total_leave);
        //dd(($annual_accrual * $date_difference) - $total_leave);
        $opening_balance1 = $annual_accrual * $date_difference;
        $opening_balance2 = $opening_balance1 - $total_leave;

        return $opening_balance2;
    }

    public function getLeaveDays($start_date, $end_date)
    {
        $leave_days = Calendar::where('weekday', 'Y')->where('public_holiday', 'N')->whereBetween('date', [$start_date, $end_date])->get();

        /*$public_holidays = PublicHoliday::select(['date'])->where('type', 'Public Holiday')->whereBetween('date', [$start_date, $end_date])->get();
        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);

        $no_of_days = $start_date->diffInDaysFiltered(function (Carbon $date){
            return $date->isWeekday();
        }, $end_date) + 1;

        $week_days_holiday = 0;

        foreach ($public_holidays as $holiday){
            if (Carbon::parse($holiday->date)->isWeekday()){
                $week_days_holiday++;
            }
        }*/

        return response()->json(['days' => count($leave_days),'dates'=>$leave_days]);
    }

    public function getAllAnnualLeaveDays(Request $request, LeaveService $service)
    {
        $leave_balance = LeaveBalance::where('resource_id', '=', $request->input('resource'))->latest()->first();

            $config = Config::first();
            $resource = Resource::where('user_id',$request->input('resource'))->first();
            
        $resource_balance_found = false;

        if ($request->has('resource') == false || empty($leave_balance) == true) {
            $employee_dates = $service->calculateEmployeeDates($resource->join_date??$resource->created_at,$request->date??now()->toDateString());
            $total_leave_applied_for = Leave::where('emp_id', $resource->user_id)->where('leave_type_id', 1)->whereNotIn('leave_status',[1,2])->whereBetween('date_from', [$employee_dates['start_date'], $request->date??now()])
                ->latest()
                ->sum('no_of_days');
            $sick_leave = $service->calculateSickLeave($request,$config,$resource);

            return response()->json(['found'=>$resource_balance_found,'leave'=>0,'sick_leave'=>$sick_leave["total_leave"],'total_leave_applied_for'=>$total_leave_applied_for]);
        } else {
            $resource_balance_found = true;
            
            $annual_leave = $service->calculateAnnualLeave($request,$config,$resource,$leave_balance);
            $sick_leave = $service->calculateSickLeave($request,$config,$resource);

            return response()->json(['found'=>$resource_balance_found,'total_leave_applied_for'=>$annual_leave['total_leave_applied_for'],'leave'=>$annual_leave['closing_balance'],'sick_leave'=>$config->sick_leave_days_per_cycle]);
        }
    }
}
