<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLogbookRequest;
use App\Http\Requests\UpdateLogbookRequest;
use App\Models\Logbook;
use App\Models\Module;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class LogbookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $logbook = Logbook::with(['status:id,description'])->sortable('id', 'desc');

        $module = Module::where('name', '=', 'App\Models\LogBook')->get();

        if (Auth::user()->canAccess($module[0]->id, 'view_all') || Auth::user()->canAccess($module[0]->id, 'view_team')) {
            if (! Auth::user()->canAccess($module[0]->id, 'view_all') && Auth::user()->canAccess($module[0]->id, 'view_team')) {
                $logbook = $logbook->whereIn('emp_id', Auth::user()->team());
            }
        } else {
            return abort(403);
        }

        if ($request->has('q') && $request->input('q') != '') {
            $logbook = Logbook::where('log_date', 'LIKE', '%'.$request->input('q').'%')
                ->orwhere('description', 'LIKE', '%'.$request->input('q').'%')
                ->orwhere('open_reading', 'LIKE', '%'.$request->input('q').'%')
                ->orwhere('km', 'LIKE', '%'.$request->input('q').'%')
                ->orwhere('fuel_cost', 'LIKE', '%'.$request->input('q').'%')
                ->orwhere('maint_cost', 'LIKE', '%'.$request->input('q').'%')
                ->orWhereHas('status', function ($query) use ($request) {
                    $query->where('description', 'LIKE', '%'.$request->input('q').'%');
                });
        }

        $logbook = $logbook->sortable(['log_date' => 'desc'])->paginate($item);

        $parameters = [
            'logbook' => $logbook,
        ];

        return view('logbook.index')->with($parameters);
    }

    public function show($logbookid): View
    {
        $parameters = [
            'logbook' => Logbook::where('id', '=', $logbookid)->get(),
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', 'all'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Active', '1'),
        ];

        return view('logbook.show')->with($parameters);
    }

    public function create(): View
    {
        $parameters = [
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', 'all'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
            'date' => Carbon::now()->toDateString(),
        ];

        return view('logbook.create')->with($parameters);
    }

    public function store(StoreLogbookRequest $request): RedirectResponse
    {
        $referrer = new Logbook();
        $referrer->log_date = $request->input('log_date');
        $referrer->emp_id = $request->input('resource');
        $referrer->description = $request->input('description');
        $referrer->open_reading = $request->input('opening_km');
        $referrer->km = $request->input('km');
        $referrer->fuel_cost = $request->input('fuel_cost');
        $referrer->maint_cost = $request->input('maint_cost');
        $referrer->status_id = $request->input('status');
        $referrer->save();

        return redirect(route('logbook.index'))->with('flash_success', 'Logbook captured successfully');
    }

    public function edit($logbookid): View
    {
        $parameters = [
            'logbook' => Logbook::where('id', '=', $logbookid)->get(),
            'resource' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', 'all'),
            'sidebar_process_statuses' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Active', '1'),
        ];

        return view('logbook.edit')->with($parameters);
    }

    public function update(UpdateLogbookRequest $request, $logbookid): RedirectResponse
    {
        $referrer = Logbook::find($logbookid);
        $referrer->log_date = $request->input('log_date');
        $referrer->emp_id = $request->input('resource');
        $referrer->description = $request->input('description');
        $referrer->open_reading = $request->input('opening_km');
        $referrer->km = $request->input('km');
        $referrer->fuel_cost = $request->input('fuel_cost');
        $referrer->maint_cost = $request->input('maint_cost');
        $referrer->status_id = $request->input('status');
        $referrer->save();

        return redirect(route('logbook.index'))->with('flash_success', 'Logbook entry updated successfully');
    }

    public function destroy($id): RedirectResponse
    {
        Logbook::destroy($id);

        return redirect()->route('logbook.index')
            ->with('success', 'Anniversary deleted successfully');
    }
}
