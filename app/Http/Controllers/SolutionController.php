<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSolutionRequest;
use App\Http\Requests\UpdateSolutionRequest;
use App\Models\Solution;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SolutionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $solution = Solution::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $solution = $solution->where('status', $request->input('status_filter'));
            }else{
                $solution = $solution->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $solution = $solution->where('description', 'like', '%'.$request->input('q').'%');
        }

        $solution = $solution->paginate($item);

        $parameters = [
            'solution' => $solution,
        ];

        return View('master_data.solution.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Solution::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.solution.create')->with($parameters);
    }

    public function store(StoreSolutionRequest $request): RedirectResponse
    {
        $solution = new Solution();
        $solution->description = $request->input('description');
        $solution->version = $request->input('version');
        $solution->status = $request->input('status');
        $solution->creator_id = auth()->id();
        $solution->save();

        return redirect(route('solution.index'))->with('flash_success', 'Master Data Solution captured successfully');
    }

    public function show($solution_id): View
    {
        $parameters = [
            'solution' => Solution::where('id', '=', $solution_id)->get(),
        ];

        return View('master_data.solution.show')->with($parameters);
    }

    public function edit($solution_id): View
    {
        $autocomplete_elements = Solution::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'solution' => Solution::where('id', '=', $solution_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.solution.edit')->with($parameters);
    }

    public function update(UpdateSolutionRequest $request, $solution_id): RedirectResponse
    {
        $solution = Solution::find($solution_id);
        $solution->description = $request->input('description');
        $solution->version = $request->input('version');
        $solution->status = $request->input('status');
        $solution->creator_id = auth()->id();
        $solution->save();

        return redirect(route('solution.index'))->with('flash_success', 'Master Data Solution saved successfully');
    }

    public function destroy($solution_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // Solution::destroy($solution_id);
        $item = Solution::find($solution_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('solution.index')->with('success', 'Master Data Solution suspended successfully');
    }
}
