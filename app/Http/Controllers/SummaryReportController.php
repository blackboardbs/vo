<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Team;
use App\Models\Time;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SummaryReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('r') ?? 15;

        $function = Assignment::with(['function_desc'])->selectRaw('assignment.function, timesheet.year, timesheet.month, SUM(CASE WHEN timeline.is_billable = 1 THEN timeline.total + (timeline.total_m/60) ELSE 0 END) AS hours,
        SUM(CASE WHEN timeline.is_billable = 1 THEN (timeline.total + (timeline.total_m/60))*assignment.invoice_rate ELSE 0 END) AS value')
            ->leftJoin('timesheet', function ($join) {
                $join->on('assignment.project_id', '=', 'timesheet.project_id');
                $join->on('assignment.employee_id', '=', 'timesheet.employee_id');
            })->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id');
        if ($request->has('company') && $request->company != '') {
            $function = $function->where('timesheet.company_id', '=', $request->company);
        }
        if ($request->has('team') && $request->team != null) {
            $function = $function->whereHas('resource_user', function ($query) use ($request) {
                $query->where('team_id', '=', $request->team);
            });
        }
        if ($request->has('employee') && $request->employee) {
            $function = $function->where('timesheet.employee_id', '=', $request->employee);
        }
        if ($request->has('yearmonth') && $request->yearmonth != null) {
            $function = $function->where('timesheet.year', '=', substr($request->yearmonth, 0, 4))->where('timesheet.month', '=', substr($request->yearmonth, 4));
        }
        if (($request->has('date_from') && $request->date_from != '') || ($request->has('date_to') && $request->date_to != '')) {
            $function = $function->leftJoin('calendar', function ($join) {
                $join->on('timesheet.year', '=', 'calendar.year');
                $join->on('timesheet.week', '=', 'calendar.week');
            })->whereBetween('calendar.date', [$request->date_from, $request->date_to]);
        }
        if ($request->has('customer') && $request->customer) {
            $function = $function->where('timesheet.customer_id', '=', $request->customer);
        }
        if ($request->has('project') && $request->project) {
            $function = $function->where('timesheet.project_id', '=', $request->project);
        }
        if ($request->has('billable') && $request->billable) {
            $function = $function->where('timeline.is_billable', '=', $request->billable);
        }
        $function = $function->groupBy('assignment.function', 'timesheet.year', 'timesheet.month')->orderBy('timesheet.year', 'desc')->paginate($item);
        $total_hours = 0;
        $total_value = 0;
        foreach ($function as $func) {
            $total_hours += $func->hours;
            $total_value += $func->value;
        }

        $calendar = Calendar::whereDate('date', '<=', Carbon::now()->toDateString())->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $yearmonth[null] = 'All';
        foreach ($calendar as $cal) {
            $month = $cal->month;
            if ($cal->month < 10) {
                $month = '0'.$cal->month;
            }
            $yearmonth[$cal->year.''.$month] = $cal->year.''.$month;
        }

        $parameters = [
            'yearmonth_dropdown' => $yearmonth,
            'customer_drop_down' => Customer::filters()->orderBy('customer_name')->pluck('customer_name', 'id'),
            'project_drop_down' => Project::filters()->orderBy('name')->pluck('name', 'id'),
            'team_drop_down' => Team::filters()->orderBy('team_name')->pluck('team_name', 'id'),
            'company_drop_down' => Company::filters()->orderBy('company_name')->pluck('company_name', 'id'),
            'resource_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->filters()->pluck('full_name', 'id')->prepend('All', null),
            'functions' => $function,
            'total_hours' => $total_hours,
            'total_value' => $total_value,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'summary_report');

        return view('reports.summary_report')->with($parameters);
    }
}
