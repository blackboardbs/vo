<?php

namespace App\Http\Controllers;

use App\Models\CalendarEvents;
use App\Models\Document;
use App\Services\StorageQuota;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class CalendarEventsController extends Controller
{
    public function __construct()
    {
        // $this->middleware('permission:calendar_1|calendar_2|calendar_3|calendar_4|calendar_5', ['only' => ['show', 'index']]);
        // $this->middleware('permission:calendar_2|calendar_3|calendar_4|calendar_5', ['only' => ['create', 'store']]);
        // $this->middleware('permission:calendar_3|calendar_4|calendar_5', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:calendar_4|calendar_5', ['only' => ['destroy']]);
        $this->middleware('auth');
    }

    public function index(): View
    {
        $calendar_events = CalendarEvents::all();
        $calendar_events = CalendarEvents::whereDate('event_date', '>=', date('Y-m-d'))
            ->get();

        return view('calendar.index')->with(['calendar_events' => $calendar_events]);
    }

    public function create(): View
    {
        return view('calendar.create');
    }

    public function store(Request $request, StorageQuota $quota)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255', /*
            'email' => 'required|email|max:255'*/
        ]);

        $flash_message = 'Event added successfully.';

        if ($validator->passes()) {
            $calendar_event = new CalendarEvents;
            $calendar_event->event = $request->input('title');
            $calendar_event->event_date = $request->input('event_date');
            if($request->input('role_ids') != null){
                $calendar_event->role_ids = implode(',', $request->input('role_ids'));
            }
            if($request->input('team_ids') != null){
                $calendar_event->team_ids = implode(',', $request->input('team_ids'));
            }
            if($request->input('user_ids') != null){
                $calendar_event->user_ids = implode(',', $request->input('user_ids'));
            }
            if($request->input('projects_ids') != null){
                $calendar_event->project_ids = implode(',', $request->input('project_ids'));
            }
            $calendar_event->details = $request->input('details');
            $calendar_event->status_id = $request->input('status_id');
            $calendar_event->row_order = '1';
            $calendar_event->creator_id = auth()->id();
            $calendar_event->save();

            if (isset($request->file)) {
                foreach ($request->file as $key => $f) {
                    if (!$quota->isStorageQuotaReached($f->getSize())){
                        $filename = (($request->document_name[$key]) ? ($request->document_name[$key].'.'.$f->getClientOriginalExtension()) : $f->getClientOriginalName().'.'.$f->getClientOriginalExtension());
                        //return $filename;
                        $f->storeAs('events', $filename);
                        //$calendar_event->document_name = $filename;

                        $document = new Document();
                        $document->type_id = null;
                        $document->document_type_id = 9; //Events documents
                        $document->name = $filename;
                        $document->file = $filename;
                        $document->creator_id = auth()->id();
                        $document->owner_id = auth()->id();
                        $document->digisign_status_id = null;
                        $document->reference_id = $calendar_event->id;
                        $document->digisign_approver_user_id = null;
                        $document->save();
                    }else{
                        $flash_message = 'Event added successfully, but files were not uploaded because you are out of storage.';
                    }
                }
            }

            //Mail::to($request->input('email'))->send(new CalendarEventMail($calendar_event->id, $calendar_event->start_date));

            /*return response()->json([
                'success' => 'Event added successfully.',
                'id' => $calendar_event->id,
                'title' => $request->input('title'),
                'start_date' => $request->input('event_date'),
                'end_date' => $request->input('event_date')
            ]);*/

            return redirect()->back()->with([
                'flash_success' => $flash_message,
                'id' => $calendar_event->id,
                'title' => $request->input('title'),
                'start_date' => $request->input('event_date'),
                'end_date' => $request->input('event_date'),
            ]);
        }

        return response()->json(['errors' => $validator->errors(), 'post_data' => $request->all()]);
    }

    public function destroy($id): JsonResponse
    {
        $calendar_event = CalendarEvents::find($id);
        $calendar_event->delete();

        return response()->json(['success' => 'Event deleted successfully.']);
    }
}
