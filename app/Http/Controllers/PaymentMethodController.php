<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\Models\PaymentMethod;
use App\Models\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        // $payment_method = PaymentMethod::sortable('description', 'asc')->paginate($item);

        $payment_method = PaymentMethod::with(['statusd:id,description'])->sortable('description', 'asc')->where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_method = PaymentMethod::with(['statusd:id,description'])->where('status', $request->input('status_filter'));
            }else{
                $payment_method = PaymentMethod::with(['statusd:id,description'])->sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_method = $payment_method->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_method = $payment_method->paginate($item);

        $parameters = [
            'payment_method' => $payment_method,
        ];

        return View('master_data.payment_method.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = PaymentMethod::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.payment_method.create')->with($parameters);
    }

    public function store(StorePaymentMethodRequest $request): RedirectResponse
    {
        $payment_method = new PaymentMethod();
        $payment_method->description = $request->input('description');
        $payment_method->status = $request->input('status');
        $payment_method->creator_id = auth()->id();
        $payment_method->save();

        return redirect(route('payment_method.index'))->with('flash_success', 'Master Data Payment Method captured successfully');
    }

    public function show($payment_method_id): View
    {
        $parameters = [
            'payment_method' => PaymentMethod::where('id', '=', $payment_method_id)->get(),
        ];

        return View('master_data.payment_method.show')->with($parameters);
    }

    public function edit($payment_method_id): View
    {
        $autocomplete_elements = PaymentMethod::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'payment_method' => PaymentMethod::where('id', '=', $payment_method_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('master_data.payment_method.edit')->with($parameters);
    }

    public function update(UpdatePaymentMethodRequest $request, $payment_method_id): RedirectResponse
    {
        $payment_method = PaymentMethod::find($payment_method_id);
        $payment_method->description = $request->input('description');
        $payment_method->status = $request->input('status');
        $payment_method->creator_id = auth()->id();
        $payment_method->save();

        return redirect(route('payment_method.index'))->with('flash_success', 'Master Data Payment Method saved successfully');
    }

    public function destroy($payment_method_id): RedirectResponse
    {
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        // PaymentMethod::destroy($payment_method_id);
        $item = PaymentMethod::find($payment_method_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('invoice_status.index')->with('success', 'Master Data Payment Method suspended successfully');
    }
}
