<?php

namespace App\Http\Controllers;

use App\Models\Board;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BoardCardController extends Controller
{
    public function create(Request $request, $id): Response
    {
        $board = Board::findOrFail($id);

        $data = $request->validate(['cards' => 'required']);

        $updated = $board->update($data);

        if ($updated) {
            event(new \App\Events\BoardCardCreated($board));
        }

        return response(['status' => $updated ? 'success' : 'error']);
    }
}
