<?php

namespace App\Http\Controllers;

use App\Models\Goal;
use App\Models\Sprint;
use Illuminate\Http\Request;

class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($sprint_id)
    {
        $sprint = Sprint::find($sprint_id);
        $goals = Goal::where('sprint_id', $sprint_id)->get();
        return response()->json(['sprint'=>$sprint,'goals'=>$goals]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $sprint_id)
    {
        $goal = new Goal();
        $goal->name = $request->name;
        $goal->sprint_id = $sprint_id;
        $goal->save();

        return response()->json(['goal' => $goal]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Goal $goal)
    {
        $goal->name = $request->name;
        $goal->save();

        return response()->json(['message' => "Edit was successful"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Goal $goal)
    {
        $goal->delete();

        return response()->json(['message' => 'Goal was deleted successfully']);
    }
}
