<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentTermsRequest;
use App\Http\Requests\UpdatePaymentTermsRequest;
use App\Models\Central\PaymentTerm;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentTermController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $payment_terms = PaymentTerm::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_terms = PaymentTerm::where('status_id', $request->input('status_filter'));
            }else{
                $payment_terms = PaymentTerm::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_terms = $payment_terms->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_terms = $payment_terms->paginate($item);

        $parameters = [
            'payment_terms' => $payment_terms,
        ];

        return View('admin.payment_terms.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = PaymentTerm::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.payment_terms.create', $parameters);
    }

    public function store(StorePaymentTermsRequest $request): RedirectResponse
    {
        $payment_terms = new PaymentTerm;
        $payment_terms->description = $request->description;
        $payment_terms->status_id = $request->status;
        $payment_terms->save();

        return redirect(route('admin.paymentterms.index'))->with('flash_success', 'Master Data Payment Term captured successfully');
    }

    public function show($id): View
    {
        $parameters = [
            'payment_term' => PaymentTerm::find($id),
        ];

        return view('admin.payment_terms.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = PaymentTerm::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::select('description', 'id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'payment_term' => PaymentTerm::find($id),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.payment_terms.edit')->with($parameters);
    }

    public function update(UpdatePaymentTermsRequest $request, $id): RedirectResponse
    {
        $payment_terms = PaymentTerm::find($id);
        $payment_terms->description = $request->description;
        $payment_terms->status_id = $request->status;
        $payment_terms->save();

        return redirect(route('admin.paymentterms.index'))->with('flash_success', 'Master Data Payment Term edited successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $item = PaymentTerm::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('admin.paymentterms.index'))->with('flash_success', 'Master Data Payment Term suspended successfully');
    }
}
