<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTermsRequest;
use App\Http\Requests\UpdateTermsRequest;
use App\Models\Central\Status;
use App\Models\Central\Term;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TermController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $terms = Term::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $terms = Term::where('status', $request->input('status_filter'));
            }else{
                $terms = Term::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $terms = $terms->where('term', 'like', '%'.$request->input('q').'%');
        }

        $terms = $terms->paginate($item);

        $parameters = [
            'terms' => $terms,
        ];

        return View('admin.terms.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('admin.terms.create')->with($parameters);
    }

    public function store(StoreTermsRequest $request): RedirectResponse
    {
        $terms = new Term();
        $terms->term = $request->input('term');
        $terms->row_order = $request->input('row_order');
        $terms->status = $request->input('status');
        $terms->creator_id = auth()->id()??1;
        $terms->save();

        return redirect(route('admin.term.index'))->with('flash_success', 'Master Term captured successfully');
    }

    public function edit($termid): View
    {
        $parameters = [
            'terms' => Term::where('id', '=', $termid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('admin.terms.edit')->with($parameters);
    }

    public function update(UpdateTermsRequest $request, $termid): RedirectResponse
    {
        $terms = Term::find($termid);
        $terms->term = $request->input('term');
        $terms->row_order = $request->input('row_order');
        $terms->status = $request->input('status');
        $terms->creator_id = auth()->id()??1;
        $terms->save();

        return redirect(route('admin.term.index'))->with('flash_success', 'Master Term saved successfully');
    }

    public function show($termid): View
    {
        $parameters = [
            'terms' => Term::where('id', '=', $termid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('admin.terms.show')->with($parameters);
    }

    public function destroy($id): RedirectResponse
    {
        $item = Term::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.term.index')->with('success', 'Master Data Term suspended successfully');
    }
}
