<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRelationshipRequest;
use App\Http\Requests\UpdateRelationshipRequest;
use App\Models\Central\Relationship;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RelationshipController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $relationship = Relationship::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $relationship = Relationship::where('status', $request->input('status_filter'));
            }else{
                $relationship = Relationship::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $relationship = $relationship->where('description', 'like', '%'.$request->input('q').'%');
        }

        $relationship = $relationship->paginate($item);

        $parameters = [
            'relationship' => $relationship,
        ];

        return View('admin.relationship.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Relationship::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.relationship.create')->with($parameters);
    }

    public function store(StoreRelationshipRequest $request): RedirectResponse
    {
        $status = new Relationship();
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id()??1;
        $status->save();

        return redirect(route('admin.relationship.index'))->with('flash_success', 'Master Data Relationship captured successfully');
    }

    public function show($relationship_id): View
    {
        $parameters = [
            'relationship' => Relationship::where('id', '=', $relationship_id)->get(),
        ];

        return View('admin.relationship.show')->with($parameters);
    }

    public function edit($relationship_id): View
    {
        $autocomplete_elements = Relationship::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'relationship' => Relationship::where('id', '=', $relationship_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.relationship.edit')->with($parameters);
    }

    public function update(UpdateRelationshipRequest $request, $relationship_id): RedirectResponse
    {
        $status = Relationship::find($relationship_id);
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->creator_id = auth()->id();
        $status->save();

        return redirect(route('admin.relationship.index'))->with('flash_success', 'Master Data Relationship saved successfully');
    }

    public function destroy($relationship_id): RedirectResponse
    {
        $item = Relationship::find($relationship_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.relationship.index')->with('success', 'Master Data Relationship suspended successfully');
    }
}
