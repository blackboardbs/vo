<?php

namespace App\Http\Controllers\Central;

use App\Enum\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\TemplateVariableRequest;
use App\Models\Central\TemplateType;
use App\Models\Central\TemplateVariable;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TemplateVariableController extends Controller
{
    public function index(): View
    {
        $variables = TemplateVariable::with(['templateType:id,name', 'status:id,description'])
            ->when(request()->status_filter, fn ($variable) => $variable->where('status_id', request()->status_filter))
            ->latest()
            ->paginate()
            ->appends(request()->query());

        return view('master_data.templatevariable.index')->with(['variables' => $variables]);
    }

    public function create(): View
    {
        return view('master_data.templatevariable.create')->with([
            'template_type_dropdown' => TemplateType::where('status_id', Status::ACTIVE->value)->pluck('name', 'id'),
        ]);
    }

    public function store(TemplateVariableRequest $request): RedirectResponse
    {
        TemplateVariable::create($request->all());
        return redirect(route('variable.index'))->with(['flash_success' => "Template Variable added successfully."]);
    }

    public function show(TemplateVariable $variable): View
    {
        return view('master_data.templatevariable.show')->with(['variable' => $variable]);
    }

    public function edit(TemplateVariable $variable): View
    {
        return view('master_data.templatevariable.edit')->with([
            'variable' => $variable,
            'template_type_dropdown' => TemplateType::where('status_id', Status::ACTIVE->value)->pluck('name', 'id'),
        ]);
    }

    public function update(TemplateVariableRequest $request, TemplateVariable $variable): RedirectResponse
    {
        $variable->update($request->all());

        return redirect(route('variable.show', $variable))->with(['flash_success' => "Template Variable updated successfully."]);
    }

    public function destroy(TemplateVariable $variable): RedirectResponse
    {
        $variable->delete();

        return redirect(route('variable.index'))->with(['flash_success' => "Template Variable deleted successfully."]);
    }
}
