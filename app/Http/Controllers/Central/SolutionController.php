<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSolutionRequest;
use App\Http\Requests\UpdateSolutionRequest;
use App\Models\Central\Solution;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SolutionController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $solution = Solution::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $solution = Solution::where('status', $request->input('status_filter'));
            }else{
                $solution = Solution::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $solution = $solution->where('description', 'like', '%'.$request->input('q').'%');
        }

        $solution = $solution->paginate($item);

        $parameters = [
            'solution' => $solution,
        ];

        return View('admin.solution.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Solution::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.solution.create')->with($parameters);
    }

    public function store(StoreSolutionRequest $request): RedirectResponse
    {
        $solution = new Solution();
        $solution->description = $request->input('description');
        $solution->version = $request->input('version');
        $solution->status = $request->input('status');
        $solution->creator_id = auth()->id()??1;
        $solution->save();

        return redirect(route('admin.solution.index'))->with('flash_success', 'Master Data Solution captured successfully');
    }

    public function show($solution_id): View
    {
        $parameters = [
            'solution' => Solution::where('id', '=', $solution_id)->get(),
        ];

        return View('admin.solution.show')->with($parameters);
    }

    public function edit($solution_id): View
    {
        $autocomplete_elements = Solution::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'solution' => Solution::where('id', '=', $solution_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.solution.edit')->with($parameters);
    }

    public function update(UpdateSolutionRequest $request, $solution_id): RedirectResponse
    {
        $solution = Solution::find($solution_id);
        $solution->description = $request->input('description');
        $solution->version = $request->input('version');
        $solution->status = $request->input('status');
        $solution->creator_id = auth()->id()??1;
        $solution->save();

        return redirect(route('admin.solution.index'))->with('flash_success', 'Master Data Solution saved successfully');
    }

    public function destroy($solution_id): RedirectResponse
    {
        $item = Solution::find($solution_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.solution.index')->with('success', 'Master Data Solution suspended successfully');
    }
}
