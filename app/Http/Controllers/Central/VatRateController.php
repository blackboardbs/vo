<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVatRateRequest;
use App\Http\Requests\UpdateVatRateRequest;
use App\Models\Central\Status;
use App\Models\Central\VatRate;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VatRateController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $vat_rate = VatRate::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $vat_rate = VatRate::where('status', $request->input('status_filter'));
            }else{
                $vat_rate = VatRate::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $vat_rate = $vat_rate->where('description', 'like', '%'.$request->input('q').'%');
        }

        $vat_rate = $vat_rate->paginate($item);

        $parameters = [
            'vat_rate' => $vat_rate,
        ];

        return View('admin.vat_rate.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = VatRate::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.vat_rate.create')->with($parameters);
    }

    public function store(StoreVatRateRequest $request): RedirectResponse
    {
        $vat_rate = new VatRate();
        $vat_rate->description = $request->input('description');
        $vat_rate->vat_rate = $request->input('vat_rate');
        $vat_rate->vat_code = $request->input('vat_code');
        $vat_rate->start_date = $request->input('start_date');
        $vat_rate->end_date = $request->input('end_date');
        $vat_rate->status = $request->input('status');
        $vat_rate->creator_id = auth()->id()??1;
        $vat_rate->save();

        return redirect(route('admin.vatrate.index'))->with('flash_success', 'Master Data Vat Rate captured successfully');
    }

    public function show($vat_rate_id): View
    {
        $parameters = [
            'vat_rate' => VatRate::where('id', '=', $vat_rate_id)->get(),
        ];

        return View('admin.vat_rate.show')->with($parameters);
    }

    public function edit($vat_rate_id): View
    {
        $autocomplete_elements = VatRate::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'vat_rate' => VatRate::where('id', '=', $vat_rate_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.vat_rate.edit')->with($parameters);
    }

    public function update(UpdateVatRateRequest $request, $vat_rate_id): RedirectResponse
    {
        $vat_rate = VatRate::find($vat_rate_id);
        $vat_rate->description = $request->input('description');
        $vat_rate->vat_rate = $request->input('vat_rate');
        $vat_rate->vat_code = $request->input('vat_code');
        $vat_rate->start_date = $request->input('start_date');
        $vat_rate->end_date = $request->input('end_date');
        $vat_rate->status = $request->input('status');
        $vat_rate->creator_id = auth()->id()??1;
        $vat_rate->save();

        return redirect(route('admin.vatrate.index'))->with('flash_success', 'Master Data Vat Rate saved successfully');
    }

    public function destroy($vat_rate_id): RedirectResponse
    {
        $item = VatRate::find($vat_rate_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.vatrate.index')->with('success', 'Master Data Vat Rate suspended successfully');
    }

    public function getVat(Request $request){

        $vat_rate = VatRate::where('vat_code',$request->vat_code)->where('end_date','>',Carbon::parse(now())->format('Y-m-d'))->where('status','1')->first();

        return response()->json(['vat'=>$vat_rate]);
    }
}
