<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Models\Central\ExpenseType;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ExpenseTypeController extends Controller
{
    public function index(Request $request){
        $item = $request->input('s') ?? 15;

        $exp_type = ExpenseType::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $exp_type = ExpenseType::where('status_id', $request->input('status_filter'));
            }else{
                $exp_type = ExpenseType::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $exp_type = $exp_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $exp_type = $exp_type->paginate($item);

        $paginate = [
            'expense_type' => $exp_type,
        ];

        return view('admin.expense_type.index')->with($paginate);
    }

    public function create(): View
    {
        return view('admin.expense_type.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    public function store(ExpenseType $expenseType): RedirectResponse
    {
        $expenseType->description = \request()->description;
        $expenseType->status_id = \request()->status;
        $expenseType->save();

        return redirect()->route('admin.expensetype.index')->with('flush_success', 'Expense Type added successfully');
    }

    public function edit(ExpenseType $expensetype): View
    {
        $parameters = [
            'expense_type' => $expensetype,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ];

        return view('admin.expense_type.edit')->with($parameters);
    }

    public function update(ExpenseType $expense_type): RedirectResponse
    {
        $expense_type->description = \request()->description;
        $expense_type->status_id = \request()->status;
        $expense_type->save();

        return redirect()->route('admin.expensetype.show', $expense_type)->with('flash_success', 'Expense type updated successfully');
    }

    public function show(ExpenseType $expensetype): View
    {
        return view('admin.expense_type.show')->with(['expense_type' => $expensetype]);
    }

    public function destroy($expense_type): RedirectResponse
    {
        $item = ExpenseType::find($expense_type);
        $item->status_id = 2;
        $item->save();

        return redirect()->back()->with('flash_success', 'Expense type suspended successfully');
    }
}
