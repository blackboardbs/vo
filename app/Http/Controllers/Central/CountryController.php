<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Models\Central\Country;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CountryController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $country = Country::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $country = Country::where('status', $request->input('status_filter'));
            }else{
                $country = Country::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $country = $country->where('name', 'like', '%'.$request->input('q').'%');
        }

        $country = $country->paginate($item);

        $parameters = [
            'country' => $country,
        ];

        return View('admin.country.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Country::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.country.create')->with($parameters);
    }

    public function store(StoreCountryRequest $request): RedirectResponse
    {
        $country = new Country();
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        $country->status = $request->input('status');
        $country->creator_id = auth()->id()??1;
        $country->save();

        return redirect(route('admin.country.index'))->with('flash_success', 'Master Country captured successfully');
    }

    public function show($countryid): View
    {
        $parameters = [
            'country' => Country::where('id', '=', $countryid)->get(),
        ];

        return View('admin.country.show')->with($parameters);
    }

    public function edit($countryid): View
    {
        $autocomplete_elements = Country::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'country' => Country::where('id', '=', $countryid)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.country.edit')->with($parameters);
    }

    public function update(UpdateCountryRequest $request, $countryid): RedirectResponse
    {
        $country = Country::find($countryid);
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        $country->status = $request->input('status');
        $country->creator_id = auth()->id()??1;
        $country->save();

        return redirect(route('admin.country.index'))->with('flash_success', 'Master Country saved successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $item = Country::find($id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.country.index')->with('success', 'Master Data Country suspended successfully');
    }
}
