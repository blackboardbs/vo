<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Models\Central\ProjectStatus;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProjectStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $project_status = ProjectStatus::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $project_status = ProjectStatus::where('status_id', $request->input('status_filter'));
            }else{
                $project_status = ProjectStatus::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $project_status = $project_status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $project_status = $project_status->paginate($item);

        $parameters = [
            'project_status' => $project_status,
        ];

        return View('admin.project_status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ProjectStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status','=',1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return View('admin.project_status.create')->with($parameters);
    }

    public function store(Request $request): RedirectResponse
    {
        $project_status = new ProjectStatus();
        $project_status->description = $request->input('description');
        $project_status->status_id = $request->input('status_id');
        $project_status->save();

        return redirect(route('admin.projectstatus.index'))->with('flash_success', 'Master Data Project Status captured successfully');
    }

    public function show($project_status_id): View
    {
        $parameters = [
            'project_status' => ProjectStatus::where('id','=',$project_status_id)->get()
        ];

        return View('admin.project_status.show')->with($parameters);
    }

    public function edit($project_status_id): View
    {
        $autocomplete_elements = ProjectStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'project_status' => ProjectStatus::where('id','=',$project_status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status','=',1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return View('admin.project_status.edit')->with($parameters);
    }

    public function update(Request $request, $project_status_id): RedirectResponse
    {
        $project_status = ProjectStatus::find($project_status_id);
        $project_status->description = $request->input('description');
        $project_status->status_id = $request->input('status');
        $project_status->save();

        return redirect(route('admin.projectstatus.index'))->with('flash_success', 'Master Data Project Status Updated successfully');
    }

    public function destroy($project_status_id): RedirectResponse
    {
        $item = ProjectStatus::find($project_status_id);
        $item->status_id = 2;
        $item->save();


        return redirect(route('admin.projectstatus.index'))->with('flash_success', 'Master Data Project Status suspended successfully');
    }
}
