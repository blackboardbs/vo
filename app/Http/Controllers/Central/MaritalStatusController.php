<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMaritalStatusRequest;
use App\Http\Requests\UpdateMaritalStatusRequest;
use App\Models\Central\MaritalStatus;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MaritalStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $marital_status = MaritalStatus::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $marital_status = MaritalStatus::where('status', $request->input('status_filter'));
            }else{
                $marital_status = MaritalStatus::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $marital_status = $marital_status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $marital_status = $marital_status->paginate($item);

        $parameters = [
            'marital_status' => $marital_status,
        ];

        return View('admin.marital_status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = MaritalStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.marital_status.create')->with($parameters);
    }

    public function store(StoreMaritalStatusRequest $request): RedirectResponse
    {
        $marital_status = new MaritalStatus();
        $marital_status->description = $request->input('description');
        $marital_status->status = $request->input('status');
        $marital_status->creator_id = auth()->id()??1;
        $marital_status->save();

        return redirect(route('admin.maritalstatus.index'))->with('flash_success', 'Master Data Marital Status captured successfully');
    }

    public function show($marital_status_id): View
    {
        $parameters = [
            'marital_status' => MaritalStatus::where('id', '=', $marital_status_id)->get(),
        ];

        return View('admin.marital_status.show')->with($parameters);
    }

    public function edit($marital_status_id): View
    {
        $autocomplete_elements = MaritalStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'marital_status' => MaritalStatus::where('id', '=', $marital_status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.marital_status.edit')->with($parameters);
    }

    public function update(UpdateMaritalStatusRequest $request, $marital_status_id): RedirectResponse
    {
        $marital_status = MaritalStatus::find($marital_status_id);
        $marital_status->description = $request->input('description');
        $marital_status->status = $request->input('status');
        $marital_status->creator_id = auth()->id()??1;
        $marital_status->save();

        return redirect(route('admin.maritalstatus.index'))->with('flash_success', 'Master Data Marital Status saved successfully');
    }

    public function destroy($marital_status_id): RedirectResponse
    {
        $item = MaritalStatus::find($marital_status_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.maritalstatus.index')->with('success', 'Master Data Marital Status suspended successfully');
    }
}
