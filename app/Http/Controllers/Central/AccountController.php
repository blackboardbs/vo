<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\AccountType;
use App\Models\Central\Account;
use App\Models\Central\Status;
use App\Models\Config;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AccountController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $account = Account::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $account = Account::where('status', $request->input('status_filter'));
            }else{
                $account = Account::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $account = $account->where('description', 'like', '%'.$request->input('q').'%');
        }

        $account = $account->paginate($item);

        $parameters = [
            'account' => $account,
            'config' => Config::first()
        ];

        return View('admin.account.index', $parameters);
    }

    public function create(): View
    {
        $accounts = Account::orderBy('description')->where('description', '!=', '')->where('description', '!=', null)->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => AccountType::orderBy('order')->pluck('description', 'order')->prepend('Account Type', '0'),
            'accounts' => $accounts,
        ];

        return View('admin.account.create')->with($parameters);
    }

    public function store(StoreAccountRequest $request): RedirectResponse
    {
        $account = new Account();
        $account->description = $request->input('description');
        $account->status = $request->input('status');
        $account->account_type_id = $request->input('account_type');
        $account->account_group = $request->input('account_group');
        $account->creator_id = 1;
        $account->save();

        return redirect(route('admin.account.index'))->with('flash_success', 'Master Data Account captured successfully');
    }

    public function show($account_id): View
    {
        $parameters = [
            'account' => Account::where('id', '=', $account_id)->get(),
        ];

        return View('admin.account.show')->with($parameters);
    }

    public function edit(Account $account): View
    {
        $accounts = Account::orderBy('description')->where('description', '!=', '')->where('description', '!=', null)->get();

        $parameters = [
            'account' => $account,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'acct_dropdown' => AccountType::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('AccountType', '0'),
            'accounts' => $accounts,
        ];

        return View('admin.account.edit')->with($parameters);
    }

    public function update(UpdateAccountRequest $request, Account $account): RedirectResponse
    {
        $account->description = $request->input('description');
        $account->status = $request->input('status');
        $account->account_type_id = $request->input('account_type');
        $account->account_group = $request->input('account_group');
        //$account->creator_id = auth()->id();
        $account->save();

        return redirect(route('admin.account.index'))->with('flash_success', 'Master Data Account saved successfully');
    }

    public function destroy(Account $account): RedirectResponse
    {

        $account->status = 2;
        $account->save();

        return redirect()->route('admin.account.index')->with('success', 'Master Data Account suspended successfully');
    }
}
