<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfessionRequest;
use App\Models\Central\Profession;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProfessionController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $professions = Profession::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $professions = Profession::where('status_id', $request->input('status_filter'));
            }else{
                $professions = Profession::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $professions = $professions->where('name', 'like', '%'.$request->input('q').'%');
        }

        $professions = $professions->paginate($item);

        $parameters = [
            'professions' => $professions,
        ];

        return view('admin.profession.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $autocomplete_elements = Profession::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $parameters = [
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.profession.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProfessionRequest $request): RedirectResponse
    {
        $profession = new Profession();
        $profession->name = $request->input('name');
        $profession->creator_id = Auth()->id();
        $profession->status_id = $request->input('status_id');
        $profession->save();

        return redirect(route('admin.profession.index'))->with('flash_success', 'Profession saved successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Profession $profession): View
    {
        $profession = Profession::find($profession->id);
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $parameters = [
            'profession' => $profession,
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.profession.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Profession $profession): View
    {
        $autocomplete_elements = Profession::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $profession = Profession::find($profession->id);

        $parameters = [
            'profession' => $profession,
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.profession.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProfessionRequest $request, Profession $profession): RedirectResponse
    {
        $profession = Profession::find($profession->id);
        $profession->name = $request->input('name');
        $profession->creator_id = Auth()->id();
        $profession->status_id = $request->input('status_id');
        $profession->save();

        return redirect(route('admin.profession.index'))->with('flash_success', 'Profession updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $profession): RedirectResponse
    {
        $item = Profession::find($profession);
        $item->status_id = 2;
        $item->save();

        return redirect(route('admin.profession.index'))->with('flash_success', 'Profession suspended successfully.');
    }
}
