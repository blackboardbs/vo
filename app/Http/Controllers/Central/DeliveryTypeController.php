<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryTypeRequest;
use App\Models\Central\DeliveryType;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DeliveryTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $delivery_type = DeliveryType::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $delivery_type = DeliveryType::where('status_id', $request->input('status_filter'));
            }else{
                $delivery_type = DeliveryType::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $delivery_type = $delivery_type->where('name', 'like', '%'.$request->input('q').'%');
        }

        $delivery_type = $delivery_type->paginate($item);

        $parameters = [
            'delivery_type' => $delivery_type,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id')
        ];


        return view('admin.delivery_type.index')->with($parameters);
    }


    public function create()
    {

    }

    public function store(Request $request): RedirectResponse
    {
        $delivery_type = new DeliveryType();
        $delivery_type->name = $request->name;
        $delivery_type->status_id = $request->status;
        $delivery_type->creator_id = auth()->id();
        $delivery_type->save();

        return redirect()->back();
    }

    public function show($id): View
    {
        return view('admin.delivery_type.show')->with(['type' => DeliveryType::find($id)]);
    }

    public function edit($id): View
    {
        $delivery_type = DeliveryType::find($id, ['name', 'status_id', 'id']);
        $status_dropdown = Status::pluck('description', 'id')->prepend('Select Status', '0');
        $parameters = [
            'delivery_type' => $delivery_type,
            'status_dropdown' => $status_dropdown
        ];

        return view('admin.delivery_type.edit')->with($parameters);
    }

    public function update(DeliveryTypeRequest $request, $id): RedirectResponse
    {
        $delivery_type = DeliveryType::find($id);
        $delivery_type->name = $request->name;
        $delivery_type->status_id = $request->status;
        $delivery_type->save();

        return redirect(route('admin.deliverytype.index'))->with('flash_success', 'Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        $item = DeliveryType::find($id);
        $item->status_id = 2;
        $item->save();
        return redirect()->back()->with('flash_success', 'suspended successfully');
    }
}
