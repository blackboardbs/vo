<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAssignmentStandardCostRequest;
use App\Http\Requests\UpdateAssignmentStandardCostRequest;
use App\Models\Central\AssignmentStandardCost;
use App\Models\Central\Status;
use App\Models\Module;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssignmentStandardCostController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('r') ?? 15;

        $standard_cost = AssignmentStandardCost::where('status_id', \App\Enum\Status::ACTIVE->value);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $standard_cost = AssignmentStandardCost::where('status_id', $request->input('status_filter'));
            }else{
                $standard_cost = AssignmentStandardCost::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $standard_cost = $standard_cost->where('description', 'like', '%'.$request->input('q').'%');
        }

        $standard_cost = $standard_cost->paginate($item);

        return view('admin.assignment_standard_cost.index')->with(['standard_cost' => $standard_cost]);
    }

    public function create()
    {
        $autocomplete_elements = AssignmentStandardCost::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status' => Status::where('status', '=', \App\Enum\Status::ACTIVE->value)->pluck('description', 'id'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.assignment_standard_cost.create')->with($parameters);
    }

    public function store(StoreAssignmentStandardCostRequest $request): RedirectResponse
    {
        $standard_cost = new AssignmentStandardCost();
        $standard_cost->description = $request->description;
        $standard_cost->standard_cost_rate = $request->standard_cost_rate;
        $standard_cost->min_rate = $request->min_rate;
        $standard_cost->max_rate = $request->max_rate;
        $standard_cost->status_id = $request->status;
        $standard_cost->save();

        return redirect()->route('admin.assignmentstandardcost.show', $standard_cost)->with('flash_success', 'Assignment Standard Cost Rate Captured Successfully');
    }

    public function show($id)
    {
        $standard_cost = AssignmentStandardCost::find($id);

        return view('admin.assignment_standard_cost.show')->with(['standard_cost' => $standard_cost]);
    }

    public function edit($id)
    {
        $autocomplete_elements = AssignmentStandardCost::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $standard_cost = AssignmentStandardCost::find($id);
        $parameters = [
            'status' => Status::where('status', '=', \App\Enum\Status::ACTIVE->value)->pluck('description', 'id'),
            'standard_cost' => $standard_cost,
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.assignment_standard_cost.edit')->with($parameters);
    }

    public function update(UpdateAssignmentStandardCostRequest $request, $id): RedirectResponse
    {
        $standard_cost = AssignmentStandardCost::find($id);
        $standard_cost->description = $request->description;
        $standard_cost->standard_cost_rate = $request->standard_cost_rate;
        $standard_cost->min_rate = $request->min_rate;
        $standard_cost->max_rate = $request->max_rate;
        $standard_cost->status_id = $request->status;
        $standard_cost->save();

        return redirect()->route('admin.assignmentstandardcost.show', $standard_cost)->with('flash_success', 'Assignment Standard Cost Rate Updated Successfully');
    }

    public function destroy($id): RedirectResponse
    {
        // AssignmentStandardCost::destroy($id);
        $item = AssignmentStandardCost::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.assignmentstandardcost.index')->with('flash_warning', 'Assignment Standard Cost Rate suspended successfully');
    }
}
