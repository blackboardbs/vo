<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTemplateTypeRequest;
use App\Http\Requests\UpdateTemplateTypeRequest;
use App\Models\Central\Status;
use App\Models\Central\TemplateType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TemplateTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $template_types = TemplateType::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $template_types = TemplateType::where('status_id', $request->input('status_filter'));
            }else{
                $template_types = TemplateType::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $template_types = $template_types->where('name', 'like', '%'.$request->input('q').'%');
        }

        $template_types = $template_types->paginate($item);

        $parameters = [
            'template_types' => $template_types,
        ];

        return View('admin.template_type.index', $parameters);
    }

    public function create(): View
    {
        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('name', 'id')->prepend('Status', '0'),
        ];

        return View('admin.template_type.create')->with($parameters);
    }

    public function store(StoreTemplateTypeRequest $request): RedirectResponse
    {
        $template_type = new TemplateType();
        $template_type->name = $request->input('name');
        $template_type->status_id = $request->input('status');
        $template_type->creator_id = auth()->id()??1;
        $template_type->save();

        return redirect(route('admin.template.index'))->with('flash_success', 'Master Data Template Type captured successfully');
    }

    public function show($template_type_id): View
    {
        $parameters = [
            'template_type' => TemplateType::find($template_type_id),
        ];

        return view('admin.template_type.show')->with($parameters);
    }

    public function edit($template_type_id): View
    {
        $parameters = [
            'template_type' => TemplateType::find($template_type_id),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('name', 'id')->prepend('Status', '0'),
        ];

        return View('admin.template_type.edit')->with($parameters);
    }

    public function update(UpdateTemplateTypeRequest $request, $template_type_id): RedirectResponse
    {
        $template_type = TemplateType::find($template_type_id);
        $template_type->name = $request->input('name');
        $template_type->status_id = $request->input('status');
        $template_type->creator_id = auth()->id()??1;
        $template_type->save();

        return redirect(route('admin.template.index'))->with('flash_success', 'Master Data Template Type Update successfully');
    }

    public function destroy($template_type_id): RedirectResponse
    {
        $item = TemplateType::find($template_type_id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('admin.template.index'))->with('flash_success', 'Master Data Template Type suspended successfully');
    }
}
