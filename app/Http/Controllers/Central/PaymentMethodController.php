<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\Models\Central\PaymentMethod;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentMethodController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $payment_method = PaymentMethod::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_method = PaymentMethod::where('status', $request->input('status_filter'));
            }else{
                $payment_method = PaymentMethod::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_method = $payment_method->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_method = $payment_method->paginate($item);

        $parameters = [
            'payment_method' => $payment_method,
        ];

        return View('admin.payment_method.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = PaymentMethod::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.payment_method.create')->with($parameters);
    }

    public function store(StorePaymentMethodRequest $request): RedirectResponse
    {
        $payment_method = new PaymentMethod();
        $payment_method->description = $request->input('description');
        $payment_method->status = $request->input('status');
        $payment_method->creator_id = auth()->id()??1;
        $payment_method->save();

        return redirect(route('admin.paymentmethod.index'))->with('flash_success', 'Master Data Payment Method captured successfully');
    }

    public function show($payment_method_id): View
    {
        $parameters = [
            'payment_method' => PaymentMethod::where('id', '=', $payment_method_id)->get(),
        ];

        return View('admin.payment_method.show')->with($parameters);
    }

    public function edit($payment_method_id): View
    {
        $autocomplete_elements = PaymentMethod::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'payment_method' => PaymentMethod::where('id', '=', $payment_method_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.payment_method.edit')->with($parameters);
    }

    public function update(UpdatePaymentMethodRequest $request, $payment_method_id): RedirectResponse
    {
        $payment_method = PaymentMethod::find($payment_method_id);
        $payment_method->description = $request->input('description');
        $payment_method->status = $request->input('status');
        $payment_method->creator_id = auth()->id()??1;
        $payment_method->save();

        return redirect(route('admin.paymentmethod.index'))->with('flash_success', 'Master Data Payment Method saved successfully');
    }

    public function destroy($payment_method_id): RedirectResponse
    {
        $item = PaymentMethod::find($payment_method_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.invoicestatus.index')->with('success', 'Master Data Payment Method suspended successfully');
    }
}
