<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Models\Central\PaymentBase;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PaymentBaseController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $payment_base = PaymentBase::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $payment_base = PaymentBase::where('status_id', $request->input('status_filter'));
            }else{
                $payment_base = PaymentBase::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $payment_base = $payment_base->where('description', 'like', '%'.$request->input('q').'%');
        }

        $payment_base = $payment_base->paginate($item);

        return view('admin.payment_base.index')->with(['payment_base' => $payment_base]);
    }

    public function create(): View
    {
        return view('admin.payment_base.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    /**
     * TODO: Create a request for validation
     */
    public function store(Request $request): RedirectResponse
    {
        $payment_base = new PaymentBase();
        $this->data($request, $payment_base);

        return redirect()->route('admin.paymentbase.index')->with('flash_success', 'Payment base created successfully.');
    }

    public function show(PaymentBase $paymentbase): View
    {
        return view('admin.payment_base.show')->with(['payment_base' => $paymentbase]);
    }

    public function edit(PaymentBase $paymentbase): View
    {
        return view('admin.payment_base.edit')
            ->with(
                [
                    'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
                    'payment_base' => $paymentbase,
                ]
            );
    }

    /**
     * TODO: Create a request for validation
     */
    public function update(Request $request, PaymentBase $paymentbase): RedirectResponse
    {
        $this->data($request, $paymentbase);

        return redirect()->route('admin.paymentbase.index')->with('flash_success', 'Payment base updated successfully');
    }

    public function destroy($paymentbase): RedirectResponse
    {
        $item = PaymentBase::find($paymentbase);
        $item->status_id = 2;
        $item->save();


        return redirect()->route('admin.paymentbase.index')->with('flash_success', 'Payment base suspended successfully');
    }

    private function data($request, $paymentBase)
    {
        $paymentBase->description = $request->description;
        $paymentBase->status_id = $request->status;
        $paymentBase->save();

        return $paymentBase;
    }
}
