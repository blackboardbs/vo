<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStatusRequest;
use App\Http\Requests\UpdateStatusRequest;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $status = Status::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $status = Status::where('status', $request->input('status_filter'));
            }else{
                $status = Status::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $status = $status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $status = $status->paginate($item);

        $parameters = [
            'status' => $status,
            //'status_description' => Status::where('status', '=', $status->status)->pluck('description')
        ];

        return View('admin.status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Status::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.status.create')->with($parameters);
    }

    public function store(StoreStatusRequest $request): RedirectResponse
    {
        $status = new Status();
        $status->description = $request->input('description');
        $status->status = $request->input('status_id');
        $status->order = Status::max('order') > 1 ? Status::max('order') + 1 : 1; //++Status::last()->order;
        $status->creator_id = auth()->id()??1;
        $status->save();

        return redirect(route('admin.status.index'))->with('flash_success', 'Master Data Status captured successfully');
    }

    public function show($status_id): View
    {
        $parameters = [
            'status' => Status::where('id', '=', $status_id)->get(),
        ];

        return View('admin.status.show')->with($parameters);
    }

    public function edit($status_id): View
    {
        $autocomplete_elements = Status::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();

        $parameters = [
            'status' => Status::where('id', '=', $status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.status.edit')->with($parameters);
    }

    public function update(UpdateStatusRequest $request, $status_id): RedirectResponse
    {
        $status = Status::find($status_id);
        $status->description = $request->input('description');
        $status->status = $request->input('status');
        $status->order = $status->order;
        $status->creator_id = auth()->id()??1;
        $status->save();

        return redirect(route('admin.status.index'))->with('flash_success', 'Master Data Status saved successfully');
    }

    public function destroy($industry_id): RedirectResponse
    {
        $item = Status::find($industry_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.status.index')->with('success', 'Master Data Status deleted successfully');
    }
}
