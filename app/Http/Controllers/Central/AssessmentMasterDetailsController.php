<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAssessmentMasterDetailsRequest;
use App\Http\Requests\UpdateAssessmentMasterDetailsRequest;
use App\Models\Central\AssessmentMaster;
use App\Models\Central\AssessmentMasterDetail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentMasterDetailsController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        // $assessment_details = AssessmentMasterDetails::sortable('group','asc')->paginate($item);

        $assessment_details = AssessmentMasterDetail::query();

        if ($request->has('q') && $request->input('q') != '') {
            $assessment_details = $assessment_details->where('assessment_measure', 'like', '%'.$request->input('q').'%')
                ->orWhere('assessment_group', 'like', '%'.$request->input('q').'%');
        }

        $assessment_details = $assessment_details->paginate($item);

        $parameters = [
            'assessment_details' => $assessment_details,
        ];

        return View('admin.assessment_details.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = AssessmentMasterDetail::orderBy('assessment_group')->where('assessment_group', '!=', null)->where('assessment_group', '!=', '')->get();

        $parameters = [
            'assessment_master' => AssessmentMaster::select('description', 'id')->pluck('description', 'id')->prepend('Select Assessment Details', '0'),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return view('admin.assessment_details.create')->with($parameters);
    }

    public function store(StoreAssessmentMasterDetailsRequest $request): RedirectResponse
    {
        $assessment_details = new AssessmentMasterDetail;
        $assessment_details->assessment_master_id = $request->assessment_master_id;
        $assessment_details->assessment_group = $request->assessment_group;
        $assessment_details->assessment_measure = $request->assessment_measure;
        $assessment_details->save();

        return redirect(route('admin.assessmentmasterdetails.index'))->with('flash_success', 'Assessment Master Detail has been captured successful');
    }

    public function show($id): View
    {
        $parameters = [
            'assessment_details' => AssessmentMasterDetail::find($id),
        ];

        return view('admin.assessment_details.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = AssessmentMasterDetail::orderBy('assessment_group')->where('assessment_group', '!=', null)->where('assessment_group', '!=', '')->get();

        $parameters = [
            'assessment_master_details' => AssessmentMasterDetail::find($id),
            'assessment_master_dropdown' => AssessmentMaster::select('description', 'id')->pluck('description', 'id')->prepend('Select Assessment Details', '0'),
            'autocomplete_elements' => $autocomplete_elements
        ];

        return view('admin.assessment_details.edit')->with($parameters);
    }

    public function update(UpdateAssessmentMasterDetailsRequest $request, $id): RedirectResponse
    {
        $assessment_details = AssessmentMasterDetail::find($id);
        $assessment_details->assessment_master_id = $request->assessment_master_id;
        $assessment_details->assessment_group = $request->assessment_group;
        $assessment_details->assessment_measure = $request->assessment_measure;
        $assessment_details->save();

        return redirect(route('admin.assessmentmasterdetails.index'))->with('flash_success', 'Assessment Master Detail has been updated successful');
    }

    public function destroy($id): RedirectResponse
    {
        AssessmentMasterDetails::destroy($id);

        return redirect(route('assessment_master_details.index'))->with('flash_success', 'Assessment Master Detail suspended successfully');
    }
}
