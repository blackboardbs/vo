<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\SprintStatusRequest;
use App\Models\Central\SprintStatus;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SprintStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $sprint_statuses = SprintStatus::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $sprint_statuses = SprintStatus::where('status_id', $request->input('status_filter'));
            }else{
                $sprint_statuses = SprintStatus::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $sprint_statuses = $sprint_statuses->where('description', 'like', '%'.$request->input('q').'%');
        }

        $sprint_statuses = $sprint_statuses->paginate($item);

        return view('admin.sprint-status.index')->with(['sprint_status' => $sprint_statuses]);
    }

    public function create(): View
    {
        return view('admin.sprint-status.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    public function store(SprintStatusRequest $request): RedirectResponse
    {
        $sprintstatus = new SprintStatus();
        $sprintstatus->description = $request->description;
        $sprintstatus->status_id = $request->status_id;
        $sprintstatus->save();

        return redirect()->route('admin.sprintstatus.index')->with('flash_success', 'Sprint status was saved successfully');
    }

    public function show(SprintStatus $sprintstatus): View
    {
        return view('admin.sprint-status.show')->with(['sprintstatus' => $sprintstatus]);
    }

    public function edit(SprintStatus $sprintstatus): View
    {
        return view('admin.sprint-status.edit')->with([
            'sprintstatus' => $sprintstatus,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id')
        ]);
    }

    public function update(SprintStatusRequest $request, SprintStatus $sprintstatus): RedirectResponse
    {
        $sprintstatus->description = $request->description;
        $sprintstatus->status_id = $request->status_id;
        $sprintstatus->save();

        return redirect()->route('admin.sprintstatus.index')->with('flash_success', 'Sprint status was updated successfully');
    }

    public function destroy($sprintstatus)
    {
        $item = SprintStatus::find($sprintstatus);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.sprintstatus.index')->with('flash_success', 'Sprint status was suspended successfully');
    }
}
