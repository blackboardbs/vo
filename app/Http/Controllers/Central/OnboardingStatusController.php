<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\OnboardingStatusRequest;
use App\Models\Central\OnboardingStatus;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class OnboardingStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $statuses = OnboardingStatus::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $statuses = OnboardingStatus::where('status_id', $request->input('status_filter'));
            }else{
                $statuses = OnboardingStatus::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $statuses = $statuses->where('description', 'like', '%'.$request->input('q').'%');
        }

        $statuses = $statuses->paginate($item);

        return view('admin.onboarding_status.index')->with(['statuses' => $statuses]);
    }

    public function create(): View
    {
        return view('admin.onboarding_status.create')
            ->with([
                'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
            ]);
    }

    public function store(OnboardingStatusRequest $request, OnboardingStatus $status): RedirectResponse
    {
        $status->description = $request->description;
        $status->status_id = $request->status_id;
        $status->save();

        return redirect()->route('admin.onboardingstatus.index')->with('flash_success', 'Onboarding status created successfully');
    }

    public function show(OnboardingStatus $onboardingstatus): View
    {
        return view('admin.onboarding_status.show')->with(['status' => $onboardingstatus]);
    }

    public function edit(OnboardingStatus $onboardingstatus): View
    {
        $parameters = [
            'status' => $onboardingstatus,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ];

        return view('admin.onboarding_status.edit')->with($parameters);
    }

    public function update(OnboardingStatusRequest $request, OnboardingStatus $onboardingstatus): RedirectResponse
    {
        $onboardingstatus->description = $request->description;
        $onboardingstatus->status_id = $request->status_id;
        $onboardingstatus->save();

        return redirect()->route('admin.onboardingstatus.index')->with(['flash_success' => 'Onboarding status updated successfully']);
    }

    public function destroy($onboardingStatus)
    {
        $item = OnboardingStatus::find($onboardingStatus);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.onboardingstatus.index')->with(['flash_success' => 'Onboarding status suspended successfully']);
    }
}
