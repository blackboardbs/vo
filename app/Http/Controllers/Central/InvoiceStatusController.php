<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInvoiceStatusRequest;
use App\Http\Requests\UpdateInvoiceStatusRequest;
use App\Models\Central\InvoiceStatus;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class InvoiceStatusController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $invoice_status = InvoiceStatus::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $invoice_status = InvoiceStatus::where('status_id', $request->input('status_filter'));
            }else{
                $invoice_status = InvoiceStatus::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $invoice_status = $invoice_status->where('description', 'like', '%'.$request->input('q').'%');
        }

        $invoice_status = $invoice_status->paginate($item);

        $parameters = [
            'invoice_status' => $invoice_status,
        ];

        return View('admin.invoice_status.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = InvoiceStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.invoice_status.create')->with($parameters);
    }

    public function store(StoreInvoiceStatusRequest $request): RedirectResponse
    {
        $invoice_status = new InvoiceStatus();
        $invoice_status->description = $request->input('description');
        $invoice_status->status_id = $request->input('status');
        $invoice_status->creator_id = auth()->id()??1;
        $invoice_status->save();

        return redirect(route('admin.invoicestatus.index'))->with('flash_success', 'Master Data Invoice Status captured successfully');
    }

    public function show($invoice_status_id): View
    {
        $parameters = [
            'invoice_status' => InvoiceStatus::where('id', '=', $invoice_status_id)->get(),
        ];

        return View('admin.invoice_status.show')->with($parameters);
    }

    public function edit($invoice_status_id): View
    {
        $autocomplete_elements = InvoiceStatus::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'invoice_status' => InvoiceStatus::where('id', '=', $invoice_status_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.invoice_status.edit')->with($parameters);
    }

    public function update(UpdateInvoiceStatusRequest $request, $invoice_status_id): RedirectResponse
    {
        $invoice_status = InvoiceStatus::find($invoice_status_id);
        $invoice_status->description = $request->input('description');
        $invoice_status->status_id = $request->input('status');
        $invoice_status->creator_id = auth()->id()??1;
        $invoice_status->save();

        return redirect(route('admin.invoicestatus.index'))->with('flash_success', 'Master Data Invoice Status saved successfully');
    }

    public function destroy($invoice_status_id): RedirectResponse
    {
        $item = InvoiceStatus::find($invoice_status_id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.invoicestatus.index')->with('success', 'Master Data Invoice Status deleted successfully');
    }
}
