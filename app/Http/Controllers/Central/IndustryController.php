<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreIndustryRequest;
use App\Http\Requests\UpdateIndustryRequest;
use App\Models\Central\Industry;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndustryController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $industry = Industry::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $industry = Industry::where('status', $request->input('status_filter'));
            }else{
                $industry = Industry::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $industry = $industry->where('description', 'like', '%'.$request->input('q').'%');
        }

        $industry = $industry->paginate($item);

        $parameters = [
            'industry' => $industry,
        ];

        return View('admin.industry.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = Industry::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.industry.create')->with($parameters);
    }

    public function store(StoreIndustryRequest $request): RedirectResponse
    {
        $industry = new Industry();
        $industry->description = $request->input('description');
        $industry->status = $request->input('status');
        $industry->creator_id = auth()->id()??1;
        $industry->save();

        return redirect(route('admin.industry.index'))->with('flash_success', 'Master Data Industry captured successfully');
    }

    public function show($industry_id): View
    {
        $parameters = [
            'industry' => Industry::where('id', '=', $industry_id)->get(),
        ];

        return View('admin.industry.show')->with($parameters);
    }

    public function edit($industry_id): View
    {
        $autocomplete_elements = Industry::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'industry' => Industry::where('id', '=', $industry_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.industry.edit')->with($parameters);
    }

    public function update(UpdateIndustryRequest $request, $industry_id): RedirectResponse
    {
        $industry = Industry::find($industry_id);
        $industry->description = $request->input('description');
        $industry->status = $request->input('status');
        $industry->creator_id = auth()->id()??1;
        $industry->save();

        return redirect(route('admin.industry.index'))->with('flash_success', 'Master Data Industry saved successfully');
    }

    public function destroy($industry_id): RedirectResponse
    {
        $item = Industry::find($industry_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.industry.index')->with('success', 'Master Data Industry suspended successfully');
    }
}
