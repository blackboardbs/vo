<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\SpecialityRequest;
use App\Models\Central\Profession;
use App\Models\Central\Speciality;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SpecialityController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $specialities = Speciality::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $specialities = Speciality::where('status_id', $request->input('status_filter'));
            }else{
                $specialities = Speciality::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $specialities = $specialities->where('name', 'like', '%'.$request->input('q').'%');
        }

        $specialities = $specialities->paginate($item);

        $parameters = [
            'specialities' => $specialities,
        ];

        return view('admin.speciality.index')->with($parameters);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $autocomplete_elements = Speciality::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $parameters = [
            'profession_drow_down' => Profession::pluck('name', 'id')->toArray(),
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.speciality.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SpecialityRequest $request): RedirectResponse
    {
        $speciality = new Speciality();
        $speciality->name = $request->input('name');
        $speciality->profession_id = $request->input('profession_id');
        $speciality->creator_id = Auth()->id();
        $speciality->status_id = $request->input('status_id');
        $speciality->save();

        return redirect(route('admin.speciality.index'))->with('flash_success', 'Speciality saved successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Speciality $speciality): View
    {
        $speciality = Speciality::find($speciality->id);
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');
        $parameters = [
            'speciality' => $speciality,
            'profession_drow_down' => Profession::pluck('name', 'id')->toArray(),
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.speciality.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Speciality $speciality): View
    {
        $autocomplete_elements = Speciality::orderBy('name')->where('name', '!=', null)->where('name', '!=', '')->get();
        $status_drop_down = Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0');

        $speciality = Speciality::find($speciality->id);

        $parameters = [
            'speciality' => $speciality,
            'profession_drow_down' => Profession::pluck('name', 'id')->toArray(),
            'autocomplete_elements' => $autocomplete_elements,
            'status_drop_down' => $status_drop_down,
        ];

        return view('admin.speciality.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SpecialityRequest $request, Speciality $speciality): RedirectResponse
    {
        $speciality = Speciality::find($speciality->id);
        $speciality->name = $request->input('name');
        $speciality->profession_id = $request->input('profession_id');
        $speciality->creator_id = Auth()->id();
        $speciality->status_id = $request->input('status_id');
        $speciality->save();

        return redirect(route('admin.speciality.index'))->with('flash_success', 'Speciality updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($speciality): RedirectResponse
    {
        // Speciality::destroy($speciality->id);
        $item = Speciality::find($speciality);
        $item->status_id = 2;
        $item->save();

        return redirect(route('admin.speciality.index'))->with('flash_success', 'Speciality suspended successfully.');
    }
}
