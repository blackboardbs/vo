<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAssetClassRequest;
use App\Http\Requests\UpdateAssetClassRequest;
use App\Models\Central\AssetClass;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssetClassController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $assert_class = AssetClass::with(['statusd:id,description'])->where('status', \App\Enum\Status::ACTIVE->value);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $assert_class = AssetClass::where('status', $request->input('status_filter'));
            }else{
                $assert_class = AssetClass::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $assert_class = $assert_class->where('description', 'like', '%'.$request->input('q').'%');
        }

        $assert_class = $assert_class->paginate($item);

        $parameters = [
            'assert_class' => $assert_class,
        ];

        return View('admin.asserts_class.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = AssetClass::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.asserts_class.create')->with($parameters);
    }

    public function store(StoreAssetClassRequest $request): RedirectResponse
    {
        $assert_class = new AssetClass();
        $assert_class->description = $request->input('description');
        $assert_class->status = $request->input('status');
        $assert_class->creator_id = auth()->id()??1;
        $assert_class->est_life_month = $request->input('est_life_month');
        $assert_class->save();

        return redirect(route('admin.assetclass.index'))->with('flash_success', 'Master Data Asset Class captured successfully');
    }

    public function show($assert_class_id): View
    {
        $parameters = [
            'asserts_class' => AssetClass::where('id', '=', $assert_class_id)->get(),
        ];

        return View('admin.asserts_class.show')->with($parameters);
    }

    public function edit($assert_class_id): View
    {
        $autocomplete_elements = AssetClass::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'assert_class' => AssetClass::where('id', '=', $assert_class_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.asserts_class.edit')->with($parameters);
    }

    public function update(UpdateAssetClassRequest $request, $assert_class_id): RedirectResponse
    {
        $assert_class = AssetClass::find($assert_class_id);
        $assert_class->description = $request->input('description');
        $assert_class->status = $request->input('status');
        $assert_class->est_life_month = $request->input('est_life_month');
        $assert_class->save();

        return redirect(route('admin.assetclass.index'))->with('flash_success', 'Master Data Asset Class saved successfully');
    }

    public function destroy($assert_class_id): RedirectResponse
    {
        $item = AssetClass::find($assert_class_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.assetclass.index')->with('success', 'Master Data Asset Class suspended successfully');
    }
}
