<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\TitleRequest;
use App\Models\Central\Status;
use App\Models\Central\Title;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TitleController extends Controller
{
    public function index(Request $request)
    {
        $item = $request->input('s') ?? 15;

        $titles = Title::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $titles = Title::where('status_id', $request->input('status_filter'));
            }else{
                $titles = Title::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $titles = $titles->where('description', 'like', '%'.$request->input('q').'%');
        }

        $titles = $titles->paginate($item);

        return view('admin.title.index')->with(['titles' => $titles]);
    }

    public function create(): View
    {
        return view('admin.title.create')->with([
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ]);
    }

    public function store(TitleRequest $request, Title $title): RedirectResponse
    {
        $this->data($request, $title);

        return redirect()->route('admin.title.index')->with('flash_success', 'Title saved successfully');
    }

    public function show(Title $title): View
    {
        return view('admin.title.show')->with(['title' => $title]);
    }

    public function edit(Title $title): View
    {
        return view('admin.title.edit')->with([
            'title' => $title,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ]);
    }

    public function update(TitleRequest $request, Title $title): RedirectResponse
    {
        $this->data($request, $title);

        return redirect()->route('admin.title.index')->with('flash_success', 'Title updated successfully');
    }

    public function destroy($title): RedirectResponse
    {
        $item = Title::find($title);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.title.index')->with('flash_success', 'Title suspended successfully');
    }

    private function data($request, $title)
    {
        $title->description = $request->description;
        $title->status_id = $request->status_id;
        $title->save();

        return $title;
    }
}
