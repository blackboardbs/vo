<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCvCompanyRequest;
use App\Http\Requests\UpdateCvCompanyRequest;
use App\Models\Central\CvCompany;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CvCompanyController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $cv_company = CvCompany::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $cv_company = CvCompany::where('status_id', $request->input('status_filter'));
            }else{
                $cv_company = CvCompany::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $cv_company = $cv_company->where('description', 'like', '%'.$request->input('q').'%');
        }

        $cv_company = $cv_company->paginate($item);

        $parameters = [
            'cv_company' => $cv_company,
        ];

        return View('admin.cv_company.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = CvCompany::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.cv_company.create')->with($parameters);
    }

    public function store(StoreCvCompanyRequest $request): RedirectResponse
    {
        //return [$request->input('description'), $request->input('status'), $request->file('company_logo')];
        $cv_company = new CvCompany();
        $cv_company->description = $request->input('description');
        $cv_company->status_id = $request->input('status');
        $cv_company->creator_id = auth()->id()??1;
        if ($request->hasFile('company_logo')) {
            $request->file('company_logo')->store('avatars/cv_company');
        }
        $cv_company->logo = (($request->file('company_logo') != null) ? $request->file('company_logo')->hashName() : '');
        $cv_company->save();

        return redirect(route('admin.cvcompany.index'))->with('flash_success', 'Master Data CV Company captured successfully');
    }

    public function show($cv_company_id): View
    {
        $parameters = [
            'cv_company' => CvCompany::where('id', '=', $cv_company_id)->get(),
        ];

        return View('admin.cv_company.show')->with($parameters);
    }

    public function edit($cv_company_id): View
    {
        $autocomplete_elements = CvCompany::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'cv_company' => CvCompany::where('id', '=', $cv_company_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.cv_company.edit')->with($parameters);
    }

    public function update(UpdateCvCompanyRequest $request, $cv_company_id): RedirectResponse
    {
        //return $request->all();
        $cv_company = CvCompany::find($cv_company_id);
        $cv_company->description = $request->input('description');
        $cv_company->status_id = $request->input('status');
        $cv_company->creator_id = auth()->id()??1;
        if ($request->hasFile('company_logo')) {
            $request->file('company_logo')->store('avatars/cv_company');
            $cv_company->logo = $request->file('company_logo')->hashName();
        }

        $cv_company->save();

        return redirect(route('admin.cvcompany.index'))->with('flash_success', 'Master Data CV Company saved successfully');
    }

    public function destroy($cv_company_id): RedirectResponse
    {
        $item = CvCompany::find($cv_company_id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.cvcompany.index')->with('success', 'Master Data Payment Method suspended successfully');
    }
}
