<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\ScoutingRoleRequest;
use App\Models\Central\ScoutingRole;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ScoutingRoleController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $roles = ScoutingRole::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $roles = ScoutingRole::where('status_id', $request->input('status_filter'));
            }else{
                $roles = ScoutingRole::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $roles = $roles->where('name', 'like', '%'.$request->input('q').'%');
        }

        $roles = $roles->paginate($item);

        return view('admin.main_role.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.main_role.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ScoutingRoleRequest $request, ScoutingRole $scoutingrole): RedirectResponse
    {
        $this->dbValues($scoutingrole, $request);

        return redirect()->route('admin.scoutingrole.index')->with('flash_success', 'Main Role stored successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(ScoutingRole $scoutingrole): View
    {
        return view('admin.main_role.show')->with(['scoutingrole' => $scoutingrole]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ScoutingRole $scoutingrole): View
    {
        return view('admin.main_role.edit')->with([
            'scoutingrole' => $scoutingrole,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ScoutingRoleRequest $request, ScoutingRole $scoutingrole): RedirectResponse
    {
        $this->dbValues($scoutingrole, $request);

        return redirect()->route('admin.scoutingrole.index')->with('flash_success', 'Main Role updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        $item = ScoutingRole::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect(route('admin.scoutingrole.index'))->with('flash_success', 'Main Role successfully suspended.');
    }

    private function dbValues($scoutingrole, $request)
    {
        $scoutingrole->name = $request->name;
        $scoutingrole->status_id = $request->status_id;
        $scoutingrole->save();
    }
}
