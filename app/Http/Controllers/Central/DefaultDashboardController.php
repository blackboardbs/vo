<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDefaultDashboardRequest;
use App\Models\Central\DefaultDashboard;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class DefaultDashboardController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;
        $default_dashboard = DefaultDashboard::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $default_dashboard = DefaultDashboard::where('status_id', $request->input('status_filter'));
            }else{
                $default_dashboard = DefaultDashboard::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $default_dashboard = $default_dashboard->where('dashboard_name', 'like', '%'.$request->input('q').'%');
        }

        $default_dashboard = $default_dashboard->paginate($item);

        $parameters = [
            'dashboards' => $default_dashboard,
        ];

        return view('admin.default_dashboard.index')->with($parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = DefaultDashboard::orderBy('dashboard_name')->where('dashboard_name', '!=', null)->where('dashboard_name', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'roles_dropdown2' => Role::pluck('display_name', 'id')->prepend('Select Role', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.default_dashboard.create')->with($parameters);
    }

    public function store(StoreDefaultDashboardRequest $request): RedirectResponse
    {
        $top_components = '';
        foreach ($request->top_components as $key => $top_component) {
            $top_components .= (($key != (count($request->top_components) - 1)) ? str_replace(' ', '_', "$top_component").',' : str_replace(' ', '_', "$top_component"));
        }
        $charts_and_tables = '';
        foreach ($request->charts_tables as $key => $charts_tables) {
            $charts_and_tables .= (($key != (count($request->charts_tables) - 1)) ? str_replace(' ', '_', "$charts_tables").',' : str_replace(' ', '_', "$charts_tables"));
        }

        $default_dashboard = new DefaultDashboard();
        $default_dashboard->role_id = $request->role_id;
        $default_dashboard->top_component = $top_components;
        $default_dashboard->charts_tables = $charts_and_tables;
        $default_dashboard->dashboard_name = $request->dashboard_name;
        $default_dashboard->status_id = $request->status_id;
        $default_dashboard->save();

        return redirect()->route('admin.dashboard.index')->with('flash_success', 'Default dashboard created successfully');
        //return $top_components;
    }

    public function show($id): View
    {
        $dashboard = DefaultDashboard::find($id);

        $parameters = [
            'dashboard' => $dashboard,
        ];

        return view('admin.default_dashboard.show')->with($parameters);
    }

    public function edit($id): View
    {
        $autocomplete_elements = DefaultDashboard::orderBy('dashboard_name')->where('dashboard_name', '!=', null)->where('dashboard_name', '!=', '')->get();

        $dashboard = DefaultDashboard::find($id);

        $parameters = [
            'dashboard' => $dashboard,
            'status_dropdown' => Status::where('status', '=', 1)->pluck('description', 'id')->prepend('Select Status', '0'),
            'roles_dropdown2' => Role::pluck('display_name', 'id')->prepend('Select Role', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('admin.default_dashboard.edit')->with($parameters);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        $top_components = '';
        foreach ($request->top_components as $key => $top_component) {
            $top_components .= (($key != (count($request->top_components) - 1)) ? str_replace(' ', '_', "$top_component").',' : str_replace(' ', '_', "$top_component"));
        }
        $charts_and_tables = '';
        foreach ($request->charts_tables as $key => $charts_tables) {
            $charts_and_tables .= (($key != (count($request->charts_tables) - 1)) ? str_replace(' ', '_', "$charts_tables").',' : str_replace(' ', '_', "$charts_tables"));
        }

        $default_dashboard = DefaultDashboard::find($id);
        $default_dashboard->role_id = $request->role_id;
        $default_dashboard->top_component = $top_components;
        $default_dashboard->charts_tables = $charts_and_tables;
        $default_dashboard->dashboard_name = $request->dashboard_name;
        $default_dashboard->status_id = $request->status_id;
        $default_dashboard->save();

        return redirect()->route('admin.dashboard.index')->with('flash_success', 'Default dashboard updated successfully');
    }

    public function destroy($id)
    {
        $item = DefaultDashboard::find($id);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.dashboard.index')->with('flash_success', 'Default dashboard suspended successfully');
    }
}
