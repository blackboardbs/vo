<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserTypeRequest;
use App\Models\Central\Status;
use App\Models\Central\UserType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserTypeController extends Controller
{
    public function index(Request $request)
    {

        $item = $request->input('s') ?? 15;

        $user_type = UserType::where('status_id', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $user_type = UserType::where('status_id', $request->input('status_filter'));
            }else{
                $user_type = UserType::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $user_type = $user_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $user_type = $user_type->paginate($item);

        return view('admin.user-type.index')->with(['usertypes' => $user_type]);
    }

    public function create(): View
    {
        return view('admin.user-type.create')->with(['status_dropdown' => Status::where('status', 1)->pluck('description', 'id')]);
    }

    public function store(UserTypeRequest $request, UserType $userType): RedirectResponse
    {
        $this->dbValues($userType, $request);

        return redirect()->route('admin.usertype.index')->with('flash_success', 'User type stored successfully');
    }

    public function show(UserType $usertype): View
    {
        return view('admin.user-type.show')->with(['usertype' => $usertype]);
    }

    public function edit(UserType $usertype): View
    {
        return view('admin.user-type.edit')->with([
            'usertype' => $usertype,
            'status_dropdown' => Status::where('status', 1)->pluck('description', 'id'),
        ]);
    }

    public function update(UserTypeRequest $request, UserType $usertype): RedirectResponse
    {
        $this->dbValues($usertype, $request);

        return redirect()->route('admin.usertype.index')->with('flash_success', 'User Type updated successfully.');
    }

    public function destroy($usertype): RedirectResponse
    {
        $item = UserType::find($usertype);
        $item->status_id = 2;
        $item->save();

        return redirect()->route('admin.usertype.index')->with('flash_success', 'User Type suspended successfully.');
    }

    private function dbValues($usertype, $request)
    {
        $usertype->description = $request->description;
        $usertype->status_id = $request->status_id;
        $usertype->save();
    }
}
