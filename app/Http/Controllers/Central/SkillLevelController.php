<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSkillRequest;
use App\Http\Requests\UpdateSkillRequest;
use App\Models\Cv;
use App\Models\Module;
use App\Models\Resource;
use App\Models\Skill;
use App\Models\SkillLevel;
use App\Models\Status;
use App\Models\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SkillLevelController extends Controller
{
    public function index(Request $request)
    {
        $module = Module::where('name', '=', 'App\Models\CVSkill')->first();

        if (Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team')) {
            if (! Auth::user()->canAccess($module->id, 'view_all') && Auth::user()->canAccess($module->id, 'view_team')) {
                //$company->whereIn('id', $r->team());
            }
        } else {
            return abort(403);
        }

        $can_create = Auth::user()->canAccess($module->id, 'create_all')
            || Auth::user()->canAccess($module->id, 'create_team');

        $can_update = Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_all')
            || Auth::user()->canAccess(isset($module->id) ? $module->id : -1, 'update_team');
    }

    public function create($cvid, $res_id)
    {
        $module = Module::where('name', '=', 'App\Models\CVSkill')->first();
        abort_unless(
            Auth::user()->canAccess($module->id, 'create_all')
            || Auth::user()->canAccess($module->id, 'create_team'),
            403
        );

        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }

        $autocomplete_elements = Skill::orderBy('res_skill')->where('res_skill', '!=', null)->where('res_skill', '!=', '')->get();

        $parameters = [
            'cv' => $cvid,
            'res' => $res_id,
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'system_dropdown' => System::orderBy('id')->pluck('description', 'id')->prepend('Skill or Tool', '0'),
            'skill_level_dropdown' => SkillLevel::orderBy('id')->pluck('description', 'id')->prepend('Skill Level', '0'),
            'skills' => Skill::where('res_id', '=', $res_id)->orderBy('row_order')->get(),
            'row_order' => $order_number,
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('skill.create')->with($parameters);
    }

    public function store(StoreSkillRequest $request, $cvid, $res_id)
    {
        $skill = new Skill;
        $skill->res_id = $res_id;
        $skill->res_skill = $request->input('res_skill');
        $skill->tool_id = $request->input('tool_id');
        $skill->tool = $request->input('tool');
        $skill->years_experience = $request->input('years_experience');
        $skill->skill_level = $request->input('skill_level');
        $skill->row_order = $request->input('row_order');
        $skill->status_id = $request->input('status_id');
        $skill->creator_id = auth()->id();
        $skill->save();

        if ($request->input('add_more') == 1) {
            return redirect(route('skill.create', ['cvid' => $cvid, 'res_id' => $res_id]))->with('flash_success', 'Skill captured successfully');
        }

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Skill captured successfully');
    }

    public function show(Request $request, Skill $skill, Cv $cv)
    {
        $module = Module::where('name', '=', 'App\Models\CVSkill')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'show_all') || Auth::user()->canAccess($module->id, 'show_team'), 403);

        $parameters = [
            'skill' => $skill,
            'cvid' => $cv,
        ];

        return view('skill.show')->with($parameters);
    }

    public function edit(Skill $skill, Cv $cv, Resource $resource)
    {
        $module = Module::where('name', '=', 'App\Models\CVSkill')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team') , 403);

        $order_number = [];
        for ($i = 0; $i < 30; $i++) {
            $order_number[$i] = $i;
        }

        $autocomplete_elements = Skill::orderBy('res_skill')->where('res_skill', '!=', null)->where('res_skill', '!=', '')->get();

        $parameters = [
            'skill' => $skill,
            'cv' => $cv->id,
            'skill_level_dropdown' => SkillLevel::orderBy('id')->pluck('description', 'id')->prepend('Please Select', '0'),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Please Select', '0'),
            'system_dropdown' => System::orderBy('description')->pluck('description', 'id')->prepend('Please Select', '0'),
            'row_order' => $order_number,
            'res' => $resource->id,
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return view('skill.edit')->with($parameters);
    }

    public function update(UpdateSkillRequest $request, $skillid, $cvid, $res_id)
    {
        $skill = Skill::find($skillid);
        $skill->res_skill = $request->input('res_skill');
        $skill->tool_id = $request->input('tool_id');
        $skill->tool = $request->input('tool');
        $skill->years_experience = $request->input('years_experience');
        $skill->skill_level = $request->input('skill_level');
        $skill->row_order = $request->input('row_order');
        $skill->status_id = $request->input('status_id');
        $skill->creator_id = auth()->id();
        $skill->save();

        if ($request->has('from_create') && $request->input('from_create') == 1) {
            return redirect(route('skill.create', ['cvid' => $request->input('cvid'), 'res_id' => $request->input('res_id')]))->with('flash_success', 'Skill updated successfully');
        }

        return redirect(route('cv.show', ['cv' => $cvid, 'resource' => $res_id]))->with('flash_success', 'Skill updated successfully');
    }

    public function destroy($id, Request $request)
    {
        $module = Module::where('name', '=', 'App\CVSkill')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'update_all') || Auth::user()->canAccess($module->id, 'update_team'), 403);

        DB::table('skill')->delete($id);
        //return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
        Skill::destroy($id);

        if ($request->has('from_create') && $request->input('from_create') == 1) {
            return redirect(route('skill.create', ['cvid' => $request->input('cvid'), 'res_id' => $request->input('res_id')]))->with('flash_success', 'Skill deleted successfully');
        }

        return redirect()->route('cv.index')->with('success', 'Skill deleted successfully');
    }
}
