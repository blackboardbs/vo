<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectTypeRequest;
use App\Http\Requests\UpdateProjectTypeRequest;
use App\Models\Central\ProjectType;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProjectTypeController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $project_type = ProjectType::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $project_type = ProjectType::where('status', $request->input('status_filter'));
            }else{
                $project_type = ProjectType::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $project_type = $project_type->where('description', 'like', '%'.$request->input('q').'%');
        }

        $project_type = $project_type->paginate($item);

        $parameters = [
            'project_type' => $project_type,
        ];

        return View('admin.project_type.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ProjectType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.project_type.create')->with($parameters);
    }

    public function store(StoreProjectTypeRequest $request): RedirectResponse
    {
        $project_type = new ProjectType();
        $project_type->description = $request->input('description');
        $project_type->status = $request->input('status_id');
        $project_type->creator_id = auth()->id()??1;
        $project_type->save();

        return redirect(route('admin.projecttype.index'))->with('flash_success', 'Master Data Project Type captured successfully');
    }

    public function show($project_type_id): View
    {
        $parameters = [
            'project_type' => ProjectType::where('id', '=', $project_type_id)->get(),
        ];

        return View('admin.project_type.show')->with($parameters);
    }

    public function edit($project_type_id): View
    {
        $autocomplete_elements = ProjectType::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'project_type' => ProjectType::where('id', '=', $project_type_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.project_type.edit')->with($parameters);
    }

    public function update(UpdateProjectTypeRequest $request, $project_type_id): RedirectResponse
    {
        $project_type = ProjectType::find($project_type_id);
        $project_type->description = $request->input('description');
        $project_type->status = $request->input('status');
        $project_type->creator_id = auth()->id()??1;
        $project_type->save();

        return redirect(route('admin.projecttype.index'))->with('flash_success', 'Master Data Project Type saved successfully');
    }

    public function destroy($project_type_id): RedirectResponse
    {
        $item = ProjectType::find($project_type_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.projecttype.index')->with('success', 'Master Data Project Type deleted successfully');
    }
}
