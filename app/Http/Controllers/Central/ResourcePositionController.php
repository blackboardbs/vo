<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreResourcePositionRequest;
use App\Http\Requests\UpdateResourcePositionRequest;
use App\Models\Central\ResourcePosition;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ResourcePositionController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $emp_positions = ResourcePosition::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $emp_positions = ResourcePosition::where('status', $request->input('status_filter'));
            }else{
                $emp_positions = ResourcePosition::query();
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $emp_positions = $emp_positions->where('description', 'like', '%'.$request->input('q').'%');
        }

        $emp_positions = $emp_positions->paginate($item);

        $parameters = [
            'emp_positions' => $emp_positions,

        ];

        return View('admin.resource_position.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = ResourcePosition::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Select status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.resource_position.create')->with($parameters);
    }

    public function store(StoreResourcePositionRequest $request): RedirectResponse
    {
        $emp_position = new ResourcePosition();
        $emp_position->description = $request->input('description');
        $emp_position->status = $request->input('status');
        $emp_position->creator_id = auth()->id()??1;
        $emp_position->save();

        return redirect(route('admin.resourceposition.index'))->with('flash_success', 'Master Resource Position captured successfully');
    }

    public function show($emp_position_id): View
    {
        $parameters = [
            'emp_position' => ResourcePosition::where('id', '=', $emp_position_id)->get(),
        ];

        return View('admin.resource_position.show')->with($parameters);
    }

    public function edit($emp_position_id): View
    {
        $autocomplete_elements = ResourcePosition::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'emp_position' => ResourcePosition::where('id', '=', $emp_position_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.resource_position.edit')->with($parameters);
    }

    public function update(UpdateResourcePositionRequest $request, $emp_position_id): RedirectResponse
    {
        $emp_position = ResourcePosition::find($emp_position_id);
        $emp_position->description = $request->input('description');
        $emp_position->status = $request->input('status');
        $emp_position->creator_id = auth()->id()??1;
        $emp_position->save();

        return redirect(route('admin.resourceposition.index'))->with('flash_success', 'Master Resource Position saved successfully');
    }

    public function destroy($emp_position_id): RedirectResponse
    {
        $item = ResourcePosition::find($emp_position_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.resourceposition.index')->with('success', 'Master Data Resource Position suspended successfully');
    }
}
