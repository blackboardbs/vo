<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSystemRequest;
use App\Http\Requests\UpdateSystemRequest;
use App\Models\Central\Status;
use App\Models\Central\System;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SystemController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $skills = System::where('status', 1);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $skills = System::where('status', $request->input('status_filter'));
            }else{
                $skills = System::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $skills = $skills->where('description', 'like', '%'.$request->input('q').'%');
        }

        $skills = $skills->paginate($item);

        $parameters = [
            'skills' => $skills,
        ];

        return View('admin.skill.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = System::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.skill.create')->with($parameters);
    }

    public function store(StoreSystemRequest $request): RedirectResponse
    {
        $skill = new System();
        $skill->description = $request->input('description');
        $skill->status = $request->input('status');
        $skill->save();

        return redirect(route('admin.skill.index'))->with('flash_success', 'Master Data Skill captured successfully');
    }

    public function show($skill_id): View
    {
        $parameters = [
            'system' => System::where('id', '=', $skill_id)->first(),
        ];

        return View('admin.skill.show')->with($parameters);
    }

    public function edit($skill_id): View
    {
        $autocomplete_elements = System::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'skill' => System::where('id', '=', $skill_id)->first(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.skill.edit')->with($parameters);
    }

    public function update(UpdateSystemRequest $request, $skill_id): RedirectResponse
    {
        $skill = System::find($skill_id);
        $skill->description = $request->input('description');
        $skill->status = $request->input('status');
        $skill->save();

        return redirect(route('admin.skill.index'))->with('flash_success', 'Master Data Skill saved successfully');
    }

    public function destroy($skill_id): RedirectResponse
    {
        $item = System::find($skill_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.skill.index')->with('flash_success', 'Master Data Skill suspended successfully');
    }
}
