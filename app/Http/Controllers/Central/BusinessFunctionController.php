<?php

namespace App\Http\Controllers\Central;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBusinessFunctionRequest;
use App\Http\Requests\UpdateBusinessFunctionRequest;
use App\Models\Central\BusinessFunction;
use App\Models\Central\Status;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BusinessFunctionController extends Controller
{
    public function index(Request $request): View
    {
        $item = $request->input('r') ?? 15;

        $business_function = BusinessFunction::where('status', \App\Enum\Status::ACTIVE->value);

        if ($request->has('status_filter') && $request->input('status_filter') != '') {
            if ($request->input('status_filter') != 0) {
                $business_function = BusinessFunction::where('status', $request->input('status_filter'));
            }else{
                $business_function = BusinessFunction::sortable('description', 'asc');
            }
        }

        if ($request->has('q') && $request->input('q') != '') {
            $business_function = $business_function->where('description', 'like', '%'.$request->input('q').'%');
        }

        $business_function = $business_function->paginate($item);

        $parameters = [
            'business_function' => $business_function,
        ];

        return View('admin.business_function.index', $parameters);
    }

    public function create(): View
    {
        $autocomplete_elements = BusinessFunction::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.business_function.create')->with($parameters);
    }

    public function store(StoreBusinessFunctionRequest $request): RedirectResponse
    {
        $business_function = new BusinessFunction();
        $business_function->description = $request->input('description');
        $business_function->status = $request->input('status');
        $business_function->creator_id = auth()->id()??1;
        $business_function->save();

        return redirect(route('admin.businessfunction.index'))->with('flash_success', 'Master Data Business Function captured successfully');
    }

    public function show($business_function_id): View
    {
        $parameters = [
            'business_function' => BusinessFunction::where('id', '=', $business_function_id)->get(),
        ];

        return View('admin.business_function.show')->with($parameters);
    }

    public function edit($business_function_id): View
    {
        $autocomplete_elements = BusinessFunction::orderBy('description')->where('description', '!=', null)->where('description', '!=', '')->get();

        $parameters = [
            'business_function' => BusinessFunction::where('id', '=', $business_function_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->where('status', '=', 1)->pluck('description', 'id')->prepend('Status', '0'),
            'autocomplete_elements' => $autocomplete_elements,
        ];

        return View('admin.business_function.edit')->with($parameters);
    }

    public function update(UpdateBusinessFunctionRequest $request, $business_function_id): RedirectResponse
    {
        $business_function = BusinessFunction::find($business_function_id);
        $business_function->description = $request->input('description');
        $business_function->status = $request->input('status');
        $business_function->creator_id = auth()->id()??1;
        $business_function->save();

        return redirect(route('admin.businessfunction.index'))->with('flash_success', 'Master Date Business Function saved successfully');
    }

    public function destroy($business_function_id): RedirectResponse
    {
        $item = BusinessFunction::find($business_function_id);
        $item->status = 2;
        $item->save();

        return redirect()->route('admin.businessfunction.index')->with('success', 'Master Data Business Function suspended successfully');
    }
}
