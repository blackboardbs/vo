<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\InvoiceContact;
use App\Services\ContactService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ContactController extends Controller
{
    public function index(Request $request, ContactService $service): View|BinaryFileResponse
    {
        abort_unless($service->hasViewPermission(), 403);

        $item = $request->input('r') ?? 15;

        $contacts = InvoiceContact::with(['status' => function ($status) {
            return $status->select(['id', 'description']);
        }])->select(['id', 'first_name', 'last_name', 'email', 'status_id', 'contact_number'])
            ->when($request->q, fn($contact) => $contact->where(fn($query) => $query->where('first_name','like','%'.$request->q.'%')
                ->orWhere('last_name','like','%'.$request->q.'%')
                ->orWhere('email','like','%'.$request->q.'%')
                ->orWhere('contact_number','like','%'.$request->q.'%')))
            ->latest()
            ->paginate($item);

        $parameter = [
            'contacts' => $contacts,
        ];

        if ($request->has('export')) return $this->export($parameter, 'contact');

        return view('contact.index')->with($parameter);
    }

    public function create(ContactService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        return view('contact.create');
    }

    public function store(ContactRequest $request, InvoiceContact $contact, ContactService $service): RedirectResponse
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $contact = $service->createContact($request, $contact);

        return redirect()->route('contact.show', $contact)->with('flash_success', 'Contact added successful');
    }

    public function show(InvoiceContact $contact, ContactService $service): View
    {
        abort_unless($service->hasShowPermissions(), 403);

        return view('contact.show')->with(['contact' => $contact]);
    }

    public function edit(InvoiceContact $contact, ContactService $service): View
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        return view('contact.edit')->with(['contact' => $contact]);
    }

    public function update(ContactRequest $request, InvoiceContact $contact, ContactService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $contact = $service->updateContact($request, $contact);

        return redirect()->route('contact.show', $contact)->with('flash_success', 'Contact Updated Successfully');
    }

    public function destroy(InvoiceContact $contact, ContactService $service): RedirectResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        $contact->delete();

        return redirect()->route('contact.index')->with('flash_success', 'Contact Deleted Successfully');
    }
}
