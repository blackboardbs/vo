<?php

namespace App\Http\Controllers;

use App\Models\CVReference;
use App\Http\Requests\CVReferenceRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CVReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        $parameters = [
            'cv_id' => $request->input('cvid'),
            'cv_res_id' => $request->input('res_id'),
        ];

        return view('cv.references.create')->with($parameters);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CVReferenceRequest $request): RedirectResponse
    {
        $cv_reference = new CVReference();
        $cv_reference->cv_id = $request->input('cv_id');
        $cv_reference->name = $request->input('name');
        $cv_reference->last_name = $request->input('last_name');
        $cv_reference->email = $request->input('email');
        $cv_reference->position = $request->input('position');
        $cv_reference->phone = $request->input('phone');
        $cv_reference->company = $request->input('company');
        $cv_reference->validate_date = $request->input('validate_date');
        $cv_reference->note = $request->input('note');
        $cv_reference->save();

        return redirect(route('cv.show', ['cv' => $request->input('cv_id'), 'resource' => $request->input('cv_res_id')]))->with('flash_success', 'CV Reference added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CVReference  $cVReference
     */
    public function show(Request $request, $id): View
    {
        $cv_reference = CVReference::find($id);

        $parameters = [
            'cv_reference' => $cv_reference,
            'cv_id' => $request->input('cvid'),
            'cv_res_id' => $request->input('res_id'),
        ];

        return view('cv.references.show')->with($parameters);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CVReference  $cVReference
     */
    public function edit(Request $request, $id): View
    {
        $cv_reference = CVReference::find($id);

        $parameters = [
            'cv_reference' => $cv_reference,
            'cv_id' => $request->input('cvid'),
            'cv_res_id' => $request->input('res_id'),
        ];

        return view('cv.references.edit')->with($parameters);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\CVReference  $cVReference
     */
    public function update($id, CVReferenceRequest $request): RedirectResponse
    {
        $cv_reference = CVReference::find($id);

        $cv_reference->name = $request->input('name');
        $cv_reference->last_name = $request->input('last_name');
        $cv_reference->email = $request->input('email');
        $cv_reference->position = $request->input('position');
        $cv_reference->phone = $request->input('phone');
        $cv_reference->company = $request->input('company');
        $cv_reference->validate_date = $request->input('validate_date');
        $cv_reference->note = $request->input('note');
        $cv_reference->save();

        return redirect(route('cv.show', ['cv' => $request->input('cv_id'), 'resource' => $request->input('cv_res_id')]))->with('flash_success', 'CV Reference updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CVReference  $cVReference
     */
    public function destroy($id, Request $request): RedirectResponse
    {
        CVReference::destroy($id);

        return redirect(route('cv.show', ['cv' => $request->input('cv_id'), 'resource' => $request->input('res_id')]))->with('flash_success', 'CV Reference deleted successfully');
    }
}
