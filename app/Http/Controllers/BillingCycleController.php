<?php

namespace App\Http\Controllers;

use App\Models\BillingCycle;
use App\Models\BillingPeriod;
use App\Http\Requests\BillingCycleRequest;
use App\Services\BillingCycleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class BillingCycleController extends Controller
{
    public function index(BillingCycleService $service): View
    {
        abort_unless($service->hasViewPermission(), 403);

        $items = \request()->s ?? 15;
        $billing_cycles = BillingCycle::with(['status:id,description'])->where('status_id', 1)
            ->when(request()->q, function ($cycle) {
                $cycle->where('name', 'like', '%'.request()->q.'%')
                    ->orWhere('description', '%'.request()->q.'%')
                    ->orWhere('max_weeks', '%'.request()->q.'%');
            });

        if (! \request()->s) {
            $billing_cycles = $billing_cycles->paginate($billing_cycles->get()->count());
        } else {
            $billing_cycles = $billing_cycles->paginate($items);
        }

        $parameters = [
            'permission' => $service->hasCreatePermissions(),
            'billing_cycles' => $billing_cycles,
        ];

        return view('billing_cycle.index')->with($parameters);
    }

    public function create(BillingCycleService $service): View
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $parameters = [
            'max_weeks_dropdown' => $service->yearWeekNumbers(),
        ];

        return view('billing_cycle.create')->with($parameters);
    }

    public function store(BillingCycleRequest $request, BillingCycle $billing_cycle, BillingCycleService $service): RedirectResponse
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $billing_cycle = $service->createBillingPeriod($request, $billing_cycle);

        activity()->on($billing_cycle)->withProperties(['full_name' => auth()->user()->name()])->log('created');

        return redirect()->route('billing.show', $billing_cycle)->with('flash_success', 'Billing cycle created successfully');
    }

    public function show(BillingCycle $billing, BillingCycleService $service): View|JsonResponse
    {
        abort_unless($service->hasViewPermission(), 403);

        $billing = $billing->load(['billingPeriods' => function ($period) {
            $period->orderBy('start_date');
        }, 'status' => function ($query) {
            $query->select('id', 'description');
        }]);

        $parameters = [
            'billing_cycle' => $billing,
            'can_create' => $service->hasCreatePermissions(),
            'can_update' => $service->hasUpdatePermissions(),
        ];

        if (request()->ajax()) {
            return response()->json(['billing_cycle' => $billing]);
        }

        return view('billing_cycle.show')->with($parameters);
    }

    public function edit(BillingCycle $billing, BillingCycleService $service): View
    {
        abort_unless(($service->hasUpdatePermissions() || $billing->is_standard), 403);

        $parameters = [
            'billing_cycle' => $billing->load('billingPeriods'),
            'weeks_dropdown' => $service->yearWeekNumbers(),
        ];

        return view('billing_cycle.edit')->with($parameters);
    }

    public function update(BillingCycleRequest $request, BillingCycle $billing, BillingCycleService $service): RedirectResponse
    {
        abort_unless(($service->hasUpdatePermissions() || $billing->is_standard), 403);

        $billing = $service->updateBillingPeriod($request, $billing);

        activity()->on($billing)->withProperties(['full_name' => auth()->user()->name()])->log('updated');

        return redirect()->route('billing.show', $billing)->with('flash_success', 'Billing cycle updated successfully');
    }

    public function destroy(BillingCycle $billing, BillingCycleService $service): JsonResponse
    {
        abort_unless($service->hasUpdatePermissions(), 403);

        activity()->on($billing)->withProperties(['full_name' => auth()->user()->name()])->log('deleted');

        BillingPeriod::where('billing_cycle_id', $billing->id)->delete();

        $billing->delete();

        return response()->json(['message' => 'Billing cycle was deleted successfully']);
    }

    public function duplicate(BillingCycle $billing, BillingCycleService $service): RedirectResponse
    {
        abort_unless($service->hasCreatePermissions(), 403);

        $billing_cycle = $billing->replicate();
        $billing_cycle->name = 'Copy of '.$billing->name;
        $billing_cycle->description = 'Copy of '.$billing->description;
        $billing_cycle->save();

        $billing_periods = $billing->load('billingPeriods');

        foreach ($billing_periods->billingPeriods as $period) {
            $billing_period = new BillingPeriod();
            $billing_period->billing_cycle_id = $billing_cycle->id;
            $billing_period->year = $period->year;
            $billing_period->period_name = $period->period_name;
            $billing_period->period_number = $period->period_number;
            $billing_period->start_date = $period->start_date;
            $billing_period->end_date = $period->end_date;
            $billing_period->save();
        }

        return redirect()->route('billing.edit', $billing_cycle)->with('flash_success', 'Billing Cycle Duplicated Successfully');
    }
}
