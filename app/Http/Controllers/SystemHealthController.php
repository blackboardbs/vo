<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SystemHealthCategory;
use App\Models\SystemHealth;
use App\Models\SystemHealthView;
use App\Models\Module;
use App\Models\ModuleRole;
use Illuminate\Support\Facades\Auth;
use App\Models\Config;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class SystemHealthController extends Controller
{
    public function index(Request $request): View
    {
        $module = Module::where('name', '=', 'App\Models\SystemHealth')->first();

        abort_unless(Auth::user()->canAccess($module->id, 'view_all') || Auth::user()->canAccess($module->id, 'view_team'), 403);

        $config = Config::first();
// dd($config); 
        $systems = SystemHealthCategory::orderBy('id','asc')->get();

        $kpis = SystemHealth::all();

        $total = $kpis->filter(function($item){ return $item; })->sum('health_score_01');

        $kpi_calculated_score = 0;

        $measures = SystemHealthView::pluck('measure','kpi_id');

        foreach($kpis as $kpi){
            if(isset($measures[$kpi->id]) && ($measures[$kpi->id] >= $config->sh_good_min && $measures[$kpi->id] <= $config->sh_good_max)){
                $kpi_calculated_score = $kpi_calculated_score + $kpi->health_score_01;
            }
            if(isset($measures[$kpi->id]) && ($measures[$kpi->id] >= $config->sh_fair_min && $measures[$kpi->id] <= $config->sh_fail_max)){
                $kpi_calculated_score = $kpi_calculated_score + $kpi->health_score_02;
            }
            if(isset($measures[$kpi->id]) && ($measures[$kpi->id] >= $config->sh_bad_min && $measures[$kpi->id] <= $config->sh_bad_max)){
                $kpi_calculated_score = $kpi_calculated_score + $kpi->health_score_03;
            }
        }

        $parameters = [
            'systems' => $systems,
            'total' => $total,
            'calc' => $kpi_calculated_score
        ];

        return View('system_health.index', $parameters);
    }

    public function getCategoryList(Request $request,$category){

        $category = SystemHealth::where('category',$category)->get();

        $parameters = [
            'data' => $category,
            'measures' => SystemHealthView::pluck('measure','kpi_id')
        ];

        return response()->json($parameters);
    }
    
    public function fixKpi(Request $request,$category){

        $category = SystemHealth::find($category);

            try{
                $query = DB::select((String)$category->fix);
            } catch(\Illuminate\Database\QueryException $ex){ 
                  }

        $parameters = [
            'data' => $category->category
        ];

        return response()->json($parameters);
    }
}
