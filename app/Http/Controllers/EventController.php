<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Event;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Project;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request): View
    {
        $item = $request->input('s') ?? 15;

        $event = Event::where('status', '=', 1)->sortable('id', 'desc');

        if ($request->has('q') && $request->input('q') != '') {
            $event = $event->where('event', 'like', '%'.$request->input('q').'%')
                ->orWhere('row_order', 'like', '%'.$request->input('q').'%')
                ->orWhere('event_date', 'like', '%'.$request->input('q').'%')
                ->orWhere('id', 'like', '%'.$request->input('q').'%')
                ->orWhereHas('statusd', function ($query) use ($request) {
                    $query->where('description', 'like', '%'.$request->q.'%');
                });
        }

        $event = $event->sortable('row_order', 'asc')->paginate($item);

        $parameters = [
            'event' => $event,
        ];

        return View('master_data.event.index', $parameters);
    }

    public function create(): View
    {
        $roles_drop_down = Role::orderBy('name')->pluck('name', 'id');
        $teams_drop_down = Team::orderBy('team_name')->pluck('team_name', 'id');
        $projects_drop_down = Project::orderBy('name')->pluck('name', 'id')->whereNotIn('status_id', [4, 5]);
        $users_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id');

        $parameters = [
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
            'roles_drop_down' => $roles_drop_down,
            'teams_drop_down' => $teams_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'users_drop_down' => $users_drop_down,
        ];

        return View('master_data.event.create')->with($parameters);
    }

    public function store(StoreEventRequest $request): RedirectResponse
    {
        $event = new Event();
        $event->event = $request->input('description');
        $event->event_date = $request->input('event_date');
        $row_order = Event::orderBy('row_order', 'desc')->first();
        $event->row_order = isset($row_order->row_order) ? ++$row_order->row_order : 0;
        /*$event->details = $request->input('status');
        $event->roles = $request->input('status');
        $event->projects = $request->input('status');
        $event->teams = $request->input('status');
        $event->users = $request->input('status');*/
        $event->status = $request->input('status');
        $event->creator_id = auth()->id();
        $event->save();

        return redirect(route('event.index'))->with('flash_success', 'Master Data Leave Type captured successfully');
    }

    public function show(Request $request, $event_id): View
    {
        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $event = Event::find($event_id);

        $team_ids = explode(',', $event->team_ids);
        $users_ids = explode(',', $event->user_ids);
        $projects_ids = explode(',', $event->project_ids);
        $roles_ids = explode(',', $event->role_ids);

        $teams = Team::whereIn('id', $team_ids)->get();
        $users = User::whereIn('id', $users_ids)->get();
        $projects = Project::whereIn('id', $projects_ids)->get();
        $roles = Role::whereIn('id', $roles_ids)->get();
        $documents = Document::where('reference_id', $event_id)->where('document_type_id', 9)->get();

        $parameters = [
            'event' => $event,
            'path' => $path,
            'teams' => $teams,
            'users' => $users,
            'projects' => $projects,
            'roles' => $roles,
            'documents' => $documents,
        ];

        return View('master_data.event.show')->with($parameters);
    }

    public function edit($event_id): View
    {
        $parameters = [
            'event' => Event::where('id', '=', $event_id)->get(),
            'status_dropdown' => Status::orderBy('id')->orderBy('order')->pluck('description', 'id')->prepend('Status', '0'),
        ];

        return View('master_data.event.edit')->with($parameters);
    }

    public function update(UpdateEventRequest $request, $event_id): RedirectResponse
    {
        $event = Event::find($event_id);
        $event->event = $request->input('description');
        $event->event_date = $request->input('event_date');
        $event->row_order = $event->row_order;
        $event->status = $request->input('status');
        $event->creator_id = auth()->id();
        $event->save();

        return redirect(route('event.index'))->with('flash_success', 'Master Data Leave Type saved successfully');
    }

    public function destroy($event_id): RedirectResponse
    {
        $event = Event::find($event_id);
        $event->status = 0;
        $event->save();

        return redirect()->route('event.index')->with('success', 'Master Data Leave Type deleted successfully');
    }
}
