<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\API\Clockify\ClockifyAPI;
use App\Models\Board;
use App\Models\BugTask;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Document;
use App\Models\Epic;
use App\Models\Feature;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\InvoiceStatus;
use App\Jobs\SendMessageJob;
use App\Jobs\SendTaskMailJob;
use App\Models\Module;
use App\Models\Notification;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\Risk;
use App\Models\Sprint;
use App\Models\StorePermission;
use App\Models\Task;
use App\Models\Assignment;
use App\Models\TaskDeliveryType;
use App\Models\TaskNote;
use App\Models\TaskStatus;
use App\Models\TaskType;
use App\Models\Team;
use App\Models\Timeline;
use App\Models\Timesheet;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\UserStory;
use App\Models\Config;
use App\Timesheet\TimesheetHelperTrait;
use App\Utils;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class TaskController extends Controller
{
    use TimesheetHelperTrait;

    private $utils;

    public function __construct()
    {
        // $this->middleware('auth');
        $this->utils = new Utils();
    }

    public function index(Request $request)
    {
        $project_id = $request->input('project_id');

        // $item = $request->input('s') ?? 15;

        // $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant', 'bugTask','timelines')->withSum('timelines', 'total')->withSum('timelines', 'total_m')->where('project_id', '=', $project_id)->sortable(['description' => 'desc']); //->paginate($item);

        $module = Module::where('name', '=', \App\Models\Task::class)->get();

        if (auth()->user()->canAccess($module[0]->id, 'view_all') || auth()->user()->canAccess($module[0]->id, 'view_team')) {
            if (! auth()->user()->canAccess($module[0]->id, 'view_all') && auth()->user()->canAccess($module[0]->id, 'view_team')) {
                $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $project_id)->whereIn('employee_id', auth()->user()->team())->sortable(['description' => 'desc']); //->paginate($item);
            }
        } else {
            return abort(403);
        }
        $project = Project::find($project_id);

        if (! isset($project)) {
            return redirect()->route('project.index')->with('flash_info', 'Please select a project');
        }

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);
        $parameters = [
            'project_id' => $project_id,
            'project' => $project,
            'permissions' => $permissions,
        ];
        
        if ($request->has('export')) return $this->export($parameters, 'task');

        return view('task.index', $parameters);
    }

    public function create(Request $request, $project_id)
    {
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id) ?? [];

        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Tasks' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }

        $userStoryID = null;
        if ($request->has('us_id')) {
            $userStoryID = $request->has('us_id');
        }
        $project = Project::find($project_id);
        $customer = Customer::find($project->customer_id);
        $consultants_ids = Assignment::where('project_id', '=', $project->id)->pluck('employee_id');
        $task_number = Task::max('id');
        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $task_type_drop_down = TaskType::orderby('name')->pluck('name', 'id');
        $invoice_status_drop_down = InvoiceStatus::orderby('description')->pluck('description', 'id');
        $project_drop_down = Project::orderby('name')->where('id', '=', $project_id)->pluck('name', 'id');
        $customer_drop_down = Customer::orderby('customer_name')->where('id', '=', $project->customer_id)->pluck('customer_name', 'id');
        $dependency_drop_down = Task::where('project_id', '=', $project->id)->orderby('description')->pluck('description', 'id')->prepend('Select Dependency', 0);
        $userStoryDropDown = UserStory::orderby('name')->pluck('name', 'id')->prepend('Select User Story', 0);
        $task_delivery_type_drop_down = TaskDeliveryType::orderby('name')->pluck('name', 'id');
        $group_task_id = Task::max('grouptask_id');

        $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $project_id)->sortable(['description' => 'desc'])->get();

        $epics = Epic::with('features.user_stories')->where('project_id', '=', $project_id)->get();

        $userStories = [];
        foreach ($epics as $epic) {
            foreach ($epic->features as $feature) {
                foreach ($feature->user_stories as $user_story) {
                    $userStories[$user_story->id] = $user_story->id.' - '.$user_story->name;
                }
            }
        }

        $project_id_ = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id_ = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $featuressArray = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('id')->toArray();
            $userStoryDropDown = UserStory::orderBy('name')->where('status_id', '!=', 5)->whereIn('feature_id', $featuressArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $parameters = [
            'sprintDropDown' => $sprintDropDown,
            'userStories' => $userStories,
            'userStoryID' => $userStoryID,
            'project_id' => $project_id,
            'project_id_' => $project_id_,
            'customer' => $customer,
            'userStoryDropDown' => $userStoryDropDown,
            'task_delivery_type_drop_down' => $task_delivery_type_drop_down,
            'task_number' => $task_number == null ? 1 : $task_number + 1,
            'project' => $project,
            'consultant_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants_ids)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'status_drop_down' => $status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'project_drop_down' => $project_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'dependency_drop_down' => $dependency_drop_down,
            'group_task_id' => isset($group_task_id) ? $group_task_id + 1 : 1,
            'tasks' => $tasks,
            'display_on_kanban_dropdown' => Task::KANBAN_DISPLAY,
        ];

        if ($request->is('bug/*')) {
            $parameters['bug'] = true;
        }

        return view('task.create')->with($parameters);
    }

    public function store(StoreTaskRequest $request, ClockifyAPI $clockify): RedirectResponse
    {
        if ($request->has('user_story_id')) {
            $user_story = UserStory::find($request->user_story_id, ['status_id']);
            if ($user_story->status_id == 4) {
                return redirect(route('project.show', $request->input('project_id')))->with('flash_info', 'You can\'t add a new task to a completed user story.');
            }
        }
        // dd($request);
        $task = new Task();
        $task->description = $request->input('description');
        $task->dependency = $request->input('dependency');
        $task->parent_activity_id = $request->input('parent_activity');
        // $task->task_type_id = $request->input('type_type_id');
        $task->employee_id = $request->input('consultant');
        $task->customer_id = $request->input('customer');
        $task->project_id = $request->input('project');
        $task->start_date = $request->input('start_date');
        $task->end_date = $request->input('end_date');
        $task->hours_planned = $request->input('hours_planned');
        $task->status = $request->input('status');
        $task->billable = $request->input('billable');
        $task->user_story_id = $request->input('user_story_id');
        $task->sprint_id = $request->input('sprint_id');
        $task->on_kanban = $request->input('on_kanban');
        $task->weight = $request->input('weight');
        $task->note_1 = $request->input('note');
        $task->invoice_date = $request->input('invoice_date');
        $task->milestone = $request->input('milestone');
        $task->invoice_ref = $request->input('invoice_ref');
        $task->invoice_status = $request->has('invoice_status') && $request->input('invoice_status') > 0 ? $request->input('invoice_status') : 0;
        $task->task_delivery_type_id = $request->input('task_delivery_type_id');
        $task->creator_id = auth()->id();
        $task->save();

        if ($request->has('bug')) {
            $bug = new BugTask();
            $bug->severity_level = $request->input('severity_level');
            $bug->ref_number = $request->input('ref_number');
            $bug->task_id = $task->id;
            $bug->save();
        }

        $taskUserEmail = isset($task->consultant) ? $task->consultant->email : '';
        $notification = new Notification;
        $notification->name = 'New Task has been created';
        $notification->link = route('task.show', $task->id);
        $notification->save();

        $user_notifications = [];

        array_push($user_notifications, [
            'user_id' => $task->id,
            'notification_id' => $notification->id,
        ]);

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);
        $utils = new Utils();

        try {
            SendTaskMailJob::dispatch($taskUserEmail, $task, $utils->helpPortal)->delay(now()->addMinutes(5));
        } catch (\Exception $exception) {
            return redirect(route('task.index', ['project_id' => $task->project_id]))->with('flash_danger', 'Task '.$task->id.' captured successfully but failed to send an email with error: '.$exception->getMessage());
        }

        if (env('CLOCKIFY_ENABLED', false)){
            $clockify_project_id = $clockify->projectId($request->input('project'));

            try {
                if ($clockify_project_id) {
                    $clockify->post('/projects/'.$clockify_project_id.'/tasks', ['name' => $task->id.' - '.$task->description]);
                }
            } catch (\Exception $exception) {
                logger($exception->getMessage());
            }
        }

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Task created successfully.');
        }

        if ($request->from == 'timeline') {
            return redirect()->back()->with(['task_id' => $task->id])->with('flash_success', 'Task captured successfully');
        }

        return redirect(route('task.index', ['project_id' => $task->project_id]))->with('flash_success', 'Task captured successfully');
    }

    public function edit($task_id, Request $request)
    {
        $task = Task::find($task_id);
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $task->project_id);

        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Tasks' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }

        $project = Project::find($task->project_id);
        $customer = Customer::find($project->customer_id);
        $consultants_ids = Assignment::where('project_id',$task->project_id)->pluck('employee_id');
        $task_number = Task::max('id');
        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $task_type_drop_down = TaskType::orderby('name')->pluck('name', 'id');
        $invoice_status_drop_down = InvoiceStatus::orderby('description')->pluck('description', 'id');
        $project_drop_down = Project::orderby('name')->where('id', '=', $project->id)->pluck('name', 'id');
        $customer_drop_down = Customer::orderby('customer_name')->where('id', '=', $project->customer_id)->pluck('customer_name', 'id');
        $dependency_drop_down = Task::where('project_id', '=', $project->id)->orderby('description')->pluck('description', 'id')->prepend('Select Dependency', 0);
        $userStoryDropDown = UserStory::orderby('name')->where('status_id', '!=', 5)->pluck('name', 'id')->prepend('Select User Story', 0);
        $task_delivery_type_drop_down = TaskDeliveryType::orderby('name')->pluck('name', 'id');
        $group_task_id = Task::max('grouptask_id');

        $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $project->id)->where('id', '!=', $task->id)->sortable(['description' => 'desc'])->get();

        $epics = Epic::with('features.user_stories')->where('project_id', '=', $project->id)->get();

        $userStories = [];
        foreach ($epics as $epic) {
            foreach ($epic->features as $feature) {
                foreach ($feature->user_stories as $user_story) {
                    $userStories[$user_story->id] = $user_story->id.' - '.$user_story->name;
                }
            }
        }

        $project_id = null;
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');

            $epicsArray = Epic::where('project_id', '=', $project_id)->pluck('id')->toArray();
            $featuressArray = Feature::orderBy('name')->whereIn('epic_id', $epicsArray)->pluck('id')->toArray();
            $userStoryDropDown = UserStory::orderBy('name')->whereIn('feature_id', $featuressArray)->pluck('name', 'id')->prepend('Please select...', 0);
        }

        $sprintDropDown = Sprint::orderBy('name')->pluck('name', 'id')->prepend('Please select...', 0);

        $parameters = [
            'userStories' => $userStories,
            'sprintDropDown' => $sprintDropDown,
            'task' => $task,
            'tasks' => $tasks,
            'project_id' => $project_id,
            'userStoryDropDown' => $userStoryDropDown,
            'task_delivery_type_drop_down' => $task_delivery_type_drop_down,
            'customer' => $customer,
            'task_number' => $task_number == null ? 1 : $task_number + 1,
            'project' => $project,
            'consultant_drop_down' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants_ids)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'status_drop_down' => $status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'invoice_status_drop_down' => $invoice_status_drop_down,
            'project_drop_down' => $project_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'dependency_drop_down' => $dependency_drop_down,
            'group_task_id' => isset($group_task_id) ? $group_task_id + 1 : 1,
            'show_on_kanban_dropdown' => Task::KANBAN_DISPLAY,
        ];

        if ($request->is('bug/*')) {
            $parameters['bug'] = true;
        }

        return view('task.edit')->with($parameters);
    }

    public function update(UpdateTaskRequest $request, $task_id): RedirectResponse
    {
        $task = Task::find($task_id);
        $task->description = $request->input('description');
        $task->dependency = $request->input('dependency');
        $task->grouptask_id = $request->input('group_task');
        $task->parent_activity_id = $request->input('parent_activity');
        $task->task_type_id = $request->input('task_type_id');
        $task->employee_id = $request->input('consultant');
        $task->customer_id = $request->input('customer');
        $task->user_story_id = $request->input('user_story_id');
        $task->sprint_id = $request->input('sprint_id');
        $task->goal_id = $request->input('goal_id');
        $task->emote_id = $request->input('emote_id')??null;
        $task->on_kanban = $request->input('on_kanban');
        $task->project_id = $request->input('project');
        $task->start_date = $request->input('start_date');
        $task->end_date = $request->input('end_date');
        $task->hours_planned = $request->input('hours_planned');
        $task->status = $request->input('status');
        $task->billable = $request->input('billable');
        $task->note_1 = $request->input('note');
        $task->invoice_date = $request->input('invoice_date');
        $task->milestone = $request->input('milestone');
        $task->invoice_ref = $request->input('invoice_ref');
        $task->task_delivery_type_id = $request->input('task_delivery_type_id');
        $task->invoice_status = $request->has('invoice_status') && $request->input('invoice_status') > 0 ? $request->input('invoice_status') : 0;
        $task->creator_id = auth()->id();
        $task->save();

        if ($request->has('bug')) {
            $bug = $task->bugTask;
            $bug->severity_level = $request->input('severity_level');
            $bug->ref_number = $request->input('ref_number');
            $bug->save();
        }

        $taskUserEmail = isset($task->consultant) ? $task->consultant->email : '';

        $notification = new Notification;
        $notification->name = 'Updated Task';
        $notification->link = route('task.show', $task->id);
        $notification->save();

        $user_notifications = [];

        array_push($user_notifications, [
            'user_id' => $task->id,
            'notification_id' => $notification->id,
        ]);

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);
        $utils = new Utils();

        SendTaskMailJob::dispatch($taskUserEmail, $task, $utils->helpPortal)->delay(now()->addMinutes(5));

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Task updated successfully.');
        }

        return redirect('task?project_id='.$request->input('project'))->with('flash_success', 'Task updated successfully');
    }

    public function saveTask(Request $request)
    {
        if ($request->has('user_story_id')) {
            $user_story = UserStory::find($request->user_story_id, ['status_id']);

            // if ($user_story->status_id == 4) {
            //     return response()->json(['message' => "You can't add a new task to a completed user story"]);
            // }
        }
        $task = new Task();
        $task->description = $request->input('description');
        $task->employee_id = $request->input('employee_id');
        $task->project_id = $request->input('project_id');
        $task->customer_id = $request->input('customer_id');
        $task->sprint_id = $request->input('sprint_id');
        $task->goal_id = $request->input('goal_id');
        $task->user_story_id = $request->input('user_story_id');
        $task->dependency = $request->input('dependency');
        $task->status = $request->input('status');
        $task->note_1 = $request->input('note_2');
        $task->start_date = Carbon::parse($request->input('start_date'))->format('Y-m-d');
        $task->end_date = Carbon::parse($request->input('end_date'))->format('Y-m-d');
        $task->billable = $request->input('billable');
        $task->on_kanban = $request->input('on_kanban');
        $task->hours_planned = $request->input('hours_planned');
        $task->weight = $request->input('weight');
        $task->priority_level = $request->input('priority');
        $task->remaining_time = $request->input('remaining_time');
        $task->definition_of_ready = $request->input('ready');
        $task->definition_of_done = $request->input('done');
        $task->creator_id = auth()->id();
        $task->save();

        $old_selected_tasks = explode(',', $request->selected_tasks);

        if ($request->has('is_bug') && $request->is_bug === true) {
            $bug = new BugTask();
            $bug->severity_level = $request->severity ?? -1;
            $bug->priority_level = $request->priority ?? 0;
            $bug->ref_number = $request->ref ?? '';
            $bug->task_id = $task->id;
            $bug->save();
        }
    

        $selected_tasks = [];
        foreach ($old_selected_tasks as $selected_task) {
            $selected_tasks[] = $selected_task;
        }
        $selected_tasks[] = $task->id;

        if ($request->hasFile('new_file')) {
            if (isset($request->new_file) && $request->new_file != null) {
                $file = 'task_'.date('Y_m_d_H_i_s').'.'.$request->new_file->getClientOriginalExtension();
                $fileName = $request->new_file->getClientOriginalName();
                $request->new_file->move(public_path('files/tasks'), $file);

                $document = new Document();
                $document->name = $fileName;
                $document->type_id = 10;
                $document->file = $file;
                $document->document_type_id = 10;
                $document->reference = 'Task';
                $document->reference_id = $task->id;
                $document->owner_id = auth()->id();
                $document->creator_id = auth()->id();
                $document->status_id = 1;
                $document->save();
            }
        }

        $taskUserEmail = isset($task->consultant) ? $task->consultant->email : '';

        $notification = new Notification;
        $notification->name = 'Updated Task';
        $notification->link = route('task.show', $task->id);
        $notification->save();

        $user_notifications = [];

        array_push($user_notifications, [
            'user_id' => $task->id,
            'notification_id' => $notification->id,
        ]);

        //link notification to the user_id of manager
        UserNotification::insert($user_notifications);
        $utils = new Utils();

        SendTaskMailJob::dispatch($taskUserEmail, $task, $utils->helpPortal);

        $tasks = Task::with(['consultant' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        },
            'bug_task' => function ($bug) {
                return $bug->select('task_id', 'severity_level','priority_level', 'ref_number');
            }, 'project' => function ($project) {
                return $project->select(['id', 'name', 'ref']);
            },'taskstatus','sprint' ])->withSum('timelines', 'total')->withSum('timelines', 'total_m')->where('id',$task->id)->first();

        return response()->json(['message' => 'Task created successfully', 'selected_tasks' => $selected_tasks,'task'=>$tasks]);
    }

    
    public function copy($task_id)
    {

        $task_old = Task::find($task_id);
        $task_bug_old = BugTask::where('task_id',$task_id)->first();

        $task = new Task();
        $task->description = $task_old->description.' - copy';
        $task->dependency = $task_old->dependency;
        $task->parent_activity_id = $task_old->parent_activity_id;
        $task->task_type_id = $task_old->task_type_id;
        $task->employee_id = $task_old->employee_id;
        $task->customer_id = $task_old->customer_id;
        $task->sprint_id = $task_old->sprint_id;
        $task->goal_id = $task_old->goal_id;
        $task->invoice_date = $task_old->invoice_date;
        $task->invoice_ref = $task_old->invoice_ref;
        $task->invoice_status = $task_old->invoice_status;
        $task->milestone = $task_old->milestone;
        $task->project_id = $task_old->project_id;
        $task->start_date = now()->toDateString();
        $task->end_date = now()->toDateString();
        $task->hours_planned = $task_old->hours_planned;
        $task->user_story_id = $task_old->user_story_id;
        $task->status = 2;
        $task->billable = $task_old->billable;
        $task->on_kanban = $task_old->on_kanban;
        $task->note_1 = $task_old->note_1;
        $task->weight = $task_old->weight;
        $task->priority_level = $task_old->priority_level;
        $task->remaining_time = $task_old->remaining_time;
        $task->task_delivery_type_id = $task_old->task_delivery_type_id;
        $task->definition_of_ready = $task_old->definition_of_ready;
        $task->definition_of_done = $task_old->definition_of_done;
        $task->creator_id = auth()->id();
        $task->save();

        if($task_bug_old){
            $bug = new BugTask();
            $bug->ref_number = $task_bug_old->ref_number;
            $bug->severity_level = $task_bug_old->severity_level;
            $bug->priority_level = $task_bug_old->priority_level;
            $bug->task_id = $task->id;
            $bug->save();
        }

        $tasks = Task::select(['id', 'description', 'employee_id', 'emote_id', 'project_id', 'status','start_date','end_date'])
        ->with(['consultant' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        },
            'bugTask' => function ($bug) {
                return $bug->select('task_id', 'severity_level', 'ref_number');
            }, 'project' => function ($project) {
                return $project->select(['id', 'name', 'ref']);
            }, ])->where('id',$task->id)->get();

        if (\request()->ajax())
            return response()->json(['task' => $tasks]);

        return redirect()->route('task.edit', $task->id)->with('success','Task copied successfully');

    }

    public function move($task_id, Request $request): JsonResponse
    {
        $task = Task::find($task_id);

        $children = Task::where('parent_activity_id', '=', $task->id)->get();

        foreach ($children as $child) {
            $update_child = Task::find($child->id);
            $update_child->status = $request->input('status');
            $update_child->save();
        }

        $task->status = $request->input('status');
        $task->save();

        return response()->json(['message' => 'Task updated successfully']);
    }

    public function updateUserStory($task_id, Request $request)
    {
        $user_story = UserStory::with(['feature','feature.epic'])->where('id',$request->input('user_story'))->get();
        
        $task = Task::find($task_id);
        if($user_story[0]->feature->epic->project_id != $task->project_id){
            return response()->json(['error'=>'yes','message' => "You can't move a task between projects."]);
        }
        $task->user_story_id = $request->input('user_story');
        $task->status = $request->input('status');
        $task->save();

        return response()->json(['message' => 'Task updated successfully']);
    }

    public function updatePriority($task_id, Request $request)
    {
        $task = Task::find($task_id);
        $task->priority_level = $request->input('priority');
        $task->status = $request->input('status');
        $task->save();

        BugTask::where('task_id',$task_id)->update(['priority_level'=>$request->priority]);

        return response()->json(['message' => 'Task updated successfully']);
    }

    public function updateConsultant($task_id, Request $request)
    {        
        $task = Task::find($task_id);
        if($task->employee_id != $request->input('employee_id')){
            $assignments = Assignment::where('project_id',$task->project_id)->get(['employee_id']);
            $user = User::where('id',$request->employee_id)->whereIn('id',$assignments)->get();
            if(count($user) == 0){
                return response()->json(['error'=>'yes','message' => "You can't move a task to a consultant not assigned to the project associated with the selected task."]);
            } else {
                $timelines = Task::with('timelines')->withSum('timelines','total')->withSum('timelines','total_m')->where('id',$task_id)->first();
                if($timelines->timelines_sum_total > 0 && $timelines->timelines_sum_total_m > 0){
                    $task_old = Task::find($request->task_id);

                    $ntask = new Task();
                    $ntask->description = $task_old->description;
                    $ntask->dependency = $task_old->dependency;
                    $ntask->parent_activity_id = $task_old->parent_activity_id;
                    $ntask->task_type_id = $task_old->task_type_id;
                    $ntask->employee_id = $request->employee_id;
                    $ntask->customer_id = $task_old->customer_id;
                    $ntask->sprint_id = $task_old->sprint_id;
                    $ntask->goal_id = $task_old->goal_id;
                    $ntask->invoice_date = $task_old->invoice_date;
                    $ntask->invoice_ref = $task_old->invoice_ref;
                    $ntask->invoice_status = $task_old->invoice_status;
                    $ntask->milestone = $task_old->milestone;
                    $ntask->project_id = $task_old->project_id;
                    $ntask->start_date = now()->toDateString();
                    $ntask->end_date = now()->toDateString();
                    $ntask->hours_planned = $task_old->hours_planned;
                    $ntask->user_story_id = $task_old->user_story_id;
                    $ntask->status = $request->status;
                    $ntask->billable = $task_old->billable;
                    $ntask->on_kanban = $task_old->on_kanban;
                    $ntask->note_1 = $task_old->note_1;
                    $ntask->weight = $task_old->weight;
                    $ntask->remaining_time = $task_old->remaining_time;
                    $ntask->definition_of_ready = $task_old->definition_of_ready;
                    $ntask->definition_of_done = $task_old->definition_of_done;
                    $ntask->task_delivery_type_id = $task_old->task_delivery_type_id;
                    $ntask->creator_id = auth()->id();
                    $ntask->save();
                    
                }
            }
        } else {
                    $task->employee_id = $request->employee_id;
                    $task->status = $request->input('status');
                    $task->save();
                }

        return response()->json(['message' => 'Task updated successfully']);
    }

    public function show(Request $request, Project $project): View
    {
        if ($request->headers->get('referer') == route('calendar.index')) {
            $path = '1';
        } else {
            $path = '0';
        }

        $_task = Task::where('project_id', '=', $project->id)->get();

        $task_id = 0;
        if (! empty($_task[0]->id)) {
            $task_id = $_task[0]->id;
        }

        $task = Task::with('consultant', 'customer', 'project', 'taskstatus', 'invoicestatus', 'taskdependency')->find($task_id);

        $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $project->id)->where('id', '!=', $task->id)->sortable(['description' => 'desc'])->get();

        $status_drop_down = TaskStatus::orderBy('description')->pluck('description', 'id');
        $userStoryDropDown = UserStory::orderby('name')->pluck('name', 'id')->prepend('Select User Story', 0);

        $epics = Epic::with('features.user_stories')->where('project_id', '=', $project->id)->get();

        $userStories = [];
        foreach ($epics as $epic) {
            foreach ($epic->features as $feature) {
                foreach ($feature->user_stories as $user_story) {
                    $userStories[$user_story->id] = $user_story->id.' - '.$user_story->name;
                }
            }
        }

        $parameters = [
            'userStories' => $userStories,
            'sprintDropdown' => Sprint::pluck('name','id'),
            'status_drop_down' => $status_drop_down,
            'userStoryDropDown' => $userStoryDropDown,
            'task' => $task,
            'tasks' => $tasks,
            'path' => $path,
            'project' => $project,
        ];

        return view('task.show')->with($parameters);
    }

    public function view(Task $task): View
    {
        $task = $task->load(['consultant', 'customer', 'project', 'taskstatus', 'invoicestatus', 'taskdependency']);
        $project = Project::find($task->project_id);
        $first_name = isset($task->consultant->first_name) ? $task->consultant->first_name : '';
        $last_name = isset($task->consultant->last_name) ? $task->consultant->last_name : '';
        $consultant = $first_name.' '.$last_name;
        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');

        $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $task->project_id)->where('id', '!=', $task->id)->sortable(['description' => 'desc'])->get();

        $epics = Epic::with('features.user_stories')->where('project_id', '=', $task->project_id)->get();

        $userStories = [];
        foreach ($epics as $epic) {
            foreach ($epic->features as $feature) {
                foreach ($feature->user_stories as $user_story) {
                    $userStories[$user_story->id] = $user_story->id.' - '.$user_story->name;
                }
            }
        }

        $task_delivery_type_drop_down = TaskDeliveryType::orderby('name')->pluck('name', 'id');

        // dd($task->user_story->feature->epic->project);

        $parameters = [
            'project' => $project,
            'userStories' => $userStories,
            'sprintDropdown' => Sprint::pluck('name','id'),
            'task_delivery_type_drop_down' => $task_delivery_type_drop_down,
            'task' => $task,
            'tasks' => $tasks,
            'status_drop_down' => $status_drop_down,
            'consultant' => $consultant,
        ];

        return view('task.view')->with($parameters);
    }

    public function boards($project_id): View
    {
        $parameters = [
            'project_id' => $project_id,
        ];

        return view('task.boards')->with($parameters);
    }

    public function getTask($task_id)
    {
        $task = Task::where('id',$task_id)->with('bugTask')->first();
        $taskNotes = TaskNote::with(['user'])->where('task_id', '=', $task_id)->where('status_id', '=', 1)->whereHas('user')->orderBy('id', 'desc')->get();

        return response()->json(['task'=>$task,'discussions'=>$taskNotes]);
    }

    public function addMessage($task_id, Request $request)
    {
        $task = Task::find($task_id);

        if ($task->status == 5) {
            return response()->json(['message' => "You can't add a new note to a completed task"]);
        }

        $taskNote = new TaskNote();
        $taskNote->task_id = $task_id;
        $taskNote->note = $request->input('message');
        $taskNote->creator_id = auth()->user()->id;
        $taskNote->status_id = 1;
        $taskNote->save();

        $project = Project::find($task->project_id);

        $user_ids = explode(',', $project->consultants);

        $users = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'email', 'id')->whereIn('id', $user_ids)->orderBy('first_name')->get();

        if (isset($users)) {
            foreach ($users as $user) {
                if (strpos($taskNote->note, '@'.$user->full_name) !== false) {
                    $preferred_name = 'Klaas';
                    $last_name = 'Rikhotso';
                    $subject = 'Task Notification: Note added to task number: #'.$taskNote->task_id;
                    SendMessageJob::dispatch($user->email, $user->full_name, $subject, $taskNote->note, $task->description, $project->id)->delay(now()->addMinutes(5));
                }
            }
        }

        return ['message' => 'Task note successfully added.'];
    }

    public function getMessages($task_id)
    {
        $taskNotes = TaskNote::with(['user'])->where('task_id', '=', $task_id)->where('status_id', '=', 1)->whereHas('user')->orderBy('id', 'desc')->get();

        return $taskNotes;
    }

    public function getCards(Request $request)
    {
        $request = $request->input('request');

        $tasks = Task::with(['consultant', 'bugTask'])->orderBy('id')->whereIn('id', $request)
            ->where('status', '!=', 5);

        $tasks = $tasks->get();

        $task_statuses = TaskStatus::where('status', '=', 1)->where('id', '!=', 5)->get();

        $cards = null;
        foreach ($task_statuses as $key => $task_status) {
            $cards[$key]['status'] = $task_status;
            foreach ($tasks as $task) {
                if (($task->status == $task_status->id)) {
                    $cards[$key]['tasks'][] = $task;
                }
            }
        }

        return $cards;
    }

    public function storeTask(Request $request): JsonResponse
    {
        if ($request->has('user_story_id')) {
            $user_story = UserStory::find($request->user_story_id, 'status_id');

            if ($user_story->status_id == 5) {
                return response()->json(['message' => "You can't add a task to a completed user story"]);
            }
        }

        $project = Project::find($request->project);

        $task = new Task;
        $task->description = $request->input('description');
        $task->task_type_id = $request->input('task_type_id');
        $task->employee_id = $request->input('consultant');
        $task->customer_id = $project->customer_id;
        $task->project_id = $project->id;
        $task->start_date = Carbon::parse($request->input('new_start_date'))->format('Y-m-d');
        $task->end_date = Carbon::parse($request->input('new_end_date'))->format('Y-m-d');
        $task->hours_planned = $request->input('hours_planned');
        $task->user_story_id = $request->input('user_story_id');
        $task->task_delivery_type_id = $request->input('task_delivery_type_id');
        $task->task_permission_id = $request->input('task_permission_id');
        $task->status = $request->input('status');
        $task->billable = $request->input('billable');
        $task->note_1 = $request->input('note');
        $task->on_kanban = $request->has('on_kanban') ? $request->input('on_kanban') : 0;
        $task->creator_id = auth()->id();
        $task->save();

        $task_type = TaskType::find($task->task_type_id);

        $tasks = Task::with(['consultant' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        },
            'bugTask' => function ($bug) {
                return $bug->select('task_id', 'severity_level','priority_level', 'ref_number');
            }, 'project' => function ($project) {
                return $project->select(['id', 'name', 'ref']);
            },'taskstatus','sprint' ])->withSum('timelines', 'total')->withSum('timelines', 'total_m')->where('id',$task->id)->get();

        return response()->json($tasks);
    }

    public function getBoards()
    {
        $response = response(Board::with('tasks')->get());

        return $response;
    }

    public function treeView($project_id): View
    {
        $tasks = Task::with('tasktype')->where('parent_activity_id', '=', 0)->where('project_id', '=', $project_id)->get();

        $tree = '<div id="mytree" class="bstree">	
                <ul>';

        foreach ($tasks as $task) {
            $level = 0;

            if($task->tasktype){
                $tree .= '<li data-id="'.$task->tasktype->name.'" data-level="'.$level.'"><span>'.$task->description.'</span>';

                if (count($task->childs)) {
                    $tree .= $this->childView($task, $level);
                }

                $tree .= '</li>';
            }
        }
        $tree .= '</ul></div>';
        //return view('task.treeview',compact('tree'));

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'tree' => $tree,
            'projects_drop_down' => Project::orderBy('name')->whereNotNull('name')->pluck('name', 'id')->prepend('All', '-1'),
            'project_id' => $project_id,
            'permissions' => $permissions,
        ];

        return view('task.treeview', $parameters);
    }

    public function treeViewNew($project_id, Request $request)
    {
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');
        }

        $project = Project::with(['tasks.bugTask','tasks.consultant','tasks.sprint','tasks.taskstatus'])->find($project_id);
        $epics = Epic::with(['sprint','status','features.sprint','features.status','features.user_stories.tasks.consultant','features.user_stories.tasks.sprint','features.user_stories.tasks.taskstatus','features.user_stories.tasks.timelines', 'features.user_stories.tasks.bugTask', 'features.user_stories.userStoryDiscussions'])
            ->orderBy('id', 'desc')
            ->where('project_id', '=', $project_id)
            ->when($request->task_id, function ($epic) {
                $epic->where('id', \request()->task_id)
                    ->orWhereHas('features', function ($feature) {
                        $feature->where('id', \request()->task_id)
                            ->orwhereHas('user_stories', function ($user_story) {
                                $user_story->where('id', \request()->task_id)
                                    ->orWhereHas('tasks', function ($task) {
                                        $task->where('id', \request()->task_id);
                                    });
                            });
                    });
            })
            ->when($request->project_id == 0, function ($epic) use ($request) {
                $epic->where('project_id', '>', $request->project_id);
            })->when($request->project_id, function ($epic) use ($request) {
                $epic->where('project_id', $request->project_id);
            })->when($request->resource, function ($epic) use ($request) {
                $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
                    $e->where('employee_id', $request->resource);
                });
            })->when($request->customer, function ($epic) use ($request) {
                $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
                    $e->where('customer_id', $request->customer);
                });
            })->when($request->start_date, function ($epic) use ($request) {
                $epic->where('start_date', '>=', $request->start_date);
            })->when($request->end_date, function ($epic) use ($request) {
                $epic->where('end_date', '<=', $request->end_date);
            })->when($request->status, function ($epic) use ($request) {
                $epic->where('status_id', $request->status);
            })->when($request->sprint, function ($epic) use ($request) {
                $epic->where('sprint_id', $request->sprint);
            })->when($request->team, function ($task) use ($request) {
                $task->whereHas('features.user_stories.tasks.consultant.resource', function ($t) use ($request) {
                    $t->where('team_id', '=', $request->team);
                });
            })->get();

        $module = Module::where('name', '=', \App\Models\Task::class)->get();

        if (auth()->user()->canAccess($module[0]->id, 'view_all') || auth()->user()->canAccess($module[0]->id, 'view_team')) {
            if (! auth()->user()->canAccess($module[0]->id, 'view_all') && auth()->user()->canAccess($module[0]->id, 'view_team')) {
                $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant')->where('project_id', '=', $project_id)->whereIn('employee_id', auth()->user()->team())->sortable(['description' => 'desc']); //->paginate($item);
            }
        } else {
            return abort(403);
        }

        $project_consultants = explode(',', $project->consultants);

        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', 0);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();
        $user_story_status_drop_down = ProjectStatus::orderBy('description')->where('status_id', '=', 1)->pluck('description', 'id');

        $year_week_drop_down = $this->getYearWeek();

        $selected_year_week = '';
        $new_year_week_drop_down = [];
        foreach ($year_week_drop_down as $key => $value) {
            if ($selected_year_week == '') {
                $selected_year_week = $key;
            }

            $new_year_week_drop_down[] = [
                'id' => $key,
                'name' => $value,
            ];
        }

        $hours_drop_down = [];
        $minutes_drop_down = [];
        for ($i = 0; $i < 24; $i++) {
            $hours_drop_down[$i] = $i;
        }

        for ($i = 0; $i < 60; $i++) {
            $minutes_drop_down[$i] = $i;
        }

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'epics' => $epics,
            'project' => $project,
            'status_drop_down' => $status_drop_down,
            'user_story_status_drop_down' => $user_story_status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'sprints_drop_down' => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', '0'),
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
            'user_story_drop_down' => UserStory::orderBy('name')->pluck('name', 'id')->prepend('Select User Story', '0'),
            'task_delivery_type_drop_down' => TaskDeliveryType::orderBy('name')->pluck('name', 'id')->prepend('Select Delivery Type', '0'),
            'permissions_drop_down' => [[1,  'Public'], [2, 'Private']],
            'dependency_drop_down' => Task::orderBy('description')->pluck('description', 'id')->prepend('Select Dependency Task', '0'),
            'year_week_drop_down' => array_values($year_week_drop_down),
            'selected_year_week' => $selected_year_week,
            'hours_drop_down' => $hours_drop_down,
            'minutes_drop_down' => $minutes_drop_down,
        ];

        //return Task::where('project_id', 101)->get();

        return view('task.tree-view-new')->with($parameters);
    }

    public function sprint($project_id, Request $request): View|BinaryFileResponse
    {
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');
        }

        $project = Project::find($project_id);

        $sprints = Sprint::orderBy('name')->where('project_id', '=', $project_id)->get();

        $project_consultants = explode(',', $project->consultants);

        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', 0);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();

        /*$consultants_ids = explode(",", $project->consultants);
        $task_constultants_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $consultants_ids)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        if ($request->has('resource') && $request->input('resource') != '') {
            $tasks = $tasks->where('employee_id', '=', $request->input('resource'));
        }

        if ($request->has('customer') && $request->input('customer') != '') {
            $tasks = $tasks->where('customer_id', '=', $request->input('customer'));
        }

        if ($request->has('team') && $request->input('team') != '') {
            //$tasks = $tasks->where('team_id', '=', $request->input('team'));
        }

        if ($request->has('status') && $request->input('status') != '') {
            $tasks = $tasks->where('status', '=', $request->input('status'));
        }

        if ($request->has('type') && $request->input('type') != '') {
            $tasks = $tasks->where('task_type_id',$request->input('type'));
        }

        if ($request->has('start_date') && $request->input('start_date') != '') {
            $tasks = $tasks->where('start_date', '>=', $request->input('start_date'));
        }

        if ($request->has('end_date') && $request->input('end_date') != '') {
            $tasks = $tasks->where('end_date', '<=', $request->input('end_date'));
        }

        if ($request->has('sprint') && $request->input('sprint') != '') {
            $tasks = $tasks->whereHas('user_story', function ($task) use($request){
                $task->where('sprint_id', $request->sprint);
            });
        }*/

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'project' => $project,
            'sprints' => $sprints,
            'status_drop_down' => $status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'sprints_drop_down' => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', '0'),
            'task_type_drop_down' => $task_type_drop_down,
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
        ];

        if ($request->has('export')) return $this->export($parameters, 'task', 'sprint');

        return view('task.sprint')->with($parameters);
    }

    public function getCustomer($project_id,Request $request){
        $project = Project::find($project_id);
        $customer = Customer::find($project->customer_id);
        
        return response()->json($customer);
    }

    public function risk($project_id, Request $request): View|BinaryFileResponse
    {
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');
        }

        $project = Project::find($project_id);

        $project_consultants = explode(',', $project->consultants);

        $risks = Risk::with('project', 'riskarea', 'status')->orderBy('id', 'desc')->where('project_id', $project_id)->paginate(15);
        $project = Project::find($project_id);
        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', '');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'risks' => $risks,
            'project' => $project,
            'status_drop_down' => $status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'sprints_drop_down' => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', '0'),
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
        ];

        if ($request->has('export')) return $this->export($parameters, 'task', 'risk');

        return view('task.risk')->with($parameters);
    }

    public function action($project_id, Request $request): View
    {
        $actions = Action::orderBy('id', 'desc')->get();
        $project = Project::find($project_id);
        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', '');
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'project' => $project,
            'actions' => $actions,
            'status_drop_down' => $status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
        ];

        return view('task.action')->with($parameters);
    }

    public function gantt($project_id, Request $request)
    {
        $project = Project::find($project_id);

        $gantt_chart = Epic::with('features.user_stories.tasks')
            ->whereProjectId($project_id)
            ->get()->map(function ($epic) {
                return [
                    'epic_id' => $epic->id,
                    'epic_name' => $epic->name,
                    'epic_start_date' => Carbon::parse($epic->start_date),
                    'epic_end_date' => Carbon::parse($epic->end_date),
                    'features' => $epic->features->map(function ($feature) {
                        return [
                            'feature_id' => $feature->id,
                            'feature_name' => $feature->name,
                            'feature_start_date' => Carbon::parse($feature->start_date),
                            'feature_end_date' => Carbon::parse($feature->end_date),
                            'user_stories' => $feature->user_stories->map(function ($story) {
                                return $story->tasks->map(function ($task) {
                                    return [
                                        'task_id' => $task->id,
                                        'task_name' => $task->description,
                                        'task_start_date' => Carbon::parse($task->start_date),
                                        'task_end_date' => Carbon::parse($task->end_date),
                                        'consultant' => substr($task->consultant->first_name ?? '', 0, 1).'. '.$task->consultant->last_name ?? null,
                                        'planned_hours' => $task->hours_planned,
                                        'status' => $task->taskstatus->description ?? null,
                                    ];
                                });
                            }),
                        ];
                    }),
                ];
            });

        $gantt_chart_end_date = '0000-00-00';
        $gantt_chart_start_date = $gantt_chart[0]['epic_start_date'] ?? '0000-00-00';

        foreach ($gantt_chart as $chart) {
            if ($chart['epic_end_date'] > $gantt_chart_end_date) {
                $gantt_chart_end_date = $chart['epic_end_date'];
            }

            if ($chart['epic_start_date'] < $gantt_chart_start_date) {
                $gantt_chart_start_date = $chart['epic_start_date'];
            }
        }

        $project_consultants = explode(',', $project->consultants);

        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', 0);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'project' => $project,
            'gantt_chart' => $gantt_chart,
            'gantt_start_date' => Carbon::parse($gantt_chart_start_date),
            'gantt_last_date' => Carbon::parse($gantt_chart_end_date),
            'status_drop_down' => $status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
            'sprints_drop_down' => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', '0'),
        ];

        return view('task.gantt')->with($parameters);
    }

    public function cost($project_id, Request $request): View
    {
        $project = Project::find($project_id);

        $project_consultants = explode(',', $project->consultants);

        $status_drop_down = TaskStatus::orderby('description')->pluck('description', 'id');
        $projects_drop_down = Project::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Project', 0);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Resource', '');
        $customer_drop_down = Customer::orderBy('customer_name')->whereNotNull('customer_name')->where('customer_name', '!=', '')->pluck('customer_name', 'id')->prepend('Customer', '');
        $team_drop_down = Team::orderBy('team_name')->whereNotNull('team_name')->where('team_name', '!=', '')->pluck('team_name', 'id')->prepend('Team', '');
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('Status', '');
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('Task Type', '');
        $task_statuses = TaskStatus::where('status', '=', 1)->get();

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        $parameters = [
            'project' => $project,
            'status_drop_down' => $status_drop_down,
            'projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'sprints_drop_down' => Sprint::orderBy('name')->pluck('name', 'id')->prepend('Sprint', '0'),
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
            'task_statuses' => $task_statuses,
            'permissions' => $permissions,
        ];

        return view('task.cost')->with($parameters);
    }

    public function childView($task, $level)
    {
        $level += 1;
        $html = '<ul>';
        foreach ($task->childs as $arr) {
            if (count($arr->childs)) {
                $html .= '<li data-id="'.$task->tasktype->name.'" data-level="'.$level.'"><span>'.$task->description.'</span>';
                //$html .='<li class="tree-view closed"><a class="tree-name">'.$arr->description.'</a>';
                $html .= $this->childView($arr, $level);
            } else {
                /*$html .='<li class="tree-view"><a class="tree-name">'.$arr->description.'</a>';
                $html .="</li>";*/
                $html .= '<li data-id="'.$task->tasktype->name.'" data-level="'.$level.'"><span>'.$task->description.'</span>';
            }
            $html .= '</li>';
        }

        $html .= '</ul>';

        return $html;
    }

    public function changeStatus(Request $request): JsonResponse
    {
        $task = Task::find($request->input('id'));

        $children = Task::where('parent_activity_id', '=', $task->id)->get();

        foreach ($children as $child) {
            $update_child = Task::find($child->id);
            $update_child->status = $request->input('value') == '' ? null : $request->input('value');
            $update_child->save();
        }

        $task->status = $request->input('value') == '' ? null : $request->input('value');
        $task->save();

        return response()->json(['message' => 'Status successfully changed']);
    }
    
    public function changeSprint(Request $request): JsonResponse
    {
        $task = Task::find($request->input('id'));

        $children = Task::where('parent_activity_id', '=', $task->id)->get();

        foreach ($children as $child) {
            $update_child = Task::find($child->id);
            $update_child->sprint_id = $request->input('value') == '' ? null : $request->input('value');
            $update_child->save();
        }

        $task->sprint_id = $request->input('value') == '' ? null : $request->input('value');
        $task->save();

        return response()->json(['message' => 'Sprint successfully changed']);
    }
    
    public function changePriority(Request $request): JsonResponse
    {
        $task = Task::find($request->input('id'));

        $bug = BugTask::where('task_id', '=', $task->id)->update(['priority_level'=>$request->input('value')]);

        return response()->json(['message' => 'Priority successfully changed']);
    }

    /*
     * This is a recursive function that re-orders the task by parent child relationship
     * @param
     * $project_id
     * $parent_type_id
     * $parent_child_task pass by reference
     * */
    private function parentChildren($project_id, $parent_type_id, &$parent_child_tasks, Request $request)
    {
        $tasks = Task::where('project_id', '=', $project_id)->where('parent_activity_id', '=', $parent_type_id); //->get();

        $tasks = $this->filterTasks($project_id, $tasks, $request);

        foreach ($tasks as $task) {
            $parent_child_tasks[] = $task;
            $this->parentChildren($project_id, $task->id, $parent_child_tasks, $request);
        }
    }

    private function filterTasks($project_id, &$tasks, Request $request)
    {
        if ($request->has('project') && $request->input('project') != '') {
            $tasks = $tasks->where('project_id', '=', $request->input('project'));
        } elseif ($request->has('project') && $request->input('project') == '') {
        } else {
            $tasks = $tasks->where('project_id', '=', $project_id);
        }

        if ($request->has('resource') && $request->input('resource') != '') {
            $tasks = $tasks->where('employee_id', '=', $request->input('resource'));
        }

        if ($request->has('customer') && $request->input('customer') != '') {
            $tasks = $tasks->where('customer_id', '=', $request->input('customer'));
        }

        if ($request->has('team') && $request->input('team') != '') {
            //$tasks = $tasks->where('team_id', '=', $request->input('team'));
        }

        if ($request->has('status') && $request->input('status') != '') {
            $tasks = $tasks->where('status', '=', $request->input('status'));
        }

        if ($request->has('type') && $request->input('type') != '') {
            $tasks = $tasks->where('task_type_id', $request->input('type'));
        }

        if ($request->has('start_date') && $request->input('start_date') != '') {
            $tasks = $tasks->where('start_date', '>=', $request->input('start_date'));
        }

        if ($request->has('end_date') && $request->input('end_date') != '') {
            $tasks = $tasks->where('end_date', '<=', $request->input('end_date'));
        }

        $tasks = $tasks->get();

        return $tasks;
    }

    public function reports(): View
    {
        return view('reports.task.index');
    }

    public function updateTask(Request $request, Task $task)
    {
        // return $request;
        if (! $request->tree_view) {
            $request = (object) $request->input('request');
        }

        if ($task->status == 5) {
            return response()->json(['message' => "You can't update a completed task"]);
        }
        $task->description = $request->description;
        $task->dependency = $request->dependency;
        $task->employee_id = $request->employee_id;
        $task->customer_id = $request->customer_id;
        $task->user_story_id = $request->user_story_id;
        $task->sprint_id = $request->sprint_id;
        $task->goal_id = $request->goal_id;
        $task->emote_id = $request->emote_id??null;
        $task->project_id = $request->project_id;
        $task->start_date = Carbon::parse($request->start_date)->toDateString();
        $task->end_date = Carbon::parse($request->end_date)->toDateString();
        $task->hours_planned = $request->hours_planned;
        $task->status = $request->status;
        $task->billable = $request->billable;
        if(isset($request->on_kanban) && $request->on_kanban != ''){
        $task->on_kanban = $request->on_kanban;
        }
        $task->note_1 = $request->note_1;
        $task->invoice_date = $request->invoice_date;
        $task->milestone = $request->milestone;
        $task->invoice_ref = $request->invoice_ref;
        $task->invoice_status = $request->invoice_status;
        $task->weight = $request->weight;
        $task->priority_level = $request->priority;
        $task->remaining_time = $request->remaining_time;
        $task->definition_of_ready = $request->definition_of_ready;
        $task->definition_of_done = $request->definition_of_done;
        $task->task_delivery_type_id = $request->task_delivery_type_id;
        $task->task_permission_id = $request->task_permission_id;
        $task->save();

        if($request->is_bug != false){
            BugTask::where('task_id',$task->id)->delete();
            $bug = new BugTask();
            $bug->severity_level = $request->severity??-1;
            $bug->priority_level = $request->priority??0;
            $bug->ref_number = $request->ref??'';
            $bug->task_id = $task->id;
            $bug->save();
        } else {
            BugTask::where('task_id',$task->id)->delete();
        }

        $tasks = Task::with(['consultant' => function ($consultant) {
            return $consultant->select(['id', 'first_name', 'last_name']);
        },
            'bugTask' => function ($bug) {
                return $bug->select('task_id', 'severity_level','priority_level', 'ref_number');
            }, 'project' => function ($project) {
                return $project->select(['id', 'name', 'ref']);
            },'taskstatus','sprint' ])->withSum('timelines', 'total')->withSum('timelines', 'total_m')->where('id',$task->id)->get();

        return response()->json(['message' => 'Task updated successfully','task'=>$tasks]);
    }

    public function uploadFile($task_id, Request $request): JsonResponse
    {
        $file = 'task_'.date('Y_m_d_H_i_s').'.'.$request->file->getClientOriginalExtension();
        $fileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('files/tasks'), $file);

        $document = new Document();
        $document->name = $fileName;
        $document->type_id = 10;
        $document->file = $file;
        $document->document_type_id = 10;
        $document->reference = 'Task';
        $document->reference_id = $task_id;
        $document->owner_id = (isset($request->user_id) ? $request->user_id : auth()->id());
        $document->creator_id = (isset($request->user_id) ? $request->user_id : auth()->id());
        $document->status_id = 1;
        $document->save();

        if ($request->ajax()) {
            return response()->json($document);
        }

        return response()->json(['success' => 'You have successfully upload file.']);
    }

    public function deleteDocument($document_id): Response
    {
        Document::destroy($document_id);

        return response()
            ->json([
                'success' => 'File successfully deleted.',
                'uri' => \request()->getUri(),
            ]);
    }
    
    public function deleteTask($task_id): JsonResponse
    {
        Task::destroy($task_id);

        return response()
            ->json([
                'success' => 'Task successfully deleted.'
            ]);
    }

    public function deleteTimeline($timeline_id): JsonResponse
    {
        DB::table('timeline')->delete($timeline_id);

        return response()->json(['success' => 'Timeline successfully deleted.']);
    }

    public function getDocuments($task_id)
    {
        $documents = Document::orderBy('id')->where('document_type_id', 10)->where('reference_id', $task_id)->where('status_id', 1)->get();

        return $documents;
    }

    public function addTimesheetTimeline(Request $taskRequest)
    {
        $request = (object) $taskRequest->input('request');
        $task = (object) $request->task;
        if ($task->status == 5) {
            return response()->json(['message' => "This Task Is Completed You can't Capture Time Against It."]);
        }
        $timesheet_data = (object) $request->timesheet;

        $timesheet_ids = Timeline::where('task_id', $task->id)->pluck('timesheet_id')->toArray();

        $company_id = isset($task->company_id) && $task->company_id > 0 ? $task->company_id : Company::first()->id;

        $timesheet = null;

        if (! empty($timesheet_ids)) {
            $year_week = $request->year_week;
            $is_split_week = false;
            $year_week_array = [];
            if (count(explode('_', $request->year_week)) > 1) {
                $is_split_week = true;
                $year_week_array = explode('_', $request->year_week);
                $year_week = $year_week_array[0];
            }

            $timesheets = Timesheet::whereIn('id', $timesheet_ids)->where('year_week', $year_week)->get();

            foreach ($timesheets as $_timesheet) {
                $first_day_of_week_array = explode('-', $_timesheet->first_day_of_week);
                $last_day_of_week_array = explode('-', $_timesheet->last_day_of_week);

                if ($is_split_week) {
                    // Check if split week is already saved
                    if (($_timesheet->last_date_of_week == $_timesheet->last_date_of_month) && ($year_week_array[1] == 1)) {
                        $timesheet = $_timesheet;
                    }

                    if (($_timesheet->first_date_of_week == 1) && ($_timesheet->last_date_of_week < 7) && ($year_week_array[1] == 2)) {
                        $timesheet = $_timesheet;
                    }
                } else {
                    if (($last_day_of_week_array[2] == $_timesheet->last_date_of_week) && ($first_day_of_week_array[2] == $_timesheet->first_date_of_week)) {
                        $timesheet = $_timesheet;
                    }
                }
            }
        }

        if (! isset($timesheet)) {
            $timesheet = $this->createTimesheet($task->employee_id, $company_id, $task->project_id, $task->customer_id, $request->year_week, (isset($request->user_id) ? $request->user_id : auth()->id()));
        }

        $timeline = new Timeline();
        $timeline->description_of_work = '';
        $timeline->timesheet_id = $timesheet->id;
        $timeline->task_id = $task->id;
        $timeline->mon = $timesheet_data->mon_hour;
        $timeline->tue = $timesheet_data->tue_hour;
        $timeline->wed = $timesheet_data->wed_hour;
        $timeline->thu = $timesheet_data->thu_hour;
        $timeline->fri = $timesheet_data->fri_hour;
        $timeline->sat = $timesheet_data->sat_hour;
        $timeline->sun = $timesheet_data->sun_hour;
        $timeline->total = $timeline->mon + $timeline->tue + $timeline->wed + $timeline->thu + $timeline->fri + $timeline->sat + $timeline->sun;
        $timeline->mon_m = $timesheet_data->mon_minute;
        $timeline->tue_m = $timesheet_data->tue_minute;
        $timeline->wed_m = $timesheet_data->wed_minute;
        $timeline->thu_m = $timesheet_data->thu_minute;
        $timeline->fri_m = $timesheet_data->fri_minute;
        $timeline->sat_m = $timesheet_data->sat_minute;
        $timeline->sun_m = $timesheet_data->sun_minute;
        $timeline->total_m = $timeline->mon_m + $timeline->tue_m + $timeline->wed_m + $timeline->thu_m + $timeline->fri_m + $timeline->sat_m + $timeline->sun_m;
        $timeline->is_billable = $timesheet_data->billable;
        $timeline->vend_is_billable = $timesheet_data->vend_billable;
        // $timeline->mileage = $timesheet_data->mileage;
        $timeline->creator_id = (isset($request->user_id) ? $request->user_id : auth()->id());
        $timeline->status = 1;
        $timeline->save();

        
        $this->utils->hoursOverspent($timesheet->employee_id, $timesheet->project_id);

        return $timeline;
    }

    public function updateTimesheetTimeline(Request $request){
        // return $request->all();
        $timeline = Timeline::find($request->id);
        $timeline->is_billable =  $request->billable;
        $timeline->vend_is_billable =  $request->vend_billable;
        $timeline->mon = $request->mon;
        $timeline->mon_m = $request->mon_m;
        $timeline->tue = $request->tue;
        $timeline->tue_m = $request->tue_m;
        $timeline->wed = $request->wed;
        $timeline->wed_m = $request->wed_m;
        $timeline->thu = $request->thu;
        $timeline->thu_m = $request->thu_m;
        $timeline->fri = $request->fri;
        $timeline->fri_m = $request->fri_m;
        $timeline->sat = $request->sat;
        $timeline->sat_m = $request->sat_m;
        $timeline->sun = $request->sun;
        $timeline->sun_m = $request->sun_m;
        $timeline->total = $timeline->mon+$timeline->tue+$timeline->wed+$timeline->thu+$timeline->fri+$timeline->sat+$timeline->sun;
        $timeline->total_m = $timeline->mon_m+$timeline->tue_m+$timeline->wed_m+$timeline->thu_m+$timeline->fri_m+$timeline->sat_m+$timeline->sun_m;
        $timeline->save();

        $timesheet = Timesheet::find($timeline->timesheet_id);

        $this->utils->hoursOverspent($timesheet->employee_id, $timesheet->project_id);

        return $timeline;
    }

    public function getTimelineDays(Request $request,$timesheet_id){
        $timesheet = Timesheet::find($timesheet_id);

        if(Carbon::parse($timesheet->first_day_of_week)->diffInDays(Carbon::parse($timesheet->last_day_of_week)) > 6){
            $date = now()->setISODate($timesheet->year, $timesheet->week);

            $parse_week_start_date = $date->copy()->startOfWeek();
            $parse_week_end_date = $date->copy()->endOfWeek();
        } else {
            $parse_week_start_date = Carbon::parse($timesheet->first_day_of_week);
            $parse_week_end_date = Carbon::parse($timesheet->last_day_of_week);
        }
        $week_days = [];

        for ($parse_week_start_date; $parse_week_start_date <= $parse_week_end_date; $parse_week_start_date->addDay()) {
            array_push($week_days, $parse_week_start_date->format('D d'));
        }

        return $week_days;
    }

    public function getTimelines($task_id)
    {
        $config = Config::first();
        $timelines = Timeline::where('task_id', $task_id)->get();

        $timeline_minutes = [];
        $times = [];
        $counter = 0;
        $total = 0;
        foreach ($timelines as $timeline) {
            $timesheet = Timesheet::find($timeline->timesheet_id);

            $timeline_minutes[$counter]['id'] = $timeline->id;
            $timeline_minutes[$counter]['timesheet_id'] = $timesheet->id;
            $timeline_minutes[$counter]['from_to'] = $timesheet->first_day_of_week.' - '.$timesheet->last_day_of_week;
            $mon = $timeline->mon * 60 + $timeline->mon_m;
            $tue = $timeline->tue * 60 + $timeline->tue_m;
            $wed = $timeline->wed * 60 + $timeline->wed_m;
            $thu = $timeline->thu * 60 + $timeline->thu_m;
            $fri = $timeline->fri * 60 + $timeline->fri_m;
            $sat = $timeline->sat * 60 + $timeline->sat_m;
            $sun = $timeline->sun * 60 + $timeline->sun_m;
            $total += $mon + $tue + $wed + $thu + $fri + $sat + $sun;
            $timeline_minutes[$counter]['total'] = $this->minutesToTime($timeline->total * 60 + $timeline->total_m);
            $timeline_minutes[$counter]['total_time'] = $total;
            $timeline_minutes[$counter]['is_billable'] = $timeline->is_billable;
            $timeline_minutes[$counter]['vend_billable'] = $timeline->vend_is_billable;
            $timeline_minutes[$counter]['canEdit'] = 0;
            $timeline_minutes[$counter]['year_week'] = $timesheet->year_week;
            $timeline_minutes[$counter]['billing_period_id'] = $timesheet->billing_period_id ?? 0;

            $timeline_minutes[$counter]["mon"] = ["hours"=>$timeline->mon,"minutes"=>$timeline->mon_m];
            $timeline_minutes[$counter]["tue"] = ["hours"=>$timeline->tue,"minutes"=>$timeline->tue_m];
            $timeline_minutes[$counter]["wed"] = ["hours"=>$timeline->wed,"minutes"=>$timeline->wed_m];
            $timeline_minutes[$counter]["thu"] = ["hours"=>$timeline->thu,"minutes"=>$timeline->thu_m];
            $timeline_minutes[$counter]["fri"] = ["hours"=>$timeline->fri,"minutes"=>$timeline->fri_m];
            $timeline_minutes[$counter]["sat"] = ["hours"=>$timeline->sat,"minutes"=>$timeline->sat_m];
            $timeline_minutes[$counter]["sun"] = ["hours"=>$timeline->sun,"minutes"=>$timeline->sun_m];
            $timeline_minutes[$counter]["mon_h"] = $timeline->mon;
            $timeline_minutes[$counter]["tue_h"] = $timeline->tue;
            $timeline_minutes[$counter]["wed_h"] = $timeline->wed;
            $timeline_minutes[$counter]["thu_h"] = $timeline->thu;
            $timeline_minutes[$counter]["fri_h"] = $timeline->fri;
            $timeline_minutes[$counter]["sat_h"] = $timeline->sat;
            $timeline_minutes[$counter]["sun_h"] = $timeline->sun;
            $timeline_minutes[$counter]["mon_m"] = $timeline->mon_m;
            $timeline_minutes[$counter]["tue_m"] = $timeline->tue_m;
            $timeline_minutes[$counter]["wed_m"] = $timeline->wed_m;
            $timeline_minutes[$counter]["thu_m"] = $timeline->thu_m;
            $timeline_minutes[$counter]["fri_m"] = $timeline->fri_m;
            $timeline_minutes[$counter]["sat_m"] = $timeline->sat_m;
            $timeline_minutes[$counter]["sun_m"] = $timeline->sun_m;
            $counter++;

        }

        $total = $this->minutesToTime($total);

        return ['timeline_minutes' => collect($timeline_minutes)->sortByDesc('timesheet_id')->values()->all(), 'total' => $total,'times'=>$times];
    }

    public function minutesToTime($minutes)
    {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;
        $negation_number = '';
        if ($hours < 0) {
            $hours *= -1;
            $negation_number = '-';
        }

        if ($minutes < 0) {
            $minutes *= -1;
            $negation_number = '-';
        }
        $hour_str = $hours < 10 ? '0'.$hours : $hours;
        $minutes_str = $minutes < 10 ? '0'.$minutes : $minutes;

        return $negation_number.$hour_str.':'.$minutes_str;
    }

    public function returnWeek($year, $month, $firstDay)
    {
        $feb_last_day = 28;
        if ($year % 4 == 0) {
            $feb_last_day = 29;
        }
        $month_days = [1 => 31, 2 => $feb_last_day, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];

        $week_days = [];
        for ($i = 0; $i <= 6; $i++) {
            $week_days[$i] = (int) $firstDay < 10 ? '0'.(int) $firstDay : ''.$firstDay;
            if ($firstDay == $month_days[(int) $month]) {
                $firstDay = 1;
            } else {
                $firstDay++;
            }
        }

        return $week_days;
    }

    public function createTimesheet($employee_id, $company_id, $project_id, $customer_id, $year_week, $user_id)
    {
        $project = Project::find($project_id);

        $timesheet = new Timesheet();
        $timesheet->employee_id = $employee_id;
        $timesheet->company_id = $company_id;
        $timesheet->project_id = $project_id;
        $timesheet->customer_id = $customer_id;

        $year_week_arr = explode('_', $year_week);

        $timesheet->year_week = $year_week_arr[0];

        $year = substr($timesheet->year_week, 0, 4);
        $week = substr($timesheet->year_week, -2);

        $year = (int) $year;
        $week = (int) $week;

        $timesheet->year = $year;
        if ($year_week == '202053_2') {
            $timesheet->year = 2021;
        }

        $billing_cycle = $this->billingCycle($project, Config::first());

        $date = now()->setISODate($year, $week);

        $billing_period = $this->getBillingPeriod($billing_cycle, $date, $year_week_arr[1] ?? 0);

        $timesheet->week = $week;
        $timesheet->month = $date->month;
        $timesheet->billing_period_id = $billing_period->id;
        $timesheet->first_day_of_week = $date->copy()->startOfWeek();
        $timesheet->last_day_of_week = $date->copy()->endOfWeek();
        $timesheet->first_day_of_month = $billing_period->start_date ?? $date->copy()->startOfMonth();
        $timesheet->last_day_of_month = $billing_period->end_date ?? $date->copy()->endOfMonth();
        $timesheet->first_date_of_week = $date->copy()->startOfWeek()->format('d');
        $timesheet->last_date_of_week = $date->copy()->endOfWeek()->format('d');
        $timesheet->first_date_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : $date->copy()->startOfMonth()->format('d');
        $timesheet->last_date_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date)->format('d') : $date->copy()->endOfMonth()->format('d');
        $timesheet->creator_id = auth()->id();
        $timesheet->status_id = 1;

        if (count($year_week_arr) == 2) {
            if ($year_week_arr[1] == 1) {
                $timesheet->last_date_of_week = $timesheet->last_date_of_month;
                $timesheet->last_day_of_week = $timesheet->last_day_of_month;
                $timesheet->last_day_of_week = $timesheet->last_day_of_month;
            }

            if ($year_week_arr[1] == 2) {
                $temp_last_date_of_month_2 = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
                $timesheet->first_date_of_week = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->format('d') : 1;
                $timesheet->month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date)->month : $date->copy()->month;
                $timesheet->first_day_of_month = isset($billing_period->start_date) ? Carbon::parse($billing_period->start_date) : $date->copy()->addWeek()->startOfMonth();
                $timesheet->last_day_of_month = isset($billing_period->end_date) ? Carbon::parse($billing_period->end_date) : $date->copy()->addWeek()->endOfMonth();
                $timesheet->last_date_of_month = $temp_last_date_of_month_2->format('d');
                $timesheet->first_day_of_week = $billing_period->start_date;
            }
        }

        $timesheet->save();

        activity()->on($timesheet)->log('created');

        return $timesheet;
    }

    public function setEmote($task_id, Request $request): JsonResponse
    {
        $task = Task::find($task_id);

        $task->emote_id = $request->emote_id;
        $task->save();

        return response()->json(['success' => 'Emote successfully added.']);
    }

    public function getProjectResources(Request $request): JsonResponse
    {
        $project_id = $request->project_id;

        $project = Project::find($project_id);
        $project_consultants = explode(',', $project->consultants);

        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->whereIn('id', $project_consultants)->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        return response()->json($resource_drop_down);
    }

    public function destroy($task_id, Request $request)
    {
        $task = Task::find($task_id);
        $project_id = $task->project_id;

        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);
        if(!$permissions){
            return abort(403);
        }
        foreach ($permissions as $permission) {
            if ($permission['section'] === 'Tasks' && $permission['permission'] === 'View') {
                return abort(403);
            }
        }
        Task::destroy($task_id);

        if ($request->has('project_id') && $request->input('project_id') > 0) {
            return redirect(route('project.show', $request->input('project_id')))->with('flash_success', 'Task deleted successfully.');
        }

        return redirect('task/'.$project_id)->with('flash_success', 'Task deleted successfully');
    }

    public function reassignTask(Request $request){

        if($request->has('timeline_count')){
            if($request->timeline_count == 0){
                $task = Task::find($request->task_id);
                $task->employee_id = $request->reassign_employee_id;
                $task->save();
            } else {
                $task_old = Task::find($request->task_id);

                $task = new Task();
                $task->description = $task_old->description;
                $task->dependency = $task_old->dependency;
                $task->parent_activity_id = $task_old->parent_activity_id;
                $task->task_type_id = $task_old->task_type_id;
                $task->employee_id = $request->reassign_employee_id;
                $task->customer_id = $task_old->customer_id;
                $task->sprint_id = $task_old->sprint_id;
                $task->goal_id = $task_old->goal_id;
                $task->invoice_date = $task_old->invoice_date;
                $task->invoice_ref = $task_old->invoice_ref;
                $task->invoice_status = $task_old->invoice_status;
                $task->milestone = $task_old->milestone;
                $task->project_id = $task_old->project_id;
                $task->start_date = now()->toDateString();
                $task->end_date = now()->toDateString();
                $task->hours_planned = $task_old->hours_planned;
                $task->user_story_id = $task_old->user_story_id;
                $task->status = 3;
                $task->billable = $task_old->billable;
                $task->on_kanban = $task_old->on_kanban;
                $task->note_1 = $task_old->note_1;
                $task->weight = $task_old->weight;
                $task->priority_level = $task_old->priority_level;
                $task->remaining_time = $task_old->remaining_time;
                $task->definition_of_ready = $task_old->definition_of_ready;
                $task->definition_of_done = $task_old->definition_of_done;
                $task->task_delivery_type_id = $task_old->task_delivery_type_id;
                $task->creator_id = auth()->id();
                $task->save();

            }
        }

        return response()->json('test');
    }

    public function copyReassignTask(Request $request){

        $task_old = Task::find($request->task_id);

        $task = new Task();
        $task->description = $task_old->description;
        $task->dependency = $task_old->dependency;
        $task->parent_activity_id = $task_old->parent_activity_id;
        $task->task_type_id = $task_old->task_type_id;
        $task->employee_id = $request->copy_employee_id;
        $task->customer_id = $task_old->customer_id;
        $task->sprint_id = $task_old->sprint_id;
        $task->goal_id = $task_old->goal_id;
        $task->invoice_date = $task_old->invoice_date;
        $task->invoice_ref = $task_old->invoice_ref;
        $task->invoice_status = $task_old->invoice_status;
        $task->milestone = $task_old->milestone;
        $task->project_id = $task_old->project_id;
        $task->start_date = now()->toDateString();
        $task->end_date = now()->toDateString();
        $task->hours_planned = $task_old->hours_planned;
        $task->user_story_id = $task_old->user_story_id;
        $task->status = $task_old->status != 1 ? 3 : 1;
        $task->billable = $task_old->billable;
        $task->on_kanban = $task_old->on_kanban;
        $task->note_1 = $task_old->note_1;
        $task->weight = $task_old->weight;
        $task->priority_level = $task_old->priority_level;
        $task->remaining_time = $task_old->remaining_time;
        $task->definition_of_ready = $task_old->definition_of_ready;
        $task->definition_of_done = $task_old->definition_of_done;
        $task->task_delivery_type_id = $task_old->task_delivery_type_id;
        $task->creator_id = auth()->id();
        $task->save();

        return response()->json('test');
    }

    public function taskTimesheets(Request $request){
        if($request->has('task_id') && $request->task_id > 0){
            $timelines = Timeline::where('task_id',$request->task_id)->count();
        }
        return response()->json($timelines ?? 0);
    }

    public function getIfTaskBillable($user_story_id){
        $is_billable = 1;

        $user_story = UserStory::find($user_story_id);

        if($user_story->billable != null){
            $is_billable = $user_story->billable;
        } else {
            $project = UserStory::with('feature.epic.project')->where('id',$user_story_id)->first();
            
            $is_billable = $project->feature->epic->project->billable;
        }
        
        return response()->json($is_billable);
    }

    public function dropdown($project)
    {
        $tasks = Task::select(['id', 'description'])->orderBy('description')->where('project_id', $project)->get();

        return response()->json($tasks);
    }

    public function getProjectTasks(Request $request,$project_id){
        if($request->has('project_id') && $request->input('project_id') > 0){
            $project = $request->input('project_id');
        } else {
            $project = $project_id;
        }
        $tasks = Task::with('project', 'taskstatus', 'tasktype', 'consultant', 'bugTask','timelines')->withSum('timelines', 'total')->withSum('timelines', 'total_m')->where('project_id', '=', $project);

        
        // if ($request->has('q') && $request->input('q') != '' && $request->input('q') != ' ') {
        //     $tasks = $tasks->where('id','like','%'.$request->input('q').'%');
        // }

        if ($request->has('resource_id') && $request->input('resource_id') > 0) {
            $tasks = $tasks->where('employee_id', '=', $request->input('resource_id'));
        }

        if ($request->has('customer_id') && $request->input('customer_id') > 0) {
            $tasks = $tasks->where('customer_id', '=', $request->input('customer_id'));
        }

        if ($request->has('team_id') && $request->input('team_id') > 0) {
            //$tasks = $tasks->where('team_id', '=', $request->input('team'));
        }

        if ($request->has('status_id') && $request->input('status_id') > 0) {
            $tasks = $tasks->where('status', '=', $request->input('status_id'));
        }

        if ($request->has('type') && $request->input('type') != '') {
            $tasks = $tasks->where('task_type_id', $request->input('type'));
        }

        if ($request->has('start_date') && $request->input('start_date') != '') {
            $tasks = $tasks->where('start_date', '>=', Carbon::parse($request->input('start_date'))->format('Y-m-d'));
        }

        if ($request->has('end_date') && $request->input('end_date') != '') {
            $tasks = $tasks->where('end_date', '<=', Carbon::parse($request->input('end_date'))->format('Y-m-d'));
        }

        if ($request->has('sprint_id') && $request->input('sprint_id') > 0) {
            $tasks = $tasks->where('sprint_id', $request->sprint_id);
        }

        $tasks = $tasks->get();
        
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);

        return response()->json(['tasks' => $tasks,'permissions'=>$permissions]);
    }

    public function getProjectTreeview(Request $request,$project_id){
        
        if ($request->has('project_id') && $request->input('project_id') > 0) {
            $project_id = $request->input('project_id');
        }

        $epics = Epic::with(['sprint','status','features.sprint','features.status','features.user_stories.tasks' => function ($query) {
        // Select the necessary fields and include a subquery for total timelines
            $query->withSum('timelines', 'total')->withSum('timelines', 'total_m');
        },'features.user_stories.tasks.consultant','features.user_stories.tasks.sprint','features.user_stories.tasks.taskstatus','features.user_stories.tasks.timelines', 'features.user_stories.tasks.bugTask', 'features.user_stories.userStoryDiscussions'])
            
            // ->when($request->project_id && $request->project_id > 0, function ($epic) use ($request) {
            //     $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
            //         $e->where('project_id', $request->project_id);
            //     });
            // })
            ->when($request->resource_id && $request->resource_id > 0, function ($epic) use ($request) {
                $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
                    $e->where('employee_id', $request->resource_id);
                });
            })->when($request->customer_id && $request->customer_id > 0, function ($epic) use ($request) {
                $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
                    $e->where('customer_id', $request->customer_id);
                });
            })->when($request->start_date && $request->start_date != '', function ($epic) use ($request) {
                $epic->where('start_date', '>=', $request->start_date);
            })->when($request->end_date && $request->end_date != '', function ($epic) use ($request) {
                $epic->where('end_date', '<=', $request->end_date);
            })->when($request->status_id && $request->status_id > 0, function ($epic) use ($request) {
                $epic->whereHas('features.user_stories.tasks', function ($e) use ($request) {
                    $e->where('status', $request->status_id);
                });
            })->when($request->sprint_id && $request->sprint_id > 0, function ($epic) use ($request) {
                $epic->where('sprint_id', $request->sprint_id);
            })->when($request->team, function ($task) use ($request) {
                $task->whereHas('features.user_stories.tasks.consultant.resource', function ($t) use ($request) {
                    $t->where('team_id', '=', $request->team);
                });
            })->where('project_id', '=', $project_id)->orderBy('id', 'desc')->get();
            
        $storePermission = new StorePermission();
        $permissions = $storePermission->getPermissions(30, $project_id);
            
        return response()->json(['epics'=>$epics,'permissions'=>$permissions]);
    }

    public function getTaskFilters(Request $request){

        $projects_drop_down = Project::orderBy('name')->filters()->whereNotNull('name')->where('name', '!=', '')->get(['name', 'id']);
        $resource_drop_down = User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->filters()->orderBy('first_name')->orderBy('last_name')->get(['full_name', 'id']);
        $customer_drop_down = Customer::orderBy('customer_name')->filters()->whereNotNull('customer_name')->where('customer_name', '!=', '')->get(['customer_name', 'id']);
        $team_drop_down = Team::orderBy('team_name')->filters()->whereNotNull('team_name')->where('team_name', '!=', '')->get(['team_name', 'id']);
        $task_status_drop_down = TaskStatus::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->get(['description', 'id']);
        $task_type_drop_down = TaskType::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->get(['name', 'id']);

        return response()->json(['projects_drop_down' => $projects_drop_down,
            'resource_drop_down' => $resource_drop_down,
            'customer_drop_down' => $customer_drop_down,
            'team_drop_down' => $team_drop_down,
            'task_status_drop_down' => $task_status_drop_down,
            'task_type_drop_down' => $task_type_drop_down,
        'sprints_drop_down' => Sprint::orderBy('name')->filters()->get(['name', 'id'])]);
    }

    public function moveTask(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->user_story_id = $request->user_story_id;
        $task->save();

        return response()->json([
            'message' => 'Task updated successfully!',
            'task' => $task
        ]);
    }

}
