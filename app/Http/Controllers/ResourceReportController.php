<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Experience;
use App\Models\Leave;
use App\Models\Project;
use App\Models\Timesheet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ResourceReportController extends Controller
{
    public function index(Request $request): View|BinaryFileResponse
    {
        $item = $request->input('s') ?? 15;

        $project_resources_counter = Project::select('name', 'consultants')->where('status_id', '=', 3)->get();

        $resource_roles_counter = Experience::select(DB::raw('count(res_id) as counter, role'))
            ->groupBy('role')
            ->limit(10)
            ->get();

        $assignments = Assignment::with(['project','consultant'])->whereIn('assignment_status', [2, 3]);

        if ($request->has('project') && $request->input('project') != '') {
            $assignments = $assignments->where('project_id', $request->input('project'));
        }

        if ($request->has('company') && $request->input('company') != '') {
            $assignments = $assignments->where('company_id', $request->input('company'));
        }

        if ($request->has('resource') && $request->input('resource') != '') {
            $assignments = $assignments->where('employee_id', $request->input('resource'));
        }

        $assignments = $assignments->sortable(['name' => 'asc'])->paginate($item);

        $date = Carbon::now();

        $working_days = [];
        $actual_hours = [];
        $hours_forecast = [];
        $assignment_months = [];
        $total_project_budget = [];
        $working_hours = [];
        $invoice_rate = [];
        $po_value = [];
        $planned_spend = [];
        $variance_po = [];

        foreach ($assignments as $assignment) {
            $leave_days_from_now = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $date->now()->toDateString())->where('date_to', '<=', $assignment->planned_end_date)->get();
            $leave_days_from_planned_start = Leave::where('emp_id', '=', $assignment->employee_id)->where('date_from', '>=', $assignment->planned_start_date)->where('date_to', '<=', $assignment->planned_end_date)->get();

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_now = 0;
            foreach ($leave_days_from_now as $leave_day_from_now) {
                $resource_leave_days_taken_now += $leave_day_from_now->no_of_days;
            }

            //Subtract leave days taken during period specified
            $resource_leave_days_taken_start = 0;
            foreach ($leave_days_from_planned_start as $leave_day_from_start) {
                $resource_leave_days_taken_start += $leave_day_from_start->no_of_days;
            }

            $calendar = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $calendar = $calendar > 0 ? $calendar - $resource_leave_days_taken_start : $calendar;
            $forecast_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$date->now()->toDateString(), $assignment->planned_end_date])->get()->count(); // Substract leave days (Use plan start date to plan end date - leave days and public holidays)
            $forecast_days = $forecast_days > 0 ? $forecast_days - $resource_leave_days_taken_now : $forecast_days;
            $assignment_days = Calendar::where('weekday', '=', 'Y')->where('public_holiday', '=', 'N')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->get()->count();
            $assignment_days = $assignment_days > 0 ? $assignment_days - $resource_leave_days_taken_start : $assignment_days;
            $number_of_month = Calendar::select('month', 'year')->whereBetween('date', [$assignment->planned_start_date, $assignment->planned_end_date])->groupBy('month', 'year')->get()->count();
            $time = Timesheet::selectRaw('SUM(CASE WHEN timeline.is_billable = '.$assignment->billable?->value.' THEN (timeline.total + (timeline.total_m/60)) ELSE 0 END) AS hours')
                ->leftJoin('timeline', 'timesheet.id', '=', 'timeline.timesheet_id')
                ->where('timesheet.project_id', '=', $assignment->project_id)
                ->where('timesheet.employee_id', '=', $assignment->employee_id)
                ->first();

            $capacity_allocation_hours = $assignment->capacity_allocation_hours > 0 ? $assignment->capacity_allocation_hours : 8;
            $budget = Assignment::selectRaw('SUM(CASE WHEN billable = '.$assignment->billable?->value.' THEN hours * '.(($assignment->billable?->value == 1) ? 'invoice_rate' : 'internal_cost_rate + external_cost_rate').' ELSE 0 END) AS value')->where('customer_po', '=', $assignment->customer_po)->where('project_id', '=', $assignment->project_id)->first();
            $rate = ($assignment->billable == 1) ? $assignment->invoice_rate : ($assignment->internal_cost_rate + $assignment->external_cost_rate);
            array_push($working_days, $calendar);
            array_push($actual_hours, $time);
            array_push($hours_forecast, $forecast_days * $capacity_allocation_hours);
            array_push($assignment_months, $number_of_month);
            array_push($total_project_budget, $budget);
            array_push($working_hours, $calendar * 8);
            array_push($invoice_rate, $rate);
            array_push($po_value, $assignment->hours * $rate);
            array_push($planned_spend, ($time->hours * $rate) + (($forecast_days * 8) * $rate));
            array_push($variance_po, $assignment->hours - ($assignment_days * $capacity_allocation_hours)); //Use plan start date to plan end date - leave days and public holidays (for assignment days)
        }

        $project_drop_down = Project::filters()->orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', '');
        $resource_drop_down = User::filters()->select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->excludes([6, 7, 8])->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', '');

        $parameters = [
            'assignments' => $assignments,
            'company' => Company::orderBy('company_name')->whereNotNull('company_name')->where('company_name', '!=', '')->pluck('company_name', 'id')->prepend('All', ''),
            'projects' => $project_drop_down,
            'resource' => $resource_drop_down,
            'project_resources_counter' => $project_resources_counter,
            'resource_roles_counter' => $resource_roles_counter,
            'working_days' => $working_days,
            'actual_hours' => $actual_hours,
            'hours_forecast' => $hours_forecast,
            'assignment_months' => $assignment_months,
            'total_project_budget' => $total_project_budget,
            'working_hours' => $working_hours,
            'invoice_rate' => $invoice_rate,
            'po_value' => $po_value,
            'planned_spend' => $planned_spend,
            'variance_po' => $variance_po,
        ];

        if ($request->has('export')) return $this->export($parameters, 'reports', 'resource');

        return view('reports.resource')->with($parameters);
    }

    public function analysis(Request $request): View
    {
        $item = $request->has('page') && $request->input('page') > 0 ? $request->input('page') : 15;
        $scoutings = Scouting::with(['processstatus', 'interviewstatus', 'role', 'vendor', 'ResourceLevel', 'businessrating', 'technicalrating'])->orderby('resource_first_name');

        $process_status_counter = [];
        $process_status_counter['declined'] = Scouting::select(DB::raw('COUNT(process_status_id) AS declined'))->where('process_status_id', '=', 1)->first()->declined;
        $process_status_counter['contract_end'] = Scouting::select(DB::raw('COUNT(process_status_id) AS contract_end'))->where('process_status_id', '=', 2)->first()->contract_end;
        $process_status_counter['opportunity'] = Scouting::select(DB::raw('COUNT(process_status_id) AS opportunity'))->where('process_status_id', '=', 3)->first()->opportunity;
        $process_status_counter['resigned'] = Scouting::select(DB::raw('COUNT(process_status_id) AS resigned'))->where('process_status_id', '=', 4)->first()->resigned;
        $process_status_counter['pending'] = Scouting::select(DB::raw('COUNT(process_status_id) AS pending'))->where('process_status_id', '=', 5)->first()->pending;
        $process_status_counter['offer_not_accepted'] = Scouting::select(DB::raw('COUNT(process_status_id) AS offer_not_accepted'))->where('process_status_id', '=', 6)->first()->offer_not_accepted;

        $interview_status_counter = [];
        $interview_status_counter['interviewed'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS interviewed'))->where('interview_status_id', '=', 1)->first()->interviewed;
        $interview_status_counter['to_interview'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS to_interview'))->where('interview_status_id', '=', 2)->first()->to_interview;
        $interview_status_counter['aes_experience'] = Scouting::select(DB::raw('COUNT(interview_status_id) AS aes_experience'))->where('interview_status_id', '=', 3)->first()->aes_experience;

        $vendors_counter = Scouting::select(DB::raw('count(scouting.id) as counter, vendor.vendor_name'))
            ->leftJoin('vendor', 'scouting.vendor_id', '=', 'vendor.id')
            ->groupBy('vendor.vendor_name')
            ->get();

        $process_statuses_drop_down = ProcessStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $business_rating_drop_down = BusinessRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $tecnical_rating_drop_down = TechnicalRating::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $process_interview_drop_down = InterviewStatus::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $role_drop_down = ScoutingRole::orderBy('name')->whereNotNull('name')->where('name', '!=', '')->pluck('name', 'id')->prepend('All', -1);
        $vendor_drop_down = Vendor::orderBy('vendor_name')->whereNotNull('vendor_name')->where('vendor_name', '!=', '')->pluck('vendor_name', 'id')->prepend('All', -1);
        $resource_level_drop_down = ResourceLevel::orderBy('description')->whereNotNull('description')->where('description', '!=', '')->pluck('description', 'id')->prepend('All', -1);

        if ($request->has('process_status') && $request->input('process_status') != -1) {
            $scoutings = $scoutings->where('process_status_id', $request->input('process_status'));
        }

        if ($request->has('process_interview') && $request->input('process_interview') != -1) {
            $scoutings = $scoutings->where('interview_status_id', $request->input('process_interview'));
        }

        if ($request->has('role') && $request->input('role') != -1) {
            $scoutings = $scoutings->where('role_id', $request->input('role'));
        }

        if ($request->has('vendor') && $request->input('vendor') != -1) {
            $scoutings = $scoutings->where('vendor_id', $request->input('vendor'));
        }

        if ($request->has('resource_level') && $request->input('resource_level') != -1) {
            $scoutings = $scoutings->where('resource_level_id', $request->input('resource_level'));
        }

        if ($request->has('businees_rating') && $request->input('businees_rating') != -1) {
            $scoutings = $scoutings->where('business_rating_id', $request->input('businees_rating'));
        }

        if ($request->has('technical_rating') && $request->input('technical_rating') != -1) {
            $scoutings = $scoutings->where('technical_rating_id', $request->input('technical_rating'));
        }

        $scoutings = $scoutings->paginate($item);

        $parameters = [
            'scoutings' => $scoutings,
            'process_statuses_drop_down' => $process_statuses_drop_down,
            'process_interview_drop_down' => $process_interview_drop_down,
            'role_drop_down' => $role_drop_down,
            'vendor_drop_down' => $vendor_drop_down,
            'business_rating_drop_down' => $business_rating_drop_down,
            'tecnical_rating_drop_down' => $tecnical_rating_drop_down,
            'resource_level_drop_down' => $resource_level_drop_down,
            'process_status_counter' => $process_status_counter,
            'interview_status_counter' => $interview_status_counter,
            'vendors_counter' => $vendors_counter,
        ];

        return view('reports.resource_analysis')->with($parameters);
    }
}
