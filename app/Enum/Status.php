<?php

namespace App\Enum;

enum Status: int
{
    case ACTIVE = 1;
    case SUSPENDED = 2;
    case CLOSED = 6;

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
