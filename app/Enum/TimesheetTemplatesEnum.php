<?php

namespace App\Enum;

enum TimesheetTemplatesEnum : int
{
    case Standard = 1;
    case Weekly = 2;
    case Arbour = 3;
    case Monthly = 4;
    case ArbourMonthly = 5;
    case ArbourMonthlyGreen = 6;
}
