<?php

namespace App\Enum;

enum InvoiceType: int
{
    case Customer = 1;
    case Vendor = 2;
}
