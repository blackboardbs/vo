<?php

namespace App\Enum;

enum Ability: int
{
    case FULL = 1;
    case VIEW = 2;
    case UPDATE = 3;
    case NONE = 4;
    case CREATE = 5;
}
