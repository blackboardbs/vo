<?php

namespace App\Enum;

enum InvoiceStatusEnum: int
{
    case Invoiced = 1;
    case Paid = 2;
    case Cancelled = 3;

    public static function keyValuePair(): array
    {
        return collect(InvoiceStatusEnum::cases())
            ->keyBy(fn($status) => $status->value)
            ->map(fn ($status) => $status->name)
            ->toArray();
    }
}
