<?php

namespace App\Enum;

enum YesOrNoEnum: int
{
    case No = 0;
    case Yes = 1;
}
