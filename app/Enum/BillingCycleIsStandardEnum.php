<?php

namespace App\Enum;

enum BillingCycleIsStandardEnum: int
{
    case YES = 1;
    case NO = 0;
}
