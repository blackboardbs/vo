<?php

namespace App;

use App\Models\RoleUser;
use App\Models\UserFavourite;

class HeaderFav
{
    public static function index()
    {
        $user_id = auth()->id();
        $user_roles = RoleUser::where('user_id', '=', $user_id)->get();
        $favourites = [];
        foreach ($user_roles as $role) {
            $favourites = UserFavourite::where('user_id', '=', $user_id)->where('role_id', '=', $role->role_id)->get();
            if (count($favourites) == 0) {
                $favourites = UserFavourite::where('user_id', '=', null)->where('role_id', '=', $role->role_id)->get();
            }
        }

        return $favourites;
    }
}
