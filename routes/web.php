<?php

use App\Http\Controllers\AnniversaryTypeController;
use App\Http\Controllers\Central\AccountController;
use App\Http\Controllers\Central\AccountElementController;
use App\Http\Controllers\Central\AccountTypeController;
use App\Http\Controllers\Central\AssessmentMasterController;
use App\Http\Controllers\Central\AssessmentMasterDetailsController;
use App\Http\Controllers\Central\AssetClassController;
use App\Http\Controllers\Central\AssignmentStandardCostController;
use App\Http\Controllers\Central\BusinessFunctionController;
use App\Http\Controllers\Central\ContractTypeController;
use App\Http\Controllers\Central\CostCenterController;
use App\Http\Controllers\Central\CountryController;
use App\Http\Controllers\Central\CvCompanyController;
use App\Http\Controllers\Central\DashboardComponentController;
use App\Http\Controllers\Central\DefaultDashboardController;
use App\Http\Controllers\Central\DefaultFavouriteController;
use App\Http\Controllers\Central\DeliveryTypeController;
use App\Http\Controllers\Central\ExpenseTypeController;
use App\Http\Controllers\Central\FavouriteController;
use App\Http\Controllers\Central\IndustryController;
use App\Http\Controllers\Central\InvoiceStatusController;
use App\Http\Controllers\Central\LeaveTypeController;
use App\Http\Controllers\Central\MaritalStatusController;
use App\Http\Controllers\Central\MedicalCertificateTypeController;
use App\Http\Controllers\Central\OnboardingStatusController;
use App\Http\Controllers\Central\PaymentBaseController;
use App\Http\Controllers\Central\PaymentMethodController;
use App\Http\Controllers\Central\PaymentTermController;
use App\Http\Controllers\Central\PaymentTypeController;
use App\Http\Controllers\Central\ProcessInterviewController;
use App\Http\Controllers\Central\ProcessStatusController;
use App\Http\Controllers\Central\ProfessionController;
use App\Http\Controllers\Central\ProjectStatusController;
use App\Http\Controllers\Central\ProjectTermController;
use App\Http\Controllers\Central\ProjectTypeController;
use App\Http\Controllers\Central\ProspectStatusController;
use App\Http\Controllers\Central\QuotationTermController;
use App\Http\Controllers\Central\RelationshipController;
use App\Http\Controllers\Central\ResourceLevelController;
use App\Http\Controllers\Central\ResourcePositionController;
use App\Http\Controllers\Central\ScoutingRoleController;
use App\Http\Controllers\Central\SkillLevelController;
use App\Http\Controllers\Central\SolutionController;
use App\Http\Controllers\Central\SpecialityController;
use App\Http\Controllers\Central\SprintStatusController;
use App\Http\Controllers\Central\StatusController;
use App\Http\Controllers\Central\SystemController;
use App\Http\Controllers\Central\SystemHealthController;
use App\Http\Controllers\Central\TemplateStyleController;
use App\Http\Controllers\Central\TemplateTypeController;
use App\Http\Controllers\Central\TermController;
use App\Http\Controllers\Central\TitleController;
use App\Http\Controllers\Central\UserController;
use App\Http\Controllers\Central\UserTypeController;
use App\Http\Controllers\Central\VatRateController;
use App\Http\Controllers\Central\VendorInvoiceStatusController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', fn() => "<div style='font-family:arial;font-size: 24px;margin: 25px;display:flex;justify-content:center;
  align-items: center;height:100dvh'>
  <div class='child'><h2>No body is home at the moment...</h2></div>
</div>");

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/login', [UserController::class, 'login']);
    Route::post('/auth', [UserController::class, 'adminLogin'])->name('login');

   Route::middleware('auth')->group(function (){
        Route::get('/', fn() => "Logged in!");
        Route::resource('/account', AccountController::class);
        Route::resource('/accounttype', AccountTypeController::class);
        Route::resource('/accountelement', AccountElementController::class);
        Route::resource('/assessmentmaster', AssessmentMasterController::class);
        Route::resource('/assessmentmasterdetails', AssessmentMasterDetailsController::class);
        Route::resource('/anniversarytype', AnniversaryTypeController::class);
        Route::resource('/assetclass', AssetClassController::class);
        Route::resource('assignmentstandardcost', AssignmentStandardCostController::class);
        Route::resource('/businessfunction', BusinessFunctionController::class);
        Route::resource('/costcenter', CostCenterController::class);
        Route::resource('/contracttype', ContractTypeController::class);
        Route::resource('/country', CountryController::class);
        Route::resource('/cvcompany', CvCompanyController::class);
        Route::resource('/customisedashboard', DashboardComponentController::class);
        Route::resource('/dashboard', DefaultDashboardController::class);
        Route::resource('/defaultfavs', DefaultFavouriteController::class);
        Route::resource('/expensetype', ExpenseTypeController::class);
        Route::resource('/deliverytype', DeliveryTypeController::class);
        Route::resource('/favourites', FavouriteController::class);
        Route::resource('/industry', IndustryController::class);
        Route::resource('/profession', ProfessionController::class);
        Route::resource('/speciality', SpecialityController::class);
        Route::resource('/sprintstatus', SprintStatusController::class);
        Route::resource('/leavetype', LeaveTypeController::class);
        Route::resource('/maritalstatus', MaritalStatusController::class);
        Route::resource('/medical', MedicalCertificateTypeController::class);
        Route::resource('/paymentbase', PaymentBaseController::class);
        Route::resource('/paymenttype', PaymentTypeController::class);
        Route::resource('/processinterview', ProcessInterviewController::class);
        Route::resource('/processstatus', ProcessStatusController::class);
        Route::resource('/prospectstatus', ProspectStatusController::class);
        Route::resource('/projectterms', ProjectTermController::class);
        Route::resource('/quotationterms', QuotationTermController::class);
        Route::resource('/relationship', RelationshipController::class);
        Route::resource('/resourcelevel', ResourceLevelController::class);
        Route::resource('/resourceposition', ResourcePositionController::class);
        Route::resource('/skilllevel', SkillLevelController::class);
        Route::resource('/solution', SolutionController::class);
        Route::resource('/skill', SystemController::class);
        Route::resource('/onboardingstatus', OnboardingStatusController::class);
        Route::resource('/title', TitleController::class);
        Route::resource('/templatestyles', TemplateStyleController::class);
        Route::resource('/scoutingrole', ScoutingRoleController::class);
        Route::resource('/usertype', UserTypeController::class);
        Route::resource('/vatrate', VatRateController::class);
        Route::resource('/invoicestatus', InvoiceStatusController::class);
        Route::resource('/paymentmethod', PaymentMethodController::class);
        Route::resource('/paymentterms', PaymentTermController::class);
        Route::resource('/projecttype', ProjectTypeController::class);
        Route::resource('/projectstatus', ProjectStatusController::class);
        Route::resource('/status', StatusController::class);
        Route::resource('/systemhealth', SystemHealthController::class);
        Route::resource('/term', TermController::class);
        Route::resource('/vendorinvoicestatus', VendorInvoiceStatusController::class);
        Route::resource('/template', TemplateTypeController::class);
    });
});

Route::webhooks('generate-tenant');
