<?php

declare(strict_types=1);

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AccountElementController;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\ActionController;
use App\Http\Controllers\AdvancedTemplatesController;
use App\Http\Controllers\AnniversaryController;
use App\Http\Controllers\AnniversaryTypeController;
use App\Http\Controllers\API\CustomerProFormaInvoiceController;
use App\Http\Controllers\API\CustomerProjectController;
use App\Http\Controllers\API\FrequencyController;
use App\Http\Controllers\API\InvoiceDefaultEmailsController;
use App\Http\Controllers\API\AssetClassController;
use App\Http\Controllers\API\AssetHistoryController;
use App\Http\Controllers\API\NotificationController;
use App\Http\Controllers\API\PremiumInvoiceController;
use App\Http\Controllers\API\SubscriptionManagementController;
use App\Http\Controllers\API\TemplateTypeController as APITemplateTypeController;
use App\Http\Controllers\API\VatRateInvokeController;
use App\Http\Controllers\API\VendorProFormaInvoiceController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApprovalRulesController;
use App\Http\Controllers\AssertsClassController;
use App\Http\Controllers\AssessmentActivitiesController;
use App\Http\Controllers\AssessmentConcernController;
use App\Http\Controllers\AssessmentHeaderController;
use App\Http\Controllers\AssessmentMasterController;
use App\Http\Controllers\AssessmentMasterDetailsController;
use App\Http\Controllers\AssessmentMeasureScoreController;
use App\Http\Controllers\AssessmentNotesController;
use App\Http\Controllers\AssessmentPlannedNextController;
use App\Http\Controllers\AssetAdditionalCostController;
use App\Http\Controllers\AssetConditionNoteController;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetRegisterController;
use App\Http\Controllers\AssignmentController;
use App\Http\Controllers\AssignmentReportController;
use App\Http\Controllers\AssignmentStandardCostController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\AvailabilityController;
use App\Http\Controllers\BankAccountTypeController;
use App\Http\Controllers\BEEMasterDataController;
use App\Http\Controllers\BillingCycleController;
use App\Http\Controllers\BillingPeriodController;
use App\Http\Controllers\BillingPeriodStyleController;
use App\Http\Controllers\BoardCardController;
use App\Http\Controllers\BoardController;
use App\Http\Controllers\BusinessFunctionController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CalendarEventController;
use App\Http\Controllers\CalendarEventsController;
use App\Http\Controllers\CashflowController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\Consultant360Controller;
use App\Http\Controllers\ConsultantBonusController;
use App\Http\Controllers\ConsultingMonthlyReport;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContractTypeController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CostCalculatorController;
use App\Http\Controllers\CostCentreController;
use App\Http\Controllers\CostCentreTermsController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CustomDashboardController;
use App\Http\Controllers\CustomerBillingController;
use App\Http\Controllers\CustomerBillingReportController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerInvoiceController;
use App\Http\Controllers\CustomerPreferenceController;
use App\Http\Controllers\CustomerProFormaController;
use App\Http\Controllers\CustomerProjectSummaryController;
use App\Http\Controllers\CustomerReportController;
use App\Http\Controllers\CustomerSummaryReportController;
use App\Http\Controllers\CustomFormattedTimesheetController;
use App\Http\Controllers\CvCompanyController;
use App\Http\Controllers\CvController;
use App\Http\Controllers\CVQualityController;
use App\Http\Controllers\CVReferenceController;
use App\Http\Controllers\CvSubmitController;
use App\Http\Controllers\CVWishListController;
use App\Http\Controllers\DashboardV2Controller;
use App\Http\Controllers\DefaultDashboardController;
use App\Http\Controllers\DefaultsFavourites;
use App\Http\Controllers\DeliveryTypeController;
use App\Http\Controllers\DigiSignController;
use App\Http\Controllers\DistributionListController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DynamicDashboardController;
use App\Http\Controllers\EmailListController;
use App\Http\Controllers\EpicController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ExpensesNotPaid;
use App\Http\Controllers\ExpenseTrackingController;
use App\Http\Controllers\ExpenseTrackingInsightController;
use App\Http\Controllers\ExpenseTypeController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\FiltersController;
use App\Http\Controllers\ForecastReportController;
use App\Http\Controllers\GoalController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndustryController;
use App\Http\Controllers\IndustryExperienceController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\InvoiceItemsController;
use App\Http\Controllers\InvoiceStatusController;
use App\Http\Controllers\JobSpecController;
use App\Http\Controllers\KanbanBoardController;
use App\Http\Controllers\LandingPageDashboardController;
use App\Http\Controllers\LeaveBalanceController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\LeaveStatementController;
use App\Http\Controllers\LeaveTypeController;
use App\Http\Controllers\LogbookController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\MaritalStatusController;
use App\Http\Controllers\MCalendarController;
use App\Http\Controllers\MedicalCertificateTypeController;
use App\Http\Controllers\MessageBoardController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\MyWorkWeekController;
use App\Http\Controllers\NotificationHistoryController;
use App\Http\Controllers\OnboardingStatusController;
use App\Http\Controllers\PaymentBaseController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PaymentTermsController;
use App\Http\Controllers\PaymentTypeController;
use App\Http\Controllers\PayrollSummaryController;
use App\Http\Controllers\PlannedExpenseController;
use App\Http\Controllers\PlanningController;
use App\Http\Controllers\ProcessInterviewController;
use App\Http\Controllers\ProcessStatusController;
use App\Http\Controllers\ProfessionController;
use App\Http\Controllers\ProFormaVendarInvoiceController;
use App\Http\Controllers\ProjectBillingReportController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectHoursSummaryController;
use App\Http\Controllers\ProjectProgressController;
use App\Http\Controllers\ProjectStatusController;
use App\Http\Controllers\ProjectTermsController;
use App\Http\Controllers\ProjectTypeController;
use App\Http\Controllers\ProjectValueSummaryController;
use App\Http\Controllers\ProspectController;
use App\Http\Controllers\ProspectStatusController;
use App\Http\Controllers\QualificationController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\QuotationTermsController;
use App\Http\Controllers\RelationshipController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\ResourceDetailsController;
use App\Http\Controllers\ResourceLevelController;
use App\Http\Controllers\ResourcePositionController;
use App\Http\Controllers\ResourceReportController;
use App\Http\Controllers\ResourceTaskController;
use App\Http\Controllers\RiskController;
use App\Http\Controllers\RiskFeedbackController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ScoutingController;
use App\Http\Controllers\ScoutingReportController;
use App\Http\Controllers\ScoutingRoleController;
use App\Http\Controllers\SetupController;
use App\Http\Controllers\SiteReportController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\SkillLevelController;
use App\Http\Controllers\SolutionController;
use App\Http\Controllers\SpecialityController;
use App\Http\Controllers\SprintController;
use App\Http\Controllers\SprintResourceController;
use App\Http\Controllers\SprintReviewController;
use App\Http\Controllers\SprintStatusController;
use App\Http\Controllers\StatementController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\SummaryReportController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\SystemHealthController;
use App\Http\Controllers\SystemHealthMasterDataController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TaskReportController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\API\TemplateController as APITemplateController;
use App\Http\Controllers\TemplateTypeController;
use App\Http\Controllers\TemplateVariableController;
use App\Http\Controllers\API\TemplateVariableController as APITemplateVariableController;
use App\Http\Controllers\TermsController;
use App\Http\Controllers\TimesheetController;
use App\Http\Controllers\TimesheetNewController;
use App\Http\Controllers\TimesheetReportController;
use App\Http\Controllers\TitleController;
use App\Http\Controllers\TrialDataContoller;
use App\Http\Controllers\UnsubscribeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\API\UserController as UsersController;
use App\Http\Controllers\UserOnboardingAppointingManagerController;
use App\Http\Controllers\UserOnboardingController;
use App\Http\Controllers\UserStoryController;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\UtilizationController;
use App\Http\Controllers\VatRateController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\VendorInvoiceController;
use App\Http\Controllers\VendorInvoiceFunctionController;
use App\Http\Controllers\VendorInvoicePaymentController;
use App\Http\Controllers\VendorInvoiceStatusController;
use App\Http\Controllers\VendorStatementController;
use App\Http\Controllers\ViewForReportController;
use App\Http\Controllers\ViewPermissionController;
use App\Http\Controllers\WarningMaintenanceController;
use App\Http\Controllers\WishListController;
use App\Http\Controllers\MyAccountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Route;
use Spatie\LaravelPdf\Facades\Pdf;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\InitializeTenancyBySubdomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\PageController;
use Stancl\Tenancy\Middleware\InitializeTenancyByPath;
use App\Http\Controllers\TaskTagsController;


/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    Auth::routes();
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login']);
    Route::post('mlogin', [LoginController::class, 'login'])->name('mlogin');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Password Reset Routes...
    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::middleware('auth')->group(function () {
        Route::get('profile/{user?}', [UserController::class, 'profile'])->name('profile');
        Route::get('settings', [UserController::class, 'settings'])->name('settings');
        Route::post('settings/update', [UserController::class, 'handleSettings'])->name('settings.update');
        Route::get('settings/{link}', fn ($link) => view('auth.settings', ['link' => $link]));
        Route::post('settings/password', [UserController::class, 'handlePassword'])->name('settings.password');
        Route::post('settings/preferences', [UserController::class, 'handlePreferences'])->name('settings.preferences');
        Route::post('settings/profile', [UserController::class, 'handleProfile'])->name('settings.profile');
        Route::post('settings/notifications', [UserController::class, 'handleNotifications'])->name('settings.notifications');
        Route::post('settings/landing', [UserController::class, 'handleLandingPage'])->name('settings.landing');

        Route::get('setup', [SetupController::class, 'index'])->name('setup');
        Route::get('welcome', [SetupController::class, 'welcome'])->name('welcome');
        Route::get('dashboard', [LandingPageDashboardController::class, 'index'])->name('dashboard.index');
        Route::get('dashboard/expired_users', [LandingPageDashboardController::class, 'expired'])->name('dashboard.expired_users');
        Route::get('dashboard/to_expire', [LandingPageDashboardController::class, 'to_expire'])->name('dashboard.to_expire');

        Route::get('storage/avatar/user/', [AssetController::class, 'getUserAvatar'])->name('user_avatar');
        Route::get('storage/avatar/cv', [AssetController::class, 'getTalentAvatar'])->name('cv_avatar');
        Route::get('storage/avatar/resource', [AssetController::class, 'getResourceAvatar'])->name('resource_avatar');
        Route::get('storage/avatar/company', [AssetController::class, 'getCompanyAvatar'])->name('company_avatar');
        Route::get('storage/avatar/customer', [AssetController::class, 'getCustomerAvatar'])->name('customer_avatar');
        Route::get('storage/avatar/vendor', [AssetController::class, 'getVendorAvatar'])->name('vendor_avatar');
        Route::get('storage/avatar/cv_company', [AssetController::class, 'getCvCompanyLogo'])->name('cv_company_logo');
        Route::get('storage/template', [AssetController::class, 'getTemplate'])->name('advanced_template');
        Route::get('storage/idpassport', [AssetController::class, 'getIdOrPassport'])->name('id_passport');
        Route::get('storage/proof_of_address', [AssetController::class, 'getProofOfAddress'])->name('proof_of_address');
        Route::get('storage/proof_of_bank_account', [AssetController::class, 'getProofOfBankAccount'])->name('proof_of_bank_account');
        Route::get('getfile/{id}', [AssetController::class, 'getFile'])->name('getfile');
        Route::get('jobspecfile/{id}', [AssetController::class, 'jobSpecDocuments'])->name('jobSpecDocuments');
        Route::get('getattachment/{id}', [AssetController::class, 'getTimeSheetAttachement'])->name('getattachment');
        Route::get('storage/avatar/asset', [AssetController::class, 'assetRegister'])->name('asset_register');
        Route::get('calendarevents/{id}', [AssetController::class, 'getCalendarEvents'])->name('events.files');
        Route::get('storage/client/agreement/{file}', [AssetController::class, 'getClientAgreement'])->name('client.agreement');
        Route::get('storage/provider/agreement/{file}', [AssetController::class, 'getProviderAgreement'])->name('provider.agreement');
        Route::get('storage/assignment/{file}', [AssetController::class, 'getAssignmentDocument'])->name('assignment.document');

        Route::get('/', [HomeController::class, 'index']);

        Route::resource('company', CompanyController::class);
        Route::resource('team', TeamController::class);

        Route::resource('utilization', UtilizationController::class);

        Route::resource('plan_exp', PlannedExpenseController::class);
        Route::post('plan_exp/copy', [PlannedExpenseController::class, 'copy'])->name('plan_exp.copy');

        Route::get('exptracking/{exptracking}/delete', [ExpenseTrackingController::class, 'destroy'])->name('exptracking.delete');
        Route::resource('exptracking', ExpenseTrackingController::class);
        Route::post('/exptracking/validate', [ExpenseTrackingController::class, 'ajaxValidation']);
        Route::get('/exptracking/{expensetracking}/approve', [ExpenseTrackingController::class, 'approve'])->name('exptracking.approve');
        Route::post('/exptracking/batchapprove', [ExpenseTrackingController::class, 'batchapprove']);
        Route::post('/exptracking/batchpay', [ExpenseTrackingController::class, 'batchpay']);
        Route::post('/exptracking/batchreject', [ExpenseTrackingController::class, 'batchreject']);
        Route::get('/exptracking/{expensetracking}/reject', [ExpenseTrackingController::class, 'reject'])->name('exptracking.reject');
        Route::get('/exptracking/{expensetracking}/pay', [ExpenseTrackingController::class, 'pay'])->name('exptracking.pay');
        Route::get('storage/exp_tracking', [AssetController::class, 'getExpTracking'])->name('exp_tracking');
        Route::post('get_ass_company', [ExpenseTrackingController::class, 'company']);
        Route::post('get_resource', [ExpenseTrackingController::class, 'resource']);
        Route::post('expense_session_put', [ExpenseTrackingController::class, 'sessionStore'])->name('expense.session.store');
        Route::post('expense_session_pull', [ExpenseTrackingController::class, 'sessionPull'])->name('expense.session.pull');

        Route::get('/exptrackinginsight', [ExpenseTrackingInsightController::class, 'index'])->name('expensetrackinginsight.index');
        Route::get('/exptrackinginsightaccount', [ExpenseTrackingInsightController::class, 'account'])->name('expensetrackinginsight.account');

        Route::get('resource/{resource}/print', [ResourceController::class, 'print'])->name('resource.print');
        Route::get('resource/{resource}/send', [ResourceController::class, 'send'])->name('resource.send');
        Route::resource('resource', ResourceController::class);

        Route::get('cvsubmit/getcustomeremail/{job_spec_id}', [CvSubmitController::class, 'getCustomerEmail'])->name('cvsubmit.getcustomeremail');
        Route::post('cvsubmit/submit', [CvSubmitController::class, 'submit'])->name('cvsubmit.submit');
        Route::resource('cvsubmit', CvSubmitController::class);
        Route::post('cv/getspeciality/{id}', [CvController::class, 'getSpeciality'])->name('cv.getspeciality');
        Route::get('cv/submit/{id}', [CvController::class, 'submit'])->name('cv.submit');
        Route::get('cv/user', [CvController::class, 'user'])->name('cv.user');
        Route::post('cv/generate/{cv_type}', [CvController::class, 'generateCV'])->name('cv.generate');
        Route::post('cv/emailcandidate', [CvController::class, 'emailCandidate'])->name('cv.emailcandidate');
        Route::post('cv/send/{cv_id}', [CvController::class, 'send'])->name('cv.send');
        Route::get('cv/search', [CvController::class, 'search'])->name('cv.search');
        Route::resource('cv', CvController::class);
        Route::resource('cvreference', CVReferenceController::class);
        Route::get('cv/{cv}', [CvController::class, 'show'])->name('cv.show');
        Route::get('cv/{cvid}/edit/{res_id}', [CvController::class, 'edit'])->name('cv.edit');
        Route::post('cv/{cvid}/update/{res_id}', [CvController::class, 'update'])->name('cv.update');
        Route::post('cv', [CvController::class, 'store'])->name('cv.store');

        Route::get('cv/{cv_id}/{res_id}/download/{template_id}', [CvController::class, 'download'])->name('cv.download');

        Route::resource('availability', AvailabilityController::class);
        Route::get('availability/create/{cvid}/{res_id}', [AvailabilityController::class, 'create'])->name('availability.create');
        Route::get('availability/{availability}/edit/{cv}/{resource}', [AvailabilityController::class, 'edit'])->name('availability.edit');
        Route::post('availability/{availability}/update/{cv}/{resource}', [AvailabilityController::class, 'update'])->name('availability.update');
        Route::post('availability/{cvid}/{res_id}', [AvailabilityController::class, 'store'])->name('availability.store');

        Route::resource('skill', SkillController::class);
        Route::get('skill/{skill}/{cv}', [SkillController::class, 'show'])->name('skill.show');
        Route::get('skill/create/{cvid}/{res_id}', [SkillController::class, 'create'])->name('skill.create');
        Route::get('skill/{skill}/edit/{cv}/{resource}', [SkillController::class, 'edit'])->name('skill.edit');
        Route::post('skill/{skillid}/update/{cvid}/{res_id}', [SkillController::class, 'update'])->name('skill.update');
        Route::post('skill/{cvid}/{res_id}', [SkillController::class, 'store'])->name('skill.store');

        Route::resource('qualification', QualificationController::class);
        Route::get('qualification/{qualification}', [QualificationController::class, 'show'])->name('qualification.show');
        Route::get('qualification/create/{cvid}/{res_id}', [QualificationController::class, 'create'])->name('qualification.create');
        Route::get('qualification/{qualificationid}/edit/{cvid}/{res_id}', [QualificationController::class, 'edit'])->name('qualification.edit');
        Route::post('qualification/{qualificationid}/update/{cvid}/{res_id}', [QualificationController::class, 'update'])->name('qualification.update');
        Route::post('qualification/{cvid}/{res_id}', [QualificationController::class, 'store'])->name('qualification.store');

        Route::resource('experience', ExperienceController::class);
        Route::get('experience/{experienceid}', [ExperienceController::class, 'show'])->name('experience.show');
        Route::get('experience/create/{cvid}/{res_id}', [ExperienceController::class, 'create'])->name('experience.create');
        Route::get('experience/{experienceid}/edit/{cvid}/{res_id}', [ExperienceController::class, 'edit'])->name('experience.edit');
        Route::post('experience/{experienceid}/update/{cvid}/{res_id}', [ExperienceController::class, 'update'])->name('experience.update');
        Route::post('experience/{cvid}/{res_id}', [ExperienceController::class, 'store'])->name('experience.store');

        Route::resource('iexperience', IndustryExperienceController::class);
        Route::get('iexperience/{iexperienceid}', [IndustryExperienceController::class, 'show'])->name('industry.show');
        Route::get('iexperience/create/{cvid}/{res_id}', [IndustryExperienceController::class, 'create'])->name('industry.create');
        Route::get('iexperience/{iexperienceid}/edit/{cvid}', [IndustryExperienceController::class, 'edit'])->name('industry.edit');
        Route::post('iexperience/{iexperienceid}/update/{cvid}/{res_id}', [IndustryExperienceController::class, 'update'])->name('industry.update');
        Route::post('iexperience/{cvid}/{res_id}', [IndustryExperienceController::class, 'store'])->name('industry.store');
        Route::post('iexperience/{iexperienceid}', [IndustryExperienceController::class, 'destroy'])->name('industry.destroy');

        Route::resource('anniversary', AnniversaryController::class);

        Route::resource('logbook', LogbookController::class);
        Route::get('logbook/{logbookid}', [LogbookController::class, 'show'])->name('logbook.show');
        Route::get('logbook/{logbookid}/edit', [LogbookController::class, 'edit'])->name('logbook.edit');
        Route::post('logbook/{logbookid}/update', [LogbookController::class, 'update'])->name('logbook.update');

        Route::get('assetreg/print/{assetregid}', [AssetRegisterController::class, 'print'])->name('assetreg.print');
        Route::resource('assetreg', AssetRegisterController::class);
        Route::post('assetreg/senddigisign/{assetregid}', [AssetRegisterController::class, 'sendDigisign'])->name('assetreg.senddigisign');
        Route::post('assetreg/writeoff/{asset}', [AssetRegisterController::class, 'writeOff'])->name('assetreg.writeoff');
        Route::get('assetconditionnote/add/{asset_id}', [AssetConditionNoteController::class, 'add'])->name('assetconditionnote.add');
        Route::resource('assetconditionnote', AssetConditionNoteController::class);

        Route::resource('leave', LeaveController::class);
        Route::get('leave/getleavedays/{start_date}/{end_date}', [LeaveController::class, 'getLeaveDays'])->name('leave.getleavedays');
        Route::get('leave/{leaveid}/approve', [LeaveController::class, 'approve'])->name('leave.approve');
        Route::get('leave/{leaveid}/decline', [LeaveController::class, 'decline'])->name('leave.decline');
        Route::post('leave/update/{leaveid}', [LeaveController::class, 'update'])->name('leave.update');
        Route::post('leave/getallannualleave', [LeaveController::class, 'getAllAnnualLeaveDays']);
        Route::post('/getassignment', [LeaveController::class, 'getAssignment']);

        Route::get('leave_balance/{user_id}/getresource', [LeaveBalanceController::class, 'getResource']);
        Route::resource('leave_balance', LeaveBalanceController::class);

        Route::resource('leave_statement', LeaveStatementController::class);
        Route::get('leave_from/get/{from}', [LeaveStatementController::class, 'getToPeriod']);
        Route::get('leave_from/checkjoindate/{resourceid}', [LeaveStatementController::class, 'checkjoinDate']);

        Route::resource('commission', CommissionController::class);
        Route::get('commission/{commid}', [CommissionController::class, 'show'])->name('commission.show');
        Route::get('commission/{commid}/edit', [CommissionController::class, 'edit'])->name('commission.edit');
        Route::post('commission/{commid}/update', [CommissionController::class, 'update'])->name('commission.update');

        Route::resource('resource_task', ResourceTaskController::class);
        Route::get('resource_task/{taskid}', [ResourceTaskController::class, 'show'])->name('resource_task.show');
        Route::get('resource_task/{taskid}/edit', [ResourceTaskController::class, 'edit'])->name('resource_task.edit');
        Route::post('resource_task/{taskid}/update', [ResourceTaskController::class, 'update'])->name('resource_task.update');

        Route::resource('customerpreference', CustomerPreferenceController::class);
        Route::get('customer/{customer}/print', [CustomerController::class, 'print'])->name('customer.print');
        Route::get('customer/{customer}/send', [CustomerController::class, 'send'])->name('customer.send');
        Route::get('customer/{customer}/printnda', [CustomerController::class, 'printNDA'])->name('customer.print.nda');
        Route::get('customer/{customer}/sendnda', [CustomerController::class, 'sendNDA'])->name('customer.send.nda');
        Route::resource('customer', CustomerController::class);

        Route::resource('site_report', SiteReportController::class);
        Route::post('site_report/store', [SiteReportController::class, 'store'])->name('site_report.store');
        Route::get('site_report/copy/{id}', [SiteReportController::class, 'copySiteReport'])->name('site_report.copy');
        Route::any('site_report/pdf/{id}', [SiteReportController::class, 'siteReportToPdf'])->name('site_report.pdf');
        // Route::post('site_report/{site_report_id}/update', [SiteReportController::class, 'update'])->name('site_report.update');
        // Route::post('add_task', [SiteReportController::class, 'add_task']);
        // Route::post('add_issue', [SiteReportController::class, 'add_issue']);
        // Route::Post('add_risk', [SiteReportController::class, 'add_risk']);
        // Route::post('add_escalation', [SiteReportController::class, 'add_escalation']);
        // Route::post('add_suggestion', [SiteReportController::class, 'add_suggestion']);
        // Route::post('add_next', [SiteReportController::class, 'add_next']);
        // Route::delete('delete_task/{id}', [SiteReportController::class, 'delete_task'])->name('site_report.delete_task');
        // Route::delete('delete_issue/{id}', [SiteReportController::class, 'delete_issue'])->name('site_report.delete_issue');
        // Route::delete('delete_risk/{id}', [SiteReportController::class, 'delete_risk'])->name('site_report.delete_risk');
        // Route::delete('delete_escalation/{id}', [SiteReportController::class, 'delete_escalation'])->name('site_report.delete_escalation');
        // Route::delete('delete_suggestion/{id}', [SiteReportController::class, 'delete_suggestion'])->name('site_report.delete_suggestion');
        // Route::delete('delete_next/{id}', [SiteReportController::class, 'delete_next'])->name('site_report.delete_next');
        // Route::get('edit_task/{id}/edit', [SiteReportController::class, 'edit_task'])->name('site_report.edit_task');
        // Route::patch('update_task/{id}', [SiteReportController::class, 'update_task'])->name('site_report.update_task');
        // Route::get('edit_issue/{id}/edit', [SiteReportController::class, 'edit_issue'])->name('site_report.edit_issue');
        // Route::patch('update_issue/{id}', [SiteReportController::class, 'update_issue'])->name('site_report.update_issue');
        // Route::get('edit_risk/{id}/edit', [SiteReportController::class, 'edit_risk'])->name('site_report.edit_risk');
        // Route::patch('update_risk/{id}', [SiteReportController::class, 'update_risk'])->name('site_report.update_risk');
        // Route::get('edit_escalation/{id}/edit', [SiteReportController::class, 'edit_escalation'])->name('site_report.edit_escalation');
        // Route::patch('update_escalation/{id}', [SiteReportController::class, 'update_escalation'])->name('site_report.update_escalation');
        // Route::get('edit_suggestion/{id}/edit', [SiteReportController::class, 'edit_suggestion'])->name('site_report.edit_suggestion');
        // Route::patch('update_suggestion/{id}', [SiteReportController::class, 'update_suggestion'])->name('site_report.update_suggestion');
        // Route::get('edit_next/{id}/edit', [SiteReportController::class, 'edit_next'])->name('site_report.edit_next');
        // Route::patch('update_next/{id}', [SiteReportController::class, 'update_next'])->name('site_report.update_next');

        Route::resource('cost_calculator', CostCalculatorController::class);
        Route::post('cost_calculator', [CostCalculatorController::class, 'calculate'])->name('cost_calculator.calculate');

        Route::get('prospects/overview', [ProspectController::class, 'overview']);
        Route::any('prospects/kanban', [ProspectController::class, 'kanban']);
        Route::resource('prospects', ProspectController::class);
        Route::get('prospects/{prospectid}', [ProspectController::class, 'show'])->name('prospects.show');
        Route::get('prospects/{prospectid}/edit', [ProspectController::class, 'edit'])->name('prospects.edit');
        Route::post('prospects/{prospectid}/update', [ProspectController::class, 'update'])->name('prospects.update');
        Route::post('prospects/delete/{prospectid}', [ProspectController::class, 'destroy']);
        Route::post('prospects/{prospectid}/copy', [ProspectController::class, 'copy']);
        Route::post('prospects', [ProspectController::class, 'store'])->name('prospects.store');
        Route::post('prospects/setemote/{id}', [ProspectController::class, 'setEmote']);
        Route::get('prospects/loadmore/{status}', [ProspectController::class, 'loadMore']);

        Route::get('vendor/{vendor}/print', [VendorController::class, 'print'])->name('vendor.print');
        Route::get('vendor/{vendor}/send', [VendorController::class, 'send'])->name('vendor.send');
        Route::get('vendor/{vendor}/printnda', [VendorController::class, 'printNDA'])->name('vendor.print.nda');
        Route::get('vendor/{vendor}/sendnda', [VendorController::class, 'sendNDA'])->name('vendor.send.nda');
        Route::resource('vendors', VendorController::class);

        Route::get('roles', [RoleController::class, 'loadpermissions'])->name('roles.index');
        Route::get('roles/utilities', [RoleController::class, 'index_utilities'])->name('roles.index_utilities');
        Route::get('roles/company', [RoleController::class, 'index_company'])->name('roles.index_company');
        Route::get('roles/resource', [RoleController::class, 'index_resource'])->name('roles.index_resource');
        Route::get('roles/customer', [RoleController::class, 'index_customer'])->name('roles.index_customer');
        Route::get('roles/vendor', [RoleController::class, 'index_vendor'])->name('roles.index_vendor');
        Route::get('roles/insight', [RoleController::class, 'index_insight'])->name('roles.index_insight');
        Route::get('roles/admin', [RoleController::class, 'index_admin'])->name('roles.index_admin');
        Route::get('roles/create', [RoleController::class, 'create'])->name('roles.create');
        Route::post('roles', [RoleController::class, 'store'])->name('roles.store');
        Route::put('roles', [RoleController::class, 'update'])->name('roles.update');
        Route::post('roles/updateadmin', [RoleController::class, 'update_admin'])->name('roles.update_admin');
        Route::post('roles/updateutilities', [RoleController::class, 'update_utilities'])->name('roles.update_utilities');
        Route::post('roles/updatecompany', [RoleController::class, 'update_company'])->name('roles.update_company');
        Route::post('roles/updateresource', [RoleController::class, 'update_resource'])->name('roles.update_resource');
        Route::post('roles/updatecustomer', [RoleController::class, 'update_customer'])->name('roles.update_customer');
        Route::post('roles/updatevendor', [RoleController::class, 'update_vendor'])->name('roles.update_vendor');
        Route::post('roles/updateinsights', [RoleController::class, 'update_insights'])->name('roles.update_insights');
        Route::delete('roles/{role?}', [RoleController::class, 'destroy'])->name('roles.destroy');

        Route::get('users/expired', [UserController::class, 'expired_users'])->name('users.expired');
        Route::get('users/expires/30', [UserController::class, 'expires_in_30'])->name('users.expires_in_30');
        Route::put('users/updatesuper/{user}', [UserController::class, 'updateSuper'])->name('users.updatesuper');
        Route::get('users/generate_token/{user}', [UserController::class, 'generateToken'])->name('users.generateToken');
        Route::get('users/revoke_token/{user}/{api}', [UserController::class, 'revokeToken'])->name('users.revokeToken');
        Route::resource('users', UserController::class);

        Route::get('users/{user}/delete', [UserController::class, 'destroy'])->name('users.destroy');
        Route::post('users/{user}/resend', [UserController::class, 'resendCredentials'])->name('resend.credentials');
        Route::post('users/{user}/resendadmin', [UserController::class, 'resendCredentialsToAdmin'])->name('resendadmin.credentials');

        Route::get('configs', [ConfigController::class, 'index'])->name('configs.index');
        Route::put('configs/{config}', [ConfigController::class, 'update'])->name('configs.update');
        Route::put('configs', [ConfigController::class, 'store'])->name('configs.store');

        Route::resource('calendar', CalendarController::class);
        Route::post('/get_events', [CalendarController::class, 'get_events']);
        Route::post('/get_roles', [RoleController::class, 'get_roles']);
        Route::post('/get_roles/utilities', [RoleController::class, 'get_utilities_roles']);
        Route::post('/get_roles/company', [RoleController::class, 'get_company_roles']);
        Route::post('/get_roles/resource', [RoleController::class, 'get_resource_roles']);
        Route::post('/get_roles/customer', [RoleController::class, 'get_customer_roles']);
        Route::post('/get_roles/vendor', [RoleController::class, 'get_vendor_roles']);
        Route::post('/get_roles/insights', [RoleController::class, 'get_insights_roles']);
        Route::post('/get_roles/admin', [RoleController::class, 'get_admin_roles']);

        Route::resource('calendarevents', CalendarEventsController::class);
        Route::get('calendarevents/accept/{id}', [CalendarEventController::class, 'accept'])->name('calendarevents.accept');
        Route::get('calendarevents/reject/{id}', [CalendarEventController::class, 'reject'])->name('calendarevents.reject');

        Route::get('recents', [HomeController::class, 'recents'])->name('recents');

        Route::resource('quotation', QuotationController::class);
        Route::resource('project', ProjectController::class);
        Route::get('project/{project_id}/copy', [ProjectController::class, 'copy'])->name('project.copy');
        Route::get('project/{project}/complete', [ProjectController::class, 'complete'])->name('project.complete');
        Route::get('project/getassignmenttasks/{project}', [ProjectController::class, 'getAssignmentTasks']);
        Route::get('assignment/gettasks/{assignment}', [AssignmentController::class, 'getTasks']);
        Route::get('/assignment/get-assignment-roles', [AssignmentController::class, 'getAssignmentRoles']);
        Route::get('/assignment/{assignment}/supplier/print', [AssignmentController::class, 'printSupplierAgreement'])->name('assignment.supplier.print');
        Route::get('/assignment/{assignment}/resource/print', [AssignmentController::class, 'printResourceAgreement'])->name('assignment.resource.print');
        Route::get('/assignment/{assignment}/supplier/send', [AssignmentController::class, 'sendSupplierAgreement'])->name('assignment.supplier.send');
        Route::get('/assignment/{assignment}/resource/send', [AssignmentController::class, 'sendResourceAgreement'])->name('assignment.resource.send');
        Route::resource('assignment', AssignmentController::class);
        Route::get('assignment/{assignment}/complete', [AssignmentController::class, 'complete'])->name('assignment.complete');
        Route::get('exchange-rate', [AssignmentController::class, 'currency']);
        Route::post('project/add-project-consultant', [ProjectController::class, 'addProjectConsultant']);
        Route::post('project/update-project-consultant', [ProjectController::class, 'updateProjectConsultant']);
        Route::post('project/remove-project-consultant', [ProjectController::class, 'removeProjectConsultant']);
        Route::get('assignment/{project}/add/{resource}', [AssignmentController::class, 'add'])->name('assignment.add');
        Route::get('assignment/addcost/{assignmentid}', [AssignmentController::class, 'addAssCost'])->name('assignment.add_ass_cost');
        Route::get('assignment/addcost/{ass_cost_id}/edit', [AssignmentController::class, 'editAssCost'])->name('assignment.edit_ass_cost');
        Route::post('get_account_element', [AssignmentController::class, 'get_account_type'])->name('assignment.account_element');
        Route::post('assignment/cost_store', [AssignmentController::class, 'storeAssCost'])->name('cost.store');
        Route::get('assignment/senddocuments/{assignment_id}', [AssignmentController::class, 'sendDocuments'])->name('assignment.senddocuments');
        Route::get('assignment/sendresourcedigisign/{assignment_id}', [AssignmentController::class, 'sendResourceAssignmentDigisign'])->name('assignment.sendresourcedigisign');
        Route::get('assignment/sendsupplierdigisign/{assignment_id}', [AssignmentController::class, 'sendSupplierAssignmentDigisign'])->name('assignment.sendsupplierdigisign');
        Route::post('getaccounttype', [AssignmentController::class, 'getAccountType']);
        Route::get('assignment/addextension/{assignment_id}', [AssignmentController::class, 'addExtension'])->name('assignment.addextension');
        Route::get('assignment/editextension/{assignment_id}', [AssignmentController::class, 'editExtension'])->name('assignment.editextension');
        Route::put('assignment/saveextension/{assignment_id}', [AssignmentController::class, 'saveExtension'])->name('assignment.saveextension');
        Route::put('assignment/updateextension/{extension_id}', [AssignmentController::class, 'updateExtension'])->name('assignment.updateextension');
        Route::delete('assignment/deleteextension/{extension_id}', [AssignmentController::class, 'deleteExtension'])->name('assignment.deleteextension');
        Route::get('document/supplierassignment/{digisign_user_id}', [DocumentController::class, 'supplierAssignment'])->name('document.supplierassignment');
        Route::get('document/resourceassignment/{document_id}', [DocumentController::class, 'resourceAssignment'])->name('document.resourceassignment');
        Route::get('document/assessmentdigisign/{document_id}', [DocumentController::class, 'assessmentDigisign'])->name('document.assessmentdigisign');
        Route::get('document/pdfviewer/{document_id}', [DocumentController::class, 'pdfViewer'])->name('document.pdfviewer');
        Route::get('document/viewer/{document_id}', [DocumentController::class, 'viewer'])->name('document.viewer');
        Route::get('document/asset/{digisign_user_id}', [DocumentController::class, 'asset'])->name('document.asset');
        Route::get('document/download', [DocumentController::class, 'download'])->name('document.download');
        Route::post('document/updatename/{document_id}', [DocumentController::class, 'updateDocumentName'])->name('document.updatename');
        Route::resource('document', DocumentController::class);

        //master data
        Route::resource('master_calendar', MCalendarController::class);
        //Route::get('master_calendar/create','MCalendarController@create')->name('master_calendar.create');
        Route::get('master_calendar/create/{type}', [MCalendarController::class, 'create'])->name('master_calendar.create');
        Route::get('master_calendar/{mcalendarid}', [MCalendarController::class, 'show'])->name('master_calendar.show');
        Route::get('master_calendar/{mcalendarid}/edit', [MCalendarController::class, 'edit'])->name('master_calendar.edit');
        Route::post('master_calendar/{mcalendarid}/update', [MCalendarController::class, 'update'])->name('master_calendar.update');
        Route::post('master_calendar', [MCalendarController::class, 'store'])->name('master_calendar.store');
        Route::post('master_calendar/storerange', [MCalendarController::class, 'storerange'])->name('master_calendar.storerange');

        Route::resource('master_term', TermsController::class);
        Route::get('master_term/create', [TermsController::class, 'create'])->name('master_term.create');
        Route::get('master_term/{termid}', [TermsController::class, 'show'])->name('master_term.show');
        Route::get('master_term/{termid}/edit', [TermsController::class, 'edit'])->name('master_term.edit');
        Route::post('master_term/{termid}/update', [TermsController::class, 'update'])->name('master_term.update');
        Route::post('master_term', [TermsController::class, 'store'])->name('master_term.store');

        Route::resource('master_cost', CostCentreController::class);
        Route::get('master_cost/create', [CostCentreController::class, 'create'])->name('master_cost.create');
        Route::get('master_cost/{costid}', [CostCentreTermsController::class, 'show'])->name('master_cost.show');
        Route::get('master_cost/{costid}/edit', [CostCentreController::class, 'edit'])->name('master_cost.edit');
        Route::post('master_cost/{costid}/update', [CostCentreController::class, 'update'])->name('master_cost.update');
        Route::post('master_cost', [CostCentreController::class, 'store'])->name('master_cost.store');

        Route::resource('master_country', CountryController::class);
        Route::get('master_country/create', [CountryController::class, 'create'])->name('master_country.create');
        Route::get('master_country/{countryid}', [CountryController::class, 'show'])->name('master_country.show');
        Route::get('master_country/{countryid}/edit', [CountryController::class, 'edit'])->name('master_country.edit');
        Route::post('master_country/{countryid}/update', [CountryController::class, 'update'])->name('master_country.update');
        Route::post('master_country', [CountryController::class, 'store'])->name('master_country.store');

        Route::resource('master_dashboard', DefaultDashboardController::class);

        Route::resource('master_bankaccounttype', BankAccountTypeController::class);

        Route::resource('cv_company', CvCompanyController::class);
        Route::post('cv_company/{cv_company_id}/update', [CvCompanyController::class, 'update'])->name('cv_company.update');

        Route::resource('resource_level', ResourceLevelController::class);
        Route::post('resource_level/{resource_level_id}/update', [ResourceLevelController::class, 'update'])->name('resource_level.update');

        Route::resource('resource_position', ResourcePositionController::class);
        Route::post('resource_position/{resource_position_id}/update', [ResourcePositionController::class, 'update'])->name('resource_position.update');

        Route::resource('leave_type', LeaveTypeController::class);
        Route::post('leave_type/{leave_type_id}/update', [LeaveTypeController::class, 'update'])->name('leave_type.update');

        Route::resource('marital_status', MaritalStatusController::class);
        Route::post('marital_status/{marital_status_id}/update', [MaritalStatusController::class, 'update'])->name('marital_status.update');

        Route::resource('anniversarytype', AnniversaryTypeController::class);
        Route::post('anniversarytype/{anniversarytype_id}/update', [AnniversaryTypeController::class, 'update'])->name('anniversarytype.update');

        Route::resource('relationship', RelationshipController::class);
        Route::post('relationship/{relationship_id}/update', [RelationshipController::class, 'update'])->name('relationship.update');

        Route::resource('asset_class', AssertsClassController::class);
        Route::post('asset_class/{assert_class_id}/update', [AssertsClassController::class, 'update'])->name('asset_class.update');

        Route::resource('industry', IndustryController::class);
        Route::post('industry/{industry_id}/update', [IndustryController::class, 'update'])->name('industry.update');

        Route::resource('status', StatusController::class);
        Route::post('status/{status_id}/update', [StatusController::class, 'update'])->name('status.update');

        Route::resource('event', EventController::class);
        Route::post('event/{event_id}/update', [EventController::class, 'update'])->name('event.update');

        Route::resource('business_function', BusinessFunctionController::class);
        Route::post('business_function/{business_function_id}/update', [BusinessFunctionController::class, 'update'])->name('business_function.update');

        Route::resource('invoice_status', InvoiceStatusController::class);
        Route::post('invoice_status/{invoice_status_id}/update', [InvoiceStatusController::class, 'update'])->name('invoice_status.update');

        Route::resource('payment_method', PaymentMethodController::class);
        Route::post('payment_method/{payment_method_id}/update', [PaymentMethodController::class, 'update'])->name('payment_method.update');

        Route::resource('skill_level', SkillLevelController::class);
        Route::post('skill_level/{skill_level_id}/update', [SkillLevelController::class, 'update'])->name('skill_level.update');

        Route::resource('solution', SolutionController::class);
        Route::post('solution/{solution_id}/update', [SolutionController::class, 'update'])->name('solution.update');

        Route::resource('system', SystemController::class);
        Route::post('system/{system_id}/update', [SystemController::class, 'update'])->name('system.update');


        Route::get('system_health/get-category-list/{category}', [SystemHealthController::class, 'getCategoryList']);
        Route::get('system_health/fix-kpi/{category}', [SystemHealthController::class, 'fixKpi']);
        Route::resource('system_health', SystemHealthController::class);

        Route::resource('system_health_master', SystemHealthMasterDataController::class);
        Route::put('system_health_master/{system_id}/update', [SystemHealthMasterDataController::class, 'update'])->name('system_health_master.update');

        Route::resource('vat_rate', VatRateController::class);
        Route::post('vat_rate/{vat_rate_id}/update', [VatRateController::class, 'update'])->name('vat_rate.update');
        Route::post('getVatRate', [VatRateController::class, 'getVat']);

        Route::resource('project_type', ProjectTypeController::class);
        Route::post('project_type/{project_type_id}/update', [ProjectTypeController::class, 'update'])->name('project_type.update');

        Route::resource('medical_master', MedicalCertificateTypeController::class);
        Route::post('medical_master/{medical_master_id}/update', [MedicalCertificateTypeController::class, 'update'])->name('medical_master.update');

        Route::resource('account_type', AccountTypeController::class);
        Route::post('account_type/{account_type_id}/update', [AccountTypeController::class, 'update'])->name('account_type.update');

        Route::resource('account', AccountController::class);
        Route::post('account/{account_id}/update', [AccountController::class, 'update'])->name('account.update');

        Route::resource('account_element', AccountElementController::class);
        Route::post('account_element/{account_element_id}/update', [AccountElementController::class, 'update'])->name('account_element.update');

        Route::resource('industry_experience', IndustryExperienceController::class);
        Route::post('industry_experience/{industry_experience_id}/update', [IndustryExperienceController::class, 'update'])->name('industry_experience.update');

        Route::resource('bee_master', BEEMasterDataController::class);
        Route::post('bee_master/{bee_master_id}/update', [BEEMasterDataController::class, 'update'])->name('bee_master.update');

        Route::resource('quotation_terms', QuotationTermsController::class);
        Route::post('quotation_terms/{quotation_terms_id}/update', [QuotationTermsController::class, 'update'])->name('quotation_terms.update');

        Route::get('vendor/invoice/view', [VendorInvoiceController::class, 'index'])->name('vendor.invoice');
        Route::post('get_ajax', [VendorInvoiceController::class, 'getAjax'])->name('get_ajax');
        Route::post('vendor/invoice/calculate', [VendorInvoiceController::class, 'calculate'])->name('vendor.calculate');
        Route::get('vendor/invoice/generated', [VendorInvoiceController::class, 'calculate'])->name('vendor.generated');
        Route::get('vendor/assignment', [VendorInvoiceController::class, 'calculate'])->name('assignment');
        Route::post('/readnotificationshistory', [UserController::class, 'readNotificationsHistory']);
        Route::resource('notifications', NotificationHistoryController::class);

        Route::get('proforma_invoice', [ProFormaVendarInvoiceController::class, 'index']);
        Route::post('proforma_invoice/view', [ProFormaVendarInvoiceController::class, 'calculate'])->name('proforma.calculate');

        Route::resource('vendor/invoice/function', VendorInvoiceFunctionController::class);
        Route::get('vendor/invoice/function/create', [VendorInvoiceFunctionController::class, 'create'])->name('function_newinvoice.create');
        Route::post('vendor/invoice/function/prepare', [VendorInvoiceFunctionController::class, 'prepare'])->name('prepare.invoice');
        Route::post('vendor/invoice/function/{invoice_id}/edit', [VendorInvoiceFunctionController::class, 'edit'])->name('edit.invoice');
        Route::patch('vendor/invoice/function/{invoice_id}/update', [VendorInvoiceFunctionController::class, 'update']);
        Route::get('vendor/invoice/function/prepare', [VendorInvoiceFunctionController::class, 'prepare'])->name('prepare.invoice');
        Route::post('vendor/invoice/function/generate', [VendorInvoiceFunctionController::class, 'generate'])->name('generate.invoice');
        Route::get('vendor/invoice/send', [VendorInvoiceFunctionController::class, 'send_invoice'])->name('send_invoice');
        /*Route::get('vendor/invoice/function/save', 'VendorInvoiceFunctionController@saveInvoice')->name('save.invoice');*/
        Route::get('vendor/invoice/save', [VendorInvoiceFunctionController::class, 'save'])->name('save.invoice');
        Route::delete('vendor/invoice/function/{invoice_id}', [VendorInvoiceFunctionController::class, 'destroy'])->name('delete.invoice');
        Route::get('vendor/invoice/{vendorInvoice}', [VendorInvoiceFunctionController::class, 'show'])->name('view.invoice');
        Route::get('vendor/invoice/pdf/{vendor_invoice}', [VendorInvoiceFunctionController::class, 'generatePDF'])->name('invoice.document');
        Route::post('vendor/invoice/function/unallocate/{id}', [VendorInvoiceFunctionController::class, 'unallocate'])->name('unallocate.invoice');
        Route::post('vendor_select_timesheet/{id}', [VendorInvoiceController::class, 'storeTimesheetId']);
        Route::post('store_timesheet', [Controller::class, 'sessionStore'])->name('session.store');
        Route::post('pull_timesheet', [Controller::class, 'sessionPull'])->name('session.pull');

        Route::resource('vendor/invoice/payment', VendorInvoicePaymentController::class);
        Route::get('vendor/invoice/process/payment/{id}', [VendorInvoicePaymentController::class, 'process'])->name('process.payment');
        Route::post('vendor/invoice/payment/store', [VendorInvoicePaymentController::class, 'store'])->name('process');

        Route::resource('vendor_invoice_status', VendorInvoiceStatusController::class);

        Route::get('timesheet/expenses', [TimesheetController::class, 'addExpenses'])->name('timesheet.expenses');
        Route::post('get_timesheet', [TimesheetController::class, 'get_timesheet']);
        Route::delete('timesheet/destroy/{timesheet_id}', [TimesheetController::class, 'destroy'])->name('timesheet.destroy');
        Route::post('timesheet/changelock',[TimesheetNewController::class, 'toggleTimesheetLock']);
        Route::post('timesheet/updatenote',[TimesheetNewController::class, 'updateNote']);
        Route::get('timesheet/add_time', [TimesheetController::class, 'addTime'])->name('timesheet.addtime');
        Route::get('timesheet/{timesheet_id}/addtimeline', [TimesheetController::class, 'addTimeLine'])->name('timesheet.addtimeline');
        Route::get('timesheet/{time_id}/edit_time', [TimesheetController::class, 'editTime'])->name('timesheet.edittime');
        Route::get('timesheet/{timesheet}/edittimeline/{timeline_id}', [TimesheetController::class, 'editTimeline'])->name('timesheet.edittimeline');
        Route::get('timesheet/{time_id}/maintain', [TimesheetController::class, 'maintain'])->name('timesheet.maintain');
        Route::get('timesheet/expense/{time_expense_id}', [TimesheetController::class, 'approveExpense'])->name('expense.approve');
        Route::get('invoice/expense/{time_expense_id}', [TimesheetController::class, 'declineExpense'])->name('expense.declined');
        Route::get('timesheet/expense/{time_expense_id}/paid', [TimesheetController::class, 'payExpense'])->name('expense.paid');
        Route::get('timesheet/{timesheet_id}/capturetime', [TimesheetController::class, 'captureTime'])->name('timesheet.capturetime');
        Route::get('timesheet/{timesheet}/createtimeline', [TimesheetController::class, 'createTimeLine'])->name('timesheet.createtimeline');
        Route::get('timesheet/checktimesheet', [TimesheetController::class, 'checkTimesheets'])->name('timesheet.check');
        Route::post('timesheet/{timesheet}/storetimeline', [TimesheetController::class, 'storeTimeLine'])->name('timesheet.storetimeline');
        Route::put('timesheet/{timesheet_id}/updatetimeline', [TimesheetController::class, 'updateTimeLine'])->name('timesheet.updatetimeline');
        Route::delete('timesheet/{timesheet_id}/destroytimeline', [TimesheetController::class, 'destroyTimeLine'])->name('timesheet.destroytimeline');
        Route::get('timesheet/{timesheet_id}/createexpense', [TimesheetController::class, 'createExpense'])->name('timesheet.createexpense');
        Route::post('timesheet/{timesheet_id}/storeexpense', [TimesheetController::class, 'storeExpense'])->name('timesheet.storeexpense');
        Route::put('timesheet/{timesheet_id}/updateexpense', [TimesheetController::class, 'updateExpense'])->name('timesheet.updateexpense');
        Route::get('timesheet/{timesheet_id}/editexpense/{expense_id}', [TimesheetController::class, 'editExpense'])->name('timesheet.editexpense');
        Route::delete('timesheet/{timesheet_id}/destroyexpense', [TimesheetController::class, 'destroyExpense'])->name('timesheet.destroyexpense');
        Route::get('timesheet/{resource_id}/getprojects', [TimesheetController::class, 'getProjects'])->name('timesheet.getprojects');
        Route::get('timesheet/{resource_id}/getclients', [TimesheetController::class, 'getClients'])->name('timesheet.getclients');
        Route::get('task-delivery-type/{task}', [TimesheetController::class, 'taskDeliveryId']);
        Route::get('get-project-timesheets/{timesheet}', [TimesheetNewController::class, 'getProjectTimesheets']);
        Route::get('get-timesheet-customer-invoice/{invoice}', [TimesheetNewController::class, 'getInvoiceDetails']);
        Route::get('get-timesheet-vendor-invoice/{invoice}', [TimesheetNewController::class, 'getVendorInvoiceDetails']);
        Route::post('maintain_projects', [TimesheetController::class, 'maintain_timesheet']);
        Route::get('getclientprojects/{resource_id}', [TimesheetController::class, 'getClientProjects']);
        Route::get('year_weeks/{project}', [TimesheetController::class, 'timesheetYearWeek']);
        Route::post('timesheet/senddigisign/{timesheet_id}', [TimesheetController::class, 'sendDigiSign'])->name('timesheet.senddigisign');
        Route::post('timesheet/emailtimesheet/{timesheet_id}', [TimesheetController::class, 'emailTimesheet'])->name('timesheet.emailtimesheet');
        Route::patch('timesheet/{tiesheet_id}', [TimesheetController::class, 'store_maintain'])->name('timesheet.store_maintain');
        Route::get('timesheet/addattachment/{tiesheet_id}', [TimesheetController::class, 'addAttachment'])->name('timesheet.addattachment');
        Route::post('timesheet/saveattachment/{tiesheet_id}', [TimesheetController::class, 'saveAttachment'])->name('timesheet.saveattachment');
        Route::get('previous_timesheet/{user}', [TimesheetController::class, 'previous_timesheets']);
        Route::post('timeline/create/print/{timesheet_id}', [TimesheetController::class, 'print'])->name('timeline.print');
        Route::get('timesheet/show/{timesheet}', [TimesheetNewController::class, 'newShow'])->name('timeline.show');
        Route::get('timesheet/print/{timesheet_id}', [TimesheetController::class, 'show'])->name('timesheet.print');
        Route::get('get-timesheet-dropdowns/{timesheet}/{billing_cycle}', [TimesheetNewController::class, 'getTimesheetDropdowns']);
        Route::resource('timesheet', TimesheetController::class);

        Route::get('cfts/tsweeks/{id}', [CustomFormattedTimesheetController::class, 'treesScapeWeeks'])->name('cfts.tsweeks');
        Route::get('cfts/blackboardmonthly/{id}', [CustomFormattedTimesheetController::class, 'blackboardMonthly'])->name('cfts.blackboardmonthly');
        Route::get('cfts/matrilogixmonthly/{id}', [CustomFormattedTimesheetController::class, 'matrilogixMonthly'])->name('cfts.matrilogixmonthly');
        Route::post('cfts/prepare', [CustomFormattedTimesheetController::class, 'prepare'])->name('cfts.prepare');
        Route::get('cfts/pdf/{id}', [CustomFormattedTimesheetController::class, 'pdf'])->name('cfts.pdf');
        Route::resource('cfts', CustomFormattedTimesheetController::class);

        Route::get('kanban/{project}', [KanbanBoardController::class, 'kanban'])->name('task.kanban');
        Route::get('getkanban/{project}', [KanbanBoardController::class, 'getKanban']);
        Route::get('kanban/project/{project}', [TaskController::class, 'getCustomer']);
        Route::post('kanban/task/timesheets', [TaskController::class, 'taskTimesheets']);
        Route::post('kanban/task/reassign', [TaskController::class, 'reassignTask']);
        Route::post('kanban/task/copyreassign', [TaskController::class, 'copyReassignTask']);
        Route::get('kanban/loadmore/{project_id}/{status}', [KanbanBoardController::class, 'loadmore']);
        Route::get('kanban/loadmoreuserstory/{project_id}/{user_story}/{status}', [KanbanBoardController::class, 'loadmoreUserStory']);
        Route::get('kanban/loadmoreconsultant/{project_id}/{employee_id}/{status}', [KanbanBoardController::class, 'loadmoreConsultant']);
        Route::get('kanban/loadmorepriority/{project_id}/{priority_id}/{status}', [KanbanBoardController::class, 'loadmorePriority']);
        Route::get('kanban/dropdowns/{project}', [KanbanBoardController::class, 'getDropDowns']);
        Route::get('kanban/getiftaskbillable/{project}', [TaskController::class, 'getIfTaskBillable']);
        Route::get('task/reports', [TaskController::class, 'reports'])->name('task.reports');
        Route::post('task/move/{task_id}', [TaskController::class, 'move'])->name('task.move');
        Route::post('task/updateuserstory/{task_id}', [TaskController::class, 'updateUserStory']);
        Route::post('task/updateconsultant/{task_id}', [TaskController::class, 'updateConsultant']);
        Route::post('task/updatepriority/{task_id}', [TaskController::class, 'updatePriority']);
        Route::post('task/storetask', [TaskController::class, 'storeTask'])->name('task.storetask');
        Route::get('task/view/{task}', [TaskController::class, 'view'])->name('task.view');
        Route::post('task/copy/{task_id}', [TaskController::class, 'copy'])->name('task.copy');
        Route::post('task/changestatus', [TaskController::class, 'changeStatus'])->name('task.changestatus');
        Route::post('task/changesprint', [TaskController::class, 'changeSprint'])->name('task.changesprint');
        Route::post('task/changepriority', [TaskController::class, 'changePriority'])->name('task.changepriority');
        Route::get('task/boards/{project_id}', [TaskController::class, 'boards'])->name('task.boards');
        Route::get('task/treeview/{project_id}', [TaskController::class, 'treeView'])->name('task.treeview');
        Route::get('task/treeviewnew/{project_id}', [TaskController::class, 'treeViewNew'])->name('task.treeviewnew');
        Route::get('task/sprint/{project_id}', [TaskController::class, 'sprint'])->name('task.sprint');
        Route::get('task/risk/{project_id}', [TaskController::class, 'risk'])->name('task.risk');
        Route::get('task/action/{project_id}', [TaskController::class, 'action'])->name('task.action');
        Route::get('task/gantt/{project_id}', [TaskController::class, 'gantt'])->name('task.gantt');
        Route::get('task/cost/{project_id}', [TaskController::class, 'cost'])->name('task.cost');
        Route::get('task/getboards', [TaskController::class, 'getBoards'])->name('task.getboards');
        Route::get('task/{project_id}', [TaskController::class, 'index'])->name('task.index');
        Route::resource('task', TaskController::class);
        Route::get('task/{project_id}/create', [TaskController::class, 'create'])->name('task.create');
        Route::get('task/{project_id}/show', [TaskController::class, 'show'])->name('task.show');
        Route::post('task/setemote/{task_id}', [TaskController::class, 'setEmote'])->name('task.setemote');
        Route::get('bug/{project}/create', [TaskController::class, 'create'])->name('bug.create');
        Route::get('bug/{task}/edit', [TaskController::class, 'edit'])->name('bug.edit');
        Route::post('task/copy/{task}', [TaskController::class, 'copy']);

        Route::get('template/gettemplate/{resource_id}', [TemplateController::class, 'getTemplate'])->name('template.gettemplate');
        Route::resource('template', TemplateController::class);
        Route::get('template/{template_id}/download', [TemplateController::class, 'download'])->name('template.download');

        Route::resource('master_template', TemplateTypeController::class);

        Route::resource('project_status', ProjectStatusController::class);

        Route::resource('project_terms', ProjectTermsController::class);

        Route::post('project.get terms', [ProjectController::class, 'getTerms'])->name('project.getterms');

        Route::get('reports', [ViewForReportController::class, 'index'])->name('reports.index');
        Route::get('reports/customer', [ViewForReportController::class, 'customer'])->name('reports.customer');
        Route::get('reports/manager', [ViewForReportController::class, 'manager'])->name('reports.manager');
        Route::get('reports/scouting', [ScoutingReportController::class, 'index'])->name('reports.scouting');
        Route::get('reports/recruitment', [ScoutingReportController::class, 'recruitment'])->name('reports.recruitment');
        Route::get('reports/scouting/analysis', [ScoutingReportController::class, 'analysis'])->name('reports.scoutinganalysis');
        Route::get('reports/scouting/job_spec', [ScoutingReportController::class, 'jobSpecSummary'])->name('reports.jobspecsammary');
        Route::get('reports/resource', [ResourceReportController::class, 'index'])->name('reports.resource');
        Route::get('reports/resource/analysis', [ResourceReportController::class, 'analysis'])->name('reports.resourceanalysis');
        Route::get('reports/consultant_expenses', [ViewForReportController::class, 'expenses'])->name('reports.consultant_expenses');
        Route::get('reports/consultant_assignment', [CustomerBillingReportController::class, 'index'])->name('reports.consultant_assignment');
        Route::get('reports/assignmentplanning', [AssignmentReportController::class, 'planning'])->name('reports.assignmentplanning');
        Route::get('reports/assignmentstatus', [AssignmentReportController::class, 'status'])->name('reports.assignmentstatus');
        Route::get('reports/projectbilling', [ProjectBillingReportController::class, 'index'])->name('reports.projectbilling');
        Route::get('reports/forecastanalysis', [ForecastReportController::class, 'index'])->name('reports.forecastanalysis');
        Route::get('reports/expenses_not_paid', [ExpensesNotPaid::class, 'index'])->name('reports.expenses_not_paid');
        Route::get('reports/planning', [PlanningController::class, 'index'])->name('reports.planning');
        Route::get('reports/consultant_monthly_report', [ConsultingMonthlyReport::class, 'index'])->name('reports.consultant_monthly_report');
        Route::get('reports/consultant_bonus', [ConsultantBonusController::class, 'index'])->name('reports.consultant_bonus');
        Route::get('reports/customer_summary_report', [CustomerSummaryReportController::class, 'index'])->name('reports.customer_summary_report');
        Route::get('reports/customer_project_summary', [CustomerProjectSummaryController::class, 'index'])->name('reports.customer_project_summary');
        Route::get('reports/customer_report', [CustomerReportController::class, 'index'])->name('reports.customer_report');
        Route::get('reports/customer_billing', [CustomerBillingController::class, 'index'])->name('reports.customer_billing');
        Route::get('reports/summary_report', [SummaryReportController::class, 'index'])->name('reports.summary_report');
        Route::get('reports/cashflow', [CashflowController::class, 'index'])->name('reports.cashflow');
        Route::get('reports/dashboard', [DashboardV2Controller::class, 'index'])->name('reports.dashboard');

        Route::post('template/{id}/getvariables', [TemplateController::class, 'getVariables']);
        Route::post('template/preview', [TemplateController::class, 'preview']);
        //Route::get('template/{id}/previewtemplate/{a_id}/{b_id}/{c_id}', [TemplateController::class, 'previewTemplate'])->name('template.previewtemplate');
        Route::post('/imageupload', [AssetController::class, 'imgUpload']);

        Route::resource('assessment_master', AssessmentMasterController::class);
        Route::get('assessment/{assessment}/print', [AssessmentHeaderController::class, 'print'])->name('assessment.print');
        Route::get('assessment/{assessment}/send', [AssessmentHeaderController::class, 'send'])->name('assessment.send');
        Route::resource('assessment', AssessmentHeaderController::class);
        Route::post('assessment/{id}', [AssessmentHeaderController::class, 'show'])->name('assessment.email');
        Route::post('prev_assessments', [AssessmentHeaderController::class, 'prev_assessment'])->name('assessment.prev');
        Route::post('add_activity_line', [AssessmentHeaderController::class, 'add_activity_line']);
        Route::post('add_assessment_plan', [AssessmentHeaderController::class, 'add_assessment_plan']);
        Route::post('add_assessment_concerns', [AssessmentHeaderController::class, 'add_assessment_concerns']);
        Route::post('add_assessment_note', [AssessmentHeaderController::class, 'add_assessment_note']);
        Route::post('assessment_measure', [AssessmentMeasureScoreController::class, 'create'])->name('assessment_measure');
        Route::get('assessment_customers/{customer_id}', [AssessmentHeaderController::class, 'get_customers'])->name('assessment_customers');
        Route::get('assessment_approver/{resource_id}', [AssessmentHeaderController::class, 'get_approver'])->name('assessment_approver');
        Route::get('assessment_activity/{assess_act_id}/edit', [AssessmentActivitiesController::class, 'edit'])->name('assessment_activity.edit');
        Route::patch('assessment_activity/{assess_act_id}', [AssessmentActivitiesController::class, 'update'])->name('assessment_activity.update');
        Route::delete('assessment_activity/{assess_act_id}', [AssessmentActivitiesController::class, 'destroy'])->name('assessment_activity.destroy');
        Route::get('assessment_planned/{assess_plan_id}/edit', [AssessmentPlannedNextController::class, 'edit'])->name('assessment_plan.edit');
        Route::patch('assessment_planned/{assess_act_id}', [AssessmentPlannedNextController::class, 'update'])->name('assessment_plan.update');
        Route::delete('assessment_planned/{assess_act_id}', [AssessmentPlannedNextController::class, 'destroy'])->name('assessment_plan.destroy');
        Route::get('assessment_concern/{assess_concern_id}/edit', [AssessmentConcernController::class, 'edit'])->name('assessment_concern.edit');
        Route::patch('assessment_concern/{assess_concern_id}', [AssessmentConcernController::class, 'update'])->name('assessment_concern.update');
        Route::delete('assessment_concern/{assess_concern_id}', [AssessmentConcernController::class, 'destroy'])->name('assessment_concern.destroy');
        Route::get('assessment_notes/{assess_concern_id}/edit', [AssessmentNotesController::class, 'edit'])->name('assessment_notes.edit');
        Route::patch('assessment_notes/{assess_concern_id}', [AssessmentNotesController::class, 'update'])->name('assessment_notes.update');
        Route::delete('assessment_notes/{assess_concern_id}', [AssessmentNotesController::class, 'destroy'])->name('assessment_notes.destroy');
        Route::get('assessment_copy/{assessment_id}', [AssessmentHeaderController::class, 'copy'])->name('assessment.copy');

        Route::get('customer_proforma_invoice', [CustomerProFormaController::class, 'create'])->name('customer.proforma');
        Route::post('customer_proforma_invoice/calculate', [CustomerProFormaController::class, 'calculate'])->name('customer.proformacalculate');
        Route::post('get_customer', [CustomerProFormaController::class, 'get_customer'])->name('customer.get_customer');
        Route::post('get_yearmonth', [CustomerProFormaController::class, 'get_yearmonth'])->name('customer.get_yearmonth');

        Route::get('customer_invoice', [CustomerInvoiceController::class, 'index'])->name('customer.invoice');
        Route::get('customer_invoice/create', [CustomerInvoiceController::class, 'create'])->name('customer.invoice_create');
        Route::post('customer_invoice/prepare', [CustomerInvoiceController::class, 'prepare'])->name('customer.invoice_prepare');
        Route::post('customer_invoice/generate', [CustomerInvoiceController::class, 'generate'])->name('customer.invoice_generate');
        Route::post('customer_invoice/save', [CustomerInvoiceController::class, 'generate'])->name('customer.invoice_save');
        Route::post('customer_invoice/send', [CustomerInvoiceController::class, 'generate'])->name('customer.invoice_send');
        Route::get('customer_invoice/{customer_invoice}', [CustomerInvoiceController::class, 'show'])->name('customer.invoice_show');
        Route::post('customer_invoice/{customer_invoice}', [CustomerInvoiceController::class, 'show'])->name('customer.invoice_show');
        Route::post('customer_invoice/unallocate/{invoice_id}', [CustomerInvoiceController::class, 'unallocate'])->name('customer.invoice_unallocate');
        Route::get('customer_invoice/payment/{invoice_id}', [CustomerInvoiceController::class, 'payment'])->name('customer.invoice_payment');
        Route::put('customer_invoice/process_payment', [CustomerInvoiceController::class, 'process_payment'])->name('customer.process_payment');
        Route::delete('customer_invoice/{invoice_id}', [CustomerInvoiceController::class, 'destroy'])->name('customer.invoice_delete');
        Route::get('customer_invoice/{invoice_id}/edit', [CustomerInvoiceController::class, 'edit'])->name('customer.invoice_edit');
        Route::patch('customer_invoice/{invoice_id}/update', [CustomerInvoiceController::class, 'update'])->name('customer.invoice_update');
        // Route::put('customerinvoice/{invoice_id}/update', [CustomerInvoiceController::class, 'update']);
        Route::post('select_timesheet/{id}', [CustomerInvoiceController::class, 'storeTimesheetId']);
        Route::get('nontimesheetinvoice/create', [CustomerInvoiceController::class, 'createNonTimesheetInvoice'])->name('nontimesheetinvoice.create');
        Route::get('nontimesheetinvoice/vendor_create', [VendorInvoiceController::class, 'createNonTimesheetInvoice'])->name('nontimesheetinvoice.vendor_create');
        Route::post('nontimesheetinvoice/store', [CustomerInvoiceController::class, 'storeNonTimesheetInvoice'])->name('nontimesheetinvoice.store');
        Route::post('nontimesheetinvoice/store_vendor', [VendorInvoiceController::class, 'storeNonTimesheetInvoice'])->name('nontimesheetinvoice.store_vendor');
        Route::get('nontimesheetinvoice/edit/{invoice_id}', [CustomerInvoiceController::class, 'editNonTimesheetInvoice'])->name('nontimesheetinvoice.edit');
        Route::patch('nontimesheetinvoice/update/{customerinvoice}', [CustomerInvoiceController::class, 'updateNonTimesheetInvoice'])->name('nontimesheetinvoice.update');
        Route::get('nontimesheetinvoice/edit_vendor/{invoice_id}', [VendorInvoiceController::class, 'editNonTimesheetInvoice'])->name('nontimesheetinvoice.edit_vendor');
        Route::get('nontimesheetinvoice/generate/{invoice_id}', [CustomerInvoiceController::class, 'generateNonTimesheetInvoice'])->name('nontimesheetinvoice.generate');
        Route::patch('nontimesheetinvoice/update/{invoice}/vendor', [VendorInvoiceController::class, 'updateNonTimesheetInvoice'])->name('nontimesheetinvoice.update_vendor');
        Route::get('nontimesheetinvoice/generate_vendor/{invoice}', [VendorInvoiceController::class, 'generateNonTimesheetInvoice'])->name('nontimesheetinvoice.generate_vendor');
        Route::post('nontimesheetinvoice/send/{customerInvoice}', [CustomerInvoiceController::class, 'sendNonTimesheetInvoice'])->name('nontimesheetinvoice.send');
        Route::post('nontimesheetinvoice/send_vendor/{vendorInvoice}', [VendorInvoiceController::class, 'sendNonTimesheetInvoice'])->name('nontimesheetinvoice.send_vendor');
        Route::get('nontimesheetinvoice/send_vendor/{vendorInvoice}', [VendorInvoiceController::class, 'sendNonTimesheetInvoice'])->name('nontimesheetinvoice.send_vendor');
        Route::post('nontimesheetinvoice/unallocate_vendor/{id}', [VendorInvoiceFunctionController::class, 'unallocateNonTimesheetInvoice'])->name('nontimesheetinvoice.unallocate_vendor');

        Route::post('get_user', [CvController::class, 'get_user'])->name('userdetails');

        Route::resource('advanced_templates', AdvancedTemplatesController::class);
        Route::get('advancedtemplate/index', [AdvancedTemplatesController::class, 'index'])->name('advancedtemplate.index');
        Route::post('advanced_templates/{id}/getvariables', [AdvancedTemplatesController::class, 'getVariables']);
        Route::post('advanced_templates/{tempid}/{id}/getvariables', [AdvancedTemplatesController::class, 'getVariables2']);
        Route::delete('advanced_templates/{template_id}', [AdvancedTemplatesController::class, 'destroy'])->name('advanced_templates.destroy');

        Route::resource('digisign', DigiSignController::class);
        Route::post('digisign/{document_id}/{resource_id}/sign', [DigiSignController::class, 'sign'])->name('digisign.sign');
        Route::post('digisign/{document_id}/{resource_id}/signtimesheet', [DigiSignController::class, 'signTimesheet'])->name('digisign.signtimesheet');
        Route::post('digisign/asset/{asset_id}/{digisign_user_id}', [DigiSignController::class, 'sendAssetDigiSign'])->name('digisign.asset');
        Route::post('digisign/supplierassignment/{assignment_id}/{digisign_user_id}', [DigiSignController::class, 'sendSupplierAssignmentDigiSign'])->name('digisign.supplierassignment');
        Route::post('digisign/resourceassignment/{assignment_id}/{digisign_user_id}', [DigiSignController::class, 'sendResourceAssignmentDigiSign'])->name('digisign.resourceassignment');
        Route::post('digisign/assessment/{id}', [DigiSignController::class, 'sendAssessmentDigisign'])->name('digisign.assessment');
        Route::post('digisign/signassessment/{assessment_id}/{digisign_user_id}', [DigiSignController::class, 'signAssessmentDigisign'])->name('digisign.signassessment');

        Route::resource('payment_terms', PaymentTermsController::class);

        Route::resource('assessment_master_details', AssessmentMasterDetailsController::class);

        Route::get('logins/fail', [LogsController::class, 'failed_logins'])->name('logins.failed');
        Route::get('logins/success', [LogsController::class, 'logins'])->name('logins.success');
        Route::resource('logins', LogsController::class);

        Route::resource('module', ModuleController::class);

        Route::post('custom_dashboard', [LandingPageDashboardController::class, 'storePreferences'])->name('preferences.dashboard');
        Route::get('reports/custom_dashboard', [CustomDashboardController::class, 'index'])->name('reports.custom_dashboard');
        Route::get('reports/custom_dashboard/{dashboard_name}/edit', [CustomDashboardController::class, 'edit'])->name('custom_dashboard.edit');
        Route::patch('custom_dashboard', [LandingPageDashboardController::class, 'updateLandingPage'])->name('update.lpdashboard');
        Route::delete('custom_dashboard_delete/{dashboard_name}', [LandingPageDashboardController::class, 'destroy'])->name('delete.dashboard');

        Route::resource('scouting', ScoutingController::class);
        Route::get('scouting/convert/{id}', [ScoutingController::class, 'convert'])->name('scouting.convert');
        Route::get('scoutingreport', [ScoutingReportController::class, 'export'])->name('scoutingreport');
        Route::get('scoutingpdf', [ScoutingReportController::class, 'pdf'])->name('scoutingpdf');
        Route::get('scouting/converttocv/{id}', [ScoutingController::class, 'convertToCV'])->name('scouting.converttocv');

        Route::get('management/projecthourssammary', [ProjectHoursSummaryController::class, 'index'])->name('projecthourssammary');
        Route::get('management/projectvaluesammary', [ProjectValueSummaryController::class, 'index'])->name('projectvaluesammary');
        Route::get('management/resourcedetails', [ResourceDetailsController::class, 'index'])->name('resourcedetails');
        Route::get('management/consultant360', [Consultant360Controller::class, 'index'])->name('consultant360');
        Route::get('management/consultant360/{res_id}', [Consultant360Controller::class, 'detailedView'])->name('consultant360details');
        Route::get('management/consultant360/{res_id}/payroll', [Consultant360Controller::class, 'payroll'])->name('consultant360payroll');

        Route::resource('assignment_standard_cost', AssignmentStandardCostController::class);

        Route::get('configs/default', [ConfigController::class, 'default_style'])->name('default.configs');

        Route::resource('favourites', FavouritesController::class);
        Route::resource('default_favs', DefaultsFavourites::class);
        Route::post('user_favourite', [DefaultsFavourites::class, 'user_store'])->name('user_store');

        Route::post('setup/startedsetup', [SetupController::class, 'startedSetup'])->name('setup.startedSetup');
        Route::post('setup/updateadmin', [SetupController::class, 'updateAdmin'])->name('setup.updateAdmin');
        Route::post('setup/updatecompany/{company_id}', [SetupController::class, 'updateCompany'])->name('setup.updateCompany');
        Route::post('setup/storeuser', [SetupController::class, 'storeUser'])->name('setup.storeuser');
        Route::post('setup/updateuser/{user_id}', [SetupController::class, 'updateUser'])->name('setup.updateuser');
        Route::get('setup/deleteuser/{user_id}', [SetupController::class, 'deleteUser'])->name('setup.deleteuser');
        Route::post('setup/storecustomer', [SetupController::class, 'storeCustomer'])->name('setup.storecustomer');
        Route::post('setup/updatecustomer/{customer_id}', [SetupController::class, 'updateCustomer'])->name('setup.updatecustomer');
        Route::get('setup/deletecustomer/{customer_id}', [SetupController::class, 'deleteCustomer'])->name('setup.deletecustomer');
        Route::post('setup/storevendor', [SetupController::class, 'storeVendor'])->name('setup.storevendor');
        Route::post('setup/updatevendor/{vendor_id}', [SetupController::class, 'updateVendor'])->name('setup.updatevendor');
        Route::get('setup/deletevendor/{vendor_id}', [SetupController::class, 'deleteVendor'])->name('setup.deletevendor');
        Route::post('setup/storecontact', [SetupController::class, 'storeContact'])->name('setup.storecontact');
        Route::post('setup/updatecontact/{contact_id}', [SetupController::class, 'updateContact'])->name('setup.updatecontact');
        Route::get('setup/deletecontact/{customer_id}', [SetupController::class, 'deleteContact'])->name('setup.deletecontact');
        Route::get('setup/configs', [SetupController::class, 'configs'])->name('setup.configs');
        Route::post('setup/configsupdate/{config_id}', [SetupController::class, 'configsUpdate'])->name('setup.configsupdate');
        Route::post('setup/finish', [SetupController::class, 'finishSetup'])->name('setup.finish');

        Route::resource('messages', MessageBoardController::class);

        Route::resource('cvwishlist', CVWishListController::class);
        Route::post('wishlist/add', [WishListController::class, 'add'])->name('wishlist.add');
        Route::resource('wishlist', WishListController::class);

        Route::resource('process_interview', ProcessInterviewController::class);

        Route::resource('process_status', ProcessStatusController::class);

        Route::resource('profession', ProfessionController::class);
        Route::resource('speciality', SpecialityController::class);

        Route::resource('cvquality', CVQualityController::class);

        Route::put('jobspec/{job_spec_id}/updatewishlist', [JobSpecController::class, 'updateWishList'])->name('jobspec.updatewishlist');
        Route::resource('jobspec', JobSpecController::class);

        //All dynamic forms will be called using this route
        Route::resource('scoutingrole', ScoutingRoleController::class);

        Route::resource('maillist', EmailListController::class);

        Route::resource('master/customise_dashboard', DynamicDashboardController::class);
        Route::post('assignment/getuserdetails', [AssignmentController::class, 'getUserDetails'])->name('assignment.getuserdetails');
        Route::get('cfts/weeklydetail/{id}', [CustomFormattedTimesheetController::class, 'weeklyDetail'])->name('cfts.weeklydetail');
        Route::get('jobspecactivitystatus/{jobSpecActivityLog}/{status_id}', [CvSubmitController::class, 'jobSpecActivityLogStatus'])->name('jobspecactivitystatus.status');

        Route::get('reports/task_reports', [TaskReportController::class, 'index'])->name('reports.task_reports');
        Route::get('reports/detail', [TaskReportController::class, 'detail'])->name('reports.task_detail');
        Route::get('reports/task_finance', [TaskReportController::class, 'finance'])->name('reports.task_finance');
        Route::get('reports/task_list', [TaskReportController::class, 'list'])->name('reports.task_list');
        Route::get('reports/task_list_description', [TaskReportController::class, 'listDescription'])->name('reports.task_list_description');
        Route::get('reports/task_utilisation', [TaskReportController::class, 'utilisation'])->name('reports.task_utilisation');
        Route::get('reports/task_summary_description', [TaskReportController::class, 'taskSummaryDescription'])->name('reports.task_summary_description');
        Route::get('reports/financial', [ViewForReportController::class, 'financial'])->name('reports.financial');


        Route::resource('customerstatements', StatementController::class);
        Route::post('customerstatementssend/{id}', [StatementController::class, 'show'])->name('statement.send');
        Route::resource('vendorstatements', VendorStatementController::class);
        Route::post('vendorstatementssend/{id}', [VendorStatementController::class, 'show'])->name('vendorstatement.send');

        Route::post('distributionlist/getjobspec', [DistributionListController::class, 'getJobSpec'])->name('distributionlist.getjobspec');
        Route::post('distributionlist/getcanditates', [DistributionListController::class, 'getCanditates'])->name('distributionlist.getcanditates');
        Route::post('distributionlist/getcustomerpreference', [DistributionListController::class, 'getCustomerPreference'])->name('distributionlist.getcustomerpreference');
        Route::resource('distributionlist', DistributionListController::class);
        Route::get('distributionlist/getemailtemplate/{config_id}', [DistributionListController::class, 'getEmailTemplate'])->name('timesheet.getemailtemplate');
        Route::post('distributionlist/dropzoneupload/{id}', [DistributionListController::class, 'dropzoneUpload'])->name('distributionlist.dropzoneupload');
        Route::get('assignment/sendemployeedigisign/{assignment_id}', [AssignmentController::class, 'sendEmployeeAssignmentDigisign'])->name('assignment.sendemployeedigisign');

        Route::post('action/accept/{id}', [ActionController::class, 'accept'])->name('action.accept');
        Route::post('action/reject/{id}', [ActionController::class, 'reject'])->name('action.reject');
        Route::post('action/complete/{id}', [ActionController::class, 'complete'])->name('action.complete');
        Route::get('action/kanban', [ActionController::class, 'kanban'])->name('action.kanban');
        Route::get('action/{project}/tasks', [ActionController::class, 'getRelatedTasks']);
        Route::post('action/move/{id}', [ActionController::class, 'move'])->name('action.move');
        Route::post('action/emotefrown/{task_id}', [ActionController::class, 'emoteFrown'])->name('action.emotefrown');
        Route::post('action/emoteexclamation/{task_id}', [ActionController::class, 'emoteExclamation'])->name('action.emoteexclamation');
        Route::post('action/emotesmile/{task_id}', [ActionController::class, 'emoteSmile'])->name('action.emotesmile');
        Route::post('action/emotenone/{task_id}', [ActionController::class, 'emoteNone'])->name('action.emotenone');
        Route::post('action/uploaddocument/{action_id}', [ActionController::class, 'uploadDocument']);
        Route::get('action/getdocuments/{action_id}', [ActionController::class, 'getDocuments']);
        Route::post('action/deletedocument/{document_id}', [ActionController::class, 'deleteDocument']);
        Route::post('action/downloaddocument/{document_id}', [ActionController::class, 'downloadDocument'])->name('action.downloadDocument');
        Route::post('action/setemote/{task_id}', [ActionController::class, 'setEmote'])->name('action.setemote');
        Route::resource('action', ActionController::class);

        Route::get('action/project/{project}', function (App\Models\Project $project) {
            return response()->json(['project' => $project]);
        });

        Route::post('action/store/task', [ActionController::class, 'storeActionTask']);

        Route::get('uploaddocument', [CvController::class, 'uploadDocument'])->name('uploaddocument');
        Route::post('storedocument', [CvController::class, 'storeDocument'])->name('storedocument');
        Route::delete('deletedocument/{document_id}', [CvController::class, 'deleteDocument'])->name('deletedocument');
        Route::get('editdocument/{document_id}', [CvController::class, 'editDocument'])->name('editdocument');
        Route::put('updatedocument/{document_id}', [CvController::class, 'updateDocument'])->name('updatedocument');

        Route::get('payrollsummary', [PayrollSummaryController::class, 'index'])->name('payroll.summary');
        Route::get('payrollsummary/approve', [PayrollSummaryController::class, 'approveExpenses'])->name('payrollsummary.approve');
        Route::get('payrollsummary/pay', [PayrollSummaryController::class, 'payExpenses'])->name('payrollsummary.pay');

        Route::get('unsubscribe', [UnsubscribeController::class, 'unsubscribe'])->name('unsubscribe');

        Route::resource('warning_maintenance', WarningMaintenanceController::class);

        Route::post('epic/getdropdowns', [EpicController::class, 'getDropdowns']);
        Route::post('epic/saveepic', [EpicController::class, 'saveEpic']);
        Route::post('epic/updateepic', [EpicController::class, 'updateEpic']);
        Route::post('epic/deleteepic', [EpicController::class, 'deleteEpic']);
        Route::resource('epic', EpicController::class);
        Route::post('/userstory/getdropdowns', [UserStoryController::class, 'getDropdowns']);
        Route::post('userstory/saveuserstory', [UserStoryController::class, 'saveUserStory']);
        Route::post('userstory/updateuserstory', [UserStoryController::class, 'updateUserStory']);
        Route::post('userstory/deleteuserstory', [UserStoryController::class, 'deleteUserStory']);
        Route::resource('userstory', UserStoryController::class);
        Route::post('userstory/{user_story_id}/discussion', [UserStoryController::class, 'userStoryDiscuss'])->name('userstory.discussion');
        Route::get('userstorydiscussion/{user_story_id}', [UserStoryController::class, 'getDiscussion'])->name('get.discussion');
        Route::post('feature/getdropdowns', [FeatureController::class, 'getDropdowns']);
        Route::post('feature/savefeature', [FeatureController::class, 'saveFeature']);
        Route::post('feature/updatefeature', [FeatureController::class, 'updateFeature']);
        Route::post('feature/deletefeature', [FeatureController::class, 'deleteFeature']);
        Route::resource('feature', FeatureController::class);
        Route::resource('sprint', SprintController::class);
        Route::resource('board', BoardController::class);
        Route::get('risk/getriskitem/{risk_area_id}', [RiskController::class, 'getRiskItem'])->name('risk.getriskitem');
        Route::resource('risk', RiskController::class);
        Route::post('risk/feedback/{risk}', [RiskFeedbackController::class, 'store']);
        Route::get('risk/feedback/avatar/{user}', [RiskFeedbackController::class, 'feedbackUserAvatar'])->name('risk.feedback.avatar');

        Route::resource('master/task/deliverytype', DeliveryTypeController::class, ['names' => 'deliverytype']);

        Route::get('report/timesheet', [TimesheetReportController::class, 'timesheet'])->name('report.timesheet');
        Route::get('report/timesummary', [TimesheetReportController::class, 'summary'])->name('report.timesummary');
        Route::get('report/timesummary/export', [TimesheetReportController::class, 'exportxls'])->name('report.timesummary.export');
        Route::get('report/projectprogress', [ProjectProgressController::class, 'projectProgress'])->name('report.projectprogress');
        Route::get('report/projectprogress3', [ProjectProgressController::class, 'projectProgressLevelThree'])->name('report.projectprogress3');
        Route::get('report/projectprogress2', [ProjectProgressController::class, 'projectProgressLevelTwo'])->name('report.projectprogress2');
        Route::get('report/projectdelivery', [ProjectProgressController::class, 'projectDelivery'])->name('report.projectdelivery');

        Route::resource('viewpermission', ViewPermissionController::class);

        Route::get('setdefaultprojectroles', [ProjectController::class, 'setDefaultProjectRoles'])->name('setdefaultprojectroles');

        Route::resource('billing', BillingCycleController::class);
        Route::post('billipng/duplicate/{billing}', [BillingCycleController::class, 'duplicate'])->name('billing.duplicate');

        Route::resource('period', BillingPeriodController::class);
        Route::get('printbillingcycle/{billingperiod}/{template_id}', [TimesheetController::class, 'printBillingCycle']);
        Route::get('billing_timesheets', [TimesheetController::class, 'billingTimesheets']);
        Route::get('billingcycle-id-invoice/{project}', [CustomerInvoiceController::class, 'billingCycle']);
        Route::get('billing/timeline/{billingCycle}', [TimesheetController::class, 'getBillinPeriods']);

        Route::get('templatestyles/customstyles', [BillingPeriodStyleController::class, 'getCustomTemplate']);
        Route::resource('templatestyles', BillingPeriodStyleController::class);

        Route::resource('approvalroles', ApprovalRulesController::class);

        Route::resource('contact', ContactController::class);

        Route::resource('expense_type', ExpenseTypeController::class);

        Route::resource('usertype', UserTypeController::class);

        Route::resource('title', TitleController::class);

        Route::resource('useronboarding', UserOnboardingController::class);

        Route::post('useronboarding/{useronboarding}/commit', [UserOnboardingController::class, 'commit']);

        Route::get('useronboarding/{useronboarding}/appontmentmanager', [UserOnboardingAppointingManagerController::class, 'create'])->name('useronboarding.appontmentmanager.create');

        Route::post('useronboarding/{useronboarding}/appontmentmanager', [UserOnboardingAppointingManagerController::class, 'store'])->name('useronboarding.appontmentmanager.store');
        Route::post('useronboarding/{useronboarding}/confirmappointment', [UserOnboardingAppointingManagerController::class, 'confirmAppointment'])->name('useronboarding.appontment.confirm');

        Route::get('appontmentmanager/{appontmentmanager}/edit', [UserOnboardingAppointingManagerController::class, 'edit'])->name('appontmentmanager.edit');

        Route::patch('appontmentmanager/{appontmentmanager}', [UserOnboardingAppointingManagerController::class, 'update'])->name('appontmentmanager.update');
        Route::get('appontmentmanager/{user}/print-permanent', [UserOnboardingAppointingManagerController::class, 'printPermanent'])->name('appontmentmanager.print.permanent');
        Route::get('appontmentmanager/{user}/print-fixed', [UserOnboardingAppointingManagerController::class, 'printFixed'])->name('appontmentmanager.print.fixed');
        Route::get('appontmentmanager/{user}/send', [UserOnboardingAppointingManagerController::class, 'send'])->name('appontmentmanager.send');

        Route::resource('contracttype', ContractTypeController::class);

        Route::resource('myaccount', MyAccountController::class);
        Route::post('myaccount/upgrade-to-premium',[MyAccountController::class, 'upgradeToPremium']);

        Route::resource('paymentbase', PaymentBaseController::class);

        Route::resource('onboardingstatus', OnboardingStatusController::class);

        Route::resource('tasktags', TaskTagsController::class);
        Route::get('task_tags', [TaskTagsController::class, 'getTaskTags']);

        Route::resource('paymenttype', PaymentTypeController::class);

        Route::post('useronboarding/{useronboarding}/issued', [UserOnboardingController::class, 'issueContract'])->name('contract.issued');

        Route::post('useronboarding/{useronboarding}/approve', [UserOnboardingController::class, 'approve'])->name('contract.approve');

        Route::post('useronboarding/{useronboarding}/received', [UserOnboardingController::class, 'contractReceived'])->name('contract.received');

        Route::post('useronboarding/{useronboarding}/signed', [UserOnboardingController::class, 'contractSigned'])->name('contract.signed');

        Route::post('useronboarding/{useronboarding}/filed', [UserOnboardingController::class, 'contractFiled'])->name('contract.filed');

        Route::post('useronboarding/{useronboarding}/enable', [UserOnboardingController::class, 'enable'])->name('useronboarding.enable');

        Route::get('filters', [FiltersController::class, 'filters']);

        Route::get('my-work-week/getfilters', [MyWorkWeekController::class, 'getFilterDropDowns']);
        Route::post('my-work-week/searchfilters', [MyWorkWeekController::class, 'getFilterDropDowns']);
        Route::get('my-work-week/get-task-overview', [MyWorkWeekController::class, 'getTaskOverview']);
        Route::get('my-work-week/get-assignment-overview', [MyWorkWeekController::class, 'getAssignmentOverview']);
        Route::get('my-work-week/get-anniversaries-overview', [MyWorkWeekController::class, 'getAnniversariesOverview']);
        Route::get('my-work-week/get-utilization-overview', [MyWorkWeekController::class, 'getUtilizationOverview']);
        Route::get('my-work-week/get-expenses-overview', [MyWorkWeekController::class, 'getExpensesOverview']);
        Route::get('my-work-week/get-missing-timesheets-overview', [MyWorkWeekController::class, 'getMissingTimesheetsOverview']);
        Route::get('my-work-week/get-assessment-overview', [MyWorkWeekController::class, 'getAssessmentOverview']);
        Route::get('my-work-week/get-leave-overview', [MyWorkWeekController::class, 'getLeaveOverview']);
        Route::get('my-work-week/get-records-overview', [MyWorkWeekController::class, 'getRecordsUpToDate']);


        Route::get('my-work-week/get-actual-vs-planned', [MyWorkWeekController::class, 'getActualVsPlanned']);
        Route::get('my-work-week/get-timesheets', [MyWorkWeekController::class, 'getTimesheets']);
        Route::get('my-work-week/get-tasks', [MyWorkWeekController::class, 'getTasks']);
        Route::get('my-work-week/get-assignment', [MyWorkWeekController::class, 'getAssignment']);
        Route::resource('my-work-week', MyWorkWeekController::class);
        Route::get('my-work-week', [MyWorkWeekController::class, 'index'])->name('my-work-week.index');
        Route::resource('prospectstatus', ProspectStatusController::class);
        Route::resource('sprintstatus', SprintStatusController::class);
        
        //Route::resource('api', ApiController::class);

        Route::post('favorites/toggle', [FavoriteController::class ,'toggleFavorite'])->name('favorites.toggle');
        Route::get('favorite-status/{id}', [FavoriteController::class, 'checkFavoriteStatus'])->name('favorite.status');
        Route::get('/favoritable-pages', [PageController::class, 'getFavoritablePages']);
        Route::get('/favorited-pages', [FavoriteController::class, 'getFavoritedPages']);
        Route::post('/favorite-pages/add', [PageController::class, 'store'])->name('favorite.store');

        Route::resource('variable', TemplateVariableController::class);
    });
});

Route::middleware([
    'api',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->prefix('api')->group(function () {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/getnotifications', [NotificationController::class, 'notificationsCount']);
    Route::post('/markallnotifications', [NotificationController::class, 'markAllNotifications']);
    Route::post('/readnotifications', [NotificationController::class, 'readNotifications']);

    Route::post('/boards/{id}/cards', [BoardCardController::class, 'create']);
    Route::post('/boards', [BoardController::class, 'create']);
    Route::put('/boards', [BoardController::class, 'update']);
    Route::get('/boards', [BoardController::class, 'index']);
    Route::get('/boards/{project_id}', [BoardController::class, 'index']);
    Route::get('/invoice/items/{invoice_id}/{invoice_type_id}', [InvoiceItemsController::class, 'getInvoiceItems']);
    Route::post('/invoice/item/create', [InvoiceItemsController::class, 'addInvoiceItem']);
    Route::post('/invoice/item/update/{id}', [InvoiceItemsController::class, 'update']);
    Route::post('/invoice/item/remove/{id}', [InvoiceItemsController::class, 'destroy']);
    Route::get('/invoice/resource', [InvoiceController::class, 'getInvoiceResources']);
    Route::get('/invoice/parameters', [InvoiceController::class, 'getInvoiceParameters']);
    Route::get('/getparent/{parent_name}/{parent_id}', [SprintController::class, 'getParent']);
    Route::post('/sprint/complete/{sprint}', \App\Http\Controllers\API\SprintController::class);
    Route::post('/task/getfilters', [TaskController::class, 'getTaskFilters']);
    Route::post('/task/deletetask/{task_id}', [TaskController::class, 'deleteTask']);
    Route::post('/task/gettasks/{project_id}', [TaskController::class, 'getProjectTasks']);
    Route::post('/task/gettreeview/{project_id}', [TaskController::class, 'getProjectTreeview']);
    Route::get('/task/{task_id}', [TaskController::class, 'getTask']);
    Route::post('/task/savetask', [TaskController::class, 'saveTask']);
    Route::post('/task/updatetask/{task}', [TaskController::class, 'updateTask']);
    Route::post('/task/message/{task_id}', [TaskController::class, 'addMessage']);
    Route::get('/task/getmessages/{task_id}', [TaskController::class, 'getMessages']);
    Route::post('/task/uploadfile/{task_id}', [TaskController::class, 'uploadFile']);
    Route::get('/task/getdocuments/{task_id}', [TaskController::class, 'getDocuments']);
    Route::post('/task/deletedocument/{task_id}', [TaskController::class, 'deleteDocument']);
    Route::post('/task/deletetimeline/{task_id}', [TaskController::class, 'deleteTimeline']);
    Route::post('/task/addtimesheettimeline', [TaskController::class, 'addTimesheetTimeline']);
    Route::post('/task/updatetimesheettimeline', [TaskController::class, 'updateTimesheetTimeline']);
    Route::get('/task/gettimesheettimelines/{task_id}', [TaskController::class, 'getTimelines']);
    Route::get('/task/gettimesheettimelinedays/{timesheet_id}', [TaskController::class, 'getTimelineDays']);
    Route::post('/taskcards', [TaskController::class, 'getCards']);
    Route::post('/reassign/task/{task}', [KanbanBoardController::class, 'reassignTask']);
    Route::get('/action/{action_id}', [ActionController::class, 'getAction']);
    Route::post('/action/updateaction/{action_id}', [ActionController::class, 'updateAction']);
    Route::post('/action/message/{action_id}', [ActionController::class, 'addMessage']);
    Route::get('/action/getmessages/{action_id}', [ActionController::class, 'getMessages']);
    Route::get('/cards', [ActionController::class, 'getCards']);
    Route::post('/getprojectresources', [TaskController::class, 'getProjectResources']);
    Route::post('/userstory/{userstory}/update', [UserStoryController::class, 'APIupdateUserStory']);
    Route::post('/userstory/uploadfile/{userstory_id}', [UserStoryController::class, 'uploadFile']);
    Route::get('/userstory/getdocuments/{userstory_id}', [UserStoryController::class, 'getDocuments']);
    Route::post('/userstory/deletedocument/{userstory_id}', [UserStoryController::class, 'deleteDocument']);
    //Route::get('/users', UsersController::class);
    Route::get('/usersdropdown', [\App\Http\Controllers\API\APIUserController::class, 'getUsersDropdown']);
    Route::controller(AssetHistoryController::class)->group(function (){
        Route::get('/issue/{asset}', 'index');
        Route::post('/issue', 'store');
        Route::patch('/issue/{issue}', 'update');
        Route::delete('/issue/{issue}', 'destroy');
    });
    Route::controller(AssetAdditionalCostController::class)->group(function(){
        Route::get('/additional-costs/{equipment}', 'index');
        Route::post('/additional-cost/{equipment}', 'store');
        Route::patch('/additional-cost/{equipment}', 'update');
        Route::delete('/additional-cost/{cost}', 'destroy');
    });
    Route::get('/est-life/{class}', AssetClassController::class);
    Route::get('/asset-retire-date', fn (Request $request) => response()->json([
        'retire_date' => \Carbon\Carbon::parse($request->aquired_date)->addMonths($request->est_life)->toDateString()
    ]));
    Route::post('/clockify', [App\Http\Controllers\API\ClockifyController::class, 'handleEntry']);
    Route::post('/clockify/updatetime', [App\Http\Controllers\API\ClockifyController::class, 'update']);
    Route::get('/goal/{sprint_id}', [GoalController::class, 'index']);
    Route::post('/goal/{sprint_id}', [GoalController::class, 'store']);
    Route::post('/goal/{goal}/update', [GoalController::class, 'update']);
    Route::get('/goal/{goal}/delete', [GoalController::class, 'destroy']);
    Route::get('/sprintreview/{sprint_id}', [SprintReviewController::class, 'index']);
    Route::post('/sprintreview/{sprint_id}', [SprintReviewController::class, 'store']);
    Route::post('/sprintreview/{sprintreview}/update', [SprintReviewController::class, 'update']);
    Route::get('/sprintreview/{sprintreview}/delete', [SprintReviewController::class, 'destroy']);
    Route::get('/sprintresource/{sprint_id}', [SprintResourceController::class, 'index']);
    Route::post('/sprintresource/{sprintresource}/update', [SprintResourceController::class, 'update']);
    Route::post('/prospect/{prospect}/discussion', [CommentController::class, 'store']);
    Route::get('/prospect/{prospect}/discussion', [CommentController::class, 'index']);
    Route::get('/tasksdropdown/{project}', [TaskController::class, 'dropdown']);
    Route::get('/plan_exp/account_element/{accountid}', [App\Http\Controllers\API\PlannedExpenseController::class, 'getAccountElement']);
    Route::post('/prospects', [App\Http\Controllers\API\ProspectController::class, 'getProspects']);
    Route::post('/prospects/move/{id}', [App\Http\Controllers\API\ProspectController::class, 'move']);
    Route::post('/prospects/add-contact', [App\Http\Controllers\API\ProspectController::class, 'addContact']);
    Route::post('/prospects/add-solution', [App\Http\Controllers\API\ProspectController::class, 'addSolution']);
    Route::post('/prospects/message/{id}', [App\Http\Controllers\API\ProspectController::class, 'addMessage']);
    Route::get('/prospects/getmessages/{id}', [App\Http\Controllers\API\ProspectController::class, 'getMessages']);
    Route::post('/prospects/uploadfile/{id}', [App\Http\Controllers\API\ProspectController::class, 'uploadFile']);
    Route::get('/prospects/getdocuments/{id}', [App\Http\Controllers\API\ProspectController::class, 'getDocuments']);
    Route::get('/prospects/deletedocument/{id}', [App\Http\Controllers\API\ProspectController::class, 'deleteDocument']);
    Route::get('/billing/check_usage/{billing_cycle_id}', App\Http\Controllers\API\BillingCycleUsageController::class);
    Route::get('trial-data', TrialDataContoller::class);

    Route::get('/batch/{batchId}', function (string $batchId) : \Illuminate\Http\JsonResponse
    {
        $batch = Bus::findBatch($batchId);

        return response()->json(['jobs_status' => $batch->finished()]);
    });

    Route::get('subscription/cancel', [SubscriptionManagementController::class, 'cancel']);
    Route::post('subscription/cancelemail', [SubscriptionManagementController::class, 'cancelemail']);
    Route::post('subscription/update', [SubscriptionManagementController::class, 'update']);

    Route::get('/frequency', FrequencyController::class);
    Route::get('/emails/{customer}', InvoiceDefaultEmailsController::class);
    Route::get('/customer-projects/{customer}', CustomerProjectController::class);

    Route::post('/token', [ApiController::class, 'handle'])->middleware('api.token');
    // Route::get('/report/timesheet', [TimesheetReportController::class, 'timesheetapi'])->middleware('api.token');
    Route::get('timesheet_report_api', [ApiController::class, 'timesheetApi'])->name('api.timesheetApi');
    Route::get('/requests', [ApiController::class, 'requests'])->name('api.requests');
    Route::get('/api_index', [ApiController::class, 'index'])->name('api.index');

    Route::get('/vatrate', VatRateInvokeController::class);

    Route::controller(VendorProFormaInvoiceController::class)->group(function (){
        Route::get('/proformaprojects/{resource}', 'project');
        Route::get('/proformaperiods', 'periods');
        Route::get('/proformavat/{user}', 'vat');
    });

    Route::get('/customervat', CustomerProFormaInvoiceController::class);

    Route::get('/site_report/getcustomerprojects/{customer_id}', [SiteReportController::class,'getCustomerProjects']);
    Route::get('/site_report/getprojectconsultants/{project_id}', [SiteReportController::class,'getProjectConsultants']);
    Route::get('/site_report/getSiteRisks/{project_id}', [SiteReportController::class,'getSiteRisks']);
    Route::post('/site_report/getTempHeader', [SiteReportController::class,'getTempHeader']);
    Route::post('/site_report/generateSiteReport', [SiteReportController::class,'generateSiteReport']);
    Route::post('/site_report/getActivities', [SiteReportController::class,'getActivities']);
    Route::post('/site_report/getPlannedActivities', [SiteReportController::class,'getPlannedActivities']);
    Route::post('/site_report/getHoursSummary', [SiteReportController::class,'getHoursSummary']);
    Route::post('/site_report/sendSiteReport', [SiteReportController::class,'sendSiteReport']);

    Route::patch('/move-task/{id}', [TaskController::class, 'moveTask'])->name('tasks.moveTask');
    Route::patch('/move-user-story/{id}', [UserStoryController::class, 'moveStory'])->name('user-stories.moveStory');
    Route::patch('/move-feature/{id}', [FeatureController::class, 'moveFeature'])->name('features.moveFeature');
    Route::patch('/move-epic/{id}', [EpicController::class, 'moveEpic'])->name('epics.moveEpic');

    Route::get('/templatetypes', APITemplateTypeController::class);

    Route::get('/variables/{templatetype}', APITemplateVariableController::class);

    Route::post('/advanced_templates/preview', [APITemplateController::class, 'previewTemplate']);
    Route::post('/advanced_templates', [APITemplateController::class, 'store']);
    Route::post('/advanced_templates/{template}', [APITemplateController::class, 'update']);
    Route::webhooks('/premimum-invoice', 'premium_invoice');

});

Route::group([
    'middleware' => [
        'custom_api',
        \Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains::class,
    ],
], function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/report/timesheet_api', [TimesheetReportController::class, 'timesheetapi']);
    });
});


