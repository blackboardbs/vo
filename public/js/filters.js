(function ($){
    $.fn.dropdownSearch = function(options) {
        let settings = $.extend({
            name: "",
            dropdown: []
        }, options );

        const url = new URL(window.location.toLocaleString())

        addValues($(this))

        $(document).on('click', e => {
            let dataList = $(this).siblings()[0];
            if (e.target.id !== dataList.id && dataList.style.display === 'block'){
                dataList.style.display = 'none';
            }
        })

        $(this).on('keyup', function (){
            if ($(this).val().length >= 3){
                const input = $(this);
                if (settings.dropdown.length){

                    let filtered = settings.dropdown.filter(item => item.name.toLowerCase().includes(input.val()))
                    if (filtered.length){
                        constructDropdowns(filtered, input)
                    }else {
                        dbCall(input)
                    }
                }else{
                    dbCall(input)
                }
            }
        })

        function dbCall(element){
            const params = url.search.replace('?', '&')
            axios.get(`/filters?${settings.name}=${element.val().replaceAll(' ', '+')+params}`)
                .then(res => {
                    const response = res.data;
                    if(response.length){
                        constructDropdowns(response, element)
                    }else{
                        $(element.siblings()[0]).html(`<span class="px-3 py-2 border-bottom d-block data-list disabled">Sorry Nothing Was Found</span>`).show(100);
                    }
                })
        }

        function constructDropdowns(data_source, element){
            let links = "";

            if (url.searchParams.get(settings.name+'_id')){
                url.searchParams.delete(settings.name+'_id')
            }

            const href = url.searchParams.size
                ?`${url.href}&`
                :`${url.href}?`;

            data_source.forEach(value => links += `<a href="${href+settings.name}_id=${value.id}" class="px-3 py-2 border-bottom d-block data-list font-weight-normal">${value.name}</a>`)
            $(element.siblings()[0]).html(links).show(100);
        }

        function addValues(element) {

            url.searchParams.forEach((value, key) =>{
                if (settings.dropdown.length && key.toLowerCase().includes(settings.name.toLowerCase())){
                    settings.dropdown.forEach((v, k) =>{
                        if (value == v.id){
                            element.val(v.name)
                        }
                    })
                }
            })
        }
    }
}(jQuery))