$(function () {
    $('#activity-table tbody').on('click', '#save-activity', function (e) {
        e.preventDefault();

        let inputs = [$('#activity-table tbody tr td textarea[name="assessment_task_description"]'), $('#activity-table tbody tr td select[name="competency_level"]'), $('#activity-table tbody tr td select[name="comfort_level"]'), $('#activity-table tbody tr td textarea[name="notes"]')];

        for (i = 0; i < inputs.length; i++){
            //console.log();
            if (inputs[i].val() == '' && inputs[i].val() == 0){
                inputs[i].addClass( "is-invalid" );
                return false;
            }else {
                inputs[i].removeClass("is-invalid")
            }
        }

        axios.post('/add_activity_line', {
            task_description: $('#activity-table tbody tr td textarea[name="assessment_task_description"]').val(),
            competency_level: $('#activity-table tbody tr td select[name="competency_level"]').val(),
            comfort_level: $('#activity-table tbody tr td select[name="comfort_level"]').val(),
            notes: $('#activity-table tbody tr td textarea[name="notes"]').val(),
            assessment_id: $('input[name="assessment_id"]').val(),
        })
            .then(function (response) {
                $('#activity-addition').hide();
                //for (i = 0; i < response.data.length; i++) {
                let url = '/assessment_activity/'+ response.data.id+'/edit';
                $('#activity-table tbody').append("<tr>" +
                    "<td>" + response.data.id + "</td>" +
                    "<td>" + response.data.assessment_task_description + "</td>" +
                    "<td>" + response.data.competency_level + "</td>" +
                    "<td>" + response.data.comfort_level + "</td>" +
                    "<td>" + response.data.notes + "</td>" +
                    "<td>" +
                    "<a href='"+url+"' class='btn btn-sm btn-success'><i class='fas fa-pencil-alt'></i></a> </td>" +
                    "</tr>")
                //}
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    let option = '';

    for (let i = 1; i < 11; i++){
        option += "<option value='"+i+"'>"+i+"</option>";
    }

    $("#add-line").on('click', function () {
        $('#activity-addition').remove();
        $('#activity-table tbody').append("<tr id='activity-addition'>" +
            "<td>&nbsp;</td>" +
            "<td><textarea name='assessment_task_description' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Task Description'></textarea></td>" +
            "<td><select name='competency_level' class='form-control form-control-sm'><option value='0'>Select Level</option>" + option +  "</select></td>" +
            "<td><select name='comfort_level' class='form-control form-control-sm'><option value='0'>Select Level</option>" + option +  "</select></td>" +
            "<td><textarea name='notes' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Notes'></textarea></td>" +
            "<td><a href=\"#\" class=\"btn btn-primary\" id=\"save-activity\"><i class='fas fa-save'></i></a></td>" +
            "</tr>")
    })

    $('#plan-table tbody').on('click', '#save-plan', function (e) {
        e.preventDefault();

        let inputs = [$('#plan-table tbody tr td textarea[name="assessment_task_description"]'),  $('#plan-table tbody tr td textarea[name="notes"]')];

        for (i = 0; i < inputs.length; i++){
            //console.log();
            if (inputs[i].val() == '' && inputs[i].val() == 0){
                inputs[i].addClass( "is-invalid" );
                return false;
            }else {
                inputs[i].removeClass("is-invalid")
            }
        }

        axios.post('/add_assessment_plan', {
            task_description: $('#plan-table tbody tr td textarea[name="assessment_task_description"]').val(),
            notes: $('#plan-table tbody tr td textarea[name="notes"]').val(),
            assessment_id: $('input[name="assessment_id"]').val(),
        })
            .then(function (response) {
                $('#plan-addition').hide();
                //for (i = 0; i < response.data.length; i++) {
                let url = '/assessment_planned/'+ response.data.id+'/edit';
                $('#plan-table tbody').append("<tr>" +
                    "<td>" + response.data.id + "</td>" +
                    "<td>" + response.data.assessment_task_description + "</td>" +
                    "<td>" + response.data.notes + "</td>" +
                    "<td><a href='"+url+"' class='btn btn-sm btn-success'><i class='fas fa-pencil-alt'></i></a> </td>" +
                    "</tr>")
                //}
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $("#add-plan").on('click', function () {
        $('#plan-addition').remove();
        $('#plan-table tbody').append("<tr id='plan-addition'>" +
            "<td>&nbsp;</td>" +
            "<td><textarea name='assessment_task_description' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Task Description'></textarea></td>" +
            "<td><textarea name='notes' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Notes'></textarea></td>" +
            "<td><a href=\"#\" class=\"btn btn-primary\" id=\"save-plan\"><i class='fas fa-save'></i></a></td>" +
            "</tr>")
    })


    $('#concerns-table tbody').on('click', '#save-concerns', function (e) {
        e.preventDefault();

        let inputs = [$('#concerns-table tbody tr td textarea[name="notes"]')];

        for (i = 0; i < inputs.length; i++){
            //console.log();
            if (inputs[i].val() == '' && inputs[i].val() == 0){
                inputs[i].addClass( "is-invalid" );
                return false;
            }else {
                inputs[i].removeClass("is-invalid")
            }
        }

        axios.post('/add_assessment_concerns', {
            notes: $('#concerns-table tbody tr td textarea[name="notes"]').val(),
            assessment_id: $('input[name="assessment_id"]').val(),
        })
            .then(function (response) {
                $('#concerns-addition').hide();
                let url = '/assessment_concern/'+ response.data.id+'/edit';
                $('#concerns-table tbody').append("<tr>" +
                    "<td>" + response.data.id + "</td>" +
                    "<td>" + response.data.notes + "</td>" +
                    "<td><a href='"+url+"' class='btn btn-sm btn-success'><i class='fas fa-pencil-alt'></i></a> </td>" +
                    "</tr>")
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $("#concerns-plan").on('click', function () {
        $('#concerns-addition').remove();
        $('#concerns-table tbody').append("<tr id='concerns-addition'>" +
            "<td>&nbsp;</td>" +
            "<td><textarea name='notes' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Notes'></textarea></td>" +
            "<td><a href=\"#\" class=\"btn btn-primary\" id=\"save-concerns\"><i class='fas fa-save'></i></a></td>" +
            "</tr>")
    })

    $('#notes-table tbody').on('click', '#save-note', function (e) {
        e.preventDefault();

        let inputs = [$('#notes-table tbody tr td textarea[name="notes"]')];

        for (i = 0; i < inputs.length; i++){
            //console.log();
            if (inputs[i].val() == '' && inputs[i].val() == 0){
                inputs[i].addClass( "is-invalid" );
                return false;
            }else {
                inputs[i].removeClass("is-invalid")
            }
        }

        axios.post('/add_assessment_note', {
            notes: $('#notes-table tbody tr td textarea[name="notes"]').val(),
            assessment_id: $('input[name="assessment_id"]').val(),

        })
            .then(function (response) {
                $('#notes-addition').hide();
                let url = '/assessment_notes/'+ response.data.id+'/edit';
                $('#notes-table tbody').append("<tr>" +
                    "<td>" + response.data.id + "</td>" +
                    "<td>" + response.data.notes + "</td>" +
                    "<td><a href='"+url+"' class='btn btn-sm btn-success'><i class='fas fa-pencil-alt'></i></a> </td>" +
                    "</tr>")

            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $("#add-note").on('click', function () {
        $('#notes-addition').remove();
        $('#notes-table tbody').append("<tr id='notes-addition'>" +
            "<td>&nbsp;</td>" +
            "<td><textarea name='notes' class=\"form-control form-control-sm\" rows=\"2\" placeholder='Notes'></textarea></td>" +
            "<td><a href=\"#\" class=\"btn btn-primary\" id=\"save-note\"><i class='fas fa-save'></i></a></td>" +
            "</tr>")
    })
})