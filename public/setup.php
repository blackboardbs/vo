<?php
/**
 * Created by PhpStorm.
 * User: Klaas Rikhotso
 * Date: 23/04/2019
 * Time: 16:41
 */

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="company.csv"');

$_REQUEST['company_name'] = 'New Company2';
$_REQUEST['user_first_name'] = 'Klaas';
$_REQUEST['user_last_name'] = 'Rikhotso';
$_REQUEST['email'] = 'klaas@blackboardbs.com';
$_REQUEST['phone'] = '0848791089';

$company_name = $_REQUEST['company_name'];
$name = $_REQUEST['user_first_name'];
$surname = $_REQUEST['user_last_name'];
$email = $_REQUEST['email'];
$phone = $_REQUEST['phone'];

$user_CSV[0] = array('id', 'parent_company_id', 'company_name', 'business_reg_no', 'vat_no', 'contact_firstname', 'contact_lastname', 'phone', 'cell', 'fax', 'email', 'web', 'postal_address_line1', 'postal_address_line2', 'postal_address_line3', 'city_suburb', 'state_province', 'postal_zipcode', 'country_id', 'company_logo', 'bbbee_level_id', 'bbbee_certificate_expiry', 'black_ownership', 'black_female_ownership', 'black_voting_rights', 'black_female_voting_rights', 'bbbee_recognition_level', 'bank_name', 'branch_code', 'bank_acc_no', 'status_id', 'creator_id', 'created_at', 'updated_at');
$user_CSV[1] = array('1', '0', $company_name, '', '', $name, $surname, $phone, '', '', $email, '', '', 'NULL', 'NULL', '', '', '', '1', 'default.png', '4', date('Y-m-d'), '0.00', '0.00', '0.00', '0.00', '', '', '', '', '1', '1', date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

//$fp = fopen('php://output', 'wb');
$fp = fopen('../database/data/company.csv', 'wb');
foreach ($user_CSV as $line) {
    // though CSV stands for "comma separated value"
    // in many countries (including France) separator is ";"
    fputcsv($fp, $line, ';');
}
fclose($fp);

$user_CSV[0] = array('id','first_name','last_name','email','phone','password','avatar','resource_id','vendor_id','customer_id','company_id','expiry_date','status_id','calendar_view','active_office_id','remember_token','created_at','updated_at');
$user_CSV[1] = array('1',$name,$surname,$email,'+27714029622','$2y$10$M/jyf7S4pL4Q8dIuzysuZexCV31j487Jh8kr7aPUZNAlhnmWc.Y2O','1.png','1','0','0','1','2030-08-01','1','task,leave,assignment,anniversary,cale,user,cinvoice,vinvoice,certificates','0','zZyDK4sZ0vu2gfS7TKZqTaHMgpboktT3Q0b2EoaDEO1hJvlODGppzVjjISmE','2018-08-15 07:13:52','2018-11-03 09:08:56');

//$fp = fopen('php://output', 'wb');
$fpa = fopen('../database/data/users.csv', 'wb');
foreach ($user_CSV as $line) {
    // though CSV stands for "comma separated value"
    // in many countries (including France) separator is ";"
    fputcsv($fpa, $line, ';');
}
fclose($fpa);

//echo exec('cd ..');
//echo exec('C:\Workspance\bo\php artisan migrate:fresh --seed');

echo "User and company data setup complete";