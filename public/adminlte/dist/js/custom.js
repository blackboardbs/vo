$(function() {

    let url = "/get_ajax";
    let date = new Date();
    let period = $("#v_period");
    let m = date.getMonth() + 1;
    let y = date.getFullYear();
    let from = $('#from-value').datepicker('getDate');
    let toValue = $("#to-value").datepicker('getDate');

    $("#v_consultant").on('change', function () {
        let consultant_id = $(this).val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {'_token': $('input[name=_token]').val(), 'consultant_id': consultant_id},
            success: function (response) {
                //console.log(response);
                $.each(response, function (key, value) {
                     let wbs = $("#v_wbs");
                     $.each(value, function (index, objTitle) {
                         console.log(objTitle);
                         if (jQuery.isEmptyObject(objTitle)){
                             $('#wbs_td').append('<span class="is-invalid">Selected Consultant has no projects</span>');
                         }else{
                             wbs.append('<option value="' + objTitle.project.id +'">' + objTitle.project.name + '</option>').fadeIn();
                         }


                     });
                 });
            }
        });

    });

    $("#v_wbs").on('change', function () {
        let wbs_id = $(this).val();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {'_token': $('input[name=_token]').val(), 'wbs_id': wbs_id},
            success: function (response) {
                console.log(response);
                $.each(response, function (key, value) {
                    let wbs = $("#v_wbs");
                    $.each(value, function (index, objTitle) {
                        //console.log(objTitle);
                        if (objTitle.month < 10){
                            period.append('<option value="' + objTitle.year  + objTitle.month +'">' + objTitle.year + '0' + objTitle.month + '</option>').fadeIn();
                        }else{
                            period.append('<option value="' + objTitle.year + objTitle.month +'">' + objTitle.year + objTitle.month + '</option>').fadeIn();
                        }


                    });
                });
            }
        });

    });

    $("#add_ass_cost_account").on('change', function () {
        let account = $(this).val();
        $.ajax({
            url: "/get_account_element",
            type: 'POST',
            dataType: 'json',
            data: {'_token': $('input[name=_token]').val(), 'account': account},
            success: function (response) {
                //console.log(response.account_element);
                $.each(response, function (key, value) {
                    let account_element = $("#ass_cost_account_element");
                    $.each(value, function (index, objTitle) {
                        console.log(objTitle);
                        account_element.append('<option value="' + objTitle.id + '">' + objTitle.description + ' (' + objTitle.account_element + ') </option>').fadeIn();
                    });
                });
            }
        });

    });

    $("#timesheet_client").on('change', function () {
        let client = $(this).val();
        $.ajax({
            url: "/get_timesheet",
            type: 'POST',
            dataType: 'json',
            data: {'_token': $('input[name=_token]').val(), 'client': client},
            success: function (response) {
                console.log(response);
                $.each(response, function (key, value) {
                    let timesheet_project = $("#timesheet_project");
                    $.each(value, function (index, objTitle) {
                        console.log(objTitle);
                        timesheet_project.append('<option value="' + objTitle.id + '">' + objTitle.name + '</option>').fadeIn();
                    });
                });
            }
        });

    });

    $("#expense_account").on('change', function () {
        let account = $(this).val();
        $.ajax({
            url: "/get_timesheet",
            type: 'POST',
            dataType: 'json',
            data: {'_token': $('input[name=_token]').val(), 'account': account},
            success: function (response) {
                //console.log(response);
                $.each(response, function (key, value) {
                    let timesheet_project = $("#expense_element");
                    $.each(value, function (index, objTitle) {
                        //console.log(objTitle);
                        timesheet_project.append('<option value="' + objTitle.id + '">' + objTitle.description + '</option>').fadeIn();
                    });
                });
            }
        });

    });


    /*
    * This code was replaced by the ajax code above i didn't want
    * to delete it in case i had to rollback
    */

/*    while (m > 0 && m < 13) {
        let mm =(m < 10) ? "0" + m : m;
        let ym = y +''+ mm;
        period.append('<option value="' + ym + '">' + ym + '</option>');
        m = m - 1;
    }
    m = 12;
    y = y - 1;
    while (m > 0 && m < 13) {
        let mm = (m < 10) ? "0" + m : m;
        let ym = y + ''+ mm;
        period.append('<option value="' + ym + '">' + ym + '</option>');
        m = m - 1;
    }*/

    /*$('#generate').on('click', () => {
        alert(from);
        return false;
        /!*if((period.val() == 0) || (from.length == 0 && toValue.length == 0)){
            $("#period_err").append('<p class="invalid-feedback">Select period</p>');
            return false;
        }*!/

    })*/

    $('#filter_week').on('change', function () {
        $('#filters').submit();
    });

    $('#filter_project').on('change', function () {
        $('#filters').submit();
    });
    $('#filter_employee').on('change', function () {
        $('#filters').submit();
    });
    $('#filter_vendor').on('change', function () {
        $('#filters').submit();
    });

});