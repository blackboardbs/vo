$(function () {
  'use strict'

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true

  var $salesChart = $('#sales-chart')
  var salesChart  = new Chart($salesChart, {
    type   : 'bar',
    data   : {
      labels  : ['JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
      datasets: [
        {
          backgroundColor: '#007bff',
          borderColor    : '#007bff',
          data           : [1000, 2000, 3000, 2500, 2700, 2500, 3000]
        },
        {
          backgroundColor: '#ced4da',
          borderColor    : '#ced4da',
          data           : [700, 1700, 2700, 2000, 1800, 1500, 2000]
        },
          {
              backgroundColor: '#9954BB',
              borderColor    : '#9954BB',
              data           : [600, 1300, 2000, 1700, 1600, 1300, 1700]
          }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              if (value >= 1000) {
                value /= 1000
                value += 'k'
              }
              return 'R' + value
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })

    var $utilsChart = $('#util-chart')
    var utilChart  = new Chart($utilsChart, {
        type   : 'bar',
        data   : {
            labels  : ['5', '10', '15', '30', '30+'],
            datasets: [
                {
                    backgroundColor: '#007bff',
                    borderColor    : '#007bff',
                    data           : [9, -2, 10, 5, -5]
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            tooltips           : {
                mode     : mode,
                intersect: intersect
            },
            hover              : {
                mode     : mode,
                intersect: intersect
            },
            legend             : {
                display: false
            },
            scales             : {
                yAxes: [{
                    // display: false,
                    gridLines: {
                        display      : true,
                        lineWidth    : '4px',
                        color        : 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks    : $.extend({
                        beginAtZero: true,

                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            if (value <= 100) {
                                value += 'M'
                            }
                            return '' + value
                        }
                    }, ticksStyle)
                }],
                xAxes: [{
                    display  : true,
                    gridLines: {
                        display: false
                    },
                    ticks    : ticksStyle
                }]
            }
        }
    })

  var $visitorsChart = $('#visitors-chart')
  var visitorsChart  = new Chart($visitorsChart, {
      type: 'bar',
      data: {
          datasets: [{
              label: 'Bar Dataset',
              data: [70, 140, 200, 170, 180, 160, 190],
              backgroundColor: '#007bff',
              borderColor    : '#007bff',

          },{
              label: 'Bar Dataset',
              data: [40, 100, 170, 140, 130, 80, 100],
              backgroundColor: '#ced4da',
              borderColor    : '#ced4da',

          },{
              label: 'Line Dataset',
              data: [100, 120, 170, 167, 180, 177, 160],
              backgroundColor     : 'transparent',
              borderColor         : '#007bff',
              pointBorderColor    : '#007bff',
              pointBackgroundColor: '#007bff',
              fill                : false,


              // Changes this dataset to become a line
              type: 'line'
          },{
              label: 'Line Dataset',
              data: [60, 80, 70, 67, 80, 77, 100],
              backgroundColor     : 'transparent',
              borderColor         : '#ced4da',
              pointBorderColor    : '#ced4da',
              pointBackgroundColor: '#ced4da',
              fill                : false,


              // Changes this dataset to become a line
              type: 'line'
          }],
          labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL']
      },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 200
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })

    var $expiredUsers = $('#expiredUsers');

    var expiredData = {
        labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
        datasets: [{
            label: "Car Speed (mph)",
            data: [0, 59, 75, 20, 20, 55, 40],
        }]
    };

    var chartOptions = {
        legend: {
            display: true,
            position: 'top',
            labels: {
                boxWidth: 0,
                fontColor: 'black'
            }
        }
    };

    var expiredChart = new Chart($expiredUsers, {
        type: 'line',
        data: expiredData,
        options: chartOptions
    });
})
