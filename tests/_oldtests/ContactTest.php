<?php

namespace Tests\Feature;

use App\Models\InvoiceContact;
use App\Models\MessageBoard;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private InvoiceContact $contact;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\InvoiceContact";

        $this->user = User::factory()->create();

        $this->contact = InvoiceContact::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('contact.index'));

        $response->assertForbidden();
    }

    public function test_list_of_contacts_can_be_retrieved(): void
    {

        InvoiceContact::factory()->count(4)->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('contact.index'));

        $response->assertOk();
        $response->assertSee('Contact');

        $this->assertDatabaseCount(InvoiceContact::class, 5);
    }

    public function test_list_of_contacts_can_be_downloaded(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('contact.index', ['export' => 1]));

        $response->assertDownload();
    }

    public function test_create_contact_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('contact.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_contact_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('contact.create'));

        $response->assertOk();
        $response->assertSee('Add Contact');
    }

    public function test_store_contact()
    {
        $auth_user = $this->loggedInUser($this->module);

        $contact = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->safeEmail(),
            'contact_number' => $this->faker->phoneNumber(),
            'birthday' => $this->faker->dateTimeBetween('-50 years', '-18 years')->format('Y-m-d'),
            'status_id' => Status::factory()->create()->id,
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('contact.store'), $contact);

        $response->assertRedirect();

        $this->assertDatabaseHas(InvoiceContact::class, $contact);
    }

    public function test_show_contact_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('contact.show', $this->contact->id));

        $response->assertForbidden();
    }

    public function test_show_contact(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('contact.show', $this->contact->id));

        $response->assertOk();
        $response->assertSee('View Contact');
    }

    public function test_can_access_edit_contact_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('contact.edit', $this->contact->id));

        $response->assertOk();
        $response->assertSee('Edit Contact');
    }

    public function test_contact_can_be_updated(): void
    {
        $this->contact->first_name = "John";
        $this->contact->last_name = "Doe";

        $contact = Arr::except($this->contact->toArray(), ['full_name', 'created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('contact.update', $this->contact->id), $contact);

        $response->assertRedirect();

        $this->assertDatabaseHas(InvoiceContact::class, $contact);
    }

    public function test_contact_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('contact.destroy', $this->contact->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(InvoiceContact::class, ['id' => $this->contact->id]);
    }
}
