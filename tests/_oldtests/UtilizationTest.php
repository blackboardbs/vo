<?php

namespace Tests\Feature;

use App\Models\Status;
use App\Models\User;
use App\Models\Utilization;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class UtilizationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private Utilization $utilization;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\PlannedUtilization";

        $this->user = User::factory()->create();

        $this->utilization = Utilization::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('utilization.index'));

        $response->assertForbidden();
    }

    public function test_list_of_utilization_can_be_retrieved(): void
    {
        Utilization::factory()->count(4)->create();
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('utilization.index'));

        $response->assertOk();
        $response->assertSee('Utilization');

        $this->assertDatabaseCount(Utilization::class, 5);
    }

    public function test_utilization_create_form_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('utilization.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_utilization_per_week_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('utilization.create', ['type' => 'week']));

        $response->assertOk();
        $response->assertSee('Add Utilization');
    }

    public function test_can_access_create_utilization_plan_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('utilization.create', ['type' => 'plan']));

        $response->assertOk();
        $response->assertSee('Add Utilization');
    }

    public function test_store_utilization()
    {
        $auth_user = $this->loggedInUser($this->module);

        $utilization = [
            'yearwk' => now()->addWeek(rand(1,100))->format("YW"),
            'res_no' => 3,
            'wk_hours' => 40,
            'cost_res_no' => 0,
            'cost_wk_hours' => 0,
            'status_id' => Status::factory()->create()->id,
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('utilization.store'), $utilization);

        $response->assertRedirect();

        $this->assertDatabaseHas(Utilization::class, $utilization);

    }

    public function test_store_utilization_plan()
    {
        $auth_user = $this->loggedInUser($this->module);

        $utilization = [
            'start_date' => now()->addWeek(4)->toDateString(),
            'end_date' => now()->addMonths(5)->toDateString(),
            'res_no' => 3,
            'wk_hours' => 40,
            'cost_res_no' => 0,
            'cost_wk_hours' => 0,
            'status_id' => Status::factory()->create()->id,
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('utilization.store'), $utilization);

        $response->assertRedirect();
    }

    public function test_can_access_edit_utilization_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('utilization.edit', $this->utilization->id));

        $response->assertOk();
        $response->assertSee('Edit Utilization');
    }

    public function test_utilization_can_be_updated(): void
    {
        $this->utilization->wk_hours = 55;
        $utilization = Arr::except($this->utilization->toArray(), ['created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('utilization.update', $this->utilization->id), $utilization);

        $response->assertRedirect();

        $this->assertDatabaseHas(Utilization::class, $utilization);
    }

    public function test_show_utilization(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('utilization.show', $this->utilization->id));

        $response->assertOk();
        $response->assertSee('View Utilization');
    }

    public function test_utilization_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('utilization.destroy', $this->utilization->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(Utilization::class, ['id' => $this->utilization->id]);
    }
}
