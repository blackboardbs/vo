<?php

namespace Tests\Feature;

use App\Models\Anniversary;
use App\Models\InvoiceContact;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class AnniversaryTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private Anniversary $anniversary;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\Anniversary";

        $this->user = User::factory()->create();

        $this->anniversary = Anniversary::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('anniversary.index'));

        $response->assertForbidden();
    }

    public function test_list_of_anniversaries_can_be_retrieved(): void
    {

        Anniversary::factory()->count(4)->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('anniversary.index'));

        $response->assertOk();
        $response->assertSee('Anniversary');

        $this->assertDatabaseCount(Anniversary::class, 5);
    }

    public function test_list_of_anniversaries_can_be_downloaded(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('anniversary.index', ['export' => 1]));

        $response->assertDownload();
    }

    public function test_create_anniversary_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('anniversary.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_anniversary_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('anniversary.create'));

        $response->assertOk();
        $response->assertSee('Add Anniversary');
    }

    public function test_store_anniversary()
    {
        $auth_user = $this->loggedInUser($this->module);

        $user = User::factory()->create();

        $anniversary = [
            'anniversary' => $this->faker->word(3, true),
            'emp_id' => $user->id,
            'name' => $user->name(),
            'email' => $user->email,
            'phone' => $user->phone,
            'contact_email' => $this->faker->safeEmail(),
            'contact_phone' => $this->faker->phoneNumber(),
            'relationship' => $this->faker->word(),
            'anniversary_date' => now()->toDateString(),
            'status_id' => Status::factory()->create()->id
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('anniversary.store'), $anniversary);

        $response->assertRedirect();

        $this->assertDatabaseHas(Anniversary::class, $anniversary);
    }

    public function test_show_anniversary_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('anniversary.show', $this->anniversary->id));

        $response->assertForbidden();
    }

    public function test_show_anniversary(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('anniversary.show', $this->anniversary->id));

        $response->assertOk();
        $response->assertSee('View Anniversary');
    }

    public function test_can_access_edit_anniversary_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('anniversary.edit', $this->anniversary->id));

        $response->assertOk();
        $response->assertSee('Edit Anniversary');
    }

    public function test_anniversary_can_be_updated(): void
    {
        $this->anniversary->name = "John Doe";

        $anniversary = Arr::except($this->anniversary->toArray(), ['created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('anniversary.update', $this->anniversary->id), $anniversary);

        $response->assertRedirect();

        $this->assertDatabaseHas(Anniversary::class, $anniversary);
    }

    public function test_anniversary_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('anniversary.destroy', $this->anniversary->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(Anniversary::class, ['id' => $this->anniversary->id]);
    }
}
