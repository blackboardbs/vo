<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\Customer;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\CustomerProfile";

        $this->user = User::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('customer.index'));

        $response->assertForbidden();
    }

    public function test_list_of_customers_can_be_retrieved(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('customer.index'));

        $response->assertOk();
        $response->assertSee('Customer');
    }

    public function test_list_of_customers_can_be_downloaded(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('customer.index', ['export' => 1]));

        $response->assertDownload();
    }

    public function test_create_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('customer.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('customer.create'));

        $response->assertOk();
        $response->assertSee('Add Customer');
    }

    public function test_store_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('customer.store'));

        $response->assertForbidden();
    }

    public function test_store_customer()
    {
        $auth_user = $this->loggedInUser($this->module);
        $customer = [
            'customer_name' => $this->faker->company(),
            'account_manager' => User::factory()->create()->id,
            'contact_firstname' => $this->faker->firstName(),
            'contact_lastname' => $this->faker->lastName(),
            'country_id' => Country::factory()->create()->id,
            'status' => Status::factory()->create()->id,
        ];
        $response = $this->actingAs($auth_user)
            ->post(route('customer.store'), $customer);

        $response->assertRedirect();

        $this->assertDatabaseHas(Customer::class, $customer);

    }

    public function test_show_permission(): void
    {

        $response = $this->actingAs($this->user)
            ->get(route('customer.show', Customer::factory()->create()->id));

        $response->assertForbidden();
    }

    public function test_show_customer(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('customer.show', Customer::factory()->create()->id));

        $response->assertOk();
    }

    public function test_edit_customer(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('customer.edit', Customer::factory()->create()->id));

        $response->assertOk();
    }

    public function test_update_customer(): void
    {
        $customer = Customer::factory()->create();
        $update = [
            'customer_name' => "New Name",
            'account_manager' => $customer->account_manager,
            'contact_firstname' => $customer->contact_firstname,
            'contact_lastname' => $customer->contact_lastname,
            'country_id' => $customer->country_id,
            'status' => $customer->status,
        ];

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('customer.update', $customer->id), $update);

        $response->assertRedirect();

        $this->assertDatabaseHas(Customer::class, $update);
    }

    public function test_delete_customer(): void
    {
        $customer = Customer::factory()->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('customer.destroy', $customer->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(Customer::class, ['id' => $customer->id]);
    }
}
