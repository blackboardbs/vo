<?php

namespace Tests\Feature;

use App\Models\MessageBoard;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class MessageBoardTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private MessageBoard $message;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\MessageBoard";

        $this->user = User::factory()->create();

        $this->message = MessageBoard::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('messages.index'));

        $response->assertForbidden();
    }

    public function test_list_of_messages_can_be_retrieved(): void
    {

        MessageBoard::factory()->count(4)->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('messages.index'));

        $response->assertOk();
        $response->assertSee('Messages');

        $this->assertDatabaseCount(MessageBoard::class, 5);
    }

    public function test_create_message_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('messages.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_message_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('messages.create'));

        $response->assertOk();
        $response->assertSee('Add Message');
    }

    public function test_store_message()
    {
        $auth_user = $this->loggedInUser($this->module);

        $message = [
            'message' => $this->faker->paragraph(),
            'message_date' => $this->faker->dateTimeBetween()->format("Y-m-d"),
            'status_id' => Status::factory()->create()->id
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('messages.store'), $message);

        $response->assertRedirect();

        $this->assertDatabaseHas(MessageBoard::class, $message);
    }

    public function test_show_message_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('messages.show', $this->message->id));

        $response->assertForbidden();
    }

    public function test_show_message(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('messages.show', $this->message->id));

        $response->assertOk();
        $response->assertSee('View Message');
    }

    public function test_can_access_edit_message_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('messages.edit', $this->message->id));

        $response->assertOk();
        $response->assertSee('Edit Message');
    }

    public function test_message_can_be_updated(): void
    {
        $this->message->message .= " This is an edited description";
        $message = Arr::except($this->message->toArray(), ['created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('messages.update', $this->message->id), $message);

        $response->assertRedirect();

        $this->assertDatabaseHas(MessageBoard::class, $message);
    }

    public function test_message_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('messages.destroy', $this->message->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(MessageBoard::class, ['id' => $this->message->id]);
    }
}
