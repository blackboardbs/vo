<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private string $module;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\User";
    }

    public function test_that_user_without_a_role_does_not_have_access()
    {
        $response = $this->actingAs(User::factory()->create())
            ->get(route('users.index'));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     */
    public function test_that_a_list_of_users_can_be_retrieved(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('users.index'));

        $response->assertStatus(200);
        $response->assertSee('Users');
    }

    public function test_create_form_can_be_accessed(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('users.create'));

        $response->assertStatus(200);
        $response->assertSee("Create User");
    }

    /*public function test_a_new_user_can_be_stored(): void
    {

    }*/
}
