<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountElement;
use App\Models\Company;
use App\Models\PlannedExpense;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class PlannedExpenseTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private PlannedExpense $planned_expense;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\PlannedExpense";

        $this->user = User::factory()->create();

        $this->planned_expense = PlannedExpense::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('plan_exp.index'));

        $response->assertForbidden();
    }

    public function test_list_of_planned_expenses_can_be_retrieved(): void
    {

        PlannedExpense::factory()->count(4)->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('plan_exp.index'));

        $response->assertOk();
        $response->assertSee('Planned Expenses');

        $this->assertDatabaseCount(PlannedExpense::class, 5);
    }

    public function test_list_of_planned_expenses_can_be_downloaded(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('plan_exp.index', ['export' => 1]));

        $response->assertDownload();
    }

    public function test_create_planned_expense_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('plan_exp.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_planned_expense_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('plan_exp.create'));

        $response->assertOk();
        $response->assertSee('Add Planned Expense');
    }

    public function test_store_planned_expenses()
    {
        $auth_user = $this->loggedInUser($this->module);

        $planned_expense = [
            'description' => $this->faker->words(rand(1,5), true),
            'plan_exp_value' => rand(100, 100000),
            'plan_exp_date' => now()->addMonths(rand(1,6))->toDateString(),
            'status' => Status::factory()->create()->id,
            'account_id' => Account::factory()->create()->id,
            'account_element_id' => AccountElement::factory()->create()->id,
            'company_id' => Company::factory()->create()->id
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('plan_exp.store'), $planned_expense);

        $response->assertRedirect();

        $this->assertDatabaseHas(PlannedExpense::class, $planned_expense);
    }

    public function test_show_planned_expense(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('plan_exp.show', $this->planned_expense->id));

        $response->assertOk();
        $response->assertSee('View Planned Expense');
    }

    public function test_can_access_edit_planned_expense_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('plan_exp.edit', $this->planned_expense->id));

        $response->assertOk();
        $response->assertSee('Edit Planned Expense');
    }

    public function test_planned_expenses_can_be_updated(): void
    {
        $this->planned_expense->description = "This is an edited description";
        $planned_expenses = Arr::except($this->planned_expense->toArray(), ['created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('plan_exp.update', $this->planned_expense->id), $planned_expenses);

        $response->assertRedirect();

        $this->assertDatabaseHas(PlannedExpense::class, $planned_expenses);
    }

    public function test_planned_expense_can_be_copied(): void
    {
        $data = [
            'start_date' => now()->format("Y-m"),
            'end_date' => now()->addMonths(6)->format("Y-m")
        ];

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->post(route('plan_exp.copy'), $data);

        $response->assertRedirect();
    }

    public function test_planned_expense_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('plan_exp.destroy', $this->planned_expense->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(PlannedExpense::class, ['id' => $this->planned_expense->id]);
    }
}
