<?php

namespace Tests\Feature;

use App\Models\BillingCycle;
use App\Models\Status;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class BillingCycleTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private BillingCycle $billing_cycle;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\BillingCycle";

        $this->user = User::factory()->create();

        $this->billing_cycle = BillingCycle::factory()->create();
    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('billing.index'));

        $response->assertForbidden();
    }

    public function test_list_of_billing_cycle_can_be_retrieved(): void
    {

        BillingCycle::factory()->count(4)->create();

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('billing.index'));

        $response->assertOk();
        $response->assertSee('Billing Cycle');

        $this->assertDatabaseCount(BillingCycle::class, 5);
    }

    public function test_create_billing_cycle_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('billing.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_billing_cycle_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('billing.create'));

        $response->assertOk();
        $response->assertSee('Add Billing Cycle');
    }

    public function test_store_billing_cycle()
    {
        $auth_user = $this->loggedInUser($this->module);

        $description = $this->faker->words(3, true);

        $billing_cycle = [
            'name' => $description,
            'description' => $description,
            'max_weeks' => 5,
            'status_id' => Status::factory()->create()->id
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('billing.store'), $billing_cycle);

        $response->assertRedirect();

        $this->assertDatabaseHas(BillingCycle::class, $billing_cycle);
    }

    public function test_show_billing_cycle_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('billing.show', $this->billing_cycle->id));

        $response->assertForbidden();
    }

    public function test_show_billing_cycle(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('billing.show', $this->billing_cycle->id));

        $response->assertOk();
        $response->assertSee('View Billing Cycle');
    }

    public function test_can_access_edit_billing_cycle_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('billing.edit', $this->billing_cycle->id));

        $response->assertOk();
        $response->assertSee('Edit Billing Cycle');
    }

    public function test_billing_cycle_can_be_updated(): void
    {
        $this->billing_cycle->description = "This is an edited description";
        $billing_cycle = Arr::except($this->billing_cycle->toArray(), ['created_at', 'updated_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('billing.update', $this->billing_cycle->id), $billing_cycle);

        $response->assertRedirect();

        $this->assertDatabaseHas(BillingCycle::class, $billing_cycle);
    }

    public function test_billing_cycle_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('billing.destroy', $this->billing_cycle->id));

        $response->assertOk();

        $this->assertSoftDeleted(BillingCycle::class, ['id' => $this->billing_cycle->id]);
    }

    public function test_billing_cycle_can_be_copied(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->post(route('billing.duplicate', $this->billing_cycle->id));

        $response->assertRedirect();
    }
}
