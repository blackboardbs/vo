<?php

namespace Tests\Feature;

use App\Models\Status;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Tests\TestCase;

class VendorTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private string $module;
    private User $user;

    private Vendor $vendor;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

        $this->module = "App\\Models\\VendorProfile";

        $this->user = User::factory()->create();

        $this->vendor = Vendor::factory()->create();


    }

    public function test_user_without_permission_cannot_access(): void
    {
        $response = $this->actingAs($this->user)->get(route('vendors.index'));

        $response->assertForbidden();
    }

    public function test_list_of_vendors_can_be_retrieved(): void
    {
        Vendor::factory()->count(4)->create();
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('vendors.index'));

        $response->assertOk();
        $response->assertSee('Vendor');

        $this->assertDatabaseCount(Vendor::class, 5);
    }

    public function test_list_of_vendors_can_be_downloaded(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('vendors.index', ['export' => 1]));

        $response->assertDownload();
    }

    public function test_create_permission(): void
    {
        $response = $this->actingAs($this->user)
            ->get(route('vendors.create'));

        $response->assertForbidden();
    }

    public function test_can_access_create_vendor_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('vendors.create'));

        $response->assertOk();
        $response->assertSee('Add Vendor');
    }

    public function test_store_vendor()
    {
        $auth_user = $this->loggedInUser($this->module);

        $vendor = [
            'vendor_name' => $this->faker->company(),
            'business_reg_no' => str(Str::random(12))->upper()->toString(),
            'vat_no' => str(Str::random(12))->upper()->toString(),
            'contact_firstname' => $this->faker->firstName(),
            'contact_lastname' => $this->faker->lastName(),
            'contact_birthday' => $this->faker->dateTimeBetween('-40 years', '-18 years')->format("Y-m-d"),
            'phone' => $this->faker->phoneNumber(),
            'cell' => $this->faker->phoneNumber(),
            'email' => $this->faker->safeEmail(),
            'account_manager' => $this->user->id,
            'assignment_approver' => $this->user->id,
            'status_id' => Status::factory()->create()->id,
            'creator_id' => $auth_user->id,
            'invoice_contact_id' => $this->user->id,
            'payment_terms_days' => 30
        ];

        $response = $this->actingAs($auth_user)
            ->post(route('vendors.store'), $vendor);

        $response->assertRedirect();

        $this->assertDatabaseHas(Vendor::class, $vendor);

    }

    public function test_show_vendor(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('vendors.show', $this->vendor->id));

        $response->assertOk();
    }

    public function test_can_access_edit_vendor_form(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->get(route('vendors.edit', $this->vendor->id));

        $response->assertOk();
        $response->assertSee('Edit Vendor');
    }

    public function test_vendor_can_be_updated(): void
    {
        $this->vendor->vendor_name = "Edited Name";
        $vendor = Arr::except($this->vendor->toArray(), ['onboarding_vendor', 'creator_id', 'updated_at', 'created_at']);

        $response = $this->actingAs($this->loggedInUser($this->module))
            ->patch(route('vendors.update', $this->vendor->id), $vendor);

        $response->assertRedirect();

        $this->assertDatabaseHas(Vendor::class, $vendor);
    }

    public function test_vendor_can_be_deleted(): void
    {
        $response = $this->actingAs($this->loggedInUser($this->module))
            ->delete(route('vendors.destroy', $this->vendor->id));

        $response->assertRedirect();

        $this->assertDatabaseMissing(Vendor::class, ['id' => $this->vendor->id]);
    }
}
