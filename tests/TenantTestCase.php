<?php

namespace Tests;


use App\Models\Tenant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;
use Stancl\Tenancy\Database\Models\Domain;
use Tests\Setup\AdminUserSetup;

abstract class TenantTestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseTransactions;
    use AdminUserSetup;

    public string $database;

    protected function setUp(): void
    {
        parent::setUp();
        $this->initializeTenancy();
    }

    protected function tearDown(): void
    {
        $this->dropTenantDatabase();
        parent::tearDown();
    }

    public function initializeTenancy()
    {
        $tenant = Tenant::create();

        $this->database = 'tenant'.$tenant->id;

        Domain::updateOrCreate(
            ['domain' => 'tenancy.localhost'],
            ['tenant_id' => $tenant->id]
        );

        tenancy()->initialize($tenant);
    }

    private function dropTenantDatabase(): void
    {
        if ($this->database) {
            DB::statement("DROP DATABASE IF EXISTS `{$this->database}`");
        }
    }
}
