<?php

namespace Tests;

use App\Enum\Ability;
use App\Enum\Status;
use App\Models\Module;
use App\Models\SimplePermissions;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createModule(string $module): Module
    {
        return Module::create([
            'name' => $module,
            'status_id' => 1
        ]);
    }

    public function simplePermission(int $role_id, array $ability, int $module_id): void
    {
        $simple_permissions = new SimplePermissions();
        $simple_permissions->module_id = $module_id;
        $simple_permissions->role_id = $role_id;
        $simple_permissions->creator_id = Auth::id();
        $simple_permissions->status_id = Status::ACTIVE->value;
        $simple_permissions->list_all_records = $ability[0];
        $simple_permissions->list_own_and_team_records = $ability[1];
        $simple_permissions->show_all_records = $ability[2];
        $simple_permissions->show_own_and_team_records = $ability[3];
        $simple_permissions->save();
    }

    public function loggedInUser(string $module): User
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $role = Role::first();
        $user->assignRole($role?->name);

        $module = $this->createModule($module);
        $this->simplePermission($role->id, [Ability::FULL->value, Ability::FULL->value, Ability::FULL->value, Ability::FULL->value], $module->id);

        return $user;
    }
}
