<?php

it('checks if the relationship with the resource exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    expect($expense->resource)->toBeInstanceOf(\App\Models\User::class);
});

it('checks if the relationship with the status exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    expect($expense->status)->toBeInstanceOf(\App\Models\Status::class);
});

it('checks if the relationship with the account exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    expect($expense->account)->toBeInstanceOf(\App\Models\Account::class);
});

it('checks if the relationship with the approver exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create(['approved_by' => \App\Models\User::factory()->create()->id]);

    expect($expense->approver)->toBeInstanceOf(\App\Models\User::class);
});

it('checks if the relationship with cost center exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create(['cost_center' => \App\Models\CostCenter::factory()->create()->id]);

    expect($expense->cost_center_description)->toBeInstanceOf(\App\Models\CostCenter::class);
});

it('checks if the relationship with company exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create(['company_id' => \App\Models\Company::factory()->create()->id]);

    expect($expense->company)->toBeInstanceOf(\App\Models\Company::class);
});

it('checks if the relationship with expense type exists', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create(['expense_type_id' => \App\Models\ExpenseType::factory()->create()->id]);

    expect($expense->expense_type)->toBeInstanceOf(\App\Models\ExpenseType::class);
});

