<?php

use Spatie\Permission\Models\Role;
use \Symfony\Component\HttpFoundation\Response;

beforeEach(function (){
    Role::insert($this->readCsv('roles.csv'));
});

it('redirects to login form if not logged in', function () {
    $response = $this->get(route('users.index'));

    $response->assertRedirect(route('login'));
});

it('aborts if the user does not have permission', function () {
    $response = login()->get(route('users.index'));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can access users list', function (){
    $response = login(module: "App\\Models\\User")->get(route('users.index'));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);
});

it('can access create user page', function(){
    $response = login(module: "App\\Models\\User")->get(route('users.create'));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);
});

it('validates required fields', function (){

    $user = [
        'first_name' => '',
        'last_name' => '',
        'email' => '',
        'role' => '',
        'login_user' => '',
        'onboarding' => 1,
        'usertype_id' => '',
        'expiry_date' => ''
    ];

    $response = login(module: "App\\Models\\User")->post(route('users.store'), $user);

    $response->assertInvalid(['first_name', 'last_name', 'email', 'role', 'usertype_id', 'expiry_date']);

    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('validates that appointment manager is required with user type', function(){
    $user = [
        'usertype_id' => 1,
        'appointment_manager_id' => ''
    ];

    $response = login(module: "App\\Models\\User")->post(route('users.store'), $user);

    $response->assertInvalid(['appointment_manager_id']);

    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('validates that email field must be an email', function(){
    $response = login(module: "App\\Models\\User")->post(route('users.store'), ['email' => fake()->word()]);
    $response->assertInvalid(['email' => 'valid email address']);
    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('creates a user with required fields only', fn() => createUser());

it('creates a user with all the fields', function (){
    $fields = [
        'phone' => fake()->phoneNumber(),
        'company_id' => 1,
        'vendor_id' => 1,
        'customer_id' => 1,
        'avatar' => \Illuminate\Http\UploadedFile::fake()->image('avatar.jpg', 250,250),
    ];

    createUser($fields);
});

it('validates that email must be unique', function (){
    $user = \App\Models\User::factory()->create();

    $fields = [
        'first_name' => fake()->firstName(),
        'last_name' => fake()->lastName(),
        'email' => $user->email,
        'role' => [1,2,3],
        'login_user' => 1,
        'onboarding' => 1,
        'usertype_id' => \App\Models\UserType::factory()->create()->id,
        'appointment_manager_id' => \App\Models\User::first()->id??1,
        'expiry_date' => now()->addYear(),
    ];

    $response = login(module: "App\\Models\\User")->post(route('users.store'), $fields);

    $response->assertInvalid(['email']);
});

it('can access edit user page', function(){
    $user = \App\Models\User::factory()->create();
    $response = login(module: "App\\Models\\User")->get(route('users.edit', $user));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);
});

it('can update a user', function (){
    $user = \App\Models\User::factory()->create();
    $name = fake()->firstName();

    $response = login(module: "App\\Models\\User")->patch(route('users.update', $user), [
        'first_name' => $name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'role' => [1,2,3],
        'status' => $user->status_id,
    ]);

    expect($user->refresh()->first_name)->toBe($name)
        ->and($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('can delete a user', function (){
    $user = \App\Models\User::factory()->create(['status_id' => \App\Enum\Status::ACTIVE->value]);

    $response = login(module: "App\\Models\\User")->get(route('users.destroy', $user->id));

    $suspended = \App\Models\User::find($user->id);

    expect($suspended->status_id)->toBe(\App\Enum\Status::SUSPENDED->value)
        ->and($suspended->login_user)->toBe(\App\Enum\YesOrNoEnum::No->value)
        ->and($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

function createUser(array $fields = []): void
{
    \App\Models\Config::insert(test()->readCsv('configs.csv'));

    $user = [
        'first_name' => fake()->firstName(),
        'last_name' => fake()->lastName(),
        'email' => fake()->safeEmail(),
        'role' => [1,2,3],
        'login_user' => 1,
        'onboarding' => 1,
        'usertype_id' => \App\Models\UserType::factory()->create()->id,
        'appointment_manager_id' => \App\Models\User::first()->id??1,
        'expiry_date' => now()->addYear(),
    ];

    $response = login(module: "App\\Models\\User")->post(route('users.store'), [...$user, ...$fields]);

    test()->assertDatabaseHas('users', [
        'email' => $user['email'],
    ]);

    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
}