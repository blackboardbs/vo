<?php

use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

beforeEach(function () {
    Role::insert($this->readCsv('roles.csv'));
    $this->module = "App\\Models\\ExpenseTracking";
});

it('aborts if it doesn\'t have permission' , function () {
    $response = login()->get(route('exptracking.index'));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can view the list of expenses with permission', function () {
    $response = login(module: $this->module)->get(route('exptracking.index'));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);

    $response->assertSee('Expense Tracking');
});

it('aborts if it tries to access create expense form without permission', function () {
    $response = login()->get(route('exptracking.create'));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can access create expense form with permission', function () {
    $response = login(module: $this->module)->get(route('exptracking.create'));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);

    $response->assertSee('Add Expense Tracking');
});

it('aborts if it tries to store a record without permission', function () {

    $fields = [
        'account' => \App\Models\Account::factory()->create()->id,
        'account_element' => \App\Models\AccountElement::factory()->create()->id,
        'description' => fake()->words(5, true),
        'company' => \App\Models\Company::factory()->create()->id,
        'exp_date' => now()->format('Y-m-d'),
        'amount' => fake()->numberBetween(10),
        'resource' => \App\Models\User::first()->id,
        'status' => \App\Models\Status::factory()->create()->id,
    ];

    $response = login()->post(route('exptracking.store'), $fields);

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('enforces required fields validation rules', function () {
    $fields = [
        'account' => '',
        'account_element' => '',
        'description' => '',
        'company' => '',
        'exp_date' => '',
        'amount' => '',
        'resource' => '',
        'status' => ''
    ];

    $response = login(module: $this->module)->post(route('exptracking.store'), $fields);

    $response->assertInvalid(array_keys($fields));

    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('requires year week if the request has an assignment', function () {
    $fields = [
        'assignment' => \App\Models\Assignment::factory()->create()->id,
    ];

    $response = login(module: $this->module)->post(route('exptracking.store'), $fields);

    $response->assertInvalid(['yearwk']);

    expect($response->getStatusCode())->toBe(Response::HTTP_FOUND);
});

it('store an expense with required fields only', function () {

    $fields = requiredFields();

    $response = login(module: $this->module)->post(route('exptracking.store'), $fields);

    $expense = [
        'account_id' => $fields['account'],
        'account_element_id' => $fields['account_element'],
        'description' => $fields['description'],
        'company_id' => $fields['company'],
        'transaction_date' => $fields['exp_date'],
        'amount' => $fields['amount'],
        'employee_id' => $fields['resource'],
        'status_id' => $fields['status'],
        'approval_status' => 1,
        'expense_type_id' => $fields['expense_type_id']
    ];

    expect(\App\Models\ExpenseTracking::where($expense)->count())->toBe(1);

    $response->assertRedirect(route('exptracking.index'));
});

it('can store a file', function () {

    $fields = requiredFields();
    $fields['document'] = UploadedFile::fake()->create('document.pdf', 100);

    $response = login(module: $this->module)->post(route('exptracking.store'), $fields);

    $response->assertRedirect(route('exptracking.index'));

});

it('aborts if it tries to access the show page without permission', function () {

    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login()->get(route('exptracking.show', $expense));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can access show expense page with permission', function () {

    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login(module: $this->module)->get(route('exptracking.show', $expense));

    $response->assertSee('View Expense Tracking');

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);
});

it('aborts if it tries to access the edit form without permission', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login()->get(route('exptracking.edit', $expense));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can access edit form with permission', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login(module: $this->module)->get(route('exptracking.edit', $expense));

    expect($response->getStatusCode())->toBe(Response::HTTP_OK);

    $response->assertSee('Edit Expense Tracking');
});

it('aborts if it tries to an expense without permission', function () {

    $fields = requiredFields();

    $expense = \App\Models\ExpenseTracking::create([
        'account_id' => $fields['account'],
        'account_element_id' => $fields['account_element'],
        'description' => $fields['description'],
        'company_id' => $fields['company'],
        'transaction_date' => $fields['exp_date'],
        'amount' => $fields['amount'],
        'employee_id' => $fields['resource'],
        'status_id' => $fields['status'],
        'approval_status' => 1,
        'expense_type_id' => $fields['expense_type_id']
    ]);

    $description = fake()->words(5, true);

    $expenseUpdate = [
        'account' => $expense->account_id,
        'account_element' => $expense->account_element_id,
        'description' => $description,
        'company' => $expense->company_id,
        'exp_date' => $expense->transaction_date,
        'amount' => $expense->amount,
        'resource' => $expense->employee_id,
        'status' => $expense->status_id,
    ];



    $response = login()->patch(route('exptracking.update', $expense), $expenseUpdate);

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);

});

it('can update an expense record with permission', function () {

    $fields = requiredFields();

    $expense = \App\Models\ExpenseTracking::create([
        'account_id' => $fields['account'],
        'account_element_id' => $fields['account_element'],
        'description' => $fields['description'],
        'company_id' => $fields['company'],
        'transaction_date' => $fields['exp_date'],
        'amount' => $fields['amount'],
        'employee_id' => $fields['resource'],
        'status_id' => $fields['status'],
        'approval_status' => 1,
        'expense_type_id' => $fields['expense_type_id']
    ]);

    $description = fake()->words(5, true);

    $expenseUpdate = [
        'account' => $expense->account_id,
        'account_element' => $expense->account_element_id,
        'description' => $description,
        'company' => $expense->company_id,
        'exp_date' => $expense->transaction_date,
        'amount' => $expense->amount,
        'resource' => $expense->employee_id,
        'status' => $expense->status_id,
    ];

    $response = login(module: $this->module)->patch(route('exptracking.update', $expense), $expenseUpdate);

    expect(\App\Models\ExpenseTracking::where('description', $description)->count())->toBe(1);

    $response->assertRedirect(route('exptracking.index'));

});

it('aborts if it tries to delete an expense without permission', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login()->delete(route('exptracking.destroy', $expense));

    expect($response->getStatusCode())->toBe(Response::HTTP_FORBIDDEN);
});

it('can delete an expense with permission', function () {
    $expense = \App\Models\ExpenseTracking::factory()->create();

    $response = login(module: $this->module)->delete(route('exptracking.destroy', $expense));

    expect(\App\Models\ExpenseTracking::find($expense->id))->toBeNull();

    $response->assertRedirect(route('exptracking.index'));
});

function requiredFields(): array
{
    return [
        'account' => \App\Models\Account::factory()->create()->id,
        'account_element' => \App\Models\AccountElement::factory()->create()->id,
        'description' => fake()->words(5, true),
        'company' => \App\Models\Company::factory()->create()->id,
        'exp_date' => now()->format('Y-m-d'),
        'amount' => fake()->numberBetween(10),
        'resource' => \App\Models\User::first()->id,
        'status' => \App\Models\Status::factory()->create()->id,
        'approval_status' => 1,
        'expense_type_id' => 1
    ];
}