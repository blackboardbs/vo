<?php

use Spatie\Permission\Models\Role;

beforeEach(function (){
    Role::insert($this->readCsv('roles.csv'));
});

it('redirects to login form if not logged in', function () {
    $response = $this->get('/');

    $response->assertRedirect(route('login'));
});

it('redirects to dashboard if logged in', function () {
    $response = login()->get('/');

    $response->assertRedirect(route('dashboard.index'));

    $this->followRedirects($response)
        ->assertStatus(200);
});
