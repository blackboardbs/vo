<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | Here you can change the setting for mail web
    | MAILWEB_LIMIT - limit the amount of messages stored in the db
    |
    */
    'MAILWEB_ENABLED' => env('MAILWEB_ENABLED', false),
    'MAILWEB_LIMIT' => env('MAILWEB_LIMIT', 20),
    'MAILWEB_TOOLBAR' => [
        'LARGE_SCREEN' => env('MAILWEB_LARGE_SCREEN', false),
        'MEDIUM_SCREEN' => env('MAILWEB_MEDIUM_SCREEN', false),
        'SMALL_SCREEN' => env('MAILWEB_SMALL_SCREEN', false),
        'HTML' => env('MAILWEB_HTML', false),
        'MARKDOWN' => env('MAILWEB_MARKDOWN', false),
    ],
];
